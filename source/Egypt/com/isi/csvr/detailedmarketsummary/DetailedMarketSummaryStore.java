package com.isi.csvr.detailedmarketsummary;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: rajeevp
 * Date: Aug 20, 2010
 * Time: 10:20:27 AM
 */
public class DetailedMarketSummaryStore implements ExchangeListener {
    private static DetailedMarketSummaryStore store = null;
    private Hashtable<String, DetailedMarketSummary> mktSStore = new Hashtable<String, DetailedMarketSummary>();
    public static ArrayList<String> boards = new ArrayList<String>();
    static String exchange = "CASE";

    public DetailedMarketSummaryStore() {
        ExchangeStore.getSharedInstance().addExchangeListener(this);
    }

    public static DetailedMarketSummaryStore getSharedInstance() {
        if (store == null)
            store = new DetailedMarketSummaryStore();

        return store;
    }

    public void addMktSummary(String id, DetailedMarketSummary dms) {
        try {
            if (!mktSStore.containsKey(id)) {
                mktSStore.put(id, dms);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeMktSummary(String id) {
        try {
            mktSStore.remove(id);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public DetailedMarketSummary getDetailedMarketSummaryIfAvailable(String id) {
        if (mktSStore.containsKey(id))
            return mktSStore.get(id);
        return null;
    }

    public DetailedMarketSummary getDetailedMktSummary(String id) {
        if (mktSStore.containsKey(id)) {
            return mktSStore.get(id);
        } else {
            String[] param = id.split(DetailedMarketSummary.KEY_SEPARATOR);
            DetailedMarketSummary dms = new DetailedMarketSummary(param[0], param[1]);
            addMktSummary(id.toUpperCase(), dms);
            return dms;
        }
    }


    //Exchange Listner Methods
    public void exchangeAdded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeRemoved(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeUpgraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeDowngraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangesAdded(boolean offlineMode) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangesLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        try {
            mktSStore.clear();
            DetailedMarketSummaryWindow.clearDMSummaries();
            DetailedMarketSummaryWindow.executeDataScript(-5, false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exchangeMasterFileLoaded(String exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTradingInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
