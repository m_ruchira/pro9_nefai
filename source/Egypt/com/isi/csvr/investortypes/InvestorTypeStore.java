package com.isi.csvr.investortypes;

import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 16, 2009
 * Time: 12:23:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class InvestorTypeStore {

    private static InvestorTypeStore store;

    private Hashtable<String, ArrayList<InvestorInfo>> investorHashTable = new Hashtable<String, ArrayList<InvestorInfo>>();

    protected static final String KEY_SEPERATOR = "~";

    public static InvestorTypeStore getSharedInstance() {
        if (store == null) {
            store = new InvestorTypeStore();
        }
        return store;
    }

    public InvestorInfo createInvestorInfoObject(String exchange, byte investorType) {
        return getInvestorInfoObject(exchange, investorType);

    }

    public String getKey(String exchange, byte investorType) {

        return exchange + KEY_SEPERATOR + investorType;
    }

    public InvestorInfo getInvestorInfoObject(String exchange, byte investorType) {

        if (exchange == null) {
            return null;
        }

        InvestorInfo info = null;
        ArrayList<InvestorInfo> list = investorHashTable.get(exchange);

        if (list == null) {
            list = new ArrayList<InvestorInfo>();
            info = new InvestorInfo(exchange, investorType);
            list.add(info);

            investorHashTable.put(exchange, list);
            return info;
        }

        if (!isObjectAlreadyExists(list, exchange, investorType)) {  // adding a new object
            info = new InvestorInfo(exchange, investorType);
            list.add(info);
            return info;
        } else {  //object already exists. should update the object
            return getInvestorObjectInArrayList(list, exchange, investorType);
        }
    }

    private boolean isObjectAlreadyExists(ArrayList list, String exchange, byte investorType) {

        for (int i = 0; i < list.size(); i++) {
            InvestorInfo info = (InvestorInfo) list.get(i);

            if (info.getExchange().equalsIgnoreCase(exchange) && info.getType() == investorType) {
                return true;
            }
        }

        return false;
    }

    private InvestorInfo getInvestorObjectInArrayList(ArrayList list, String exchange, byte investorType) {

        for (int i = 0; i < list.size(); i++) {
            InvestorInfo info = (InvestorInfo) list.get(i);

            if (info.getExchange().equalsIgnoreCase(exchange) && info.getType() == investorType) {
                return info;
            }
        }

        return null;
    }

    public ArrayList<InvestorInfo> getDataForExchange(String exchange) {
            if (investorHashTable != null) {
                return investorHashTable.get(exchange);
            } else {
                return null;
            }
    }

    public void fireDataUpdated() {
        InvestorTypeWindow.getSharedInstance().populateData();
    }


}
