package com.isi.csvr.investortypes;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.table.UpdateableTable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Feb 14, 2009
 * Time: 11:11:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class PrimaryInvestorChart extends JPanel implements ComponentListener, MouseListener, MouseMotionListener, UpdateableTable, Themeable {

    private ArrayList<InvestorBarModel> info = new ArrayList<InvestorBarModel>();

    private int yAxisWidth = 40;
    private int xAxisHeight = 40;

    private int chartWidth = 450;
    private int chartHeight = 250; //these are the effective width and height of the chart
    private int legendHeight = 10;

    //colors
    private Color gridColor;
    private Color chartBackColor;
    private Color chartOutsideColor;    
    private Color chartAxisFontColor;
    private Color barLeftColor;
    private Color barRightColor;
    private Color barRightDomColor;
    private Color barRightArabColor;
    private Color barRightForeignColor;
    private Color chartBorderColor;

    private double[] vals;

    private double minY = Double.MAX_VALUE;
    private double maxY = -Double.MIN_VALUE;

    private double xFactor;
    private double yFactor;

    private double bestXPitch = 10;
    private double bestYPitch = 100;
    private double xStart = 1;
    private double yStart = 1;

    private double adj = 10;
    private String legend;
    private int barWidth = 10;

    private final float BAR_WIDTH_FACTOR = .3f;
    private final float[] PriceVolPitch = {1f, 2f, 4f, 5f, 8f};
    private final float ALPHA_TRANSPARENT = .2f;
    private boolean isLeftToRight = true;

    private Font axisFont = new Font("Arial", Font.BOLD, 10);
    private Font xAxisFont = new Font("Arial", Font.BOLD, 11);
    private Font legendFont = new Font("Arial", Font.BOLD, 11);

    private TWDecimalFormat formatter = new TWDecimalFormat("###,###,##0");
    private TWDecimalFormat priceFormatter = new TWDecimalFormat("###,###,##0.00");
    private TWDecimalFormat pctFormatter = new TWDecimalFormat("#0.00");
    private float[] dashArr = new float[]{3, 6, 3, 6};

    private int DIVIDER = 10;
    private Font yAxisFont = new Font("Arial", Font.PLAIN, 11);
    private int HGAP = 0;

    private final String strBuy = Language.getString("BUY");
    private final String strSell = Language.getString("SELL");

    public PrimaryInvestorChart(ArrayList<InvestorBarModel> info) {

        this.info = info;
        this.addComponentListener(this);
        Theme.registerComponent(this);
        this.setToolTipText("");

        gridColor = Theme.getColor("INV_BAR_CHART_GRID_COLOR");
        chartBackColor = Theme.getColor("INV_BAR_CHART_BACK_COLOR");
        chartAxisFontColor = Theme.getColor("INV_BAR_CHART_AXIS_FONT_COLOR");
        chartBorderColor = Theme.getColor("INV_BAR_CHART_BORDER_COLOR");
        chartOutsideColor = Theme.getColor("INV_BOTTOM_COLOR");//Theme.getColor("INV_BAR_CHART_OUTSIDE_COLOR");
        barLeftColor = Theme.getColor("INV_BAR_CHART_LEFT_COLOR");

        barRightDomColor = Theme.getColor("INV_BAR_CHART_RIGHT_DOM_COLOR");
        barRightArabColor = Theme.getColor("INV_BAR_CHART_RIGHT_ARAB_COLOR");
        barRightForeignColor = Theme.getColor("INV_BAR_CHART_RIGHT_FOREIGN_COLOR");

        if (!Language.isLTR()) {
            isLeftToRight = false;
        }
    }

    public void calculatePixelValues() {

        minY = Double.MAX_VALUE;
        maxY = -Double.MIN_VALUE;
        chartWidth = Math.max(this.getWidth() - yAxisWidth - 10, 100);
        chartHeight = Math.max((this.getHeight() - xAxisHeight - legendHeight), 80);

        if (info == null || info.size() == 0) {
            return;
        }

        int number = info.size() + 1; //TODO:IMPORTANT
        HGAP = (chartWidth - (2 * DIVIDER)) / number;  //Horizontal gap between two horizonal lines

        vals = new double[info.size()];
        for (int i = 0; i < info.size(); i++) {
            vals[i] = info.get(i).getValue();
        }

        for (double value : vals) {
            minY = Math.min(minY, value);
            maxY = Math.max(maxY, value);
        }

        adj = (maxY - minY) * 0.2f;
        maxY = 110; //hardcodede here. no problem...

        minY = (minY > 0) ? 0 : minY;

        yFactor = chartHeight / ((maxY - minY));
        bestYPitch = getBestYInterval(maxY - minY, chartHeight);
        yStart = 0; //hardcoded to zero since y basis must start from zero always.          
    }

    public void paint(Graphics g) {
        super.paint(g);

        //g.setColor(Color.BLACK);
        g.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
        g.setColor(chartOutsideColor);
        g.fillRect(1, 1, this.getWidth() - 2, this.getHeight() - 2);

        if (info == null || info.size() == 0) {

            //drawing the empty chart here in offline mode..
            g.setColor(chartBackColor);
            g.fillRect(yAxisWidth, legendHeight, chartWidth, chartHeight);

            drawOfflineXAxisLabels(g);
            drawOfflineYAxisLabels(g);
            drawOfflineLegend(g);

            g.setColor(chartBorderColor);
            g.drawRect(yAxisWidth, legendHeight, chartWidth, chartHeight);

            return;
        }

        g.setColor(chartBackColor);
        g.fillRect(yAxisWidth, legendHeight, chartWidth, chartHeight);

        drawYAxisLabels(g);
        drawXAxisLabels(g);
        drawBars(g);
        drawLegend(g);

        g.setColor(chartBorderColor);
        g.drawRect(yAxisWidth, legendHeight, chartWidth, chartHeight);

    }

    private void drawBars(Graphics g) {

        //barWidth = (int) (HGAP * 2.0 * BAR_WIDTH_FACTOR);
        //int xStart = (int) (yAxisWidth + (HGAP * (1 - BAR_WIDTH_FACTOR)));
        int xStart = yAxisWidth + DIVIDER;
        int zeroPos = (int) getPixelForYValue(0);

        Graphics2D g2 = (Graphics2D) g;
        g2.setStroke(new BasicStroke(1.2f));

        for (int i = 0; i < info.size(); i++) {
            InvestorBarModel investorBar = info.get(i);
            double value = investorBar.getValue();
            if (value > 0) {

                if (investorBar.getType() == Meta.INVESTER_TYPE_DOMESTIC) {
                    barLeftColor = InvestorTypeWindow.getSharedInstance().getDomesticColor();
                    barRightColor = barRightDomColor;
                } else if (investorBar.getType() == Meta.INVESTER_TYPE_ARAB) {
                    barLeftColor = InvestorTypeWindow.getSharedInstance().getArabicColor();
                    barRightColor = barRightArabColor;
                } else if (investorBar.getType() == Meta.INVESTER_TYPE_FOREIGN) {
                    barLeftColor = InvestorTypeWindow.getSharedInstance().getForeignColor();
                    barRightColor = barRightForeignColor;
                }
                GradientPaint greentowhite = new GradientPaint(xStart, 0, barLeftColor, xStart + HGAP, 0, barRightColor);
                g2.setPaint(greentowhite);

                int ht = (int) ((value - 0) * yFactor);
                int barTop = zeroPos - ht;
                g.fillRect(xStart, barTop, HGAP, ht);
            }
            if (i == (info.size() / 2) - 1) {
                xStart = xStart + 2 * HGAP;
            } else {
                xStart = xStart + HGAP;
            }
        }
    }

    private void drawYAxisLabels(Graphics g) {

        float labelLength = 0;
        float labelHeight = 0;
        g.setColor(this.getForeground());
        //double currVal = yStart;    //TODO:
        double currVal = 0;
        float x = yAxisWidth - 10;
        float yAdj = 0;

        Graphics2D g2D = (Graphics2D) g;
        g.setFont(yAxisFont);
        while (currVal <= maxY) {
            float y = getPixelForYValue(currVal);

            g.setColor(gridColor);
            //g2D.setStroke(new BasicStroke(1.2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 0, dashArr, 0));
            g.drawLine(yAxisWidth, (int) y, (yAxisWidth + chartWidth), (int) y);

            g.setColor(chartAxisFontColor);
            String strVal = "";
            if (currVal <= 100) {
                strVal = formatter.format(currVal) + "%";
            }
            labelLength = g.getFontMetrics(yAxisFont).stringWidth(strVal);
            g.setColor(chartAxisFontColor);
            g.drawString(strVal, (int) (x - labelLength), (int) (y));
            g.drawLine(yAxisWidth, (int) y, yAxisWidth - 3, (int) y);
            currVal += bestYPitch;
        }
    }

    public void drawXAxisLabels(Graphics g) {

        int xStart = yAxisWidth + DIVIDER + HGAP / 2;

        g.setFont(xAxisFont);
        for (int i = 0; i < info.size(); i++) {
            InvestorBarModel investorBar = info.get(i);
            String val = pctFormatter.format(investorBar.getValue());
            int labelLen = g.getFontMetrics(xAxisFont).stringWidth(val);
            g.setColor(chartAxisFontColor);
            g.drawLine(xStart, legendHeight + chartHeight, xStart, legendHeight + chartHeight + 8);
            g.drawString(val + "%", xStart - (labelLen) / 2, (legendHeight + chartHeight + 20));

            g.setColor(gridColor);
            //((Graphics2D) g).setStroke(new BasicStroke(1.2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 0, dashArr, 0));

            g.drawLine(xStart, legendHeight, xStart, (legendHeight + chartHeight)); //TODO: horizontal values
            g.drawLine(xStart, (legendHeight + chartHeight), xStart, (legendHeight + chartHeight));

            if (i == info.size() / 2 - 1) {

                g.setColor(chartBorderColor);
                ((Graphics2D) g).setStroke(new BasicStroke(1.2f));
                //((Graphics2D) g).setStroke(new BasicStroke(1.2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 0, dashArr, 0));
                xStart += HGAP;
                g.drawLine(xStart, legendHeight, xStart, (legendHeight + chartHeight));
                xStart += HGAP;

            } else {
                ((Graphics2D) g).setStroke(new BasicStroke(1.2f));
                xStart += HGAP;
            }
        }
    }

    public void drawLegend(Graphics g) {

        g.setColor(chartAxisFontColor);
        g.setFont(xAxisFont);

        int y = (int) getPixelForYValue(100);

        int len = g.getFontMetrics(legendFont).stringWidth(strBuy);
        g.drawString(strBuy, yAxisWidth + (chartWidth / 2 - len) / 2, y - 5);

        len = g.getFontMetrics(legendFont).stringWidth(strSell);
        g.drawString(strSell, yAxisWidth + chartWidth / 2 + (chartWidth / 2 - len) / 2, y - 5);

    }

    private double getBestYInterval(double val, float pixLen) {

        double HALF_INCH = 72 / 3d;
        val = val / pixLen * HALF_INCH;

        float pitch = PriceVolPitch[0];
        for (long j = 1; j < Long.MAX_VALUE; j *= 10) {
            float Multiplier = ((float) j / 1000000000);
            for (float aPriceVolPitch : PriceVolPitch) {
                if (val >= (double) Multiplier * aPriceVolPitch) {
                    pitch = (Multiplier * aPriceVolPitch);
                } else {
                    return pitch;
                }
            }
            if (j > Long.MAX_VALUE / 10f) {
                return pitch;
            }
        }
        return pitch;
    }

    private float getPixelForYValue(double value) {
        return chartHeight + legendHeight - (float) ((value - minY) * yFactor);
    }

    private double getStart(double min, double bestPitch) {
        return bestPitch * Math.round(min / bestPitch);
    }

    public void componentResized(ComponentEvent e) {
        calculatePixelValues();
        repaint();
    }

    public void componentMoved(ComponentEvent e) {

    }

    public void componentShown(ComponentEvent e) {

    }

    public void componentHidden(ComponentEvent e) {

    }

    public void mouseClicked(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mouseDragged(MouseEvent e) {

    }

    public void mouseMoved(MouseEvent e) {


    }

    public void runThread() {

    }

    public void removeThread() {

    }

    public String getTitle() {
        return "bar chart updator";
    }

    public Constants.ThreadTypes getTableID() {
        return Constants.ThreadTypes.DEFAULT;
    }

    public void applyTheme() {
        gridColor = Theme.getColor("INV_BAR_CHART_GRID_COLOR");
        chartBackColor = Theme.getColor("INV_BAR_CHART_BACK_COLOR");
        chartAxisFontColor = Theme.getColor("INV_BAR_CHART_AXIS_FONT_COLOR");
        chartBorderColor = Theme.getColor("INV_BAR_CHART_BORDER_COLOR");
        chartOutsideColor = Theme.getColor("INV_BOTTOM_COLOR");//Theme.getColor("INV_BAR_CHART_OUTSIDE_COLOR");
        barLeftColor = Theme.getColor("INV_BAR_CHART_LEFT_COLOR");
        barRightDomColor = Theme.getColor("INV_BAR_CHART_RIGHT_DOM_COLOR");
        barRightArabColor = Theme.getColor("INV_BAR_CHART_RIGHT_ARAB_COLOR");
        barRightForeignColor = Theme.getColor("INV_BAR_CHART_RIGHT_FOREIGN_COLOR");
    }

    public void setFormatter(byte decimalPlaces) {

        if (decimalPlaces == 2) {
            priceFormatter.applyPattern("###,###,###.00");
        } else if (decimalPlaces == 3) {
            priceFormatter.applyPattern("###,###,###.000");
        } else if (decimalPlaces == 4) {
            priceFormatter.applyPattern("###,###,###.0000");
        }
    }

    public void setInfo(ArrayList<InvestorBarModel> info) {
        this.info = info;
    }

    public void drawOfflineXAxisLabels(Graphics g) {

        int number = 7;
        HGAP = (chartWidth - 2 * DIVIDER) / number;

        int xStart = yAxisWidth + DIVIDER + HGAP / 2;
        g.setFont(xAxisFont);
        for (int i = 0; i < 6; i++) {
            g.setColor(chartAxisFontColor);
            g.drawLine(xStart, legendHeight + chartHeight, xStart, legendHeight + chartHeight + 6);
            //g.drawString(val + " %", xStart - (labelLen) / 2, (legendHeight + chartHeight + 20));

            g.setColor(gridColor);
            //((Graphics2D) g).setStroke(new BasicStroke(1.2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 0, dashArr, 0));

            g.drawLine(xStart, legendHeight, xStart, (legendHeight + chartHeight)); //TODO: horizontal values
            g.drawLine(xStart, (legendHeight + chartHeight), xStart, (legendHeight + chartHeight));

            if (i == (number / 2) - 1) {

                g.setColor(chartBorderColor);
                ((Graphics2D) g).setStroke(new BasicStroke(1.2f));
                //((Graphics2D) g).setStroke(new BasicStroke(1.2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 0, dashArr, 0));
                xStart += HGAP;
                g.drawLine(xStart, legendHeight, xStart, (legendHeight + chartHeight));
                xStart += HGAP;

            } else {
                ((Graphics2D) g).setStroke(new BasicStroke(1.2f));
                xStart += HGAP;
            }
        }
    }


    public void drawOfflineYAxisLabels(Graphics g) {

        g.setColor(this.getForeground());
        double currVal = 0;
        int labelLength;
        float x = yAxisWidth - 10;
        float yAdj = 0;

        Graphics2D g2D = (Graphics2D) g;
        g.setFont(yAxisFont);
        maxY = 110;
        minY = 0;
        while (currVal <= maxY) {
            float y = chartHeight + legendHeight - (float) ((currVal - minY) * (chartHeight / maxY));

            g.setColor(gridColor);
            //g2D.setStroke(new BasicStroke(1.2f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 0, dashArr, 0));
            g.drawLine(yAxisWidth, (int) y, (yAxisWidth + chartWidth), (int) y);

            g.setColor(chartAxisFontColor);

            String strVal = "";
            if (currVal <= 100) {
                strVal = formatter.format(currVal) + "%";
            }
            labelLength = g.getFontMetrics(yAxisFont).stringWidth(strVal);
            g.setColor(chartAxisFontColor);
            //g.drawString(strVal, (int) (x - labelLength), (int) (y));

            g.setColor(chartAxisFontColor);
            g.drawLine(yAxisWidth, (int) y, yAxisWidth - 3, (int) y);
            currVal += 10;
        }
    }


    public void drawOfflineLegend(Graphics g) {

        g.setColor(chartAxisFontColor);
        g.setFont(xAxisFont);

        int y = (int) (chartHeight + legendHeight - ((100 - minY) * (chartHeight / maxY)));

        int len = g.getFontMetrics(legendFont).stringWidth(strBuy);
        g.drawString(strBuy, yAxisWidth + (chartWidth / 2 - len) / 2, y - 5);

        len = g.getFontMetrics(legendFont).stringWidth(strSell);
        g.drawString(strSell, yAxisWidth + chartWidth / 2 + (chartWidth / 2 - len) / 2, y - 5);

    }

}


