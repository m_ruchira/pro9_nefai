package com.isi.csvr.investortypes;

import com.isi.csvr.plugin.*;
import com.isi.csvr.plugin.event.*;
import com.isi.csvr.shared.*;
import com.isi.csvr.properties.ViewSettingsManager;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Feb 17, 2009
 * Time: 3:51:10 PM
 * To change this template use File | Settings | File Templates.
 */
public class InvestorTypesPlugin implements Plugin {

    public void load(Object... params) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void showUI(Object... params) {
        InvestorTypeWindow.getSharedInstance().setVisible(true);
    }

    public void save(Object... params) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isLoaded() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getCaption() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void unload(Object... params) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void pluginEvent(PluginEvent event, Object... data) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isDataNeeded() {
        return true;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setData(int id, Object... params) {                                                       
        try {
            if (id == Meta.MESSAGE_TYPE_INVEST) {
                InvestorInfo invInfo = InvestorTypeStore.getSharedInstance().createInvestorInfoObject((String) params[0], Byte.parseByte((String) params[1]));
                invInfo.setValues(((String) params[2]).split(Meta.FD));
                invInfo.downloadSummaryFile((String) params[0]);
                invInfo.loadHistoryData();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            System.out.println("********** setData in InvestorPlugin ************");
        }
    }                                              

    public void applyWorkspace(String id, String value) {
        if (id.equals(getViewSettingID())) {
            InvestorTypeWindow.getSharedInstance().setWorkspaceString(value);
        }
    }

    public String getWorkspaceString(String id) {
        if (id.equals(getViewSettingID())) {
            return InvestorTypeWindow.getSharedInstance().getWorkspaceString();
        }
        return null;
    }

    public String getViewSettingID() {
        return "" + ViewSettingsManager.INVESTER_WINDOW_VIEW;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getWindowTypeID() {
        return -1;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getWindowTypeCategory() {
        return Meta.WINDOW_TYPE_CATEGORY_INVALID;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
