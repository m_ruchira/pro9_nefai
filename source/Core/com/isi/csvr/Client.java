// Copyright (c) 2001 Integrated Systems International (ISI)
package com.isi.csvr;

import com.dfn.mtr.mix.beans.ChangePassword;
import com.dfn.mtr.mix.beans.MIXObject;
import com.dfn.mtr.mix.common.MIXConstants;
import com.isi.csvr.StretchedHeatMap.StretchedHeatMapframe;
import com.isi.csvr.TradeStation.TradeStationAutoUpdateFrame;
import com.isi.csvr.TradeStation.TradeStationHistoryUpdator;
import com.isi.csvr.TradeStation.TradeStationManager;
import com.isi.csvr.alert.AlertManager;
import com.isi.csvr.alert.AlertSetup;
import com.isi.csvr.announcement.*;
import com.isi.csvr.announcementnew.AnnouncementSearchWindow;
import com.isi.csvr.bandwidthoptimizer.MainUI;
import com.isi.csvr.bandwidthoptimizer.OptimizedTreeRenderer;
import com.isi.csvr.broadcastmessage.MessageWindow;
import com.isi.csvr.brokers.BrokersWindow;
import com.isi.csvr.brokers.TopBrokersWindow;
import com.isi.csvr.browser.BrowserFrame;
import com.isi.csvr.cachestore.Cacheble;
import com.isi.csvr.cachestore.CachedStore;
import com.isi.csvr.cashFlowWatch.CashFlowHistory;
import com.isi.csvr.cashFlowWatch.CashFlowHistoryRenderer;
import com.isi.csvr.cashFlowWatch.CashFlowWatcherUI;
import com.isi.csvr.chart.*;
import com.isi.csvr.chart.analysistechniques.AnalysTechRenderer;
import com.isi.csvr.chart.customindicators.IndicatorFactory;
import com.isi.csvr.chart.radarscreen.engine.RadarIndicatorFactory;
import com.isi.csvr.chart.radarscreen.ui.RadarScreenWindow;
import com.isi.csvr.chart.radarscreen.ui.RadarTableRenderer;
import com.isi.csvr.chat.ChatFrame;
import com.isi.csvr.communication.ConnectorFactory;
import com.isi.csvr.communication.ConnectorInterface;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.communication.tcp.TCPConnector;
import com.isi.csvr.communication.udp.Authenticator;
import com.isi.csvr.config.ConnectivityHandler;
import com.isi.csvr.customformular.CustomFormulaStore;
import com.isi.csvr.customformular.FormularEditor;
import com.isi.csvr.customindex.CustomIndexStore;
import com.isi.csvr.customizer.TableCuztomizerUI;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.*;
import com.isi.csvr.dde.DDEServer;
import com.isi.csvr.dnd.DraggableTable;
import com.isi.csvr.downloader.HistoryArchiveRegister;
import com.isi.csvr.downloader.HistoryDownloadManager;
import com.isi.csvr.downloader.HistoryDownloader;
import com.isi.csvr.downloader.TWUpdateDownloader;
import com.isi.csvr.entitlements.EntitlementRenderer;
import com.isi.csvr.entitlements.EntitlementsTree;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.financialcalender.FinancialCalenderStore;
import com.isi.csvr.financialcalender.FinancialCalenderWindow;
import com.isi.csvr.forex.CustomSettingsStore;
import com.isi.csvr.forex.ForexBoard;
import com.isi.csvr.forex.ForexBuySell;
import com.isi.csvr.globalIndex.CheckBoxNodeRenderer;
import com.isi.csvr.globalIndex.GlobalIndexStore;
import com.isi.csvr.globalIndex.GlobalIndexWindow;
import com.isi.csvr.globalMarket.ForexTableRenderer;
import com.isi.csvr.globalMarket.GlobalMarketRenderer;
import com.isi.csvr.globalMarket.GlobalMarketSummary;
import com.isi.csvr.globalMarket.GlobalMarketSummaryWindow;
import com.isi.csvr.heatmap.HeatMapFrame;
import com.isi.csvr.help.HelpManager;
import com.isi.csvr.help.TipOfDayWindow;
import com.isi.csvr.history.ExportDialog;
import com.isi.csvr.history.HistorySettings;
import com.isi.csvr.ie.BrowserManager;
import com.isi.csvr.ie.IEBrowser;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.indices.IndicesFrame;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.marketdepth.DepthCalculatorRenderer;
import com.isi.csvr.marketdepth.DepthCalculatorWindow;
import com.isi.csvr.marketdepth.DepthStore;
import com.isi.csvr.metastock.MetaStockAutoUpdateFrame;
import com.isi.csvr.metastock.MetaStockHistoryUpdator;
import com.isi.csvr.metastock.MetaStockManager;
import com.isi.csvr.middlewarehandler.MWMenuCreationListener;
import com.isi.csvr.middlewarehandler.MWMenuCreationNotifier;
import com.isi.csvr.middlewarehandler.MWTradingHandler;
import com.isi.csvr.mist.MISTModel;
import com.isi.csvr.mist.MISTRenderer;
import com.isi.csvr.mist.MISTTable;
import com.isi.csvr.mist.MISTTableCustomizerDialog;
import com.isi.csvr.mutualfund.MutualFundModel;
import com.isi.csvr.mutualfund.MutualFundPanel;
import com.isi.csvr.mutualfund.MutualFundStore;
import com.isi.csvr.netcalculator.CalcCellEditor;
import com.isi.csvr.netcalculator.ChangeRecord;
import com.isi.csvr.netcalculator.NetCalcModel;
import com.isi.csvr.netcalculator.NetCalcStore;
import com.isi.csvr.news.NewsProvidersStore;
import com.isi.csvr.news.NewsStore;
import com.isi.csvr.news.NewsWindow;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.ohlcbacklog.OHLCDownloaderUI;
import com.isi.csvr.options.OptionSymbolBuilder;
import com.isi.csvr.options.OptionWindow;
import com.isi.csvr.performaceHive.PerformaceHiveWindow;
import com.isi.csvr.plugin.PluginStore;
import com.isi.csvr.portfolio.*;
import com.isi.csvr.print.PrintManager;
import com.isi.csvr.properties.*;
import com.isi.csvr.radarscreen.RadarScreenDataLoader;
import com.isi.csvr.radarscreen.RadarScreenInterface;
import com.isi.csvr.regionalquotes.RQuoteWindow;
import com.isi.csvr.scanner.Results.ScannerRenderer;
import com.isi.csvr.scanner.ScanTreeRenderer;
import com.isi.csvr.scanner.scannerGUIs.MainScannerWindow;
import com.isi.csvr.sectormap.SectorMap;
import com.isi.csvr.sectoroverview.SectorOverviewRenderer;
import com.isi.csvr.sectoroverview.SectorOverviewUI;
import com.isi.csvr.sectoroverview.SectorOverviewUpdator;
import com.isi.csvr.shared.*;
import com.isi.csvr.shortcutkeys.ShortCutViewer;
import com.isi.csvr.sideBar.SideBar;
import com.isi.csvr.sideBar.SymbolNavigator;
import com.isi.csvr.sideBar.SymbolTreeRenderer;
import com.isi.csvr.smartalerts.SmartAlertWindow;
import com.isi.csvr.stockreport.StockReportWindow;
import com.isi.csvr.symbolfilter.FunctionBuilder;
import com.isi.csvr.symbolfilter.FunctionListManager;
import com.isi.csvr.symbolfilter.FunctionListStore;
import com.isi.csvr.symbolfilter.FunctionTableRenderer;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.symbolselector.CompanyCellEditor;
import com.isi.csvr.symbolselector.CompanyTableRenderer;
import com.isi.csvr.tabbedpane.TWTabEvent;
import com.isi.csvr.tabbedpane.TWTabbedPaleListener;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.*;
import com.isi.csvr.theme.TableThemeSettings;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.ticker.advanced.LowerTicker;
import com.isi.csvr.ticker.advanced.MiddleTicker;
import com.isi.csvr.ticker.advanced.PanelSetter;
import com.isi.csvr.ticker.advanced.UpperTicker;
import com.isi.csvr.ticker.custom.Ticker;
import com.isi.csvr.ticker.custom.TickerFrame;
import com.isi.csvr.ticker.custom.TradeFeeder;
import com.isi.csvr.topstocks.TopStocksRenderer;
import com.isi.csvr.topstocks.TopStocksWindow;
import com.isi.csvr.trade.*;
import com.isi.csvr.trade.ui.MarketTradeFilterUI;
import com.isi.csvr.trade.ui.TradeFilterUI;
import com.isi.csvr.tradebacklog.TradeBacklogStore;
import com.isi.csvr.tradebacklog.TradeDownloaderUI;
import com.isi.csvr.trading.AutoTradeLoginDaemon;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.*;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.portfolio.TradingPfDCRenderer;
import com.isi.csvr.trading.portfolio.TradingPortfolioRenderer;
import com.isi.csvr.trading.portfolio.TradingPortfolioWindow;
import com.isi.csvr.trading.shared.*;
import com.isi.csvr.trading.ui.*;
import com.isi.csvr.trading.ui.TransactionDialog;
import com.isi.csvr.trading.ui.orderentry.MiniTradingFrame;
import com.isi.csvr.util.*;
import com.isi.csvr.variationmap.VariationMapUpdator;
import com.isi.csvr.volumewatcher.VolumeWatcherUI;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;
import com.isi.csvr.wicalculator.WhatifCalculatorFrame;
import com.isi.csvr.win32.NativeMethods;
import com.isi.util.FlexFlowLayout;
import com.isi.util.FlexGridLayout;
import com.mubasher.win32.Win32IdleTime;
import com.mubasher.win32.Win32IdleTimeListener;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.plaf.metal.MetalLookAndFeel;
import javax.swing.table.TableColumnModel;
import javax.swing.table.TableModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;
import java.awt.print.PageFormat;
import java.awt.print.PrinterJob;
import java.beans.PropertyVetoException;
import java.io.*;
import java.math.BigInteger;
import java.text.NumberFormat;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.zip.ZipInputStream;

import static javax.swing.JOptionPane.INFORMATION_MESSAGE;

/**
 * A Swing-based top level window class. This is the main GUI component of the client.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class Client
        implements WindowListener, ViewConstants, ActionListener, PopupMenuListener, ApplicationListener, TWTabbedPaleListener, Win32IdleTimeListener, MWMenuCreationListener {

    private Table marketTable;
//    private Table submarketTable;

    private static boolean g_bShowDisconnectMessage = true;
    private static Client g_oInstance;
    private BorderLayout borderLayout1 = new BorderLayout(0, 0);
    public TWDesktop oTopDesktop = new TWDesktop();
    private JPanel g_oMarketWatchPanel = new JPanel();
    public JTree oTree;
    private static ClientTable g_oTempTable;
    private PortfolioTable g_oPortfolioTable;
    private MISTTable g_oMistTable;
    private ForexBoard g_oFXBoard;
    private SymbolNavigator g_oSideBar;
    private ClientTable oTable;
    private DefaultMutableTreeNode g_oTreeTop;
    private TreeRenderer g_oTreeRenderer;
    private DDEServer ddeServer;
    private Stock stockObj;
    private String symb;

    private IEBrowser jIE;

    private JPopupMenu g_oTablePopup = new JPopupMenu();
    private JPopupMenu hivePopup = new JPopupMenu();
    private TableMenuToolbar g_oTableTableMenuToolbar;
    private JPopupMenu g_oTableMultiPopup = new JPopupMenu();
    private JPopupMenu g_oFramePopup = new JPopupMenu();
    private JPopupMenu g_oWatchListPopup = new JPopupMenu();
    private DefaultMutableTreeNode g_oMyStocksTreeNode;
    private DefaultMutableTreeNode g_oFunctionTreeNode;
    private DefaultMutableTreeNode g_oMarketWatchTreeNode;
    private DefaultMutableTreeNode g_oSectorsTreeNode;
    private DefaultMutableTreeNode g_oPortfolioTreeNode;
    private DefaultMutableTreeNode g_oMISTTreeNode;
    private DefaultMutableTreeNode g_oForexTreeNode;

    private NetCalcStore g_oNetCalcStore;
    private TradeStore g_oTradeStore;
    private MapTodaysTrades g_oTodaysTrades;
    private MouseAdapter g_oSymbolMouseListener;
    private MouseAdapter g_oTableMouseListener;
    private TreePath g_oSelectedPath;
    private LogoManager g_oLogoManager;
    private ViewSettingsManager g_oViewSettings;
    private static long g_lLastEodDate;
    private boolean g_bTickerEnabled = true;
    private BrowserFrame g_oBrowserFrame;
    private String g_sWorkspaceFile = null;
    private ToolBar toolBar;

    static SimpleProgressBar lblStatus = new SimpleProgressBar();
    static SimpleProgressBar lblDownloadStatus = new SimpleProgressBar();
    static JLabel lblExpiryStatus = new JLabel();
    static JLabel loyaltyPoints = new JLabel();

    static PanelSetter pSetter = new PanelSetter();

    private StatusBar statusBarPanel;
    private JPanel tabPanel;
    private TWTabbedPane windowTabs;
    //private CWTabbedPaneUI tabbedPaneUI;
    boolean mainboardSelected;
    boolean portfolioSelected = false;
    boolean mistViewSelected = false;
    boolean forexViewSelected = false;
    boolean sideBarSelected = false;
    boolean volumeWatcherSelected = false;
    boolean cashFlowWatcherSelected = false;
    private Ticker g_oTicker;
    private UpperTicker g_uTicker;
    private MiddleTicker g_mTicker;
    private LowerTicker g_lTicker;
    private WindowDaemon g_oWindowDaemon;
    private TWMenuItem g_mnuTreePopEditView;
    private TWMenuItem mnuTreePopDelete;
    private TWMenuItem mnuTreePopDelete1;
    private TWMenuItem mnuColumnSettings;
    private TWMenuItem mnuColumnSettings1;
    private TWMenuItem mnuPrint;
    private TWMenuItem adjustWidths;
    private TWMenuItem mnuCustomize;

    private TWMenu mnuDecimalPlaces;
    private TWRadioButtonMenuItem mnuDefaultDecimal;
    private TWRadioButtonMenuItem mnuNoDecimal;
    private TWRadioButtonMenuItem mnuOneDecimal;
    private TWRadioButtonMenuItem mnuTwoDecimal;
    private TWRadioButtonMenuItem mnuThrDecimal;
    private TWRadioButtonMenuItem mnuFouDecimal;
//    TWMenuItem mnuDefaultDecimal;
//    TWMenuItem mnuNoDecimal;
//    TWMenuItem mnuOneDecimal;
//    TWMenuItem mnuTwoDecimal;
//    TWMenuItem mnuThrDecimal;
//    TWMenuItem mnuFouDecimal;

    /* Table popup menu */
    private TWMenuItem mnuHeatMap;
    private TWMenuItem mnuStretchedHeatMap;
    private TWMenuItem mnuAddSymbol;
    private TWMenuItem mnuTimeNSales;
    private TWMenuItem mnuStockReport;
    private TWMenuItem mnuCompanyPfofile;
    private TWMenuItem g_mnuOptionSymbolBuilder;
    private TWMenuItem mnuTimeNSalesSummary;
    private TWMenuItem mnuDTQ;
    private TWMenuItem mnuSnapQ;
    private TWMenuItem mnuCashFQ;
    private TWMenuItem mnuFullQ;
    private TWMenuItem mnuEnable;
    private TWMenuItem mnuDisable;

    private TWMenuItem mnuMiniTrade;

    //    TWMenuItem g_mnuTimeNSalesForView;
    //    TWMenuItem mnuFilteredTicker;
    private TWMenuItem mnuDepthSpecial;
    private TWMenuItem mnuDepthByOrder;
    private TWMenuItem mnuDepthByPrice;
    private TWMenuItem mnuDepthOddLot;
    private TWMenuItem mnuAddwebOrder;
    //TWMenuItem mnuCompanyProfile;
    private TWMenuItem mnuCompanyFinancials;
    private TWMenuItem mnuAnnouncementSearch;
    private TWMenuItem g_mnuFD2;
    private TWMenuItem mnuRQuotes;
    private TWMenuItem mnuCorporateActions;
    private TWMenuItem g_mnuStockReports;
    private TWMenuItem g_mnuForexBuySel;
    private TWMenuItem g_mnuOptions;
    private TWMenuItem g_mnuFutures;
    private TWMenuItem g_mnuNewsSearch;
    private TWMenuItem mnuRemoveSymbol;
    private TWMenuItem mnuGraph;
    private TWMenuItem mnuGraphInProChart;
    private TWMenuItem mnuGraphHive;
    private TWMenuItem mnuAlert;
    private TWMenuItem mnuExportIntraday;
    private TWMenuItem mnuSymbolFilter;
    private TWMenuItem mnuLink;
    private TWMenuItem mnuLinkH;
    private TWMenuItem mnuCopy;
    private TWMenuItem mnuAllEnable;
    private TWMenuItem mnuAllDisable;


    private TWSeparator separator;
    private TWMenuItem mnuCopyH;
    private TWMenuItem g_mnuMultiDelete;
    private TWMenuItem mnuUnsort;
    private TWMenuItem mnuTPCalc;
    //    TWMenu mnuProTrade;
    //    TWMenuItem mnuProSell;
    //    TWMenuItem mnuProBuy;
    private TWMenuItem mnuBuyCover;
    private TWMenuItem mnuSellCover;
    private TWMenu mnuTrade;
    private TWMenuItem mnuSell;
    private TWMenuItem mnuBuy;
    private TWMenuItem mnuBlockOrder;
    private TWMenuItem mnuScaleOrder;
    private TWMenuItem mnuQuickTrade;
    private TWMenuItem mnuVasStart;
    private TWMenuItem mnuVasEnd;

    private TWMenu mnuTradeHive;
    private TWMenuItem mnuSellHive;
    private TWMenuItem mnuBuyHive;
    private TWMenuItem mnuTimeNSalesHive;
    private TWMenuItem mnuCashFQHive;
    private TWMenuItem mnuDepthByPriceHive;
    private TWMenuItem mnuAlertHive;
    private String activeHiveSymbol = "";

    private TWMenuItem mnuSaveSettings;

    private TWMenu watchListPopup;
    //newly added
    private TWMenu addToWatchList;
    private TWMenu addToMistview;
    private TWMenu addToCutomView;
    private TWMenuItem mnueditAlias;

    private TWMenuItem mnuFilter1;
    private TWMenuItem mnuFilter;
    /* Class filter menu*/
    private TWMenu g_mnuAssetClasses;

    private ChartFrame chartFrame;
    private boolean showInProChart;

    private ConnectorInterface connector;

    private Menus g_oMenus;

    static long g_lMarketTime = 0;

    private String g_sConnectionStatus;

    // Added by Bandula - 02-07-02
    private TickerFrame tickerFrame; //Bug ID <#0014> Ticker resize when windows are cascaded
    // Announcement Window Items
    private InternalFrame sectorMapFrame;
    private InternalFrame announcementFrame;
    private InternalFrame announcementSearchFrame;
    //    private InternalFrame broadcastMessageFrame;
    //    private static SectorOverviewUpdator secOverviewUpdator;
    private static Thread feederThreads;

    public static SectorOverviewUpdator feeders;

    private InternalFrame mutualFundtFrame;
    private InternalFrame currencyFrame;

    // Announcement Window Items
    //    private InternalFrame newsFrame;
    //    private InternalFrame newsSearchFrame;
    //    private CombinedNewsWindow newsFrame;
    private NewsWindow NEWSFrame;
    private GlobalMarketSummary g_oGlobalMarketSummary;
    private FinancialCalenderWindow CAFrame;
    private DepthCalculatorWindow depthcalcWindow;
    private GlobalIndexWindow GIFrame;

    //sath  corparate action search result
    //private CASearchPanel caSearchPanel;

    private OptionSymbolBuilder optBuilder;


    // Indices Window Items
    private IndicesFrame g_oIndicesFrame;
    private InternalFrame g_oGlobalIndexFrame;

    // Market View Window Items
    private InternalFrame g_oExchangeFrame;

//    private InternalFrame g_oMarketFrame;

    // Market Time & Sales View Window Items
    //private InternalFrame g_oTimeSalesFrame;

    // Full Market Time & Sales View Window Items
    private InternalFrame g_oFullTimeSalesFrame;

    // Pending Orders Window Items
    private InternalFrame blotterFrame;

    // Open Positions Window Items
    private InternalFrame openPositionsFrame;

    // Pending Orders Window Items
    private InternalFrame orderQFrame;

    // Pending Orders Window Items
    private InternalFrame programedOrderFrame;

    private InternalFrame customerSupportFrame;

    private InternalFrame stockTransferWindow;

    private InternalFrame brokersWindow;

    private InternalFrame stockReportFrame;


    private JComboBox exchangeCombo;
    private ArrayList exchanges;

    // Net Change Calculator window settings
    private Table changeCalcTable;
    private NetCalcModel netCalcModel;
    private InternalFrame netCalcFrame;

    private static TWDateFormat openMarketTimeFormatter;
    private static TWDateFormat closedMarketTimeFormatter;
    private static Date marketTime;

    public Navigator symbolBar;
    public SearchNavigator symbolSearchBar;

    public ClientTable defaultBoardtable = null;
    private JDialog exitDialog;
    private boolean canExit = false;
    private long lastLeftClickTilme = 0;
    private long lastRightClickTilme = 0;
    private PortfolioWindow pfWindow;
    private TradingPortfolioWindow tradingPortfolio;
    public JPanel tickerPanel;
    public JPanel bottomtickerPanel;
    private RapidOrderEntryPanel rapidOrderEntryPanel;
    private JPanel quickTradePanel;
    private boolean tickerFixed = true;
    public TWMenuItem floatMenu;
    private HeatMapFrame heatMap;
    private FunctionBuilder functionBuilder;
    private ChatFrame chatWindow = null;
    private static MessageDialog oMessageDialog;     // Disconnected Mesage Dialog
    private AutoTradeLoginDaemon autoTradeLoginDaemon;
    private MainBoardLoader mainBoardLoader;
    public TWFrame window;
    private TWUpdateDownloader twUpdateDownloader;
    public JPanel tabAndStatusbar;
    public static boolean criticalState = false;
    private ImageLabel descriptLabel;
    private ImageLabel descriptLabelHive;
    private JButton removeTanIndexButton;
    private JButton leftTrigger;
    private JButton rightTrigger;
    private MainScannerWindow scanner;
    private NewPFCreationWindow newPFCreationWindow;
    private ArrayList<JMenuItem> vasMenuItems;
    private JPanel leftSideBarPan;
    private ViewSetting oSetting;
    private JPanel leftTriggerMainPan;
    private boolean isClosed = false;
    public static boolean ISTICKERFIXED;

    public static boolean WATCHLIST_SELECT = false;
    public static boolean needToReconnect = false;
    public static boolean isEnable = false;
    public static boolean isSelected = false;
    public static boolean isAllDefault = true;

    public static boolean IsMultiSelected = false;
    public static boolean IS_TIK_FLOAT = false;
    public static boolean IS_BANDOP_VISIBLE = false;
    public static boolean IS_TICKER_FIXED_WM = false;
    private boolean printInProgress = false;
    private TWMenu mnuMWTrade;
    private TWMenuItem mnuMWSell;
    private TWMenuItem mnuMWBuy;

//    TWMenuItem descriptionmenu;

//    private ViewSetting oViewSettingRef;
//    private Table oDetailQuoteRef;


    /**
     * Constructs a new instance.
     */

    public Client() {
        //super();
//        window = new TWFrame();
        System.out.println("client start ==" + (System.currentTimeMillis() / 1000));
        if (!Settings.getBooleanItem("DO_NOT_SHOW_RESOLUTION_MSG")) {
            JLabel message = new JLabel(Language.getString("MSG_INCOMPATIBLE_RESOLUTOIN"));
            JCheckBox check = new JCheckBox(Language.getString("MSG_DO_NOT_DISPLAY_AGAIN"));
            Object[] options = {message, check};
            if (Toolkit.getDefaultToolkit().getScreenSize().getWidth() <= 800) {
                JOptionPane.showMessageDialog(window, options,
                        Language.getString("WARNING"), JOptionPane.WARNING_MESSAGE);
            }
            if (check.isSelected()) {
                Settings.setItem("DO_NOT_SHOW_RESOLUTION_MSG", "true");
            }
            message = null;
            check = null;
            options = null;
        }

//        StockMaster.loadStocks();
        g_oLogoManager = new LogoManager();
//        Theme.getInstance().loadTheme(Settings.getLastThemeID());
        window = new TWFrame();
        window.setVisible(false);
        openMarketTimeFormatter = new TWDateFormat(Language.getString("MARKET_TIME_OPEN"));
        closedMarketTimeFormatter = new TWDateFormat(Language.getString("MARKET_TIME_CLOSE"));
        marketTime = new Date();
//        tradeDateFormat = new SimpleDateFormat("HHmmss");

        Settings.setRunMode();

        try {
            window.addWindowListener(this);
            g_oMenus = new Menus(this);
            LogoManager.setPtogress(2);
            createPopup();
            createHivePopup();
//            Theme.getInstance().loadTheme(Settings.getLastThemeID());
            g_oInstance = this;
//            System.out.println("jbinit started =="+(System.currentTimeMillis()/1000));
            LogoManager.setPtogress(4);
            jbInit();
//            System.out.println("jbinit ended =="+(System.currentTimeMillis()/1000));
//            TradeStore.init();
            TradeBacklogStore.getSharedInstance(); // inititlize it
//            LogoManager.setPtogress(95);
            addMouseListenerToTree();
            Application.getInstance().addApplicationListener(this);
            MWMenuCreationNotifier.getInstance().addMenuCreationListener(this);
            //AboutBox.getSharedInstance(); // pre-load the about box;
            LogoManager.setPtogress(100);
            g_oLogoManager.dispose();

            ddeServer = new DDEServer();
//            ddeServer.runServer();

            connector = ConnectorFactory.createConnector();

            //TODO: followings are for Radar Screen
            RadarScreenDataLoader.getInstance();
            Thread updatorThread = new Thread(RadarScreenInterface.getInstance(), "Radar Screen");
            updatorThread.start();

            //SatelliteConnector satelliteConnector = new SatelliteConnector();

            /*Rectangle virtualBounds = new Rectangle();
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            for (int j = 0; j < gs.length; j++) {
                GraphicsDevice gd = gs[j];
                GraphicsConfiguration[] gc = gd.getConfigurations();
                for (int i = 0; i < gc.length; i++) {
                    virtualBounds = virtualBounds.union(gc[i].getBounds());
                }
            }
            getFrame().setBounds(virtualBounds);
            virtualBounds = null;
            ge = null;
            gs = null;*/

            /* Show the Window */
            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    if (!Settings.isDualScreenMode()) {
                        window.setExtendedState(JFrame.MAXIMIZED_BOTH);
                    } else {
                        Rectangle virtualBounds = new Rectangle();
                        GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
                        GraphicsDevice[] gs = ge.getScreenDevices();
                        for (int j = 0; j < gs.length; j++) {
                            GraphicsDevice gd = gs[j];
                            GraphicsConfiguration[] gc = gd.getConfigurations();
                            for (int i = 0; i < gc.length; i++) {
                                virtualBounds = virtualBounds.union(gc[i].getBounds());
                            }
                        }
                        getFrame().setBounds(virtualBounds);
                        virtualBounds = null;
                        ge = null;
                        gs = null;
                    }
                    System.out.println("showing time ==" + System.currentTimeMillis());

                    oTopDesktop.updateUI();
                    window.repaint();

                    window.setVisible(true);
                    if (Settings.isFullScreenMode()) {
                        toggleScreen(true);
                    }
                    showEULA();
                    oTree.addTreeExpansionListener(g_oMenus); // to prevent tree being visible unexpectedly
                    //showWhatzNew();
                    if (Settings.isOfflineMode()) {
                        mainBoardLoader.showInitializinMessage(true);
                    }
//                    Application.getInstance().fireApplicationLoaded(); // notofy application listeners that the app loading completed
                    //todo - changed
                    new Thread("Client Loading Thread") {

                        public void run() {
                            try {
                                oTopDesktop.setVisible(false);
                                Application.getInstance().fireApplicationLoaded(); // notofy application listeners that the app loading completed
                                System.out.println("after application loaded==" + System.currentTimeMillis());
                                if (Settings.isOfflineMode()) {
                                    ExchangeStore.getSharedInstance().notifyExchangesLoaded();
                                    System.out.println("after exchanges loaded==" + System.currentTimeMillis());
                                    CurrencyStore.getSharedInstance().notifyCurrenciesLoaded();
                                    System.out.println("after Currencies loaded==" + System.currentTimeMillis());
                                    Settings.isOfflineDataLoaded = true;
                                }
//                                openSavedDefaultWorkSpace();
                                oTopDesktop.setVisible(true);
                                System.out.println("oTopDesktop.setVisible==" + System.currentTimeMillis());
                                oTopDesktop.setDetachedFrames();
                                System.out.println("oTopDesktop.setDetachedFrames==" + System.currentTimeMillis());
                                oTopDesktop.setDetachedClientTables();
                                System.out.println("oTopDesktop.setDetachedClientTables==" + System.currentTimeMillis());
                            } catch (Exception e) {
                                System.out.println("exception catched in run in client ==" + System.currentTimeMillis());
                                e.printStackTrace();
                            } finally {
                                System.out.println("in the finally method==" + System.currentTimeMillis());
                                mainBoardLoader.closeInitializinMessage();
                                System.out.println("mainBoardLoader.closeInitializinMessage==" + System.currentTimeMillis());
                                mainBoardLoader.setFirstTimeLoaded(false);
                                System.out.println("mainBoardLoader.setFirstTimeLoaded==" + System.currentTimeMillis());
                                connector.start();
                                if(TWControl.isEnableWebLogin()){
                                    String params = TWControl.getPostParams();
                                    if(!Language.isLTR()){
                                        BrowserManager.getInstance().navigate("LOGIN3",TWControl.getRequestURL("TRADE_LOGIN"),Language.getString("TRADE_LOGIN"),null,params, false);  //Templates\Login\login.html
                                    } else {
                                        BrowserManager.getInstance().navigate("LOGIN3",TWControl.getRequestURL("TRADE_LOGIN"),Language.getString("TRADE_LOGIN"), false);  //Templates\Login\login.html
                                    }
                                    BrowserManager.getInstance().setVisible("LOGIN3", false);
                                }
                                if (TWControl.isSingleSingnOn() && TWControl.getSSOType() == TWControl.SSO_TYPES.Direct) {
                                    FrameAnalyser.analyseBrokers(TWControl.getSSOBrokerDetails());
                                }
                            }

                        }
                    }.start();

                    SystemTrayManager.createSystemTrayIcon();
                    if (Settings.isShowWelcomeScreen()) {
                        new Thread("Tip Of the Day") {
                            public void run() {
//                                if (HelpManager.isKeyAvailable("TIP_OF_THE_DAY")) {
                                if (HelpManager.isTodAvailable()) {
                                    try {
                                        Thread.sleep(10000);
                                    } catch (InterruptedException e) {
                                    }
                                    new TipOfDayWindow().showUI();
                                }
                            }
                        }.start();
                    }
//                    if (Settings.isShowWelcomeScreen()) {
//                        SwingUtilities.invokeLater(new Runnable() {
//                            public void run() {
//
//                                new WelcomeScreen();
//                                SystemTrayManager.showMessage(Language.getString("WELCOME_TO_PRO"));
//                            }
//                        });
//                    } else {
//                        SwingUtilities.invokeLater(new Runnable() {
//                            public void run() {
//                                if (Menus.isTipOfDayAvailable()) {
//                                    new TipOfDayWindow();
//                                }
////                                new WelcomeScreen();
//                                SystemTrayManager.showMessage(Language.getString("WELCOME_TO_PRO"));
//                            }
//                        });
//
//                    }
                }
            });

            //addExitThread();
            Runtime.getRuntime().gc();
            Runtime.getRuntime().gc();

            /*// extract the tutorial if exists
            TutorialExtractor tutorialExtractor = new TutorialExtractor();
            if (tutorialExtractor.exists()) {
                Thread thread = new Thread(tutorialExtractor, "Tutorial Extractor");
                thread.start();
            }*/

            if (Settings.couldNotApplyUpdate()) {
                String msg = Language.getString("UPDATE_APPLY_FAILED_NO_WRITE_ACCESS");
                String path = System.getProperty("user.dir");
                path = path.replace("\\", "\\\\");
                msg = msg.replaceFirst("\\[PATH\\]", path);
                SharedMethods.showMessage(msg, JOptionPane.ERROR_MESSAGE);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showEULA() {
        /* Show the end user acreement */
        String eulaShown = Settings.getItem("EULA_SHOWN");
        try {
            if ((eulaShown.equalsIgnoreCase("false"))) {
                EULAWindow eulaWindow = new EULAWindow(window, true);
                eulaWindow.show();
                eulaWindow.requestFocus();
                if (!eulaWindow.isAccepted())
                    System.exit(0);
                Theme.unRegisterComponent(eulaWindow);
            }
        } catch (Exception e) {

        }
    }

    /**
     * Retuns a client instance
     */
    public static Client getInstance() {
        return g_oInstance;
    }

    public JFrame getFrame() {
        return window;
    }

    /*public void showWhatzNew() {
        if (!Settings.getBooleanItem("DONOT_SHOW_WATHZ_NEW")) {
            JOptionPane optionPane = new JOptionPane(Language.getString("WHATZ_NEW_MESSAGE"));
            optionPane.setOptionType(JOptionPane.OK_CANCEL_OPTION);
            final JDialog dialog = optionPane.createDialog(window, Language.getString("WINDOW_TITLE_CLIENT"));
            TWButton okButton = new TWButton(Language.getString("OK"));
            okButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    dialog.dispose();
                }
            });
            TWButton showButton = new TWButton(Language.getString("HOW_WHATZ_NEW"));
            showButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    NativeMethods.showWhatzNewHelp();
                    Settings.setItem("DONOT_SHOW_WATHZ_NEW", "true");
                    dialog.dispose();
                }
            });
            Object[] temp = {okButton, showButton};
            optionPane.setOptions(temp);

            dialog.setVisible(true);
            Settings.setItem("DONOT_SHOW_WATHZ_NEW", "true");
        }
    }*/

    /**
     * Create the main menu
     */
    private void createManinMenu() {
        window.setJMenuBar(g_oMenus.createManinMenu());
    }

    private void createHivePopup() {
        hivePopup.addPopupMenuListener(this);
        descriptLabelHive = new ImageLabel("");
        descriptLabelHive.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        descriptLabelHive.setVisible(true);
        hivePopup.add(descriptLabelHive);

        mnuTradeHive = new TWMenu(Language.getString("MUBASHER_TRADE"), "trade.gif");
        mnuTradeHive.setBackground(Theme.getColor("BOARD_TABLE_CELL_TRADE_BGCOLOR1"));
        mnuTradeHive.setEnabled(false);
        mnuBuyHive = new TWMenuItem(Language.getString("BUY"), "buy.gif");
        mnuBuyHive.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        mnuBuyHive.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if ((DataStore.getSharedInstance().getStockObject(getSelectedSymbol())).getInstrumentType() == Meta.INSTRUMENT_FOREX) {
                    ForexBuySell fBS = ForexBuySell.getSharedInstance(1, null);//getInstance();
                    fBS.setKey(getSelectedSymbol());
                    fBS.setVisible(true);
                    GUISettings.setLocationRelativeTo(fBS, getDesktop());

                } else {
                    doTransaction(TradeMeta.BUY, null, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
                }
            }
        });
        mnuTradeHive.add(mnuBuyHive);

        mnuSellHive = new TWMenuItem(Language.getString("SELL"), "sell.gif");
        mnuSellHive.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
        mnuSellHive.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if ((DataStore.getSharedInstance().getStockObject(getSelectedSymbol())).getInstrumentType() == Meta.INSTRUMENT_FOREX) {
                    ForexBuySell fBS = ForexBuySell.getSharedInstance(1, null);//getInstance();
                    fBS.setKey(getSelectedSymbol());
                    fBS.setVisible(true);
                    GUISettings.setLocationRelativeTo(fBS, getDesktop());
                } else {
                    doTransaction(TradeMeta.SELL, null, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
                }
            }
        });
        mnuTradeHive.add(mnuSellHive);

        mnuGraphHive = new TWMenuItem(Language.getString("GRAPH"), "graph.gif");
        mnuGraphHive.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //ShowGraph_Selected();
                if (ChartInterface.isFirstTimeChartLoading()) {
                    final WorkInProgressIndicator in = new WorkInProgressIndicator(WorkInProgressIndicator.CHART_LOADING_IN_PROGRESS);
                    in.setVisible(true);
                    new Thread() {
                        public void run() {
                            try {
                                showChart(activeHiveSymbol, null, null);
                            } catch (Exception e) {
                                e.printStackTrace();  //defalt action
                            }

                            in.dispose();
                            ChartInterface.setFirstTimeChartLoading(false);
                        }
                    }.start();
                } else {
                    showChart(activeHiveSymbol, null, null);
                }
            }
        });

        mnuTimeNSalesHive = new TWMenuItem(Language.getString("TIME_AND_SALES"), "time&sales.gif");
        mnuTimeNSalesHive.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mnu_TimeNSalesSymbol(activeHiveSymbol, Constants.MAINBOARD_TYPE, false, false, LinkStore.LINK_NONE);
            }
        });

        mnuCashFQHive = new TWMenuItem(Language.getString("CASHFLOW_DQ"), "cashflowdetailquote.gif");
        mnuCashFQHive.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(activeHiveSymbol), Meta.IT_CashFlow)) {
                    showCashFlowDetailQuote(activeHiveSymbol, false, false, false, LinkStore.LINK_NONE);

                } else {
                    String message = Language.getString("FEATURE_NOT_AVAILABLE");
                    message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(activeHiveSymbol)).getDescription());
                    SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                }
            }
        });

        mnuDepthByPriceHive = new TWMenuItem(Language.getString("MARKET_DEPTH_BY_PRICE"), "mktby_price.gif");
        mnuDepthByPriceHive.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(activeHiveSymbol), Meta.IT_MarketDepthByPrice)) {
                    depthByPrice(activeHiveSymbol, false, Constants.MAINBOARD_TYPE);

                } else {
                    String message = Language.getString("FEATURE_NOT_AVAILABLE");
                    message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(activeHiveSymbol)).getDescription());
                    SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                }
            }
        });

        mnuAlertHive = new TWMenuItem(Language.getString("SET_ALERT"), "alert.gif");
        mnuAlertHive.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mnu_AlertSetup(activeHiveSymbol);
            }
        });


        if (Settings.isTCPMode()) {
            mnuTradeHive.updateUI();
            hivePopup.add(mnuTradeHive);
        }
        hivePopup.add(mnuGraphHive);
        hivePopup.add(mnuTimeNSalesHive);
        hivePopup.add(mnuCashFQHive);
        hivePopup.add(mnuDepthByPriceHive);
        if (Settings.isTCPMode()) {
            hivePopup.add(mnuAlertHive);
        }

        GUISettings.applyOrientation(hivePopup);

    }

    /**
     * Crreates popup menus
     */
    private void createPopup() {
        /* Multi select table popup menu*/

        g_oTableMultiPopup.add(getCopyMenu());
        g_oTableMultiPopup.add(getCopyWithHeadMenu());
        g_oTableMultiPopup.add(getEnableMenu());
        g_oTableMultiPopup.add(getDisableMenu());

        g_oTableMultiPopup.add(getLinkMenu());
        g_oTableMultiPopup.add(getLinkMenuWithHeaders());

        g_mnuMultiDelete = new TWMenuItem(Language.getString("DELETE"), "rmsymbol.gif");
        g_mnuMultiDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                deleteMultipleSymbols();
            }
        });
        g_oTableMultiPopup.add(g_mnuMultiDelete);

        GUISettings.applyOrientation(g_oTableMultiPopup);

        /* Table popup menu */
        watchListPopup = new TWMenu(Language.getString("WATCHLIST_OPTIONS"), "watchlistoptions.gif");

        g_oTablePopup.addPopupMenuListener(this);
        g_oWatchListPopup.addPopupMenuListener(this);

        //Symbol Short Description
        descriptLabel = new ImageLabel("");
        descriptLabel.setAlignmentX(JLabel.CENTER_ALIGNMENT);
        descriptLabel.setVisible(false);
        descriptLabel.setOpaque(true);
//        g_oTablePopup.add(descriptLabel);

//        descriptionmenu = new TWMenuItem("");
//        descriptionmenu.setAlignmentX(TWMenuItem.CENTER);
//        descriptionmenu.setOpaque(true);
//        descriptionmenu.setEnabled(false);
//        descriptionmenu.setHorizontalAlignment(TWMenuItem.CENTER);
//        descriptionmenu.setVisible(false);
//        g_oTablePopup.setLayout(new ColumnLayout(true));
        g_oTablePopup.add(descriptLabel);
//        g_oTablePopup.add(descriptionmenu);
        // copy / link items
        //todo  removed by udaya (included in new popup window)
        //     g_oTablePopup.add(getCopyMenu());
        //g_oWatchListPopup.
        //     g_oTablePopup.add(getCopyWithHeadMenu());

        //     g_oTablePopup.add(getLinkMenu());
        //      g_oTablePopup.add(getLinkMenuWithHeaders());
        //      separator=new TWSeparator();
        //g_oTablePopup.add(separator);

        //      g_oTablePopup.add(new TWSeparator());

        // normal trade
        mnuTrade = new TWMenu(Language.getString("MUBASHER_TRADE"), "trade.gif");
        mnuTrade.setBackground(Theme.getColor("BOARD_TABLE_CELL_TRADE_BGCOLOR1"));
        mnuTrade.setEnabled(false);
        mnuBuy = new TWMenuItem(Language.getString("BUY"), "buy.gif");
        mnuBuy.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        mnuBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if ((DataStore.getSharedInstance().getStockObject(getSelectedSymbol())).getInstrumentType() == Meta.INSTRUMENT_FOREX) {
                    ForexBuySell fBS = ForexBuySell.getSharedInstance(1, null);//getInstance();
                    fBS.setKey(getSelectedSymbol());
                    fBS.setVisible(true);
                    GUISettings.setLocationRelativeTo(fBS, getDesktop());

                } else {
                    doTransaction(TradeMeta.BUY, null, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
                }
            }
        });
        mnuTrade.add(mnuBuy);

        mnuSell = new TWMenuItem(Language.getString("SELL"), "sell.gif");
        mnuSell.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
        mnuSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if ((DataStore.getSharedInstance().getStockObject(getSelectedSymbol())).getInstrumentType() == Meta.INSTRUMENT_FOREX) {
                    ForexBuySell fBS = ForexBuySell.getSharedInstance(1, null);//getInstance();
                    fBS.setKey(getSelectedSymbol());
                    fBS.setVisible(true);
                    GUISettings.setLocationRelativeTo(fBS, getDesktop());
                } else {
                    doTransaction(TradeMeta.SELL, null, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
                }
            }
        });
        mnuTrade.add(mnuSell);

        //middleware trade todo:***************************************************************************************
        mnuMWTrade = new TWMenu(Language.getString("MUBASHER_TRADE"), "trade.gif");
        mnuMWTrade.setBackground(Theme.getColor("BOARD_TABLE_CELL_TRADE_BGCOLOR1"));
        mnuMWTrade.setVisible(false);

        //todo:********************************************************************************************************
        mnuBuyCover = new TWMenuItem(Language.getString("BUY_COVER"), "prog_buy.gif");
        mnuBuyCover.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        mnuBuyCover.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doTransaction(TradeMeta.BUY, null, 0, null, false, 0, TradeMeta.MODE_PROGRAMMED, TradeMeta.SOURCE_BOARD, null, true, null);
            }
        });
        //mnuTrade.add(mnuBuyCover);
        mnuSellCover = new TWMenuItem(Language.getString("SELL_COVER"), "prog_sell.gif");
        mnuSellCover.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
        mnuSellCover.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doTransaction(TradeMeta.SELL, null, 0, null, false, 0, TradeMeta.MODE_PROGRAMMED, TradeMeta.SOURCE_BOARD, null, true, null);
            }
        });
        //mnuTrade.add(mnuSellCover);

        mnuBlockOrder = new TWMenuItem(Language.getString("BLOCK_ORDER"), "blockorder.gif");
        mnuBlockOrder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
        });
        //mnuTrade.add(mnuBlockOrder);
//        mnuTrade.add(g_mnuForexBuySel);

        mnuScaleOrder = new TWMenuItem(Language.getString("SCALE_ORDER"), "scaleorder.gif");
        mnuScaleOrder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
            }
        });
        //mnuTrade.add(mnuScaleOrder);

        mnuQuickTrade = new TWMenuItem(Language.getString("RAPID_TRADE"), "rapidorder.gif");
        mnuQuickTrade.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                Stock stock = DataStore.getSharedInstance().getStockObject(getSelectedSymbol());
//                rapidOrderEntryPanel.populateOrder(stock);
//                showRapidOrderPanel(true);
                populateRapidOrderPanel();
            }
        });
        mnuTrade.add(mnuQuickTrade);
        //  ==temp mini trade
        mnuMiniTrade = new TWMenuItem(Language.getString("MINI_TRADE"), "minitradesum.gif");
        mnuMiniTrade.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doMiniTradeSymbol(null, System.currentTimeMillis() + "");
            }
        });

        if (!TWControl.isMiniTradeDisabled()) {
            mnuTrade.add(mnuMiniTrade);
            mnuMiniTrade.setVisible(true);
        }


        if (Settings.isTCPMode()) {
            g_oTablePopup.add(mnuTrade);
        }
        g_oTablePopup.add(mnuMWTrade);
        // programmed trade
        /*mnuProTrade = new TWMenu(Language.getString("MUBASHER_PROGRAMMED_TRADE"), "programtrade.gif");
        mnuProTrade.setBackground(Theme.getColor("BOARD_TABLE_CELL_TRADE_BGCOLOR1"));
        mnuProBuy = new TWMenuItem(Language.getString("BUY"), "prog_buy.gif");
        mnuProBuy.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        mnuProBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doTransaction(TradeMeta.BUY, null, 0, null, false, 0, TradeMeta.MODE_PROGRAMMED, TradeMeta.SOURCE_BOARD, null);
            }
        });
        mnuProTrade.add(mnuProBuy);
        mnuProSell = new TWMenuItem(Language.getString("SELL"), "prog_sell.gif");
        mnuProSell.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
        mnuProSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doTransaction(TradeMeta.SELL, null, 0, null, false, 0, TradeMeta.MODE_PROGRAMMED, TradeMeta.SOURCE_BOARD, null);
            }
        });
        mnuProTrade.add(mnuProSell);

        if (Settings.isTCPMode()){
            g_oTablePopup.add(mnuProTrade);
        }*/

        // view related items
        g_mnuTreePopEditView = new TWMenuItem(Language.getString("EDIT_VIEW"), "editview.gif");
        g_mnuTreePopEditView.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                editView();
            }

        });
        g_mnuTreePopEditView.setVisible(false);
        g_mnuTreePopEditView.setEnabled(false);

        mnuTreePopDelete = new TWMenuItem(Language.getString("DELETE_VIEW"), "deleteview.gif");
        mnuTreePopDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                g_mnuTreePopDelete_Selected(e);
            }
        });
        mnuTreePopDelete.setVisible(false);
        mnuTreePopDelete.setEnabled(false);

        mnuPrint = new TWMenuItem(Language.getString("PRINT"), "print.gif");
        mnuPrint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                printView();
            }
        });
        watchListPopup.add(mnuPrint);
        //g_oWatchListPopup.add(mnuPrint);

        adjustWidths = new TWMenuItem(Language.getString("ADJUST_WIDTHS"), "resizecols.gif");
        adjustWidths.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (mainboardSelected) {
                    ((DraggableTable) g_oTempTable.getTable()).adjustColumnWidthsToFit();
                } else if (portfolioSelected) {
                    ((SmartTable) g_oPortfolioTable.getTable().getTable()).adjustColumnWidthsToFit(10);
                } else if (mistViewSelected) {
                    ((SmartTable) g_oMistTable.getTable().getTable()).adjustColumnWidthsToFitinMIST(10);
                } else if (volumeWatcherSelected) {
                    ((DraggableTable) g_oTempTable.getTable()).adjustColumnWidthsToFit();
                }
            }
        });
        watchListPopup.add(adjustWidths);
        //g_oWatchListPopup.add(adjustWidths);

//        mnuFilteredTicker = new TWMenuItem(Language.getString("TICKER"), "ticker.gif");
//        mnuFilteredTicker.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                customizeTicker(true);
//            }
//        });
//        watchListPopup.add(mnuFilteredTicker);

        //
        mnuColumnSettings = new TWMenuItem(Language.getString("COLUMN_SETTINGS"), "colsetting.gif");
        watchListPopup.add(mnuColumnSettings);
        //g_oWatchListPopup.add(mnuColumnSettings);
        mnuColumnSettings.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                g_mnuTreePopSettings_Selected();
            }
        });


        mnuUnsort = new TWMenuItem(Language.getString("UNSORT"), "unsort.gif");
        watchListPopup.add(mnuUnsort);
        //g_oWatchListPopup.add(mnuUnsort);
        mnuUnsort.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                unsortSymbols();
            }
        });

        watchListPopup.add(createSelectionPopup());
        //g_oWatchListPopup.add(createSelectionPopup());

        mnuCustomize = new TWMenuItem(Language.getString("CUSTOMIZE"), "customize.gif");
        watchListPopup.add(mnuCustomize);
        //g_oWatchListPopup.add(mnuCustomize);
        mnuCustomize.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showCustomizer();
            }
        });
        TWMenuItem mnuCustomize1 = new TWMenuItem(Language.getString("CUSTOMIZE"), "customize.gif");
        //watchListPopup.add(mnuCustomize);
        g_oWatchListPopup.add(mnuCustomize1);
        mnuCustomize1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showCustomizer();
            }
        });

        //todo added by Dilum
        mnuSaveSettings = new TWMenuItem("<html><font color=red><B><U>Save Settings", "resetdefault.gif");
        mnuSaveSettings.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    if (mainboardSelected) {
                        g_oTempTable.saveColumnPositions();
                        //                    g_oTempTable.getTable().setFont(g_oTempTable.getViewSettings().getFont());
                        SharedMethods.saveCustomViewSetting(g_oTempTable.getViewSettings());
                    } else if (portfolioSelected) {
                        g_oPortfolioTable.getTable().getModel().saveColumnPositions();
                        g_oPortfolioTable.getTable().getModel().getViewSettings().setFont(g_oPortfolioTable.getTable().getTable().getFont());
                        SharedMethods.saveCustomViewSetting(g_oPortfolioTable.getTable().getModel().getViewSettings());
                    } else if (mistViewSelected) {
                        g_oMistTable.getTable().getModel().saveColumnPositions();
                        g_oMistTable.getTable().getModel().getViewSettings().setFont(g_oMistTable.getTable().getTable().getFont());
                        SharedMethods.saveCustomViewSetting(g_oMistTable.getTable().getModel().getViewSettings());
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });
        mnuSaveSettings.setVisible(false);
        mnuSaveSettings.setEnabled(false);
        watchListPopup.add(mnuSaveSettings);

        // symbol specific items

        //g_oTablePopup.add(new JSeparator());

        if(TWControl.isEnableWebLogin()){
        mnuAddwebOrder = new TWMenuItem(Language.getString("ADD_ORDER"),"trade.gif");
        mnuAddwebOrder.setVisible(true);
        mnuAddwebOrder.setEnabled(true);
        mnuAddwebOrder.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                String key = getSelectedSymbol();
                Stock stock = DataStore.getSharedInstance().getStockObject(key);
                String isinCode = stock.getISINCode();

                String params = TWControl.getPostParams();
                String url= TWControl.getRequestURL("WEB_ORDER")+isinCode;

                BrowserManager man = BrowserManager.getInstance();
                man.navigate("LOGIN3",url,Language.getString("ADD_ORDER"),null,params);

            }
        });
//        g_oTablePopup.add(mnuAddwebOrder);
        }
        

        mnuGraph = new TWMenuItem(Language.getString("GRAPH"), "graph.gif");
        g_oTablePopup.add(mnuGraph);
        mnuGraph.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //ShowGraph_Selected();
                showInProChart = false;
                if (ChartInterface.isFirstTimeChartLoading()) {
                    final WorkInProgressIndicator in = new WorkInProgressIndicator(WorkInProgressIndicator.CHART_LOADING_IN_PROGRESS);
                    in.setVisible(true);
                    new Thread() {
                        public void run() {
                            try {
                                showChart(null, null, null);
                            } catch (Exception e) {
                                e.printStackTrace();  //defalt action 
                            }
                            in.dispose();
                            ChartInterface.setFirstTimeChartLoading(false);
                        }
                    }.start();
                } else {
                    showChart(null, null, null);
                }
            }
        });

        mnuGraphInProChart = new TWMenuItem(Language.getString("UNICHARTS"), "menugraph.gif");
        g_oTablePopup.add(mnuGraphInProChart);
        mnuGraphInProChart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showInProChart = true;
                if (ChartInterface.isFirstTimeChartLoading()) {
                    final WorkInProgressIndicator in = new WorkInProgressIndicator(WorkInProgressIndicator.CHART_LOADING_IN_PROGRESS);
                    in.setVisible(true);
                    new Thread() {
                        public void run() {
                            try {
                                showChart(null, null, null);
                            } catch (Exception e) {
                                e.printStackTrace();  //defalt action
                            }
                            in.dispose();
                            ChartInterface.setFirstTimeChartLoading(false);
                        }
                    }.start();
                } else {
                    showChart(null, null, null);
                }
            }
        });

        mnuAddSymbol = new TWMenuItem(Language.getString("ADD_SYMBOLS"), "addsymbol.gif");
        g_oTablePopup.add(mnuAddSymbol);
        mnuAddSymbol.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mnu_AddSymbol(true);
            }
        });

        mnuRemoveSymbol = new TWMenuItem(Language.getString("REMOVE_SYMBOL"), "rmsymbol.gif");
        g_oTablePopup.add(mnuRemoveSymbol);
        mnuRemoveSymbol.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mnu_RemoveSymbol();
            }
        });
//----------------------- added by Shanika to enable symbols------------------

        mnuEnable = new TWMenuItem(Language.getString("ENABLE_SYMBOL"), "symbolenable.gif");
        mnuEnable.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                enabelSymbol(null, false);
            }
        });
        mnuEnable.setVisible(false);
//        g_oTablePopup.add(mnuEnable); todo: change order

        mnuDisable = new TWMenuItem(Language.getString("DISABLE_SYMBOL"), "symboldisable.gif");
        mnuDisable.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                disableSymbol(null, false);
            }
        });
        mnuDisable.setVisible(false);
//        g_oTablePopup.add(mnuDisable); // moved down, next to set alert

        /* //  ==temp mini trade
        mnuMiniTrade = new TWMenuItem(Language.getString("MINI_TRADE"), "symbolenable.gif");
        mnuMiniTrade.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                doMiniTradeSymbol(null, System.currentTimeMillis() + "");
            }
        });
        mnuMiniTrade.setVisible(true);*/
//        g_oTablePopup.add(mnuMiniTrade);

        // temp -- mini trade
        mnuTimeNSales = new TWMenuItem(Language.getString("TIME_AND_SALES"), "time&sales.gif");
        mnuTimeNSales.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                     new Thread() {
//            public void run() {
//                try {
//                    Plugin plugin = PluginStore.getPlugin("MEDIA_PLAYER");
//
//                    if (plugin == null) {
//                        plugin = PluginStore.activate("MEDIA_PLAYER");
//                    }
//                    plugin.showUI(null);
//                } catch (Exception e1) {
//                    e1.printStackTrace();
//                }
//            }
//        }.start();
                mnu_TimeNSalesSymbol(null, Constants.MAINBOARD_TYPE, false, false, LinkStore.LINK_NONE);
            }
        });
        g_oTablePopup.add(mnuTimeNSales);
        mnuStockReport = new TWMenuItem(Language.getString("WINDOW_TITLE_STOCK_REPORT"), "stockreports.gif");
        mnuStockReport.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                searchStockReports();
                getStockReports(getSelectedSymbol());
            }
        });
        mnuStockReport.setVisible(false);
//        g_oTablePopup.add(mnuStockReport);
        ///////////////////////////////////////////////////////////////////////
        mnuCompanyPfofile = new TWMenuItem(Language.getString("COMPANY_PROFILE"), "companyprofile.gif");
        mnuCompanyPfofile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                searchStockReports();
                showCompanyProfile();
            }
        });
//        mnuCompanyPfofile.setVisible(true);
        g_mnuOptionSymbolBuilder = new TWMenuItem(Language.getString("MENU_OPTION_BUILDER"), "optsymbolbuilder.gif");
        g_mnuOptionSymbolBuilder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mnu_FindOptionSymbol(true);
            }
        });
        g_mnuOptionSymbolBuilder.setVisible(true);
        g_oTablePopup.add(g_mnuOptionSymbolBuilder);
        //////////////////////////////////////////////////////////////////////
        mnuTimeNSalesSummary = new TWMenuItem(Language.getString("TRADE_SUMMARY"), "time&salessum.gif");
        mnuTimeNSalesSummary.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mnu_TimeNSalesSummaryForSymbol(null, false, false, LinkStore.LINK_NONE);
            }
        });
        g_oTablePopup.add(mnuTimeNSalesSummary);

        mnuDTQ = new TWMenuItem(Language.getString("DETAIL_QUOTE"), "detailquote.gif");
        mnuDTQ.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showDetailQuote(null);
            }
        });
        g_oTablePopup.add(mnuDTQ);

        mnuSnapQ = new TWMenuItem(Language.getString("SNAP_QUOTE"), "snapquote.gif");
        mnuSnapQ.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(getSelectedSymbol()), Meta.IT_SnapQuote)) {
                    showSnapQuote(null, false, false, false, LinkStore.LINK_NONE);

                } else {
                    String message = Language.getString("FEATURE_NOT_AVAILABLE");
                    message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(getSelectedSymbol())).getDescription());
                    SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                }

            }
        });
        g_oTablePopup.add(mnuSnapQ);

        mnuCashFQ = new TWMenuItem(Language.getString("CASHFLOW_DQ"), "cashflowdetailquote.gif");
        mnuCashFQ.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(getSelectedSymbol()), Meta.IT_CashFlow)) {
                    showCashFlowDetailQuote(null, false, false, false, LinkStore.LINK_NONE);

                } else {
                    String message = Language.getString("FEATURE_NOT_AVAILABLE");
                    message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(getSelectedSymbol())).getDescription());
                    SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                }
            }
        });
        g_oTablePopup.add(mnuCashFQ);

        mnuFullQ = new TWMenuItem(Language.getString("MULTI_DIALOG_MENU"), "combinewindow.gif");
        mnuFullQ.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                System.out.println("in common popup");
                if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(getSelectedSymbol()), Meta.IT_FullQuote)) {
                    //  MultiDialogWindow.getSharedInstance().setVisible(true);
                    showFullQuote(getSelectedSymbol(), false, Constants.FULL_QUOTE_TYPE, false, LinkStore.LINK_NONE);

                } else {
                    String message = Language.getString("FEATURE_NOT_AVAILABLE");
                    message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(getSelectedSymbol())).getDescription());
                    SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                }

            }
        });
        g_oTablePopup.add(mnuFullQ);

        mnuHeatMap = new TWMenuItem(Language.getString("POPUP_HEAT_MAP_FOR_VIEW"), "mktmap.gif");
        mnuHeatMap.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //showHeatMapForTheWatchList(g_oTempTable);
                if (!mistViewSelected) {
                    showHeatMapForTheWatchList(g_oTempTable, g_oTempTable.getMarketCode());
                } else {
                    showHeatMapForTheWatchList(g_oMistTable);
                }
            }
        });
        mnuHeatMap.setVisible(false);
        mnuHeatMap.setEnabled(false);
        g_oTablePopup.add(mnuHeatMap);

        mnuStretchedHeatMap = new TWMenuItem("Stretched Market Map", "mktmap.gif");
        mnuStretchedHeatMap.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //showHeatMapForTheWatchList(g_oTempTable);
                if (!mistViewSelected) {
                    showStretchedHeatMapForTheWatchList(g_oTempTable);
                } else {
                    showStretchedHeatMapForTheWatchList(g_oMistTable);
                }
            }
        });
        mnuStretchedHeatMap.setVisible(false);
        mnuStretchedHeatMap.setEnabled(false);
//        g_oTablePopup.add(mnuStretchedHeatMap);

        mnuDepthByPrice = new TWMenuItem(Language.getString("MARKET_DEPTH_BY_PRICE"), "mktby_price.gif");
        // SAMA
        //g_mnuFullMarketDepth.setVisible(false);
        mnuDepthByPrice.setVisible(false);
        mnuDepthByPrice.setEnabled(false);
        mnuDepthByPrice.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(getSelectedSymbol()), Meta.IT_MarketDepthByPrice)) {
                    depthByPrice(null, false, Constants.MAINBOARD_TYPE);

                } else {
                    String message = Language.getString("FEATURE_NOT_AVAILABLE");
                    message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(getSelectedSymbol())).getDescription());
                    SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                }
            }
        });
        g_oTablePopup.add(mnuDepthByPrice);

        mnuDepthByOrder = new TWMenuItem(Language.getString("MARKET_DEPTH_BY_ORDER"), "mktby_order.gif");
        // SAMA
        mnuDepthByOrder.setVisible(false);
        mnuDepthByOrder.setEnabled(false);
        mnuDepthByOrder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(getSelectedSymbol()), Meta.IT_MarketDepthByOrder)) {
                    depthByOrder(null, false, false, LinkStore.LINK_NONE);

                } else {
                    String message = Language.getString("FEATURE_NOT_AVAILABLE");
                    message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(getSelectedSymbol())).getDescription());
                    SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                }
            }
        });
        g_oTablePopup.add(mnuDepthByOrder);

        mnuDepthOddLot = new TWMenuItem(Language.getString("MARKET_DEPTH_ODD_LOT"), "marketdepthoddlot.gif");
        mnuDepthOddLot.setVisible(false);
        mnuDepthOddLot.setEnabled(false);
        mnuDepthOddLot.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                depthOddLot(null, false, Constants.MAINBOARD_TYPE);
            }
        });
        g_oTablePopup.add(mnuDepthOddLot);


        mnuDepthSpecial = new TWMenuItem(Language.getString("SPECIAL_ORDER_BOOK"), "specialorderbook.gif");
        mnuDepthSpecial.setVisible(false);
        mnuDepthSpecial.setEnabled(false);
        mnuDepthSpecial.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(getSelectedSymbol()), Meta.IT_SpecialOrderBook)) {
                    depthSpecialByOrder(null, false, false, LinkStore.LINK_NONE);

                } else {
                    String message = Language.getString("FEATURE_NOT_AVAILABLE");
                    message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(getSelectedSymbol())).getDescription());
                    SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                }
            }
        });
        g_oTablePopup.add(mnuDepthSpecial);

        mnuTPCalc = new TWMenuItem(Language.getString("PRICE_CALC"), "ordercalc.gif");
        mnuTPCalc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mnu_MDepth_Calculator(null, TWTypes.TradeSides.BUY, false, false, LinkStore.LINK_NONE);
            }
        });
        mnuTPCalc.setVisible(false);
        mnuTPCalc.setEnabled(false);
        g_oTablePopup.add(mnuTPCalc);

        /*TWMenu mnuFilter = new TWMenu(Language.getString("FILTER"));
        symb =getSelectedSymbol();
        System.out.println("Symbol "+ symb);
        DataStore dStore = DataStore.getSharedInstance();
        dStore.initialize();
        stockObj = dStore.getStockObject(symb);
        TWMenuItem sectorFilter = new TWMenuItem(Language.getString("SECTOR"));
        //sectorFilter.setActionCommand(Settings.NULL_STRING);
        sectorFilter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev){

                String sector = stockObj.getSectorCode();
                //String exchange = get
                Symbols symbols = new Symbols(symb,true);
                symbols.setFilter(new SectorFilter());
            }
        });
        //mnuFilter.add(sectorFilter);
        TWMenuItem currencyFilter = new TWMenuItem(Language.getString("CURRENCY"));
        //currencyFilter.setActionCommand(Settings.NULL_STRING);
        currencyFilter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev){
                String exchange = stockObj.getCurrencyCode();

            }
        });
        //mnuFilter.add(currencyFilter);
        //g_oTablePopup.add(mnuFilter);
*/
        /*mnuCompanyProfile = new TWMenuItem(Language.getString("COMPANY_PROFILE"), "companyprofile.gif");
        //g_mnuCompanyProfile.setVisible(false);
        //g_mnuCompanyProfile.setEnabled(false);
        mnuCompanyProfile.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showCompanyProfile();
            }
        });
        g_oTablePopup.add(mnuCompanyProfile);*/

        mnuCompanyFinancials = new TWMenuItem(Language.getString("COMPANY_FINANCIALS"), "companyfinancial.gif");
        //g_mnuCompanyProfile.setVisible(false);
        //g_mnuCompanyProfile.setEnabled(false);
        mnuCompanyFinancials.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //showCompanyProfile();
            }
        });
        // g_oTablePopup.add(mnuCompanyFinancials);

        mnuAnnouncementSearch = new TWMenuItem(Language.getString("ANNOUNCEMENTS"), "announsearch.gif");
        mnuAnnouncementSearch.setVisible(false);
        mnuAnnouncementSearch.setEnabled(false);
        mnuAnnouncementSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                searchSymbolAnnouncements();
            }
        });

        g_oTablePopup.add(mnuAnnouncementSearch);

        mnuRQuotes = new TWMenuItem(Language.getString("POPUP_REGIONAL_QUOTES"), "regionalquotes.gif");
        mnuRQuotes.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showRegionalQuotes(null);
            }
        });
        //if (TWControl.getBooleanItem("REGIONAL_QUOTES")){
        g_oTablePopup.add(mnuRQuotes);
        //}

        g_mnuFD2 = new TWMenuItem(Language.getString("POPUP_FD2"), "fundamentaldata.gif");
        g_mnuFD2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mnu_FD2Symbol(null, false);
            }
        });
        //if (TWControl.getBooleanItem("FUNDAMENTAL_DATA")){
//        g_oTablePopup.add(g_mnuFD2);  todo: removed
        //}

        mnuCorporateActions = new TWMenuItem(Language.getString("CORPORATE_ACTIONS"), "corporateactions.gif");
        mnuCorporateActions.setVisible(true);
        mnuCorporateActions.setEnabled(true);
        mnuCorporateActions.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                FinancialCalenderWindow.getSharedInstance().setVisible(true);
                String sSelectedSymbol = getSelectedSymbol();
                if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                    SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                    return;
                }
                if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                    SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                    return;
                }
                FinancialCalenderStore.getSharedInstance().sendSymbolRequest(sSelectedSymbol);
                mnu_CorporateAction_ActionPerformed(sSelectedSymbol);
            }
        });
//        if (TWControl.getBooleanItem("CORPORATE_ACTIONS")){
        g_oTablePopup.add(mnuCorporateActions);
//        }

//        g_oTablePopup.add(mnuCompanyPfofile);

        g_mnuStockReports = new TWMenuItem(Language.getString("POPUP_STOCKREPORTS"), "stockreport.gif");
        g_mnuStockReports.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                getStockReports(getSelectedSymbol());
            }
        });
        //if (TWControl.getBooleanItem("STOCK_REPORTS")){
        g_oTablePopup.add(g_mnuStockReports);
        //}

        g_mnuForexBuySel = new TWMenuItem(Language.getString("FX_BUYSELL_TITLE"), "stockreport.gif");
        g_mnuForexBuySel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ForexBuySell fBS = ForexBuySell.getInstance();
                fBS.setKey(getSelectedSymbol());
                fBS.setVisible(true);

            }
        });
//        if (true){
//            g_oTablePopup.add(g_mnuForexBuySel);
//        }

        g_mnuOptions = new TWMenuItem(Language.getString("POPUP_OPTIONS"), "optionchain.gif");
        g_mnuOptions.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mnu_Options(getSelectedSymbol(), null, false, false);
            }
        });
        //if (TWControl.getBooleanItem("OPTION_CHAIN")){
        g_oTablePopup.add(g_mnuOptions);
        //}

        g_mnuFutures = new TWMenuItem(Language.getString("POPUP_FUTURES"), "optionchain.gif");
        g_mnuFutures.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                mnu_FuturesChain(getSelectedSymbol(), null, false, false);
            }
        });
        //if (TWControl.getBooleanItem("OPTION_CHAIN")){
        g_oTablePopup.add(g_mnuFutures);
        //}

        g_mnuNewsSearch = new TWMenuItem(Language.getString("NEWS"), "newssearch.gif");
        g_mnuNewsSearch.setVisible(false);
        g_mnuNewsSearch.setEnabled(false);
        g_mnuNewsSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                searchSymbolNews();
            }
        });
        g_oTablePopup.add(g_mnuNewsSearch);

        /* VAS Menu items begin here*/
        mnuVasStart = new TWMenuItem("VAS_START");
        mnuVasStart.setVisible(false);
        g_oTablePopup.add(mnuVasStart);

        mnuVasEnd = new TWMenuItem("VAS_END");
        mnuVasEnd.setVisible(false);
        g_oTablePopup.add(mnuVasEnd);

        mnuAlert = new TWMenuItem(Language.getString("SET_ALERT"), "alert.gif");
        mnuAlert.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if ((!Settings.isConnected())) {
                    new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                    return;
                }
                SmartAlertWindow.getSharedInstance().setVisible(true);
                SmartAlertWindow.getSharedInstance().loadSmartAleartForSymbol(getSelectedSymbol()); //commented
                // mnu_AlertSetup();

            }
        });
        mnuAlert.setVisible(false);
        mnuAlert.setEnabled(false);
        g_oTablePopup.add(mnuDisable);   //
        g_oTablePopup.add(mnuEnable);
        if (Settings.isTCPMode()) {
            g_oTablePopup.add(mnuAlert);
        }

        mnuExportIntraday = new TWMenuItem(Language.getString("EXPORT_TRADES"), "exporttrades.gif");
        mnuExportIntraday.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                exportCompanyTrades(getSelectedSymbol());
            }
        });
        //  Below feature is transfer to time and sales window
//        g_oTablePopup.add(mnuExportIntraday);

        mnuSymbolFilter = new TWMenuItem(Language.getString("EDIT_FUNCTION"), "editcondition.gif");
        mnuSymbolFilter.setActionCommand(Constants.NULL_STRING);
        mnuSymbolFilter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                editFunctionView();
            }
        });
        g_oTablePopup.add(mnuSymbolFilter);

        mnueditAlias = new TWMenuItem(Language.getString("EDIT_ALIAS"), "editalias.gif");
        mnueditAlias.setActionCommand(Constants.NULL_STRING);
        mnueditAlias.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                editAlias();
            }
        });
        g_oTablePopup.add(mnueditAlias);

        addToWatchList = new TWMenu(Language.getString("SEND_TO_WATCHLIST"), "sendtowatch.gif");
        addToCutomView = new TWMenu(Language.getString("MY_STOCKS"), "mystock.gif");
        addToMistview = new TWMenu(Language.getString("TREE_MY_MIST"), "classicview.gif");

        addToWatchList.addMenuListener(new MenuListener() {

            public void menuSelected(MenuEvent e) {
                addToWatchList.removeAll();
                addToCutomView.removeAll();
                addToMistview.removeAll();
//                addTOPortfolioview.removeAll();
                final String sNewSymbol = getSelectedSymbol();
                if (!sNewSymbol.equals("")) {
                    Vector mainboard_view = ViewSettingsManager.getMainBoardViews();
                    Enumeration en = mainboard_view.elements();
                    while (en.hasMoreElements()) {
                        final ViewSetting vs = (ViewSetting) en.nextElement();
                        if (vs.getMainType() == ViewSettingsManager.CUSTOM_VIEW) {
                            TWMenuItem t1 = new TWMenuItem(vs.getCaption());
                            t1.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent e) {
                                    // SharedMethods.getKey(my_exchange, my_symbol);
                                    Symbols symbols = vs.getSymbols();
                                    ClientTable table = (ClientTable) vs.getParent();
                                    if (!symbols.appendSymbol(sNewSymbol.toUpperCase())) {
                                        int location = symbols.getLocation(sNewSymbol);
                                        if (location >= 0) {
                                            (table.getTable()).setRowSelectionInterval(location * 2, location * 2);
                                            (table.getTable()).setColumnSelectionInterval(0, (table.getTable()).getColumnCount() - 1);
                                            GUISettings.createDragImage(((StringTransferObject) table.getModel().getValueAt(table.getTable().getSelectedRow(), 0)).getValue(), table.getGraphics());
                                        }
                                    }
                                    DataStore.getSharedInstance().addSymbolRequest(sNewSymbol);
                                    TWActions.setIgnoreKeyRelaeseForNextMessageDialog(true);
                                    table.updateUI();
                                    //                                   DataStore.getSharedInstance().addSymbolRequest(sNewSymbol);

                                }
                            });

                            //                        addToCutomView.add(t1);
                            if (mainboardSelected) {
                                //                            System.out.println("selected table id = "+g_oTempTable.getViewSettings().getID());
                                if (!g_oTempTable.getViewSettings().getID().equals(vs.getID())) {
                                    addToCutomView.add(t1);
                                }
                            } else {
                                addToCutomView.add(t1);
                            }
                            //                        addToWatchList.add(addToCutomView);
                        } else if (vs.getMainType() == ViewSettingsManager.MIST_VIEW) {
                            TWMenuItem t2 = new TWMenuItem(vs.getCaption());
                            t2.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent e) {
                                    String sNewSymbol = getSelectedSymbol();   //SharedMethods.getKey(my_exchange, my_symbol);
                                    Symbols symbols = vs.getSymbols();
                                    MISTTable table = (MISTTable) vs.getParent();
                                    if (!symbols.appendSymbol(sNewSymbol.toUpperCase())) {
                                        int location = symbols.getLocation(sNewSymbol);
                                        if (location >= 0) {
                                            (table.getTable().getTable()).setRowSelectionInterval(location * 2, location * 2);
                                            (table.getTable().getTable()).setColumnSelectionInterval(0, (table.getTable().getTable()).getColumnCount() - 1);
                                            GUISettings.createDragImage(((StringTransferObject) table.getModel1().getValueAt(table.getTable().getSelectedRow(), 0)).getValue(), table.getGraphics());
                                        }
                                    }
                                    ((MISTModel) table.getModel1()).setHighLightedRow(table.getTable().getSelectedRow());
                                    ((MISTModel) table.getModel1()).adjustViewPortToSymbol();
                                    DataStore.getSharedInstance().addSymbolRequest(sNewSymbol);
                                    TWActions.setIgnoreKeyRelaeseForNextMessageDialog(true);
                                    table.updateUI();
                                }
                            });
                            if (mistViewSelected) {
                                //                            System.out.println("selected misttable id = "+g_oMistTable.getViewSettings().getID());
                                if (!g_oMistTable.getViewSettings().getID().equals(vs.getID())) {
                                    addToMistview.add(t2);
                                }
                            } else {
                                addToMistview.add(t2);
                            }
                            //                        addToMistview.add(t2);
                            //                        addToWatchList.add(addToMistview);
                        }
                    }
                    addToMistview.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/classicview.gif"));
                    addToCutomView.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/mystock.gif"));
                    if (addToCutomView.getItemCount() > 0) {
                        addToWatchList.add(addToCutomView);
                    }
                    if (addToMistview.getItemCount() > 0) {
//                        addToWatchList.add(addToMistview);
                    }
                }
                GUISettings.applyOrientation(addToCutomView);
                GUISettings.applyOrientation(addToMistview);
                GUISettings.applyOrientation(addToWatchList);
                SwingUtilities.updateComponentTreeUI(addToMistview);
                SwingUtilities.updateComponentTreeUI(addToCutomView);
            }

            public void menuDeselected(MenuEvent e) {
            }

            public void menuCanceled(MenuEvent e) {
            }
        });

        g_oTablePopup.add(addToWatchList);


        g_oTablePopup.add(watchListPopup);

        watchListPopup.add(g_oMenus.getCurrencyPopup());
        //g_oWatchListPopup.add(g_oMenus.getCurrencyPopup());
        watchListPopup.add(g_oMenus.createWatchlistFilterPopup());
        //g_oWatchListPopup.add(g_oMenus.createWatchlistFilterPopup());
        watchListPopup.add(new TWSeparator());
        //g_oWatchListPopup.add(new JSeparator());
        watchListPopup.add(g_mnuTreePopEditView);
        //g_oWatchListPopup.add(g_mnuTreePopEditView);
        watchListPopup.add(mnuTreePopDelete);
        //g_oWatchListPopup.add(mnuTreePopDelete);

//-----------------------------------------------------------------------------------------------------------------------------

        mnuColumnSettings1 = new TWMenuItem(Language.getString("COLUMN_SETTINGS"), "colsetting.gif");
        watchListPopup.add(mnuColumnSettings1);
        //g_oWatchListPopup.add(mnuColumnSettings);
        mnuColumnSettings1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                g_mnuTreePopSettings_Selected();
            }
        });
        g_oWatchListPopup.add(mnuColumnSettings1);
//        if (g_oTempTable != null){
//            setDecimalPlaces(((DraggableTable) g_oTempTable.getTable()).getDecimalPlaces());
//        }else if(mistViewSelected){
////            setDecimalPlaces(((MISTTable) g_oMistTable.getTable()).getDecimalPlaces());
//        }
//        setDecimalPlaces(((DraggableTable) g_oTempTable.getTable()).getDecimalPlaces());
        createDecimalMenu();

        mnuFilter = new TWMenuItem(Language.getString("FILTER"), "filter.gif");
        mnuFilter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                String sym = getSelectedSymbol();
                FilterableTradeStore store = CompanyTradeStore.getSharedInstance().getCompanyTradesStore(sym);
                Symbols symbols;
                if (!mistViewSelected) {
                    symbols = g_oTempTable.getSymbols();
                } else {
                    symbols = g_oMistTable.getViewSettings().getSymbols();
                }
                //Symbols symbols = g_oTempTable.getSymbols();
                MarketWatchFilter mWatchFilter = new MarketWatchFilter(Client.getInstance().getFrame(), g_oTempTable, symbols);
                mWatchFilter.setVisible(true);
                g_oTempTable.getTable().updateUI();
            }
        });
        mnuFilter.setVisible(false);
        watchListPopup.add(mnuFilter);

        mnuFilter1 = new TWMenuItem(Language.getString("FILTER"), "filter.gif");
        mnuFilter1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ev) {
                String sym = getSelectedSymbol();
                FilterableTradeStore store = CompanyTradeStore.getSharedInstance().getCompanyTradesStore(sym);
                Symbols symbols = g_oTempTable.getSymbols();
                MarketWatchFilter mWatchFilter = new MarketWatchFilter(Client.getInstance().getFrame(), g_oTempTable, symbols);
                mWatchFilter.setVisible(true);
                g_oTempTable.getTable().updateUI();
            }
        });
        mnuFilter1.setVisible(false);
        g_oWatchListPopup.add(mnuFilter1);

        mnuTreePopDelete1 = new TWMenuItem(Language.getString("DELETE_VIEW"), "deleteview.gif");
        mnuTreePopDelete1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                g_mnuTreePopDelete_Selected(e);
            }
        });
        mnuTreePopDelete1.setVisible(false);
//        mnuTreePopDelete1.setEnabled(false);
        g_oWatchListPopup.add(mnuTreePopDelete1);

//-----------------------------------------------------------------------------------------------------------------------------
        GUISettings.applyOrientation(g_oTablePopup);
        GUISettings.applyOrientation(g_oWatchListPopup);

        g_oSymbolMouseListener = new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                System.out.println("Right " + SwingUtilities.isRightMouseButton(e));

                String sKeyIn = getSelectedSymbol();

                String exchange = SharedMethods.getExchangeFromKey(sKeyIn);
                //String symbol = SharedMethods.getSymbolFromKey(sKeyIn);

//                if (isLeftGesture()) {
//                    showSnapQuote(sKeyIn, false);
//                    resetGueature();
//                    return;
//                } else if (isRightGesture()) {
//                    try {
//                        //Client.getInstance().showProChart(sKeyIn, null, null);
//                        depthByPrice(sKeyIn, false, Constants.MAINBOARD_TYPE);
//                        resetGueature();
//                    } catch (Exception e1) {
//                    }
//                    return;
//                }

                //Bug Id <#0034> included 'SharedMethods.checkInstrumentType(sKeyIn, Meta.QUOTE)' to prevent market depth popup and rapid order entry for indexes
                if (!SwingUtilities.isRightMouseButton(e)) {
                    if ((sKeyIn != null) && (!sKeyIn.trim().equals(Constants.NULL_STRING)) && (g_oTempTable != null)) {
                        int selColumn = g_oTempTable.getTable().convertColumnIndexToModel(g_oTempTable.getTable().getSelectedColumn());
//                        System.out.println("in client set symbol");
//                        if (MultiDialogWindow.getSharedInstance().isVisible()) {
////                            System.out.println("set symbol called from Client");
//                          //  MultiDialogWindow.getSharedInstance().setSymbol(SharedMethods.getKey(sKeyIn));
//                        }
                        if (e.getClickCount() > 1) {
                            if ((selColumn == StockData.COLUMN_SYMBOL) || (selColumn == StockData.COLUMN_COMPANY_CODE)) { // double clicked symbol column
                                symbolDoubleClicked(sKeyIn, e);
                            } else if (selColumn == StockData.COLUMN_OHLC) {
                                showChart(sKeyIn, null, null);
                            } else if ((!SharedMethods.checkInstrumentType(sKeyIn, Meta.INSTRUMENT_INDEX)) &&
                                    ((selColumn == StockData.COLUMN_BID_PRICE) || (selColumn == StockData.COLUMN_BID_QUANTITY))) { //Bug Id <#0034>
                                if (TradingShared.isReadyForTrading() && TradingShared.isTradableExchange(exchange)) {
                                    if ((DataStore.getSharedInstance().getStockObject(getSelectedSymbol())).getInstrumentType() == Meta.INSTRUMENT_FOREX) {
                                        ForexBuySell fBS = ForexBuySell.getSharedInstance(1, null);//getInstance();
                                        fBS.setKey(getSelectedSymbol());
                                        fBS.setVisible(true);
                                        GUISettings.setLocationRelativeTo(fBS, getDesktop());
                                    } else {
                                        TradeMethods.getSharedInstance().showSellScreen(null);
                                    }
                                } else {
                                    if (ExchangeStore.getSharedInstance().isMarketDepthByPriceAvailable(SharedMethods.getExchangeFromKey(getSelectedSymbol()))) {
                                        depthByPrice(null, false, Constants.MAINBOARD_TYPE);
                                    } else {
                                        showDetailQuote(null);
                                    }
                                }
                            } else if ((!SharedMethods.checkInstrumentType(sKeyIn, Meta.INSTRUMENT_INDEX)) &&
                                    ((selColumn == StockData.COLUMN_ASK_PRICE) || (selColumn == StockData.COLUMN_ASK_QUANTITY))) { //Bug Id <#0034>
                                if (TradingShared.isReadyForTrading() && TradingShared.isTradableExchange(exchange)) {
                                    if ((DataStore.getSharedInstance().getStockObject(getSelectedSymbol())).getInstrumentType() == Meta.INSTRUMENT_FOREX) {
                                        ForexBuySell fBS = ForexBuySell.getSharedInstance(1, null);//getInstance();
                                        fBS.setKey(getSelectedSymbol());
                                        fBS.setVisible(true);
                                        GUISettings.setLocationRelativeTo(fBS, getDesktop());
                                    } else {
                                        TradeMethods.getSharedInstance().showBuyScreen(null);
                                    }
                                } else {
                                    if (ExchangeStore.getSharedInstance().isMarketDepthByPriceAvailable(SharedMethods.getExchangeFromKey(getSelectedSymbol()))) {
                                        depthByPrice(null, false, Constants.MAINBOARD_TYPE);
                                    } else {
                                        showDetailQuote(null);
                                    }
                                }
                            } else if (selColumn == StockData.COLUMN_BID_ASK_RATIO) {
                                if (ExchangeStore.getSharedInstance().isMarketDepthByPriceAvailable(SharedMethods.getExchangeFromKey(getSelectedSymbol()))) {
                                    depthByPrice(null, false, Constants.MAINBOARD_TYPE);
                                } else {
                                    showDetailQuote(null);
                                }
                            } else if ((selColumn == StockData.COLUMN_CASH_FLOW_CASH_MAP) || (selColumn == StockData.COLUMN_CASH_FLOW_NET_VALUE)
                                    || (selColumn == StockData.COLUMN_CASH_FLOW_IN_TURNOVER) || (selColumn == StockData.COLUMN_CASH_FLOW_OUT_TURNOVER)
                                    || (selColumn == StockData.COLUMN_CASH_FLOW_IN_ORDERS) || (selColumn == StockData.COLUMN_CASH_FLOW_OUT_ORDERS)
                                    || (selColumn == StockData.COLUMN_CASH_FLOW_IN_VOLUME) || (selColumn == StockData.COLUMN_CASH_FLOW_OUT_VOLUME)) {
                                if (ExchangeStore.isValidIinformationType(SharedMethods.getExchangeFromKey(sKeyIn), Meta.IT_CashFlow)) {
                                    showCashFlowDetailQuote(null, true, false, false, LinkStore.LINK_NONE);
                                } else {
                                    showDetailQuote(null);
                                }
                            } else {
                                showDetailQuote(null);
                            }
                        } else {
                            if ((!SharedMethods.checkInstrumentType(sKeyIn, Meta.INSTRUMENT_INDEX)) && (rapidOrderEntryPanel.isVisible())) { //Bug Id <#0034>
                                populateRapidOrderPanel();
                            }
                        }
                    } else if ((sKeyIn != null) && (!sKeyIn.trim().equals(Constants.NULL_STRING)) &&
                            (g_oMistTable != null)) {
//                        System.out.println("Double Clicked on MIST Table");
                        int selColumn = g_oMistTable.getTable().getTable().convertColumnIndexToModel(g_oMistTable.
                                getTable().getTable().getSelectedColumn());
                        int selRow = g_oMistTable.getTable().getTable().getSelectedRow();
                        if (((selColumn == 0) || (selColumn == 1)) && (selRow % 2 == 0)) {
                            int newsStatus = DataStore.getSharedInstance().getStockObject(sKeyIn).getNewsAvailability();
                            if ((newsStatus == Constants.ITEM_NOT_READ) ||
                                    (newsStatus == Constants.ITEM_READ)) {
                                if (e.getClickCount() > 1) {
                                    mnu_AnnouncementsSymbol(sKeyIn);
                                }
                            } else {
                                if (e.getClickCount() > 1) {
                                    showDetailQuote(null); //sKeyIn);
                                }
                            }
                        } else {
                            if (e.getClickCount() > 1) {
                                showDetailQuote(null); //sKeyIn);
                            }
                        }
                    }
                }

                int iRow;
                int iCol;
                if (SwingUtilities.isRightMouseButton(e)) {
                    try {
                        watchListPopup.setVisible(true);
                        if (g_oTempTable != null) {
                            if (g_oTempTable.isCustomType()) {
                                mnuAddSymbol.setVisible(true);
                                mnuAddSymbol.setEnabled(true);
                                mnueditAlias.setVisible(true);
                                mnueditAlias.setEnabled(true);
                                mnuUnsort.setVisible(true);
                                mnuUnsort.setEnabled(true);
                                mnuRemoveSymbol.setVisible(true);
                                mnuRemoveSymbol.setEnabled(true);
                                g_mnuMultiDelete.setVisible(true);
                                g_mnuMultiDelete.setEnabled(true);
                                mnuTreePopDelete.setVisible(true);
                                mnuTreePopDelete.setEnabled(true);

//  =================================== Added by Shanika -- To enable/Disable multiple symbols on Watchlists

                                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
                                    isSelected = false;
                                    ((DraggableTable) g_oTempTable.getTable()).setEnableMultipleSymbols();
                                    if (isAllDefault && Settings.isConnected()) {
                                        if (isSelected) {
                                            mnuAllEnable.setVisible(true);
                                            mnuAllDisable.setVisible(false);
                                        }
                                        if (!isSelected) {
                                            mnuAllEnable.setVisible(false);
                                            mnuAllDisable.setVisible(true);
                                        }
                                    } else {
                                        mnuAllEnable.setVisible(false);
                                        mnuAllDisable.setVisible(false);
                                        isAllDefault = true;
                                    }
                                }

//                               setPopupMenu(null,false);
//                                if(!isEnable){
//                                mnuEnable.setVisible(true);
//                                mnuDisable.setVisible(false);
//                                }
//                                else if(isEnable){
//                                 mnuEnable.setVisible(false);
//                                mnuDisable.setVisible(true);
//
//                            }
                                //change start\
                                mnuTreePopDelete1.setVisible(true);
                                mnuTreePopDelete1.setEnabled(true);
                                //change end
                                g_mnuTreePopEditView.setVisible(true);
                                g_mnuTreePopEditView.setEnabled(true);
                                mnuSymbolFilter.setVisible(false);
                                mnuSymbolFilter.setEnabled(false);
                                mnuFilter.setVisible(false);
                                mnuFilter1.setVisible(false);
                                mnuHeatMap.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                                mnuHeatMap.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                                mnuStretchedHeatMap.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                                mnuStretchedHeatMap.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                            } else if (g_oTempTable.isFunctionType()) {

                                mnuAllEnable.setVisible(false);
                                mnuAllDisable.setVisible(false);
                                mnuAddSymbol.setVisible(false);
                                mnuAddSymbol.setEnabled(false);
                                mnueditAlias.setVisible(true);
                                mnueditAlias.setEnabled(true);
                                mnuUnsort.setVisible(true);
                                mnuUnsort.setEnabled(true);
                                mnuRemoveSymbol.setVisible(false);
                                mnuRemoveSymbol.setEnabled(false);
                                g_mnuMultiDelete.setVisible(false);
                                g_mnuMultiDelete.setEnabled(false);
                                mnuTreePopDelete.setVisible(true);
                                mnuTreePopDelete.setEnabled(true);
                                //change start\
                                mnuTreePopDelete1.setVisible(false);
                                mnuTreePopDelete1.setEnabled(false);
                                //change end
                                g_mnuTreePopEditView.setVisible(true);
                                g_mnuTreePopEditView.setEnabled(true);
                                mnuSymbolFilter.setVisible(true);
                                mnuSymbolFilter.setEnabled(true);
                                mnuFilter.setVisible(false);
                                mnuFilter1.setVisible(false);
                                mnuHeatMap.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                                mnuHeatMap.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                                mnuStretchedHeatMap.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                                mnuStretchedHeatMap.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                            } else if (g_oTempTable.isTopStocksType()) {

                                mnuAllEnable.setVisible(false);
                                mnuAllDisable.setVisible(false);
                                mnuAddSymbol.setVisible(false);
                                mnuAddSymbol.setEnabled(false);
                                mnuUnsort.setVisible(false);
                                mnuUnsort.setEnabled(false);
                                mnuRemoveSymbol.setVisible(false);
                                mnuRemoveSymbol.setEnabled(false);
                                g_mnuMultiDelete.setVisible(false);
                                g_mnuMultiDelete.setEnabled(false);
                                mnuTreePopDelete.setVisible(false);
                                mnuTreePopDelete.setEnabled(false);
                                //change start\
                                mnuTreePopDelete1.setVisible(false);
                                mnuTreePopDelete1.setEnabled(false);
                                //change end
                                g_mnuTreePopEditView.setVisible(false);
                                g_mnuTreePopEditView.setEnabled(false);
                                mnuSymbolFilter.setVisible(false);
                                mnuSymbolFilter.setEnabled(false);
                                mnuHeatMap.setVisible(false);
                                mnuHeatMap.setEnabled(false);
                                mnuStretchedHeatMap.setVisible(false);
                                mnuStretchedHeatMap.setEnabled(false);
                                mnuFilter.setVisible(false);
                                mnuFilter1.setVisible(false);
                            } else if ((g_oTempTable.isVolumeWatcherType()) || (g_oTempTable.isMarketType()) || (g_oTempTable.isCashFlowWatcherType())) {

                                mnuFilter.setVisible(false);
                                mnuFilter.setEnabled(false);
                                mnuFilter1.setVisible(false);
                                mnuFilter1.setEnabled(false);
                                if (g_oTempTable.isMarketType()) {

                                    if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
                                        isSelected = false;
                                        ((DraggableTable) g_oTempTable.getTable()).setEnableMultipleSymbols();
                                        if (isAllDefault && Settings.isConnected()) {
                                            if (isSelected) {
                                                mnuAllEnable.setVisible(true);
                                                mnuAllDisable.setVisible(false);
                                            }
                                            if (!isSelected) {
                                                mnuAllEnable.setVisible(false);
                                                mnuAllDisable.setVisible(true);
                                            }
                                        } else {
                                            mnuAllEnable.setVisible(false);
                                            mnuAllDisable.setVisible(false);
                                            isAllDefault = true;
                                        }
                                    }

//  =================================== Added by Shanika -- To enable/Disable multiple symbols on Mainboard

                                    mnuFilter.setVisible(true);
                                    mnuFilter.setEnabled(true);
                                    mnuFilter1.setVisible(true);
                                    mnuFilter1.setEnabled(true);
                                } else {
                                    mnuAllEnable.setVisible(false);
                                    mnuAllDisable.setVisible(false);
                                }
                                mnuAddSymbol.setVisible(false);
                                mnuAddSymbol.setEnabled(false);
                                mnuRemoveSymbol.setVisible(false);
                                mnuRemoveSymbol.setEnabled(false);
                                mnuUnsort.setVisible(false);
                                mnuUnsort.setEnabled(false);
                                g_mnuMultiDelete.setVisible(false);
                                g_mnuMultiDelete.setEnabled(false);
                                mnuTreePopDelete.setVisible(false);
                                mnuTreePopDelete.setEnabled(false);
                                mnuTreePopDelete1.setVisible(false);
                                mnuTreePopDelete1.setEnabled(false);
                                g_mnuTreePopEditView.setVisible(false);
                                g_mnuTreePopEditView.setEnabled(false);
                                mnuSymbolFilter.setVisible(false);
                                mnuSymbolFilter.setEnabled(false);
                                mnuHeatMap.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                                mnuHeatMap.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                                if ((g_oTempTable.isVolumeWatcherType()) || (g_oTempTable.isCashFlowWatcherType())) {

                                    mnuHeatMap.setVisible(false);
                                    mnuHeatMap.setEnabled(false);

                                    g_oMenus.getWatchlistFilterMenu().setEnabled(false);
                                    g_oMenus.getWatchlistFilterMenu().setVisible(false);

                                }

                                mnuStretchedHeatMap.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                                mnuStretchedHeatMap.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));

                            } else {

                                mnuAllEnable.setVisible(false);
                                mnuAllDisable.setVisible(false);
                                mnuFilter.setVisible(false);
                                mnuFilter1.setVisible(false);
                                if (g_oTempTable.isMarketType()) {
                                    mnuFilter.setVisible(true);
                                    mnuFilter1.setVisible(true);
                                }
                                mnuAddSymbol.setVisible(false);
                                mnuAddSymbol.setEnabled(false);
                                mnueditAlias.setVisible(true);
                                mnueditAlias.setEnabled(true);
                                mnuRemoveSymbol.setVisible(false);
                                mnuRemoveSymbol.setEnabled(false);
                                mnuUnsort.setVisible(false);
                                mnuUnsort.setEnabled(false);
                                g_mnuMultiDelete.setVisible(false);
                                g_mnuMultiDelete.setEnabled(false);
                                mnuTreePopDelete.setVisible(false);
                                mnuTreePopDelete.setEnabled(false);
                                mnuTreePopDelete1.setVisible(false);
                                mnuTreePopDelete1.setEnabled(false);
                                g_mnuTreePopEditView.setVisible(false);
                                g_mnuTreePopEditView.setEnabled(false);
                                mnuSymbolFilter.setVisible(false);
                                mnuSymbolFilter.setEnabled(false);
                                mnuHeatMap.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                                mnuHeatMap.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                                mnuStretchedHeatMap.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                                mnuStretchedHeatMap.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                            }
                            iRow = g_oTempTable.getTable().rowAtPoint(new Point(e.getX(), e.getY()));
                            iCol = g_oTempTable.getTable().columnAtPoint(new Point(e.getX(), e.getY()));                    //g_oTablePopup.doLayout();

                            if ((g_oTempTable.getTable().getSelectedColumnCount() > 1)
                                    || (g_oTempTable.getTable().getSelectedRowCount() > 1)) {

                                if (!g_oTempTable.isDetached()) {
                                    GUISettings.showPopup(g_oTableMultiPopup, e.getComponent(), e.getX(), e.getY());
                                }
                            } else {
                                g_oTempTable.getTable().setRowSelectionInterval(iRow, iRow);
                                g_oTempTable.getTable().setColumnSelectionInterval(iCol, iCol);
                                enableCopyMenu(true);
                                enableLinkMenu(true);
                                mnuColumnSettings.setVisible(true);
                                mnuColumnSettings.setEnabled(true);
                                if (!g_oTempTable.isDetached()) {
                                    GUISettings.showPopup(g_oTablePopup, e.getComponent(), e.getX(), e.getY());
                                    Toolkit oToolkit = Toolkit.getDefaultToolkit();
                                    Dimension screenSize = oToolkit.getScreenSize();
                                    if ((g_oTablePopup.getLocationOnScreen().getY() + g_oTablePopup.getHeight()) > (screenSize.getHeight() - 50)) {
                                        int y = (int) (screenSize.height - (g_oTablePopup.getHeight() + 50));
                                        g_oTablePopup.setLocation((int) g_oTablePopup.getLocationOnScreen().getX(), y);
                                    }

//                                    SwingUtilities.invokeLater(new Runnable() {
                                    new Thread() {
                                        public void run() {
                                            if (g_oTableTableMenuToolbar == null) {
                                                g_oTableTableMenuToolbar = new TableMenuToolbar(getFrame());
                                            }
                                            g_oTableTableMenuToolbar.setParentJPopup(g_oTablePopup);
                                            g_oTableTableMenuToolbar.setMistViewSelected(mistViewSelected);
                                            g_oTableTableMenuToolbar.setG_oMistTable(g_oMistTable);
                                            g_oTableTableMenuToolbar.setG_oTempTable(g_oTempTable);
                                            g_oTableTableMenuToolbar.setG_oMenus(g_oMenus);
                                            g_oTableTableMenuToolbar.setMainboardSelected(mainboardSelected);
                                            g_oTableTableMenuToolbar.setPortfolioSelected(portfolioSelected);
                                            g_oTableTableMenuToolbar.setG_oPortfolioTable(g_oPortfolioTable);
                                            g_oTableTableMenuToolbar.setVisible(true);
                                        }
                                    }.start();
                                }
                                ;
                            }

                        } else if (g_oPortfolioTable != null) {
                            mnuAddSymbol.setVisible(true);
                            mnuAddSymbol.setEnabled(true);
                            mnueditAlias.setVisible(true);
                            mnueditAlias.setEnabled(true);
                            mnuRemoveSymbol.setVisible(true);
                            mnuRemoveSymbol.setEnabled(true);
                            mnuUnsort.setVisible(false);
                            mnuUnsort.setEnabled(false);
                            mnuTreePopDelete.setVisible(true);
                            mnuTreePopDelete.setEnabled(true);
                            //change start\
                            mnuTreePopDelete1.setVisible(false);
                            mnuTreePopDelete1.setEnabled(false);
                            //change end
                            g_mnuTreePopEditView.setVisible(true);
                            g_mnuTreePopEditView.setEnabled(true);

                            iRow = g_oPortfolioTable.getTable().getTable().rowAtPoint(new Point(e.getX(), e.getY()));
                            iCol = g_oPortfolioTable.getTable().getTable().columnAtPoint(new Point(e.getX(), e.getY()));                    //g_oTablePopup.doLayout();

                            if ((g_oPortfolioTable.getTable().getTable().getSelectedColumnCount() > 1)
                                    || (g_oPortfolioTable.getTable().getTable().getSelectedRowCount() > 1)) {
                            } else {
                                g_oPortfolioTable.getTable().getTable().setRowSelectionInterval(iRow, iRow);
                                g_oPortfolioTable.getTable().getTable().setColumnSelectionInterval(iCol, iCol);
                                enableCopyMenu(false);
                                enableLinkMenu(false);
                                mnuColumnSettings.setVisible(true);
                                mnuColumnSettings.setEnabled(true);
//                            descriptionmenu.setBackground(Theme.getColor("MAIN_CLICK_SYMBOL_BG_COLOR"));
//                            descriptionmenu.setForeground(Theme.getColor("MAIN_CLICK_SYMBOL_FG_COLOR"));
                                if (!g_oTempTable.isDetached()) {
                                    GUISettings.showPopup(g_oTablePopup, e.getComponent(), e.getX(), e.getY());
                                }
//                            descriptionmenu.setBackground(Theme.getColor("MAIN_CLICK_SYMBOL_BG_COLOR"));
//                            descriptionmenu.setForeground(Theme.getColor("MAIN_CLICK_SYMBOL_FG_COLOR"));
//                            descriptLabel.setPreferredSize(new Dimension(g_oTablePopup.getPreferredSize().width,22));
//                            descriptLabel.setSize(new Dimension(g_oTablePopup.getSize().width,22));
//                            descriptLabel.setBackground(Theme.getColor("MAIN_CLICK_SYMBOL_BG_COLOR"));
//                            descriptLabel.setForeground(Theme.getColor("MAIN_CLICK_SYMBOL_FG_COLOR"));
                            }

                        } else if (mistViewSelected) {
                            mnuAddSymbol.setVisible(true);
                            mnuAddSymbol.setEnabled(true);
                            mnueditAlias.setVisible(true);
                            mnueditAlias.setEnabled(true);
                            mnuRemoveSymbol.setVisible(true);
                            mnuRemoveSymbol.setEnabled(true);
                            mnuUnsort.setVisible(false);
                            mnuUnsort.setEnabled(false);
                            mnuSymbolFilter.setVisible(false);
                            mnuSymbolFilter.setEnabled(false);
                            mnuFilter1.setVisible(false);
                            mnuFilter1.setEnabled(false);
                            mnuFilter.setVisible(false);
                            mnuFilter.setEnabled(false);

                            //------------- Need to add bandwidth validation ---------------------------------------
//                             (g_oMistTable.getTable().getSmartTable()).enableMultipleSybols(false);
                            //  =================================== Added by Shanika -- To enable/Disable multiple symbols on Watchlists

                            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
                                isSelected = false;
                                (g_oMistTable.getTable().getSmartTable()).setEnableMultipleSymbols();
                                if (isAllDefault && Settings.isConnected()) {
                                    if (isSelected) {
                                        mnuAllEnable.setVisible(true);
                                        mnuAllDisable.setVisible(false);
                                    }
                                    if (!isSelected) {
                                        mnuAllEnable.setVisible(false);
                                        mnuAllDisable.setVisible(true);
                                    }
                                } else {
                                    mnuAllEnable.setVisible(false);
                                    mnuAllDisable.setVisible(false);
                                    isAllDefault = true;
                                }
                            }
                            //change start\
                            mnuTreePopDelete1.setVisible(false);
                            mnuTreePopDelete1.setEnabled(false);
                            //change end
                            mnuHeatMap.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                            mnuHeatMap.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                            mnuStretchedHeatMap.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));
                            mnuStretchedHeatMap.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap));

                            iRow = g_oMistTable.getTable1().rowAtPoint(new Point(e.getX(), e.getY()));
                            iCol = g_oMistTable.getTable1().columnAtPoint(new Point(e.getX(), e.getY()));                    //g_oTablePopup.doLayout();

//                        if ((g_oMistTable.getTable1().getSelectedColumnCount() > 1)
//                                || (g_oMistTable.getTable1().getSelectedRowCount() > 1)) {
//                        } else {
//                          g_oMistTable.getTable1().setRowSelectionInterval(iRow, iRow);
//                          g_oMistTable.getTable1().setColumnSelectionInterval(iCol, iCol);
                            g_oMistTable.getTable().setPopupActive(false);
                            enableCopyMenu(false);
                            enableLinkMenu(false);
                            mnuColumnSettings.setVisible(false);
                            mnuColumnSettings.setEnabled(false);
//                            descriptionmenu.setBackground(Theme.getColor("MAIN_CLICK_SYMBOL_BG_COLOR"));
//                            descriptionmenu.setForeground(Theme.getColor("MAIN_CLICK_SYMBOL_FG_COLOR"));
                            if (!g_oMistTable.isDetached()) {
                                if ((g_oMistTable.getTable1().getSelectedColumnCount() > 1)
                                        || (g_oMistTable.getTable1().getSelectedRowCount() > 1)) {
                                    enableCopyMenu(true);
                                    GUISettings.showPopup(g_oTableMultiPopup, e.getComponent(), e.getX(), e.getY());
                                    if (g_oTableTableMenuToolbar != null) {
                                        g_oTableTableMenuToolbar.setVisible(false);
                                    }
                                } else {
                                    enableCopyMenu(false);
                                    GUISettings.showPopup(g_oTablePopup, e.getComponent(), e.getX(), e.getY());
                                    if (g_oTableTableMenuToolbar == null) {
                                        g_oTableTableMenuToolbar = new TableMenuToolbar(getFrame());
                                    }
                                    g_oMistTable.getTable().getPopup().setTMTavailable(false);
                                    g_oTableTableMenuToolbar.setParentJPopup(g_oTablePopup);
                                    g_oTableTableMenuToolbar.setMistViewSelected(mistViewSelected);
                                    g_oTableTableMenuToolbar.setG_oMistTable(g_oMistTable);
                                    g_oTableTableMenuToolbar.setG_oTempTable(g_oTempTable);
                                    g_oTableTableMenuToolbar.setG_oMenus(g_oMenus);

                                    g_oTableTableMenuToolbar.setMainboardSelected(mainboardSelected);
                                    g_oTableTableMenuToolbar.setPortfolioSelected(portfolioSelected);
                                    g_oTableTableMenuToolbar.setG_oPortfolioTable(g_oPortfolioTable);
                                    g_oTableTableMenuToolbar.setVisible(true);
                                }
//                            descriptionmenu.setBackground(Theme.getColor("MAIN_CLICK_SYMBOL_BG_COLOR"));
//                            descriptionmenu.setForeground(Theme.getColor("MAIN_CLICK_SYMBOL_FG_COLOR"));
//                            descriptLabel.setPreferredSize(new Dimension(g_oTablePopup.getPreferredSize().width,22));
//                            descriptLabel.setSize(new Dimension(g_oTablePopup.getSize().width,22));
//                            descriptLabel.setBackground(Theme.getColor("MAIN_CLICK_SYMBOL_BG_COLOR"));
//                            descriptLabel.setForeground(Theme.getColor("MAIN_CLICK_SYMBOL_FG_COLOR"));
                            }


                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }

                    //== todo : remove this
                    if (!TWControl.isMiniTradeDisabled())
                        mnuMiniTrade.setVisible(true);
                }
            }

            public void mousePressed(MouseEvent e) {

                if (SwingUtilities.isRightMouseButton(e)) {
                    lastRightClickTilme = System.currentTimeMillis();
                } else {
                    lastLeftClickTilme = System.currentTimeMillis();
                }
                g_oTempTable = (ClientTable) (SwingUtilities.getAncestorOfClass(ClientTable.class, (Component) e.getSource()));
                g_oPortfolioTable = (PortfolioTable) (SwingUtilities.getAncestorOfClass(PortfolioTable.class, (Component) e.getSource()));
                g_oMistTable = (MISTTable) (SwingUtilities.getAncestorOfClass(MISTTable.class, (Component) e.getSource()));
                g_oFXBoard = (ForexBoard) (SwingUtilities.getAncestorOfClass(ForexBoard.class, (Component) e.getSource()));
                g_oSideBar = (SymbolNavigator) SwingUtilities.getAncestorOfClass(SymbolNavigator.class, (Component) e.getSource());

                if (g_oTempTable != null) { // client table clicked
                    try {
                        if (g_oTempTable.isVolumeWatcherType()) {
                            System.out.println("volume_watcher clicked......");
                            volumeWatcherSelected = true;
                            mainboardSelected = false;
                            portfolioSelected = false;
                            mistViewSelected = false;
                            forexViewSelected = false;
                            sideBarSelected = false;
                            cashFlowWatcherSelected = false;
                        } else if (g_oTempTable.isCashFlowWatcherType()) {
                            cashFlowWatcherSelected = true;
                            volumeWatcherSelected = false;
                            mainboardSelected = false;
                            portfolioSelected = false;
                            mistViewSelected = false;
                            forexViewSelected = false;
                            sideBarSelected = false;
                        } else {
                            mainboardSelected = true;
                            portfolioSelected = false;
                            mistViewSelected = false;
                            forexViewSelected = false;
                            sideBarSelected = false;
                            volumeWatcherSelected = false;
                            cashFlowWatcherSelected = false;
                        }
                    } catch (Exception e1) {
                    }
                    /*g_mnuTreePopEditView.setVisible(TWControl.isServerSideCustOverridden() || ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigWatchList));
                    g_mnuTreePopEditView.setEnabled(TWControl.isServerSideCustOverridden() || ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigWatchList));
                    mnuTreePopDelete.setVisible(TWControl.isServerSideCustOverridden() || ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigWatchList));
                    mnuTreePopDelete.setEnabled(TWControl.isServerSideCustOverridden() || ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigWatchList));*/
                } else if (g_oPortfolioTable != null) { // portfolio table clicked
                    mainboardSelected = false;
                    portfolioSelected = true;
                    mistViewSelected = false;
                    forexViewSelected = false;
                    sideBarSelected = false;
                    volumeWatcherSelected = false;
                    cashFlowWatcherSelected = false;
                    g_mnuTreePopEditView.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigPortfolio));
                    g_mnuTreePopEditView.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigPortfolio));
                    mnuTreePopDelete.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigPortfolio));
                    mnuTreePopDelete.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigPortfolio));
                } else if (g_oMistTable != null) { // Classic view clicked
                    mainboardSelected = false;
                    portfolioSelected = false;
                    mistViewSelected = true;
                    forexViewSelected = false;
                    sideBarSelected = false;
                    volumeWatcherSelected = false;
                    cashFlowWatcherSelected = false;
                    g_mnuTreePopEditView.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigWatchList));
                    g_mnuTreePopEditView.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigWatchList));
                    mnuTreePopDelete.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigWatchList));
                    mnuTreePopDelete.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigWatchList));

                } else if (g_oFXBoard != null) {
                    mainboardSelected = false;
                    portfolioSelected = false;
                    mistViewSelected = false;
                    forexViewSelected = true;
                    sideBarSelected = false;
                    volumeWatcherSelected = false;
                    cashFlowWatcherSelected = false;

                } else if (g_oSideBar != null) {
                    mainboardSelected = false;
                    portfolioSelected = false;
                    mistViewSelected = false;
                    forexViewSelected = false;
                    sideBarSelected = true;
                    volumeWatcherSelected = false;
                    cashFlowWatcherSelected = false;
                }
            }
        };

        g_oTableMouseListener = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                if (SwingUtilities.isRightMouseButton(e)) {
                    if (g_oTempTable != null) {
                        if (g_oTempTable.isFunctionType()) {
                            g_oFramePopup.show(e.getComponent(), e.getX(), e.getY());
                            //change start
                        } else if (g_oTempTable.isCustomType()) {
                            mnuFilter1.setVisible(false);   //
                            mnuTreePopDelete1.setVisible(true);
                            mnuTreePopDelete1.setEnabled(true);
                            if (!g_oTempTable.isDetached()) {
                                g_oWatchListPopup.show(e.getComponent(), e.getX(), e.getY());
                            }
                            //change end
                        } else {
                            if ((g_oTempTable != null) && (g_oTempTable.isMarketType())) {
                                mnuFilter1.setVisible(true);
                                //change start
                                mnuTreePopDelete1.setVisible(false);
                                mnuTreePopDelete1.setEnabled(false);
                                //change end
                            } else {
                                //change start
                                mnuTreePopDelete1.setVisible(false);
                                mnuTreePopDelete1.setEnabled(false);
                                //change end
                                mnuFilter1.setVisible(false);
                            }
                            g_oWatchListPopup.show(e.getComponent(), e.getX(), e.getY());
                        }
                    } else {
                        mnuFilter1.setVisible(false);
                        //change start
                        mnuTreePopDelete1.setVisible(false);
                        mnuTreePopDelete1.setEnabled(false);
                        //change end
//                        g_oWatchListPopup.show(e.getComponent(), e.getX(), e.getY());
                    }
                    /* Focus the table on the view */
                    if (!mistViewSelected) {
//                        ClientTable view = new ClientTable();
                        g_oTempTable = (ClientTable) (SwingUtilities.getAncestorOfClass(ClientTable.class, (Component) e.getSource()));
//                        view = null;
                    }
                    /*if (g_oTempTable.isFunctionType()) {
                        g_oFramePopup.show(e.getComponent(), e.getX(), e.getY());
                    } else {
                        if ((g_oTempTable != null) && (g_oTempTable.isMarketType())){
                            mnuFilter1.setVisible(true);
                        } else {
                            mnuFilter1.setVisible(false);
                        }
                        g_oWatchListPopup.show(e.getComponent(), e.getX(), e.getY());

                    }*
                    /* Focus the table on the view *
                    ClientTable view = new ClientTable();
                    g_oTempTable = (ClientTable) (SwingUtilities.getAncestorOfClass(view.getClass(), (Component) e.getSource()));
                    view = null;*/
                }
            }
        };

    }

    public MouseAdapter getSymbolMouseListener() {
        return g_oSymbolMouseListener;
    }

    public MouseAdapter getTableMouseListener() {
        return g_oTableMouseListener;
    }

    public DefaultMutableTreeNode getMarketWatchTreeNode() {
        return g_oMarketWatchTreeNode;
    }

    public DefaultMutableTreeNode getSectorsTreeNode() {
        return g_oSectorsTreeNode;
    }

    public MapTodaysTrades getTodaysTrades() {
        return g_oTodaysTrades;
    }

    private boolean isRightGesture() {
        if (Math.abs(lastLeftClickTilme - lastRightClickTilme) < 300) {
            if (lastRightClickTilme > lastLeftClickTilme) {
                return true;
            }
        }
        return false;
    }

    private boolean isLeftGesture() {
        if (Math.abs(lastRightClickTilme - lastLeftClickTilme) < 300) {
            if (lastLeftClickTilme > lastRightClickTilme) {
                return true;
            }
        }
        return false;
    }

    private void resetGueature() {
        lastLeftClickTilme = 10000;
        lastRightClickTilme = 0;
    }

    private void symbolDoubleClicked(String key, MouseEvent e) {
        int announcementAvailability = DataStore.getSharedInstance().getStockObject(key).getAnnouncementAvailability();
        int newsAvailability = DataStore.getSharedInstance().getStockObject(key).getNewsAvailability();
        int nonDefaultNewsAvailability = DataStore.getSharedInstance().getStockObject(key).getNewsAvailability();
        boolean other = DataStore.getSharedInstance().getStockObject(key).getOtherSymbolEventAvailbility();
        boolean isSymbolEnabled = DataStore.getSharedInstance().getStockObject(key).isSymbolEnabled();
        switch (announcementAvailability) {
            case Constants.ITEM_NOT_AVAILABLE:
                switch (newsAvailability) {
                    case Constants.ITEM_NOT_AVAILABLE:
                        if (other) {
                            //call other window
                        } else {
                            if (isSymbolEnabled) {
                                showDetailQuote(null);
                            }
                        }
                        break;
                    case Constants.ITEM_NOT_READ:
                    case Constants.ITEM_READ:
                        if (other) {
                            selectNewsOrAnnouncements(key, e.getX(), e.getY(), other, false, true);
                        } else {
                            // call news
                            searchSymbolNews();
                        }
//                        searchSymbolNews();
                        break;
                }
                break;
            case Constants.ITEM_NOT_READ:
                switch (newsAvailability) {
                    case Constants.ITEM_NOT_AVAILABLE:
                        if (other) {
                            selectNewsOrAnnouncements(key, e.getX(), e.getY(), other, true, false);
                        } else {
                            mnu_AnnouncementsSymbol(key);
                        }
                        break;
                    case Constants.ITEM_NOT_READ:
                    case Constants.ITEM_READ:
                        selectNewsOrAnnouncements(key, e.getX(), e.getY(), other, true, true);
                        break;
                }
                break;
            case Constants.ITEM_READ:
                switch (newsAvailability) {
                    case Constants.ITEM_NOT_AVAILABLE:
                        if (other) {
                            selectNewsOrAnnouncements(key, e.getX(), e.getY(), other, true, false);
                        } else {
                            mnu_AnnouncementsSymbol(key);
                        }
                        break;
                    case Constants.ITEM_NOT_READ:
                    case Constants.ITEM_READ:
                        selectNewsOrAnnouncements(key, e.getX(), e.getY(), other, true, true);
                        break;
                }
        }
//        switch (nonDefaultNewsAvailability) {
//            case Constants.ITEM_NOT_AVAILABLE:
////                showDetailQuote(null);
//                break;
//            case Constants.ITEM_NOT_READ:
//            case Constants.ITEM_READ:
//                searchSymbolNews();
//                break;
//        }
    }

    private void selectNewsOrAnnouncements(final String key, final int x, final int y, boolean other, boolean announcement, boolean news) {
        JPopupMenu pop = new JPopupMenu();
        if (news) {
            TWMenuItem mnuNews = new TWMenuItem(Language.getString("NEWS"), "newssearch.gif");
            mnuNews.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    searchSymbolNews();
                }
            });
            pop.add(mnuNews);
            mnuNews.updateUI(); // paint the icon
        }
        if (announcement) {
            TWMenuItem mnuAnnounce = new TWMenuItem(Language.getString("ANNOUNCEMENTS"), "announsearch.gif");
            mnuAnnounce.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    mnu_AnnouncementsSymbol(key);
                }
            });
            pop.add(mnuAnnounce);
            mnuAnnounce.updateUI();
        }
        if (other) {
            Stock st = DataStore.getSharedInstance().getStockObject(key);
            if (st != null) {

                if (st.getSymbolEventStatus(Constants.EVENT_FINANCIAL_CALENDAR) != Constants.ITEM_NOT_AVAILABLE) {
                    TWMenuItem mnuFcal = new TWMenuItem(Language.getString("FINANCIAL_CALENDAR"), "announsearch.gif");
                    mnuFcal.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            mnu_OtherEvents(key);
                        }
                    });
                    pop.add(mnuFcal);
                    mnuFcal.updateUI();
                }
                if (st.getSymbolEventStatus(Constants.EVENT_INSIDER_TRADES) != Constants.ITEM_NOT_AVAILABLE) {
                    TWMenuItem mnuITr = new TWMenuItem(Language.getString("INSIDER_TRADES"), "evtinsidertrade.gif");
                    mnuITr.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            mnu_OtherEvents(key);
                        }
                    });
                    pop.add(mnuITr);
                    mnuITr.updateUI();
                }
                if (st.getSymbolEventStatus(Constants.EVENT_RESEARCH_MATERIAL) != Constants.ITEM_NOT_AVAILABLE) {
                    TWMenuItem mnuRM = new TWMenuItem(Language.getString("RESEARCH_MATERIAL"), "evtresearch.gif");
                    mnuRM.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            mnu_OtherEvents(key);
                        }
                    });
                    pop.add(mnuRM);
                    mnuRM.updateUI();
                }


            }
//            TWMenuItem mnuOther = new TWMenuItem(Language.getString("OTHER_EVENTS"), "announsearch.gif");
//            mnuOther.addActionListener(new ActionListener() {
//                public void actionPerformed(ActionEvent e) {
//                    mnu_OtherEvents(key);
//                }
//            });
//            pop.add(mnuOther);

        }


        GUISettings.applyOrientation(pop);
//        mnuAnnounce.updateUI(); // paint the icon
        pop.show(g_oTempTable.getTable(), x, y);
    }

    private TWMenu createSelectionPopup() {
        TWMenu selectionPopup = new TWMenu(Language.getString("SELECTION_MODE"), "selectionmode.gif");

        TWMenuItem mnuCell = new TWMenuItem(Language.getString("CELL"), "cellselect.gif");
        mnuCell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (mainboardSelected) {
                    ((DraggableTable) g_oTempTable.getTable()).setCellSelectionEnabled(true);
                    ((DraggableTable) g_oTempTable.getTable()).setRowSelectionAllowed(true);
                    ((DraggableTable) g_oTempTable.getTable()).setColumnSelectionAllowed(true);
                } else if (portfolioSelected) {
                    g_oPortfolioTable.getTable().getTable().setCellSelectionEnabled(true);
                    g_oPortfolioTable.getTable().getTable().setRowSelectionAllowed(true);
                    g_oPortfolioTable.getTable().getTable().setColumnSelectionAllowed(true);
                } else if (mistViewSelected) {
                    g_oMistTable.getTable().getTable().setCellSelectionEnabled(true);
                    g_oMistTable.getTable().getTable().setRowSelectionAllowed(true);
                    g_oMistTable.getTable().getTable().setColumnSelectionAllowed(true);
                } else if (volumeWatcherSelected) {

                    VolumeWatcherUI.getSharedInstance().cellSelectionCell();
                } else if (cashFlowWatcherSelected) {
                    CashFlowWatcherUI.getSharedInstance().cellSelectionCell();
                }
            }
        });
        selectionPopup.add(mnuCell);

        TWMenuItem mnuLine = new TWMenuItem(Language.getString("ROW"), "rowselect.gif");
        mnuLine.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (mainboardSelected) {
                    ((DraggableTable) g_oTempTable.getTable()).setCellSelectionEnabled(true);
                    ((DraggableTable) g_oTempTable.getTable()).setRowSelectionAllowed(true);
                    ((DraggableTable) g_oTempTable.getTable()).setColumnSelectionAllowed(false);
                } else if (portfolioSelected) {
                    g_oPortfolioTable.getTable().getTable().setCellSelectionEnabled(true);
                    g_oPortfolioTable.getTable().getTable().setRowSelectionAllowed(true);
                    g_oPortfolioTable.getTable().getTable().setColumnSelectionAllowed(false);
                } else if (mistViewSelected) {
                    g_oMistTable.getTable().getTable().setCellSelectionEnabled(true);
                    g_oMistTable.getTable().getTable().setRowSelectionAllowed(true);
                    g_oMistTable.getTable().getTable().setColumnSelectionAllowed(false);
                } else if (volumeWatcherSelected) {

                    VolumeWatcherUI.getSharedInstance().cellSelectionRow();
                } else if (cashFlowWatcherSelected) {
                    CashFlowWatcherUI.getSharedInstance().cellSelectionRow();
                }
            }
        });
        selectionPopup.add(mnuLine);

        return selectionPopup;
    }

    public TWMenu getCellSelectionPopup() {
        return createSelectionPopup();
    }

    public TWMenuItem getCopyMenu() {
        mnuCopy = new TWMenuItem(Language.getString("COPY"), "copy.gif");
        mnuCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK));
        mnuCopy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //((DraggableTable) g_oTempTable.getTable()).copyCells(false);
                if (!mistViewSelected) {
                    ((DraggableTable) g_oTempTable.getTable()).copyCells(false);
                } else {
                    (g_oMistTable.getTable().getSmartTable()).copyCells(false);
                }
            }
        });
        return mnuCopy;
    }

    public TWMenuItem getEnableMenu() {

        mnuAllEnable = new TWMenuItem(Language.getString("ENABLE_SYMBOL"), "symbolenable.gif");
//        mnuAllEnable.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK));
        mnuAllEnable.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                WATCHLIST_SELECT = true;
                //((DraggableTable) g_oTempTable.getTable()).copyCells(false);
                if (!mistViewSelected) {
                    ((DraggableTable) g_oTempTable.getTable()).enableMultipleSybols(false);
                } else {
                    (g_oMistTable.getTable().getSmartTable()).enableMultipleSybols(false);
                }
            }
        });
        mnuAllEnable.setVisible(false);
        return mnuAllEnable;

    }

    public TWMenuItem getDisableMenu() {

        mnuAllDisable = new TWMenuItem(Language.getString("DISABLE_SYMBOL"), "symboldisable.gif");
//        mnuAllDisable.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK));
        mnuAllDisable.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                WATCHLIST_SELECT = true;
                //((DraggableTable) g_oTempTable.getTable()).copyCells(false);
                if (!mistViewSelected) {
                    ((DraggableTable) g_oTempTable.getTable()).disableMultipleSybols(false);
                } else {
                    (g_oMistTable.getTable().getSmartTable()).disableMultipleSybols(false);
                }
            }
        });
        mnuAllDisable.setVisible(false);
        return mnuAllDisable;

    }


    public TWMenuItem getCopyWithHeadMenu() {
        mnuCopyH = new TWMenuItem(Language.getString("COPY_WITH_HEAD"), "copywh.gif");
        mnuCopyH.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_DOWN_MASK));
        mnuCopyH.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //((DraggableTable) g_oTempTable.getTable()).copyCells(true);
                if (!mistViewSelected) {
                    ((DraggableTable) g_oTempTable.getTable()).copyCells(true);
                } else {
                    (g_oMistTable.getTable().getSmartTable()).copyCells(true);
                }
            }
        });
        return mnuCopyH;
    }


    public TWMenuItem getLinkMenu() {
        mnuLink = new TWMenuItem(Language.getString("LINK_TO_XL"), "link.gif");
        mnuLink.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.CTRL_DOWN_MASK));
        mnuLink.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) { //DDE
                if (!Settings.isConnected()) {
                    new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                } else if (ExchangeStore.isValidSystemFinformationType(Meta.IT_DDE_Link)) {
                    ((DraggableTable) g_oTempTable.getTable()).linkCells(false);
                } else {
                    SharedMethods.showNonBlockingMessage(Language.getString("DDE_NOT_SUBSCRIBED"), JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        return mnuLink;
    }

    public TWMenuItem getLinkMenuWithHeaders() {
        mnuLinkH = new TWMenuItem(Language.getString("LINK_WITH_HEAD"), "linkwh.gif");
        mnuLinkH.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.ALT_DOWN_MASK));
        mnuLinkH.addActionListener(new ActionListener() { //DDE

            public void actionPerformed(ActionEvent e) {
                if (!Settings.isConnected()) {
                    new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                } else if (ExchangeStore.isValidSystemFinformationType(Meta.IT_DDE_Link)) {
                    ((DraggableTable) g_oTempTable.getTable()).linkCells(true);
                } else {
                    SharedMethods.showNonBlockingMessage(Language.getString("DDE_NOT_SUBSCRIBED"), JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        return mnuLinkH;
    }


    public void enableCopyMenu(boolean status) {
        mnuCopy.setVisible(status);
        mnuCopy.setEnabled(status);
        mnuCopyH.setVisible(status);
        mnuCopyH.setEnabled(status);
    }

    public void enableLinkMenu(boolean status) {
        mnuLink.setVisible(status);
        mnuLink.setEnabled(status);
        mnuLinkH.setVisible(status);
        mnuLinkH.setEnabled(status);
    }

    public void deleteMultipleSymbols() {
        //int[] rows = g_oTempTable.getTable().getSelectedRows();
        int[] rows = null;
        if (!mistViewSelected) {
            rows = g_oTempTable.getTable().getSelectedRows();
        } else {
            rows = g_oMistTable.getTable().getTable().getSelectedRows();
        }
        String[] symbols;


        if (rows.length > 0) {

            ShowMessage oMessage = new ShowMessage(Language.getString("MSG_DELETE_SYMBOL"),
                    "W", true);

            int retVal = oMessage.getReturnValue();

            if (retVal != 1) return;

            symbols = new String[rows.length];
            TableModel oModel;

            /*oModel = g_oTempTable.getModel();
            for (int i = 0; i < rows.length; i++) {
                symbols[i] = (String) oModel.getValueAt(rows[i], 46) +
                        Constants.KEY_SEPERATOR_CHARACTER + (String) oModel.getValueAt(rows[i], 0);
            }

            for (int i = 0; i < rows.length; i++) {
                g_oTempTable.getSymbols().removeSymbol(symbols[i]);
                sendRemoveRequest(symbols[i]);
            }

            g_oTempTable.getTable().updateUI();*/
            if (!mistViewSelected) {
                oModel = g_oTempTable.getModel();
                for (int i = 0; i < rows.length; i++) {
                    try {
                        symbols[i] = SharedMethods.getKey(((StringTransferObject) oModel.getValueAt(rows[i], -2)).getValue(),
                                ((StringTransferObject) oModel.getValueAt(rows[i], -1)).getValue(), (int) ((LongTransferObject) oModel.getValueAt(rows[i], -11)).getValue());
                    } catch (Exception e) {
//                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                for (int i = 0; i < rows.length; i++) {
                    g_oTempTable.getSymbols().removeSymbol(symbols[i]);
                    sendRemoveRequest(symbols[i]);
                }
                g_oTempTable.getTable().setRowSelectionInterval(0, 0);
                g_oTempTable.getTable().updateUI();

            } else {
                oModel = g_oMistTable.getModel1();
                for (int i = 0; i < rows.length; i++) {
                    if (g_oMistTable.getTable1().getRowCount() != (rows[i] + 1)) { // if not last row
                        symbols[i] = g_oMistTable.getViewSettings().getSymbols().getFilteredSymbols()[(int)
                                rows[i] / 2];
                    }
                }

                for (int i = 0; i < rows.length; i++) {
                    if (symbols[i] != null) {
                        g_oMistTable.getViewSettings().getSymbols().removeSymbol(symbols[i]);
                    }
                }
                g_oMistTable.getTable().updateUI();
            }
            TradeFeeder.refreshSymbolIndex();
        }
        rows = null;
        symbols = null;

    }

    private Object getFilterMenu() {

        TWMenuItem mnuCust = new TWMenuItem(Language.getString("EDIT_FUNCTION"), "editcondition.gif");
        mnuCust.setActionCommand(Constants.NULL_STRING);
        mnuCust.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                editFunctionView();
            }
        });

        TWMenuItem mnuDelete = new TWMenuItem(Language.getString("DELETE_VIEW"), "deleteview.gif");
        mnuDelete.setActionCommand(Constants.NULL_STRING);
        mnuDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                g_mnuTreePopDelete_Selected(e);
            }
        });
        //JPopupMenu mnuFilter = new JPopupMenu(Language.getString("EDIT_FUNCTION"));
        JPopupMenu mnuFilter = new JPopupMenu();
        mnuFilter.add(mnuCust);
        mnuFilter.add(mnuDelete);
        return mnuFilter;
    }

    public void editAlias() {
        new EditAliasDialog();
    }

    public void editFunctionView() {
        if (isValidSystemWindow(Meta.IT_FunctionBuilder, false)) {
//            functionBuilder.setLocationRelativeTo(window);
            functionBuilder.setFunction(((FunctionListStore) g_oTempTable.getSymbols()).getFunction());
            String oldName = g_oTempTable.getViewSettings().getCaptions();
            functionBuilder.setName(oldName);
            functionBuilder.show();

            String name = null;
            while (true) {
                name = functionBuilder.getName();
                if (name == null) {
                    return;
                } else if ((!oldName.equalsIgnoreCase(name)) && (!isValidName(Language.getLanguageSpecificString(name, ","), ViewSettingsManager.FILTERED_VIEW))) {
                    new ShowMessage(false, Language.getString("MSG_DUPLICATE_VIEW_NAME"), "E");
                } else {
                    break;
                }
                functionBuilder.show();
            }
            final String function = functionBuilder.getFunction();
            g_oTempTable.getViewSettings().setCaptions(name);
            g_oTempTable.setTypeDescription(Language.getString("FUNCTION_WATCHLIST"));
            g_oTempTable.setTitle(g_oTempTable.getViewSettings().getCaption());
            oTree.updateUI();
            ((FunctionListStore) g_oTempTable.getViewSettings().getSymbols()).setCaptions(name);
            //g_oTempTable.updateGUI();

            SwingUtilities.invokeLater(new Runnable() {
                public void run() {
                    if (function != null) {
                        ((FunctionListStore) g_oTempTable.getSymbols()).setFunction(function);
                        g_oTempTable.getSymbols().reFilter();
                        g_oTempTable.getTable().updateUI();
                    }
                }
            });
        }
    }


    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {

        symbolBar = new Navigator();
        symbolBar.setLayer(GUISettings.SYMBOLBAR_LAYER);
        symbolBar.getKeybordListener().requestFocus();
        symbolBar.setLocation(-1000, -1000);
        oTopDesktop.add(symbolBar);

        //KSE new CR-by chandika
        symbolSearchBar = SearchNavigator.getSharedInstance();
        symbolSearchBar.setLayer(GUISettings.SYMBOLBAR_LAYER);
        symbolSearchBar.getKeybordListener().requestFocus();
        symbolSearchBar.setLocation(-1000, -1000);
        symbolSearchBar.setSize(200, 60);
        symbolSearchBar.setResizable(false);
        oTopDesktop.add(symbolSearchBar);


        window.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        customizeFileChooser();
        customizeColorChooser();

        window.getContentPane().setLayout(borderLayout1);
        window.setTitle(UnicodeUtils.getNativeString(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER));
        window.setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));

        if (Settings.isDualScreenMode()) {
            Rectangle virtualBounds = new Rectangle();
            GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
            GraphicsDevice[] gs = ge.getScreenDevices();
            for (int j = 0; j < gs.length; j++) {
                GraphicsDevice gd = gs[j];
                GraphicsConfiguration[] gc = gd.getConfigurations();
                for (int i = 0; i < gc.length; i++) {
                    virtualBounds = virtualBounds.union(gc[i].getBounds());
                }
            }

            window.setBounds(virtualBounds);
            if (Settings.getScreenSize() != null) {
                window.setSize(Settings.getScreenSize());
            }
        } else {
            Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
            window.setSize(new Dimension((int) d.getWidth(), (int) d.getHeight() - 25));
        }

        MetaStockManager.getInstance().init();

        ExchangeStore.initialize();

        g_oViewSettings = new ViewSettingsManager();

        ExchangeMap.initialize();

        CurrencyStore.initialize();

        LogoManager.setPtogress(6);
        SymbolMaster.loadStocks();

        g_oTodaysTrades = new MapTodaysTrades();
        DataStore.initialize();

        LogoManager.setPtogress(10);

        DefaultSettingsManager.getSharedInstance().loadSettings();

        SectorStore.initialize();

        HistoryDownloadManager.initialize();

        TradeSummaryStore.getSharedInstance().load();

        DepthStore.getInstance();

        OHLCStore.getInstance();

        LogoManager.setPtogress(12);
        g_oTradeStore = new TradeStore();

        LogoManager.setPtogress(23);
        g_oNetCalcStore = new NetCalcStore();

        oTopDesktop.putClientProperty("JDesktopPane.dragMode", "outline");

/* #############################################################################
        Create the tree
   ############################################################################*/
        g_oTreeTop = new DefaultMutableTreeNode(Constants.NULL_STRING);
        g_oTreeRenderer = new TreeRenderer();

        oTree = new JTree(g_oTreeTop);
        oTree.setRootVisible(false);
        oTree.setFocusable(false);
        oTree.setShowsRootHandles(false);
        oTree.putClientProperty("jTree.lineStyle", "Angled");
        oTree.setCellRenderer(g_oTreeRenderer);
        oTree.setBackground(Theme.getColor("MENU_BGCOLOR"));
        oTree.setToggleClickCount(1);

        Vector oViewSettingVec = ViewSettingsManager.getViewsVector();
        Object[] views = oViewSettingVec.toArray();
        ViewSettingComparator comparator = new ViewSettingComparator();
        comparator.setColumn(ViewSettingComparator.CAPTION);
        Arrays.sort(views, comparator);

        g_oSectorsTreeNode = new DefaultMutableTreeNode(Language.getString("MARKET_SECTORS"));
        g_oForexTreeNode = new DefaultMutableTreeNode(Language.getString("FOREX_WATCH"));
        DefaultMutableTreeNode oNodeForexWatchList = new DefaultMutableTreeNode(new TreeMenuObject(Language.getString("WHATCH_LIST_CREATE_NEW"), 3));
        g_oForexTreeNode.add(oNodeForexWatchList);
        g_oMyStocksTreeNode = new DefaultMutableTreeNode(Language.getString("MY_STOCKS"));
        DefaultMutableTreeNode oNodeNewMyStock = new DefaultMutableTreeNode(new TreeMenuObject(Language.getString("WHATCH_LIST_CREATE_NEW"), 0));

        g_oMyStocksTreeNode.add(oNodeNewMyStock);
        g_oTreeTop.add(g_oMyStocksTreeNode);

        g_oFunctionTreeNode = new DefaultMutableTreeNode(Language.getString("FUNCTION_LISTS"));
        DefaultMutableTreeNode oNodeNewConditionalWL = new DefaultMutableTreeNode(new TreeMenuObject(Language.getString("WHATCH_LIST_CREATE_NEW"), 1));

        g_oFunctionTreeNode.add(oNodeNewConditionalWL);
        g_oTreeTop.add(g_oFunctionTreeNode);

        g_oMarketWatchTreeNode = new DefaultMutableTreeNode(Language.getString("MARKET_WATCH"));

        g_oPortfolioTreeNode = new DefaultMutableTreeNode(Language.getString("TREE_MY_PORTFOLIO"));
        g_oMISTTreeNode = new DefaultMutableTreeNode(Language.getString("TREE_MY_MIST"));
        DefaultMutableTreeNode oNodeNewClassicView = new DefaultMutableTreeNode(new TreeMenuObject(Language.getString("WHATCH_LIST_CREATE_NEW"), 2));

//        g_oMISTTreeNode.add(oNodeNewClassicView);

        views = null;
        comparator = null;

//        g_oTreeTop.add(g_oMISTTreeNode);
//        g_oTreeTop.add(g_oForexTreeNode);
        g_oTreeTop.add(g_oMarketWatchTreeNode);
        oTree.expandPath(new TreePath(g_oSectorsTreeNode.getPath()));

        g_oTreeTop.add(g_oSectorsTreeNode);

        CustomFormulaStore.getSharedInstance().load();

        CustomIndexStore.getSharedInstance().load();

        WatchListManager.getInstance().load();
        FunctionListManager.getInstance().load();

        LogoManager.setPtogress(40);
        mainBoardLoader = new MainBoardLoader(this);
        LogoManager.setPtogress(72);

        functionBuilder = new FunctionBuilder(getInstance().getFrame());//Create the function builder dialog

        twUpdateDownloader = new TWUpdateDownloader();

        g_oMarketWatchPanel.setLayout(new BorderLayout());

        toolBar = new ToolBar(this);

        Theme.setSelectedID(Settings.getLastThemeID());
        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new TWActions(this));
        createManinMenu();

        setLastConnectedTime();
        ExchangeStore.getSharedInstance().addExchangeListener(g_oMenus);
        CurrencyStore.getSharedInstance().addCurrencyListener(g_oMenus);

        bottomtickerPanel = new JPanel(new BorderLayout(0, 0)) {
            public void revalidate() {

            }
        };

        tickerPanel = new JPanel(new BorderLayout(0, 0)) {
            public void revalidate() {
            }
        };

        quickTradePanel = new JPanel(new BorderLayout(0, 0)) {
            public void revalidate() {
            }
        };
        rapidOrderEntryPanel = new RapidOrderEntryPanel();
        rapidOrderEntryPanel.setVisible(false);
        quickTradePanel.add(rapidOrderEntryPanel);

        g_oMarketWatchPanel.add(toolBar, BorderLayout.NORTH);

        new PanelSetter(tickerPanel, g_oMarketWatchPanel, window);

        g_oMarketWatchPanel.add(tickerPanel, BorderLayout.SOUTH);
        g_oMarketWatchPanel.add(quickTradePanel, BorderLayout.CENTER);
        ViewSetting settings = new ViewSetting();
        g_oTicker = new Ticker(settings);
//        g_oTicker.setPreferredSize(new Dimension(500, 55));
//        g_oTicker = new Ticker(settings);
        g_uTicker = new UpperTicker(settings);
        g_mTicker = new MiddleTicker(settings);
        g_lTicker = new LowerTicker(settings);

//        g_oTicker.setPreferredSize(new Dimension(500, 55));
//         g_oTicker.setPreferredSize(new Dimension(500, 55));
//         g_oTicker.setPreferredSize(new Dimension(500, 55));

//        new PanelSetter(tickerPanel, g_oMarketWatchPanel, window);

        statusBarPanel = new StatusBar();
        statusBarPanel.setOpaque(true);

        boolean image2Available = false;
        boolean image1Available = false;
        TWImageLabel lblCompanyLogo1 = new TWImageLabel();
        lblCompanyLogo1.setImageFileName("companyLogo1_" + Language.getLanguageTag() + ".gif");
        TWImageLabel lblCompanyLogo2 = new TWImageLabel();
        lblCompanyLogo2.setImageFileName("companyLogo2_" + Language.getLanguageTag() + ".gif");
        JPanel pnlConnectionStatus = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 0));
        pnlConnectionStatus.setOpaque(false);
        lblCompanyLogo1.setAlignmentX(SwingConstants.CENTER);
        lblCompanyLogo2.setHorizontalAlignment((int) SwingConstants.TRAILING);
        lblExpiryStatus.setFont(lblExpiryStatus.getFont().deriveFont(Font.BOLD));
        lblExpiryStatus.setCursor(new Cursor(Cursor.HAND_CURSOR));
        lblExpiryStatus.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                showEntitlements(false);
            }
        });

        pnlConnectionStatus.add(lblStatus);
        pnlConnectionStatus.add(lblDownloadStatus);
        lblStatus.setVerticalAlignment(SwingConstants.CENTER);
        lblDownloadStatus.setHorizontalAlignment(SwingConstants.LEADING);
        lblDownloadStatus.setVerticalAlignment(SwingConstants.CENTER);
        statusBarPanel.add(pnlConnectionStatus);
        //statusBarPanel.add(lblCompanyLogo1);
        File image1Test = new File("./images/theme" + Theme.getID() + "/companyLogo1_" + Language.getLanguageTag() + ".gif");
        if (image1Test.isFile()) { // company logo 1  is available
            image1Available = true;
        }
        File image2Test = new File("./images/theme" + Theme.getID() + "/companyLogo2_" + Language.getLanguageTag() + ".gif");
        if (image2Test.isFile()) { // company logo 2  is available
            image2Available = true;
        }
        JPanel expiaryAndLoyalty = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 0));
        expiaryAndLoyalty.setOpaque(false);
        expiaryAndLoyalty.add(loyaltyPoints);
        expiaryAndLoyalty.add(lblExpiryStatus);

        if (!image1Available && !image2Available) {
            statusBarPanel.add(lblCompanyLogo1);
            statusBarPanel.add(expiaryAndLoyalty);
//            statusBarPanel.add(lblExpiryStatus);
        } else if (!image1Available && image2Available) {
            statusBarPanel.add(expiaryAndLoyalty);
//            statusBarPanel.add(lblExpiryStatus);
            statusBarPanel.add(lblCompanyLogo2);
        } else if (image1Available && !image2Available) {
            statusBarPanel.add(lblCompanyLogo1);
            statusBarPanel.add(expiaryAndLoyalty);
//            statusBarPanel.add(lblExpiryStatus);
        } else {
            statusBarPanel.add(lblCompanyLogo1);
            statusBarPanel.add(lblCompanyLogo2);
        }
        statusBarPanel.getLayout().layoutContainer(statusBarPanel);

        window.getContentPane().add(g_oMarketWatchPanel, BorderLayout.NORTH);
        window.getContentPane().add(oTopDesktop, BorderLayout.CENTER);

        // class filter menu
        g_oFramePopup = (JPopupMenu) getFilterMenu();
        GUISettings.applyOrientation(g_oFramePopup);

        setStatus(Language.getString("NOT_CONNECTED"));
        //UIManager.addPropertyChangeListener(new UISwitchListener((JComponent) window.getRootPane()));
        LogoManager.setPtogress(78);

        windowTabs = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Bottom, TWTabbedPane.CONTENT_PLACEMENT.Simulated, "dt");
        GUISettings.applyOrientation(windowTabs);

        createTabbedpanel();

        /*themeChanged(Settings.getLastThemeID());*/

// #############################################################################
//      Add Watch lists
// #############################################################################
        WatchListStore[] watchListStores = WatchListManager.getInstance().getStores();
        WatchListStore watchListStore;
        for (int s = 0; s < watchListStores.length; s++) {
            watchListStore = watchListStores[s];
            if (watchListStore.getListType() == WatchListStore.MIST_LIST_TABLE_TYPE) {
                createNewMistView(false, watchListStore);
            } else if (watchListStore.getListType() == WatchListStore.FOREX_TYPE_TYPE) {
                createNewForexView(false, watchListStore);
            } else if (watchListStore.getListType() == WatchListStore.NORMAL_LIST_TABLE_TYPE) {

                createNewView(false, watchListStore);
            } else {
                SymbolPanelStore.getSharedInstance().setSymbols(watchListStore.getSymbols());

            }
        }
        watchListStores = null;
        watchListStore = null;

// #############################################################################
//      Add Function lists
// #############################################################################
//        System.out.println("function list store laodede =="+(System.currentTimeMillis()/1000));
        Object[] stores = FunctionListManager.getInstance().getStores();
        FunctionListStore functionListStore;
        for (int s = 0; s < stores.length; s++) {
            functionListStore = (FunctionListStore) stores[s];
            createNewFilteredView(false, functionListStore);
        }
        stores = null;
        functionListStore = null;

        oTopDesktop.revalidate();
        oTopDesktop.updateUI();
        oTree.updateUI();

        tickerFrame = new TickerFrame(Language.getString("TICKER"), settings) {
            public Dimension getPreferredSize() {
                return getSize();
            }

            public void windowClosing() {

            }
        };
        tickerFrame.setResizeWindowOnTitleToggle(true);
        settings.setParent(tickerFrame);
        tickerFrame.getContentPane().add(g_oTicker);
        oTopDesktop.add(tickerFrame);
        TradeFeeder.setVisible(false);
        tickerFrame.setClosable(true);
        tickerFrame.setDetachable(true);
        tickerFrame.setTickerType();
        g_oTicker.setParent(tickerFrame);
        tickerFrame.setWindowType(Meta.WT_Ticker);
        LogoManager.setPtogress(82);

        /*  Initialize the ticker filter */
        TWTypes.TickerFilter tickerMode;
        String filterData = Settings.getTickerFilterData();
        ArrayList<Symbols> symbols = new ArrayList<Symbols>();
        Symbols[] tickerFilter = new Symbols[0];

        if ((Settings.getTickerFilter() != null) && ((Settings.getTickerFilter()).equals(TWTypes.TickerFilter.MODE_WATCHLIST.toString()))) {
            tickerMode = TWTypes.TickerFilter.MODE_WATCHLIST;
            if (filterData != null) {
                WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
                for (WatchListStore watchlist : watchlists) {
                    if (filterData.indexOf(watchlist.getId()) >= 0) {
                        symbols.add(watchlist);
                    }
                }
                tickerFilter = symbols.toArray(tickerFilter);
            }
        } else {
            tickerMode = TWTypes.TickerFilter.MODE_ALL;
            tickerFilter = new Symbols[0];
        }
        TradeFeeder.setFilter(tickerMode, tickerFilter);
        filterData = null;
        symbols = null;
        tickerFilter = null;

        floatMenu = new TWMenuItem(Language.getString("FIX_TICKER"), "floatticker.gif");
        /* floatMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (isTickerFixed()) {
                    floatTicker();
                } else {
                    fixTicker();
                }
            }
        });*/
//        floatMenu.add(mnuTickerFixTop);
//        g_oTicker.getMenu().add(floatMenu, 4);

//         TWMenuItem floatmnu = (TWMenuItem) g_oTicker.getMenu().getComponent(4);
////        floatmnu.setText(Language.getString("FLOAT_TICKER"));
//        floatmnu.add(mnuTickerFixTop);

        GUISettings.applyOrientation(g_oTicker.getMenu()); // make sure the added menu item is in correct orientation
        tickerFrame.setResizable(true);
//        tickerFrame.setBounds(0, 0, 500, 55);   // ---------- Shanika -----------
        tickerFrame.setLayer(GUISettings.TICKER_LAYER);

//        g_oIndicesFrame =mainBoardLoader.createIndicesFrame();
        g_oIndicesFrame = IndicesFrame.getSharedInstance();//mainBoardLoader.createIndicesFrame();
        g_oIndicesFrame.popuplateExchanges();

        TradeDownloaderUI.getSharedInstance().init();

        OHLCDownloaderUI.getSharedInstance().init();

//        TopStocksWindow.init();

//        System.out.println("111167"+System.currentTimeMillis());

//        createBrodMessageFrame();

//        System.out.println("111168"+System.currentTimeMillis());

        if (Language.isLTR()) {
            window.getContentPane().add(SideBar.getSharedInstance().createSideBar(), BorderLayout.WEST);
        } else {
            window.getContentPane().add(SideBar.getSharedInstance().createSideBar(), BorderLayout.EAST);
        }
        window.getContentPane().add(createBottomPanel(), BorderLayout.SOUTH);

        //------------------------- Added by Shanika--- to load the optimized symbols--

        /*  if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
        try {
            MainUI.loadSymbols();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        }*/
//       createPortfolioView(false);

//        System.out.println("111170"+System.currentTimeMillis());

//       createTradingPortfolioView(false);

//        System.out.println("111171"+System.currentTimeMillis());

        LogoManager.setPtogress(88);
        g_oWindowDaemon = new WindowDaemon(g_oViewSettings);

        /*try {
            MultiDialogWindow.getSharedInstance();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }*/

        LogoManager.setPtogress(97);
        /*
        Bellow two lines were commented and added to thread gropings to optimize thread usage
         */
//        IndexPanel.getInstance().activate(); // must activate after applying the workspace
//        StretchedIndexPanel.getInstance().activate();
        StretchedIndexPanel.getInstance().refresh();

        setProxySettings();

        System.out.println("111177" + System.currentTimeMillis());

        if (!Language.isLTR()) {
            GUISettings.applyOrientation(window, ComponentOrientation.RIGHT_TO_LEFT);
            oTree.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        }

//        KeyboardFocusManager.getCurrentKeyboardFocusManager().addKeyEventDispatcher(new TWActions(this));

        if (Settings.isFullScreenMode()) {
            statusBarPanel.setVisible(false);
        }
        VariationMapUpdator.activate();

        ToolTipManager.sharedInstance().setLightWeightPopupEnabled(false);
        removeTabIndexPanel();


        themeChanged(Settings.getLastThemeID());
//        try {
//            ConfigLoader configloader = new ConfigLoader(); //mix cofiguration load
//            Settings.TRADE_ROUTER_PORT = com.dfn.mtr.mix.util.Settings.MIX_END_CLIENT_PORT;
//        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
//        themeChanged(Settings.getLastThemeID()); // dont ask why? Theme does not apply with one call. fix it if u can.

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                IndicatorFactory.rebuildAssembly();
                RadarIndicatorFactory.rebuildAssembly();
            }
        });
    }

    public TWFrame getWindow() {
        return window;
    }

    public boolean hideShowToolBar() {
        toolBar.setVisible(!toolBar.isVisible());
        return toolBar.isVisible();
    }

    public void hideShowToolBar(boolean status) {
        toolBar.setVisible(status);
        setShowHideButtons(toolBar.isVisible());
    }

    public boolean isToolBarVisible() {
        return toolBar.isVisible();
    }

    /*
    * Sets the focus to the selected table component
    */

    public void valueChanged() //TreeSelectionEvent e)
    {
        ClientTable oClientTable;

        SectorOverviewUI overViewUI;

        DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                oTree.getLastSelectedPathComponent();
        if (node == null) return;
        try {
            Object nodeInfo = node.getUserObject();

            if (node.isLeaf()) {
                try {
                    if (nodeInfo instanceof MISTTable) {
                        mainboardSelected = false;
                        portfolioSelected = false;
                        mistViewSelected = true;
                        forexViewSelected = false;
                        MISTTable view = (MISTTable) nodeInfo;
                        if (!view.isVisible()) {
                            if (view.isDetached()) {
                                ((JFrame) view.getDetachedFrame()).setExtendedState(JFrame.NORMAL);
                                ((JFrame) view.getDetachedFrame()).setVisible(true);
                            } else {
                                view.showWindow();
//                                view.setVisible(true);
                            }
                        } else {
                            view.setSelected(true);
                        }
                        g_oMistTable = view;
                        view = null;

                    } else if (nodeInfo instanceof TreeMenuObject) {
                        TreeMenuObject treemenu = (TreeMenuObject) nodeInfo;
                        if (treemenu.getType() == 0) {
                            this.createNewView(true, null);
                        } else if (treemenu.getType() == 1) {
                            this.createNewFilteredView(true, null);
                        } else if (treemenu.getType() == 2) {
                            this.createNewMistView(true, null);
                        } else if (treemenu.getType() == 3) {
                            this.createNewForexView(true, null);
                        }
                    } else if (nodeInfo instanceof ClientTable) {
                        oClientTable = (ClientTable) nodeInfo;
                        if (oClientTable.isFunctionType() && (!isValidSystemWindow(Meta.IT_FunctionBuilder, false))) {
                            oClientTable = null;
                            return;
                        }
                        mainboardSelected = true;
                        portfolioSelected = false;
                        mistViewSelected = false;
                        forexViewSelected = false;
                        oClientTable.setIcon(false);
                        if (!oClientTable.isVisible()) {
                            if (oClientTable.isDetached()) {
                                ((JFrame) oClientTable.getDetachedFrame()).setExtendedState(JFrame.NORMAL);
                                ((JFrame) oClientTable.getDetachedFrame()).setVisible(true);
                            } else {
                                oClientTable.showWindow();
                            }
                        } else {
                            oClientTable.setSelected(true);
                        }
                        g_oTempTable = oClientTable;
                        oClientTable = null;
                    } else if (nodeInfo instanceof SectorOverviewUI) {
                        System.out.println("im a sector overview node------------");
                        overViewUI = (SectorOverviewUI) nodeInfo;
                        overViewUI.setUISelected(true);
//                        overViewUI.setVisible(true);
                        SectorOverviewUpdator.getSharedInstance().sectorExchangesGatherer(((SectorOverviewUI) nodeInfo).getExObject());
                        //---Commented -09.05.21 -
//                        getInstance().startSectorGatherer();
                        SectorOverviewUpdator.getSharedInstance().sectorTempExchangesGatherer(((SectorOverviewUI) nodeInfo).getExObject());
                        overViewUI.setVisible(true);

                    } else if (nodeInfo instanceof ForexBoard) {
                        mainboardSelected = false;
                        portfolioSelected = false;
                        mistViewSelected = false;
                        forexViewSelected = true;
                        ForexBoard fb = (ForexBoard) nodeInfo;
                        try {
                            this.getDesktop().add(fb);
                        } catch (Exception e) {
                            this.getDesktop().add(fb);
                        }
                        fb.setVisible(true);

                    }
                    /*oClientTable = (ClientTable) nodeInfo;
                    if (oClientTable.isFunctionType() && (!isValidSystemWindow(Meta.IT_FunctionBuilder, false))) {
                        oClientTable = null;
                        return;
                    }
                    mainboardSelected = true;
                    portfolioSelected = false;
                    mistViewSelected = false;
                    oClientTable.setIcon(false);
                    if (!oClientTable.isVisible())
                        oClientTable.showWindow();
                    else
                        oClientTable.setSelected(true);
                    g_oTempTable = oClientTable;
                    oClientTable = null;*/

                } catch (Exception e) {
                    e.printStackTrace();
                }
                g_oMenus.closeTreePopup();
//                }

            }
        } catch (Exception x) {
            x.printStackTrace();
        }
    }

    public void startSectorGatherer() {
//        if (feeders == null) {
        if (SectorOverviewUpdator.tempexchangesList.isEmpty() || feeders == null) {

            feeders = new SectorOverviewUpdator();
            feederThreads = new Thread(feeders, "Sector Overview Updator");
            feederThreads.start();
        }
    }

    public void stopSectorGatherer() {
        if (SectorOverviewUpdator.tempexchangesList.size() == 0) {

            SectorOverviewUpdator.isWIndowActive = false;
            feeders = null;
        }

    }

    public InternalFrame getTickerFrame() {
        return tickerFrame;
    }


    public RapidOrderEntryPanel getRapidOrderPanel() {
        return rapidOrderEntryPanel;
    }

    public void populateRapidOrderPanel() {
        if (TradingShared.isReadyForTrading()) {
            Stock stock = DataStore.getSharedInstance().getStockObject(getSelectedSymbol());
            if (stock != null) {
                rapidOrderEntryPanel.clear();
                rapidOrderEntryPanel.populateOrder(stock);
                rapidOrderEntryPanel.focusQuantity();
            } else {
                rapidOrderEntryPanel.clear();
                rapidOrderEntryPanel.focusSymbol();
            }
            showRapidOrderPanel(true);
        }
    }

    public void mnuRapidOrderActionPerformed() {
        if (!rapidOrderEntryPanel.isVisible()) {
            showRapidOrderPanel(true);
            populateRapidOrderPanel();
        } else {
            showRapidOrderPanel(false);
        }
    }


    public void loadRapidOrderFromDepth(String skey, double price, long qty, int side) {
        //   if (!rapidOrderEntryPanel.isVisible()) {

        if (TradingShared.isReadyForTrading()) {
            showRapidOrderPanel(true);
            Stock stock = DataStore.getSharedInstance().getStockObject(skey);
            if (stock != null) {
                rapidOrderEntryPanel.clear();
                rapidOrderEntryPanel.populateOrder(stock);
                rapidOrderEntryPanel.populateFromDepth(price, qty, stock.getSymbol(), side);
                //   rapidOrderEntryPanel.focusQuantity();
            } else {
                rapidOrderEntryPanel.clear();
                rapidOrderEntryPanel.focusSymbol();
            }
            showRapidOrderPanel(true);
        }
        /*} else {
            showRapidOrderPanel(false);
        }*/
    }

    public void showRapidOrderPanel(boolean show) {
        rapidOrderEntryPanel.setVisible(show);
        oTopDesktop.updateUI();
    }

    public boolean isRapidOrderPanelVisible() {
        return rapidOrderEntryPanel.isVisible();
    }

    public void adjustTickerSettings() {
        g_oTicker.Ticker_ComponentResized();
    }

    /**
     * Returns an instance of the tree
     */
    public JTree getTree() {
        return oTree;
    }

    public void unsortSymbols() {
        try {
            if (mainboardSelected) {
                SortButtonRenderer oRend = null;
                try {
                    oRend = (SortButtonRenderer) g_oTempTable.getTable1().
                            getColumnModel().getColumn(g_oTempTable.getViewSettings()
                            .getSortColumn()).getHeaderRenderer();
                } catch (Exception e) {
                    // may be already unsorted
                    e.printStackTrace();
                }
                g_oTempTable.getViewSettings().setSortColumn(-1);
                g_oTempTable.getSymbols().unsortSymbols();
                try {
                    oRend.unsort();
                    oRend = null;
                } catch (Exception e) {
                    // if above try failed. this also fails
                    e.printStackTrace();
                }
                g_oTempTable.getTable().updateUI();
                g_oTempTable.getTable().getTableHeader().updateUI();
            } else if (mistViewSelected) {
                g_oMistTable.getViewSettings().getSymbols().unsortSymbols();
                g_oMistTable.getViewSettings().setSortColumn(-1);
                g_oMistTable.getTable1().updateUI();
            } else if (portfolioSelected) {
                g_oPortfolioTable.getTable().unsort();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addMouseListenerToTree() {
        MouseListener ml = new MouseAdapter() {
            public void mousePressed(MouseEvent e) {
                int selRow = oTree.getRowForLocation(e.getX(), e.getY());
                g_oSelectedPath = oTree.getPathForLocation(e.getX(), e.getY());
                if (selRow != -1) {
                    oTree.setSelectionPath(g_oSelectedPath);
                    valueChanged();

                    DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                            oTree.getLastSelectedPathComponent();

                    if (node == null) return;
                }
            }
        };
        oTree.addMouseListener(ml);
    }

    public void removePortfolioViews() {
        int iCount = g_oPortfolioTreeNode.getChildCount();

        for (int i = 0; i < iCount; i++) {
            DefaultMutableTreeNode oChild = (DefaultMutableTreeNode) g_oPortfolioTreeNode.getChildAt(i);
            PortfolioTable oChildWindow = (PortfolioTable) oChild.getUserObject();
            ViewSettingsManager.removeMainBoardSetting(oChildWindow.getViewSettings());
            oTopDesktop.remove(oChildWindow);
            oChildWindow.dispose();
            oChild = null;
        }
        g_oPortfolioTreeNode.removeAllChildren();
    }

    public void removeMistViews() {
        int iCount = g_oMISTTreeNode.getChildCount();

        for (int i = 0; i < iCount; i++) {
            DefaultMutableTreeNode oChild = (DefaultMutableTreeNode) g_oMISTTreeNode.getChildAt(i);
            MISTTable oChildWindow = (MISTTable) oChild.getUserObject();
            ViewSettingsManager.removeMainBoardSetting(oChildWindow.getViewSettings());
            oTopDesktop.remove(oChildWindow);
            oChildWindow.dispose();
            oChild = null;
        }
        g_oMISTTreeNode.removeAllChildren();
    }

    /**
     * Create a new custom table view on the board
     */
    public void createNewView(boolean manual, WatchListStore store) {
        String sNewName;
        int watchlistType = 0;
        if (manual) {
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Watchlist, "NewWatchlist");
            /* Accept the new name for the view via user input*/
            CreateView oCreateView = new CreateView(window, Language.getString("CREATE_VIEW"), true, true);
            while (true) {
//                    int watchlistType = 0 ;
                sNewName = oCreateView.getNewName();
                watchlistType = oCreateView.getSelectedType();

                if (sNewName == null) {
                    return;
                } else if (isValidName(oCreateView.getTypedName(), ViewSettingsManager.CUSTOM_VIEW)) {
                    break;
                } else {
                    new ShowMessage(false, Language.getString("MSG_DUPLICATE_VIEW_NAME"), "E");
                }
            }
            store = WatchListManager.getInstance().createNewStore(sNewName, watchlistType);
//            store.setSelectedType(watchlistType);
            oCreateView.dispose();

        } else {
            /* Accept the new name from the param. This occurs when the
               view is being created from the workspace */
            watchlistType = store.getSelectedType();
            sNewName = store.getCaption();
        }

        oTable = new ClientTable();
        oTable.setWindowType(Meta.WT_WatchList);
        oTopDesktop.add(oTable);
        oTable.setResizable(true);
        oTable.setMaximizable(true);
        oTable.setClosable(true);
        oTable.setVisible(false);
        oTable.setIconifiable(true);
        oTable.setLinkWindowControl(true);
        oTable.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        //oTable.setDataStore(g_oDataStore);
        oTable.setSortable(true);
        oTable.setTypeDescription(Language.getString("MY_STOCKS"));

        ViewSetting oSettings;
        try {
            /* Read the default view in the view settings file*/
            oSettings = (ViewSetting) ViewSettingsManager.getView(0, "0").clone(); // get the default view
            if (oSettings == null)
                throw (new Exception("View not found"));

            oSettings.setID(store.getId());
            oSettings.temp = true;
            oSettings.setCaptions(sNewName);
            oSettings.setSymbols(store);
        } catch (Exception e) {
            ViewSetting refSetting = ViewSettingsManager.getView(ViewSettingsManager.MARKET_VIEW << 8, "6");
            if (refSetting == null) {
                new ShowMessage(true, Language.getString("MSG_CANNOT_CREATE"), "E");
                return;
            }
            oSettings = new ViewSetting();

            oSettings.setMainType(ViewSettingsManager.CUSTOM_VIEW);
            oSettings.setColumnHeadings(Language.getList("TABLE_COLUMNS"));
            oSettings.setID(store.getId());
            oSettings.setColumns(refSetting.getColumns());
            oSettings.setFixedColumns(refSetting.getFixedColumns()); // symbols
            oSettings.setHiddenColumns(refSetting.getHiddenColumns()); // n of bids, no of asks
            oSettings.setRenderingIDs(ViewSettingsManager.getRenderingIDs(ViewSettingsManager.CUSTOM_VIEW));
            oSettings.setSymbols(new Symbols(Constants.NULL_STRING, true));
            oSettings.setCaptions(sNewName);
            oSettings.setSize(new Dimension(300, 200));
        }
        oTable.setSize(oSettings.getSize());
        store.setFilter(new WatchlistFilter());

        oSettings.setSubType((byte) 1);

        //GUISettings.setColumnSettings(oSettings, TWColumnSettings.getItem("MAIN_BOARD_COLS"));

        oTable.setViewSettings(oSettings);
        oTable.setSymbols(oSettings.getSymbols());

        DefaultMutableTreeNode oNode = new DefaultMutableTreeNode(oTable);
        g_oMyStocksTreeNode.add(oNode);
        DefaultTreeModel oTreeModel = (DefaultTreeModel) oTree.getModel();
        oTable.setTreePath(oTreeModel.getPathToRoot(oNode));

        oTable.createTable(true);//Language.getTableCols("TABLE_COLUMNS"));
        oTable.getTable().addMouseListener(g_oSymbolMouseListener);
        oTable.getScrollPane().getViewport().addMouseListener(g_oTableMouseListener);

        oTopDesktop.getDesktopManager().maximizeFrame(oTable);
        oTable.getTable().getTableHeader().updateUI();

//        if (manual){
        WorkspaceManager oWSM = new WorkspaceManager(this, getViewSettingsManager());
        oWSM.applyInitialWSPForMyStocks(oSettings);
//        }
        try {
            oSettings.setColumns(ColumnTemplateStore.getSharedInstance().getColumnTemplate(watchlistType).getDefaultColumns());
            if (watchlistType == Constants.WATCHLIST_TYPE_FOREX_VIEW_) {
                setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        oTable.updateClientTable();
        oTable.adjustColumns();
        oTable.applySettings();

        oTree.updateUI();
        ViewSettingsManager.addMainBoardSetting(oSettings);
        WatchListManager.getInstance().fireWatchlistAdded(store.getId());

        //oTable.setLocation(-100, -100); // invalidate the location to indicate this is a just created window
        if (manual) {
            oTable.showWindow();
            oTable.setLocation(oSettings.getLocation());
//            oTable.setLocationRelativeTo(oTopDesktop);
        } else {

            oTable.setVisible(false);
        }
        oSettings = null;
        oTable.updateGUI();
        oTable.applyTheme();
        oTable = null;
    }

    public void createNewForexView(boolean manual, WatchListStore store) {
        String sNewName;
        if (manual) {
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Watchlist, "NewForexView");
            /* Accept the new name for the view via user input*/
            CreateView oCreateView = new CreateView(window, Language.getString("CREATE_VIEW"), true, false);
            while (true) {
                sNewName = oCreateView.getNewName();
                if (sNewName == null)
                    return;
                else if (isValidFXWatchName(oCreateView.getTypedName(), ViewSettingsManager.FOREX_VIEW)) {
                    String s = oCreateView.getTypedName();
                    break;
                } else {
                    oCreateView.clearCaptions();
                    new ShowMessage(false, Language.getString("MSG_DUPLICATE_VIEW_NAME"), "E");
                }
            }
            store = WatchListManager.getInstance().createNewStore(WatchListStore.FOREX_TYPE_TYPE, sNewName);
            oCreateView.dispose();

        } else {
            /* Accept the new name from the param. This occurs when the
               view is being created from the workspace */
            sNewName = store.getCaption();
        }
        ViewSetting oSettings = null;
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings = (ViewSetting) ViewSettingsManager.getForexView("8").clone();
            if (oSettings == null) {
                throw (new Exception("View not found"));
            }
            oSettings.setCaptions(sNewName);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        ForexBoard fxb = new ForexBoard();
        fxb.setViewSetting(oSettings);
        fxb.setWatchListID(store.getId());
        fxb.setTitle(sNewName);
        fxb.setVisible(false);
        oSettings.setID(store.getId());
        oSettings.setCaptions(sNewName);
        oSettings.setMainType(ViewSettingsManager.FOREX_VIEW);
        oSettings.setParent(fxb);
        this.getDesktop().add(fxb);
        fxb.getSymbolsFromWatchList(store);
        fxb.applySettings();
        DefaultMutableTreeNode oNode = new DefaultMutableTreeNode(fxb);
        g_oForexTreeNode.add(oNode);
        try {
            if (manual) {
                WorkspaceManager oWSM = new WorkspaceManager(this, getViewSettingsManager());
                oWSM.applyInitialWSPForForex(oSettings);
                fxb.setVisible(true);
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        ViewSettingsManager.addForexSetting(oSettings);
        g_oViewSettings.addSetting(oSettings);
        oTree.updateUI();

    }
//    public void createForexBoard(){
//       ForexBoard fxb = new ForexBoard();//ForexBoard.getSharedInstance();
//       this.getDesktop().add(fxb);
//       fxb.setVisible(true);
//    }

    public void createNewFormula() {
        FormularEditor.getSharedInstance().setType(FormularEditor.NEW_FORMULA);
        FormularEditor.getSharedInstance().clearField();
        FormularEditor.getSharedInstance().showWindow();

        try {
            if (FormularEditor.getSharedInstance().isVisible()) {
                if (FormularEditor.getSharedInstance().isIcon()) {
                    FormularEditor.getSharedInstance().setIcon(false);
                }
            }
            FormularEditor.getSharedInstance().setSelected(true);
        } catch (PropertyVetoException e) {

        }
    }

    public void createNewMistView(boolean manual, WatchListStore store) {
        //Change start
        String sNewName;
        MISTTable frame;
        if (manual) {
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Watchlist, "NewClassicView");
            /* Accept the new name for the view via user input*/
            CreateView oCreateView = new CreateView(getFrame(),
                    Language.getString("CREATE_VIEW"), true, false);
            while (true) {
                sNewName = oCreateView.getNewName();
                if (sNewName == null) {
                    return;
                } else if (isValidName(oCreateView.getTypedName(), ViewSettingsManager.MIST_VIEW)) {
                    break;
                } else {
                    oCreateView.clearCaptions();
                    new ShowMessage(false, Language.getString("MSG_DUPLICATE_VIEW_NAME"), "E");
                }
            }
            store = WatchListManager.getInstance().createNewStore(WatchListStore.MIST_LIST_TABLE_TYPE, sNewName);
            oCreateView.dispose();
        } else {
            /* Accept the new name from the param. This occurs when the
               view is being created from the workspace */
            sNewName = store.getCaption();
        }

        ViewSetting oSettings;
        try {
            /* Read the default view in the view settings file */
            oSettings = (ViewSetting) g_oViewSettings.getMISTView(1536, "1").clone(); // get the default view
            if (oSettings == null) {
                throw (new Exception("MIST View not found"));
            }
            oSettings.setID(store.getId());
            oSettings.setCaptions(sNewName);
            oSettings.setSymbols(store);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        frame = new MISTTable(oSettings);
        frame.createTable();
        oTopDesktop.add(frame);
        frame.setResizable(true);
        frame.setMaximizable(true);
        frame.setClosable(true);
        frame.setVisible(false);
        frame.setIconifiable(true);
        frame.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
        frame.setSize(500, 300);


        oSettings.setID(store.getId());
        store.setFilter(new WatchlistFilter());
        MISTModel model = new MISTModel();
        model.setViewSettings(oSettings);
        Table table = new Table(); // table with no popup support inherited

        oSettings.setParent(frame);
        table.setGroupableMISTModel(model);
        com.isi.csvr.mist.MISTRenderer render = new com.isi.csvr.mist.MISTRenderer(oSettings.getColumnHeadings(), model.getRendIDs());
        render.setModel(model);
        model.setMISTTable(table, render);
        render = null;
//        model.setMISTTable(table);

        DefaultMutableTreeNode oNode = new DefaultMutableTreeNode(frame);
        g_oMISTTreeNode.add(oNode);

        frame.setTable(table);
        oTopDesktop.getDesktopManager().maximizeFrame(frame);
        frame.applySettings();

        TableColumnModel cm = table.getTable().getColumnModel();
//        ColumnGroup g_company = new ColumnGroup(Language.getString("DESCRIPTION"));
//        g_company.add(cm.getColumn(0));
//        g_company.add(cm.getColumn(1));
        ColumnGroup g_trade = new ColumnGroup(Language.getString("TRADE"));
        g_trade.add(cm.getColumn(2));
        g_trade.add(cm.getColumn(3));
        g_trade.add(cm.getColumn(4));
        ColumnGroup g_ask = new ColumnGroup(Language.getString("OFFER"));
        g_ask.add(cm.getColumn(5));
        g_ask.add(cm.getColumn(6));
        g_ask.add(cm.getColumn(7));
        ColumnGroup g_bid = new ColumnGroup(Language.getString("BID"));
        g_bid.add(cm.getColumn(8));
        g_bid.add(cm.getColumn(9));
        g_bid.add(cm.getColumn(10));

        GroupableTableHeader header = (GroupableTableHeader) (table.getTable().getTableHeader());
//        header.addGroup(g_company);
        header.addGroup(g_trade);
        header.addGroup(g_ask);
        header.addGroup(g_bid);

        table.createGroupableTable();
        table.getTable().addMouseListener(g_oSymbolMouseListener);

        GUISettings.applyOrientation(frame);

        frame.getTable1().addMouseListener(g_oTableMouseListener);
        frame.getTable().getPopup().setTMTavailable(false);

//        if (manual){
//            WorkspaceManager oWSM = new WorkspaceManager(this, getViewSettingsManager());
//            oWSM.applyInitialWSPForMyStocks(oSettings);
//        }
        //frame.setVisible(true);
        try {
            if (manual) {
                WorkspaceManager oWSM = new WorkspaceManager(this, getViewSettingsManager());
                oWSM.applyInitialWSPForMyStocks(oSettings);
                frame.showWindow();
//                frame.setVisible(true);
                ((SmartTable) g_oMistTable.getTable().getTable()).adjustColumnWidthsToFitinMIST(10);
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        ViewSettingsManager.addMainBoardSetting(oSettings);
        table.updateGUI();
        oTree.updateUI();
        g_oViewSettings.addSetting(oSettings);
        frame = null;
        model = null;
        table = null;
//        System.out.println("-----------4");
        //Change end
    }

    /**
     * Create a new custom table view on the board
     */
    public void createNewFilteredView(boolean manual, FunctionListStore store) {
        String sNewName;
        String function = null;

        if (manual) {

            if ((ExchangeStore.getSharedInstance().defaultCount() == 0)) {
                SharedMethods.showMessage(Language.getString("MSG_FULL_MARKETS_ONLY"), JOptionPane.ERROR_MESSAGE);
                return;
            }

            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Watchlist, "NewConditionalView");
            /* Accept the new name for the view via user input*/
//            functionBuilder.setLocationRelativeTo(getInstance().getFrame());
            functionBuilder.setFunction(null);
            functionBuilder.setName("");
            while (true) {
                functionBuilder.show();
                sNewName = functionBuilder.getName();
                if (sNewName == null)
                    return;
                else if (isValidName(Language.getLanguageSpecificString(functionBuilder.getName(), ","), ViewSettingsManager.FILTERED_VIEW)) {
                    break;
                } else {
                    new ShowMessage(false, Language.getString("MSG_DUPLICATE_VIEW_NAME"), "E");
                }
            }
            function = functionBuilder.getFunction();

            store = FunctionListManager.getInstance().createNewStore(sNewName);
//            oCreateView.dispose();
        } else {
            /* Accept the new name from the param. This occurs when the
               view is being created from the workspace */
            //sNewName = sCaptions;
            sNewName = store.getCaptions();
        }

        oTable = new ClientTable();
        oTable.setWindowType(Meta.WT_FilteredWatchList);
        oTopDesktop.add(oTable);
        oTable.setResizable(true);
        oTable.setMaximizable(true);
        oTable.setClosable(true);
        oTable.setVisible(false);
        oTable.setIconifiable(true);
        oTable.setLinkWindowControl(true);
        oTable.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        //oTable.setDataStore(g_oDataStore);
        oTable.setSortable(true);
        oTable.setTypeDescription(Language.getString("FUNCTION_WATCHLIST"));
        store.setParent(oTable);

        ViewSetting oSettings;
        try {
            /* Read the default view in the view settings file*/
            oSettings = (ViewSetting) ViewSettingsManager.getView(ViewSettingsManager.FILTERED_VIEW << 8, "0").clone(); // get the default view
            if (oSettings == null)
                throw (new Exception("View not found"));

            oSettings.setID(store.getId());

            oSettings.setCaptions(sNewName);
            oSettings.setSymbols(store);
        } catch (Exception e) {
            ViewSetting refSetting = ViewSettingsManager.getView(ViewSettingsManager.MARKET_VIEW << 8, "6");
            if (refSetting == null) {
                new ShowMessage(true, Language.getString("MSG_CANNOT_CREATE"), "E");
                return;
            }
            oSettings = new ViewSetting();

            oSettings.setMainType(ViewSettingsManager.FILTERED_VIEW);
            oSettings.setColumnHeadings(Language.getList("TABLE_COLUMNS"));
            oSettings.setID(store.getId());
            oSettings.setColumns(refSetting.getColumns());
            oSettings.setFixedColumns(refSetting.getFixedColumns()); // symbols
            oSettings.setHiddenColumns(refSetting.getHiddenColumns()); // n of bids, no of asks
            oSettings.setRenderingIDs(ViewSettingsManager.getRenderingIDs(ViewSettingsManager.FILTERED_VIEW));
            oSettings.setSymbols(new Symbols(Constants.NULL_STRING, true));
            oSettings.setCaptions(sNewName);
            oSettings.setSize(new Dimension(300, 200));
        }
        oTable.setSize(oSettings.getSize());

        oSettings.setSubType((byte) 1);

        GUISettings.setColumnSettings(oSettings, TWColumnSettings.getItem("MAIN_BOARD_COLS"));
        oTable.setViewSettings(oSettings);
        oTable.setSymbols(oSettings.getSymbols());

        DefaultMutableTreeNode oNode = new DefaultMutableTreeNode(oTable);
        g_oFunctionTreeNode.add(oNode);
        DefaultTreeModel oTreeModel = (DefaultTreeModel) oTree.getModel();
        oTable.setTreePath((Object[]) oTreeModel.getPathToRoot(oNode));

        oTable.createTable(true);//Language.getTableCols("TABLE_COLUMNS"));
        oTable.getTable().addMouseListener(g_oSymbolMouseListener);
        oTable.getScrollPane().getViewport().addMouseListener(g_oTableMouseListener);

        oTopDesktop.getDesktopManager().maximizeFrame(oTable);

//        if (manual){
        WorkspaceManager oWSM = new WorkspaceManager(this, getViewSettingsManager());
        oWSM.applyInitialWSPForMyStocks(oSettings);
//        }
        oTable.updateClientTable();
        oTable.getTable().getTableHeader().updateUI();
        oTable.adjustColumns();
        oTable.applySettings();
//        GUISettings.applyOrientation(oTable.g);

        if (manual) {
            ((FunctionListStore) oTable.getSymbols()).setFunction(function);
        }
        oTree.updateUI();
        ViewSettingsManager.addMainBoardSetting(oSettings);

        //oTable.setLocation(-100, -100); // invalidate the location to indicate this is a just created window
        if (manual) {
            oTable.showWindow();
            oTable.setLocation(oSettings.getLocation());
//            oTable.setLocationRelativeTo(oTopDesktop);
        }
        oSettings = null;
        oTable.updateGUI();
        oTable.applyTheme();
        oTable = null;

    }

    public void removeFuntionTreeNode() {
        g_oFunctionTreeNode.removeFromParent();
        oTree.updateUI();
    }

    public void removeSectorTreeNode() {
        g_oSectorsTreeNode.removeFromParent();
        oTree.updateUI();
    }

    public void removeMarketTreeNode() {
        g_oMarketWatchTreeNode.removeFromParent();
        oTree.updateUI();
    }


    /**
     * Create a new custom table view on the board
     */
    public void createNewPortfolio(String sCaptions, String sID, PortfolioStore store) {
        String sNewName;
        PortfolioTable oTable;

        if (sCaptions == null) {
            if (!ExchangeStore.isValidSystemFinformationType(Meta.IT_Portfolio)) return;
            /* Accept the new name for the view via user input*/
            CreateView oCreateView = new CreateView(window, Language.getString("CREATE_VIEW"), true, false);
            while (true) {
                sNewName = oCreateView.getNewName();
                if (sNewName == null)
                    return;
                else if (isValidName(oCreateView.getTypedName(), ViewSettingsManager.PORTFOLIO_VIEW)) {
                    break;
                } else {
                    new ShowMessage(false, Language.getString("MSG_DUPLICATE_VIEW_NAME"), "E");
                }
            }
            oCreateView.dispose();
        } else {
            /* Accept the new name from the param. This occurs when the
               view is being created from the workspace */
            sNewName = sCaptions;
        }

        oTable = new PortfolioTable();
        oTable.createTable();
        oTable.addInternalFrameListener(oTable);
        oTable.setLayer(GUISettings.TOP_LAYER);
        oTopDesktop.add(oTable);
        oTable.setResizable(true);
        oTable.setMaximizable(true);
        oTable.setClosable(true);
        oTable.setVisible(false);
        oTable.setIconifiable(true);
        oTable.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oTable.setSize(500, 300);


        ViewSetting oSettings;
        try {
            /* Read the default view in the view settings file*/
            oSettings = (ViewSetting) g_oViewSettings.getPortfolioView(1280, "1").clone(); // get the default view
            if (oSettings == null)
                throw (new Exception("View not found"));
            if (sID == null)
                oSettings.setID(Constants.NULL_STRING + System.currentTimeMillis());
            else
                oSettings.setID(sID);

            oSettings.setCaptions(sNewName);
            oSettings.setSymbols(new Symbols(Constants.NULL_STRING, true));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        String sColOrder = Settings.getItem("PORTFOLIO_COL_ORDER");
        try {
            setColumnSettings(oSettings, sColOrder);
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (sID == null)
            oSettings.setID(Constants.NULL_STRING + System.currentTimeMillis());
        else
            oSettings.setID(sID);

        if (store == null) {
            store = new PortfolioStore();
        }
        PortfolioModel model = new PortfolioModel(store, oTable);
        model.setViewSettings(oSettings);
        Table table = new Table(); // table with no popup support inherited


        oSettings.setParent(oTable);
        oTable.setDataStore(store);
        table.setModel(model);
        model.setPortfolioTable(table);

        oSettings.setSupportingObject(store);

        DefaultMutableTreeNode oNode = new DefaultMutableTreeNode(oTable);
        g_oPortfolioTreeNode.add(oNode);

        oTable.setTable(table);
        table.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() > 1) {
                    mnuDetailQuoteSymbol(null);
                }
            }
        });
        table.getTable().addMouseListener(g_oSymbolMouseListener);
        oTopDesktop.getDesktopManager().maximizeFrame(oTable);
        oTable.applySettings();
        table.getModel().updateGUI();
        GUISettings.applyOrientation(oTable);
        oTable.show();
        oTable.updateUI();
        oTree.updateUI();
        ViewSettingsManager.addMainBoardSetting(oSettings);

        oTable = null;
        model = null;
        table = null;
    }

// --Commented out by Inspection START (3/29/05 2:31 PM):
//    public int getCustomViewCount() {
//        return g_oMyStocksTreeNode.getChildCount();
//    }
// --Commented out by Inspection STOP (3/29/05 2:31 PM)

    public void setShowHideButtons(boolean status) {
        g_oMenus.setShowHideButtons(status);
    }

    /**
     * Creates a new custom table view on the board
     */
    public void editView() {
        String sNewName;
        int type = ViewSettingsManager.CUSTOM_VIEW;
        /* Accept the new name for the view */
        g_oTablePopup.setVisible(false);

        CreateView oCreateView = new CreateView(window, Language.getString("EDIT_VIEW"), true, false);
        if (mainboardSelected) {
            if (g_oTempTable.isFunctionType()) {
                type = ViewSettingsManager.FILTERED_VIEW;
                oCreateView.setCaptions(((FunctionListStore) g_oTempTable.getViewSettings().getSymbols()).getCaptions());
            } else {
                type = ViewSettingsManager.CUSTOM_VIEW;
                oCreateView.setCaptions(((WatchListStore) g_oTempTable.getViewSettings().getSymbols()).getCaptions());
            }

        } else if (portfolioSelected) {
            type = ViewSettingsManager.PORTFOLIO_VIEW;
            oCreateView.setCaptions(g_oPortfolioTable.getTable().getModel().getViewSettings().getCaptions());
        } else if (mistViewSelected) {
            type = ViewSettingsManager.MIST_VIEW;
            oCreateView.setCaptions(g_oMistTable.getModel1().getViewSettings().getCaptions());
        } else if (forexViewSelected) {
            type = ViewSettingsManager.FOREX_VIEW;
            oCreateView.setCaptions(g_oFXBoard.getViewSetting().getCaptions());
        }
        while (true) {
            sNewName = oCreateView.getNewName();
            if (sNewName == null)
                return;
            else if (isValidName(oCreateView.getTypedName(), type))
                break;
            else
                new ShowMessage(false, Language.getString("MSG_DUPLICATE_VIEW_NAME"), "E");
        }
        if (mainboardSelected) {
            g_oTempTable.getViewSettings().setCaptions(sNewName);
            if (g_oTempTable.isFunctionType()) {
                ((FunctionListStore) g_oTempTable.getViewSettings().getSymbols()).setCaptions(sNewName);
            } else {
                ((WatchListStore) g_oTempTable.getViewSettings().getSymbols()).setCaptions(sNewName);
            }
            g_oTempTable.updateGUI();
            g_oTempTable.repaint();
        } else if (portfolioSelected) {
            g_oPortfolioTable.getTable().getModel().getViewSettings().setCaptions(sNewName);
            g_oPortfolioTable.updateGUI();
        } else if (forexViewSelected) {
            g_oFXBoard.getViewSetting().setCaptions(sNewName);
            ((WatchListStore) WatchListManager.getInstance().getStore(g_oFXBoard.getViewSetting().getID())).setCaptions(sNewName);
            g_oFXBoard.updateGUI();//updateGUI();
            oTree.updateUI();
        } else {
            g_oMistTable.getViewSettings().setCaptions(sNewName);
            ((WatchListStore) g_oMistTable.getViewSettings().getSymbols()).setCaptions(sNewName);
            g_oMistTable.updateGUI();
        }
        oCreateView.dispose();
        oCreateView = null;
        oTree.updateUI();

    }


    public boolean isValidName(String sNewName, int type) {
        Vector oNames = ViewSettingsManager.getMainBoardViews();
        String sCaption;
        for (int i = 0; i < oNames.size(); i++) {
            ViewSetting oSetting = (ViewSetting) oNames.elementAt(i);
            if (oSetting.getMainType() != type)
                continue;
            sCaption = Language.getLanguageSpecificString(oSetting.getCaption(), ",");
            if (sCaption.trim().equals(sNewName.trim()))
                return false;
        }
        return true;
    }

    public boolean isValidFXWatchName(String sNewName, int type) {
        String sCaption;
        Hashtable fxhaHashtable = ViewSettingsManager.getForexViews();
        Enumeration fxEnum = fxhaHashtable.elements();
        while (fxEnum.hasMoreElements()) {
            ViewSetting oSetting = (ViewSetting) fxEnum.nextElement();
            if (oSetting.getMainType() != type)
                continue;
            sCaption = Language.getLanguageSpecificString(oSetting.getCaptions(), ",");
            if (sCaption.trim().equals(sNewName.trim()))
                return false;
        }
        return true;
    }

    public void showProChart() { //This should be renamed as showProChart - coz.. this shows charts inside Chart Frame -Sathyajith 2009 - 01 - 16
        if (chartFrame == null) {
            createChartFrame();
        }
        if (chartFrame.getLocation().getX() == -1000) {
            GUISettings.setLocationRelativeTo(chartFrame, oTopDesktop); // center the chart frame
        }

        try {
            if (!chartFrame.isVisible() && !chartFrame.isDetached()) {
                if (chartFrame.isIcon()) {
                    chartFrame.setIcon(false);
                }
                int result = JOptionPane.OK_OPTION;
                int numberOfGraphsToBeDeleted = 0;
                if (chartFrame.getTotalChartCount() + chartFrame.getProChartCount() > chartFrame.getMaxChartCount() && !chartFrame.isDetached()) {
                    String message = "Maximum number of Graphs allowed to be open at a given time has exceeded." +
                            "\nPlease close open Graphs, or Graphs open in ProChart will be closed.\nYou have %s Graphs opened in ProChart.";
                    message = String.format(message, chartFrame.getProChartCount());
                    result = JOptionPane.showConfirmDialog(this.getDesktop(), message, "Error", JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);
                    numberOfGraphsToBeDeleted = chartFrame.getTotalChartCount() + chartFrame.getProChartCount() - chartFrame.getMaxChartCount();
                }
                if (result == JOptionPane.OK_OPTION) {
                    //close the necessary graphs first from ProChart
                    JInternalFrame[] frames = chartFrame.getDesktop().getAllFrames();
                    for (int i = 0; i < numberOfGraphsToBeDeleted; i++) {
                        if (frames[i] instanceof GraphFrame) {
                            ((GraphFrame) frames[i]).closeWindow();
                        }
                    }
                    chartFrame.show();
                    chartFrame.setTotalChartCount(chartFrame.getProChartCount() + chartFrame.getTotalChartCount() + numberOfGraphsToBeDeleted);
                    chartFrame.setSelected(true);
                    SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Chart, "ProChart");
                }
            } else {
                if (chartFrame.isIcon()) {
                    chartFrame.setIcon(false);
                }
                chartFrame.setSelected(true);
            }
            //System.out.println(chartFrame.getSize());
        } catch (PropertyVetoException ex) {
            ex.printStackTrace();
        }
    }

    public ChartFrame getChartFrame() {
        if (chartFrame == null) {
            createChartFrame();
        }
        return chartFrame;
    }

    public void show_indexGraph(Table table) {
        try {
            int row = table.getTable().getSelectedRow();
            if (row >= 0) {
                showChart(SharedMethods.getKey((String) table.getTable().getModel().getValueAt(row, -1),
                        (String) table.getTable().getModel().getValueAt(row, 0), Meta.INSTRUMENT_INDEX), null, null);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Column settins selected for the table
     */
    public void g_mnuTreePopSettings_Selected() {
        Customizer c = null;
        ViewSetting settings = null;

        if (mainboardSelected) {
            g_oTempTable.saveColumnPositions();
            settings = g_oTempTable.getViewSettings();
            ////////////////////////////////////////////////////////////////////////////////////////////

            ////////////////////////////////////////////////////////////////////////////////////////////
            c = new Customizer(g_oTempTable, settings);

        } else if (portfolioSelected) {
            g_oPortfolioTable.getTable().getModel().saveColumnPositions();
            c = new Customizer(g_oPortfolioTable.getTable());
        } else if (mistViewSelected) {
            g_oMistTable.getTable().getModel().saveColumnPositions();
            settings = g_oMistTable.getViewSettings();
            c = new Customizer(g_oMistTable.getTable());
        } else if (volumeWatcherSelected) {
            g_oTempTable.saveColumnPositions();
            settings = g_oTempTable.getViewSettings();
            c = new Customizer(g_oTempTable, settings);
        } else if (cashFlowWatcherSelected) {
            g_oTempTable.saveColumnPositions();
            settings = g_oTempTable.getViewSettings();
            c = new Customizer(g_oTempTable, settings);
        }

        c.showDialog();
        settings = null;
    }

    public void showCustomizer() {
        Font oFont = null;

        if (mainboardSelected) {
            int bodyGap = ((TableThemeSettings) g_oTempTable.getTable()).getBodyGap();

            TableCuztomizerUI.getSharedInstance().show((TableThemeSettings) g_oTempTable.getTable());
            if (oFont != TableCuztomizerUI.getSharedInstance().getBodyFont()) {
                g_oTempTable.getTable().setFont(TableCuztomizerUI.getSharedInstance().getBodyFont());
                g_oTempTable.getViewSettings().setFont(TableCuztomizerUI.getSharedInstance().getBodyFont());
                try {
                    g_oTempTable.getTable().setFont(g_oTempTable.getViewSettings().getFont());
                    FontMetrics oMetrices = g_oTempTable.getTable().getGraphics().getFontMetrics();
                    g_oTempTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + TableCuztomizerUI.getSharedInstance().getBodyGap());
                } catch (Exception e) {
                }
            } else if (bodyGap != TableCuztomizerUI.getSharedInstance().getBodyGap()) {
                try {
                    FontMetrics oMetrices = g_oTempTable.getTable().getGraphics().getFontMetrics();
                    g_oTempTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + TableCuztomizerUI.getSharedInstance().getBodyGap());
                } catch (Exception e) {
                }
            }

            Object oRend;
            if (oFont != TableCuztomizerUI.getSharedInstance().getHeadingFont()) {
                g_oTempTable.getViewSettings().setHeaderFont(TableCuztomizerUI.getSharedInstance().getHeadingFont());
                oRend = g_oTempTable.getTable().getColumnModel().getColumn(0).getHeaderRenderer();
                if (oRend instanceof SortButtonRenderer) {
                    ((SortButtonRenderer) oRend).setHeaderFont(TableCuztomizerUI.getSharedInstance().getHeadingFont());
                    ((SortButtonRenderer) oRend).updateUI();
                }

                g_oTempTable.getTable().getTableHeader().updateUI();
                g_oTempTable.getTable().updateUI();
            }
            g_oTempTable.getTable().updateUI();
            g_oTempTable.updateUI();
        } else if (portfolioSelected) {
            //oFont = oFontChooser.showDialog(Language.getString("SELECT_FONT"), g_oPortfolioTable.getTable().getTable().getFont());
            if (oFont != null) {
                g_oPortfolioTable.getTable().getTable().setFont(oFont);
                g_oPortfolioTable.getTable().getModel().getViewSettings().setFont(oFont);
                try {
                    g_oPortfolioTable.getTable().setFont(g_oPortfolioTable.getViewSettings().getFont());
                    FontMetrics oMetrices = g_oPortfolioTable.getTable().getGraphics().getFontMetrics();
                    g_oPortfolioTable.getTable().getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + 2);
                } catch (Exception e) {
                }
            }
            FontMetrics oMetrices = g_oPortfolioTable.getTable().getGraphics().getFontMetrics();
            g_oPortfolioTable.getTable().getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + g_oPortfolioTable.getTable().getSmartTable().getBodyGap());
        } else if (mistViewSelected) {
            int bodyGap = g_oMistTable.getTable().getSmartTable().getBodyGap();
            MISTTableCustomizerDialog.getSharedInstance().show(g_oMistTable.getTable());
            if (oFont != MISTTableCustomizerDialog.getSharedInstance().getBodyFont()) {
                g_oMistTable.getTable().setFont(MISTTableCustomizerDialog.getSharedInstance().
                        getBodyFont());
                g_oMistTable.getViewSettings().setFont(MISTTableCustomizerDialog.getSharedInstance().
                        getBodyFont());
                try {
                    g_oMistTable.getTable().setFont(g_oMistTable.getViewSettings().getFont());
                    FontMetrics oMetrices = g_oMistTable.getTable().getGraphics().getFontMetrics();
                    g_oMistTable.getTable().getTable().setRowHeight(oMetrices.getMaxAscent() +
                            oMetrices.getMaxDescent() + MISTTableCustomizerDialog.getSharedInstance().getBodyGap());
                } catch (Exception e) {
                }
            } else if (bodyGap != MISTTableCustomizerDialog.getSharedInstance().getBodyGap()) {
                try {
                    FontMetrics oMetrices = g_oTempTable.getTable().getGraphics().getFontMetrics();
                    g_oTempTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + MISTTableCustomizerDialog.getSharedInstance().getBodyGap());
                } catch (Exception e) {
                }
            }
        } else if (volumeWatcherSelected || cashFlowWatcherSelected) {

            int bodyGap = ((TableThemeSettings) g_oTempTable.getTable()).getBodyGap();
            TableCuztomizerUI.getSharedInstance().show((TableThemeSettings) g_oTempTable.getTable());
            if (oFont != TableCuztomizerUI.getSharedInstance().getBodyFont()) {
                g_oTempTable.getTable().setFont(TableCuztomizerUI.getSharedInstance().getBodyFont());
                g_oTempTable.getViewSettings().setFont(TableCuztomizerUI.getSharedInstance().getBodyFont());
                try {
                    g_oTempTable.getTable().setFont(g_oTempTable.getViewSettings().getFont());
                    FontMetrics oMetrices = g_oTempTable.getTable().getGraphics().getFontMetrics();
                    g_oTempTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + TableCuztomizerUI.getSharedInstance().getBodyGap());
                } catch (Exception e) {
                }
            } else if (bodyGap != TableCuztomizerUI.getSharedInstance().getBodyGap()) {
                try {
                    FontMetrics oMetrices = g_oTempTable.getTable().getGraphics().getFontMetrics();
                    g_oTempTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + TableCuztomizerUI.getSharedInstance().getBodyGap());
                } catch (Exception e) {
                }
            }

            Object oRend;
            if (oFont != TableCuztomizerUI.getSharedInstance().getHeadingFont()) {
                g_oTempTable.getViewSettings().setHeaderFont(TableCuztomizerUI.getSharedInstance().getHeadingFont());
                oRend = g_oTempTable.getTable().getColumnModel().getColumn(0).getHeaderRenderer();
                if (oRend instanceof SortButtonRenderer) {
                    ((SortButtonRenderer) oRend).setHeaderFont(TableCuztomizerUI.getSharedInstance().getHeadingFont());
                    ((SortButtonRenderer) oRend).updateUI();
                }

                g_oTempTable.getTable().getTableHeader().updateUI();
                g_oTempTable.getTable().updateUI();
            }
            g_oTempTable.getTable().updateUI();
            g_oTempTable.updateUI();

            //TableCustomizerDialog.getSharedInstance().show((TableThemeSettings) g_oTempTable.getTable());


        }

    }

    /**
     * Delete the selectd node from the tree, including its table view
     */
    public void g_mnuTreePopDelete_Selected(ActionEvent e) {
        ShowMessage oMessage = new ShowMessage(Language.getString("MSG_DELETE_VIEW"), "W", true);
        if (oMessage.getReturnValue() == 1) {

            DefaultMutableTreeNode oNode;
            if (mainboardSelected) {
                if (g_oTempTable.isFunctionType()) {
                    oNode = findNode(g_oFunctionTreeNode, g_oTempTable);
                } else {
                    oNode = findNode(g_oMyStocksTreeNode, g_oTempTable);
                }
            } else if (mistViewSelected) {
                oNode = findNode(g_oMISTTreeNode, g_oMistTable);
            } else if (forexViewSelected) {
                oNode = findNode(g_oForexTreeNode, g_oFXBoard);
            } else {
                oNode = findNode(g_oPortfolioTreeNode, g_oPortfolioTable);
            }
            if (oNode == null) return;
            DefaultTreeModel oModel = new DefaultTreeModel(g_oTreeTop);
            oModel.removeNodeFromParent(oNode);
            if (mainboardSelected) {
                if (g_oTempTable.isFunctionType()) {
                    ClientTable oTable = (ClientTable) oNode.getUserObject();
                    FunctionListManager.getInstance().removeStore((FunctionListStore) oTable.getSymbols());
                    ViewSettingsManager.removeMainBoardSetting(oTable.getViewSettings());
                    oTable.stopUpdating();
                    oTable.dispose();
                } else {
                    ClientTable oTable = (ClientTable) oNode.getUserObject();
                    WatchListManager.getInstance().removeStore((WatchListStore) oTable.getSymbols());
                    ViewSettingsManager.removeMainBoardSetting(oTable.getViewSettings());
                    oTable.stopUpdating();
                    oTopDesktop.remove(oTable);
                    oTable.dispose();
                }
            } else if (portfolioSelected) {
                PortfolioTable oTable = (PortfolioTable) oNode.getUserObject();
                ViewSettingsManager.removeMainBoardSetting(oTable.getTable().getModel().getViewSettings());
                JInternalFrame tns = oTable.getTimeAndSalesWindow();
                oTopDesktop.remove(tns);
                tns.dispose();
                oTopDesktop.remove(oTable);
                oTable.dispose();
                tns = null;
            } else if (mistViewSelected) {
                MISTTable oTable = (MISTTable) oNode.getUserObject();
                WatchListManager.getInstance().removeStore((WatchListStore) oTable.getViewSettings().getSymbols());
                ViewSettingsManager.removeMainBoardSetting(oTable.getViewSettings());
//                g_oViewSettings.removeSetting(oTable.getViewSettings());
                oTopDesktop.remove(oTable);
                oTable.dispose();
            } else if (forexViewSelected) {
//                                MISTTable oTable = (MISTTable) oNode.getUserObject();
                WatchListManager.getInstance().removeStore(((WatchListStore) WatchListManager.getInstance().getStore(g_oFXBoard.getViewSetting().getID())));
                ViewSettingsManager.removeForexWatchSettings(g_oFXBoard.getViewSetting());
//                g_oViewSettings.removeSetting(oTable.getViewSettings());
                oTopDesktop.remove(g_oFXBoard);
                g_oFXBoard.dispose();
            }
            SharedMethods.updateComponent(oTree);
            SharedMethods.updateComponent(oTopDesktop);
//            oTree.updateUI();
//            themeChanged(Settings.getLastThemeID());
//            oTopDesktop.refreshDesktop();
        }
    }

    private DefaultMutableTreeNode findNode(DefaultMutableTreeNode parent, Object table) {
        Enumeration children = parent.children();
        DefaultMutableTreeNode node = null;

        while (children.hasMoreElements()) {
            node = (DefaultMutableTreeNode) children.nextElement();
            if (node.getUserObject() == table) {
                break;
            }
            node = null;
        }
        children = null;
        return node;
    }

    public void updateTree() {
        oTree.updateUI();
    }

// --Commented out by Inspection START (3/29/05 2:28 PM):
//    /**
//     * Connect to the server
//     */
//    public void connectToServer() {
//
//
//    }
// --Commented out by Inspection STOP (3/29/05 2:28 PM)

    public void resetConnectionCounter() {
        if (connector instanceof TCPConnector) {
            System.out.println("in reset Connection Counter");
            connector.setIdle();
            ((TCPConnector) connector).resetConnectionTime();
        }
    }

    public void stopPriceConnection() {
        if (connector instanceof TCPConnector) {
            ((TCPConnector) connector).setStopped();
        }
    }

    /**
     * Disconnect from the stocknet server
     */
    public void disconnectFromServer() {
        connector.closeSocket();
    }

    public void disconnectFromTradingServer() {
        autoTradeLoginDaemon.closeSocket();
        autoTradeLoginDaemon.closeSecondarySocket();
    }

    public void showTicker() {
        if (!tickerFrame.isVisible()) {
            TradeFeeder.setVisible(true);
            try {
                tickerFrame.setSelected(true);
            } catch (PropertyVetoException ex) {
            }
            SharedSettings.Frame_Header_Changed_by = 1;
            tickerFrame.resizeFrame();
        }
    }


    /*public void mnu_ticker_ActionPerformed() {
        if (isTickerFixed() && SharedSettings.IS_CUSTOM_SELECTED) {
            ISTICKERFIXED = true;
            tickerFrame.setResizable(false);
            oTopDesktop.remove(tickerFrame);
            tickerPanel.add(tickerFrame);
            tickerPanel.updateUI();
            window.validate();
            setTickerFixed(true);
            floatMenu.setText(Language.getString("FLOAT_TICKER"));
            if (windowTabs.contains(tickerFrame)) {
                // Tab-if (TWDesktop.tabHashTable.containsKey(tickerFrame)) {
                TWDesktop.removeWindowtab(tickerFrame);
            }
        }
        if (tickerFrame.isVisible()) {
            tickerFrame.setVisible(false);
            TradeFeeder.setVisible(false);
        } else if (!tickerFrame.isVisible() && SharedSettings.IS_CUSTOM_SELECTED) {
            tickerFrame.show();
            TradeFeeder.setVisible(true);
            try {
                tickerFrame.setSelected(true);
            } catch (PropertyVetoException ex) {
            }
            tickerFrame.resizeFrame();
        } else {
            ISTICKERFIXED = true;
            tickerFrame.setResizable(false);
            tickerPanel.updateUI();
            window.validate();
            setTickerFixed(true);
            floatMenu.setText(Language.getString("FLOAT_TICKER"));
            if (windowTabs.contains(tickerFrame)) {
                // Tab-if (TWDesktop.tabHashTable.containsKey(tickerFrame)) {
                TWDesktop.removeWindowtab(tickerFrame);
            }
        }
        getDesktop().updateUI();
    }*/

    public void mnu_ticker_ActionPerformed() {
        if (SharedSettings.IS_CUSTOM_SELECTED) {
            tickerFrame.show();
            TradeFeeder.setVisible(true);
            try {
                tickerFrame.setSelected(true);
            } catch (PropertyVetoException ex) {
            }
            SharedSettings.Frame_Header_Changed_by = 1;
            tickerFrame.resizeFrame();

        } else if (SharedSettings.IS_ADVANCED_SELECTED) {

            tickerFrame.show();
            try {
                tickerFrame.setSelected(true);
            } catch (PropertyVetoException ex) {
            }

            SharedSettings.Frame_Header_Changed_by = 1;
            //HolderPanel.getInstance().getPreferredSize();
            PanelSetter.holderPanel.getPreferredSize();
            TickerFrame.setSizeOnFrame();
            SharedMethods.updateComponent(Ticker.getInstance());

            tickerFrame.resizeFrame();
            SharedMethods.updateComponent(PanelSetter.holderPanel);
            Client.getInstance().getDesktop().revalidate();
            TradeFeeder.setVisible(false);
        } else {
            tickerFrame.setVisible(false);
            TradeFeeder.setVisible(false);

        }
        getDesktop().updateUI();
        SharedMethods.updateComponent(tabAndStatusbar);
        SharedMethods.updateComponent(tabPanel);
    }


    public void mnu_ticker_ActionPerformed_by_Button() {
        if (tickerFrame.isVisible() && SharedSettings.IS_CUSTOM_SELECTED) {
            tickerFrame.setVisible(false);
            TradeFeeder.setVisible(false);
            SharedSettings.selectCustomTicker(false);
            SharedSettings.lastSelected = SharedSettings.tickerCust;
        } else if (tickerFrame.isVisible() && SharedSettings.IS_ADVANCED_SELECTED) {
            SharedSettings.sharedSettings_Off_by_button();
        } else {
            if (SharedSettings.IS_CUSTOM_SELECTED || (SharedSettings.lastSelected.equals(SharedSettings.tickerCust))) {
                SharedSettings.lastSelected = SharedSettings.tickerCust;
            } else {
                SharedSettings.lastSelected = SharedSettings.tickerAdv;
            }
            SharedSettings.sharedSettings_On_by_button();
        }
        SharedMethods.updateComponent(tabAndStatusbar);
        SharedMethods.updateComponent(tabPanel);
        getDesktop().updateUI();
    }

    public void mnu_download_ActionPerformed(boolean bFull, JComponent source) {
        /*if (!Settings.isConnected()) {
            new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            return;
        }*/
        if (!ChartInterface.isHistoryEnabled()) {
            new ShowMessage(Language.getString("MSG_NOT_SUBSCRIBED_FOR_HISTORY"), "E");
            return;
        }

        if (HistoryDownloader.POPULATION > 0) {
            SharedMethods.showNonBlockingMessage(Language.getString("MSG_HISTORY_DOWNLOAD_IN_PROGRESS")
                    , JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        String exchangeID;
        if (ExchangeStore.getSharedInstance().defaultCount() > 1) {
            ExchangeSelectorDialog selectorDialog = new ExchangeSelectorDialog(Language.getString("SELECT_HISTORY_DOWNLOAD_EXCHANGE"));
            exchangeID = selectorDialog.getExchange();
            selectorDialog = null;
        } else {
            exchangeID = ExchangeStore.getSharedInstance().getDefaultExchange().getSymbol();
        }

        if (exchangeID != null) {
            // check if the markt is open and give a warning msg
            boolean canProceed = false;
            if (ExchangeStore.getSharedInstance().getExchange(exchangeID).getMarketStatus() == Meta.MARKET_OPEN) {
                int result = SharedMethods.showConfirmMessage(Language.getString("HISTORY_DOWNLOAD_WARING"), JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                if (result == JOptionPane.YES_OPTION) {
                    canProceed = true;
                }
            } else {
                canProceed = true;
            }

            if (canProceed) {
                Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeID);
                exchange.setLastEODDate(0);
                HistoryArchiveRegister.getSharedInstance().resetExchange(exchangeID);
                HistoryDownloadManager.getSharedInstance().requestHistory(exchange);
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Tools, "DownloadHistory");
            }
        }
    }

    /*public void mnu_download_ActionPerformed(boolean bFull, JComponent source) {
        /*if (!Settings.isConnected()) {
            new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            return;
        }*
        if (!ChartInterface.isHistoryEnabled()) {
            new ShowMessage(Language.getString("MSG_NOT_SUBSCRIBED_FOR_HISTORY"), "E");
            return;
        }

        if (HistoryDownloader.POPULATION > 0) {
            SharedMethods.showNonBlockingMessage(Language.getString("MSG_HISTORY_DOWNLOAD_IN_PROGRESS")
                    , JOptionPane.INFORMATION_MESSAGE);
            return;
        }

        String exchangeID;
        if (ExchangeStore.getSharedInstance().defaultCount() > 1) {
            ExchangeSelectorDialog selectorDialog = new ExchangeSelectorDialog(Language.getString("SELECT_HISTORY_DOWNLOAD_EXCHANGE"));
            exchangeID = selectorDialog.getExchange();
            selectorDialog = null;
        } else {
            exchangeID = ExchangeStore.getSharedInstance().getDefaultExchange().getSymbol();
        }

        if (exchangeID != null) {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeID);
            exchange.setLastEODDate(0);
            HistoryArchiveRegister.getSharedInstance().resetExchange(exchangeID);
            HistoryDownloadManager.getSharedInstance().requestHistory(exchange);
        }
    }*/

    public void updateMetaStockDB() {

        String exchangeID;
        if (ExchangeStore.getSharedInstance().defaultCount() > 1) {
            ExchangeSelectorDialog selectorDialog = new ExchangeSelectorDialog(Language.getString("SELECT_METASTOCK_UPDATE_EXCHANGE"));
            exchangeID = selectorDialog.getExchange();
            selectorDialog = null;
        } else {
            exchangeID = ExchangeStore.getSharedInstance().getDefaultExchange().getSymbol();
        }

        final String selExch = exchangeID;

        if (selExch != null) {
            final MetaStockHistoryUpdator metaStock2HistoryUpdator = new MetaStockHistoryUpdator(MetaStockHistoryUpdator.HISTORY,
                    exchangeID, MetaStockHistoryUpdator.MANUAL, true);
            final WorkInProgressIndicator workIn = new WorkInProgressIndicator(WorkInProgressIndicator.UPDATING_METASTOCK);
            workIn.setVisible(true);
            new Thread("Metastock updator- Intraday - Manual") {
                public void run() {
                    metaStock2HistoryUpdator.update();
                    MetaStockManager.getInstance().updateIntrdayHistory(selExch);
                    workIn.dispose();
                }
            }.start();
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.MetaStock, "ManualUpdate");
        }

    }

    public void updateTradeStationDB() {

        String exchangeID;
        if (ExchangeStore.getSharedInstance().defaultCount() > 1) {
            ExchangeSelectorDialog selectorDialog = new ExchangeSelectorDialog(Language.getString("SELECT_TRADESTATION_UPDATE_EXCHANGE"));
            exchangeID = selectorDialog.getExchange();
            selectorDialog = null;
        } else {
            exchangeID = ExchangeStore.getSharedInstance().getDefaultExchange().getSymbol();
        }

        final String selExch = exchangeID;

        if (selExch != null) {
            final TradeStationHistoryUpdator tsHistoryUpdator = new TradeStationHistoryUpdator(MetaStockHistoryUpdator.HISTORY,
                    exchangeID, MetaStockHistoryUpdator.MANUAL, true);
            final WorkInProgressIndicator workIn = new WorkInProgressIndicator(WorkInProgressIndicator.UPDATING_TRADESTATION);
            workIn.setVisible(true);
            new Thread("Trade Station updator- Intraday - Manual") {
                public void run() {
                    tsHistoryUpdator.update();
                    TradeStationManager.getInstance().updateIntrdayHistory(selExch);
                    workIn.dispose();
                }
            }.start();
            //SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.MetaStock, "ManualUpdate");
        }

    }

    /**
     * Exit the application
     */
    public void systemExit_ActionPerformed() {
        exitApplication(isSaveCurrentView());
    }


    public void exitApplication(boolean save) {
        if (save) {
            final WorkInProgressIndicator workInProgressIndicator =
                    new WorkInProgressIndicator(WorkInProgressIndicator.EXITING);
            Thread thread = new Thread("Client-systemExit_ActionPerformed 1") {
                public void run() {
                    workInProgressIndicator.setVisible(true);

                }
            };
            thread.start();

            Thread thread2 = new Thread("Client-systemExit_ActionPerformed 2") {
                public void run() {
                    saveDataOnExit();
                    deleteTempFiles();
                    SystemTrayManager.unloadFromTray();
                    System.exit(0);
                }
            };
            thread2.start();
        }
    }

    public void saveDataOnExit() {
        try {
            Settings.setExitMode();
            DataStore.saveData();
            ValidatedSymbols.getSharedInstance().saveData();
            SymbolMaster.saveSymbolAlias();
            if (Settings.isSaveTimeNSales()) {
                TradeStore.saveData();
                Thread.yield();
            } else {
                TradeStore.clearPreviouslySaved();
                Thread.yield();
            }
            IPSettings.save();
            NetCalcStore.saveData();
            PFStore.saveTransactions(PFStore.transFileName);
            WatchListManager.getInstance().save();
            FunctionListManager.getInstance().save();
            AnnouncementStore.getSharedInstance().saveData();
            DefaultSettingsManager.getSharedInstance().saveSettings();
            if (getMarketTime() > 0) {
                HistorySettings.setLastConnectedDate(Constants.NULL_STRING + getMarketTime());
            }
            Settings.LAST_TIME_SAVE = true;
            Settings.save();
            HistorySettings.save();
            ddeServer.stopServer();
            OHLCStore.getInstance().saveData();
            OrderQueue.getSharedInstance().saveData();
            ProgramedOrderStore.getSharedInstance().saveData();
            ExchangeStore.getSharedInstance().save();
            ExchangeMap.saveData();
            TradePortfolios.getInstance().saveSettings();
            CustomFormulaStore.getSharedInstance().save();
            com.isi.csvr.datastore.CurrencyStore.getSharedInstance().save();
            //todo remove for KSE 26/10/06
            CustomIndexStore.getSharedInstance().save();
            MessageList.getSharedInstance().save();
            TradeSummaryStore.getSharedInstance().save();
            PluginStore.save();
            GlobalIndexStore.getSharedInstance().save();
            //forex Custom settings
            CustomSettingsStore.getSharedInstance().save();
            //indices frame settings
            IndicesFrame.getSharedInstance().save();
            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
                if (!OptimizedTreeRenderer.selectedSymbols.isEmpty()) {
                    MainUI.getSharedInstance().save_BandWidthOptimized_Symbols();
                }
                MainUI.saveSettings();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            File file = new File(Settings.getAbsolutepath() + "formula");
            SharedMethods.deleteFiles(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            File file = new File(Settings.getAbsolutepath() + "\\Temp\\TEMP_FILES\\");
            System.out.println("Deleting Temp Folder files");
            SharedMethods.deleteFiles(file);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void deleteTempFiles() {
        try {
            File tempDownloades = new File(Settings.getAbsolutepath() + "temp/downloads");
            File[] files = tempDownloades.listFiles();

            for (int i = 0; i < files.length; i++) {
                files[i].delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*
    * Open the workspace file
    */

    public String openWorkspaceFile() {
        int status = 0;
        String extentions[] = {"wsp", "wspx"};

        JFileChooser oFC = new JFileChooser(Settings.getAbsolutepath() + "workspaces");
        GUISettings.localizeFileChooserHomeButton(oFC);
        oFC.setFileSelectionMode(JFileChooser.FILES_ONLY);
        oFC.setDialogTitle(Language.getString("OPEN_WSP"));
        //extentions[0] = "wsp";
        TWFileFilter oFilter = new TWFileFilter(Language.getString("WORKSPACE"), extentions);
        oFC.setFileFilter(oFilter);
        GUISettings.applyOrientation(oFC);
        status = oFC.showOpenDialog(window);
        if (status == JFileChooser.APPROVE_OPTION) {
            return oFC.getSelectedFile().getAbsolutePath();
        } else {
            return null;
        }


    }

    /*
    * Open the workspace file
    */

    public String saveWorkspaceFile(boolean bSaveAs) {
        int status = 0;
        String sFile;
        String[] extentions = {"wsp", "wspx"};

        JFileChooser oFC = new JFileChooser(Settings.getAbsolutepath() + "workspaces");

        GUISettings.localizeFileChooserHomeButton(oFC);
        oFC.setFileSelectionMode(JFileChooser.FILES_ONLY);
        if (bSaveAs) {
            oFC.setDialogTitle(Language.getString("SAVE_AS_WSP"));
        } else {
            oFC.setDialogTitle(Language.getString("SAVE_WSP"));
        }
        //extentions[0] = "wsp";
        TWFileFilter oFilter = new
                TWFileFilter(Language.getString("WORKSPACE"), extentions);
        oFC.setFileFilter(oFilter);
        GUISettings.applyOrientation(oFC);

        status = oFC.showSaveDialog(window);
        File[] fileList = null;
        if (status == JFileChooser.APPROVE_OPTION) {
            try {
                if (bSaveAs) {
                    fileList = oFC.getCurrentDirectory().listFiles();
                    for (File file : fileList) {
                        if (file.getName().split("\\.")[0].equalsIgnoreCase(oFC.getSelectedFile().getName().split("\\.")[0])) {
//                                System.out.println("matching file found in  saving workspace...");
                            String message = Language.getString("REPLACE_WORKSPACE_MSG");
                            message = message.replaceAll("\\[WORKSPACE_NAME\\]", file.getName().split("\\.")[0]);
                            int option = SharedMethods.showConfirmMessage(message, JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                            if (option == JOptionPane.YES_OPTION) {
                                break;
                            } else {
                                return null;
                            }
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            sFile = oFC.getSelectedFile().getAbsolutePath().trim();
            if (!sFile.endsWith(".wspx"))
                sFile += ".wspx";
            g_sWorkspaceFile = sFile;
            return sFile;
        } else {
            return null;
        }
    }

    public void mnuSaveAs_ActionPerformed() {
        //setUndecorated(!isUndecorated());
        try {
            String sFile = saveWorkspaceFile(true);

            if (sFile != null) {
                g_sWorkspaceFile = sFile;
                Application.getInstance().fireWorkspaceWillSave();
                WorkspaceManager oWSM = new WorkspaceManager(this, g_oViewSettings);
                oWSM.saveWorkspace(g_sWorkspaceFile);
                notifyDefaultWSMenu();
                oWSM = null;
                setTitleText();
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Workspace, "Save");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Market Depth Calculator
     */
    /*public void mnu_MDepth_Calculator(String sSymbol, TWTypes.TradeSides side, boolean fromWSP) {
        try {


            String sSelectedSymbol = null;

            if (sSymbol == null) {
                sSelectedSymbol = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(sSymbol))
                    sSelectedSymbol = sSymbol;
                else
                    sSelectedSymbol = SymbolRepository.findKey(sSymbol);
            }

            if ((sSelectedSymbol == null) || (sSelectedSymbol.equals(Constants.NULL_STRING))) {
                new ShowMessage(Language.getString("MSG_SELECT_A_SYMBOL"), "E");
                return;
            }

            if ((!fromWSP) && (!Settings.isConnected())) {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                return;
            } else
            if ((!ExchangeStore.isValidIinformationType(SharedMethods.getExchangeFromKey(sSelectedSymbol), Meta.IT_DepthCalculator)) && (!fromWSP))
            {
                new ShowMessage("<html><b>" + Language.getString("TARGET_PRICE_CALC") + "&nbsp;&nbsp;</b><br>" +
                        Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
                return;
            }

            /* check if the window is already created
                If so, do not create it again, just focus it
             *
            InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.
                    MARKET_DEPTH_CALCULATOR_VIEW
                    + "|" + sSelectedSymbol);
            if (oFrame != null) {
                oFrame.show();
                try {
                    oFrame.setSelected(true);
                } catch (Exception x) {
                }

                if (oFrame.isIcon()) {
                    oFrame.setIcon(false);
                }
                return;
            } else {
                System.out.println("Received Frame is NULL");

            }
            ViewSetting oViewSetting = null;
            Table table = new Table();
            table.hideCustomizer();
            table.getPopup().hideCopywHeadMenu();
            table.setSortingDisabled();
            table.setAutoResize(true);
            //oDepthCal.setUseSameFontForHeader(true);
            table.setWindowType(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW);

            oViewSetting = g_oViewSettings.getDepthCalView(ViewSettingsManager.
                    MARKET_DEPTH_CALCULATOR_VIEW
                    + "|" + sSelectedSymbol);

            if (oViewSetting == null) {
                oViewSetting = g_oViewSettings.getSymbolView("DEPTH_CALCULATOR");
                oViewSetting = oViewSetting.getObject(); // clone the object
                oViewSetting.setID(sSelectedSymbol);
                g_oViewSettings.putDepthCalView(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW
                        + "|" + sSelectedSymbol, oViewSetting);
            } else {
                //dtqTable.setLoadedFromTheme(true);
            }

            DepthCalculatorModel model = new DepthCalculatorModel(side);
            model.setSymbol(sSelectedSymbol);
            model.setViewSettings(oViewSetting);
            table.hideHeader();
            table.setCalcModel(model);
            model.setTable(table);
            table.getTable().getTableHeader().setReorderingAllowed(false);
            model.applyColumnSettings();
            table.getTable().getTableHeader().setFont(table.getTable().getFont());
            JPanel calcStatus = new JPanel();
            BoxLayout calcStatusLayout = new BoxLayout(calcStatus, BoxLayout.LINE_AXIS);
            calcStatus.setLayout(calcStatusLayout);
            calcStatus.add(new JLabel(Language.getString("MSG_DEPTH_CALC_FOOTER")));
            calcStatus.add(Box.createHorizontalGlue());
            JLabel lblApprox = new JLabel(Language.getString("MSG_DEPTH_CALC_APPROXYMATED"));
            lblApprox.setFont(lblApprox.getFont().deriveFont(Font.BOLD));
            calcStatus.add(lblApprox);
            table.setSouthPanel(calcStatus);
            table.updateGUI();

            InternalFrame frame = new InternalFrame(table, g_oWindowDaemon);
            oViewSetting.setParent(frame);

            // Create the Calculator Header Panel
            MDepthCalculator configPanel = new MDepthCalculator(sSelectedSymbol, model, side);
            frame.setColumnResizeable(true);
            frame.setPrintable(true);
            frame.setSize(oViewSetting.getSize());
            frame.setLocation(oViewSetting.getLocation());
            frame.getContentPane().setLayout(new BorderLayout());
            frame.getContentPane().add(configPanel, BorderLayout.NORTH);
            frame.initSupportingObjcts(1);
            frame.addSupportingObject(configPanel, 0);
            frame.getContentPane().add(table, BorderLayout.CENTER);
            frame.setColumnResizeable(true);
            frame.setPrintable(true);
            frame.setResizable(true);
            frame.setClosable(true);
            frame.setMaximizable(false);
            frame.setIconifiable(true);
            oTopDesktop.add(frame);
            model.setParent(frame);
            configPanel.setViewSettings(oViewSetting);
            frame.setTitle(Language.getString("TARGET_PRICE_CALC") + " : " +
                    DataStore.getSharedInstance().getStockObject(sSelectedSymbol).getLongDescription());
            table.addTableUpdateListener(model);
            frame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            frame.updateUI();
            frame.applySettings();

            frame.show();
            frame.setLayer(GUISettings.TOP_LAYER);
            frame.setOrientation();
            g_oViewSettings.setWindow(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW + "|" +
                    sSelectedSymbol, frame);

//            String requestID = Meta.PRICE_DEPTH + "|" + sSelectedSymbol;
//            addDepthRequest(requestID, sSelectedSymbol, Meta.PRICE_DEPTH);
//            frame.setDataRequestID(DepthStore.getInstance(), sSelectedSymbol, requestID);
//            if (!loadedFromWorkspace) {
//                frame.setLocationRelativeTo(oTopDesktop);
//                oBidViewSetting.setLocation(frame.getLocation());
//            }

            String requestID = Meta.PRICE_DEPTH + "|" + sSelectedSymbol;
            addDepthRequest(requestID, sSelectedSymbol, Meta.PRICE_DEPTH);
            frame.setDataRequestID(DepthStore.getInstance(), sSelectedSymbol, requestID);

            if (!fromWSP) {
                frame.setLocationRelativeTo(oTopDesktop);
                oViewSetting.setLocation(frame.getLocation());
            }
            frame = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
    public void mnu_MDepth_Calculator(String sSymbol, TWTypes.TradeSides side, boolean fromWSP, boolean isLinked, String linkGroup) {
        if (!fromWSP) SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Depth, "OrderBookCalculator");
        try {

            String sSelectedSymbol = null;

            if (sSymbol == null) {
                sSelectedSymbol = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(sSymbol))
                    sSelectedSymbol = sSymbol;
                else
                    sSelectedSymbol = DataStore.getSharedInstance().findKey(sSymbol);
            }

            if ((sSelectedSymbol == null) || (sSelectedSymbol.equals(Constants.NULL_STRING))) {
                new ShowMessage(Language.getString("SELECT_A_SYMBOL"), "E");
                return;
            }

            if ((!fromWSP) && (!Settings.isConnected())) {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                return;
            } else if ((!ExchangeStore.isValidIinformationType(SharedMethods.getExchangeFromKey(sSelectedSymbol), Meta.IT_DepthCalculator)) && (!fromWSP)) {
                new ShowMessage("<html><b>" + Language.getString("TARGET_PRICE_CALC") + "&nbsp;&nbsp;</b><br>" +
                        Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
                return;
            }
            if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return;
            }
            if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return;
            }

            /* check if the window is already created
               If so, do not create it again, just focus it
            */
            if (!isLinked) {
                InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW
                        + "|" + sSelectedSymbol);
                if (oFrame != null) {
                    oFrame.show();
                    try {
                        oFrame.setSelected(true);
                    } catch (Exception x) {
                    }

                    if (oFrame.isIcon()) {
                        oFrame.setIcon(false);
                    }
                    return;
                } else {
                    System.out.println("Received Frame is NULL");
                }
            }
            DepthCalculatorWindow depthCalculatorWindow = new DepthCalculatorWindow(g_oWindowDaemon, g_oViewSettings, sSelectedSymbol, side, fromWSP, isLinked, linkGroup);
//            oTopDesktop.add(depthCalculatorWindow);
//            depthCalculatorWindow.show();
//            depthCalculatorWindow.setLayer(GUISettings.TOP_LAYER);
//            depthCalculatorWindow.setOrientation();
//            g_oViewSettings.setWindow(ViewSettingsManager.MARKET_DEPTH_CALCULATOR_VIEW + "|" +
//                    sSelectedSymbol, depthCalculatorWindow);
//
//            String requestID = Meta.PRICE_DEPTH + "|" + sSelectedSymbol;
//            addDepthRequest(requestID, sSelectedSymbol, Meta.PRICE_DEPTH);
//            depthCalculatorWindow.setDataRequestID(DepthStore.getInstance(), sSelectedSymbol, requestID);
//            depthCalculatorWindow.applySettings();
//            depthCalculatorWindow.show();
//            if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
//                DataStore.getSharedInstance().addSymbolRequest(sSelectedSymbol);
//            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void mnu_cascadeWindows() {
        //WindowArranger w = new WindowArranger();
        WindowArranger.tileFrames(oTopDesktop, WindowArranger.CASCADE);
        //w = null;
    }

// --Commented out by Inspection START (3/29/05 2:32 PM):
//    public void mnu_TileHorizWindows() {
//        //WindowArranger w = new WindowArranger();
//        WindowArranger.tileFrames(oTopDesktop, WindowArranger.HORIZONTAL);
//        //w = null;
//    }
// --Commented out by Inspection STOP (3/29/05 2:32 PM)

// --Commented out by Inspection START (3/29/05 2:32 PM):
//    public void mnu_TileVerticalWindows() {
//        //WindowArranger w = new WindowArranger();
//        WindowArranger.tileFrames(oTopDesktop, WindowArranger.VERTICAL);
//        //w = null;
//    }
// --Commented out by Inspection STOP (3/29/05 2:32 PM)

    public void mnu_TileAllWindows() {
        WindowArranger.tileFrames(oTopDesktop, WindowArranger.ARRANGE);
    }

    public void mnu_TileAllWindowsH() {
        WindowArranger.tileFrames(oTopDesktop, WindowArranger.HORIZONTAL);
    }

    public void mnu_TileAllWindowsV() {
        WindowArranger.tileFrames(oTopDesktop, WindowArranger.VERTICAL);
    }

// --Commented out by Inspection START (3/29/05 2:32 PM):
//    public void mnu_RestoreWindows() {
//        //WindowArranger w = new WindowArranger();
//        WindowArranger.restoreAll(oTopDesktop);
//        //w = null;
//    }
// --Commented out by Inspection STOP (3/29/05 2:32 PM)

// --Commented out by Inspection START (3/29/05 2:31 PM):
//    public void mnu_MinimizeWindows() {
//        //WindowArranger w = new WindowArranger();
//        WindowArranger.minimizeWindows(oTopDesktop);
//        //w = null;
//    }
// --Commented out by Inspection STOP (3/29/05 2:31 PM)


    public void mnuSave_ActionPerformed() {
        try {
            String sFile;

            if ((g_sWorkspaceFile == null) || (g_sWorkspaceFile.equals("")))
                sFile = saveWorkspaceFile(false);
            else
                sFile = g_sWorkspaceFile;
            if (sFile != null) {
                WorkspaceManager oWSM = new WorkspaceManager(this, g_oViewSettings);
                Application.getInstance().fireWorkspaceWillSave();
                oWSM.saveWorkspace(sFile);
                g_sWorkspaceFile = sFile;
                setTitleText();
                notifyDefaultWSMenu();
                if (sFile.endsWith(".wsp")) {
                    String newFileName = g_sWorkspaceFile.substring(0, g_sWorkspaceFile.lastIndexOf(".")) + ".wspx";
                    Settings.setDefaultWorkspace(newFileName);
                }
                oWSM = null;
                sFile = null;
                canExit = true;
            } else {
                canExit = false;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void mnu_SelectedSymbols_ActionPerformed() {
        try {
            if (!MainUI.getSharedInstance().isVisible()) {
                getDesktop().add(MainUI.getSharedInstance());
                MainUI.getSharedInstance().loadTree();
                MainUI.getSharedInstance().setVisible(true);
            } else {
                if (MainUI.getSharedInstance().isIcon()) {
                    MainUI.getSharedInstance().setIcon(false);
                    MainUI.getSharedInstance().setSelected(true);
                }


            }
//            MainUI.getSharedInstance().setVisible(false);
        } catch (Exception e) {

        }

    }


    public void mnuOpen_ActionPerformed() {
        try {
            String sFile = openWorkspaceFile();
            if (sFile != null) {
                g_sWorkspaceFile = sFile;
                WorkspaceManager oWSM = new WorkspaceManager(this, g_oViewSettings);
                oWSM.loadWorkspace(sFile, false);
                setTitleText();
                notifyDefaultWSMenu();
                oWSM = null;
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Workspace, "Open");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getSelectedWorkspace() {
        return g_sWorkspaceFile;
    }

    public void openWorkSpace(String wsName) {
        try {
            g_sWorkspaceFile = wsName;
            WorkspaceManager oWSM = new WorkspaceManager(this, g_oViewSettings);
            oWSM.loadWorkspace(wsName, true);
            setTitleText();
            notifyDefaultWSMenu();
            oTopDesktop.setDetachedFrames();
            oTopDesktop.setDetachedClientTables();
            oWSM = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

//        try {
//            g_sWorkspaceFile = wsName;
//            WorkspaceManager oWSM = new WorkspaceManager(this, g_oViewSettings);
//            //if (Settings.isDefaultThemeAvailable()) {
//            System.out.println("workspace = "+Settings.getDefaultWorkspace());
//            try {
//                oWSM.loadWorkspace(wsName);
//            } catch (Exception e) {
//            }
//            setTitleText();
//            notifyDefaultWSMenu();
//            //} else {
//            //    openDefaultWorkSpace(); // open the system default WSpace
//            //}
//            oWSM = null;
//            oTopDesktop.revalidate();
//            oTopDesktop.updateUI();
//
//            if ((defaultBoardtable != null) && (defaultBoardtable.isVisible() && (Settings.isScrollingOn()))) {
//                try {
//                    defaultBoardtable.setSelected(true);
//                } catch (PropertyVetoException ex) {
//                }
//                defaultBoardtable.getTable().requestFocus();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            //openDefaultWorkSpace();
//        }
    }


    public void openSavedDefaultWorkSpace(boolean isDefaultWSPAvailable) {
        try {
            File file = new File(Settings.getDefaultWorkspace());
            if (file.isAbsolute()) {
                g_sWorkspaceFile = Settings.getDefaultWorkspace();
            } else {
                g_sWorkspaceFile = Settings.getAbsolutepath() + Settings.getDefaultWorkspace();
            }
            WorkspaceManager oWSM = new WorkspaceManager(this, g_oViewSettings);
            //if (Settings.isDefaultThemeAvailable()) {
            try {
                oWSM.loadWorkspace(g_sWorkspaceFile, isDefaultWSPAvailable);
            } catch (Exception e) {
            }
            setTitleText();
            notifyDefaultWSMenu();
            //} else {
            //    openDefaultWorkSpace(); // open the system default WSpace
            //}
            oWSM = null;

//            oTopDesktop.revalidate(); //Bug id <#0037> problem in column order in the main board windows
            oTopDesktop.updateUI();

            if ((defaultBoardtable != null) && (defaultBoardtable.isVisible() && (Settings.isScrollingOn()))) {
                try {
                    defaultBoardtable.setSelected(true);
                } catch (PropertyVetoException ex) {

                }
                defaultBoardtable.getTable().requestFocus();
            }
        } catch (Exception e) {
            e.printStackTrace();
            //openDefaultWorkSpace();
        }
    }

    public void notifyDefaultWSMenu() {
        try {
            if (!g_sWorkspaceFile.equals(Settings.getDefaultWorkspace())) {
                g_oMenus.setEnableMakeDefaultWS(true);
            } else {
                g_oMenus.setEnableMakeDefaultWS(false);
            }
        } catch (Exception e) {
            g_oMenus.setEnableMakeDefaultWS(true);
        }
    }

    /**
     * Open the system default workspace
     */
//    private void openDefaultWorkSpace() {
//        try {
//            WorkspaceManager oWSM = new WorkspaceManager(this, g_oViewSettings);
//            oWSM.loadWorkspace(Settings.SYSTEM_PATH + "default.dll");
//            oWSM = null;
//        } catch (Exception e) {
//            //e.printStackTrace();
//        }
//    }
    public void mnuMakeDefault_ActionPerformed() {
        if (g_sWorkspaceFile != null) {
            Settings.setDefaultWorkspace(g_sWorkspaceFile);
            g_oMenus.setEnableMakeDefaultWS(false);
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.System, "SetDefaultWSP");
        }
    }

    public Menus getMenus() {
        return g_oMenus;
    }

    /**
     * Connect to the Server using tcp/ip
     */
    public void connectActionPerformed(boolean manualMode) {
        if (!Settings.isTCPMode()) {
            Authenticator.getSharedInstance().getUserInfo();
        } else {
            if (!Settings.isConnected()) {
                g_oMenus.lockServerConnection();
                connector.setIdle();
            } else {
                setCanShowDisconnectMessage(false);
                g_oMenus.setDisConnectedStatus();
                Settings.setInvalid();
            }
            setTitleText();
        }
    }

    public void disconnectActionPerformed() {
        if (Settings.isTCPMode()) {
            if (Settings.isConnected()) {
                //g_oMenus.lockServerConnection();
                disconnectFromServer();
                //connector.setMode(TCPConnector.STOPPED);
                setDisConnectedStatus(true);

            }
            //setTitleText();
        }
    }

    /**
     * Connect to the Server using tcp/ip
     */
    public void mnu_TradingConnect_ActionPerformed(boolean manualMode) {

        String path_en = (System.getProperties().get("user.dir") + "\\Templates\\Login\\EN\\login.html");
        String path_ar = (System.getProperties().get("user.dir") + "\\Templates\\Login\\AR\\login.html");
        if(TWControl.isEnableWebLogin()){
            String params = TWControl.getPostParams();
//            String params = "__EVENTTARGET=ctl00%24ArabicLinkButton&__EVENTARGUMENT=&__VIEWSTATE=%2FwEPDwUKMTIyMjkyNDgzNg9kFgJmD2QWAgIDD2QWDgIDD2QWAgIFDw8WAh4HVmlzaWJsZWhkZAIHDw8WAh8AaGRkAgkPDxYCHwBoZGQCCw8PFgIeBFRleHQFFFR1ZXNkYXksIDA3IEphbiAyMDE0ZGQCDQ9kFgICAw8PFgIfAQUFTG9naW5kZAIPD2QWAgIDD2QWAgIBD2QWAgIDD2QWBAIBDw8WAh8BZWRkAgMPDxYCHwFlZGQCEQ8PFgIfAQUKMS41LjkuMTAyMmRkGAEFIGN0bDAwJE1haW5Db250ZW50JExvZ2luTXVsdGlWaWV3Dw9kZmSHa2kaenGcaw92f4pCI%2B%2BIF%2ByU8g%3D%3D&__PREVIOUSPAGE=jmxUYmZYbKQqXO7lDmorO01vJVtbvsqhvcBFXD98omR7XDAJzxmNXlT4ZOfV-LCap0il8tFe7nMa_oX_ripsclBnyQY1&__EVENTVALIDATION=%2FwEWCwKmy%2FznAwKOkrn2CAKLkrn2CALVhM%2F7BgKMkrn2CAKRkrn2CAKSkrn2CAKpifTvDALinozKBALv8O2RCgL06qmjCqNVta63BQcHmJX92i5HHqgEsKtA&ctl00%24etrade";
            if(!Language.isLTR()){
//  latest              BrowserManager.getInstance().navigate("LOGIN3",TWControl.getRequestURL("TRADE_LOGIN"),Language.getString("TRADE_LOGIN"),null,params);  //Templates\Login\login.html
//                BrowserManager.getInstance().navigate("LOGIN3",path_ar,Language.getString("TRADE_LOGIN"),null,params);
                BrowserManager man = BrowserManager.getInstance();
                man.navigate("LOGIN3",TWControl.getRequestURL("TRADE_LOGIN"),Language.getString("TRADE_LOGIN"),null,params, false);

                IEBrowser br = man.getBrowser("LOGIN3");
                Dimension dSize = new Dimension(300,340);
                br.setLoginWindow(true);
                br.setBrowserSize(dSize);
                br.setAutoscrolls(false);
                br.setResizable(false);
                br.setMaximizable(false);
                br.setLocationRelativeTo(Client.getInstance().getDesktop());
                br.setPreferredSize(dSize);
                br.setSize(dSize);
                br.pack();

            } else {
//                BrowserManager.getInstance().navigate("LOGIN3",path_en,Language.getString("TRADE_LOGIN"));
                BrowserManager man = BrowserManager.getInstance();
                man.navigate("LOGIN3",TWControl.getRequestURL("TRADE_LOGIN"),Language.getString("TRADE_LOGIN"),false);
                IEBrowser br = man.getBrowser("LOGIN3");
                Dimension dSize = new Dimension(300,340);
                br.setLoginWindow(true);
                br.setBrowserSize(dSize);
                br.setAutoscrolls(false);
                br.setResizable(false);
                br.setMaximizable(false);
                br.setLocationRelativeTo(Client.getInstance().getDesktop());
                br.setPreferredSize(dSize);
                br.setSize(dSize);
                br.pack();
            }
            BrowserManager.getInstance().getBrowser("LOGIN3").setVisible(false);
            getMenus().getWebTrade().setVisible(true);
            getMenus().getMnuTrade().setVisible(false);
            getMenus().getWebTrade().setEnabled(false);
            mnuTrade.setVisible(false);
        } else if (!getMenus().getMnuTrade().isVisible()) {
            getMenus().getWebTrade().setVisible(false);
            getMenus().getMnuTrade().setVisible(true);
            mnuTrade.setVisible(true);
            getMenus().getMnuMWTrade().setVisible(false);
            mnuMWTrade.setVisible(false);
            MWTradingHandler.getSharedInstance().clearBrokerDetails();
        }

        try {
            if (MWTradingHandler.getSharedInstance().isVisible()) {
                MWTradingHandler.getSharedInstance().setVisible(false);
            }
        } catch (Exception e1) {
            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (autoTradeLoginDaemon != null && !TWControl.isEnableWebLogin()) {
            if (TradingShared.isConnected()) {
                //setTradeSessionDisconnected(true);
                int result = SharedMethods.showConfirmMessage(TradingShared.getMessageString("MSG_DISCONNECT_TRADE"), JOptionPane.WARNING_MESSAGE);
                if (result == JOptionPane.YES_OPTION) {
                    TradingShared.setDisconnectionMode(TradingConstants.DISCONNECTION_MANUAL);
                    com.isi.csvr.trading.IPSettings.getSharedInstance().resetIPIndex();
                    autoTradeLoginDaemon.closeSocket();
                    autoTradeLoginDaemon.closeSecondarySocket();
                    removeLoyaltyPoints();
                }
            } else {
                g_oMenus.lockTradeServerConnection();
                AutoTradeLoginDaemon.setIdle();
                autoTradeLoginDaemon.interrupt();
            }
        }
    }

    public void Win32IdleEventOccured(Win32IdleTime.State state) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (state.equals(Win32IdleTime.State.IDLE)) {
            mnu_TradingConnect_ActionPerformed(true);
        }
    }

    /**
     * Sets connected status on the gui
     */
    public void setConnectedStatus() {
        if (oMessageDialog != null) {
//            oMessageDialog = new MessageDialog(Client.getInstance().getFrame(),
//                    Language.getString("WINDOW_TITLE_CLIENT"), false);
            oMessageDialog.setVisible(false);
        }
//        oMessageDialog.setVisible(false);
//        MessageDialog.getSharedInstance().setStatus(MessageDialog.STATUS_SHOW);
//        MessageDialog.getSharedInstance().setVisible(false);
        if (Settings.isTCPMode()) {
            g_oMenus.setConnectedStatus();
            setPriceConnectionBulbStatus();
//            setPriceConnectionSucess();
            mnuAnnouncementSearch.setVisible(true);
            mnuAnnouncementSearch.setEnabled(true);
            //mnuCorporateActions.setVisible(true);
            //mnuCorporateActions.setEnabled(true);
            g_mnuNewsSearch.setVisible(true);
            g_mnuNewsSearch.setEnabled(true);
        } else {
            setRecevingSnapshots();
        }

        ConnectionNotifier.getInstance().fireTWConnected();
        /*System.out.println("===== Checking broker status =====");
        if(TradingShared.isOkForTradeConnection()){
            getMenus().unlockTradeServerConnection();
            System.out.println("===== unlocked ======");
        }*/
        setDownloadStatus("");
    }

    /**
     * Sets disconnected status on the gui
     */
    public void setDisConnectedStatus(boolean manualMode) {
        SharedMethods.printLine("Disconnected==", manualMode);
        if (TradingShared.isConnected() || TradingShared.isConnecting()) {

        } else if (!(TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct))) {
            TradingShared.setOkForTradeConnection(false); // stop trade logins while price path is not avaiable
        }
        setStatusProgress(0); // reset shapshot downloading progress
        g_oMenus.setDisConnectedStatus();
        setPriceConnectionBulbStatus();
//        setPriceConnectionDown();
        ConnectionNotifier.getInstance().fireTWDisconnected();
        mnuDepthByPrice.setVisible(false);
        mnuDepthByPrice.setEnabled(false);
        mnuDepthByOrder.setVisible(false);
        mnuDepthByOrder.setEnabled(false);
        mnuDepthOddLot.setVisible(false);
        mnuDepthOddLot.setEnabled(false);
        mnuTPCalc.setVisible(false);
        mnuTPCalc.setEnabled(false);
        mnuDepthSpecial.setVisible(false);
        mnuDepthSpecial.setEnabled(false);
        mnuAnnouncementSearch.setVisible(false);
        mnuAnnouncementSearch.setEnabled(false);
        //mnuCorporateActions.setVisible(false);
        //mnuCorporateActions.setEnabled(false);
        g_mnuNewsSearch.setVisible(false);
        g_mnuNewsSearch.setEnabled(false);
        TradingShared.setDisconnectionMode(TradingConstants.DISCONNECTION_PRICE_CONNECTION);
        /*if (autoTradeLoginDaemon != null)
            autoTradeLoginDaemon.closeSocket(); // disconnect from trading system*/

        setStatus(Language.getString("NOT_CONNECTED"));
        setTitleText();


        if (!manualMode) {
            //System.out.println("Manual Mode is false");
            connector.setMode(TCPConnector.IDLE);
        }

        if (!TradingShared.isConnected() && !((TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)))) {
            g_oMenus.lockTradeServerConnection();
            g_oMenus.unlockServerConnection();
        }
        if ((Client.getInstance() != null) && (Client.getInstance().getFrame().getExtendedState() != JFrame.ICONIFIED) && (!Settings.isOfflineMode())) {
            if (oMessageDialog == null) {
                oMessageDialog = new MessageDialog(Client.getInstance().getFrame(),
                        Language.getString("WINDOW_TITLE_CLIENT"), false);
            }
            oMessageDialog.setLocationRelativeTo(Client.getInstance().getFrame());
            if (!manualMode && !(TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct))) {
                oMessageDialog.setVisible(true);
            }
        }
    }

    public void validatePopupMenuItems() {
        String key = getSelectedSymbol();
        String exchange = SharedMethods.getExchangeFromKey(key);
        if (key.equals(Constants.NULL_STRING)) {
            descriptLabel.setText("");
            descriptLabel.setVisible(false);
        } else {
            descriptLabel.setText((DataStore.getSharedInstance().getStockObject(key)).getShortDescription());
            descriptLabel.setVisible(true);
        }
        int instrumentType;
        try {
            instrumentType = DataStore.getSharedInstance().getStockObject(key).getInstrumentType();
        } catch (Exception e) {
            instrumentType = Meta.INSTRUMENT_QUOTE;
        }

        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
//            setPopupMenu(key);
            if (key != Constants.NULL_STRING && key != null) {
                if (ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(key)).isDefault() && !ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(key)).isExpired() && Settings.isConnected()) {
                    setPopupMenu(key);
                    if (!isEnable) {
                        mnuEnable.setVisible(true);
                        mnuDisable.setVisible(false);
                    } else if (isEnable) {
                        mnuEnable.setVisible(false);
                        mnuDisable.setVisible(true);
                    }
                } else {
                    mnuEnable.setVisible(false);
                    mnuDisable.setVisible(false);
                }
            }
        }

        try {
            if (g_oTempTable.isVolumeWatcherType() || g_oTempTable.isCashFlowWatcherType() && ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
                mnuEnable.setVisible(false);
                mnuDisable.setVisible(false);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


        mnuDepthByPrice.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_MarketDepthByPrice, true));
        mnuDepthByPrice.setEnabled(isValidInfoType(getSelectedSymbol(), Meta.IT_MarketDepthByPrice, true));
        mnuDepthByOrder.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_MarketDepthByOrder, true));
        mnuDepthByOrder.setEnabled(isValidInfoType(getSelectedSymbol(), Meta.IT_MarketDepthByOrder, true));
        mnuDepthSpecial.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_SpecialOrderBook, true));
        mnuDepthSpecial.setEnabled(isValidInfoType(getSelectedSymbol(), Meta.IT_SpecialOrderBook, true));
        mnuTPCalc.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_DepthCalculator, true));
        mnuTPCalc.setEnabled(isValidInfoType(getSelectedSymbol(), Meta.IT_DepthCalculator, true));
        mnuStockReport.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_StockReports, true));
        g_mnuStockReports.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_StockReports, true));
        mnuRQuotes.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_RegionalQuotes, true));
        g_mnuFD2.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_FundamentalData, true));
        mnuCorporateActions.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_CorporateActions, true));
        mnuCompanyPfofile.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_CompanyProfile, true));
        g_mnuOptions.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_OptionChain, true));
        g_mnuOptionSymbolBuilder.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_OptionChain, true));
        g_mnuFutures.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_FuturesChain, true));
        mnuCashFQ.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_CashFlow, true));
        if (ExchangeStore.getSharedInstance().checkFeatureAvailability(exchange, Meta.IT_SnapQuote)) {

            mnuSnapQ.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_SnapQuote, true));
        } else {
            mnuSnapQ.setVisible(false);
        }
        mnuFullQ.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_FullQuote, true));
        /*mnuCompanyProfile.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_CompanyProfile, true));
        mnuCompanyProfile.setEnabled(isValidInfoType(getSelectedSymbol(), Meta.IT_CompanyProfile, true));*/
        if (TWControl.isOddLotEnabled()) {
            mnuDepthOddLot.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_MarketDepthByPrice, true));
            mnuDepthOddLot.setEnabled(isValidInfoType(getSelectedSymbol(), Meta.IT_MarketDepthByPrice, true));
//            mnuDepthByOrder.setVisible(false);
//            mnuDepthByOrder.setEnabled(false);

        }
//        if (ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()) {
//            mnuAnnouncementSearch.setVisible(true);
//            mnuAnnouncementSearch.setEnabled(true);
//        }else{
//            mnuAnnouncementSearch.setVisible(false);
//            mnuAnnouncementSearch.setEnabled(false);
//        }
        if (ExchangeStore.getSharedInstance().isAnnouncementMenuAvailable(getSelectedSymbol())) {
            mnuAnnouncementSearch.setVisible(isValidSystemWindow(Meta.IT_AnnouncementSearch, true));
            mnuAnnouncementSearch.setEnabled(isValidSystemWindow(Meta.IT_AnnouncementSearch, true));
        } else {
            mnuAnnouncementSearch.setVisible(false);
            mnuAnnouncementSearch.setEnabled(false);
        }
        g_mnuNewsSearch.setVisible(isValidSystemWindow(Meta.IT_News, true));
        g_mnuNewsSearch.setEnabled(isValidSystemWindow(Meta.IT_News, true));

        if (key == Constants.NULL_STRING || key == null) {
            mnuAlert.setVisible(false);
            mnuAlert.setEnabled(false);
            mnuRemoveSymbol.setVisible(false);
            mnuRemoveSymbol.setEnabled(false);
            mnueditAlias.setVisible(false);
            mnueditAlias.setEnabled(false);
            mnuDisable.setVisible(false);
            mnuDisable.setEnabled(false);
            mnuDTQ.setVisible(false);
            mnuDTQ.setEnabled(false);
            mnuGraph.setVisible(false);
            mnuGraph.setEnabled(false);
            g_mnuNewsSearch.setEnabled(false);
            g_mnuNewsSearch.setVisible(false);
            addToWatchList.setVisible(false);
            addToWatchList.setEnabled(false);
        } else {
            mnuAlert.setVisible(isValidSystemWindow(Meta.IT_Alerts, true));
            mnuAlert.setEnabled(isValidSystemWindow(Meta.IT_Alerts, true));
        }

        //g_mnuTreePopEditView.setVisible(TWControl.isServerSideCustOverridden() || ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigWatchList));
        g_mnuTreePopEditView.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigWatchList));
        //mnuTreePopDelete.setVisible(TWControl.isServerSideCustOverridden() || ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigWatchList));
        mnuTreePopDelete.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_ConfigWatchList));

        try {
            int vasStart = g_oTablePopup.getComponentIndex(mnuVasStart);
            int vasEnd = g_oTablePopup.getComponentIndex(mnuVasEnd);
            Component[] items = new Component[vasEnd - vasStart - 1]; // take items in between
            int j = 0;
            for (int i = vasStart + 1; i < vasEnd; i++, j++) {
                items[j] = g_oTablePopup.getComponent(i);
            }
            validateVasMenus(items, exchange, instrumentType);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        if (instrumentType == Meta.INSTRUMENT_INDEX) {
//            g_mnuOptions.setVisible(false);
            g_mnuFutures.setVisible(false);
            mnuStockReport.setVisible(false);
            g_mnuStockReports.setVisible(false);
            mnuCorporateActions.setVisible(false);
            mnuCompanyPfofile.setVisible(false);

            mnuDepthByOrder.setVisible(false);
            mnuDepthByPrice.setVisible(false);
            mnuDepthSpecial.setVisible(false);
            mnuTPCalc.setVisible(false);
            mnuTimeNSales.setVisible(false);
            mnuTimeNSalesSummary.setVisible(false);
            mnuExportIntraday.setVisible(false);
            mnuAlert.setVisible(isValidSystemWindow(Meta.IT_Alerts, true));
            mnuSnapQ.setVisible(false);
            mnuCashFQ.setVisible(false);
            mnuFullQ.setVisible(false);
        } else {
            //todo - added temporary
//            mnuTimeNSales.setVisible(true);
            mnuTimeNSales.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_SymbolTimeAndSales, true));
            Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);

            if (ex != null && (ex.getMarketStatus() == Meta.MARKET_CLOSE || ex.getMarketStatus() == Meta.MARKET_PRECLOSE)) {
                mnuTimeNSalesSummary.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_TimeAndSales_Summary, true));
            } else {
                mnuTimeNSalesSummary.setVisible(isValidInfoType(getSelectedSymbol(), Meta.IT_TimeAndSales_Summary, true));
                mnuTimeNSalesSummary.setEnabled(false);
            }
            mnuExportIntraday.setVisible(true);
        }

        try {
            mnuTrade.setEnabled(TradingShared.isReadyForTrading() && TradingShared.isTradableExchange(exchange));
//            mnuProTrade.setEnabled(TradingShared.isReadyForTrading() && TradingShared.isTradableExchange(exchange) && TradingShared.isConditionalTradableExchange(exchange));
//            mnuProTrade.setVisible(TradingShared.isReadyForTrading() && TradingShared.isTradableExchange(exchange) && TradingShared.isConditionalTradableExchange(exchange));
            //if(instrumentType == Meta.FOREX){
            //    g_mnuForexBuySel.setVisible(true);
            //    mnuBuy.setVisible(false);
            //    mnuSell.setVisible(false);
            //} else {
            //    g_mnuForexBuySel.setVisible(false);
            //    mnuBuy.setVisible(true);
            //    mnuSell.setVisible(true);
            //}
        } catch (Exception e) {
        }

        try {
            if (mainboardSelected) {
                g_oMenus.createCurrencyPopup();
                g_oMenus.getCurrencyPopup().setVisible(true);
                g_oMenus.getCurrencyPopup().setEnabled(true);
                if (g_oTempTable.getCurrency() == null) {
                    try {
                        ((TWRadioButtonMenuItem) g_oMenus.getCurrencyPopup().getItem(0)).setSelected(true);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } else {
                    JMenu currencyPopup = g_oMenus.getCurrencyPopup();
                    int itemCount = currencyPopup.getItemCount();
                    for (int i = 1; i < itemCount; i++) {
                        TWRadioButtonMenuItem item = (TWRadioButtonMenuItem) currencyPopup.getItem(i);
                        if (item.getActionCommand().indexOf(g_oTempTable.getCurrency()) >= 0) {
                            item.setSelected(true);
                            break;
                        }
                        item = null;
                    }
                    currencyPopup = null;
                }
                if (g_oTempTable.isCustomType()) {
                    g_oMenus.getWatchlistFilterMenu().setVisible(true);
                    g_oMenus.getWatchlistFilterMenu().setEnabled(true);
                    g_oMenus.getWatchlistFilterUI().setClientTable(g_oTempTable);
                } else {
                    g_oMenus.getWatchlistFilterMenu().setVisible(false);
                }
                g_oMenus.getWatchlistFilterUI().populateUI();
//                setDecimalPlaces(((DraggableTable)g_oTempTable.getTable()).getDecimalPlaces());
//                mnuCopy.setVisible (true);
//                mnuCopyH.setVisible(true);
//                mnuLink.setVisible (true);
//                mnuLinkH.setVisible(true);
//                separator.setVisible(true);
//                watchListPopup.setVisible(true);
//                mnueditAlias.setVisible(true);
//                mnuAddSymbol.setVisible(true);
//                mnuRemoveSymbol.setVisible(true);
//                mnuSymbolFilter.setVisible(true);

                setDecimalPlaces(((DraggableTable) g_oTempTable.getTable()).getDecimalPlaces());


            } else if (mistViewSelected) {
                g_oMenus.createCurrencyPopup();
                g_oMenus.getCurrencyPopup().setVisible(true);
                g_oMenus.getCurrencyPopup().setEnabled(true);
                try {
                    (g_oMenus.getCurrencyPopup().getItem(0)).setSelected(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (g_oMistTable.getViewSettings().getCurrency() == null) {
                } else {
                    JMenu currencyPopup = g_oMenus.getCurrencyPopup();
                    int itemCount = currencyPopup.getItemCount();
                    for (int i = 1; i < itemCount; i++) {
                        TWRadioButtonMenuItem item = (TWRadioButtonMenuItem) currencyPopup.getItem(i);
                        if (item.getActionCommand().indexOf(g_oMistTable.getViewSettings().getCurrency()) >= 0) {
                            ((MISTModel) g_oMistTable.getModel1()).setCurrency(g_oMistTable.getViewSettings().getCurrency());
                            item.setSelected(true);
                            g_oMistTable.setWindowTitle();
                            break;
                        }
                        item = null;
                    }
                    currencyPopup = null;
                }
                g_oMenus.getWatchlistFilterMenu().setVisible(true);
                g_oMenus.getWatchlistFilterUI().setMISTTable(g_oMistTable);
                g_oMenus.getWatchlistFilterUI().populateUI();
                setDecimalPlaces(((MISTModel) g_oMistTable.getTable().getModel()).getDecimalPlaces());
//                mnuCopy.setVisible (true);
//                mnuCopyH.setVisible(true);
//                mnuLink.setVisible (true);
//                mnuLinkH.setVisible(true);
//                separator.setVisible(true);
//                watchListPopup.setVisible(true);
//                mnueditAlias.setVisible(true);
//                mnuAddSymbol.setVisible(true);
//                mnuRemoveSymbol.setVisible(true);
//                mnuSymbolFilter.setVisible(true);
            } else if (sideBarSelected) {
                mnuCopy.setVisible(false);
                mnuCopyH.setVisible(false);
                mnuLink.setVisible(false);
                mnuLinkH.setVisible(false);
                mnuAllEnable.setVisible(false);
                mnuAllDisable.setVisible(false);

                if (separator != null) {
                    separator.setVisible(false);
                }
                watchListPopup.setVisible(false);
                mnueditAlias.setVisible(false);
                mnuAddSymbol.setVisible(false);
                mnuRemoveSymbol.setVisible(false);
                mnuSymbolFilter.setVisible(false);
                mnuHeatMap.setVisible(false);

            } else if (volumeWatcherSelected || cashFlowWatcherSelected) {

                g_oMenus.getCurrencyPopup().setVisible(false);
                g_oMenus.getCurrencyPopup().setEnabled(false);

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (key != null) {
            Stock s = DataStore.getSharedInstance().getStockObject(key);
            if (s != null) {
                if (s.isSymbolEnabled()) {
                    mnuDTQ.setEnabled(true);
                    mnuSnapQ.setEnabled(true);

                    mnuGraph.setEnabled(true);
                    mnuGraphInProChart.setEnabled(true);
                    mnuTimeNSales.setEnabled(true);
                    mnuDepthByPrice.setEnabled(true);
                    mnuDepthByOrder.setEnabled(true);
                    mnuDepthSpecial.setEnabled(true);
                    mnuTPCalc.setEnabled(true);
                } else {
                    mnuCashFQ.setEnabled(false);
                    mnuFullQ.setEnabled(false);
                    mnuDTQ.setEnabled(false);
                    mnuSnapQ.setEnabled(false);

                    mnuGraph.setEnabled(false);
                    mnuGraphInProChart.setEnabled(false);
                    mnuTimeNSales.setEnabled(false);
                    mnuDepthByPrice.setEnabled(false);
                    mnuDepthByOrder.setEnabled(false);
                    mnuDepthSpecial.setEnabled(false);
                    mnuTPCalc.setEnabled(false);
                }
            }
        }
        //enabling optio for testing.....,.,
        //mnuStockReport.setVisible(true);
    }

    private void validateVasMenus(Component[] items, String selectedExchange, int instrumentType) {


        for (Component item : items) {

            if ((item instanceof JMenuItem) || (item instanceof JMenu)) {
                try {
                    boolean validExchange = false;
                    boolean validInstrument = false;
                    item.setVisible(true);

                    String[] exchanges = ((String[]) ((JMenuItem) item).getClientProperty("exchanges"));
                    String[] instruments = ((String[]) ((JMenuItem) item).getClientProperty("instruments"));
                    if (exchanges.length == 0) {
                        if (instruments.length == 0) {
                            item.setVisible(true); // no defined exchanges. i.e. no restriction
                        } else {
                            for (String instrument : instruments) {
                                if ((instruments != null) && (Integer.parseInt(instrument) == instrumentType)) {
                                    validInstrument = true;
                                    break;
                                }
                            }
                            item.setVisible(validInstrument);
                        }
                    } else {
                        //item.setVisible(false);

                        for (String exchange : exchanges) {
                            if ((exchange != null) && (selectedExchange.equals(exchange))) {
                                validExchange = true;
                                break;
                            }
                        }
                        if (validExchange) {
                            if (instruments.length == 0) {
                                item.setVisible(true); // no defined exchanges. i.e. no restriction
                            } else {
                                for (String instrument : instruments) {
                                    if ((instruments != null) && (Integer.parseInt(instrument) == instrumentType)) {
                                        validInstrument = true;
                                        break;
                                    }
                                }
                                item.setVisible(validInstrument);
                            }
                        } else {
                            item.setVisible(false);
                        }
                        if (item instanceof JMenu) {
                            validateVasMenus(((JMenu) item).getMenuComponents(), selectedExchange, instrumentType);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    item.setVisible(true);
                }
            }
        }
    }

    public void closeInvalidFrames() {
        if (!isValidSystemWindow(Meta.IT_MarketMap, true)) { // close the market map of not subscribed
            if (heatMap != null) {
                heatMap.closeWindow();
            }
        }

        /* if (!Settings.isTradeEnabledMode()) {
            g_oTimeSalesFrame.setVisible(false);
        }*/
    }

    /**
     * Change password option is selected
     */
    public void mnu_mnuChangePass_ActionPerformed() {
        if (Settings.isConnected()) {
            boolean sucess = doChangePass();
            if (sucess) {
                SharedMethods.updateGoogleAnalytics("ChangePW");
            }
        } else {
            new ShowMessage(Language.getString("MSG_NOT_LOGGED_IN"), "I");
        }
    }

    public void mnu_Config_ActionPerformed() {
//        Config c = new Config(window, Language.getString("CONFIGURE"), true);
        //GUISettings.applyOrientation(c, ComponentOrientation.LEFT_TO_RIGHT);
//        c.showDialog();
        ConnectivityHandler ccc = new ConnectivityHandler(window, Language.getString("CONFIGURE"), true);
        //GUISettings.applyOrientation(c, ComponentOrientation.LEFT_TO_RIGHT);
        ccc.show();
        if (ccc.isOkPressed()) {
            setProxySettings();
            if (!Settings.isConnected()) { // if not already connected, try to connect
                if ((Settings.getItemFromBulk("DEFAULT_USER") != null) &&
                        (!Settings.getItemFromBulk("DEFAULT_USER").trim().equals(Constants.NULL_STRING)) &&
                        (Settings.getItemFromBulk("PASSWORD") != null) &&
                        (!Settings.getItemFromBulk("PASSWORD").trim().equals(Constants.NULL_STRING))) {
                    // if both user and pass are present, then connect
                    if (connector.getMode() != TCPConnector.IDLE) {
                        resetConnectionCounter();
                    }
                }
            }
            SharedMethods.updateGoogleAnalytics("Config");
        }
        ccc = null;
    }

    public void mnu_AppSettingss_ActionPerformed() {
        ApplicationSettings.getInstance().setVisible(true);
    }
    /*public static void doChangePass() {
        ChangePass changePass = new ChangePass(getInstance().getFrame(),
                Language.getString("CHANGE_PASSWORD"), null, ChangePass.Modes.MODE_INCLUDE_OLD_PW);
        changePass.showDialog();
        String newPassword = changePass.getNewPassword();
        String oldPassword = changePass.getOldPassword();
        changePass.dispose();
        changePass = null;
        if (newPassword != null) {
            SendQFactory.addData(Meta.CHANGE_PASSWORD + Meta.DS
                    +Settings.getSessionID()  + Meta.ID
                    + SharedMethods.encrypt(oldPassword) + Meta.ID
                    + SharedMethods.encrypt(newPassword));
        }
    }*/

    public static boolean doChangePass() {
        ChangePass changePass = new ChangePass(getInstance().getFrame(),
                Language.getString("CHANGE_PASSWORD"), null, ChangePass.Modes.MODE_INCLUDE_OLD_PW);
        changePass.showDialog();
        String newPassword = changePass.getNewPassword();
        String oldPassword = changePass.getOldPassword();
        changePass.dispose();
        changePass = null;

        if (newPassword != null) {
            SendQFactory.addData(Constants.PATH_PRIMARY, Meta.CHANGE_PASSWORD + Meta.DS
                    + Settings.getSessionID() + Meta.ID
                    + SharedMethods.encrypt(oldPassword) + Meta.ID
                    + SharedMethods.encrypt(newPassword));
        }
        return changePass.isOkClicked();
    }

    /**
     * Remove symbol option is selected
     */
    public void mnu_RemoveSymbol() {
        String sSymbol;

        sSymbol = getSelectedSymbol();

        if (!sSymbol.trim().equals(Constants.NULL_STRING)) {
            ShowMessage oMessage = new ShowMessage(Language.getString("MSG_DELETE_SYMBOL"),
                    "W", true);

            int i = oMessage.getReturnValue();
            if (i == 1) {
                if (mainboardSelected) {
                    g_oTempTable.getSymbols().removeSymbol(sSymbol);
                    g_oTempTable.updateUI();
                    g_oTempTable.getTable().updateUI();
                    sendRemoveRequest(sSymbol);
                } else if (portfolioSelected) {
                    g_oPortfolioTable.getDataStore().removeSymbol(sSymbol);
                    g_oPortfolioTable.getTable().getTable().updateUI();
                } else if (mistViewSelected) {
                    g_oMistTable.getTable().getModel().getViewSettings().getSymbols().removeSymbol(sSymbol);
                    g_oMistTable.updateUI();
                }
                TradeFeeder.refreshSymbolIndex();
                String ex = SharedMethods.getExchangeFromKey(sSymbol);
                boolean isValid = ExchangeStore.getSharedInstance().isDefault(ex);
                {
                    if (!isValid) {

                        try {
                            OptimizedTreeRenderer.selectedSymbols.remove(sSymbol);
                            Stock st = DataStore.getSharedInstance().getStockObject(sSymbol);
                            st.setSymbolEnabled(false);
                            String exchange = st.getExchange();
                            String ssymbol = st.getSymbol();
                            int instype = st.getInstrumentType();
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    }
                }
            }
        }
    }

    private void sendRemoveRequest(String key) {
        String symbol = SharedMethods.getSymbolFromKey(key);
        String exchange = SharedMethods.getExchangeFromKey(key);
        int instrumentType = SharedMethods.getInstrumentTypeFromKey(key);
        //if (ExchangeStore.getSharedInstance().isDefault(exchange)){
//            SendQFactory.addRemoveRequest(exchange, symbol, Meta.MESSAGE_TYPE_EQUITY);
        DataStore.getSharedInstance().removeSymbolRequest(exchange, symbol, instrumentType);
        //}

    }

    public String getSelectedBaseSymbol() {
        String key = getSelectedSymbol();
        if ((key != null) && (!key.equals(Constants.NULL_STRING))) {
            Stock stock = DataStore.getSharedInstance().getStockObject(key);
            if (stock != null) {
                return SharedMethods.getKey(stock.getExchange(), stock.getOptionBaseSymbol(), stock.getInstrumentType());
            }
        }
        return null;
    }

    /**
     * Returns the symbol of the selectd row on the table
     */
    public String getSelectedSymbol() {
        try {
            boolean isSideBar = false;
            int iRow = 0;
            String sSymbol = null;
            String exchange = null;
            int instrumentType = -1;
            if (mainboardSelected) {
                if (g_oTempTable.isVisible()) {
                    iRow = g_oTempTable.getTable().getSelectedRow();

                    TableModel oModel = g_oTempTable.getModel();
                    sSymbol = ((StringTransferObject) oModel.getValueAt(iRow, -1)).getValue();
                    exchange = ((StringTransferObject) oModel.getValueAt(iRow, -2)).getValue();
                    instrumentType = (int) ((LongTransferObject) oModel.getValueAt(iRow, -11)).getValue();
                } else {
                    return Constants.NULL_STRING;
                }
            } else if (portfolioSelected) {
                if (g_oPortfolioTable.isVisible()) {
                    iRow = g_oPortfolioTable.getTable().getTable().getSelectedRow();

                    TableModel oModel = g_oPortfolioTable.getTable().getTable().getModel();
                    sSymbol = (String) oModel.getValueAt(iRow, 0);
                } else {
                    return Constants.NULL_STRING;
                }
            } else if (mistViewSelected) {
                if (g_oMistTable.isVisible()) {
                    iRow = g_oMistTable.getTable1().getSelectedRow();
                    String key = null;
                    if (g_oMistTable.getTable1().getRowCount() != (iRow + 1)) { // if not last row
                        key = g_oMistTable.getViewSettings().getSymbols().getFilteredSymbols()[(int) iRow / 2];
                        sSymbol = SharedMethods.getSymbolFromKey(key);
                        exchange = SharedMethods.getExchangeFromKey(key);
                        instrumentType = SharedMethods.getInstrumentTypeFromKey(key);
                    } else {
                        return Constants.NULL_STRING;
                    }
                    key = null;
                } else {
                    return Constants.NULL_STRING;
                }
            } else if (sideBarSelected) {
                if (g_oSideBar.isVisible()) {
                    isSideBar = true;
                    //System.out.println("Side bar test : " + g_oSideBar.getSelectedKey());
                    sSymbol = SharedMethods.getSymbolFromKey(g_oSideBar.getSelectedKey());
                    exchange = SharedMethods.getExchangeFromKey(g_oSideBar.getSelectedKey());
                    instrumentType = SharedMethods.getInstrumentTypeFromKey(g_oSideBar.getSelectedKey());
                } else {
                    return Constants.NULL_STRING;
                }
            } else if (volumeWatcherSelected) {
                if (g_oTempTable.isVisible()) {
                    iRow = g_oTempTable.getTable().getSelectedRow();
                    TableModel oModel = g_oTempTable.getModel();
                    sSymbol = ((StringTransferObject) oModel.getValueAt(iRow, -1)).getValue();
                    exchange = ((StringTransferObject) oModel.getValueAt(iRow, -2)).getValue();
                    instrumentType = (int) ((LongTransferObject) oModel.getValueAt(iRow, -11)).getValue();
                } else {
                    return Constants.NULL_STRING;
                }
            } else if (cashFlowWatcherSelected) {
                if (g_oTempTable.isVisible()) {
                    iRow = g_oTempTable.getTable().getSelectedRow();
                    TableModel oModel = g_oTempTable.getModel();
                    sSymbol = ((StringTransferObject) oModel.getValueAt(iRow, -1)).getValue();
                    exchange = ((StringTransferObject) oModel.getValueAt(iRow, -2)).getValue();
                    instrumentType = (int) ((LongTransferObject) oModel.getValueAt(iRow, -11)).getValue();
                } else {
                    return Constants.NULL_STRING;
                }
            }

            if ((exchange.equals("")) || (symbolBar.equals(""))) {
                descriptLabel.setText("");
                descriptLabel.setVisible(false);
//                descriptionmenu.setVisible(false);
                return Constants.NULL_STRING;
            } else {
                descriptLabel.setVisible(true);
//                descriptionmenu.setVisible(true);
//                descriptionmenu.setText((DataStore.getSharedInstance().getStockObject(exchange + Constants.KEY_SEPERATOR_CHARACTER + sSymbol)).getShortDescription());

                if (isSideBar && DataStore.getSharedInstance().getStockObject(SharedMethods.getKey(exchange, sSymbol, instrumentType)) == null) {
                    DataStore.getSharedInstance().createStockObject(exchange, sSymbol, instrumentType);
//                    if (!ExchangeStore.getSharedInstance().getExchange(exchange).isDefault() && OptimizedTreeRenderer.selectedSymbols.containsKey(DataStore.getSharedInstance().createStockObject(exchange, sSymbol, instrumentType).getKey())) {
//                        DataStore.getSharedInstance().getStockObject(exchange, sSymbol, instrumentType).setSymbolEnabled(true);
//                    }
                    if (!ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()) {
                        DataStore.getSharedInstance().getStockObject(exchange, sSymbol, instrumentType).setSymbolEnabled(true);
                    }
                }
                descriptLabel.setText((DataStore.getSharedInstance().getStockObject(SharedMethods.getKey(exchange, sSymbol, instrumentType))).getShortDescription());

                return SharedMethods.getKey(exchange, sSymbol, instrumentType);
            }
        } catch (Exception e) {
            return Constants.NULL_STRING;
        }
    }

    /**
     * Add a new symbol to the table
     */
    public void mnu_AddSymbol(boolean isNeedToCheckCellEditor) {
        try {
            if (mainboardSelected) {
                if (!isNeedToCheckCellEditor || !g_oTempTable.isCellEditMode()) {
                    SymbolSearch oCompanies = SymbolSearch.getSharedInstance();// new SymbolSearch(g_oTempTable.getTitle(), false,false);//, Meta.QUOTE);
                    oCompanies.setTitle(g_oTempTable.getTitle());
                    oCompanies.setSingleMode(false);
                    oCompanies.setIndexSearchMode(false);
                    oCompanies.init();
                    oCompanies.setSymbols(g_oTempTable.getSymbols());
                    oCompanies.setShowDefaultExchangesOnly(false);
                    oCompanies.showDialog(true);
                    g_oTempTable.updateGUI();
                    oCompanies = null;
                    g_oTempTable.getSymbols().reallocateIndex(); // recreate the symbol index
                } else {
                    new ShowMessage(Language.getString("MSG_PLEASE_EXIT_FROM_EDIT_MODE"), "E");
                }
//            } else if (portfolioView) {
//                ViewSetting settings = g_oPortfolioTable.getTable().getModel().getViewSettings();
//                PortfolioStore store = (PortfolioStore) settings.getSupportingObject();
//                Symbols symbols = new Symbols();
//                symbols.setSymbols(store.getSymbols());
//                Companies oCompanies = new Companies(true);
//                oCompanies.setSymbols(symbols);
//                oCompanies.showDialog();
//                oCompanies.dispose();
//                store.addSymbols(symbols.getSymbols());
//                g_oPortfolioTable.getTable().getTable().updateUI();
////                g_oPortfolioTable.updateTimeAnsSalesSymbols(true);
//                oCompanies = null;
//                settings = null;
//                store = null;
                /*} else if (mistView) {
           Companies oCompanies = new Companies(true);
           oCompanies.setSymbols(g_oMistTable.getTable().getModel().getViewSettings().getSymbols());
           oCompanies.showDialog();
           g_oMistTable.updateGUI();
           oCompanies.dispose();
           oCompanies = null;
           g_oMistTable.getTable().getModel().getViewSettings().getSymbols().reallocateIndex(); // recreate the symbol index*/
            } else if (mistViewSelected) {
                SymbolSearch oCompanies = SymbolSearch.getSharedInstance();
                oCompanies.setTitle(g_oMistTable.getTitle());
                oCompanies.setSingleMode(false);
                oCompanies.setIndexSearchMode(false);
                oCompanies.init();
                oCompanies.setSymbols(g_oMistTable.getTable().getModel().getViewSettings().getSymbols());
                oCompanies.setShowDefaultExchangesOnly(false);
                oCompanies.showDialog(true);
                g_oMistTable.updateGUI();
                oCompanies = null;
                g_oMistTable.getTable().getModel().getViewSettings().getSymbols().reallocateIndex(); // recreate the symbol index
            }
            TradeFeeder.refreshSymbolIndex();

        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Launch the Top Stocks Window
     */
    public void mnu_topStocks() {
        try {
            if (!TopStocksWindow.getSharedInstance().isVisible()) {
                TopStocksWindow.getSharedInstance().setVisible(true);
                SharedMethods.updateGoogleAnalytics("TopStocks");
            } else if (TopStocksWindow.getSharedInstance().isIcon()) {
                TopStocksWindow.getSharedInstance().setIcon(false);
            } else if (TopStocksWindow.getSharedInstance().isSelected()) {
                TopStocksWindow.getSharedInstance().closeWindow();
            } else {
                TopStocksWindow.getSharedInstance().setSelected(true);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    /**
     * Launch the Top Brokers Window
     */
    public void mnu_topBrokers() {
        try {
            if (!TopBrokersWindow.getSharedInstance().isVisible()) {
                TopBrokersWindow.getSharedInstance().setVisible(true);
            } else if (TopBrokersWindow.getSharedInstance().isIcon()) {
                TopBrokersWindow.getSharedInstance().setIcon(false);
            } else if (TopBrokersWindow.getSharedInstance().isSelected()) {
                TopBrokersWindow.getSharedInstance().closeWindow();
            } else {
                TopBrokersWindow.getSharedInstance().setSelected(true);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

// --Commented out by Inspection START (3/29/05 2:32 PM):
//    /**
//     * Customize ticker option is selected
//     */
//    public void mnu_customizeTicker() {
//        //#g_oTicker.searchSymbols();
//    }
// --Commented out by Inspection STOP (3/29/05 2:32 PM)

    public void applyTheme(String iID) {
        window.repaint();
        themeChanged(iID);
    }

    public void searchSymbolAnnouncements(String symbol) {
//        SimpleDateFormat dateFormatyyyy = new SimpleDateFormat("yyyyMMdd");
//        String endDate = dateFormatyyyy.format(new Date(System.currentTimeMillis()));
//        String searchString = Meta.ANNOUNCEMENT_SEARCH + Meta.DS + Meta.TYPE_SYMBOL + Meta.FD +
//                symbol + Meta.FD + "19700101" + Meta.FD + endDate;
//        SearchedAnnouncementStore.getSharedInstance().initSearch();
//        SearchedAnnouncementStore.getSharedInstance().doSearch(searchString, Language.getLanguageTag(), Integer.MAX_VALUE,ExchangeStore.getSharedInstance().getExchange(symbol).getSymbol());
//        SearchedAnnouncementStore.getSharedInstance().disableButtons();
//        showAnnouncementSearchWIndow();
//        setAnnouncementSearchTitle(symbol, Language.getString("SEARCHING"));
//        dateFormatyyyy = null;
//        endDate = null;
//        searchString = null;
    }

    /*public void searchSymbolNews(String symbol) {
        SimpleDateFormat dateFormatyyyy = new SimpleDateFormat("yyyyMMdd");
        String endDate = dateFormatyyyy.format(new Date(System.currentTimeMillis()));
        String searchString = Meta.ANNOUNCEMENT_SEARCH + Meta.DS + Meta.TYPE_SYMBOL + Meta.FD +
                symbol + Meta.FD + "19700101" + Meta.FD + endDate;
        SearchedNewsStore.getSharedInstance().initSearch();
        SearchedNewsStore.getSharedInstance().doSearch(searchString, Language.getLanguageTag(), Integer.MAX_VALUE);
        SearchedNewsStore.getSharedInstance().disableButtons();
        showNewsSearchWIndow();
        //setNewsSearchTitle(symbol, true, Language.getString("SEARCHING"));
        dateFormatyyyy = null;
        endDate = null;
        searchString = null;
    }*/

    /**
     * Color setting
     */
    public void themeChanged(String iID) {
        if (!Theme.getInstance().isValidTheme(iID)) {
            iID = Theme.getInstance().getDefaultThemeID();
        }

        MetalLookAndFeel.setCurrentTheme(Theme.getInstance());
        Theme.getInstance().loadTheme(iID);
        try {
            UIManager.setLookAndFeel("javax.swing.plaf.metal.MetalLookAndFeel");
        } catch (Exception e) {
            e.printStackTrace();
        }

        Theme.fireThemeChanged();
        if (criticalState) {
            lblExpiryStatus.setForeground(Theme.getColor("EXPIRY_DATE_CRITICAL_COLOR"));
        } else {
            lblExpiryStatus.setForeground(Theme.getColor("EXPIRY_DATE_NORMAL_COLOR"));
        }

        mnuTrade.setBackground(Theme.getColor("BOARD_TABLE_CELL_TRADE_BGCOLOR1"));
        mnuBuy.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        mnuSell.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
        mnuBuyCover.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        mnuSellCover.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));

        oTree.setBackground(Theme.getColor("MENU_BGCOLOR"));
        ExchangeTableRenderer.reload();
        CombinedDepthRenderer.reload();
        TreeRenderer.reload();
        ScanTreeRenderer.reload();
        CurrencyRenderer.reload();
        MISTRenderer.reload();
        DepthCalculatorRenderer.reload();
        DQTableRenderer.reload();
        MarketAskDepthRenderer.reload();
        MarketBidDepthRenderer.reload();
        SnapQuoteRenderer.reload();
//        oTree.updateUI();
        FunctionTableRenderer.reload();
        FundTransferTableRenderer.reload();
        CashFlowDetailQuoteRenderer.reload();
        TradingPfDCRenderer.reload();
        ScannerRenderer.reload();
        EntitlementRenderer.reload();

        try {
            g_oMenus.loadButtonImages(iID);
            g_oMenus.loadMenuImages(iID);
            g_oMenus.loadTheme();
        } catch (Exception e) {
            e.printStackTrace();
        }
        loadCloseButtonImage(iID);
        // Reload Renderers
        AnnouncementRenderer.reload();
        TableRenderer.reload();
        CheckBoxNodeRenderer.reload();
        TradeTableRenderer.reload();
        TradingPortfolioRenderer.reload();
        CommonTableRenderer.reload();
        OpenPositionsRenderer.reload();
        TopStocksRenderer.reload();
        TimeAndSalesRenderer.reload();
        CompanyTableRenderer.reload();
        PortfolioRenderer.reload();
        PortfolioRendererAll.reload();
        AdvanceSellTableRenderer.reload();
        SplitAdjustmentRenderer.reload();
        CommissionListRenderer.reload();
        Settings.saveTheme(iID);
        GraphDataRenderer.reload();
        GlobalMarketRenderer.reload();
        ForexTableRenderer.reload();
        CashFlowHistoryRenderer.reload();
        SectorOverviewRenderer.reload();
        TimeNSalesSummeryRenderer.reload();
        AnalysTechRenderer.reload();
        AccountSummaryTableRenderer.reload();

        /*if (windowTabs != null) {
            windowTabs.updateUI();
        }*/

        try {
            SwingUtilities.updateComponentTreeUI(g_oFramePopup);
            SwingUtilities.updateComponentTreeUI(g_oTablePopup);
            SwingUtilities.updateComponentTreeUI(g_oWatchListPopup);
            SwingUtilities.updateComponentTreeUI(g_oTableMultiPopup);
            SwingUtilities.updateComponentTreeUI(getFrame());
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        ActionMap amap = SwingUtilities.getUIActionMap(new JOptionPane());
        amap.remove("close");

        InputMap map = SwingUtilities.getUIInputMap(new JDesktopPane(), 1);
        map.remove(KeyStroke.getKeyStroke(KeyEvent.VK_F6, InputEvent.CTRL_MASK));
        map.remove(KeyStroke.getKeyStroke(KeyEvent.VK_F6, 0));
        map.remove(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, InputEvent.CTRL_MASK));

        map = SwingUtilities.getUIInputMap(new JTable(), 1);
        map.remove(KeyStroke.getKeyStroke(KeyEvent.VK_TAB, 0));

        lblStatus.setProgressColor(Theme.getColor("STATUSBAR_PROGRESSBAR_PROGRESS_COLOR"));
        lblDownloadStatus.setProgressColor(Theme.getColor("STATUSBAR_PROGRESSBAR_PROGRESS_COLOR"));
        lblStatus.setBackgroundColor(Theme.getColor("STATUS_BAR_COLOR"));
        lblDownloadStatus.setBackgroundColor(Theme.getColor("STATUS_BAR_COLOR"));
        lblStatus.setForeground(Theme.getColor("STATUSBAR_PROGRESSBAR_FGCOLOR"));
        lblDownloadStatus.setForeground(Theme.getColor("STATUSBAR_PROGRESSBAR_FGCOLOR"));

        Color backgroud = Theme.getOptionalColor("STATUS_BAR_COLOR");
        if (backgroud != null) {
            statusBarPanel.setBackground(backgroud);
        } else {
            statusBarPanel.setBackground(Theme.getColor("BACKGROUND_COLOR"));
        }
        if (g_oTableTableMenuToolbar != null) {
            g_oTableTableMenuToolbar.dispose();
        }
        g_oTableTableMenuToolbar = new TableMenuToolbar(getFrame());
        backgroud = null;

        CommonTableMenuToolbar.getSharedInstance().destroy();
        CommonTableMenuToolbar.getSharedInstance();

        try {
            Color bgColor = Theme.getOptionalColor("TAB_DT_BGCOLOR");
            tabPanel.setBackground(bgColor);
        } catch (Exception e) {
            Color bgColor = Theme.getOptionalColor("BACKGROUND_COLOR");
            tabPanel.setBackground(bgColor);
        }
        SymbolTreeRenderer.reload();
        SymbolNavigator.getSharedInstance().updateTree();
        ChartSymbolTreeRenderer.reload();
        ChartSymbolNavigator.getSharedInstance().updateTree();
        RadarTableRenderer.reload();
    }


    /**
     * Alert setup option is selected
     */
    public void mnu_AlertSetup() {
        mnu_AlertSetup(getSelectedSymbol());
    }

    public void mnu_AlertSetup(String symbol) {
        try {
            if (isValidWindow(Meta.IT_Alerts)) {
                if (!symbol.equals(Constants.NULL_STRING)) {
                    AlertSetup alert = new AlertSetup();
                    getDesktop().add(alert);
                    if (symbol == null) {
                        alert.showDialog(getSelectedSymbol());
                    } else {
                        alert.showDialog(symbol);
                    }
                    alert = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mnu_AlertManager() {
        if (isValidWindow(Meta.IT_Alerts)) {
            AlertManager.getSharedInstance().show();
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Alert, "Manager");
        }
    }

    public void validateWindowData(String key, String id) {
        if ((key == null) || (key.trim().equals(""))) return;

        String requestID = id + ":" + System.currentTimeMillis();
        SendQFactory.addValidateRequest(key, requestID, null, 0);
        SymbolsRegistry.getSharedInstance().rememberRequest(requestID, "N/A");
        System.out.println("Validation req sent " + key + " " + id);
    }


    public void setPopupMenu(String key) {

        String selectedKey = null;
        boolean loadedFromWorkspace = false;

        if (key == null) {
            selectedKey = getSelectedSymbol();
        } else {
            if (SharedMethods.isFullKey(key))
                selectedKey = key;
            else
                selectedKey = DataStore.getSharedInstance().findKey(key);
        }

        if (selectedKey != null && !(selectedKey.equals(""))) {
            Enumeration keys = OptimizedTreeRenderer.selectedSymbols.keys();
            while (keys.hasMoreElements()) {
                String keyval = (String) keys.nextElement();

                if (selectedKey.equals(keyval)) {
                    Boolean stockRet = OptimizedTreeRenderer.selectedSymbols.get(keyval);
                    if (stockRet) {
                        isEnable = true;
                    } else {
                        isEnable = false;
                    }
                    break;
                } else {
                    isEnable = false;
                }
            }
        }
    }

    public void enabelSymbol(String key, boolean symbolRegistered) {
        String selectedKey = null;
        boolean loadedFromWorkspace = false;

        if (key == null) {
            selectedKey = getSelectedSymbol();
        } else {
            if (SharedMethods.isFullKey(key))
                selectedKey = key;
            else
                selectedKey = DataStore.getSharedInstance().findKey(key);
        }

        String selectedSymbol = getSelectedSymbol();
        StringTokenizer fields = new StringTokenizer(selectedKey, "~");
        String first = fields.nextToken();
        String second = fields.nextToken();
        String third = fields.nextToken();
        MainUI.selectedSymbols.put(selectedSymbol, true);
        OptimizedTreeRenderer.selectedSymbols.put(selectedSymbol, true);

        Stock st = DataStore.getSharedInstance().getStockObject(selectedKey);
        st.setSymbolEnabled(true);
        SendQFactory.addOptimizationRecord(first, second + ":" + Integer.parseInt(third), Integer.parseInt(third));
        getDesktop().updateUI();
    }


    public void disableSymbol(String key, boolean symbolRegistered) {
        String selectedKey = null;
        boolean loadedFromWorkspace = false;

        if (key == null) {
            selectedKey = getSelectedSymbol();
        } else {
            if (SharedMethods.isFullKey(key))
                selectedKey = key;
            else
                selectedKey = DataStore.getSharedInstance().findKey(key);
        }

        String selectedSymbol = getSelectedSymbol();
        StringTokenizer fields = new StringTokenizer(selectedKey, "~");
        String first = fields.nextToken();
        String second = fields.nextToken();
        String third = fields.nextToken();
        MainUI.selectedSymbols.put(selectedSymbol, false);

        OptimizedTreeRenderer.selectedSymbols.remove(selectedSymbol);
        Stock st = DataStore.getSharedInstance().getStockObject(selectedKey);
        st.setSymbolEnabled(false);
        SendQFactory.removeOptimizationRecord(first, second + ":" + Integer.parseInt(third), Integer.parseInt(third));
//        DataStore.getSharedInstance().removeStock(first, second, Integer.parseInt(third));
//        DataStore.getSharedInstance().createStockObject(first, second, Integer.parseInt(third));
        getDesktop().updateUI();
    }

//    public void showSnapQuoteLinked(String key,boolean symbolRegistered){
//        showSnapQuote(key,symbolRegistered);
//        InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.SNAP_QUOTE
//                    + "|" + key);
//        LinkStore.getSharedInstance().LinkToSidebar(oFrame);
//
//    }

    public void doMiniTradeSymbol(String key, String timeStamp) {
        /*  MiniTradingFrame miniTrade = new MiniTradingFrame("Mini Trading");
        miniTrade.setVisible(true);
        getDesktop().add(miniTrade);*/
//        bulkFrame b = new bulkFrame();
        String selectedKey;
        if (key == null) {
            selectedKey = getSelectedSymbol();
        } else {
            if (SharedMethods.isFullKey(key))
                selectedKey = key;
            else
                selectedKey = DataStore.getSharedInstance().findKey(key);
        }
        MiniTradingFrame miniTrade = new MiniTradingFrame(selectedKey, timeStamp);
        miniTrade.setVisible(true);
        miniTrade.setFocus();


    }

    public void showSnapQuote(String key, boolean symbolRegistered, boolean fromWSp, boolean isLinked, String linkgroup) {
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.DetailQuote, "SnapQuote");
        try {

            String selectedKey = null;
            boolean loadedFromWorkspace = false;

            if (key == null) {
                selectedKey = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(key))
                    selectedKey = key;
                else
                    selectedKey = DataStore.getSharedInstance().findKey(key);
            }

            if (selectedKey.equals(Constants.NULL_STRING))
                return;

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            boolean isSymbolEnabled = DataStore.getSharedInstance().getStockObject(selectedKey).isSymbolEnabled();
            if (isSymbolEnabled) {
                if (!isLinked) {
                    InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.SNAP_QUOTE
                            + "|" + selectedKey);
                    if (oFrame != null) {
                        oFrame.show();
                        try {
                            oFrame.setSelected(true);
                        } catch (Exception x) {
                        }

                        if (oFrame.isIcon())
                            oFrame.setIcon(false);
                        return;
                    }
                }
                ViewSetting oViewSetting = null;
                Table oDetailQuote = new Table();

                SnapQuoteFrame snapFrme = new SnapQuoteFrame(oDetailQuote, g_oWindowDaemon, selectedKey, fromWSp, isLinked, symbolRegistered, linkgroup);

                if (snapFrme != null) {
                    return;
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showCashFlowDetailQuote(String key, boolean symbolRegistered, boolean fromWsp, boolean isLinked, String linkGroup) {
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.DetailQuote, "CashFlow");
        try {
            String selectedKey = null;
            boolean loadedFromWorkspace = false;

            if (key == null) {
                selectedKey = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(key))
                    selectedKey = key;
                else
                    selectedKey = DataStore.getSharedInstance().findKey(key);
            }

            if (selectedKey.equals(Constants.NULL_STRING))
                return;

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            if (!isLinked) {
                InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW
                        + "|" + selectedKey);
                if (oFrame != null) {
                    oFrame.show();
                    try {
                        oFrame.setSelected(true);
                    } catch (Exception x) {
                    }

                    if (oFrame.isIcon())
                        oFrame.setIcon(false);
                    return;
                }
            }
            ViewSetting oViewSetting = null;

            Table oDetailQuote = new Table();
            CashFlowDetailQuoteFrame dqFrame = new CashFlowDetailQuoteFrame(oDetailQuote, g_oWindowDaemon, selectedKey, fromWsp, isLinked, symbolRegistered, linkGroup);
            if (dqFrame != null) {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public void requestDetqilQuote(String key){
        if ((key == null) || (key.trim().equals(""))) return;

        String requestID = "DQ:" + System.currentTimeMillis();
        SendQFactory.addValidateRequest(key, requestID);
        SymbolsRegistry.getSharedInstance().rememberRequest(requestID, "N/A");
    }*/

// --- Added by Shanika-- For 3 row tickers------

    public void showDetailQuoteExternal(String symbol) {
        showDetailQuoteExternal(symbol, false);
    }

    public void showDetailQuoteExternal(String symbol, boolean symbolRegistered) {
        String key;
        if (symbol != null) {
            key = symbol;

            Stock stock = DataStore.getSharedInstance().getStockObject(key);
            if ((stock.getInstrumentType() == Meta.INSTRUMENT_INDEX)) {
                mnu_DetailQuoteIndex(key, symbolRegistered);
            } else if ((stock.getInstrumentType() == Meta.INSTRUMENT_MUTUALFUND)) {
                mnu_DetailQuoteMutualFund(key, symbolRegistered);
            } else if ((stock.getInstrumentType() == Meta.INSTRUMENT_OPTION)) {
                mnu_DetailQuoteOptions(key, symbolRegistered);
            } else if ((stock.getInstrumentType() == Meta.INSTRUMENT_FOREX)) {
                mnu_DetailQuoteForex(key, symbolRegistered);
            } else {
                mnuDetailQuoteSymbol(key, symbolRegistered, false, false, LinkStore.LINK_NONE);
            }

            stock = null;
        }
    }

    public void showDetailQuote(String symbol) {
        showDetailQuote(symbol, false);
    }

    public void showDetailQuote(String symbol, boolean symbolRegistered) {
        String key;

        if (symbol == null)
            key = getSelectedSymbol();
        else
            key = symbol;
        Stock stock = DataStore.getSharedInstance().getStockObject(key);
        boolean isSymbolEnabled = stock.isSymbolEnabled();
        if (isSymbolEnabled) {
            if (SharedMethods.getSymbolType(stock.getInstrumentType()) == Meta.SYMBOL_TYPE_INDEX) {
                mnu_DetailQuoteIndex(key, symbolRegistered);
            } else if (SharedMethods.getSymbolType(stock.getInstrumentType()) == Meta.SYMBOL_TYPE_MUTUALFUND) {
                mnu_DetailQuoteMutualFund(key, symbolRegistered);
            } else if (stock.getInstrumentType() == Meta.SYMBOL_TYPE_OPTIONS) {
                mnu_DetailQuoteOptions(key, symbolRegistered);
            } else if (SharedMethods.getSymbolType(stock.getInstrumentType()) == Meta.SYMBOL_TYPE_FOREX) {
                mnu_DetailQuoteForex(key, symbolRegistered);
            } else if (SharedMethods.getSymbolType(stock.getInstrumentType()) == Meta.INSTRUMENT_BOND) {
                mnuDetailQuoteBond(key, symbolRegistered, false, false, LinkStore.LINK_NONE);
            } else {
                mnuDetailQuoteSymbol(key, symbolRegistered, false, false, LinkStore.LINK_NONE);
            }
        }
        stock = null;
    }

    public void showDetailQuote(String symbol, boolean symbolRegistered, boolean fromWsp, boolean isLinked, String group) {
        String key;

        if (symbol == null)
            key = getSelectedSymbol();
        else
            key = symbol;
        Stock stock = DataStore.getSharedInstance().getStockObject(key);
        boolean isSymbolEnabled = stock.isSymbolEnabled();
        if (isSymbolEnabled) {
            if ((SharedMethods.getSymbolType(stock.getInstrumentType()) == Meta.SYMBOL_TYPE_INDEX)) {
                mnu_DetailQuoteIndex(key, symbolRegistered);
            } else if ((SharedMethods.getSymbolType(stock.getInstrumentType()) == Meta.SYMBOL_TYPE_MUTUALFUND)) {
                mnu_DetailQuoteMutualFund(key, symbolRegistered);
            } else if ((stock.getInstrumentType() == Meta.SYMBOL_TYPE_OPTIONS)) {
                mnu_DetailQuoteOptions(key, symbolRegistered);
            } else if ((SharedMethods.getSymbolType(stock.getInstrumentType()) == Meta.SYMBOL_TYPE_FOREX)) {
                mnu_DetailQuoteForex(key, symbolRegistered);
            } else if ((SharedMethods.getSymbolType(stock.getInstrumentType()) == Meta.INSTRUMENT_BOND)) {
                mnuDetailQuoteBond(key, symbolRegistered, false, false, LinkStore.LINK_NONE);
            } else {
                mnuDetailQuoteSymbol(key, symbolRegistered, fromWsp, isLinked, group);
            }
        }
        stock = null;
    }

    public void mnuPortfolioDQ(int selectedRow, TradePortfolios datastore, String key) {

        try {
            String symbol = SharedMethods.getSymbolFromKey(key);
            boolean loadedFromWorkspace = false;
            InternalFrame oFrame = g_oViewSettings.getWindow("" + ViewSettingsManager.DETAIL_QUOTE + " | " + symbol);
            if (oFrame != null) {
                oFrame.show();
                try {
                    oFrame.setSelected(true);
                } catch (Exception x) {
                }

                if (oFrame.isIcon())
                    oFrame.setIcon(false);
                return;
            }
            ViewSetting oViewSetting = null;

            Table table = new Table();
            table.hideHeader();
            table.getPopup().hideCopywHeadMenu();
            table.setSortingDisabled();
            table.setDQTableType();
            table.hideCustomizer();
            table.getPopup().showSetDefaultMenu();
            table.setWindowType(ViewSettingsManager.DETAIL_QUOTE);

            oViewSetting = g_oViewSettings.getDTQView("" + ViewSettingsManager.DETAIL_QUOTE + " | " + symbol);
            int fontSize = 0;
            if (oViewSetting == null) {
                oViewSetting = g_oViewSettings.getSymbolView("DETAIL_QUOTE");
                oViewSetting = oViewSetting.getObject(); // clone the object
                //oViewSetting.setID(sSelectedSymbol);
                GUISettings.setColumnSettings(oViewSetting, TWColumnSettings.getItem("PORTFOLIO_DETAIL_QUOTE_COLS"));
                g_oViewSettings.putDTQView("" + ViewSettingsManager.DETAIL_QUOTE + " | " + symbol, oViewSetting);
                //todo added by Dilum
                SharedMethods.applyCustomViewSetting(oViewSetting);
                DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oViewSetting.getType(), oViewSetting);
                fontSize = oViewSetting.getFont().getSize();
            } else {
                loadedFromWorkspace = true;
            }

            PortfolioDetailQuoteModel dqModel = new PortfolioDetailQuoteModel(selectedRow, datastore);
            //dqModel.setSymbol(sSelectedSymbol);
            dqModel.setViewSettings(oViewSetting);
            table.setModel(dqModel);
            dqModel.setTable(table);
            table.setUseSameFontForHeader(true);
            table.getTable().getTableHeader().setReorderingAllowed(false);
            dqModel.applyColumnSettings();
            /*int decimalPlaces;
            try {
                String currencyCode = TradingShared.getTrader().findAccountByPortfolio(((ValuationRecord)datastore.getValuationList().get(selectedRow)).getPortfolioID()).getCurrency();
                decimalPlaces = CurrencyStore.getSharedInstance().getDecimalPlaces(currencyCode);
            } catch (Exception e) {
                e.printStackTrace();
                decimalPlaces = 2;
            }*/

            dqModel.setDecimalCount(SharedMethods.getDecimalPlaces(key));

            table.updateGUI();
            table.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            table.getScrollPane().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
            table.getScrollPane().setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);
            final ViewSetting oViewSettingRef = oViewSetting;
            final Table oDetailQuoteRef = table;
            if (fontSize == 0) {
                fontSize = 18;
            }
            final int defaultsize = fontSize;
            InternalFrame frame = new InternalFrame(table, g_oWindowDaemon) {
                public Table tabRef2 = oDetailQuoteRef;
                ViewSetting viewRef2 = oViewSettingRef;
                Dimension dim;
//                 int defaultsize = 18;

                public void componentResized(ComponentEvent e) {
                    if (this.getOrigDimension().height > 0 && this.getOrigDimension().width > 0) {
                        if (!tabRef2.isFontResized()) {
                            dim = reSizeSpecial(this.getSize(), this.getOldSize());
                            if (!this.getSize().equals(dim)) {
                                resizeRows(dim, this.getOrigDimension().height);
                            }
                            this.setSize(dim);
                        } else {
                            Dimension olddim = new Dimension(this.getOldSize().width, this.getSize().height);
                            dim = resizeToFont(this.getSize(), olddim);
                            this.setSize(dim);
                            tabRef2.setFontResized(false);
                        }
                    }
                    if (dim == null) {
                        System.out.println("Dim null default size activated");
                        Dimension dim2 = viewRef2.getSize();
                        this.setSize(dim2);
                        this.setisLoadedFromWsp(true);
                        this.setOldSize(dim2);
                        this.setOrigDimention(new Dimension(620, 489));
                        resizeRows(dim2, this.getOrigDimension().height);
                    }
                    tabRef2.getSmartTable().adjustColumnWidthsToFit(10);
                }

                public void resizeRows(Dimension dim, int origHeight) {
                    Font oFont = viewRef2.getFont();

                    if (oFont == null) {
                        oFont = tabRef2.getTable().getFont();
                    }
//                    int defaultsize = oFont.getSize();

                    int titlteHeight = this.getTitlebarHeight();
                    float ratio = (((float) defaultsize) / origHeight);

                    int newSize = (int) (ratio * dim.height);


                    if (oFont != null) {

                        Font oFont2 = new TWFont(oFont.getFamily(), oFont.getStyle(), newSize);

                        tabRef2.getTable().setFont(oFont2);
                        FontMetrics oMetrices = tabRef2.getFontMetrics(oFont2);
                        int rowCount = tabRef2.getTable().getRowCount();
                        int emptySpace = ((dim.height - titlteHeight) - 2 - ((oMetrices.getMaxAscent() + oMetrices.getMaxDescent()) * rowCount));
                        int newGap = (int) Math.floor((emptySpace / rowCount));

                        tabRef2.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap);
                        int finspace = (dim.height - titlteHeight) - 4 - (oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap) * rowCount;
                        if (finspace > 0) {
                            int hight = this.dim.height - finspace;
                            int width = this.dim.width;
                            this.dim.setSize(width, hight);
                            this.setOldSize(this.dim);
                        }
                        oMetrices = null;
                        oFont = null;
                        oFont2 = null;

                    }
                }
            };

//            InternalFrame frame = new InternalFrame(table, g_oWindowDaemon);
            oViewSetting.setParent(frame);
            frame.setColumnResizeable(true);
            frame.setDetachable(true);
            frame.setPrintable(true);
//            frame.setSize(oViewSetting.getSize());
            frame.setLocation(oViewSetting.getLocation());
            frame.getContentPane().add(table);
            frame.setResizable(true);
            frame.setClosable(true);
            frame.setMaximizable(false);
            frame.setIconifiable(true);
            frame.addComponentListener(frame);
            oTopDesktop.add(frame);

            String str = Language.getString("PORTFOLIO_DETAIL_QUOTE");
            str = str.replaceAll("\\[SYMBOL\\]", symbol);
            frame.setTitle(str);
            frame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            frame.updateUI();
            frame.applySettings();
            //oDetailQuote.getTable().setShowVerticalLines(true);

            dqModel.updateGUI();
            frame.setLayer(GUISettings.TOP_LAYER);
            frame.setOrientation();
            g_oViewSettings.setWindow("" + ViewSettingsManager.DETAIL_QUOTE + " | " + symbol, frame);

            ((SmartTable) table.getTable()).adjustColumnWidthsToFit(100);
            frame.updateUI();
            table.packFrame();

            if (!loadedFromWorkspace) {
//                frame.setLocationRelativeTo(oTopDesktop);
//                oViewSetting.setLocation(frame.getLocation());
                if (!oViewSetting.isLocationValid()) {
                    frame.setLocationRelativeTo(oTopDesktop);
                    oViewSetting.setLocation(frame.getLocation());
                } else {
                    frame.setLocation(oViewSetting.getLocation());
                }
            }
            frame.setOrigDimention(frame.getSize());
            frame.show();
            frame.setisLoadedFromWsp(false);
            frame = null;
        } catch (PropertyVetoException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public void mnuDetailQuoteSymbol(String sSymbol) {
        mnuDetailQuoteSymbol(sSymbol, false, false, false, LinkStore.LINK_NONE);
    }


    public void mnuDetailQuoteSymbol(String key, boolean symbolRegistered, boolean frommWSP, boolean isLinked, String group) {
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.DetailQuote, "Equity");
        try {
            String selectedKey = null;
            boolean loadedFromWorkspace = false;

            if (key == null) {
                selectedKey = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(key))
                    selectedKey = key;
                else
                    selectedKey = DataStore.getSharedInstance().findKey(key);
            }

            if (selectedKey.equals(Constants.NULL_STRING))
                return;

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            if (!isLinked) {
                InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.DETAIL_QUOTE
                        + "|" + selectedKey);
                if (oFrame != null) {
                    oFrame.show();
                    try {
                        oFrame.setSelected(true);
                    } catch (Exception x) {
                    }

                    if (oFrame.isIcon())
                        oFrame.setIcon(false);
                    return;
                }
            }
            ViewSetting oViewSetting = null;
            int fontsize = 0;
            Table table = new Table();
            DetailQuoteFrame dqframe = new DetailQuoteFrame(table, g_oWindowDaemon, selectedKey, frommWSP, symbolRegistered, isLinked, group);
            if (dqframe != null) {
                dqframe.checkForDetach();
                return;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mnuDetailQuoteBond(String sSymbol) {
        mnuDetailQuoteBond(sSymbol, false, false, false, LinkStore.LINK_NONE);
    }


    public void mnuDetailQuoteBond(String key, boolean symbolRegistered, boolean frommWSP, boolean isLinked, String group) {
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.DetailQuote, "Bond");
        try {
            String selectedKey = null;
            boolean loadedFromWorkspace = false;

            if (key == null) {
                selectedKey = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(key))
                    selectedKey = key;
                else
                    selectedKey = DataStore.getSharedInstance().findKey(key);
            }

            if (selectedKey.equals(Constants.NULL_STRING))
                return;

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            if (!isLinked) {
                InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.DETAIL_QUOTE
                        + "|" + selectedKey);
                if (oFrame != null) {
                    oFrame.show();
                    try {
                        oFrame.setSelected(true);
                    } catch (Exception x) {
                    }

                    if (oFrame.isIcon())
                        oFrame.setIcon(false);
                    return;
                }
            }
            ViewSetting oViewSetting = null;
            int fontsize = 0;
            Table table = new Table();
            DetailQuoteFrameBonds dqframe = new DetailQuoteFrameBonds(table, g_oWindowDaemon, selectedKey, frommWSP, symbolRegistered, isLinked, group);
            if (dqframe != null) {
                return;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mnu_DetailQuoteMutualFund(String sSymbol, boolean symbolRegistered) {
        try {
            String sSelectedSymbol = null;
            boolean loadedFromWorkspace = false;

            if (sSymbol == null) {
                sSelectedSymbol = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(sSymbol))
                    sSelectedSymbol = sSymbol;
                else
                    sSelectedSymbol = DataStore.getSharedInstance().findKey(sSymbol);
            }

            if (sSelectedSymbol.equals(Constants.NULL_STRING))
                return;

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.DETAIL_QUOTE
                    + "|" + sSelectedSymbol);
            if (oFrame != null) {
                oFrame.show();
                try {
                    oFrame.setSelected(true);
                } catch (Exception x) {
                }

                if (oFrame.isIcon())
                    oFrame.setIcon(false);
                return;
            }
            ViewSetting oViewSetting = null;

            Table oDetailQuote = new Table();
            oDetailQuote.hideHeader();
            oDetailQuote.getPopup().hideCopywHeadMenu();
            oDetailQuote.setSortingDisabled();
            oDetailQuote.setDQTableType();
            oDetailQuote.hideCustomizer();
            oDetailQuote.getPopup().showSetDefaultMenu();
            oDetailQuote.getPopup().showCustomizerMenu();
            oDetailQuote.setWindowType(ViewSettingsManager.DETAIL_QUOTE);

            oViewSetting = g_oViewSettings.getDTQView(ViewSettingsManager.DETAIL_QUOTE
                    + "|" + sSelectedSymbol);
            int fontSize = 0;
            if (oViewSetting == null) {
                oViewSetting = g_oViewSettings.getSymbolView("DETAIL_QUOTE");
                oViewSetting = oViewSetting.getObject(); // clone the object
                oViewSetting.setID(sSelectedSymbol);
                g_oViewSettings.putDTQView(ViewSettingsManager.DETAIL_QUOTE
                        + "|" + sSelectedSymbol, oViewSetting);
                //todo added by Dilum
                SharedMethods.applyCustomViewSetting(oViewSetting);
                DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oViewSetting.getType(), oViewSetting);
                fontSize = oViewSetting.getFont().getSize();
            } else {
                loadedFromWorkspace = true;
            }

            MutualFundDetailQuoteModel dqModel = new MutualFundDetailQuoteModel();
            dqModel.setDecimalCount(SharedMethods.getDecimalPlaces(sSelectedSymbol));
            dqModel.setSymbol(sSelectedSymbol);
            dqModel.setViewSettings(oViewSetting);
            oDetailQuote.setModel(dqModel);
            dqModel.setTable(oDetailQuote);
            oDetailQuote.setUseSameFontForHeader(true);
            oDetailQuote.getTable().getTableHeader().setReorderingAllowed(false);
            dqModel.applyColumnSettings();

            oDetailQuote.updateGUI();

            oDetailQuote.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            oDetailQuote.getScrollPane().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
            oDetailQuote.getScrollPane().setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

            final ViewSetting oViewSettingRef = oViewSetting;
            final Table oDetailQuoteRef = oDetailQuote;
            if (fontSize == 0) {
                fontSize = 18;
            }
            final int defaultsize = fontSize;
            InternalFrame frame = new InternalFrame(oDetailQuote, g_oWindowDaemon) {
                public Table tabRef2 = oDetailQuoteRef;
                ViewSetting viewRef2 = oViewSettingRef;
                Dimension dim;
//                int defaultsize = 18;

                public void componentResized(ComponentEvent e) {
                    if (this.getOrigDimension().height > 0 && this.getOrigDimension().width > 0) {
                        if (!tabRef2.isFontResized()) {
                            dim = reSizeSpecial(this.getSize(), this.getOldSize());
                            if (!this.getSize().equals(dim)) {
                                resizeRows(dim, this.getOrigDimension().height);
                            }
                            this.setSize(dim);
                        } else {
                            Dimension olddim = new Dimension(this.getOldSize().width, this.getSize().height);
                            dim = resizeToFont(this.getSize(), olddim);
                            this.setSize(dim);
                            tabRef2.setFontResized(false);
                        }
                    }
                    if (dim == null) {
                        System.out.println("Dim null default size activated");
                        Dimension dim2 = viewRef2.getSize();
                        this.setSize(dim2);
                        this.setisLoadedFromWsp(true);
                        this.setOldSize(dim2);
                        this.setOrigDimention(new Dimension(620, 489));
                        resizeRows(dim2, this.getOrigDimension().height);
                    }
                    tabRef2.getSmartTable().adjustColumnWidthsToFit(10);
                }

                public void resizeRows(Dimension dim, int origHeight) {
                    Font oFont = viewRef2.getFont();
                    if (oFont == null) {
                        oFont = tabRef2.getTable().getFont();
                    }
//                    int defaultsize = oFont.getSize();
                    int titlteHeight = this.getTitlebarHeight();
                    float ratio = (((float) defaultsize) / origHeight);
                    int newSize = (int) (ratio * dim.height);
                    if (oFont != null) {
                        Font oFont2 = new TWFont(oFont.getFamily(), oFont.getStyle(), newSize);
                        tabRef2.getTable().setFont(oFont2);
                        FontMetrics oMetrices = tabRef2.getFontMetrics(oFont2);
                        int rowCount = tabRef2.getTable().getRowCount();
                        int emptySpace = ((dim.height - titlteHeight) - 2 - ((oMetrices.getMaxAscent() + oMetrices.getMaxDescent()) * rowCount));
                        int newGap = (int) Math.floor((emptySpace / rowCount));
                        tabRef2.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap);
                        int finspace = (dim.height - titlteHeight) - 4 - (oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap) * rowCount;
                        if (finspace > 0) {
                            int hight = this.dim.height - finspace;
                            int width = this.dim.width;
                            this.dim.setSize(width, hight);
                            this.setOldSize(this.dim);
                        }
                        oMetrices = null;
                        oFont = null;
                        oFont2 = null;
                    }
                }
            };
//            InternalFrame frame = new InternalFrame(oDetailQuote, g_oWindowDaemon);
            oViewSetting.setParent(frame);
            frame.setShowServicesMenu(false);
            frame.setDetachable(true);
            frame.setPrintable(true);
//            frame.setSize(oViewSetting.getSize());
            frame.setLocation(oViewSetting.getLocation());
            frame.getContentPane().add(oDetailQuote);
            frame.setResizable(true);
            frame.setClosable(true);
            frame.setMaximizable(false);
            frame.setIconifiable(true);
            frame.addComponentListener(frame);
            oTopDesktop.add(frame);
            frame.setTitle(Language.getString("DETAIL_QUOTE") + " : " +
                    DataStore.getSharedInstance().getStockObject(sSelectedSymbol).getLongDescription() + "" +
                    " (" + sSelectedSymbol + ") ");
            frame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            frame.updateUI();
            frame.applySettings();

            if (!symbolRegistered) {
                DataStore.getSharedInstance().addSymbolRequest(sSelectedSymbol);
            } else if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                DataStore.getSharedInstance().addSymbolRequest(sSelectedSymbol);
            }

            dqModel.updateGUI();


            frame.show();
            frame.setLayer(GUISettings.TOP_LAYER);
            frame.setOrientation();
            g_oViewSettings.setWindow(ViewSettingsManager.DETAIL_QUOTE + "|" +
                    sSelectedSymbol, frame);

            ((SmartTable) oDetailQuote.getTable()).adjustColumnWidthsToFit(40);
            frame.updateUI();
            oDetailQuote.packFrame();


            if (!loadedFromWorkspace) {
//                frame.setLocationRelativeTo(oTopDesktop);
//                oViewSetting.setLocation(frame.getLocation());
                if (!oViewSetting.isLocationValid()) {
                    frame.setLocationRelativeTo(oTopDesktop);
                    oViewSetting.setLocation(frame.getLocation());
                } else {
                    frame.setLocation(oViewSetting.getLocation());
                }
            }
            frame.setOrigDimention(frame.getSize());
            frame.setisLoadedFromWsp(false);
            frame = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mnu_DetailQuoteOptions(String sSymbol, boolean symbolRegistered) {
        try {
            String sSelectedSymbol = null;
            boolean loadedFromWorkspace = false;

            if (sSymbol == null) {
                sSelectedSymbol = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(sSymbol))
                    sSelectedSymbol = sSymbol;
                else
                    sSelectedSymbol = DataStore.getSharedInstance().findKey(sSymbol);
            }

            if (sSelectedSymbol.equals(Constants.NULL_STRING))
                return;

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.DETAIL_QUOTE
                    + "|" + sSelectedSymbol);
            if (oFrame != null) {
                oFrame.show();
                try {
                    oFrame.setSelected(true);
                } catch (Exception x) {
                }

                if (oFrame.isIcon())
                    oFrame.setIcon(false);
                return;
            }
            ViewSetting oViewSetting = null;

            Table oDetailQuote = new Table();
            oDetailQuote.hideHeader();
            oDetailQuote.getPopup().hideCopywHeadMenu();
            oDetailQuote.setSortingDisabled();
            oDetailQuote.setDQTableType();
            oDetailQuote.hideCustomizer();
            oDetailQuote.getPopup().showSetDefaultMenu();
            oDetailQuote.getPopup().showCustomizerMenu();
            oDetailQuote.setWindowType(ViewSettingsManager.DETAIL_QUOTE);

            oViewSetting = g_oViewSettings.getDTQView(ViewSettingsManager.DETAIL_QUOTE
                    + "|" + sSelectedSymbol);
            int fontSize = 0;
            if (oViewSetting == null) {
                oViewSetting = g_oViewSettings.getSymbolView("DETAIL_QUOTE");
                oViewSetting = oViewSetting.getObject(); // clone the object
                oViewSetting.setID(sSelectedSymbol);
                g_oViewSettings.putDTQView(ViewSettingsManager.DETAIL_QUOTE
                        + "|" + sSelectedSymbol, oViewSetting);
                //todo added by Dilum
                SharedMethods.applyCustomViewSetting(oViewSetting);
                DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oViewSetting.getType(), oViewSetting);
                fontSize = oViewSetting.getFont().getSize();
            } else {
                loadedFromWorkspace = true;
            }

            OptionDetailQuoteModel dqModel = new OptionDetailQuoteModel();
            dqModel.setDecimalCount(SharedMethods.getDecimalPlaces(sSelectedSymbol));
            dqModel.setSymbol(sSelectedSymbol);
            dqModel.setViewSettings(oViewSetting);
            oDetailQuote.setModel(dqModel);
            dqModel.setTable(oDetailQuote);
            oDetailQuote.setUseSameFontForHeader(true);
            oDetailQuote.getTable().getTableHeader().setReorderingAllowed(false);
            dqModel.applyColumnSettings();

            oDetailQuote.updateGUI();

            oDetailQuote.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            oDetailQuote.getScrollPane().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
            oDetailQuote.getScrollPane().setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

            final ViewSetting oViewSettingRef = oViewSetting;
            final Table oDetailQuoteRef = oDetailQuote;
            if (fontSize == 0) {
                fontSize = 18;
            }
            final int defaultsize = fontSize;
            InternalFrame frame = new InternalFrame(oDetailQuote, g_oWindowDaemon) {
                public Table tabRef2 = oDetailQuoteRef;
                ViewSetting viewRef2 = oViewSettingRef;
                Dimension dim;
//                int defaultsize = 18;

                public void componentResized(ComponentEvent e) {
                    if (this.getOrigDimension().height > 0 && this.getOrigDimension().width > 0) {
                        if (!tabRef2.isFontResized()) {
                            dim = reSizeSpecial(this.getSize(), this.getOldSize());
                            if (!this.getSize().equals(dim)) {
                                resizeRows(dim, this.getOrigDimension().height);
                            }
                            this.setSize(dim);
                        } else {
                            Dimension olddim = new Dimension(this.getOldSize().width, this.getSize().height);
                            dim = resizeToFont(this.getSize(), olddim);
                            this.setSize(dim);
                            tabRef2.setFontResized(false);
                        }
                    }
                    if (dim == null) {
                        System.out.println("Dim null default size activated");
                        Dimension dim2 = viewRef2.getSize();
                        this.setSize(dim2);
                        this.setisLoadedFromWsp(true);
                        this.setOldSize(dim2);
                        this.setOrigDimention(new Dimension(620, 489));
                        resizeRows(dim2, this.getOrigDimension().height);
                    }
                    tabRef2.getSmartTable().adjustColumnWidthsToFit(10);
                }

                public void resizeRows(Dimension dim, int origHeight) {
                    Font oFont = viewRef2.getFont();
                    if (oFont == null) {
                        oFont = tabRef2.getTable().getFont();
                    }
//                    int defaultsize = oFont.getSize();
                    int titlteHeight = this.getTitlebarHeight();
                    float ratio = (((float) defaultsize) / origHeight);
                    int newSize = (int) (ratio * dim.height);
                    if (oFont != null) {
                        Font oFont2 = new TWFont(oFont.getFamily(), oFont.getStyle(), newSize);
                        tabRef2.getTable().setFont(oFont2);
                        FontMetrics oMetrices = tabRef2.getFontMetrics(oFont2);
                        int rowCount = tabRef2.getTable().getRowCount();
                        int emptySpace = ((dim.height - titlteHeight) - 2 - ((oMetrices.getMaxAscent() + oMetrices.getMaxDescent()) * rowCount));
                        int newGap = (int) Math.floor((emptySpace / rowCount));
                        tabRef2.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap);
                        int finspace = (dim.height - titlteHeight) - 4 - (oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap) * rowCount;
                        if (finspace > 0) {
                            int hight = this.dim.height - finspace;
                            int width = this.dim.width;
                            this.dim.setSize(width, hight);
                            this.setOldSize(this.dim);
                        }
                        oMetrices = null;
                        oFont = null;
                        oFont2 = null;
                    }
                }
            };
//            InternalFrame frame = new InternalFrame(oDetailQuote, g_oWindowDaemon);
            oViewSetting.setParent(frame);
            frame.setShowServicesMenu(false);
            frame.setDetachable(true);
            frame.setPrintable(true);
//            frame.setSize(oViewSetting.getSize());
            frame.setLocation(oViewSetting.getLocation());
            frame.getContentPane().add(oDetailQuote);
            frame.setResizable(true);
            frame.setClosable(true);
            frame.setMaximizable(false);
            frame.setIconifiable(true);
            frame.addComponentListener(frame);
            oTopDesktop.add(frame);
            String LongD = DataStore.getSharedInstance().getStockObject(sSelectedSymbol).getLongDescription();

            if (LongD != null && !LongD.isEmpty() && !LongD.equals(Constants.DEFAULT_STRING)) {
                frame.setTitle(Language.getString("DETAIL_QUOTE") + " : " +
                        DataStore.getSharedInstance().getStockObject(sSelectedSymbol).getLongDescription() + "" +
                        " (" + SharedMethods.getDisplayKey(sSelectedSymbol) + ") ");
            } else {
                frame.setTitle(Language.getString("DETAIL_QUOTE") + " : " +
                        "" +
                        " (" + SharedMethods.getDisplayKey(sSelectedSymbol) + ") ");
            }
            frame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            frame.updateUI();
            frame.applySettings();

            if (!symbolRegistered) {
                DataStore.getSharedInstance().addSymbolRequest(sSelectedSymbol);
            } else if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                DataStore.getSharedInstance().addSymbolRequest(sSelectedSymbol);
            }

            dqModel.updateGUI();


            frame.show();
            frame.setLayer(GUISettings.TOP_LAYER);
            frame.setOrientation();
            g_oViewSettings.setWindow(ViewSettingsManager.DETAIL_QUOTE + "|" +
                    sSelectedSymbol, frame);

            ((SmartTable) oDetailQuote.getTable()).adjustColumnWidthsToFit(40);
            frame.updateUI();
            oDetailQuote.packFrame();


            if (!loadedFromWorkspace) {
//                frame.setLocationRelativeTo(oTopDesktop);
//                oViewSetting.setLocation(frame.getLocation());
                if (!oViewSetting.isLocationValid()) {
                    frame.setLocationRelativeTo(oTopDesktop);
                    oViewSetting.setLocation(frame.getLocation());
                } else {
                    frame.setLocation(oViewSetting.getLocation());
                }
            }
            frame.setOrigDimention(frame.getSize());
            frame.setisLoadedFromWsp(false);
            frame = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mnu_DetailQuoteForex(String sSymbol, boolean symbolRegistered) {
        try {
            String sSelectedSymbol = null;
            boolean loadedFromWorkspace = false;

            if (sSymbol == null) {
                sSelectedSymbol = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(sSymbol))
                    sSelectedSymbol = sSymbol;
                else
                    sSelectedSymbol = DataStore.getSharedInstance().findKey(sSymbol);
            }

            if (sSelectedSymbol.equals(Constants.NULL_STRING))
                return;

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.DETAIL_QUOTE_FOREX
                    + "|" + sSelectedSymbol);
            if (oFrame != null) {
                oFrame.show();
                try {
                    oFrame.setSelected(true);
                } catch (Exception x) {
                }

                if (oFrame.isIcon())
                    oFrame.setIcon(false);
                return;
            }
            ViewSetting oViewSetting = null;

            Table oDetailQuote = new Table();
            oDetailQuote.hideHeader();
            oDetailQuote.getPopup().hideCopywHeadMenu();
            oDetailQuote.setSortingDisabled();
            oDetailQuote.setDQTableType();
            oDetailQuote.hideCustomizer();
            oDetailQuote.getPopup().showSetDefaultMenu();
            oDetailQuote.getPopup().showCustomizerMenu();
            oDetailQuote.setWindowType(ViewSettingsManager.DETAIL_QUOTE_FOREX);

            oViewSetting = g_oViewSettings.getDTQView(ViewSettingsManager.DETAIL_QUOTE_FOREX
                    + "|" + sSelectedSymbol);
            int fontSize = 0;
            if (oViewSetting == null) {
                oViewSetting = g_oViewSettings.getSymbolView("DETAIL_QUOTE_FOREX");
                oViewSetting = oViewSetting.getObject(); // clone the object
                oViewSetting.setID(sSelectedSymbol);
                g_oViewSettings.putDTQView(ViewSettingsManager.DETAIL_QUOTE_FOREX
                        + "|" + sSelectedSymbol, oViewSetting);
                //todo added by Dilum
                SharedMethods.applyCustomViewSetting(oViewSetting);
                DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oViewSetting.getType(), oViewSetting);
                fontSize = oViewSetting.getFont().getSize();
            } else {
                loadedFromWorkspace = true;
            }

            ForexDetailQuoteModel dqModel = new ForexDetailQuoteModel();
            dqModel.setDecimalCount(SharedMethods.getDecimalPlaces(sSelectedSymbol));
            dqModel.setSymbol(sSelectedSymbol);
            dqModel.setViewSettings(oViewSetting);
            oDetailQuote.setModel(dqModel);
            dqModel.setTable(oDetailQuote);
            oDetailQuote.setUseSameFontForHeader(true);
            oDetailQuote.getTable().getTableHeader().setReorderingAllowed(false);
            dqModel.applyColumnSettings();

            oDetailQuote.updateGUI();

            oDetailQuote.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            oDetailQuote.getScrollPane().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
            oDetailQuote.getScrollPane().setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

            final ViewSetting oViewSettingRef = oViewSetting;
            final Table oDetailQuoteRef = oDetailQuote;
            if (fontSize == 0) {
                fontSize = 18;
            }
            final int defaultsize = fontSize;
            InternalFrame frame = new InternalFrame(oDetailQuote, g_oWindowDaemon) {
                public Table tabRef2 = oDetailQuoteRef;
                ViewSetting viewRef2 = oViewSettingRef;
                Dimension dim;
//                int defaultsize = 18;

                public void componentResized(ComponentEvent e) {
                    if (this.getOrigDimension().height > 0 && this.getOrigDimension().width > 0) {
                        if (!tabRef2.isFontResized()) {
                            dim = reSizeSpecial(this.getSize(), this.getOldSize());
                            if (!this.getSize().equals(dim)) {
                                resizeRows(dim, this.getOrigDimension().height);
                            }
                            this.setSize(dim);
                        } else {
                            Dimension olddim = new Dimension(this.getOldSize().width, this.getSize().height);
                            dim = resizeToFont(this.getSize(), olddim);
                            this.setSize(dim);
                            tabRef2.setFontResized(false);
                        }
                    }
                    if (dim == null) {
                        System.out.println("Dim null default size activated");
                        Dimension dim2 = viewRef2.getSize();
                        this.setSize(dim2);
                        this.setisLoadedFromWsp(true);
                        this.setOldSize(dim2);
                        this.setOrigDimention(new Dimension(620, 489));
                        resizeRows(dim2, this.getOrigDimension().height);
                    }
                    tabRef2.getSmartTable().adjustColumnWidthsToFit(10);
                }

                public void resizeRows(Dimension dim, int origHeight) {
                    Font oFont = viewRef2.getFont();
                    if (oFont == null) {
                        oFont = tabRef2.getTable().getFont();
                    }
//                    int defaultsize = oFont.getSize();
                    int titlteHeight = this.getTitlebarHeight();
                    float ratio = (((float) defaultsize) / origHeight);
                    int newSize = (int) (ratio * dim.height);
                    if (oFont != null) {
                        Font oFont2 = new TWFont(oFont.getFamily(), oFont.getStyle(), newSize);
                        tabRef2.getTable().setFont(oFont2);
                        FontMetrics oMetrices = tabRef2.getFontMetrics(oFont2);
                        int rowCount = tabRef2.getTable().getRowCount();
                        int emptySpace = ((dim.height - titlteHeight) - 2 - ((oMetrices.getMaxAscent() + oMetrices.getMaxDescent()) * rowCount));
                        int newGap = (int) Math.floor((emptySpace / rowCount));
                        tabRef2.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap);
                        int finspace = (dim.height - titlteHeight) - 4 - (oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap) * rowCount;
                        if (finspace > 0) {
                            int hight = this.dim.height - finspace;
                            int width = this.dim.width;
                            this.dim.setSize(width, hight);
                            this.setOldSize(this.dim);
                        }
                        oMetrices = null;
                        oFont = null;
                        oFont2 = null;
                    }
                }
            };
//            InternalFrame frame = new InternalFrame(oDetailQuote, g_oWindowDaemon);
            oViewSetting.setParent(frame);
            frame.setShowServicesMenu(false);
            frame.setDetachable(true);
            frame.setPrintable(true);
//            frame.setSize(oViewSetting.getSize());
            frame.setLocation(oViewSetting.getLocation());
            frame.getContentPane().add(oDetailQuote);
            frame.setResizable(true);
            frame.setClosable(true);
            frame.setMaximizable(false);
            frame.setIconifiable(true);
            frame.addComponentListener(frame);
            oTopDesktop.add(frame);
            frame.setTitle(Language.getString("DETAIL_QUOTE") + " : " +
                    DataStore.getSharedInstance().getStockObject(sSelectedSymbol).getLongDescription() + "" +
                    " (" + sSelectedSymbol + ") ");
            frame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            frame.updateUI();
            frame.applySettings();

            if (!symbolRegistered) {
                DataStore.getSharedInstance().addSymbolRequest(sSelectedSymbol);
            } else if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                DataStore.getSharedInstance().addSymbolRequest(sSelectedSymbol);
            }

            dqModel.updateGUI();


            frame.show();
            frame.setLayer(GUISettings.TOP_LAYER);
            frame.setOrientation();
            g_oViewSettings.setWindow(ViewSettingsManager.DETAIL_QUOTE_FOREX + "|" +
                    sSelectedSymbol, frame);

            ((SmartTable) oDetailQuote.getTable()).adjustColumnWidthsToFit(40);
            frame.updateUI();
            oDetailQuote.packFrame();


            if (!loadedFromWorkspace) {
//                frame.setLocationRelativeTo(oTopDesktop);
//                oViewSetting.setLocation(frame.getLocation());
                if (!oViewSetting.isLocationValid()) {
                    frame.setLocationRelativeTo(oTopDesktop);
                    oViewSetting.setLocation(frame.getLocation());
                } else {
                    frame.setLocation(oViewSetting.getLocation());
                }
            }
            frame.setOrigDimention(frame.getSize());
            frame.setisLoadedFromWsp(false);
            frame = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mnu_DetailQuoteIndex(String sSymbol, boolean symbolRegistered) {
        try {
            String sSelectedSymbol = null;
            boolean loadedFromWorkspace = false;

            if (sSymbol == null) {
                sSelectedSymbol = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(sSymbol))
                    sSelectedSymbol = sSymbol;
                else
                    sSelectedSymbol = DataStore.getSharedInstance().findKey(sSymbol);
            }

            if (sSelectedSymbol.equals(Constants.NULL_STRING))
                return;

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.DETAIL_QUOTE
                    + "|" + sSelectedSymbol);
            if (oFrame != null) {
                oFrame.show();
                try {
                    oFrame.setSelected(true);
                } catch (Exception x) {
                }

                if (oFrame.isIcon())
                    oFrame.setIcon(false);
                return;
            }
            ViewSetting oViewSetting = null;

            Table oDetailQuote = new Table();
            oDetailQuote.hideHeader();
            oDetailQuote.getPopup().hideCopywHeadMenu();
            oDetailQuote.setSortingDisabled();
            oDetailQuote.setDQTableType();
            oDetailQuote.hideCustomizer();
            oDetailQuote.getPopup().showSetDefaultMenu();
            oDetailQuote.getPopup().showCustomizerMenu();
            oDetailQuote.setWindowType(ViewSettingsManager.DETAIL_QUOTE);

            oViewSetting = g_oViewSettings.getDTQView(ViewSettingsManager.DETAIL_QUOTE
                    + "|" + sSelectedSymbol);
            int fontSize = 0;
            if (oViewSetting == null) {
                oViewSetting = g_oViewSettings.getSymbolView("DETAIL_QUOTE");
                oViewSetting = oViewSetting.getObject(); // clone the object
                oViewSetting.setID(sSelectedSymbol);
                g_oViewSettings.putDTQView(ViewSettingsManager.DETAIL_QUOTE
                        + "|" + sSelectedSymbol, oViewSetting);
                //todo added by Dilum
                SharedMethods.applyCustomViewSetting(oViewSetting);
                DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oViewSetting.getType(), oViewSetting);
                fontSize = oViewSetting.getFont().getSize();
            } else {
                loadedFromWorkspace = true;
            }

            IndexDetailQuoteModel dqModel = new IndexDetailQuoteModel();
            dqModel.setDecimalCount(SharedMethods.getDecimalPlaces(sSelectedSymbol));
            dqModel.setSymbol(sSelectedSymbol);
            dqModel.setViewSettings(oViewSetting);
            oDetailQuote.setModel(dqModel);
            dqModel.setTable(oDetailQuote);
            oDetailQuote.setUseSameFontForHeader(true);
            oDetailQuote.getTable().getTableHeader().setReorderingAllowed(false);
            dqModel.applyColumnSettings();

            oDetailQuote.updateGUI();

            oDetailQuote.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            oDetailQuote.getScrollPane().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
            oDetailQuote.getScrollPane().setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

            final ViewSetting oViewSettingRef = oViewSetting;
            final Table oDetailQuoteRef = oDetailQuote;
            if (fontSize == 0) {
                fontSize = 18;
            }
            final int defaultsize = fontSize;
            InternalFrame frame = new InternalFrame(oDetailQuote, g_oWindowDaemon) {
                public Table tabRef2 = oDetailQuoteRef;
                ViewSetting viewRef2 = oViewSettingRef;
                Dimension dim;
//                int defaultsize = 18;

                public void componentResized(ComponentEvent e) {
                    if (this.getOrigDimension().height > 0 && this.getOrigDimension().width > 0) {
                        if (!tabRef2.isFontResized()) {
                            dim = reSizeSpecial(this.getSize(), this.getOldSize());
                            if (!this.getSize().equals(dim)) {
                                resizeRows(dim, this.getOrigDimension().height);
                            }
                            this.setSize(dim);
                        } else {
                            Dimension olddim = new Dimension(this.getOldSize().width, this.getSize().height);
                            dim = resizeToFont(this.getSize(), olddim);
                            this.setSize(dim);
                            tabRef2.setFontResized(false);
                        }
                    }
                    if (dim == null) {
                        System.out.println("Dim null default size activated");
                        Dimension dim2 = viewRef2.getSize();
                        this.setSize(dim2);
                        this.setisLoadedFromWsp(true);
                        this.setOldSize(dim2);
                        this.setOrigDimention(new Dimension(620, 489));
                        resizeRows(dim2, this.getOrigDimension().height);
                    }
                    tabRef2.getSmartTable().adjustColumnWidthsToFit(10);
                }

                public void resizeRows(Dimension dim, int origHeight) {
                    Font oFont = viewRef2.getFont();
                    if (oFont == null) {
                        oFont = tabRef2.getTable().getFont();
                    }
//                    int defaultsize = oFont.getSize();
                    int titlteHeight = this.getTitlebarHeight();
                    float ratio = (((float) defaultsize) / origHeight);
                    int newSize = (int) (ratio * dim.height);
                    if (oFont != null) {
                        Font oFont2 = new TWFont(oFont.getFamily(), oFont.getStyle(), newSize);
                        tabRef2.getTable().setFont(oFont2);
                        FontMetrics oMetrices = tabRef2.getFontMetrics(oFont2);
                        int rowCount = tabRef2.getTable().getRowCount();
                        int emptySpace = ((dim.height - titlteHeight) - 2 - ((oMetrices.getMaxAscent() + oMetrices.getMaxDescent()) * rowCount));
                        int newGap = (int) Math.floor((emptySpace / rowCount));
                        tabRef2.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap);
                        int finspace = (dim.height - titlteHeight) - 4 - (oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap) * rowCount;
                        if (finspace > 0) {
                            int hight = this.dim.height - finspace;
                            int width = this.dim.width;
                            this.dim.setSize(width, hight);
                            this.setOldSize(this.dim);
                        }
                        oMetrices = null;
                        oFont = null;
                        oFont2 = null;
                    }
                }
            };
//            InternalFrame frame = new InternalFrame(oDetailQuote, g_oWindowDaemon);
            oViewSetting.setParent(frame);
            frame.setShowServicesMenu(false);
            frame.setDetachable(true);
            frame.setPrintable(true);
//            frame.setSize(oViewSetting.getSize());
            frame.setLocation(oViewSetting.getLocation());
            frame.getContentPane().add(oDetailQuote);
            frame.setResizable(true);
            frame.setClosable(true);
            frame.setMaximizable(false);
            frame.setIconifiable(true);
            frame.addComponentListener(frame);
            oTopDesktop.add(frame);
            frame.setTitle(Language.getString("DETAIL_QUOTE") + " : " +
                    DataStore.getSharedInstance().getStockObject(sSelectedSymbol).getLongDescription() + "" +
                    " (" + SharedMethods.getSymbolFromKey(sSelectedSymbol) + ") ");
            frame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            frame.updateUI();
            frame.applySettings();

            if (!symbolRegistered) {
                DataStore.getSharedInstance().addSymbolRequest(sSelectedSymbol);
            } else if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                DataStore.getSharedInstance().addSymbolRequest(sSelectedSymbol);
            }

            dqModel.updateGUI();


            frame.show();
            frame.setLayer(GUISettings.TOP_LAYER);
            frame.setOrientation();
            g_oViewSettings.setWindow(ViewSettingsManager.DETAIL_QUOTE + "|" +
                    sSelectedSymbol, frame);

            ((SmartTable) oDetailQuote.getTable()).adjustColumnWidthsToFit(40);
            frame.updateUI();
            oDetailQuote.packFrame();


            if (!loadedFromWorkspace) {
//                frame.setLocationRelativeTo(oTopDesktop);
//                oViewSetting.setLocation(frame.getLocation());
                if (!oViewSetting.isLocationValid()) {
                    frame.setLocationRelativeTo(oTopDesktop);
                    oViewSetting.setLocation(frame.getLocation());
                } else {
                    frame.setLocation(oViewSetting.getLocation());
                }
            }
            frame.setOrigDimention(frame.getSize());
            frame.setisLoadedFromWsp(false);
            frame = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public void mnu_DetailQuoteMFund(int index) {
        try {
            boolean loadedFromWorkspace = false;

*//* check if the window is already created
    If so, do not create it again, just focus it
*//*
            InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.DETAIL_QUOTE_MFUND
                    + "|" + index);
            if (oFrame != null) {
                oFrame.show();
                try {
                    oFrame.setSelected(true);
                } catch (Exception x) {
                }

                if (oFrame.isIcon())
                    oFrame.setIcon(false);
                return;
            }
            ViewSetting oViewSetting = null;

            Table oDetailQuote = new Table();
            oDetailQuote.hideHeader();
            oDetailQuote.getPopup().hideCopywHeadMenu();
            oDetailQuote.setSortingDisabled();
            oDetailQuote.setDQTableType();
            oDetailQuote.hideCustomizer();
            oDetailQuote.getPopup().showSetDefaultMenu();
            oDetailQuote.setWindowType(ViewSettingsManager.DETAIL_QUOTE_MFUND);

            oViewSetting = g_oViewSettings.getDTQView(ViewSettingsManager.DETAIL_QUOTE_MFUND
                    + "|" + index);
            int fontSize = 0;
            if (oViewSetting == null) {
                oViewSetting = g_oViewSettings.getSymbolView("DETAIL_QUOTE_MFUND");
                oViewSetting = oViewSetting.getObject(); // clone the object
                oViewSetting.setID("" + index);
                g_oViewSettings.putDTQView(ViewSettingsManager.DETAIL_QUOTE_MFUND
                        + "|" + index, oViewSetting);
                DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oViewSetting.getType(),oViewSetting);
                fontSize = oViewSetting.getFont().getSize();
            } else {
                loadedFromWorkspace = true;
            }

            MutualFundDetailQuoteModel dqModel = new MutualFundDetailQuoteModel();
            dqModel.setSymbol(index);
            dqModel.setViewSettings(oViewSetting);
            oDetailQuote.setModel(dqModel);
            dqModel.setTable(oDetailQuote);
            oDetailQuote.setUseSameFontForHeader(true);
            oDetailQuote.getTable().getTableHeader().setReorderingAllowed(false);
            dqModel.applyColumnSettings();

            oDetailQuote.updateGUI();
            oDetailQuote.getTable().setAutoResizeMode(JTable.AUTO_RESIZE_ALL_COLUMNS);
            oDetailQuote.getScrollPane().setVerticalScrollBarPolicy(JScrollPane.VERTICAL_SCROLLBAR_NEVER);
            oDetailQuote.getScrollPane().setHorizontalScrollBarPolicy(JScrollPane.HORIZONTAL_SCROLLBAR_NEVER);

            final ViewSetting oViewSettingRef = oViewSetting;
            final Table  oDetailQuoteRef = table;
            if(fontSize == 0){
                fontSize = 18;
            }
            final int defaultsize = fontSize;
            InternalFrame frame = new InternalFrame(oDetailQuote, g_oWindowDaemon){
                 public Table tabRef2 = oDetailQuoteRef;
                 ViewSetting viewRef2 = oViewSettingRef;
                 Dimension dim;
//                int defaultsize = 18;

                 public void componentResized(ComponentEvent e) {
                    if(this.getOrigDimension().height >0 && this.getOrigDimension().width>0){
                        if(!tabRef2.isFontResized()){
                            dim= reSizeSpecial(this.getSize(),this.getOldSize());
                            if(!this.getSize().equals(dim)){
                                resizeRows(dim,this.getOrigDimension().height);
                            }
                            this.setSize(dim);
                       }else{
                           Dimension olddim = new Dimension(this.getOldSize().width,this.getSize().height);
                           dim= resizeToFont(this.getSize(),olddim);
                           this.setSize(dim);
                           tabRef2.setFontResized(false);
                       }
                    }
                    if(dim == null){
                        System.out.println("Dim null default size activated");
                        Dimension dim2 = viewRef2.getSize();
                        this.setSize(dim2);
                        this.setisLoadedFromWsp(true);
                        this.setOldSize(dim2);
                        this.setOrigDimention(new Dimension(620,489));
                        resizeRows(dim2,this.getOrigDimension().height);
                    }
                     tabRef2.getSmartTable().adjustColumnWidthsToFit(10);
                }

                public void resizeRows(Dimension dim, int origHeight){
                    Font oFont = viewRef2.getFont();
                    if (oFont == null) {
                        oFont = tabRef2.getTable().getFont();
                    }
//                    int defaultsize = oFont.getSize();
                    int titlteHeight = this.getTitlebarHeight();;
                    float ratio = (((float)defaultsize)/origHeight);
                    int newSize =(int)(ratio*dim.height);
                    if (oFont != null) {
                        Font oFont2 = new TWFont(oFont.getFamily(),oFont.getStyle(),newSize);
                        tabRef2.getTable().setFont(oFont2);
                        FontMetrics oMetrices = tabRef2.getFontMetrics(oFont2);
                        int rowCount = tabRef2.getTable().getRowCount();
                        int emptySpace = ((dim.height - titlteHeight)- 2 -((oMetrices.getMaxAscent() + oMetrices.getMaxDescent())*rowCount));
                        int newGap = (int)Math.floor((emptySpace/rowCount));
                        tabRef2.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap);
                        int finspace = (dim.height - titlteHeight)-4-  (oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + newGap)*rowCount;
                        if(finspace >0){
                            int hight = this.dim.height-finspace;
                            int width = this.dim.width;
                            this.dim.setSize(width,hight);
                            this.setOldSize(this.dim);
                        }
                        oMetrices = null;
                        oFont = null;
                        oFont2 = null;
                    }
                }
            };

//            InternalFrame frame = new InternalFrame(oDetailQuote, g_oWindowDaemon);
            oViewSetting.setParent(frame);
            frame.setColumnResizeable(true);
            frame.setDetachable(true);
            frame.setPrintable(true);
//            frame.setSize(oViewSetting.getSize());
            frame.setLocation(oViewSetting.getLocation());
            frame.getContentPane().add(oDetailQuote);
            frame.setResizable(true);
            frame.setClosable(true);
            frame.setMaximizable(false);
            frame.setIconifiable(true);
            frame.addComponentListener(frame);
            oTopDesktop.add(frame);
            MutualFund fund = MutualFundStore.getInstance().getRecord(index);
            if (fund != null) {
                frame.setTitle(Language.getString("DETAIL_QUOTE") + " : " + fund.getFundName());
                fund = null;
            }
            frame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            frame.updateUI();
            frame.applySettings();
            //oDetailQuote.getTable().setShowVerticalLines(true);

            dqModel.updateGUI();


            frame.show();
            frame.setLayer(GUISettings.TOP_LAYER);
            frame.setOrientation();
            g_oViewSettings.setWindow(ViewSettingsManager.DETAIL_QUOTE_MFUND + "|" +
                    index, frame);

            ((SmartTable) oDetailQuote.getTable()).adjustColumnWidthsToFit(40);
            frame.updateUI();
            oDetailQuote.packFrame();

            if (!loadedFromWorkspace) {
                frame.setLocationRelativeTo(oTopDesktop);
                oViewSetting.setLocation(frame.getLocation());
                frame.setOrigDimention(frame.getSize());
            }
            frame.setisLoadedFromWsp(false);
            frame = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /*public int adjustColWidths(JTable table, boolean twoColMode) {
        int[] colWidths = new int[4];
        Arrays.fill(colWidths, 0);
        int spacing = (int) table.getIntercellSpacing().getWidth();

        FontMetrics fMetrics = table.getFontMetrics(table.getFont());
        for (int i = 0; i < table.getRowCount(); i++) {
            JLabel comp = (JLabel) table.getCellRenderer(i, 0).getTableCellRendererComponent(table, table.getModel().getValueAt(i, 0), false, false, i, 0);
            if (fMetrics.stringWidth(comp.getText()) > colWidths[0]) {
                colWidths[0] = fMetrics.stringWidth(comp.getText());
            }

            comp = (JLabel) table.getCellRenderer(i, 1).getTableCellRendererComponent(table, table.getModel().getValueAt(i, 1), false, false, i, 1);
            if (fMetrics.stringWidth(comp.getText()) > colWidths[1]) {
                colWidths[1] = fMetrics.stringWidth(comp.getText());
            }

            if (!twoColMode) {
                comp = (JLabel) table.getCellRenderer(i, 2).getTableCellRendererComponent(table, table.getModel().getValueAt(i, 2), false, false, i, 2);
                if (fMetrics.stringWidth(comp.getText()) > colWidths[2]) {
                    colWidths[2] = fMetrics.stringWidth(comp.getText());
                }
                comp = (JLabel) table.getCellRenderer(i, 3).getTableCellRendererComponent(table, table.getModel().getValueAt(i, 3), false, false, i, 3);
                if (fMetrics.stringWidth(comp.getText()) > colWidths[3]) {
                    colWidths[3] = fMetrics.stringWidth(comp.getText());
                }
            }
        }
        int totalWidth = 0;
        table.getColumn("0").setPreferredWidth(colWidths[0] + spacing * 4);
        totalWidth += colWidths[0] + spacing * 4;
        table.getColumn("1").setPreferredWidth(Math.max(colWidths[1], colWidths[3]) + spacing * 4);
        totalWidth += Math.max(colWidths[1], colWidths[3]) + spacing * 4;
        if (!twoColMode) {
            table.getColumn("2").setPreferredWidth(colWidths[2] + spacing * 4);
            totalWidth += colWidths[2] + spacing * 4;
            table.getColumn("3").setPreferredWidth(Math.max(colWidths[1], colWidths[3]) + spacing * 4);
            totalWidth += Math.max(colWidths[1], colWidths[3]) + spacing * 4;
        }
        table.updateUI();
        return totalWidth;
    }*/


    public String exportAllTrades(TWList<Trade> trades, String exchange, String fileName) {
//        String sFile = null;  //todo:changed
        String[] extentions1 = new String[1];
        String[] extentions2 = new String[1];
        String[] extentions3 = new String[1];
        final TWList<Trade> trd = trades;  //todo:changed
        final String exg = exchange; //todo:changed
        final String file = fileName; //todo:changed
        final BigInteger columns;//showSelectColumns(true); //todo:changed
//        BigInteger columns = BigInteger.valueOf(0);
        UIManager.put("FileChooser.fileNameLabelText", Language.getString("FILENAME"));
        final TWFileChooser twFileChooser = new TWFileChooser(window, Settings.getAbsolutepath());   //todo:changed
        extentions1[0] = "txt";
        extentions2[0] = "csv";
        extentions3[0] = "doc";

        twFileChooser.getFileChooserComponent().setAcceptAllFileFilterUsed(false);
        twFileChooser.getFileChooserComponent().setFileSelectionMode(JFileChooser.FILES_ONLY);
        twFileChooser.setDialogTitle(Language.getString("EXPORT_TO_FILE"));
        twFileChooser.setTradeType(TWFileChooser.ALL);

        final TWFileFilter oFilterTXT = new
                TWFileFilter(Language.getString("TXT_FORMAT"), extentions1);    //todo:changed
        final TWFileFilter oFilterCSV = new
                TWFileFilter(Language.getString("CSV_FORMAT"), extentions2); //todo:changed
        final TWFileFilter oFilterDOC = new
                TWFileFilter(Language.getString("DOC_FORMAT"), extentions3);   //todo:changed

        twFileChooser.getFileChooserComponent().setFileFilter(oFilterCSV);
        twFileChooser.getFileChooserComponent().setFileFilter(oFilterTXT);
        if (Constants.isMSWordAvailable == 0) {
            WordWriter.isMSWordAvailable();
        }
        if (Constants.isMSWordAvailable == 1) {
            twFileChooser.getFileChooserComponent().setFileFilter(oFilterDOC);
        }
        twFileChooser.setLocationRelativeTo(window);
        GUISettings.applyOrientation(twFileChooser);
        int status = twFileChooser.showDialog();
        columns = twFileChooser.getSelectedColumns();

        try {
            if (status == TWFileChooser.STATUS_APPLY) {
                new Thread(new Runnable() {
                    public void run() {
                        String sFile = twFileChooser.getPath();
                        TWFileFilter fileFilter = (TWFileFilter) twFileChooser.getFileChooserComponent().getFileFilter();
                        if (fileFilter == oFilterTXT) {
                            if (sFile.indexOf(".") < 0)
                                sFile += ".txt";
                        } else if (fileFilter == oFilterCSV) {
                            if (sFile.indexOf(".") < 0)
                                sFile += ".csv";
                        } else if (fileFilter == oFilterDOC) {
                            if (sFile.indexOf(".") < 0)
                                sFile += ".doc";
                        }

                        File[] fileList = twFileChooser.getFileChooserComponent().getCurrentDirectory().listFiles();
                        if (isValidFile(fileList, sFile)) {
                            String columnHeadings = getColumnHeadingsForTrades(true, columns);

                            if (fileFilter == oFilterCSV || fileFilter == oFilterTXT) {
                                FileOutputStream out = null;
                                try {
                                    out = new FileOutputStream(sFile);
                                    out.write(columnHeadings.getBytes());
                                } catch (IOException e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                                if (trd != null) {
                                    Trade trade;
                                    for (int i = 0; i < trd.size(); i++) {
                                        trade = (Trade) trd.get(i);
                                        try {
                                            out.write(SharedMethods.getFormatedTradeString(true, trade.getExchange(), trade.toTradeString(), columns));
                                        } catch (IOException e) {
                                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                        }
                                        trade = null;
                                    }
                                } else {
                                    System.out.println("Export from Historical Intraday");
                                    exportTrades(out, columns, exg, file);
                                }
                                // trades = null;
                                try {
                                    out.close();
                                } catch (IOException e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                            } else if (fileFilter == oFilterDOC) {
//                    PageSetup setup = new PageSetup(twFileChooser);
//                    int val = setup.showDialog();
                                if (twFileChooser.pageSetupVal == PageSetup.STATUS_OK) {
                                    WordWriter writer = new WordWriter();
//                    writer.setText(columnHeadings);
                                    writer.addColumnNames(columnHeadings);
                                    if (trd != null) {
                                        Trade trade;
                                        for (int i = 0; i < trd.size(); i++) {
                                            trade = (Trade) trd.get(i);
                                            //                            writer.appendText(new String(SharedMethods.getFormatedTradeString(true, trade.getExchange(), trade.toTradeString(), columns)));
                                            writer.addDetails(new String(SharedMethods.getFormatedTradeString(true, trade.getExchange(), trade.toTradeString(), columns)));
                                            trade = null;
                                        }
                                    } else {
                                        System.out.println("Export from Historical Intraday");
                                        exportTrades(writer, columns, exg, file);
                                    }
                                    writer.setPage(PageSetup.selectedOrientation, PageSetup.selectedPaperSize, PageSetup.selectedLeftMargin, PageSetup.selectedRightMargin, PageSetup.selectedTopMargin, PageSetup.selectedBottomMargin);
                                    writer.writeTable();
                                    writer.saveFile(sFile);
                                }
                            }
                        }
//                        trades = null;
                    }
                }).start();
                /*sFile = twFileChooser.getPath();
                TWFileFilter fileFilter = (TWFileFilter) twFileChooser.getFileChooserComponent().getFileFilter();
                if (fileFilter == oFilterTXT) {
                    if (sFile.indexOf(".") < 0)
                        sFile += ".txt";
                } else if (fileFilter == oFilterCSV) {
                    if (sFile.indexOf(".") < 0)
                        sFile += ".csv";
                } else if (fileFilter == oFilterDOC) {
                    if (sFile.indexOf(".") < 0)
                        sFile += ".doc";
                }

                String columnHeadings = getColumnHeadingsForTrades(true, columns);

                if (fileFilter == oFilterCSV || fileFilter == oFilterTXT) {
                    FileOutputStream out = new FileOutputStream(sFile);
                    out.write(columnHeadings.getBytes());
                    if (trades != null) {
                        Trade trade;
                        for (int i = 0; i < trades.size(); i++) {
                            trade = (Trade) trades.get(i);
                            out.write(SharedMethods.getFormatedTradeString(true, trade.getExchange(), trade.toTradeString(), columns));
                            trade = null;
                        }
                    } else {
                        System.out.println("Export from Historical Intraday");
                        exportTrades(out, columns, exchange, fileName);
                    }
                    // trades = null;
                    out.close();
                } else if (fileFilter == oFilterDOC) {
//                    PageSetup setup = new PageSetup(twFileChooser);
//                    int val = setup.showDialog();
                    if (twFileChooser.pageSetupVal == PageSetup.STATUS_OK) {
                        WordWriter writer = new WordWriter();
//                    writer.setText(columnHeadings);
                        writer.addColumnNames(columnHeadings);
                        if (trades != null) {
                            Trade trade;
                            for (int i = 0; i < trades.size(); i++) {
                                trade = (Trade) trades.get(i);
                                //                            writer.appendText(new String(SharedMethods.getFormatedTradeString(true, trade.getExchange(), trade.toTradeString(), columns)));
                                writer.addDetails(new String(SharedMethods.getFormatedTradeString(true, trade.getExchange(), trade.toTradeString(), columns)));
                                trade = null;
                            }
                        } else {
                            System.out.println("Export from Historical Intraday");
                            exportTrades(writer, columns, exchange, fileName);
                        }
                        writer.setPage(PageSetup.selectedOrientation, PageSetup.selectedPaperSize, PageSetup.selectedLeftMargin, PageSetup.selectedRightMargin, PageSetup.selectedTopMargin, PageSetup.selectedBottomMargin);
                        writer.writeTable();
                        writer.saveFile(sFile);
                    }
                }
                trades = null;*/
            } else {
//                sFile = null; // file chooser cancelled    //todo:changed
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        extentions1 = null;
        extentions2 = null;
//        twFileChooser = null;   //todo:changed

//        return sFile;   //todo:changed
        return "";
    }

    public String exportAllDetailedTrades(CachedStore trades, String exchange, String fileName) {
        String sFile = null;
        String[] extentions1 = new String[1];
        String[] extentions2 = new String[1];

        BigInteger columns = BigInteger.valueOf(0);//showSelectColumns(true);
        UIManager.put("FileChooser.fileNameLabelText", Language.getString("FILENAME"));
        TWFileChooser twFileChooser = new TWFileChooser(window, Settings.getAbsolutepath());
        extentions1[0] = "txt";
        extentions2[0] = "csv";
        twFileChooser.getFileChooserComponent().setAcceptAllFileFilterUsed(false);
        twFileChooser.getFileChooserComponent().setFileSelectionMode(JFileChooser.FILES_ONLY);
        twFileChooser.setDialogTitle(Language.getString("EXPORT_TO_FILE"));
        twFileChooser.setTradeType(TWFileChooser.ALL);
        TWFileFilter oFilterTXT = new
                TWFileFilter(Language.getString("TXT_FORMAT"), extentions1);
        TWFileFilter oFilterCSV = new
                TWFileFilter(Language.getString("CSV_FORMAT"), extentions2);
        twFileChooser.getFileChooserComponent().setFileFilter(oFilterCSV);
        twFileChooser.getFileChooserComponent().setFileFilter(oFilterTXT);

        twFileChooser.setLocationRelativeTo(window);
        int status = twFileChooser.showDialog();
        columns = twFileChooser.getSelectedColumns();

        try {
            if (status == TWFileChooser.STATUS_APPLY) {
                sFile = twFileChooser.getPath();
                if (twFileChooser.getFileChooserComponent().getFileFilter() == oFilterTXT) {
                    if (sFile.indexOf(".") < 0)
                        sFile += ".txt";
                } else if (twFileChooser.getFileChooserComponent().getFileFilter() == oFilterCSV) {
                    if (sFile.indexOf(".") < 0)
                        sFile += ".csv";
                }
                File[] fileList = twFileChooser.getFileChooserComponent().getCurrentDirectory().listFiles();
                if (isValidFile(fileList, sFile)) {
                    FileOutputStream out = new FileOutputStream(sFile);
                    out.write(getColumnHeadingsForTrades(true, columns).getBytes());
                    if (trades != null) {
                        Trade trade;

                        Enumeration<Cacheble> unfilteredcache = trades.getAllCachedObjects();
                        while (unfilteredcache.hasMoreElements()) {
                            trade = (Trade) unfilteredcache.nextElement();

                            out.write(SharedMethods.getFormatedTradeString(true, trade.getExchange(), trade.toTradeString(), columns));
                            trade = null;
                        }
                        /* for (int i = 0; i < trades.size(); i++) {
                            trade = (Trade) trades.get(i);
                            out.write(SharedMethods.getFormatedTradeString(true, trade.getExchange(), trade.toTradeString(), columns));
                            trade = null;
                        }*/
                    } else {
                        System.out.println("Export from Historical Intraday");
                        exportTrades(out, columns, exchange, fileName);
                    }
                    trades = null;
                    out.close();
                }
            } else {
                sFile = null; // file chooser cancelled
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        extentions1 = null;
        extentions2 = null;
        twFileChooser = null;

        return sFile;
    }

    private boolean isValidFile(File[] fileList, String sFile) {
        boolean validFile = true;
        try {
            for (File file : fileList) {
                if (file.getPath().equalsIgnoreCase(sFile)) {
                    String message = Language.getString("REPLACE_FILE_MSG");
                    int option = SharedMethods.showConfirmMessage(message, JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                    if (option == JOptionPane.YES_OPTION) {
                        validFile = true;
                        break;
                    } else {
                        validFile = false;
                    }
                }
            }

        } catch (Exception e) {
            validFile = false;
            e.printStackTrace();
        }
        return validFile;
    }

    private void exportTrades(FileOutputStream out, BigInteger columns, String exchange, String fileName) {
        SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat dateFormatIn = new SimpleDateFormat("yyyyMMdd");
        String archive = Settings.getTradeArchivePath() + "\\" + exchange + "\\" + fileName;
        try {
            String date = dateFormatOut.format(dateFormatIn.parse(fileName.split("\\.")[0].substring(0, 8)));
            ZipInputStream zIn = new ZipInputStream(new FileInputStream(archive));
            DataInputStream in = new DataInputStream(zIn);
            zIn.getNextEntry();
            String data;
            int count = 0;
            while (true) {
                count++;
                data = in.readLine();
                if (data != null) {
                    out.write(SharedMethods.getFormatedTradeString(true, exchange, DetailedTradeStore.getSharedInstance().createTrade(ExchangeMap.getIDFor(exchange), data, date, false).toTradeString(), columns));
                    if ((count % 100) == 0) {
                        Thread.sleep(1);
                    }
                } else {
                    break;
                }
            }
            zIn.close();
            zIn = null;
            data = null;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private void exportTrades(WordWriter writer, BigInteger columns, String exchange, String fileName) {
        SimpleDateFormat dateFormatOut = new SimpleDateFormat("dd/MM/yyyy");
        SimpleDateFormat dateFormatIn = new SimpleDateFormat("yyyyMMdd");
        String archive = Settings.getTradeArchivePath() + "\\" + exchange + "\\" + fileName;
        try {
            String date = dateFormatOut.format(dateFormatIn.parse(fileName.split("\\.")[0].substring(0, 8)));
            ZipInputStream zIn = new ZipInputStream(new FileInputStream(archive));
            DataInputStream in = new DataInputStream(zIn);
            zIn.getNextEntry();
            String data;
            int count = 0;
            while (true) {
                count++;
                data = in.readLine();
                if (data != null) {
                    writer.addDetails(new String(SharedMethods.getFormatedTradeString(true, exchange, DetailedTradeStore.getSharedInstance().createTrade(ExchangeMap.getIDFor(exchange), data, date, false).toTradeString(), columns)));
                    if ((count % 100) == 0) {
                        Thread.sleep(1);
                    }
                } else {
                    break;
                }
            }
            zIn.close();
            zIn = null;
            data = null;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private void exportCompanyTrades(String symbol) {
        String sFile;
        String[] extentions1 = new String[1];
        String[] extentions2 = new String[1];
        String[] extentions3 = new String[1];

        BigInteger columns;// = showSelectColumns(false);

        UIManager.put("FileChooser.fileNameLabelText", Language.getString("FILENAME"));
        TWFileChooser twFileChooser = new TWFileChooser(window, Settings.getAbsolutepath());
        twFileChooser.setTradeType(TWFileChooser.COMPANY);
        twFileChooser.getFileChooserComponent().setSelectedFile(new File(getSelectedSymbol()));
        twFileChooser.getFileChooserComponent().setFileSelectionMode(JFileChooser.FILES_ONLY);
        twFileChooser.setDialogTitle(Language.getString("EXPORT_TO_FILE"));
        extentions1[0] = "txt";
        extentions2[0] = "csv";
        extentions3[0] = "doc";
        TWFileFilter oFilterTXT = new
                TWFileFilter(Language.getString("TXT_FORMAT"), extentions1);
        TWFileFilter oFilterCSV = new
                TWFileFilter(Language.getString("CSV_FORMAT"), extentions2);
        TWFileFilter oFilterDOC = new
                TWFileFilter(Language.getString("DOC_FORMAT"), extentions3);

        twFileChooser.getFileChooserComponent().setAcceptAllFileFilterUsed(false);
        twFileChooser.getFileChooserComponent().setFileFilter(oFilterCSV);
        twFileChooser.getFileChooserComponent().setFileFilter(oFilterTXT);

        if (Constants.isMSWordAvailable == 0) {
            WordWriter.isMSWordAvailable();
        }
        if (Constants.isMSWordAvailable == 1) {
            twFileChooser.getFileChooserComponent().setFileFilter(oFilterDOC);
        }

        //int status = oFC.showSaveDialog(this);
        twFileChooser.setLocationRelativeTo(window);
        //setLocationRelativeTo
        int status = twFileChooser.showDialog();
        columns = twFileChooser.getSelectedColumns();
        try {
            if (status == TWFileChooser.STATUS_APPLY) {
                sFile = twFileChooser.getPath();
                TWFileFilter fileFilter = (TWFileFilter) twFileChooser.getFileChooserComponent().getFileFilter();
                if (fileFilter == oFilterTXT) {
                    if (sFile.indexOf(".") < 0)
                        sFile += ".txt";
                } else if (fileFilter == oFilterCSV) {
                    if (sFile.indexOf(".") < 0)
                        sFile += ".csv";
                } else if (fileFilter == oFilterDOC) {
                    if (sFile.indexOf(".") < 0)
                        sFile += ".doc";
                }

                String columnHeadings = getColumnHeadingsForTrades(false, columns);
                //String columnHeadingsStr = new String(columnHeadings);
                TWList<Trade> trades = CompanyTradeStore.getSharedInstance().getCompanyTradesStore(symbol).getStore();

                if (fileFilter == oFilterCSV || fileFilter == oFilterTXT) {
                    FileOutputStream out = new FileOutputStream(sFile);
                    out.write(columnHeadings.getBytes());

                    Trade trade;
                    if (trades != null) {
                        for (int i = 0; i < trades.size(); i++) {
                            trade = trades.get(i);
                            out.write(SharedMethods.getFormatedTradeString(false, trade.getExchange(), SharedMethods.toCompanyTradeString(trade), columns));
                            trade = null;
                        }
                    }
                    out.close();
                } else if (fileFilter == oFilterDOC) {
//                    PageSetup setup = new PageSetup(twFileChooser);
//                    int val = setup.showDialog();
                    if (twFileChooser.pageSetupVal == PageSetup.STATUS_OK) {
                        WordWriter writer = new WordWriter();
//                        writer.setText(columnHeadings);
                        writer.addColumnNames(columnHeadings);
                        Trade trade;
                        if (trades != null) {
                            for (int i = 0; i < trades.size(); i++) {
                                trade = trades.get(i);
//                                writer.appendText(new String(SharedMethods.getFormatedTradeString(false, trade.getExchange(), SharedMethods.toCompanyTradeString(trade), columns)));
                                writer.addDetails(new String(SharedMethods.getFormatedTradeString(false, trade.getExchange(), SharedMethods.toCompanyTradeString(trade), columns)));

                                trade = null;
                            }
                        }
                        writer.setPage(PageSetup.selectedOrientation, PageSetup.selectedPaperSize, PageSetup.selectedLeftMargin, PageSetup.selectedRightMargin, PageSetup.selectedTopMargin, PageSetup.selectedBottomMargin);
                        writer.writeTable();
                        writer.saveFile(sFile);
                    }
                }

                trades = null;
            } else {
                sFile = null; // file chooser cancelled
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        twFileChooser = null;
        extentions1 = null;
        extentions2 = null;
    }

    private String getColumnHeadingsForTrades(boolean allTrades, BigInteger columns) {

        String[] headings;
        StringBuffer buffer = new StringBuffer("");

        if (allTrades)
            headings = Language.getList("EXPORT_ALL_TRADES_COLS");
        else
            headings = Language.getList("EXPORT_COMPANY_TRADES_COLS");

        for (int i = 0; i < headings.length; i++) {
            if ((columns.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(1))) {
                buffer.append(",");
                buffer.append(headings[i]);
            }
            columns = columns.shiftRight(1);
        }
        buffer.append("\r\n");
        return buffer.substring(1);
    }


    public void mnu_AnnouncementsSymbol(String sSymbol) {
        try {
            String sSelectedSymbol;

            if (sSymbol == null) {
                sSelectedSymbol = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(sSymbol))
                    sSelectedSymbol = sSymbol;
                else
                    sSelectedSymbol = DataStore.getSharedInstance().findKey(sSymbol);
            }

            if (DataStore.getSharedInstance().isValidSymbol(sSelectedSymbol)) {
                Stock stock = DataStore.getSharedInstance().getStockObject(sSelectedSymbol);
                stock.setAnnouncementAvailability(Constants.ITEM_READ);
                stock = null;
            }

            if (sSelectedSymbol.equals(Constants.NULL_STRING))
                return;

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.ANNOUNCEMENT_VIEW + "|" +
                    sSelectedSymbol);
            if (oFrame != null) {
                oFrame.setIcon(false);
                oFrame.show();
                return;
            }

            ViewSetting oViewSetting = null;

            Table table = new Table();
            table.setAutoResize(true);
            CompanyAnnounceModel mode = new CompanyAnnounceModel(sSelectedSymbol);

            oViewSetting = g_oViewSettings.getAnnouncementView(ViewSettingsManager.ANNOUNCEMENT_VIEW
                    + "|" + sSelectedSymbol);

            if (oViewSetting == null) {
                oViewSetting = g_oViewSettings.getSymbolView("ANNOUNCEMENT_VIEW");
                oViewSetting = oViewSetting.getObject(); // clone the object
                oViewSetting.setID(sSelectedSymbol);
                g_oViewSettings.putAnnouncementView(ViewSettingsManager.ANNOUNCEMENT_VIEW
                        + "|" + sSelectedSymbol, oViewSetting);
            }
            mode.setViewSettings(oViewSetting);
            table.setModel(mode);
            mode.setTable(table);
            mode.applyColumnSettings();

            InternalFrame oAnnouncementFrame = new InternalFrame(null, Constants.NULL_STRING, g_oWindowDaemon, table);
            oViewSetting.setParent(oAnnouncementFrame);
            oAnnouncementFrame.setSize(oViewSetting.getSize());
            oAnnouncementFrame.setLocation(oViewSetting.getLocation());
            oAnnouncementFrame.setResizable(true);
            oAnnouncementFrame.setClosable(true);
            oAnnouncementFrame.setMaximizable(true);
            oAnnouncementFrame.setIconifiable(true);
            oTopDesktop.add(oAnnouncementFrame);
            oAnnouncementFrame.setTitle(Language.getString("ANNOUNCEMENTS") + " : " +
                    DataStore.getSharedInstance().getStockObject(sSelectedSymbol).getLongDescription());
            oAnnouncementFrame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            oAnnouncementFrame.getContentPane().add(table);
            table.updateGUI();
            oAnnouncementFrame.applySettings();
            GUISettings.applyOrientation(oAnnouncementFrame);
            oAnnouncementFrame.setLocationRelativeTo(oTopDesktop);
            oAnnouncementFrame.show();

            mode.adjustRows();
            oAnnouncementFrame.setLayer(GUISettings.TOP_LAYER);
            oAnnouncementFrame.setOrientation();
            table.getTable().getColumn("0").setPreferredWidth(5);
            g_oViewSettings.setWindow(ViewSettingsManager.ANNOUNCEMENT_VIEW + "|" + sSelectedSymbol, oAnnouncementFrame);
            table.getTable().addMouseListener(new AnnouncementMouseListener(table.getTable(),
                    ANNOUNCMENT_TYPE.COMPANY));

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mnu_OtherEvents(String sKey) {
        if (!sKey.isEmpty() && sKey != null) {

//            OtherSymbolEventsFrame other = new OtherSymbolEventsFrame(sKey);
//            oTopDesktop.add(other);
//            other.setLayer(GUISettings.TOP_LAYER);
//            other.show();

        }


    }

    public void customizeTicker(boolean showTickerNow) {
        /*if (portfolioView) {
            TradeFeeder.setSymbolFilter(g_oPortfolioTable.getDataStore());
            TradeFeeder.clearOnlineData();
            tickerFrame.setTitle(Language.getString("TICKER") + " - " + g_oPortfolioTable.getTitle());
            if (showTickerNow) {
                tickerFrame.show();
                TradeFeeder.setVisible(true);
            }
        } else if (g_bTableView) {
            if (g_oTempTable.getViewSettings().isMarketType()) {
                Ticker.setMode(Ticker.MODE_ALL);
                TradeFeeder.clearOnlineData();
                tickerFrame.setTitle(Language.getString("TICKER") + " - " + g_oTempTable.getTitle());
            } else {
                TradeFeeder.setSymbolFilter(g_oTempTable.getSymbols());
                TradeFeeder.clearOnlineData();
                tickerFrame.setTitle(Language.getString("TICKER") + " - " + g_oTempTable.getTitle());
            }
            if (showTickerNow) {
                tickerFrame.show();
                TradeFeeder.setVisible(true);
            }
        } else if (mistView) {
            // to do
        }*/
    }

    public void mnu_TimeNSalesSummaryForSymbolLinked(String sSymbol) {
        mnu_TimeNSalesSummaryForSymbol(sSymbol, false, false, LinkStore.LINK_NONE);
        InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW +
                "|" + sSymbol);
        LinkStore.getSharedInstance().LinkToSidebar(oFrame);
    }


    public void mnu_TimeNSalesSummaryForSymbol(String sSymbol, boolean isFromWSP, boolean isLinked, String linkGroup) {
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.TimeNSales, "Summary");
        try {
            final String sSelectedSymbol;
            boolean loadedFromWorkspace = false;
            byte stockDecimalPlaces = 2;

            if (sSymbol == null) {
                sSelectedSymbol = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(sSymbol))
                    sSelectedSymbol = sSymbol;
                else
                    sSelectedSymbol = DataStore.getSharedInstance().findKey(sSymbol);
            }


            if (sSelectedSymbol.equals(Constants.NULL_STRING)) {
                return;
            }
            if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return;
            }
            if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return;
            }
            try {
                stockDecimalPlaces = SharedMethods.getDecimalPlaces(sSelectedSymbol);
            } catch (Exception e) {
                stockDecimalPlaces = 2;
            }

            /* check if the window is already created
                If so, do not create it again, just focus it
             */
            if (!isLinked) {
                InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.TIMEnSALES_SUMMARY_VIEW +
                        "|" + sSelectedSymbol);
                if (oFrame != null) {
                    oFrame.setIcon(false);
                    oFrame.show();
                    oFrame.setSelected(true);
                    oFrame.getTableComponent().getTable().requestFocus();
                    return;
                }
            }
            ViewSetting oViewSetting = null;

            Table table = new Table();
            TimeNSalesSymbolSummaryFrame summary = new TimeNSalesSymbolSummaryFrame(null, Constants.NULL_STRING,
                    g_oWindowDaemon, table, sSelectedSymbol, stockDecimalPlaces, isFromWSP, isLinked, linkGroup);
            if (summary != null) {
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Time and sales
     */
    public InternalFrame mnu_TimeNSalesSymbol(String sSymbol, byte type, boolean fromWSP, boolean isLinked, String linkGroup) {

        try {
            final String sSelectedSymbol;
            boolean loadedFromWorkspace = false;
            byte stockDecimalPlaces = 2;


            if (type != Constants.FULL_QUOTE_TYPE) {
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.TimeNSales, "Symbol");
                if (sSymbol == null) {
                    sSelectedSymbol = getSelectedSymbol();
                } else {
                    if (SharedMethods.isFullKey(sSymbol))
                        sSelectedSymbol = sSymbol;
                    else
                        sSelectedSymbol = DataStore.getSharedInstance().findKey(sSymbol);
                }
            } else {
//                if(sSymbol!=null)
//                    sSelectedSymbol = sSymbol;
//                else
                //sSelectedSymbol = MultiDialogWindow.TABLE_ID;
                sSelectedSymbol = sSymbol;
            }

            if (sSelectedSymbol.equals(Constants.NULL_STRING))
                return null;

            if ((!ExchangeStore.isValidIinformationType(SharedMethods.getExchangeFromKey(sSelectedSymbol), Meta.IT_SymbolTimeAndSales)) && (!fromWSP)) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        new ShowMessage("<html><b>" + Language.getString("TIME_AND_SALES") + "&nbsp;&nbsp;</b><br>" +
                                Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
                    }
                });
                return null;
            }
            if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return null;
            }
            if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return null;
            }
            if (ExchangeStore.isDelayed(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeDelayedmessage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return null;
            }

            try {
                stockDecimalPlaces = SharedMethods.getDecimalPlaces(sSelectedSymbol);
            } catch (Exception e) {
                stockDecimalPlaces = 2;
            }

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            if (type != Constants.FULL_QUOTE_TYPE && !isLinked) {

                InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.TIMEnSALES_VIEW2 + "|" +
                        sSelectedSymbol);
                if (oFrame != null) {
                    oFrame.setIcon(false);
                    oFrame.show();
                    oFrame.setSelected(true);
                    oFrame.getTableComponent().getTable().requestFocus();
                    return oFrame;
                }
            }

            ViewSetting oViewSetting = null;

            Table oTimeNsales = new Table();

            TimeNSalesSymbolFrame timensalesframe = new TimeNSalesSymbolFrame(null, Constants.NULL_STRING, g_oWindowDaemon, oTimeNsales, sSelectedSymbol, type, fromWSP, stockDecimalPlaces, isLinked, linkGroup);
            if (timensalesframe != null) {
                return timensalesframe;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;

    }

    private void showFrame(JInternalFrame frame) {
        try {
            frame.setVisible(true);
            if (frame.isIcon()) {
                frame.setIcon(false);
                frame.setSelected(true);
            } else if (!frame.isSelected()) {
                frame.setSelected(true);
            } else {
                frame.setVisible(false);
            }
        } catch (PropertyVetoException e) {
        }
    }

    public void showTimeAndSalesSummary(TWMenuItem menuItem, TradeSummaryPanel summaryPanel, String key) {
        summaryPanel.setVisible(!summaryPanel.isVisible());

        ViewSetting setting = g_oViewSettings.getTimenSalesView(key);
        setting.putProperty(VC_TnS_SUMMARY, summaryPanel.isVisible());
        setting = null;

        if (summaryPanel.isVisible()) {
            menuItem.setText(Language.getString("HIDE_TnS_SUMMARY"));
            menuItem.setIconFile("hidesummary.gif");
        } else {
            menuItem.setText(Language.getString("SHOW_SUMMARY"));
            menuItem.setIconFile("showsummary.gif");
        }
    }

    public void filterTimeAndSalesSummary(String key) {
        FilterableTradeStore store = CompanyTradeStore.getSharedInstance().getCompanyTradesStore(key);
        if (store != null) {
            TradeFilterUI ui = new TradeFilterUI(window, Language.getString("FILTER"), store);
            ui.setVisible(true);
            ui.dispose();
            ui = null;
            InternalFrame frame = g_oViewSettings.getWindow(ViewSettingsManager.TIMEnSALES_VIEW2 + "|" + key);
            if (frame != null) {
                frame.getTable1().updateUI();
                frame = null;
            }
        }
    }

    public void filterMarketTimeAndSalesSummary(String exchange, AllTradesModel model) {
        FilterableTradeStore store = model.getStore();
        if (store != null) {
            MarketTradeFilterUI ui = new MarketTradeFilterUI(window, Language.getString("FILTER"), store, model);
            ui.setVisible(true);
            ui.dispose();
            ui = null;
            InternalFrame frame = g_oViewSettings.getWindow(ViewSettingsManager.MARKET_TIMEnSALES_VIEW + "|" + exchange);
            if (frame != null) {
                frame.getTable1().updateUI();
                frame = null;
            }
        }
    }

    public void filterTimeAndSalesSummaryForDetailedTrades() {
        FilterableTradeStore store = DetailedTradeStore.getSharedInstance().getFilterableStore();
        if (store != null) {
            TradeFilterUI ui = new TradeFilterUI(window, Language.getString("FILTER"), store);
            ui.setVisible(true);
            ui.dispose();
            ui = null;

//            DetailedTradeStore.getSharedInstance().getSummaryPanel().setvalues();
            /*if (g_oFullTimeSalesFrame != null) {
                g_oFullTimeSalesFrame.getTable1().updateUI();
            }*/
        }
    }

    public void depthByOrderLinked(String key, boolean fromWSP) {
        depthByOrder(key, fromWSP, false, LinkStore.LINK_NONE);
        InternalFrame frame = g_oViewSettings.getWindow(ViewSettingsManager.DEPTH_ASK_VIEW + "|" +
                key);
        LinkStore.getSharedInstance().LinkToSidebar(frame);
    }

    public void depthByOrder(String key, boolean fromWSP, boolean isLinked, String linkGroup) {
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Depth, "ByOrder");
        try {

            String selectedKey;
            boolean loadedFromWorkspace = false;
            byte stockDecimalPlaces = 2;

            if (key == null) {
                selectedKey = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(key))
                    selectedKey = key;
                else
                    selectedKey = DataStore.getSharedInstance().findKey(key);
            }

            if (selectedKey.equals(Constants.NULL_STRING))
                return;

            try {
                stockDecimalPlaces = SharedMethods.getDecimalPlaces(selectedKey);
            } catch (Exception e) {
                stockDecimalPlaces = 2;
            }


            if ((!fromWSP) && (!Settings.isConnected())) {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                return;
            } else if ((!ExchangeStore.isValidIinformationType(SharedMethods.getExchangeFromKey(selectedKey), Meta.IT_MarketDepthByOrder)) && (!fromWSP)) {
                new ShowMessage("<html><b>" + Language.getString("MARKET_DEPTH_BY_ORDER") + "&nbsp;&nbsp;</b><br>" +
                        Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
                return;
            }
            if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(selectedKey))) {
                SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(selectedKey));
                return;
            }
            if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(selectedKey))) {
                SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(selectedKey));
                return;
            }
            if (ExchangeStore.isDelayed(SharedMethods.getExchangeFromKey(selectedKey))) {
                SharedMethods.showExchangeDelayedmessage(SharedMethods.getExchangeFromKey(selectedKey));
                return;
            }

/* check if the window is already created
                If so, do not create it again, just focus it
            */
            if (!isLinked) {
                InternalFrame depthFrame = g_oViewSettings.getWindow(ViewSettingsManager.DEPTH_ASK_VIEW + "|" +
                        selectedKey);
                if (depthFrame != null) {
                    depthFrame.setIcon(false);
                    depthFrame.show();
//                depthFrame.requestFocusInWindow();
                    depthFrame.setSelected(true);
                    depthFrame.getTableComponent().getTable().requestFocus();
                    return;
                }
            }

            ViewSetting oBidViewSetting = null;
            ViewSetting oAskViewSetting = null;

// Create the Bid Market Depth Table
            Table bidTable = new Table();
            Table askTable = new Table();
            DepthByOrderNormalFrame depthByOrder = new DepthByOrderNormalFrame(Constants.NULL_STRING, g_oWindowDaemon, bidTable, new Table[]{askTable}, fromWSP, selectedKey, isLinked, linkGroup);
            if (depthByOrder != null) {
                return;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void depthByPriceCombinedLonkToSidebar(final String key, boolean fromWSP, final byte type) {
        InternalFrame frame = depthByPriceCombined(key, fromWSP, type, true, LinkStore.LINK_NONE);
//        if(frame!= null){
//            LinkStore.getSharedInstance().LinkToSidebar(frame);
//        }
    }

    public InternalFrame depthByPriceCombined(final String key, boolean fromWSP, final byte type, boolean isLinked, String linkGroup) {

        try {
            final String selectedKey;
            boolean loadedFromWorkspace = false;
            byte stockDecimalPlaces = 2;

            if (type != Constants.FULL_QUOTE_TYPE) {
                if (key == null) {
                    selectedKey = getSelectedSymbol();
                } else {
                    if (SharedMethods.isFullKey(key))
                        selectedKey = key;
                    else
                        selectedKey = DataStore.getSharedInstance().findKey(key);
                }
            } else {
//                if(key!=null)
//                    selectedKey = key;
//                else
                //   selectedKey = MultiDialogWindow.TABLE_ID;
                selectedKey = key;
            }

            if (selectedKey.equals(Constants.NULL_STRING))
                return null;

            try {
                stockDecimalPlaces = SharedMethods.getDecimalPlaces(selectedKey);
            } catch (Exception e) {
                stockDecimalPlaces = 2;
            }

            if (type != Constants.FULL_QUOTE_TYPE) {
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Depth, "Combined");
                if ((!fromWSP) && (!Settings.isConnected())) {
                    new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                    return null;
                } else if ((!ExchangeStore.isValidIinformationType(SharedMethods.getExchangeFromKey(selectedKey), Meta.IT_MarketDepthByPrice)) && (!fromWSP)) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            new ShowMessage("<html><b>" + Language.getString("MARKET_DEPTH_BY_PRICE") + "&nbsp;&nbsp;</b><br>" +
                                    Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
                        }
                    });
                    return null;
                }
                if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(selectedKey))) {
                    SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(selectedKey));
                    return null;
                }
                if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(selectedKey))) {
                    SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(selectedKey));
                    return null;
                }
                if (ExchangeStore.isDelayed(SharedMethods.getExchangeFromKey(selectedKey))) {
                    SharedMethods.showExchangeDelayedmessage(SharedMethods.getExchangeFromKey(selectedKey));
                    return null;
                }
            }

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            if (type != Constants.FULL_QUOTE_TYPE && !isLinked) {
                InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.COMBINED_DEPTH_VIEW + "|" +
                        selectedKey);
                if (oFrame != null) {
                    oFrame.setIcon(false);
                    oFrame.show();
                    oFrame.setSelected(true);
                    oFrame.getTableComponent().getTable().requestFocus();
                    return oFrame;
                }
            }

            ViewSetting oBidViewSetting = null;

            // Create the Bid Market Depth Table
            final Table bidTable = new Table();
            DepthByPriceCombinedFrame depthcombined = new DepthByPriceCombinedFrame(null, Constants.NULL_STRING, g_oWindowDaemon, bidTable, selectedKey, fromWSP, type, isLinked, linkGroup);
            if (depthcombined != null) {
                return depthcombined;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public InternalFrame createDepthOddLot(final String key, boolean fromWSP, final byte type, boolean isLinked, String linkGroup) {

        try {
            final String selectedKey;

            if (type != Constants.FULL_QUOTE_TYPE) {
                if (key == null) {
                    selectedKey = getSelectedSymbol();
                } else {
                    if (SharedMethods.isFullKey(key))
                        selectedKey = key;
                    else
                        selectedKey = DataStore.getSharedInstance().findKey(key);
                }
            } else {
//
                selectedKey = MultiDialogWindow.TABLE_ID;
            }

            if (selectedKey.equals(Constants.NULL_STRING))
                return null;

            if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(selectedKey))) {
                SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(selectedKey));
                return null;
            }
            if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(selectedKey))) {
                SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(selectedKey));
                return null;
            }

            if (!isLinked) {
                InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.DEPTH_ODDLOT_VIEW + "|" +
                        selectedKey);
                if (oFrame != null) {
                    oFrame.setIcon(false);
                    oFrame.show();
                    oFrame.setSelected(true);
                    oFrame.getTableComponent().getTable().requestFocus();
                    return oFrame;
                }
            }
//            ViewSetting oddLotiewSetting = null;

            // Create the Bid Market Depth Table
            final Table oddLotTable = new Table();
            DepthOddLotFrame oddlot = new DepthOddLotFrame(null, Constants.NULL_STRING, g_oWindowDaemon, oddLotTable, fromWSP, selectedKey, type, isLinked, linkGroup);
            if (oddlot != null) {
                return oddlot;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }


    public InternalFrame depthByPrice(final String key, boolean fromWSP, byte parentWindowType) {
        if (Settings.isShowCombinedOrderBook()) {
            return depthByPriceCombined(key, false, parentWindowType, false, LinkStore.LINK_NONE);
        } else {
            return depthByPriceNormal(key, false, parentWindowType, false, LinkStore.LINK_NONE);
        }
    }


    public InternalFrame depthOddLot(final String key, boolean fromWSP, byte parentWindowType) {
        return createDepthOddLot(key, fromWSP, parentWindowType, false, LinkStore.LINK_NONE);
    }

    public InternalFrame showFullQuote(final String key, boolean fromWSP, final byte parentWindowType, boolean isLinked, String linkGroup) {
        try {
            final String selectedKey;

            if (key == null) {
                selectedKey = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(key))
                    selectedKey = key;
                else
                    selectedKey = DataStore.getSharedInstance().findKey(key);
            }

            if (selectedKey.equals(Constants.NULL_STRING))
                return null;

            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Depth, "ByPrice");
            if ((!fromWSP) && (!Settings.isConnected())) {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                return null;
            } else if ((!ExchangeStore.isValidIinformationType(SharedMethods.getExchangeFromKey(selectedKey), Meta.IT_MarketDepthByPrice)) && (!fromWSP)) {
                SwingUtilities.invokeLater(new Runnable() {
                    public void run() {
                        new ShowMessage("<html><b>" + Language.getString("MARKET_DEPTH_BY_PRICE") + "&nbsp;&nbsp;</b><br>" +
                                Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
                    }
                });
                return null;
            }
            if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(selectedKey))) {
                SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(selectedKey));
                return null;
            }
            if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(selectedKey))) {
                SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(selectedKey));
                return null;
            }
            if (ExchangeStore.isDelayed(SharedMethods.getExchangeFromKey(selectedKey))) {
                SharedMethods.showExchangeDelayedmessage(SharedMethods.getExchangeFromKey(selectedKey));
                return null;
            }

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            if (!isLinked) {
                InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.FULL_QUOTE_VIEW + "|" +
                        selectedKey);
                if (oFrame != null) {
                    oFrame.setIcon(false);
                    oFrame.show();
                    oFrame.setSelected(true);
                    oFrame.getTableComponent().getTable().requestFocus();
                    return oFrame;
                }
            }

            MultiDialogWindow depthbyPrice = new MultiDialogWindow(g_oWindowDaemon, selectedKey, fromWSP, isLinked, linkGroup);
            if (depthbyPrice != null) {
                return depthbyPrice;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        return null;
    }


    public InternalFrame depthByPriceNormal(final String key, boolean fromWSP, final byte parentWindowType, boolean isLinked, String linkGroup) {

        try {
            final String selectedKey;

            if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
                if (key == null) {
                    selectedKey = getSelectedSymbol();
                } else {
                    if (SharedMethods.isFullKey(key))
                        selectedKey = key;
                    else
                        selectedKey = DataStore.getSharedInstance().findKey(key);
                }
            } else {
                selectedKey = key;
            }

            if (selectedKey.equals(Constants.NULL_STRING))
                return null;

            if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Depth, "ByPrice");
                if ((!fromWSP) && (!Settings.isConnected())) {
                    new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                    return null;
                } else if ((!ExchangeStore.isValidIinformationType(SharedMethods.getExchangeFromKey(selectedKey), Meta.IT_MarketDepthByPrice)) && (!fromWSP)) {
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            new ShowMessage("<html><b>" + Language.getString("MARKET_DEPTH_BY_PRICE") + "&nbsp;&nbsp;</b><br>" +
                                    Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
                        }
                    });
                    return null;
                }
                if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(selectedKey))) {
                    SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(selectedKey));
                    return null;
                }
                if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(selectedKey))) {
                    SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(selectedKey));
                    return null;
                }
                if (ExchangeStore.isDelayed(SharedMethods.getExchangeFromKey(selectedKey))) {
                    SharedMethods.showExchangeDelayedmessage(SharedMethods.getExchangeFromKey(selectedKey));
                    return null;
                }
            }

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            if (parentWindowType != Constants.FULL_QUOTE_TYPE && !isLinked) {
                InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" +
                        selectedKey);
                if (oFrame != null) {
                    oFrame.setIcon(false);
                    oFrame.show();
                    oFrame.setSelected(true);
                    oFrame.getTableComponent().getTable().requestFocus();
                    return oFrame;
                }
            }

            // Create the Bid Market Depth Table
            Table bidTable = new Table();
            Table askTable = new Table();

            DepthByPriceNormalFrame depthbyPrice = new DepthByPriceNormalFrame(Constants.NULL_STRING, g_oWindowDaemon, bidTable, new Table[]{askTable}, selectedKey, fromWSP, parentWindowType, isLinked, linkGroup);
            if (depthbyPrice != null) {
                return depthbyPrice;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

        return null;
    }

    public void depthSpecialByOrder(String key, boolean fromWSP, boolean isLinked, String linkGroup) {
        if (!fromWSP) SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Depth, "Special");
        try {
//            if ((!fromWSP) && (!Settings.isConnected())) {
//                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
//                return;
//            } else if ((!Settings.isValidSystemWindow(Meta.WT_MarketDepthWindow)) && (!fromWSP)) {
//                new ShowMessage("<html><b>" + Language.getString("SPECIAL_ORDER_BOOK") + "&nbsp;&nbsp;</b><br>" +
//                        Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
//                return;
//            }
            String selectedKey;
            boolean loadedFromWorkspace = false;
            byte stockDecimalPlaces = 2;

            if (key == null) {
                selectedKey = getSelectedSymbol();
            } else {
                if (SharedMethods.isFullKey(key))
                    selectedKey = key;
                else
                    selectedKey = DataStore.getSharedInstance().findKey(key);
            }

            if (selectedKey.equals(Constants.NULL_STRING))
                return;

            try {
                stockDecimalPlaces = SharedMethods.getDecimalPlaces(selectedKey);
            } catch (Exception e) {
                stockDecimalPlaces = 2;
            }


            if ((!fromWSP) && (!Settings.isConnected())) {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                return;
            } else if ((!ExchangeStore.isValidIinformationType(SharedMethods.getExchangeFromKey(selectedKey), Meta.IT_SpecialOrderBook)) && (!fromWSP)) {
                new ShowMessage("<html><b>" + Language.getString("SPECIAL_ORDER_BOOK") + "&nbsp;&nbsp;</b><br>" +
                        Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
                return;
            }
            if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(selectedKey))) {
                SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(selectedKey));
                return;
            }
            if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(selectedKey))) {
                SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(selectedKey));
                return;
            }
            if (ExchangeStore.isDelayed(SharedMethods.getExchangeFromKey(selectedKey))) {
                SharedMethods.showExchangeDelayedmessage(SharedMethods.getExchangeFromKey(selectedKey));
                return;
            }

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            if (!isLinked) {
                InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.DEPTH_SPECIAL_ASK_VIEW + "|" +
                        selectedKey);
                if (oFrame != null) {
                    oFrame.setIcon(false);
                    oFrame.show();
                    oFrame.setSelected(true);
                    oFrame.getTableComponent().getTable().requestFocus();
                    return;
                }
            }
            ViewSetting oBidViewSetting = null;
            ViewSetting oAskViewSetting = null;

            // Create the Bid Market Depth Table
            Table tableBid = new Table();
            Table tableAsk = new Table();
            DepthSpecialByOrderFrame depthspecial = new DepthSpecialByOrderFrame(Constants.NULL_STRING, g_oWindowDaemon, tableBid, new Table[]{tableAsk}, selectedKey, fromWSP, isLinked, linkGroup);
            if (depthspecial != null) {
                return;
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showCompanyProfile() {
        Thread thread = new Thread("showCompanyProfile") {
            public void run() {
                try {
                    String key = getSelectedSymbol();
                    String symbol = SharedMethods.getSymbolFromKey(key);
                    String exchange = SharedMethods.getExchangeFromKey(key);
                    getBrowserFrame().showLoading(Constants.DOC_TYPE_COMPANY_PROFILE);
                    getBrowserFrame().setTitle(Language.getString("COMPANY_PROFILE"));
                    SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, Meta.COMPANY_PROFILE + Meta.DS + exchange + Meta.FD + symbol +
                            Meta.FD + Language.getLanguageTag() + Meta.EOL, exchange);

                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };
        thread.start();
    }

    /**
     * *******All methods calls from the default template ******************************************
     */
    public void showChart(final String symbol,
                          final String settings/* woekspace settings */,
                          final String id/* workspace id */) {
        showChart(symbol, settings, id, true, true);  // //
    }

    public void showChartFromWSP(final String symbol,
                                 final String settings/* woekspace settings */,
                                 final String id/* workspace id */) {
        showChart(symbol, settings, id, true, true);

    }

    public void showChartLinkedToSideBarOld(final String symbol,
                                            final String settings/* woekspace settings */,
                                            final String id/* workspace id */) {

        GraphFrameSymbolwise graphframe = new GraphFrameSymbolwise(null, null, symbol, settings, id, true, true, false);
        if (graphframe != null) {
            LinkStore.getSharedInstance().addFrameTolinkedGroup(graphframe, LinkStore.LINK_NONE);
        }
    }

    public void showChartLinkedToSideBar(String symbol,
                                         final String settings/* woekspace settings */,
                                         final String id/* workspace id */) {

        /*************check symbol validity********************************************************************************/
        if (id == null) { // not loading from a workspace. must have a symbol
            if (symbol == null) {
                symbol = getSelectedSymbol();
                if (!symbol.equals("")) {
                    if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)))) {
                        SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)));
                        return;
                    }
                    if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)))) {
                        SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)));
                        return;
                    }
                } else {
                    new ShowMessage(false, Language.getString("SYMBOL_NOT_FOUND"), "E");
                    return;
                }
            } else {
                if (!symbol.equals("")) {
                    if (ExchangeStore.isExpired((SharedMethods.getExchangeFromKey(symbol)))) {
                        SharedMethods.showExchangeExpiryMesage((SharedMethods.getExchangeFromKey(symbol)));
                        return;
                    }
                    if (ExchangeStore.isInactive((SharedMethods.getExchangeFromKey(symbol)))) {
                        SharedMethods.showExchangeInactiveMesage((SharedMethods.getExchangeFromKey(symbol)));
                        return;
                    }
                } else {
                    new ShowMessage(false, Language.getString("SYMBOL_NOT_FOUND"), "E");
                    return;
                }

            }
        }
        /*************check symbol validity********************************************************************************/
        ChartFrame cf = ChartFrame.getSharedInstance();
        if (cf.getTotalChartCount() >= cf.getMaxChartCount()) {
            String message = "Graph limit exceeded. Maximum %s graphs allowed.";
            message = String.format(message, cf.getMaxChartCount());
            JOptionPane.showConfirmDialog(this.getDesktop(), message, "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
        } else {
            GraphFrameSymbolwise graphframe = new GraphFrameSymbolwise(null, null, symbol, settings, id, true, true, false);
            if (graphframe != null) {
                LinkStore.getSharedInstance().addFrameTolinkedGroup(graphframe, LinkStore.LINK_NONE);
            }
            cf.setTotalChartCount(cf.getTotalChartCount() + 1);
        }
    }

    public void showChart(String symbol,
                          String settings/* woekspace settings */,
                          String id,/* workspace id */
                          boolean mode, /*true - intraday, history otherwise*/boolean usingDefTemplate) {

        /*************check symbol validity********************************************************************************/
        if (id == null) { // not loading from a workspace. must have a symbol
            if (symbol == null) {
                symbol = getSelectedSymbol();
                if (!symbol.equals("")) {
                    if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)))) {
                        SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)));
                        return;
                    }
                    if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)))) {
                        SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)));
                        return;
                    }
                } else {
                    showProChart();
                    return;
                }
            } else {
                if (!symbol.equals("")) {
                    if (ExchangeStore.isExpired((SharedMethods.getExchangeFromKey(symbol)))) {
                        SharedMethods.showExchangeExpiryMesage((SharedMethods.getExchangeFromKey(symbol)));
                        return;
                    }
                    if (ExchangeStore.isInactive((SharedMethods.getExchangeFromKey(symbol)))) {
                        SharedMethods.showExchangeInactiveMesage((SharedMethods.getExchangeFromKey(symbol)));
                        return;
                    }
                } else {
                    new ShowMessage(false, Language.getString("SYMBOL_NOT_FOUND"), "E");
                    SharedMethods.printLine("tracing not found ", true);
                    return;
                }

            }
        }
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Chart, "Symbol");
        ChartFrame cf = ChartFrame.getSharedInstance();
        if (cf.getTotalChartCount() >= cf.getMaxChartCount()) {
            String message = "Graph limit exceeded. Maximum %s graphs allowed.";
            message = String.format(message, cf.getMaxChartCount());
            JOptionPane.showConfirmDialog(this.getDesktop(), message, "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
        } else {
            GraphFrameSymbolwise graphframe = new GraphFrameSymbolwise(null, null, symbol, settings, id, mode, usingDefTemplate, showInProChart);
            cf.setTotalChartCount(cf.getTotalChartCount() + 1);
        }
        showInProChart = false;
    }


    public void showChartOld(String symbol,
                             String settings/* woekspace settings */,
                             String id,/* workspace id */
                             boolean mode, /*true - intraday, history otherwise*/boolean usingDefTemplate) {

        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Chart, "Symbol");
        GraphFrameSymbolwise graphframe = new GraphFrameSymbolwise(null, null, symbol, settings, id, mode, usingDefTemplate, false);
        if (graphframe != null) {
            return;
        }

//        ViewSetting defaultSettings;
//        GraphFrame graph = new GraphFrame(null);
//        graph.setDetached(true);
//
//        oTopDesktop.add(graph);
//        graph.setResizable(true);
//        graph.setMaximizable(true);
//        graph.setIconifiable(true);
//        graph.setClosable(true);
//        try {
//            graph.setSelected(true);
//        } catch (Exception ex) {
//        }
//
//        if (id == null) { // creating a new chart
//            defaultSettings = g_oViewSettings.getSymbolView("OUTER_CHART").getObject();
//            if (defaultSettings != null) {
//                defaultSettings.setParent(graph);
//                graph.setViewSettings(defaultSettings);
//                graph.setSize(defaultSettings.getSize());
//                GUISettings.setLocationRelativeTo(graph, oTopDesktop);
//                defaultSettings.setID("" + System.currentTimeMillis());
//                graph.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//            }
//
//        } else { // loading from workspace
//            defaultSettings = g_oViewSettings.getChartView(ViewSettingsManager.OUTER_CHART + "|" + id);
//            defaultSettings.setParent(graph);
//            graph.setViewSettings(defaultSettings);
//            defaultSettings.setID(id);
//            graph.applySettings();
//            if (settings != null) {
////                GraphStream.setBasicChartProperties(settings, graph);
//                LayoutFactory.LoadLayout(
//                        new File(defaultSettings.getProperty(ViewConstants.VC_CHART_LAYOUT)), null, true, graph, true);
//            }
//            //graph.applySettings();
//        }
//
//        if (id == null) { // not loading from a workspace. must have a symbol
//            //****************************intertupt to sending ohlc requeist should be done only when a new chart is created
//
//            if (symbol == null) {
//                graph.isSendOHLCRequest = false;
//                symbol = getSelectedSymbol();
//                if (!symbol.equals("")) {
//                    if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)))) {
//                        SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)));
//                        return;
//                    }
//                    if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)))) {
//                        SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(SharedMethods.getExchangeFromKey(symbol)));
//                        return;
//                    }
//                    graph.popupGraphData(symbol);
//                } else {
//                    new ShowMessage(false, Language.getString("SYMBOL_NOT_FOUND"), "E");
//                    return;
//                }
//            } else {
//                if (!symbol.equals("")) {
//                    if (ExchangeStore.isExpired((SharedMethods.getExchangeFromKey(symbol)))) {
//                        SharedMethods.showExchangeExpiryMesage((SharedMethods.getExchangeFromKey(symbol)));
//                        return;
//                    }
//                    if (ExchangeStore.isInactive((SharedMethods.getExchangeFromKey(symbol)))) {
//                        SharedMethods.showExchangeInactiveMesage((SharedMethods.getExchangeFromKey(symbol)));
//                        return;
//                    }
//                    /* Symbol selected from the main board */
//            graph.popupGraphData(symbol);
//                } else {
//                    new ShowMessage(false, Language.getString("SYMBOL_NOT_FOUND"), "E");
//                    return;
//                }
//
//            }
//            graph.setGraphMode(mode, false); //high priority this is not the correct way. need to finalize with sathyagith
//        }
//
//
//        graph.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        defaultSettings = null;
//        graph.show();
//        if (ChartSettings.getSettings().isOpenInProChart() && id == null) { //TODO
//            graph.toggleDetach();
//        }
//        graph = null;
    }

// --Commented out by Inspection START (3/29/05 2:28 PM):
//    public void createChart(String settings) {
//        GraphFrame graph = new GraphFrame(null);
//        oTopDesktop.add(graph);
//        graph.setResizable(true);
//        graph.setMaximizable(true);
//        graph.setIconifiable(true);
//        graph.setClosable(true);
//        GraphStream.setBasicChartProperties(settings, graph);
//        graph.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        graph.setLayer(GUISettings.TOP_LAYER);
//        graph.show();
//        graph = null;
//    }
// --Commented out by Inspection STOP (3/29/05 2:28 PM)

    public void addDepthRequest(String requestID, String key, short type) {
        String request;
        String removeRequest;

        String symbol = SharedMethods.getSymbolFromKey(key);
        String exchange = SharedMethods.getExchangeFromKey(key);
        if (ExchangeStore.isExpired(exchange) || ExchangeStore.isInactive(exchange)) {
            return;
        }
        int instrumentType = SharedMethods.getInstrumentTypeFromKey(key);
        try {
            Stock stock = DataStore.getSharedInstance().getStockObject(key);
            if ((stock != null) && (stock.getMarketCenter() != null)) {
                if (symbol.endsWith("." + stock.getMarketCenter())) {
                    symbol = symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter()));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        request = Meta.ADD_SYMBOL_REQUEST + Meta.DS + type + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType;
        removeRequest = Meta.REMOVE_SYMBOL_REQUEST + Meta.DS + type + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType;
//        DataStore.getSharedInstance().addSymbolRequest(key);
//        DataStore.getSharedInstance().removeSymbolRequest(key);
        RequestManager.getSharedInstance().addAddRequest(ExchangeStore.getSharedInstance().getDataPath(exchange), requestID, request);
        RequestManager.getSharedInstance().addRemoveRequest(ExchangeStore.getSharedInstance().getDataPath(exchange), requestID, removeRequest);

    }

    public void addTradeRequest(String requestID, String key) {
        String request;
        String removeRequest;
        String symbol = SharedMethods.getSymbolFromKey(key);
        String exchange = SharedMethods.getExchangeFromKey(key);
        if (ExchangeStore.isExpired(exchange) || ExchangeStore.isInactive(exchange)) {
            return;
        }
        int instrumentType = SharedMethods.getInstrumentTypeFromKey(key);
        String marketCenter = "";
        try {
            Stock stock = DataStore.getSharedInstance().getStockObject(key);
            marketCenter = stock.getMarketCenter();
            if ((stock != null) && (stock.getMarketCenter() != null)) {
                if (symbol.endsWith("." + stock.getMarketCenter())) {
                    symbol = symbol.substring(0, symbol.lastIndexOf("." + stock.getMarketCenter()));
                }
            }
            if (marketCenter == null) {
                marketCenter = "";
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        request = Meta.ADD_SYMBOL_REQUEST + Meta.DS + Meta.COMBINED_TRADE + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType + Meta.FD + marketCenter;        //Meta.COMBINED_TRADE
        removeRequest = Meta.REMOVE_SYMBOL_REQUEST + Meta.DS + Meta.COMBINED_TRADE + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType;   //Meta.COMBINED_TRADE   Meta.MESSAGE_TYPE_COMBINED_TRADE

        RequestManager.getSharedInstance().addAddRequest(ExchangeStore.getSharedInstance().getDataPath(exchange), requestID, request);
        RequestManager.getSharedInstance().addRemoveRequest(ExchangeStore.getSharedInstance().getDataPath(exchange), requestID, removeRequest);

    }

    /**
     * Time and sales option is selected Modified - 02/07/02 Bandula
     */
    public void mnu_MarketTimeNSales_ActionPerformed() {
        try {
            String exchange;
            if (ExchangeStore.getSharedInstance().defaultCount() > 1) {
                ExchangeSelectorDialog selectorDialog = new ExchangeSelectorDialog(Language.getString("SELECT_TIMEnSALES_EXCHANGE"), 0);//0 for selection type, 0 represents Market T&S
                exchange = selectorDialog.getExchange();
                selectorDialog = null;
            } else {
                exchange = ExchangeStore.getSharedInstance().getDefaultExchange().getSymbol();
            }
            if (exchange != null) {
                InternalFrame frame = mainBoardLoader.getTimeNSalesFrame(exchange);
                if (frame.isVisible()) {
                    if (frame.isIcon()) {
                        frame.setIcon(false);
                        frame.setSelected(true);
                    } else if (!frame.isSelected()) {
                        frame.setSelected(true);
                    } else {
                        frame.setVisible(false);
                    }
                } else {
                    frame.setVisible(true);
                    String requestID = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + exchange;
                    addMarketTradeRequest(requestID, exchange);
                    frame.setDataRequestID(TradeStore.getSharedInstance(), exchange, requestID);
                    SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.TimeNSales, "Market");
                    //SendQFactory.addAddRequest(exchange, null, Meta.COMBINED_TRADE); // register for time and sales
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addSR6Request(String requestID,String exchange, int type){

        String request = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + type + Meta.FD + exchange;
        RequestManager.getSharedInstance().sendAddRequest(ExchangeStore.getSharedInstance().getDataPath(exchange), requestID, request);
        /*try{

        outPeriodic.write(request);
        outPeriodic.write("\n");
        outPeriodic.flush();
        }catch(Exception exp2){
            exp2.printStackTrace();
        }*/
    }

    public void removeSR6Request(String exchange, String symbol, int type, int instrumentType, String requestID){

        String request = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + type + Meta.FD + exchange;
        RequestManager.getSharedInstance().sendAddRequest(ExchangeStore.getSharedInstance().getDataPath(exchange), requestID, request);
    }

    public void addMarketTradeRequest(String requestID, String exchange) {
        String request;
        String removeRequest;
        if (ExchangeStore.isExpired(exchange) || ExchangeStore.isInactive(exchange)) {
            return;
        }
        request = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + Meta.COMBINED_TRADE + Meta.FD + exchange;

        removeRequest = Meta.REMOVE_EXCHANGE_REQUEST + Meta.DS + Meta.COMBINED_TRADE + Meta.FD + exchange;

        RequestManager.getSharedInstance().sendAddRequest(ExchangeStore.getSharedInstance().getDataPath(exchange), requestID, request);
        RequestManager.getSharedInstance().addRemoveRequest(ExchangeStore.getSharedInstance().getDataPath(exchange), requestID, removeRequest);
    }

    public void removeMarketTimeNSalesRequest(String exchange) {
        if (Settings.isConnected()) {
            SendQFactory.addRemoveRequest(exchange, null, Meta.COMBINED_TRADE, -1, null); // register for time and sales
        }
    }

    /**
     * Time and sales option is selected Modified - 02/07/02 Bandula
     */
    public void mnu_FullTimeNSales_ActionPerformed() {
        try {
            if (Settings.getMarketStatus() == Settings.MARKET_OPEN) {
                ShowMessage message = new ShowMessage(Language.getString("ALL_TRADES_DOWNLOAD_WARNING_MESSAGE"), "W", true);
                if (message.getReturnValue() != 1) {
                    return;
                }
            }

            /* Download for which exchange */
            String exchange;
            if (ExchangeStore.getSharedInstance().defaultCount() > 1) {
                ExchangeSelectorDialog selectorDialog = new ExchangeSelectorDialog(Language.getString("SELECT_TIMEnSALES_DOWNLOAD_EXCHANGE"));
                exchange = selectorDialog.getExchange();
                selectorDialog = null;
            } else {
                exchange = ExchangeStore.getSharedInstance().getDefaultExchange().getSymbol();
            }

            if (exchange != null) {
                DetailedTradeStore.getSharedInstance().activateCurrentDataDownload(exchange);
                if (g_oFullTimeSalesFrame == null) {
                    g_oFullTimeSalesFrame = createFullTimeSalesFrame();
                }
                g_oFullTimeSalesFrame.setLayer(GUISettings.TOP_LAYER);
                if (!g_oFullTimeSalesFrame.isVisible()) {
                    g_oFullTimeSalesFrame.setVisible(true);
                } else {
                    g_oFullTimeSalesFrame.setSelected(true);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showDetailedTrades() {
        if (g_oFullTimeSalesFrame == null) {
            g_oFullTimeSalesFrame = createFullTimeSalesFrame();
        }
        g_oFullTimeSalesFrame.show();
    }

    public void updateDetailedTrades() {
        if (g_oFullTimeSalesFrame != null) {
            g_oFullTimeSalesFrame.getTable1().updateUI();
        }
    }

    /**
     * Market Summary option is selected Modified - 02/07/02 Bandula
     */
    public void mnu_ExchangeSummary_ActionPerformed() {
        if (g_oExchangeFrame == null) {
            g_oExchangeFrame = createExchangesFrame();
        }
        if (g_oExchangeFrame.isVisible()) {
            try {
                if (g_oExchangeFrame.isIcon()) {
                    g_oExchangeFrame.setIcon(false);
                    g_oExchangeFrame.setSelected(true);
                } else if (!g_oExchangeFrame.isSelected()) {
                    g_oExchangeFrame.setSelected(true);
                } else {
                    g_oExchangeFrame.setVisible(false);
                }
            } catch (Exception e) {
            }
        } else {
            g_oExchangeFrame.setVisible(true);
            SharedMethods.updateGoogleAnalytics("MarketSummary");
        }
        g_oExchangeFrame.addInternalFrameListener(new InternalFrameListener() {
            public void internalFrameOpened(InternalFrameEvent e) {


            }

            public void internalFrameClosing(InternalFrameEvent e) {
                MarketModel.removeSymbolRequests();
            }

            public void internalFrameClosed(InternalFrameEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void internalFrameIconified(InternalFrameEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void internalFrameDeiconified(InternalFrameEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void internalFrameActivated(InternalFrameEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void internalFrameDeactivated(InternalFrameEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
    }

    /*public void mnu_MarketSummary_ActionPerformed() {
           if (g_oMarketFrame.isVisible()) {
               try {
                   if (g_oMarketFrame.isIcon()) {
                       g_oMarketFrame.setIcon(false);
                       g_oMarketFrame.setSelected(true);
                   } else if (!g_oMarketFrame.isSelected()) {
                       g_oMarketFrame.setSelected(true);
                   } else {
                       g_oMarketFrame.setVisible(false);
                   }
               } catch (Exception e) {
               }
           } else {
               g_oMarketFrame.setVisible(true);
           }
       }*/

    /**
     * Market Indices option is selected Modified - 02/07/02 Bandula
     */
    public void mnu_GlobalIndex_ActionPerformed() {
        GlobalIndexWindow.getSharedInstance().showFrame();

    }

    public void mnu_MarketIndices_ActionPerformed() {
//        mainBoardLoader.indexFrame.show();
        if (g_oIndicesFrame == null) {
//            g_oIndicesFrame = mainBoardLoader.createIndicesFrame();
            g_oIndicesFrame = IndicesFrame.getSharedInstance();//mainBoardLoader.createIndicesFrame();
            g_oIndicesFrame.popuplateExchanges();
        }
        if (g_oIndicesFrame.isVisible()) {
            try {
                if (g_oIndicesFrame.isIcon()) {
                    g_oIndicesFrame.setIcon(false);
                    g_oIndicesFrame.showWindow();
                    g_oIndicesFrame.setSelected(true);
                } else if (!g_oIndicesFrame.isSelected()) {
                    g_oIndicesFrame.setSelected(true);
                } else {
                    g_oIndicesFrame.setVisible(false);
                }
            } catch (Exception e) {
            }
        } else {
            g_oIndicesFrame.showWindow();
            SharedMethods.updateGoogleAnalytics("MarketIndices");
        }
    }


    //change start

    public void customIndexStoreModified() {
        if (mainBoardLoader != null && mainBoardLoader.getIndicesModel() != null) {
            mainBoardLoader.getIndicesModel().exchageModified();
            mainBoardLoader.getIndicesModel().adjustRows();
            if (g_oIndicesFrame != null) {
                g_oIndicesFrame.repaint();
            } else if (g_oGlobalIndexFrame != null) {
                g_oGlobalIndexFrame.repaint();
            }
        }
    }
    //change end

    public void mnu_OrderQ_ActionPerformed() {
        if (orderQFrame == null) {
            orderQFrame = createOrderQFrame();
        }
        if (orderQFrame.isVisible()) {
            try {
                if (orderQFrame.isIcon()) {
                    orderQFrame.setIcon(false);
                    orderQFrame.setSelected(true);
                } else if (!orderQFrame.isSelected()) {
                    orderQFrame.setSelected(true);
                } else {
                    orderQFrame.setVisible(false);
                }
            } catch (Exception e) {
            }
        } else {
            orderQFrame.setVisible(true);
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "BasketOrders");
        }
    }

    public void mnu_ProgOrders_ActionPerformed() {
        if (programedOrderFrame == null) {
            programedOrderFrame = createProgramedOrderFrame();
        }
        if (programedOrderFrame.isVisible()) {
            try {
                if (programedOrderFrame.isIcon()) {
                    programedOrderFrame.setIcon(false);
                    programedOrderFrame.setSelected(true);
                } else if (!programedOrderFrame.isSelected()) {
                    programedOrderFrame.setSelected(true);
                } else {
                    programedOrderFrame.setVisible(false);
                }
            } catch (Exception e) {
            }
        } else {
            programedOrderFrame.setVisible(true);
        }
    }

    public void mnu_Bloter_ActionPerformed() {
//        This was commented inorder to reduce the amount of requests generate
//        new BlotterFrame().fireUpdateData();
        if (TradingShared.isReadyForTrading()) {
            if (blotterFrame == null) {
                blotterFrame = TradeMethods.getSharedInstance().createBlotterFrame();
            }
            if (blotterFrame.isVisible()) {
                try {
                    if (blotterFrame.isIcon()) {
                        blotterFrame.setIcon(false);
                        blotterFrame.setSelected(true);
                    } else if (!blotterFrame.isSelected()) {
                        blotterFrame.setSelected(true);
                    } else {
                        blotterFrame.setVisible(false);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else {
                blotterFrame.setVisible(true);
                // Added by Shanaka
                TradeMethods.requestPortfolioData(Constants.PATH_PRIMARY);
                TradeMethods.requestAccountData(Constants.PATH_PRIMARY);
                if (TradingShared.SECONDARY_LOGIN_SUCCESS) {
                    TradeMethods.requestPortfolioData(Constants.PATH_SECONDARY);
                    TradeMethods.requestAccountData(Constants.PATH_SECONDARY);
                }
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "OrderList");
            }
//            new BlotterFrame().fireUpdateData();
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }
    }

    public void mnuOpenPositionsActionPerformed() {
        //if (TradingShared.isReadyForTrading()) {
        if (openPositionsFrame == null) {
            openPositionsFrame = TradeMethods.getSharedInstance().createOpenPositionsFrame();
        }
        if (openPositionsFrame.isVisible()) {
            try {
                if (openPositionsFrame.isIcon()) {
                    openPositionsFrame.setIcon(false);
                    openPositionsFrame.setSelected(true);
                } else if (!openPositionsFrame.isSelected()) {
                    openPositionsFrame.setSelected(true);
                } else {
                    openPositionsFrame.setVisible(false);
                }
            } catch (Exception e) {
            }
        } else {
            openPositionsFrame.setVisible(true);
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "OpenPositions");
        }
        //} else {
        //    SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        //}
    }

    public void mnu_StockTrans_ActionPerformed() {
        if (TradingShared.isReadyForTrading()) {
            if (stockTransferWindow == null) {
                stockTransferWindow = TradeMethods.getSharedInstance().createStockTranferFrame();
            }
            if (stockTransferWindow.isVisible()) {
                try {
                    if (stockTransferWindow.isIcon()) {
                        stockTransferWindow.setIcon(false);
                        stockTransferWindow.setSelected(true);
                    } else if (!stockTransferWindow.isSelected()) {
                        stockTransferWindow.setSelected(true);
                    } else {
                        stockTransferWindow.setVisible(false);
                    }
                } catch (Exception e) {
                }
            } else {
                stockTransferWindow.setVisible(true);
            }
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }
    }

    public void mnu_KeyFunction_ActionPerformed() {
//        // ExportDialog oExportDialog = new ExportDialog(window, false);
//        KeyFunctionUI keyFunction = new KeyFunctionUI(window, false);
//        getDesktop().add(keyFunction);
//        keyFunction.setLocationRelativeTo(Client.getInstance().getDesktop());
//        keyFunction.setVisible(true);
//        keyFunction = null;

        ShortCutViewer keyFunction = ShortCutViewer.getInstance();
        getDesktop().add(keyFunction);
        keyFunction.setLocationRelativeTo(Client.getInstance().getDesktop());
        keyFunction.setVisible(true);
        keyFunction = null;
    }

    public void mnu_BrokerList_ActionPerformed() {
        if (brokersWindow == null) {
            brokersWindow = BrokersWindow.getSharedInstance();
        }
        if (brokersWindow.isVisible()) {
            try {
                if (brokersWindow.isIcon()) {
                    brokersWindow.setIcon(false);
                    brokersWindow.setSelected(true);
                } else if (!brokersWindow.isSelected()) {
                    brokersWindow.setSelected(true);
                } else {
                    brokersWindow.setVisible(false);
                }
            } catch (Exception e) {
            }
        } else {
            brokersWindow.setVisible(true);
        }
    }

    /**
     * News option is selected
     */
    public void mnu_News_ActionPerformed() {
        //new part
        if (NEWSFrame == null) {
            createNewsFrame();
        }
        NEWSFrame.selectRealTimeNews();
        NEWSFrame.setDataStore(true);
        NEWSFrame.updateUI();
        NEWSFrame.setDefaultValues();
        if (NEWSFrame.isVisible()) {
            try {
                if (NEWSFrame.isIcon()) {
                    NEWSFrame.setIcon(false);
                    NEWSFrame.setSelected(true);
                } else if (!NEWSFrame.isSelected()) {
                    NEWSFrame.setSelected(true);
                } else {
                    NEWSFrame.setVisible(false);
                }
            } catch (Exception e) {
            }
        } else {
            NEWSFrame.setVisible(true);
        }
    }

    public void mnu_PerformaceHive_ActionPerformed() {
        PerformaceHiveWindow.getSharedInstance().setVisible(true);
    }


    public void mnu_CorporateAction_ActionPerformed(String key) {
        if (CAFrame == null) {
            createCAFrame();
        }
        CAFrame.setDataStore(true);
        CAFrame.updateUI();
        CAFrame.setDefaultValues();
        CAFrame.setTitle(SharedMethods.getSymbolFromKey(key) + " - " + Language.getString("CORPORATE_ACTIONS"));
        if (CAFrame.isVisible()) {
            try {
                if (CAFrame.isIcon()) {
                    CAFrame.setIcon(false);
                    CAFrame.setSelected(true);
                } else if (!CAFrame.isSelected()) {
                    CAFrame.setSelected(true);
                } else {
                    CAFrame.setVisible(false);
                }
            } catch (Exception e) {
            }
        } else {
            CAFrame.setVisible(true);
        }
        //newsFrame.setLocationRelativeTo(this.getDesktop());
    }


    public void toggleAnnouncementWindow() {
        if (announcementFrame == null) {
            createAnnouncementFrame();
        }
        AnnouncementSearchWindow.getSharedInstance().selectRealtimeAnnoucement();
        if (announcementFrame.isVisible()) {
            try {
                if (announcementFrame.isIcon()) {
                    announcementFrame.setIcon(false);
                    announcementFrame.setSelected(true);
                } else if (!announcementFrame.isSelected()) {
                    announcementFrame.setSelected(true);
                } else {
                    announcementFrame.setVisible(false);
                }
            } catch (Exception e) {
            }
        } else {
            announcementFrame.setVisible(true);
        }
    }

    public void toggleSectorMapWindow() {
        if (sectorMapFrame == null) {
            createSectorMap();
        }
        if (sectorMapFrame.isVisible()) {
            try {
                if (sectorMapFrame.isIcon()) {
                    sectorMapFrame.setIcon(false);
                    sectorMapFrame.setSelected(true);
                } else if (!sectorMapFrame.isSelected()) {
                    sectorMapFrame.setSelected(true);
                } else {
                    sectorMapFrame.setVisible(false);
                }
            } catch (Exception e) {
            }
        } else {
            sectorMapFrame.setVisible(true);
        }
    }

    public void toggleBroadcastMessageWindow() {
        /*if (broadcastMessageFrame == null) {
            createBrodMessageFrame();
        }*/
        InternalFrame broadcastMessageFrame = MessageWindow.getSharedInstance();
        if (broadcastMessageFrame.isVisible()) {
            try {
                if (broadcastMessageFrame.isIcon()) {
                    broadcastMessageFrame.setIcon(false);
                    broadcastMessageFrame.setSelected(true);
                } else if (!broadcastMessageFrame.isSelected()) {
                    broadcastMessageFrame.setSelected(true);
                } else {
                    broadcastMessageFrame.setVisible(false);
                }
            } catch (Exception e) {
            }
        } else {
            broadcastMessageFrame.setVisible(true);
        }
    }

    public void focusAnnouncementWindow() {
        if (announcementFrame == null) {
            createAnnouncementFrame();
        }
        AnnouncementSearchWindow.getSharedInstance().selectRealtimeAnnoucement();
        if (announcementFrame.isVisible()) {
            try {
                if (announcementFrame.isIcon()) {
                    announcementFrame.setIcon(false);
                    announcementFrame.show();
                } else if (!announcementFrame.isSelected()) {
                    announcementFrame.show();
                }
            } catch (Exception e) {
            }
        } else {
            announcementFrame.setVisible(true);
        }
    }

    public void toggleNewsWindow() {
//        if(newsFrame ==null){
//            createNewsFrame();
//        }
//        newsFrame.setDataStore(false);
//        newsFrame.updateUI();
//        newsFrame.setDefaultValues();
//        if (newsFrame.isVisible()) {
//            try {
//                if (newsFrame.isIcon()) {
//                    newsFrame.setIcon(false);
//                    newsFrame.setSelected(true);
//                } else if (!newsFrame.isSelected()) {
//                    newsFrame.setSelected(true);
//                } else {
//                    newsFrame.setVisible(false);
//                }
//            } catch (Exception e) {
//            }
//        } else {
//            newsFrame.setVisible(true);
//        }
//        newsFrame.setLocationRelativeTo(this.getDesktop());
        //new part
        if (NEWSFrame == null) {
            createNewsFrame();
        }
        NEWSFrame.setDataStore(false);
        NEWSFrame.updateUI();
        NEWSFrame.setDefaultValues();
        if (NEWSFrame.isVisible()) {
            try {
                if (NEWSFrame.isIcon()) {
                    NEWSFrame.setIcon(false);
                    NEWSFrame.setSelected(true);
                } else if (!NEWSFrame.isSelected()) {
                    NEWSFrame.setSelected(true);
                } else {
                    NEWSFrame.setVisible(false);
                }
            } catch (Exception e) {
            }
        } else {
            NEWSFrame.setVisible(true);
        }
        NEWSFrame.setLocationRelativeTo(this.getDesktop());
    }


    public void focusNewsWindow() {
//        if(newsFrame ==null){
//            createNewsFrame();
//        }
//        if (newsFrame.isVisible()) {
//            try {
//                if (newsFrame.isIcon()) {
//                    newsFrame.setIcon(false);
//                    newsFrame.show();
//                } else if (!newsFrame.isSelected()) {
//                    newsFrame.show();
//                }
//            } catch (Exception e) {
//            }
//        } else {
//            newsFrame.setVisible(true);
//        }
        //new part
        if (NEWSFrame == null) {
            createNewsFrame();
        }
        NEWSFrame.selectRealTimeNews();
        if (NEWSFrame.isVisible()) {
            try {
                if (NEWSFrame.isIcon()) {
                    NEWSFrame.setIcon(false);
                    NEWSFrame.show();
                } else if (!NEWSFrame.isSelected()) {
                    NEWSFrame.show();
                }
            } catch (Exception e) {
            }
        } else {
            NEWSFrame.setVisible(true);
        }

    }


    public void mnu_MutualFund_ActionPerformed() {

        if (isValidSystemWindow(Meta.WT_MutualFund, false)) {
            if (mutualFundtFrame == null) {
                mutualFundtFrame = createMutualFundFrame();
            }
            if (mutualFundtFrame.isVisible()) {
                try {
                    if (mutualFundtFrame.isIcon()) {
                        mutualFundtFrame.setIcon(false);
                        mutualFundtFrame.setSelected(true);
                    } else if (!mutualFundtFrame.isSelected()) {
                        mutualFundtFrame.setSelected(true);
                    } else {
                        mutualFundtFrame.setVisible(false);
                    }
                } catch (Exception e) {
                }
            } else {
                mutualFundtFrame.setVisible(true);
            }
        }
    }

    public void showCurrencyFrame() {
        if (currencyFrame == null) {
            currencyFrame = createCurrencyFrame();
        }
        if (currencyFrame.isVisible()) {
            try {
                if (currencyFrame.isIcon()) {
                    currencyFrame.setIcon(false);
                    currencyFrame.setSelected(true);
                } else if (!currencyFrame.isSelected()) {
                    currencyFrame.setSelected(true);
                }
            } catch (Exception e) {
            }
        } else {
            currencyFrame.setVisible(true);
            ((SmartTable) currencyFrame.getTable1()).adjustColumnWidthsToFit(20);
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Currency, "View");
        }
    }

    public void showAnnouncementSearchWIndow(String key, long from, long end) {

        if (announcementFrame == null) {
            createAnnouncementFrame();
        }
        AnnouncementSearchWindow.getSharedInstance().selectAnnoucementSearch();
        AnnouncementSearchWindow.getSharedInstance().setSearchDates(from, end);
        AnnouncementSearchWindow.getSharedInstance().searchForSymbol(key, from, end);
    }

    public void searchAnnouncements(String key, long start, long end) {

        SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
        String symbol = SharedMethods.getSymbolFromKey(key);
        String exchange = SharedMethods.getExchangeFromKey(key);

//        String searchString = Meta.ANNOUNCEMENT_SEARCH + Meta.DS + Meta.TYPE_SYMBOL + Meta.FD +
//                exchange + Meta.FD + symbol + Meta.FD + dateFormat.format(new Date(start)) + Meta.FD + dateFormat.format(new Date(end));
//
//        System.out.println(searchString);

        SearchedAnnouncementStore.getSharedInstance().initSearch();
//        SearchedAnnouncementStore.getSharedInstance().doSearch(searchString, Language.getLanguageTag(), Integer.MAX_VALUE, exchange);

        SearchedAnnouncementStore.getSharedInstance().disableButtons();
        showAnnouncementSearchWIndow(key, start, end);
        setAnnouncementSearchTitle(symbol, Language.getString("SEARCHING"));
    }

    public void setAnnouncementSearchTitle(String symbol, String title) {
        String caption = Language.getString("ANNOUNCEMENTS");

        if ((symbol != null) && (!symbol.equals("*"))) {
            //if (equity) {
            Stock stock = DataStore.getSharedInstance().getStockObject(symbol);
            if (stock != null) {
                caption = caption + " - " + stock.getLongDescription();
                AnnouncementSearchPanel.getSharedInstance().setCompanyName(stock.getLongDescription());
                stock = null;
            }
            // } else {
//                if (symbol.equals("TADAWUL")) {
//                    caption = caption + " - " + Language.getString("MARKET");
//                    AnnouncementSearchPanel.getSharedInstance().setCompanyName(Language.getString("MARKET"));
//                } else {
//            caption = caption + " - " + IndexStore.getDescription(symbol);
//            AnnouncementSearchPanel.getSharedInstance().setCompanyName(IndexStore.getDescription(symbol));
//                }
            //}
        } else {
            String oldCaption = AnnouncementSearchPanel.getSharedInstance().getCompanyName();
            if (oldCaption != null)
                caption = caption + " - " + oldCaption;
        }

        if (title != null) {
            caption = caption + " - " + title;
        }
        if (announcementSearchFrame == null) {
            announcementSearchFrame = createAnnouncementSearchFrame();
        }
        announcementSearchFrame.setTitle(caption);
    }

    /*public void setNewsSearchTitle(String symbol, boolean equity, String title) {

    }*/

    public void showNewsSearchWIndow() {
//        if(newsFrame ==null){
//            createNewsFrame();
//        }
//        newsFrame.setDataStore(false);
//        if (newsFrame.isVisible()) {
//            try {
//                if (newsFrame.isIcon()) {
//                    newsFrame.setIcon(false);
//                    newsFrame.setSelected(true);
//                } else if (!newsFrame.isSelected()) {
//                    newsFrame.setSelected(true);
//                }
//            } catch (Exception e) {
//            }
//        } else {
//            newsFrame.setVisible(true);
//        }
//        newsFrame.setLocationRelativeTo(this.getDesktop());
        ///////////////////////////////////////////////////////////////// new part
        if (NEWSFrame == null) {
            createNewsFrame();
        }
        NEWSFrame.selectNewsSearch();
        NEWSFrame.setDataStore(false);
        if (NEWSFrame.isVisible()) {
            try {
                if (NEWSFrame.isIcon()) {
                    NEWSFrame.setIcon(false);
                    NEWSFrame.setSelected(true);
                } else if (!NEWSFrame.isSelected()) {
                    NEWSFrame.setSelected(true);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            NEWSFrame.setVisible(true);
        }
//        NEWSFrame.setLocationRelativeTo(this.getDesktop());
    }


    public void setTickerOrIndexPanel(String sID) {
    }


    public void setNewsSearchTitle(String title) {
        if (NEWSFrame == null) {
            createNewsFrame();
        }
        NEWSFrame.selectNewsSearch();
        if (title != null) {
            NEWSFrame.setTitle(Language.getString("NEWS") + " - " + title);
        } else {
            NEWSFrame.setTitle(Language.getString("NEWS"));
        }
        NEWSFrame.updateSearchedTable();
    }

    /**
     * Export history option is selected
     */
    public void mnu__Export_ActionPerformed() {
        ExportDialog oExportDialog = new ExportDialog(window, false);
        getDesktop().add(oExportDialog);
        oExportDialog.setLocationRelativeTo(Client.getInstance().getDesktop());
        oExportDialog.setVisible(true);
        oExportDialog = null;

    }

    public String getTickerOrIndexPanel() {
        if (g_bTickerEnabled)
            return "TICKER";
        else
            return "INDICES";
    }

    public Ticker getTicker() {
        return g_oTicker;
    }

    public UpperTicker getUpperTicker() {
        return g_uTicker;
    }

    public MiddleTicker getMiddleTicker() {
        return g_mTicker;
    }

    public LowerTicker getLowerTicker() {
        return g_lTicker;
    }

// --Commented out by Inspection START (3/29/05 2:32 PM):
//    public void mnu_switchPanel_ActionPerformed() {
//    }
// --Commented out by Inspection STOP (3/29/05 2:32 PM)

    public BrowserFrame getBrowserFrame() {
        if (g_oBrowserFrame == null) {
            createBrowserFrame();
        }
        return g_oBrowserFrame;
    }

    public IEBrowser getWebBrowserFrame() {
        if (jIE == null) {
            jIE = new IEBrowser();           //IEBrowser.MODE_NORMAL
            jIE.setSize(new Dimension(760, 560));
            GUISettings.setLocationRelativeTo(jIE, getFrame());
        }
        return jIE;
    }

    /**
     *
     */
    private void customizeFileChooser() {

        UIManager.put("FileChooser.acceptAllFileFilterText", Language.getString("ALL_FILES"));
        UIManager.put("FileChooser.cancelButtonText", Language.getString("CANCEL"));
        UIManager.put("FileChooser.cancelButtonToolTipText", Language.getString("CANCEL"));
        UIManager.put("FileChooser.detailsViewButtonToolTipText", Language.getString("DETAILS"));
        UIManager.put("FileChooser.homeFolderToolTipText", Language.getString("MY_DOCUMENTS"));
        UIManager.put("FileChooser.fileNameLabelText", Language.getString("FILENAME"));
        UIManager.put("FileChooser.filesOfTypeLabelText", Language.getString("FILE_FORMAT"));
        UIManager.put("FileChooser.listViewButtonToolTipText", Language.getString("LIST"));
        UIManager.put("FileChooser.lookInLabelText", Language.getString("LOOK_IN"));
        UIManager.put("FileChooser.saveInLabelText", Language.getString("SAVE_IN"));
        UIManager.put("FileChooser.newFolderToolTipText", Language.getString("NEW_FOLDER"));
        UIManager.put("FileChooser.openButtonText", Language.getString("OPEN_FILE"));
        UIManager.put("FileChooser.openButtonToolTipText", Language.getString("OPEN_FILE"));
        UIManager.put("FileChooser.directoryOpenButtonButtonText", Language.getString("OPEN_FILE"));
        UIManager.put("FileChooser.directoryOpenButtonToolTipText", Language.getString("OPEN_FILE"));
        UIManager.put("FileChooser.saveButtonText", Language.getString("SAVE"));
        UIManager.put("FileChooser.saveButtonToolTipText", Language.getString("SAVE"));
        UIManager.put("FileChooser.saveDialogTitleText", Language.getString("SAVE"));
        UIManager.put("FileChooser.upFolderToolTipText", Language.getString("UP_FOLDER"));
//        UIManager.put("FileChooser.listViewBackground", Color.RED); 


    }

    private void customizeColorChooser() {
        UIManager.put("ColorChooser.cancelText", Language.getString("CANCEL"));
        UIManager.put("ColorChooser.okText", Language.getString("OK"));
        UIManager.put("ColorChooser.resetText", Language.getString("RESET"));
        UIManager.put("ColorChooser.rgbBlueText", Language.getString("BLUE"));
        UIManager.put("ColorChooser.rgbGreenText", Language.getString("GREEN"));
        UIManager.put("ColorChooser.rgbRedText", Language.getString("RED"));
        UIManager.put("ColorChooser.swatchesRecentText", Language.getString("RECENT"));
        UIManager.put("ColorChooser.swatchesNameText", Language.getString("PREDEFINED"));
    }

    /**
     * Sets the status bar message
     */
    public static void setStatus(String sStatus) {
        lblStatus.setText(sStatus);
    }

    public static void setExpiryStatus(String sStatus, boolean critical) {
        criticalState = false;
        String userType;
        if (Settings.getUserPaymentType() == Meta.USER_PAYMENT_TYPE_TRIAL) {
            userType = Language.getString("TRIAL_USER");
        } else if (Settings.getUserPaymentType() == Meta.USER_PAYMENT_TYPE_DEMO) {
            userType = Language.getString("DEMO_USER");
        } else {
            userType = "";
        }
        if (critical) {
            lblExpiryStatus.setForeground(Theme.getColor("EXPIRY_DATE_CRITICAL_COLOR"));
            criticalState = true;
        } else {
            lblExpiryStatus.setForeground(Theme.getColor("EXPIRY_DATE_NORMAL_COLOR"));
            criticalState = false;
        }
        lblExpiryStatus.setText(userType + " " + sStatus);
    }

    public static void setLoyaltyPoints(String points) {
        NumberFormat amountFormat = NumberFormat.getNumberInstance();
        loyaltyPoints.setForeground(Theme.getColor("EXPIRY_DATE_CRITICAL_COLOR"));
        String result = Language.getString("LOYALTY_MESSAGE");
        result = result.replaceAll("\\[POINTS\\]", amountFormat.format(Integer.parseInt(points)));
        loyaltyPoints.setFont(lblExpiryStatus.getFont().deriveFont(Font.BOLD));
        loyaltyPoints.setText(result);
    }

    public static void removeLoyaltyPoints() {
        loyaltyPoints.setText("");
    }

    public static void setStatusProgress(int value) {
        lblStatus.setValue(value);
        lblStatus.repaint();
    }

    public static void setDownloadStatusMaxProgress(int value) {
        lblDownloadStatus.setMaximum(value);
    }

    public static void setDownloadStatusProgress(int value) {
        lblDownloadStatus.setValue(value);
        lblDownloadStatus.repaint();
    }

    public static void setStatusMaxProgress(int value) {
        lblStatus.setMaximum(value);
    }

    public void setConnectionStatus(String sStatus) {
        setConnectionStatus(sStatus, true);
    }

    public void setConnectionStatus(String sStatus, boolean showMessageDialog) {
        g_sConnectionStatus = sStatus;
        setStatus(sStatus);
        setPriceConnectionBulbStatus();
        if ((showMessageDialog) && (oMessageDialog != null)) {
            /*if (((sStatus != null) && sStatus.equals(Language.getString("NOT_CONNECTED"))) && TradingShared.isConnected()){
                oMessageDialog.setMessage(Language.getString("MSG_PRICE_DISCONNECTION_WARNING"));
            }else {*/
            oMessageDialog.setMessage(sStatus);
            /*}*/
        }
        setTitleText();
    }

    public String getConnectionStatus() {
        return g_sConnectionStatus;
    }


    public void mnuShowPageFormat() {

        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                PageFormat pFormat = null;
                PrinterJob pJob = PrinterJob.getPrinterJob();
                if (Settings.getPageFormat() == null) {
                    pFormat = pJob.defaultPage();
                    Settings.setPageFormat(pFormat);
                } else {
                    pFormat = Settings.getPageFormat();
                }
                pFormat = pJob.pageDialog(pFormat);
                if (pFormat != null) {
                    Settings.setPageFormat(pFormat);
                    SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.System, "PageSetup");
                }
            }
        });
    }


    public void setTitleText() {
        String sStatus = getConnectionStatus();
        String sWorkSpace = null;

        if (sStatus != null)
            sStatus = " - " + sStatus;
        else
            sStatus = "";

        if (g_sWorkspaceFile != null) {
            try {
                sWorkSpace = " [" + g_sWorkspaceFile.substring(g_sWorkspaceFile.lastIndexOf("\\") + 1) + "] ";
            } catch (Exception e) {
                sWorkSpace = "";
            }

        } else {
            sWorkSpace = "";
        }
        String broker = "";
        try {
            broker = TradingShared.getBrokerID();
        } catch (Exception e) {
            broker = "";
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        if ((!sStatus.equals(Language.getString("NOT_CONNECTED"))) &&
                (!sStatus.equals(Language.getString("OBTAINING_IP"))) &&
                (Settings.getActiveIP() != null)) {
            String cmID = Settings.getCMID();
            if (cmID != null) {
//                window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + sStatus + " [" + Settings.getActiveIP()+ " : " + cmID + "] " + sWorkSpace);
                if (TradingShared.isOkForTradeConnection() && !TWControl.isEnableWebLogin()) {
                    if (TradingShared.isConnecting()) {
                        String str = Language.getString("CONNECTING_TO_TRADING");
                        str = str.replace("[BROKER]", broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                        str = str.replace("[BROKER]", Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " [" + Settings.getActiveIP() + " : " + cmID + "] " + " - " + str + " - " + sWorkSpace);
                        str = null;
                    } else if (TradingShared.isConnected()) {
                        String str = Language.getString("CONNECTED_TO_BROKER");
                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " [" + Settings.getActiveIP() + " : " + cmID + "] " + " - " + str + " - " + sWorkSpace);
                        str = null;
                    } else if (TradingShared.SECONDARY_LOGIN_SUCCESS) {
                        String str = Language.getString("CONNECTED_TO_BROKER_SECONDARY");
                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " [" + Settings.getActiveIP() + " : " + cmID + "] " + " - " + str + " - " + " - " + str + " - " + sWorkSpace);
                        str = null;
                    } else {
                        if (!MWTradingHandler.getSharedInstance().isVisible()) {
                            window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " [" + Settings.getActiveIP() + " : " + cmID + "] " + " - " + Language.getString("NOT_CONNECTED_TO_TRADING") + " - " + sWorkSpace);
                        } else {
                            window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " [" + Settings.getActiveIP() + " : " + cmID + "] ");
                        }
                    }
                } else {
                    window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " [" + Settings.getActiveIP() + " : " + cmID + "] " + " - " + sWorkSpace);
                }
            } else {
//                window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + sStatus + " [" + Settings.getActiveIP() + "] " + sWorkSpace);
                if (TradingShared.isOkForTradeConnection() && !TWControl.isEnableWebLogin()) {
                    if (TradingShared.isConnecting()) {
                        String str = Language.getString("CONNECTING_TO_TRADING");
                        str = str.replace("[BROKER]", broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                        str = str.replace("[BROKER]", Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " [" + Settings.getActiveIP() + "] " + " - " + str + " - " + sWorkSpace);
                        str = null;
                    } else if (TradingShared.isConnected()) {
                        String str = Language.getString("CONNECTED_TO_BROKER");
                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " [" + Settings.getActiveIP() + "] " + " - " + str + " - " + sWorkSpace);
                        str = null;
                    } else if (TradingShared.SECONDARY_LOGIN_SUCCESS) {
                        String str = Language.getString("CONNECTED_TO_BROKER_SECONDARY");
                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " [" + Settings.getActiveIP() + "] " + " - " + str + " - " + " - " + str + " - " + sWorkSpace);
                        str = null;
                    } else {
                        if (!MWTradingHandler.getSharedInstance().isVisible()) {
                            window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " [" + Settings.getActiveIP() + "] " + " - " + Language.getString("NOT_CONNECTED_TO_TRADING") + " - " + sWorkSpace);
                        } else {
                            window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " [" + Settings.getActiveIP() + "] ");
                        }
                    }
                } else
                    window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " [" + Settings.getActiveIP() + "] " + " - " + sWorkSpace);
            }
        } else {
//            window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + sStatus + sWorkSpace);
//            if(TradingShared.isOkForTradeConnection()){
//                window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + sStatus + " - " + Language.getString("NOT_CONNECTED_TO_TRADING") + " - "  + sWorkSpace);
            if (TradingShared.isOkForTradeConnection() && !TWControl.isEnableWebLogin()) {
                if (TradingShared.isConnecting()) {
                    String str = Language.getString("CONNECTING_TO_TRADING");
                    str = str.replace("[BROKER]", broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                    str = str.replace("[BROKER]", Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                    window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " - " + str + " - " + sWorkSpace);
                    str = null;
                } else if (TradingShared.isConnected()) {
                    String str = Language.getString("CONNECTED_TO_BROKER");
                    str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                    str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                    window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " - " + str + " - " + sWorkSpace);
                    str = null;
                } else if (TradingShared.SECONDARY_LOGIN_SUCCESS) {
                    String str = Language.getString("CONNECTED_TO_BROKER_SECONDARY");
                    str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                    str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                    window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " - " + str + " - " + sWorkSpace);
                    str = null;
                } else {
                    if (!MWTradingHandler.getSharedInstance().isVisible()) {
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " - " + Language.getString("NOT_CONNECTED_TO_TRADING") + " - " + sWorkSpace);
                    } else {
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus);
                    }
                }
            } else {
                window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION_NUMBER + sStatus + " - " + sWorkSpace);
            }
        }
        //change end
    }

    //-- If neeed to change title without brackets

    /*  public void setTitleText() {
        String sStatus = getConnectionStatus();
        String sWorkSpace = null;

        if (sStatus != null)
            sStatus = " - " + sStatus;
        else
            sStatus = "";

        if (g_sWorkspaceFile != null) {
            try {
                sWorkSpace = " [" + g_sWorkspaceFile.substring(g_sWorkspaceFile.lastIndexOf("\\") + 1) + "] ";
            } catch (Exception e) {
                sWorkSpace = "";
            }

        } else {
            sWorkSpace = "";
        }
        String broker = "";
        try {
            broker = TradingShared.getBrokerID();
        } catch (Exception e) {
            broker = "";
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        if ((!sStatus.equals(Language.getString("NOT_CONNECTED"))) &&
                (!sStatus.equals(Language.getString("OBTAINING_IP"))) &&
                (Settings.getActiveIP() != null)) {
            String cmID = Settings.getCMID();
            if (cmID != null) {
//                window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + sStatus + " [" + Settings.getActiveIP()+ " : " + cmID + "] " + sWorkSpace);
                if (TradingShared.isOkForTradeConnection()) {
                    if (TradingShared.isConnecting()) {
                        String str = Language.getString("CONNECTING_TO_TRADING");
                        str = str.replace("[BROKER]", broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                        str = str.replace("[BROKER]", Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus + " - "+ Settings.getActiveIP() + " : " + cmID + " - " + str + " - " + sWorkSpace);
                        str = null;
                    } else if (TradingShared.isConnected()) {
                        String str = Language.getString("CONNECTED_TO_BROKER");
                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus + " - "+ Settings.getActiveIP() + " : " + cmID +  " - " + str + " - " + sWorkSpace);
                        str = null;
                    } else if (TradingShared.SECONDARY_LOGIN_SUCCESS) {
                        String str = Language.getString("CONNECTED_TO_BROKER_SECONDARY");
                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus + " - "+ Settings.getActiveIP() + " : " + cmID +  " - " + str + " - " + " - " + str + " - " + sWorkSpace);
                        str = null;
                    } else {
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus +" - "+ Settings.getActiveIP() + " : " + cmID + " - " + Language.getString("NOT_CONNECTED_TO_TRADING") + " - " + sWorkSpace);
                    }
                } else {
                    window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus +" - "+  Settings.getActiveIP() + " : " + cmID +  " - " + sWorkSpace);
                }
            } else {
//                window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + sStatus + " [" + Settings.getActiveIP() + "] " + sWorkSpace);
                if (TradingShared.isOkForTradeConnection()) {
                    if (TradingShared.isConnecting()) {
                        String str = Language.getString("CONNECTING_TO_TRADING");
                        str = str.replace("[BROKER]", broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                        str = str.replace("[BROKER]", Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus +" - "+ Settings.getActiveIP() +" - " + str + " - " + sWorkSpace);
                        str = null;
                    } else if (TradingShared.isConnected()) {
                        String str = Language.getString("CONNECTED_TO_BROKER");
                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus + " - "+ Settings.getActiveIP() +  " - " + str + " - " + sWorkSpace);
                        str = null;
                    } else if (TradingShared.SECONDARY_LOGIN_SUCCESS) {
                        String str = Language.getString("CONNECTED_TO_BROKER_SECONDARY");
                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                        str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus + " - "+ Settings.getActiveIP() + " - " + str + " - " + " - " + str + " - " + sWorkSpace);
                        str = null;
                    } else {
                        window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus  + " - "+Settings.getActiveIP() +  " - " + Language.getString("NOT_CONNECTED_TO_TRADING") + " - " + sWorkSpace);
                    }
                } else
                    window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus +" - "+ Settings.getActiveIP() + " - " + sWorkSpace);
            }
        } else {
//            window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + sStatus + sWorkSpace);
//            if(TradingShared.isOkForTradeConnection()){
//                window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + sStatus + " - " + Language.getString("NOT_CONNECTED_TO_TRADING") + " - "  + sWorkSpace);
            if (TradingShared.isOkForTradeConnection()) {
                if (TradingShared.isConnecting()) {
                    String str = Language.getString("CONNECTING_TO_TRADING");
                    str = str.replace("[BROKER]", broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                    str = str.replace("[BROKER]", Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                    window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus + " - " + str + " - " + sWorkSpace);
                    str = null;
                } else if (TradingShared.isConnected()) {
                    String str = Language.getString("CONNECTED_TO_BROKER");
                    str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                    str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                    window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus + " - " + str + " - " + sWorkSpace);
                    str = null;
                } else if (TradingShared.SECONDARY_LOGIN_SUCCESS) {
                    String str = Language.getString("CONNECTED_TO_BROKER_SECONDARY");
                    str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + broker);//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
//                    str = str.replace("[BROKER]", TradingShared.getTrsID() + " : " + Brokers.getSharedInstance().getSelectedBroker().getDecription());//Brokers.getSharedInstance().getBrokerName(Settings.getItem("TRADE_BROKER")));
                    window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus + " - " + str + " - " + sWorkSpace);
                    str = null;
                } else {
                    window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus + " - " + Language.getString("NOT_CONNECTED_TO_TRADING") + " - " + sWorkSpace);
                }
            } else {
                window.setTitle(Language.getString("WINDOW_TITLE_CLIENT") + " " + Settings.TW_VERSION + sStatus + " - " + sWorkSpace);
            }
        }
        //change end
    }*/


    /**
     * Sets the status for downloading
     */
    public static void setDownloadStatusOn(int type) {
        /*if (type == Meta.HISTORY)
            lblDownloadStatus.setText(Language.getString("STATUS_HISTORY_DOWNLOAD"));
        else if (type == Meta.OHLC)
            lblDownloadStatus.setText(Language.getString("STATUS_OHLC_DOWNLOAD"));
        else if (type == Meta.TRADES)
            lblDownloadStatus.setText(Language.getString("STATUS_TRADE_DOWNLOAD"));
        else
            lblDownloadStatus.setText(Settings.NULL_STRING);*/
    }

    public static void setDownloadStatus(String text) {
        lblDownloadStatus.setText(text);
    }

    /**
     * Toggles the chat available icon if the
     * iCount is greater then zero
     */
//    public static void setChatAvailable(int iCount)
//    {
//
//    }

    /**
     * Sets the status to receveing snapshots mode
     */
    public void setPriceConnectionBulbStatus() {
        if (Settings.isConnected() && Settings.isUSConnectionSuccess()) {
            g_oMenus.getPriceConnectiontBulb().setImageFileName("green_bulb.gif");
            g_oMenus.getPriceConnectiontBulb().setToolTipText(Language.getString("PRICE_CONNECTED"));
        } else if (Settings.isConnected()) {
            if (ExchangeStore.isSecondaryPathConnectionRequired()) {
                g_oMenus.getPriceConnectiontBulb().setImageFileName("green_red_bulb.gif");
            } else {
                g_oMenus.getPriceConnectiontBulb().setImageFileName("green_bulb.gif");
                g_oMenus.getPriceConnectiontBulb().setToolTipText(Language.getString("PRICE_CONNECTED"));
            }
        } else if (Settings.isUSConnectionSuccess()) {
            g_oMenus.getPriceConnectiontBulb().setImageFileName("red_green_bulb.gif");
        } else {
            g_oMenus.getPriceConnectiontBulb().setImageFileName("red_bulb.gif");
            g_oMenus.getPriceConnectiontBulb().setToolTipText(Language.getString("PRICE_DISCONNECTED"));
        }
    }

//    public void setPriceConnectionSucess() {
//        g_oMenus.getPriceConnectiontBulb().setImageFileName("green_bulb.gif");
//    }
//
//    /**
//     * Sets the status to not receveing snapshots mode
//     */
//    public void setPriceConnectionDown() {
//        g_oMenus.getPriceConnectiontBulb().setImageFileName("red_bulb.gif");
//    }

    /**
     * Sets the status to receveing snapshots mode
     */
    public void setTradeConnectionBulbStatus() {
        if (TradingShared.isConnected() && TradingShared.SECONDARY_LOGIN_SUCCESS && !TradingShared.INVALID_SECONDARY_LOGIN) {
            g_oMenus.getTradeConnectiontBulb().setImageFileName("green_bulb.gif");
            g_oMenus.getTradeConnectiontBulb().setToolTipText(Language.getString("TRADE_CONNECTED"));
        } else if (TradingShared.isConnected()) {
            if (TradingShared.isSecondaryLoginEnabled()) {
                g_oMenus.getTradeConnectiontBulb().setImageFileName("green_red_bulb.gif");
            } else {
                g_oMenus.getTradeConnectiontBulb().setImageFileName("green_bulb.gif");
                g_oMenus.getTradeConnectiontBulb().setToolTipText(Language.getString("TRADE_CONNECTED"));
            }
        } else {
            g_oMenus.getTradeConnectiontBulb().setImageFileName("red_bulb.gif");
            g_oMenus.getTradeConnectiontBulb().setToolTipText(Language.getString("TRADE_DISCONNECTED"));
        }
    }

//    public void setTradeConnectionSucess() {
//        g_oMenus.getTradeConnectiontBulb().setImageFileName("green_bulb.gif");
//    }

    /**
     * Sets the status to not receveing snapshots mode
     */
//    public void setUSConnectionDown() {
//        g_oMenus.getUSConnectiontBulb().setImageFileName("red_bulb.gif");
//    }
//
//    /**
//     * Sets the status to receveing snapshots mode
//     */
//    public void setUSConnectionSucess() {
//        g_oMenus.getUSConnectiontBulb().setImageFileName("green_bulb.gif");
//    }

    /**
     * Sets the status to not receveing snapshots mode
     */
//    public void setTradeConnectionDown() {
//        g_oMenus.getTradeConnectiontBulb().setImageFileName("red_bulb.gif");
//    }

    /**
     * Sets the status to receveing snapshots mode
     */
    public void setNotAuthenticated() {
        g_oMenus.getPriceConnectiontBulb().setImageFileName("yellow_bulb.gif");
    }

    /**
     * Sets the status to receveing snapshots mode
     */
    public void setRecevingSnapshots() {
        g_oMenus.getPriceConnectiontBulb().setImageFileName("green_bulb.gif");
        g_oMenus.getPriceConnectiontBulb().setToolTipText(Language.getString("PRICE_CONNECTED"));
    }

// --Commented out by Inspection START (3/29/05 2:33 PM):
//    /**
//    * Sets the status to not receveing snapshots mode
//    */
//    public void setNotRecevingSnapshots()
//    {
//    	g_oMenus.getPriceConnectiontBulb().setIcon(new ImageIcon("images/Common/red_bulb.gif"));
//    }
// --Commented out by Inspection STOP (3/29/05 2:33 PM)

    // Added By Bandula - 13 - 10 - 2001
    // Set the Market Offline Status
//    private void setMarketOfflineStatus() {
//        switch (HistorySettings.getMarketStatus()) {
//            case 1:
//                marketStatus = 1;
//                setMarketPreOpen();
//                break;
//            case 2:
//                marketStatus = 2;
//                setMarketOpen();
//                break;
//            case 3:
//                marketStatus = 3;
//                setMarketClose();
//                break;
//            case 4:
//                marketStatus = 4;
//                setMarketPreClose();
//                break;
//        }
//    }

    /**
     * Sets the market preopen status
     */
//    public void setMarketPreOpen() {
//        marketStatus = 1;
//        g_oMenus.setMarketStatus(Language.getString("STATUS_PREOPEN"), Theme.getColor("MARKET_STATUS_PREOPEN_COLOR"));
//    }
//
//    /**
//     * Sets the market open status
//     */
//    public void setMarketOpen() {
//        marketStatus = 2;
//        g_oMenus.setMarketStatus(Language.getString("STATUS_OPEN"), Theme.getColor("MARKET_STATUS_OPEN_COLOR"));
//    }
//
//    /**
//     * Sets the market close status
//     */
//    public void setMarketClose() {
//        marketStatus = 3;
//        g_oMenus.setMarketStatus(Language.getString("STATUS_CLOSE"), Theme.getColor("MARKET_STATUS_CLOSED_COLOR"));
//    }
//
//    /**
//     * Sets the market close status
//     */
//    public void setMarketPreClose() {
//        marketStatus = 4;
//        g_oMenus.setMarketStatus(Language.getString("STATUS_PRECLOSE"), Theme.getColor("MARKET_STATUS_PRECLOSED_COLOR"));
//    }

    /**
     * Sets the market time
     */
    public static void setMarketTime(String sTime) {
        g_lMarketTime = (new Long(sTime));
    }

    /*public static void setActiveMarketDate(String date) {
        MARKET_DATE = date;

        Calendar cal = Calendar.getInstance();
        cal.set(Calendar.YEAR, Integer.parseInt(date.substring(0, 4)));
        cal.set(Calendar.MONTH, Integer.parseInt(date.substring(4, 6)) - 1);
        cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(date.substring(6, 8)));
        cal.set(Calendar.HOUR_OF_DAY, 0);
        cal.set(Calendar.MINUTE, 0);
        cal.set(Calendar.SECOND, 0);
        cal.set(Calendar.MILLISECOND, 0);
        MARKET_TIME = cal.getTime().getTime();
        cal = null;
    }*/

    public void setSelectedTable(ClientTable oTable) {
        mainboardSelected = true;
        g_oTempTable = oTable;
    }

    public boolean isMainBoardSelected() {
        return mainboardSelected;
    }

    public ClientTable getSelectedTable() {
        return g_oTempTable;
    }

    public JDesktopPane getDesktop() {
        return oTopDesktop;
    }


    // Sets the Last Connected Time

    private static void setLastConnectedTime() {
        //String dateString = Settings.NULL_STRING; //formatter.format(currentTime_1);
        long lLastConndate = 0;

        /* If never connected before, show nothing for the
            last connected date */
        try {
            lLastConndate = Long.parseLong(HistorySettings.getLastConnectedDate());
            g_lMarketTime = lLastConndate;
        } catch (Exception e) {
            lLastConndate = 0;
        }
        if (lLastConndate == Settings.NULL_DATE)
            return;

        try {
//            g_lblMarketTime.setFont(new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.PLAIN, 12));
//            showMarketTime();
        } catch (Exception e) {
            e.printStackTrace();
//            g_lblMarketTime.setText(Settings.NULL_STRING);
        }
    }

    /**
     * Update the market time
     */
    /*public static void updateMarketTime() {
        if (g_lMarketTime > 0)// && (!Settings.isMarketClosed()))
        {
            g_lMarketTime += 1000;
            showMarketTime();
        }
    }*/

    /*public static void showMarketTime() {
        g_lblMarketTime.setText(getFormattedmarketTime(g_lMarketTime));
    }*/
    public static String getFormattedmarketTime(long time) {
        return getFormattedmarketTime(time, Settings.isMarketClosed());
    }

    public static String getFormattedmarketTime(long time, boolean marketClosed) {
        //SimpleDateFormat marketTimeFormatter;
        //String sTime;
        marketTime.setTime(time);

        if (marketClosed)
            return closedMarketTimeFormatter.format(marketTime);
        else
            return openMarketTimeFormatter.format(marketTime);
    }


    /**
     * Sets the current market date
     */
    public static void setCurrentdate(String sDate) {
        g_lMarketTime = Long.parseLong(sDate); // Added by Bandula  - 18-10-01
        setLastConnectedTime();
    }

    public long getMarketTime() {
        return g_lMarketTime;
    }

    public static void setLatEodDate(long lDate) {
        g_lLastEodDate = lDate;
    }

// --Commented out by Inspection START (3/29/05 2:38 PM):
//    public static long getLatEodDate() {
//        return g_lLastEodDate;
//    }
// --Commented out by Inspection STOP (3/29/05 2:38 PM)

    /**
     * Converts the given value to an int
     */
    private int intValue(String sValue) throws Exception {
        return (Integer.parseInt(sValue.trim()));
    }

    /* Window liateners */

    public void windowActivated(WindowEvent e) {

    }

    public void windowDeactivated(WindowEvent e) {


    }

    public void windowOpened(WindowEvent e) {
    }

    public void windowClosing(WindowEvent e) {
        systemExit_ActionPerformed();
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
        //added by charithn
        if (SmartZoomWindow.getObject() != null && SmartZoomWindow.getObject().isSmartZoomWindowVisible()) {
            SmartZoomWindow.getSharedInstance().setVisible(false);
        }
    }

    public void windowDeiconified(WindowEvent e) {
        window.repaint();
//        SwingUtilities.updateComponentTreeUI(window); // make sure window in painted correctly

        //added by charithn
//        if (SmartZoomWindow.getObject() != null && BottomToolBar.isSmartZoomWindowVisible) {
//            SmartZoomWindow.getSharedInstance().setVisible(true);
//        }
        if (SmartZoomWindow.getObject() != null && SmartZoomWindow.getObject().isSmartZoomWindowVisible()) {
            SmartZoomWindow.getSharedInstance().setVisible(true);
        }
    }

    public boolean isSaveCurrentView() {
        canExit = false;
        TWButton save = new TWButton(Language.getString("SAVE_N_EXIT"));
        TWButton exit = new TWButton(Language.getString("EXIT"));
        TWButton cancel = new TWButton(Language.getString("CANCEL"));

        Object options[] = {exit, save, cancel};

        JOptionPane pane = new JOptionPane();
        pane.setMessage(Language.getString("MSG_SAVE_BEFORE_EXIT"));
        pane.setOptions(options);
        pane.setMessageType(JOptionPane.WARNING_MESSAGE);

        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_SaveWSP))
            save.setEnabled(true);
        else
            save.setEnabled(false);

        exitDialog = pane.createDialog(window, Language.getString("WARNING"));
        exitDialog.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);

        save.addActionListener(this);
        save.setActionCommand("SAVE");

        cancel.addActionListener(this);
        cancel.setActionCommand("CANCEL");

        exit.addActionListener(this);
        exit.setActionCommand("EXIT");

        GUISettings.applyOrientation(exitDialog);
        exitDialog.setModal(true);
        exitDialog.setVisible(true);

        return canExit;
    }

// --Commented out by Inspection START (3/29/05 2:31 PM):
//    public boolean isExit() {
//        //String sSymbol;
//
//        ShowMessage oMessage = new ShowMessage(Language.getString("MSG_EXIT_MESSAGE"), "W", true, true);
//        int i = oMessage.getReturnValue();
//
//        if (i == 1) {
//            return true;
//        }
//        return false;
//    }
// --Commented out by Inspection STOP (3/29/05 2:31 PM)

// --Commented out by Inspection START (3/29/05 2:34 PM):
//    public void showCannotExit() {
//        ShowMessage oMessage = new ShowMessage(Language.getString("MSG_CANNOT_EXIT_MESSAGE"),
//                "W");
//        oMessage.getReturnValue();
//
//    }
// --Commented out by Inspection STOP (3/29/05 2:34 PM)

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("EXIT")) {
            canExit = true;
        } else if (e.getActionCommand().equals("SAVE")) {
            mnuSave_ActionPerformed();
        } else if (e.getActionCommand().equals("CANCEL")) {
            canExit = false;
        }
        exitDialog.dispose();
    }

// --Commented out by Inspection START (3/29/05 2:35 PM):
//    public void treeCollapsed(TreeExpansionEvent event) {
//    }
// --Commented out by Inspection STOP (3/29/05 2:35 PM)

    /*public static boolean canShowDisconnectMessage() {
        return g_bShowDisconnectMessage;
    }*/

    public static void setCanShowDisconnectMessage(boolean status) {
        g_bShowDisconnectMessage = status;
    }

// --Commented out by Inspection START (3/29/05 2:30 PM):
//    public void enableDownloadButton() {
//        g_oMenus.enableDownloadButton();
//    }
// --Commented out by Inspection STOP (3/29/05 2:30 PM)

    private void setProxySettings() {
    }

//    private boolean toBoolean(String sValue)
//    {
//        try
//        {
//            if (Integer.parseInt(sValue) == 1)
//                return true;
//            else
//                return false;
//            }
//        catch(Exception e)
//        {
//            return false;
//        }
//    }

// --Commented out by Inspection START (3/29/05 2:35 PM):
//    public void xlLink_ActionPerformed() {
//        try {
//            Runtime.getRuntime().exec("start twdata.xls");
//        } catch (Exception e2) {
//        }
//    }
// --Commented out by Inspection STOP (3/29/05 2:35 PM)


    public ViewSettingsManager getViewSettingsManager() {
        return g_oViewSettings;
    }

    protected void setColumnSettings(ViewSetting oSetting, String sSettings) throws Exception {
        StringTokenizer oTokens = new StringTokenizer(sSettings, ",");

        oSetting.setColumnSettings(null);
        if ((oTokens.countTokens() % 3) == 0) {
            int iCount = (int) (oTokens.countTokens() / 3);
            int[][] aiColumnSettings = new int[iCount][3];

            for (int i = 0; i < iCount; i++) {
                aiColumnSettings[i][0] = intValue(oTokens.nextToken());
                aiColumnSettings[i][1] = intValue(oTokens.nextToken());
                aiColumnSettings[i][2] = intValue(oTokens.nextToken());
            }
            oSetting.setColumnSettings(aiColumnSettings);
        }
        oTokens = null;
    }

    private Table table;

    private InternalFrame createAnnouncementSearchFrame() {
        final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
        final String searching = Language.getString("SEARCHING");

        table = new Table() {
            public void paint(Graphics g) {
                super.paint(g);
                if (SearchedAnnouncementStore.getSharedInstance().isSearchInProgress()) {
                    FontMetrics fontMetrics = table.getFontMetrics(getFont());
                    g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
                            (getHeight()) / 2);
                    g.drawImage(icon.getImage(),
                            (getWidth() - fontMetrics.stringWidth(searching)) / 2 + fontMetrics.stringWidth(searching) + 10,
                            (getHeight() - icon.getImage().getHeight(this)) / 2, this);
                    fontMetrics = null;
                }
            }
        };
        AnnouncementModel model = new AnnouncementModel();
        model.setDirectionLTR(Language.isLTR());
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("ANNOUNCEMENT_SEARCH");
//        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("BTW_ANNOUNCEMENT_COLS"));
        model.setViewSettings(oSetting);
        table.setModel(model);
        table.setAutoResize(false);
        AnnouncementMouseListener oSelectionListener = new AnnouncementMouseListener(table.getTable(),
                ANNOUNCMENT_TYPE.SEARCHED);
        table.getTable().getSelectionModel().addListSelectionListener(oSelectionListener);
        table.getTable().addMouseListener(oSelectionListener);
        table.getPopup().setAutoWidthAdjustMenuVisible(false);
        table.setPreferredSize(new Dimension(500, 100));
        model.setDataStore(SearchedAnnouncementStore.getSharedInstance().getStore());
        model.setTable(table);
        model.applyColumnSettings();
        table.updateGUI();
        announcementSearchFrame = new InternalFrame(table);
        announcementSearchFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oSetting.setParent(announcementSearchFrame);
        announcementSearchFrame.getContentPane().add(table);
        oTopDesktop.add(announcementSearchFrame);
        announcementSearchFrame.setResizable(true);
        announcementSearchFrame.setClosable(true);
        announcementSearchFrame.setMaximizable(true);
        announcementSearchFrame.setIconifiable(true);
        announcementSearchFrame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        announcementSearchFrame.updateUI();
        announcementSearchFrame.applySettings();
        announcementSearchFrame.setLayer(GUISettings.TOP_LAYER);
        announcementSearchFrame.setVisible(false);


        JPanel bottomPanel = new JPanel();
        String[] bottomPanelWidths = {"0", "100%", "0"};
        String[] bottonPanelHeights = {"30"};
        FlexGridLayout bottmPanelLayout = new FlexGridLayout(bottomPanelWidths, bottonPanelHeights, 5, 0, true, true);
        bottomPanel.setLayout(bottmPanelLayout);
        TWButton btnPrev = new TWButton(Language.getString("PREVIOUS"));
//        btnPrev.setGradientPainted(false);
//        btnPrev.setBorder(BorderFactory.createEmptyBorder());
        btnPrev.addActionListener(AnnouncementSearchPanel.getSharedInstance());
        btnPrev.setActionCommand("PREVIOUS");
        TWButton btnNext = new TWButton(Language.getString("NEXT"));
//        btnNext.setGradientPainted(false);
//        btnNext.setBorder(BorderFactory.createEmptyBorder());
        btnNext.addActionListener(AnnouncementSearchPanel.getSharedInstance());
        btnNext.setActionCommand("NEXT");
        JPanel centerPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 1));
//        JLabel label = new JLabel(Language.getString("SELECT_LANGUAGE"));
//        centerPanel.add(label);
        String[] saLanguages = Language.getLanguageCaptions();
        String[] saLanguageIDs = Language.getLanguageIDs();
        //JRadioButton rbEnglish = null;
        //JRadioButton rbArabic = null;
        /*try {
            rbEnglish = new JRadioButton(saLanguages[0]);
            rbEnglish.setActionCommand(saLanguageIDs[0]);
            rbEnglish.addActionListener(AnnouncementSearchPanel.getSharedInstance());
            centerPanel.add(rbEnglish);
            if (Language.getLanguageTag().equals(saLanguageIDs[0]))
                rbEnglish.setSelected(true);
            rbArabic = new JRadioButton(saLanguages[1]);
            rbArabic.setActionCommand(saLanguageIDs[1]);
            rbArabic.addActionListener(AnnouncementSearchPanel.getSharedInstance());
            centerPanel.add(rbArabic);
            if (Language.getLanguageTag().equals(saLanguageIDs[1]))
                rbArabic.setSelected(true);
            ButtonGroup group = new ButtonGroup();
            group.add(rbArabic);
            group.add(rbEnglish);
        } catch (Exception e) {
        }*/
        //TWButton btnReload = new TWButton(Language.getString("RELOAD"));
        //btnReload.setActionCommand("RELOAD");
        //btnReload.addActionListener(AnnouncementSearchPanel.getSharedInstance());
        //centerPanel.add(btnReload);
        TWButton btnSearch = new TWButton(Language.getString("SEARCH"));
        btnSearch.setActionCommand("SEARCH");
        btnSearch.addActionListener(AnnouncementSearchPanel.getSharedInstance());
        centerPanel.add(btnSearch);

        bottomPanel.add(btnPrev);
        bottomPanel.add(centerPanel);
        bottomPanel.add(btnNext);
        table.setSouthPanel(bottomPanel);

        SearchedAnnouncementStore.getSharedInstance().setButtonReferences(btnNext, btnPrev,
                /*btnReload*/ btnSearch/*, rbEnglish, rbArabic*/);
        GUISettings.applyOrientation(announcementSearchFrame);

        return announcementSearchFrame;
    }

    private void createAnnouncementFrame() {
        announcementFrame = AnnouncementSearchWindow.getSharedInstance();
    }

    public void createSectorMap() {
        SectorMap sectorMap = new SectorMap();
        sectorMapFrame = sectorMap.getFrame();
        GUISettings.applyOrientation(sectorMapFrame);
        oTopDesktop.add(sectorMapFrame);
        sectorMapFrame.setVisible(false);
    }

    private void createNewsFrame() {
//        newsFrame = CombinedNewsWindow.getSharedInstance();
//        newsFrame.setLocationRelativeTo(this.getDesktop());
//        GUISettings.applyOrientation(newsFrame);
//        oTopDesktop.add(newsFrame);
        //new part
        try {
            NEWSFrame = NewsWindow.getSharedInstance();
            NEWSFrame.setLocationRelativeTo(this.getDesktop());
            GUISettings.applyOrientation(NEWSFrame);
            NEWSFrame.setLayer(GUISettings.TOP_LAYER);
            oTopDesktop.add(NEWSFrame);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void createGlobalMarketSummary(boolean isLoadedfromWSP) {
        if (g_oGlobalMarketSummary != null && !isLoadedfromWSP) {
            GlobalMarketSummaryWindow.getSharedInstance().loadTabsFromVseting();

        }
        if (g_oGlobalMarketSummary == null) {
            g_oGlobalMarketSummary = new GlobalMarketSummary();
        }
        GlobalMarketSummaryWindow.getSharedInstance().showFrame();
        if (!isLoadedfromWSP) {
            GlobalMarketSummaryWindow.getSharedInstance().shoAllTables();

        }
    }

    private void createCAFrame() {
        CAFrame = FinancialCalenderWindow.getSharedInstance();
//        newsFrame.setVisible(true);
//        CAFrame.setLocationRelativeTo(this.getDesktop());
        GUISettings.applyOrientation(CAFrame);
        oTopDesktop.add(CAFrame);
    }

    /*private void createBrodMessageFrame() {
        MessageWindow messageWindow = MessageWindow.getSharedInstance();
        broadcastMessageFrame = messageWindow.getFrame();
        GUISettings.applyOrientation(broadcastMessageFrame);
        oTopDesktop.add(broadcastMessageFrame);
    }*/

    /*private InternalFrame createAnnouncementSearchFrame() {
        Table table = new Table();
        AnnouncementModel model = new AnnouncementModel();
        model.setDirectionLTR(Language.isLTR());
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("ANNOUNCEMENT_SEARCH");
        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("BTW_ANNOUNCEMENT_COLS"));
        model.setViewSettings(oSetting);
        table.setModel(model);
        table.setAutoResize(true);
        AnnouncementMouseListener oSelectionListener = new AnnouncementMouseListener(table.getTable(),
                ANNOUNCMENT_TYPE.SEARCHED);
        table.getTable().getSelectionModel().addListSelectionListener(oSelectionListener);
        table.getTable().addMouseListener(oSelectionListener);
        table.getPopup().setAutoWidthAdjustMenuVisible(false);
        table.setPreferredSize(new Dimension(500, 100));
        model.setDataStore(SearchedAnnouncementStore.getSharedInstance().getStore());
        model.setTable(table);
        model.applyColumnSettings();
        table.updateGUI();
        announcementSearchFrame = new InternalFrame(table);
        announcementSearchFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oSetting.setParent(announcementSearchFrame);
        announcementSearchFrame.getContentPane().add(table);
        oTopDesktop.add(announcementSearchFrame);
        announcementSearchFrame.setResizable(true);
        announcementSearchFrame.setClosable(true);
        announcementSearchFrame.setMaximizable(true);
        announcementSearchFrame.setIconifiable(true);
        announcementSearchFrame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        announcementSearchFrame.updateUI();
        announcementSearchFrame.applySettings();
        announcementSearchFrame.setLayer(GUISettings.TOP_LAYER);
        announcementSearchFrame.setVisible(false);


        JPanel bottomPanel = new JPanel();
        String[] bottomPanelWidths = {"0", "100%", "0"};
        String[] bottonPanelHeights = {"30"};
        FlexGridLayout bottmPanelLayout = new FlexGridLayout(bottomPanelWidths, bottonPanelHeights, 5, 0, true, true);
        bottomPanel.setLayout(bottmPanelLayout);
        TWButton btnPrev = new TWButton(Language.getString("PREVIOUS"));
        btnPrev.setGradientPainted(false);
        btnPrev.setBorder(BorderFactory.createEmptyBorder());
        btnPrev.addActionListener(AnnouncementSearchPanel.getSharedInstance());
        btnPrev.setActionCommand("PREVIOUS");
        TWButton btnNext = new TWButton(Language.getString("NEXT"));
        btnNext.setGradientPainted(false);
        btnNext.setBorder(BorderFactory.createEmptyBorder());
        btnNext.addActionListener(AnnouncementSearchPanel.getSharedInstance());
        btnNext.setActionCommand("NEXT");
        JPanel centerPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 1));
        JLabel label = new JLabel(Language.getString("SELECT_LANGUAGE"));
        centerPanel.add(label);
        String[] saLanguages = Language.getLanguageCaptions();
        String[] saLanguageIDs = Language.getLanguageIDs();
        JRadioButton rbEnglish = null;
        JRadioButton rbArabic = null;
        try {
            rbEnglish = new JRadioButton(saLanguages[0]);
            rbEnglish.setActionCommand(saLanguageIDs[0]);
            rbEnglish.addActionListener(AnnouncementSearchPanel.getSharedInstance());
            centerPanel.add(rbEnglish);
            if (Language.getLanguageTag().equals(saLanguageIDs[0]))
                rbEnglish.setSelected(true);
            rbArabic = new JRadioButton(saLanguages[1]);
            rbArabic.setActionCommand(saLanguageIDs[1]);
            rbArabic.addActionListener(AnnouncementSearchPanel.getSharedInstance());
            centerPanel.add(rbArabic);
            if (Language.getLanguageTag().equals(saLanguageIDs[1]))
                rbArabic.setSelected(true);
            ButtonGroup group = new ButtonGroup();
            group.add(rbArabic);
            group.add(rbEnglish);
        } catch (Exception e) {
        }
        TWButton btnReload = new TWButton(Language.getString("RELOAD"));
        btnReload.setActionCommand("RELOAD");
        btnReload.addActionListener(AnnouncementSearchPanel.getSharedInstance());
        centerPanel.add(btnReload);
        TWButton btnSearch = new TWButton(Language.getString("SEARCH"));
        btnSearch.setActionCommand("SEARCH");
        btnSearch.addActionListener(AnnouncementSearchPanel.getSharedInstance());
        centerPanel.add(btnSearch);

        bottomPanel.add(btnPrev);
        bottomPanel.add(centerPanel);
        bottomPanel.add(btnNext);
        table.setSouthPanel(bottomPanel);

        SearchedAnnouncementStore.getSharedInstance().setButtonReferences(btnNext, btnPrev,
                btnReload, btnSearch, rbEnglish, rbArabic);

        return announcementSearchFrame;
    }*

    private void createAnnouncementFrame() {
        AnnouncementWindow announcementWindow = new AnnouncementWindow();
        announcementFrame = announcementWindow.getFrame();
        oTopDesktop.add(announcementFrame);
    }

    private void createNewsFrame() {
        NewsWindow newsWindow = new NewsWindow();
        newsFrame = newsWindow.getFrame();
        oTopDesktop.add(newsFrame);
    }

    private InternalFrame createNewsSearchFrame() {
        Table table = new Table();
        NewsModel model = new NewsModel();
        model.setDirectionLTR(Language.isLTR());
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("NEWS_SEARCH");
        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("BTW_NEWS_COLS"));
        model.setViewSettings(oSetting);
        table.setModel(model);
        table.setAutoResize(true);
        NewsMouseListener oSelectionListener = new NewsMouseListener(table.getTable(),
                NEWS_TYPES.SEARCHED);
        table.getTable().getSelectionModel().addListSelectionListener(oSelectionListener);
        table.getTable().addMouseListener(oSelectionListener);
        table.getPopup().setAutoWidthAdjustMenuVisible(false);
        table.setPreferredSize(new Dimension(500, 100));
        model.setDataStore(SearchedNewsStore.getSharedInstance().getStore());
        model.setTable(table);
        model.applyColumnSettings();
        table.updateGUI();
        newsSearchFrame = new InternalFrame(table);
        newsSearchFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oSetting.setParent(newsSearchFrame);
        newsSearchFrame.getContentPane().add(table);
        oTopDesktop.add(newsSearchFrame);
        newsSearchFrame.setResizable(true);
        newsSearchFrame.setClosable(true);
        newsSearchFrame.setMaximizable(true);
        newsSearchFrame.setIconifiable(true);
        newsSearchFrame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        newsSearchFrame.updateUI();
        newsSearchFrame.applySettings();
        newsSearchFrame.setLayer(GUISettings.TOP_LAYER);
        newsSearchFrame.setVisible(false);


        JPanel bottomPanel = new JPanel();
        String[] bottomPanelWidths = {"0", "100%","0", "0"};
        String[] bottonPanelHeights = {"30"};
        FlexGridLayout bottmPanelLayout = new FlexGridLayout(bottomPanelWidths, bottonPanelHeights, 5, 5, true, true);
        bottomPanel.setLayout(bottmPanelLayout);
        TWButton btnPrev = new TWButton(Language.getString("PREVIOUS"));
        btnPrev.setGradientPainted(false);
        btnPrev.setBorder(BorderFactory.createEmptyBorder());
        //btnPrev.addActionListener(NewsSearchPanel.getSharedInstance());
        btnPrev.addActionListener(NewsSearchWindow.getInstance());
        btnPrev.setActionCommand("PREVIOUS");
        TWButton btnNext = new TWButton(Language.getString("NEXT"));
        btnNext.setGradientPainted(false);
        btnNext.setBorder(BorderFactory.createEmptyBorder());
        //btnNext.addActionListener(NewsSearchPanel.getSharedInstance());
        btnNext.addActionListener(NewsSearchWindow.getInstance());
        btnNext.setActionCommand("NEXT");
        JPanel centerPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 1));
        JLabel label = new JLabel(Language.getString("SELECT_LANGUAGE"));
        centerPanel.add(label);
        String[] saLanguages = Language.getLanguageCaptions();
        String[] saLanguageIDs = Language.getLanguageIDs();
        JRadioButton rbEnglish = null;
        JRadioButton rbArabic = null;
        try {
            rbEnglish = new JRadioButton(saLanguages[0]);
            rbEnglish.setActionCommand(saLanguageIDs[0]);
            //rbEnglish.addActionListener(NewsSearchPanel.getSharedInstance());
            rbEnglish.addActionListener(NewsSearchWindow.getInstance());
            centerPanel.add(rbEnglish);
            if (Language.getLanguageTag().equals(saLanguageIDs[0]))
                rbEnglish.setSelected(true);
            rbArabic = new JRadioButton(saLanguages[1]);
            rbArabic.setActionCommand(saLanguageIDs[1]);
            //rbArabic.addActionListener(NewsSearchPanel.getSharedInstance());
            rbArabic.addActionListener(NewsSearchWindow.getInstance());
            centerPanel.add(rbArabic);
            if (Language.getLanguageTag().equals(saLanguageIDs[1]))
                rbArabic.setSelected(true);
            ButtonGroup group = new ButtonGroup();
            group.add(rbArabic);
            group.add(rbEnglish);
        } catch (Exception e) {
        }
        TWButton btnReload = new TWButton(Language.getString("RELOAD"));
        btnReload.setActionCommand("RELOAD");
        //btnReload.addActionListener(NewsSearchPanel.getSharedInstance());
        btnReload.addActionListener(NewsSearchWindow.getInstance());
        centerPanel.add(btnReload);
        TWButton btnSearch = new TWButton(Language.getString("SEARCH"));
        btnSearch.setActionCommand("SEARCH");
        //btnSearch.addActionListener(NewsSearchPanel.getSharedInstance());
        btnSearch.addActionListener(NewsSearchWindow.getInstance());
        centerPanel.add(btnSearch);

        bottomPanel.add(btnPrev);
        bottomPanel.add(new JLabel());//centerPanel);
        bottomPanel.add(btnSearch);
        bottomPanel.add(btnNext);
        table.setSouthPanel(bottomPanel);

        SearchedNewsStore.getSharedInstance().setButtonReferences(btnNext, btnPrev,
                btnReload, btnSearch, rbEnglish, rbArabic);

        return newsSearchFrame;
    }*/

    private InternalFrame createCurrencyFrame() {
        CurrencyWindow currencyWindow = new CurrencyWindow();
        oTopDesktop.add(currencyWindow.getFrame());
        return currencyWindow.getFrame();
    }


    private InternalFrame createMutualFundFrame() {
        final Table mfTable = new Table();
        MutualFundModel mfModel = new MutualFundModel(MutualFundStore.getInstance());
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("MUTUAL_FUND");
        mfModel.setViewSettings(oSetting);
        mfTable.setSortingEnabled();
        mfTable.setModel(mfModel);
        mfTable.setAutoResize(true);
        mfTable.getPopup().setAutoWidthAdjustMenuVisible(false);
        mfTable.setPreferredSize(new Dimension(500, 100));
        MutualFundPanel mutualFundPanel = new MutualFundPanel();
        mfTable.setNorthPanel(mutualFundPanel);
        MutualFundStore.getInstance().setPanel(mutualFundPanel);

        mfModel.setTable(mfTable);
        mfModel.applyColumnSettings();
        mfTable.updateGUI();

        MutualFundStore.getInstance().setTable(mfTable);

        mutualFundtFrame = new InternalFrame(mfTable);
        mutualFundtFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        mutualFundtFrame.setColumnResizeable(true);
        mutualFundtFrame.setDetachable(true);
        mutualFundtFrame.setPrintable(true);
        oSetting.setParent(mutualFundtFrame);
        mutualFundtFrame.getContentPane().add(mfTable);
        oTopDesktop.add(mutualFundtFrame);
        mutualFundtFrame.setResizable(true);
        mutualFundtFrame.setClosable(true);
        mutualFundtFrame.setMaximizable(true);
        mutualFundtFrame.setIconifiable(true);
        mutualFundtFrame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        mutualFundtFrame.updateUI();
        mutualFundtFrame.applySettings();
        mutualFundtFrame.setLayer(GUISettings.TOP_LAYER);
        mutualFundtFrame.setVisible(false);
        /*mfTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() > 1) {
                    int index = ((TableSorter) mfTable.getTable().getModel()).getUnsortedRowFor(mfTable.getTable().getSelectedRow());
                    mnu_DetailQuoteMFund(index);
                }
            }
        });*/
        GUISettings.applyOrientation(mutualFundtFrame);
        return mutualFundtFrame;
    }


    public Table getSectorsTable() {
        return mainBoardLoader.getSectorTable();
    }

    private InternalFrame createFullTimeSalesFrame() {
        final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
        final String searching = Language.getString("LOADING");
//         Table table = new Table() ;
        table = new Table() {
            public void paint(Graphics g) {
                super.paint(g);
                if (DetailedTradeStore.isWritingCache) {
                    FontMetrics fontMetrics = table.getFontMetrics(getFont());
                    g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
                            (getHeight()) / 2);
                    g.drawImage(icon.getImage(),
                            (getWidth() - (fontMetrics.stringWidth(searching)) / 2) - (getWidth() / 2),
                            (getHeight() - icon.getImage().getHeight(this)) / 2 + 15, table.getBackground(), this);

                    fontMetrics = null;

                }
            }
        };
        table.setThreadID(Constants.ThreadTypes.TIME_N_SALES);
        table.setWindowType(ViewSettingsManager.FULL_TIMEnSALES_VIEW);
        TWMenuItem exportTrades = new TWMenuItem(Language.getString("EXPORT_ALL_TRADES"), "exporttrades.gif");
        exportTrades.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                exportAllDetailedTrades(DetailedTradeStore.getSharedInstance().getStore(), null, null);
            }
        });
        table.getPopup().setMenuItem(exportTrades);

        table.setPreferredSize(new Dimension(450, 100));
        AllTradesModel model = new AllTradesModel();
//        DetailedTradeModel model = new DetailedTradeModel();
//        model.setDataStore(DetailedTradeStore.getSharedInstance().getFilterableStore().getStore());
        model.setDataStore(DetailedTradeStore.getSharedInstance().getFilterableStore());
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("BTW_FULL_TIME&SALES");
        model.setViewSettings(oSetting);
        table.setModel(model);
        model.setTable(table);
        model.applyColumnSettings();
        table.updateGUI();
        table.killThread();

        final TWMenuItem filterMenuItem = new TWMenuItem(Language.getString("FILTER"), "filter.gif");
        filterMenuItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                filterTimeAndSalesSummaryForDetailedTrades();
            }
        });
        table.getPopup().setMenuItem(filterMenuItem);

        g_oFullTimeSalesFrame = new InternalFrame(table);

        g_oFullTimeSalesFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        g_oFullTimeSalesFrame.setColumnResizeable(true);
        g_oFullTimeSalesFrame.setDetachable(true);
        g_oFullTimeSalesFrame.setPrintable(true);
        g_oFullTimeSalesFrame.getContentPane().add(table);
        oTopDesktop.add(g_oFullTimeSalesFrame);
        oSetting.setParent(g_oFullTimeSalesFrame);
        g_oFullTimeSalesFrame.setResizable(true);
        g_oFullTimeSalesFrame.setClosable(true);
        g_oFullTimeSalesFrame.setMaximizable(true);
        g_oFullTimeSalesFrame.setIconifiable(true);
        g_oFullTimeSalesFrame.setTitle(Language.getString(oSetting.getCaptionID()));
        g_oFullTimeSalesFrame.updateUI();
        g_oFullTimeSalesFrame.applySettings();
        g_oFullTimeSalesFrame.setLayer(GUISettings.TOP_LAYER);
        g_oFullTimeSalesFrame.setVisible(false);
        DetailedTradeStore.getSharedInstance().setGui(g_oFullTimeSalesFrame);
        DetailedTradeStore.getSharedInstance().interuptThread();
        g_oFullTimeSalesFrame.addInternalFrameListener(DetailedTradeStore.getSharedInstance());
        GUISettings.applyOrientation(g_oFullTimeSalesFrame);

        return g_oFullTimeSalesFrame;
    }


    private InternalFrame createExchangesFrame() {
        marketTable = new Table(new int[]{0});
        marketTable.setWindowType(ViewSettingsManager.EXCHANGE_VIEW);
//        marketTable.getPopup().showLinkMenus();
        final MarketModel oMarketTableModel = new MarketModel();
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("BTW_EXCHANGES");
        oMarketTableModel.setViewSettings(oSetting);
        marketTable.setModel(oMarketTableModel);
        oMarketTableModel.setTable(marketTable);
        oMarketTableModel.setDataForNonDefaults();

        InternalFrame frame = new InternalFrame(marketTable);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        frame.setColumnResizeable(true);
        frame.setDetachable(true);
        frame.setPrintable(true);
        oSetting.setParent(frame);
        frame.getContentPane().add(marketTable);
        oTopDesktop.add(frame);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setMaximizable(true);
        frame.setIconifiable(true);
        frame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        frame.updateUI();
        frame.applySettings();
        frame.setLayer(GUISettings.TOP_LAYER);
        frame.setVisible(false);

        marketTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int col = marketTable.getTable().columnAtPoint(e.getPoint());
                int row = marketTable.getTable().rowAtPoint(e.getPoint());

                if (col == 0) { // expander column
                    String exchangeID = (String) oMarketTableModel.getValueAt(row, -1);
                    if (exchangeID != null) {
                        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeID);
                        if (exchange != null) {
                            if (exchange.getExpansionStatus() == Constants.EXPANSION_STATUS_COLLAPSED) {
                                exchange.setExpansionStatus(Constants.EXPANSION_STATUS_EXPANDED);
                                ExchangeStore.getSharedInstance().rebuildAllMarkets();
                                oMarketTableModel.rebuildStore();
                                marketTable.getTable().updateUI();
                            } else if (exchange.getExpansionStatus() == Constants.EXPANSION_STATUS_EXPANDED) {
                                exchange.setExpansionStatus(Constants.EXPANSION_STATUS_COLLAPSED);
                                ExchangeStore.getSharedInstance().rebuildAllMarkets();
                                oMarketTableModel.rebuildStore();
                                marketTable.getTable().updateUI();
                            }
                        }
                    }
                }

            }
        });
        oMarketTableModel.rebuildStore();
        GUISettings.applyOrientation(frame);
        return frame;
    }

    /*private InternalFrame createMarketFrame(){
             submarketTable = new Table();
//        marketTable.getPopup().showLinkMenus();
            oMarketTableModel = new SubMarketModel(null);
            ViewSetting oSetting = ViewSettingsManager.getSummaryView("BTW_MARKETS");
            oMarketTableModel.setViewSettings(oSetting);
            submarketTable.setModel(oMarketTableModel);
            oMarketTableModel.setTable(submarketTable);
            submarketTable.updateUI();
            oMarketTableModel.updateGUI();

            InternalFrame frame = new InternalFrame(submarketTable);
            frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            frame.setColumnResizeable(true);
            frame.setPrintable(true);
            oSetting.setParent(frame);
            frame.getContentPane().add(submarketTable);
            oTopDesktop.add(frame);
            frame.setResizable(true);
            frame.setClosable(true);
            frame.setMaximizable(true);
            frame.setIconifiable(true);
            frame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
            frame.updateUI();
            frame.applySettings();
            frame.setLayer(GUISettings.TOP_LAYER);
            frame.setVisible(false);
            JPanel northPanel = getExchangePanel();
            submarketTable.setNorthPanel(northPanel);
            return frame;
        }*/

    public Table getAssetClassTable() {
        return marketTable;
    }

    public void showNetCalculator() {
        if (isValidSystemWindow(Meta.IT_HistoryAnalyzer, false)) {
            if (netCalcFrame == null) {
                netCalcFrame = createChangeCalcFrame();
            }
            netCalcFrame.show();

            try {
                if (netCalcFrame.isVisible()) {
                    if (netCalcFrame.isIcon()) {
                        netCalcFrame.setIcon(false);
                    }
                }
                netCalcFrame.setSelected(true);
            } catch (PropertyVetoException e) {

            }
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Tools, "HistoryAnalyser-View");
        }
//        else
//            new ShowMessage("<html><b>" + Language.getString("HISTORY_ANALYZER") + "&nbsp;&nbsp;</b><br>" +
//                    Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");

    }

// --Commented out by Inspection START (3/29/05 2:30 PM):
//    public NetCalcModel getCalculatorModel() {
//        return netCalcModel;
//    }
// --Commented out by Inspection STOP (3/29/05 2:30 PM)

    public InternalFrame createChangeCalcFrame() {

        ViewSetting netCalcSettings = ViewSettingsManager.getSummaryView("HISTORY_ANALYZER");
        if (netCalcSettings == null) return null;

        netCalcModel = null;
        changeCalcTable = new Table();
        netCalcModel = NetCalcModel.getSharedInstance();
        changeCalcTable.addTableUpdateListener(netCalcModel);

        netCalcModel.setDirectionLTR(Language.isLTR());

        netCalcModel.setViewSettings(netCalcSettings);
        netCalcSettings.setSupportingObject(netCalcModel);

        changeCalcTable.setModel(netCalcModel);
        netCalcModel.setTable(changeCalcTable);
        netCalcModel.applyColumnSettings();

        /*ListButton lstbtnColumns = new ListButton(ListButton.TYPE_TEXT_TEXT, Language.isLTR());
        Object[] items = new Object[2];
        ListButtonItem lb1 = new ListButtonItem("", null, Language.getString("CURRENT_DAY"), 0f);
        items[0] = lb1;
        ListButtonItem lb2 = new ListButtonItem("", null, Language.getString("CUSTOM"), 0f);
        items[1] = lb2;
        lstbtnColumns.setItems(items);
        lstbtnColumns.setSelectedIndex(0);*/
        //lstbtnColumns.setBackground(this.getBackground());

        String data[] = {Language.getString("CURRENT_DAY"), Language.getString("CUSTOM")}; // "Current Day", "Custom"};
        TWComboBox combo = new TWComboBox(data);
        combo.setBorder(null);
        changeCalcTable.getTable().setDefaultEditor(Long.class, new CalcCellEditor(window, combo));

        netCalcFrame = new InternalFrame(changeCalcTable, g_oWindowDaemon);
        netCalcFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        netCalcSettings.setParent(netCalcFrame);
        netCalcFrame.getContentPane().add(changeCalcTable);
        oTopDesktop.add(netCalcFrame);
        netCalcFrame.setResizable(true);
        netCalcFrame.setClosable(true);
        netCalcFrame.setMaximizable(true);
        netCalcFrame.setIconifiable(true);
        netCalcFrame.setColumnResizeable(true);
        netCalcFrame.setDetachable(true);
        netCalcFrame.setPrintable(true);
        netCalcFrame.setTitle(Language.getString("HISTORY_ANALYZER")); // netCalcModel.getCaptionID()));
        netCalcFrame.updateUI();
//        GUISettings.setColumnSettings(netCalcSettings, TWColumnSettings.getItem("HISTORY_ANALYZER_COLS"));
        netCalcFrame.applySettings();
        netCalcFrame.setLayer(10);

        netCalcModel.adjustRows();
        changeCalcTable.getTable().setDefaultEditor(String.class, new CompanyCellEditor(new JComboBox()));
        //netCalcFrame.setVisible(true);

//        TWMenuItem ValuationMenu = new TWMenuItem(Language.getString("VALUATION_METHOD"), "valuationmethod.gif");
//        ValuationMenu.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                if(netCalcModel.getValuationType()==Constants.VALUATION_MODE_VWAP){
//                     g_mnuValuationPrice_VWAP.setSelected(true);
//                } else{
//                     g_mnuValuationPrice_last.setSelected(true);
//                }
//                netCalcModel.setValuationType(Constants.VALUATION_MODE_VWAP);
//            }
//        });
        changeCalcTable.getPopup().add(valautionCalcRecord());
//        GUISettings.applyOrientation(changeCalcTable.getPopup());

        TWMenuItem deleteMenu = new TWMenuItem(Language.getString("DELETE"), "rmsymbol.gif");
        deleteMenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                deleteChangeCalcRecord();
            }
        });
        changeCalcTable.getPopup().setMenuItem(deleteMenu);
        GUISettings.applyOrientation(netCalcFrame);
        GUISettings.applyOrientation(changeCalcTable.getPopup());

        netCalcSettings = null;
        changeCalcTable = null;

        return netCalcFrame;
    }

    private TWMenu valautionCalcRecord() {

        final TWCheckBoxMenuItem g_mnuValuationPrice_last = new TWCheckBoxMenuItem("Add valuation Price_last");
        g_mnuValuationPrice_last.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                netCalcModel.setValuationType(Constants.VALUATION_MODE_LAST);
                int[] selectedRows = netCalcModel.getTable().getSelectedRows();
                if (selectedRows.length == 0) return;

                if (selectedRows.length == 1) {
                    ChangeRecord record = NetCalcStore.getRecord(selectedRows[0]);
                    record.setValuationType(Constants.VALUATION_MODE_LAST);
                    netCalcModel.resetValues(record);
                }
                for (int index = selectedRows.length - 1; index >= 0; index--) {
                    ChangeRecord record = NetCalcStore.getRecord(selectedRows[index]);
                    record.setValuationType(Constants.VALUATION_MODE_LAST);
                    netCalcModel.resetValues(record);
                }
                netCalcModel.getTable().updateUI();
            }
        });

        final TWCheckBoxMenuItem g_mnuValuationPrice_VWAP = new TWCheckBoxMenuItem("Add valuation Price_VWAP");
        g_mnuValuationPrice_VWAP.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                netCalcModel.setValuationType(Constants.VALUATION_MODE_VWAP);
                int[] selectedRows = netCalcModel.getTable().getSelectedRows();
                if (selectedRows.length == 0) return;

                if (selectedRows.length == 1) {
                    ChangeRecord record = NetCalcStore.getRecord(selectedRows[0]);
                    record.setValuationType(Constants.VALUATION_MODE_VWAP);
                    netCalcModel.resetValues(record);
                }
                for (int index = selectedRows.length - 1; index >= 0; index--) {
                    ChangeRecord record = NetCalcStore.getRecord(selectedRows[index]);
                    record.setValuationType(Constants.VALUATION_MODE_VWAP);
                    netCalcModel.resetValues(record);
                }
                netCalcModel.getTable().updateUI();
            }
        });

        ButtonGroup btnGroup_valuationPrice = new ButtonGroup();
        TWMenu ValuationPrice = new TWMenu(Language.getString("VALUATION_METHOD"), "valuationmethod.gif");
        ValuationPrice.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int selectedRows = netCalcModel.getTable().getSelectedRow();

                if (NetCalcStore.getRecord(selectedRows).getValuationType() == Constants.VALUATION_MODE_VWAP) {
                    g_mnuValuationPrice_VWAP.setSelected(true);
                    g_mnuValuationPrice_last.setSelected(false);
                } else {
                    g_mnuValuationPrice_last.setSelected(true);
                    g_mnuValuationPrice_VWAP.setSelected(false);
                }
            }
        });

        ValuationPrice.addMenuListener(new MenuListener() {
            public void menuSelected(MenuEvent e) {
                int selectedRows = netCalcModel.getTable().getSelectedRow();

                if (NetCalcStore.getRecord(selectedRows).getValuationType() == Constants.VALUATION_MODE_VWAP) {
                    g_mnuValuationPrice_VWAP.setSelected(true);
                    g_mnuValuationPrice_last.setSelected(false);
                } else {
                    g_mnuValuationPrice_last.setSelected(true);
                    g_mnuValuationPrice_VWAP.setSelected(false);
                }
            }

            public void menuDeselected(MenuEvent e) {

            }

            public void menuCanceled(MenuEvent e) {

            }
        });

        g_mnuValuationPrice_last.setText(Language.getString("LAST_PRICE"));
        g_mnuValuationPrice_VWAP.setText(Language.getString("VWAP"));
        ValuationPrice.setText(Language.getString("VALUATION_METHOD"));
        btnGroup_valuationPrice.add(g_mnuValuationPrice_last);
        btnGroup_valuationPrice.add(g_mnuValuationPrice_VWAP);
        ValuationPrice.add(g_mnuValuationPrice_last);
        ValuationPrice.add(g_mnuValuationPrice_VWAP);
        int selectedRows = netCalcModel.getTable().getSelectedRow();
        if ((selectedRows > -1) && (NetCalcStore.getRecord(selectedRows).getValuationType() == Constants.VALUATION_MODE_VWAP)) {
            g_mnuValuationPrice_VWAP.setSelected(true);
        } else {
            g_mnuValuationPrice_last.setSelected(true);
        }
        GUISettings.applyOrientation(ValuationPrice);
        return ValuationPrice;

    }


    private void deleteChangeCalcRecord() {
        int[] selectedRows = netCalcModel.getTable().getSelectedRows();

        if (selectedRows.length == 0) return;

        if (selectedRows.length == 1) {
            String symbol = (String) netCalcModel.getTable().getModel().getValueAt(selectedRows[0], 0);
            if ((symbol == null) || (symbol.equals(""))) {
                if ((NetCalcStore.size() + 1) > 1) {
                    netCalcModel.removeRecord(selectedRows[0]);
                }
                return;
            }
        }


        ShowMessage oMessage = new ShowMessage(Language.getString("MSG_DELETE_SYMBOL"), "W", true);
        int i = oMessage.getReturnValue();

        if (i == 1) {
            for (int index = selectedRows.length - 1; index >= 0; index--) {

                String symbol = (String) netCalcModel.getTable().getModel().getValueAt(selectedRows[index], 0);
                if ((symbol == null) || (symbol.equals(""))) { // not a valid row
                    if ((NetCalcStore.size() + 1) > index) {
                        netCalcModel.removeRecord(selectedRows[index]);
                    }
                    continue;
                }

                if (index >= 0) {
                    netCalcModel.removeRecord(selectedRows[index]);
                }
            }
            netCalcModel.getTable().updateUI();
        }
    }

    /*public void addExitThread() {
        Thread t = new Thread("Client-addExitThread") {
            public void run() {
                if (Settings.isTwUpdateAvailable()) {
                    try {
//                        Runtime.getRuntime().exec("start \"" + System.getProperties().get("user.dir") +
//                                "\\AUTOUPDATE\\PROCESS_UPDATE.bat\"");
                        Runtime.getRuntime().exec("start \"" + System.getProperties().get("user.dir") +
                                "\\AutoUpdate\\AutoUpdate.exe\" \"" + System.getProperties().getProperty("user.dir") + "\"");
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        };

        Runtime.getRuntime().addShutdownHook(t);
    }*/

    public void initializeFrameAnalyser(FrameAnalyser frameAnalyser) {
        frameAnalyser.setTodaysTrades(g_oTodaysTrades);
    }

    public void showArabicNumbers(boolean showArabic) {
        g_oMenus.getArabicNumbersMenu().setSelected(showArabic);
        Settings.setShowArabicNumbers(showArabic);
        GUISettings.reloadGUIForNumberChange();
    }

    public void showAlias(boolean showAlias) {
        g_oMenus.getAliasMenu().setSelected(showAlias);
        Settings.setShowAlias(showAlias);
    }

    public void showHelp() {
        NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_GENERAL"));
    }

    public boolean isValidWindow(int type) {
        return isValidSystemWindow(type, false);
    }

    public boolean isValidInfoType(String key, int type, boolean noGUI) {
        if ((!noGUI) && (!Settings.isConnected())) {
            new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            return false;
        } else if (ExchangeStore.isValidIinformationType(SharedMethods.getExchangeFromKey(key), type)) {
            return true;
        } else {
            String titleTag = "NA";
            switch (type) {
                case Meta.IT_Portfolio:
                    titleTag = "MY_PORTFOLIO";
                    break;
                case Meta.IT_Alerts:
                    titleTag = "ALERT";
                    break;
                case Meta.IT_MarketMap:
                    titleTag = "HEATMAP_TITLE";
                    break;
                case Meta.IT_MarketDepthByPrice:
                    titleTag = "MARKET_DEPTH_BY_PRICE";
                    break;
                case Meta.IT_MarketDepthByOrder:
                    titleTag = "MARKET_DEPTH_BY_ORDER";
                    break;
                case Meta.IT_AnnouncementSearch:
                    titleTag = "ANNOUNCEMENT_SEARCH";
                    break;
                /* case Meta.IT_News:
                titleTag = "NEWS";
                break;*/
                case Meta.IT_CompanyProfile:
                    titleTag = "COMPANY_PROFILE";
                    break;
                case Meta.WT_MutualFund:
                    titleTag = "MUTUAL_FUND";
                    break;
                case Meta.IT_DepthCalculator:
                    titleTag = "TARGET_PRICE_CALC";
                    break;
                case Meta.IT_FunctionBuilder:
                    titleTag = "FUNCTION_LISTS";
                    break;
                case Meta.IT_RegionalQuotes:
                    titleTag = "POPUP_REGIONAL_QUOTES";
                    break;
            }
            if (!noGUI) {
                new ShowMessage("<html><b>" + Language.getString(titleTag) + "&nbsp;&nbsp;</b><br>" +
                        Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
            }
            return false;
        }
    }

    //priority formalise this method

    public boolean isValidSystemWindow(int type, boolean noGUI) {
        if ((!noGUI) && (!Settings.isConnected())) {
            new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            return false;
        } else if (ExchangeStore.isValidSystemFinformationType(type)) {
            return true;
        } else {
            String titleTag = "NA";
            switch (type) {
                case Meta.IT_Portfolio:
                    titleTag = "MY_PORTFOLIO";
                    break;
                case Meta.IT_Alerts:
                    titleTag = "ALERT";
                    break;
                case Meta.IT_AnnouncementSearch:
                    titleTag = "ANNOUNCEMENT_SEARCH";
                    break;
//                case Meta.IT_NewsSearch:
//                    titleTag = "NEWS_SEARCH";
//                    break;
                /*case Meta.IT_News:
                titleTag = "NEWS";
                break;*/
                case Meta.IT_CompanyProfile:
                    titleTag = "COMPANY_PROFILE";
                    break;
                case Meta.WT_MutualFund:
                    titleTag = "MUTUAL_FUND";
                    break;
                case Meta.IT_DepthCalculator:
                    titleTag = "TARGET_PRICE_CALC";
                    break;
                case Meta.IT_FunctionBuilder:
                    titleTag = "FUNCTION_LISTS";
                    break;
                case Meta.IT_HistoryAnalyzer:
                    titleTag = "HISTORY_ANALYZER";
                    break;
            }
            if (!noGUI) {
                new ShowMessage("<html><b>" + Language.getString(titleTag) + "&nbsp;&nbsp;</b><br>" +
                        Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
            }
            return false;
        }
    }

// --Commented out by Inspection START (3/29/05 2:31 PM):
//    public static int getMarketStatus() {
//        return marketStatus;
//    }
// --Commented out by Inspection STOP (3/29/05 2:31 PM)

    public WindowDaemon getWindowDaemon() {
        return g_oWindowDaemon;
    }

    public void createPortfolioView(boolean manual) {
        ViewSetting oSettings = null;
        if (pfWindow != null) {
            if (manual) {

                try {
                    if (!pfWindow.isVisible()) {
                        pfWindow.setVisible(true);
                    } else if (pfWindow.isIcon()) {
                        pfWindow.setIcon(false);
                    } else if (pfWindow.isSelected()) {
                        pfWindow.closeWindow();
                    } else {
                        pfWindow.setSelected(true);
                    }
                } catch (PropertyVetoException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            return;
        }
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings = (ViewSetting) g_oViewSettings.getPortfolioView(1280, "2").clone();
            if (oSettings == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        pfWindow = new PortfolioWindow(oSettings); //g_oCurrencyStore);
        oTopDesktop.add(pfWindow);
        pfWindow.setResizable(true);
        pfWindow.setMaximizable(true);
        pfWindow.setClosable(true);
        try {
            pfWindow.setMaximum(false);
        } catch (Exception ex) {
        }
        pfWindow.setIconifiable(true);
        pfWindow.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        pfWindow.setLayer(GUISettings.TOP_LAYER);
        pfWindow.setSize(700, 400);


        oSettings.setParent(pfWindow);
        pfWindow.applySettings();

        if (manual) {
            try {
                pfWindow.setVisible(true);
                pfWindow.setSelected(true);
                GUISettings.setLocationRelativeTo(pfWindow, getDesktop());
            } catch (Exception ex1) {
            }
        }
        pfWindow.updateUI();
        g_oViewSettings.addPortfolioSetting(oSettings);
    }

    private void printView() {
        /*if (g_oTempTable.isFunctionType()) {
            SharedMethods.printTable(g_oTempTable.getTable(), PrintManager.TABLE_TYPE_DEFAULT, g_oTempTable.getTitle());
        } else if (g_oTempTable.isCustomType())
            SharedMethods.printTable(g_oTempTable.getTable(), PrintManager.TABLE_TYPE_WATCHLIST, g_oTempTable.getTitle());
        else if (g_bTableView)
            SharedMethods.printTable(g_oTempTable.getTable(), PrintManager.TABLE_TYPE_DEFAULT, g_oTempTable.getTitle());
        else if (portfolioView)
            SharedMethods.printTable(g_oPortfolioTable.getTable().getTable(), PrintManager.TABLE_TYPE_DEFAULT,
                    g_oPortfolioTable.getTitle());*/

        if (mainboardSelected)
            if (!printInProgress) {
                new Thread() {
                    public void run() {
                        printInProgress = true;
                        g_oTempTable.printContents();
                        printInProgress = false;
                    }
                }.start();
            } else if (portfolioSelected)
                SharedMethods.printTable(g_oPortfolioTable.getTable().getTable(), PrintManager.TABLE_TYPE_DEFAULT,
                        g_oPortfolioTable.getTitle());
            else if (mistViewSelected)
                SharedMethods.printTable(g_oMistTable.getTable().getTable(),
                        PrintManager.TABLE_TYPE_MIST_VIEW, g_oMistTable.getTitle());
    }

    public boolean isTickerFixed() {
        return tickerFixed;
    }

    public void setTickerFixed(boolean tickerFixed) {
        this.tickerFixed = tickerFixed;

    }

    public void fixTicker() {
        ISTICKERFIXED = true;
        Ticker.tickerFixedToTopPanel = true;
        tickerFrame.setResizable(false);
        oTopDesktop.remove(tickerFrame);
        tickerPanel.add(tickerFrame);
        tickerPanel.updateUI();
        window.validate();
        setTickerFixed(true);
        if (windowTabs.contains(tickerFrame)) {
            // Tab-if (TWDesktop.tabHashTable.containsKey(tickerFrame)) {
            TWDesktop.removeWindowtab(tickerFrame);
        }
    }

    public void floatTicker() {
        ISTICKERFIXED = false;
        tickerFrame.setResizable(true);

        if (Ticker.tickerSetToTop) {
            tickerPanel.remove(tickerFrame);
        } else {
            bottomtickerPanel.remove(tickerFrame);
        }
        oTopDesktop.add(tickerFrame);
        oTopDesktop.updateUI();
        setTickerFixed(false);
        if (!windowTabs.contains(tickerFrame)) {
            if (tickerFrame.isVisible()) {
                TWDesktop.addWindowTab(tickerFrame);
            }
        }
    }

    public void fixTickerToBottom() {
        ISTICKERFIXED = true;
        Ticker.tickerFixedToTopPanel = false;

        tickerFrame.setResizable(false);
        oTopDesktop.remove(tickerFrame);
        bottomtickerPanel.add(tickerFrame);
        bottomtickerPanel.updateUI();
        window.validate();
        setTickerFixed(true);
        floatMenu.setText(Language.getString("FLOAT_TICKER"));
        if (windowTabs.contains(tickerFrame)) {
            // Tab-if (TWDesktop.tabHashTable.containsKey(tickerFrame)) {
            TWDesktop.removeWindowtab(tickerFrame);
        }
    }

    public PortfolioWindow getPortfolioWindow() {
        if (pfWindow == null) {
            createPortfolioView(false);
        }
        return pfWindow;
    }

    public void showAnnouncementSearchPanel() {
        if (isValidSystemWindow(Meta.IT_AnnouncementSearch, false)) {
            AnnouncementSearchPanel.getSharedInstance().setLocationRelativeTo(Client.getInstance().getDesktop());
            AnnouncementSearchPanel.getSharedInstance().setVisible(true);
        }
    }

    private void searchSymbolAnnouncements() {
        if (isValidSystemWindow(Meta.IT_AnnouncementSearch, false)) {
            String symbol = getSelectedSymbol();
            if ((symbol != null) && (!symbol.equals(""))) {
                AnnouncementSearchWindow.getSharedInstance().searchForSymbol(getSelectedSymbol(), Calendar.YEAR, 100);
                symbol = null;
            }
        }
    }

    private void searchSymbolNews() {
        String key = getSelectedSymbol();
        String providerID = "MUBASHER";

        try {
            ArrayList<String> exchangeSupported = NewsProvidersStore.getSharedInstance().getAllProvidersForExchange(SharedMethods.getExchangeFromKey(key));
            if (exchangeSupported.size() == 0) {
                new ShowMessage(Language.getString("NO_NEWS_PROVIDERS"), "I");
                return;
            } else {
                String provider = "";
                for (String id : exchangeSupported) {
                    provider += "," + id;
                }
                System.out.println("Provider List : " + provider);
                providerID = provider.trim().substring(1);
            }

        } catch (HeadlessException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        try {
            if (isValidSystemWindow(Meta.IT_News, false)) {
                key = getSelectedSymbol();
                if ((key != null) && (!key.equals(""))) {
                    String originalkey = key;
                    String symbol = SharedMethods.getSymbolFromKey(key);
                    String exchange = SharedMethods.getExchangeFromKey(key);
                    int instrument = SharedMethods.getInstrumentTypeFromKey(key);
                    if (ExchangeStore.getSharedInstance().getExchange(exchange).hasSubMarkets()) {
                        symbol = symbol.split(Constants.MARKET_SEPERATOR_CHARACTER)[0];
                        key = SharedMethods.getKey(exchange, symbol, instrument);
                    }
                    NewsWindow.getSharedInstance().searchForSymbol(key, 0, providerID);
//                    Stock stock = DataStore.getSharedInstance().getStockObject(key);
                    Stock stock = DataStore.getSharedInstance().getStockObject(originalkey);
                    if (stock != null) {
                        if (stock.getNewsAvailability() != Constants.ITEM_NOT_AVAILABLE) {
                            stock.setNewsAvailability(Constants.ITEM_READ);
                        }
                    }
                    stock = null;
                }
                key = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showFullHeatMap() {
//        Exchange exch = ExchangeStore.getSharedInstance().getExchange(ExchangeStore.getSelectedExchangeID());
//        String[] sSymbols = null;
//
//        sSymbols = DataStore.getSharedInstance().getSymbolsObject(exch.getSymbol(), null).getFilteredSymbols();
//
//        heatMap.show(sSymbols, table.getTitle());
//        heatMap.setWatchlistID(table.getViewSettings().getType() + "|" +
//                table.getViewSettings().getID());
//
//        try {
//            heatMap.setSelected(true);
//        } catch (PropertyVetoException e) {
//        }
//
        try {
            showHeatMapForTheWatchList(mainBoardLoader.getClientTable(ExchangeStore.getSelectedExchangeID()), ExchangeStore.getSelectedExchangeID());
        } catch (Exception e) {
            String exchange = ExchangeStore.getSharedInstance().getDefaultExchange().getSymbol();
            showHeatMapForTheWatchList(mainBoardLoader.getClientTable(exchange), exchange);
            exchange = null;
        }
//        showHeatMapForTheWatchList(defaultBoardtable);
    }


    public void showHeatMapForTheWatchList(ClientTable table, String exchangeID) {
        //if (isValidSystemWindow(Meta.IT_MarketMap)) {
        System.out.println("table == " + table.getTitle());
        String[] sSymbols = null;

        sSymbols = table.getSymbols().getFilteredSymbols();
        if (heatMap == null) {
            createHeatMap();
        }
//        if (heatMap.isVisible()) {
//            try {
//                if (heatMap.isIcon()) {
//                    heatMap.setIcon(false);
//                    heatMap.setSelected(true);
//                } else if (!heatMap.isSelected()) {
//                    heatMap.setSelected(true);
//                } else {
//                    heatMap.setVisible(false);
//                }
//            } catch (Exception e) {
//            }
//        } else {
        heatMap.setWatchlistID(table.getViewSettings().getType() + "|" +
                table.getViewSettings().getID());
        try {
            heatMap.setSelectedExchange(exchangeID);
        } catch (Exception e) {
            heatMap.setSelectedExchange(null);

        }
        try {
            String[] arr = table.getTitle().split(":");
            heatMap.setSelectedMarket(arr[1].substring(2));
            arr = null;
        } catch (Exception e) {
            heatMap.setSelectedMarket(null);
        }
        heatMap.show(sSymbols, table.getTitle());
        try {
            if (heatMap.isVisible()) {
                if (heatMap.isIcon()) {
                    heatMap.setIcon(false);
                }
            }

            heatMap.setSelected(true);
        } catch (PropertyVetoException e) {
        }
        //}

        //}
    }

    public void showHeatMapForTheWatchList(MISTTable table) {
        String[] sSymbols = null;

        sSymbols = table.getViewSettings().getSymbols().getFilteredSymbols();
        if (heatMap == null) {
            createHeatMap();
        }
//        if (heatMap.isVisible()) {
//            try {
//                if (heatMap.isIcon()) {
//                    heatMap.setIcon(false);
//                    heatMap.setSelected(true);
//                } else if (!heatMap.isSelected()) {
//                    heatMap.setSelected(true);
//                } else {
//                    heatMap.setVisible(false);
//                }
//            } catch (Exception e) {
//            }
//        } else {
        heatMap.setWatchlistID(table.getViewSettings().getType() + "|" +
                table.getViewSettings().getID());
        heatMap.setSelectedExchange(null);
        heatMap.show(sSymbols, table.getTitle());
        try {
            heatMap.setSelected(true);
        } catch (PropertyVetoException e) {
        }
//        }

    }

    public void setSymbolsForHeatMap(String watchlidtID) {
        try {
            if (heatMap == null) {
                createHeatMap();
            }
            Object[] windows = oTopDesktop.getWindows();
            for (int i = 0; i < windows.length; i++) {
                try {
                    if (windows[i] instanceof ClientTable) {
                        ClientTable table = (ClientTable) windows[i];
                        ViewSetting viewSetting = (ViewSetting) table.getViewSettings();

                        if (watchlidtID.equals(viewSetting.getType() + "|" + viewSetting.getID())) {
                            // store = (WatchListStore)viewSetting.getSymbols();
                            String[] sSymbols;
                            sSymbols = table.getSymbols().getFilteredSymbols();
                            heatMap.setSymbols(sSymbols, viewSetting.getCaption());
                            heatMap.setSelectedExchange(table.getMarketCode());
                            viewSetting = null;
                            sSymbols = null;
                            table = null;
                            break;
                        }
                        viewSetting = null;
                        table = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {

        }
    }


    public void createHeatMap() {
        ViewSetting oSettings = null;
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings = (ViewSetting) ViewSettingsManager.getSummaryView("HEATMAP");
            if (oSettings == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        heatMap = new HeatMapFrame(oSettings); //g_oCurrencyStore);
        oSettings.setParent(heatMap);
        heatMap.setResizable(true);
        heatMap.setMaximizable(false);
        heatMap.setClosable(true);
        heatMap.setDetachable(true);
        heatMap.setIconifiable(true);
        heatMap.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
//        heatMap.setSize(700, 400);

        oTopDesktop.add(heatMap);
//        oTopDesktop.setLayer(heatMap, GUISettings.TOP_LAYER);
        oSettings.setParent(heatMap);

        heatMap.applySettings();
        oTopDesktop.setLayer(heatMap, GUISettings.TOP_LAYER);
        heatMap.updateUI();
        GUISettings.applyOrientation(heatMap);
    }

    // ---------- Trading related methods ----------

    /*public void createTradingPortfolioView(boolean manual) {
        ViewSetting oSettings = null;
        if (tradingPortfolio != null) {
            if (manual) {
                if (TradingShared.isReadyForTrading()) {
                    try {
                        if (!tradingPortfolio.isVisible()) {
                            tradingPortfolio.setVisible(true);
                            GUISettings.setLocationRelativeTo(tradingPortfolio, getDesktop());
                        } else if (tradingPortfolio.isIcon()) {
                            tradingPortfolio.setIcon(false);
                        } else if (tradingPortfolio.isSelected()) {
                            tradingPortfolio.closeWindow();
                        } else {
                            tradingPortfolio.setSelected(true);
                            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "Portfolio");
                        }
                    } catch (PropertyVetoException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
                }
            }
            return;
        }
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings = (ViewSetting) g_oViewSettings.getPortfolioView(1280, "4").clone();
            if (oSettings == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        tradingPortfolio = new TradingPortfolioWindow(oSettings); //g_oCurrencyStore);
        oTopDesktop.add(tradingPortfolio);
        tradingPortfolio.setResizable(true);
        tradingPortfolio.setMaximizable(true);
        tradingPortfolio.setClosable(true);
        try {
            tradingPortfolio.setMaximum(false);
        } catch (Exception ex) {
        }
        tradingPortfolio.setIconifiable(true);
        tradingPortfolio.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        tradingPortfolio.setLayer(GUISettings.TOP_LAYER);
        tradingPortfolio.setSize(700, 400);


        oSettings.setParent(tradingPortfolio);
        tradingPortfolio.applySettings();
        tradingPortfolio.updateUI();
        g_oViewSettings.addPortfolioSetting(oSettings);
    }*/

    public void createTradingPortfolioView(boolean manual) {
        ViewSetting oSettings1 = null;
        ViewSetting oSettings2 = null;
        if (tradingPortfolio != null) {
            if (manual) {
                if (TradingShared.isReadyForTrading()) {
                    try {
                        if (!tradingPortfolio.isVisible()) {
                            tradingPortfolio.setVisible(true);
                            // Added by Shanaka
                            TradeMethods.requestPortfolioData(Constants.PATH_PRIMARY);
                            TradeMethods.requestAccountData(Constants.PATH_PRIMARY);
//                            GUISettings.setLocationRelativeTo(tradingPortfolio, getDesktop());
                        } else if (tradingPortfolio.isIcon()) {
                            tradingPortfolio.setIcon(false);
                        } else if (tradingPortfolio.isSelected()) {
                            tradingPortfolio.closeWindow();
                        } else {
                            tradingPortfolio.setSelected(true);
                        }
                    } catch (PropertyVetoException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
                }
            }
            return;
        }
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings1 = (ViewSetting) g_oViewSettings.getPortfolioView(1280, "4").clone();
            if (oSettings1 == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings2 = (ViewSetting) g_oViewSettings.getPortfolioView(1280, "5").clone();
            if (oSettings2 == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        tradingPortfolio = new TradingPortfolioWindow(oSettings1, oSettings2); //g_oCurrencyStore);
//        oTopDesktop.add(tradingPortfolio);
        tradingPortfolio.setResizable(true);
        tradingPortfolio.setMaximizable(true);
        tradingPortfolio.setClosable(true);
        try {
            tradingPortfolio.setMaximum(false);
        } catch (Exception ex) {
        }
        tradingPortfolio.setIconifiable(true);
        tradingPortfolio.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        tradingPortfolio.setLayer(GUISettings.TOP_LAYER);
        oTopDesktop.add(tradingPortfolio);
        tradingPortfolio.setSize(700, 400);


        oSettings1.setParent(tradingPortfolio);
        oSettings2.setParent(tradingPortfolio);
        tradingPortfolio.applySettings();
        tradingPortfolio.updateUI();
        g_oViewSettings.addPortfolioSetting(oSettings1);
        g_oViewSettings.addPortfolioSetting(oSettings2);
    }

    public TradingPortfolioWindow getTradingPortfolio() {
        if (tradingPortfolio == null) {
            try {
                createTradingPortfolioView(false);
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return tradingPortfolio;
    }

    public void tradingSessionEnabled() {
        try {
            g_oMenus.setTradeConnectionEnabled(true);
            TradingShared.setLevel2AuthenticationPending(false);
            TradingShared.setLevel2Password(null);
            setTradeConnectionBulbStatus();
            ArrayList pfs = TradePortfolios.getInstance().getPortfolioList();
            try {
                for (int i = 0; i < pfs.size(); i++) {
                    final TradingPortfolioRecord record = (TradingPortfolioRecord) pfs.get(i);
                    if (isForexSupportedPF(record.getExchangeString())) {
                        g_oTreeTop.insert(g_oForexTreeNode, 3);
                        getMenus().getForexMenuItem().setVisible(true);
                        getMenus().getForexMenuItem().setEnabled(true);

                    }

                }
            } catch (Exception e) {
                //            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            oTree.updateUI();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public boolean isForexSupportedPF(String exchangeList) {
        try {
            String[] arr = exchangeList.split(",");
            Hashtable<String, String> temp = new Hashtable<String, String>();
            for (int i = 0; i < arr.length; i++) {
                temp.put(arr[i], arr[i]);
            }
            if (temp.containsKey("FORX")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

    public void setTradeSessionDisconnected(boolean manual) {
//        setTradeConnectionDown();
        TradingShared.setDisconnected();
        mnuTrade.setEnabled(false);
        g_oMenus.setTradeConnectionEnabled(false);
        if (manual) {
            if ((TWControl.isSingleSingnOn() && TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                disconnectFromServer();
                setDisConnectedStatus(true);
            }
            AutoTradeLoginDaemon.setMode(AutoTradeLoginDaemon.STOPPED);
        } else {
            AutoTradeLoginDaemon.setMode(AutoTradeLoginDaemon.DISCONNECTED);
        }
        TradingConnectionNotifier.getInstance().fireTWDisconnected();
        TradingShared.setLevel2AuthenticationPending(false);
        TradingShared.setLevel2Password(null);
        setTradeConnectionBulbStatus();
        //TradingShared.getTrader().inInitialize();
        if (!(Settings.isConnected())) {
            if (!(TWControl.isSingleSingnOn() && TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                g_oMenus.lockTradeServerConnection();
            } else {
                g_oMenus.lockServerConnection();
            }
        } else {
            if ((TWControl.isSingleSingnOn() && TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                g_oMenus.lockServerConnection();
            }
        }
        try {
            g_oTreeTop.remove(g_oForexTreeNode);
            getMenus().getForexMenuItem().setVisible(true);
            getMenus().getForexMenuItem().setEnabled(true);
        } catch (Exception e) {
        }
    }

    public void initiateTradeLogin() {
        if (autoTradeLoginDaemon == null)
            autoTradeLoginDaemon = new AutoTradeLoginDaemon();
    }

    private InternalFrame createOrderQFrame() {
        BasketOrderFrame basketOrder = new BasketOrderFrame();
        orderQFrame = basketOrder.getInternalFrame();
        return orderQFrame;
        /*final Table orderQTable = new Table(new int[]{2});

        orderQTable.setPreferredSize(new Dimension(500, 100));
        OrderQModel orderQrModel = new OrderQModel();
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("ORDER_Q");
        orderQrModel.setViewSettings(oSetting);
        orderQTable.setModel(orderQrModel);
        orderQTable.setWindowType(ViewSettingsManager.TRADE_QUEUED_TRADES);
        orderQrModel.setTable(orderQTable);
        orderQrModel.applyColumnSettings();
        orderQrModel.updateGUI();

        orderQFrame = new InternalFrame(orderQTable);
//        orderQFrame.getContentPane().setLayout(new BorderLayout());
        orderQFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oSetting.setParent(orderQFrame);
        orderQFrame.getContentPane().add(orderQTable);
//        orderQFrame.getContentPane().add(createQComponentsPanel(orderQTable) , BorderLayout.CENTER);
        oTopDesktop.add(orderQFrame);
        orderQFrame.setResizable(true);
        orderQFrame.setClosable(true);
        orderQFrame.setMaximizable(true);
        orderQFrame.setIconifiable(true);
        orderQFrame.setColumnResizeable(true);
        orderQFrame.setDetachable(true);
        orderQFrame.setPrintable(true);
        orderQFrame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        orderQFrame.updateUI();
        orderQFrame.applySettings();
        orderQFrame.setLayer(GUISettings.TOP_LAYER);
        orderQFrame.setVisible(false);

        orderQTable.getPopup().add(new TWSeparator());

        TWMenuItem mnuDelete = new TWMenuItem(Language.getString("DELETE"), "deleteorder.gif");
        mnuDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //OrderQueue.getSharedInstance().removeTransaction(orderQTable.getTable().getSelectedRow());
                //orderQTable.getTable().repaint();

                int[] transactions = orderQTable.getTable().getSelectedRows();
                if ((orderQTable.getTable().getSelectedRowCount() > 0) &&
                        (SharedMethods.showConfirmMessage(Language.getString("MSG_DELETE_SELECTED_ORDERS"), JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION)) {
                    QueuedTransaction transaction;
                    for (int i = transactions.length - 1; i >= 0; i--) {
                        transaction = OrderQueue.getSharedInstance().getTransaction(i);
                        OrderQueue.getSharedInstance().removeTransaction(transaction);
                        transaction = null;
                    }
                }
                transactions = null;
                orderQTable.getTable().repaint();
            }
        });
        //orderQTable.getPopup().add(mnuDelete);

        TWMenuItem mnuSelectBuy = new TWMenuItem(Language.getString("SELECT_BUY_ORDERS"), "selectbuyorders.gif");
        mnuSelectBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                    markQueuedOrders(TradeMeta.BUY);
                }
            }
        });
        orderQTable.getPopup().add(mnuSelectBuy);

        TWMenuItem mnuSelectSell = new TWMenuItem(Language.getString("SELECT_SELL_ORDERS"), "selectsellorders.gif");
        mnuSelectSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                    markQueuedOrders(TradeMeta.SELL);
                }
            }
        });
        orderQTable.getPopup().add(mnuSelectSell);

        TWMenuItem mnuSelectAll = new TWMenuItem(Language.getString("SELECT_ALL"), "selectall.gif");
        mnuSelectAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                    markQueuedOrders(TradeMeta.ALL);
                }
            }
        });
        orderQTable.getPopup().add(mnuSelectAll);

        TWMenuItem mnuUnSelectAll = new TWMenuItem(Language.getString("UNSELECT_ALL"), "unselectall.gif");
        mnuUnSelectAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                    markQueuedOrders(TradeMeta.NONE);
                }
            }
        });
        orderQTable.getPopup().add(mnuUnSelectAll);

        GUISettings.applyOrientation(orderQTable.getPopup());

        orderQTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                *//*if (e.getClickCount() > 1) {
                    if (TradingShared.isReadyForTrading()) {
                        QueuedTransaction transaction = OrderQueue.getSharedInstance().getTransaction(orderQTable.getTable().getSelectedRow());
                        doTransaction(TradeMeta.AMEND, transaction, true, transaction.getQID() , TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, transaction.getPortfolioNo());
                    }
                }*//*
            }
        });
        ((SmartTable) orderQTable.getTable()).setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
        orderQTable.setSouthPanel(createOrderQButtonPanel());
        orderQTable.setNorthPanel(createQComponentsPanel(orderQTable));
        GUISettings.applyOrientation(orderQFrame);

        return orderQFrame;*/
    }

    private InternalFrame createProgramedOrderFrame() {
        final Table table = new Table(new int[]{2});

//        TWMenuItem mnuAmend = new TWMenuItem(Language.getString("AMEND_ORDER"));
//        mnuAmend.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                doTransaction(TradeMeta.AMEND,
//                        ProgramedOrderStore.getSharedInstance().getTransaction(orderTable.getTable().getSelectedRow()), false, TradeMeta.MODE_PROGRAMMED, TradeMeta.SOURCE_BOARD);
//            }
//        });
//        orderTable.getPopup().setMenuItem(mnuAmend);

//        TWMenuItem mnuDelete = new TWMenuItem(Language.getString("CANCEL_ORDER"));
//        mnuDelete.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                doTransaction(TradeMeta.CANCEL,
//                        ProgramedOrderStore.getSharedInstance().getTransaction(orderTable.getTable().getSelectedRow()), false, TradeMeta.MODE_PROGRAMMED, TradeMeta.SOURCE_BOARD);
//            }
//        });
//        orderTable.getPopup().setMenuItem(mnuDelete);

        table.setPreferredSize(new Dimension(500, 100));
        ProgramedOrderModel orderModel = new ProgramedOrderModel();
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("PROG_ORDERS");
        orderModel.setViewSettings(oSetting);
        table.setModel(orderModel);
        table.setWindowType(ViewSettingsManager.TRADE_PROGRAMED_ORDERS);
        orderModel.setTable(table);
        orderModel.applyColumnSettings();
        orderModel.updateGUI();

        InternalFrame frame = new InternalFrame(table);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oSetting.setParent(frame);
        frame.getContentPane().add(table);
        oTopDesktop.add(frame);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setMaximizable(true);
        frame.setIconifiable(true);
        frame.setTitle(Language.getString(oSetting.getCaptionID()));
        frame.updateUI();
        frame.applySettings();
        frame.setLayer(GUISettings.TOP_LAYER);
        frame.setVisible(false);

//        orderTable.getTable().addMouseListener(new MouseAdapter() {
//            public void mouseClicked(MouseEvent e) {
//                if (e.getClickCount() > 1) {
//                    doTransaction(TradeMeta.AMEND,
//                            ProgramedOrderStore.getSharedInstance().getTransaction(orderTable.getTable().getSelectedRow()), true, TradeMeta.MODE_PROGRAMMED, TradeMeta.SOURCE_BOARD);
//                }
//            }
//        });

        return frame;
    }

    private JPanel createQComponentsPanel(final Table orderQTable) {
        String[] widths = {"100%", "0"};
        FlexFlowLayout layout = new FlexFlowLayout(widths);
        JPanel panel = new JPanel(layout);
        final Dimension size = new Dimension(300, 0);

        //JPanel panel2 = new JPanel();
        //panel2.setLayout(new com.isi.util.ColumnLayout());
        final JPanel basketPanel = new JPanel();
        basketPanel.setPreferredSize(new Dimension(380, 30));
        String[] width = {"80", "160", "100", "100", "100"};
        String[] height = {"20"};
        basketPanel.setLayout(new FlexGridLayout(width, height, 5, 5));

        final ArrayList<TWComboItem> basketList = new ArrayList<TWComboItem>();
        TWComboModel basketModel = new TWComboModel(basketList);
        final JComboBox basketCombo = new JComboBox(basketModel);

        JLabel basketLabel = new JLabel(Language.getString("SELECTED_BASKET"));
        TWButton addNewBtn = new TWButton(Language.getString("ADD"));
        TWButton deleteBtn = new TWButton(Language.getString("DELETE"));
        TWButton renameBtn = new TWButton(Language.getString("RENAME"));

        basketPanel.add(basketLabel);
        basketPanel.add(basketCombo);
        basketPanel.add(addNewBtn);
        basketPanel.add(renameBtn);
        basketPanel.add(deleteBtn);
        GUISettings.applyOrientation(basketPanel);

        basketCombo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int index = basketCombo.getSelectedIndex();
                String captionID = basketList.get(index).getId();
                OrderQueue.getSharedInstance().setSelectedQID(new Long(captionID));
                orderQTable.getTable().updateUI();
            }
        });

        int QCount = OrderQueue.getSharedInstance().getQCount();
        if (QCount == 0) {
            QComponent component = OrderQueue.getSharedInstance().createComponent(Language.getString("DEFAULT_BASKET"));
            basketList.add(new TWComboItem("" + component.getId(), component.getValue()));
        } else {
            for (int i = 0; i < QCount; i++) {
                QComponent component = OrderQueue.getSharedInstance().getQConmonent(i);
                basketList.add(component);
            }
        }
        OrderQueue.getSharedInstance().setSelectedQID(new Long(OrderQueue.getSharedInstance().getQConmonent(0).getId()));
        if (basketList != null) {
            basketCombo.setSelectedIndex(0);
        }

        addNewBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CreateView oCreateView = new CreateView(window, Language.getString("CREATE_VIEW"), true, false);
                String caption = oCreateView.getNewName();

                //Bug ID <#0001>
                if (caption != null) {
                    if (!isValidBasket(basketList, caption)) {
                        new ShowMessage(false, Language.getString("MSG_DUPLICATE_VIEW_NAME"), "E");
                    } else {
                        QComponent component = OrderQueue.getSharedInstance().createComponent(caption);
                        basketList.add(component);
                        basketCombo.setSelectedItem(component);
                        basketCombo.updateUI();
                    }
                }
                oCreateView = null;
            }
        });


        deleteBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int index = basketCombo.getSelectedIndex();
                String captionID = basketList.get(index).getId();
                if (basketList.size() == 1) {
                    SharedMethods.showMessage(Language.getString("MSG_CANNOT_DELETE_BASKET"), JOptionPane.ERROR_MESSAGE);
                } else {
                    int i = SharedMethods.showConfirmMessage(Language.getString("MSG_DELETE_BASKET"), JOptionPane.WARNING_MESSAGE);
                    if (i == JOptionPane.OK_OPTION) {
                        OrderQueue.getSharedInstance().removeQueue(new Long(captionID));
                        basketList.remove(index);
                        orderQTable.getTable().repaint();
                        basketCombo.setSelectedIndex(0);
                        basketCombo.updateUI();
                    }
                }
            }
        });

        renameBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int index = basketCombo.getSelectedIndex();
                String captionID = basketList.get(index).getId();
                String captionValue = basketList.get(index).getValue();
                CreateView oCreateView = new CreateView(window, Language.getString("RENAME") + " " + basketList.get(index).getValue(), true, false);
                QComponent component = OrderQueue.getSharedInstance().getQComponent(new Long(captionID));
                oCreateView.setCaptions(component.getFullCaption());
                String caption = oCreateView.getNewName();
                if (caption != null) { //Bug id <#0005>
                    if (!isValidBasket(basketList, caption)) {
                        new ShowMessage(false, Language.getString("MSG_DUPLICATE_VIEW_NAME"), "E");
                    } else {
                        component.setCaption(caption);
                        basketCombo.setSelectedIndex(index);
                        basketCombo.updateUI();
                    }
                }
            }
        });

        return basketPanel;
    }

    private boolean isValidBasket(ArrayList<TWComboItem> basketList, String newItem) {
        try {
            for (TWComboItem item : basketList) {
                if (Language.getLanguageSpecificString(item.getValue(), ",").equals(Language.getLanguageSpecificString(newItem, ","))) {
                    return false;
                }
            }

            return true;
        } catch (Exception e) {
            return true;
        }
    }

    private JPanel createOrderQButtonPanel() {
        /*JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        TWButton btnExec = new TWButton(Language.getString("EXECUTE"));
        panel.add(btnExec);
        btnExec.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                executeQueuedOrders();
            }
        });
        TWButton btnDelete = new TWButton(Language.getString("DELETE"));
        panel.add(btnDelete);
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                deleteQueuedOrders();
            }
        });
        return panel;*/
        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"122", "102", "100%", "100"}, new String[]{"24"}, 2, 3));
        final TWButton selectAll = new TWButton(Language.getString("BASKET_SELECT_ALL"));
        int size = OrderQueue.getSharedInstance().size();
        QueuedTransaction transaction;
        boolean isAllSelected = true;
        for (int i = size - 1; i >= 0; i--) {
            transaction = OrderQueue.getSharedInstance().getTransaction(i);
            if (!transaction.isSelected()) {
                isAllSelected = false;
                break;
            }
            transaction = null;
        }
        if (isAllSelected) {
            selectAll.setText(Language.getString("BASKET_UNSELECT_ALL"));
//            selectAll.setSelected(false);
        } else {
            selectAll.setText(Language.getString("BASKET_SELECT_ALL"));
//            selectAll.setSelected(false);
        }
        selectAll.setSize(new Dimension(120, 18));
        selectAll.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (selectAll.getText().equals(Language.getString("BASKET_SELECT_ALL"))) {
                    markQueuedOrders(TradeMeta.ALL);
                    selectAll.setText(Language.getString("BASKET_UNSELECT_ALL"));
//                    selectAll.setSelected(false);
                } else {
                    markQueuedOrders(TradeMeta.NONE);
                    selectAll.setText(Language.getString("BASKET_SELECT_ALL"));
//                    selectAll.setSelected(false);
                }
            }
        });
        panel.add(selectAll);

        TWButton btnDelete = new TWButton(Language.getString("DELETE"));
        btnDelete.setSize(new Dimension(100, 18));
        panel.add(btnDelete);
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                    deleteQueuedOrders();
                } else {
                    SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        panel.add(new JLabel(""));

        TWButton btnExec = new TWButton(Language.getString("EXECUTE"));
        btnExec.setSize(new Dimension(100, 18));
        panel.add(btnExec);
        btnExec.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                executeQueuedOrders();
            }
        });

        return panel;
    }

    private void executeQueuedOrders() {
        int size = OrderQueue.getSharedInstance().size();
        QueuedTransaction transaction;
        boolean itemsSelected = false;
        boolean first = true;

        if (TradingShared.isReadyForTrading()) {

            for (int i = size - 1; i >= 0; i--) {
                transaction = OrderQueue.getSharedInstance().getTransaction(i);
                if (transaction.isSelected()) {
                    itemsSelected = true;
                }
                transaction = null;
            }

            if ((itemsSelected) &&
                    (SharedMethods.showConfirmMessage(Language.getString("MSG_EXECUTE_BASKET_ORDERS"), JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION)) {
                for (int i = size - 1; i >= 0; i--) {
                    transaction = OrderQueue.getSharedInstance().getTransaction(i);
                    if (transaction.isSelected()) {
                        if (TradingShared.isExchangeAllowedToTrade(transaction.getExchange(), false)) {
                            if (first) { // authenticate only the first order of the list
                                boolean sucess = TradeMethods.getSharedInstance().sendNewOrder(transaction, true);
                                if (!sucess) break; //Bug ID <#0014>
                                first = false;
                            } else {
                                TradeMethods.getSharedInstance().sendNewOrder(transaction, false);
                            }
                            OrderQueue.getSharedInstance().removeTransaction(transaction);
                        }
                    }
                    transaction = null;
                }
            }
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }
    }

    private void markQueuedOrders(int type) {
        int size = OrderQueue.getSharedInstance().size();
        QueuedTransaction transaction;

//        if (TradingShared.isReadyForTrading()) {
        for (int i = size - 1; i >= 0; i--) {
            transaction = OrderQueue.getSharedInstance().getTransaction(i);
            if ((type == TradeMeta.ALL) || (type == transaction.getSide())) {
                transaction.setSelected(true);
            } else if (type == TradeMeta.NONE) {
                transaction.setSelected(false);
            }
            transaction = null;
//        }
        }
    }

    private void deleteQueuedOrders() {
        int size = OrderQueue.getSharedInstance().size();
        boolean itemsSelected = false;
        QueuedTransaction transaction;

        for (int i = size - 1; i >= 0; i--) {
            transaction = OrderQueue.getSharedInstance().getTransaction(i);
            if (transaction.isSelected()) {
                itemsSelected = true;
            }
            transaction = null;
        }

        if ((itemsSelected) &&
                (SharedMethods.showConfirmMessage(Language.getString("MSG_DELETE_SELECTED_ORDERS"), JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION)) {
            for (int i = size - 1; i >= 0; i--) {
                transaction = OrderQueue.getSharedInstance().getTransaction(i);
                if (transaction.isSelected()) {
                    OrderQueue.getSharedInstance().removeTransaction(transaction);
                }
                transaction = null;
            }
        }

    }

    public void doTransaction(int type, String symbol, long quantity, boolean queued, long qid, int executionType, byte source, String portfolio, String bookKeeper) {
        doTransaction(type, symbol, quantity, null, queued, qid, executionType, source, portfolio, bookKeeper, -1D);
    }

    public void doTransactionForDuplicate(int type, String symbol, long quantity, boolean queued, long qid, int executionType, byte source, String portfolio, String bookKeeper, double price, Transaction transaction) {
        doTransaction(type, symbol, quantity, transaction, queued, qid, executionType, source, portfolio, bookKeeper, price);
    }

    public void doTransaction(int type, String symbol, long quantity, boolean queued, long qid, int executionType, byte source, String portfolio, boolean isSqureOff) {
        doTransaction(type, symbol, quantity, null, queued, qid, executionType, source, portfolio, isSqureOff, null, -1D);
    }

    public void doTransaction(int type, Transaction transaction, boolean queued, long qid, int executionType, byte source, String portfolio, String bookKeeper) {
        doTransaction(type, null, 0, transaction, queued, qid, executionType, source, portfolio, bookKeeper, -1D);
    }

    private TransactionDialog doTransaction(int type, String symbol, long quantity, Transaction transaction, boolean queued, long qID,
                                            int executionType, byte source, String portfolio, boolean isSqureOff, String bookKeeper, double price) {
        TransactionDialog transactionDialog = null;

        if (TradingShared.isReadyForTrading()) {
            boolean needToConnect = true;

            if (!Settings.isConnected()) {
                int status = SharedMethods.showConfirmMessage(Language.getString("MSG_PRICE_NOT_CONNECTED"), JOptionPane.WARNING_MESSAGE);
                if (status == JOptionPane.YES_OPTION) {
                    needToConnect = true;
                } else {
                    needToConnect = false;
                }
            }
           
            if (needToConnect) {
                if (!false) {
                    if (symbol == null) {
                        if (transaction != null) {
                            String transactionsymbol = SharedMethods.getKey(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType());
                            transactionDialog = new TransactionDialog(portfolio, transactionsymbol, quantity, type, queued, qID, executionType, bookKeeper);
                        } else {
                            if (TradingShared.isShariaTestPassed(getSelectedSymbol())) {
                                transactionDialog = new TransactionDialog(portfolio, getSelectedSymbol(), quantity, type, queued, qID, executionType, bookKeeper);
                            } else {
                                return null;
                            }
                        }
                    } else {
                        if (TradingShared.isShariaTestPassed(symbol)) {
                            transactionDialog = new TransactionDialog(portfolio, symbol, quantity, type, queued, qID, executionType, bookKeeper);
                        } else {
                            return null;
                        }
                    }
                    if (transaction != null) {
                        transactionDialog.setTransaction(transaction, type);
                    }
                    transactionDialog.setSquareOrderType(isSqureOff);
                    if (price >= 0) {
                        transactionDialog.setPrice(price);
                    }
                    getDesktop().add(transactionDialog);
                    // GUISettings.setLocationRelativeTo(transactionDialog, getDesktop());
                    transactionDialog.setLocationRelativeTo(getDesktop());
                    transactionDialog.setVisible(true);
                }
            }
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }

        return transactionDialog;
    }

    public TransactionDialog doTransaction(int type, String symbol, long quantity, Transaction transaction, boolean queued, long qID,
                                           int executionType, byte source, String portfolio, String bookKeeper) {
        return doTransaction(type, symbol, quantity, transaction, queued, qID, executionType, source, portfolio, false, bookKeeper, -1D);
    }

    public TransactionDialog doTransaction(int type, String symbol, long quantity, Transaction transaction, boolean queued, long qID,
                                           int executionType, byte source, String portfolio, String bookKeeper, double price) {
        return doTransaction(type, symbol, quantity, transaction, queued, qID, executionType, source, portfolio, false, bookKeeper, price);
    }

    public TransactionDialog doTransaction(int type, String symbol, long quantity, Transaction transaction, boolean queued, long qID,
                                           int executionType, byte source, String portfolio, boolean isSqureOff, String bookKeeper) {
        return doTransaction(type, symbol, quantity, transaction, queued, qID, executionType, source, portfolio, isSqureOff, bookKeeper, -1D);
    }


//     public TransactionDialog doTransaction(int type, String symbol, long quantity, Transaction transaction, boolean queued, long qID,
//                                           int executionType, byte source, String portfolio, boolean isSquareOffOrder) {
//        return doTransaction(type, symbol, quantity, transaction, queued, qID, executionType, source, portfolio, false, isSquareOffOrder);
//
//    }

    public boolean changeTradePass(String message) {
        if (TradingShared.isConnected()) {
            try {
                ChangeTradingPass changePass = new ChangeTradingPass(Client.getInstance().getFrame(),
                        Language.getString("CHANGE_TRADE_PASSWORD"), message);
                changePass.showDialog();
                String newPassword = changePass.getNewPassword();
                String oldPassword = changePass.getOldPassword();
                String newTradingPassword = changePass.getNewTradingPassword();
                String oldTRadingPassword = changePass.getOldTradingPassword();
                changePass.dispose();
                changePass = null;

                if (newPassword != null) {
                    TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_CHANGE_PW);
                    tradeMessage.addData("A" + TradingShared.getTrader().getUserID(Constants.PATH_PRIMARY));
                    tradeMessage.addData("B" + oldPassword);
                    tradeMessage.addData("C" + newPassword);
                    if ((TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW_ALLWAYS) ||
                            (TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW)) {
                        tradeMessage.addData("D" + oldTRadingPassword);
                        tradeMessage.addData("E" + newTradingPassword);
                    }
                    MIXObject changePasswordReq = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_AUTHENTICATION, MIXConstants.REQUEST_TYPE_CHANG_PWD, Constants.PATH_PRIMARY);
                    ChangePassword changePWD = new ChangePassword();
                    changePWD.setUserID(TradingShared.getTrader().getUserID(Constants.PATH_PRIMARY));

                    if(TWControl.isPasswordEncryptionEnabled()){

                        String md5PasswordOld = TradeMethods.getEncryptedPassword(oldPassword);
                        String md5PasswordNew = TradeMethods.getEncryptedPassword(newPassword);
                        int replaceDigit = TradeMethods.getReplaceDigitForSalt(md5PasswordOld);

                        String passwordOld = TradeMethods.getSaltedPassword(md5PasswordOld, replaceDigit);
                        String passwordNew = TradeMethods.getSaltedPassword(md5PasswordNew, replaceDigit);

                        changePWD.setOldLoginPwd(passwordOld);
                        changePWD.setNewLoginPwd(passwordNew);
                        changePWD.setPwRandomCharPosition(replaceDigit);
                    }else{
                        changePWD.setOldLoginPwd(oldPassword);
                        changePWD.setNewLoginPwd(newPassword);
                    }

                    if ((TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW_ALLWAYS) ||
                            (TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW)) {
                        changePWD.setOldTradingPwd(oldTRadingPassword);
                        changePWD.setNewTradingPwd(newTradingPassword);
                    }
                    changePWD.setChangePwdType(MIXConstants.CHANGE_PWD_BOTH);
                    changePasswordReq.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{changePWD});
                    // com.isi.csvr.trading.SendQueue.getSharedInstance().writeData(tradeMessage.toString(), Constants.PATH_PRIMARY);
                    com.isi.csvr.trading.SendQueue.getSharedInstance().writeData(changePasswordReq.getMIXString(), Constants.PATH_PRIMARY);
                    tradeMessage = null;
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                System.out.println(e + "");
                e.printStackTrace();
                return false;
            }
        } else {
            return true; // assume as suceeded
        }

    }

    public void popupMenuCanceled(PopupMenuEvent e) {

    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        validatePopupMenuItems();
    }

    public void toggleScreen(final boolean fullScreen) {
        if (fullScreen) {
            Settings.setScreenSize(window.getWidth(), window.getHeight());
        }
        Container container = window.getContentPane();
        TWFrame frame = new TWFrame();
        if (fullScreen) {
            frame.setUndecorated(true);
        }

        frame.setContentPane(container);
        frame.setJMenuBar(window.getJMenuBar());
        frame.setTitle(window.getTitle());
        frame.setIconImage(window.getIconImage());
        frame.addWindowListener(this);
        window.setVisible(false);
        if (fullScreen) {
            window.setBounds(frame.getBounds());
        } else {
            window.setBounds(new Rectangle(0, 0, Toolkit.getDefaultToolkit().getScreenSize().width, Toolkit.getDefaultToolkit().getScreenSize().height));
        }
        window.dispose();
        window = null;
        window = frame;
        window.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                if (fullScreen) {
                    window.setExtendedState(Frame.MAXIMIZED_BOTH);
                    window.setVisible(true);
                } else {
                    if (!Settings.isDualScreenMode()) {
                        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
                        window.setSize(new Dimension((int) d.getWidth(), (int) d.getHeight() - 25));
                        window.setExtendedState(JFrame.MAXIMIZED_BOTH);
                    } else {
                        window.setSize(Settings.getScreenSize());
                    }
                    window.setVisible(true);
                    window.repaint();
                }
            }
        }

        );
        Settings.setFullScreenMode(fullScreen);
    }

    /*  private static DisplayMode getBestDisplayMode(GraphicsDevice device) {
      for (int x = 0, xn = MODES.length; x < xn; x++) {
        DisplayMode[] modes = device.getDisplayModes();
        for (int i = 0, in = modes.length; i < in; i++) {
          if (modes[i].getWidth() == MODES[x].getWidth()
              && modes[i].getHeight() == MODES[x].getHeight()
              && modes[i].getBitDepth() == MODES[x].getBitDepth()) {
            return MODES[x];
          }
        }
      }
      return null;
    }*/

    public void showEntitlements
            (
                    boolean center) {
        EntitlementsTree.initialize();
        getDesktop().add(EntitlementsTree.getSharedInstance());
        EntitlementsTree.getSharedInstance().setLayer(GUISettings.TOP_LAYER);
        if (center) {
            EntitlementsTree.getSharedInstance().setLocationRelativeTo(getDesktop());
        } else {
            Dimension size = EntitlementsTree.getSharedInstance().getSize();
            EntitlementsTree.getSharedInstance().setLocation(getDesktop().getWidth() - (int) size.getWidth(),
                    getDesktop().getHeight() - (int) size.getHeight() - 20);
            EntitlementsTree.getSharedInstance().showAllNodes(true);
        }
        EntitlementsTree.getSharedInstance().show();

    }

    public void setTableCurrency(String currency) {
        if ((currency != null) && (currency.equals("*"))) { // use default currency
            currency = null;
        }
        if (mainboardSelected) {
            g_oTempTable.setCurrency(currency);
            UpdateNotifier.setShapshotUpdated();
//            CurrencyStore.getSharedInstance().print();
        } else if (mistViewSelected) {
            g_oMistTable.getViewSettings().setCurrency(currency);
            ((MISTModel) g_oMistTable.getTable().getModel()).setCurrency(currency);
            g_oMistTable.setWindowTitle();
//            UpdateNotifier.setShapshotUpdated();
        }
    }

    public JPopupMenu getTablePopup() {
        return g_oTablePopup;
    }

    public JPopupMenu getHivePopup() {
        return hivePopup;
    }

    public JPopupMenu getWatchListPopup() {
        return g_oWatchListPopup;
    }

    public void openCustomerSupportFrame() {
        if (customerSupportFrame == null) {
            customerSupportFrame = new CustomerSupportMessageWindow();
            getDesktop().add(customerSupportFrame);
            getDesktop().setLayer(customerSupportFrame, GUISettings.TOP_LAYER);
            customerSupportFrame.setLocationRelativeTo(getDesktop());
        }
        customerSupportFrame.show();
    }

    /*private JPanel getExchangePanel() {
            JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEADING));

            JLabel label = new JLabel(Language.getString("EXCHANGE"));
            panel.add(label);

            exchanges = new ArrayList();

            exchangeCombo = new JComboBox(new TWComboModel(exchanges));
            popuplateExchanges();
            exchangeCombo.addActionListener(new ActionListener(){
                public void actionPerformed(ActionEvent e){
                    if(e.getSource()== exchangeCombo){
                        TWComboItem comboItem = (TWComboItem) exchangeCombo.getSelectedItem();
                        oMarketTableModel.setExchange(comboItem.getId());
                    }
                }
            });
            panel.add(exchangeCombo);
            return panel;
        }*/

    private void popuplateExchanges() {
        exchanges.clear();
        try {
            for (int i = 0; i < ExchangeStore.getSharedInstance().count(); i++) {
                Exchange exchange = ExchangeStore.getSharedInstance().getExchange(i);
                if (exchange.isDefault() && !exchange.isExpired() && !exchange.isInactive()) {
                    TWComboItem comboItem = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                    exchanges.add(comboItem);
                }
                exchange = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {
            exchangeCombo.updateUI();
            exchangeCombo.setSelectedIndex(0);
        } catch (Exception e) {
        }
    }

    private void createDecimalMenu() {
        mnuDecimalPlaces = new TWMenu(Language.getString("POPUP_DECIMAL_PLACES"), "decimalplaces.gif");
        ButtonGroup oGroup = new ButtonGroup();
        g_oWatchListPopup.add(mnuDecimalPlaces);
        watchListPopup.add(mnuDecimalPlaces);

        mnuDefaultDecimal = new TWRadioButtonMenuItem(Language.getString("DEFAULT_DECIMAL"));
        mnuDecimalPlaces.add(mnuDefaultDecimal);
        mnuDefaultDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuDefaultDecimal);
        mnuNoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_NO_DECIMAL"));
        mnuDecimalPlaces.add(mnuNoDecimal);
        mnuNoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //QuoteStore.setDecimalPlaces(ESPConstants.ONE_DECIMAL_PLACES);
                //TableRenderer.reload();
                setDecimalPlaces(Constants.NO_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuNoDecimal);
        mnuOneDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_ONE_DECIMAL"));
        mnuDecimalPlaces.add(mnuOneDecimal);
        mnuOneDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //QuoteStore.setDecimalPlaces(ESPConstants.ONE_DECIMAL_PLACES);
                //TableRenderer.reload();
                setDecimalPlaces(Constants.ONE_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuOneDecimal);
        mnuTwoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_TWO_DECIMAL"));
        mnuDecimalPlaces.add(mnuTwoDecimal);
        mnuTwoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //QuoteStore.setDecimalPlaces(ESPConstants.TWO_DECIMAL_PLACES);
                //TableRenderer.reload();
                setDecimalPlaces(Constants.TWO_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuTwoDecimal);
        mnuThrDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_THREE_DECIMAL"));
        mnuDecimalPlaces.add(mnuThrDecimal);
        mnuThrDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //QuoteStore.setDecimalPlaces(ESPConstants.THREE_DECIMAL_PLACES);
                //TableRenderer.reload();
                setDecimalPlaces(Constants.THREE_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuThrDecimal);
        mnuFouDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_FOUR_DECIMAL"));
        mnuDecimalPlaces.add(mnuFouDecimal);
        mnuFouDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //QuoteStore.setDecimalPlaces(ESPConstants.FOUR_DECIMAL_PLACES);
                //TableRenderer.reload();
                setDecimalPlaces(Constants.FOUR_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuFouDecimal);
    }

    private void setDecimalPlaces(byte places) {
        if (!mistViewSelected) {
            ((DraggableTable) g_oTempTable.getTable()).setDecimalPlaces(places);
            g_oTempTable.updateGUI();
            g_oTempTable.updateUI();
            g_oTempTable.getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, places);
        } else {
            ((MISTModel) g_oMistTable.getTable().getModel()).setDecimalPlaces(places);
            g_oMistTable.getTable().updateUI();
            g_oMistTable.getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, places);
        }
        if (places == Constants.NO_DECIMAL_PLACES) {
            mnuNoDecimal.setSelected(true);
        } else if (places == Constants.ONE_DECIMAL_PLACES) {
            mnuOneDecimal.setSelected(true);
        } else if (places == Constants.TWO_DECIMAL_PLACES) {
            mnuTwoDecimal.setSelected(true);
        } else if (places == Constants.THREE_DECIMAL_PLACES) {
            mnuThrDecimal.setSelected(true);
        } else if (places == Constants.FOUR_DECIMAL_PLACES) {
            mnuFouDecimal.setSelected(true);
        } else if (places == Constants.UNASSIGNED_DECIMAL_PLACES) {
            mnuDefaultDecimal.setSelected(true);
        }
    }

    public void openChatSupportFrame() {
        if (chatWindow != null) {
            chatWindow.dispose();
            chatWindow = null;
        }
        chatWindow = new ChatFrame();
        chatWindow.show();
    }

    public void openWhatifCalculatorframe() {
        WhatifCalculatorFrame.getSharedInstannce().showWindow();
    }

    public void showScanner() {
        if (scanner == null) {
            scanner = new MainScannerWindow();
        } else {
            scanner.setVisible(true);

            try {
                if (scanner.isIcon())
                    scanner.setIcon(false);
                scanner.setSelected(true);

            } catch (PropertyVetoException e) {

            }
        }
    }



    public void openMetaStockAutoUpdateFrame() {
        MetaStockAutoUpdateFrame.getSharedInstannce().showWindow();
    }

    public void openTradeStationAutoUpdateFrame() {
        TradeStationAutoUpdateFrame.getSharedInstannce().showWindow();
    }

    public void createSummaryWindow(String viewsettingID) {
        if (viewsettingID.equals("BTW_ANNOUNCEMENT")) {
            createAnnouncementFrame();
            AnnouncementSearchWindow.getSharedInstance().selectRealtimeAnnoucement();
            /*} else if(viewsettingID.equals("BTW_BROADCAST_MESSAGE")){
            createBrodMessageFrame();*/
        } else if (viewsettingID.equals("BTW_SECTORMAP")) {
            createSectorMap();
        } else if (viewsettingID.equals("NEWS_SEARCH")) {
            createNewsFrame();
            NewsWindow.getSharedInstance().selectRealTimeNews();
        } else if (viewsettingID.equals("BTW_NEWS")) {
            createNewsFrame();
            NewsWindow.getSharedInstance().selectRealTimeNews();
            NewsStore.getSharedInstance().applySourceFilter();
        } else if (viewsettingID.equals("BTW_FINANCIAL_CALENDER")) {
            createCAFrame();
        } else if (viewsettingID.equals("MUTUAL_FUND")) {
            mutualFundtFrame = createMutualFundFrame();
        } else if (viewsettingID.equals("ANNOUNCEMENT_SEARCH")) {
            createAnnouncementFrame();
            AnnouncementSearchWindow.getSharedInstance().selectRealtimeAnnoucement();
        } else if (viewsettingID.equals("BTW_SECTORS")) {
//            g_oIndicesFrame = mainBoardLoader.createIndicesFrame();
            g_oIndicesFrame = IndicesFrame.getSharedInstance();// mainBoardLoader.createIndicesFrame();
            g_oIndicesFrame.popuplateExchanges();
        } else if (viewsettingID.equals("BTW_FULL_TIME&SALES")) {
            g_oFullTimeSalesFrame = createFullTimeSalesFrame();
        } else if (viewsettingID.equals("BTW_EXCHANGES")) {
            g_oExchangeFrame = createExchangesFrame();
        } else if (viewsettingID.equals("HISTORY_ANALYZER")) {
            netCalcFrame = createChangeCalcFrame();
        } else if (viewsettingID.equals("HEATMAP")) {
            createHeatMap();
        } else if (viewsettingID.equals("ALERT")) {
            AlertManager.getSharedInstance();
        } else if (viewsettingID.equals("BLOTTER")) {
            blotterFrame = TradeMethods.getSharedInstance().createBlotterFrame();
        } else if (viewsettingID.equals("OPENPOSITIONS")) {
            openPositionsFrame = TradeMethods.getSharedInstance().createOpenPositionsFrame();
        } else if (viewsettingID.equals("STOCKTRANSFER")) {
            stockTransferWindow = TradeMethods.getSharedInstance().createStockTranferFrame();
        } else if (viewsettingID.equals("ORDER_Q")) {
            orderQFrame = createOrderQFrame();
        } else if (viewsettingID.equals("PROG_ORDERS")) {
            programedOrderFrame = createProgramedOrderFrame();
        } else if (viewsettingID.equals("ACCOUNT")) {
            TradeMethods.getSharedInstance().createAccountWindow();
            TradeMethods.getSharedInstance().getAccountWindow().setisLoadedFromWsp(true);

        } else if (viewsettingID.equals("FUNDTRANSFER")) {
            TradeMethods.getSharedInstance().createFundTranferFrame();
        } else if (viewsettingID.equals("ORDER_SEARCH")) {
            TradeMethods.getSharedInstance().createOrderSearchWindow();
        } else if (viewsettingID.equals("CURRENCY_TABLE")) {
            currencyFrame = createCurrencyFrame();
        } else if (viewsettingID.equals("MULTIWINDOW")) {
            // MultiDialogWindow.getSharedInstance();
        } else if (viewsettingID.equals("UNICHART")) {
            createChartFrame();
        } else if (viewsettingID.equals("BROKERS")) {
            brokersWindow = BrokersWindow.getSharedInstance();
            System.out.println("broker window created");
        } else if (viewsettingID.equals("BROWSER")) {
            createBrowserFrame();
        } else if (viewsettingID.equals("CASHLOG_SEARCH")) {
            TradeMethods.getSharedInstance().createCashLogSearchWindow();
//        }else if(viewsettingID.equals("BTW_ANNOUNCEMENT")){
//            createAnnouncementFrame();
//        }else if(viewsettingID.equals("BTW_ANNOUNCEMENT")){
//            createAnnouncementFrame();
//        }else if(viewsettingID.equals("BTW_ANNOUNCEMENT")){
//            createAnnouncementFrame();
        } else if (viewsettingID.equals("GLOBAL_INDEX")) {
            if (ViewSettingsManager.getSummaryView(viewsettingID).isVisible()) {
                createGlobalIndexFrame();
            }
        } else if (viewsettingID.equals("SIDE_BAR")) {
            if (ViewSettingsManager.getSummaryView(viewsettingID).isVisible()) {
                createSideBar();
            }
        } else if (viewsettingID.equals("VOLUME_WATCHER_VIEW")) {
            if (ViewSettingsManager.getSummaryView(viewsettingID).isVisible()) {
                VolumeWatcherUI.getSharedInstance().setLoadedFromWsp(true);
                VolumeWatcherUI.getSharedInstance().showVolumeWatcher(true);
            }
        } else if (viewsettingID.equals("CASHFLOW_WATCHER_VIEW")) {
            if (ViewSettingsManager.getSummaryView(viewsettingID).isVisible()) {
                CashFlowWatcherUI.getSharedInstance();
            }
        } else if (viewsettingID.equals("GLOBAL_MARKET_SUMMARY")) {
            createGlobalMarketSummary(true);
        } else if (viewsettingID.equals("TOP_STOCKS")) {
            TopStocksWindow.getSharedInstance();
        } else if (viewsettingID.equals("BTW_BROADCAST_MESSAGE")) {
            MessageWindow.getSharedInstance();
//            createBrodMessageFrame();
        } else if (viewsettingID.equals("IPO_SUBSCRIBE")) {
            if (ViewSettingsManager.getSummaryView(viewsettingID).isVisible()) {
                TradeMethods.getSharedInstance().showIPOSubscribeFrame();
            }
        } else if (viewsettingID.equals("CASHFLOWHISTORY_SUMMARY_VIEW")) {
            CashFlowHistory.getSharedInstance().showSummaryFromWSP();
            // CashFlowHistorySummaryUI.getSharedInstance().setLoadedFromWsp(true);
        } else if (viewsettingID.equals("MARGINABLE_SYMBOLS")) {
            if (ViewSettingsManager.getSummaryView(viewsettingID).isVisible()) {
                TradeMethods.getSharedInstance().showMarginableSymbolFrame();
            }
        }
        /* else if (viewsettingID.equals("BANDWIDTH_OPTIMIZER")) {
            if (ViewSettingsManager.getSummaryView(viewsettingID).isVisible()) {
                IS_BANDOP_VISIBLE = true;
                getDesktop().add(MainUI.getSharedInstance());
                MainUI.getSharedInstance().loadTree();
                MainUI.getSharedInstance().setVisible(true);
            }
        }*/
        else if (viewsettingID.equals("RADAR_SCREEN_VIEW")) {
            if (ViewSettingsManager.getSummaryView(viewsettingID).isVisible()) {
                RadarScreenWindow.getSharedInstance().showWindow();
            }
        }
    }

    public void createForexView(String viewsettingID) {
//        if(viewsettingID.equals("8")){
//           createNewForexView(true,null);
//        }
    }

    private void createSideBar() {

        SideBar.getSharedInstance().setSideBar();
    }

    private void createGlobalIndexFrame() {
        GIFrame = GlobalIndexWindow.getSharedInstance();
        GUISettings.applyOrientation(GIFrame);
        GIFrame.setLayer(GUISettings.TOP_LAYER);
        oTopDesktop.add(GIFrame);
        GIFrame.setVisible(true);
    }

    private void createChartFrame() {
        chartFrame = ChartFrame.getSharedInstance();
        oTopDesktop.add(chartFrame);
        chartFrame.setResizable(true);
        chartFrame.setMaximizable(true);
        chartFrame.setIconifiable(true);
        chartFrame.setClosable(true);
        chartFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        GUISettings.applyOrientation(chartFrame);
        chartFrame.setLayer(GUISettings.TOP_LAYER);
        chartFrame.applySettings();
        /*ViewSetting defaultSettings = ViewSettingsManager.getSummaryView("UNICHART");
        if (defaultSettings != null) {
            chartFrame.setSize(defaultSettings.getSize());
        } else {
            chartFrame.setSize(500, 500);
        }
        chartFrame.setLocation(-1000, -1000); // invalidate the location so that it can be centered
        defaultSettings.setParent(chartFrame);
        defaultSettings = null;*/


    }

    private void createBrowserFrame() {
        ViewSetting oBrowserSettings = ViewSettingsManager.getSummaryView("BROWSER");
        g_oBrowserFrame = new BrowserFrame();
//g_oBrowserNode = new DefaultMutableTreeNode(g_oBrowserFrame);
        g_oBrowserFrame.setLayer(GUISettings.TOP_LAYER);
        oTopDesktop.add(g_oBrowserFrame);
        g_oBrowserFrame.setResizable(true);
        g_oBrowserFrame.setMaximizable(true);
        g_oBrowserFrame.setIconifiable(true);
        g_oBrowserFrame.setClosable(true);
        g_oBrowserFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
//g_oTreeTop.add(g_oBrowserNode);
        oBrowserSettings.setParent(g_oBrowserFrame);
        g_oBrowserFrame.setViewSettings(oBrowserSettings);
        GUISettings.applyOrientation(g_oBrowserFrame);
    }

    private void createDecimalMenu(JPopupMenu popup, final Table table) {
        TWMenu mnuDecimalPlaces = new TWMenu(Language.getString("POPUP_DECIMAL_PLACES"), "decimalplaces.gif");
        popup.add(mnuDecimalPlaces);

        //transHistoryTable.getPopup().add(mnuDecimalPlaces);
        ButtonGroup oGroup = new ButtonGroup();
        mnuNoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_NO_DECIMAL"));
        mnuDecimalPlaces.add(mnuNoDecimal);
        mnuNoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.NO_DECIMAL_PLACES, table);
            }
        });
        if (((SmartTable) table.getTable()).getDecimalPlaces() == Constants.NO_DECIMAL_PLACES) {
            mnuNoDecimal.setSelected(true);
        }
        oGroup.add(mnuNoDecimal);
        mnuOneDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_ONE_DECIMAL"));
        mnuDecimalPlaces.add(mnuOneDecimal);
        mnuOneDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.ONE_DECIMAL_PLACES, table);
            }
        });
        if (((SmartTable) table.getTable()).getDecimalPlaces() == Constants.ONE_DECIMAL_PLACES) {
            mnuOneDecimal.setSelected(true);
        }
        oGroup.add(mnuOneDecimal);
        mnuTwoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_TWO_DECIMAL"));
        mnuDecimalPlaces.add(mnuTwoDecimal);
        mnuTwoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.TWO_DECIMAL_PLACES, table);
            }
        });
        if (((SmartTable) table.getTable()).getDecimalPlaces() == Constants.TWO_DECIMAL_PLACES) {
            mnuTwoDecimal.setSelected(true);
        }
        oGroup.add(mnuTwoDecimal);
        mnuThrDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_THREE_DECIMAL"));
        mnuDecimalPlaces.add(mnuThrDecimal);
        mnuThrDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.THREE_DECIMAL_PLACES, table);
            }
        });
        if (((SmartTable) table.getTable()).getDecimalPlaces() == Constants.THREE_DECIMAL_PLACES) {
            mnuThrDecimal.setSelected(true);
        }
        oGroup.add(mnuThrDecimal);
        mnuFouDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_FOUR_DECIMAL"));
        mnuDecimalPlaces.add(mnuFouDecimal);
        mnuFouDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.FOUR_DECIMAL_PLACES, table);
            }
        });
        if (((SmartTable) table.getTable()).getDecimalPlaces() == Constants.FOUR_DECIMAL_PLACES) {
            mnuFouDecimal.setSelected(true);
        }
        oGroup.add(mnuFouDecimal);

        GUISettings.applyOrientation(mnuDecimalPlaces);
    }

    private void setDecimalPlaces(byte places, Table table) {
//        ((SmartTable)table.getTable()).setDecimalPlaces(places);
//        ((SmartTable)table.getTable()).updateUI();
        table.getModel().setDecimalCount(places);
//        table.updateGUI();
        table.getModel().getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, places);
    }

    public void showStretchedHeatMap() {
        showStretchedHeatMapForTheWatchList(mainBoardLoader.getClientTable(ExchangeStore.getSelectedExchangeID()));
    }

    public void showStretchedHeatMapForTheWatchList(ClientTable table) {
        String[] sSymbols = null;
        sSymbols = table.getSymbols().getFilteredSymbols();
        StretchedHeatMapframe sthmf = StretchedHeatMapframe.getSharedInstance(sSymbols, table.getTitle());//new StretchedHeatMapframe(sSymbols, table.getTitle());
        sthmf.setClosable(true);
        sthmf.setIconifiable(true);
        sthmf.setResizable(true);
        oTopDesktop.add(sthmf);
        sthmf.show();
        //StretchedHeatMapframe.getSharedInstance(sSymbols, table.getTitle());//
    }

    public void showStretchedHeatMapForTheWatchList(MISTTable table) {
        System.out.println("STRETCHED_HEAT_MAP table title = " + table.getTitle());
        String[] sSymbols = null;
        sSymbols = table.getViewSettings().getSymbols().getFilteredSymbols();
        System.out.println("Testing SYMBOL : " + sSymbols[0]);
        StretchedHeatMapframe sthmf = StretchedHeatMapframe.getSharedInstance(sSymbols, table.getTitle());//new StretchedHeatMapframe(sSymbols, table.getTitle());
        sthmf.setClosable(true);
        sthmf.setIconifiable(true);
        sthmf.setResizable(true);
        oTopDesktop.add(sthmf);
        sthmf.show();
        //StretchedHeatMapframe.getSharedInstance(sSymbols, table.getTitle());//
    }

    private void createTabbedpanel() {
        Dimension d = Toolkit.getDefaultToolkit().getScreenSize();
        tabPanel = new JPanel();
        // tab- tabPanel.setPreferredSize(new Dimension((int) d.getWidth(), 23));
        // tab-  windowTabs.setPreferredSize(new Dimension((int) d.getWidth(), 23));
        tabPanel.setOpaque(true);

        // tab-windowTabs.setUI(new CWTabbedPaneUI());
        windowTabs.addTabPanelListener(this);
        String[] PanelWidths = {"100%", "0"};
        String[] PanelHeights = {"0"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(PanelWidths, PanelHeights, 3, 0);
        tabPanel.setLayout(flexGridLayout);
        tabPanel.setOpaque(true);
        // tab-windowTabs.setTabPlacement(JTabbedPane.BOTTOM);
        // tab-windowTabs.setTabLayoutPolicy(JTabbedPane.SCROLL_TAB_LAYOUT);
        tabPanel.add(windowTabs);
        windowTabs.setBorder(BorderFactory.createEmptyBorder());
        removeTanIndexButton = new JButton();
        removeTanIndexButton.setContentAreaFilled(false);
        removeTanIndexButton.setBorder(BorderFactory.createEmptyBorder());
        //removeTanIndexButton.setPreferredSize(new Dimension(23, 23));
        removeTanIndexButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                removeTabIndexPanel();
                Client.getInstance().getMenus().setTabIndexVisibleMode(false);
                Settings.setTabIndex(false);
            }
        });
        tabPanel.add(removeTanIndexButton);
    }

    public void loadCloseButtonImage(String themeID) {

        removeTanIndexButton.setIcon(new ImageIcon("images/Theme" + Settings.getLastThemeID() + "/closetabs.gif"));

    }

    private JPanel createBottomPanel() {
        tabAndStatusbar = new JPanel();
        tabAndStatusbar.setLayout(new BorderLayout());
        if (Settings.isTabIndex()) { // todo enable tab here
            tabAndStatusbar.add(bottomtickerPanel, BorderLayout.NORTH);
            tabAndStatusbar.add(tabPanel, BorderLayout.CENTER);
            tabAndStatusbar.add(statusBarPanel, BorderLayout.SOUTH);
        } else {
            tabAndStatusbar.add(bottomtickerPanel, BorderLayout.NORTH);
            tabAndStatusbar.add(statusBarPanel, BorderLayout.SOUTH);
        }
        return tabAndStatusbar;
    }

    public TWTabbedPane getWindowTab() {
        return windowTabs;
    }

    public JPanel getBottomPanel() {
        return tabAndStatusbar;
    }

    public JPanel getTabIndexPanel() {
        return tabPanel;
    }

    public void removeTabIndexPanel() {
        JPanel bottomPanel = getBottomPanel();
        bottomPanel.removeAll();
        bottomPanel.setLayout(new BorderLayout());
        bottomPanel.add(bottomtickerPanel, BorderLayout.NORTH);
        bottomPanel.add(statusBarPanel, BorderLayout.SOUTH);
        statusBarPanel.updateUI();
        bottomPanel.updateUI();
        oTopDesktop.refreshDesktop();


    }

    public void addTabIndexpanel(boolean status) {
        try { // todo enable tab here
            if (status) {
                JPanel bottomPanel = getBottomPanel();
                if (bottomPanel == null) {
                    bottomPanel = createBottomPanel();
                }
                bottomPanel.removeAll();
                bottomPanel.setLayout(new BorderLayout());
                tabAndStatusbar.add(bottomtickerPanel, BorderLayout.NORTH);
                tabAndStatusbar.add(getTabIndexPanel(), BorderLayout.CENTER);

                tabAndStatusbar.add(statusBarPanel, BorderLayout.SOUTH);
                bottomPanel.updateUI();
                window.getContentPane().doLayout();
            } else {
                removeTabIndexPanel();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void applicationLoading(int percentage) {

    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {

    }

    public void applicationReadyForTransactions() {
//                new Thread("main") {
//            public void run() {
//                try {
//                    Plugin plugin = PluginStore.getPlugin("MEDIA_PLAYER");
//                    if (plugin == null) {
//                        plugin = PluginStore.activate("MEDIA_PLAYER");
//                    }
//                    plugin.showUI("MEDIA_PLAYER");
//                } catch (Exception e1) {
//                    e1.printStackTrace();
//                }
//            }
//        }.start();

    }

    public void applicationExiting() {

    }

    public void applicationTimeZoneChanged(TimeZone zone) {

    }

    public void workspaceWillLoad() {

    }

    public void workspaceLoaded() {
//        new Thread() {
//            public void run() {
//                try {
//                    Plugin plugin = PluginStore.getPlugin("MEDIA_PLAYER");
//                    if (plugin == null) {
//                        plugin = PluginStore.activate("MEDIA_PLAYER");
//                    }
//                    plugin.showUI("MEDIA_PLAYER");
//                } catch (Exception e1) {
//                    e1.printStackTrace();
//                }
//            }
//        }.start();

    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void snapshotProcessingStarted(Exchange exchange) {

    }

    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void selectedExchangeChanged(Exchange exchange) {

    }


    public void tabStareChanged(TWTabEvent event) {

//    public void stateChanged(ChangeEvent e) {
        try {
            if (event.getState() == TWTabEvent.STATE_SELECTED) {
                JInternalFrame iframe = (JInternalFrame) windowTabs.getSelectedComponent();
//                SharedMethods.printLine("Selected " + iframe.getTitle());
                if (iframe.isIcon()) {
                    if (!iframe.isSelected()) {
                        iframe.setSelected(true);
                    }
                } else if (!iframe.isSelected()) {
                    iframe.setSelected(true);
                }
            }
        } catch (PropertyVetoException e1) {
            System.out.println("InternalFrame selection failed......");
            e1.printStackTrace();                                             //To change body of catch statement use File | Settings | File Templates.
        }

        /*if (e.getSource() == windowTabs) {
           Enumeration ee = TWDesktop.tabHashTable.keys();
            while (ee.hasMoreElements()) {
                InternalFrame iframe = (InternalFrame) ee.nextElement();
                if (windowTabs.getSelectedComponent().equals(TWDesktop.tabHashTable.get(iframe))) {
                    //SharedMethods.printLine("Change Event " + iframe.getTitle(), true);
                    try {
                        if (iframe.isIcon()) {
                            //iframe.setIcon(false);
                            iframe.setSelected(true);
                        }else if (!iframe.isSelected()) {
                            iframe.setSelected(true);
                        }
                    } catch (PropertyVetoException e1) {
                        System.out.println("InternalFrame selection failed......");
                        e1.printStackTrace();                                             //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        }*/
    }

    public void setFXBoardSelected(ForexBoard fxb) {
        g_oFXBoard = fxb;
        mainboardSelected = false;
        portfolioSelected = false;
        mistViewSelected = false;
        forexViewSelected = true;
    }

    public void setSideBarSelected(SymbolNavigator sBar) {
        g_oSideBar = sBar;
        mainboardSelected = false;
        portfolioSelected = false;
        mistViewSelected = false;
        forexViewSelected = false;
        sideBarSelected = true;
    }

    private void createStockReportsView() {

        StockReportWindow stockReportWindow = new StockReportWindow();
        stockReportFrame = new InternalFrame();
        stockReportFrame.setTitle(Language.getString("WINDOW_TITLE_STOCK_REPORT"));
        ViewSetting setting = g_oViewSettings.getSummaryView("STOCK_REPORTS");
        if (setting != null) {
            stockReportFrame.setSize(setting.getSize());
            stockReportFrame.setMinimumSize(setting.getSize());
            stockReportFrame.setMaximumSize(setting.getSize());
        } else {
            stockReportFrame.setSize(420, 180);
        }
        setting = null;
        stockReportFrame.setLocation(-100, -100);
        stockReportFrame.getContentPane().add(stockReportWindow);
        stockReportFrame.setResizable(true);
        stockReportFrame.setClosable(true);
        stockReportFrame.setIconifiable(true);
        stockReportFrame.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
        stockReportFrame.setLayer(GUISettings.TOP_LAYER);
        oTopDesktop.add(stockReportFrame);
        setStockReportDownloading(false);
    }

    public void closeStockReportWindow() {
        stockReportFrame.setVisible(false);
    }

    public InternalFrame getStockReportFrame() {
        if (stockReportFrame == null) {
            createStockReportsView();
        }
        return stockReportFrame;
    }

    public void setStockReportDownloading(boolean inprogress) {
        if (inprogress) {
            stockReportFrame.setTitle(Language.getString("WINDOW_TITLE_STOCK_REPORT") + " - " +
                    Language.getString("LBL_DOWNLOADING"));
        } else {
            stockReportFrame.setTitle(Language.getString("WINDOW_TITLE_STOCK_REPORT"));
            StockReportWindow.setBusy(false);
        }
    }

    public void showEmptyStockreportWindow() {
        getStockReportFrame().setVisible(true);
        StockReportWindow.showLoading();//setData(Settings.NULL_STRING);
        if (stockReportFrame.getLocation().getX() == -100) // never shown before
            GUISettings.setLocationRelativeTo(stockReportFrame, oTopDesktop);

    }

    public void sendStockReportRequest(String symbol, String exchange) {
        SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, Meta.STOCK_REPORT_SEARCH + Meta.DS + exchange + Meta.FD + symbol + Meta.EOL, exchange, Meta.STOCK_REPORT_SEARCH);
        showEmptyStockreportWindow();
    }

    public void savePDF(String id) {
        try {
            Process process = Runtime.getRuntime().exec("START temp\\" + id + ".pdf");
            process.waitFor();
            if (process.exitValue() != 0) {
                new ShowMessage(Language.getString("MSG_PDF_NOT_INSTALLED"), "E");
            }
            process = null;
            stockReportFrame.setTitle(Language.getString("WINDOW_TITLE_STOCK_REPORT"));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
        setStockReportDownloading(false);
    }

    /**
     * Fundemental Data 2
     */
    public void mnu_FD2Symbol(String sSymbol, boolean fromWorkspace) {
        String sSelectedSymbol = null;
        ViewSetting oViewSetting = null;

        try {
            if (sSymbol == null)
                sSelectedSymbol = getSelectedSymbol();
            else
                sSelectedSymbol = sSymbol;

            if (sSelectedSymbol == null)
                return;

            if (sSelectedSymbol.equals(Constants.NULL_STRING))
                return;

            if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return;
            }
            if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return;
            }
            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.FUNDAMENTAL_DATA_VIEW
                    + "|" + sSelectedSymbol);
            if (oFrame != null) {
                oFrame.show();
                return;
            }

//            QuoteStore.setSymbol(false,sSelectedSymbol);

            //Table oTimeNsales = new Table();
            /*  FD2Table fd2Table = new FD2Table(); //sSelectedSymbol);
            fd2Table.setSymbol(sSelectedSymbol);*/
            oViewSetting = g_oViewSettings.getFD2View(ViewSettingsManager.FUNDAMENTAL_DATA_VIEW
                    + "|" + sSelectedSymbol);

            if (oViewSetting == null) {
                //System.out.println("inside null");
                oViewSetting = g_oViewSettings.getSymbolView("FUNDAMENTAL_DATA");
                //System.out.println(oViewSetting == null);
                oViewSetting = oViewSetting.getObject(); // clone the object
                oViewSetting.setID(sSelectedSymbol);
                g_oViewSettings.putFD2View(ViewSettingsManager.FUNDAMENTAL_DATA_VIEW
                        + "|" + sSelectedSymbol, oViewSetting);
//            } else {
//                fd2Table.setLoadedFromTheme(true);
            }

            FD2Table oFD2Frame = new FD2Table(oViewSetting, sSelectedSymbol);
            //InternalFrame oFD2Frame = new InternalFrame(fd2Table, g_oWindowDaemon, ESPConstants.FD2Window, sSelectedSymbol);
            oFD2Frame.setViewSettings(oViewSetting);
            oViewSetting.setParent(oFD2Frame);
            oFD2Frame.setWindowDeamon(g_oWindowDaemon);
            oFD2Frame.setSize(oViewSetting.getSize());
//            oFD2Frame.setLocation(oViewSetting.getLocation());
            oFD2Frame.setWindowType(ViewSettingsManager.FUNDAMENTAL_DATA_VIEW);
            oFD2Frame.setResizable(false);
            oFD2Frame.setClosable(true);
            oFD2Frame.setMaximizable(false);
            oFD2Frame.setIconifiable(true);
            oFD2Frame.setColumnResizeable(false);
            oTopDesktop.add(oFD2Frame);
            try {
                oFD2Frame.setTitle(Language.getString("WINDOW_TITLE_FD2") + " : " + SharedMethods.getDisplayKey(sSelectedSymbol));
            } catch (Exception e) {
                oFD2Frame.setTitle(Language.getString("WINDOW_TITLE_FD2") + " : " +
                        sSelectedSymbol);
            }
//            oFD2Frame.setMenuItem(g_oMenus);
            oFD2Frame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
//            oFD2Frame.getTableComponent().getPopup().showSetDefaultMenu();
//            oViewSetting.setVisible(true);

            if (!fromWorkspace) {
                GUISettings.setLocationRelativeTo(oFD2Frame, getDesktop());
                oViewSetting.setLocation(oFD2Frame.getLocation());
            }

//            fd2Table.setLoadedFromTheme(false);
            //==oFD2Frame.setLayer(10);
            oFD2Frame.setOrientation();
            GUISettings.applyOrientation(oFD2Frame);
//            oFD2Frame.setWindowID(ViewSettingsManager.FUNDAMENTAL_DATA_VIEW + "|" + sSelectedSymbol);
            oFD2Frame.applySettings();
            g_oViewSettings.setWindow(ViewSettingsManager.FUNDAMENTAL_DATA_VIEW + "|" +
                    sSelectedSymbol, oFD2Frame);
            oFD2Frame.setLayer(GUISettings.TOP_LAYER);
            oFD2Frame.show();
            oFD2Frame = null;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mnu_Options(String sSymbol, String instrumentType, boolean fromWorkspace, boolean removeBaseOnExit) {

        final String sSelectedSymbol;
        try {
            if (sSymbol == null) {
                sSelectedSymbol = getSelectedSymbol();
            } else {
                sSelectedSymbol = sSymbol;
            }

            if ((sSelectedSymbol == null) || (sSelectedSymbol.equals(Constants.NULL_STRING)))
                return;

            if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return;
            }
            if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return;
            }
            // check if the window is already created
            // If it exists, do not create it, just focus it
            InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.OPTION_CALLS_VIEW + "|" +
                    sSelectedSymbol);
            if (oFrame != null) {
                if (oFrame.isIcon()) {
                    oFrame.setIcon(false);
                }
//                OptionStore.getSharedInstance().sendOptionChainRequest();
                oFrame.show();
                oFrame.setSelected(true);
                return;
            }

            if (OptionWindow.getPopulation() >= Settings.getMaximumOPRACount()) {
                if (!fromWorkspace) {
                    String str = Language.getString("MSG_OPTION_WINDOW_LIMIT_EXCEED");
                    str = str.replaceAll("\\[COUNT\\]", "" + Settings.getMaximumOPRACount());
                    new ShowMessage(str, "E");
                }
                return;
            }
//            if (fromWorkspace){ // since this is not from workspace, thre will not be a symbol validation. Must add base manually
//                sSelectedSymbol = SharedMethods.getKey(sSelectedSymbol);
//            }
            OptionWindow optionsChain = new OptionWindow(sSelectedSymbol, g_oViewSettings, true, OptionStore.CHAIN_TYPE_OPTION);
            InternalFrame oOptionFrame = new InternalFrame(null, Constants.NULL_STRING,
                    g_oWindowDaemon, optionsChain.getTable()) {

                public boolean finalizeWindow() {
                    OptionStore.getSharedInstance().clearSymbolData(sSelectedSymbol, OptionStore.CHAIN_TYPE_OPTION);
                    return super.finalizeWindow();
                }
            };
            oOptionFrame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            optionsChain.setParent(oOptionFrame);
            oOptionFrame.getContentPane().setLayout(new BorderLayout());
            oOptionFrame.getContentPane().add(optionsChain.getNorthPanel(), BorderLayout.NORTH);
            oOptionFrame.getContentPane().add(optionsChain.getTable(), BorderLayout.CENTER);
            oOptionFrame.setLocation(optionsChain.getViewSettings().getLocation());
            oOptionFrame.setSize(optionsChain.getViewSettings().getSize());
            oOptionFrame.setClosable(true);
            oOptionFrame.setMaximizable(true);
            oOptionFrame.setColumnResizeable(true);
            oOptionFrame.setResizable(true);
            oOptionFrame.setIconifiable(true);
            oOptionFrame.updateUI();
            oOptionFrame.applySettings();
            oOptionFrame.setLayer(GUISettings.TOP_LAYER);
            oTopDesktop.add(oOptionFrame);

            if (!fromWorkspace) {
                if (!optionsChain.getViewSettings().isLocationValid()) {
                    GUISettings.setLocationRelativeTo(oOptionFrame, getDesktop());
                }
            }
            oOptionFrame.setVisible(optionsChain.getViewSettings().isVisible());
            if (!fromWorkspace) {
                oOptionFrame.setVisible(true);
            }
            GUISettings.applyOrientation(oOptionFrame);
            // Set the Window Reference
            g_oViewSettings.setWindow(ViewSettingsManager.OPTION_CALLS_VIEW + "|" + sSelectedSymbol, oOptionFrame);
            optionsChain.activate();
            optionsChain = null;
            oOptionFrame = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void mnu_FuturesChain(String sSymbol, String instrumentType, boolean fromWorkspace, boolean removeBaseOnExit) {

        final String sSelectedSymbol;
        try {
            if (sSymbol == null) {
                sSelectedSymbol = getSelectedSymbol();
            } else {
                sSelectedSymbol = sSymbol;
            }

            if ((sSelectedSymbol == null) || (sSelectedSymbol.equals(Constants.NULL_STRING)))
                return;

            if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return;
            }
            if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return;
            }
            // check if the window is already created
            // If it exists, do not create it, just focus it
            InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.FUTURES_CALLS_VIEW + "|" +
                    sSelectedSymbol);
            if (oFrame != null) {
                if (oFrame.isIcon()) {
                    oFrame.setIcon(false);
                }
//                OptionStore.getSharedInstance().sendOptionChainRequest();
                oFrame.show();
                oFrame.setSelected(true);
                return;
            }

            if (OptionWindow.getPopulation() >= Settings.getMaximumOPRACount()) {
                if (!fromWorkspace) {
                    String str = Language.getString("MSG_OPTION_WINDOW_LIMIT_EXCEED");
                    str = str.replaceAll("\\[COUNT\\]", "" + Settings.getMaximumOPRACount());
                    new ShowMessage(str, "E");
                }
                return;
            }
//            if (fromWorkspace){ // since this is not from workspace, thre will not be a symbol validation. Must add base manually
//                sSelectedSymbol = SharedMethods.getKey(sSelectedSymbol);
//            }
            OptionWindow optionsChain = new OptionWindow(sSelectedSymbol, g_oViewSettings, true, OptionStore.CHAIN_TYPE_FUTURES);
            InternalFrame oOptionFrame = new InternalFrame(null, Constants.NULL_STRING, g_oWindowDaemon, optionsChain.getTable()) {

                public boolean finalizeWindow() {
                    OptionStore.getSharedInstance().clearSymbolData(sSelectedSymbol, OptionStore.CHAIN_TYPE_FUTURES);
                    return super.finalizeWindow();
                }
            };
            oOptionFrame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            optionsChain.setParent(oOptionFrame);
            oOptionFrame.getContentPane().setLayout(new BorderLayout());
            oOptionFrame.getContentPane().add(optionsChain.getNorthPanel(), BorderLayout.NORTH);
            oOptionFrame.getContentPane().add(optionsChain.getTable(), BorderLayout.CENTER);
            oOptionFrame.setLocation(optionsChain.getViewSettings().getLocation());
            oOptionFrame.setSize(optionsChain.getViewSettings().getSize());
            oOptionFrame.setClosable(true);
            oOptionFrame.setMaximizable(true);
            oOptionFrame.setResizable(true);
            oOptionFrame.setColumnResizeable(true);
            oOptionFrame.setIconifiable(true);
            oOptionFrame.updateUI();
            oOptionFrame.applySettings();
            oOptionFrame.setLayer(GUISettings.TOP_LAYER);
            oTopDesktop.add(oOptionFrame);

            if (!fromWorkspace) {
                if (!optionsChain.getViewSettings().isLocationValid()) {
                    GUISettings.setLocationRelativeTo(oOptionFrame, getDesktop());
                }
            }
            oOptionFrame.setVisible(optionsChain.getViewSettings().isVisible());
            if (!fromWorkspace) {
                oOptionFrame.setVisible(true);
            }
            GUISettings.applyOrientation(oOptionFrame);
            // Set the Window Reference
            g_oViewSettings.setWindow(ViewSettingsManager.FUTURES_CALLS_VIEW + "|" + sSelectedSymbol, oOptionFrame);
            optionsChain.activate();
            optionsChain = null;
            oOptionFrame = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void getStockReports(String symbol) {
//        if (isValidInfoType(symbol,Meta.IT_Stock_Reports,false)) {
        if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(symbol))) {
            SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(symbol));
            return;
        }
        if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(symbol))) {
            SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(symbol));
            return;
        }
        if (symbol != null) {
            showEmptyStockreportWindow();
            Client.getInstance().sendStockReportRequest(
                    SharedMethods.getSymbolFromKey(symbol), SharedMethods.getExchangeFromKey(symbol));
        }
//        }
    }

    public void showRegionalQuotes(String symbol) {
        if (symbol == null)
            symbol = getSelectedSymbol();

//		Stock stock = DataStore.getSharedInstance().getStockObject(symbol);

//		if (stock.getInstrumentType() == Meta.OPTIONS){
//			mnu_RegionalOpraQuote(symbol,null,false);
//		}else{
        mnu_RegionalQuoteSymbol(symbol, false);
//		}
    }

    /*public void mnu_RegionalOpraQuote(String sSymbol,String caption, boolean fromWorkspace) {
		//String sTitle = null;
		try {
			//System.out.println("sSymbol " + sSymbol);
			String sSelectedSymbol = sSymbol;

			// check if the window is already created
			// If it exists, do not create it, just focus it
			InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.REGIONAL_OPRA_QUOTE_VIEW + "|" +
				sSelectedSymbol);
			if (oFrame != null) {
				oFrame.show();
				return;
			}

			if (caption == null){
				try{
					caption = DataStore.getSharedInstance().getStockObject(sSelectedSymbol).getShortDescription();
				}catch(Exception e){
					caption = sSelectedSymbol;
				}
			}

			RegionalOpraQuoteWindow rQuoteWindow  = new RegionalOpraQuoteWindow(sSelectedSymbol, g_oViewSettings);
			//InternalFrame oRQuoteFrame = new InternalFrame(sSelectedSymbol, g_oWindowDaemon, ESPConstants.RegionalQuoteWindow);
			InternalFrame oRQuoteFrame = new InternalFrame(g_oWindowDaemon);
			//oRQuoteFrame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
			rQuoteWindow.setParent(oRQuoteFrame,caption);
			oRQuoteFrame.getContentPane().add(rQuoteWindow);
			rQuoteWindow.setSymbol(sSelectedSymbol);
			oTopDesktop.add(oRQuoteFrame);
			oRQuoteFrame.setWindowType(ViewSettingsManager.REGIONAL_OPRA_QUOTE_VIEW);
//			oRQuoteFrame.setMenuItem(g_oMenus);
			oRQuoteFrame.setResizable(true);
			oRQuoteFrame.setClosable(true);
			oRQuoteFrame.setMaximizable(true);
			oRQuoteFrame.setIconifiable(true);
			oRQuoteFrame.updateUI();
			oRQuoteFrame.applySettings();
			oRQuoteFrame.setLayer(GUISettings.TOP_LAYER+1);
			oRQuoteFrame.show();
            if (!fromWorkspace){
                GUISettings.setLocationRelativeTo(oRQuoteFrame,getDesktop());
            }

			GUISettings.applyOrientation(oRQuoteFrame);
			// Set the Window Reference
//			g_oViewSettings.setWindow(ViewSettingsManager.REGIONAL_OPRA_QUOTE_VIEW + "|" +
//				sSelectedSymbol, oRQuoteFrame);
			oRQuoteFrame = null;

		} catch(Exception e) {
			e.printStackTrace();
		}
    }*/

    public void mnu_RegionalQuoteSymbol(String sSymbol, boolean fromWorkspace) {
        //String sTitle = null;
        try {
            //System.out.println("sSymbol " + sSymbol);
            final String sSelectedSymbol;
            String caption;
            if (sSymbol == null)
                sSelectedSymbol = getSelectedSymbol();
            else
                sSelectedSymbol = sSymbol;

            if ((sSelectedSymbol == null) || (sSelectedSymbol.equals("")))
                return;

            if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeExpiryMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return;
            }
            if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(sSelectedSymbol))) {
                SharedMethods.showExchangeInactiveMesage(SharedMethods.getExchangeFromKey(sSelectedSymbol));
                return;
            }
            try {
                caption = DataStore.getSharedInstance().getStockObject(sSelectedSymbol).getShortDescription();
            } catch (Exception e) {
                caption = sSelectedSymbol;
            }
            // check if the window is already created
            // If it exists, do not create it, just focus it


            InternalFrame oFrame = g_oViewSettings.getWindow(ViewSettingsManager.REGIONAL_QUOTE_VIEW + "|" +
                    sSelectedSymbol);
            if (oFrame != null) {
                if (oFrame.isIcon()) {
                    oFrame.setIcon(false);
                }
                oFrame.show();
                oFrame.setSelected(true);
                return;
            }

            if (RQuoteWindow.getPopulation() >= Settings.getMaximumRQuoteCount()) {
                if (!fromWorkspace)
                    new ShowMessage(Language.getString("MSG_RQUOTE_WINDOW_LIMIT_EXCEED") + " COUNT" + Settings.getMaximumRQuoteCount(), "E");
                return;
            }

            RQuoteWindow rQuoteWindow = new RQuoteWindow(sSelectedSymbol, g_oViewSettings);
            //InternalFrame oRQuoteFrame = new InternalFrame(sSelectedSymbol, g_oWindowDaemon, ESPConstants.RegionalQuoteWindow);
            InternalFrame oRQuoteFrame = new InternalFrame(g_oWindowDaemon) {

                public boolean finalizeWindow() {
                    DataStore.getSharedInstance().removeSymbolRequest(SharedMethods.getExchangeFromKey(sSelectedSymbol),
                            SharedMethods.getSymbolFromKey(sSelectedSymbol), (int) Meta.MESSAGE_TYPE_REGIONAL_QUOTE);
                    return super.finalizeWindow();
                }
            };
            //oRQuoteFrame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            rQuoteWindow.setParent(oRQuoteFrame, caption);
            oRQuoteFrame.getContentPane().add(rQuoteWindow);
            rQuoteWindow.setSymbol(sSelectedSymbol);
            oTopDesktop.add(oRQuoteFrame);
            oRQuoteFrame.setWindowType(ViewSettingsManager.REGIONAL_QUOTE_VIEW);
//            oRQuoteFrame.setMenuItem(g_oMenus);
            oRQuoteFrame.setTable(rQuoteWindow.getTable());
            oRQuoteFrame.setResizable(true);
            oRQuoteFrame.setClosable(true);
            oRQuoteFrame.setMaximizable(true);
            oRQuoteFrame.setIconifiable(true);
            oRQuoteFrame.setColumnResizeable(true);
            oRQuoteFrame.updateUI();
            oRQuoteFrame.applySettings();
            oRQuoteFrame.setLayer(GUISettings.TOP_LAYER);
            oRQuoteFrame.getTableComponent().getPopup().showSetDefaultMenu();
            oRQuoteFrame.show();
            if (!fromWorkspace) {
                if (!rQuoteWindow.getViewSettings().isLocationValid()) {
                    GUISettings.setLocationRelativeTo(oRQuoteFrame, getDesktop());
                    rQuoteWindow.getViewSettings().setLocation(oRQuoteFrame.getLocation());
                } else {
                    oRQuoteFrame.setLocation(rQuoteWindow.getViewSettings().getLocation());
                }
            }

            GUISettings.applyOrientation(oRQuoteFrame);
            // Set the Window Reference
            g_oViewSettings.setWindow(ViewSettingsManager.REGIONAL_QUOTE_VIEW + "|" +
                    sSelectedSymbol, oRQuoteFrame);
            oRQuoteFrame = null;

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showCASearchPanel() {
        /*caSearchPanel = new CASearchPanel(getFrame());
        caSearchPanel.show();*/
    }

    public void createOptionBuilder() {
        optBuilder = new OptionSymbolBuilder(this.getFrame(), Language.getString("OPT_WINDOW_TITLE"), true);
    }

    public OptionSymbolBuilder getOptionSymbolBuilder() {
        return optBuilder;
    }

    public void mnu_FindOptionSymbol(boolean isInvokedFromMainMenu) {
        if (optBuilder == null) {
            createOptionBuilder();
        }
        optBuilder.setInvokeMethod(isInvokedFromMainMenu);
        optBuilder.populateWindowList();

//        if (isInvokedFromMainMenu){
        String symbol = this.getSelectedSymbol();
        if (symbol != null) {
            optBuilder.setBaseSymbol(symbol);
        } else {
            optBuilder.setBaseSymbol(Constants.NULL_STRING);
        }
        optBuilder.showSymbolBuilder();
    }

    public void addSymbolToWatchList(String symbol, String command, int istrumentType) {
        Object[] windowList = ((TWDesktop) Client.getInstance().getDesktop()).getWindows();
        if (windowList == null)
            return;
        ViewSetting oSettings = null;

        for (int i = 0; i < windowList.length; i++) {
            try {
                JInternalFrame frame = (JInternalFrame) windowList[i];
                if (!(frame instanceof ClientTable)) {
                    continue;
                }
                oSettings = ((ClientTable) frame).getViewSettings();
                if (oSettings.isCutomType()) {
                    if (oSettings.getID().equals(command)) {
//                        String requestID = "MainBoard:" + System.currentTimeMillis();
                        String requestID = DataStore.getSharedInstance().getSequenceID();
                        SendQFactory.addValidateRequest(SharedMethods.getSymbolFromKey(symbol), requestID, SharedMethods.getExchangeFromKey(symbol), SharedMethods.getInstrumentTypeFromKey(symbol));
                        SymbolsRegistry.getSharedInstance().rememberRequest(requestID, frame);
//                            ((ClientTable)frame).getSymbols().appendSymbol(symbol);
//                            oSettings.getSymbols().appendSymbol(symbol);
//                            ((ClientTable)frame).getSymbols().reFilter();
//                            TradeFeeder.refreshSymbolIndex();
//                            ((ClientTable)frame).updateUI();
                        System.out.println("symbol added to watchlist");
                        break;
                    }
                }
                frame = null;

            } catch (Exception ex) {
            }
        }
        windowList = null;
    }

    public void enableCustomViewSettings() {
        mnuSaveSettings.setVisible(true);
        mnuSaveSettings.setEnabled(true);
    }

    // init mouse guerstures
    /*MouseGestures mouseGestures;
    private void initMouseGestures() {
        mouseGestures = new MouseGestures();
        mouseGestures.setGridSize(10);
        mouseGestures.setMouseButton(MouseEvent.BUTTON3_MASK);
        mouseGestures.addMouseGesturesListener(new MouseGesturesListener() {
            public void gestureMovementRecognized(String currentGesture) {
//                setGestureString(currentGesture);
            }

            public void processGesture(String gesture) {
                try {
                    Thread.sleep(200);
                    setGestureString(gesture);
                } catch (InterruptedException e) {}
                setGestureString("");
            }
        });
        mouseGestures.start();
    }*/

    public void setGestureString(String gesture) {
        try {
            String key = getSelectedSymbol();
            if (key != null) {
                TWActions.getInstance().performMouseGestures(gesture);
                /*if (gesture.equals("U")){
                   showCashFlowDetailQuote(key,false);
               } else*/
                /*if (gesture.equals("URDRU")){
                  depthSpecialByOrder(key, false);
              } else if (gesture.equals("URDLU")){
                  depthSpecialByOrder(key, false);
              } else if (gesture.equals("ULDRU")){
                  depthByOrder(key, false);
              } else */
                /* if (gesture.equals("R")) {
                    showChart(key, null, null, true, false); //not default template ,mode must be force fully set |intraday
                } else if (gesture.equals("L")) {
                    showChart(key, null, null, false, false); ////not default template ,mode must be force fully set |history
                } else if (gesture.equals("U")) {
                    if (isValidInfoType(getSelectedSymbol(), Meta.IT_SymbolTimeAndSales, true)) {
                        mnu_TimeNSalesSymbol(key, Constants.MAINBOARD_TYPE, false, false, LinkStore.LINK_NONE);
                    }
                } else if (gesture.equals("D")) {
                    if (isValidInfoType(getSelectedSymbol(), Meta.IT_CashFlow, true)) {
                        showCashFlowDetailQuote(key, false, false, false, LinkStore.LINK_NONE);
                    }
                } else if (gesture.equals("UR")) {
                    if (isValidInfoType(getSelectedSymbol(), Meta.IT_DepthCalculator, true)) {
                        mnu_MDepth_Calculator(null, TWTypes.TradeSides.BUY, false, false, LinkStore.LINK_NONE);
                    }
                } else if (gesture.equals("UL")) {
                    if (isValidInfoType(getSelectedSymbol(), Meta.IT_SpecialOrderBook, true)) {
                        depthSpecialByOrder(key, false, false, LinkStore.LINK_NONE);
                    }
                } else if (gesture.equals("DR")) {
                } else if (gesture.equals("DL")) {
                }*/
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public ArrayList<JMenuItem> getVasMenuItemList() {
        if (vasMenuItems == null) {
            vasMenuItems = new ArrayList<JMenuItem>();
        }
        return vasMenuItems;
    }

    public boolean isVolumeWatcherSelected() {
        return volumeWatcherSelected;
    }

    public void putAllToSameLayerActionPerformed() {
        oTopDesktop.fireLayerChanged();
    }

    public void updateHivePopup(String symbolDescription, String symbolKey) {
        activeHiveSymbol = symbolKey;
        descriptLabelHive.setText(symbolDescription);
        descriptLabelHive.repaint();
        mnuTradeHive.updateUI();
        mnuSellHive.updateUI();
        mnuBuyHive.updateUI();
        mnuGraphHive.updateUI();
        mnuTimeNSalesHive.updateUI();
        mnuCashFQHive.updateUI();
        mnuDepthByPriceHive.updateUI();
        mnuAlertHive.updateUI();
    }

    public AutoTradeLoginDaemon getAutoTradeLoginDaemon() {
        return autoTradeLoginDaemon;
    }

    public TWMenu getMnuMWTrade() {
        return mnuMWTrade;
    }

    public void mwTradeMenuCreated() {
        mnuTrade.setVisible(false);
        mnuMWTrade.setVisible(true);
    }

    public void mwServerDisconnected() {
        //To change body of implemented methods use File | Settings | File Templates.

    }

    public void updateMarketTNSTickMode(String exchange, int selectedMode) {
        //((TWMenu) mainBoardLoader.getTimeNSalesFrame("DFM").getTableComponent().getPopup().getComponent(4)).getPopupMenu().getComponent(0)
        ((TWRadioButtonMenuItem) ((TWMenu) mainBoardLoader.getTimeNSalesFrame(exchange).getTableComponent().getPopup().getComponent(4)).getPopupMenu().getComponent(selectedMode)).setSelected(true);
//       mainBoardLoader.getTimeNSalesFrame(exchange).getTableComponent().getPopup();
    }

    public boolean isBlotterVisible() {
        if (blotterFrame.isVisible())
            return true;
        return false;
    }


}
