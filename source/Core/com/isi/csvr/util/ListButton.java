package com.isi.csvr.util;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.lang.reflect.Method;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class ListButton extends TWButton implements ActionListener {

    public static final byte TYPE_TEXT_TEXT = 0;
    public static final byte TYPE_IMAGE_IMAGE = 1;
    public static final byte TYPE_IMAGE_TEXT = 2;
    //public static final byte TYPE_DESCRIPTION = 3;
    final int FIXED_WIDTH = 14;
    final int LGAP = 5;
    final int VGAP = 3;
    final int VADJ = 12;

    private byte type;
    private Color borderColor;
    private Color activeBorderColor;
    private boolean showBorder;
    private Object[] items;
    private String toolTip;
    private Object selectedItem;
    private int selectedIndex;
    private JPopupMenu popup;
    private boolean duelMode;//toggle and popup
    private boolean isLtoR;
    private Font font;
    Method method;
    private ListButtonActionListener parent;
    private ListButton self;
    private String text=null;

    public ListButton(byte type, boolean isLtoR) {
        this(type, null, isLtoR);
    }

    public ListButton(byte type, ListButtonActionListener parent, boolean isLtoR) {
        EtchedBorder eb = (EtchedBorder) BorderFactory.createEtchedBorder();
        borderColor = Color.DARK_GRAY;
        self = this;
        eb = null;
        showBorder = true;
        items = null;
        toolTip = "";
        this.type = type;
        this.method = method;
        this.parent = parent;
        selectedIndex = -1;
        selectedItem = null;
        popup = new JPopupMenu();
        duelMode = true;
        this.isLtoR = isLtoR;
        this.addActionListener(this);
        font = Theme.getDefaultFont(Font.BOLD, 12);
    }

    public void addListButtonActionListener(ListButtonActionListener parent){
        this.parent = parent;
    }


    public void actionPerformed(ActionEvent e) {
        int popW = popup.getPreferredSize().width;
        popup.show(this, this.getWidth() - popW, this.getHeight());
    }

    public void updateUI() {
        super.updateUI();
        try {
            SwingUtilities.updateComponentTreeUI(popup);
        } catch (Exception e) {
        }
    }


    public void setText(String text) {
        this.text = text;
    }

    public void paint(Graphics g) {
        //super.paint(g);
        g.setColor(getBackground());
        g.fillRect(1, 1, getWidth() - 2, getHeight() - 2);
        if (showBorder) {
            if (isEnabled()) {
                g.setColor(borderColor);
            } else {
                g.setColor(Color.LIGHT_GRAY); //should be Theme.getColor("DISABLED_BORDER_COLOR");
            }
            g.drawRect(0, 0, getWidth() - 1, getHeight() - 1);
        }
        if((text != null)){
            Rectangle r = getTextRect();
            g.setClip(r);
            g.setFont(font);
            g.setColor(getForeground());
            ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
            g.drawString(text, r.x + LGAP, r.y + VADJ + VGAP);
        }else{
        if (selectedIndex >= 0) {
            drawText(g);
        }
        }
        drawArrow(g);
    }

    private void drawText(Graphics g) {
        if ((selectedItem != null) && (selectedItem instanceof ListButtonItem)) {
            Rectangle r = getTextRect();
            ListButtonItem lbi = (ListButtonItem) selectedItem;
            g.setClip(r);
            switch (type) {
                case TYPE_TEXT_TEXT:
                default:
                    g.setFont(font);
                    g.setColor(getForeground());
                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
                    g.drawString(lbi.getDescription(), r.x + LGAP, r.y + VADJ + VGAP);
                    break;
                case TYPE_IMAGE_TEXT:
                case TYPE_IMAGE_IMAGE:
                    Image img = lbi.getImageIcon().getImage();
                    int tmpY = (r.height - img.getHeight(this)) / 2;
                    g.drawImage(img, r.x+ LGAP, r.y + tmpY, this);
                    break;
//				case TYPE_IMAGE_TEXT:
//					g.drawImage(lbi.getImage(), r.x+LGAP, r.y, this);
//					g.setColor(getForeground());
//					g.drawString(lbi.getDescription(), lbi.getImage().getWidth(this)+r.x+2*LGAP, r.y+VADJ);
//					break;
//				case TYPE_DESCRIPTION:
//					g.setColor(getForeground());
//					g.drawString(lbi.getDescription(), r.x+LGAP, r.y+VADJ);
//					break;
            }
        }
    }

    public int getprefferedWidth() {
        return 0;
    }

    private void drawArrow(Graphics g) {
        int halfW = 3;
        Rectangle fxR = getFixedRect();
        g.setClip(fxR);
        if (isEnabled()) {
            g.setColor(getForeground());
        } else {
            g.setColor(Color.LIGHT_GRAY); //should be Theme.getColor("DISABLED_BORDER_COLOR");
        }
        int x = fxR.x + fxR.width / 2;
        int y = fxR.y + fxR.height / 2;
        int[] xP = {x - halfW, x + halfW, x};
        int[] yP = {y - halfW, y - halfW, y + halfW};
        g.fillPolygon(xP, yP, 3);
        if (showBorder) {
            if (isEnabled()) {
                g.setColor(borderColor);
            } else {
                g.setColor(Color.LIGHT_GRAY); //should be Theme.getColor("DISABLED_BORDER_COLOR");
            }
            g.drawLine(fxR.x, 0, fxR.x, getHeight());
        }
    }

    private Rectangle getTextRect() {
        if (isLtoR) {
            return new Rectangle(0, 0, getWidth() - FIXED_WIDTH, getHeight());
        } else {
            return new Rectangle(FIXED_WIDTH, 0, getWidth() - FIXED_WIDTH, getHeight());
        }
    }

    private Rectangle getFixedRect() {
        if (isLtoR) {
            return new Rectangle(getWidth() - FIXED_WIDTH, 0, FIXED_WIDTH, getHeight());
        } else {
            return new Rectangle(0, 0, FIXED_WIDTH, getHeight());
        }
    }

    //activeBorderColor
    public Color getActiveBorderColor() {
        return activeBorderColor;
    }

    public void setActiveBorderColor(Color activeBorderColor) {
        this.activeBorderColor = activeBorderColor;
    }

    //borderColor
    public Color getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    //showBorder
    public boolean isShowBorder() {
        return showBorder;
    }

    public void setShowBorder(boolean showBorder) {
        this.showBorder = showBorder;
    }

    public Object[] getItems() {
        return items;
    }

    public byte getType() {
        return type;
    }

    //toolTip
    public String getToolTip() {
        return toolTip;
    }

    public void setToolTip(String toolTip) {
        this.toolTip = toolTip;
    }

    public boolean isDuelMode() {
        return duelMode;
    }

    public void setDuelMode(boolean duelMode) {
        this.duelMode = duelMode;
    }

    public JPopupMenu getPopup() {
        return popup;
    }

    public int getSelectedIndex() {
        return selectedIndex;
    }

    public void setSelectedIndex(int selectedIndex) {
        selectedItem = null;
        this.selectedIndex = -1;
        if ((selectedIndex >= 0) && (items != null) && (selectedIndex < items.length)) {
            this.selectedIndex = selectedIndex;
            selectedItem = items[selectedIndex];
        }
    }

    public Object getSelectedItem() {
        return selectedItem;
    }

    public void setSelectedItem(String str) {
        selectedItem = null;
        selectedIndex = -1;
        if (items != null) {
            for (int i = 0; i < items.length; i++) {
                ListButtonItem lbi = (ListButtonItem) items[i];
                if (str.equals(lbi.getDescription())) {
                    selectedItem = lbi;
                    selectedIndex = i;
                    break;
                }
            }
        }
    }

    public void setSelectedItem(Object obj) {
        selectedItem = null;
        selectedIndex = -1;
        if (items != null) {
            for (int i = 0; i < items.length; i++) {
                if (obj == items[i]) {
                    selectedItem = obj;
                    selectedIndex = i;
                    break;
                }
            }
        }
    }

    public void setItems(Object[] items) {
        this.items = items;
        refreshPopupMenu();
    }

    public void removeAll() {
       popup.removeAll();
    }

    public void refreshPopupMenu() {
        popup.removeAll();
        TWMenuItem mi;

        if (items != null) {
            for (int i = 0; i < items.length; i++) {
                final ListButtonItem lbi = (ListButtonItem) items[i];
                final int index = i;
                switch (type) {
                    case TYPE_TEXT_TEXT:
                    case TYPE_IMAGE_TEXT:
                    default:
                        mi = new TWMenuItem(lbi.getDescription());
                        break;
                    case TYPE_IMAGE_IMAGE:
                        mi = new TWMenuItem(lbi.getImageIcon());
                        mi.setHorizontalAlignment(SwingConstants.CENTER);
                        break;
                }
                mi.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        text = null;
                        selectedIndex = index;
                        selectedItem = lbi;
                        try {
                            parent.listButtonClicked(self);
                        } catch (Exception excp) {
                            excp.printStackTrace();
                        }
                    }
                });
                popup.add(mi);
                mi.setIconTextGap(0);
            }
        }
        GUISettings.applyOrientation(popup);
    }

    public Dimension getPreferredSize() {
        FontMetrics fm = this.getFontMetrics(font);
        int w = FIXED_WIDTH + 10, fw = FIXED_WIDTH + 10;
        ListButtonItem lbi;
        if (items != null) {
            for (int i = 0; i < items.length; i++) {
                lbi = (ListButtonItem) items[i];
                switch (type) {
                    case TYPE_TEXT_TEXT:
                        if (lbi.getDescription() != null){
                            w = Math.max(w, fw + fm.stringWidth(lbi.getDescription()));
                        }
                        break;
                    case TYPE_IMAGE_IMAGE:
                    case TYPE_IMAGE_TEXT:
                    default:
                        w = Math.max(w, fw + lbi.getImageIcon().getImage().getWidth(this));
                        break;
                }
            }
        }
        return new Dimension(w, 22);
    }
}