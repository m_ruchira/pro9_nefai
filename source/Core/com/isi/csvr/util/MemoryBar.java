package com.isi.csvr.util;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 27, 2003
 * Time: 8:47:28 AM
 * To change this template use Options | File Templates.
 */
public class MemoryBar extends JComponent {

    long maxMemory;
    long totMemory;
    long freMemory;

    public void update(long maxMemory, long totMemory, long freMemory) {
        this.maxMemory = maxMemory;
        this.totMemory = totMemory;
        this.freMemory = freMemory;
        repaint();
    }

    public void paintComponent(Graphics g) {

        int width = getWidth();
        int tot = (int) (((double) width) * ((double) totMemory / (double) maxMemory));
        int fre = (int) (((double) width) * ((double) freMemory / (double) maxMemory));

        g.setColor(Color.BLUE);
        g.fillRect(0, 0, getWidth(), getHeight());
        g.setColor(Color.RED);
        g.fillRect(0, 0, tot, getHeight());
        g.setColor(Color.GREEN);
        g.fillRect(tot - fre, 0, fre, getHeight());
    }

}
