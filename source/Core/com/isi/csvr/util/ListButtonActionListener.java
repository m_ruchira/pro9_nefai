package com.isi.csvr.util;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 14, 2007
 * Time: 11:39:12 AM
 */
public interface ListButtonActionListener {
    public void listButtonClicked(Object source);
}
