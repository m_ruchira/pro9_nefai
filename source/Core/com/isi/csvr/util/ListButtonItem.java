package com.isi.csvr.util;

import javax.swing.*;
import java.awt.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class ListButtonItem implements Comparable {

    private String id;
    private String description;
    private float value;
    private ImageIcon imageIcon;
    private Color upColor;
    private Color downColor;

    public ListButtonItem(String id, ImageIcon image, String description, float value) {
        this.id = id;
        this.imageIcon = image;
        this.description = description;
        this.value = value;
        this.upColor = null;
        this.downColor = null;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public ImageIcon getImageIcon() {
        return imageIcon;
    }

    public void setImageIcon(ImageIcon image) {
        this.imageIcon = image;
    }

    public String getID() {
        return id;
    }

    public void setID(String id) {
        this.id = id;
    }

    public float getValue() {
        return value;
    }

    public void setValue(float value) {
        this.value = value;
    }

    public void setUpColor(Color upColor) {
        this.upColor = upColor;
    }

    public Color getUpColor() {
        return upColor;
    }

    public void setDownColor(Color downColor) {
        this.downColor = downColor;
    }

    public Color getDownColor() {
        return downColor;
    }

    public int compareTo(Object other) {
        return getDescription().compareToIgnoreCase(((ListButtonItem)other).getDescription());
    }
}