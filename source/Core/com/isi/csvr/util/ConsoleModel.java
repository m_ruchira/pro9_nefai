package com.isi.csvr.util;

import com.isi.csvr.shared.DynamicArray;

import javax.swing.*;
import javax.swing.event.ListDataListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 8, 2003
 * Time: 2:46:03 PM
 * To change this template use Options | File Templates.
 */
class ConsoleModel implements ListModel {
    private DynamicArray store;

    ConsoleModel(DynamicArray store) {
        this.store = store;
    }

    // list modal
    public int getSize() {
        System.err.println(store.size());
        return store.size();
    }


    public Object getElementAt(int index) {
        System.err.println(index + "--" + store.get(index));
        return store.get(index);
    }


    public void addListDataListener(ListDataListener l) {

    }


    public void removeListDataListener(ListDataListener l) {

    }
}
