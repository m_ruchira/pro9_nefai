package com.isi.csvr.keyfunction;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: May 26, 2008
 * Time: 1:33:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class KeySelectionPanel extends JFrame implements KeyListener {
    JPanel keyPanel;
    TWButton cancelButton;
    TWButton okButton;
    JLabel pressedKey;
    static TWTextField textField;
    JPanel leftPanel;
    JPanel rightPanel;
    JCheckBox shiftBox;
    JCheckBox ctrlBox;
    JCheckBox altBox;
    String selected = "";
    boolean isShift;
    boolean isAlt;
    boolean isCtrl;
    char[] charArray = new char[]{'a','b','c','d','e','f','g','h','i','j','k','l','m','n','o','p','q','r','s','t','u','v','w','x','y','z','/','?','+','=',';',':','<',',','|','{','[','}',']','\\'};

   private MainComboBox box;
    private KeyFunctionPanel panel;
    String[] array;
    JComboBox choice;
    private int defaultCloseOperation = HIDE_ON_CLOSE;
    static boolean isSaved = false;
    static String important;
    static boolean isCanceled;

    public KeySelectionPanel(MainComboBox selectedCombo){
          box = selectedCombo;
        important = (String) box.getItemAt(0);
//        System.out.println("000000000000000000000"+important);
    }

    public KeySelectionPanel(String[] comboText) {
        array = comboText;
    }

    public void createUI(){

       this.setTitle(Language.getString("KEY_COMBINATION_SELECTION"));
       this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.requestFocusInWindow();
        this.setLocation(900,500);
                this.setSize(350, 130);
                this.setResizable(false);
                this.setFocusable(true);
               // this.addWindowFocusListener((WindowFocusListener)this);
                this.setVisible(true);

        this.addWindowListener(new WindowAdapter(){//<-----------
         public void windowClosing(WindowEvent we){
          windowsClosingMethod();

          if(!KeyFunctionPanel.isValid){
              windowsClosingSavingMethod();
          }

             if(isCanceled){
              windowsClosingCanceledMethod();
             }
          
      }});


        keyPanel = new JPanel(new FlexGridLayout(new String[]{"35%","65%"},new String[]{"100%"},20,5));
        leftPanel = new JPanel(new FlexGridLayout(new String[]{"100%"},new String[]{"20","20","20"},20,5));
        rightPanel = new JPanel(new FlexGridLayout(new String[]{"50%","50%"},new String[]{"20","20"},5,5));

        final JLabel first =new JLabel("select key");
        pressedKey = new JLabel("Type the key");

        textField = new TWTextField(20);
        textField.addKeyListener(this);

        shiftBox = new JCheckBox(Language.getString("SHIFT"));
        altBox = new JCheckBox(Language.getString("ALT"));
        ctrlBox = new JCheckBox(Language.getString("CTRL"));

         altBox.addItemListener(new ItemListener(){
             public void itemStateChanged(ItemEvent e) {
        Object source = e.getItemSelectable();
        if(source == altBox){
            isAlt = true;
        }
         if (e.getStateChange() == ItemEvent.DESELECTED) {
            //c = '-';
           isAlt = false;

        }

         }});



         ctrlBox.addItemListener(new ItemListener(){
             public void itemStateChanged(ItemEvent e) {
        Object source = e.getItemSelectable();
        if(source == ctrlBox){
            isCtrl = true;
        }
         if (e.getStateChange() == ItemEvent.DESELECTED) {
             isCtrl = false;

        }

         }});


         shiftBox.addItemListener(new ItemListener(){
             public void itemStateChanged(ItemEvent e) {
        Object source = e.getItemSelectable();
        if(source == shiftBox){
            isShift = true;
        }
         if (e.getStateChange() == ItemEvent.DESELECTED) {
            isShift = false;
        }

         }});


        cancelButton = new TWButton(Language.getString("CANCEL"));
        okButton = new TWButton(Language.getString("SAVE"));
         choice = new JComboBox();
        choice.addItem("<None>");
        choice.addItem(Language.getString("ALT"));
        choice.addItem(Language.getString("CTRL"));
        choice.addItem(Language.getString("SHIFT"));

        leftPanel.add(altBox);
        leftPanel.add(ctrlBox);

        leftPanel.add(shiftBox);
        rightPanel.add(pressedKey);
        rightPanel.add(textField);
        rightPanel.add(cancelButton);

        rightPanel.add(okButton);

        keyPanel.add(leftPanel);
        keyPanel.add(rightPanel);
        cancelButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {
                isCanceled = true;
                 altBox.setSelected(false);
                 ctrlBox.setSelected(false);
                 shiftBox.setSelected(false);
                textField.setText("");
            }
        });

        okButton.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e) {

               isSaved = true;
               String retunString =completingText();
               String textFromArea ="";
                 textFromArea = textField.getText();
               System.out.println("textfieldooo"+textFromArea);
                 panel = new KeyFunctionPanel(retunString+"+"+textFromArea);
               String nullString = "";
               if((textFromArea.equals(nullString)) && (retunString.equals(nullString))) {
                panel.settingChoice("<None>", box );
               }

               else if(!(textFromArea.equals(nullString)) && (retunString.equals(nullString))){
                   panel.settingChoice(textFromArea, box );

               }

               else if((textFromArea.equals(nullString)) && !(retunString.equals(nullString))){
                   panel.settingChoice(retunString, box );

               }
               else if(!(textFromArea.equals(nullString)) && !(retunString.equals(nullString))){
                   panel.settingChoice(retunString+"+"+textFromArea, box );

               }

            }

        });

         this.add(keyPanel);

     isSaved = false;
    }

    public void keyTyped(KeyEvent e) {
         textField.setText("");
        int keyCode = e.getKeyCode();

        int numpandwn = 95;
        int numpanup = 112;

        if((keyCode>numpandwn) && (keyCode<numpanup)){
            textField.setText("");
        }

        String comparingString = String.valueOf(e.getKeyChar());
        System.out.println("))))))))))))))))))"+comparingString);
        // f8 = 'F8'; 

        if(keyCode==59)
        textField.setText("");
        if(keyCode==61)
        textField.setText("");
        if(keyCode==45)
        textField.setText("");
        if(keyCode==47)
        textField.setText("");
        if(keyCode==96)
        textField.setText("");
        if(keyCode==91)
        textField.setText("");
        if(keyCode==92)
        textField.setText("");
        if(keyCode==93)
        textField.setText("");
        if(keyCode==39)
        textField.setText("");
        if(keyCode==44)
        textField.setText("");
        if(keyCode==46)
        textField.setText("");
        if(keyCode==47)




        if(keyCode==37)
        textField.setText("");
        if(keyCode==38)
        textField.setText("");
        if(keyCode==39)
        textField.setText("");
        if(keyCode==40)
        textField.setText("");
        if(keyCode==45)
        textField.setText("");
        if(keyCode==46)
        textField.setText("");
        if(keyCode==144)
        textField.setText("");
        if(keyCode==145)
        textField.setText("");
        if(keyCode==19)
        textField.setText("");
        if(keyCode==186)
        textField.setText("");
        if(keyCode==187)
        textField.setText("");
        if(keyCode==189)
        textField.setText("");
        if(keyCode==191)
        textField.setText("");
        if(keyCode==192)
        textField.setText("");
        if(keyCode==219)
        textField.setText("");
        if(keyCode==220)
        textField.setText("");
        if(keyCode==222)
        textField.setText("");
        if(keyCode==188)
        textField.setText("");
        if(keyCode==190)
        textField.setText("");
        if(keyCode==191)
        textField.setText("");

        char comparingChar = e.getKeyChar();

        for(int r=0;r<charArray.length;r++){
            if(comparingChar == charArray[r]){
                textField.setText("");
            }
        }
//        char myChar = '';
     //   System.out.println("kkkkkkkkkkkkkkkey"+keyCode);
        int numdwn = 47;
        int numup = 58;
        if((keyCode>numdwn) && (keyCode<numup)){
   //         System.out.println("im in------");
            textField.setText("");
        }

        if(keyCode==112)
               textField.setText("F1");
               if(keyCode==113)
               textField.setText("F2");
               if(keyCode==114)
               textField.setText("F3");
               if(keyCode==115)
               textField.setText("F4");
               if(keyCode==116)
               textField.setText("F5");
               if(keyCode==117)
               textField.setText("F6");
               if(keyCode==118)
               textField.setText("F7");
               if(keyCode==119)
               textField.setText("F8");
               if(keyCode==120)
               textField.setText("F9");
               if(keyCode==121)
               textField.setText("F10");
               if(keyCode==122)
               textField.setText("F11");
               if(keyCode==123)
               textField.setText("F12");


         if(keyCode==59)
        textField.setText("");
        if(keyCode==61)
        textField.setText("");
        if(keyCode==45)
        textField.setText("");
        if(keyCode==47)
        textField.setText("");
        if(keyCode==96)
        textField.setText("");
        if(keyCode==91)
        textField.setText("");
        if(keyCode==92)
        textField.setText("");
        if(keyCode==93)
        textField.setText("");
        if(keyCode==39)
        textField.setText("");
        if(keyCode==44)
        textField.setText("");
        if(keyCode==46)
        textField.setText("");
        if(keyCode==47)
            //   65 -90 , 112 -123 ,
               if(keyCode==8)
               textField.setText(Language.getString("BCKSPACE"));

             if(keyCode==27)
              textField.setText(Language.getString("ESC"));
                if(keyCode==32)
               textField.setText(Language.getString("SPACE"));
                if(keyCode==33)
               textField.setText(Language.getString("PAGE_UP"));
                if(keyCode==34)
               textField.setText(Language.getString("PAGE_DOWN"));
                if(keyCode==35)
               textField.setText(Language.getString("END"));
                 if(keyCode==36)
               textField.setText(Language.getString("HOME"));

        
    }

    public void keyPressed(KeyEvent e) {
         textField.setText("");
         char comparingChar = e.getKeyChar();
        String comparingString = String.valueOf(e.getKeyChar());

        for(int r=0;r<charArray.length;r++){
            if(comparingChar == charArray[r]){
                textField.setText("");
            }
        }


        int keyCode = e.getKeyCode();

         int numpandwn = 95;
        int numpanup = 112;

        if((keyCode>numpandwn) && (keyCode<numpanup)){
            textField.setText("");
        }
        if(keyCode==59)
        textField.setText("");
        if(keyCode==61)
        textField.setText("");
        if(keyCode==45)
        textField.setText("");
        if(keyCode==47)
        textField.setText("");
        if(keyCode==96)
        textField.setText("");
        if(keyCode==91)
        textField.setText("");
        if(keyCode==92)
        textField.setText("");
        if(keyCode==93)
        textField.setText("");
        if(keyCode==39)
        textField.setText("");
        if(keyCode==44)
        textField.setText("");
        if(keyCode==46)
        textField.setText("");
        if(keyCode==47)

        if(keyCode==37)
        textField.setText("");
        if(keyCode==38)
        textField.setText("");
        if(keyCode==39)
        textField.setText("");
        if(keyCode==40)
        textField.setText("");
        if(keyCode==45)
        textField.setText("");
        if(keyCode==46)
        textField.setText("");
        if(keyCode==144)
        textField.setText("");
        if(keyCode==145)
        textField.setText("");
        if(keyCode==19)
        textField.setText("");
        if(keyCode==186)
        textField.setText("");
        if(keyCode==187)
        textField.setText("");
        if(keyCode==189)
        textField.setText("");
        if(keyCode==191)
        textField.setText("");
        if(keyCode==192)
        textField.setText("");
        if(keyCode==219)
        textField.setText("");
        if(keyCode==220)
        textField.setText("");
        if(keyCode==222)
        textField.setText("");
        if(keyCode==188)
        textField.setText("");
        if(keyCode==190)
        textField.setText("");
        if(keyCode==191)
        textField.setText("");

         if(keyCode==112)
        textField.setText("F1");
        if(keyCode==113)
        textField.setText("F2");
        if(keyCode==114)
        textField.setText("F3");
        if(keyCode==115)
        textField.setText("F4");
        if(keyCode==116)
        textField.setText("F5");
        if(keyCode==117)
        textField.setText("F6");
        if(keyCode==118)
        textField.setText("F7");
        if(keyCode==119)
        textField.setText("F8");
        if(keyCode==120)
        textField.setText("F9");
        if(keyCode==121)
        textField.setText("F10");
        if(keyCode==122)
        textField.setText("F11");
        if(keyCode==123)
        textField.setText("F12");
     //   65 -90 , 112 -123 ,
        if(keyCode==8)
        textField.setText(Language.getString("BCKSPACE"));

          if(keyCode==27)
        textField.setText(Language.getString("ESC"));
         if(keyCode==32)
        textField.setText(Language.getString("SPACE"));
         if(keyCode==33)
        textField.setText(Language.getString("PAGE_UP"));
         if(keyCode==34)
        textField.setText(Language.getString("PAGE_DOWN"));
         if(keyCode==35)
        textField.setText(Language.getString("END"));
          if(keyCode==36)
        textField.setText(Language.getString("HOME"));


    }

    public void keyReleased(KeyEvent e) {

        char comparingChar = e.getKeyChar();

        for(int r=0;r<charArray.length;r++){
            if(comparingChar == charArray[r]){
                textField.setText("");
            }
        }

        int keyCode = e.getKeyCode();
        String comparingString = String.valueOf(e.getKeyChar());

         int numpandwn = 95;
        int numpanup = 112;

        if((keyCode>numpandwn) && (keyCode<numpanup)){
            textField.setText("");
        }

        char myChar = '';


        if(keyCode==37)
        textField.setText("");
        if(keyCode==38)
        textField.setText("");
        if(keyCode==39)
        textField.setText("");
        if(keyCode==40)
        textField.setText("");
        if(keyCode==45)
        textField.setText("");
        if(keyCode==46)
        textField.setText("");
        if(keyCode==144)
        textField.setText("");
        if(keyCode==145)
        textField.setText("");
        if(keyCode==19)
        textField.setText("");
        if(keyCode==186)
        textField.setText("");
        if(keyCode==187)
        textField.setText("");
        if(keyCode==189)
        textField.setText("");
        if(keyCode==191)
        textField.setText("");
        if(keyCode==192)
        textField.setText("");
        if(keyCode==219)
        textField.setText("");
        if(keyCode==220)
        textField.setText("");
        if(keyCode==222)
        textField.setText("");
        if(keyCode==188)
        textField.setText("");
        if(keyCode==190)
        textField.setText("");
        if(keyCode==191)
        textField.setText("");

        int numdwn = 47;
        int numup = 58;
        if((keyCode>numdwn) && (keyCode<numup)){
           
            textField.setText("");
        }


        if(keyCode==112)
               textField.setText("F1");
               if(keyCode==113)
               textField.setText("F2");
               if(keyCode==114)
               textField.setText("F3");
               if(keyCode==115)
               textField.setText("F4");
               if(keyCode==116)
               textField.setText("F5");
               if(keyCode==117)
               textField.setText("F6");
               if(keyCode==118)
               textField.setText("F7");
               if(keyCode==119)
               textField.setText("F8");
               if(keyCode==120)
               textField.setText("F9");
               if(keyCode==121)
               textField.setText("F10");
               if(keyCode==122)
               textField.setText("F11");
               if(keyCode==123)
               textField.setText("F12");
            //   65 -90 , 112 -123 ,
               if(keyCode==8)
               textField.setText(Language.getString("BCKSPACE"));

                 if(keyCode==27)
               textField.setText(Language.getString("ESC"));
                if(keyCode==32)
               textField.setText(Language.getString("SPACE"));
                if(keyCode==33)
               textField.setText(Language.getString("PAGE_UP"));
                if(keyCode==34)
               textField.setText(Language.getString("PAGE_DOWN"));
                if(keyCode==35)
               textField.setText(Language.getString("END"));
                 if(keyCode==36)
               textField.setText(Language.getString("HOME"));


    }


    public String completingText(){
        if(isAlt&&isCtrl&&isShift){
            selected = altBox.getText()+"+"+ctrlBox.getText()+"+"+shiftBox.getText();
        }

         if(isAlt&&isCtrl&&!isShift){
            selected = altBox.getText()+"+"+ctrlBox.getText();
        }

         if(isAlt&&!isCtrl&&isShift){
            selected = altBox.getText()+"+"+shiftBox.getText();
        }
         if(isAlt&&!isCtrl&&!isShift){
            selected = altBox.getText();
        }
         if(!isAlt&&isCtrl&&isShift){
            selected = ctrlBox.getText()+"+"+shiftBox.getText();
        }
         if(!isAlt&&isCtrl&&!isShift){
            selected = ctrlBox.getText();
        }
         if(!isAlt&&!isCtrl&&isShift){
            selected = shiftBox.getText();
        }


      return selected;

}

     public int getDefaultCloseOperation() {
        return defaultCloseOperation;
    }

    public void settingInitialData(){
        JOptionPane.showMessageDialog(this, Language.getString("SAVE_CHANGES"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
    }

     public void windowsClosingMethod(){
       if(!isSaved){
              settingInitialData();
              panel.settingUnSellected(box,important);
          }
    }

     public void windowsClosingSavingMethod(){
//       if(!isSaved){
//              settingInitialData();
         System.out.println("im in windowsClosingSavingMethod");
              panel.settingUnSellected(box,important);
//          }
    }

    public void windowsClosingCanceledMethod(){

              panel.settingUnSellected(box,important);
//          }
        isCanceled = false;
    }

    public MainComboBox sendBox(){
        return box;
    }


  }
