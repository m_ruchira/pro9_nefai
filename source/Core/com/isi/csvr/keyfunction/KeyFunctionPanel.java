package com.isi.csvr.keyfunction;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWPasswordField;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.virtualkeyboard.VirtualKeyboard;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.Hashtable;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: May 21, 2008
 * Time: 1:40:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class KeyFunctionPanel extends JPanel implements ActionListener{

    JPanel mainPanel;
   //   private VirtualFunctionKeyBoard jframe;
    TWPasswordField pwd;
     static MyButton[] button ;
    static MyButton[] buttonSpecial ;

    static MainComboBox[] comboBoxList;
    static MainComboBox[] comboBoxListSpecial;

   static String[] comboText =new String[]{};
   static  boolean isValid;
   static Hashtable<String, String> comboBoxText;

    String disable = "Disable";

    MainComboBox boxObject;
    static String textFromHash;
       KeySelectionPanel selPanel = new KeySelectionPanel(comboText);

    JPanel downPanel;
     KeyFunctionSettings keySettings = new KeyFunctionSettings();
    String[] lableArray = {Language.getString("BUY_ORDER_ENTRY_WINDOW"),Language.getString("SELL_ORDER_ENTRY_WINDOW"),
                            Language.getString("DERV_BUY_ORDER_ENTRY_WINDOW"),Language.getString("DERV_SELL_ORDER_ENTRY_WINDOW"),
                            Language.getString("NCDEX_BUY_ORDER_ENTRY_WINDOW"),Language.getString("NCDEX_SELL_ORDER_ENTRY_WINDOW"),
                            Language.getString("ORDER_BOOK"),Language.getString("MARKET_WATCH"),Language.getString("BEST_FIVE"),
                            Language.getString("MARKET_PICTURE"),Language.getString("TRADE_BOOK"),Language.getString("MARKET_SNAPSHOT"),
                            Language.getString("MESSAGE_LOG"),Language.getString("FAST_ORDER_ENTRY"),Language.getString("TO_CANCEL_A_PENDING_ORDER"),
                            Language.getString("TO_MODIFY_A_PENDING_ORDER"),Language.getString("TO_CANCEL_ALL_PENDING_ORDER"),
                            Language.getString("TICKER_EQUITY"),Language.getString("TICKER_DERIVATIVES"),Language.getString("EQUITY_NET_POSITION"),
                            Language.getString("DERIVATIVES_NET_POSITION"),Language.getString("INTEGRATED_NET_POSITION"),
                            Language.getString("SECURITY_INFORMATION"),Language.getString("CONTRACT_INFORMATION"),
                            Language.getString("SNAP_QUATE_EQUITY"),Language.getString("DERIVATIVES_SNAP_QUOTE"),Language.getString("MOST_ACTIVE_SECURITIES_USING_FILTER_OPTION"),
                            Language.getString("MOST_ACTIVE_SECURITIES"),Language.getString("NSE_AUCTION_INQUIRY"),Language.getString("BSE_AUCTION_INQUIRY"),
                            Language.getString("MARKET_MOVEMENT"),Language.getString("DERIVATIVES_BUY_ORDER_ENTRY_WINDOW"),Language.getString("DERIVATIVES_SELL_ORDER_ENTRY_WINDOW"),
                            Language.getString("SPREAD_ORDER"),Language.getString("SPREAD_ORDERS_BOOK"),Language.getString("SANP_QUOTE_DERIVATIVES"),
                            Language.getString("TOP_GAINERS_LOSERS_USING_FILTER_OPTION"),Language.getString("TOP_GAINERS_LOSERS"),Language.getString("FAST_ORDER_ENTRY_DERIVATIVES_WINDOW"),
                            Language.getString("MARKET_MOVEMENT_DERIVATIVES"),Language.getString("LOG_ON_TO_THE_SERVER"),Language.getString("LOG_OFF_FROM_THE_SERVER"),
                            Language.getString("TO_PRINT_A_SCREEN"),Language.getString("GO_TO_SECURITY_TOOLBAR"),Language.getString("LOCK_WORKSTATION"),
                            Language.getString("GO_TO_START_OF_THE_PAGE"),Language.getString("GO_TO_END_OF_THE_PAGE"),Language.getString("TO_CLOSE_S/W"),
                            Language.getString("SPREAD_OFFLINE"),Language.getString("BULK_ORDERS_BOOK"),Language.getString("BASKET_TRADING"),
                            Language.getString("ONLINE_BACK_UP"),Language.getString("PORTFOLIO"),Language.getString("ONLINE_BACKUP"),
                            Language.getString("SPREAD/ARBITRAGE_WATCH"),Language.getString("EXPRESSION_BUILDER"),Language.getString("SURVEILLANCE_WATCH"),
                            Language.getString("COLUMN_PROFILE"),Language.getString("PREFERENCES"),
                            Language.getString("SYSTEM_CONFIGURATION")};

    String[] specialLableArray = {Language.getString("PERCENTAGE_CHART"),Language.getString("FIXED_TIME_INTERVAL_CHART"),Language.getString("LINEAR_TIME_X_AXIS"),
                                    Language.getString("AUTO_SCROLL"),Language.getString("MANAGE_SCRIPS"),Language.getString("CHART_PROPERTIES"),
                                    Language.getString("TO_SCROLL_LEFT"),Language.getString("TO_SCROLL_RIGHT"),
                                    Language.getString("TO_MOVE_FIRST_POINT"),Language.getString("TO_MOVE_LAST_POINT")};
    private String[] comboSpecialText;
//    private String[] arrayString = new String[]{};


    public  Hashtable<String, String> getFunctionKeys() {
        return functionKeys;
    }

    public  void setFunctionKeys(Hashtable<String, String> functionKeys) {
        System.out.println("here setting method"+functionKeys.size());
        KeyFunctionPanel.functionKeys = functionKeys;
        keySettings.saveCustomViewSettings(functionKeys);
    }

    private static Hashtable<String,String> functionKeys = new Hashtable<String, String>();

    public KeyFunctionPanel(String t){
    }

    public KeyFunctionPanel(){
        super();
        keySettings.loadCustomViewSettings();
        Hashtable gettingHash;
         gettingHash = keySettings.getCustomViewSettings();


            setOpaque(true);

            JLabel instructionsLeft = new JLabel(""+Language.getString("FUNCTIONS"));

            instructionsLeft.setFont(this.getFont().deriveFont(Font.BOLD, new Float(11)));

            JLabel instructionsRight = new JLabel(Language.getString("KEYS"));
            instructionsRight.setFont(this.getFont().deriveFont(Font.BOLD, new Float(11)));

        JLabel instructions = new JLabel("Special Keys");
         instructions.setFont(this.getFont().deriveFont(Font.BOLD, new Float(12)));

            JPanel instructionPanel = new JPanel(new FlexGridLayout(new String[]{"60%","40%"},new String[]{"10"},20,5));

            instructionPanel.add(instructionsLeft);
            instructionPanel.add(instructionsRight);



            mainPanel = new JPanel(new FlexGridLayout(new String[]{"60%","40%"},new String[]{"20","20","20","20","20","20","20","20","20"
                               ,"20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20"
                                ,"20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20"
                                ,"20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20","20"},20,5));

        downPanel = new JPanel(new FlexGridLayout(new String[]{"45%","30%","25%"},new String[]{"20"},20,1));



        JLabel bulLable = new JLabel("");
        JComboBox combo = new JComboBox();
        combo.addItem("first");
        combo.addItem("second");


        TWButton abortChangesbtn = new TWButton("Abort Changes");
        TWButton saveBtn = new TWButton("Save");
         saveBtn.addActionListener(new ActionListener(){
             public void actionPerformed(ActionEvent e) {
                 System.out.println("im performed");
               setData();
             }
         });

         abortChangesbtn.addActionListener(new ActionListener(){
             public void actionPerformed(ActionEvent e) {
                 System.out.println("im performed abort");
                 selPanel.windowsClosingSavingMethod();
             }

         });
        downPanel.add(new JLabel(""));
        downPanel.add(new JLabel(""));

//        downPanel.add(abortChangesbtn);
        downPanel.add(saveBtn);

        comboBoxList = new MainComboBox[60];
        comboBoxListSpecial = new MainComboBox[10];
         button = new MyButton[60];
        buttonSpecial = new MyButton[10];

      


         for(int t=0;t<comboBoxList.length;t++){

             String fromHash = (String) gettingHash.get("function" + t);
             textFromHash = fromHash;
               if(fromHash.equals(disable)) {
              comboText = new String[]{fromHash, "Define New Combination"};
              comboBoxList[t] = new MainComboBox();

             comboBoxList[t].addItem(comboText[0]);
             comboBoxList[t].addItem(comboText[1]);

             KeySelectionPanel selPanel = new KeySelectionPanel(comboText);

               }

             if(!(fromHash.equals(disable))) {
              comboText = new String[]{fromHash, "Define New Combination", "Disable"};
              comboBoxList[t] = new MainComboBox();

             comboBoxList[t].addItem(comboText[0]);
             comboBoxList[t].addItem(comboText[1]);
             comboBoxList[t].addItem(comboText[2]);

             KeySelectionPanel selPanel = new KeySelectionPanel(comboText);

               }
        }

          for(int s=0;s<comboBoxListSpecial.length;s++){

            String fromSpecialHash = (String) gettingHash.get("function"+(s+60));

              comboSpecialText = new String[]{fromSpecialHash, "Define New Combination", "Disable"};
              comboBoxListSpecial[s] = new MainComboBox();

             comboBoxListSpecial[s].addItem(comboSpecialText[0]);
             comboBoxListSpecial[s].addItem(comboSpecialText[1]);
             comboBoxListSpecial[s].addItem(comboSpecialText[2]);
        }

//       JLabel myLable = new JLabel("f");
//        myLable.setSize(new Dimension(10,10));

        for(int c=0;c<lableArray.length;c++){

            mainPanel.add(new JLabel(lableArray[c]));
            mainPanel.add(comboBoxList[c]);
        }

         for(int t=0;t<comboBoxListSpecial.length;t++){
          comboBoxListSpecial[t].addActionListener(comboBoxListSpecial[t]);
        }


        for(int t=0;t<comboBoxList.length;t++){
          comboBoxList[t].addActionListener(comboBoxList[t]);
        }
        mainPanel.add(instructions);
        mainPanel.add(bulLable);

        for(int d=0;d<specialLableArray.length;d++){
            mainPanel.add(new JLabel(specialLableArray[d]));
            mainPanel.add(comboBoxListSpecial[d]);
        }

        mainPanel.setBackground(Color.white);
        JScrollPane scroller = new JScrollPane(mainPanel);
        scroller.setPreferredSize(new Dimension(100,100));

       setLayout(new BorderLayout());
       add(instructionPanel, BorderLayout.NORTH);
        add(scroller, BorderLayout.CENTER);
        add(downPanel, BorderLayout.SOUTH);

    }


       public void settingDefault(){

            Hashtable gettingHash2;
         gettingHash2 = keySettings.getCustomViewSettings();
           for(int t=0;t<comboBoxList.length;t++){
               comboBoxList[t].removeAll();

             String fromHash = (String) gettingHash2.get("function" + t);
               if(fromHash.equals(disable)) {
              comboText = new String[]{fromHash, "Define New Combination"};
              comboBoxList[t] = new MainComboBox();
             comboBoxList[t].addItem(comboText[0]);
             comboBoxList[t].addItem(comboText[1]);

             KeySelectionPanel selPanel = new KeySelectionPanel(comboText);
               }

             if(!(fromHash.equals(disable))) {
              comboText = new String[]{fromHash, "Define New Combination", "Disable"};
              comboBoxList[t] = new MainComboBox();
              comboBoxList[t].addItem(comboText[0]);
             comboBoxList[t].addItem(comboText[1]);
             comboBoxList[t].addItem(comboText[2]);

             KeySelectionPanel selPanel = new KeySelectionPanel(comboText);

               }

        }

        for(int s=0;s<comboBoxListSpecial.length;s++){

            String fromSpecialHash = (String) gettingHash2.get("function"+(s+60));
            comboBoxList[s].removeAll();

              comboSpecialText = new String[]{fromSpecialHash, "Define New Combination", "Disable"};
              comboBoxListSpecial[s] = new MainComboBox();
              comboBoxListSpecial[s].addItem(comboSpecialText[0]);
             comboBoxListSpecial[s].addItem(comboSpecialText[1]);
             comboBoxListSpecial[s].addItem(comboSpecialText[2]);
        }
       }

    public void thirdMethod(MainComboBox secondComboBox){
        
    }

      public void setDataAll(String completeText, String text){
          for(int s=0;s<button.length;s++){
              this.functionKeys.put(text,completeText);
          }
        keySettings.saveCustomViewSettings(functionKeys);
    }

     public void setData(){

          for(int s=0;s<comboBoxList.length;s++){
                  this.functionKeys.put("function"+s, (String) comboBoxList[s].getSelectedItem());
          }

         for(int r=0;r<comboBoxListSpecial.length;r++){
              this.functionKeys.put("function"+(r+60), (String) comboBoxListSpecial[r].getSelectedItem());
         }
         System.out.println("size of saving hash"+functionKeys.size());
        setFunctionKeys(functionKeys);
    }


    public void checkingSameFunctions(){
         comboBoxText = new Hashtable();
         System.out.println("in checkingSameFunctions");
          for(int s=0;s<comboBoxList.length;s++){
                  this.comboBoxText.put("function"+s, (String) comboBoxList[s].getSelectedItem());
          }

         for(int r=0;r<comboBoxListSpecial.length;r++){
              this.comboBoxText.put("function"+(r+60), (String) comboBoxListSpecial[r].getSelectedItem());
         }

    }

    public void checkingProgress(String textToCheck){

        checkingSameFunctions();
        Enumeration e =comboBoxText.keys();
        while (e.hasMoreElements()) {
         Object key = e.nextElement();
         Object value = comboBoxText.get(key);
            if(value.equals(textToCheck)){

                this.isValid=false;
                 KeySelectionPanel selPanel = new KeySelectionPanel(comboText);
              JOptionPane.showMessageDialog(this, Language.getString("KEY_ERROR_MESSAGE"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
                 break;

            }

            else{
                this.isValid = true;

        }
 }


    }

    public void actionPerformed(ActionEvent e) {
//        System.out.println("action");
    }

    public void settingChoice(String test, MainComboBox box){

        checkingProgress(test);
        if(isValid){
            System.out.println("checking isvalid"+isValid);
               box.removeAllItems();
                comboText[0] = test;

                  box.addItem(comboText[0]);
              box.addItem(comboText[1]);
              box.addItem(comboText[2]);
        }

        else{
//            System.out.println("qqqqqqqqqqqqqqqqqqq");
        }
        checkingSameFunctions();

    }

    public void settingDisabled(String test, MainComboBox box){
               box.removeAllItems();
                comboText[0] = test;

                  box.addItem(comboText[0]);
              box.addItem(comboText[1]);
             // box.addItem(comboText[2]);

    }

     public void settingUnSellected( MainComboBox box, String important){

               box.removeAllItems();
         comboText[0] = important;

         box.addItem(comboText[0]);
         box.addItem(comboText[1]);
         box.addItem(comboText[2]);

    }
    
}
