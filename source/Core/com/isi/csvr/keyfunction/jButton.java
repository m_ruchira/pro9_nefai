package com.isi.csvr.virtualkeyboard;

import com.isi.csvr.shared.*;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Jagoda S.D.
 * Date: Apr 29, 2005
 * Time: 3:42:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class jButton extends JButton {
    public jButton() {
        setButton();
    }

    public jButton(String sr) {
        setButton();
        this.setText(sr);
    }

    public void setButton() {
        this.setPreferredSize(new Dimension(20, 20));
        Font font = new TWFont("Arial", Font.BOLD, 16);
        this.setFont(font);
    }

    public Insets getInsets() {
        return new Insets(1, 1, 1, 1);
    }
}
