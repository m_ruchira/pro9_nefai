package com.isi.csvr.theme;

import java.util.Comparator;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class ThemeComparator implements Comparator {

    public ThemeComparator() {
    }

    public int compare(Object o1, Object o2) {
        String[] a1 = (String[]) o1;
        String[] a2 = (String[]) o2;
        return a1[2].compareTo(a2[2]);
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}