// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.theme;

/**
 * This class maintains color and font settings for a
 * give theme.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.reports.ReportUtilities;
import com.isi.csvr.shared.*;
import com.isi.csvr.ticker.GlobalSettings;
import com.isi.csvr.ticker.advanced.UpperPanelSettings;

import javax.swing.*;
import javax.swing.plaf.BorderUIResource;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.FontUIResource;
import javax.swing.plaf.IconUIResource;
import javax.swing.plaf.basic.BasicBorders;
import javax.swing.plaf.metal.DefaultMetalTheme;
import java.awt.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.Properties;

public class Theme extends DefaultMetalTheme {

    public static Color MENU_SELECTION_COLOR;
    public static Color MENU_GRADIENT1_COLOR;
    public static Color MENU_GRADIENT2_COLOR;
    public static Color BUTTON_GRADIENT1_COLOR;
    public static Color BUTTON_GRADIENT2_COLOR;

    public static Color SECTORMAP_MINUS_DARK_COLOR;
    public static Color SECTORMAP_MINUS_LIGHT_COLOR;
    public static Color SECTORMAP_PLUS_DARK_COLOR;
    public static Color SECTORMAP_PLUS_LIGHT_COLOR;

    public static Color SIDEBAR_LIGHT_COLOR;
    public static Color SIDEBAR_DARK_COLOR;

//	public static Color BID_COLOR;
//	public static Color ASK_COLOR;
    public static Color BIDASK_UP_COLOR;
    public static Color BIDASK_DOWN_COLOR;
    public static Color TNS_SUCCESSIVE_TRDES_COLOR;
    public static Color INDEXPANEL_CASHMAP_BORDER_COLOR;
    public static Color INDEXPANEL_FORECAST_INDEX_COLOR;
    private static Color menuFontColor;
    private static Properties themeTable;
    private static long g_lLastUpdated = 0;
    private static String id;
    private static String[][] themes;

    public static float mainBoardAlphaLevel = 0.5f;
    public static boolean mainBoardAlphEnabled = false;

    private ColorUIResource primary2 = new ColorUIResource(Color.white);
    private ColorUIResource primary3 = new ColorUIResource(Color.yellow);
    private ColorUIResource secondary3 = new ColorUIResource(Color.cyan);
    private static ColorUIResource black = new ColorUIResource(Color.yellow);
    private ColorUIResource white = new ColorUIResource(Color.cyan);

    private static Hashtable<Themeable, Themeable> g_oThemeClients = new Hashtable<Themeable, Themeable>();

//    public static Font DEFAULT_FONT = new TWFont("Arial", Font.PLAIN, 12);

    private static Theme self;
    private static final String nameTag = "NAME";
    private static final String imageTag = "IMAGE";

    /**
     * Constructor
     */
    private Theme() {
        //g_oBG = new Color(255,255,0);
        initialize();
        getThemeList();
    }

    public static Theme getInstance() {
        if (self == null) {
            self = new Theme();
        }
        return self;
    }

    private void initialize() {
        try {
            UIManager.put("InternalFrame.closeButtonToolTip", Language.getString("CLOSE_WINDOW"));
            UIManager.put("InternalFrame.iconButtonToolTip", Language.getString("MINIMIZE_WINDOW"));
            UIManager.put("InternalFrame.restoreButtonToolTip", Language.getString("RESTORE_WINDOW"));
            UIManager.put("InternalFrame.maxButtonToolTip", Language.getString("MAXIMIZE_WINDOW"));
            UIManager.put("InternalFrame.printButtonToolTip", Language.getString("PRINT_WINDOW"));
            UIManager.put("InternalFrame.servicesButtonToolTip", Language.getString("NAVIGATOR"));
            UIManager.put("InternalFrame.resizeColsButtonToolTip", Language.getString("ADJUST_WIDTHS"));
            UIManager.put("InternalFrame.detachButtonToolTip", Language.getString("DETACH_WINDOW"));
            UIManager.put("InternalFrame.lnkGroupButtonToolTip", Language.getString("LINK_GRP"));
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static String[][] getThemeList() {
        File files = new File("Themes\\");
        File[] themefiles = files.listFiles(new ThemeFileFilter());
        if (themefiles == null) {
            themefiles = new File[0];
        } else {
            themes = new String[themefiles.length][4];
            for (int i = 0; i < themes.length; i++) {
                try {
                    themes[i][1] = themefiles[i].getAbsolutePath(); // file path
                    themes[i][2] = getThemeName(themefiles[i].getAbsolutePath()); // theme name
                    themes[i][3] = getThemeImage(themefiles[i].getAbsolutePath()); // theme name
                    themes[i][0] = getThemeID(themefiles[i].getName());  // id
                } catch (Exception ex) {
                    themes[i][0] = "0";
                }
            }
        }
        ThemeComparator comparator = new ThemeComparator();
        Arrays.sort(themes, comparator);
        comparator = null;
        return themes;
    }

    public String[][] getThemes() {
        return themes;
    }

    private static String getThemeID(String filename) {
        String[] name = filename.split("\\."); // break from the dot
        String id = name[0].substring(5); // extract the id from the name
        name = null;
        return id;
    }

    private static String getThemeName(String path) {
        try {
            String buffer;

            // breake tag and value
            int count = 0;
            String nameEn = "";
            String name = "";
            String[] pair;
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
            nameEn = Language.getString("THEME_NAME"+" " +in.readLine().split("=")[1]);
            while(count < 1){
                try {
                    buffer = in.readLine();
                    pair = buffer.split("=");
//                    if(pair[0].equalsIgnoreCase(nameTag+"_EN")){
//                        count++;
//                        nameEn = pair[1];
//                    }
                    if(pair[0].equalsIgnoreCase(nameTag+"_"+Language.getLanguageTag())){
                        count++;
                        name = pair[1];
                    }
                    buffer = null;
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
//            buffer = in.readLine();
            in.close();
            in = null;
            buffer = null;

            // breake according to the language
//            pair = pair[1].split("\\|");
            if(name.equals("")){
                name = nameEn;
            }
            return UnicodeUtils.getNativeString(name);

        } catch (Exception ex) {
            return Language.getString("NA");
        }
    }

    private static String getThemeImage(String path) {
        try {
            String buffer;

            // breake tag and value
//            DataInputStream in = new DataInputStream(new FileInputStream(path));
            BufferedReader in = new BufferedReader(new InputStreamReader(new FileInputStream(path)));
            boolean isFound = false;
            String[] pair = null;
            String imageName = "";
            while(!isFound){
                try {
                    buffer = in.readLine();
                    pair = buffer.split("=");
                    if(pair[0].equals(imageTag)){
                        isFound = true;
                        imageName = pair[1];
                    }
                    buffer = null;
                } catch (Exception e) {
                    isFound = false;
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
//            in.readLine();
//            buffer = in.readLine();
            in.close();
            in = null;
//            String[] pair = buffer.split("=");
            buffer = null;

            // breake according to the language
            return imageName;
//            return pair[1];

        } catch (Exception ex) {
            return Language.getString("NA");
        }
    }

    public static Color getBlackColor() {
        return black;
    }

    /**
     * Register a component as a theme applicable component
     * The component regestered must implement applyTheme()
     * method of Themeable interface
     * @param oComponent
     */
    public static void registerComponent(Themeable oComponent) {
        g_oThemeClients.put(oComponent, oComponent);
    }

    /**
     * Un-Register a themeable component from the theme
     * @param oComponent
     */
    public static void unRegisterComponent(Themeable oComponent) {
        g_oThemeClients.remove(oComponent);
    }

    /**
     * Apply the theme change for all Themeable components
     * in the theme clients Hashtable
     */
    public static void fireThemeChanged() {
//        WorkInProgressIndicator workIn = new WorkInProgressIndicator(WorkInProgressIndicator.APPLYING_THEME);
//        workIn.setVisible(true);
        Enumeration clients = g_oThemeClients.keys();
        while (clients.hasMoreElements()) {
            try {
                Themeable client = (Themeable) clients.nextElement();
                client.applyTheme();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        workIn.dispose();
//        workIn = null;
    }

    public boolean isValidTheme(String sID){
        File file = new File(".\\themes\\Theme" + sID + ".properties");
         return file.exists();
    }

    public String getDefaultThemeID(){
        try {
            return themes[0][0];
        } catch(Exception e) {
            return null;
        }
    }

    /**
     * Loads the given theme from the file
     * @param sID
     */
    public void loadTheme(String sID) {

        
        try {
            id = sID;
            FileInputStream oIn = new FileInputStream(".\\themes\\Theme" + sID + ".properties");
            themeTable = new Properties();
            themeTable.load(oIn);
            oIn.close();
            loadMetalTheme();

            Enumeration keys = themeTable.keys();
            while (keys.hasMoreElements()) {
                try {
                    String key = (String) keys.nextElement();
                    if (key.startsWith("*")) {
                        String uiKey = key.substring(1);
                        if (UIManager.get(uiKey) instanceof ColorUIResource) {
                            UIManager.put(uiKey, new ColorUIResource(new Color(Integer.parseInt(themeTable.getProperty(key)))));
                        } else if (UIManager.get(uiKey) instanceof FontUIResource) {
                            String[] fontData = themeTable.getProperty(key).split(",");
                            UIManager.put(uiKey, new FontUIResource(fontData[0], Integer.parseInt(fontData[1]),
                                    Integer.parseInt(fontData[02])));
                            fontData = null;
                        } else {
                            UIManager.put(uiKey, themeTable.getProperty(key));
                        }
                        uiKey = null;
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            keys = null;


            g_lLastUpdated = System.currentTimeMillis();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static long getLasetUpdatedTime() {
        return g_lLastUpdated;
    }

    public static String getID() {
        return id;
    }

    public static void setSelectedID(String selected){
        id = selected;
    }

    /**
     * Returns the InnerLogo File Name for the selected Theme
     */
    public static String getInnerLogoName() {
        return "images/Theme" + id + "/Logo.jpg";
    }

    /**
     * Returns a color object for this id
     */
    /*public static boolean getBoolean(String sID) {
        String sParam;
        try {
            sParam = (String) themeTable.get(sID);
            if (sParam.equals("1")) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }*/

    /**
     * Returns a color object for this id
     * @return
     */
    public static Color getColor(String sID) {
        String sParam;
        try {
            sParam = (String) themeTable.get(sID);
            if (sParam == null) {
                sParam = "0";
            }
        } catch (Exception e) {
            sParam = "0";
        }
        return new Color((new Integer(sParam)));
    }

    public static Color getOptionalColor(String sID) {
        String sParam;
        try {
            sParam = (String) themeTable.get(sID);
            if (sParam == null) {
                return null;
            } else {
                return new Color((new Integer(sParam)));
            }
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns a color object for this id
     */
    public static Color getDTQHeadColor() {
        String sParam;
        try {
            sParam = (String) themeTable.get("MENU_SELECTION_BGCOLOR");
            if (sParam == null) {
                sParam = "0";
            }
        } catch (Exception e) {
            sParam = "0";
        }
        return new Color((new Integer(sParam)).intValue());
    }

    /**
     * Returns a color object for this id
     */
    public static String getFontName(String sID) {
        String sParam;

        /* Check for the arabic default font first */
        try {
            if (!Language.isLTR()) {
                if (isFontAvialble(TWControl.getItem("ARABIC_FONT"))) {
                    return TWControl.getItem("ARABIC_FONT");
                }
            }
        }
        catch (Exception ex) {
        }

       /* try {
            sParam = (String) themeTable.get(sID);
            if (sParam == null) {
                sParam = "AR PL SungtiL GB";
            } else {
                sParam = "AR PL SungtiL GB";//sParam.split(",")[0];
            }
        } catch (Exception e) {
            sParam = "AR PL SungtiL GB";
        }*/

        try {
            sParam = (String) themeTable.get(sID);
            if (sParam == null) {
                sParam = "Arial";
            }else {
                sParam = sParam.split(",")[0];
            }
        } catch (Exception e) {
            sParam = "Arial";
        }
        return sParam;
    }

    public static Font getDefaultFont(int style, int size) {
        if (Language.isLTR()) {
            return (new TWFont(getFontName("DEFAULT_FONT"), style, size));
        } else {
            return (new TWFont(getFontName("DEFAULT_FONT"), style, size));
        }
    }

    public static Font getDefaultFont() {
        return (new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 12));
    }

    /**
     * Returns the font object
     */
    public Font getFont() {
        return new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14);
    }
    public Font getMainBoardDefaultFont(){
        return new TWFont(getFontName("DEFAULT_FONT"),1,14);
    }

    public static Color getMenuFontColor() {
        return menuFontColor;
    }

    private static boolean isFontAvialble(String name) {
        String[] list = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        for (int i = 0; i < list.length; i++) {
            if (list[i].compareToIgnoreCase(name) == 0) {
                list = null;
                return true;
            }
        }
        list = null;
        return false;
    }

    private void validateDefaultFont() {
        /*Font fontP = new Font(getFontName("DEFAULT_FONT"), Font.PLAIN, 12);
        Font fontB = new Font(getFontName("DEFAULT_FONT"), Font.BOLD, 12);
        char trackingChar = Language.getString("LANG").toCharArray()[0];

        if (!((fontP.canDisplay(trackingChar)) && (fontB.canDisplay(trackingChar)))) {
            *//* Default fobt is not suitable *//*
            fontP = new Font("Dialog", Font.PLAIN, 12);
            fontB = new Font("Dialog", Font.BOLD, 12);
            if (((fontP.canDisplay(trackingChar)) && (fontB.canDisplay(trackingChar)))) {
                themeTable.put("DEFAULT_FONT", "dialog");
            } else {
                themeTable.put("DEFAULT_FONT", "sansserif");
            }
        }*/

        TWFont template = new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 10); // trmplate font

        Enumeration keys = UIManager.getDefaults().keys();
        while (keys.hasMoreElements()) {
            try {
                Object key = keys.nextElement();
                Object value = UIManager.get(key);
                if (value instanceof FontUIResource){
                    if (!((String)key).startsWith("PasswordField")){
                        Font font = (FontUIResource)value;
                        UIManager.put(key, template.deriveFont(font.getStyle(), font.getSize()));
                        font = null;
//                        System.out.println("Font Set " + key + " " + template.getName());
                    }
                }
            } catch (Exception e) {
            }
        }
    }

    /* Metal Theme values */
    private void loadMetalTheme() {
        primary2 = new ColorUIResource(getColor("BACKGROUND_COLOR"));
//        primary2 = new ColorUIResource(getColor("MENU_SELECTION_BGCOLOR"));
        primary3 = new ColorUIResource(getColor("BACKGROUND_COLOR"));
        //ColorUIResource(getColor("WINDOW_TITLE_BGCOLOR"));
        secondary3 = new ColorUIResource(getColor("BACKGROUND_COLOR"));
        white = new ColorUIResource(getColor("LIGHT_COLOR"));
        black = new ColorUIResource(getColor("BLACK_COLOR"));
        UIDefaults oDefaults = UIManager.getDefaults();

        ReportUtilities.upColor = getColor("REPORT_UP_FGCOLOR");
        ReportUtilities.downColor = getColor("REPORT_DOWN_FGCOLOR");
        ReportUtilities.normalColor = getColor("REPORT_NORMAL_FGCOLOR");
        ReportUtilities.evenRowBGColor = getColor("REPORT_EVEN_ROW_BGCOLOR");
        ReportUtilities.oddRowBGColor = getColor("REPORT_ODD_ROW_BGCOLOR");
        ReportUtilities.evenRowFGColor = getColor("REPORT_EVEN_ROW_FGCOLOR");
        ReportUtilities.oddRowFGColor = getColor("REPORT_ODD_ROW_FGCOLOR");
        ReportUtilities.gridColor = getColor("REPORT_GRID_COLOR");
        ReportUtilities.headerBGColor = getColor("REPORT_HEADER_BGCOLOR");
        ReportUtilities.headerFGColor = getColor("REPORT_HEADER_FGCOLOR");
        UpperPanelSettings.SMALL_TRADE_COLOR = getColor("SMALL_TRADE_FG_COLOR");

        BIDASK_UP_COLOR = getColor("BIDASK_MAP_BID_COLOR");
        BIDASK_DOWN_COLOR = getColor("BIDASK_MAP_ASK_COLOR");
        TNS_SUCCESSIVE_TRDES_COLOR = getColor("TNS_SUCCESSIVE_TRDES_COLOR");
        INDEXPANEL_CASHMAP_BORDER_COLOR = getColor("INDEXPANEL_CASHMAP_BORDER_COLOR");
        INDEXPANEL_FORECAST_INDEX_COLOR = getColor("INDEXPANEL_FORECAST_INDEX_COLOR");
        mainBoardAlphaLevel = getFloat("MAIN_BOARD_SELECT_ALPHA", -1f);
        mainBoardAlphEnabled = mainBoardAlphaLevel > 0;

        validateDefaultFont();

        if (Language.isLTR()) {
            //Defaults.put("TextPane.font",new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 10)));
            oDefaults.put("Table.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14)));
            oDefaults.put("Label.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 12)));
            oDefaults.put("List.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14)));
            oDefaults.put("ToolTip.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14)));
            oDefaults.put("TableHeader.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 10)));
            oDefaults.put("Tree.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 12)));
            oDefaults.put("InternalFrame.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 10)));
            oDefaults.put("Menu.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 11)));
            oDefaults.put("MenuItem.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 11)));
            oDefaults.put("MenuBar.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 11)));
            oDefaults.put("CheckBoxMenuItem.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 11)));
            oDefaults.put("EditorPane.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 12)));
            oDefaults.put("RadioButtonMenuItem.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 11)));
            oDefaults.put("RadioButton.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 12)));
        } else {
            oDefaults.put("TextPane.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 12)));
            oDefaults.put("Table.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14)));
            oDefaults.put("Label.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14)));
            oDefaults.put("List.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14)));
            oDefaults.put("ToolTip.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14)));
            oDefaults.put("TableHeader.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 12)));
            oDefaults.put("Tree.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14)));
            oDefaults.put("InternalFrame.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 12)));
            oDefaults.put("Menu.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14)));
            oDefaults.put("MenuItem.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14)));
            oDefaults.put("MenuBar.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14)));
            oDefaults.put("CheckBoxMenuItem.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 12)));
            oDefaults.put("EditorPane.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14)));
            oDefaults.put("RadioButtonMenuItem.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 12)));
            oDefaults.put("RadioButton.font", new FontUIResource(new TWFont(getFontName("DEFAULT_FONT"), Font.PLAIN, 14)));
        }

        oDefaults.put("TextField.selectionBackground", new ColorUIResource(getColor("MENU_SELECTION_BGCOLOR")));
        oDefaults.put("TextArea.selectionBackground", new ColorUIResource(getColor("MENU_SELECTION_BGCOLOR")));
        oDefaults.put("EditorPane.selectionBackground", new ColorUIResource(getColor("MENU_SELECTION_BGCOLOR")));
        oDefaults.put("FormattedTextField.selectionBackground", new ColorUIResource(getColor("MENU_SELECTION_BGCOLOR")));
        oDefaults.put("PasswordField.selectionBackground", new ColorUIResource(getColor("MENU_SELECTION_BGCOLOR")));
        oDefaults.put("Panel.background", new ColorUIResource(getColor("BACKGROUND_COLOR")));
        oDefaults.put("Button.background", new ColorUIResource(getColor("BACKGROUND_COLOR")));
        oDefaults.put("Menu.background", new ColorUIResource(getColor("BACKGROUND_COLOR")));
        oDefaults.put("MenuItem.background", new ColorUIResource(getColor("BACKGROUND_COLOR")));
        oDefaults.put("Menu.selectionBackground", new ColorUIResource(getColor("MENU_SELECTION_BGCOLOR")));
        oDefaults.put("MenuItem.selectionBackground", new ColorUIResource(getColor("MENU_SELECTION_BGCOLOR")));
        oDefaults.put("RadioButtonMenuItem.selectionBackground", new ColorUIResource(getColor("MENU_SELECTION_BGCOLOR")));
        oDefaults.put("RadioButtonMenuItem.background", new ColorUIResource(getColor("BACKGROUND_COLOR")));
        oDefaults.put("CheckBoxMenuItem.background", new ColorUIResource(getColor("BACKGROUND_COLOR")));
        oDefaults.put("CheckBoxMenuItem.selectionBackground", new ColorUIResource(getColor("MENU_SELECTION_BGCOLOR")));

        oDefaults.put("TextField.caretForeground", new ColorUIResource(getColor("INPUT_TEXT_FGCOLOR")));
        oDefaults.put("TextArea.caretForeground", new ColorUIResource(getColor("INPUT_TEXT_FGCOLOR")));
        oDefaults.put("EditorPane.caretForeground", new ColorUIResource(getColor("INPUT_TEXT_FGCOLOR")));
        oDefaults.put("FormattedTextField.caretForeground", new ColorUIResource(getColor("INPUT_TEXT_FGCOLOR")));
        oDefaults.put("PasswordField.caretForeground", new ColorUIResource(getColor("INPUT_TEXT_FGCOLOR")));
        oDefaults.put("Menu.foreground", new ColorUIResource(getColor("MENU_FGCOLOR")));
        oDefaults.put("MenuItem.foreground", new ColorUIResource(getColor("MENU_FGCOLOR")));
        oDefaults.put("Menu.selectionForeground", new ColorUIResource(getColor("MENU_SELECTION_FGCOLOR")));
        oDefaults.put("MenuItem.selectionForeground", new ColorUIResource(getColor("MENU_SELECTION_FGCOLOR")));
        oDefaults.put("RadioButtonMenuItem.selectionForeground", new ColorUIResource(getColor("MENU_SELECTION_FGCOLOR")));
        oDefaults.put("RadioButtonMenuItem.foreground", new ColorUIResource(getColor("MENU_FGCOLOR")));
        oDefaults.put("CheckBoxMenuItem.foreground", new ColorUIResource(getColor("MENU_FGCOLOR")));
        oDefaults.put("CheckBoxMenuItem.selectionForeground", new ColorUIResource(getColor("MENU_SELECTION_FGCOLOR")));

        oDefaults.put("TextField.foreground", new ColorUIResource(getColor("INPUT_TEXT_FGCOLOR")));
        oDefaults.put("TextArea.foreground", new ColorUIResource(getColor("INPUT_TEXT_FGCOLOR")));
        oDefaults.put("EditorPane.foreground", new ColorUIResource(getColor("INPUT_TEXT_FGCOLOR")));
        oDefaults.put("Viewport.foreground", new ColorUIResource(getColor("INPUT_TEXT_FGCOLOR")));
        oDefaults.put("FormattedTextField.foreground", new ColorUIResource(getColor("INPUT_TEXT_FGCOLOR")));
        oDefaults.put("TextPane.foreground", new ColorUIResource(Color.black));
        oDefaults.put("Table.background", new ColorUIResource(getColor("BACKGROUND_COLOR")));
        oDefaults.put("TextField.background", new ColorUIResource(getColor("INPUT_TEXT_BGCOLOR")));
        oDefaults.put("TextArea.background", new ColorUIResource(getColor("INPUT_TEXT_BGCOLOR")));
        oDefaults.put("EditorPane.background", new ColorUIResource(getColor("INPUT_TEXT_BGCOLOR")));
        oDefaults.put("FormattedTextField.background", new ColorUIResource(getColor("INPUT_TEXT_BGCOLOR")));
        oDefaults.put("PasswordField.foreground", new ColorUIResource(getColor("INPUT_TEXT_FGCOLOR")));
        oDefaults.put("PasswordField.background", new ColorUIResource(getColor("INPUT_TEXT_BGCOLOR")));

        oDefaults.put("Label.foreground", new ColorUIResource(getColor("LABEL_FGCOLOR")));
        oDefaults.put("OptionPane.messageForeground", new ColorUIResource(getColor("LABEL_FGCOLOR")));
        oDefaults.put("Button.foreground", new ColorUIResource(getColor("LABEL_FGCOLOR")));
        oDefaults.put("CheckBox.foreground", new ColorUIResource(getColor("LABEL_FGCOLOR")));
        oDefaults.put("RadioButton.foreground", new ColorUIResource(getColor("LABEL_FGCOLOR")));
        oDefaults.put("TitledBorder.titleColor", new ColorUIResource(getColor("LABEL_FGCOLOR")));
        oDefaults.put("ComboBox.foreground", new ColorUIResource(getColor("COMBO_LIST_FGCOLOR")));
        oDefaults.put("List.foreground", new ColorUIResource(getColor("LIST_FGCOLOR")));
        oDefaults.put("List.background", new ColorUIResource(getColor("LIST_BGCOLOR")));

        UIManager.put("TitledBorder.border", new BorderUIResource(BorderFactory.createLineBorder(getComponentBorderColor())));
        UIManager.put("InternalFrame.border", new BorderUIResource(GUISettings.getTWFrameBorder()));
        UIManager.put("PopupMenu.border", new BorderUIResource(BorderFactory.createLineBorder(getColor("MENU_BORDER_COLOR"), getInt("MENU_BORDER_THICKNESS", 1))));
        UIManager.put("Menu.border", new BorderUIResource(BorderFactory.createEmptyBorder()));
        UIManager.put("Spinner.border", new BorderUIResource((BorderFactory.createLineBorder(getComponentBorderColor()))));
        UIManager.put("Spinner.arrowButtonBorder", new BorderUIResource((BorderFactory.createLineBorder(getComponentBorderColor()))));
        UIManager.put("TextArea.border", getTextFieldBorder());
        UIManager.put("PasswordField.border", new BorderUIResource((BorderFactory.createLineBorder(getComponentBorderColor()))));
//        UIManager.put("ScrollPane.border", new BorderUIResource(new BorderUIResource(BorderFactory.createEmptyBorder())));
        UIManager.put("TextField.border", getTextFieldBorder());
        UIManager.put("Button.border", new BorderUIResource(new BorderUIResource.CompoundBorderUIResource(BorderFactory.createLineBorder(getComponentBorderColor()),
                new BasicBorders.MarginBorder())));



        oDefaults.put("CheckBox.icon", new TWCheckBoxIcon(new ImageIcon(getTheamedImagePath("checkboxUnSelected")),
                new ImageIcon(getTheamedImagePath("checkboxUnSelectedDisabled")),
                new ImageIcon(getTheamedImagePath("checkboxSelected")),
                new ImageIcon(getTheamedImagePath("checkboxSelectedDisabled"))));//TREE_BGCOLOR
        oDefaults.put("CheckBoxMenuItem.checkIcon", new TWCheckBoxIcon(new ImageIcon(getTheamedImagePath("checkboxUnSelected")),
                new ImageIcon(getTheamedImagePath("checkboxUnSelectedDisabled")),
                new ImageIcon(getTheamedImagePath("checkboxSelected")),
                new ImageIcon(getTheamedImagePath("checkboxSelectedDisabled"))));//TREE_BGCOLOR

        oDefaults.put("RadioButton.icon", new TWRadioButtonIcon(new ImageIcon(getTheamedImagePath("radioButtonUnSelected")),
                new ImageIcon(getTheamedImagePath("radioButtonUnSelectedDisabled")),
                new ImageIcon(getTheamedImagePath("radioButtonSelected")),
                new ImageIcon(getTheamedImagePath("radioButtonSelectedDisabled"))));//TREE_BGCOLOR
        oDefaults.put("RadioButtonMenuItem.checkIcon", new TWRadioButtonIcon(new ImageIcon(getTheamedImagePath("radioButtonUnSelected")),
                new ImageIcon(getTheamedImagePath("radioButtonUnSelectedDisabled")),
                new ImageIcon(getTheamedImagePath("radioButtonSelected")),
                new ImageIcon(getTheamedImagePath("radioButtonSelectedDisabled"))));//TREE_BGCOLOR

        oDefaults.put("Tree.collapsedIcon", new IconUIResource(new ImageIcon("images/theme" + id + "/TreeCollapsed.gif")));
        oDefaults.put("Tree.expandedIcon", new IconUIResource(new ImageIcon("images/theme" + id + "/TreeExpanded.gif")));
//        oDefaults.put("Tree.background", new ColorUIResource(getColor("BACKGROUND_COLOR")));//TREE_BGCOLOR
//        oDefaults.put("Tree.closedIcon", new IconUIResource(new ImageIcon("images/themes/TreeFolderClosed.gif")));
//        oDefaults.put("Tree.openIcon", new IconUIResource(new ImageIcon("images/themes/TreeFolderOpened.gif")));
//        oDefaults.put("Tree.leafIcon", new IconUIResource(new ImageIcon("images/themes/TreeLeaf.gif")));
//        oDefaults.put("Tree.leafIcon", new IconUIResource(new ImageIcon("images/themes/TreeLeaf.gif")));
//        oDefaults.put("Tree.textBackground", new ColorUIResource(getColor("BACKGROUND_COLOR")));
        oDefaults.put("Tree.selectionBackground", new ColorUIResource(getColor("MENU_SELECTION_BGCOLOR")));
        UIManager.put("DesktopIconUI", "com.isi.csvr.shared.TWDesktopIconUI");
        UIManager.put("ScrollBarUI", "com.isi.csvr.shared.TWScrollBarUI");
        UIManager.put("ButtonUI", "com.isi.csvr.shared.TWButtonUI");
        UIManager.put("InternalFrameUI", "com.isi.csvr.shared.TWInternalFrameUI");
        UIManager.put("ComboBoxUI", "com.isi.csvr.util.ExtendedComboUI");
        UIManager.put("GroupableTableHeaderUI", "com.isi.csvr.util.GroupableTableHeaderUI");
        UIManager.put("InternalFrame.activeTitleBackgroundDark", new ColorUIResource(getColor("INTERNALFRAME_ACTIVE_TITLE_BGCOLOR_DARK")));
        UIManager.put("InternalFrame.activeTitleBackgroundLight", new ColorUIResource(getColor("INTERNALFRAME_ACTIVE_TITLE_BGCOLOR_LIGHT")));
        UIManager.put("InternalFrame.activeTitleForeground", new ColorUIResource(getColor("INTERNALFRAME_ACTIVE_TITLE_FGCOLOR")));
        UIManager.put("InternalFrame.inactiveTitleBackgroundDark", new ColorUIResource(getColor("INTERNALFRAME_INACTIVE_TITLE_BGCOLOR_DARK")));
        UIManager.put("InternalFrame.inactiveTitleBackgroundLight", new ColorUIResource(getColor("INTERNALFRAME_INACTIVE_TITLE_BGCOLOR_LIGHT")));
        UIManager.put("InternalFrame.inactiveTitleForeground", new ColorUIResource(getColor("INTERNALFRAME_INACTIVE_TITLE_FGCOLOR")));

        UIManager.put("InternalFrame.closeIcon", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_close"))));
        UIManager.put("InternalFrame.closeIconRollOver", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_close_R"))));
        UIManager.put("InternalFrame.iconifyIcon", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_minimize"))));
        UIManager.put("InternalFrame.iconifyIconRollOver", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_minimize_R"))));
        UIManager.put("InternalFrame.maximizeIcon", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_maximize"))));
        UIManager.put("InternalFrame.maximizeIconRollOver", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_maximize_R"))));
        UIManager.put("InternalFrame.minimizeIcon", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_normal"))));
        UIManager.put("InternalFrame.minimizeIconRollOver", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_normal_R"))));
        UIManager.put("InternalFrame.resizeIcon", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_resize"))));
        UIManager.put("InternalFrame.resizeIconRollOver", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_resize_R"))));
        UIManager.put("InternalFrame.detachIcon", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_detach"))));
        UIManager.put("InternalFrame.detachIconRollOver", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_detach_R"))));
        UIManager.put("InternalFrame.printIcon", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_print"))));
        UIManager.put("InternalFrame.printIconRollOver", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_print_R"))));
        UIManager.put("InternalFrame.servicesIcon", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_services"))));
        UIManager.put("InternalFrame.servicesIconRollOver", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/btn_services_R"))));
        UIManager.put("InternalFrame.linkgrpRed", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/lnk_red"))));
        UIManager.put("InternalFrame.linkgrpGreen", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/lnk_green"))));
        UIManager.put("InternalFrame.linkgrpBlue", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/lnk_blue"))));
        UIManager.put("InternalFrame.linkgrpYellow", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/lnk_yellow"))));
        UIManager.put("InternalFrame.linkgrpWhite", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/lnk_white"))));
        UIManager.put("InternalFrame.linkgrpNotLink", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/lnk_notlink"))));
        UIManager.put("InternalFrame.icon", new IconUIResource(new ImageIcon(getTheamedImagePath("frame/internalicon"))));





        UIManager.put("ScrollBar.buttonIcon", new ImageIcon(getTheamedImagePath("scroll/button")));
        UIManager.put("ScrollBar.trackUpIcon", new ImageIcon(getTheamedImagePath("scroll/trackUp")));
        UIManager.put("ScrollBar.trackDownIcon", new ImageIcon(getTheamedImagePath("scroll/trackDown")));
        UIManager.put("ScrollBar.trackIcon", new ImageIcon(getTheamedImagePath("scroll/trackCenter")));
        UIManager.put("ScrollBar.thumbUpIcon", new ImageIcon(getTheamedImagePath("scroll/ThumbUp")));
        UIManager.put("ScrollBar.thumbDownIcon", new ImageIcon(getTheamedImagePath("scroll/ThumbDown")));
        UIManager.put("ScrollBar.thumbIcon", new ImageIcon(getTheamedImagePath("scroll/ThumbCenter")));

        UIManager.put("ScrollBar.disabledButtonIcon", new ImageIcon(getTheamedImagePath("scroll/DisabledButton")));
        UIManager.put("ScrollBar.disabledTrackUpIcon", new ImageIcon(getTheamedImagePath("scroll/DisabledTrackUp")));
        UIManager.put("ScrollBar.disabledTrackDownIcon", new ImageIcon(getTheamedImagePath("scroll/DisabledTrackDown")));
        UIManager.put("ScrollBar.disabledTrackIcon", new ImageIcon(getTheamedImagePath("scroll/DisabledTrackCenter")));
        UIManager.put("ScrollBar.disabledThumbUpIcon", new ImageIcon(getTheamedImagePath("scroll/DisabledThumbUp")));
        UIManager.put("ScrollBar.disabledThumbDownIcon", new ImageIcon(getTheamedImagePath("scroll/DisabledThumbDown")));
        UIManager.put("ScrollBar.disabledThumbIcon", new ImageIcon(getTheamedImagePath("scroll/DisabledThumbCenter")));

        try {
            UIManager.put("InternalFrame.gradientImageSelectedCenter", new ImageIcon(getTheamedImagePath("frame/titleImageS")));
            UIManager.put("InternalFrame.gradientImageUnSelectedCenter", new ImageIcon(getTheamedImagePath("frame/titleImageU")));
            UIManager.put("InternalFrame.gradientImageSelectedLeft", new ImageIcon(getTheamedImagePath("frame/titleImageSL")));
            UIManager.put("InternalFrame.gradientImageSelectedRight", new ImageIcon(getTheamedImagePath("frame/titleImageSR")));
            UIManager.put("InternalFrame.gradientImageUnSelectedLeft", new ImageIcon(getTheamedImagePath("frame/titleImageUL")));
            UIManager.put("InternalFrame.gradientImageUnSelectedRight", new ImageIcon(getTheamedImagePath("frame/titleImageUR")));
           /* if (!Language.isLTR()) {
                File file = new File("images/theme" + id + "/frame/titleImageSL_RTL.gif"); // check of RTL files exist
                if (file.exists()){
                    UIManager.put("InternalFrame.gradientImageSelectedLeft", new IconUIResource(new ImageIcon("images/theme" + id + "/frame/titleImageSL_RTL.gif")));
                    UIManager.put("InternalFrame.gradientImageSelectedRight", new IconUIResource(new ImageIcon("images/theme" + id + "/frame/titleImageSR_RTL.gif")));
                    UIManager.put("InternalFrame.gradientImageUnSelectedLeft", new IconUIResource(new ImageIcon("images/theme" + id + "/frame/titleImageUL_RTL.gif")));
                    UIManager.put("InternalFrame.gradientImageUnSelectedRight", new IconUIResource(new ImageIcon("images/theme" + id + "/frame/titleImageUR_RTL.gif")));
                }
            }*/

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        MENU_SELECTION_COLOR = getColor("MENU_SELECTION_BGCOLOR");
        MENU_GRADIENT1_COLOR = getColor("MENU_GRADIENT1_COLOR");
        MENU_GRADIENT2_COLOR = getColor("MENU_GRADIENT2_COLOR");
        BUTTON_GRADIENT1_COLOR = getColor("BUTTON_GRADIENT1_COLOR");
        BUTTON_GRADIENT2_COLOR = getColor("BUTTON_GRADIENT2_COLOR");

        SECTORMAP_MINUS_DARK_COLOR = getColor("SECTORMAP_MINUS_DARK_COLOR");
        SECTORMAP_MINUS_LIGHT_COLOR = getColor("SECTORMAP_MINUS_LIGHT_COLOR");
        SECTORMAP_PLUS_LIGHT_COLOR = getColor("SECTORMAP_PLUS_LIGHT_COLOR");
        SECTORMAP_PLUS_DARK_COLOR = getColor("SECTORMAP_PLUS_DARK_COLOR");

        SIDEBAR_LIGHT_COLOR = getOptionalColor("SIDEBAR_GRADIENT_LIGHT_COLOR");
        SIDEBAR_DARK_COLOR = getOptionalColor("SIDEBAR_GRADIENT_DARK_COLOR");

//        BID_COLOR = getColor("BOARD_TABLE_CELL_BID_BGCOLOR1");
//        ASK_COLOR = getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1");
        menuFontColor = new ColorUIResource(getColor("MENU_FGCOLOR"));
    }

    //protected ColorUIResource getPrimary1() { return new ColorUIResource(Color.red);}
    protected ColorUIResource getPrimary2() {
        return primary2;
    }

    protected ColorUIResource getPrimary3() {
        return primary3;
    }

    //protected ColorUIResource getSecondary1() { return new ColorUIResource(Color.blue);}
    //protected ColorUIResource getSecondary2() { return new ColorUIResource(Color.green);}
    protected ColorUIResource getSecondary3() {
        return secondary3;
    }

    protected ColorUIResource getBlack() {
        return black;
    }

    protected ColorUIResource getWhite() {
        return white;
    }

    protected ColorUIResource getComponentBorderColor() {
        return new ColorUIResource(getColor("COMPONENT_BORDER_COLOR"));
    }

    private BorderUIResource getTextFieldBorder(){
        return new BorderUIResource(new BorderUIResource.CompoundBorderUIResource(BorderFactory.createLineBorder(getComponentBorderColor()),
                BorderFactory.createEmptyBorder(0,2,0,2)));
    }

    public static String getHighlightedColor (){
        //Color color = getOptionalColor("SYMBOL_HIGHLIGHT_COLOR");
        try {
            return Integer.toHexString(Integer.parseInt(themeTable.getProperty("SYMBOL_HIGHLIGHT_COLOR")));
            /*String str = Integer.toHexString(color.getRed()) ;
            str = str + Integer.toHexString(color.getGreen());
            str = str +  Integer.toHexString(color.getBlue());
            return str;*/
        } catch (Exception e) {
            return "#FFFF00";
        }
    }
    public static String getSideBarExchangeColor() {
        try {
            return Integer.toHexString(Integer.parseInt(themeTable.getProperty("SIDEBAR_EXCHANGE_COLOR")));

        } catch (Exception e) {
            return "000000";
        }
    }
    public static String getSideBarWatchListColor() {
        try {
            return Integer.toHexString(Integer.parseInt(themeTable.getProperty("SIDEBAR_WATCHLIST_COLOR")));

        } catch (Exception e) {
            return "000000";
        }
    }
    public static String getHexaColor(String color){
         try {
            return Integer.toHexString(Integer.parseInt(themeTable.getProperty(color)));

        } catch (Exception e) {
            return "000000";
        }
    }
    public static String getString(String id){
        return themeTable.getProperty(id);
    }

    public static long getLong(String id){
        try {
            return Long.parseLong(themeTable.getProperty(id));
        } catch (Exception e) {
            return -1;
        }
    }

    public static int getInt(String id, int defaultValue){
        try {
            return Integer.parseInt(themeTable.getProperty(id));
        } catch (Exception e) {
            return defaultValue;
        }
    }

    public static Color getAdminMessageBtnColor(){
//        try {
//            Color color = getOptionalColor("BROADCAST_MESSAGE_BTN_BG_COLOR");
//            if(color!=null){
//                return color;
//            }else{
//                return Color.WHITE;
//            }
//        } catch (Exception e) {
//            return Color.WHITE;
//        }
            return Color.WHITE;
        }
//    }

    private static float getFloat(String id){
        return getFloat(id, 0);
    }

    private static float getFloat(String id, float defauLt){
        String sParam;
        try {
            sParam = (String) themeTable.get(id);
            if (sParam == null) {
                return defauLt;
            }
            return Float.parseFloat(sParam);
        } catch (Exception e) {
            return defauLt;
        }

    }

    public static void applyFGColor(Component component, String id){
        Color color = getOptionalColor(id);
        if (color != null){
            component.setForeground(color);
        }
    }

    public static void applyBGColor(Component component, String id){
        Color color = getOptionalColor(id);
        if (color != null){
            component.setBackground(color);
        }
    }


    public static String getTheamedImagePath(String path){
        String imagePath = "images/theme" + id + "/";
        File file = new File(imagePath + path + ".gif");
        
        if (!file.exists()){
            file = new File(imagePath + path + ".jpg");
            if (!file.exists()){
                return imagePath + path + ".png";
            } else{
                return imagePath + path + ".jpg";
            }
        } else {
            return imagePath + path + ".gif";
        }
    }
}

