package com.isi.csvr.table;

import com.isi.csvr.*;
import com.isi.csvr.table.updator.TableUpdateManager;
import com.isi.csvr.customindex.CustomIndexInterface;
import com.isi.csvr.customindex.CustomIndexListener;
import com.isi.csvr.customindex.CustomIndexStore;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.IOException;
import java.io.File;
import java.util.*;

public class IndexPanel extends JPanel
        implements Themeable, MouseListener, ActionListener, ExchangeListener, ApplicationListener, CustomIndexListener, UpdateableTable {

    private static final int CASH_MAP = 0;
    private static final int CASH_MAP_PCT = 1;
    private static final int COMMODITY = 2;

    private static final int ROW_1_HEIGHT = 15;
    private static final int ROW_2_HEIGHT = 18;
    private static final int PANEL_WIDTH = 765; //830; // 865;// 825;// 750; //682; //752

    private static String CARD_CASHMAP = "CMAP";
    private static String CARD_COMMODITIES = "COMMO";

    private ImageIcon downImage;
    private ImageIcon upImage;
    private Color upColor;
    private Color downColor;
    private Color normalColor;
    private Color upColorCmap;
    private Color downColorCmap;
    private Color normalColorCmap;
    private Color openColor;
    private Color preopenColor;
    private Color closedColor;
    private Color preClosedColor;
    private Color indexGraphFGColor;
    private Color cashMapFgClor;


    private JPopupMenu marketPopup;
    private JPopupMenu cashMapPopup;

    private IconLable lblIndexName = new IconLable(true);
    private JLabel lblIndexValue = new JLabel("");
    private JLabel lblIndexChange = new JLabel("");
    private JLabel lblIndexImage = new JLabel("");
    private JLabel lblIndexPChange = new JLabel("");

    private JLabel lblGapRow = new JLabel();
    private JLabel lblGap1 = new JLabel();
    private JLabel lblGap2 = new JLabel();
    private JLabel lblGap3 = new JLabel();
    private JLabel lblGap4 = new JLabel();
    private JLabel lblGap5 = new JLabel();
    private JLabel lblGap6 = new JLabel();

    private IconLable lblCapTurnover = new IconLable();
    private IconLable lblCapVolume = new IconLable();
    private IconLable lblCapTrades = new IconLable();
    private IconLable lblCashmap = new IconLable(0, true);

    private JLabel lblValueTurnover = new JLabel();
    private JLabel lblValueVolume = new JLabel();
    private JLabel lblValueTrades = new JLabel();
    private JLabel lblValueCashMap = new JLabel();

    private IconLable lblValueCashMapCommoditiesContainer = new IconLable(1, true);
    private JLabel lblValueCashMapContainer = new JLabel();
    private JLabel lblValueCommoditiesContainer = new JLabel();
    private JLabel lblValueCommoditieslast = new JLabel();
    private JLabel lblValueCommoditiesChange = new JLabel();
    private JLabel lblValueCommoditiesPctChange = new JLabel();


    private SimplevariationImage imgValueCashmap = new SimplevariationImage();

    private IconLable lblMarketName = new IconLable();
    private JLabel lblMarketTime = new JLabel();
    private JLabel lblMarketStatus = new JLabel();

    private Thread thread;
    private TWDecimalFormat decimalFormat;
    private TWDecimalFormat numberFormat;
    private Date marketTime;

    private TWDateFormat openMarketTimeFormatter;
    private TWDateFormat closedMarketTimeFormatter;

    private static String symbol = "";
    private static String key = "";
    private JPopupMenu indices;
    private String prevKey;
    private static String workspaceKey = null;

    private String prevMarket;
    private ImageIcon imageicon;
    private ImageIcon imageiconL;
    private ImageIcon imageiconR;

    private ImageIcon imageiconup;
    private ImageIcon imageiconLup;
    private ImageIcon imageiconRup;

    private ImageIcon imageicondown;
    private ImageIcon imageiconLdown;
    private ImageIcon imageiconRdown;

    private ImageIcon imageGraphBG;
    private int imagewidth;
    private int imagewidthup;
    private int imagewidthdown;
    private Font boldFnt;
    private Font indexIDFnt;
    private Font valueFont;
    private Font valueFontNew;

    private static boolean isCustomindex = false;

    private static IndexPanel self = null;
    public double[] indexmapvals = new double[242];
    // public boolean graphMode = false;
    private boolean graphMode = false;
    private boolean isEnglish = true;
    public static String savedKey = "";
    public static String savedSymbol = "";
    private Hashtable<String, NonDefaultIndexList> nonDefaultIndexes;

    private int cashmapMode = 0;
    TWRadioButtonMenuItem menuCashMap;
    TWRadioButtonMenuItem menuNetCashMap;
    ImageBorder cashMapBorder;
    String previousCommoditySymbol = "";

    private final int SYMBOL_TYPE_DEF_INDEX = 0;
    private final int SYMBOL_TYPE_COM_INDEX = 1;
    private int lastNonDefaultSymbolType = SYMBOL_TYPE_DEF_INDEX;
    private String lastNonDefaultSymbol = "";

    private TWFont graphFont;
    private TWDecimalFormat highLowFormat;
    ButtonGroup grp;
    ArrayList<CommoditiesStore.CommodityItem> comoditiesList;

    ArrayList<String> comIndexList = new ArrayList<String>();

    private boolean isResolution2048 = false;

    private IndexPanel() {
        setLayout(new FlowLayout(FlowLayout.LEADING, 0, 0));
        isEnglish = Language.isLTR();
        nonDefaultIndexes = new Hashtable<String, NonDefaultIndexList>();
        isResolution2048 = Toolkit.getDefaultToolkit().getScreenSize().getWidth() >= 1280;

        openMarketTimeFormatter = new TWDateFormat(Language.getString("MARKET_TIME_OPEN"));
        closedMarketTimeFormatter = new TWDateFormat(Language.getString("MARKET_TIME_CLOSE"));

        menuCashMap = new TWRadioButtonMenuItem(Language.getString("INDEX_PAN_PCTCASH"));
        menuCashMap.setActionCommand("N");
        menuNetCashMap = new TWRadioButtonMenuItem(Language.getString("INDEX_PAN_NETCASH"));
        menuNetCashMap.setActionCommand("N");
        cashMapPopup = new JPopupMenu();
        cashMapPopup.setLightWeightPopupEnabled(false);
        grp = new ButtonGroup();
        grp.add(menuCashMap);
        grp.add(menuNetCashMap);
        cashMapPopup.add(menuCashMap);
        cashMapPopup.add(menuNetCashMap);
        menuNetCashMap.setSelected(true);
        GUISettings.applyOrientation(menuNetCashMap);
        menuCashMap.addActionListener(this);
        menuNetCashMap.addActionListener(this);
        GUISettings.applyOrientation(cashMapPopup);
        cashMapBorder = new ImageBorder(2, 2, 2, 2, Color.BLACK);

        highLowFormat = SharedMethods.getDecimalFormat(0);
        graphFont = new TWFont("arial", Font.PLAIN, 8);

        /*  Do not make the font bold in Arabic mode.
            It may cause displaying of unknown unicode symbols */

        lblIndexName.setPreferredSize(new Dimension(188, ROW_1_HEIGHT));
        lblIndexName.setHorizontalAlignment(SwingConstants.CENTER);
        lblIndexName.setOpaque(true);

        lblIndexName.addMouseListener(this);
        lblIndexName.setCursor(new Cursor(Cursor.HAND_CURSOR));
        lblIndexValue.setPreferredSize(new Dimension(66, ROW_2_HEIGHT));
        lblIndexValue.setHorizontalAlignment(SwingConstants.RIGHT);

        lblIndexChange.setPreferredSize(new Dimension(52, ROW_2_HEIGHT));
        lblIndexChange.setHorizontalAlignment(SwingConstants.RIGHT);
        lblIndexImage.setPreferredSize(new Dimension(10, ROW_2_HEIGHT));
        lblIndexImage.setHorizontalAlignment(SwingConstants.CENTER);
        lblIndexPChange.setPreferredSize(new Dimension(55, ROW_2_HEIGHT));
        lblIndexPChange.setHorizontalAlignment(SwingConstants.RIGHT);

        lblCapTurnover.setPreferredSize(new Dimension(121, ROW_1_HEIGHT));
        lblCapTurnover.setHorizontalAlignment(SwingConstants.CENTER);
        lblCapTurnover.setOpaque(true);
        lblCapTurnover.setText(Language.getString("TURNOVER"));
        lblCapVolume.setPreferredSize(new Dimension(88, ROW_1_HEIGHT));
        lblCapVolume.setHorizontalAlignment(SwingConstants.CENTER);
        lblCapVolume.setOpaque(true);
        lblCapVolume.setText(Language.getString("VOLUME"));
        lblCapTrades.setPreferredSize(new Dimension(65, ROW_1_HEIGHT));
        lblCapTrades.setHorizontalAlignment(SwingConstants.CENTER);
        lblCapTrades.setOpaque(true);
        lblCapTrades.setText(Language.getString("INDEX_PANEL_TRADES"));

        lblCashmap.setPreferredSize(new Dimension(74, ROW_1_HEIGHT));
        lblCashmap.setHorizontalAlignment(SwingConstants.CENTER);
        lblCashmap.setOpaque(true);
        lblCashmap.setText(Language.getString("INDEX_PAN_NETCASH"));
        lblCashmap.addMouseListener(this);
        lblCashmap.setCursor(new Cursor(Cursor.HAND_CURSOR));

        lblValueTurnover.setPreferredSize(new Dimension(117, ROW_2_HEIGHT));
        lblValueTurnover.setHorizontalAlignment(SwingConstants.RIGHT);

        lblValueVolume.setPreferredSize(new Dimension(88, ROW_2_HEIGHT));
        lblValueVolume.setHorizontalAlignment(SwingConstants.RIGHT);

        lblValueTrades.setPreferredSize(new Dimension(65, ROW_2_HEIGHT));
        lblValueTrades.setHorizontalAlignment(SwingConstants.RIGHT);

        // lblValueCashMap.setPreferredSize(new Dimension(95, ROW_2_HEIGHT));
        lblValueCashMap.setPreferredSize(new Dimension(74, ROW_2_HEIGHT));
        lblValueCashMap.setHorizontalAlignment(SwingConstants.CENTER);
        lblValueCashMap.setVerticalAlignment(SwingConstants.CENTER);
        lblValueCashMap.setHorizontalTextPosition(SwingConstants.CENTER);
        lblValueCashMap.setVerticalTextPosition(SwingConstants.CENTER);
        imgValueCashmap.setWidth(70);
        lblValueCashMapCommoditiesContainer.setLayout(new CardLayout());

        if (isResolution2048) {
            lblValueCashMapCommoditiesContainer.setPreferredSize(new Dimension(174, ROW_2_HEIGHT));
            lblCashmap.setPreferredSize(new Dimension(174, ROW_1_HEIGHT));
            lblValueCommoditieslast.setPreferredSize(new Dimension(74, ROW_2_HEIGHT));
            lblValueCommoditieslast.setHorizontalAlignment(SwingConstants.CENTER);
            lblValueCommoditieslast.setVerticalAlignment(SwingConstants.CENTER);
            lblValueCommoditieslast.setHorizontalTextPosition(SwingConstants.CENTER);
            lblValueCommoditieslast.setVerticalTextPosition(SwingConstants.CENTER);

            lblValueCommoditiesChange.setPreferredSize(new Dimension(50, ROW_2_HEIGHT));
            lblValueCommoditiesChange.setHorizontalAlignment(SwingConstants.CENTER);
            lblValueCommoditiesChange.setVerticalAlignment(SwingConstants.CENTER);
            lblValueCommoditiesChange.setHorizontalTextPosition(SwingConstants.CENTER);
            lblValueCommoditiesChange.setVerticalTextPosition(SwingConstants.CENTER);

            lblValueCommoditiesPctChange.setPreferredSize(new Dimension(50, ROW_2_HEIGHT));
            lblValueCommoditiesPctChange.setHorizontalAlignment(SwingConstants.CENTER);
            lblValueCommoditiesPctChange.setVerticalAlignment(SwingConstants.CENTER);
            lblValueCommoditiesPctChange.setHorizontalTextPosition(SwingConstants.CENTER);
            lblValueCommoditiesPctChange.setVerticalTextPosition(SwingConstants.CENTER);

            lblValueCommoditiesContainer.setLayout(new BorderLayout());
            lblValueCommoditiesContainer.add(lblValueCommoditieslast, BorderLayout.WEST);
            lblValueCommoditiesContainer.add(lblValueCommoditiesChange, BorderLayout.CENTER);
            lblValueCommoditiesContainer.add(lblValueCommoditiesPctChange, BorderLayout.EAST);

            lblValueCashMapContainer.setLayout(new FlexGridLayout(new String[]{"50%", "70", "50%"}, new String[]{ROW_2_HEIGHT + ""}));
            lblValueCashMapContainer.add(new JLabel());
            lblValueCashMapContainer.add(lblValueCashMap);
            lblValueCashMapContainer.add(new JLabel());

            lblValueCashMapCommoditiesContainer.add(CARD_CASHMAP, lblValueCashMapContainer);
            lblValueCashMapCommoditiesContainer.add(CARD_COMMODITIES, lblValueCommoditiesContainer);


        } else {
            lblValueCashMapCommoditiesContainer.setPreferredSize(new Dimension(74, ROW_2_HEIGHT));

            lblValueCommoditieslast.setPreferredSize(new Dimension(74, ROW_2_HEIGHT));
            lblValueCommoditieslast.setHorizontalAlignment(SwingConstants.CENTER);
            lblValueCommoditieslast.setVerticalAlignment(SwingConstants.CENTER);
            lblValueCommoditieslast.setHorizontalTextPosition(SwingConstants.CENTER);
            lblValueCommoditieslast.setVerticalTextPosition(SwingConstants.CENTER);

            lblValueCommoditiesContainer.setLayout(new BorderLayout());
            //lblValueCommoditiesContainer.add(lblValueCommoditieslast, BorderLayout.WEST);
            lblValueCommoditiesContainer.add(lblValueCommoditieslast, BorderLayout.CENTER);
            // lblValueCommoditiesContainer.add(lblValueCommoditiesPctChange, BorderLayout.EAST);

            lblValueCashMapContainer.setLayout(new FlexGridLayout(new String[]{"50%", "70", "50%"}, new String[]{ROW_2_HEIGHT + ""}));
            lblValueCashMapContainer.add(new JLabel());
            lblValueCashMapContainer.add(lblValueCashMap);
            lblValueCashMapContainer.add(new JLabel());

            lblValueCashMapCommoditiesContainer.add(CARD_CASHMAP, lblValueCashMapContainer);
            lblValueCashMapCommoditiesContainer.add(CARD_COMMODITIES, lblValueCommoditiesContainer);
        }

        imgValueCashmap.setHeight(ROW_2_HEIGHT - 4);
        lblMarketName.setPreferredSize(new Dimension(219, ROW_1_HEIGHT));
        lblMarketName.setHorizontalAlignment(SwingConstants.CENTER);
        lblMarketName.setOpaque(true);
        lblMarketName.addMouseListener(this);
        lblMarketName.setCursor(new Cursor(Cursor.HAND_CURSOR));

        lblMarketTime.setPreferredSize(new Dimension(135, ROW_2_HEIGHT));
        lblMarketTime.setHorizontalAlignment(SwingConstants.TRAILING);
        //  lblMarketTime.setBorder(BorderFactory.createLineBorder(Color.red));

        lblMarketStatus.setPreferredSize(new Dimension(88, ROW_2_HEIGHT));
        lblMarketStatus.setHorizontalAlignment(SwingConstants.LEADING);
        //  lblMarketStatus.setBorder(BorderFactory.createLineBorder(Color.red));


        lblGap1.setPreferredSize(new Dimension(5, ROW_1_HEIGHT));
        lblGap2.setPreferredSize(new Dimension(2, ROW_1_HEIGHT));
        lblGap3.setPreferredSize(new Dimension(0, ROW_2_HEIGHT));
        lblGap4.setPreferredSize(new Dimension(3, ROW_2_HEIGHT));

        if (Language.isLTR()) {
            lblGap5.setPreferredSize(new Dimension(8, ROW_2_HEIGHT));
            lblGap6.setPreferredSize(new Dimension(10, ROW_2_HEIGHT));
        } else {
            lblGap5.setPreferredSize(new Dimension(10, ROW_2_HEIGHT));
            lblGap6.setPreferredSize(new Dimension(2, ROW_2_HEIGHT));

        }
        lblGapRow.setPreferredSize(new Dimension(PANEL_WIDTH, 5));
        if (isResolution2048) {
            this.setPreferredSize(new Dimension(PANEL_WIDTH + 100, ROW_1_HEIGHT + ROW_2_HEIGHT));
        } else {
            this.setPreferredSize(new Dimension(PANEL_WIDTH, ROW_1_HEIGHT + ROW_2_HEIGHT));
        }

        JLabel cashmapgap = new JLabel();
        cashmapgap.setPreferredSize(new Dimension(1, ROW_1_HEIGHT));

        this.add(lblIndexName);

        this.add(lblGap1);
        this.add(lblMarketName);

        this.add(lblGap2);
        this.add(lblCapVolume);
        this.add(lblCapTurnover);
        this.add(lblCapTrades);
        this.add(cashmapgap);
        this.add(lblCashmap);

        this.add(lblIndexValue);
        this.add(lblIndexChange);
        this.add(lblIndexImage);
        this.add(lblIndexPChange);

        this.add(lblGap3);
        this.add(lblMarketTime);
        this.add(lblGap4);
        this.add(lblMarketStatus);

        this.add(lblGap5);
        this.add(lblValueVolume);
        this.add(lblValueTurnover);
        this.add(lblValueTrades);
        if (isEnglish) {
            //   this.add(lblGap6);
        }
        if (Language.isLTR()) {
            JLabel lblgap7 = new JLabel();
            lblgap7.setPreferredSize(new Dimension(2, ROW_2_HEIGHT));
            this.add(lblgap7);
        }

        this.add(lblValueCashMapCommoditiesContainer);

        try {
            upImage = new ImageIcon("images/Common/up.gif");
        } catch (Exception e) {
            upImage = null;
        }

        try {
            downImage = new ImageIcon("images/Common/down.gif");
        } catch (Exception e) {
            downImage = null;
        }
        numberFormat = new TWDecimalFormat("###,##0");
        decimalFormat = new TWDecimalFormat("###,##0.00");

        createIndexPopupMenu(false);

        Theme.registerComponent(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
//        lblIndexName.setText(symbol);
        setOpaque(false);
        applyTheme();

        Application.getInstance().addApplicationListener(this);
        CustomIndexStore.getSharedInstance().addCustomIndexListener(this);
        GUISettings.applyOrientation(this);
        TableUpdateManager.addTable(this);
        CommoditiesStore.getSharedInstance();
        //  CommoditiesStore.getSharedInstance().loadCommoditiesFile();
        // revalidateCashMapPopup(false);

        //addBorders(this);
    }


    public static IndexPanel getInstance() {
        if (self == null) {
            self = new IndexPanel();
        }
        return self;
    }

    /*public void activate() {
        if (thread == null) {
            thread = new Thread(this, "IndexPanel");
            thread.start();
        }
    }*/

    public void setMarketName(String name) {
        lblMarketName.setText(name);
    }


    public void updateMarketTime() {
        try {
            Exchange exchange = ExchangeStore.getSharedInstance().getSelectedExchange();
            long time = 0;
            if (Settings.isConnected()) {
                time = MarketTimer.getSharedInstance().getMarketTime(exchange.getSymbol());
            } else {
                time = exchange.getMarketDateTime();
            }

            marketTime = new Date(exchange.getZoneAdjustedTime(time));

            if (ExchangeStore.getSharedInstance().getSelectedExchange().getMarketStatus() == Constants.CLOSED)
                lblMarketTime.setText(closedMarketTimeFormatter.format(marketTime));
            else
                lblMarketTime.setText(openMarketTimeFormatter.format(marketTime));

            marketTime = null;
            exchange = null;
        } catch (Exception e) {
            lblMarketTime.setText("");
        }

        //optimize color objects
        try {
            switch (ExchangeStore.getSharedInstance().getSelectedExchange().getMarketStatus()) {
                case Constants.OPEN:
                    lblMarketStatus.setText(Language.getString("MKT_STATUS_OPEN"));
                    lblMarketStatus.setForeground(openColor);
                    break;
                case Constants.PREOPEN:
                    lblMarketStatus.setText(Language.getString("MKT_STATUS_PRE_OPEN"));
                    lblMarketStatus.setForeground(preopenColor);
                    break;
                case Constants.PRECLOSED:
                    lblMarketStatus.setText(Language.getString("MKT_STATUS_PRE_CLOSE"));
                    lblMarketStatus.setForeground(preClosedColor);
                    break;
                case Constants.PRECLOSE_WITH_TRADES:
                    lblMarketStatus.setText(Language.getString("MKT_STATUS_PRE_CLOSE"));
                    lblMarketStatus.setForeground(preClosedColor);
                    break;
                case Constants.CLOSE_WITH_TRADES:
                    lblMarketStatus.setText(Language.getString("MKT_STATUS_PRE_CLOSE"));
                    lblMarketStatus.setForeground(preClosedColor);
                    break;
                case Constants.TRADING_AT_LAST:
                    lblMarketStatus.setText(Language.getString("MKT_STATUS_TRADE_AT_LAST"));
                    lblMarketStatus.setForeground(preClosedColor);
                    break;
                case Constants.CLOSED:
                    lblMarketStatus.setText(Language.getString("MKT_STATUS_CLOSE"));
                    lblMarketStatus.setForeground(closedColor);
                    break;
                case Constants.CLOSING_AUCTION:
                    lblMarketStatus.setText(Language.getString("MKT_CLOSING_AUCTION"));
                    lblMarketStatus.setForeground(preopenColor);
                    break;
                case Constants.CLOSING_AUCTION_NEW:
                    lblMarketStatus.setText(Language.getString("MKT_CLOSING_AUCTION"));
                    lblMarketStatus.setForeground(preopenColor);
                    break;
                default:
                    lblMarketStatus.setText("");
            }
        } catch (Exception e) {
            lblMarketStatus.setText("");
        }

    }

    private synchronized void createIndexPopupMenu(boolean selectMainIndex) {

        boolean hasElements = false;

        if (indices == null)
            indices = new JPopupMenu();
        indices.setLightWeightPopupEnabled(false);
        String selectedExchange = null;
        Enumeration<String> sectors = null;


        indices.removeAll();
        removeSymbolRequests();
        try {
            selectedExchange = ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol();
            if (!ExchangeStore.getSharedInstance().getSelectedExchange().isDefault()) {
                NonDefaultIndexList list = loadIndexesForNonDefault(selectedExchange);
                Iterator iterator = list.getEnabledIndexes().iterator();
                while (iterator.hasNext()) {
                    StringKeys index = ((StringKeys) iterator.next());
                    String selectedsymbol = index.getKey();

                    TWMenuItem item;
                    //Bug Id <#????> 8.20.009 / 29 June 2006
                    // Added this segment to check for empty descriptions
                    if ((index.toString() == null) || (index.toString().equals(Language.getString("NA")))) {
                        item = new TWMenuItem(selectedsymbol);
                    } else {
                        item = new TWMenuItem(index.toString());
                    }
                    //  item.setActionCommand("I" + selectedExchange + Constants.KEY_SEPERATOR_CHARACTER + selectedsymbol + Constants.KEY_SEPERATOR_CHARACTER + Meta.INSTRUMENT_INDEX);
                    item.setActionCommand("I" + selectedsymbol);
                    item.addActionListener(this);

                    indices.add(item);
                    hasElements = true;
                    selectedsymbol = null;
                    item = null;
                }
                String sKey = list.getMainIndex().getKey();

                addSymbolRequest(sKey);
                setIndex(DataStore.getSharedInstance().getCompanyName(sKey));
                StretchedIndexPanel.getInstance().setIndex(DataStore.getSharedInstance().getCompanyName(sKey));
                if (selectMainIndex) {
                    this.symbol = SharedMethods.getSymbolFromKey(sKey);
                    this.key = sKey;
                    isCustomindex = false;
                    StretchedIndexPanel.isCustomindex = false;
                    StretchedIndexPanel.symbol = symbol;
                    StretchedIndexPanel.key = this.key;
                    setIndex(DataStore.getSharedInstance().getCompanyName(key));

                }
            } else {
                ArrayList<StringKeys> symbolList;//= new ArrayList<StringKeys>();
                /*sectors = DataStore.getSharedInstance().getFilteredList(selectedExchange, Meta.INSTRUMENT_INDEX);
                String selectedsymbol1 = "";
                while (sectors.hasMoreElements()) {
                    selectedsymbol1 = sectors.nextElement();
                    Stock sector1 = DataStore.getSharedInstance().getStockObject(selectedExchange, selectedsymbol1, Meta.INSTRUMENT_INDEX);
                    StringKeys selectedsymbol = new StringKeys((String) sectors.nextElement(), sector1.getLongDescription());
//                    symbolList.add(selectedsymbol);
                    sector1 = null;
                }*/
                comIndexList.clear();
                symbolList = getIndiciesForExchange(selectedExchange);
                if (SharedMethods.indexSortingOrderByDefault(selectedExchange)) {
                    Collections.sort(symbolList);
                }
                Iterator iterator = symbolList.iterator();
                while (iterator.hasNext()) {
                    try {
                        String selectedsymbol = ((StringKeys) iterator.next()).getKey();
                        Stock sector = DataStore.getSharedInstance().getStockObject(selectedExchange, selectedsymbol, Meta.INSTRUMENT_INDEX);

                        TWMenuItem item;
                        //Bug Id <#????> 8.20.009 / 29 June 2006
                        // Added this segment to check for empty descriptions
                        if ((sector == null) || (sector.getLongDescription() == null) || (sector.getLongDescription().equals(Language.getString("NA")))) {
                            item = new TWMenuItem(selectedsymbol);
                        } else {
                            item = new TWMenuItem(sector.getLongDescription());
                        }
                        item.setActionCommand("I" + selectedExchange + Constants.KEY_SEPERATOR_CHARACTER + sector.getSymbol() + Constants.KEY_SEPERATOR_CHARACTER + Meta.INSTRUMENT_INDEX);
                        item.addActionListener(this);
                        if (isMainIndex(selectedExchange, selectedsymbol)) { // check for main index Bug id <#0037> 8.20.009 / 29 June 2006 added null checking
                            //                    if ((sector.getParams() != null) && (sector.getParams().indexOf("M") >= 0)) { // check for main index Bug id <#0037> 8.20.009 / 29 June 2006 added null checking
                            String symbol = selectedsymbol;
                            String exchange = ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol();

                            setIndex(DataStore.getSharedInstance().getCompanyName(SharedMethods.getKey(exchange, symbol, Meta.INSTRUMENT_INDEX)));
                            StretchedIndexPanel.getInstance().setIndex(DataStore.getSharedInstance().getCompanyName(SharedMethods.getKey(exchange, symbol, Meta.INSTRUMENT_INDEX)));
                            if (selectMainIndex) {
                                this.symbol = sector.getSymbol();
                                this.key = SharedMethods.getKey(selectedExchange, sector.getSymbol(), Meta.INSTRUMENT_INDEX);
                                isCustomindex = false;
                                StretchedIndexPanel.isCustomindex = false;
                                StretchedIndexPanel.symbol = symbol;
                                StretchedIndexPanel.key = this.key;
                                setIndex(DataStore.getSharedInstance().getCompanyName(key));

                            }
                            symbol = null;
                            exchange = null;
                        }
                        indices.add(item);
                        hasElements = true;
                        selectedsymbol = null;
                        item = null;
                        sector = null;
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }

            sectors = null;
            Enumeration<String> names = CustomIndexStore.getSharedInstance().getColumnNames();
            while (names.hasMoreElements()) {
                TWMenuItem item;
                CustomIndexInterface object = CustomIndexStore.getSharedInstance().getCustomIndexObject(names.nextElement());
                if (object.getExchanges().equals(selectedExchange)) {
                    if (object.getDescription() != null && !object.getDescription().equals("")) {
                        item = new TWMenuItem(object.getDescription());
                        setIndex(object.getDescription());
                    } else {
                        item = new TWMenuItem(object.getSymbol());
                        setIndex(object.getSymbol());
                    }
                    item.setActionCommand("CUSTOM_INDEX" + Constants.KEY_SEPERATOR_CHARACTER + object.getSymbol());
                    item.addActionListener(this);
                    indices.add(item);
                    hasElements = true;
                }
            }
            selectedExchange = null;
        } catch (Exception e) {
            e.printStackTrace();
        }

        SwingUtilities.updateComponentTreeUI(indices);
        GUISettings.applyOrientation(indices);

        if (symbol == null || (!hasElements && symbol.trim().isEmpty())) {
            symbol = "*";
        }

    }

    public ArrayList<StringKeys> getIndiciesForExchange(String exchange) {
        ArrayList<StringKeys> symbolList = new ArrayList<StringKeys>();
        try {
            SmartProperties exgIndexes = new SmartProperties(Meta.DS, true);
//            Vector keyVector = new Vector();
//            String key = null;
//            String value = null;
            String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/index_" + Language.getSelectedLanguage() + ".msf";
            exgIndexes.loadCompressed(sLangSpecFileName);

            //Searching indexes
            Enumeration indexEnum = exgIndexes.keys();
            while (indexEnum.hasMoreElements()) {
                String skey = (String) indexEnum.nextElement();
                String name = skey;
                ////////////////////////////////////////////////////////////////
                String data = (String) exgIndexes.get(skey);
                try {
                    String[] field = data.split(Meta.FS);
                    name = field[2];

                } catch (Exception e) {
                    name = skey;

                }

                ////////////////////////////////////////////////////////////////
                StringKeys selectedsymbol = new StringKeys(skey, name);
                symbolList.add(selectedsymbol);
            }

            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_COM_INDEX)) {
                try {
                    sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/cust_index_" + Language.getSelectedLanguage() + ".msf";
                    exgIndexes = new SmartProperties(Meta.DS);
                    File f = new File(sLangSpecFileName);
                    boolean bIsLangSpec = f.exists();
                    if (bIsLangSpec) {
                        exgIndexes.loadCompressed(sLangSpecFileName);
                        indexEnum = exgIndexes.keys();
                        while (indexEnum.hasMoreElements()) {
                            try {
                                String skey = (String) indexEnum.nextElement();
                                String data = (String) exgIndexes.get(skey);
                                if (data.split(Meta.RS)[1].equals(Settings.getInstitutionCode())) {
                                    String[] field = data.split(Meta.FS);
                                    String dataRec = field[5] + Meta.ID + exchange + Meta.ID + field[0] + Meta.ID + field[5] + Meta.ID + field[2] + Meta.ID + field[3] + Meta.ID + field[4] + Meta.ID + "" + Meta.ID + field[5];
                                    String keyR = SharedMethods.getKey(exchange, skey, Meta.INSTRUMENT_INDEX);
                                    StringKeys index = new StringKeys(skey, field[2]);
                                    symbolList.add(index);
                                    comIndexList.add(keyR);
                                    DataStore.getSharedInstance().createStockObject(exchange, skey, Meta.INSTRUMENT_INDEX);
                                    ValidatedSymbols.getSharedInstance().addSymbol(keyR, dataRec);
                                    //                                addSymbolRequest(keyR);
                                }
                            } catch (Exception e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return symbolList;
    }

    private boolean isMainIndex(String exchange, String symbol) {
        boolean isMainIndex = false;
        try {
            SmartProperties exgIndexes = new SmartProperties(Meta.DS);
            String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/index_" + Language.getSelectedLanguage() + ".msf";
            exgIndexes.loadCompressed(sLangSpecFileName);
            String data = (String) exgIndexes.get(symbol);
            try {
                String[] field = data.split(Meta.FS);
                if (field[1].equals("IM")) { //--To Solve the conflict in MSM -Shanika
                    isMainIndex = true;
                }
                /*if (Integer.parseInt(data.split(Meta.RS)[1]) == 1) {
//                    String[] field = data.split(Meta.FS);
                    String dataRec = field[5] + Meta.ID + exchange + Meta.ID + field[0] + Meta.ID + field[5] + Meta.ID + field[2] + Meta.ID + field[3] + Meta.ID + field[4] + Meta.ID + "" + Meta.ID + field[5];
                    String keyR = SharedMethods.getKey(exchange, symbol, Meta.INSTRUMENT_INDEX);
                    StringKeys index = new StringKeys(keyR, field[2]);
                    //todo check for duplicates

                    if (field[1].equals("IM")) { //--To Solve the conflict in MSM -Shanika
                        isMainIndex = true;
                    }

                } else {
                    //do nothing
                }*/
            } catch (Exception e) {
                e.printStackTrace();

            }


        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        return isMainIndex;
    }

    public static void setIndexID(String id) {
        workspaceKey = id;
        key = id;
        if (key.startsWith("CUSTOM")) {
            isCustomindex = true;
            StretchedIndexPanel.isCustomindex = true;
        }
        symbol = SharedMethods.getSymbolFromKey(key);
        savedKey = id;
        savedSymbol = SharedMethods.getSymbolFromKey(key);
    }

    public static String getIndexID() {
        return key;
    }

    public void applyTheme() {
        valueFont = new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.BOLD, 12);
        valueFontNew = new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.BOLD, 10);
        if (Language.isLTR()) {
            boldFnt = new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.BOLD, 12);
            indexIDFnt = new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.BOLD, 12);
        } else {
            boldFnt = new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.PLAIN, 12);
            indexIDFnt = new TWFont(Theme.getFontName("DEFAULT_FONT"), Font.PLAIN, 12);
        }

        lblMarketStatus.setFont(valueFontNew);
        lblValueTurnover.setFont(valueFont);
        lblValueVolume.setFont(valueFont);
        lblValueTrades.setFont(valueFont);
        lblValueCashMap.setFont(valueFont);
        lblMarketTime.setFont(valueFontNew);

        lblValueCommoditieslast.setFont(valueFont);
        lblValueCommoditiesChange.setFont(valueFont);
        lblValueCommoditiesPctChange.setFont(valueFont);

        lblIndexName.setFont(indexIDFnt);
        lblIndexValue.setFont(boldFnt);
        lblIndexChange.setFont(boldFnt);
        lblIndexPChange.setFont(boldFnt);
        lblCapTurnover.setFont(boldFnt);
        lblCapVolume.setFont(boldFnt);
        lblCapTrades.setFont(boldFnt);
        lblCashmap.setFont(boldFnt);
        lblMarketName.setFont(boldFnt);

        Color captionBGColor = Theme.getColor("MARKET_TITLE_BGCOLOR");
        Color captionFGColor = Theme.getColor("MARKET_TITLE_FGCOLOR");
        upColor = Theme.getColor("TOOLBAR_UP_FGCOLOR");
        downColor = Theme.getColor("TOOLBAR_DOWN_FGCOLOR");
        normalColor = Theme.getColor("TOOLBAR_FGCOLOR");
        upColorCmap = Theme.getColor("TOOLBAR_UP_FGCOLOR_CASHFLOW");
        downColorCmap = Theme.getColor("TOOLBAR_DOWN_FGCOLOR_CASHFLOW");
        normalColorCmap = Theme.getColor("TOOLBAR_FGCOLOR_CASHFLOW");
        cashMapFgClor = Theme.getColor("INDEXPAN_CASH_MAP_FG");
        openColor = Theme.getColor("MARKET_STATUS_OPEN_COLOR");
        preopenColor = Theme.getColor("MARKET_STATUS_PREOPEN_COLOR");
        closedColor = Theme.getColor("MARKET_STATUS_CLOSED_COLOR");
        preClosedColor = Theme.getColor("MARKET_STATUS_PRECLOSED_COLOR");
        Border captionBorder = BorderFactory.createLineBorder(Theme.getColor("MARKET_TITLE_BORDER_COLOR"), 1);

        lblMarketTime.setForeground(normalColor);
        lblIndexValue.setForeground(normalColor);
        lblValueTurnover.setForeground(normalColor);
        lblValueVolume.setForeground(normalColor);
        lblValueTrades.setForeground(normalColor);
        lblValueCashMap.setForeground(cashMapFgClor);

        lblIndexName.setBackground(captionBGColor);
        lblIndexName.setForeground(captionFGColor);
        lblIndexName.setBorder(captionBorder);
        lblCapTurnover.setBackground(captionBGColor);
        lblCapTurnover.setForeground(captionFGColor);
        lblCapTurnover.setBorder(captionBorder);
        lblCapVolume.setBorder(captionBorder);
        lblCapVolume.setBackground(captionBGColor);
        lblCapVolume.setForeground(captionFGColor);
        lblCapTrades.setBorder(captionBorder);
        lblCapTrades.setBackground(captionBGColor);
        lblCapTrades.setForeground(captionFGColor);
        lblCashmap.setBorder(captionBorder);
        lblCashmap.setBackground(captionBGColor);
        lblCashmap.setForeground(captionFGColor);
        lblMarketName.setBackground(captionBGColor);
        lblMarketName.setForeground(captionFGColor);
        lblMarketName.setBorder(captionBorder);
        try {
            imageicon = null;
            imageGraphBG = null;
            imageicon = new ImageIcon(Theme.getTheamedImagePath("indextitletile"));
            imageiconL = new ImageIcon(Theme.getTheamedImagePath("indextitletileL"));
            imageiconR = new ImageIcon(Theme.getTheamedImagePath("indextitletileR"));

            imageiconup = new ImageIcon(Theme.getTheamedImagePath("top-md"));
            imageiconLup = new ImageIcon(Theme.getTheamedImagePath("top-lf"));
            imageiconRup = new ImageIcon(Theme.getTheamedImagePath("top-rg"));

            imageicondown = new ImageIcon(Theme.getTheamedImagePath("bot-md"));
            imageiconLdown = new ImageIcon(Theme.getTheamedImagePath("bot-lf"));
            imageiconRdown = new ImageIcon(Theme.getTheamedImagePath("bot-rg"));

            imageGraphBG = new ImageIcon(Theme.getTheamedImagePath("indexGraphBGMain"));
            imagewidth = imageicon.getIconWidth();
            imagewidthup = imageiconup.getIconWidth();
            imagewidthdown = imageicondown.getIconWidth();
        } catch (Exception e) {
            imageicon = null;
            imagewidth = 0;
        }

        indexGraphFGColor = Theme.getColor("INDEXGRAPH_FG");
        cashMapBorder.setColor(Theme.getColor("INDEXPAN_CASHMAP_BORDER"));
    }

    public Dimension getPreferredSize() {
        if (isResolution2048) {
            return new Dimension(PANEL_WIDTH + 100, ROW_1_HEIGHT + ROW_2_HEIGHT);
        } else {
            return new Dimension(PANEL_WIDTH, ROW_1_HEIGHT + ROW_2_HEIGHT);

        }
    }

    /*  public void run() {


        if (System.getProperty("DisableIndexPanel") != null) {
            return;
        }

        updateGUI();
        while (true) {
            try {
                updateGUI();
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(1000);
            } catch (Exception e) {
                // e.printStackTrace();
            }
            repaint();
        }
    }*/

    private synchronized void updateGUI() {

        try {
            if (ExchangeStore.getSelectedExchangeID() != null) {
                if (!ExchangeStore.getSharedInstance().isValidExchange(ExchangeStore.getSelectedExchangeID())) { // oops login changed. need to reset exchanges
                    ExchangeStore.setSelectedExchangeID(null);
                    prevMarket = null;
                    return;
                }
                if (prevMarket == null) { // first time. market not yet selected
                    createIndexPopupMenu(false);
                } else if (!prevMarket.equals(ExchangeStore.getSelectedExchangeID())) {
                    createIndexPopupMenu(true);
                }

                prevMarket = ExchangeStore.getSelectedExchangeID();

                // check
                lblMarketName.setText(ExchangeStore.getSharedInstance().getSelectedExchange().getDescription());
            }

            Exchange exchangeObject = ExchangeStore.getSharedInstance().getSelectedExchange();
            if (exchangeObject == null) {
                exchangeObject = ExchangeStore.getSharedInstance().getDefaultExchange();
                if (exchangeObject != null) {
                    ExchangeStore.setSelectedExchangeID(exchangeObject.getSymbol());
                    lblMarketName.setText(ExchangeStore.getSharedInstance().getSelectedExchange().getDescription());
                    createIndexPopupMenu(true);
                    updateMarketTime();
                }
            }
            if (exchangeObject != null) {
                lblValueTurnover.setText(formatNumber(exchangeObject.getTurnover()));
                lblValueVolume.setText(formatNumber(exchangeObject.getVolume()));
                lblValueTrades.setText(formatNumber(exchangeObject.getNoOfTrades()));
                StretchedIndexPanel.getInstance().setVolumn(exchangeObject.getTurnover());
                StretchedIndexPanel.getInstance().setNoofTrades(exchangeObject.getNoOfTrades());
                if (cashmapMode == CASH_MAP && (!isGraphMode())) {

                    double value = exchangeObject.getCashMapPrecentage();
                    imgValueCashmap.setValue(value);
                    if (!Double.isNaN(value)) {
                        lblValueCashMap.setForeground(cashMapFgClor);
                        lblValueCashMap.setText(formatDecimal(value * 100));
                    } else {
                        lblValueCashMap.setText(Language.getString("NA"));
                        lblValueCashMap.setForeground(normalColorCmap);
                    }


                } else if (cashmapMode == CASH_MAP_PCT && (!isGraphMode())) {
                    double value = exchangeObject.getNetCashMapPercentage();

                    if (Double.isNaN(value)) {
                        lblValueCashMap.setText(Language.getString("NA"));
                        lblValueCashMap.setForeground(normalColorCmap);
                    } else {
                        lblValueCashMap.setText(formatDecimal(value) + "%");
                        setFontColorCashMap(lblValueCashMap, value);
                    }
                } else if (cashmapMode == COMMODITY && (!isGraphMode())) {
                    Stock stk = DataStore.getSharedInstance().getStockObject(previousCommoditySymbol);
                    double value = 0;
                    double change = 0;
                    double pctChange = 0;
                    if (stk != null) {
                        value = DataStore.getSharedInstance().getStockObject(previousCommoditySymbol).getLastTradeValue();
                        change = DataStore.getSharedInstance().getStockObject(previousCommoditySymbol).getChange();
                        pctChange = DataStore.getSharedInstance().getStockObject(previousCommoditySymbol).getPercentChange();
                    }

                    if (Double.isNaN(value)) {
                        lblValueCashMap.setText(Language.getString("NA"));
                        lblValueCashMap.setForeground(normalColor);
                    } else {
                        lblValueCommoditieslast.setText(formatDecimal(value));
                        lblValueCommoditiesChange.setText(formatDecimal(change));
                        lblValueCommoditiesPctChange.setText(formatDecimal(pctChange) + "%");
                        lblValueCommoditieslast.setForeground(normalColorCmap);
                        //setFontColorCashMap(lblValueCommoditieslast, value);
                        setFontColorCashMap(lblValueCommoditiesChange, change);
                        setFontColorCashMap(lblValueCommoditiesPctChange, pctChange);
                    }
                }
            }
            exchangeObject = null;

        } catch (Exception e) {
            e.printStackTrace();

        }

        try {
            if (!isCustomindex) {
                Stock index = DataStore.getSharedInstance().getStockObject(ExchangeStore.getSelectedExchangeID(), symbol, Meta.INSTRUMENT_INDEX);
                boolean isEnabled = ExchangeStore.isValidSystemFinformationType(Meta.IT_TASI_FORCAST);
                if (index != null) { // may fail while loading the applicatiion
                    if (index.isPredictedIndex() && isEnabled) {
                        lblIndexValue.setText(formatDecimal(index.getPredictedIndex()));
                        lblIndexChange.setText(formatDecimal(index.getPredictedIndexChange()));
                        lblIndexPChange.setText(formatDecimal(index.getPredictedIndexPctChange()) + "%");
                        setFontColor(lblIndexChange, index.getPredictedIndexChange());
                        setFontColor(lblIndexPChange, index.getPredictedIndexPctChange());

                    } else {
                        lblIndexValue.setText(formatDecimal(index.getLastTradeValue()));
                        lblIndexChange.setText(formatDecimal(index.getChange()));
                        lblIndexPChange.setText(formatDecimal(index.getPercentChange()) + "%");
                        setFontColor(lblIndexChange, index.getChange());
                        setFontColor(lblIndexPChange, index.getPercentChange());
                    }
                    lblIndexImage.setIcon(formatIcon(index.getPercentChange()));
                    if ((index.getLongDescription() == null) || (index.getLongDescription().equals(Language.getString("NA")))) {
                        lblIndexName.setText(index.getSymbol());
                    } else {
                        lblIndexName.setText(index.getLongDescription());
                    }
                    if (index.isPredictedIndex() && isEnabled) {
                        lblIndexName.setForeground2(Theme.INDEXPANEL_FORECAST_INDEX_COLOR);
                        lblIndexName.setText2(Language.getString("INDEX_PAN_PREDECTIED"));
                    } else {
                        lblIndexName.setText2("");
                    }
                } else if ((symbol != null) && (symbol.equals("*"))) {
                    Exchange exchangeObject = ExchangeStore.getSharedInstance().getSelectedExchange();
                    if (exchangeObject != null) {
                        try {
                            //  symbol =SharedMethods.getSymbolFromKey(exchangeObject.getMainIndexForExchange());
                            symbol = exchangeObject.getMainIndexForExchange();
                            key = SharedMethods.getKey(exchangeObject.getSymbol(), exchangeObject.getMainIndexForExchange(), Meta.INSTRUMENT_INDEX);
                        } catch (Exception e) {
                            symbol = "*";
                            key = "";
                        }
                    }
                    setIndex("");
                    lblIndexValue.setText("");
                    lblIndexChange.setText("");
                    lblIndexPChange.setText("");
                    lblIndexName.setText2("");
                    lblIndexImage.setIcon(null);
                    // key = "";
                } else if ((prevKey == null) || (prevKey.equals("null")) || (!prevKey.equals(key))) {
                    String descr = DataStore.getSharedInstance().getCompanyName(key);
                    if (descr.equals("")) { // no index selected.
                        createIndexPopupMenu(true);
                    } else {
                        setIndex(descr);
                    }
                    prevKey = key;
                    descr = null;
                } else {
                    lblIndexName.setText("");
                    lblIndexValue.setText("");
                    lblIndexChange.setText("");
                    lblIndexPChange.setText("");
                    lblIndexName.setText2("");
                    key = "";
                    lblIndexImage.setIcon(null);
                }
                index = null;
                if (((symbol != null) && (!symbol.equals(""))) && (!symbol.equals("*"))) {

                } else {
                    Enumeration<String> sectors = null;
                    sectors = DataStore.getSharedInstance().getFilteredList(ExchangeStore.getSelectedExchangeID(), Meta.INSTRUMENT_INDEX);
                    String selectedsymbol1 = "";
                    while (sectors.hasMoreElements()) {
                        String sSymbol = sectors.nextElement();
                        Stock indexTemp = DataStore.getSharedInstance().getStockObject(ExchangeStore.getSelectedExchangeID(), sSymbol, Meta.INSTRUMENT_INDEX);
                        if ((indexTemp.getParams() != null) && (indexTemp.getParams().indexOf("M") >= 0)) {
                            symbol = sSymbol;
                            StretchedIndexPanel.setIndexID(SharedMethods.getKey(ExchangeStore.getSelectedExchangeID(), symbol, Meta.INSTRUMENT_INDEX));
                            SharedMethods.printLine("Setting retail" + symbol, true);
                        }
//                        indexTemp.
                    }
                }


            } else {
                Stock index = DataStore.getSharedInstance().getStockofCustomIndex(symbol, Meta.INSTRUMENT_INDEX);
                if (index != null) {
                    lblIndexValue.setText(formatDecimal(index.getLastTradeValue()));
                    lblIndexChange.setText(formatDecimal(index.getChange()));
                    setFontColor(lblIndexChange, index.getChange());
                    lblIndexPChange.setText(formatDecimal(index.getPercentChange()) + "%");
                    setFontColor(lblIndexPChange, index.getPercentChange());
                    lblIndexImage.setIcon(formatIcon(index.getPercentChange()));
                    if ((index.getLongDescription() == null) || (index.getLongDescription().equals(Language.getString("NA")))) {
                        lblIndexName.setText(index.getSymbol());
                    } else {
                        lblIndexName.setText(index.getLongDescription());
                    }
                } else if ((prevKey == null) || (!prevKey.equals(key))) {
                    String descr = DataStore.getSharedInstance().getStockofCustomIndex(symbol, Meta.INSTRUMENT_INDEX).getLongDescription();
                    if (descr.equals("")) { // no index selected.
                        createIndexPopupMenu(false);
                    } else {
                        setIndex(descr);
                    }
                    prevKey = key;
                    descr = null;
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();
        }
        StretchedIndexPanel.getInstance().adjustLabelWiths();
        updateMarketTime();
        repaint();
    }

    /**
     * private void selectDefaultIndex() {
     * //look for the market index of the selected market
     * <p/>
     * String selectedMarket = ExchangeStore.getSelectedExchangeID();
     * if (selectedMarket != null) {
     * Enumeration sectors = DataStore.getSharedInstance().
     * getFilteredList(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(),
     * Meta.INDEX);
     * while (sectors.hasMoreElements()) {
     * String tsymbol = (String) sectors.nextElement();
     * String texchange = ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol();
     * Stock index = DataStore.getSharedInstance().getStockObject(texchange, tsymbol);
     * if (index.getParams().contains("M")) { // check if main index
     * setIndex(DataStore.getSharedInstance().getCompanyName(texchange +
     * Constants.KEY_SEPERATOR_CHARACTER + tsymbol));
     * key = texchange + Constants.KEY_SEPERATOR_CHARACTER + tsymbol;
     * break;
     * }
     * tsymbol = null;
     * texchange = null;
     * index = null;
     * }
     * sectors = null;
     * }
     * selectedMarket = null;
     * }
     */
    public void refresh() {
        updateGUI();
    }

    private void setFontColor(JLabel label, double value) {
        if (value > 0)
            label.setForeground(upColor);
        else if (value < 0)
            label.setForeground(downColor);
        else
            label.setForeground(normalColor);
    }

    private void setFontColorCashMap(JLabel label, double value) {
        if (value > 0)
            label.setForeground(upColorCmap);
        else if (value < 0)
            label.setForeground(downColorCmap);
        else
            label.setForeground(normalColorCmap);
    }


    private String formatDecimal(double value) {
        try {
            return decimalFormat.format(value);
        } catch (Exception e) {
            e.printStackTrace();
            return "0.00";
        }
    }

    private String formatNumber(double value) {
        try {
            return numberFormat.format(value);
        } catch (Exception e) {
            e.printStackTrace();
            return "0";
        }
    }

    private ImageIcon formatIcon(double value) {
        try {
            if (value > 0)
                return upImage;
            else if (value < 0)
                return downImage;
            else
                return null;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }


    /**
     * Invoked when the mouse button has been clicked (pressed
     * and released) on a component.
     */
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e) && e.getSource() != lblCashmap) {
            if (e.getSource() == lblMarketName) {
                createMarketPopup();
                marketPopup.show(lblMarketName, e.getX(), e.getY());
            } else if (Language.isLTR()) {
                GUISettings.showPopup(indices, this, 0, lblIndexChange.getY());
                SwingUtilities.updateComponentTreeUI(indices);
            } else {
                GUISettings.showPopup(indices, this, this.getWidth() -
                        (int) indices.getPreferredSize().getWidth(), lblIndexChange.getY());
                SwingUtilities.updateComponentTreeUI(indices);
            }
        } else {
            if ((e.getSource() == lblIndexName) && ((key != null) && (!key.equalsIgnoreCase("null")) && (!key.trim().isEmpty()))) {
                if ((symbol != null) && (!symbol.equals("*"))) {
                    Client.getInstance().showChart(SharedMethods.getKey(ExchangeStore.getSelectedExchangeID(), symbol, Meta.INSTRUMENT_INDEX), null, null);
                } else {
//                    Client.getInstance().showChart(null, null, null);
                }
            } else if (e.getSource() == lblCashmap && !isGraphMode()) {
                // if(Language.isLTR()){
                GUISettings.showPopup(cashMapPopup, this, lblValueCashMapCommoditiesContainer.getX(), lblValueCashMapCommoditiesContainer.getY());
                //   }
                SwingUtilities.updateComponentTreeUI(cashMapPopup);
            }
        }
    }

    private void createMarketPopup() {
        ArrayList<TWMenuItem> items = new ArrayList<TWMenuItem>();
        marketPopup = null;
        marketPopup = new JPopupMenu();
        marketPopup.setLightWeightPopupEnabled(false);
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.getEnabledForIndexPanel() == 1) {
                if (!isMarketItemAlreadyAdded(exchange.getSymbol()) && ExchangeStore.getSharedInstance().ismarketSummaryAvailable(exchange.getSymbol())) {
                    TWMenuItem item = new TWMenuItem(exchange.getDescription());
                    item.setActionCommand("M" + exchange.getSymbol());
                    item.addActionListener(this);
                    //marketPopup.add(item);
                    items.add(item);
                }
            } else if (exchange.getEnabledForIndexPanel() == -1 && exchange.isDefault()) {
                if (!isMarketItemAlreadyAdded(exchange.getSymbol()) && ExchangeStore.getSharedInstance().ismarketSummaryAvailable(exchange.getSymbol())) {
                    TWMenuItem item = new TWMenuItem(exchange.getDescription());
                    item.setActionCommand("M" + exchange.getSymbol());
                    item.addActionListener(this);
                    //marketPopup.add(item);
                    items.add(item);
                }
            }

            //  if (exchange.isDefault()) {
//                if (!isMarketItemAlreadyAdded(exchange.getSymbol())) {
//                    TWMenuItem item = new TWMenuItem(exchange.getDescription());
//                    item.setActionCommand("M" + exchange.getSymbol());
//                    item.addActionListener(this);
//                    //marketPopup.add(item);
//                    items.add(item);
//                }
            //  }
            exchange = null;
            exchange = null;
        }

        Collections.sort(items);

        for (TWMenuItem item : items) {
            marketPopup.add(item);
        }

        GUISettings.applyOrientation(marketPopup);
        exchanges = null;
    }

    private boolean isMarketItemAlreadyAdded(String exchange) {
        Component[] items = marketPopup.getComponents();
        for (int i = 0; i < items.length; i++) {
            if (((TWMenuItem) items[i]).getActionCommand().equals(exchange)) {
                return true;
            }
        }
        return false;
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     */
    public void mousePressed(MouseEvent e) {

    }

    /**
     * Invoked when a mouse button has been released on a component.
     */
    public void mouseReleased(MouseEvent e) {

    }

    /**
     * Invoked when the mouse enters a component.
     */
    public void mouseEntered(MouseEvent e) {
        if (e.getSource() == lblIndexName)
            lblIndexName.setBorder(BorderFactory.createLineBorder(Theme.getColor("MARKET_TITLE_SELECTED_BORDER_COLOR"), 1));
        if (e.getSource() == lblMarketName)
            lblMarketName.setBorder(BorderFactory.createLineBorder(Theme.getColor("MARKET_TITLE_SELECTED_BORDER_COLOR"), 1));
    }

    /**
     * Invoked when the mouse exits a component.
     */
    public void mouseExited(MouseEvent e) {
        if (e.getSource() == lblIndexName)
            lblIndexName.setBorder(BorderFactory.createLineBorder(Theme.getColor("MARKET_TITLE_BORDER_COLOR"), 1));
        if (e.getSource() == lblMarketName)
            lblMarketName.setBorder(BorderFactory.createLineBorder(Theme.getColor("MARKET_TITLE_BORDER_COLOR"), 1));
    }

    /**
     * Invoked when an action occurs.
     */
    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().startsWith("I")) { // index
            removeSymbolRequests();
            isCustomindex = false;
            StretchedIndexPanel.isCustomindex = false;
            key = e.getActionCommand().substring(1);
            symbol = SharedMethods.getSymbolFromKey(key);
            addSymbolRequest(key);
            StretchedIndexPanel.key = e.getActionCommand().substring(1);
            StretchedIndexPanel.symbol = SharedMethods.getSymbolFromKey(key);
//            exchange = SharedMethods.getExchangeFromKey(key);
            setIndex(DataStore.getSharedInstance().getCompanyName(key));
            StretchedIndexPanel.getInstance().setIndex(DataStore.getSharedInstance().getCompanyName(key));

        } else if (e.getActionCommand().startsWith("CUSTOM_INDEX")) { // Custom index
            isCustomindex = true;
            StretchedIndexPanel.isCustomindex = true;
            key = e.getActionCommand();
            symbol = SharedMethods.getSymbolFromKey(key);
            StretchedIndexPanel.key = key;
            StretchedIndexPanel.symbol = symbol;
//            exchange = SharedMethods.getExchangeFromKey(key);
            setIndex(DataStore.getSharedInstance().getStockofCustomIndex(symbol, Meta.INSTRUMENT_INDEX).getLongDescription());
            StretchedIndexPanel.getInstance().setIndex(DataStore.getSharedInstance().getStockofCustomIndex(symbol, Meta.INSTRUMENT_INDEX).getLongDescription());

        } else if (e.getSource().equals(menuCashMap) || e.getSource().equals(menuNetCashMap)) {
            if (menuCashMap.isSelected()) {
                lblValueCashMap.setIcon(imgValueCashmap);
                cashmapMode = CASH_MAP;
                lblCashmap.setText(Language.getString("INDEX_PAN_PCTCASH"));
                lblCashmap.setToolTipText(Language.getString("INDEX_PAN_CASHMAP_TOOLTIP"));
            } else {
                lblValueCashMap.setIcon(null);
                cashmapMode = CASH_MAP_PCT;
                lblCashmap.setText(Language.getString("INDEX_PAN_NETCASH"));
                lblCashmap.setToolTipText(Language.getString("INDEX_PAN_NETCASH_TOOLTIP"));

            }
            if (previousCommoditySymbol != null && !previousCommoditySymbol.isEmpty()) {
                DataStore.getSharedInstance().removeSymbolRequest(previousCommoditySymbol);
            }
            CardLayout cl = (CardLayout) lblValueCashMapCommoditiesContainer.getLayout();
            cl.show(lblValueCashMapCommoditiesContainer, CARD_CASHMAP);

//                thread.interrupt();

        } else if (e.getActionCommand().startsWith("C")) {
            cashmapMode = COMMODITY;
            lblValueCashMap.setIcon(null);
            String sKey = e.getActionCommand().split(",")[1];
            if (previousCommoditySymbol != null && !previousCommoditySymbol.isEmpty()) {
                DataStore.getSharedInstance().removeSymbolRequest(previousCommoditySymbol);
            }
            CommoditiesStore.getSharedInstance().addSymbolRequest(sKey);
            previousCommoditySymbol = sKey;
            // lblCashmap.setText(SharedMethods.getSymbolFromKey(sKey));
            lblCashmap.setText(((TWRadioButtonMenuItem) e.getSource()).getText());
            if (checkForDelayed(sKey)) {
                lblCashmap.setToolTipText(Language.getString("SYMBOL_STATUS_DELAYED_DATA"));
            } else {
                lblCashmap.setToolTipText("");
            }
            CardLayout cl = (CardLayout) lblValueCashMapCommoditiesContainer.getLayout();
            cl.show(lblValueCashMapCommoditiesContainer, CARD_COMMODITIES);

        } else { // market
            ExchangeStore.setSelectedExchangeID(e.getActionCommand().substring(1));
            lblMarketName.setText(ExchangeStore.getSharedInstance().getSelectedExchange().getDescription());
            StretchedIndexPanel.getInstance().lblMarketName.setText(ExchangeStore.getSharedInstance().getSelectedExchange().getDisplayExchange());
//            createIndexPopupMenu();
            isCustomindex = false;
            StretchedIndexPanel.isCustomindex = false;
            createIndexPopupMenu(true);

            updateMarketTime();

            //Application.getInstance().fireApplicationTimeZoneChanged(ExchangeStore.getSharedInstance().getSelectedExchange().getActiveTimeZone());
        }

//        if (e.getActionCommand().startsWith("I")) { // index
//            key = e.getActionCommand().substring(1);
//            symbol = SharedMethods.getSymbolFromKey(key);
////            exchange = SharedMethods.getExchangeFromKey(key);
//            setIndex(DataStore.getSharedInstance().getCompanyName(key));
////            updateGUI();
//        } else { // market
//            ExchangeStore.setSelectedExchangeID(e.getActionCommand().substring(1));
//            lblMarketName.setText(ExchangeStore.getSharedInstance().getSelectedExchange().getDescription());
////            createIndexPopupMenu();
//            createIndexPopupMenu(true);
//            updateMarketTime();
//
//            //Application.getInstance().fireApplicationTimeZoneChanged(ExchangeStore.getSharedInstance().getSelectedExchange().getActiveTimeZone());
//        }
    }

    public synchronized void setIndex(String descr) {
        lblIndexName.setText(descr);
        SharedMethods.updateComponent(lblIndexName);
//        lblIndexName.updateUI();
    }

    /*public synchronized static boolean isIndexIDChanged() {
        if (indexIDChanged) {
            setIndexIDChanged(false);
            return true;
        }
        return false;
    }*/

    /*public synchronized static void setIndexIDChanged(boolean status) {
        indexIDChanged = status;
    }*/

    /* Workspace Listener */

    public void applicationExiting() {

    }

    public void applicationLoaded() {

    }

    public void applicationLoading(int percentage) {

    }

    public void applicationReadyForTransactions() {
        CommoditiesStore.getSharedInstance().clear();
        CommoditiesStore.getSharedInstance().loadCommoditiesFile();
        //CommoditiesStore.getSharedInstance().sendValidationRequests();
        revalidateCashMapPopup(true);
    }

    public void applicationTimeZoneChanged(TimeZone zone) {

    }

    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void snapshotProcessingStarted(Exchange exchange) {

    }

    public void workspaceLoaded() {
//        activate();
    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void workspaceWillLoad() {
        // revalidateCashMapPopup();
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        if (workspaceKey == null) {
            updateGUI();
        } else {
            setIndexID(workspaceKey);
            prevMarket = null;
            updateGUI();
        }
//        createIndexPopupMenu(false);
    }

    public void exchangesLoaded() {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMustInitialize(Exchange exchange) {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void customIndexAdded(String index) {
        createIndexPopupMenu(false);
    }

    public void customIndexRemoved(String index) {
        createIndexPopupMenu(false);
    }

    public void customIndexEditted(String index) {
        createIndexPopupMenu(false);
    }

    public void paint(Graphics g) {
        //painting the index graph
        if (graphMode) {
            super.paint(g);
            ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            try {
                if (Language.isLTR()) {
                    g.translate(413, 0);
                } else {
                    g.translate(0, 0);
                }

                if (Language.isLTR()) {
                    g.drawImage(imageGraphBG.getImage(), 0, 0, this);
                } else {
                    if (isResolution2048) {
                        g.drawImage(imageGraphBG.getImage(), 0, 0, this);
                    } else {
                        g.drawImage(imageGraphBG.getImage(), 0, 0, imageGraphBG.getIconWidth() - 100, imageGraphBG.getIconHeight(), this);
                    }
                }
                Graphics2D g2D = (Graphics2D) g;
                if (isResolution2048) {
                    g2D.scale(390d / 240d, 1);
                } else {
                    g2D.scale(300d / 240d, 1);
                }
                g.setColor(indexGraphFGColor);
                int h = 31;
                int w = 240;
                double[] arr;
                if (ExchangeStore.isExpired(SharedMethods.getExchangeFromKey(key))) {
                    return;
                }
                if (ExchangeStore.isInactive(SharedMethods.getExchangeFromKey(key))) {
                    return;
                }
                if (key.equals("")) {
                    return;
                }
                DynamicArray ohlc = OHLCStore.getInstance().getOHLCForIndex(key);

                if (ohlc.size() <= 240) {
                    arr = new double[ohlc.size() + 2];
                    arr[0] = 1000000;
                    for (int j = 0; j < ohlc.size(); j++) {
                        IntraDayOHLC intradayohlc = (IntraDayOHLC) ohlc.get(j);
                        arr[j + 2] = intradayohlc.getClose();
                        arr[0] = Math.min(arr[0], intradayohlc.getClose());
                        arr[1] = Math.max(arr[1], intradayohlc.getClose());

                    }
                } else {
                    int constant = ohlc.size() - 240;
                    arr = new double[242];
                    arr[0] = 1000000;
                    for (int j = ohlc.size() - 240; j < ohlc.size(); j++) {
                        IntraDayOHLC intradayohlc = (IntraDayOHLC) ohlc.get(j);
                        arr[j + 2 - constant] = intradayohlc.getClose();
                        arr[0] = Math.min(arr[0], intradayohlc.getClose());
                        arr[1] = Math.max(arr[1], intradayohlc.getClose());

                    }
                }


                try {
                    g.setFont(graphFont);
                    if (arr[0] != 1000000) {
                        g.drawString(highLowFormat.format(arr[1]) + "", 2, 8);
                        g.drawString(highLowFormat.format(arr[0]) + "", 2, 29);
                    }

                    if ((arr != null) && (arr.length > 0) && (arr[1] != Double.MIN_VALUE)) {
                        g.setColor(indexGraphFGColor);
                        double[] rangearray = new double[arr.length];
                        for (int i = 0; i < arr.length; i++) {
                            rangearray[i] = (arr[i] - arr[0]);
                        }
                        double yratio = ((h - 2) / rangearray[1]);
                        double xgap = (w) / (double) w;
                        int[] xcordinates = new int[arr.length];
                        for (int i = 0; i < arr.length; i++) {
                            xcordinates[i] = i + 30;
                        }

                        for (int i = 0; i < arr.length - 3; i++) {
                            if (rangearray[i + 3] >= 0) {
                                g.drawLine(xcordinates[i], (int) ((h) - (rangearray[i + 2]) * yratio) + 1, xcordinates[i + 1],
                                        (int) ((h) - (rangearray[i + 3]) * yratio) + 1);
                            } else {
                                break;
                            }
                        }

                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
                ohlc = null;

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            super.paint(g);
        }
    }

    public void runThread() {
        //To change body of implemented methods use File | Settings | File Templates.

        if (System.getProperty("DisableIndexPanel") != null) {
            return;
        }
//        updateGUI();
        if (!Client.getInstance().getFrame().isVisible()) {
            return;
        }
        try {
            updateGUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
//        repaint();

    }

    public String getTitle() {
        return "index panel updator";  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Constants.ThreadTypes getTableID() {
        return Constants.ThreadTypes.INDEX_PANEL;  //To change body of implemented methods use File | Settings | File Templates.
    }


    class IconLable extends JPanel {
        //        private ImageIcon icon;
        //        private int width;
        private JLabel label;
        private JLabel label2;
        private JPanel cumilativepannel;
        private boolean joined;
        private int direction;


        public IconLable(boolean extendedMode) {
            label = new JLabel();
            label2 = new JLabel();
            // cumilativepannel = new JPanel(new FlexFlowLayout(new String[]{"50","100%"},1,0));
            cumilativepannel = new JPanel(new BorderLayout(0, 0));
            cumilativepannel.add(label, BorderLayout.CENTER);
            if (Language.isLTR()) {
                cumilativepannel.add(label2, BorderLayout.WEST);
            } else {
                cumilativepannel.add(label2, BorderLayout.EAST);
            }
            cumilativepannel.setOpaque(false);
            this.setLayout(new BorderLayout());
            //  label.setBorder(BorderFactory.createLineBorder(Color.black));
            //  label2.setBorder(BorderFactory.createLineBorder(Color.black));
            this.add(cumilativepannel, BorderLayout.CENTER);
            GUISettings.applyOrientation(cumilativepannel);
        }

        public IconLable() {
            label = new JLabel();
            this.setLayout(new BorderLayout());
            this.add(label, BorderLayout.CENTER);
        }

        public IconLable(int direction, boolean joined) {
            this.joined = joined;
            this.direction = direction;
            if (direction == 0) {
                label = new JLabel();
                this.setLayout(new BorderLayout());
                this.add(label, BorderLayout.CENTER);
            }
        }

        public void setText(String text) {
            label.setText(text);
        }

        public void setText2(String text) {
            if (label2 != null)
                text = " " + text + " ";
            label2.setText(text);
        }

        public void setFont(Font font) {
            super.setFont(font);
            if (label != null) {
                label.setFont(font);    //.deriveFont(Font.BOLD));

                if (label2 != null) {
                    label2.setFont(font);    //.deriveFont(Font.BOLD));
                }
            }
        }

        public void setFont2(Font font) {
            if (label2 != null) {
                label2.setFont(font);    //.deriveFont(Font.BOLD));
            }
        }

        public void setForeground(Color fg) {
            super.setForeground(fg);
            if (label != null) {
                label.setForeground(fg);
                if (label2 != null) {
                    label2.setForeground(fg);
                }
            }
        }

        public void setForeground2(Color fg) {
            if (label2 != null) {
                label2.setForeground(fg);
            }
        }

        public void setHorizontalAlignment(int alignment) {
            label.setHorizontalAlignment(alignment);
            if (label2 != null) {
                label2.setHorizontalAlignment(alignment);
            }
        }

        public void setIcon(ImageIcon icon) {
            label.setIcon(icon);
        }
//        public void setImage(ImageIcon icon){
//            this.icon = icon;
//            width = icon.getIconWidth();
//        }

        public void paint(Graphics g) {
            try {
                if (joined && direction == 0 && (imageiconup != null) && (imagewidthup > 0)) {
                    int width = getWidth();
                    for (int i = 0; i <= width; i += imagewidthup) {
                        g.drawImage(imageiconup.getImage(), i, 0, this);
                    }
                    if (imageiconLup != null) {
                        g.drawImage(imageiconLup.getImage(), 0, 0, this);
                    }
                    if (imageiconRup != null) {
                        g.drawImage(imageiconRup.getImage(), getWidth() - imageiconRup.getIconWidth(), 0, this);
                    }
                    super.paintChildren(g);
                    /*if(this.getBorder()!=null){
                        this.getBorder().paintBorder(this,g,0,0,this.getWidth(),this.getHeight());
                    }*/
                } else if (joined && direction == 1 && (imageicondown != null) && (imagewidthdown > 0)) {
                    int width = getWidth();
                    for (int i = 0; i <= width; i += imagewidthdown) {
                        g.drawImage(imageicondown.getImage(), i, 0, this);
                    }
                    if (imageiconLdown != null) {
                        g.drawImage(imageiconLdown.getImage(), 0, 0, this);
                    }
                    if (imageiconRdown != null) {
                        g.drawImage(imageiconRdown.getImage(), getWidth() - imageiconRdown.getIconWidth(), 0, this);
                    }
                    super.paintChildren(g);
                    /*if(this.getBorder()!=null){
                        this.getBorder().paintBorder(this,g,0,0,this.getWidth(),this.getHeight());
                    }*/
                } else if ((imageicon != null) && (imagewidth > 0)) {
                    int width = getWidth();
                    for (int i = 0; i <= width; i += imagewidth) {
                        g.drawImage(imageicon.getImage(), i, 0, this);
                    }
                    if (imageiconL != null) {
                        g.drawImage(imageiconL.getImage(), 0, 0, this);
                    }
                    if (imageiconR != null) {
                        g.drawImage(imageiconR.getImage(), getWidth() - imageiconR.getIconWidth(), 0, this);
                    }
                    super.paintChildren(g);
                    /*if(this.getBorder()!=null){
                        this.getBorder().paintBorder(this,g,0,0,this.getWidth(),this.getHeight());
                    }*/
                } else {
                    super.paint(g);
                }
            } catch (Exception e) {

            }
//            super.paintChildren(g);
//            super.paint(g);
        }
    }

    class StringKeys implements Comparable {
        String key;
        String name = "";

        StringKeys(String key1, String name1) {
            key = key1;
            name = name1;
        }

        public String toString() {
            return name;
        }

        public String getKey() {
            return key;
        }

        public int compareTo(Object o) {
            return toString().compareTo(o.toString());
        }

    }

    class NonDefaultIndexList {
        private ArrayList<StringKeys> enabledIndexes;
        String exchange;
        StringKeys mainIndex = null;

        NonDefaultIndexList(String exchange) {
            enabledIndexes = new ArrayList<StringKeys>();
            comIndexList.clear();
            loadIndexFile(exchange);
            this.exchange = exchange;
        }

        private void loadIndexFile(String exchange) {
            try {
                SmartProperties exgIndexes = new SmartProperties(Meta.DS, true);
                Vector keyVector = new Vector();
                String key = null;
                String value = null;
                String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/index_" + Language.getSelectedLanguage() + ".msf";
                exgIndexes.loadCompressed(sLangSpecFileName);

                //Searching indexes
                Enumeration indexEnum = exgIndexes.keys();
                while (indexEnum.hasMoreElements()) {
                    String skey = (String) indexEnum.nextElement();
                    String data = (String) exgIndexes.get(skey);
                    try {
                        String[] field = data.split(Meta.FS);
                        String keyR = SharedMethods.getKey(exchange, skey, Meta.INSTRUMENT_INDEX);
                        StringKeys index = new StringKeys(keyR, field[2]);
                        String dataRec = field[5] + Meta.ID + exchange + Meta.ID + field[0] + Meta.ID + field[5] + Meta.ID + field[2] + Meta.ID + field[3] + Meta.ID + field[4] + Meta.ID + "" + Meta.ID + field[5];
                        enabledIndexes.add(index);
                        if (field[1].equals("IM")) { //--To Solve the conflict in MSM -Shanika
                            mainIndex = index;
                        }
                        index = null;
                        ValidatedSymbols.getSharedInstance().addSymbol(keyR, dataRec);
                        if (SharedMethods.indexSortingOrderByDefault(exchange)) {
                            Collections.sort(enabledIndexes);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();

                    }
                }
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_COM_INDEX)) {
                    try {
                        sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/cust_index_" + Language.getSelectedLanguage() + ".msf";
                        exgIndexes = new SmartProperties(Meta.DS);
                        File f = new File(sLangSpecFileName);
                        boolean bIsLangSpec = f.exists();
                        if (bIsLangSpec) {
                            exgIndexes.loadCompressed(sLangSpecFileName);
                            indexEnum = exgIndexes.keys();
                            while (indexEnum.hasMoreElements()) {
                                try {
                                    String skey = (String) indexEnum.nextElement();
                                    String data = (String) exgIndexes.get(skey);
                                    if (data.split(Meta.RS)[1].equals(Settings.getInstitutionCode())) {
                                        String[] field = data.split(Meta.FS);
                                        String dataRec = field[5] + Meta.ID + exchange + Meta.ID + field[0] + Meta.ID + field[5] + Meta.ID + field[2] + Meta.ID + field[3] + Meta.ID + field[4] + Meta.ID + "" + Meta.ID + field[5];
                                        String keyR = SharedMethods.getKey(exchange, skey, Meta.INSTRUMENT_INDEX);
                                        StringKeys index = new StringKeys(keyR, field[2]);
                                        enabledIndexes.add(index);
                                        comIndexList.add(keyR);
                                        ValidatedSymbols.getSharedInstance().addSymbol(keyR, dataRec);
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                Collections.sort(enabledIndexes);
                if (mainIndex == null && !enabledIndexes.isEmpty()) {
                    mainIndex = enabledIndexes.get(0);
                }
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }


        }

        public String getExchange() {
            return exchange;
        }

        public void setExchange(String exchange) {
            this.exchange = exchange;
        }

        public ArrayList<StringKeys> getEnabledIndexes() {
            return enabledIndexes;
        }

        public void setEnabledIndexes(ArrayList<StringKeys> enabledIndexes) {
            this.enabledIndexes = enabledIndexes;
        }

        public StringKeys getMainIndex() {
            return mainIndex;
        }

        public void setMainIndex(StringKeys mainIndex) {
            this.mainIndex = mainIndex;
        }
    }

    private NonDefaultIndexList loadIndexesForNonDefault(String exgKey) {
        //removeSymbolRequests();
        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exgKey);
        if (exchange != null && !exchange.isDefault()) {
            if (nonDefaultIndexes.get(exgKey) == null) {
                nonDefaultIndexes.put(exgKey, new NonDefaultIndexList(exgKey));
            }
            return nonDefaultIndexes.get(exgKey);
        } else {
            return null;
        }

    }

    public void setLastNonDefaultSymbol(String lastNonDefaultSymbol) {
        this.lastNonDefaultSymbol = lastNonDefaultSymbol;
    }

    private void removeSymbolRequests() {
        if (lastNonDefaultSymbol != null && !lastNonDefaultSymbol.isEmpty()) {
            if (lastNonDefaultSymbolType == SYMBOL_TYPE_COM_INDEX) {
                DataStore.getSharedInstance().removeSymbolRequest(SharedMethods.getExchangeFromKey(lastNonDefaultSymbol), SharedMethods.getSymbolFromKey(lastNonDefaultSymbol),
                        SharedMethods.getInstrumentTypeFromKey(lastNonDefaultSymbol), Meta.CUST_INDEX);
            } else {
                DataStore.getSharedInstance().removeSymbolRequest(lastNonDefaultSymbol);
            }
            lastNonDefaultSymbol = "";
            lastNonDefaultSymbolType = SYMBOL_TYPE_DEF_INDEX;
        }
    }

    private void addSymbolRequest(String sKey) {
        Exchange ex = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sKey));
        if (ex != null) {
            if (comIndexList.contains(sKey)) {
                DataStore.getSharedInstance().addSymbolRequest(ex.getSymbol(), SharedMethods.getSymbolFromKey(sKey),
                        SharedMethods.getInstrumentTypeFromKey(sKey), Meta.CUST_INDEX);
                lastNonDefaultSymbol = sKey;
                lastNonDefaultSymbolType = SYMBOL_TYPE_COM_INDEX;
            } else if (!ex.isDefault()) {
                DataStore.getSharedInstance().addSymbolRequest(sKey);
                lastNonDefaultSymbol = sKey;
                lastNonDefaultSymbolType = SYMBOL_TYPE_DEF_INDEX;
            }
        }
    }

    private class SimplevariationImage extends ImageIcon {
        public static final int TYPE_BID_ASK = 0;
        public static final int TYPE_SPREAD = 1;
        public static final int TYPE_RANGE = 2;
        public static final int TYPE_CHANGE = 3;
        public static final int TYPE_TICK = 4;
        public static final int TICK_FILTER = 3;
        public static final int TYPE_OHLC_MAP = 5;
        public static final int TYPE_CASH_MAP = 6;
        public static final int TYPE_CASH_FLOW = 7;
//    public static final int TYPE_FULL_MAP=8;

        private int type;
        private int height = 10;
        private int width = 50;
        private int CLEARENCE = 4;
        private int LOW_CLEARENCE = 2;
        private Double value = Double.NaN;
        private int intValue = 0;
        private int imageWidth = 0;
        private String exchange;
        private double[] values;//=new double[13];
        private double[] fullMapValues = new double[7];


        public SimplevariationImage() {
            fullMapValues[0] = 20;
            fullMapValues[6] = 70;
            imageWidth = width - (CLEARENCE * 2);
        }

        public int getIconHeight() {
            return height;
        }

        public int getIconWidth() {
            return imageWidth;
        }

        public void setValue(Double value) {
            this.value = value;
        }


        public void setWidth(int width) {
            this.width = width;
            imageWidth = width - (CLEARENCE * 2);
        }

        public void setHeight(int height) {
            this.height = height;
            CLEARENCE = height / 4;
        }


        public void paintIcon(Component c, Graphics g, int x, int y) {
            if (!Double.isNaN(value)) {
                try {
                    g.setColor(Theme.BIDASK_DOWN_COLOR);
                    g.fillRect(2, 2, c.getWidth() - 4, c.getHeight() - 4);
                    g.setColor(Theme.BIDASK_UP_COLOR);
                    g.fillRect(2, 2, (int) (value * (c.getWidth() - 4)), c.getHeight() - 4);
                    g.setColor(Theme.INDEXPANEL_CASHMAP_BORDER_COLOR);
                    g.drawRect(2, 2, c.getWidth() - 4, c.getHeight() - 4);
                } catch (Exception e) {
                }
            }
        }


    }

    public String isCashmapMode() {
        if (cashmapMode == COMMODITY) {
            return COMMODITY + "," + previousCommoditySymbol;
        } else {
            return cashmapMode + "";
        }

    }

    // use this mtd only for loading from workspace

    public void setCashmapMode(String cashmapMode) {
        String mode = cashmapMode.split(",")[0];
        int cMode = 0;

        try {
            cMode = Integer.parseInt(mode);
        } catch (NumberFormatException e) {
            cMode = 0;
        }
        this.cashmapMode = cMode;
        if (cMode == CASH_MAP) {
            lblValueCashMap.setIcon(imgValueCashmap);
            menuCashMap.setSelected(true);
            lblCashmap.setText(Language.getString("INDEX_PAN_PCTCASH"));
            lblCashmap.setToolTipText(Language.getString("INDEX_PAN_CASHMAP_TOOLTIP"));
        } else if (cMode == CASH_MAP_PCT) {
            lblValueCashMap.setIcon(null);
            menuNetCashMap.setSelected(true);
            lblCashmap.setText(Language.getString("INDEX_PAN_NETCASH"));
            lblCashmap.setToolTipText(Language.getString("INDEX_PAN_NETCASH_TOOLTIP"));
        } else if (cMode == COMMODITY) {
            String[] list = cashmapMode.split(",");
            if (list.length == 2) {
                String sKey = list[1];
                Component selected = checkForMenu(sKey);
                previousCommoditySymbol = sKey;
                if (selected != null) {
                    menuNetCashMap.setSelected(false);
                    menuCashMap.setSelected(false);
                    ((TWRadioButtonMenuItem) selected).setSelected(true);
                    //   cashMapPopup.setSelected(selected);
                    //     grp.setSelected(((TWCheckBoxMenuItem)selected).ge ,true);
                    this.cashmapMode = COMMODITY;
                    lblValueCashMap.setIcon(null);


                    CommoditiesStore.getSharedInstance().addSymbolRequest(sKey);
                    previousCommoditySymbol = sKey;
                    // lblCashmap.setText(SharedMethods.getSymbolFromKey(sKey));
                    lblCashmap.setText(((TWRadioButtonMenuItem) selected).getText());
                    if (checkForDelayed(sKey)) {
                        lblCashmap.setToolTipText(Language.getString("SYMBOL_STATUS_DELAYED"));
                    } else {
                        lblCashmap.setToolTipText("");
                    }
                    CardLayout cl = (CardLayout) lblValueCashMapCommoditiesContainer.getLayout();
                    cl.show(lblValueCashMapCommoditiesContainer, CARD_COMMODITIES);

                } else {
//                    this.cashmapMode = CASH_MAP_PCT;
//                    lblValueCashMap.setIcon(imgValueCashmap);
//                    menuCashMap.setSelected(true);
//                    lblCashmap.setText(Language.getString("INDEX_PAN_PCTCASH"));
//                    lblCashmap.setToolTipText(Language.getString("INDEX_PAN_CASHMAP_TOOLTIP"));
                    lblValueCashMap.setIcon(null);
                    menuNetCashMap.setSelected(true);
                    lblCashmap.setText(Language.getString("INDEX_PAN_NETCASH"));
                    lblCashmap.setToolTipText(Language.getString("INDEX_PAN_NETCASH_TOOLTIP"));
                }
            } else {
//                this.cashmapMode = CASH_MAP;
//                lblValueCashMap.setIcon(imgValueCashmap);
//                menuCashMap.setSelected(true);
//                lblCashmap.setText(Language.getString("INDEX_PAN_PCTCASH"));
//                lblCashmap.setToolTipText(Language.getString("INDEX_PAN_CASHMAP_TOOLTIP"));
            }
        }
    }

    private Component checkForMenu(String sKey) {
        try {
            Component[] components = cashMapPopup.getComponents();
            for (int i = 0; i < components.length; i++) {
                Component comp = components[i];
                Component defaultcomp = components[0];
                if (comp instanceof TWRadioButtonMenuItem && ((TWRadioButtonMenuItem) (comp)).getActionCommand().startsWith("C")) {
                    String Command = ((TWRadioButtonMenuItem) (comp)).getActionCommand();
                    String[] data = Command.split(",");
                    if (data.length == 2 && data[1].equals(sKey)) {
                        return comp;
                    }
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public boolean isGraphMode() {
        return graphMode;
    }

    public void setGraphMode(boolean graphMode) {
        this.graphMode = graphMode;
        if (graphMode) {
            lblCashmap.setCursor(null);
        } else {
            lblCashmap.setCursor(new Cursor(Cursor.HAND_CURSOR));
        }
    }

    private void revalidateCashMapPopup(boolean connected) {
        cashMapPopup.removeAll();
        grp = new ButtonGroup();
        grp.add(menuCashMap);
        grp.add(menuNetCashMap);
        cashMapPopup.add(menuCashMap);
        cashMapPopup.add(menuNetCashMap);

        ArrayList<CommoditiesStore.CommodityItem> items = CommoditiesStore.getSharedInstance().getCommodityList();
        comoditiesList = items;
        if (!items.isEmpty()) {
            for (int i = 0; i < items.size(); i++) {
                CommoditiesStore.CommodityItem commodity = items.get(i);
                TWRadioButtonMenuItem menuitem = new TWRadioButtonMenuItem(UnicodeUtils.getNativeString(commodity.description));

                menuitem.setActionCommand("C," + commodity.sKey);
                menuitem.addActionListener(this);

                grp.add(menuitem);
                cashMapPopup.add(menuitem);
            }

        }
        GUISettings.applyOrientation(cashMapPopup);

        if (connected && cashmapMode == COMMODITY) {
            if (grp.getButtonCount() > 2 && (checkForMenu(previousCommoditySymbol) != null)) {
                setCashmapMode(cashmapMode + "," + previousCommoditySymbol);
            } else {
                this.cashmapMode = CASH_MAP_PCT;
                setCashmapMode(cashmapMode + "");
            }
        }

    }

    private boolean checkForDelayed(String sKey) {
        if (comoditiesList != null && comoditiesList.size() > 0) {
            for (int i = 0; i < comoditiesList.size(); i++) {
                CommoditiesStore.CommodityItem item = comoditiesList.get(i);
                if (item.sKey.equals(sKey) && item.delayed) {
                    return true;
                }
            }
            return false;

        } else {
            return false;
        }
    }


}
