package com.isi.csvr.table;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.properties.ViewSetting;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
//import javax.swing.table.TableColumnModel;
//import javax.swing.table.DefaultTableColumnModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 8, 2005
 * Time: 2:07:52 PM
 */
public class CurrencyModel extends CommonTable
        implements TableModel, CommonTableInterface, DetailQuote {

    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();
    private ArrayList<String> selected;
    private int decimalCount = 3;

    public CurrencyModel(){
        selected = new ArrayList<String>();
    }

    public void setSymbol(String symbol) {

    }

    public int getColumnCount() {
        try {
            return selected.size() + 1;
//            return CurrencyStore.getCurrencyIndex().size() + 1;
        } catch (Exception e) {
            return 0;
        }
    }


    public void setDecimalCount(int decimalCount) {
        this.decimalCount = decimalCount;
    }

    public int getRowCount() {
        try {
            return selected.size();
//            return CurrencyStore.getCurrencyIndex().size();
        } catch (Exception e) {
            return 0;
        }
    }

    public void setArray(ArrayList<String> selected){
        this.selected.clear();
        this.selected.addAll(selected);

        System.out.println("Array size = "+ this.selected.size());
    }

    public Object getValueAt(int row, int col) {
        try {
            if(col == -2){
                return ""+decimalCount;
            } else if (col == 0) {
                return selected.get(row);
            } else {
//                return doubleTransferObject.setValue(CurrencyStore.getRate(CurrencyStore.getCurrencyIndex().get(row),
//                        CurrencyStore.getCurrencyIndex().get(col - 1)));
                return doubleTransferObject.setValue(CurrencyStore.getRate(selected.get(row),selected.get(col-1)));
            }
        } catch (Exception e) {
            return Constants.NULL_STRING;
        }
    }

//    public Object getValueAt(int row, int col) {
//        try {
//            if (col == 0) {
//                return CurrencyStore.getCurrencyIndex().get(row);
//            } else {
//                return doubleTransferObject.setValue(CurrencyStore.getRate(CurrencyStore.getCurrencyIndex().get(row),
//                        CurrencyStore.getCurrencyIndex().get(col - 1)));
//            }
//        } catch (Exception e) {
//            return Constants.NULL_STRING;
//        }
//    }

    public String getColumnName(int col) {
        if (col == 0) {
            return Constants.NULL_STRING;
        } else {
            return selected.get(col - 1);
        }
    }

    public Class getColumnClass(int iCol) {
        return Object.class;
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public int getRenderingID(int row, int col) {
        switch (row) {
            case 0:
                if (col == 0) {
                    return 1;
                } else {
                    return 3;
                }
            default:
                if (col == 0) {
                    return 1;
                } else {
                    return 3;
                }
        }
    }

    public long getTimeOffset() {
        return 0;
    }

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }
}
