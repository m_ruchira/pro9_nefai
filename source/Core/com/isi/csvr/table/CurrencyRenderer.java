// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.shared.LongTransferObject;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

public class CurrencyRenderer extends TWBasicTableRenderer implements TableCellRenderer {
    private int g_iNumberAlign;
    private int g_iCenterAlign;

    private String g_sNA = "NA";

    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color g_oHeaderBGColor;
    private static Color g_oHeaderFGColor;

    private Color headerFG;
    private Color headerBG;

    private Color foreground, background;
    private TWDecimalFormat oPriceFormat;

    private double doubleValue;
    private long longValue;

    private static Border selectedBorder;
    private static Border unselectedBorder;
    private static Border headerBorder;

    public CurrencyRenderer() {

        reload();

        oPriceFormat = new TWDecimalFormat(" ###,##0.000 ");

        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
    }

    public void propertyChanged(int property) {
    }

    public static void reloadForPrinting() {
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.white;
        g_oBG2 = Color.white;
        g_oHeaderBGColor = Color.white;
        g_oHeaderFGColor = Color.black;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            g_oHeaderBGColor = Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR");
            g_oHeaderFGColor = Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR");
            selectedBorder = BorderFactory.createMatteBorder(0, 0, 2, 0, g_oSelectedFG);
            unselectedBorder = BorderFactory.createMatteBorder(0, 0, 2, 0, g_oFG1);
            headerBorder = UIManager.getBorder("TableHeader.cellBorder");
        } catch (Exception e) {
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } /*else if (row > 10) {
                if (column < 2) {
                    if (row % 2 == 0) {
                        foreground = sett.getBidColor1FG();
                        background = sett.getBidColor1BG();
                    } else {
                        foreground = sett.getBidColor2FG();
                        background = sett.getBidColor2BG();
                    }
                } else {
                    if (row % 2 == 0) {
                        foreground = sett.getAskColor1FG();
                        background = sett.getAskColor1BG();
                    } else {
                        foreground = sett.getAskColor2FG();
                        background = sett.getAskColor2BG();
                    }
                }
            } else */

            else {
                if (row % 2 == 0) {
                foreground = sett.getRowColor1FG();
                background = sett.getRowColor1BG();
            } else {
                foreground = sett.getRowColor2FG();
                background = sett.getRowColor2BG();
            }
            }
            headerBG = sett.getHeaderColorBG();
            headerFG = sett.getHeaderColorFG();
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
            headerBG = g_oHeaderBGColor;
            headerFG = g_oHeaderFGColor;
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        setDecimalPalces(Integer.parseInt((String) table.getModel().getValueAt(row, -2)));

        int iRendID = getRenderingID(column);

        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    lblRenderer.setBackground(headerBG);
                    lblRenderer.setForeground(headerFG);
                    lblRenderer.setBorder(headerBorder);
                    break;
                case 3: // PRICE
                    try {
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        if (doubleValue >= 0) {
                            if (row != (column - 1)) {
                                lblRenderer.setText(oPriceFormat.format(doubleValue));
                                lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                            } else {
                                lblRenderer.setText("-");
                                lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                            }
                        } else {
                            lblRenderer.setText("");
                        }
                        break;
                    } catch (ClassCastException e) {
                        longValue = ((LongTransferObject) (value)).getValue();
                        if (longValue > 0) {
                            if (row != (column - 1)) {
                                lblRenderer.setText(oPriceFormat.format(longValue));
                                lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                            } else {
                                lblRenderer.setText("-");
                                lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                            }
                        } else {
                            lblRenderer.setText("");
                        }
                        break;
                    }
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            e.printStackTrace();
            lblRenderer.setText("");
        }

        return lblRenderer;
    }

    private int getRenderingID(int iCol) {
        switch (iCol) {
            case 0:
                return 1;
            default:
                return 3;
        }
    }

    public void setDecimalPalces(int decimal) {
        switch (decimal) {
            case 0:
                oPriceFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                break;
            case 1:
                oPriceFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                break;
            case 2:
                oPriceFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                break;
            case 3:
                oPriceFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                break;
            case 4:
                oPriceFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                break;
            case 5:
                oPriceFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL);
                break;
            case 6:
                oPriceFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL);
                break;
            case 7:
                oPriceFormat.applyPattern(Constants.PATTERN_SEVEN_DECIMAL);
                break;
            case 8:
                oPriceFormat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL);
                break;
            case 9:
                oPriceFormat.applyPattern(Constants.PATTERN_NINE_DECIMAL);
                break;
            case 10:
                oPriceFormat.applyPattern(Constants.PATTERN_TEN_DECIMAL);
                break;
        }
    }
}
