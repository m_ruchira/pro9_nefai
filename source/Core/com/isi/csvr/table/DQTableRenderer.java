// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;

public class DQTableRenderer extends TWBasicTableRenderer {
    private int g_iStringAlign;
    private int g_iStringOppositeAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private TWDateFormat g_oDateFormat;
    private TWDateFormat g_oDateTimeFormatHMS;
    private TWDateFormat g_oTimeFormat;
    private TWDateFormat g_oDateTimeFormat = new TWDateFormat(" HH:mm:ss ");
    private static String g_sNA = "NA";
    private static String g_sBID = "NA";
    private static String g_sASK = "NA";
    private String priceMarketOrder = "NA";
    private String priceMarketOnOpening = "NA";

    private static Color g_oUpFGColor;
    private static Color g_oDownFGColor;
    private static Color g_oUpBGColor;
    private static Color g_oDownBGColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color g_oDownColor;
    private static Color g_oUpColor;
    private Color changeUpColor;
    private Color changeDownColor;
    private Color valueUpFGColor;
    private Color valueUpBGColor;
    private Color valueDownFGColor;
    private Color valueDownBGColor;

    private static Color generalBGColor;
    private static Color generalFGColor;
    private static Color planeBGColor;
    private static Color planeFGColor;
    private static Color bidBGColor;
    private static Color bidFGColor;
    private static Color offerBGColor;
    private static Color offerFGColor;

    private static Color generalEvenBGColor;
    private static Color planeEvenBGColor;
    private static Color bidEvenBGColor;
    private static Color offerEvenBGColor;

    private Color foreground, background;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oLongDecimalFormat;
    private TWDecimalFormat oCurrencyDecimalFormat;
    private TWDecimalFormat oPctChangeFormat;
    private TWDecimalFormat oQuantityFormat;

    private Date date = new Date();
    private double doubleValue;
    private long longValue;
    private int intValue;
    private int rendID;
    private long timeOffset;
    private long now;
    private long updateDirection;
    private int insType = 0;
    private boolean isStockModel = false;


    public DQTableRenderer() {
        reload();
        g_oDateFormat = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));//yyyy/MM/dd"));
        g_oDateTimeFormatHMS = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));//" dd:MM:yyyy '-' HH:mm:ss ");
        g_oTimeFormat = new TWDateFormat(Language.getString("SYMBOL_TIME_FORMAT"));//" HH:mm:ss ");
        oPctChangeFormat = new TWDecimalFormat(" ###,##0.00 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
        oLongDecimalFormat = new TWDecimalFormat(" ###,##0.0000 ");
        priceMarketOrder = Language.getString("DEPTH_PRICE_MARKET_ORDER");
        priceMarketOnOpening = Language.getString("DEPTH_PRICE_MARKET_ON_OPENING");
        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
        g_iStringOppositeAlign = JLabel.RIGHT;
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {

    }

    public void propertyChanged(int property) {
    }

    public static void reloadForPrinting() {
        g_oUpFGColor = Color.black;
        g_oDownFGColor = Color.black;
        g_oUpBGColor = Color.white;
        g_oDownBGColor = Color.white;
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
        g_oDownColor = Color.black;
        g_oUpColor = Color.black;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oUpFGColor = Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR");
            g_oDownFGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR");
            g_oUpBGColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownBGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            g_oDownColor = g_oDownBGColor;
            g_oUpColor = g_oUpBGColor;
            g_sNA = " " + Language.getString("NA") + " ";
            g_sBID = " " + Language.getString("BID") + " ";
            g_sASK = " " + Language.getString("OFFER") + " ";

            generalBGColor = Theme.getColor("DTQ_GENERAL_BGCOLOR");
            generalFGColor = Theme.getColor("DTQ_GENERAL_FGCOLOR");
            planeBGColor = Theme.getColor("DTQ_PLANE_BGCOLOR");
            planeFGColor = Theme.getColor("DTQ_PLANE_FGCOLOR");

            bidBGColor = Theme.getColor("DTQ_BID_BGCOLOR");
            bidFGColor = Theme.getColor("DTQ_BID_FGCOLOR");
            offerBGColor = Theme.getColor("DTQ_OFFER_BGCOLOR");
            offerFGColor = Theme.getColor("DTQ_OFFER_FGCOLOR");

            generalEvenBGColor = Theme.getColor("DTQ_GENERAL_EVEN_BGCOLOR");
            planeEvenBGColor = Theme.getColor("DTQ_PLANE_EVEN_BGCOLOR");
            bidEvenBGColor = Theme.getColor("DTQ_BID_EVEN_BGCOLOR");
            offerEvenBGColor = Theme.getColor("DTQ_OFFER_EVEN_BGCOLOR");

//            generalBGColor = Color.BLUE;
//            generalFGColor = Color.BLACK;
//            planeBGColor = Color.WHITE;
//            planeFGColor = Color.BLACK;
//            bidBGColor = Color.GREEN;
//            bidFGColor = Color.BLACK;
//            offerBGColor = Color.RED;
//            offerFGColor = Color.BLACK;


        } catch (Exception e) {
            g_oUpBGColor = Color.green;
            g_oDownBGColor = Color.red;
            g_oUpFGColor = Color.black;
            g_oDownFGColor = Color.black;
            g_oDownColor = Color.red;
            g_oUpColor = Color.green;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        try {
            insType = (Integer) table.getModel().getValueAt(-1, column);
        } catch (Exception e) {
//            e.printStackTrace();
        }

        try {
            isStockModel = (Boolean) table.getModel().getValueAt(-2, column);
        } catch (Exception e) {
            isStockModel = false;
        }
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();

//                if ((insType == 0) || (insType == 68) ||(insType == 61)) {
                if (isStockModel) {
                    lblRenderer.setBorder(null);
                }
            }/* else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }*/

            else {
                try {
                    if (isStockModel) {
                        if ((0 <= row) && (row < 7)) {
                            if (row % 2 == 0) {
                                foreground = generalFGColor;
                                background = generalEvenBGColor;
                            } else {
                                foreground = generalFGColor;
                                background = generalBGColor;
                            }

                        } else if (((9 <= row) && (row < 13)) && ((0 <= column) && (column < 2))) {
                            if (row % 2 == 0) {
                                foreground = bidFGColor;
                                background = bidBGColor;
                            } else {
                                foreground = bidFGColor;
                                background = bidEvenBGColor;
                            }

                        } else if (((9 <= row) && (row < 13)) && ((2 <= column) && (column < 4))) {
                            if (row % 2 == 0) {
                                foreground = offerFGColor;
                                background = offerBGColor;

                            } else {
                                foreground = offerFGColor;
                                background = offerEvenBGColor;
                            }

                        } else {
                            foreground = planeFGColor;
                            background = planeBGColor;
                        }
                        lblRenderer.setBorder(null);

                    } else {
                        if (row % 2 == 0) {
                            foreground = sett.getRowColor1FG();
                            background = sett.getRowColor1BG();
                        } else {
                            foreground = sett.getRowColor2FG();
                            background = sett.getRowColor2BG();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            // -------------- Added by Shanika --------------- 

            changeUpColor = sett.getPositiveChangeFG();
            changeDownColor = sett.getNegativeChangeFG();
            valueUpBGColor = sett.getCellHighLightedUpBG();
            valueUpFGColor = sett.getCellHighLightedUpFG();
            valueDownBGColor = sett.getCellHighLightedDownBG();
            valueDownFGColor = sett.getCellHighLightedDownFG();
            if ((table.getModel().getValueAt(row, 0)).equals("1")) {
                foreground = sett.getSmallTradeFG();
            }
        } else {
            try {
                sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();

                if (isSelected) {
                    foreground = g_oSelectedFG;
                    background = g_oSelectedBG;
                    if (isStockModel) {
                        lblRenderer.setBorder(null);
                    }
                } else {
                    if (isStockModel) {
                        if ((0 <= row) && (row < 8)) {
                            if (row % 2 == 0) {
                                foreground = generalFGColor;
                                background = generalEvenBGColor;

                            } else {
                                foreground = generalFGColor;
                                background = generalBGColor;
                            }


                        } else if (((9 <= row) && (row < 13)) && ((0 <= column) && (column < 2))) {

                            if (row % 2 == 0) {
                                foreground = bidFGColor;
                                background = bidBGColor;

                            } else {
                                foreground = bidFGColor;
                                background = bidEvenBGColor;
                            }

                        } else if (((9 <= row) && (row < 13)) && ((2 <= column) && (column < 4))) {
                            if (row % 2 == 0) {
                                foreground = offerFGColor;
                                background = offerBGColor;

                            } else {
                                foreground = offerFGColor;
                                background = offerEvenBGColor;
                            }

                        } else {
                            foreground = planeFGColor;
                            background = planeBGColor;
                        }
                        lblRenderer.setBorder(null);

                    } else {
                        if (row % 2 == 0) {
                            foreground = sett.getRowColor1FG();
                            background = sett.getRowColor1BG();

                        } else {
                            foreground = g_oFG2;
                            background = g_oBG2;
                        }

                    }
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }


            changeUpColor = g_oUpColor;
            changeDownColor = g_oDownColor;
            valueUpBGColor = g_oUpBGColor;
            valueUpFGColor = g_oUpFGColor;
            valueDownBGColor = g_oDownBGColor;
            valueDownFGColor = g_oDownFGColor;
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);


        try {
            oPriceFormat = ((ExchangeFormatInterface) table.getModel()).getDecimalFormat();
        } catch (Exception e) {
        }

        try {
            oCurrencyDecimalFormat = SharedMethods.getDecimalFormat((Byte) table.getModel().getValueAt(-1, 0));
//            if (oCurrencyDecimalFormat == null) {
//                oCurrencyDecimalFormat = SharedMethods.getDecimalFormat((Integer) table.getModel().getValueAt(-1, 0));
//            }
        } catch (Exception e) {
        }

        rendID = ((DetailQuote) table.getModel()).getRenderingID(row, column);
        timeOffset = ((DetailQuote) table.getModel()).getTimeOffset();

        try {
            lblRenderer.setIcon(null);
            switch (rendID) {
                case 0: // DEFAULT
                    lblRenderer.setText(" " + value + " ");
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // DESCRIPTION right
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    lblRenderer.setText(" " + value + " ");
                    break;
                case 2: // DESCRIPTION
                    lblRenderer.setBackground(background); //table.getTableHeader().getBackground());
                    lblRenderer.setForeground(foreground);
                    lblRenderer.setText(" " + value + " ");
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                        lblRenderer.setText(g_sNA);
                    } else if (doubleValue <= 0) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oPriceFormat.format(doubleValue)));
                    }
                    break;
                case 'n': // MIN-MAX
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                        lblRenderer.setText(g_sNA);
                    } else if (doubleValue <= 0) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oPriceFormat.format(doubleValue)));
                    }
                    break;
                case 'V': // PRICE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oLongDecimalFormat.format(doubleValue)));
                    }
                    break;
                case 'C': // PRICE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oCurrencyDecimalFormat.format(doubleValue)));
                    }
                    break;
                case 4: // QUANTITY
                    longValue = ((LongTransferObject) (value)).getValue();
                    lblRenderer.setText(oQuantityFormat.format(longValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 5: // CHANGE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(changeUpColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(changeDownColor);
                    break;
                case 6: // % CHANGE
                    //adPrice = (double[])value;
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    lblRenderer.setText(oPctChangeFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(changeUpColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(changeDownColor);
                    break;
                case 7: // DATE
                    longValue = ((LongTransferObject) (value)).getValue();
                    if (longValue <= Constants.ONE_DAY)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue + timeOffset);
                        lblRenderer.setText(g_oDateFormat.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringOppositeAlign);
                    break;
                case 8: {// DATE TIME
                    longValue = ((LongTransferObject) (value)).getValue();
                    if (longValue <= Constants.ONE_DAY)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue + timeOffset);
                        lblRenderer.setText(g_oDateTimeFormat.format(date));
                    }

                    lblRenderer.setHorizontalAlignment(g_iStringOppositeAlign);
                    break;
                }
                case 'D': {// DATE TIME with secs
                    longValue = toLongValue(value);
                    if (longValue <= Constants.ONE_DAY)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue + timeOffset);
                        lblRenderer.setText(g_oDateTimeFormatHMS.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                }
                /*case 9: // TICK
                    intValue = (int) toLongValue(value);
                    switch (intValue) {
                        case Settings.TICK_UP:
                            lblRenderer.setIcon(new ImageIcon("images/up.gif"));
                            lblRenderer.setText("");
                            break;
                        case Settings.TICK_DOWN:
                            lblRenderer.setIcon(new ImageIcon("images/down.gif"));
                            lblRenderer.setText("");
                            break;
                        default:
                            lblRenderer.setIcon(null);
                            lblRenderer.setText("");
                    }
                    lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                    break;*/
                case 'A': // TIME
                    longValue = ((LongTransferObject) (value)).getValue();
                    if (longValue == 0) {
                        lblRenderer.setText(g_sNA);
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    } else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oTimeFormat.format(date));
                        System.out.println(lblRenderer.getText());
                        lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                    }
                    break;
                case 'B': // Broker ID
                    if ((value == null) || (((String) value)).equals("")
                            || (((String) value)).equals("null"))
                        lblRenderer.setText(g_sNA);
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 'P': // PRICE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    updateDirection = ((DoubleTransferObject) (value)).getFlag();
                    now = System.currentTimeMillis();
                    if (doubleValue == Constants.DEPTH_PRICE_MARKET_ORDER_VALUE) {
                        lblRenderer.setText(priceMarketOrder + " ");
                    } else if (doubleValue == Constants.DEPTH_PRICE_MARKET_ON_OPENING_VALUE) {
                        lblRenderer.setText(priceMarketOnOpening + " ");
                    } else {
                        lblRenderer.setText(oPriceFormat.format(doubleValue));
                    }
//                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    if ((now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                        if (updateDirection > 0) {
                            lblRenderer.setBackground(valueUpBGColor);
                            lblRenderer.setForeground(valueUpFGColor);
                        } else if (updateDirection < 0) {
                            lblRenderer.setForeground(valueDownFGColor);
                            lblRenderer.setBackground(valueDownBGColor);
                        }
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'p': // PRICE with no decimals
                    //adPrice = (double[]) value;
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    updateDirection = ((DoubleTransferObject) (value)).getFlag();
                    now = System.currentTimeMillis();
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    if ((now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                        if (updateDirection > 0) {
                            lblRenderer.setBackground(valueUpBGColor);
                            lblRenderer.setForeground(valueUpFGColor);
                        } else if (updateDirection < 0) {
                            lblRenderer.setForeground(valueDownFGColor);
                            lblRenderer.setBackground(valueDownBGColor);
                        }
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'Q': // Quantity with coloured bg
                    longValue = ((LongTransferObject) (value)).getValue();
                    updateDirection = ((LongTransferObject) (value)).getFlag();
                    now = System.currentTimeMillis();
                    lblRenderer.setText(oQuantityFormat.format(longValue));
                    if ((now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                        if (updateDirection > 0) {
                            lblRenderer.setBackground(valueUpBGColor);
                            lblRenderer.setForeground(valueUpFGColor);
                        } else if (updateDirection < 0) {
                            lblRenderer.setForeground(valueDownFGColor);
                            lblRenderer.setBackground(valueDownBGColor);
                        } else {
                            lblRenderer.setBorder(null);
                        }
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'R': // Bid / Offer Ratio
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    if ((Double.isNaN(doubleValue)) || (doubleValue == -1)) {
                        lblRenderer.setText(g_sNA);
                    } else if ((doubleValue == Double.MAX_VALUE) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                        lblRenderer.setText(g_sBID);
                    } else if ((doubleValue == 0)) {
                        lblRenderer.setText(g_sASK);
                    } else {
                        lblRenderer.setText((oPriceFormat.format(doubleValue)));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'S': // Spread
                    doubleValue = ((DoubleTransferObject) (value)).getValue();
                    if (Double.isNaN(doubleValue)) {
                        lblRenderer.setText(g_sNA);
                    } else if ((doubleValue == Double.MAX_VALUE) || (doubleValue == Double.POSITIVE_INFINITY)) {
                        lblRenderer.setText(g_sBID);
                    } else if ((doubleValue == Double.MIN_VALUE) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                        lblRenderer.setText(g_sASK);
                    } else {
                        lblRenderer.setText((oPriceFormat.format(doubleValue)));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;

                case 'k':
                    if (isSelected) {
                        foreground = g_oSelectedFG;
                        background = g_oSelectedBG;
                        if (isStockModel) {
                            lblRenderer.setBorder(null);
                        }
                        lblRenderer.setText(" " + value + " ");
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    } else {
                        lblRenderer.setBackground(bidEvenBGColor); //table.getTableHeader().getBackground());
                        lblRenderer.setForeground(bidFGColor);
                        lblRenderer.setText(" " + value + " ");
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    }
                    break;
                case 'l':
                    if (isSelected) {
                        foreground = g_oSelectedFG;
                        background = g_oSelectedBG;
                        if (isStockModel) {
                            lblRenderer.setBorder(null);
                        }
                        lblRenderer.setText(" " + value + " ");
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    } else {
                        lblRenderer.setBackground(bidBGColor); //table.getTableHeader().getBackground());
                        lblRenderer.setForeground(bidFGColor);
                        lblRenderer.setText(" " + value + " ");
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    }
                    break;
                case 'm':
                    if (isSelected) {
                        foreground = g_oSelectedFG;
                        background = g_oSelectedBG;
                        if (isStockModel) {
                            lblRenderer.setBorder(null);
                        }
                        lblRenderer.setText(" " + value + " ");
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    } else {
                        lblRenderer.setBackground(offerEvenBGColor); //table.getTableHeader().getBackground());
                        lblRenderer.setForeground(offerFGColor);
                        lblRenderer.setText(" " + value + " ");
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    }
                    break;
                case 'x':
                    if (isSelected) {
                        foreground = g_oSelectedFG;
                        background = g_oSelectedBG;
                        if (isStockModel) {
                            lblRenderer.setBorder(null);
                        }
                        lblRenderer.setText(" " + value + " ");
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    } else {
                        lblRenderer.setBackground(offerBGColor); //table.getTableHeader().getBackground());
                        lblRenderer.setForeground(offerFGColor);
                        lblRenderer.setText(" " + value + " ");
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    }
                    break;
                case 'e':
                    if (isSelected) {
                        foreground = g_oSelectedFG;
                        background = g_oSelectedBG;
                        if (isStockModel) {
                            lblRenderer.setBorder(null);
                        }
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                            lblRenderer.setText(g_sNA);
                        } else {
                            lblRenderer.setText((oPriceFormat.format(doubleValue)));
                        }
                    } else {
                        lblRenderer.setBackground(bidEvenBGColor); //table.getTableHeader().getBackground());
                        lblRenderer.setForeground(bidFGColor);
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                            lblRenderer.setText(g_sNA);
                        } else {
                            lblRenderer.setText((oPriceFormat.format(doubleValue)));
                        }
                    }
                    break;
                case 'f':
                    if (isSelected) {
                        foreground = g_oSelectedFG;
                        background = g_oSelectedBG;
                        if (isStockModel) {
                            lblRenderer.setBorder(null);
                        }
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        lblRenderer.setText(oPctChangeFormat.format(doubleValue));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
//                        if (doubleValue > 0)
//                            lblRenderer.setForeground(changeUpColor);
//                        else if (doubleValue < 0)
//                            lblRenderer.setForeground(changeDownColor);
                    } else {
                        lblRenderer.setBackground(offerEvenBGColor); //table.getTableHeader().getBackground());
                        lblRenderer.setForeground(offerFGColor);
                        doubleValue = ((DoubleTransferObject) (value)).getValue();
                        lblRenderer.setText(oPctChangeFormat.format(doubleValue));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
//                        if (doubleValue > 0)
//                            lblRenderer.setForeground(changeUpColor);
//                        else if (doubleValue < 0)
//                            lblRenderer.setForeground(changeDownColor);
                    }
                    break;
                case 'g':
                    if (isSelected) {
                        foreground = g_oSelectedFG;
                        background = g_oSelectedBG;
                        if (isStockModel) {
                            lblRenderer.setBorder(null);
                        }
                        longValue = ((LongTransferObject) (value)).getValue();
                        updateDirection = ((LongTransferObject) (value)).getFlag();
                        now = System.currentTimeMillis();
                        lblRenderer.setText(oQuantityFormat.format(longValue));

                        if ((now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                            if (updateDirection > 0) {
                                lblRenderer.setBackground(valueUpBGColor);
                                lblRenderer.setForeground(valueUpFGColor);
                            } else if (updateDirection < 0) {
                                lblRenderer.setForeground(valueDownFGColor);
                                lblRenderer.setBackground(valueDownBGColor);
                            } else {
                                lblRenderer.setBorder(null);
                            }
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    } else {
                        lblRenderer.setBackground(bidBGColor); //table.getTableHeader().getBackground());
                        lblRenderer.setForeground(bidFGColor);
                        longValue = ((LongTransferObject) (value)).getValue();
                        updateDirection = ((LongTransferObject) (value)).getFlag();
                        now = System.currentTimeMillis();
                        lblRenderer.setText(oQuantityFormat.format(longValue));

                        if ((now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                            if (updateDirection > 0) {
                                lblRenderer.setBackground(valueUpBGColor);
                                lblRenderer.setForeground(valueUpFGColor);
                            } else if (updateDirection < 0) {
                                lblRenderer.setForeground(valueDownFGColor);
                                lblRenderer.setBackground(valueDownBGColor);
                            } else {
                                lblRenderer.setBorder(null);
                            }
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    }
                    break;
                case 'h':
                    if (isSelected) {
                        foreground = g_oSelectedFG;
                        background = g_oSelectedBG;
                        if (isStockModel) {
                            lblRenderer.setBorder(null);
                        }
                        longValue = ((LongTransferObject) (value)).getValue();
                        updateDirection = ((LongTransferObject) (value)).getFlag();
                        now = System.currentTimeMillis();
                        lblRenderer.setText(oQuantityFormat.format(longValue));

                        if ((now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                            if (updateDirection > 0) {
                                lblRenderer.setBackground(valueUpBGColor);
                                lblRenderer.setForeground(valueUpFGColor);
                            } else if (updateDirection < 0) {
                                lblRenderer.setForeground(valueDownFGColor);
                                lblRenderer.setBackground(valueDownBGColor);
                            } else {
                                lblRenderer.setBorder(null);
                            }
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    } else {
                        lblRenderer.setBackground(offerBGColor); //table.getTableHeader().getBackground());
                        lblRenderer.setForeground(offerFGColor);
                        longValue = ((LongTransferObject) (value)).getValue();
                        updateDirection = ((LongTransferObject) (value)).getFlag();
                        now = System.currentTimeMillis();
                        lblRenderer.setText(oQuantityFormat.format(longValue));

                        if ((now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) { // is recently changed
                            if (updateDirection > 0) {
                                lblRenderer.setBackground(valueUpBGColor);
                                lblRenderer.setForeground(valueUpFGColor);
                            } else if (updateDirection < 0) {
                                lblRenderer.setForeground(valueDownFGColor);
                                lblRenderer.setBackground(valueDownBGColor);
                            } else {
                                lblRenderer.setBorder(null);
                            }
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    }
                    break;
                case 'i':
                    if (isSelected) {
                        foreground = g_oSelectedFG;
                        background = g_oSelectedBG;
                        if (isStockModel) {
                            lblRenderer.setBorder(null);
                        }
                        lblRenderer.setText(" " + value + " ");
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    } else {
                        lblRenderer.setBackground(planeBGColor); //table.getTableHeader().getBackground());
                        lblRenderer.setForeground(planeFGColor);
                        lblRenderer.setText(" " + value + " ");
                        lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    }
                    break;
                case 'j':
                    if (isSelected) {
                        foreground = g_oSelectedFG;
                        background = g_oSelectedBG;
                        if (isStockModel) {
                            lblRenderer.setBorder(null);
                        }
                        longValue = ((LongTransferObject) (value)).getValue();
                        if (longValue <= Constants.ONE_DAY)
                            lblRenderer.setText(g_sNA);
                        else {
                            date.setTime(longValue + timeOffset);
                            lblRenderer.setText(g_oDateFormat.format(date));
                        }
                        lblRenderer.setHorizontalAlignment(g_iStringOppositeAlign);
                    } else {
                        lblRenderer.setBackground(planeBGColor); //table.getTableHeader().getBackground());
                        lblRenderer.setForeground(planeFGColor);
                        longValue = ((LongTransferObject) (value)).getValue();
                        if (longValue <= Constants.ONE_DAY)
                            lblRenderer.setText(g_sNA);
                        else {
                            date.setTime(longValue + timeOffset);
                            lblRenderer.setText(g_oDateFormat.format(date));
                        }
                        lblRenderer.setHorizontalAlignment(g_iStringOppositeAlign);
                    }
                    break;
                case 'u':
                    break;
                case 'w':
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            lblRenderer.setText("");
        }


        return lblRenderer;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }
}