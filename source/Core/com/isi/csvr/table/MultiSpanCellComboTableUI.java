package com.isi.csvr.table;

import com.isi.csvr.shared.Language;
import com.isi.csvr.portfolio.ValuationRecordAll;
import com.isi.csvr.portfolio.VAluationModelAll;
import com.isi.csvr.TableSorter;

import javax.swing.*;
import javax.swing.plaf.basic.BasicTableUI;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Aug 28, 2008
 * Time: 8:11:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class MultiSpanCellComboTableUI  extends BasicTableUI {

    public MultiSpanCellComboTableUI() {
        
    }

    public void paint(Graphics g, JComponent c) {
        Rectangle oldClipBounds = g.getClipBounds();
        Rectangle clipBounds = new Rectangle(oldClipBounds);
        int tableWidth = table.getColumnModel().getTotalColumnWidth();
        clipBounds.width = Math.min(clipBounds.width, tableWidth);
        g.setClip(clipBounds);

        int firstIndex = table.rowAtPoint(new Point(0, clipBounds.y));
        int lastIndex = table.getRowCount() - 1;

        Rectangle rowRect = new Rectangle(0, 0,
                tableWidth, table.getRowHeight() + table.getRowMargin());
        rowRect.y = firstIndex * rowRect.height;

        for (int index = firstIndex; index <= lastIndex; index++) {
            if (rowRect.intersects(clipBounds)) {
                paintRow(g, index);
            }
            rowRect.y += rowRect.height;
        }
        g.setClip(oldClipBounds);
    }

    private void paintRow(Graphics g, int row) {
        Rectangle rect = g.getClipBounds();
        // boolean drawn  = false;


        int numColumns = table.getColumnCount();
        int totRows = table.getRowCount();
        boolean isAlltable = false;
        if(table.getModel() instanceof TableSorter)
            isAlltable = ((TableSorter)table.getModel()).getModel() instanceof VAluationModelAll;
        int recordtype=-1;
        if(isAlltable){
        recordtype = (Integer)table.getModel().getValueAt(row,-3);
        }

        for (int column = 0; column < numColumns; column++) {
            Rectangle cellRect = table.getCellRect(row, column, true);

            if (row == totRows-1 && column<8) {
                if (Language.isLTR())
                 //   cellRect = combine(table.getCellRect(row, 0, true), table.getCellRect(row, 1, true));
                    cellRect = combine(table.getCellRect(row, 0, true), table.getCellRect(row, 1, true),table.getCellRect(row, 2, true),table.getCellRect(row, 3, true),table.getCellRect(row, 4, true),table.getCellRect(row, 5, true),table.getCellRect(row, 6, true),table.getCellRect(row, 7, true));
                else
                    cellRect = combine(table.getCellRect(row, 7, true),table.getCellRect(row, 6, true),table.getCellRect(row, 5, true),table.getCellRect(row, 4, true),table.getCellRect(row, 3, true),table.getCellRect(row, 2, true),table.getCellRect(row, 1, true), table.getCellRect(row, 0, true));
            }
            if( isAlltable && recordtype==ValuationRecordAll.PF_TOTAL_TYPE&& column<8 && row!=totRows-1  ){
                 if (Language.isLTR())
                 //   cellRect = combine(table.getCellRect(row, 0, true), table.getCellRect(row, 1, true));
                    cellRect = combine(table.getCellRect(row, 0, true), table.getCellRect(row, 1, true),table.getCellRect(row, 2, true),table.getCellRect(row, 3, true),table.getCellRect(row, 4, true),table.getCellRect(row, 5, true),table.getCellRect(row, 6, true),table.getCellRect(row, 7, true));
                else
                    cellRect = combine(table.getCellRect(row, 7, true),table.getCellRect(row, 6, true),table.getCellRect(row, 5, true),table.getCellRect(row, 4, true),table.getCellRect(row, 3, true),table.getCellRect(row, 2, true),table.getCellRect(row, 1, true), table.getCellRect(row, 0, true));
            }
//            if (row == 10) {
//                if (((column == 0) || (column == 1))) {
//                    if (Language.isLTR())
//                        cellRect = combine(table.getCellRect(row, 0, true), table.getCellRect(row, 1, true));
//                    else
//                        cellRect = combine(table.getCellRect(row, 1, true), table.getCellRect(row, 0, true));
//                } else if (((column == 2) || (column == 3))) {
//                    if (Language.isLTR())
//                        cellRect = combine(table.getCellRect(row, 2, true), table.getCellRect(row, 3, true));
//                    else
//                        cellRect = combine(table.getCellRect(row, 3, true), table.getCellRect(row, 2, true));
//                }
//            }
            if (cellRect.intersects(rect)) {
                //drawn = true;
                paintCell(g, cellRect, row, column);
            } //else {
            //   if (drawn) break;
            //}

        }
    }

    private void paintCell(Graphics g, Rectangle cellRect, int row, int column) {
        int spacingHeight = table.getRowMargin();
        int spacingWidth = table.getColumnModel().getColumnMargin();

        try {
            cellRect.setBounds(cellRect.x + spacingWidth / 2,
                    cellRect.y + spacingHeight / 2,
                    cellRect.width - spacingWidth, cellRect.height - spacingHeight);

            if (table.isEditing() && table.getEditingRow() == row &&
                    table.getEditingColumn() == column) {
                Component component = table.getEditorComponent();
                component.setBounds(cellRect);
                component.validate();
            } else {
                TableCellRenderer renderer = table.getCellRenderer(row, column);
                Component component = table.prepareRenderer(renderer, row, column);

                if (component.getParent() == null) {
                    rendererPane.add(component);
                }
                rendererPane.paintComponent(g, component, table, cellRect.x, cellRect.y,
                        cellRect.width, cellRect.height, true);
                if (table.getShowHorizontalLines()) {
                    g.setColor(table.getGridColor());
                    g.drawLine(cellRect.x, cellRect.y, cellRect.x + cellRect.width, cellRect.y);
                    if (table.getRowCount() == row + 1) {
                        g.drawLine(cellRect.x, cellRect.y + cellRect.height, cellRect.x + cellRect.width, cellRect.y + cellRect.height);
                    }
                }
                if (table.getShowVerticalLines()) {
                    g.setColor(table.getGridColor());
                    g.drawLine(cellRect.x, cellRect.y, cellRect.x, cellRect.y + cellRect.height);
                    if (table.getColumnCount() == column + 1) {
                        g.drawLine(cellRect.x + cellRect.width, cellRect.y, cellRect.x + cellRect.width, cellRect.y);
                    }
                }
            }
        } catch (Exception e) {

        }
    }

    private Rectangle combine(Rectangle sRect1, Rectangle sRect2) {
        Rectangle sRect = new Rectangle((int) sRect1.getX(), (int) sRect1.getY(),
                (int) sRect1.getWidth() + (int) sRect2.getWidth(), (int) sRect1.getHeight());
        return sRect;
    }

    private Rectangle combine(Rectangle sRect1, Rectangle sRect2, Rectangle sRect3, Rectangle sRect4) {
        Rectangle sRect = new Rectangle((int) sRect1.getX(), (int) sRect1.getY(),
                (int) sRect1.getWidth() + (int) sRect2.getWidth() +
                        (int) sRect3.getWidth() + (int) sRect4.getWidth(), (int) sRect1.getHeight());
        return sRect;
    }
    private Rectangle combine(Rectangle sRect1, Rectangle sRect2, Rectangle sRect3, Rectangle sRect4, Rectangle sRect5, Rectangle sRect6, Rectangle sRect7, Rectangle sRect8){
         Rectangle sRect = new Rectangle((int) sRect1.getX(), (int) sRect1.getY(),
                (int) sRect1.getWidth() + (int) sRect2.getWidth() +
                        (int) sRect3.getWidth() + (int) sRect4.getWidth() + (int) sRect5.getWidth() + (int) sRect6.getWidth() + (int) sRect7.getWidth() + (int) sRect8.getWidth(), (int) sRect1.getHeight());
        return sRect;
    }
    
}
