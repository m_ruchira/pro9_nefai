package com.isi.csvr.table;

import com.isi.csvr.shared.Constants;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jul 6, 2005
 * Time: 5:07:18 PM
 */
public interface UpdateableTable {

    public void runThread();

    public String getTitle();

    public Constants.ThreadTypes getTableID();
}
