package com.isi.csvr.table;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 18, 2004
 * Time: 10:56:51 AM
 * To change this template use File | Settings | File Templates.
 */
public interface DetailQuote {
    public int getRenderingID(int iRow, int iCol);

    public long getTimeOffset();
}
