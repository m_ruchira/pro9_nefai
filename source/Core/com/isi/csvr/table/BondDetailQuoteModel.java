package com.isi.csvr.table;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.LongTransferObject;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: Jul 28, 2009
 * Time: 10:24:57 AM
 * To change this template use File | Settings | File Templates.
 */
public class BondDetailQuoteModel extends CommonTable
        implements DetailQuote, TableModel, CommonTableInterface {

    private String symbol;
    private long timeOffset;

    private static LongTransferObject longTrasferObject = new LongTransferObject();
    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();

    /**
     * Constructor
     */
    public BondDetailQuoteModel() {
    }

    public void setSymbol(String symbolIn) {
        symbol = symbolIn;
    }

    /* --- Table Model methods start from here --- */

    public int getColumnCount() {
        return 4;
    }

    public int getRowCount() {
        return 9;
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            if (symbol == null)
                return "";
            Stock stock = DataStore.getSharedInstance().getStockObject(symbol);
            timeOffset = stock.getTimeOffset();
            if (stock == null)
                return "";

            //---------------------------- new Changes ---------------- Shanika ----

            switch (iRow) {
                case -2:
                    return true;
                case -1:
                    return stock.getInstrumentType();
                case 0:
                    switch (iCol) {
                        case 0:
                            return Language.getString("BONDS_LAST_TRADE");
                        case 1:
                            doubleTransferObject.setFlag(stock.getLastTradeFlag());
                            return doubleTransferObject.setValue(stock.getLastTradeValue());
                        case 2:
                            return Language.getString("YIELD");
                        case 3:
                            return doubleTransferObject.setValue(stock.getYield());

                    }
                case 1:
                    switch (iCol) {
                        case 0:
                            return Language.getString("CHANGE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getChange());
                        case 2:
                            return Language.getString("PCT_CHANGE");
                        case 3:
                            return doubleTransferObject.setValue(stock.getPercentChange());
                    }
                case 2:
                    switch (iCol) {
                        case 0:
                            return Language.getString("COUPON_RATE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getCouponRate());
                        case 2:
                            return Language.getString("COUPON_FREQ");
                        case 3:
                            return doubleTransferObject.setValue(stock.getCouponFrequancy());
                    }
                case 3:
                    switch (iCol) {
                        case 0:
                            return Language.getString("FACE_VALUE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getFaceValue());
                        case 2:
                            return Language.getString("OUTSTANDING_AMT");
                        case 3:
                            return doubleTransferObject.setValue(stock.getOutstandingAmount());

                    }
                case 4:
                    switch (iCol) {
                        case 0:
                            return Language.getString("BONDS_BEST_BID");
                        case 1:
                            doubleTransferObject.setFlag(stock.getBestBidPriceFlag());
                            return doubleTransferObject.setValue(stock.getBestBidPrice());
                        case 2:
                            return Language.getString("BONDS_BEST_ASK");
                        case 3:
                            doubleTransferObject.setFlag(stock.getBestAskPriceFlag());
                            return doubleTransferObject.setValue(stock.getBestAskPrice());

                    }
                case 5:
                    switch (iCol) {
                        case 0:
                            return Language.getString("BONDS_BEST_BID_QUANTITY");
                        case 1:
                            return longTrasferObject.setValue(stock.getBestBidQuantity());
                        case 2:
                            return Language.getString("BONDS_BEST_ASK_QUANTITY");
                        case 3:
                            return longTrasferObject.setValue(stock.getBestAskQuantity());

                    }
                case 6:
                    switch (iCol) {
                        case 0:
                            return Language.getString("MATUARITY_DATE");
                        case 1:
                            return longTrasferObject.setValue(stock.getMatuarityDate());
                        case 2:
                            return Language.getString("SETTLEMENT_DATE");
                        case 3:
                            return longTrasferObject.setValue(stock.getSettlementDate());

                    }
                case 7:
                    switch (iCol) {
                        case 0:
                            return Language.getString("DATE_OF_ISSUE");
                        case 1:
                            return longTrasferObject.setValue(stock.getDateofIssue());
                        case 2:
                            return Language.getString("NEXT_COUPON_DATE");
                        case 3:
                            return longTrasferObject.setValue(stock.getCouponDate());
                    }
                case 8:
                    switch (iCol) {
                        case 0:
                            return Language.getString("ISSUE_AMOUNT");
                        case 1:
                            return doubleTransferObject.setValue(stock.getIssuedAmount());
                        case 2:
                            return Language.getString("DAY_COUNT_METHOD");
                        case 3:
                            return setDayCount(stock.getDayCountMethod());
                    }
            }

            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public int getRenderingID(int iRow, int iCol) {
        switch (iRow) {
            case 0:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 1:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 'P';

                }
            case 2:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 3;

                }
            case 3:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 3;

                }
            case 4:
                switch (iCol) {
                    case 0:
                        return 'k';
                    case 1:
                        return 'e';  //3

                    case 2:
                        return 'm';
                    case 3:
                        return 'f';

                }
            case 5:
                switch (iCol) {
                    case 0:
                        return 'l';
                    case 1:
                        return 'g';   //Q
                    case 2:
                        return 'x';
                    case 3:
                        return 'h';
                }
            case 6:
                switch (iCol) {
                    case 0:
                        return 'i';
                    case 1:
                        return 'j';
                    case 2:
                        return 'i';
                    case 3:
                        return 'j';
                }
            case 7:
                switch (iCol) {
                    case 0:
                        return 'i';
                    case 1:
                        return 'j';
                    case 2:
                        return 'i';
                    case 3:
                        return 'j';

                }
            case 8:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 1;

                }
        }
        return 0;
    }

    public long getTimeOffset() {
        return timeOffset;
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Model metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[7];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 3);
        customizerRecords[3] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_VALUE_UP_ROW, Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR"), Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[6] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_VALUE_DOWN_ROW, Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR"), Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }

    private String setDayCount(double val) {
        String value = null;
        if (val == 1) {
            value = "30E_360";
        } else if (val == 2) {
            value = "30U_360";
        } else if (val == 3) {
            value = "ACT_360";
        } else if (val == 4) {
            value = "ACT_365";
        } else if (val == 5) {
            value = "ACT_ACT";
        } else if (val == 6) {
            value = "ACT_PER";
        } else {
            value = "N/A";
        }
        return value;
    }
}
