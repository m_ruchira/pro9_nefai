package com.isi.csvr.table;

import com.isi.csvr.shared.Constants;

import javax.swing.*;
import java.awt.*;
import java.util.EventObject;

public class LongCellEditor extends DefaultCellEditor {

    private JTextField textField;
    //private DecimalFormat oPriceFormat     = new DecimalFormat("###,##0.00");
    private boolean clearValue = false;

    public LongCellEditor(JTextField txt) {
        super(txt);
        textField = txt;
    }

    public Object getCellEditorValue() {
        return textField.getText();
    }

    public Component getTableCellEditorComponent(JTable table, Object value,
                                                 boolean isSelected, int row, int column) {
        if (clearValue)
            textField.setText(Constants.NULL_STRING);
        else
            textField.setText("" + ((Long) (value)).longValue());

        return textField;
    }

    public boolean isCellEditable(EventObject anEvent) {
        if (anEvent == null) {
            clearValue = true;
        } else {
            clearValue = false;
        }
        return super.isCellEditable(anEvent);
    }


}