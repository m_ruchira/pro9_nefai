package com.isi.csvr.table;

import com.isi.csvr.SortButtonRenderer;
import com.isi.csvr.Client;
import com.isi.csvr.trade.AllTradesModel;
import com.isi.csvr.trade.TimeNSalesModel;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.mist.MISTTable;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.bandwidthoptimizer.OptimizedTreeRenderer;
import com.isi.csvr.bandwidthoptimizer.MainUI;
import com.isi.csvr.datastore.StockData;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.shared.*;
import com.isi.csvr.customizer.CommonTableSettings;

import javax.swing.*;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;
import javax.swing.text.Document;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableModel;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Enumeration;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

public class SmartTable extends JTable
        implements ActionListener, ClipboardOwner, PropertyChangeListener {

    private boolean customThemeEnabled = false;
    private boolean isHeaderOn = true;
    private boolean useSameFontForHeader = false;
    private boolean gridOn;
    private Object tableSettings;
    private int[] numFixedColumns;
    private Clipboard clip;
    private byte decimalPlaces = 2;
    private int bodyGap = 2;

    public SmartTable(TableModel oDataModel) {
        this(oDataModel, new int[]{0});
    }

    public SmartTable(TableModel oDataModel, int[] numFixedColumns) {
        super(oDataModel);

        this.numFixedColumns = numFixedColumns;
        //setCellSelectionEnabled(true);
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);

        clip = getToolkit().getSystemClipboard();
        // register the keyboard action to copy cells
        this.registerKeyboardAction(this, "COPY", KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK), JComponent.WHEN_FOCUSED);
        // register the keyboard action to copy cells with the column heading
        this.registerKeyboardAction(this, "COPYwHEAD", KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_DOWN_MASK), JComponent.WHEN_FOCUSED);
        // register the keyboard action to link cells
        setShowGrid(false);

        setCellSelectionEnabled(true);
        setRowSelectionAllowed(true);
        setColumnSelectionAllowed(false);

        //setGridColor(Color.red);
        //setShowVerticalLines(true);
        //setShowHorizontalLines(false);
        //setIntercellSpacing(new Dimension(1,1));
        setIntercellSpacing(new Dimension(0, 0));
        getTableHeader().addPropertyChangeListener("tableColumnResizeEvent", this);
    }

    public SmartTable() {
        this(new int[]{0});
    }

    public SmartTable(int[] numFixedColumns) {
        super();

        this.numFixedColumns = numFixedColumns;

        //setCellSelectionEnabled(true);
        setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        clip = getToolkit().getSystemClipboard();
        // register the keyboard action to copy cells
        this.registerKeyboardAction(this, "COPY", KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK), JComponent.WHEN_FOCUSED);
        // register the keyboard action to copy cells with the column heading
        this.registerKeyboardAction(this, "COPYwHEAD", KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_DOWN_MASK), JComponent.WHEN_FOCUSED);
        // register the keyboard action to link cells

        setShowGrid(false);

        setCellSelectionEnabled(true);
        setRowSelectionAllowed(true);
        setColumnSelectionAllowed(false);

        getTableHeader().addPropertyChangeListener("tableColumnResizeEvent", this);

        //setGridColor(Color.red);
        //setShowVerticalLines(true);
        //setShowHorizontalLines(false);
        //setIntercellSpacing(new Dimension(4,4));

    }

    public void setDecimalPlaces(byte decimalPlaces) {
        this.decimalPlaces = decimalPlaces;
    }

    public byte getDecimalPlaces() {
        return decimalPlaces;
    }

    protected JTableHeader createDefaultTableHeader() {
        return new TWTableHeader(columnModel);
    }

    /*public void setShowGrid(boolean status){
        super.setShowGrid(true);
    }*/

    /*public boolean getShowGrid(){
        return true;
    }*/

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("COPY")) {
            copyCells(false);
        } else {
            e.getActionCommand().equals("COPYwHEAD");
            copyCells(true);
        }
    }

    public void disableMultipleSybols(boolean copyHead) {
        int col;
        int cols;
        int row = this.getSelectedRow();
        int rows = this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        String symbol = null;
        String exchange = null;
        int instrument = -1;
        if (getCellSelectionEnabled()) {
            col = this.getSelectedColumn();
            cols = this.getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
//        Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();

        Hashtable<String, String> selectedExchanges = new Hashtable<String, String>();
        ArrayList filteredSymbols = new ArrayList();
        for (int r = 0; r < rows; r++) {
            try {
                symbol = ((StringTransferObject) this.getModel().getValueAt(row + r, -3)).getValue();
                exchange = ((StringTransferObject) this.getModel().getValueAt(row + r, -2)).getValue();
                instrument = (int) ((LongTransferObject) this.getModel().getValueAt(row + r, -1)).getValue();


                Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
                selectedExchanges.put(stock.getExchange(), stock.getExchange());
                filteredSymbols.add(stock);
                Client.needToReconnect = false;

//                if (OptimizedTreeRenderer.selectedSymbols.containsKey(stock.getKey())) {
//                    int instrument_type = SharedMethods.getInstrumentFromExchangeKey(symbol);
//                    if (exchangeRequests.containsKey(instrument_type)) {
//                        ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(instrument_type);
//                        gettingArrayFromHash.add(stock);
//                    } else {
//                        ArrayList<Stock> freshArray = new ArrayList<Stock>();
//                        freshArray.add(stock);
//                        exchangeRequests.put(instrument_type, freshArray);
//                    }
//                    MainUI.selectedSymbols.put(stock.getKey(), false);
//                    OptimizedTreeRenderer.selectedSymbols.remove(stock.getKey());
//                    stock.setSymbolEnabled(false);
//                }

                buffer.append("\n");
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        Enumeration<String> exkeys = selectedExchanges.keys();
        while (exkeys.hasMoreElements()) {
            Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();

            String currenetEx = exkeys.nextElement();
            for (int j = 0; j < filteredSymbols.size(); j++) {
                Stock newStock = (Stock) filteredSymbols.get(j);
                if (newStock.getExchange().equals(currenetEx)) {
                    if (OptimizedTreeRenderer.selectedSymbols.containsKey(newStock.getKey())) {
                    int instrument_type = SharedMethods.getInstrumentFromExchangeKey(symbol);
                    if (exchangeRequests.containsKey(instrument_type)) {
                        ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(instrument_type);
                            gettingArrayFromHash.add(newStock);
                    } else {
                        ArrayList<Stock> freshArray = new ArrayList<Stock>();
                            freshArray.add(newStock);
                        exchangeRequests.put(instrument_type, freshArray);
                    }
                        MainUI.selectedSymbols.put(newStock.getKey(), false);
                        OptimizedTreeRenderer.selectedSymbols.remove(newStock.getKey());
                        newStock.setSymbolEnabled(false);
                }
            }
        }
        Enumeration<Integer> keys = exchangeRequests.keys();
        while (keys.hasMoreElements()) {
            String stockExchange = "";
            Integer key = keys.nextElement();
            ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(key);

            int arraySize = (gettingArrayFromHash.size() / 100);
            int reqDevider = arraySize + 1;

            int position = 0;
            int max = gettingArrayFromHash.size();

            for (int j = 1; j <= reqDevider; j++) {
                ArrayList exchangeSymbols = new ArrayList();

                int currentPos;
                if ((j * 100) > max) {
                    currentPos = max;
                } else {
                    currentPos = (j * 100);
                }
                for (int i = position; i < currentPos; i++) {
                    Stock fulKey = gettingArrayFromHash.get(i);
                    stockExchange = fulKey.getExchange();
                    int messageType = SharedMethods.getSymbolType(fulKey.getInstrumentType());
                    Exchange exx = ExchangeStore.getSharedInstance().getExchange(fulKey.getExchange());
                    if ((exx.isUserSubMarketBreakdown()) && (exx.getSubMarketCount() > 0)) {
                        exchangeSymbols.add(fulKey.getSymbolCode() + ":" + fulKey.getInstrumentType());
                    } else {
                        exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
                    }
                    position = position + 1;
                }
                String selectedExchangesSymbols = "";
                for (int f = 0; f < exchangeSymbols.size(); f++) {
                    selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
                }

                String exchangeAddRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);
            SendQFactory.removeOptimizationAllRecord(key, stockExchange, exchangeAddRequest);
        }
        }
            
        }

        if (Client.needToReconnect) {
            MainUI.save_BandWidthOptimized_Symbols();
            MainUI.saveSettings();
            MainUI.selectedSymbols = new Hashtable<String, Boolean>();
            Client.needToReconnect = false;
            Client.WATCHLIST_SELECT = false;
            Client.getInstance().disconnectFromServer();
        }
        symbol = null;
        buffer = null;
    }


    public void enableMultipleSybols(boolean copyHead) {
        int col;
        int cols;
        int row = this.getSelectedRow();
        int rows = this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        String symbol = null;
        String exchange = null;
        int instrument = -1;
        if (getCellSelectionEnabled()) {
            col = this.getSelectedColumn();
            cols = this.getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }


        int modelIndex;
//        Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();

        Hashtable<String, String> selectedExchanges = new Hashtable<String, String>();
        ArrayList filteredSymbols = new ArrayList();
        for (int r = 0; r < rows; r++) {
            try {
                symbol = ((StringTransferObject) this.getModel().getValueAt(row + r, -3)).getValue();
                exchange = ((StringTransferObject) this.getModel().getValueAt(row + r, -2)).getValue();
                instrument = (int) ((LongTransferObject) this.getModel().getValueAt(row + r, -1)).getValue();


                Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);

                selectedExchanges.put(stock.getExchange(), stock.getExchange());
                filteredSymbols.add(stock);
                Client.needToReconnect = false;

                /*  if (!OptimizedTreeRenderer.selectedSymbols.containsKey(stock.getKey())) {
                    int instrument_type = SharedMethods.getInstrumentFromExchangeKey(symbol);
                    if (exchangeRequests.containsKey(instrument_type)) {
                        ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(instrument_type);
                        gettingArrayFromHash.add(stock);
                    } else {
                        ArrayList<Stock> freshArray = new ArrayList<Stock>();
                        freshArray.add(stock);
                        exchangeRequests.put(instrument_type, freshArray);
                    }
                    MainUI.selectedSymbols.put(stock.getKey(), true);

                    OptimizedTreeRenderer.selectedSymbols.put(stock.getKey(), true);
                    stock.setSymbolEnabled(true);
                }*/

                buffer.append("\n");
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        Enumeration<String> exkeys = selectedExchanges.keys();
        while (exkeys.hasMoreElements()) {
            Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();

            String currenetEx = exkeys.nextElement();
            for (int j = 0; j < filteredSymbols.size(); j++) {
                Stock newStock = (Stock) filteredSymbols.get(j);
                if (newStock.getExchange().equals(currenetEx)) {
//                    if (!OptimizedTreeRenderer.selectedSymbols.containsKey(stock.getKey())) {
                    int instrument_type = SharedMethods.getInstrumentFromExchangeKey(symbol);
                    if (exchangeRequests.containsKey(instrument_type)) {
                        ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(instrument_type);
                        gettingArrayFromHash.add(newStock);
                    } else {
                        ArrayList<Stock> freshArray = new ArrayList<Stock>();
                        freshArray.add(newStock);
                        exchangeRequests.put(instrument_type, freshArray);
                    }

                    MainUI.selectedSymbols.put(newStock.getKey(), true);
                    OptimizedTreeRenderer.selectedSymbols.put(newStock.getKey(), true);
                    newStock.setSymbolEnabled(true);
//                                }
                }
            }

        Enumeration<Integer> keys = exchangeRequests.keys();
        while (keys.hasMoreElements()) {
            String stockExchange = "";
            Integer key = keys.nextElement();
            ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(key);

            int arraySize = (gettingArrayFromHash.size() / 100);
            int reqDevider = arraySize + 1;

            int position = 0;
            int max = gettingArrayFromHash.size();

            for (int j = 1; j <= reqDevider; j++) {
            ArrayList exchangeSymbols = new ArrayList();

                int currentPos;
                if ((j * 100) > max) {
                    currentPos = max;
                } else {
                    currentPos = (j * 100);
                }
                for (int i = position; i < currentPos; i++) {
                Stock fulKey = gettingArrayFromHash.get(i);
                stockExchange = fulKey.getExchange();
                    int messageType = SharedMethods.getSymbolType(fulKey.getInstrumentType());
                    Exchange exx = ExchangeStore.getSharedInstance().getExchange(fulKey.getExchange());
                    if ((exx.isUserSubMarketBreakdown()) && (exx.getSubMarketCount() > 0)) {
                        exchangeSymbols.add(fulKey.getSymbolCode() + ":" + fulKey.getInstrumentType());
                    } else {
                exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
            }
                    position = position + 1;
                }
            String selectedExchangesSymbols = "";
            for (int f = 0; f < exchangeSymbols.size(); f++) {
                selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
            }
            String exchangeAddRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);
            SendQFactory.addOptimizationAllRecord(key, stockExchange, exchangeAddRequest);
            }
        }
        }
        if (Client.needToReconnect) {
            MainUI.save_BandWidthOptimized_Symbols();
            MainUI.saveSettings();
            Client.needToReconnect = false;
            Client.WATCHLIST_SELECT = false;
            Client.getInstance().disconnectFromServer();
        }

        symbol = null;
        buffer = null;
    }


    public void setEnableMultipleSymbols() {
        int col;
        int cols;
        int row = this.getSelectedRow();
        int rows = this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        String symbol = null;
        String exchange = null;
        int instrument = -1;
        if (getCellSelectionEnabled()) {
            col = this.getSelectedColumn();
            cols = this.getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        int modelIndex;
        Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();

        if (rows > 1) {
            for (int r = 0; r < rows; r++) {
                try {
                    symbol = ((StringTransferObject) this.getModel().getValueAt(row + r, -3)).getValue();
                    exchange = ((StringTransferObject) this.getModel().getValueAt(row + r, -2)).getValue();
                    instrument = (int) ((LongTransferObject) this.getModel().getValueAt(row + r, -1)).getValue();


                    Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);

                    if (!ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()) {
                        Client.isAllDefault = false;
                    }

                    if (!OptimizedTreeRenderer.selectedSymbols.containsKey(stock.getKey())) {
//                    OptimizedTreeRenderer.selectedSymbols.put(stock.getKey(), true);

                        Client.isSelected = true;
//                    exchangeSymbols.add(stock.getSymbol()+":"+stock.getInstrumentType());
                    }

                    buffer.append("\n");
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }


        symbol = null;
        buffer = null;
    }

    public void copyCells(boolean copyHead) {
        int col;
        int cols;
        Document d;
        int row = this.getSelectedRow();
        int rows = this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        //String symbol = null;

        if (getCellSelectionEnabled()) {
            col = this.getSelectedColumn();
            cols = this.getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        if (copyHead)
            buffer.append(copyHeaders());

        int modelIndex;
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                modelIndex = this.convertColumnIndexToModel(col + c);
                if (this.getColumn("" + modelIndex).getWidth() > 2) {
                    JLabel comp = null;
                    try {
                        comp = (JLabel) getCellRenderer(r + row, col + c).getTableCellRendererComponent(this,
                                getModel().getValueAt(r + row, modelIndex), false, false, r + row, col + c);
                        View v = (View) comp.getClientProperty(BasicHTML.propertyKey);
                        if (v != null) {
                            d = v.getDocument();
                            buffer.append(d.getText(0, d.getLength()).trim());
                        }else if(getModel() instanceof TimeNSalesModel){
                            buffer.append(((TimeNSalesModel)getModel()).getValue(r + row, modelIndex));
                        }else if(getModel() instanceof AllTradesModel){
                            buffer.append(((AllTradesModel)getModel()).getValue(r + row, modelIndex));
                        }
                        else {
                            buffer.append(comp.getText());
                        }

//                        buffer.append(comp.getText());
                    } catch (Exception e) {
                        buffer.append("");
                    }
                    if (c != (cols - 1))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                    comp = null;
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        //symbol = null;
        buffer = null;
    }

    public String copyHeaders() {
        int col;
        int cols;
        StringBuffer buffer = new StringBuffer("");

        if (getCellSelectionEnabled()) {
            col = this.getSelectedColumn();
            cols = this.getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        int modelIndex;
        for (int c = 0; c < cols; c++) {
            modelIndex = this.convertColumnIndexToModel(col + c);
            if (this.getColumn("" + modelIndex).getWidth() > 2) {
                buffer.append((String) this.getColumn("" + modelIndex).getHeaderValue());
                if (c != (cols - 1))  // do not append the tab char to the last item of the row
                    buffer.append("\t");
            }
        }
        buffer.append("\n");

        String temp = buffer.toString().replaceAll("<html><p align=center>", "");
        temp = temp.replaceAll("<br>", " / ");
        return temp;
    }

    public int adjustColumnWidthsToFit(int minWidth) {
        return adjustColumnWidthsToFit(minWidth, 0, this.getColumnCount());
    }

    public int adjustColumnWidthsToFit(int minWidth, int start, int end) {
        return adjustColumnWidthsToFit(minWidth, start, end, this.getRowCount());
    }
    
    public int adjustColumnWidthsToFit(int minWidth, int start, int end, int rowCount) {
        int[] colWidths = new int[this.getColumnCount()];
        int model;
        String text;
        Document d;

        Arrays.fill(colWidths, minWidth);
//        int spacing = (int) getIntercellSpacing().getWidth();


        JLabel comp = null;
        Insets insets = null;
        int gap = 0;
        try {
            comp = (JLabel) getCellRenderer(0, 0).getTableCellRendererComponent(this, getModel().getValueAt(0, 0), false, false, 0, 0);
            insets = comp.getInsets();
            gap = insets.left + insets.right;
        } catch (Exception e) {
            gap = 5;
        }
        //insets = null;

        FontMetrics fMetrics = null;
        try {
            //JLabel headerRenderer = getCo
            //(JLabel)getColumnModel().getColumn(0).getHeaderRenderer();
            fMetrics = getFontMetrics(getTableHeader().getFont());
            gap += 15;
            //(headerRenderer.getBorder().getBorderInsets(headerRenderer).left*2);

            for (int column = start; column < end; column++) {
                //model = this.convertColumnIndexToModel(column);

                try {
                    String colName  = (String) getColumnModel().getColumn(column).getHeaderValue();  /*Issue due to groupable columns fit to width,corrected by getting column names from Column.
                                                                                                     Earlier it was taken from viewsettings .. --Shanika -- 2009.07.08*/
//                  if ((fMetrics.stringWidth(getColumnName(column)) + gap) > colWidths[column]) {
                    if ((fMetrics.stringWidth(colName) + gap) > colWidths[column]) {
                        colWidths[column] = fMetrics.stringWidth(colName) + gap;
                    }
                } catch (Exception ex) {
                }
            }
            //headerRenderer = null;
        } catch (Exception ex) {
            // this
        }

        if (getRowCount() > 0) {
            fMetrics = getFontMetrics(getFont());
            gap = insets.left + insets.right + 2;

//        String text;
            for (int row = 0; row < rowCount; row++) {
                for (int column = start; column < end; column++) {
                    try {
                        model = this.convertColumnIndexToModel(column);
                        if (getColumnModel().getColumn(column).getWidth() > 0) {
                            comp = (JLabel) getCellRenderer(row, column).getTableCellRendererComponent(this,
                                    getModel().getValueAt(row, model), false, false, row, column);

                            View v = (View) comp.getClientProperty(BasicHTML.propertyKey);
                            if (v != null) {
                                d = v.getDocument();
                                text = d.getText(0, d.getLength()).trim();
                            } else {
                                text = comp.getText();
                            }

                            if ((fMetrics.stringWidth(text) + gap) > colWidths[column]) {
                                colWidths[column] = fMetrics.stringWidth(text) + gap;
                            }
                        }
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            }
        }

        for (int column = start; column < end; column++) {
            if (this.getColumnModel().getColumn(column).getPreferredWidth() > 0)
                this.getColumnModel().getColumn(column).setPreferredWidth(colWidths[column]);
        }

        updateUI();
        return (int) getTableHeader().getPreferredSize().getWidth();
    }


    public int getBodyGap() {
        return bodyGap;
    }

    public void setBodyGap(int bodyGap) {
        this.bodyGap = bodyGap;
        try {
            FontMetrics oMetrices = getGraphics().getFontMetrics();
            setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + bodyGap);
            updateUI();
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public int adjustColumnWidthsToFitinMIST(int minWidth) {
        int columnCount = this.getColumnCount();
        int[] colWidths = new int[2 * columnCount];
        int model;
        String text;
        Document d;

        Arrays.fill(colWidths, minWidth);
        int spacing = (int) getIntercellSpacing().getWidth();

        JLabel comp = (JLabel) getCellRenderer(0, 0).getTableCellRendererComponent(this, getModel().getValueAt(0, 0), false, false, 0, 0);
        Insets insets = comp.getInsets();
        int gap = insets.left + insets.right;

        FontMetrics fMetrics = null;
        try {
            fMetrics = getFontMetrics(getTableHeader().getFont());
//            gap += 2;

            for (int column = 0; column < columnCount; column++) {
                try {
                    String temp = (getColumnName(column)).replace("<html><p align=center>", "");
                    temp = temp.replace("<br>", " / ");
                    String[] cols = temp.split("/");
                    String selectedcolumn = "";
                    for (String col : cols) {
                        if ((fMetrics.stringWidth(selectedcolumn)) < (fMetrics.stringWidth(col))) {
                            selectedcolumn = col;
                        }
                    }
//                    System.out.println("Component = "+ selectedcolumn + " width = "+(fMetrics.stringWidth(selectedcolumn) + (gap*(selectedcolumn.toCharArray().length))));
                    if ((fMetrics.stringWidth(selectedcolumn) + (gap * (selectedcolumn.toCharArray().length))) > colWidths[2 * column]) { //gap*(getIndexSymbol(column)).toCharArray().length)
                        colWidths[(2 * column)] = fMetrics.stringWidth(selectedcolumn) + (gap * (selectedcolumn.toCharArray().length));
                        colWidths[(2 * column + 1)] = fMetrics.stringWidth(selectedcolumn) + (gap * (selectedcolumn.toCharArray().length));
                    }
                } catch (Exception ex) {
                }
            }
        } catch (Exception ex) {
        }

        fMetrics = getFontMetrics(getFont());
        gap = insets.left + insets.right; // + 2;

//        String text;
        for (int row = 0; row < getRowCount() - 1; row = row + 2) {
//            if(row%2==0){
            for (int column = 0; column < columnCount; column++) {
                try {
                    model = this.convertColumnIndexToModel(column);
                    if (getColumnModel().getColumn(model).getWidth() > 0) {
                        comp = (JLabel) getCellRenderer(row, column).getTableCellRendererComponent(this,
                                getModel().getValueAt(row, model), false, false, row, column);
/*                            System.out.println("Comp = "+ comp.getText());*/
                        View v = (View) comp.getClientProperty(BasicHTML.propertyKey);
                        if (v != null) {
                            d = v.getDocument();
                            text = d.getText(0, d.getLength()).trim();
                        } else {
                            text = comp.getText();
                        }
//                            System.out.println("Char count = "+text.toCharArray().length);
//                            System.out.println("Text = "+ (fMetrics.stringWidth(text) + (gap*(text.toCharArray().length))));
//                            System.out.println("Col with = "+ colWidths[2*column ]);
                        if ((fMetrics.stringWidth(text) + (gap * (text.toCharArray().length)) + 2) > colWidths[2 * column]) {
                            colWidths[2 * column] = (fMetrics.stringWidth(text) + (gap * (text.toCharArray().length)) + 2);
                        }
//                            System.out.println("Col with = "+ colWidths[2*column]+" for column = "+ 2*column);
                    }
                } catch (Exception ex) {
                    //ex.printStackTrace();
                }
            }
//            }else{

            for (int column2 = 0; column2 < columnCount; column2++) {
                try {
                    model = this.convertColumnIndexToModel(column2);
                    if (getColumnModel().getColumn(model).getWidth() > 0) {
                        comp = (JLabel) getCellRenderer(row + 1, column2).getTableCellRendererComponent(this,
                                getModel().getValueAt(row + 1, model), false, false, row + 1, column2);
//                            System.out.println("Comp = "+ comp.getText());
                        View v = (View) comp.getClientProperty(BasicHTML.propertyKey);
                        if (v != null) {
                            d = v.getDocument();
                            text = d.getText(0, d.getLength()).trim();
                        } else {
                            text = comp.getText();
                        }
//                            System.out.println("Text = "+ (fMetrics.stringWidth(text) + (gap*(text.toCharArray().length))));
//                            System.out.println("Col with = "+ colWidths[(2*column2 +1)]);
                        if ((fMetrics.stringWidth(text) + (gap * (text.toCharArray().length)) + 2) > colWidths[(2 * column2 + 1)]) {
                            colWidths[(2 * column2 + 1)] = (fMetrics.stringWidth(text) + (gap * (text.toCharArray().length)) + 2);
                        }
//                            System.out.println("Col with = "+ colWidths[(2*column2 +1)] + " for column = "+  (2*column2 +1));
                    }
                } catch (Exception ex) {
                    //ex.printStackTrace();
                }
            }
//            }
        }
        int i = 1;
        for (int column = 0; column < colWidths.length; column = column + i) {
            int firstrowWith = 0;
            int secondrowWith = 0;
            int firstRowGap = 0;
            int secondRowGap = 0;

            if (column == 0) {
                firstrowWith = colWidths[column + 2];
                secondrowWith = colWidths[column + 1] + colWidths[column + 3];
//                System.out.println("first row = "+ firstrowWith);
//                System.out.println("second row = "+ secondrowWith);
//                System.out.println("col1 = "+ (colWidths[column +1]));
//                System.out.println("col2 = "+ (colWidths[column +3]));
                i = 4;
            } else {
                firstrowWith = colWidths[column]; //+ colWidths[column +2] + colWidths[column +4];
                secondrowWith = colWidths[column + 1]; // + colWidths[column +3] + colWidths[column +5];
                i = 2; //6;
            }
            if (firstrowWith > secondrowWith) {
                firstRowGap = 0;
                secondRowGap = (firstrowWith - secondrowWith) * 2 / i + 1;
            } else if (secondrowWith > firstrowWith) {
                firstRowGap = (secondrowWith - firstrowWith) * 2 / i + 1;
                secondRowGap = 0;
            } else {
                firstRowGap = 0;
                secondRowGap = 0;
            }
            int k = 0;
            if (firstRowGap > 0) {
                for (int j = 0; j < i / 2; j++) {
//                    System.out.println("Column = "+(column/2+k) );
//                    System.out.println("Set with = "+ (colWidths[column + (2*k +1)]));
                    this.getColumnModel().getColumn((column / 2 + k)).setPreferredWidth(colWidths[column + (2 * k + 1)]);
                    k++;
                }
            } else {
                for (int j = 0; j < i / 2; j++) {
//                    System.out.println("Column = "+(column/2+k) );
//                    System.out.println("Set with = "+ (colWidths[column + (2*k)]));
                    this.getColumnModel().getColumn((column / 2 + k)).setPreferredWidth((colWidths[column + (2 * k + 1)] + secondRowGap));
                    k++;
                }
            }
            k = 0;
        }

        updateUI();
        return (int) getTableHeader().getPreferredSize().getWidth();
    }

    public boolean isCuatomThemeEnabled() {
        return customThemeEnabled;
    }

    public void setCustomThemeEnabled(boolean status) {
        customThemeEnabled = status;
        updateUI();
    }

    public void setTableSettings(Object sett) {
        tableSettings = sett;
    }

    public Object getTableSettings() {
        return tableSettings;
    }

    /* public void setGdidOn(boolean gridOn) {
        this.gridOn = gridOn;
        setShowGrid(gridOn);

        if (gridOn) {
            this.setIntercellSpacing(new Dimension(1, 1));
            this.setGridColor(((CommonTableSettings) tableSettings).getGridColor());
        } else {
            this.setIntercellSpacing(new Dimension(0, 0));
        }
    }

    public boolean isGdidOn() {
        return gridOn;
    }*/

    public void updateHeaderUI() {
        if (getColumnModel().getColumn(0).getHeaderRenderer() instanceof SortButtonRenderer) {
            SortButtonRenderer oRend = (SortButtonRenderer) getColumnModel().getColumn(0).getHeaderRenderer();
            oRend.setHeaderFont(((TableSettings) tableSettings).getHeadingFont());
            oRend.setHeaderFontFG(((CommonTableSettings) tableSettings).getHeaderColorFG()); // todo :check only
            oRend.setCustomThemeEnable(((CommonTableSettings) tableSettings).isCustomThemeEnabled());
//             super.getModel().getViewSettings().putProperty(ViewConstants.CS_CUSTOMIZED, chkEnable.isSelected());
            getTableHeader().updateUI();
        } else {
            getTableHeader().setFont(((TableSettings) tableSettings).getHeadingFont());
            getTableHeader().updateUI();
        }
    }

    public void setBodyFont() {
//        this.tableSelectedFont = font.deriveFont(Font.BOLD);
        Font oFont = ((TableSettings) tableSettings).getBodyFont();
        super.setFont(oFont); //((TableSettings)tableSettings).getBodyFont());
        FontMetrics oMetrices = getGraphics().getFontMetrics();
        setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + bodyGap);
        updateUI();
    }

    public void hideHeader() {
        isHeaderOn = false;
    }

    public boolean isHeaderOn() {
        return isHeaderOn;
    }

    public boolean isUseSameFontForHeader() {
        return useSameFontForHeader;
    }

    public void setUseSameFontForHeader(boolean newUseSameFontForHeader) {
        useSameFontForHeader = newUseSameFontForHeader;
    }


    public void lostOwnership(Clipboard clipboard, Transferable contents) {
    }

    public int[] getNumFixedColumns() {
        return numFixedColumns;
    }

    public void propertyChange(PropertyChangeEvent evt) {
        if (getTableHeader().getComponentOrientation().isLeftToRight()) {
            adjustColumnWidthsToFit(10, (Integer) evt.getNewValue(), (Integer) evt.getNewValue() + 1);
        } else {
            adjustColumnWidthsToFit(10, (Integer) evt.getNewValue() - 1, (Integer) evt.getNewValue());
        }
    }

    public int convertColumnIndexToModel(int viewColumnIndex) {
        return super.convertColumnIndexToModel(viewColumnIndex);    //To change body of overridden methods use File | Settings | File Templates.
    }
}