// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

/**
 * The table modal class for the Sectors table
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.*;
import com.isi.csvr.dde.DDELinkInterface;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.ShowMessage;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;

import javax.swing.JTable;

public class IndicesModal extends CommonTable implements TableModel, CommonTableInterface, ClipboardOwner, DDELinkInterface {
    private Clipboard clip;
    private String exchange;
    private Vector symbols;
    SmartProperties indexMaster;
    Hashtable<String, Vector<String>> indexTable = new Hashtable<String, Vector<String>>();
    Hashtable<String, ArrayList<String>> comindexTable = new Hashtable<String, ArrayList<String>>();
    private boolean indicesLoaded = false;
    private Properties savedIndexes;

    /**
     * Constructor
     */
    public IndicesModal(String exchange) {
        clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        setExchange(exchange);
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
        if (exchange != null) {
            init();
        }
    }

    public void setSymbol(String symbol) {

    }


    public void exchageModified() {
        indicesLoaded= false;
        init();
    }

    private void init() {
        try {
            if (!indicesLoaded) {
                loadExchangeWiseIndexes();
                checkAvailabilityOfPreviouslySaved();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadExchangeWiseIndexes() {
        try {
            Enumeration exchangeEnum = ExchangeStore.getSharedInstance().getExchanges();
            while (exchangeEnum.hasMoreElements()) {
                Exchange exg = (Exchange) exchangeEnum.nextElement();
                if (!exg.isDefault()) {
                    SmartProperties exgIndexes = new SmartProperties(Meta.DS,true);
                    Vector keyVector = new Vector();
                    String key = null;
                    String value = null;
                    String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exg.getSymbol() + "/index_" + Language.getSelectedLanguage() + ".msf";
                    File f = new File(sLangSpecFileName);
                    boolean bIsLangSpec = f.exists();
                    if (bIsLangSpec) {
                        exgIndexes.loadCompressed(sLangSpecFileName);
                    }

                    //Searching indexes
                    Enumeration indexEnum = exgIndexes.keys();
                    while (indexEnum.hasMoreElements()) {
                        String skey = (String) indexEnum.nextElement();
                        String data = (String) exgIndexes.get(skey);
                        try {
                            if (Integer.parseInt(data.split(Meta.RS)[1]) == 1) {
                                String[] field = data.split(Meta.FS);
                                String dataRec = field[5] + Meta.ID + exg.getSymbol() + Meta.ID + field[0] + Meta.ID + field[5] + Meta.ID + field[2] + Meta.ID + field[3] + Meta.ID + field[4] + Meta.ID + "" + Meta.ID + field[5];
                                String keyR = SharedMethods.getKey(exg.getSymbol(), skey, Meta.INSTRUMENT_INDEX);
                                if (!isVectorContainsKey(skey, keyVector)) {
                                    keyVector.add(skey);
                                }
                                ValidatedSymbols.getSharedInstance().addSymbol(keyR, dataRec);
                            } else {
                                //do nothing
                            }
                        } catch (Exception e) {
                            e.printStackTrace();

                        }
                    }
                    indexTable.put(exg.getSymbol(), keyVector);
                    getComIndexList(exg.getSymbol());
                    indicesLoaded = true;
                } else {
                    //need to implemet default exchanges scenario
//                    Enumeration defaultEnum = DataStore.getSharedInstance().getFilteredList(exg.getSymbol(), Meta.INSTRUMENT_INDEX);
                    Vector defaultVector = new Vector();
                    SmartProperties exgIndexes = new SmartProperties(Meta.DS, true);
                    String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exg.getSymbol() + "/index_" + Language.getSelectedLanguage() + ".msf";
                    try {
                        File f = new File(sLangSpecFileName);
                        boolean bIsLangSpec = f.exists();
                        if (bIsLangSpec) {
                            exgIndexes.loadCompressed(sLangSpecFileName);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    Enumeration defaultEnum = exgIndexes.keys();
//                    while (indexEnum.hasMoreElements()) {
//                        String skey = (String) indexEnum.nextElement();
//                        try {
//                            String keyR = SharedMethods.getKey(curExchange.getSymbol(), skey, Meta.INSTRUMENT_INDEX);
//                            arrayOfIndexes.add(keyR);
//                        } catch (Exception e) {
//                            e.printStackTrace();
//
//                        }
//                    }
                    while (defaultEnum.hasMoreElements()) {
                        defaultVector.add(defaultEnum.nextElement());
                    }
                    indexTable.put(exg.getSymbol(), defaultVector);
                    getComIndexList(exg.getSymbol());

                }
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_COM_INDEX)) {
                    String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exg.getSymbol() + "/cust_index_" + Language.getSelectedLanguage() + ".msf";
                    SmartProperties exgIndexes = new SmartProperties(Meta.DS);
                    File f = new File(sLangSpecFileName);
                    boolean bIsLangSpec = f.exists();
                    if (bIsLangSpec) {
                        exgIndexes.loadCompressed(sLangSpecFileName);
                        Enumeration indexEnum = exgIndexes.keys();
                        while (indexEnum.hasMoreElements()) {
                            try {
                                String skey = (String) indexEnum.nextElement();
                                String data = (String) exgIndexes.get(skey);
                                if (data.split(Meta.RS)[1].equals(Settings.getInstitutionCode())) {
                                    String[] field = data.split(Meta.FS);
                                    String dataRec = field[5] + Meta.ID + exg.getSymbol() + Meta.ID + field[0] + Meta.ID + field[5] + Meta.ID + field[2] + Meta.ID + field[3] + Meta.ID + field[4] + Meta.ID + "" + Meta.ID + field[5];
                                    String keyR = SharedMethods.getKey(exg.getSymbol(), skey, Meta.INSTRUMENT_INDEX);
                                    Vector keyVector = indexTable.get(exg.getSymbol());
                                    if (keyVector == null) {
                                        keyVector = new Vector();
                                        keyVector.add(skey);
                                        indexTable.put(exg.getSymbol(), keyVector);
                                        getComIndexList(exg.getSymbol()).add(skey);
                                        indicesLoaded = true;
                                        ValidatedSymbols.getSharedInstance().addSymbol(keyR, dataRec);
                                    } else if (!isVectorContainsKey(skey, keyVector)) {
                                        keyVector.add(skey);
                                        getComIndexList(exg.getSymbol()).add(skey);
                                        ValidatedSymbols.getSharedInstance().addSymbol(keyR, dataRec);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void checkAvailabilityOfPreviouslySaved() {
        savedIndexes = new Properties();
        try {
            FileInputStream oIn = new FileInputStream(Settings.SYSTEM_PATH + "/indices.msf");
            savedIndexes.load(oIn);
            oIn.close();
            Enumeration savedEnum = savedIndexes.keys();
            while (savedEnum.hasMoreElements()) {
                String exchange = (String) savedEnum.nextElement();
                Exchange exg = ExchangeStore.getSharedInstance().getExchange(exchange);
                if ((exg != null) && (!exg.isDefault())) {
                    String[] symbolList = ((String) savedIndexes.get(exchange)).split(",");
                    if (symbolList.length > 1) {
                        indexTable.get(exchange).removeAllElements();
                        getComIndexList(exg.getSymbol()).clear();
                    }
                    //reading index_ files to retrieve meta data for index symbols
                    SmartProperties exgIndexes = null;
                    try {
                        exgIndexes = new SmartProperties(Meta.DS,true);
                        String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exg.getSymbol() + "/index_" + Language.getSelectedLanguage() + ".msf";
                        File f = new File(sLangSpecFileName);
                        boolean bIsLangSpec = f.exists();
                        if (bIsLangSpec) {
                            exgIndexes.loadCompressed(sLangSpecFileName);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    SmartProperties exgIndexes2 = null;
                    try {
                        exgIndexes2 = new SmartProperties(Meta.DS);
                        String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exg.getSymbol() + "/cust_index_" + Language.getSelectedLanguage() + ".msf";
                        File f = new File(sLangSpecFileName);
                        boolean bIsLangSpec = f.exists();
                        if (bIsLangSpec && ExchangeStore.isValidSystemFinformationType(Meta.IT_COM_INDEX)) {
                            exgIndexes2.loadCompressed(sLangSpecFileName);
                        }
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    for (String aSymbolList : symbolList) {
                        if (aSymbolList.equals("")) continue;

                        String data = (String) exgIndexes.get(aSymbolList);
                        if (data != null) {
                            if (!isVectorContainsKey(aSymbolList, indexTable.get(exchange))) {
                                indexTable.get(exchange).add(aSymbolList);
                            }
                            String[] field = data.split(Meta.FS);
                            String dataRec = field[5] + Meta.ID + exg.getSymbol() + Meta.ID + field[0] + Meta.ID + field[5] + Meta.ID + field[2] + Meta.ID + field[3] + Meta.ID + field[4] + Meta.ID + "" + Meta.ID + field[5];
                            String keyR = SharedMethods.getKey(exchange, aSymbolList, Meta.INSTRUMENT_INDEX);
                            ValidatedSymbols.getSharedInstance().addSymbol(keyR, dataRec);
                        } else {
                            data = (String) exgIndexes2.get(aSymbolList);
                            if ((data != null)) {

                                if ((data.split(Meta.RS)[1].equals(Settings.getInstitutionCode()))) {
                                    String[] field = data.split(Meta.FS);
                                    String dataRec = field[5] + Meta.ID + exg.getSymbol() + Meta.ID + field[0] + Meta.ID + field[5] + Meta.ID + field[2] + Meta.ID + field[3] + Meta.ID + field[4] + Meta.ID + "" + Meta.ID + field[5];
                                    String keyR = SharedMethods.getKey(exchange, aSymbolList, Meta.INSTRUMENT_INDEX);
                                    ValidatedSymbols.getSharedInstance().addSymbol(keyR, dataRec);
                                    if (!isVectorContainsKey(aSymbolList, indexTable.get(exchange))) {
                                        indexTable.get(exchange).add(aSymbolList);
                                        getComIndexList(exg.getSymbol()).add(aSymbolList);
                                    }
                                }
                            }
                        }

                    }
                    exgIndexes = null;
                } else {
                    //do nothing
                    String[] symbolList = ((String) savedIndexes.get(exchange)).split(",");
                    SmartProperties exgIndexes2 = null;
                    try {
                        exgIndexes2 = new SmartProperties(Meta.DS);
                        String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exg.getSymbol() + "/cust_index_" + Language.getSelectedLanguage() + ".msf";
                        File f = new File(sLangSpecFileName);
                        boolean bIsLangSpec = f.exists();
                        if (bIsLangSpec && ExchangeStore.isValidSystemFinformationType(Meta.IT_COM_INDEX)) {
                            exgIndexes2.loadCompressed(sLangSpecFileName);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    for (String aSymbolList : symbolList) {
                        if (!aSymbolList.equals("")) {
                            String data = (String) exgIndexes2.get(aSymbolList);

                            if (data != null) {
                                if ((data.split(Meta.RS)[1].equals(Settings.getInstitutionCode()))) {
                                    String[] field = data.split(Meta.FS);
                                    String dataRec = field[5] + Meta.ID + exg.getSymbol() + Meta.ID + field[0] + Meta.ID + field[5] + Meta.ID + field[2] + Meta.ID + field[3] + Meta.ID + field[4] + Meta.ID + "" + Meta.ID + field[5];
                                    String keyR = SharedMethods.getKey(exchange, aSymbolList, Meta.INSTRUMENT_INDEX);
                                    ValidatedSymbols.getSharedInstance().addSymbol(keyR, dataRec);
                                    if (!isVectorContainsKey(aSymbolList, indexTable.get(exchange))) {
                                        indexTable.get(exchange).add(aSymbolList);
                                        getComIndexList(exg.getSymbol()).add(aSymbolList);
                                    }
                                }
                            } else if (!isVectorContainsKey(aSymbolList, indexTable.get(exchange))) {
                                indexTable.get(exchange).add(aSymbolList);
                            }
                        }
                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();

        }

    }

     private ArrayList<String> getComIndexList(String exhange) {
        ArrayList<String> vector = comindexTable.get(exhange);
        if (vector == null) {
            vector = new ArrayList<String>();
            comindexTable.put(exhange, vector);
        }
         return vector;
     }

    /* --- Table Modal's metods start from here --- */


    private Vector<String> getExchange(String exhange) {
        if (!exhange.equalsIgnoreCase("CUSTOM_INDEX")) {
            Vector<String> vector = indexTable.get(exhange);
            if (vector == null) {
                vector = new Vector<String>();
                indexTable.put(exhange, vector);
                comindexTable.put(exhange, new ArrayList<String>());
            }
            return vector;
        } else {
            Vector vector = new Vector<String>();
            Enumeration customIndexEnum = DataStore.getSharedInstance().getSymbols(exhange);
            while (customIndexEnum.hasMoreElements()) {
                String symbol = (String) customIndexEnum.nextElement();
                vector.add(SharedMethods.getSymbolFromExchangeKey(symbol));
            }
            indexTable.put(exhange, vector);
            comindexTable.put(exhange, new ArrayList<String>());
            return vector;
        }
    }

    public void savedIndexesForExchanges() {
        try {
            Enumeration savingEnum = indexTable.keys();
            while (savingEnum.hasMoreElements()) {
                String exchange = (String) savingEnum.nextElement();
                Vector listVector = getExchange(exchange);
                String list = "";
                for (int i = 0; i < listVector.size(); i++) {
                    list = list + "," + listVector.get(i);

                }
                try {
                    if (!exchange.equalsIgnoreCase("CUSTOM_INDEX")) {
                        savedIndexes.put(exchange, list.substring(1));
                    }
                } catch (Exception e) {
                    if (!exchange.equalsIgnoreCase("CUSTOM_INDEX")) {
                        savedIndexes.put(exchange, "");
                    }
                }
                synchronized (savedIndexes) {
                    FileOutputStream oOut = new FileOutputStream(Settings.SYSTEM_PATH + "/indices.msf");
                    savedIndexes.store(oOut, "");
                    oOut.close();

                }
            }

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        try {
            return getExchange(exchange).size();
        } catch (Exception e) {
            return 0;
        }
    }

    public Object getValueAt(int iRow, int iCol) {
        // Symbol,Description,Index,Today's initial,Net change,
        // % Change,Volume,Turnover,# Trades, # Ups,# Downs,#No Change,
        // # No Trade,52 Wk High,52 Wk High Date,52 Wk Low,52 Wk Low Date

        // removed for cairo
        //String sID = g_oIndices.getSymbols()[iRow];
        try {
            Stock oIndex = DataStore.getSharedInstance().getStockObject(exchange, getExchange(exchange).get(iRow), Meta.INSTRUMENT_INDEX);

            switch (iCol) {
                case -4:
                    return oIndex.getDecimalCount();
                case -3:
                    return oIndex.getKey();
                case -2:
                    return Meta.INSTRUMENT_INDEX;
                case -1:
                    return exchange;
                case 0:
                    return oIndex.getSymbol();
                case 1:
                    if (oIndex.getLongDescription().equals(Language.getString("NA"))) {
                        return oIndex.getSymbol();
                    } else
                        return oIndex.getLongDescription();
                case 2:
                    return "" + oIndex.getLastTradeValue();
                case 3:
                    return "" + oIndex.getCurrentAdjstVal();
                case 4:
                    return "" + oIndex.getChange();
                case 5:
                    return "" + oIndex.getPercentChange();
                case 6:
                    return "" + oIndex.getTurnover();
                case 7:
                    return "" + oIndex.getVolume();
                case 8:
                    return "" + oIndex.getTodaysOpen();
                case 9:
                    return "" + oIndex.getHigh();
                case 10:
                    return "" + oIndex.getLow();
                case 11:
                    return "" + oIndex.getPreviousClosed();
                case 12:
                    return "" + oIndex.getAvgTradePrice();
                default:
                    return "0";
            }
        } catch (Exception e) {
            return Constants.NULL_STRING;
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        //return getValueAt(0, iCol).getClass();
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 12:
            case 11:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public void addIndex(String key) {
        String symbol = SharedMethods.getSymbolFromKey(key);
        String indexExg = SharedMethods.getExchangeFromKey(key);
        if (indexExg.equalsIgnoreCase(exchange)) {
            getExchange(exchange).add(symbol);
        } else {
            new ShowMessage(Language.getString("MSG_EXCHANGE_NOT_MATCHED"), "E");
        }

    }

    public void removeIndex(String symbol) {
        getExchange(exchange).remove(symbol);
        DataStore.getSharedInstance().removeSymbolRequest(exchange, symbol, Meta.INSTRUMENT_INDEX);

    }

    private boolean isVectorContainsKey(String key, Vector<String> vector) {
        if (vector.size() > 0) {
            boolean isContains = false;
            for (String skey : vector) {
                if (skey.equalsIgnoreCase(key)) {
                    isContains = true;
                }

            }
            return isContains;

        } else {
            return false;
        }

    }

    public void clearStore() {
        try {
            getExchange(exchange).removeAllElements();
            getExchange(exchange).trimToSize();
            getExchange(exchange).clear();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public Hashtable getIndexTable() {
        return indexTable;
    }

    public Hashtable getComIndexTable() {
        return comindexTable;
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }

    public void getDDEString(Table table, boolean withHeadings) {


        JTable jtable = table.getTable();
        int col;
        int cols;
        int row = table.getTable().getSelectedRow();
        int rows = table.getTable().getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        String symbol = null;
        String exchange = null;
        int instrument = -1;
        if (jtable.getCellSelectionEnabled()) {
            col = jtable.getSelectedColumn();
            cols = jtable.getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        if (withHeadings)
            buffer.append(copyHeaders(jtable));
        int modelIndex;
        for (int r = row; r < (rows + row); r++) {
            symbol = (String) jtable.getModel().getValueAt(r, 0);
            exchange = (String) jtable.getModel().getValueAt(r, -1);
            instrument = Integer.parseInt(((Short) jtable.getModel().getValueAt(r, -2)).toString());
            if (symbol.equals(Constants.NULL_STRING)) {
                buffer.append("\n");
                continue;
            }
            for (int c = 0; c < cols; c++) {
                modelIndex = jtable.convertColumnIndexToModel(col + c);
                if (jtable.getColumn("" + modelIndex).getWidth() != 0) {
                    if (IndicesModal.isDynamicData(modelIndex)) {
                        buffer.append("=MRegionalDdeServer|'IN");
                        buffer.append(SharedMethods.getKey(exchange, symbol, instrument));
                        buffer.append("'!'");
                        buffer.append(modelIndex);
                        buffer.append("'*1");
                    } else {
                        buffer.append(table.getTable().getModel().getValueAt(r,modelIndex));
                    }
                    if (c != (cols - 1))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
        symbol = null;


    }

    public String copyHeaders(JTable table) {
        int col;
        int cols;
        //int row = this.getSelectedRow();
        //int rows= this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (table.getCellSelectionEnabled()) {
            col = table.getSelectedColumn();
            cols = table.getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
        int modelIndex;
        for (int c = 0; c < cols; c++) {
            modelIndex = table.convertColumnIndexToModel(col + c);
            if (table.getColumn("" + modelIndex).getWidth() != 0) {
                buffer.append((String) table.getColumn("" + modelIndex).getHeaderValue());
                if (c != (cols - 1))  // do not append the tab char to the last item of the row
                    buffer.append("\t");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {

    }

    public String getDDEString(String id, int col) {
        for(int i = 0; i<getRowCount();i++){
            if(DataStore.getSharedInstance().getStockObject(exchange, getExchange(exchange).get(i), Meta.INSTRUMENT_INDEX).getKey().equals(id)){
                return String.valueOf(getValueAt(i,col));
            }
        }
        return "0";
    }

    public static boolean isDynamicData(int column) {
        switch (column) {
            case 0:
            case 1:
                return false;
            default:
                return true;
        }
    }
}

