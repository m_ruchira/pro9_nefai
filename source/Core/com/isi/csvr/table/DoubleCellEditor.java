package com.isi.csvr.table;

import com.isi.csvr.shared.Constants;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;
import java.util.EventObject;

public class DoubleCellEditor extends DefaultCellEditor {

    private JTextField textField;
    private DecimalFormat oPriceFormat = new DecimalFormat("#####0.00");
    private boolean clearValue = false;

    public DoubleCellEditor(JTextField txt) {
        super(txt);
        textField = txt;
    }

    public Object getCellEditorValue() {
        //System.out.println(" textField.getText() " + textField.getText());
        return textField.getText();
    }

    public Component getTableCellEditorComponent(JTable table, Object value,
                                                 boolean isSelected, int row, int column) {
        if (clearValue)
            textField.setText(Constants.NULL_STRING);
        else
            textField.setText(oPriceFormat.format(((Double) (value)).doubleValue()));

        return textField;
    }

    public boolean isCellEditable(EventObject anEvent) {
        if (anEvent == null) {
            clearValue = true;
        } else {
            clearValue = false;
        }
        return super.isCellEditable(anEvent);
    }


}