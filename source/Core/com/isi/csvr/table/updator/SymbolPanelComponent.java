package com.isi.csvr.table.updator;

import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.shared.*;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.Client;
import com.isi.csvr.table.UpdateableTable;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.SymbolPanelStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.border.BevelBorder;
import java.awt.event.MouseListener;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.*;
import java.text.DecimalFormat;

/**
 * Created by IntelliJ IDEA. User: admin Date: 09-Oct-2007 Time: 11:57:47 To change this template use File | Settings |
 * File Templates.
 */
// Copyright (c) 2001 Integrated Systems International (ISI)
//import com.isi.csvr.datastore.IndexPanelStore;
//import com.isi.csvr.datastore.QuoteStore;

public class SymbolPanelComponent extends JPanel implements  MouseListener, Themeable, ActionListener, UpdateableTable {

    TWDecimalFormat  oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
    TWDecimalFormat oChangeFormat = new TWDecimalFormat(" ###,##0.00 ");
    private JLabel lblIndexName;
    private JLabel lblPrice;
    private JLabel lblChange;
    private int index;
    private String indexName;
    private int currentTick;
    private Color drawTopColor = null;
    private Color drawBottomColor = null;
    private JPanel upperPanel;
    private JPanel lowerPanel;

    // COLORS
    private Color topNormalColor;
    private Color topUpColor;
    private Color topDownColor;
    private Color bottomNormalColor;
    private Color bottomUpColor;
    private Color bottomDownColor;
    private ImageIcon upImage;
    private ImageIcon downImage;
    private JPopupMenu popup;

    TWMenuItem mnuDQ;
    TWMenuItem mnuChart;
    TWMenuItem mnuChange;
    TWMenuItem mnuGraph;
    TWMenuItem mnuNormalmode;
    TWMenuItem mnuOptionChain;
    String key = "";
    private ImageIcon imageicon;
    private int imagewidth;
    private boolean graphMode = false;

    /**
     * Constructor
     */
    public SymbolPanelComponent(int index) {
        try {
            this.index = index;
            currentTick = Settings.TICK_NOCHANGE;
            createLayout();
            createMenu();
            validatepopup();
            Theme.registerComponent(this);
        } catch (Exception e) {
        }
        loadTheme();
        applyTheme();
        upperPanel.updateUI();
        this.updateUI();
        TableUpdateManager.addTable(this);
    }

    private void createMenu() {
        popup = new JPopupMenu();
        mnuDQ = new TWMenuItem(Language.getString("SYMBOL_PANEL_DQ"),"detailquote.gif");
        mnuDQ.addActionListener(this);
        popup.add(mnuDQ);
        mnuChart = new TWMenuItem(Language.getString("SYMBOL_PANEL_CHART"),"graph.gif");
        mnuChart.addActionListener(this);
        popup.add(mnuChart);
        mnuChange = new TWMenuItem(Language.getString("SYMBOL_PANEL_CHANGE_SYMBOL"),"changesymbol.gif");
        mnuChange.addActionListener(this);
        popup.add(mnuChange);
        mnuGraph = new TWMenuItem("Set to Graph mode");
        mnuGraph.addActionListener(this);
        //popup.add(mnuGraph );
        mnuNormalmode = new TWMenuItem("Set to Normal mode");
        mnuNormalmode.addActionListener(this);
        //popup.add(mnuNormalmode);
        mnuOptionChain=new TWMenuItem(Language.getString("POPUP_OPTIONS"), "optionchain.gif");
        mnuOptionChain.addActionListener(this);
        popup.add(mnuOptionChain);
        mnuNormalmode.setVisible(false);
        if (graphMode) {
            mnuNormalmode.setVisible(true);
        }
        GUISettings.applyOrientation(popup);

    }

    private void validatepopup() {
        if (graphMode) {
            mnuNormalmode.setVisible(true);
            mnuGraph.setVisible(false);
        } else {
            mnuNormalmode.setVisible(false);
            mnuGraph.setVisible(true);
        }
        if (ExchangeStore.isValidIinformationType(SharedMethods.getExchangeFromKey(getKey()), Meta.IT_OptionChain) ) {
            mnuOptionChain.setVisible(true);
        } else {
            mnuOptionChain.setVisible(false);
        }
    }

    private void createLayout() {
        this.setLayout(new GridLayout(2, 1, 0, 0));
        this.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Theme.getColor("SYMBOLPANEL_SHADOW"), Theme.getColor("SYMBOLPANEL_HIGHLIGHT")));
        upperPanel = new JPanel(new FlexGridLayout(new String[]{"100%", "0"}, new String[]{"100%"}, 0, 0)) { // FlowLayout(FlowLayout.LEADING, 5, 5));

            public void paint(Graphics g) {
                if (!graphMode) {
                    try {
                        if ((imageicon != null) && (imagewidth > 0)) {
                            int width = getWidth();
                            for (int i = 0; i <= width; i += imagewidth) {
                                g.drawImage(imageicon.getImage(), i, 0, this);
                            }
                            super.paintChildren(g);
                            if (this.getBorder() != null) {
                                this.getBorder().paintBorder(this, g, 0, 0, this.getWidth(), this.getHeight());
                            }
                        } else {
                            super.paint(g);
                        }
                    } catch (Exception e) {

                    }
                }
            }
        };
        lblIndexName = new JLabel(Language.getString("SYMBOL_PANEL_FIRST_TIME"));

        lblIndexName.setForeground(Theme.getColor("SYMBOLPANEL_FGCOLOR"));
        lblIndexName.setHorizontalAlignment(SwingConstants.CENTER);
        lblIndexName.addMouseListener(this);
        lblPrice = new JLabel();

        lblChange = new JLabel();    //"00.00 ( 00.00% ) "
        lblChange.setOpaque(true);
        lblChange.setBackground(Theme.getColor("SYMBOLPANEL_BGCOLOR"));
        lblIndexName.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));   // 12
        lblPrice.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
        lblPrice.setHorizontalAlignment(JLabel.TRAILING);
        lblChange.setHorizontalAlignment(JLabel.TRAILING);
        lblChange.setFont(new java.awt.Font("Dialog", Font.BOLD, 11));

        upperPanel.add(lblIndexName);
        upperPanel.add(lblPrice);
        upperPanel.addMouseListener(this);
        upperPanel.setCursor(new Cursor(Cursor.HAND_CURSOR));

        lowerPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        lowerPanel.setBackground(Color.GRAY);
        lowerPanel.setOpaque(true);
//        lowerPanel.add(new JLabel());
        lowerPanel.add(lblChange);
        lowerPanel.addMouseListener(this);
        lowerPanel.setCursor(new Cursor(Cursor.HAND_CURSOR));

        this.add(upperPanel);
        this.add(lowerPanel);

        upImage = new ImageIcon("images/common/up.gif");
        downImage = new ImageIcon("images/common/down.gif");
        GUISettings.applyOrientation(this);
        GUISettings.applyOrientation(upperPanel);
        GUISettings.applyOrientation(lowerPanel);

    }

    public String getIndexName() {
        return indexName;
    }

    public void setIndexName(String indName) {
        indexName = indName;
        if (Language.isLTR()) {
            lblIndexName.setHorizontalAlignment(SwingConstants.LEADING);
            lblIndexName.setText("  " + indexName);
        } else {
            lblIndexName.setHorizontalAlignment(SwingConstants.LEADING);
            lblIndexName.setText(indexName + "  ");
        }
    }

    public void run() {
        Stock indexObj = null;
        while (true) {
            try {
                indexObj = DataStore.getSharedInstance().getStockObject(SymbolPanelStore.getSharedInstance().getSymbolAt(index));
                setKey(indexObj.getKey());
                if (indexObj != null) {
                    setIndexData(indexObj);
                    upperPanel.setToolTipText(indexObj.getLongDescription());
                    lowerPanel.setToolTipText(indexObj.getLongDescription());
                }
                Thread.sleep(1000);
            } catch (Exception e) {
                try {
                    Thread.sleep(1000);
                } catch (Exception e2) {

                }
//                e.printStackTrace();
            }
        }
    }


    public void loadTheme() {
        Color drawColor = null;
        topNormalColor = Theme.getColor("TOOLBAR_FGCOLOR");
        topUpColor = Theme.getColor("TOOLBAR_UP_FGCOLOR");
        topDownColor = Theme.getColor("TOOLBAR_DOWN_FGCOLOR");
        bottomNormalColor = Theme.getColor("TOOLBAR_FGCOLOR");
        bottomUpColor = Theme.getColor("TOOLBAR_UP_FGCOLOR");
        bottomDownColor = Theme.getColor("TOOLBAR_DOWN_FGCOLOR");
        lblIndexName.setForeground(Theme.getColor("SYMBOLPANEL_FGCOLOR"));
        lblChange.setBackground(Theme.getColor("SYMBOLPANEL_BGCOLOR"));
        switch (currentTick) {
            case Settings.TICK_UP:
                drawColor = Theme.getColor("TOOLBAR_UP_FGCOLOR");
                break;
            case Settings.TICK_DOWN:
                drawColor = Theme.getColor("TOOLBAR_DOWN_FGCOLOR");
                break;
            default:
                drawColor = Theme.getColor("TOOLBAR_DOWN_FGCOLOR");
        }
        lblChange.setForeground(drawColor);
        lblPrice.setForeground(drawColor);
    }

    //    public void setIndexData(String symbol,double price, double change, double prevClosed) {
    public void setIndexData(Stock stock) {
        double perChange = 0F;

        if (stock.getSymbol() != null){
            if (!stock.getShortDescription().equals(Language.getString("NA"))) {
                if (stock.getShortDescription().equals(stock.getSymbol())) {
                    setIndexName(stock.getLongDescription().trim());
                } else {
                    setIndexName(stock.getLongDescription());
                }
            } else {
//                if (stock.getShortDescription().equals(stock.getSymbol())) {
//                    setIndexName(stock.getLongDescription().trim());
//                } else {
//                    setIndexName(stock.getSymbol());
//
//                }
                setIndexName(SharedMethods.getSymbolFromKey(this.key));
            }
        }

        if (stock!=null) {
            if (stock.getChange() > 0) {
                currentTick = Settings.TICK_UP;
            } else if (stock.getChange() < 0) {
                currentTick = Settings.TICK_DOWN;
            } else {
                currentTick = Settings.TICK_NOCHANGE;
            }
            perChange = stock.getPercentChange();
        } else {
            perChange=0.00;
        }

        switch (currentTick) {
            case Settings.TICK_UP:
                lblPrice.setIcon(upImage);
                drawTopColor = topUpColor;
                drawBottomColor = bottomUpColor;

                break;
            case Settings.TICK_DOWN:
                lblPrice.setIcon(downImage);
                drawTopColor = topDownColor;
                drawBottomColor = bottomDownColor;

                break;
            default:
                lblPrice.setIcon(null);
                drawTopColor = topNormalColor;
                drawBottomColor = bottomNormalColor;

        }
        lblChange.setForeground(drawBottomColor);
        lblPrice.setForeground(drawTopColor);
        lblPrice.setHorizontalTextPosition(JLabel.LEADING);

        if (stock!=null) {
            oPriceFormat = SharedMethods.getDecimalFormat(stock.getDecimalCount() );
            oChangeFormat = SharedMethods.getDecimalFormat(stock.getDecimalCount() ); 
            lblPrice.setText(oPriceFormat.format(stock.getLastTradeValue()));
            lblChange.setText(oChangeFormat.format(stock.getChange()) + "  (" + oChangeFormat.format(perChange) + "%) ");
        } else {
            lblPrice.setText(oPriceFormat.format(0.00));
            lblChange.setText(oChangeFormat.format(0.00) + "  (" + oChangeFormat.format(0.00) + "%) ");
        }
        this.updateUI();
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(popup);
        this.setBorder(BorderFactory.createBevelBorder(BevelBorder.RAISED, Theme.getColor("SYMBOLPANEL_SHADOW"), Theme.getColor("SYMBOLPANEL_HIGHLIGHT")));

        topNormalColor = Theme.getColor("TOOLBAR_FGCOLOR");
        topUpColor = Theme.getColor("TOOLBAR_UP_FGCOLOR");
        topDownColor = Theme.getColor("TOOLBAR_DOWN_FGCOLOR");
        bottomNormalColor = Theme.getColor("TOOLBAR_FGCOLOR");
        bottomUpColor = Theme.getColor("TOOLBAR_UP_FGCOLOR");
        bottomDownColor = Theme.getColor("TOOLBAR_DOWN_FGCOLOR");
        lblIndexName.setForeground(Theme.getColor("SYMBOLPANEL_FGCOLOR"));
        lblChange.setBackground(Theme.getColor("SYMBOLPANEL_BGCOLOR"));
        try {
            imageicon = null;
            imageicon = new ImageIcon("images/theme" + Theme.getID() + "/symbolpaneltitletile.png");
            imagewidth = imageicon.getIconWidth();
        } catch (Exception e) {
            imageicon = null;
            imagewidth = 0;
        }
        this.updateUI();
        this.updateUI();
        upperPanel.updateUI();
    }

    /**
     * Invoked when the mouse button has been clicked (pressed
     * and released) on a component.
     */
    public void mouseClicked(MouseEvent e) {

        if (SwingUtilities.isRightMouseButton(e)) {
            validatepopup();
            if (!lblIndexName.getText().equals(Language.getString("SYMBOL_PANEL_FIRST_TIME")) && (lblIndexName.getText() != null)) {
                GUISettings.showPopup(popup, (Component) e.getSource(), e.getX(), e.getY());
            }

        } else if (e.getSource() == lblIndexName) {
            if (lblIndexName.getText().equals(Language.getString("SYMBOL_PANEL_FIRST_TIME")) && (lblIndexName.getText() != null)) {
                changeSymbol();
            } else {
                Client.getInstance().showDetailQuote(getKey(), false);
                //Client.getInstance().showChart(getKey(), null, null);
            }
        } else {
            if (e.getClickCount() > 1) {
                if (Settings.isConnected()) {

                }
            }
        }

    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     */
    public void mousePressed(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button has been released on a component.
     */
    public void mouseReleased(MouseEvent e) {

    }

    /**
     * Invoked when the mouse enters a component.
     */
    public void mouseEntered(MouseEvent e) {

    }

    /**
     * Invoked when the mouse exits a component.
     */
    public void mouseExited(MouseEvent e) {

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == mnuDQ) {
            Client.getInstance().showDetailQuote(getKey(), false);
        }
        if (e.getSource() == mnuChart) {
            Client.getInstance().showChart(getKey(), null, null);
        }
        if (e.getActionCommand().equals("CHART")) {
        }
        if (e.getSource() == mnuChange) {
            changeSymbol();
        }
        if (e.getSource() == mnuGraph) {
            System.out.println("setting to graph mode");
            graphMode = true;
            repaint();
        }
        if (e.getSource() == mnuNormalmode) {
            graphMode = false;
            repaint();
        }
        if (e.getActionCommand().equals("ALERT")) {
        }
        if(e.getSource().equals(mnuOptionChain)){
            Client.getInstance().mnu_Options(getKey(), null, false, false);
        }
    }

    private void optionChain() {

    }

    private void changeSymbol() {
//        String symbol = SymbolPanelStore.getSymbolAt(index);
        Symbols symbols = new Symbols();
        SymbolSearch oCompanies = SymbolSearch.getSharedInstance();//new SymbolSearch(Language.getString("WINDOW_TITLE_SEARCH"),true,true);
        oCompanies.setTitle(Language.getString("INDEX_PANEL"));
        oCompanies.setSingleMode(true);
        oCompanies.setIndexSearchMode(false);
        oCompanies.setSymbols(symbols);
        oCompanies.showDialog(true);
        SymbolPanelStore.getSharedInstance().setChangedSymbiol(index, symbols.getSymbols()[0]);
        oCompanies = null;
        symbols = null;
    }

    private void setKey(String key) {
        this.key = key;
    }

    private String getKey() {
        return key;
    }

    public void paint(Graphics g) {
        if (graphMode) {
            g.setColor(Color.black);
            g.fillRect(0, 0, this.getWidth(), this.getHeight());

            /////////////////////////////////////////////////
//                    g.setColor(Color.RED);
            int h = 31;
            int w = 240;
            double[] arr;
            DynamicArray ohlc = OHLCStore.getInstance().getOHLCForIndex(key);

            if (ohlc.size() <= 240) {
                arr = new double[ohlc.size() + 2];
                arr[0] = 1000000;
                for (int j = 0; j < ohlc.size(); j++) {
                    IntraDayOHLC intradayohlc = (IntraDayOHLC) ohlc.get(j);
                    arr[j + 2] = intradayohlc.getClose();
                    arr[0] = Math.min(arr[0], intradayohlc.getClose());
                    arr[1] = Math.max(arr[1], intradayohlc.getClose());

                }
            } else {
                int constant = ohlc.size() - 240;
                arr = new double[242];
                arr[0] = 1000000;
                for (int j = ohlc.size() - 240; j < ohlc.size(); j++) {
                    IntraDayOHLC intradayohlc = (IntraDayOHLC) ohlc.get(j);
                    arr[j + 2 - constant] = intradayohlc.getClose();
                    arr[0] = Math.min(arr[0], intradayohlc.getClose());
                    arr[1] = Math.max(arr[1], intradayohlc.getClose());

                }
            }


            try {
                if ((arr != null) && (arr.length > 0) && (arr[1] != Double.MIN_VALUE)) {
                    g.setColor(Theme.BIDASK_UP_COLOR);
                    double[] rangearray = new double[arr.length];
                    for (int i = 0; i < arr.length; i++) {
                        rangearray[i] = (arr[i] - arr[0]);
                    }
                    double yratio = ((h - 2) / rangearray[1]);
                    double xgap = (w) / (double) w;
                    int[] xcordinates = new int[arr.length];
                    for (int i = 0; i < arr.length; i++) {
                        xcordinates[i] = i;
                    }

                    for (int i = 0; i < arr.length - 3; i++) {
                        if (rangearray[i + 3] >= 0) {
                            g.drawLine(xcordinates[i], (int) ((h) - (rangearray[i + 2]) * yratio) + 1, xcordinates[i + 1], (int) ((h) - (rangearray[i + 3]) * yratio) + 1);
                        } else {
                            break;
                        }
                    }

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            g.setColor(Color.WHITE);
            g.setFont(new TWFont("Arial", Font.BOLD, 12));
            g.drawString(SharedMethods.getSymbolFromKey(key), 150, 30);
            g.setColor(Color.GREEN);
            Graphics2D g2D = (Graphics2D) g;
            g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.5f));
            g.drawRect(0, 0, this.getWidth(), this.getHeight());
            for (int i = 0; i < 25; i++) {
                g.drawLine((i * 10), 0, (i * 10), this.getHeight());
            }
            for (int i = 0; i < 4; i++) {
                g.drawLine(0, (i * 11), this.getWidth(), (i * 11));
            }

        } else {
            super.paint(g);
        }
    }

    public void runThread() {
        Stock indexObj = null;

        try {
            indexObj = DataStore.getSharedInstance().getStockObject(SymbolPanelStore.getSharedInstance().getSymbolAt(index));
            setKey(indexObj.getKey());
            if (indexObj != null) {
                setIndexData(indexObj);
                upperPanel.setToolTipText(indexObj.getLongDescription());
                lowerPanel.setToolTipText(indexObj.getLongDescription());
            }
            Thread.sleep(1000);
        } catch (Exception e) {

        }

    }

    public String getTitle() {
       return "symbol panel updator";  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Constants.ThreadTypes getTableID() {
       return Constants.ThreadTypes.SYMBOL_PANEL;
    }
    
}