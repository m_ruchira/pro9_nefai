// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

import com.isi.csvr.TableSorter;
import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;


public class OpenPositionsRenderer
        extends TWBasicTableRenderer {
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private TWDateFormat g_oDateTimeFormatHM;

    private String g_sNA = "NA";

    private static Color g_oUpColor;
    private static Color g_oDownColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private Color upColor;
    private Color downColor;


    private Color foreground, background;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oPriceFormat1;
    private TWDecimalFormat oQuantityFormat;
    private DefaultTableCellRenderer lblRenderer;
    private double doubleValue;
    private long longValue;
    private Date date;
    private boolean expandable;
    private boolean expanded;

    private static ImageIcon collapsedImage;
    private static ImageIcon expandedImage;
    private static ImageIcon squareImage;
    String price;
    String currency;


    public OpenPositionsRenderer(String[] asColumns, int[] asRendIDs) {
        g_asRendIDs = asRendIDs;

        reload();
        g_oDateTimeFormatHM = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HM_FORMAT"));//" yyyy/MM/dd HH:mm ");
        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oPriceFormat1 = new TWDecimalFormat(" ###,##0.00 ");
        oPriceFormat1.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");

        date = new Date();

        try {
            collapsedImage = new ImageIcon("images/Theme" + Theme.getID() + "/TreeFolderClosed.gif");
        } catch (Exception e) {
            collapsedImage = null;
        }

        try {
            expandedImage = new ImageIcon("images/Theme" + Theme.getID() + "/TreeFolderOpened.gif");
        } catch (Exception e) {
            expandedImage = null;
        }

        try {
            squareImage = new ImageIcon("images/Theme" + Theme.getID() + "/closeposition.gif");
        } catch (Exception e) {
            squareImage = null;
        }

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
    }

    public static void reloadForPrinting() {
        g_oUpColor = Color.black;
        g_oDownColor = Color.black;
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
    }


    public static void reload() {
        reloadRenderer();
        try {
            g_oUpColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
        } catch (Exception e) {
            g_oUpColor = Color.green;
            g_oDownColor = Color.red;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public void propertyChanged(int property) {
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
        g_asRendIDs = asRendIDs;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
            upColor = sett.getPositiveChangeFG();
            downColor = sett.getNegativeChangeFG();
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
            upColor = g_oUpColor;
            downColor = g_oDownColor;
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        try {
            try {
                oPriceFormat = (((ExchangeFormatInterface) table.getModel()).getDecimalFormat());
            } catch (Exception e) {
                oPriceFormat = (((ExchangeFormatInterface) ((TableSorter) table.getModel()).getModel()).getDecimalFormat());
            }
        } catch (Exception e) {
            // do nothing
        }

        int iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    if (Settings.isShowArabicNumbers())
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 2: // DESCRIPTION
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE
                    doubleValue = ((DoubleTransferObject) (value)).getValue();// toDoubleValue(value);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                            (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                             lblRenderer.setText("");
                    } else {
                        lblRenderer.setText((oPriceFormat1.format(doubleValue)));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // QUANTITY
                    lblRenderer.setText(oQuantityFormat.format(toLongValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                    break;
                case 5: // CHANGE
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        doubleValue = ((DoubleTransferObject) (value)).getValue();//toDoubleValue(value);
                        lblRenderer.setText(oPriceFormat.format(doubleValue));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        if (doubleValue > 0)
                            lblRenderer.setForeground(upColor);
                        else if (doubleValue < 0)
                            lblRenderer.setForeground(downColor);
                    }
                    break;
                case 6: // Profit 1
                    if (((String) value).contains("~")) {
                        price = ((String) value).split("~")[0];
                        currency = ((String) value).split("~")[1];
                        doubleValue = Double.parseDouble(price);//(DoubleTransferObject) (value)).getValue();// toDoubleValue(value);
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                                (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                            lblRenderer.setText("");
                        } else {
                            lblRenderer.setText((oPriceFormat1.format(doubleValue)) + "(" + currency + ")");
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    } else {
                        lblRenderer.setText("");
//                        doubleValue = ((DoubleTransferObject) (value)).getValue();// toDoubleValue(value);
//                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
//                                (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
//                            lblRenderer.setText("");
//                        } else {
//                            lblRenderer.setText((oPriceFormat1.format(doubleValue)));
//
//                        }
//                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    }

                    break;
                case 8: {// DATE TIME
                    longValue = toLongValue(value);
                    if (longValue <= Constants.ONE_DAY)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateTimeFormatHM.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                }
                case 9: // profit 2
                    if (((String) value).contains("~")) {
                        price = ((String) value).split("~")[0];
                        currency = ((String) value).split("~")[1];
                        doubleValue = Double.parseDouble(price);//((DoubleTransferObject) (value)).getValue();// toDoubleValue(value);
                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                                (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                            lblRenderer.setText("");
                        } else {
                            lblRenderer.setText((oPriceFormat1.format(doubleValue)) + "(" + currency + ")");
                        }
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        break;
                    } else {
                        lblRenderer.setText("");
//                        doubleValue = ((DoubleTransferObject) (value)).getValue();// toDoubleValue(value);
//                        if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
//                                (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
//                            lblRenderer.setText("");
//                        } else {
//                            lblRenderer.setText((oPriceFormat1.format(doubleValue)));
//                        }
//                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
//                        break;
                    }

                case 'x': // Expand / Collapse icon
                    try {
                        expandable = (Boolean) value;
                        lblRenderer.setText("");
                        lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                        if (expandable) {
                            expanded = (Boolean) table.getModel().getValueAt(row, -101);
                            if (expanded) {
                                lblRenderer.setIcon(expandedImage);
                            } else {
                                lblRenderer.setIcon(collapsedImage);
                            }
                        } else {
                            lblRenderer.setIcon(null);
                        }
                    } catch (Exception e) {
                        expandable = false;
                    }
                    break;
                case 'c': // square item
                    lblRenderer.setText("");
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    if ((Boolean) value) {
                        lblRenderer.setIcon(squareImage);
                    } else {
                        lblRenderer.setIcon(null);
                    }
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
//            System.out.println("Open position rendere fails");
//            e.printStackTrace();
        }


        return lblRenderer;
    }

    private long toLongValue(Object oValue) throws Exception {
        return ((LongTransferObject) oValue).getValue();
    }

    private int toIntValue(Object oValue) throws Exception {
        return ((IntTransferObject) oValue).getValue();
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return ((DoubleTransferObject) oValue).getValue();
    }
}
