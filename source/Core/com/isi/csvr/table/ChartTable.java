package com.isi.csvr.table;

import com.isi.csvr.ChartInterface;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.chart.DataTableModel;
import com.isi.csvr.chart.GraphDataRenderer;
import com.isi.csvr.chart.GraphFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.GUISettings;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 21, 2006
 * Time: 10:34:30 AM
 * To change this template use File | Settings | File Templates.
 */
public class ChartTable implements ChartTableInterface {

    private Table table;
    private DataTableModel tableModel;

    public ChartTable() {
    }

    public boolean isTableNotCreated(){
        return tableModel == null;
    }

    public JTable createTable(GraphFrame frame, ViewSetting g_oViewSettings){
        if (tableModel == null) {
            tableModel = new DataTableModel(frame);
            try {
                GUISettings.setColumnSettings(g_oViewSettings, ChartInterface.getColumnSettings("CHART_DATA_COLS"));
                tableModel.setViewSettings(g_oViewSettings);
            } catch (Exception e) {
                e.printStackTrace();
            }
            table = new Table();
            table.setSortingEnabled();
            table.setModel(tableModel);
            tableModel.setTable(table, new GraphDataRenderer(g_oViewSettings.getColumnHeadings(), tableModel.getRendIDs()));
        }

        return table.getTable();
            //tablePanel.add(dataTable);
    }

     public void setStore(DynamicArray store, String key) {
//         tableModel.setStore(store, key);
    }

    public int[] getRendIDs(){
        return tableModel.getRendIDs();
    }

    public Object getValueAt(int row, int col){
        return tableModel.getValueAt(row, col);
    }

    public int getRowCount(){
        return tableModel.getRowCount();
    }

    public void updateGUI(){
        table.updateGUI();
    }

    public void setMenuItem(TWMenuItem item){
        table.getPopup().setMenuItem(item);
    }

    public void hideTitleBarMenu(){
        table.getPopup().hideTitleBarMenu();
    }

    public JTable getJTable(){
        return table.getTable();
    }

    public void killThread(){
        table.killThread();
    }

    public void setSortingEnabled(){
        table.setSortingEnabled();
    }
}
