package com.isi.csvr.table;

/**
 * @author : Uditha Nagahawatta
 * @date : 25 July 2002
 * @purpose: Event listener to notify table models prior to table update request
 */

public interface TableUpdateListener {
    public void tableUpdating();
}