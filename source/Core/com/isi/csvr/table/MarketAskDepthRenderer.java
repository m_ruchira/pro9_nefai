// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDateFormat;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;


public class MarketAskDepthRenderer extends TWBasicTableRenderer {

    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private TWDateFormat g_oTimeFormat = new TWDateFormat(" hh:mm:ss ");
    private TWDateFormat g_oDateTimeFormatHMS;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oQuantityFormat;
    private String g_sNA = "NA";
    private String priceMarketOrder = "NA";
    private String priceMarketOnOpening = "NA";

    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;

    public MarketAskDepthRenderer() {
        g_oDateTimeFormatHMS = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));
    }

    public void propertyChanged(int property) {

    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        g_asRendIDs = asRendIDs;
        reload();
        g_sNA = Language.getString("NA");
        priceMarketOrder = Language.getString("DEPTH_PRICE_MARKET_ORDER");
        priceMarketOnOpening = Language.getString("DEPTH_PRICE_MARKET_ON_OPENING");
        g_iStringAlign = JLabel.LEADING;
        g_iNumberAlign = JLabel.RIGHT;
        oQuantityFormat = new TWDecimalFormat(" ###,##0  ");
    }

    public void setBidRenderer(boolean status) {
    }

    public static void reloadForPrinting() {
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("DEPTH_BY_ORDER_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("DEPTH_BY_ORDER_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("DEPTH_BY_ORDER_ASK_FGCOLOR1");
            g_oBG1 = Theme.getColor("DEPTH_BY_ORDER_ASK_BGCOLOR1");
            g_oFG2 = Theme.getColor("DEPTH_BY_ORDER_ASK_FGCOLOR2");
            g_oBG2 = Theme.getColor("DEPTH_BY_ORDER_ASK_BGCOLOR2");
            //}
        } catch (Exception e) {
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        Color foreground, background;
        oPriceFormat = (((ExchangeFormatInterface) table.getModel()).getDecimalFormat());

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getDepthOrderSelectedColorFG();
                background = sett.getDepthOrderSelectedColorBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getDepthOrderAskColor1FG();
                    background = sett.getDepthOrderAskColor1BG();
                } else {
                    foreground = sett.getDepthOrderAskColor2FG();
                    background = sett.getDepthOrderAskColor2BG();
                }
            }
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        try {
            oPriceFormat = ((ExchangeFormatInterface) table.getModel()).getDecimalFormat();
        } catch (Exception e) {
            e.printStackTrace();
        }

        int iRendID = 0;
        double price = 0;
        iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                case 1: // SYMBOL
                case 2: // DESCRIPTION
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE
                    price = toDoubleValue(value);
                    if(price == Constants.DEPTH_PRICE_MARKET_ORDER_VALUE){
                        lblRenderer.setText(priceMarketOrder);
                    } else if(price == Constants.DEPTH_PRICE_MARKET_ON_OPENING_VALUE){
                        lblRenderer.setText(priceMarketOnOpening);
                    } else {
                        lblRenderer.setText(oPriceFormat.format(price));
                    }
//                    lblRenderer.setText(oPriceFormat.format(toDoubleValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // QUANTITY
                    lblRenderer.setText(oQuantityFormat.format(toLongValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                    break;
                case 7: // DATE
                    long lDate = toLongValue(value);
                    if (lDate == 0) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        Date dDate = new Date(lDate);
                        lblRenderer.setText(g_oTimeFormat.format(dDate));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 8: // DATE TIME
                    long lDateTime = toLongValue(value);
                    if (lDateTime == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        Date dDateTime = new Date(lDateTime);
                        lblRenderer.setText(g_oTimeFormat.format(dDateTime));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'A': // TIME
                    long lTime = toLongValue(value);
                    if (lTime == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        Date dTime = new Date(lTime);
                        lblRenderer.setText(g_oTimeFormat.format(dTime));
                        lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                    }
                    break;
                case 'B': // Broker ID
                    if ((value == null) || (((String) value)).equals("")
                            || (((String) value)).equals("null"))
                        lblRenderer.setText(g_sNA);
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 'D': {// DATE TIME with secs
                    long lDateTimeHMS = toLongValue(value);
                    if (lDateTimeHMS == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        Date dDateTime = new Date(lDateTimeHMS);
                        lblRenderer.setText(g_oDateTimeFormatHMS.format(dDateTime));
                        dDateTime = null;
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                }
                default:
                    lblRenderer.setText("");
            }
        }
        catch (Exception e) {
            lblRenderer.setText("");
        }


        return lblRenderer;
    }

    private long toLongValue(Object oValue) {
        try {
            return Long.parseLong((String) oValue);
        }
        catch (Exception e) {
            return 0;
        }
    }

    private int toIntValue(Object oValue) {
        try {
            return Integer.parseInt((String) oValue);
        }
        catch (Exception e) {
            return 0;
        }
    }

    private double toDoubleValue(Object oValue) {
        try {
            return Double.parseDouble((String) oValue);
        }
        catch (Exception e) {
            return 0;
        }
    }
}

