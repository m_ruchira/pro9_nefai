package com.isi.csvr.table;

import com.isi.csvr.chart.GraphFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.TWMenuItem;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 21, 2006
 * Time: 11:11:10 AM
 * To change this template use File | Settings | File Templates.
 */
public interface ChartTableInterface {
    boolean isTableNotCreated();

    JTable createTable(GraphFrame frame, ViewSetting g_oViewSettings);

    void setStore(DynamicArray store, String key);

    int[] getRendIDs();

    Object getValueAt(int row, int col);

    int getRowCount();

    void updateGUI();

    void setMenuItem(TWMenuItem item);

    void hideTitleBarMenu();

    JTable getJTable();

    void killThread();

    void setSortingEnabled();
}
