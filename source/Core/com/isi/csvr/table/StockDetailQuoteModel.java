// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

/**
 * A Class class.
 * <P>
 * @author Bandula
 */

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

public class StockDetailQuoteModel extends CommonTable
        implements DetailQuote, TableModel, CommonTableInterface {

    private String symbol;
    private long timeOffset;

    private static LongTransferObject longTrasferObject = new LongTransferObject();
    private static DoubleTransferObject doubleTransferObject = new DoubleTransferObject();

    /**
     * Constructor
     */
    public StockDetailQuoteModel() {
    }

    public void setSymbol(String symbolIn) {
        symbol = symbolIn;
    }

    /* --- Table Model methods start from here --- */

    public int getColumnCount() {
        return 4;
    }

    public int getRowCount() {
        return 18;
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            if (symbol == null)
                return "";
            Stock stock = DataStore.getSharedInstance().getStockObject(symbol);
            timeOffset = stock.getTimeOffset();
            if (stock == null)
                return "";

            //---------------------------- new Changes ---------------- Shanika ----

            switch (iRow) {
                case -2:
                    return true;
                case -1:
                    return stock.getInstrumentType();
                case 0:
                    switch (iCol) {
                        case 0:
                            return Language.getString("OPEN");
                        case 1:
                            return doubleTransferObject.setValue(stock.getTodaysOpen());
                        case 2:
                            return Language.getString("LASTTRADE_TIME");
                        case 3:
                            return longTrasferObject.setValue(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(stock.getExchange(), stock.getLastTradeTime()));

                    }
                case 1:
                    switch (iCol) {
                        case 0:
                            return Language.getString("HIGH_DQ");
                        case 1:
                            return doubleTransferObject.setValue(stock.getHigh());
                        case 2:
                            return Language.getString("LAST_TRADE");
                        case 3:
                            doubleTransferObject.setFlag(stock.getLastTradeFlag());
                            return doubleTransferObject.setValue(stock.getLastTradeValue());
                    }
                case 2:
                    switch (iCol) {
                        case 0:
                            return Language.getString("LOW_DQ");
                        case 1:
                            return doubleTransferObject.setValue(stock.getLow());
                        case 2:
                            return Language.getString("LASTTRADE_QTY");
                        case 3:
                            longTrasferObject.setFlag(stock.getTradeQuantityFlag());
                            return longTrasferObject.setValue(stock.getTradeQuantity());
                    }
                case 3:
                    switch (iCol) {
                        case 0:
                            return Language.getString("CLOSE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getTodaysClose());
                        case 2:
                            return Language.getString("CHANGE");
                        case 3:
                            return doubleTransferObject.setValue(stock.getChange());

                    }
                case 4:
                    switch (iCol) {
                        case 0:
                            return Language.getString("VWAP");
                        case 1:
                            if(stock.getExchange().equals("TDWL")){
                                  if (ExchangeStore.isValidIinformationType(stock.getExchange(),Meta.IT_TWAP_ClosingVWAP))
                                     return doubleTransferObject.setValue(stock.getAvgTradePrice());
                                     else
                                     return ""+ Double.NaN;

                                     } else
                                    return doubleTransferObject.setValue(stock.getAvgTradePrice());
                        case 2:
                            return Language.getString("PCT_CHANGE");
                        case 3:
                            return doubleTransferObject.setValue(stock.getPercentChange());

                    }
                case 5:
                    switch (iCol) {
                        case 0:
                            return Language.getString("VOLUME");
                        case 1:
                            longTrasferObject.setFlag(stock.getVolumeFlag());
                            return longTrasferObject.setValue(stock.getVolume());
                        case 2:
                            return Language.getString("TRADES");
                        case 3:
                            return longTrasferObject.setValue(stock.getNoOfTrades());

                    }
                case 6:
                    switch (iCol) {
                        case 0:
                            return Language.getString("TURNOVER");
                        case 1:
                            doubleTransferObject.setFlag(stock.getTurnoverFlag());
                            return doubleTransferObject.setValue(stock.getTurnover());
                        case 2:
                            return Language.getString("AVG_VOL");
                        case 3:
                            return longTrasferObject.setValue(stock.getAvgTradeVolume());

                    }

                case 7:
                    switch (iCol) {
                        case 0:
                            return Language.getString("CLOSING_VWAP");
                        case 1:
                            return doubleTransferObject.setValue(stock.getClosingVWAP());
                        case 2:
                            return Language.getString("TWAP");
                        case 3:
                            return doubleTransferObject.setValue(stock.getTWAP());
                    }
                case 8:
                    switch (iCol) {
                        case 0:
                            return Language.getString("PREV_CLOSE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getPreviousClosed());
                        case 2:
                            return Language.getString("MIN_PRICE");
                        case 3:
                            return doubleTransferObject.setValue(stock.getMinPrice());
                    }
                case 9:
                    switch (iCol) {
                        case 0:
                            return Language.getString("BIDASK_RATIO");
                        case 1:
                            return doubleTransferObject.setValue(stock.getBidaskRatio());
                        case 2:
                            return Language.getString("MAX_PRICE");
                        case 3:
                            return doubleTransferObject.setValue(stock.getMaxPrice());
                    }
                case 10:
                    switch (iCol) {
                        case 0:
                            return Language.getString("BEST_BID");
                        case 1:
                            doubleTransferObject.setFlag(stock.getBestBidPriceFlag());
                            return doubleTransferObject.setValue(stock.getBestBidPrice());
                        case 2:
                            return Language.getString("BEST_ASK");
                        case 3:
                            doubleTransferObject.setFlag(stock.getBestAskPriceFlag());
                            return doubleTransferObject.setValue(stock.getBestAskPrice());
                    }
                case 11:
                    switch (iCol) {
                        case 0:
                            return Language.getString("BEST_BID_QUANTITY");
                        case 1:
                            return longTrasferObject.setValue(stock.getBestBidQuantity());
                        case 2:
                            return Language.getString("BEST_ASK_QUANTITY");
                        case 3:
                            return longTrasferObject.setValue(stock.getBestAskQuantity());
                    }
                case 12:
                    switch (iCol) {
                        case 0:
                            return Language.getString("TOTAL_BID_QUANTITY_SHORT");
                        case 1:
                            return longTrasferObject.setValue(stock.getTotalBidQty());
                        case 2:
                            return Language.getString("TOTAL_ASK_QUANTITY_SHORT");
                        case 3:
                            return longTrasferObject.setValue(stock.getTotalAskQty());
                    }
                case 13:
                    switch (iCol) {
                        case 0:
                            return Language.getString("NO_OF_BIDS");
                        case 1:
                            return longTrasferObject.setValue(stock.getNoOfBids());
                        case 2:
                            return Language.getString("NO_OF_ASKS");
                        case 3:
                            return longTrasferObject.setValue(stock.getNoOfAsks());
                    }
                case 14:
                    switch (iCol) {
                        case 0:
                            return Language.getString("RANGE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getRange());
                        case 2:
                            return Language.getString("LT_DATE");
                        case 3:
                            return longTrasferObject.setValue(stock.getLastTradedDate());
                    }
                case 15:
                    switch (iCol) {
                        case 0:
                            return Language.getString("PCT_RANGE");
                        case 1:
                            return doubleTransferObject.setValue(stock.getPctRange());
                        case 2:
                            return Language.getString("LT_PRICE");
                        case 3:
                            return doubleTransferObject.setValue(stock.getLastTradedPrice());
                    }
                case 16:
                    switch (iCol) {
                        case 0:
                            return Language.getString("SPREAD");
                        case 1:
                            return doubleTransferObject.setValue(stock.getSpread());
                        case 2:
                            return Language.getString("52WK_HIGH");
                        case 3:
                            return doubleTransferObject.setValue(stock.getHighPriceOf52Weeks());
                    }
                case 17:
                    switch (iCol) {
                        case 0:
                            return Language.getString("PCT_SPREAD");
                        case 1:
                            return doubleTransferObject.setValue(stock.getPctSpread());
                        case 2:
                            return Language.getString("52WK_LOW");
                        case 3:
                            return doubleTransferObject.setValue(stock.getLowPriceOf52Weeks());
                    }
                /*case 18:
                    switch (iCol) {
                        case 0:
                            return Language.getString("PER");
                        case 1:
                            return doubleTransferObject.setValue(stock.getPER());
                        case 2:
                            return Language.getString("MARKET_CAP_MIL");
                        case 3:
                            return doubleTransferObject.setValue(stock.getMarketCap());
                    }*/
                case 18:
                    switch (iCol) {
                        case 0:
                            return Language.getString("YIELD");
                        case 1:
                            return doubleTransferObject.setValue(stock.getYield());
                        case 2:
                            return Language.getString("PBR");
                        case 3:
                            return doubleTransferObject.setValue(stock.getPBR());
                    }
            }

            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public int getRenderingID(int iRow, int iCol) {
        switch (iRow) {
            case 0:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 8;
                }
            case 1:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 'P';

                }
            case 2:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 'Q';

                }
            case 3:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 5;

                }
            case 4:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 6;

                }
            case 5:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'Q';
                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 6:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'p';
                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 7:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 3;

                }
            case 8:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 'n';
                }
            case 9:
                switch (iCol) {

                    case 0:
                        return 2;
                    case 1:
                        return 'R';

                    case 2:
                        return 2;
                    case 3:
                        return 'n';
                }
            case 10:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'P';

                    case 2:
                        return 2;
                    case 3:
                        return 'P';
                }
            case 11:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 4;

                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 12:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 4;
                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 13:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 4;
                    case 2:
                        return 2;
                    case 3:
                        return 4;
                }
            case 14:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 7;
                }
            case 15:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 16:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'S';
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 17:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 'S';
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                    }
                case 18:
                    switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }
            case 19:
                switch (iCol) {
                    case 0:
                        return 2;
                    case 1:
                        return 3;
                    case 2:
                        return 2;
                    case 3:
                        return 3;
                }    
        }
        return 0;
    }

    public long getTimeOffset() {
        return timeOffset;
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Model metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[7];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 3);
        customizerRecords[3] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_VALUE_UP_ROW, Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR"), Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[6] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_VALUE_DOWN_ROW, Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR"), Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }
}

