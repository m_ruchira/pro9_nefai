package com.isi.csvr.table;

import com.isi.csvr.dnd.DraggableTable;

import javax.swing.*;
import javax.swing.event.MouseInputListener;
import javax.swing.plaf.ComponentUI;
import javax.swing.plaf.TableHeaderUI;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.util.Enumeration;

/*
 * @(#)BasicTableHeaderUI.java	1.60 03/01/23
 *
 * Copyright 2003 Sun Microsystems, Inc. All rights reserved.
 * SUN PROPRIETARY/CONFIDENTIAL. Use is subject to license terms.
 */


/**
 * BasicTableHeaderUI implementation
 *
 * @author Alan Chung
 * @author Philip Milne
 * @version 1.60 01/23/03
 */
public class TWTableHeaderUI extends TableHeaderUI {

    private static Cursor resizeCursor = Cursor.getPredefinedCursor(Cursor.E_RESIZE_CURSOR);
    private static Toolkit toolkit = Toolkit.getDefaultToolkit();
    private static Image image = toolkit.getImage("images/common/drag_cursor.gif");
    private static Cursor draggCursor = toolkit.createCustomCursor(image , new Point(0,0), "");
    private int selectedColumn = -1;
//
// Instance Variables
//

    /**
     * The JTableHeader that is delegating the painting to this UI.
     */
    protected TWTableHeader header;
    protected CellRendererPane rendererPane;

    // Listeners that are attached to the JTable
    protected MouseInputListener mouseInputListener;

    /**
     * This inner class is marked &quot;public&quot; due to a compiler bug.
     * This class should be treated as a &quot;protected&quot; inner class.
     * Instantiate it only within subclasses of BasicTableUI.
     */
    public class MouseInputHandler implements MouseInputListener {

        private int mouseXOffset;
        private int draggingColumn = -1;
        private int direction = 0;
        private boolean foundFixColumn = false;
        private int fixedColumn = -1;
        private int dragDirection = -1;

//        private Cursor otherResizeCursor = resizeCursor;
//        private Cursor otherDraggingCursor = draggCursor;

        public void mouseClicked(MouseEvent e) {
            try {

                if (getResizingColumn(e.getPoint(), header.columnAtPoint(e.getPoint())) != null){
                    int col = header.getTable().convertColumnIndexToView(getResizingColumn(e.getPoint(), header.columnAtPoint(e.getPoint())).getModelIndex());

                    if (e.getClickCount() > 1){
                        header.firePropertyChange("tableColumnResizeEvent", -1, col);
                    }
                } else {
                    /*
                        check the above line where "e.getModifiers() * 1000"
                        this is because if in property listener old val 0 new val, no event fired.
                        to avoid that i have multiplied the modifier by 1000 assuming number of cols are less than 1000
                    */
                    header.firePropertyChange("tableColumnClickedEvent", e.getModifiers() * 1000, header.columnAtPoint(e.getPoint()));
                }
            } catch (Exception e1) {

            }
        }

        private boolean canResize(TableColumn column) {
            return (column != null) && header.getResizingAllowed() && column.getResizable();
        }

        private TableColumn getResizingColumn(Point p) {
            return getResizingColumn(p, header.columnAtPoint(p));
        }

        private TableColumn getResizingColumn(Point p, int column) {
            if (column == -1) {
                return null;
            }
            Rectangle r = header.getHeaderRect(column);
            r.grow(-3, 0);
            if (r.contains(p)) {
                return null;
            }
            int midPoint = r.x + r.width / 2;
            int columnIndex;
            if (header.getComponentOrientation().isLeftToRight()) {
                columnIndex = (p.x < midPoint) ? column - 1 : column;
            } else {
                columnIndex = (p.x < midPoint) ? column : column - 1;
            }
            if (columnIndex == -1) {
                return null;
            }
            if(columnIndex >= header.getColumnModel().getColumnCount()){
                columnIndex = header.getColumnModel().getColumnCount() - 1;
            }
            return header.getColumnModel().getColumn(columnIndex);
        }

        public void mousePressed(MouseEvent e) {
            header.setDraggedColumn(null);
            header.setResizingColumn(null);
            header.setDraggedDistance(0);
            draggingColumn = -1;
            foundFixColumn = false;
            fixedColumn = -1;
            Point p = e.getPoint();

            // First find which header cell was hit
            TableColumnModel columnModel = header.getColumnModel();
            int index = header.columnAtPoint(p);
            if (index != -1) {
                // The last 3 pixels + 3 pixels of next column are for resizing
                TableColumn resizingColumn = getResizingColumn(p, index);
                if (canResize(resizingColumn)) {
                    header.setResizingColumn(resizingColumn);
                    if (header.getComponentOrientation().isLeftToRight()) {
                        mouseXOffset = p.x - resizingColumn.getWidth();
                    } else {
                        mouseXOffset = p.x + resizingColumn.getWidth();
                    }
                    draggingColumn = index;
                    header.setCursor(resizeCursor);
                } else if (header.getReorderingAllowed()) {
                    int modelIndex = header.getTable().convertColumnIndexToModel(index);
                    if (!isFixedColumn(modelIndex)) { // my code
//                    if ((((SmartTable) header.getTable()).getNumFixedColumns()) != (modelIndex)) { // my code
                        TableColumn hitColumn = columnModel.getColumn(index);
                        //header.setDraggedColumn(hitColumn);
                        mouseXOffset = p.x;
                        draggingColumn = index;
                    }
                }
            }
        }

        public void mouseMoved(MouseEvent e) {
            if (canResize(getResizingColumn(e.getPoint()))){
                if (header.getCursor() != resizeCursor) {
                    header.setCursor(resizeCursor);
                }
            } else {
                header.setCursor(Cursor.getDefaultCursor());

            }
        }

        public void mouseDragged(MouseEvent e) {
            synchronized (header) {
                    int mouseX = e.getX();
                    TableColumn resizingColumn = header.getResizingColumn();
                    TableColumn draggedColumn =  header.getColumnModel().getColumn(draggingColumn);

                    boolean headerLeftToRight = header.getComponentOrientation().isLeftToRight();

                    if (resizingColumn != null) {
                        int oldWidth = resizingColumn.getWidth();
                        int newWidth;
                        if (headerLeftToRight) {
                            newWidth = mouseX - mouseXOffset;
                        } else {
                            newWidth = mouseXOffset - mouseX;
                        }
                        resizingColumn.setWidth(newWidth);

                        Container container;
                        if ((header.getParent() == null) ||
                                ((container = header.getParent().getParent()) == null) ||
                                !(container instanceof JScrollPane)) {
                            return;
                        }

                        if (!container.getComponentOrientation().isLeftToRight() &&
                                !headerLeftToRight) {
                            JTable table = header.getTable();
                            if (table != null) {
                                JViewport viewport = ((JScrollPane) container).getViewport();
                                int viewportWidth = viewport.getWidth();
                                int diff = newWidth - oldWidth;
                                int newHeaderWidth = table.getWidth() + diff;

                                /* Resize a table */
                                Dimension tableSize = table.getSize();
                                tableSize.width += diff;
                                table.setSize(tableSize);

                                /* If this table is in AUTO_RESIZE_OFF mode and
                                 * has a horizontal scrollbar, we need to update
                                 * a view's position.
                                 */
                                if ((newHeaderWidth >= viewportWidth) &&
                                        (table.getAutoResizeMode() == JTable.AUTO_RESIZE_OFF)) {
                                    Point p = viewport.getViewPosition();
                                    p.x = Math.max(0, Math.min(newHeaderWidth - viewportWidth, p.x + diff));
                                    viewport.setViewPosition(p);

                                    /* Update the original X offset value. */
                                    mouseXOffset += diff;
                                }
                            }
                        }
                } else if (draggedColumn != null) {
                        if (header.getCursor() != draggCursor){
                            header.setCursor(draggCursor);
                        }
                        Point p = e.getPoint();
                        int index = header.columnAtPoint(p);
                        int draggedDistance = 0;
                        if (headerLeftToRight) {
                            draggedDistance = mouseX - mouseXOffset;
                        } else {
                            draggedDistance = mouseXOffset - mouseX;
                        }
                        direction = (draggedDistance < 0) ? -1 : 1;
                        if((draggingColumn != index) && (selectedColumn != index) && (index != -1)){
                        int modelIndex = header.getTable().convertColumnIndexToModel(index);
                        if(/*!foundFixColumn && */isFixedColumn(modelIndex)){
                            fixedColumn = index;
                            foundFixColumn = true;
                            dragDirection = direction;
                        } /*else if((direction == dragDirection) && (index < dragDirection * fixedColumn)){
                            foundFixColumn = false;
                            fixedColumn = -1;
                        } else if((direction != dragDirection) && (index < dragDirection * fixedColumn)){
                            foundFixColumn = false;
                            fixedColumn = -1;
                        }*/else if(foundFixColumn && index < direction * fixedColumn){
                            foundFixColumn = false;
                            fixedColumn = -1;
                        }
                        if(!foundFixColumn){
                            selectedColumn = index;
                            header.repaint();
                        }
                    }
            }
        }
        }

        private boolean isFixedColumn(int column){
            if (header.getTable() instanceof SmartTable){
                int[] fixed = (((SmartTable) header.getTable()).getNumFixedColumns());
                for (int aFixed : fixed) {
                    if (aFixed == column) {
                        return true;
                    }
                }
            } /*else if (header.getTable() instanceof DraggableTable) {
                String[] fixed = (((DraggableTable) header.getTable()).getFixedColumns());
                for (String aFixed : fixed) {
                    String[] col = aFixed.split(",");
                    if (Integer.parseInt(col[0]) == column) {
                        return true;
                    }
            }
            }*/
            return false;
        }

        public void mouseReleased(MouseEvent e) {
            try {
                TableColumn resizingColumn = header.getResizingColumn();
                TableColumn draggedColumn = header.getColumnModel().getColumn(draggingColumn);//header.getDraggedColumn();
                if(resizingColumn != null){
                    header.setResizingColumn(null);
                    header.setDraggedColumn(null);
                    header.setCursor(Cursor.getDefaultCursor());
                    return;
                }
                Point p = e.getPoint();
                int index = header.columnAtPoint(p);
                if(foundFixColumn){
                    index = fixedColumn - direction;
                    System.out.println("Fixed Column =="+fixedColumn);
                    System.out.println("directionn =="+direction);
                    System.out.println("index Column =="+index);
                }
                /*if (header.getTable() instanceof DraggableTable) {
                    String[] fixed = (((DraggableTable) header.getTable()).getFixedColumns());
                    for (String aFixed : fixed) {
                        String[] col = aFixed.split(",");
                        if (draggedColumn.getModelIndex() == Integer.parseInt(col[0])) {
                            return;
                        }
                    }
                }*/
                if(index <0 && direction > 0){
                    index = header.getColumnModel().getColumnCount() -1;
                } else if(index <0 && direction < 0){
                    index = 0;
                }
                if (draggedColumn != null) {
                    header.getColumnModel().moveColumn(draggingColumn,index);
                    int modelIndex = header.getColumnModel().getColumn(draggingColumn).getModelIndex();
                    header.firePropertyChange("tableColumnDragCompleted", modelIndex, header.getTable().convertColumnIndexToView(modelIndex));
                    draggingColumn = -1;
    //                swapDraggingCursor();
                    header.setCursor(Cursor.getDefaultCursor());
                }
                selectedColumn = -1;
                /*setDraggedDistance(0, viewIndexForColumn(header.getDraggedColumn()));
                    if (header.getDraggedColumn() != null) {
                        int modelIndex = header.getDraggedColumn().getModelIndex();
                        header.firePropertyChange("tableColumnDragCompleted", modelIndex, header.getTable().convertColumnIndexToView(modelIndex));
                    }*/
                header.setResizingColumn(null);
                header.setDraggedColumn(null);
                header.setCursor(Cursor.getDefaultCursor());
            } catch (Exception e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        public void mouseEntered(MouseEvent e) {
        }

        public void mouseExited(MouseEvent e) {
        }
//
// Protected & Private Methods
//

        private void setDraggedDistance(int draggedDistance, int column) {
            header.setDraggedDistance(draggedDistance);
            if (column != -1) {
                header.getColumnModel().moveColumn(column, column);
            }
        }
    }

//
//  Factory methods for the Listeners
//

    /**
     * Creates the mouse listener for the JTable.
     */
    protected MouseInputListener createMouseInputListener() {
        return new MouseInputHandler();
    }

//
//  The installation/uninstall procedures and support
//
    public static ComponentUI createUI(JComponent h) {
        return new javax.swing.plaf.basic.BasicTableHeaderUI();
    }

//  Installation
    public void installUI(JComponent c) {
        header = (TWTableHeader) c;

        rendererPane = new CellRendererPane();
        header.add(rendererPane);

        installDefaults();
        installListeners();
        installKeyboardActions();
    }

    /**
     * Initialize JTableHeader properties, e.g. font, foreground, and background.
     * The font, foreground, and background properties are only set if their
     * current value is either null or a UIResource, other properties are set
     * if the current value is null.
     *
     * @see #installUI
     */
    protected void installDefaults() {
        LookAndFeel.installColorsAndFont(header, "TableHeader.background",
                "TableHeader.foreground", "TableHeader.font");
    }

    /**
     * Attaches listeners to the JTableHeader.
     */
    protected void installListeners() {
        mouseInputListener = createMouseInputListener();

        header.addMouseListener(mouseInputListener);
        header.addMouseMotionListener(mouseInputListener);
    }

    /**
     * Register all keyboard actions on the JTableHeader.
     */
    protected void installKeyboardActions() {
    }

// Uninstall methods
    public void uninstallUI(JComponent c) {
        uninstallDefaults();
        uninstallListeners();
        uninstallKeyboardActions();

        header.remove(rendererPane);
        rendererPane = null;
        header = null;
    }

    protected void uninstallDefaults() {
    }

    protected void uninstallListeners() {
        header.removeMouseListener(mouseInputListener);
        header.removeMouseMotionListener(mouseInputListener);

        mouseInputListener = null;
    }

    protected void uninstallKeyboardActions() {
    }

//
// Paint Methods and support
//
    public void paint(Graphics g, JComponent c) {
        if (header.getColumnModel().getColumnCount() <= 0) {
            return;
        }
        boolean ltr = header.getComponentOrientation().isLeftToRight();

        Rectangle clip = g.getClipBounds();
        Point left = clip.getLocation();
        Point right = new Point(clip.x + clip.width - 1, clip.y);
        TableColumnModel cm = header.getColumnModel();
        int cMin = header.columnAtPoint(ltr ? left : right);
        int cMax = header.columnAtPoint(ltr ? right : left);
        // This should never happen.
        if (cMin == -1) {
            cMin = 0;
        }
        // If the table does not have enough columns to fill the view we'll get -1.
        // Replace this with the index of the last column.
        if (cMax == -1) {
//            System.out.println("columns are missing in the column model");
            cMax = cm.getColumnCount() - 1;
        }

        TableColumn draggedColumn = header.getDraggedColumn();
        int columnWidth;
        int columnMargin = cm.getColumnMargin();
        Rectangle cellRect = header.getHeaderRect(cMin);
        TableColumn aColumn;
        if (ltr) {
            for (int column = cMin; column <= cMax; column++) {
                aColumn = cm.getColumn(column);
                columnWidth = aColumn.getWidth();
                cellRect.width = columnWidth - columnMargin;
                if (aColumn != draggedColumn) {
                    paintCell(g, cellRect, column);
                }
                cellRect.x += columnWidth;
            }
        } else {
            aColumn = cm.getColumn(cMin);
            if (aColumn != draggedColumn) {
                columnWidth = aColumn.getWidth();
                cellRect.width = columnWidth - columnMargin;
                cellRect.x += columnMargin;
                paintCell(g, cellRect, cMin);
            }
            for (int column = cMin + 1; column <= cMax; column++) {
                aColumn = cm.getColumn(column);
                columnWidth = aColumn.getWidth();
                cellRect.width = columnWidth - columnMargin;
                cellRect.x -= columnWidth;
                if (aColumn != draggedColumn) {
                    paintCell(g, cellRect, column);
                }
            }
        }

        // Paint the dragged column if we are dragging.
        /*if (draggedColumn != null) {
            int draggedColumnIndex = viewIndexForColumn(draggedColumn);
            Rectangle draggedCellRect = header.getHeaderRect(draggedColumnIndex);

            // Draw a gray well in place of the moving column.
            g.setColor(header.getParent().getBackground());
            g.fillRect(draggedCellRect.x, draggedCellRect.y,
                    draggedCellRect.width, draggedCellRect.height);

            draggedCellRect.x += header.getDraggedDistance();

            // Fill the background.
            g.setColor(header.getBackground());
            g.fillRect(draggedCellRect.x, draggedCellRect.y,
                    draggedCellRect.width, draggedCellRect.height);


            paintCell(g, draggedCellRect, draggedColumnIndex);
            g.setColor(header.getForeground());
            g.drawString((String)draggedColumn.getHeaderValue(),draggedCellRect.x, draggedCellRect.y );
        }*/

        // Remove all components in the rendererPane.
        rendererPane.removeAll();
    }

    private Component getHeaderRenderer(int columnIndex) {
            TableColumn aColumn = header.getColumnModel().getColumn(columnIndex);
            TableCellRenderer renderer = aColumn.getHeaderRenderer();
            if (renderer == null) {
                renderer = header.getDefaultRenderer();
            }

            return renderer.getTableCellRendererComponent(header.getTable(),
                    aColumn.getHeaderValue(), false, false,
                    -1, columnIndex);
    }

    private Component getHeaderRenderer(TableColumn aColumn, int columnIndex) {
//        TableColumn aColumn = header.getColumnModel().getColumn(columnIndex);
//        try {
        TableCellRenderer renderer = aColumn.getHeaderRenderer();
        if (renderer == null) {
            renderer = header.getDefaultRenderer();
        }
        return renderer.getTableCellRendererComponent(header.getTable(),
                aColumn.getHeaderValue(), false, false,
                -1, columnIndex);
//        } catch(Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            return null;
//        }
    }

    private void paintCell(Graphics g, Rectangle cellRect, int columnIndex) {
        ((Graphics2D)g).setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
        Component component = getHeaderRenderer(columnIndex);
        rendererPane.paintComponent(g, component, header, cellRect.x, cellRect.y,
                cellRect.width, cellRect.height, true);
        if(columnIndex == selectedColumn){
            Color oldColor = g.getColor();
            Rectangle r = header.getHeaderRect(selectedColumn);
//            System.out.println("selected column =="+selectedColumn);
//            r.grow(-2, 0);
//            int midPoint = r.x + r.width / 2;
//            int width  = draggedColumn.getPreferredWidth();
//            int height = header.getHeight();
//            Font oldFont = header.getGraphics().getFont();
//            header.getGraphics().drawRect(p.x,p.y, width, height);
//            System.out.println("ssoout"+text);
//            Color oldColor = header.getBackground();
            g.setColor(Color.RED);
            g.drawRect(r.x+1, r.y +1,r.width -2 ,r.height -2);
            g.drawRect(r.x+2, r.y +2,r.width - 4,r.height-4);
            g.setColor(oldColor);

        }

    }

    private int viewIndexForColumn(TableColumn aColumn) {
        TableColumnModel cm = header.getColumnModel();
        for (int column = 0; column < cm.getColumnCount(); column++) {
            if (cm.getColumn(column) == aColumn) {
                return column;
            }
        }
        return -1;
    }

//
// Size Methods
//
    private int getHeaderHeight() {
        int height = 0;
            boolean accomodatedDefault = false;
            TableColumnModel columnModel = header.getColumnModel();
            for (int column = 0; column < columnModel.getColumnCount(); column++) {
                    TableColumn aColumn = columnModel.getColumn(column);
                    // Configuring the header renderer to calculate its preferred size is expensive.
                    // Optimise this by assuming the default renderer always has the same height.
                    if (aColumn.getHeaderRenderer() != null || !accomodatedDefault) {
        //                Component comp = getHeaderRenderer(column);
                        Component comp = getHeaderRenderer(aColumn, column);
                        int rendererHeight = comp.getPreferredSize().height;
                        height = Math.max(height, rendererHeight);
                        // If the header value is empty (== "") in the
                        // first column (and this column is set up
                        // to use the default renderer) we will
                        // return zero from this routine and the header
                        // will disappear altogether. Avoiding the calculation
                        // of the preferred size is such a performance win for
                        // most applications that we will continue to
                        // use this cheaper calculation, handling these
                        // issues as `edge cases'.
                        if (rendererHeight > 0) {
                            accomodatedDefault = true;
                        }
                    }
            }
        return height;
    }

    private Dimension createHeaderSize(long width) {
            TableColumnModel columnModel = header.getColumnModel();
            // None of the callers include the intercell spacing, do it here.
            if (width > Integer.MAX_VALUE) {
                width = Integer.MAX_VALUE;
            }
            return new Dimension((int) width, getHeaderHeight());
    }

    /**
     * Return the minimum size of the header. The minimum width is the sum
     * of the minimum widths of each column (plus inter-cell spacing).
     */
    public Dimension getMinimumSize(JComponent c) {
        long width = 0;
        Enumeration enumeration = header.getColumnModel().getColumns();
        while (enumeration.hasMoreElements()) {
            TableColumn aColumn = (TableColumn) enumeration.nextElement();
            width = width + aColumn.getMinWidth();
        }
        return createHeaderSize(width);
    }

    /**
     * Return the preferred size of the header. The preferred height is the
     * maximum of the preferred heights of all of the components provided
     * by the header renderers. The preferred width is the sum of the
     * preferred widths of each column (plus inter-cell spacing).
     */
    public Dimension getPreferredSize(JComponent c) {
        synchronized (header) {
            long width = 0;
            Enumeration enumeration = header.getColumnModel().getColumns();
            while (enumeration.hasMoreElements()) {
                TableColumn aColumn = (TableColumn) enumeration.nextElement();
                width = width + aColumn.getPreferredWidth();
            }
            return createHeaderSize(width);
        }
    }

    /**
     * Return the maximum size of the header. The maximum width is the sum
     * of the maximum widths of each column (plus inter-cell spacing).
     */
    public Dimension getMaximumSize(JComponent c) {
        long width = 0;
        Enumeration enumeration = header.getColumnModel().getColumns();
        while (enumeration.hasMoreElements()) {
            TableColumn aColumn = (TableColumn) enumeration.nextElement();
            width = width + aColumn.getMaxWidth();
        }
        return createHeaderSize(width);
    }

}  // End of Class BasicTableHeaderUI



