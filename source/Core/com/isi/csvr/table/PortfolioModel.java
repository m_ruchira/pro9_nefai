// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.table;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.PortfolioTable;
import com.isi.csvr.datastore.*;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.ticker.custom.TradeFeeder;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;


public class PortfolioModel extends CommonTable
        implements TableModel, CommonTableInterface {

    private PortfolioStore dataStore;
    private PortfolioTable table;
    //private String  baseCurrency = "SR";

    /**
     * Constructor
     */
    public PortfolioModel(PortfolioStore dataStore, PortfolioTable table) {
        this.dataStore = dataStore;
        this.table = table;
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public void setSymbol(String symbol) {

    }

    public int getRowCount() {
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Portfolio)) {
            return dataStore.getRowCount() + 2;
        } else {
            return 0;
        }
    }

    public Object getValueAt(int row, int col) {
        //float conversionRate = CurrencyStore.getCurrency("SR|" + getBaseCurrency());
        if (row < (getRowCount() - 2)) { // only transaction rows
            PortfolioRecord record = dataStore.getValueAt(row);
            Stock stock = DataStore.getSharedInstance().getStockObject(record.getSymbol());
            switch (col) {
                case 0:
                    return record.getSymbol();
                case 1:
                    return stock.getLongDescription();
                case 2:
                    return stock.getCurrencyCode();
                case 3:
                    return new Double(CurrencyStore.getRate(dataStore.getBaseCurrency(), stock.getCurrencyCode()));
                case 4:
                    return new Long(record.getHolding());
                case 5:
                    return new Double(record.getAvgCost());
                case 6:
                    if (stock.getLastTradeValue() > 0)
                        return new Double(stock.getLastTradeValue());
                    else
                        return new Double(stock.getPreviousClosed());
                case 7:
                    return new Double(record.getCoupon());
                case 8:
                    return new Double(record.getTotalCost());
                case 9:
                    return record.getMarketValue();
                case 10:
                    return record.getGainLoss();
                case 11:
                    return record.getPctGainLoss();
                default:
                    return "";
            }
        } else if (row == (getRowCount() - 2)) {
            return "";
        } else if (row == (getRowCount() - 1)) {
            switch (col) {
                case 8:
                    return new Double(dataStore.getTotalCost());
                case 9:
                    return dataStore.getTotalMarketValue();
                case 10:
                    return dataStore.getTotalGainLoss();
                case 11:
                    return dataStore.getTotalPctGainLoss();
                default:
                    return "";
            }
        } else {
            return "";
        }

    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        //return getValueAt(0, iCol).getClass();
        if ((iCol == 6) || (iCol >= 9)) {
            return Number[].class;
        } else {
            return getValueAt(0, iCol).getClass();
        }
        //return Object.class;

        /*switch(super.getViewSettings().getRendererID(iCol))
        {
            case 0:
            case 1:
            case 2:
            case 4:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 3:
            case 5:
            case 6:
            //case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }*/
        /*switch(super.getViewSettings().getRendererID(iCol))
      {
          case 0:
          case 1:
          case 2:
          case 'B':
              return String.class;
          case 'P':
          case 'Q':
              return Number[].class;
          case 3:
          case 5:
          case 6:
          case 4:
          case 7:
          case 8:
          case 'M':
          case 'S':
              return Number.class;
          default:
              return Object.class;
      }  */
    }

    public boolean isCellEditable(int row, int col) {
        if ((row == (getRowCount() - 2)) && (col == 0)) {
            return true;
        } else if ((row < (getRowCount() - 2)) && ((col == 4) || (col == 5) || (col == 7))) {
            return true;
        }

        return false;
    }

    public void setValueAt(Object value, int rowIndex, int column) {
        if (column == 0) {
            if (DataStore.getSharedInstance().isValidSymbol(((String) value).toUpperCase())) {
                PortfolioRecord record = new PortfolioRecord();
                record.setSymbol(((String) value).toUpperCase());
                record.setHolding(0);
                record.setAvgCost(0);
                dataStore.addRecord(record);
                record = null;
                getTable().updateUI();
//				table.updateTimeAnsSalesSymbols(true);
                TradeFeeder.refreshSymbolIndex();
            }
        } else if (column == 4) {
            try {
                if (!isEmptyRow(rowIndex)) {
                    if (toLongValue(value) >= 0) {
                        if (toLongValue(value) <= 99999999) {
                            PortfolioRecord record = dataStore.getRecord(getSymbol(rowIndex));
                            if (record != null) {
                                record.setHolding(toLongValue(value));
                            }
                            record = null;
                        } else {
                            JOptionPane.showMessageDialog(getTable1(),
                                    Language.getString("NUMBER_TOO_LONG"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    getTable().updateUI();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (column == 5) {
            try {
                if (!isEmptyRow(rowIndex)) {
                    if (toDoubleValue(value) >= 0) {
                        if (toDoubleValue(value) <= 99999) {
                            PortfolioRecord record = dataStore.getRecord(getSymbol(rowIndex));
                            if (record != null) {
//                                record.setAvgCost((float) toDoubleValue(value));
                                record.setAvgCost(toDoubleValue(value));
                            }
                            record = null;
                        } else {
                            JOptionPane.showMessageDialog(getTable1(),
                                    Language.getString("NUMBER_TOO_LONG"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    getTable().updateUI();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if (column == 7) {
            try {
                if (!isEmptyRow(rowIndex)) {
                    if (toDoubleValue(value) >= 0) {
                        if (toDoubleValue(value) <= 99999) {
                            PortfolioRecord record = dataStore.getRecord(getSymbol(rowIndex));
                            if (record != null) {
                                record.setCoupon((float) toDoubleValue(value));
                            }
                            record = null;
                        } else {
                            JOptionPane.showMessageDialog(getTable1(),
                                    Language.getString("NUMBER_TOO_LONG"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                        }
                    }
                    getTable().updateUI();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public String getSymbol(int row) {
        return (String) getValueAt(row, 0);
    }

    public boolean isEmptyRow(int row) {
        if (getSymbol(row).equals("")) {
            return true;
        }
        return false;
    }
//    private long toLongValue(Object oValue) throws Exception
//    {
//        //return ((Long)oValue).longValue();
//         return Long.parseLong((String)oValue);
//    }
//
//    private int toIntValue(Object oValue) throws Exception
//    {
//        return Integer.parseInt((String)oValue);
//    }
//
//    private double toDoubleValue(Object oValue) throws Exception
//    {
//        return ((Double)oValue).doubleValue();
//    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return Double.parseDouble((String) oValue);
    }

    private int toIntValue(Object oValue) throws Exception {
        return Integer.parseInt((String) oValue);
    }

    private float toFloatValue(Object oValue) throws Exception {
        return Float.parseFloat((String) oValue);
    }
    /*public void setBaseCurrency(String newBaseCurrency){
        System.out.println("setting base " + newBaseCurrency);
        baseCurrency = newBaseCurrency;
    }

    public String getBaseCurrency(){
        return baseCurrency;
    } */
}

