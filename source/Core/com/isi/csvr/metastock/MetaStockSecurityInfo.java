package com.isi.csvr.metastock;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Jun 24, 2010
 * Time: 11:41:25 AM
 * To change this template use File | Settings | File Templates.
 */
public class MetaStockSecurityInfo {

    private String symbol;
    private String description;

    public MetaStockSecurityInfo(String symbol, String desc) {
        this.symbol = symbol;
        this.description = desc;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }


}
