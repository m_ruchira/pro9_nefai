package com.isi.csvr.metastock;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jul 26, 2004
 * Time: 2:32:36 PM
 */
public class HistoryRecord {
    public int date;
    public float open;
    public float high;
    public float low;
    public float close;
    public float volume;

    public HistoryRecord(int date,float open,float high,float low,float close,float volume){
          this.date = date;
          this.open = open;
          this.high = high;
          this.low = low;
          this.close = close;
          this.volume = volume;
    }
}
