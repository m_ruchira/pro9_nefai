/**
 * Author Uditha Nagahawatta
 * Listens to dde requests and register them until they are removed.
 * Registered requests are updated every second.
 * @see DdeDataRecord, DdeRecordStore
 */
package com.isi.csvr.dde;

import com.isi.csvr.Client;
import com.isi.csvr.MainBoardLoader;
import com.isi.csvr.cashFlowWatch.CashFlowHistorySummaryUI;
import com.isi.csvr.cashFlowWatch.CashFlowSummaryModel;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.StockData;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.indices.IndicesFrame;
import com.isi.csvr.marketdepth.CombinedBidAsk;
import com.isi.csvr.marketdepth.DepthObject;
import com.isi.csvr.marketdepth.DepthStore;
import com.isi.csvr.sectoroverview.SectorOverviewModal;
import com.isi.csvr.sectoroverview.SectorOverviewUI;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.IndicesModal;
import com.isi.csvr.table.MarketModel;
import com.neva.Coroutine;
import com.neva.DdeException;
import com.neva.DdeServerTransactionEvent;
import com.neva.DdeServerTransactionRejectedException;

import javax.swing.*;
import java.util.TimeZone;


public class DDEServer implements Runnable, ApplicationListener, ConnectionListener {
    private Thread th;
    private com.neva.DdeServer ddeServer;
    private int xltablefmt;
    private DdeRecordStore dataStore;
    private static final String LOCK = "";
    private static long lastErorMessageTime = 0;
    private static boolean ddeOn = true;

    /**
     * Strt the dde server.
     */

    public DDEServer(){
        Application.getInstance().addApplicationListener(this);
        ConnectionNotifier.getInstance().addConnectionListener(this);
    }

    public void runServer() {
        System.out.println("Running dde");
        dataStore = new DdeRecordStore();

        try {
//            Application.getInstance().addApplicationListener(this);
//            ConnectionNotifier.getInstance().addConnectionListener(this);
            xltablefmt = com.neva.DdeServer.ddeRegisterClipboardFormat("XlTable");

            ddeServer = new com.neva.DdeServer();
            ddeServer.setService("MRegionalDdeServer");

            //Register connection event listener
            ddeServer.addDdeServerConnectionEventListener(new com.neva.DdeServerConnectionEventAdaptor() {
                public void onConnect(com.neva.DdeServerConnectionEvent e)
                        throws com.neva.DdeServerConnectionRejectedException {
                    // Make sure the client is asking for the right topic
                    sendDDEOnMessage();
                    /*if (e.getTopic().substring(0, 2).equals("AS")){ // check if a market summary request
                        if (ExchangeStore.getSharedInstance().isValidExchange(e.getTopic().substring(2))){
                            return;
                        }
                    } else {
                        if (isValidItem(e.getTopic().substring(2))) {
                            return;
                        }
                    }*/
                    if (isValidItem(e.getTopic())) {
                        return;
                    }
                    throw new com.neva.DdeServerConnectionRejectedException();
                }
            });

            //Register transaction event listener
            ddeServer.addDdeServerTransactionEventListener(new com.neva.DdeServerTransactionEventAdaptor() {

                public void onAdvStart(com.neva.DdeServerTransactionEvent e)
                        throws com.neva.DdeServerTransactionRejectedException {
                    // Make sure the client is asking for the right topic and item
                    if (ExchangeStore.isValidSystemFinformationType(Meta.IT_DDE_Link)) {
                        SharedMethods.updateGoogleAnalytics("DDELink");
                        if (isValidItem(e.getTopic())) {
                            try {
                                int iColumn = Integer.parseInt(e.getItem());
                                DdeDataRecord record = new DdeDataRecord(e.getTopic(), iColumn);

                                synchronized (LOCK) {
                                    dataStore.addRecord(record);
                                    record = null;
                                }
                            } catch (Exception x) {
                            }
                            return;
                        }
                    } else {
                        showRejectMessage();
                    }

                    throw new com.neva.DdeServerTransactionRejectedException();
                }

                public void onAdvStop(DdeServerTransactionEvent event)
                        throws DdeServerTransactionRejectedException {
                    synchronized (LOCK) {
                        dataStore.removeRecord(event.getTopic(), event.getItem());
                        if (dataStore.getSize() == 0) {
                            sendDDEOffMessage();
                        }
                    }
                }

                public void onAdvReq(com.neva.DdeServerTransactionEvent e)
                        throws com.neva.DdeServerTransactionRejectedException {
                    //Data is ready. Check whether the format is correct
                    if ((isDdeOn()) && (e.getFormat() == xltablefmt)) {
                        DdeDataRecord record = dataStore.getRecord(e.getTopic(), e.getItem());
                        String value = getValue(record);
                        record.setLastValue(value);
                        byte[] xldata = buildXLTable(value);
                        e.setRequestedData(xldata);
                        record = null;
                        value = null;
                        return;
                    }
                    throw new com.neva.DdeServerTransactionRejectedException();
                }

                public void onRequest(com.neva.DdeServerTransactionEvent e)
                        throws com.neva.DdeServerTransactionRejectedException {
                    // Make sure the client is asking for the right topic and item
                    /*if (e.getTopic().substring(0, 2).equals("AS")){ // check if a market summary request
                        if (ExchangeStore.getSharedInstance().isValidExchange(e.getTopic().substring(2))){
                            return;
                        }
                    }else{
                        if (!isValidItem(e.getTopic().substring(2))) {
                            throw new com.neva.DdeServerTransactionRejectedException();
                        }
                    }*/
                    if (!isValidItem(e.getTopic())) {
                       throw new com.neva.DdeServerTransactionRejectedException();
                    }
                    //Return requested data. Check whether the requested format is the one that we support
                    if (e.getFormat() == xltablefmt) {

                        DdeDataRecord record = dataStore.getRecord(e.getTopic(), e.getItem());
                        if (record != null) {
                            String value = getValue(record);
                            record.setLastValue(value);
                            byte[] xldata = buildXLTable(value);
                            e.setRequestedData(xldata);
                            record = null;
                            value = null;
                        }
                        return;
                    }

                    throw new com.neva.DdeServerTransactionRejectedException();
                }
            });

            // Start DDE server
            /* ddeServer.start();
          System.out.println("Dde server started.");
          System.out.println("Waiting for transaction...");*/


            // Create a thread to simulate changing data
            th = new Thread(this, "DdeServer");
            th.start();

        } catch (Exception exc) {
            System.out.println("Unable to start DDE server");
            exc.printStackTrace();
        }
    }

    byte[] buildXLTable(String s) {
        // This method answers data in xltable format
        byte[] arr = new byte[12 + 1 + s.length()];
        Coroutine.setWORDAtOffset(arr, 16, 0);
        Coroutine.setWORDAtOffset(arr, 4, 2);
        Coroutine.setWORDAtOffset(arr, 1, 4);
        Coroutine.setWORDAtOffset(arr, 1, 6);
        Coroutine.setWORDAtOffset(arr, 2, 8);
        Coroutine.setWORDAtOffset(arr, 1 + s.length(), 10);
        Coroutine.setBYTEAtOffset(arr, s.length(), 12);
        System.arraycopy(s.getBytes(), 0, arr, 13, s.length());
        return arr;

    }

    /**
     * Update all dde clients every second
     * Update is done only if source data is changed
     */
    public void run() {
        while (true) {
            try {
                if (dataStore.getSize() > 0) {
                    synchronized (LOCK) {
                        Object[] records = dataStore.getRecords();
                        DdeDataRecord record;

                        for (int i = 0; i < records.length; i++) {
                            record = (DdeDataRecord) records[i];
                            if (isValueChanged(record)) {
                                ddeServer.postAdvise(record.getTopic(), "" + record.getColumn());
                            }
                        }
                        records = null;
                        record = null;
                    }
                }
                Thread.sleep(1000);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void stopServer() {
        try {
            ddeServer.stop();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    /**
     * Check if the item (Symbol) is valid
     *
     * @param request
     */
    private boolean isValidItem(String request) {
        try {
            if (request.substring(0, 2).equals("SO")){ // check if a sector overview request
                return ExchangeStore.getSharedInstance().isValidExchange(request.substring(2).split(Constants.KEY_SEPERATOR_CHARACTER)[0]);
            } else {
                return DataStore.getSharedInstance().isValidSymbol(request.substring(2));
            }
        } catch (Exception e) {
            return false;
        }

    }

    /**
     * Check if the value of the dde record has been chenged
     */
    private boolean isValueChanged(DdeDataRecord record) {
        String value = null;
        value = getValue(record);// StockData.getValue(record.getSymbol(), record.getColumn());
        return (!value.equals(record.getLastValue()));
    }

    /**
     * Return the value for the topic/item pair
     *
     * @return string value for the topic/item pair
     */
    private String getValue(DdeDataRecord record) {
        try {
            int iItem = record.getColumn();
            String value = null;
            if (record.getTopic().substring(0, 2).equals("BO")) {
                value = StockData.getValue(record.getTopic().substring(2), iItem);
            }else if(record.getTopic().substring(0,2).equals("SO")){
                String exg = record.getTopic().substring(2).split(Constants.KEY_SEPERATOR_CHARACTER)[0];
                if (MainBoardLoader.tableStore.containsKey(exg)) {
                    value = ((SectorOverviewModal)((SectorOverviewUI)(MainBoardLoader.tableStore.get(exg))).getTableComponent().getModel()).getDDEString(record.getTopic().substring(2).split(Constants.KEY_SEPERATOR_CHARACTER)[1],iItem);
                }
            }else if (record.getTopic().substring(0, 2).equals("IN")) {
                value = ((IndicesModal) IndicesFrame.getSharedInstance().getTableComponent().getModel()).getDDEString(record.getTopic().substring(2), iItem);
            } else if (record.getTopic().substring(0, 2).equals("AS")) {
                value = ((MarketModel) Client.getInstance().getAssetClassTable().getModel()).getDDEString(record.getTopic().substring(2), iItem);
            }else if(record.getTopic().substring(0, 2).equals("CF")){
                value = ((CashFlowSummaryModel) CashFlowHistorySummaryUI.getSharedInstance().getSummaryTable().getModel()).getDDEString(record.getTopic().substring(2), iItem);
            }
            else if(record.getTopic().substring(0, 2).equals("CB")){
                DepthObject depthObject = DepthStore.getInstance().getDepthFor(record.getTopic().substring(2));
                CombinedBidAsk combined = (CombinedBidAsk) depthObject.getComnbinedOrderList().get(iItem/100);
                value = combined.getDDEValue(iItem % 100);
                combined = null;
            }else {
                try {
                    BidAsk bidAsk;
                    byte side;
                    if (record.getTopic().substring(1, 2).equals("B")) {
                        side = DepthObject.BID;
                    } else {
                        side = DepthObject.ASK;
                    }
                    DepthObject depthObject = DepthStore.getInstance().getDepthFor(record.getTopic().substring(2));
                    if (record.getTopic().substring(0, 1).equals("P")) {
                        bidAsk = (BidAsk) depthObject.getPriceList(side).get(iItem / 100);
                        value = bidAsk.getDDEValue(DepthObject.TYTE_PRICE, iItem % 100);
                    } else if (record.getTopic().substring(0, 1).equals("O")) {
                        bidAsk = (BidAsk) depthObject.getOrderList(side).get(iItem / 100);
                        value = bidAsk.getDDEValue(DepthObject.TYTE_ORDER, iItem % 100);
                    } else if (record.getTopic().substring(0, 1).equals("S")) {
                        bidAsk = (BidAsk) depthObject.getSpecialList(side).get(iItem / 100);
                        value = bidAsk.getDDEValue(DepthObject.TYTE_SPECIAL, iItem % 100);
                    }
                    bidAsk = null;
                } catch (Exception e) {
                    value = "0";
                }
            }
            return value;
        } catch (Exception e) {
            //e.printStackTrace();
            return "error";
        }
    }

    public static boolean isDdeOn() {
        return ddeOn;
    }

    public static void setDdeOn(boolean ddeOn) {
        DDEServer.ddeOn = ddeOn;
    }

    /**
     * set the last valu for the data record
     */
    private void setLastValue(DdeDataRecord record) {
        try {
            String value = null;
            value = getValue(record);
            record.setLastValue(value);
        } catch (Exception e) {
            record.setLastValue("");
        }
    }

    private synchronized void showRejectMessage() {
        if ((System.currentTimeMillis() - lastErorMessageTime) > 5000) {
            lastErorMessageTime = System.currentTimeMillis();
            SharedMethods.showNonBlockingMessage(Language.getString("DDE_NOT_SUBSCRIBED"), JOptionPane.WARNING_MESSAGE);
        }
    }

    private void sendDDEOnMessage() {
        SendQFactory.addData(Constants.PATH_PRIMARY, Meta.DDE_ON + Meta.DS + Settings.getSessionID());
    }

    private void sendDDEOffMessage() {
        SendQFactory.addData(Constants.PATH_PRIMARY, Meta.DDE_OFF + Meta.DS + Settings.getSessionID());
    }

    /**
     * Application exit in progress
     */
    public void applicationExiting() {

    }

    /**
     * Fires after application loaded and main window is shown
     */
    public void applicationLoaded() {
        runServer();

    }

    /**
     * Application loading in progress
     *
     * @param percentage
     */
    public void applicationLoading(int percentage) {

    }

    /**
     * Fires after Application authenticated and initialized
     */
    public void applicationReadyForTransactions() {
        //if (ExchangeStore.isValidSystemFinformationType(Meta.IT_DDE_Link)){
        try {
            ddeServer.start();
        } catch (DdeException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        //}
    }

    /**
     * Fires after application time zone change
     *
     * @param zone
     */
    public void applicationTimeZoneChanged(TimeZone zone) {

    }

    /**
     * Processing of snapshot ended
     */
    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    /**
     * Processing of snapshot started
     */
    public void snapshotProcessingStarted(Exchange exchange) {

    }

    /**
     * Workspace load completed
     */
    public void workspaceLoaded() {

    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    /**
     * Workspace about to load
     */
    public void workspaceWillLoad() {

    }

    /**
     * Connected to the server
     */
    public void twConnected() {
        if (dataStore.getSize() > 0) {
            sendDDEOnMessage();
        }
    }

    /**
     * Connection with the server failed
     */
    public void twDisconnected() {
        ddeServer.stop();
    }
}
