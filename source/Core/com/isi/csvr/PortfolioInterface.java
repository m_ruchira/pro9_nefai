package com.isi.csvr;

import com.isi.csvr.bandwidthoptimizer.OptimizedTreeRenderer;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.*;
import com.isi.csvr.history.HistoryFilesManager;
import com.isi.csvr.portfolio.PFStore;
import com.isi.csvr.portfolio.PFUtilities;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.datastore.FutureBaseAttributes;
import com.isi.csvr.trading.datastore.FutureBaseStore;
import com.isi.csvr.trading.shared.TradeKey;

import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class PortfolioInterface {
    public static final float DEFAULT_DOUBLE_VALUE = -9999;
    public static final String DEFAULT_THEME_PATH = "images/Theme";

    public PortfolioInterface() {
    }

    public static byte getPortfolioVersion() {
        return PFUtilities.TADAWUL_VERSION;
    }

    /**
     * @param isExtraSymbol
     * @param key
     */
    public static void setSymbol(boolean isExtraSymbol, TradeKey key) {
        Stock stock = DataStore.getSharedInstance().getStockByExchangeKey(key.getExchange(),key.getSymbol());
        boolean status = false;
        String exchange = SharedMethods.getExchangeFromKey(stock.getKey());
        Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);
        if (ex.isDefault() && !OptimizedTreeRenderer.selectedSymbols.containsKey(stock.getKey())) {
            SendQFactory.addOptimizationRecord(exchange, SharedMethods.getSymbolFromKey(stock.getKey()) + ":" + SharedMethods.getInstrumentTypeFromKey(stock.getKey()), SharedMethods.getInstrumentTypeFromKey(stock.getKey()));
            OptimizedTreeRenderer.selectedSymbols.put(stock.getKey(), true);
//            Stock st = DataStore.getSharedInstance().getStockObject(key);
//            if(st!=null){
//                status = st.isSymbolEnabled();
//            }else{
//                status = true;
//            }
        } else {
            if(ValidatedSymbols.getSharedInstance().getFrame(stock.getKey())==null && !ex.isDefault()){
                String requestID = "PF_UNCHECKED_ADDSYMBOL" + ":" + System.currentTimeMillis();
                SendQFactory.addValidateRequest(SharedMethods.getSymbolFromKey(stock.getKey()), requestID, exchange, SharedMethods.getInstrumentTypeFromKey(stock.getKey()) );
            }else{
                DataStore.getSharedInstance().addSymbolRequest(stock.getKey());
            }
            OptimizedTreeRenderer.selectedSymbols.put(stock.getKey(), true);
        }

        if (stock != null) {
            stock.setSymbolEnabled(true);
        }


    }
    public static void setSymbol(boolean isExtraSymbol, String key) {
        boolean status = false;
        String exchange = SharedMethods.getExchangeFromKey(key);
        Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);
        if (ex.isDefault() && !OptimizedTreeRenderer.selectedSymbols.containsKey(key)) {
            SendQFactory.addOptimizationRecord(exchange, SharedMethods.getSymbolFromKey(key) + ":" + SharedMethods.getInstrumentTypeFromKey(key), SharedMethods.getInstrumentTypeFromKey(key));
            OptimizedTreeRenderer.selectedSymbols.put(key, true);
//            Stock st = DataStore.getSharedInstance().getStockObject(key);
//            if(st!=null){
//                status = st.isSymbolEnabled();
//            }else{
//                status = true;
//            }
        } else {
            if(ValidatedSymbols.getSharedInstance().getFrame(key)==null && !ex.isDefault()){
                 String requestID = "PF_UNCHECKED_ADDSYMBOL" + ":" + System.currentTimeMillis();
                DataStore.getSharedInstance().checkSymbolAvailability(SharedMethods.getSymbolFromKey(key), requestID, exchange, SharedMethods.getInstrumentTypeFromKey(key));

//                SendQFactory.addValidateRequest(SharedMethods.getSymbolFromKey(key), requestID, exchange, SharedMethods.getInstrumentTypeFromKey(key) );
            }else{
            DataStore.getSharedInstance().addSymbolRequest(key);
            }
            OptimizedTreeRenderer.selectedSymbols.put(key, true);
        }

        Stock stock = DataStore.getSharedInstance().getStockObject(key);
        if (stock != null) {
            stock.setSymbolEnabled(true);
        }


    }

    /**
     * @param key
     * @param caller
     */
    public static void validateSymbol(String key, String callerID, Object caller) {
        if ((ExchangeStore.isNoneDefaultExchangesAvailable())) { // must validate from server
            String requestID = callerID + ":" + System.currentTimeMillis();
            SendQFactory.addValidateRequest(key, requestID, null, 0);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, caller);
            //return true;
        } else {
//            if ((DataStore.getSharedInstance().isValidSymbol(key))) {
            String symbol = SymbolMaster.getExchangeForSymbol(key, false);
            if ((symbol != null)) {
                ((TransactionDialogInterface) caller).setSymbol(symbol);
            } else {
                ((TransactionDialogInterface) caller).setSymbol(null);
                new ShowMessage(false, Language.getString("MSG_INVLID_SYMBOL"), "E");
            }
            symbol = null;
        }
    }

    public static String getCompanyName(String symbol) {
        String key = DataStore.getSharedInstance().searchSymbol(symbol);
        if (key != null) {
            return DataStore.getSharedInstance().getStockObject(key).getLongDescription();
        }
        return "";
    }

    public static boolean isOfflineDataAvailable() {
        return Settings.isConnected();
    }

    public static String getSymbolFromKey(String sKey) {
        return sKey;
    }

    public static Stock getStockObject(String sKey) {
        return DataStore.getSharedInstance().getStockObject(sKey);
    }
 public static Stock getStockObject(String exchange,String symbol) {
        return DataStore.getSharedInstance().getStockByExchangeKey(exchange,symbol);
    }

    public static String getSymbol(Stock stock) {
        return stock.getSymbol();
    }

    public static String getExchangeCode(Stock stock) {
        return stock.getExchange();
    }

    public static String getCurrency(Stock stock) {
        if ((stock.getCurrencyCode() == null) || (stock.getCurrencyCode().equals("N/A")))
            return "N/A";
        else
            return stock.getCurrencyCode();
    }

    public static String getCompanyName(Stock stock) {
        return stock.getShortDescription();
    }

    public static String getCompanyCode(Stock stock) {
        return stock.getCompanyCode();
    }

    public static double getLastTrade(Stock stock) {
        if (Settings.getAfterMarketValuationMode() == Constants.AFTER_MARKET_VALUATION_MODE_CLOSE) {
            if (stock.getInstrumentType() == Meta.FUTURES) {
                double priceCorrectioFactor = 1;
                FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(stock.getExchange(), stock.getOptionBaseSymbol(),stock.getInstrumentType()));
                if (futureBaseAttributes != null) {
                    priceCorrectioFactor = futureBaseAttributes.getPriceCorrectionFactor();
                }
                return stock.getLastTradeValue() * priceCorrectioFactor;
            } else {
                return getMarketPriceByState(stock.getKey());
            }
        } else {
            if (stock.getInstrumentType() == Meta.INSTRUMENT_FUTURE) {
                double priceCorrectioFactor = 1;
                FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(
                        SharedMethods.getKey(stock.getExchange(), stock.getOptionBaseSymbol(), stock.getInstrumentType()));
                if (futureBaseAttributes != null) {
                    priceCorrectioFactor = futureBaseAttributes.getPriceCorrectionFactor();
                }
                return stock.getLastTradeValue() * priceCorrectioFactor;
            } else {
                return stock.getLastTradeValue();
            }
        }
    }
       public static double getMarketPriceByState(String sKey){
        double marketPrice=0f;
        try {
            String symbol   = SharedMethods.getSymbolFromKey(sKey);
            String exchange = SharedMethods.getExchangeFromKey(sKey);
            Exchange exg=ExchangeStore.getSharedInstance().getExchange(exchange);
            Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
            int status=exg.getMarketStatus();
            if (stock!=null) {
                switch(status){
                    case Meta.MARKET_PREOPEN:
                    case Meta.MARKET_PRECLOSE:
                    case Meta.MARKET_CLOSE:
                        marketPrice=getMarketPrice(stock);
                        break;
                    case Meta.MARKET_OPEN:
                        marketPrice=stock.getLastTradeValue();
                        break;

                }
                stock=null;
            } else {
                marketPrice= 0f;
            }

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            marketPrice= 0f;
        }
        return marketPrice;
    }
     private static double getMarketPrice(Stock stock){
        if(stock.getTodaysClose()>0){
            return stock.getTodaysClose();
        }else{
            if(stock.getLastTradedPrice()>0){
               return stock.getLastTradedPrice();
            }else{
               return stock.getPreviousClosed();
            }

        }
    }
    public static double getBidPrice(Stock stock) {
//        float value[] = new float[2];
//        value[0] = (float)stock.getBestBidPrice();
//        value[1] = (float)stock.getBestBidPrice();
        return stock.getBestBidPrice();
//        return stock.getBestBidPrice();
    }

    public static double getAskPrice(Stock stock) {
//        float value[] = new float[2];
//        value[0] = (float)stock.getBestAskPrice();
//        value[1] = (float)stock.getBestAskPrice()[1];
        return stock.getBestAskPrice();
//        return stock.getBestAskPrice();
    }

    public static long getVolume(Stock stock) {
        return stock.getVolume();
    }

    public static double getPreviousClosed(Stock stock) {
        if (stock.getInstrumentType() == Meta.INSTRUMENT_FUTURE) {
            double priceCorrectioFactor = 1;
            FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(
                    SharedMethods.getKey(stock.getExchange(), stock.getOptionBaseSymbol(), stock.getInstrumentType()));
            if (futureBaseAttributes != null) {
                priceCorrectioFactor = futureBaseAttributes.getPriceCorrectionFactor();
            }
            return stock.getPreviousClosed() * priceCorrectioFactor;
        } else {
            return stock.getPreviousClosed();
        }
    }

    public static float getVWAP(Stock stock) {
        return (float) stock.getAvgTradePrice();
    }

    public static float[] getChange(Stock stock) {
//        double[] change = new double[2];
//        change[0] = stock.getChange();
//        return change;

        float value[] = new float[2];
        value[0] = (float) stock.getChange();
        return value;
    }

    public static int getThemeID() {
        try {
            return Integer.parseInt(Theme.getID());
        } catch (NumberFormatException ex) {
            return 0;
        }
    }

    public static boolean isContainedSymbol(String symbol) {
        if ((DataStore.getSharedInstance().isValidSymbol(symbol))) {
            return true;
        } else {
            return false;
        }
    }

    public static String getColumnSetting(String sID) {
        return TWColumnSettings.getItem(sID);
    }

    public static String getShortDescription(String symbol) {
        if (DataStore.getSharedInstance().isValidSymbol(symbol)) {
            return DataStore.getSharedInstance().getStockObject(symbol).getShortDescription();
//        } else if (IndexStore.isValidIndex(symbol)) {
//            return IndexStore.getDescription(symbol);
        }
        return SharedMethods.getSymbolFromKey(symbol);
    }

    public static float getMarketPriceForDay(String sKey, String sDate) {
        try {
            String symbol = SharedMethods.getSymbolFromKey(sKey);
            String exchange = SharedMethods.getExchangeFromKey(sKey);
            Vector vecData = HistoryFilesManager.readHistoryDataForDate(symbol, exchange, sDate);
            if ((vecData != null) && (vecData.size() > 0)) {
                StringTokenizer oTokenizer = new StringTokenizer((String) vecData.elementAt(0), Meta.FS);
                oTokenizer.nextToken();         // Date
                oTokenizer.nextToken();         // open
                oTokenizer.nextToken();         // high
                oTokenizer.nextToken();         // low
                vecData = null;
                return Float.parseFloat(oTokenizer.nextToken());
            } else {
                SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
                String lastDate = format.format(new Date(Client.getInstance().getMarketTime()));
                format = null;
                if (lastDate.equals(sDate)) {
                    Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
                    if (PFStore.isVWAPMode()) {
                        if (stock.getAvgTradePrice() > 0)
                            return (float) stock.getAvgTradePrice();
                        else
                            return (float) stock.getPreviousClosed();
                    } else {
                        if (stock.getLastTradeValue() > 0)
                            return (float) stock.getLastTradeValue();
                        else
                            return (float) stock.getPreviousClosed();
                    }
                } else {
                    new ShowMessage(Language.getString("MSG_NO_HISTORY_DATA_FOR_THE_DATE"), "I");
                    return 0f;
                }
            }
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    public static Symbols getSymbolsObject() {
        return new Symbols(); //Meta.QUOTE);
    }

    public static Symbols getSymbolsObject(String sSymbols) {
        return new Symbols(sSymbols, false);
    }

    public static void removeSymbol(TradeKey key) {
        Stock stock=DataStore.getSharedInstance().getStockByExchangeKey(key.getExchange(),key.getSymbol());
        DataStore.getSharedInstance().removeSymbolRequest(stock.getKey());
        stock=null;
    }
    public static void removeSymbol(String key) {
        DataStore.getSharedInstance().removeSymbolRequest(key);
    }

    public static boolean isValidForTransactions() {
//        if (!Settings.isConnected()) {
//            new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
//            return false;
//        } else if (!ExchangeStore.isValidSystemFinformationType(Meta.IT_Portfolio)) {
//            new ShowMessage("<html><b>" + Language.getString("PORTFOLIO_SIMULATOR") + "&nbsp;&nbsp;</b><br>" +
//                    Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
//            return false;
        if (!ExchangeStore.isValidSystemFinformationType(Meta.IT_Portfolio)) {
            new ShowMessage("<html><b>" + Language.getString("PORTFOLIO_SIMULATOR") + "&nbsp;&nbsp;</b><br>" +
                    Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
            return false;
        } else {
            return true;
        }
    }

    public static boolean isCountExceedGlobalLimit(String symbol) {
        return false;
    }

    public static boolean isCountExceedGlobalLimit(String[] symbols) {
        return false;
    }

    public static String getFormattedPrintingTime() {
        TWDateFormat df = new TWDateFormat("dd MM yyyy - HH:mm:ss");
        df.setTimeZone(Settings.getCurrentZone());
        long timeMillis = System.currentTimeMillis();
        //#####for Tadawul use the following#########
//        long timeMillis = Client.getInstance().getMarketTime();
        timeMillis = timeMillis;
        return df.format(new Date(timeMillis));
    }

    public static double getTodaysClose(Stock stock) {
        return stock.getTodaysClose();
    }

}