package com.isi.csvr.help;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.ToolBarButton;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.win32.NativeMethods;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.Client;
import com.isi.util.FlexGridLayout;
import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.WebBrowser;
import com.jniwrapper.win32.ie.event.NewWindowEventHandler;
import com.jniwrapper.win32.ie.event.StatusCode;
import com.jniwrapper.win32.ie.event.WebBrowserEventsHandler;
import org.w3c.dom.Element;

import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;
import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Oct 16, 2008
 * Time: 12:14:29 PM
 */
public class TipOfDayWindow extends JDialog implements WebBrowserEventsHandler, WindowListener, NewWindowEventHandler {

    private Browser editorPane;
    private JPanel btnPanel;
    private NavigationPanel navigatePanel;
    private JPanel bottomPanel;
    private JPanel centerPanel;
    private JCheckBox showStartup;
    private int width = 550;
    private int height = 220;
    private static int selectedIndex = 0;
    private int tipCount = 0;
    private ToolBarButton btnPrevious;
    private ToolBarButton btnNext;
    ImageIcon border;
    ImageIcon imageNext;
    ImageIcon imagePrevious;
    public static boolean visible;

    public TipOfDayWindow() {
        super(Client.getInstance().getFrame(), false);
        try {
            try {
                tipCount = HelpManager.getTipCount();
            } catch (Exception e) {
                e.printStackTrace();
                tipCount = 0;
            }
            setLayout(new BorderLayout());
            editorPane = new Browser();
            editorPane.setPreferredSize(new Dimension(width, height));
            centerPanel = new JPanel();
            centerPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}));
            bottomPanel = new JPanel(new BorderLayout());
            centerPanel.setPreferredSize(new Dimension(width, height));
            centerPanel.setOpaque(true);
            centerPanel.add(editorPane);
            add(centerPanel, BorderLayout.CENTER);
            setResizable(true);
            setSize(width, 330);
            setTitle(Language.getString("TIP_OF_THE_DAY_WINDOW_TITLE"));
            setLocationRelativeTo(Client.getInstance().getFrame());

            editorPane.doLayout();
            editorPane.updateUI();
            setDefaultCloseOperation(DISPOSE_ON_CLOSE);
            btnPanel = new JPanel(new FlexGridLayout(new String[]{"200","100%", "150"}, new String[]{"30"}, 0, 0));
            GUISettings.applyOrientation(btnPanel);
            navigatePanel = new NavigationPanel();
            btnPanel.setPreferredSize(new Dimension(width, 30));
            border = new ImageIcon(getTheamedImagePath("tipofthedaybg"));
            imageNext = new ImageIcon(getTheamedImagePath("tipofday_Next"));
            imagePrevious = new ImageIcon(getTheamedImagePath("tipofday_Previous"));
            btnNext = new ToolBarButton();
            btnNext.setPreferredSize(new Dimension(20, 19));
            btnNext.setToolTipText(Language.getString("SEARCH"));
            btnNext.setEnabled(true);
            btnNext.setVisible(true);
            btnNext.setIcon(getIconFromString("search"));
            btnNext.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    loadPage(getNextToolTipID());
                }
            });

            btnPrevious = new ToolBarButton();
            btnPrevious.setPreferredSize(new Dimension(20, 19));
            btnPrevious.setToolTipText(Language.getString("SEARCH"));
            btnPrevious.setEnabled(true);
            btnPrevious.setVisible(true);
            btnPrevious.setIcon(getIconFromString("search"));
            btnPrevious.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    loadPage(getPreviousToolTipID());
                }
            });

            JLabel tipOfTheLable = new JLabel();
            tipOfTheLable.setText(Language.getString("TIP_OF_THE_DAY_MENU"));
            showStartup = new JCheckBox();
            showStartup.setForeground(Color.black);
            showStartup.setSelected(!Settings.isShowWelcomeScreen());
            showStartup.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Settings.setShowWelcomeScreen(!showStartup.isSelected());
                }
            });
            showStartup.setPreferredSize(new Dimension(width, 22));
            JPanel checkPanel=new JPanel();
            checkPanel.setLayout(new FlexGridLayout(new String[]{"15%","85%"}, new String[]{"30"}, 0, 0));
            checkPanel.add(showStartup);
            checkPanel.setBackground(Color.white);
            checkPanel.add(new JLabel(Language.getString("DO_NOT_SHOW_TIP_ON_STARTUP")));
            GUISettings.applyOrientation(checkPanel);
            btnPanel.add(checkPanel);
            btnPanel.add(new JLabel());
            btnPanel.add(navigatePanel);

            bottomPanel.setOpaque(true);
            bottomPanel.add(btnPanel, BorderLayout.SOUTH);
            bottomPanel.setPreferredSize(new Dimension(width, 32));
            add(bottomPanel, BorderLayout.SOUTH);
            ((JPanel) getContentPane()).setBorder(BorderFactory.createEmptyBorder(5, 10, 5, 10));

            try {
                selectedIndex = Integer.parseInt(Settings.getItem("LAST_TIP_ID"));
            } catch (NumberFormatException e) {
                selectedIndex = 0;
            }
            applyTheme();
            loadPage(selectedIndex);
            editorPane.setEventHandler(this);
            addWindowListener(this);
            editorPane.setNewWindowHandler(this);
            setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String getTheamedImagePath(String path) {
        String imagePath = "./Templates/images/ws/" + Language.getLanguageTag() + "/";

        File file = new File(imagePath + path + ".gif");

        if (!file.exists()) {
            file = new File(imagePath + path + ".jpg");
            if (!file.exists()) {
                return imagePath + path + ".png";
            } else {
                return imagePath + path + ".jpg";
            }
        } else {
            return imagePath + path + ".gif";
        }
    }

    private void loadPage(int index) {
        try {
            editorPane.navigate("mk:@MSITStore:" + System.getProperty("user.dir") + "\\help\\"+Language.getLanguageTag()+"\\"+HelpManager.getTodFileName()+"/Tip" + index + ".htm");
        } catch (Exception e) {
        }
    }

    public int getNextToolTipID() {
        if (selectedIndex < tipCount) {
            selectedIndex = selectedIndex + 1;
        } else {
            selectedIndex = 0;
        }
        return selectedIndex;
    }

    public int getPreviousToolTipID() {
        if (selectedIndex > 0) {
            selectedIndex = selectedIndex - 1;
        } else if (selectedIndex == 0) {
            selectedIndex = tipCount;
        } else {
            selectedIndex = tipCount - 1;
        }
        return selectedIndex;
    }

    public static int getLastSelectedTipID() {
        return selectedIndex;
    }

    public static void setLastSelectedTipID(int index) {
        selectedIndex = index;
    }

    public void applyTheme() {
        this.getContentPane().setBackground(Color.white);
        ((JPanel) this.getContentPane()).setOpaque(true);
        this.setBackground(Color.white);
        bottomPanel.setOpaque(false);
        showStartup.setOpaque(false);
        btnPanel.setOpaque(false);
        navigatePanel.setOpaque(false);
        centerPanel.setOpaque(false);
//        SwingUtilities.updateComponentTreeUI(this);
    }

    public Icon getIconFromString(String iconFile) {
        Icon icn = null;
        try {
            icn = new ImageIcon(Theme.getTheamedImagePath(iconFile));
        } catch (Exception ex) {

        }
        return icn;
    }

    public void showUI() {
        if (visible) {
            dispose();
//            editorPane.close();
        } else {
            setVisible(true);
        }
    }

    public boolean beforeFileDownload() {
        return false;
    }

    public boolean beforeNavigate(WebBrowser webBrowser, String s, String s1, String s2, String s3) {
        if (s.startsWith("help:")) {
            try {
                NativeMethods.showHelp(new File(s.substring(5)).getAbsolutePath());
            } catch (Exception e) {
                e.printStackTrace();
            }
            return true;
        }
        return false;
    }

    public Dimension clientAreaSizeRequested(Dimension dimension) {
        return null;
    }

    public boolean navigationErrorOccured(WebBrowser webBrowser, String s, String s1, StatusCode statusCode) {
        return false;
    }

    public boolean windowClosing(boolean b) {
        return false;
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
        Settings.setItem("LAST_TIP_ID", "" + selectedIndex);
        visible = false;
    }

    public void windowClosing(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowOpened(WindowEvent e) {
        visible = true;
    }

    public NewWindowAction newWindow() {
        Element activeElement = editorPane.getDocument().getActiveElement();
        String tagName = activeElement.getTagName();

        if ("a".equalsIgnoreCase(tagName)) {
            String s = activeElement.getAttribute("href");

            if ((s != null) && (s.startsWith("help:"))) {
                try {
                    NativeMethods.showHelp(new File(s.substring(5)).getAbsolutePath());
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            // button, other
        }
        return new NewWindowAction(NewWindowAction.ACTION_CANCEL);
    }


    class NavigationPanel extends JPanel implements MouseListener {

        NavigationPanel() {
            this.setOpaque(false);
            this.setLayout(new FlexGridLayout(new String[]{"100%", "20", "20"}, new String[]{"100%"}, 6, 0));
            this.addMouseListener(this);
//            GUISettings.applyOrientation(this);

        }

        public void paint(Graphics g) {
            super.paint(g);
            ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
            if (Language.isLTR()) {
            g.drawImage(border.getImage(), 0, 0, this);
                g.drawImage(imagePrevious.getImage(), 100, 5, this);
                g.drawImage(imageNext.getImage(), 125, 5, this);
            } else {
                g.drawImage(border.getImage(), 0, 0, this);
                g.drawImage(imagePrevious.getImage(), 5, 5, this);
                g.drawImage(imageNext.getImage(), 30, 5, this);
            }
        }

        public void mouseClicked(MouseEvent e) {
            //todo:acording to the language get the correct position to perform action - shanika
//            if (Language.getLanguageTag().equals("en")) {
            if (Language.isLTR()) {
                if ((e.getX() > 100) && (e.getX() < 125)) {
                    loadPage(getPreviousToolTipID());
                } else if ((e.getX() > 126) && (e.getX() < 146)) {
                    loadPage(getNextToolTipID());
                }
            } else {
               if ((e.getX() > 5) && (e.getX() < 30)) {
                loadPage(getPreviousToolTipID());
                } else if ((e.getX() > 31) && (e.getX() < 55)) {
                loadPage(getNextToolTipID());
            }
            }
//            }
            //To change body of implemented methods use File | Settings | File Templates.
        }

        public void mousePressed(MouseEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        public void mouseReleased(MouseEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        public void mouseEntered(MouseEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }

        public void mouseExited(MouseEvent e) {
            //To change body of implemented methods use File | Settings | File Templates.
        }
    }
}
