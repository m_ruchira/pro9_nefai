package com.isi.csvr.ohlcbacklog;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 13-Jun-2007
 * Time: 14:29:54
 * To change this template use File | Settings | File Templates.
 */

import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.event.ProgressListener;
import com.isi.csvr.history.HistorySettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartFileChooser;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.File;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Chandika Hewage
 * Date: Aug 20, 2003
 * Time: 2:43:23 PM
 * To change this template use Options | File Templates.
 */
public class OHLCDownloaderUI extends InternalFrame
        implements ActionListener, OHLCDataChangeListener,
        ProgressListener, Themeable, ExchangeListener, NonResizeable {
    public OHLCFileList fileList;
    private JCheckBox chkAutoDownload;
    private TWButton btnDownload;
    private TWButton btnClose;
    private TWComboBox exchangeCombo;
    private ArrayList<TWComboItem> exchangeList;
    private JLabel lblDestination;
    private String exchange;
    private static OHLCDownloaderUI self;

    public OHLCDownloaderUI() throws HeadlessException {
        super(false);
        Theme.registerComponent(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        Client.getInstance().getDesktop().add(this);
        //OHLCHistoryDownloader.getSharedInstance().clearOlderFiles();


    }

    public static OHLCDownloaderUI getSharedInstance() {
        if (self == null) {
            self = new OHLCDownloaderUI();
        }
        return self;
    }

    public void init() {
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        String[] widths = {"100%"};
        String[] heights = {"0", "0", "100%", "30"};
        getContentPane().setLayout(new FlexGridLayout(widths, heights, 0, 5));

        JPanel exchangePanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 10, 2));
        JLabel exchangeLabel = new JLabel(Language.getString("EXCHANGE"));
        exchangeList = new ArrayList<TWComboItem>();
        exchangeCombo = new TWComboBox(new TWComboModel(exchangeList));
        exchangeCombo.setActionCommand("EXCHANGE");
        exchangeCombo.setPreferredSize(new Dimension(200, 25));
        exchangePanel.add(exchangeLabel);
        exchangePanel.add(exchangeCombo);
        getContentPane().add(exchangePanel);

        JLabel lblTitle = new JLabel("  " + Language.getString("AVAILABLE_DATES") + "  ");
        getContentPane().add(lblTitle);

        JPanel listPanel = new JPanel(new BorderLayout());
        JPanel panel0 = new JPanel();
        String[] widths1 = {"130", "70", "100%"};
        String[] heights1 = {"0"};
        panel0.setLayout(new FlexGridLayout(widths1, heights1, 0, 5));
        JLabel lblHead2 = new JLabel("  " + Language.getString("DATE") + "  ", JLabel.CENTER);
        lblHead2.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
        JLabel lblHead3 = new JLabel("  " + Language.getString("SIZE") + "  ", JLabel.CENTER);
        lblHead3.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
        JLabel lblHead4 = new JLabel("  " + Language.getString("STATUS") + "  ");
        lblHead4.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.RAISED));
        panel0.add(lblHead2);
        panel0.add(lblHead3);
        panel0.add(lblHead4);
        listPanel.add(panel0, BorderLayout.NORTH);

        fileList = new OHLCFileList(OHLCBacklogStore.getSharedInstance());
        listPanel.add(new JScrollPane(fileList), BorderLayout.CENTER);
        getContentPane().add(listPanel);

        chkAutoDownload = new JCheckBox(Language.getString("AUTO_DOWNLOAD_RECENT_FILE"));
        String value = HistorySettings.getProperty("AUTO_DOWNLOAD_LAST_TNS");
        if ((value == null) || (value.equals("")) || (value.equals("0"))) {
            chkAutoDownload.setSelected(false);
        } else {
            chkAutoDownload.setSelected(true);
        }
        value = null;
        chkAutoDownload.setActionCommand("AUTO");
        chkAutoDownload.addActionListener(this);
        //getContentPane().add(chkAutoDownload);

        JPanel panel2 = new JPanel();
        panel2.setLayout(new FlowLayout(FlowLayout.TRAILING));
        btnDownload = new TWButton(Language.getString("DOWNLOAD"));
        btnDownload.setActionCommand("DOWNLOAD");
        btnDownload.addActionListener(this);
        btnClose = new TWButton(Language.getString("CLOSE"));
        btnClose.setActionCommand("CLOSE");
        btnClose.addActionListener(this);
        if (Settings.isTCPMode()) {
            panel2.add(btnDownload);
        }
        panel2.add(btnClose);
        getContentPane().add(panel2);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setResizable(false);
        setSize(400, 450);
        this.setClosable(true);
        setTitle(Language.getString("OHLC_DOWNLOADER"));

        OHLCBacklogStore.getSharedInstance().addDataListener(this);
        OHLCHistoryDownloader.getSharedInstance().addProgressListener(this);

        GUISettings.applyOrientation(this);

        fileList.updateUI();

    }

//    public void setVisible(boolean status) {
//        super.setVisible(status);
//        if (status == true) {
//
//        }
//    }

    public void initFileList() {
        for (int i = 1; i < fileList.getModel().getSize(); i++) {
            OHLCFileListItem item = (OHLCFileListItem) fileList.getModel().getElementAt(i);
            item.setStatus(OHLCFileList.STATUS_SAVED);
            item = null;
        }
        fileList.repaint();
    }

    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);

        if (e.getActionCommand().equals("DOWNLOAD")) {
//            OHLCHistoryDownloader.getSharedInstance().clearOlderFiles();
            OHLCHistoryDownloader.getSharedInstance().startDownload();
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Tools, "HistoricalIntrDayOHLC");

        } else if (e.getActionCommand().equals("CLOSE")) {
            setVisible(false);
        } else if (e.getActionCommand().equals("AUTO")) {
            HistorySettings.setProperty("AUTO_DOWNLOAD_LAST_TNS", chkAutoDownload.isSelected() ? "1" : "0");
            OHLCBacklogStore.getSharedInstance().setAutoDownloadRecent(chkAutoDownload.isSelected());
        } else if (e.getActionCommand().equals("BROWSE")) {
            browseFolders();
        } else if (e.getActionCommand().equals("OPEN_FILE")) {
            showFolder();
        } else if (e.getActionCommand().equals("EXCHANGE")) {
            exchange = ((TWComboItem) exchangeCombo.getSelectedItem()).getId();
            OHLCBacklogStore.getSharedInstance().setCurrentExchange(exchange);
            fileList.updateUI();
        }
    }

    public void browseFolders() {
        UIManager.put("FileChooser.fileNameLabelText", Language.getString("FOLDERNAME"));
        SmartFileChooser chooser = new SmartFileChooser(Settings.INTRADAY_OHLC_PATH );
        GUISettings.localizeFileChooserHomeButton(chooser);
        chooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        chooser.setDialogTitle(Language.getString("OPEN_FOLDER"));
        chooser.hideContainerOf((String) UIManager.get("FileChooser.fileNameLabelText"));
        chooser.hideContainerOf((String) UIManager.get("FileChooser.filesOfTypeLabelText"));

        int status = chooser.showOpenDialog(this);
        if (status == JFileChooser.APPROVE_OPTION) {
            //Settings.setTradeArchivePath(chooser.getSelectedFile().getAbsolutePath());
            lblDestination.setText("<HTML><U>" + Settings.INTRADAY_OHLC_PATH  + "</U>");
            initFileList();
            updateFileStatus();
        }


    }

    public void showFolder() {
        try {
            Runtime.getRuntime().exec("start explorer \"" + Settings.getTradeArchivePath() + "\"");
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
    }

    public synchronized void updateFileStatus() {
        try {

            for (int i = 0; i < fileList.getModel().getSize(); i++) {
                OHLCFileListItem item = (OHLCFileListItem) fileList.getModel().getElementAt(i);
//                try {
//                    item.setDescription(dateFormatOut.format(dateFormatIn.parse(tokens[i].substring(0,8))));
//                } catch (ParseException e) {
//                    item.setDescription(tokens[i]);
//                }
                OHLCBacklogStore.getSharedInstance().currentFile = item.getFileName();
                File[] files = OHLCBacklogStore.getSharedInstance().getDownloadedFiles(exchange);
//                item.setFileName(currentFile);
//                item.setSize(Integer.parseInt(tokens[i+1]));
//                item = updateStore(item);

                OHLCBacklogStore.getSharedInstance().checkAvalability(files, item); // check if the file is already downloaded
                item = null;
                files = null;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
//        dataListener.listDownloaded();
//        fireDataChanged();
//        downloadRecentFile();
    }

    public void dataChanged() {
        fileList.repaint();// updateUI();
    }

    public void listDownloaded() {
        fileList.updateUI();
    }

    public void show() {
        setLocationRelativeTo(Client.getInstance().getDesktop());
//        this.setVisible(true);
        super.show();
        OHLCBacklogStore.getSharedInstance().requestData();
        OHLCBacklogStore.getSharedInstance().setCurrentExchange(exchange);

        fileList.updateUI();
//        OHLCHistoryDownloader.getSharedInstance().clearOlderFiles();

        exchangeCombo.setSelectedIndex(0);
    }

    public void setProcessStarted(String id) {
        btnDownload.setEnabled(false);
        exchangeCombo.setEnabled(false);
        btnClose.setText(Language.getString("HIDE"));
    }

    public void setMax(String id, int max) {

    }

    public void setProcessCompleted(String id, boolean sucess) {
        btnDownload.setEnabled(true);
        exchangeCombo.setEnabled(true);
        btnClose.setText(Language.getString("CLOSE"));
        fileList.unselectAll();
    }

    public void setProgress(String id, int progress) {
        fileList.repaint();
    }

    /*public void show(boolean b) {
        super.show(b);
        setLocationRelativeTo(Client.getInstance().getDesktop());
    }*/

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(getContentPane());
        fileList.initColors();
    }

    /*public void mouseClicked(MouseEvent e) {
        showFolder();
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }*/

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange,long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        exchangeCombo.removeActionListener(this);
        //exchangeCombo.removeAllItems();

        exchangeList.clear();
        for (int i = 0; i < ExchangeStore.getSharedInstance().count(); i++) {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(i);
            if (exchange.isDefault() && !exchange.isExpired() && !exchange.isInactive()) {
                TWComboItem comboItem = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                exchangeList.add(comboItem);
            }
            exchange = null;
        }
        Collections.sort(exchangeList);
        exchangeCombo.updateUI();

        if (exchange != null) {
            try {
                for (int i = 0; i < exchangeCombo.getItemCount(); i++) {
                    if (((TWComboItem) exchangeCombo.getItemAt(i)).getId().equals(exchange)) {
                        exchangeCombo.setSelectedIndex(i);
                        break;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use Options | File Templates.
            }
        } else {
            if (exchangeCombo.getModel().getSize() > 0) {
                if (exchangeCombo.getSelectedIndex() >= 0) {
                    exchange = ((TWComboItem) exchangeCombo.getItemAt(exchangeCombo.getSelectedIndex())).getId();
                } else {
                    exchange = ((TWComboItem) exchangeCombo.getItemAt(0)).getId();
                }
            }
        }

        exchangeCombo.addActionListener(this);
    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}

