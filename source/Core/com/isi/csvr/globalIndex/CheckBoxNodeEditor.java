package com.isi.csvr.globalIndex;

import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeCellEditor;
import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.EventObject;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 25-Mar-2008 Time: 10:40:38 To change this template use File | Settings
 * | File Templates.
 */
class CheckBoxNodeEditor extends AbstractCellEditor implements TreeCellEditor {

  CheckBoxNodeRenderer renderer = new CheckBoxNodeRenderer();

  ChangeEvent changeEvent = null;

  JTree tree;

  public CheckBoxNodeEditor(JTree tree) {
    this.tree = tree;
  }

  public Object getCellEditorValue() {
    CheckLabel checkbox = renderer.getLeafRenderer();
    CheckBoxNode checkBoxNode =renderer.getlfRenderer();// new CheckBoxNode(checkbox.getText(),checkbox.isSelected());
    checkBoxNode.setSelected(checkbox.isSelected());
    return checkBoxNode;
  }

  public boolean isCellEditable(EventObject event) {
    boolean returnValue = false;
    if (event instanceof MouseEvent) {
      MouseEvent mouseEvent = (MouseEvent) event;
            TreePath path = tree.getPathForLocation(mouseEvent.getX(),
                mouseEvent.getY());
            if (path != null) {
              Object node = path.getLastPathComponent();
              if ((node != null) && (node instanceof DefaultMutableTreeNode)) {
                DefaultMutableTreeNode treeNode = (DefaultMutableTreeNode) node;
                Object userObject = treeNode.getUserObject();
                returnValue = ((treeNode.isLeaf()) && (userObject instanceof CheckBoxNode));
              }
            }

    }
    return returnValue;
  }

  public Component getTreeCellEditorComponent(JTree tree, Object value,
      boolean selected, boolean expanded, boolean leaf, int row) {

    final Component editor = renderer.getTreeCellRendererComponent(tree, value,
        true, expanded, leaf, row, true);

    // editor always selected / focused
    MouseListener itemListener = new  MouseListener() {
      public void mouseClicked(MouseEvent itemEvent) {
        if (stopCellEditing()) {
            CheckBoxNode cbn=(CheckBoxNode)renderer.getlfRenderer();
            if(cbn.isSelected()){
                cbn.setSelected(false);
            }else{
                cbn.setSelected(true);
            }
            fireEditingStopped();
        }
      }
      public void mousePressed(MouseEvent itemEvent){

      }
      public void mouseReleased(MouseEvent itemEvent){

      }
      public void mouseEntered(MouseEvent itemEvent){

      }
      public void mouseExited(MouseEvent itemEvent){

      }
    };
    if (editor instanceof JLabel) {
      ((JLabel) editor).addMouseListener(itemListener);
    }

    return editor;
  }
}
