package com.isi.csvr.globalIndex;

import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.shared.*;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Exchange;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 07-Apr-2008 Time: 11:28:35 To change this template use File | Settings
 * | File Templates.
 */
public class GlobalIndexStore {
    public static GlobalIndexStore self;
    private ArrayList<String> keys;

    public static GlobalIndexStore getSharedInstance() {
        if (self == null) {
            self = new GlobalIndexStore();
        }
        return self;
    }

    private GlobalIndexStore() {
        init();
    }


    public void addGlobalIndexToStore(String key) {
        try {
            if (!keys.contains(key)) {
            keys.add(key);
                DataStore.getSharedInstance().createStockObject(SharedMethods.getExchangeFromKey(key), SharedMethods.getSymbolFromKey(key),Meta.INSTRUMENT_INDEX);
                SendQFactory.addAddRequest(Constants.PATH_PRIMARY, SharedMethods.getExchangeFromKey(key), SharedMethods.getSymbolFromKey(key), Meta.SYMBOL_TYPE_INDEX, Meta.INSTRUMENT_INDEX, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
        public void removeGlobalIndexToStore(String key) {
        try {
            if (keys.contains(key)) {
                keys.remove(key);
                SendQFactory.addRemoveRequest(Constants.PATH_PRIMARY, SharedMethods.getExchangeFromKey(key), SharedMethods.getSymbolFromKey(key), Meta.SYMBOL_TYPE_INDEX, Meta.INSTRUMENT_INDEX, null);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void sendAddSymbolRequests() {
        try {

            if (keys.size()>0) {
                for(int i=0;i<keys.size();i++) {
                    SendQFactory.addAddRequest(Constants.PATH_PRIMARY, SharedMethods.getExchangeFromKey(keys.get(i)), SharedMethods.getSymbolFromKey(keys.get(i)), Meta.SYMBOL_TYPE_INDEX, Meta.INSTRUMENT_INDEX, null);
        }
    }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendRermoveSymbolRequests() {
        try {
            if (keys.size() > 0) {
                for (int i = 0; i < keys.size(); i++) {
                    SendQFactory.addRemoveRequest(Constants.PATH_PRIMARY, SharedMethods.getExchangeFromKey(keys.get(i)), SharedMethods.getSymbolFromKey(keys.get(i)), Meta.SYMBOL_TYPE_INDEX, Meta.INSTRUMENT_INDEX, null);
            }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    public void init() {
        keys = new ArrayList<String>();
        load();

    }

    public void save() {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(Settings.getAbsolutepath()+"datastore/globalindices.mdf"));
            out.writeObject(keys);
            out.flush();
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void load() {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(Settings.getAbsolutepath()+"datastore/globalindices.mdf"));
            keys = (ArrayList<String>) in.readObject();
            if (keys.size() > 0) {
                for (int i = 0; i < keys.size(); i++) {
                    addGlobalIndexToStore(keys.get(i));
            }
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ArrayList<String> getKeys() {
        return keys;
    }

    public Stock getObject(int index) {
        try {
            return DataStore.getSharedInstance().getStockObject(keys.get(index));
        } catch (Exception e) {
            return null;
        }
    }

    public boolean contains(String key){
        return keys.contains(key);
    }

    public int size() {
        return keys.size();
    }

    public void clear(){
        keys.clear();
    }

  
}
