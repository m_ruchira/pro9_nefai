package com.isi.csvr.globalIndex;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.table.Table;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 20-Mar-2008 Time: 12:27:15 To change this template use File | Settings
 * | File Templates.
 */
public class GlobalIndexWindow extends InternalFrame implements InternalFrameListener {
    public static GlobalIndexWindow self;
    ViewSetting oSetting;
    Table oGlobalIndexTable;
    GlobalIndexModel oGlobalIndexModel;
    //    public static Vector indexStore;
    IndicesSelectionTree indicesTree;

    public static GlobalIndexWindow getSharedInstance() {
        if (self == null) {
            self = new GlobalIndexWindow();
        }
        return self;
    }

    private GlobalIndexWindow() {
        createUI();
    }

    private void createUI() {
        oSetting = ViewSettingsManager.getSummaryView("GLOBAL_INDEX");
        oSetting.setParent(this);
        this.setSize(oSetting.getSize());
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        this.add(createTable());
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTitle(Language.getString(oSetting.getCaptionID()));
        this.setLocation(oSetting.getLocation());
        this.setTable(oGlobalIndexTable);
        super.applySettings();
        Client.getInstance().getDesktop().add(this);
        setLocationRelativeTo(Client.getInstance().getDesktop());
        setLayer(GUISettings.TOP_LAYER);

        GUISettings.applyOrientation(this);
    }

    private Table createTable() {
        oGlobalIndexTable = new Table();
        oGlobalIndexTable.setSortingEnabled();
        oGlobalIndexTable.setPreferredSize(new Dimension(500, 100));
        oGlobalIndexModel = new GlobalIndexModel();
        oGlobalIndexModel.setViewSettings(oSetting);
        oGlobalIndexTable.setModel(oGlobalIndexModel);
        oGlobalIndexModel.setTable(oGlobalIndexTable);
        oGlobalIndexModel.applyColumnSettings();
        oGlobalIndexModel.updateGUI();
        oSetting.setParent(this);

        //adding indices search menu item to popup
        final TWMenuItem indicesSearch = new TWMenuItem(Language.getString("ADD_GLOBAL_INDEX"), "addGIndex.gif");
        indicesSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showIndicesSelectionTree();
            }
        });
        oGlobalIndexTable.getPopup().setMenuItem(indicesSearch);

        return oGlobalIndexTable;
    }

    public void showFrame() {
        if (Settings.isConnected()){
            GlobalIndexStore.getSharedInstance().sendAddSymbolRequests();
        }
        setVisible(true);
    }

    public void showIndicesSelectionTree() {
        IndicesSelectionTree.getSharedInstance().showFrame();
    }


    public void internalFrameOpened(InternalFrameEvent e) {

    }

    public void internalFrameClosing(InternalFrameEvent e) {
        GlobalIndexStore.getSharedInstance().sendRermoveSymbolRequests();
    }

    public void internalFrameClosed(InternalFrameEvent e) {

    }

    public void internalFrameIconified(InternalFrameEvent e) {

    }

    public void internalFrameDeiconified(InternalFrameEvent e) {

    }

    public void internalFrameActivated(InternalFrameEvent e) {

    }

    public void internalFrameDeactivated(InternalFrameEvent e) {

    }

}
