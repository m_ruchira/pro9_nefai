package com.isi.csvr.sectormap;

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.announcement.AnnouncementMouseListener;
import com.isi.csvr.announcement.ANNOUNCMENT_TYPE;
import com.isi.csvr.announcement.AnnouncementStore;
import com.isi.csvr.table.Table;
import com.isi.csvr.table.AnnouncementModel;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 3, 2007
 * Time: 8:56:17 PM
 */
public class SectorMap implements ActionListener, InternalFrameListener {
    private InternalFrame frame;
    private ArrayList<TWComboItem> exchanges;
    private TWComboBox exchangeCombo;

    public SectorMap() {
        init();
    }

    private void init(){
        Table table = new Table();
        SectorMapModel model = new SectorMapModel();
        model.setDirectionLTR(Language.isLTR());
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("BTW_SECTORMAP");
        model.setViewSettings(oSetting);
        table.setModel(model);
        table.setAutoResize(false);
        AnnouncementMouseListener oSelectionListener = new AnnouncementMouseListener(table.getTable(),
                ANNOUNCMENT_TYPE.ALL);
        table.getTable().getSelectionModel().addListSelectionListener(oSelectionListener);
        table.getTable().addMouseListener(oSelectionListener);
        table.getPopup().setAutoWidthAdjustMenuVisible(false);
        table.setPreferredSize(new Dimension(500, 100));

//        model.setDataStore(AnnouncementStore.getSharedInstance().getFilteredAnnouncements());
        table.setWindowType(ViewSettingsManager.SECTORMAP_VIEW);
        model.setTable(table);
        model.applyColumnSettings();
        table.setNorthPanel(getExchangePanel());
        table.updateGUI();
        frame = new InternalFrame(table);
        frame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oSetting.setParent(frame);
        frame.getContentPane().add(table);
        //oTopDesktop.add(announcementFrame);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setMaximizable(true);
        frame.setIconifiable(true);
        frame.setColumnResizeable(true);
        frame.setDetachable(true);
        frame.setPrintable(true);
        frame.setTitle(Language.getString(oSetting.getCaptionID()));
        frame.updateUI();
        frame.applySettings();
        frame.setLayer(GUISettings.TOP_LAYER);
        frame.setVisible(false);
        frame.addInternalFrameListener(this);
    }

    private JPanel getExchangePanel() {
        //JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEADING));

        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"110","200"},new String[]{"22"},6,6,true,true ));

        JLabel label = new JLabel(Language.getString("EXCHANGE"));
        panel.add(label);

        exchanges = new ArrayList<TWComboItem>();

        exchangeCombo = new TWComboBox(new TWComboModel(exchanges));
        popuplateExchanges();
        try {
            exchangeCombo.updateUI();
            exchangeCombo.setSelectedIndex(0);
//            if(northPanel!=null)
//                northPanel.doLayout();
        } catch (Exception e) {
        }
        exchangeCombo.addActionListener(this);
        panel.add(exchangeCombo);
        panel.doLayout();
        return panel;

    }

    private void popuplateExchanges() {
        exchanges.clear();
        try {
            for (int i = 0; i < ExchangeStore.getSharedInstance().count(); i++) {
                try {
                    Exchange exchange = ExchangeStore.getSharedInstance().getExchange(i);
                    if (exchange.isDefault() && !exchange.isExpired() && !exchange.isInactive()) {
                        TWComboItem comboItem = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                        exchanges.add(comboItem);
                    }
                    exchange = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            Collections.sort(exchanges);
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    public void actionPerformed(ActionEvent e) {
        String exchange = exchanges.get(exchangeCombo.getSelectedIndex()).getId();
        SectorMapStore.getSharedInstance().setExchange(exchange);
    }

    public InternalFrame getFrame(){
        return frame;
    }


    public void internalFrameActivated(InternalFrameEvent e) {
        SectorMapStore.getSharedInstance().setActive(true);
        try {
            SectorMapStore.getSharedInstance().setExchange(exchanges.get( exchangeCombo.getSelectedIndex()).getId());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void internalFrameClosed(InternalFrameEvent e) {
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        SectorMapStore.getSharedInstance().setActive(false);
    }

    public void internalFrameDeactivated(InternalFrameEvent e) {
    }

    public void internalFrameDeiconified(InternalFrameEvent e) {
        SectorMapStore.getSharedInstance().setActive(true);
        try {
            SectorMapStore.getSharedInstance().setExchange(exchanges.get(exchangeCombo.getSelectedIndex()).getId());
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void internalFrameIconified(InternalFrameEvent e) {
        SectorMapStore.getSharedInstance().setActive(false);
    }

    public void internalFrameOpened(InternalFrameEvent e) {
    }
}
