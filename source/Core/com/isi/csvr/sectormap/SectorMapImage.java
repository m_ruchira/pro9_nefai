package com.isi.csvr.sectormap;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 4, 2007
 * Time: 9:43:22 PM
 */

import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.datastore.ExchangeStore;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 26, 2004
 * Time: 1:12:22 PM
 * To change this template use File | Settings | File Templates.
 */
public class SectorMapImage extends ImageIcon {
    public static final int TYPE_BID_ASK = 0;
    public static final int TYPE_SPREAD = 1;
    public static final int TYPE_RANGE = 2;
    public static final int TYPE_CHANGE = 3;
    public static final int TYPE_TICK = 4;
    public static final int TICK_FILTER = 3;


    private int type;
    private int height = 10;
    private int width = 50;
    private int CLEARENCE = 4;
    private int[] value;
    private int intValue = 0;
    private int imageWidth = 0;
    private String exchange;


    public SectorMapImage() {
        imageWidth = width - (CLEARENCE * 2);
    }

    public int getIconHeight() {
        return height;
    }

    public int getIconWidth() {
        return imageWidth;
    }

    public void setValue(int[] value) {
        this.value = value;
    }

    public void setValue(int value) {
        this.intValue = value;
    }

//    public void setValue(byte[] value){
//        this.byteData = value;
//    }

    public void setWidth(int width) {
        this.width = width;
        imageWidth = width - (CLEARENCE * 2);
    }

    public void setHeight(int height) {
        this.height = height;
        CLEARENCE = height / 4;
    }


    public void setType(int type) {
        this.type = type;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        int val;
        int old = 0;

//            g.setColor(Color.BLACK);
//            g.fillRect(0, 0, width, height);

        x += CLEARENCE;
        y += CLEARENCE;

        if (value[4] == 0) {
            return;
        }

        val = (value[0] * imageWidth) / value[4];
        g.setColor(Theme.SECTORMAP_MINUS_DARK_COLOR);
        g.fillRect(x, y, val, height - (CLEARENCE * 2));

        old += val;
        val = (value[1] * imageWidth) / value[4];
        g.setColor(Theme.SECTORMAP_MINUS_LIGHT_COLOR);
        g.fillRect(x + old, y, val, height - (CLEARENCE * 2));

        old += val;
        val = (value[2] * imageWidth) / value[4];
        g.setColor(Theme.SECTORMAP_PLUS_LIGHT_COLOR);
        g.fillRect(x + old, y, val, height - (CLEARENCE * 2));

        old += val;
        val = (value[3] * imageWidth) / value[4];
        g.setColor(Theme.SECTORMAP_PLUS_DARK_COLOR);
        g.fillRect(x + old, y, val, height - (CLEARENCE * 2));
        /*switch (type) {
    case TYPE_BID_ASK:
        if (value > 1) {
            x += ((imageWidth / 2));
            y += CLEARENCE;
            val = Math.max(Math.min((int) ((value * imageWidth) / (ExchangeStore.getSharedInstance().getExchange(exchange).getBidAskMax() * 2)), imageWidth / 2), 1);
            g.setColor(Theme.BIDASK_UP_COLOR);
            g.fillRect(x, y, val, height - (CLEARENCE * 2));
        } else if (value == 0) {
            // do nothing
        } else {
            //x += CLEARENCE;
            y += CLEARENCE;
            val = (int) ((value * imageWidth) / (ExchangeStore.getSharedInstance().getExchange(exchange).getBidAskMin() * 2));
            val = Math.max((int) (((1 - value) * (imageWidth / 2)) / 1.0001), 1);
            g.setColor(Theme.BIDASK_DOWN_COLOR);
            g.fillRect(x + (imageWidth / 2) - val, y, val, height - (CLEARENCE * 2));
        }
        break;
    case TYPE_SPREAD:
        if (value > 0) {
            x += ((imageWidth / 2));
            y += CLEARENCE;
            val = Math.max(Math.min((int) ((value * imageWidth) / (Stock.spreadMax * 2)), imageWidth / 2), 1);
            g.setColor(Theme.BIDASK_UP_COLOR);
            g.fillRect(x, y, val, height - (CLEARENCE * 2));
        } else if (value == 0) {
            // do nothing
        } else {
            //x += CLEARENCE;
            y += CLEARENCE;
            val = Math.max(Math.min((int) ((value * imageWidth) / (Stock.spreadMin * 2)), imageWidth / 2), 1);
            g.setColor(Theme.BIDASK_DOWN_COLOR);
            g.fillRect(x + (imageWidth / 2) - val, y, val, height - (CLEARENCE * 2));
        }
        break;
    case TYPE_CHANGE:
        if (value > 0) {
            x += (imageWidth / 2);
            y += CLEARENCE;
            val = Math.max(Math.min((int) ((value * imageWidth) / (Stock.changeMax * 2)), imageWidth / 2), 1);
            g.setColor(Theme.BIDASK_UP_COLOR);
            g.fillRect(x, y, val, height - (CLEARENCE * 2));
        } else if (value == 0) {
            // do nothing
        } else {
            //x += CLEARENCE;
            y += CLEARENCE;
            val = Math.max(Math.min((int) ((value * imageWidth) / (Stock.changeMin * 2)), imageWidth / 2), 1);
            g.setColor(Theme.BIDASK_DOWN_COLOR);
            g.fillRect(x + (imageWidth / 2) - val, y, val, height - (CLEARENCE * 2));
        }
        break;
    case TYPE_RANGE:
        if (value > 0) {
            x += CLEARENCE;
            y += CLEARENCE;
            val = Math.max(Math.min((int) ((value * imageWidth) / (Stock.rangeMax)), imageWidth), 1);
            g.setColor(Theme.BIDASK_UP_COLOR);
            g.fillRect(x, y, val, height - (CLEARENCE * 2));
        } else if (value == 0) {
            // do nothing
        }
        break;
    case TYPE_TICK:
        //x += CLEARENCE;
        y += CLEARENCE;
        int barHeight;
        int offset;
        int squareSize = Math.min(100, imageWidth / Stock.TICK_MAP_LENGTH);
        for (int i = 0; i < Stock.TICK_MAP_LENGTH; i++) {
            if (i == 0) {
                barHeight = height - (CLEARENCE * 2);
                offset = 0;
            } else {
                barHeight = height - (CLEARENCE * 2) - 4;
                offset = 2;
            }
            if (((intValue & (TICK_FILTER << i * 2)) >> (i * 2)) == Settings.TICK_UP) {
                g.setColor(Theme.BIDASK_UP_COLOR);
                g.fillRect(x + (i * squareSize), y + offset, squareSize - 1, barHeight);
            } else if (((intValue & (TICK_FILTER << i * 2)) >> (i * 2)) == Settings.TICK_DOWN) {
                g.setColor(Theme.BIDASK_DOWN_COLOR);
                g.fillRect(x + (i * squareSize), y + offset, squareSize - 1, barHeight);
//                        }else if (((intValue & (TICK_FILTER << i*2)) >> (i*2)) == Settings.TICK_NOCHANGE){
//                            g.setColor(Color.gray);
//                            g.drawRect(x+(i*squareSize),y+offset,squareSize-1,barHeight-1);
            }
            g.setColor(Color.gray);
            g.drawRect(x + (i * squareSize), y + offset, squareSize - 1, barHeight - 1);
        }*/
        /*x += CLEARENCE;
        y += CLEARENCE;
        int barHeight;
        int offset;
        int squareSize = Math.min(100,imageWidth/byteData.length);
        for (int i = 0; i < byteData.length; i++) {
            if (i==0){
                barHeight = height-(CLEARENCE*2);
                offset = 0;
            }else{
                barHeight = height-(CLEARENCE*2)-4;
                offset = 2;
            }
            if (byteData[i] == Settings.TICK_UP){
                g.setColor(Color.green);
                g.fillRect(x+(i*squareSize),y+offset,squareSize-1,barHeight);
            }else if (byteData[i] == Settings.TICK_DOWN){
                g.setColor(Color.red);
                g.fillRect(x+(i*squareSize),y+offset,squareSize-1,barHeight);
            }else if (byteData[i] == Settings.TICK_NOCHANGE){
                g.setColor(Color.gray);
                g.drawRect(x+(i*squareSize),y+offset,squareSize-1,barHeight-1);
            }
        }*/
//                    break;
    }


}
