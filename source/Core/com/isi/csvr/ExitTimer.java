package com.isi.csvr;

import com.isi.csvr.shared.Settings;

import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jul 11, 2005
 * Time: 7:09:12 PM
 */
public class ExitTimer implements Runnable {

    public void run() {
        int today;
        int lastExitDate;
        int exitHour;

        try {
            lastExitDate = Integer.parseInt(Settings.getItem("LAST_EXIT_DATE"));
        } catch (NumberFormatException e) {
            lastExitDate = -1;
        }
        try {
            exitHour = Integer.parseInt(Settings.getItem("EXIT_HOUR"));
        } catch (NumberFormatException e) {
            exitHour = 0;
        }
        while (true) {
            try {
                Calendar calendar = Calendar.getInstance(Settings.getCurrentZone());
                today = calendar.get(Calendar.DAY_OF_MONTH);
                if ((lastExitDate != today) && (calendar.get(Calendar.HOUR_OF_DAY) == exitHour)) {
                    Settings.setItem("LAST_EXIT_DATE", "" + today);
                    Client.getInstance().mnuSave_ActionPerformed();
                    Client.getInstance().saveDataOnExit();
                    Client.getInstance().deleteTempFiles();
                    System.exit(0);
                }
                calendar = null;
                Thread.sleep(600000L); // every 10 minutes
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
