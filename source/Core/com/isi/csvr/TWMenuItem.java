package com.isi.csvr;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.TWComboItem;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.shortcutkeys.AcceleratorListener;
import com.isi.csvr.shortcutkeys.AcceleratorStore;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 4, 2005
 * Time: 3:30:24 PM
 */
public class TWMenuItem extends JMenuItem implements Comparable, AcceleratorListener {

    private String iconFile;
    private String acceleratorText;
    private int acceleratorID;

    public TWMenuItem() {
        super();
        setUI(new TWMenuItemUI());
    }

    public TWMenuItem(String text) {
        super(text);
        setUI(new TWMenuItemUI());
    }

    public TWMenuItem(Icon icon) {
        super(icon);
        setUI(new TWMenuItemUI());
    }

    public TWMenuItem(String text, Icon icon) {
        super(text, icon);
        setUI(new TWMenuItemUI());
    }

    public TWMenuItem(String text, String icon) {
        super(text);
        iconFile = icon;
        setUI(new TWMenuItemUI());
    }

     public TWMenuItem(String text, boolean acceleratorListener) {
        super(text);
        setUI(new TWMenuItemUI());
         if(acceleratorListener){
             AcceleratorStore.getInstance().addAcceleratorListener(this);
         }
    }

     public TWMenuItem(String text, Icon icon, boolean acceleratorListener) {
        super(text, icon);
        setUI(new TWMenuItemUI());
         if(acceleratorListener){
             AcceleratorStore.getInstance().addAcceleratorListener(this);
         }
    }

    public TWMenuItem(String text, String icon, boolean acceleratorListener) {
        super(text);
        iconFile = icon;
        setUI(new TWMenuItemUI());
        if(acceleratorListener){
             AcceleratorStore.getInstance().addAcceleratorListener(this);
         }
    }

    public Dimension getPreferredSize() {
        Dimension dimension = super.getPreferredSize();
        dimension.setSize(dimension.getWidth() + Constants.OUTLOOK_MENU_COLUMN_WIDTH, dimension.getHeight());
        return dimension;
    }

    public void setVisible(boolean status) {
        super.setVisible(status);
        super.setEnabled(status);
    }

    public void paint(Graphics g) {
        g.translate(Constants.OUTLOOK_MENU_COLUMN_WIDTH * Constants.OUTLOOK_MENU_SIDE, 0);
        super.paint(g);
        g.translate(-(Constants.OUTLOOK_MENU_COLUMN_WIDTH * Constants.OUTLOOK_MENU_SIDE), 0);
        g.setColor(Theme.MENU_SELECTION_COLOR);
        //g.fillRect(0,0,ESPConstants.OUTLOOK_MENU_COLUMN_WIDTH,20);
    }

    public String getIconFile() {
        return iconFile;
    }

    public void setIconFile(String iconFile) {
        this.iconFile = iconFile;
    }

//    public TWMenuItemUI getUI(){
//        return new TWMenuItemUI();
//    }

    public void updateUI() {
        try {
            setBorderPainted(false);
            if (iconFile != null)
                super.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/" + iconFile));
            super.updateUI();
            setUI(new TWMenuItemUI());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int compareTo(Object other) {
        return getText().compareToIgnoreCase(((TWMenuItem)other).getText());
    }

    public String getAcceleratorText() {
        return acceleratorText;
    }

    public void setAccelerator(String acceleratorText) {
        this.acceleratorText = acceleratorText;
    }

    public void acceleratorChanged() {
        String accText = TWActions.getInstance().getActionKeyForID(getAcceleratorID());
        setAccelerator(accText);
    }

    public int getAcceleratorID() {
        return acceleratorID;
    }

    public void setAcceleratorID(int acceleratorID) {
        this.acceleratorID = acceleratorID;
        setAccelerator(TWActions.getInstance().getActionKeyForID(acceleratorID));
    }
}
