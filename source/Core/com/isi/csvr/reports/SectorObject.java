package com.isi.csvr.reports;

import com.isi.csvr.shared.SharedMethods;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class SectorObject {
    private String sectorID;
    private int noOfTrades;
    private int traded;
    private int ups;
    private int downs;
    private int noChange;
    private double turnover;
    private long volume;
    private String description;

    public SectorObject(String sectorID, String description) {
        this.sectorID = sectorID;
        this.description = description;
    }

    public String getDescription() {
        return description;
    }

    public long getVolume() {
        return volume;
    }

    public void addVolume(long volume) {
        this.volume += volume;
    }

    public double getTurnover() {
        return turnover;
    }

    public void addTurnover(double turnover) {
        this.turnover += turnover;
    }

    /*public SectorObject(String sectorID) {
        this.sectorID = sectorID;
    }*/
    public int getDowns() {
        return downs;
    }

    public void setDowns(int downs) {
        this.downs += downs;
    }

    public int getNoChange() {
        return noChange;
    }

    public void setNoChange(int noChange) {
        this.noChange += noChange;
    }

    public int getNoOfTrades() {
        return noOfTrades;
    }

    public void setNoOfTrades(int noOfTrades) {
        this.noOfTrades += noOfTrades;
    }

    public String getSectorID() {
        return sectorID;
    }

    public int getTraded() {
        return traded;
    }

    public void setTraded(int traded) {
        this.traded += traded;
    }

    public int getUps() {
        return ups;
    }

    public void setUps(int ups) {
        this.ups += ups;
    }

    public String getSymbol() {
        String symbol = SharedMethods.getSymbolFromKey(sectorID);
        if (symbol == null)
            return "";
        else
            return symbol;
    }

}