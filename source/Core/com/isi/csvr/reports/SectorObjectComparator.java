package com.isi.csvr.reports;

import java.util.Comparator;
//import com.isi.csvr.shared.Stock;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class SectorObjectComparator implements Comparator {

    public SectorObjectComparator() {
    }

    public int compare(Object o1, Object o2) {
        String obj1 = ((SectorObject) o1).getSectorID();
        String obj2 = ((SectorObject) o2).getSectorID();

        return (obj1.compareTo(obj2));
    }

}