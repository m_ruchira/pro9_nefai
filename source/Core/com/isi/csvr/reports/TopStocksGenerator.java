package com.isi.csvr.reports;

import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.TWControl;
import com.isi.csvr.topstocks.TopstocksObject;
import com.isi.csvr.datastore.DataStore;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * User: sa
 * Date: Apr 7, 2005
 * Time: 1:22:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopStocksGenerator {
    private String[] equitySymbols;

    private TopStocksComparator comparatorTopGainers;
    private TopStocksComparator comparatorTopLosers;
    private TopStocksComparator comparatorMostVolumes;
    private TopStocksComparator comparatorMostTrades;

    private String[] arrayOfTopGainers;
    private String[] arrayOfTopLosers;
    private String[] arrayOfMostVolumes;
    private String[] arrayOfMostTrades;

    private ReportGenerator parent;
    private MarketSummaryReportDataStore dataStore;

    public TopStocksGenerator(ReportGenerator parent, MarketSummaryReportDataStore dataStore) {
//        self = this;
        this.parent = parent;
        this.dataStore = dataStore;

        comparatorMostTrades = new TopStocksComparator(Meta.MOST_ACTIVE_BY_TRADES);
        comparatorMostVolumes = new TopStocksComparator(Meta.MOST_ACTIVE_BY_VOLUME);
        comparatorTopGainers = new TopStocksComparator(Meta.TOP_GAINERS_BY_PCNTCHANGE);
        comparatorTopLosers = new TopStocksComparator(Meta.TOP_LOSERS_BY_PCNTCHANGE);

        comparatorMostTrades.setDataStore(dataStore);
        comparatorMostVolumes.setDataStore(dataStore);
        comparatorTopGainers.setDataStore(dataStore);
        comparatorTopLosers.setDataStore(dataStore);
    }

    public void generateTopStocks() {
        Stock stock = null;

        ArrayList<String> listTopGainers;
        ArrayList<String> listTopLosers;
        ArrayList<String> listMostActiveByVolume;
        ArrayList<String> listMostActiveByTrades;

        listMostActiveByTrades = new ArrayList<String>();
        listMostActiveByVolume = new ArrayList<String>();
        listTopGainers = new ArrayList<String>();
        listTopLosers = new ArrayList<String>();

        if (equitySymbols == null)
            return;

        for (int i = 0; i < equitySymbols.length; i++) {
            stock = dataStore.getStockObject(equitySymbols[i]);
//            stock = DataStore.getSharedInstance().getStockObject(equitySymbols[i]);
            if (stock == null)
                continue;
            if (stock.getVolume() > 0) {
                listMostActiveByVolume.add(stock.getKey());
            }
            if (stock.getNoOfTrades() > 0) {
                listMostActiveByTrades.add(stock.getKey());
            }
            if (stock.getPercentChange() > 0) {
                listTopGainers.add(stock.getKey());
            }
            if (stock.getPercentChange() < 0) {
                listTopLosers.add(stock.getKey());
            }
            stock = null;
        }
        Collections.sort(listMostActiveByTrades, comparatorMostTrades);
        Collections.sort(listMostActiveByVolume, comparatorMostVolumes);
        Collections.sort(listTopGainers, comparatorTopGainers);
        Collections.sort(listTopLosers, comparatorTopLosers);

        arrayOfTopGainers = createDataArrays(listTopGainers);
        arrayOfTopLosers = createDataArrays(listTopLosers);
        arrayOfMostTrades = createDataArrays(listMostActiveByTrades);
        arrayOfMostVolumes = createDataArrays(listMostActiveByVolume);

        listMostActiveByTrades = null;
        listTopGainers = null;
        listTopLosers = null;
        listMostActiveByVolume = null;
//        clearStores();
    }

    private String[] createDataArrays(ArrayList<String> list) {
        String[] array = new String[TWControl.getIntItem("REPORT_TOP_STOCKS_COUNT", 5)];
        for (int i = 0; i < array.length; i++) {
            if (list.size() > i)
                array[i] = list.get(i);
        }
        return array;
    }

    public void disposeObjects() {
        arrayOfMostTrades = null;
        arrayOfMostVolumes = null;
        arrayOfTopGainers = null;
        arrayOfTopLosers = null;
        parent = null;
        equitySymbols = null;
    }

    public void setEquitySymbols(String[] symbols) {
        this.equitySymbols = symbols;
        generateTopStocks();
    }

    public String[] getListTopGainers() {
        return arrayOfTopGainers;
    }

    public String[] getListTopLosers() {
        return arrayOfTopLosers;
    }

    public String[] getListMostActiveByVolume() {
        return arrayOfMostVolumes;
    }

    public String[] getListMostActiveByTrades() {
        return arrayOfMostTrades;
    }
}
