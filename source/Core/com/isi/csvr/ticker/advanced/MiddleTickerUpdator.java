package com.isi.csvr.ticker.advanced;

import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 9, 2008
 * Time: 5:02:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class MiddleTickerUpdator extends Thread {
    private boolean active = true;

    public MiddleTickerUpdator() {
        super("Summary MiddleTicker");
        start();
    }

    public void deactivate() {
        try {
            active = false;
            super.stop();
            MiddleTickerFeeder.isQueueEmpty = true;
            MiddleTickerFeeder.isExchangeModeSelected = false;
            MiddleTickerFeeder.tradeEQueue.clear();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        int tickerStatus;
        boolean played = false;
        while (active) {
            Symbols[] symbols = MiddleTickerFeeder.getFilter();
            try {
                if (MiddleTickerFeeder.isVisible() && !(MiddleTickerFeeder.isNewsModeSelected)) {
                    for (Symbols symbolsObj : symbols) {
                        if (symbolsObj != null) {

                            played = false;
                            String[] keys = symbolsObj.getSymbols();
                            for (String key : keys) {
                                try {
                                    Stock stock = DataStore.getSharedInstance().getStockObject(key);
                                    if ((stock != null && stock.isSymbolEnabled() && MiddleFilterPanel.selectedExchangesForEx.contains(stock.getExchange())) || (MiddleTickerFeeder.isWatchListModeSelected)) {
                                        Exchange exch = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(key));

                                        if ((MiddleFilterPanel.showExTraded && MiddlePanelSettings.MODE == MiddlePanelSettings.exchangelist) || (MiddleFilterPanel.showWatchListTraded && MiddlePanelSettings.MODE == MiddlePanelSettings.watchlist)) {
                                            if ((MiddleFilterPanel.runExOnClose && MiddleFilterPanel.mode == TWTypes.TickerFilter.MODE_EXCHANGE) || (MiddleFilterPanel.runWatchListOnClose && MiddleFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)) {
                                                if (stock.getLastTradeValue() > 0) {
//                                                if ((!exch.isDefault()) && stock.isLastTradeUpdatedMiddle()) {

                                                    if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown()))) {
//                                                        stock.setLastTradeUpdatedMiddle();
                                                        CommonTickerObject oTickerData = new CommonTickerObject();

                                                        if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                        }

                                                        MiddleTickerFeeder.addData(oTickerData);
                                                        played = true;
                                                        delay(MiddleTickerFeeder.getSummaryTickerDelay());
                                                    } else {
                                                        if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null) &&
                                                                (ExchangeStore.getSharedInstance().getMarket(exch.getSymbol(), stock.getMarketID()).isShowInTicker()))) {
                                                            stock.setLastTradeUpdatedMiddle();
                                                            CommonTickerObject oTickerData = new CommonTickerObject();
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                            MiddleTickerFeeder.addData(oTickerData);
                                                            played = true;
                                                            delay(MiddleTickerFeeder.getSummaryTickerDelay());
                                                        }
                                                    }
//                                                }
                                                }
                                            } else {
                                                if (((!exch.isDefault()) || ((exch.getMarketStatus() == Constants.OPEN)||(exch.getMarketStatus() == Constants.TRADING_AT_LAST))) && stock.isLastTradeUpdatedMiddle()) { //---- Eliminated == || (exch.getMarketStatus() == Constants.OPEN))
                                                    if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown())) && stock.getLastTradedPrice() > 0) {
                                                        stock.setLastTradeUpdatedMiddle();
                                                        CommonTickerObject oTickerData = new CommonTickerObject();

                                                        if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                        }

                                                        MiddleTickerFeeder.addData(oTickerData);
                                                        played = true;
                                                        delay(MiddleTickerFeeder.getSummaryTickerDelay());
                                                    } else {
                                                        if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null) &&
                                                                (ExchangeStore.getSharedInstance().getMarket(exch.getSymbol(), stock.getMarketID()).isShowInTicker()))) {
                                                            stock.setLastTradeUpdatedMiddle();
                                                            CommonTickerObject oTickerData = new CommonTickerObject();
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                            MiddleTickerFeeder.addData(oTickerData);
                                                            played = true;
                                                            delay(MiddleTickerFeeder.getSummaryTickerDelay());
                                                        }
                                                    }
                                                }
                                            }
                                        } else
                                        if ((MiddleFilterPanel.showExAll && MiddlePanelSettings.MODE == MiddlePanelSettings.exchangelist) || (MiddleFilterPanel.showWatchListAll && MiddlePanelSettings.MODE == MiddlePanelSettings.watchlist)) {
                                            if ((MiddleFilterPanel.runExOnClose && MiddleFilterPanel.mode == TWTypes.TickerFilter.MODE_EXCHANGE) || (MiddleFilterPanel.runWatchListOnClose && MiddleFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)) {
                                                //                                            if ((!exch.isDefault())) { === Eliminated
                                                if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown())) && stock.getLastTradedPrice() > 0) { //=== Eliminated ---(&& stock.getLastTradedPrice() > 0)
                                                    //                                                    stock.setLastTradeUpdatedUpper();
                                                    CommonTickerObject oTickerData = new CommonTickerObject();

                                                    if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                                        oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                stock.getTradeQuantity(), (float) stock.getChange(),
                                                                (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                    }

                                                    MiddleTickerFeeder.addData(oTickerData);
                                                    played = true;
                                                    delay(MiddleTickerFeeder.getSummaryTickerDelay());
                                                } else {
                                                    if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null))) {
                                                        //                                                        stock.setLastTradeUpdatedUpper();
                                                        CommonTickerObject oTickerData = new CommonTickerObject();
                                                        oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                stock.getTradeQuantity(), (float) stock.getChange(),
                                                                (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                        MiddleTickerFeeder.addData(oTickerData);
                                                        played = true;
                                                        delay(MiddleTickerFeeder.getSummaryTickerDelay());
                                                    }
                                                }
                                                //                                            }
                                            } else {
                                                if (((!exch.isDefault()) || ((exch.getMarketStatus() == Constants.OPEN)||(exch.getMarketStatus() == Constants.TRADING_AT_LAST)))) { //--- Eliminated --  || (exch.getMarketStatus() == Constants.OPEN) --Shanika
                                                    if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown())) && stock.getLastTradedPrice() > 0) { //=== Eliminated ---(&& stock.getLastTradedPrice() > 0)
                                                        //                                                    stock.setLastTradeUpdatedUpper();
                                                        CommonTickerObject oTickerData = new CommonTickerObject();

                                                        if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                        }

                                                        MiddleTickerFeeder.addData(oTickerData);
                                                        played = true;
                                                        delay(MiddleTickerFeeder.getSummaryTickerDelay());
                                                    } else {
                                                        if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null))) {
//                                                                (ExchangeStore.getSharedInstance().getMarket(exch.getSymbol(), stock.getMarketID()).isShowInTicker()))) {
                                                            //                                                        stock.setLastTradeUpdatedUpper();
                                                            CommonTickerObject oTickerData = new CommonTickerObject();
                                                            oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                                    stock.getTradeQuantity(), (float) stock.getChange(),
                                                                    (float) stock.getPercentChange(), 1, 0, stock.getInstrumentType());
                                                            MiddleTickerFeeder.addData(oTickerData);
                                                            played = true;
                                                            delay(MiddleTickerFeeder.getSummaryTickerDelay());
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        //----------------- end default --------------------

                                    }
                                    stock = null;
                                } catch (Exception e) {
                                    System.out.println("Error in key " + key); //todo fix the market code issue
                                    e.printStackTrace();
                                    delay(100);
                                }
                            }
                            if (!played) { // nothing played in this round
                                delay(1000);
                            }
                            keys = null;
                        }
                    }
                } else {
                    delay(2000);
                }
            } catch (Exception e) {
                delay(1000);
            }
            delay(MiddleTickerFeeder.getSummaryTickerDelay());
        }
    }

    private void delay(long time) {
        try {
            Thread.sleep(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
     