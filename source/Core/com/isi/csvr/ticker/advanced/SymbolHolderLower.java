package com.isi.csvr.ticker.advanced;

import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.news.NewsMouseListener;
import com.isi.csvr.news.NEWS_TYPES;
import com.isi.csvr.Menus;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jul 2, 2008
 * Time: 5:42:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class SymbolHolderLower {


    private String symbol;
    private String acronym;
    private String price;
    private String quantity;
    private String percentChange;
    private String change;
    private int splits;
    private String strSplits;
    private int status;
    private String testString;
    private Double testDouble;
    private double max_Price;
    private double min_Price;
    private double lastTrPrice;
    private String exkey;
    private int sperchangelength;
    private int ssymbollength;
    private int sserieslength;
    private int smarketIdlength;
    private int slastTradedlength;
    private int squantitylength;
    private int slastTradedPricelength;
    private int symbolLength;
    private int perChaLength;
    private int changeLength;
    private int priceLength;
    private int quantityLength;
    private int splitLength;
    private int tradeAreaWidth;         // Holds the trade Area Width
    private int mesgAreaWidth;          // Holds the message Area Width
    private int scrollingDir;           // holds the Scrolling Direction
    private int earlierStartXPos;            // Previous displayed X Position
    private int perChangeDisplayOffset; // Per Changed display x Position
    private int totLen;
    private String scrollMessage;          // Holds the message
    private String statusOfMessage;        // Holds the message status

    private static TWDecimalFormat intFormat = new TWDecimalFormat("#,##0");
    private static TWDecimalFormat floatFormat = new TWDecimalFormat("#,##0.00");
    private int displayStatus;
    private int prevDisplayStatus;
    private boolean isMessageMode;

    //  ----------------- For News Ticker -----------------------
    private String newsProvider = null;
    private String newssymbol = null;
    private String exchange = null;
    private String newsID = null;
    private long newsDate = 0;
    private String language = null;
    private String headLine = null;
    private String body = null;
    private String keywords = null;
    private String source = null;
    private int instrumentType = 0;// -1;
    private int newsSymbolLength;
    private int newsAreaWidth;         // Holds the News Area Width
    public static boolean isScrolable = true;
    private String nId = "";
    public static boolean isMouseOverSymbol = true;
    public static boolean isDataCome = false;

    //  ------------------ For Announcements Ticker ---------------------
    public String ansexchange = null;
    private String anssymbol = "";
    private int ansinstrumentType = 0;
    private String key = null;
    private long announcementTime;
    private String announcementNo = null;
    private String anslanguage = null;
    private String ansheadLine = null;
    private String message = null;
    private String url = null;
    private int ansSymbolLength;
    private int ansAreaWidth;

    public void setNewsData(String newsProvider, String newssymbol, String exchange, String newsID, long newsDate,
                            String language, String headLine, String body, String keywords, String source, int instrumentType) {
        this.newsProvider = newsProvider;
        this.newssymbol = newssymbol;
        this.exchange = exchange;
        this.newsID = newsID;
        this.newsDate = newsDate;
        this.language = language;
        this.headLine = headLine;
        this.body = body;
        this.keywords = keywords;
        this.source = source;
        this.instrumentType = instrumentType;// -1;
        calculateNewsArea();
    }

    private void calculateNewsArea() {
        newsSymbolLength = LowerTickerPanel.smallFm.stringWidth(headLine);
        newsAreaWidth = newsSymbolLength + 30;
    }

    public void setAnnouncementData(String ex, String symbol, int ins, String key, String no, String lang, String headline,
                                    String message, String url) {
        this.ansexchange = ex;
        this.anssymbol = symbol;
        this.ansinstrumentType = ins;
        this.key = key;
        this.announcementNo = no;
        this.anslanguage = lang;
        this.ansheadLine = headline;
        this.message = message;
        this.url = url;

        calculateAnsArea();

    }

    private void calculateAnsArea() {
//        ansSymbolLength = LowerTickerPanel.smallFm.stringWidth(ansheadLine);
        ansSymbolLength = MiddleTickerPanel.smallFm.stringWidth(anssymbol) +  MiddleTickerPanel.smallFm.stringWidth(":") + MiddleTickerPanel.smallFm.stringWidth(ansheadLine);
        ansAreaWidth = ansSymbolLength + 30;
    }

    public void setData(String symbol, String acronym, String price, String quantity,
                        Double change, String perChange, int splits, int status, String exchange, int instrument, String key) {
        this.symbol = symbol;
        if ((acronym == null) || (acronym.length() == 0)) {
            acronym = symbol;
        }
        this.acronym = acronym;
        this.price = formatPriceField(price, exchange, symbol, instrument);
        this.quantity = formatField(quantity);
        this.percentChange = formatDecimalField(perChange);
        this.splits = splits;
        this.strSplits = formatField(splits);
        this.status = status;
        this.testDouble = change;
        this.testString = String.valueOf(change);
        this.change = formatPriceField(testString, exchange, symbol, instrument);
        this.lastTrPrice = Double.parseDouble(price);
        this.exkey = key;

        calculateTradeAreaWidth();          // Calculate the display area
        setMaxMinPrice(symbol, exchange, instrument);
    }

    public void revalidateExchange() {
        try {
            if (isMessageMode)
                calculateMessageWidth();          // Calculate the display area
            else
                calculateTradeAreaWidth();          // Calculate the display area
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void revalidateNews() {
        try {
            calculateNewsArea();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void revalidateAnounce() {
        try {
            calculateAnsArea();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void setMaxMinPrice(String symbol, String exchange, int instrument) {
        DataStore dtStore = DataStore.getSharedInstance();
        Stock stk = dtStore.getStockObject(exchange, symbol, instrument);
        try {
            max_Price = stk.getMaxPrice();
        } catch (Exception e) {
            max_Price = Double.MAX_VALUE;
        }
        try {
            min_Price = stk.getMinPrice();
        } catch (Exception e) {
            min_Price = Double.MIN_VALUE;
        }
    }

    private void calculateTradeAreaWidth() {
        int aggOffset = 0;
        sperchangelength = LowerTickerPanel.smallFm.stringWidth(price);

        if (LowerPanelSettings.isChangeMode) {
            sperchangelength = LowerTickerPanel.smallFm.stringWidth(change);
        } else {
            sperchangelength = LowerTickerPanel.smallFm.stringWidth(percentChange) + LowerTickerPanel.smallFm.stringWidth("%");
        }

        splitLength = LowerTickerPanel.smallFm.stringWidth("(" + strSplits + ")");

        // This has to be changed according to the displaying field
        if (LowerPanelSettings.isSymbolShowing)
            this.ssymbollength = LowerTickerPanel.bigFm.stringWidth(symbol); // + 10;
        else
            this.ssymbollength = LowerTickerPanel.bigFm.stringWidth(acronym); // + 10;

        squantitylength = LowerTickerPanel.smallFm.stringWidth(quantity) + LowerTickerPanel.bigFm.stringWidth(" ");
        slastTradedPricelength = LowerTickerPanel.smallFm.stringWidth(price);
//        tradeAreaWidth = ssymbollength + squantitylength + slastTradedPricelength + 70; // + space;
        //  tradeAreaWidth = ssymbollength + squantitylength + slastTradedPricelength + sperchangelength + 70; // + space;

        if (splits > 1) {
            tradeAreaWidth = ssymbollength + splitLength + squantitylength + slastTradedPricelength + sperchangelength + 70; // + space;
        } else {
            tradeAreaWidth = ssymbollength + squantitylength + slastTradedPricelength + sperchangelength + 70; // + space;
        }

    }

    private String formatPriceField(String sValue, String exchange, String symbol, int instrument) {
        double cellValue = 0;
        String value = null;

        if (!sValue.trim().equals("")) {
            cellValue = new Double(sValue);
        }
        value = SharedMethods.getDecimalFormat(exchange, symbol, instrument).format(cellValue);
        return value;
    }


    private String formatField(String sValue) {
        double cellValue = 0;
        String value = null;

        if (!sValue.trim().equals("")) {
            cellValue = new Double(sValue);
        }
        if (cellValue <= 0) {
            return " ";
        }
        value = intFormat.format(cellValue);
        return value;
    }

    private String formatField(int value) {
        return intFormat.format(value);
    }

    private String formatDecimalField(String sValue) {
        double cellValue = 0;
        String value = null;

        if (!sValue.trim().equals("")) {
            cellValue = new Double(sValue);
        }
        value = floatFormat.format(cellValue);
        return value;
    }

    public void setScrollingDir(int newDir) {
        this.scrollingDir = newDir;
    }

    public int getElementLength() {
        return this.tradeAreaWidth;

    }

    public int getNewsElementLength() {
        return this.newsAreaWidth;
    }

    public int getAnnouncementElementLength() {
        return this.ansAreaWidth;
    }

    public int getEarlierXPos() {
        return earlierStartXPos;
    }

    public void setEarlierXPos(int width) {
        earlierStartXPos = width;
    }

    public int getDisplayStatus() {
        return displayStatus;
    }

    public int getScrollingDir() {
        return this.scrollingDir;
    }

    public void draw(int startingXPos, int direction, Graphics textG) {
        if (direction == 1) {
            int offsetDistance = 0;         // Holds the X offsets from the starting X Position
            if (isScrolable && isDataCome) {
                startingXPos = startingXPos - LowerTickerPanel.PIXEL_INCREMENT;
            }
            earlierStartXPos = startingXPos;
            if (LowerTickerPanel.isMouseOver) {
                if (testDouble < 0) {
                    textG.setColor(LowerPanelSettings.DOWN_COLOR);
                } else if (testDouble > 0) {
                    textG.setColor(LowerPanelSettings.UP_COLOR);
                } else if (testDouble == 0) {
                    textG.setColor(LowerPanelSettings.NO_CHANGE_COLOR);
                }
                if (LowerPanelSettings.isSymbolShowing) {
                    textG.setFont(LowerTickerPanel.bigFont);
                    textG.drawString(symbol, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                } else {
                    textG.setFont(LowerTickerPanel.bigFont);
                    textG.drawString(acronym, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                }

                if (LowerPanelSettings.isChangeMode) {
                    textG.setFont(LowerTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                } else {
                    textG.setFont(LowerTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + percentChange + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + percentChange + "%" + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                }
            }

            if (!LowerTickerPanel.isMouseOver) {
                if (testDouble > 0) {
                    textG.setColor(LowerPanelSettings.UP_COLOR);
                } else if (testDouble < 0) {
                    textG.setColor(LowerPanelSettings.DOWN_COLOR);
                } else if (testDouble == 0) {
                    textG.setColor(LowerPanelSettings.NO_CHANGE_COLOR);
                }
                if (LowerPanelSettings.isSymbolShowing) {
                    textG.setFont(LowerTickerPanel.bigFont);
                    textG.drawString(symbol, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                } else {
                    textG.setFont(LowerTickerPanel.bigFont);
                    textG.drawString(acronym, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                }

                if (LowerPanelSettings.isChangeMode) {
                    textG.setFont(LowerTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                } else {
                    textG.setFont(LowerTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + percentChange + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + percentChange + "%" + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                }
            }
            if (LowerTickerPanel.isMouseOver) {
                int xbeginPosition = startingXPos;
                int xlength = startingXPos + getElementLength();
                if (isMouseOverSymbol) {
                    if ((xbeginPosition <= LowerTickerPanel.xPos) && (LowerTickerPanel.xPos <= xlength)) {
                        textG.setColor(LowerPanelSettings.SYMBOL_HIGHLIGHT_COLOR);
                        if (LowerPanelSettings.isSymbolShowing) {
                            textG.setFont(LowerTickerPanel.bigFont);
                            textG.drawString(symbol, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                        } else {
                            textG.setFont(LowerTickerPanel.bigFont);
                            textG.drawString(acronym, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                        }

                        if (LowerPanelSettings.isChangeMode) {
                            textG.setFont(LowerTickerPanel.smallFont);
                            if (splits > 1) {
                                textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                            } else {
                                textG.drawString(" " + quantity + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                            }
                        } else {
                            textG.setFont(LowerTickerPanel.smallFont);
                            if (splits > 1) {
                                textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + percentChange + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                            } else {
                                textG.drawString(" " + quantity + "@" + price + "" + "(" + percentChange + "%" + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                            }
                        }
                        isScrolable = false;
                        LowerTickerPanel.exkey = exkey;
                    }
                }
            }
        }

        if (direction == 2) {
            int offsetDistance = 0;         // Holds the X offsets from the starting X Position
            if (isScrolable && isDataCome) {
                startingXPos = startingXPos + LowerTickerPanel.PIXEL_INCREMENT;
            }
            earlierStartXPos = startingXPos;
            if (LowerTickerPanel.isMouseOver) {
                if (testDouble > 0) {
                    textG.setColor(LowerPanelSettings.UP_COLOR);
                } else if (testDouble < 0) {
                    textG.setColor(LowerPanelSettings.DOWN_COLOR);

                } else if (testDouble == 0) {
                    textG.setColor(LowerPanelSettings.NO_CHANGE_COLOR);
                }
                if (LowerPanelSettings.isSymbolShowing) {
                    textG.setFont(LowerTickerPanel.bigFont);
                    textG.drawString(symbol, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                } else {
                    textG.setFont(LowerTickerPanel.bigFont);
                    textG.drawString(acronym, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                }

                if (LowerPanelSettings.isChangeMode) {
                    textG.setFont(LowerTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                } else {
                    textG.setFont(LowerTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + percentChange + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + percentChange + "%" + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                }
            }
            if (!LowerTickerPanel.isMouseOver) {
                if (testDouble > 0) {
                    textG.setColor(LowerPanelSettings.UP_COLOR);
                } else if (testDouble < 0) {
                    textG.setColor(LowerPanelSettings.DOWN_COLOR);

                } else if (testDouble == 0) {
                    textG.setColor(LowerPanelSettings.NO_CHANGE_COLOR);
                }
                if (LowerPanelSettings.isSymbolShowing) {
                    textG.setFont(LowerTickerPanel.bigFont);
                    textG.drawString(symbol, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                } else {
                    textG.setFont(LowerTickerPanel.bigFont);
                    textG.drawString(acronym, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                }

                if (LowerPanelSettings.isChangeMode) {
                    textG.setFont(LowerTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                } else {
                    textG.setFont(LowerTickerPanel.smallFont);
                    if (splits > 1) {
                        textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + percentChange + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    } else {
                        textG.drawString(" " + quantity + "@" + price + "" + "(" + percentChange + "%" + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                    }
                }
            }

            if (LowerTickerPanel.isMouseOver) {
                int xbeginPosition = startingXPos;
                int xlength = startingXPos + getElementLength();
                if (isMouseOverSymbol) {
                    if ((xbeginPosition <= LowerTickerPanel.xPos) && (LowerTickerPanel.xPos <= xlength)) {
                        textG.setColor(LowerPanelSettings.SYMBOL_HIGHLIGHT_COLOR);
                        if (LowerPanelSettings.isSymbolShowing) {
                            textG.setFont(LowerTickerPanel.bigFont);
                            textG.drawString(symbol, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                        } else {
                            textG.setFont(LowerTickerPanel.bigFont);
                            textG.drawString(acronym, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                        }

                        if (LowerPanelSettings.isChangeMode) {
                            textG.setFont(LowerTickerPanel.smallFont);
                            if (splits > 1) {
                                textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                            } else {
                                textG.drawString(" " + quantity + "@" + price + "" + "(" + change + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                            }
                        } else {
                            textG.setFont(LowerTickerPanel.smallFont);
                            if (splits > 1) {
                                textG.drawString(" " + quantity + "(" + strSplits + ")" + "@" + price + "" + "(" + percentChange + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                            } else {
                                textG.drawString(" " + quantity + "@" + price + "" + "(" + percentChange + "%" + ")", startingXPos + ssymbollength, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                            }
                        }
                        isScrolable = false;
                        LowerTickerPanel.exkey = exkey;
                    }
                }
            }
        }
    }

    private void calculateMessageWidth() {
        mesgAreaWidth = LowerPanelSettings.symbolFm.stringWidth(this.scrollMessage) + LowerPanelSettings.LEFT_SEPERATOR;
    }


    public SymbolHolderLower() {
        displayStatus = 0;
        prevDisplayStatus = 0;
        scrollMessage = "";   // Holds the message
        statusOfMessage = "";   // Holds the message status
    }

    public void drawNews(int startingXPos, int scrollingDirection, Graphics2D textG) {
        if (scrollingDirection == 1) {
            int offsetDistance = 0;         // Holds the X offsets from the starting X Position

            if (isScrolable && isDataCome) {
                startingXPos = startingXPos - 1;
            }
            earlierStartXPos = startingXPos;
            if (LowerTickerPanel.isMouseOver) {
                textG.setColor(LowerPanelSettings.NEWS_TITLE_COLOR);
                textG.setFont(LowerTickerPanel.smallFont);
                textG.drawString(headLine, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
            }

            if (!LowerTickerPanel.isMouseOver) {
                textG.setColor(LowerPanelSettings.NEWS_TITLE_COLOR);
                textG.setFont(LowerTickerPanel.smallFont);
                textG.drawString(headLine, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
            }
            if (LowerTickerPanel.isMouseOver) {
                int xbeginPosition = startingXPos;
                int xlength = startingXPos + getNewsElementLength();
                if (isMouseOverSymbol) {
                    if ((xbeginPosition <= LowerTickerPanel.xPos) && (LowerTickerPanel.xPos <= xlength)) {
                        textG.setColor(LowerPanelSettings.NEWS_HIGHLIGHT_COLOR);
                        textG.setFont(LowerTickerPanel.smallFont);
                        textG.drawString(headLine, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                        isScrolable = false;
                        showNewsWindow(newsID);
                    }
                }
            }
        } else {
            int offsetDistance = 0;         // Holds the X offsets from the starting X Position

            if (isScrolable && isDataCome) {
                startingXPos = startingXPos + 1;
            }
            earlierStartXPos = startingXPos;
            if (LowerTickerPanel.isMouseOver) {
                textG.setColor(LowerPanelSettings.NEWS_TITLE_COLOR);
                textG.setFont(LowerTickerPanel.smallFont);
                textG.drawString(headLine, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
            }

            if (!LowerTickerPanel.isMouseOver) {
                textG.setColor(LowerPanelSettings.NEWS_TITLE_COLOR);
                textG.setFont(LowerTickerPanel.smallFont);
                textG.drawString(headLine, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
            }
            if (LowerTickerPanel.isMouseOver) {
                int xbeginPosition = startingXPos;
                int xlength = startingXPos + getNewsElementLength();
                if (isMouseOverSymbol) {
                    if ((xbeginPosition <= LowerTickerPanel.xPos) && (LowerTickerPanel.xPos <= xlength)) {
                        textG.setColor(LowerPanelSettings.NEWS_HIGHLIGHT_COLOR);
                        textG.setFont(LowerTickerPanel.smallFont);
                        textG.drawString(headLine, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                        isScrolable = false;
                        showNewsWindow(newsID);
                    }
                }
            }

        }
    }

    public void drawAnnouncements(int startingXPos, int scrollingDirection, Graphics2D textG) {

        if (scrollingDirection == 1) {
            int offsetDistance = 0;         // Holds the X offsets from the starting X Position
            if (isScrolable && isDataCome) {
                startingXPos = startingXPos - 1;
            }
            earlierStartXPos = startingXPos;
            if (LowerTickerPanel.isMouseOver) {
                textG.setColor(LowerPanelSettings.ANNOUNCEMENT_TITLE_COLOR);
                textG.setFont(LowerTickerPanel.smallFont);
                textG.drawString(anssymbol+ ":"+ansheadLine, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
            }
            if (!LowerTickerPanel.isMouseOver) {
                textG.setColor(LowerPanelSettings.ANNOUNCEMENT_TITLE_COLOR);
                textG.setFont(LowerTickerPanel.smallFont);
                textG.drawString(anssymbol+ ":"+ansheadLine, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
            }
            if (LowerTickerPanel.isMouseOver) {
                int xbeginPosition = startingXPos;
                int xlength = startingXPos + getAnnouncementElementLength();
                if (isMouseOverSymbol) {
                    if ((xbeginPosition <= LowerTickerPanel.xPos) && (LowerTickerPanel.xPos <= xlength)) {
                        textG.setColor(LowerPanelSettings.ANNOUNCEMENT_HIGHLIGHT_COLOR);
                        textG.setFont(LowerTickerPanel.smallFont);
                        textG.drawString(anssymbol+ ":"+ansheadLine, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                        isScrolable = false;
                        LowerTickerPanel.ansId = announcementNo;
                        LowerTickerPanel.time = announcementTime;
                        LowerTickerPanel.message = message;
                        LowerTickerPanel.title = ansheadLine;
                        LowerTickerPanel.symbol = anssymbol;
                    }
                }
            }
        } else {
            int offsetDistance = 0;         // Holds the X offsets from the starting X Position
            if (isScrolable && isDataCome) {
                startingXPos = startingXPos + 1;
            }
            earlierStartXPos = startingXPos;
            if (LowerTickerPanel.isMouseOver) {
                textG.setColor(LowerPanelSettings.ANNOUNCEMENT_TITLE_COLOR);
                textG.setFont(LowerTickerPanel.smallFont);
                textG.drawString(anssymbol+ ":"+ansheadLine, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
            }
            if (!LowerTickerPanel.isMouseOver) {
                textG.setColor(LowerPanelSettings.ANNOUNCEMENT_TITLE_COLOR);
                textG.setFont(LowerTickerPanel.smallFont);
                textG.drawString(anssymbol+ ":"+ansheadLine, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
            }
            if (LowerTickerPanel.isMouseOver) {
                int xbeginPosition = startingXPos;
                int xlength = startingXPos + getAnnouncementElementLength();
                if (isMouseOverSymbol) {
                    if ((xbeginPosition <= LowerTickerPanel.xPos) && (LowerTickerPanel.xPos <= xlength)) {
                        textG.setColor(LowerPanelSettings.ANNOUNCEMENT_HIGHLIGHT_COLOR);
                        textG.setFont(LowerTickerPanel.smallFont);
                        textG.drawString(anssymbol+ ":"+ansheadLine, startingXPos, LowerPanelSettings.INDIA_DRAW_HEIGHT);
                        isScrolable = false;
                        LowerTickerPanel.ansId = announcementNo;
                        LowerTickerPanel.time = announcementTime;
                        LowerTickerPanel.message = message;
                        LowerTickerPanel.title = ansheadLine;
                        LowerTickerPanel.symbol = anssymbol;
                    }
                }
            }
        }
    }

    public void showNewsWindow(String newsID) {
        LowerTickerPanel.newsId = newsID;
    }
}
