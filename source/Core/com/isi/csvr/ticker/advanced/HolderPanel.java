package com.isi.csvr.ticker.advanced;

import com.isi.csvr.ticker.custom.Ticker;
import com.isi.csvr.ticker.GlobalSettings;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Mar 16, 2009
 * Time: 9:20:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class HolderPanel extends JPanel implements Themeable {

    public static HolderPanel selfRef;
    public static int ADV_TICKER_HEIGHT = 100;
    public static int ADV_TICKER_WIDTH = 1362;  //1276


    public static HolderPanel getInstance() {
        if (selfRef == null) {
            selfRef = new HolderPanel();
        }
        return selfRef;
    }

    public HolderPanel() {
        this.addComponentListener(new ComponentListener() {
            public void componentResized(ComponentEvent e) {
                CommonSettings.Ticker_Width_Before = CommonSettings.TICKER_WIDTH_LOWER;
                CommonSettings.IS_EQ_DATA_COME = true;
                CommonSettings.IS_DERI_DATA_COME = true;
                CommonSettings.IS_INT_DATA_COME = true;
                switch (SharedSettings.Frame_Header_Changed_by) {
                    case 0:
                        setPreferredSize(new Dimension(ADV_TICKER_WIDTH, ADV_TICKER_HEIGHT));
                        break;
                    case 1:
                        if (Ticker.settings.isHeaderVisible()) {
                            setPreferredSize(new Dimension(ADV_TICKER_WIDTH, (ADV_TICKER_HEIGHT - GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT)));
                        } else {
                            setPreferredSize(new Dimension(ADV_TICKER_WIDTH, ADV_TICKER_HEIGHT));
                        }
                        break;
                }
                /* if (!SharedSettings.Frame_Header_Changed) {
                    if (Ticker.settings.isHeaderVisible()) {
                        setPreferredSize(new Dimension(ADV_TICKER_WIDTH, (ADV_TICKER_HEIGHT - GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT)));
                    } else {
                        setPreferredSize(new Dimension(ADV_TICKER_WIDTH, ADV_TICKER_HEIGHT));
                    }
                } else {
                    setPreferredSize(new Dimension(ADV_TICKER_WIDTH, ADV_TICKER_HEIGHT));
                }*/

               // CommonSettings.adjustTickerSize(getWidth());
                CommonSettings.adjustTickerSize(getPreferredSize().width);
            }

            public void componentMoved(ComponentEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void componentShown(ComponentEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }

            public void componentHidden(ComponentEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });
        selfRef = this;
    }

    public void setTickerSize(int width, int height) {
        this.setPreferredSize(new Dimension(width, height));
    }

    public Dimension getPreferredSize() {

        Dimension dim = new Dimension(0, 0);
        if (!Ticker.settings.isHeaderVisible()) {
            if (PanelSetter.upperTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, UpperPanelSettings.INDIA_TICKER_HEIGHT));
            }
            if (PanelSetter.middleTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + MiddlePanelSettings.INDIA_TICKER_HEIGHT)));
            }
            if (PanelSetter.lowerTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + LowerPanelSettings.INDIA_TICKER_HEIGHT)));
            }
            System.out.println("Ticker frame dim : "+dim.width+ " , "+dim.height);
            return dim;

        } else {
            if (PanelSetter.upperTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, UpperPanelSettings.INDIA_TICKER_HEIGHT));
            }
            if (PanelSetter.middleTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + MiddlePanelSettings.INDIA_TICKER_HEIGHT)));
            }
            if (PanelSetter.lowerTicker) {
                dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + LowerPanelSettings.INDIA_TICKER_HEIGHT)));
            }

            dim = new Dimension(new Dimension(CommonSettings.TICKER_WIDTH_LOWER, (dim.height + GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT)));
            return dim;
        }
    }

    public void applyTheme() {
        this.setBackground(UpperPanelSettings.TICKER_BORDER_COLOR);
    }
}
