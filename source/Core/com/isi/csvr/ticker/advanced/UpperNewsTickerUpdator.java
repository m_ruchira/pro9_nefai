package com.isi.csvr.ticker.advanced;

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.news.NewsStore;
import com.isi.csvr.news.News;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 30, 2008
 * Time: 10:49:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class UpperNewsTickerUpdator extends Thread{
    private boolean active = true;
    private DynamicArray dataStore;

      public UpperNewsTickerUpdator() {
          super("Summary UpperTicker");
          start();
      }

  public void deactivate() {
      try {
          active = false;
          super.stop();
          UpperTickerFeeder.isQueueEmpty = true;
          UpperTickerFeeder.isNewsModeSelected = false;
          UpperTickerFeeder.newsQueue.clear();

      } catch (Exception e) {
          e.printStackTrace();
      }
  }


     public void run() {
         while (active) {
             if (UpperTickerFeeder.newsQueue != null && UpperTickerFeeder.newsQueue.isEmpty()) {
                 newsModeSelector();
             }
             try {
                 Thread.sleep(300);
             } catch (InterruptedException e) {
                 e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
             }
         }
     }


    public void newsModeSelector() {
        dataStore = NewsStore.getSharedInstance().getFilteredNewsStore();
        for (int i = 0; i < dataStore.size(); i++) {
            UpperTickerFeeder.addNewsData((News) dataStore.get(i));
        }
        UpperTickerFeeder.isNewsModeSelected = true;
    }
}
