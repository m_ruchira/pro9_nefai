package com.isi.csvr.ticker.advanced;

import com.isi.csvr.TWRadioButtonMenuItem;
import com.isi.csvr.Client;
import com.isi.csvr.news.NewsProvidersStore;
import com.isi.csvr.news.NewsProvider;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.watchlist.WatchListStore;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.TWTypes;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.TWCustomCheckBox;
import com.isi.csvr.shared.Settings;

import javax.swing.*;
import java.util.*;
import java.awt.*;
import java.io.IOException;
import java.io.FileInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 22, 2008
 * Time: 1:21:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class LowerPanelSettings {
    public static Hashtable exchngeHash = new Hashtable();
    public static boolean FIRST_EXCHANGE = false;
    public static boolean FIRST_WATCH = false;
    public static boolean FIRST_ANS = false;
    public static boolean FIRST_NEWS = false;
    public final static int LEFT_SEPERATOR = 30; //50; //30;
    public final static int RIGHT_SEPERATOR = 30; //10; //5;
    public final static int MENU_IMAGE_HEIGHT = 40; //48;
    public final static int MENU_IMAGE_WIDTH = 20;
    public final static int MENU_AREA_HEIGHT = 48;   //50;
    public final static int MENU_AREA_WIDTH = 20; //25;   //100;
    public final static int MENU_SEPERATOR_WIDTH = 0; //5;
    public static int UNIT_INCREMENT = 1;        // No of pixel Positions per a move
    public static boolean ENGLISH_VERSION = true; //true;
    public static boolean CONTINUE_LOOP = false;
    public static boolean isSymbolShowing = false;
    public static boolean CONTINUE_EQ_LOOP = false;
    public static boolean CONTINUE_DERI_LOOP = false;
    public static boolean CONTINUE_INT_LOOP = false;
    public static TWRadioButtonMenuItem radioE;
    public static TWRadioButtonMenuItem radioD;
    public static TWRadioButtonMenuItem radioI;

    public static boolean showSummaryTicker = false;
    public static boolean isChangeMode = true;
    public static int STOCK_TICKER_HEIGHT = 90; //42; //30; // 300
    public static int SYMBOL_DISPLAY_HEIGHT = 12;
    public static int QTY_DISPLAY_HEIGHT = 24;
    public static int DETAIL_DISPLAY_HEIGHT = 36;
    public static int MAX_MIN_RECT_HEIGHT = 3;
    public static int MAX_MIN_RECT_ALLOWENCE = 5;
    public static int MAX_RECT_DISPLAY_HEIGHT = 1;
    public static int MIN_RECT_DISPLAY_HEIGHT = 86;

    public static int FONT_MEDIUM = 12;
    public static int FONT_STYLE = Font.BOLD;
    public static FontMetrics symbolFm;
    public static FontMetrics smallFm;
    public static Font bigFont;
    public static Font smallFont;

    public final static String IMAGE_PATH = "images/ticker/";
    public final static String MENU_IMAGE_FILE = "icon_buttons";

    public final static String UP = "U";
    public final static String DOWN = "D";
    public final static String NOCHANGE = "N";
    public final static String SMALL = "S";
    public final static String VALNULL = "";

    public static boolean FIRST_MIXED_MODE = false;
    public static final String TICKER_RECORD_DELIMETER = ";";
    public static Color UP_COLOR = Color.green.darker();
    public static Color DOWN_COLOR = Color.red;
    public static Color NO_CHANGE_COLOR = Color.black;
    public static Color NULL_COLOR = Color.black;
    public static Color SMALL_TRADE_COLOR = Color.black;
    public static Color TICKER_BORDER_COLOR = Color.black;
    public static Color TICKER_MIN_PRICE_COLOR = Color.black;
    public static Color TICKER_MAX_PRICE_COLOR = Color.black;

    public static Color SYMBOL_HIGHLIGHT_COLOR = Color.MAGENTA;
    public static Color TICKER_BACKGROUND = Color.white;
    public static Color TICKER_SEPERATOR = new Color(51, 102, 153);
    public static Color NEWS_TITLE_COLOR = Color.BLACK;
    public static Color NEWS_HIGHLIGHT_COLOR = Color.RED;

    public static Color ANNOUNCEMENT_TITLE_COLOR = Color.BLACK;
    public static Color ANNOUNCEMENT_HIGHLIGHT_COLOR = Color.RED;

    public static Color FILTER_PANEL_COLOR = Color.WHITE;
    public static Color FILTER_PANEL_HEADER_COLOR = Color.WHITE;


    public static int TICKER_WIDTH = 600;
    public static int INDIA_TICKER_HEIGHT = 29;  //27
    public static int INDIA_DRAW_HEIGHT = 19;  //17
    public static String IMAGE_LIST;
    public static boolean watchListMode = false;
    public static boolean exchnageListMode = false;
    public static boolean checkNoneMode = false;

    public static int exchangelist = 1;
    public static int watchlist = 2;
    public static int anslist = 3;
    public static int newslist = 4;

    public static int MODE = newslist;

    public static boolean IS_EQ_SELECTED = false;
    public static boolean IS_DERI_SELECTED = false;
    public static boolean IS_INT_SELECTED = false;

    public static int EQ_TIK_UP = 1;
    public static int EQ_TIK_DWN = 0;
    public static int EQ_STATUS = EQ_TIK_UP;
    public static int S_EQ_STATUS = EQ_TIK_UP;

    public static int FONT_SMALL = 13;  //11
    public static int FONT_BIG = 16;  //14
    public static int BIG_FONT_STYLE = Font.BOLD;
    public static int SMALL_FONT_STYLE = Font.BOLD;
    public static String FONT_TYPE = "Arial";
//    public static ArrayList<String> WatchLists = new ArrayList<String>();
//    public static ArrayList<String> ExchangeLists = new ArrayList<String>();
//    public static ArrayList<String> AnsLists = new ArrayList<String>();
    //    public static ArrayList<String> NewsLists = new ArrayList<String>();
    private static String selectedExchangeForTickers;
    static TWTypes.TickerFilter tickerMode;
    static PanelSetter panelSet = new PanelSetter();
    private static ArrayList<String> exchangeList = new ArrayList<String>();
    private static ArrayList<String> watchList = new ArrayList<String>();
    private static ArrayList<String> announcementsList = new ArrayList<String>();
    private static ArrayList<String> newsList = new ArrayList<String>();

    private static Properties prop;
    public static ArrayList<String> newssources;

    public static boolean DEF_RELOD = true;


    public static ArrayList<String> getExchangeList() {
        return exchangeList;
    }

    public static void setExchangeList(ArrayList<String> exchangeLists) {
        exchangeList = exchangeLists;
    }

    public static ArrayList<String> getNewsList() {
        return newsList;
    }

    public static void setNewsList(ArrayList<String> newsList) {
        LowerPanelSettings.newsList = newsList;
    }

    public static ArrayList<String> getAnnouncementsList() {
        return announcementsList;
    }

    public static void setAnnouncementsList(ArrayList<String> announcementsList) {
        LowerPanelSettings.announcementsList = announcementsList;
    }

    public static ArrayList<String> getWatchList() {
        return watchList;
    }

    public static void setWatchList(ArrayList<String> watchList) {
        LowerPanelSettings.watchList = watchList;
    }

    public static void loadingFromWorkSpace(String indtickerWSString) {
        ArrayList<String> WatchLists = new ArrayList<String>();
        ArrayList<String> ExchangeLists = new ArrayList<String>();
        ArrayList<String> AnsLists = new ArrayList<String>();
        ArrayList<String> NewsLists = new ArrayList<String>();
        DEF_RELOD = true;
        CommonSettings.IS_INT_SET_LOD = true;

        String loadString = indtickerWSString;
        ArrayList<String> tickerData = new ArrayList<String>();
        StringTokenizer fields = new StringTokenizer(loadString, Meta.RS);
        while (fields.hasMoreElements()) {
            tickerData.add(fields.nextToken());
        }
        LowerPanelSettings.BIG_FONT_STYLE = Integer.parseInt(tickerData.get(0));
        LowerPanelSettings.SMALL_FONT_STYLE = Integer.parseInt(tickerData.get(1));
        LowerPanelSettings.FONT_BIG = Integer.parseInt(tickerData.get(2));
        LowerPanelSettings.FONT_SMALL = Integer.parseInt(tickerData.get(3));
        LowerPanelSettings.FONT_TYPE = tickerData.get(4);
        LowerPanelSettings.INDIA_DRAW_HEIGHT = Integer.parseInt(tickerData.get(5));
        LowerPanelSettings.INDIA_TICKER_HEIGHT = Integer.parseInt(tickerData.get(6));
        LowerPanelSettings.EQ_STATUS = Integer.parseInt(tickerData.get(7));
        LowerPanelSettings.S_EQ_STATUS = LowerPanelSettings.EQ_STATUS;
        LowerPanelSettings.MODE = Integer.parseInt(tickerData.get(8));
        String Exchangelists = tickerData.get(9);
        LowerFilterPanel.EXCHANGE_LIST = Exchangelists;
        StringTokenizer exchange = new StringTokenizer(Exchangelists, TICKER_RECORD_DELIMETER);
        while (exchange.hasMoreElements()) {
            ExchangeLists.add(exchange.nextToken());
        }
        setExchangeList(ExchangeLists);
        String Watchlists = tickerData.get(10);

        LowerFilterPanel.WATCH_LIST = Watchlists;
        StringTokenizer watch = new StringTokenizer(Watchlists, TICKER_RECORD_DELIMETER);
        while (watch.hasMoreElements()) {
            WatchLists.add(watch.nextToken());
        }
        setWatchList(WatchLists);
        String Anslists = tickerData.get(11);

        LowerFilterPanel.ANNOUNCE_LIST = Anslists;

        StringTokenizer ans = new StringTokenizer(Anslists, TICKER_RECORD_DELIMETER);
        while (ans.hasMoreElements()) {
            AnsLists.add(ans.nextToken());
        }
        setAnnouncementsList(AnsLists);
        String Newslists = tickerData.get(12);

        LowerFilterPanel.NEWS_LIST = Newslists;
        StringTokenizer news = new StringTokenizer(Newslists, TICKER_RECORD_DELIMETER);
        while (news.hasMoreElements()) {
            NewsLists.add(news.nextToken());
        }
        setNewsList(NewsLists);

        LowerTicker.tickerSpeed = tickerData.get(13);
        Client.getInstance().getLowerTicker().applySavedTickerVariables();
        LowerTickerPanel.reloadFont(LowerTickerPanel.getInstance());
        if (LowerPanelSettings.MODE == 1) {
            LowerFilterPanel.EXG_SAVED = true;
            LowerTickerFeeder.isExchangeModeSelected = true;
            LowerFilterPanel.mode = TWTypes.TickerFilter.MODE_EXCHANGE;
            LowerTickerPanel.getInstance().reloadFontforExchange();
        }
        if (LowerPanelSettings.MODE == 2) {
            LowerTickerFeeder.isWatchListModeSelected = true;
            LowerFilterPanel.mode = TWTypes.TickerFilter.MODE_WATCHLIST;
            LowerTickerPanel.getInstance().reloadFontforExchange();
        }
        if (LowerPanelSettings.MODE == 3) {
            LowerFilterPanel.ANS_SAVED = true;
            LowerTickerFeeder.isAnnouncementModeSelected = true;
            LowerFilterPanel.mode = TWTypes.TickerFilter.MODE_ANNOUNCEMENT;
            LowerTickerPanel.getInstance().reloadFontforAnnouncements();
        }

        if (LowerPanelSettings.MODE == 4) {
            LowerFilterPanel.NEWS_SAVED = true;
            LowerTickerFeeder.isNewsModeSelected = true;
            LowerFilterPanel.mode = TWTypes.TickerFilter.MODE_NEWS;
            LowerTickerPanel.getInstance().reloadFontforNews();
        }

        LowerFilterPanel.showExTraded = Boolean.parseBoolean(tickerData.get(14));
        LowerFilterPanel.showExAll = Boolean.parseBoolean(tickerData.get(15));
        LowerFilterPanel.runExOnClose = Boolean.parseBoolean(tickerData.get(16));
        LowerFilterPanel.showWatchListTraded = Boolean.parseBoolean(tickerData.get(17));
        LowerFilterPanel.showWatchListAll = Boolean.parseBoolean(tickerData.get(18));
        LowerFilterPanel.runWatchListOnClose = Boolean.parseBoolean(tickerData.get(19));
        LowerFilterPanel.isVeryFirstTime = Boolean.parseBoolean(tickerData.get(20));
        try {
            LowerFilterPanel.getInstance().applyFilter();
            LowerTickerPanel.getInstance().adjustControls();
            LowerTickerPanel.getInstance().redrawTicker();
            LowerTickerPanel.getInstance().updateUI();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (LowerPanelSettings.EQ_STATUS == 1) {
            CommonSettings.INT_UP = true;
            panelSet.setIntergratedPanel(true);
            CommonSettings.selectIntegrityTicker(true);
            IS_EQ_SELECTED = true;
        }else if (LowerPanelSettings.EQ_STATUS == 0) {
            CommonSettings.INT_UP = false;
            panelSet.setIntergratedPanel(false);
            CommonSettings.selectIntegrityTicker(false);
            IS_EQ_SELECTED = false;
        }
    }


    public static void defaultSettingsLoader() {
        if (!DEF_RELOD && !CommonSettings.IS_INT_SET_LOD) {
            LowerPanelSettings.MODE = newslist;
            try {
                loadSettings();
                setNewsSources();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            loadNewsSources();
            LowerFilterPanel.NEWS_SAVED = true;

            LowerTickerFeeder.isNewsModeSelected = true;
            LowerFilterPanel.mode = TWTypes.TickerFilter.MODE_NEWS;
            LowerTickerPanel.getInstance().reloadFontforNews();

            LowerFilterPanel.getInstance().applyFilter();
            LowerTickerPanel.getInstance().adjustControls();
            LowerTickerPanel.getInstance().redrawTicker();
            LowerTickerPanel.getInstance().updateUI();
            LowerPanelSettings.EQ_STATUS = EQ_TIK_UP;
            LowerPanelSettings.S_EQ_STATUS = LowerPanelSettings.EQ_STATUS;
            if (LowerPanelSettings.EQ_STATUS == 1) {
                CommonSettings.INT_UP = true;
                panelSet.setIntergratedPanel(true);
                CommonSettings.selectIntegrityTicker(true);
                IS_EQ_SELECTED = true;
            }
            if (!SharedSettings.is_Lower_Very_First_Time) {
            CommonSettings.IS_INT_SET_LOD = true;
            LowerPanelSettings.DEF_RELOD = true;
        }
    }
    }


    public static void loadSettings() throws IOException {
        prop = new Properties();
        FileInputStream oIn = null;
        try {
            oIn = new FileInputStream(Settings.getAbsolutepath() + "DataStore/news.dll");
            prop.load(oIn);
            oIn.close();

        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    public static void setNewsSources() {
        try {
            newssources = new ArrayList<String>();
            String sources1 = prop.getProperty("SELECTED_SOURCES");
            if (sources1 != null && !sources1.equals("")) {
                String[] sources = sources1.split(",");
                for (int i = 0; i < sources.length; i++) {
                    newssources.add(sources[i]);
                }
            }
            if (newssources.size() == 0) {
                Enumeration<NewsProvider> nProviders = NewsProvidersStore.getSharedInstance().getAllProviders();
                while (nProviders.hasMoreElements()) {

                    NewsProvider np = nProviders.nextElement();
                    newssources.add(np.getDescription());
                }
            }
        } catch (Exception e) {

            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static void loadNewsSources() {
        ArrayList tempNewsList = new ArrayList();
            try {
                NewsProvidersStore.getSharedInstance().loadProviders();
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
                Hashtable<String, String> providers = NewsProvidersStore.getSharedInstance().getProviderMap();
                Enumeration<String> providerObjects = providers.keys();
                while (providerObjects.hasMoreElements()) {
                    String id = providerObjects.nextElement();
                    NewsProvider dnp = NewsProvidersStore.getSharedInstance().getProvider(id);
                    if (dnp != null) {
                        tempNewsList.add(dnp.getID());
                    }
                }
                setNewsList(tempNewsList);
            } catch (Exception e) {
            }
        }
    }
