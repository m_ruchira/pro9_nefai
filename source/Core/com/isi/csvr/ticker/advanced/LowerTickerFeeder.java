package com.isi.csvr.ticker.advanced;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWTypes;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.news.News;
import com.isi.csvr.announcement.Announcement;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jul 3, 2008
 * Time: 8:55:01 AM
 * To change this template use File | Settings | File Templates.
 */
public class LowerTickerFeeder implements Runnable {
    public static TWTypes.TickerFilter mode = TWTypes.TickerFilter.MODE_WATCHLIST;

    public static List<CommonTickerObject> tradeEQueue;
    public static List<News> newsQueue;
    public static List<Announcement> announcementQueue;
    public static boolean isExchangeModeSelected = false;
    public static boolean isWatchListModeSelected = false;
    public static boolean isAnnouncementModeSelected = false;
    public static boolean isNewsModeSelected = false;

    private static LowerTickerPanel tradeP;
    private static DynamicArray marketESummaryList;
    public static Symbols[] filter;
    private static boolean eTickVisible = false;
    private static LowerTickerUpdator summaryETickerUpdatorSp;
    private static LowerNewsTickerUpdator newsTickerUpdator;
    private static LowerAnnouncementsTickerUpdator ansTickerUpdator;
    public static boolean isQueueEmpty = true;
    private CommonTickerObject readEObject;
    private News readNewsObject;
    private Announcement readAnnouncementObject;
    private static int summaryTickerDelay;

    public LowerTickerFeeder(LowerTickerPanel tickerEquityPanel) {

        tradeP = tickerEquityPanel;
        tradeEQueue = Collections.synchronizedList(new LinkedList<CommonTickerObject>());
        newsQueue = Collections.synchronizedList(new LinkedList<News>());
        announcementQueue = Collections.synchronizedList(new LinkedList<Announcement>());
        marketESummaryList = new DynamicArray(50);
        filter = new Symbols[0];
//        Application.getInstance().addApplicationListener(this);
    }

    public void run() {
        while (true) {
            if (isVisible() && Settings.isConnected()) { // !isDisconnected) {
                if (isExchangeModeSelected || isWatchListModeSelected) {
                    if (isQueueEmpty) {
                        emptyNewsQueue();
                        emptyAnsQueue();
                        LowerTickerPanel.tradeIObjectList = new DynamicArray();
                        LowerTickerPanel.tradeNewsObjectList = new DynamicArray();
                        LowerTickerPanel.tradeAnnouncementsObjectList = new DynamicArray();
                        isQueueEmpty = false;
                    }
                    try {
                        if (tradeEQueue != null && tradeEQueue.size() > 0) {
                            SymbolHolderLower.isDataCome = true;
                            LowerTickerPanel.INT_DATA = true;
                            readEquityTradeData();
                        } else {
                            SymbolHolderLower.isDataCome = false;
                            LowerTickerPanel.getInstance().redrawTicker();
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    try {
                        Thread.sleep(40);
                    } catch (InterruptedException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    tradeEQueue.clear();
                }
                if (isNewsModeSelected) {
                    if (isQueueEmpty) {
                        emptyexchangeQueue();
                        emptyAnsQueue();
                        LowerTickerPanel.tradeNewsObjectList = new DynamicArray();
                        LowerTickerPanel.tradeAnnouncementsObjectList = new DynamicArray();
                        LowerTickerPanel.tradeIObjectList = new DynamicArray();
                        isQueueEmpty = false;
                    }
                    if (newsQueue != null && newsQueue.size() > 0) {
                        SymbolHolderLower.isDataCome = true;
                        LowerTickerPanel.INT_DATA = true;
                        readNewsData();
                        try {
                            Thread.sleep(40);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        SymbolHolderLower.isDataCome = false;
                        LowerTickerPanel.getInstance().redrawTicker();
                    }
                } else {
                    newsQueue.clear();
                }
                if (isAnnouncementModeSelected) {
                    if (isQueueEmpty) {
                        emptyexchangeQueue();
                        emptyNewsQueue();
                        LowerTickerPanel.tradeAnnouncementsObjectList = new DynamicArray();
                        LowerTickerPanel.tradeNewsObjectList = new DynamicArray();
                        LowerTickerPanel.tradeIObjectList = new DynamicArray();
                        isQueueEmpty = false;
                    }
                    if (announcementQueue != null && announcementQueue.size() > 0) {
                        SymbolHolderLower.isDataCome = true;
                        LowerTickerPanel.INT_DATA = true;
                        readAnnouncementsData();

                        try {
                            Thread.sleep(40);

                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    } else {
                        SymbolHolderLower.isDataCome = false;
                        LowerTickerPanel.getInstance().redrawTicker();
                    }
                } else {
                    announcementQueue.clear();
                }
            }
            if (!isVisible()) {
                clearOnlineData();
            }
            try {
                Thread.sleep(50);

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

    }


    public void readNewsData() {
        int sleepInterval = 10;
        while (isVisible() && (newsQueue.size() > 0) && Settings.isConnected()) {
            if (CommonSettings.IS_INT_DATA_COME) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                if (CommonSettings.IS_INT_DATA_COME) {
                    continue;
                }
            }
            if (newsQueue.size()>0) {
                readNewsObject = newsQueue.remove(0);
            }
            if (readNewsObject != null) {
                for (int i = 0; i < LowerFilterPanel.selectedNewsExchanges.size(); i++) {
                    String newssource = LowerFilterPanel.selectedNewsExchanges.get(i);
                    String provider = readNewsObject.getNewsProvider();
                    String category = readNewsObject.getCategory();
                    if ((provider.contains(newssource)) || (category.contains(newssource))) {
                        tradeP.createNewsTicker(readNewsObject.getNewsProvider(), readNewsObject.getSymbol(), readNewsObject.getExchange(),
                                readNewsObject.getNewsID() + "", readNewsObject.getNewsDate(),
                                readNewsObject.getLanguage(), readNewsObject.getHeadLine() + "", readNewsObject.getBody(), readNewsObject.getKeywords(),
                                readNewsObject.getSource(), readNewsObject.getInstrumentType());
                    }
                }
                tradeP.setSleepInterval(sleepInterval);
            }
            readNewsObject = null;
        }
    }


    public void readAnnouncementsData() {
        int sleepInterval = 10;
        while (isVisible() && (announcementQueue.size() > 0) && Settings.isConnected()) {
            if (CommonSettings.IS_INT_DATA_COME) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                if (CommonSettings.IS_INT_DATA_COME) {
                    continue;
                }
            }
            readAnnouncementObject = announcementQueue.remove(0);
            if (readAnnouncementObject != null) {
                if (LowerFilterPanel.selectedExchanges.contains(readAnnouncementObject.getExchange())) {
                    tradeP.createAnnounementTicker(readAnnouncementObject.getExchange(), readAnnouncementObject.getSymbol(), readAnnouncementObject.getInstrumentType(),
                            readAnnouncementObject.getKey() + "", readAnnouncementObject.getAnnouncementNo(),
                            readAnnouncementObject.getLanguage(), readAnnouncementObject.getHeadLine() + "", readAnnouncementObject.getMessage(), readAnnouncementObject.getUrl());
                }
                tradeP.setSleepInterval(sleepInterval);
            }
            readAnnouncementObject = null;

        }
    }

    public static void clearData() {
        tradeEQueue.clear();
    }

    private void readEquityTradeData() {
        int sleepInterval = 10;
        while (isVisible() && (tradeEQueue.size() > 0) && Settings.isConnected()) {
            if (CommonSettings.IS_INT_DATA_COME) {
                try {
                    Thread.sleep(100);
                } catch (InterruptedException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                if (CommonSettings.IS_INT_DATA_COME) {
                    continue;
                }
            }
            readEObject = tradeEQueue.remove(0);
            if (readEObject != null) {
             Exchange ex = ExchangeStore.getSharedInstance().getExchange(readEObject.getExchange());

                 if ((!ex.isDefault() && ex.getMarketStatus() == Constants.DEFAULT_STATUS) || ((ex.getMarketStatus() == Constants.OPEN)||(ex.getMarketStatus() == Constants.TRADING_AT_LAST)) ||
                          ((LowerFilterPanel.runExOnClose && LowerFilterPanel.mode == TWTypes.TickerFilter.MODE_EXCHANGE) ||
                                    (LowerFilterPanel.runWatchListOnClose && LowerFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST))){
                    tradeP.createNewETradeTicker(readEObject.getExchange(), readEObject.getSymbol(), readEObject.getPrice() + "",
                            readEObject.getQuantity() + "", readEObject.getStatus(),
                            readEObject.getChange(), readEObject.getPercentChange() + "", readEObject.getSplits(), readEObject.getInstrument(), readEObject.getKey());
                }

                if (LowerTicker.CURRENT_TICKER_SPEED == 1) {
                    sleepInterval = 6;
                } else if (LowerTicker.CURRENT_TICKER_SPEED == 2) {
                    sleepInterval = 3;
                } else if (LowerTicker.CURRENT_TICKER_SPEED == 3) {
                    sleepInterval = 2;
                }
                tradeP.setSleepInterval(sleepInterval);
            }
            readEObject = null;
        }
        if (!isVisible()) {
            clearOnlineData();
        }
    }


    public static void clearOnlineData() {
    }


    public static void emptyexchangeQueue() {
        while (!tradeEQueue.isEmpty()) {
            tradeEQueue.remove(0);
        }
    }

    public static void emptyNewsQueue() {
        while (!newsQueue.isEmpty()) {
            newsQueue.remove(0);
        }
    }

    public static void emptyAnsQueue() {
        while (!announcementQueue.isEmpty()) {
            announcementQueue.remove(0);
        }
    }

    public static void setFilter(TWTypes.TickerFilter newMode, Symbols[] filterData) {
        filter = filterData;
        if (filter != null) {
            mode = newMode;
        } else {
            mode = TWTypes.TickerFilter.MODE_WATCHLIST;
        }
    }

    public static Symbols[] getFilter() {
        return filter;
    }

    public static List<CommonTickerObject> getQueue() {
        return tradeEQueue;
    }

    public static int size() {
        return tradeEQueue.size();
    }

    public static void setVisible(boolean bVisible) {
        eTickVisible = bVisible;
        if (bVisible) {
            if (Settings.isShowSummaryTicker()) {
//                activateSummaryTickerUpdator(bVisible);
            }
        }
    }

    public static boolean isVisible() {
        return eTickVisible;
    }


    public static void activateSummaryTickerUpdator(boolean activate) {
        if (activate) {
            if (summaryETickerUpdatorSp == null) {
if(LowerPanelSettings.showSummaryTicker){
                summaryETickerUpdatorSp = new LowerTickerUpdator();
            }
            }
        } else {
            if (summaryETickerUpdatorSp != null) {
                summaryETickerUpdatorSp.deactivate();
                summaryETickerUpdatorSp = null;
            }
        }
    }

    public static void activateNewstickerUpdator(boolean activate) {
        if (activate) {
            if (newsTickerUpdator == null) {
                newsTickerUpdator = new LowerNewsTickerUpdator();
            }
        } else {
            if (newsTickerUpdator != null) {
                newsTickerUpdator.deactivate();
                newsTickerUpdator = null;
            }
        }
    }

    public static void activateAnnouncementtickerUpdator(boolean activate) {
        if (activate) {
            if (ansTickerUpdator == null) {
                ansTickerUpdator = new LowerAnnouncementsTickerUpdator();
            }
        } else {
            if (ansTickerUpdator != null) {
                ansTickerUpdator.deactivate();
                ansTickerUpdator = null;
            }
        }
    }

    public String getCompanyName(String exchange, String symbol, int instrument) {
        return DataStore.getSharedInstance().getShortDescription(exchange, symbol, instrument);
    }


    public static void addData(CommonTickerObject oTickerData) {
        tradeEQueue.add(oTickerData);
    }

    public static void addNewsData(News newsData) {
        newsQueue.add(newsData);
    }

    public static void addAnnouncementData(Announcement ansData) {
        announcementQueue.add(ansData);
    }


    public static int getSummaryTickerDelay() {
        return summaryTickerDelay;
    }

    public static void setSummaryTickerDelay(int summaryTickerDelay) {
        LowerTickerFeeder.summaryTickerDelay = summaryTickerDelay;
    }
}
