package com.isi.csvr.ticker.advanced;

import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.event.*;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.*;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.ticker.custom.TradeFeeder;
import com.isi.csvr.ticker.custom.TickerFrame;
import com.isi.csvr.ticker.custom.Ticker;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.Symbols;

import javax.swing.*;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.PopupMenuEvent;
import java.awt.event.MouseListener;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.ActionEvent;
import java.awt.*;
import java.util.TimeZone;
import java.util.StringTokenizer;
import java.util.Enumeration;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Oct 9, 2008
 * Time: 5:24:11 PM
 * To change this template use File | Settings | File Templates.
 */


public class MiddleTicker extends JPanel implements
        MouseListener, InternalFrameListener, Themeable,
        ActionListener, ExchangeListener, WatchlistListener, PopupMenuListener, ApplicationListener {

    //##########################################################################
    // Variable Decleration for the UpperTicker Object
    //##########################################################################
    public final static int MODE_ALL = 0;
    public final static int MODE_SYMBOL = 2;
    //    public static int CURRENT_TICKER_SPEED = 3;
    public static int CURRENT_TICKER_SPEED = 1;
    private ViewSetting settings;
    private static JPopupMenu popupMenu;
    private static TWMenuItem showHideTitle;
    private TWMenuItem mnuFont;
    private TWMenuItem showHideSymbol;
    private TWMenuItem showHideChange;
    private TWMenuItem mnuCloseTicker;

    public TWMenuItem floatMenu;
    public TWMenu fixMenu;

    public static TWMenuItem mnuTickerFixedTop;
    public static TWMenuItem mnuTickerFixedBottom;

    private TWRadioButtonMenuItem mnuSpeedSlow;
    private TWRadioButtonMenuItem mnuSpeedMedium;
    private TWRadioButtonMenuItem mnuSpeedFast;
    private TWMenu tickerFilter;
    private TWMenuItem mnuSummaryTicker;
    private TWMenuItem mnuClassicTicker;
    private TWMenu mnuSummaryTickerSpeed;
    public static int SLOW_DELAY = 600;
    private ProcessListener parentFrame;
    String previousExchange = "";
    public static String tickerSpeed = "SPEED_SLOW";
    public static boolean IS_DEFAULT = false;
    PanelSetter panelSetter = new PanelSetter();

    CommonSettings globalS = new CommonSettings();
//     Menus m = new Menus(tickerClient);
//##########################################################################
// Method Definitions for the UpperTicker Object
//##########################################################################

    /**
     * Constructs a new instance.
     */
    public MiddleTicker(ViewSetting settings) {
        try {
            this.settings = settings;
            init();
        } catch (Exception e) {
            e.printStackTrace();
        }
        Theme.registerComponent(this);
        popupMenu.addPopupMenuListener(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        WatchListManager.getInstance().addWatchlistListener(this);
        Application.getInstance().addApplicationListener(this);
    }

    public void actions_Mouse_Click() {
        if (!Ticker.settings.isHeaderVisible()) {
            showHideTitle.setText(Language.getString("SHOW_TITLEBAR"));
            showHideTitle.setActionCommand("SHOW_TITLEBAR");
            showHideTitle.setIconFile("showtitle.gif");
        } else {
            showHideTitle.setText(Language.getString("HIDE_TITLEBAR"));
            showHideTitle.setActionCommand("HIDE_TITLEBAR");
            showHideTitle.setIconFile("hidetitle.gif");
        }
        if (Client.getInstance().isTickerFixed()) {

            if (Ticker.tickerFixedToTopPanel) {
                mnuTickerFixedTop.setEnabled(false);
                mnuTickerFixedBottom.setEnabled(true);
            } else {
                mnuTickerFixedTop.setEnabled(true);
                mnuTickerFixedBottom.setEnabled(false);
            }
            popupMenu.getComponent(6).setVisible(true);
            popupMenu.getComponent(7).setVisible(true);

        } else {
            mnuTickerFixedBottom.setEnabled(true);
            mnuTickerFixedTop.setEnabled(true);
            popupMenu.getComponent(6).setVisible(false);
            popupMenu.getComponent(7).setVisible(true);
        }

        if (Client.getInstance().getTickerFrame().isDetached()) {
            popupMenu.getComponent(6).setVisible(false);
            popupMenu.getComponent(7).setVisible(false);
            popupMenu.getComponent(11).setVisible(false);
        } else {
            popupMenu.getComponent(11).setVisible(true);
        }

        if (MiddleTickerFeeder.isExchangeModeSelected || MiddleTickerFeeder.isWatchListModeSelected) {
            popupMenu.getComponent(5).setVisible(true);
            popupMenu.getComponent(4).setVisible(true);  // -- change
            popupMenu.getComponent(3).setVisible(true);  //---symbol
            popupMenu.getComponent(9).setVisible(true);
        } else if (MiddleTickerFeeder.isAnnouncementModeSelected || MiddleTickerFeeder.isNewsModeSelected) {
            popupMenu.getComponent(4).setVisible(false);  // -- change
            popupMenu.getComponent(3).setVisible(false);  //---symbol
            popupMenu.getComponent(9).setVisible(false);
        }

        boolean isTimeNSalesEnable = false;
        if (ExchangeStore.getSharedInstance().defaultCount() > 0) {
            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exchange = (Exchange) exchanges.nextElement();
                if (ExchangeStore.isValidIinformationType(exchange.getSymbol(), Meta.IT_TradeTicker)) {   //
                    if (ExchangeStore.isValidIinformationType(exchange.getSymbol(), Meta.IT_MarketTimeAndSales)) {
                        isTimeNSalesEnable = true;
                        break;
                    }
                }
            }
        }

        /*    if ((Settings.isShowSummaryTicker()) && (ExchangeStore.getSharedInstance().defaultCount() > 0)) {
                    if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TradeTicker)) {   //
                        if (ExchangeStore.isValidIinformationType(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), Meta.IT_SymbolTimeAndSales)) {
                            popupMenu.getComponent(0).setVisible(true);
                        } else {
                            popupMenu.getComponent(0).setVisible(false);
                        }
                    } else {
                        popupMenu.getComponent(0).setVisible(false);
                    }
                }
        */


        if (MiddleTickerFeeder.isExchangeModeSelected || MiddleTickerFeeder.isWatchListModeSelected) {
            if (isTimeNSalesEnable) {
                popupMenu.getComponent(0).setVisible(true);
            } else {
                popupMenu.getComponent(0).setVisible(false);
            }
        } else {
            popupMenu.getComponent(0).setVisible(false);

        }

    }

    public MiddleTicker() {
        //To change body of created methods use File | Settings | File Templates.
    }

    public TWMenuItem getTitleBarMenu() {
        return showHideTitle;
    }

    /**
     * <P>Initialize the Ticker object. Creates the sub objects that constitutes
     * the StockeNet Ticker and allowing them to run as seperate processes.
     */
    public void init() throws Exception {

        popupMenu = new JPopupMenu();
        showHideTitle = new TWMenuItem();
        mnuFont = new TWMenuItem();
        showHideSymbol = new TWMenuItem();
        showHideChange = new TWMenuItem();
        if (Language.isLTR()) {
            MiddlePanelSettings.ENGLISH_VERSION = true;
        } else {
            MiddlePanelSettings.ENGLISH_VERSION = false;
        }

        showHideTitle.addActionListener(this);
        showHideTitle.setIconFile("showtitle.gif");
        if (!Ticker.settings.isHeaderVisible()) {
            showHideTitle.setActionCommand("SHOW_TITLEBAR");
            showHideTitle.setIconFile("showtitle.gif");
        } else {
            showHideTitle.setActionCommand("HIDE_TITLEBAR");
            showHideTitle.setIconFile("hidetitle.gif");
        }

        mnuFont.addActionListener(this);
        mnuFont.setActionCommand("FONT");
        mnuFont.setIconFile("font.gif");
        mnuFont.setText(Language.getString("FONT"));
        showHideSymbol.addActionListener(this);
        showHideSymbol.setIconFile("showsymbol.gif");
        showHideSymbol.setActionCommand("SHOW_SYMBOL");
        showHideSymbol.setText(Language.getString("SHOW_SYMBOL"));
        showHideChange.addActionListener(this);
        showHideChange.setIconFile("showchange.gif");
        showHideChange.setActionCommand("SHOW_PER_CHANGE");
        showHideChange.setText(Language.getString("SHOW_PER_CHANGE"));
        mnuSummaryTicker = new TWMenuItem(Language.getString("SHOW_SUMMARY_TICKER"), "showsmryticker.gif");
        mnuSummaryTicker.addActionListener(this);
        /*    if (MiddlePanelSettings.showSummaryTicker) {
            mnuSummaryTicker.setText(Language.getString("SHOW_TRADE_TICKER"));
            mnuSummaryTicker.setActionCommand("SUMMARY_TRADE");

        } else {
            mnuSummaryTicker.setText(Language.getString("SHOW_SUMMARY_TICKER"));
            mnuSummaryTicker.setActionCommand("SUMMARY");
        }*/
        mnuSummaryTicker.setVisible(true);
        popupMenu.setLightWeightPopupEnabled(false);
        popupMenu.add(mnuSummaryTicker);

        mnuClassicTicker = new TWMenuItem(Language.getString("CUSTOM_TICKER"), "ticker_cla.gif");
        mnuClassicTicker.addActionListener(this);
        mnuClassicTicker.setActionCommand("CUSTOM_TICKER");
        popupMenu.add(mnuClassicTicker);

        // UpperTicker Speed menu
        mnuSummaryTickerSpeed = new TWMenu(Language.getString("TICKER_SPEED"), "tickerspeed.gif");
        ButtonGroup speedGroup = new ButtonGroup();
        mnuSpeedSlow = new TWRadioButtonMenuItem(Language.getString("SPEED_SLOW"));
        mnuSpeedSlow.addActionListener(this);
        mnuSpeedSlow.setActionCommand("SPEED_SLOW");
        speedGroup.add(mnuSpeedSlow);
        mnuSummaryTickerSpeed.add(mnuSpeedSlow);
        mnuSpeedMedium = new TWRadioButtonMenuItem(Language.getString("SPEED_MEDIUM"));
        mnuSpeedMedium.addActionListener(this);
        mnuSpeedMedium.setActionCommand("SPEED_MEDIUM");
        speedGroup.add(mnuSpeedMedium);
        mnuSummaryTickerSpeed.add(mnuSpeedMedium);
        mnuSpeedFast = new TWRadioButtonMenuItem(Language.getString("SPEED_FAST"));
        mnuSpeedFast.addActionListener(this);
        mnuSpeedFast.setActionCommand("SPEED_FAST");
        speedGroup.add(mnuSpeedFast);
        mnuSummaryTickerSpeed.add(mnuSpeedFast);
        applySavedTickerVariables();
        mnuCloseTicker = new TWMenuItem(Language.getString("CLOSE"), "close.gif");
        mnuCloseTicker.addActionListener(this);
        mnuCloseTicker.setActionCommand("CLOSE_TICKER");

        fixMenu = new TWMenu(Language.getString("FIX_TICKER"), "floatticker.gif");
        floatMenu = new TWMenuItem(Language.getString("FLOAT_TICKER"), "floatticker.gif");
        floatMenu.addActionListener(this);
        floatMenu.setText(Language.getString("FLOAT_TICKER"));
        floatMenu.setActionCommand("FLOAT_TICKER");

        ButtonGroup tickerFixingGroup = new ButtonGroup();
        mnuTickerFixedTop = new TWMenuItem(Language.getString("TICKER_FIXED_TOP"));
        mnuTickerFixedTop.addActionListener(this);
        mnuTickerFixedTop.setActionCommand("FIX_TOP");
        fixMenu.add(mnuTickerFixedTop);

        mnuTickerFixedBottom = new TWMenuItem(Language.getString("TICKER_FIXED_BOTTOM"));
        mnuTickerFixedBottom.addActionListener(this);
        mnuTickerFixedBottom.setActionCommand("FIX_BOTTOM");
        fixMenu.add(mnuTickerFixedBottom);

        tickerFilter = new TWMenu(Language.getString("FILTER"), "filter.gif");
        tickerFilter.getPopupMenu().addPopupMenuListener(this);
        popupMenu.add(showHideTitle);
        //popupMenu.add(allwaysOnTop);
        popupMenu.add(showHideSymbol);
        popupMenu.add(showHideChange);
        popupMenu.add(mnuFont);
        popupMenu.add(floatMenu);
        popupMenu.add(fixMenu);
        popupMenu.add(new TWSeparator());
        popupMenu.add(mnuSummaryTickerSpeed);
        popupMenu.add(tickerFilter);
        popupMenu.add(mnuCloseTicker);
        this.setLayout(new BorderLayout());
        this.setBackground(MiddlePanelSettings.TICKER_BACKGROUND); // Color.black);
        SymComponent aSymComponent = new SymComponent();
        this.addComponentListener(aSymComponent);

        UIManager.addPropertyChangeListener(new UISwitchListener(this));
        applyTheme();
        GUISettings.applyOrientation(popupMenu);

    } // End Method

    public void applySavedTickerVariables() {
        String savedSpeed = tickerSpeed;
        if ((savedSpeed != null) && (savedSpeed.equals("SPEED_SLOW") || savedSpeed.equals("SPEED_MEDIUM") || savedSpeed.equals("SPEED_FAST"))) {
            setTickerSpeed(savedSpeed);
        } else {
            setTickerSpeed("SPEED_FAST");
        }
        MiddlePanelSettings.isSymbolShowing = Settings.getBooleanItem("M_TICKER_SHOW_SYMBOL");
        setSymbolCaption();
        MiddlePanelSettings.isChangeMode = Settings.getBooleanItem("M_TICKER_CHANGE_MODE");
        setChangeOption();
        MiddlePanelSettings.showSummaryTicker = Settings.getBooleanItem("M_SUMMARY_TICKER");
        setTradeSummeryOptions();

    }

    private void setTradeSummeryOptions() {
        if (MiddlePanelSettings.showSummaryTicker) {
            mnuSummaryTicker.setText(Language.getString("SHOW_TRADE_TICKER"));
            mnuSummaryTicker.setActionCommand("SUMMARY_TRADE");
        } else {
            mnuSummaryTicker.setText(Language.getString("SHOW_SUMMARY_TICKER"));
            mnuSummaryTicker.setActionCommand("SUMMARY");
            mnuSummaryTickerSpeed.setEnabled(false);
            setTickerSpeed("SPEED_SLOW");
        }
    }

    private void setTickerSpeed(String speed) {

        if (speed.equals("SPEED_SLOW")) {
            tickerSpeed = "SPEED_SLOW";
            mnuSpeedSlow.setSelected(true);
            MiddleTickerFeeder.setSummaryTickerDelay(1200);
            CURRENT_TICKER_SPEED = 1;
            MiddleTickerPanel.PIXEL_INCREMENT = 1;
        } else if (speed.equals("SPEED_MEDIUM")) {
            tickerSpeed = "SPEED_MEDIUM";
            mnuSpeedMedium.setSelected(true);
            MiddleTickerFeeder.setSummaryTickerDelay(450);
            CURRENT_TICKER_SPEED = 2;
            MiddleTickerPanel.PIXEL_INCREMENT = 2;

        } else if (speed.equals("SPEED_FAST")) {
            tickerSpeed = "SPEED_FAST";
            mnuSpeedFast.setSelected(true);
            MiddleTickerFeeder.setSummaryTickerDelay(100);
            CURRENT_TICKER_SPEED = 3;
            MiddleTickerPanel.PIXEL_INCREMENT = 6;
        }
        Settings.saveItem("TICKER_SPEED", speed);
    }


    public void applyTheme() {
        try {
            MiddlePanelSettings.TICKER_BACKGROUND = Theme.getColor("MIDDLE_TICKER_BGCOLOR");
            MiddlePanelSettings.NO_CHANGE_COLOR = Theme.getColor("MIDDLE_TICKER_NO_CHANGE_FGCOLOR");
            MiddlePanelSettings.UP_COLOR = Theme.getColor("MIDDLE_TICKER_UP_FGCOLOR");
            MiddlePanelSettings.DOWN_COLOR = Theme.getColor("MIDDLE_TICKER_DOWN_FGCOLOR");
            MiddlePanelSettings.TICKER_BORDER_COLOR = Theme.getColor("MIDDLE_TICKER_BORDER_COLOR");
            MiddlePanelSettings.SYMBOL_HIGHLIGHT_COLOR = Theme.getColor("MIDDLE_SYMBOL_HIGHLIGHT_COLOR");
            MiddlePanelSettings.NEWS_TITLE_COLOR = Theme.getColor("MIDDLE_TICKER_NEWS_TITLE_COLOR");
            MiddlePanelSettings.NEWS_HIGHLIGHT_COLOR = Theme.getColor("MIDDLE_TICKER_NEWS_HIGHLIGHT_COLOR");
            MiddlePanelSettings.ANNOUNCEMENT_TITLE_COLOR = Theme.getColor("MIDDLE_TICKER_ANNOUNCEMENT_TITLE_COLOR");
            MiddlePanelSettings.ANNOUNCEMENT_HIGHLIGHT_COLOR = Theme.getColor("MIDDLE_TICKER_ANNOUNCEMENT_HIGHLIGHT_COLOR");
            MiddlePanelSettings.FILTER_PANEL_COLOR = Theme.getColor("FILTER_PANEL_BGCOLOR");
            MiddlePanelSettings.FILTER_PANEL_HEADER_COLOR = Theme.getColor("FILTER_PANEL_HEADER_BGCOLOR");

        } catch (Exception e) {
            e.printStackTrace();
            MiddlePanelSettings.TICKER_BACKGROUND = Color.black;
            MiddlePanelSettings.NO_CHANGE_COLOR = Color.white;
            MiddlePanelSettings.FILTER_PANEL_COLOR = Color.WHITE;
        }
        MiddleTickerPanel.getInstance().fontAdjuster(MiddleTickerPanel.getInstance());
        MiddleTickerPanel.getInstance().adjustControls();
        MiddleTickerPanel.getInstance().redrawTicker();
        MiddleTickerPanel.getInstance().adjustTickerControls();
        SharedMethods.updateComponent(MiddleTickerPanel.getInstance());

        SwingUtilities.updateComponentTreeUI(popupMenu);
    }

    public JPopupMenu getMenu() {
        return popupMenu;
    }

//************************************************************************
//     Section that handles the Mouse Events

    //************************************************************************

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isRightMouseButton(e)) {
            if (!settings.isHeaderVisible()) {
                showHideTitle.setText(Language.getString("SHOW_TITLEBAR"));
                showHideTitle.setIconFile("showtitle.gif");
            } else {
                showHideTitle.setText(Language.getString("HIDE_TITLEBAR"));
                showHideTitle.setIconFile("hidetitle.gif");
            }
            if (Client.getInstance().isTickerFixed()) {
                popupMenu.getComponent(6).setVisible(true);
                popupMenu.getComponent(7).setVisible(false);
            } else {
                mnuTickerFixedBottom.setSelected(false);
                mnuTickerFixedTop.setSelected(false);
                popupMenu.getComponent(6).setVisible(false);
                popupMenu.getComponent(7).setVisible(true);
            }

            if (Client.getInstance().getTickerFrame().isDetached()) {
                popupMenu.getComponent(6).setVisible(false);
                popupMenu.getComponent(7).setVisible(false);
                popupMenu.getComponent(11).setVisible(false);
            } else {
                popupMenu.getComponent(11).setVisible(true);
            }
            if (MiddleTickerFeeder.isExchangeModeSelected || MiddleTickerFeeder.isWatchListModeSelected) {
                popupMenu.getComponent(5).setVisible(true);
                popupMenu.getComponent(4).setVisible(true);  // -- change
                popupMenu.getComponent(3).setVisible(true);  //---symbol
            } else if (MiddleTickerFeeder.isAnnouncementModeSelected || MiddleTickerFeeder.isNewsModeSelected) {
                popupMenu.getComponent(5).setVisible(false);  // --speed
                popupMenu.getComponent(4).setVisible(false);  // -- change
                popupMenu.getComponent(3).setVisible(false);  //---symbol
            }
            System.out.println("Detachbility : " + Client.getInstance().getTickerFrame().isDetached());
            if (Client.getInstance().getTickerFrame().isDetached()) {
                popupMenu.getComponent(5).setVisible(false);
            } else {
                popupMenu.getComponent(5).setVisible(true);
            }
            if ((Settings.isShowSummaryTicker()) && (ExchangeStore.getSharedInstance().defaultCount() > 0)) {
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TradeTicker)) {   //
                    if (ExchangeStore.isValidIinformationType(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), Meta.IT_SymbolTimeAndSales)) {
                        popupMenu.getComponent(0).setVisible(true);
                    } else {
                        popupMenu.getComponent(0).setVisible(false);
                    }
                } else {
                    popupMenu.getComponent(0).setVisible(false);
                }
            }

            GUISettings.showPopup(popupMenu, this, e.getX(), e.getY());
        }
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
        IS_DEFAULT = false;
        Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = exchanges.nextElement();
            if (exchange.isDefault()) {
                IS_DEFAULT = true;
            }
        }
        if (!IS_DEFAULT) {

            TWTypes.TickerFilter tickerMode;
            String currentMode = Settings.getTickerFilter();
            if (currentMode.equals(TWTypes.TickerFilter.MODE_WATCHLIST.toString())) {
                System.out.println("current mode is watchlist");
            } else {
                Settings.setTickerFilter(TWTypes.TickerFilter.MODE_WATCHLIST.toString());
                Symbols[] symbols = new Symbols[0];
                ArrayList<Symbols> templist = new ArrayList<Symbols>();
                StringBuilder selectedIDs = new StringBuilder();
                WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
                for (WatchListStore watchlist : watchlists) {
                    if (watchlist.getListType() == WatchListStore.NORMAL_LIST_TABLE_TYPE) {
                        templist.add(watchlist);
                        selectedIDs.append(watchlist.getId());
                        selectedIDs.append(",");
                        watchlist = null;
                    }
                }
                symbols = templist.toArray(symbols);
                TradeFeeder.setFilter(TWTypes.TickerFilter.MODE_WATCHLIST, symbols);
                Settings.setTickerFilterData(selectedIDs.toString());
            }
        }
    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {

    }

    public void Ticker_ComponentResized() {

    }

    public void setHideShowMenuCaption() {
        if (!Ticker.settings.isHeaderVisible()) {
            showHideTitle.setText(Language.getString("SHOW_TITLEBAR"));
            showHideTitle.setActionCommand("SHOW_TITLEBAR");
            showHideTitle.setIconFile("showtitle.gif");
            Ticker.IS_TITLE_SHOW = false;
        } else {
            showHideTitle.setText(Language.getString("HIDE_TITLEBAR"));
            showHideTitle.setActionCommand("HIDE_TITLEBAR");
            showHideTitle.setIconFile("hidetitle.gif");
            Ticker.IS_TITLE_SHOW = true;
        }

    }

    /**
     * This class tracks the Ticker resizing event and do the layouting as necessary
     */
    class SymComponent extends java.awt.event.ComponentAdapter {
    }

// Adjust the control details when the UpperTicker is resized


    public void internalFrameOpened(InternalFrameEvent e) {
    }


    public void internalFrameClosing(InternalFrameEvent e) {

    }

    public void internalFrameClosed(InternalFrameEvent e) {

    }

    public void internalFrameIconified(InternalFrameEvent e) {
    }


    public void internalFrameDeiconified(InternalFrameEvent e) {
    }


    public void internalFrameActivated(InternalFrameEvent e) {

    }

    public void internalFrameDeactivated(InternalFrameEvent e) {
    }


    public void actionPerformed(ActionEvent e) {

        if (e.getActionCommand().equals("CLOSE_TICKER")) {
            globalS.deselectDeravativesTicker(false);
            panelSetter.setDerivativesPanel(false);
            HolderPanel.getInstance().getPreferredSize();
            TickerFrame.setSizeOnFrame();

            Ticker.getInstance().updateUI();
            SharedSettings.Frame_Header_Changed_by = 1;
            Client.getInstance().getTickerFrame().resizeFrame();
            HolderPanel.getInstance().updateUI();

            SharedMethods.updateComponent(MiddleTickerPanel.getInstance());
            Client.getInstance().getDesktop().revalidate();
        }
        if (e.getActionCommand().equals("HIDE_TITLEBAR")) {
            InternalFrame frame = (InternalFrame) Ticker.settings.getParent();
            frame.setTitleVisible(!frame.isTitleVisible(), true);

            HolderPanel.getInstance().getPreferredSize();
            TickerFrame.setSizeOnFrame();
            Ticker.getInstance().updateUI();
            SharedSettings.Frame_Header_Changed_by = 1;
            Client.getInstance().getTickerFrame().resizeFrame();
            setHideShowMenuCaption();
            frame = null;
        } else if (e.getActionCommand().equals("SHOW_TITLEBAR")) {
            SharedSettings.Frame_Header_Changed_by = 0;
            InternalFrame frame = (InternalFrame) Ticker.settings.getParent();
            frame.setTitleVisible(!frame.isTitleVisible(), true);
            setHideShowMenuCaption();
            HolderPanel.getInstance().getPreferredSize();
            TickerFrame.setSizeOnFrame();
            Ticker.getInstance().updateUI();
            Client.getInstance().getTickerFrame().resizeFrame();
            HolderPanel.getInstance().updateUI();

            SharedMethods.updateComponent(MiddleTickerPanel.getInstance());
            Client.getInstance().getDesktop().revalidate();
            frame = null;
        } else if (e.getActionCommand().equals("FONT")) {
            FontChooser oFontChooser = new FontChooser(Client.getInstance().getFrame(), true);
            Font oFont = null;
            oFont = new TWFont(MiddlePanelSettings.FONT_TYPE, MiddlePanelSettings.BIG_FONT_STYLE, MiddlePanelSettings.FONT_BIG);
            oFont = oFontChooser.showDialog(Language.getString("SELECT_FONT"), oFont);
            if (oFont != null) {
                MiddlePanelSettings.BIG_FONT_STYLE = oFont.getStyle();
                MiddlePanelSettings.SMALL_FONT_STYLE = oFont.getStyle();
                MiddlePanelSettings.FONT_TYPE = oFont.getName();
                MiddlePanelSettings.FONT_BIG = oFont.getSize();
                MiddlePanelSettings.FONT_SMALL = MiddlePanelSettings.FONT_BIG - 3;
                MiddleTickerPanel.reloadFont(MiddleTickerPanel.getInstance());
                if (MiddleTickerFeeder.isExchangeModeSelected || MiddleTickerFeeder.isWatchListModeSelected) {
                    MiddleTickerPanel.getInstance().reloadFontforExchange();
                } else if (MiddleTickerFeeder.isNewsModeSelected) {
                    MiddleTickerPanel.getInstance().reloadFontforNews();

                } else if (MiddleTickerFeeder.isAnnouncementModeSelected) {
                    MiddleTickerPanel.getInstance().reloadFontforAnnouncements();

                }
                MiddleTickerPanel.getInstance().adjustControls();
                MiddleTickerPanel.getInstance().redrawTicker();
                HolderPanel.getInstance().getPreferredSize();
                TickerFrame.setSizeOnFrame();

                Ticker.getInstance().updateUI();
                SharedSettings.Frame_Header_Changed_by = 1;
                Client.getInstance().getTickerFrame().resizeFrame();
                HolderPanel.getInstance().updateUI();
                if (DetachableRootPane.IS_DETACHED) {
                    DetachableRootPane.FONT_SELECTED = true;
                    DetachableRootPane.getInstance().fontResized();
                }

                SharedMethods.updateComponent(MiddleTickerPanel.getInstance());
                Client.getInstance().getDesktop().revalidate();
            }
        }
/* else if (e.getActionCommand().equals("SUMMARY")) {
            mnuSummaryTicker.setText(Language.getString("SHOW_TRADE_TICKER"));
            Settings.setShowSummaryTicker(true);

            mnuSummaryTickerSpeed.setEnabled(true);
            mnuSummaryTicker.setActionCommand("SUMMARY_TRADE");
            RequestManager.getSharedInstance().removeRequest(Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());

        }*/
        else if (e.getActionCommand().equals("SPEED_SLOW")) {
            setTickerSpeed(e.getActionCommand());
        } else if (e.getActionCommand().equals("SPEED_MEDIUM")) {
            setTickerSpeed(e.getActionCommand());
        } else if (e.getActionCommand().equals("SPEED_FAST")) {
            setTickerSpeed(e.getActionCommand());
        } else if (e.getActionCommand().equals("SHOW_CHANGE")) {
            setHideShowChangeOption();
        } else if (e.getActionCommand().equals("SHOW_PER_CHANGE")) {
            setHideShowChangeOption();
        } else if (e.getActionCommand().equals("SHOW_SYMBOL")) {
            setHideShowSymbolCaption();
        } else if (e.getActionCommand().equals("SHOW_DESCRIPTION")) {
            setHideShowSymbolCaption();
        } else if (e.getActionCommand().equals("CUSTOM_TICKER")) {
            SharedSettings.showClassicTicker();
        } else if (e.getActionCommand().equals("FIX_TOP")) {
            Ticker.tickerSetToTop = true;
            mnuTickerFixedTop.setSelected(true);
            Client.getInstance().fixTicker();
        } else if (e.getActionCommand().equals("FIX_BOTTOM")) {
            Ticker.tickerSetToTop = true;
            mnuTickerFixedBottom.setSelected(true);
            Client.getInstance().fixTickerToBottom();
        } else if (e.getActionCommand().equals("FLOAT_TICKER")) {
            Client.getInstance().floatTicker();
            mnuTickerFixedBottom.setSelected(false);
            mnuTickerFixedTop.setSelected(false);
        } else if (e.getActionCommand().equals("SUMMARY")) {
            mnuSummaryTicker.setText(Language.getString("SHOW_TRADE_TICKER"));
//            Settings.setShowSummaryTicker(true);
            MiddlePanelSettings.showSummaryTicker = true;

            Settings.saveItem("U_SUMMARY_TICKER", "" + MiddlePanelSettings.showSummaryTicker);
            if (MiddlePanelSettings.MODE == MiddlePanelSettings.exchangelist) {
                MiddleTickerFeeder.isExchangeModeSelected = true;
            } else if (MiddlePanelSettings.MODE == MiddlePanelSettings.watchlist) {
                MiddleTickerFeeder.isWatchListModeSelected = true;

            }
            if (MiddleTickerFeeder.isVisible()) {
                MiddleTickerFeeder.activateSummaryTickerUpdator(true);
            }
            mnuSummaryTickerSpeed.setEnabled(true);
            mnuSummaryTicker.setActionCommand("SUMMARY_TRADE");
//            RequestManager.getSharedInstance().sendRemoveRequest(Meta.MESSAGE_TYPE_COMBINED_TRADE + "|"+ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());
            ArrayList<String> exchangeList = MiddlePanelSettings.getExchangeList();
            for (String anExchangeList : exchangeList) {
                if (ExchangeStore.isValidIinformationType(anExchangeList, Meta.IT_TradeTicker)) {   //
                    if (ExchangeStore.isValidIinformationType(anExchangeList, Meta.IT_MarketTimeAndSales)) {
                        RequestManager.getSharedInstance().removeRequest(Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + anExchangeList);
                    }
                }
            }

        } else if (e.getActionCommand().equals("SUMMARY_TRADE")) {
            mnuSummaryTicker.setText(Language.getString("SHOW_SUMMARY_TICKER"));
//            Settings.setShowSummaryTicker(false);
            MiddlePanelSettings.showSummaryTicker = false;
            Settings.saveItem("U_SUMMARY_TICKER", "" + MiddlePanelSettings.showSummaryTicker);

            if (MiddleTickerFeeder.isVisible()) {
                MiddleTickerFeeder.activateSummaryTickerUpdator(false);
            }

            if (MiddlePanelSettings.MODE == MiddlePanelSettings.exchangelist) {
                MiddleTickerFeeder.isExchangeModeSelected = true;
            } else if (MiddlePanelSettings.MODE == MiddlePanelSettings.watchlist) {
                MiddleTickerFeeder.isWatchListModeSelected = true;

            }
            mnuSummaryTickerSpeed.setEnabled(false);
            setTickerSpeed("SPEED_SLOW");
            mnuSummaryTicker.setActionCommand("SUMMARY");

            String exchangeTNSEnable = "";
            ArrayList<String> exchangeList = new ArrayList<String>();
            Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exchange = exchanges.nextElement();
                if (ExchangeStore.isValidIinformationType(exchange.getSymbol(), Meta.IT_TradeTicker) && exchange.isDefault()) {   //
                    if (ExchangeStore.isValidIinformationType(exchange.getSymbol(), Meta.IT_MarketTimeAndSales)) {
                        String requestID = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + exchange.getSymbol();
                        Client.getInstance().addMarketTradeRequest(requestID, exchange.getSymbol());
                        exchangeList.add(exchange.getDisplayExchange());
                    }
                }
            }
            for (int r = 0; r < exchangeList.size(); r++) {
                if (r == 0) {
                    exchangeTNSEnable = exchangeList.get(r);
                } else {
                    exchangeTNSEnable = exchangeTNSEnable + "," + exchangeList.get(r);
                }
            }
            JOptionPane.showMessageDialog(this, Language.getString("TICKER_TNS_ENABLE_MSG") + exchangeTNSEnable, Language.getString("INFORMATION"), JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void setHideShowSymbolCaption() {
        MiddlePanelSettings.isSymbolShowing ^= true;
//        MiddlePanelSettings.isSymbolShowing = !MiddlePanelSettings.isSymbolShowing;
        setSymbolCaption();
        MiddleTickerPanel.getInstance().reloadFontforExchange();

        MiddleTickerPanel.getInstance().redrawTicker();
        Settings.saveItem("M_TICKER_SHOW_SYMBOL", "" + MiddlePanelSettings.isSymbolShowing);
    }

    public void setExchangePreferences(TWCheckBoxMenuItem selectedItem) {
    }


    public void showFilter() {

    }

    private void setSymbolCaption() {
        if (MiddlePanelSettings.isSymbolShowing) {
            showHideSymbol.setText(Language.getString("SHOW_SYMBOL_DESCRIPTION"));
            showHideSymbol.setActionCommand("SHOW_DESCRIPTION");
        } else {
            showHideSymbol.setText(Language.getString("SHOW_SYMBOL"));
            showHideSymbol.setActionCommand("SHOW_SYMBOL");
        }
    }

    public void setHideShowChangeOption() {
        MiddlePanelSettings.isChangeMode ^= true;
        setChangeOption();

        MiddleTickerPanel.getInstance().reloadFontforExchange();

        MiddleTickerPanel.getInstance().redrawTicker();
        Settings.saveItem("M_TICKER_CHANGE_MODE", "" + MiddlePanelSettings.isChangeMode);
    }

    private void setChangeOption() {
        if (MiddlePanelSettings.isChangeMode) {
            showHideChange.setText(Language.getString("SHOW_PER_CHANGE"));
            showHideChange.setActionCommand("SHOW_PER_CHANGE");
        } else {
            showHideChange.setText(Language.getString("SHOW_CHANGE"));
            showHideChange.setActionCommand("SHOW_CHANGE");
        }
    }

    public void setParent(ProcessListener parent) {
        parentFrame = parent;
    }

    /**
     */
    public String toString() {
        String wsString = "" + MiddlePanelSettings.TICKER_WIDTH + "|" +
                MiddlePanelSettings.STOCK_TICKER_HEIGHT + "|" +
                MiddlePanelSettings.FONT_TYPE + "|" +
                MiddlePanelSettings.FONT_STYLE + "|" +
                MiddlePanelSettings.FONT_BIG;
        return wsString;
    }

    public void applyWorkSpace(String wsString) {
        StringTokenizer st = new StringTokenizer(wsString, "|", false);
        try {
            MiddlePanelSettings.TICKER_WIDTH = Integer.parseInt(st.nextToken());
            MiddlePanelSettings.STOCK_TICKER_HEIGHT = Integer.parseInt(st.nextToken());
            MiddlePanelSettings.FONT_TYPE = st.nextToken();
            MiddlePanelSettings.FONT_STYLE = Integer.parseInt(st.nextToken());
            MiddlePanelSettings.FONT_BIG = Integer.parseInt(st.nextToken());
            MiddlePanelSettings.FONT_MEDIUM = MiddlePanelSettings.FONT_BIG - 3;
            MiddlePanelSettings.FONT_SMALL = MiddlePanelSettings.FONT_BIG - 4;

        } catch (Exception ex) {
            ex.printStackTrace();
        }
        st = null;
    }

    /* Popup menu listeners */

    public void popupMenuCanceled(PopupMenuEvent e) {
    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        tickerFilter.removeAll();
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        tickerFilter.removeAll();
        tickerFilter.getPopupMenu().setLayout(new FlowLayout(FlowLayout.CENTER, 0, 0));
        MiddleFilterPanel filterPanel = new MiddleFilterPanel(tickerFilter);
        tickerFilter.add(filterPanel);
        try {
            if (((DetachableRootPane) Ticker.getInstance().getRootPane()).isDetached()) {
                showHideTitle.setVisible(false);
                mnuClassicTicker.setVisible(false);

            } else {
                showHideTitle.setVisible(true);
                mnuClassicTicker.setVisible(true);

            }
        } catch (Exception e1) {
            showHideTitle.setVisible(true);
        }
        tickerFilter.getPopupMenu().pack();
    }

    /* Exchange listener metods */

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
    }

    public void exchangesLoaded() {
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void symbolAdded(String key, String listID) {
        // check inside FilterPanel class for this method's usage
    }

    public void symbolRemoved(String key, String listID) {
        // check inside FilterPanel class for this method's usage
    }

    public void watchlistAdded(String listID) {
        //buildWatchlistFilterMenu();
    }

    public void watchlistRemoved(String listID) {
        //buildWatchlistFilterMenu();
    }

    public void watchlistRenamed(String listID) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    public void tickersMouseActions(MouseEvent e, MiddleTickerPanel equityPanel) {
        if (SwingUtilities.isRightMouseButton(e)) {
            actions_Mouse_Click();
            GUISettings.showPopup(popupMenu, equityPanel, e.getX(), e.getY());
        }
    }

}
