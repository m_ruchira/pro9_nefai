package com.isi.csvr.ticker.uicomponents;

import com.isi.csvr.ticker.uicomponents.TickerFilterableComponent;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Feb 17, 2009
 * Time: 4:42:16 PM
 * To change this template use File | Settings | File Templates.
 */
public interface TickerFilterableComponentListener {

    public void changeState(TickerFilterableComponent state);

    public void resizeFrame();
}
