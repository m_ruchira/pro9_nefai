package com.isi.csvr.ticker;

import com.isi.csvr.*;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.ticker.advanced.*;
import com.isi.csvr.ticker.custom.TradeFeeder;
import com.isi.csvr.ticker.custom.Ticker;
import com.isi.csvr.ticker.custom.TickerFrame;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;
import java.util.ArrayList;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Nov 4, 2008
 * Time: 11:22:53 AM
 * To change this template use File | Settings | File Templates.
 */
public class SharedSettings {

    public static JPanel advancedTickerPanel = new JPanel();
    public static String tickerCust = "custom";
    public static String tickerAdv = "advance";
    public static String lastSelected = "";

    public static TWCheckBoxMenuItem radioE;
    public static TWCheckBoxMenuItem radioD;
    public static TWSeparator seperator = new TWSeparator();

    public static boolean IS_CUSTOM_SELECTED = false;
    public static boolean IS_ADVANCED_SELECTED = false;

    public static boolean Frame_Header_Changed = false;
    public static boolean Advanced_Ticker_Selector = false;

    public static boolean is_Very_First_Time = false;

    public static boolean is_Upper_Very_First_Time = false;
    public static boolean is_Middle_Very_First_Time = false;
    public static boolean is_Lower_Very_First_Time = false;
    public static int Frame_Header_Changed_by = 1;


    public static int select = 1;
    public static int deselect = 0;

    public static int custom = deselect;
    public static int advanced = deselect;

    public static boolean ISTICKERCHECKED = false;

    public static TWMenu getTickerMenu() {

        TWMenu ticker = new TWMenu(Language.getString("TICKER"), "ticker.gif");
        final TWCheckBoxMenuItem menuItemE = new TWCheckBoxMenuItem(Language.getString("CUSTOM_TICKER"));
        final TWCheckBoxMenuItem menuItemD = new TWCheckBoxMenuItem(Language.getString("ADVANCED_TICKER"));
//        menuItemD.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER));
         menuItemD.setEnabled(false);
        radioE = menuItemE;
        radioD = menuItemD;
        menuItemE.setSelected(false);
        menuItemE.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (menuItemE.isSelected()) {
                    if (!DetachableRootPane.IS_DETACHED) {
                        IS_CUSTOM_SELECTED = true;
                        IS_ADVANCED_SELECTED = false;
                        selectAdvancedTicker(false);
                        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                            CommonSettings.advancedPanelDeSetting();
                            CommonSettings.deselectingAdvancedTickers();
                        }
                        CardLayout cl = (CardLayout) (Ticker.getInstance().getLayout());
                        cl.show(Ticker.getInstance(), Ticker.CUS);
                        Client.getInstance().mnu_ticker_ActionPerformed();

                        selectCustomTicker(true);
                        custom = select;
                        advanced = deselect;
                        lastSelected = tickerCust;
                    } else {
                        menuItemE.setSelected(false);
                    }
                } else if (!menuItemE.isSelected()) {
                    if (!DetachableRootPane.IS_DETACHED) {
                        IS_CUSTOM_SELECTED = false;
                        Client.getInstance().mnu_ticker_ActionPerformed();
                        selectCustomTicker(false);
                        custom = deselect;
                    } else {
                        menuItemE.setSelected(true);
                    }
                }
                if (!DetachableRootPane.IS_DETACHED) {
                    if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                        CommonSettings.enablingAdvancedTickers();
                    }
                }
            }
        });
        ticker.add(menuItemE);

        menuItemD.setSelected(false);
        menuItemD.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (menuItemD.isSelected()) {
                    if (!DetachableRootPane.IS_DETACHED) {
                        IS_ADVANCED_SELECTED = true;
                        IS_CUSTOM_SELECTED = false;
                        selectCustomTicker(false);
                        selectAdvancedTicker(true);
                        if (!Advanced_Ticker_Selector) {
                            CommonSettings.EQ_UP = true;
                            CommonSettings.DERI_UP = true;
                            CommonSettings.INT_UP = true;
                            Advanced_Ticker_Selector = true;
                        }
                        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                            CommonSettings.advancedPanelSetting();
                        }
                        CardLayout cl = (CardLayout) (Ticker.getInstance().getLayout());
                        cl.show(Ticker.getInstance(), Ticker.ADV);

                        /*if (!UpperPanelSettings.DEF_RELOD) {
                            PanelSetter.upperTicker = true;
                            PanelSetter.middleTicker = true;
                            PanelSetter.lowerTicker = true;
                        }*/
                        Client.getInstance().mnu_ticker_ActionPerformed();
                        CommonSettings.deselectingAdvancedTickers();
                        advanced = select;
                        custom = deselect;
                        lastSelected = tickerAdv;
                    } else {
                        menuItemD.setSelected(false);
                    }
                } else if (!menuItemD.isSelected()) {
                    if (!DetachableRootPane.IS_DETACHED) {
                        IS_ADVANCED_SELECTED = false;
                        selectAdvancedTicker(false);
                        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                            CommonSettings.deselectingAdvancedTickers();
                            CommonSettings.advancedPanelDeSetting();
                        }
                        Client.getInstance().mnu_ticker_ActionPerformed();

                        advanced = deselect;
                    } else {
                        menuItemD.setSelected(false);
                    }
                }
                UpperPanelSettings.DEF_RELOD = false;
                MiddlePanelSettings.DEF_RELOD = false;
                LowerPanelSettings.DEF_RELOD = false;
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                    CommonSettings.enablingAdvancedTickers();
                }
            }
        });
        ticker.add(menuItemD);
        CommonSettings.setTicker(ticker);
//        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
//        ticker.add(new TWSeparator());
        ticker.add(seperator);
            ticker.add(CommonSettings.getTickerMenu());
//        }


        return ticker;
    }

    public static void selectCustomTicker(boolean status) {
        radioE.setSelected(status);
        IS_CUSTOM_SELECTED = status;
    }

    public static void selectAdvancedTicker(boolean status) {
        radioD.setSelected(status);
        IS_ADVANCED_SELECTED = status;
    }

    public static void loadingFromWorkSpace(String indtickerWSString) {
        String loadString = indtickerWSString;
        ArrayList<String> tickerData = new ArrayList<String>();
        StringTokenizer fields = new StringTokenizer(loadString, Meta.RS);
        while (fields.hasMoreElements()) {
            tickerData.add(fields.nextToken());
        }

        ISTICKERCHECKED = true;
        int customd = Integer.parseInt(tickerData.get(0));
        int advancedd = Integer.parseInt(tickerData.get(1));


        boolean iscusVisible = Boolean.parseBoolean(tickerData.get(2));
        boolean isVisible = Boolean.parseBoolean(tickerData.get(3));

        if(advancedd ==1 && !ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER) ){
            customd= 1;
            if(isVisible){
                iscusVisible = true;
            }
        }
        boolean isFixed = Boolean.parseBoolean(tickerData.get(4));
        boolean isFixedToTOp = Boolean.parseBoolean(tickerData.get(5));
        is_Very_First_Time = Boolean.parseBoolean(tickerData.get(6));
        is_Upper_Very_First_Time = is_Very_First_Time;
        is_Middle_Very_First_Time = is_Upper_Very_First_Time;
        is_Lower_Very_First_Time = is_Upper_Very_First_Time;
        if (isVisible) {
            if (isFixed) {
                if (isFixedToTOp) {
                    Client.getInstance().fixTicker();
                } else {
                    Client.getInstance().fixTickerToBottom();
                }
            } else {
                Client.getInstance().floatTicker();
            }
        }

        if (customd == 0) {
            if (advancedd == 1) {
//                lastSelected = tickerAdv;
//                IS_ADVANCED_SELECTED = true;
//                IS_CUSTOM_SELECTED = false;
//                selectCustomTicker(false);
//                if (isVisible) {
//                    selectAdvancedTicker(true);
//                }
//                CommonSettings.enablingAdvancedTickers();
//                CardLayout cl = (CardLayout) (Ticker.getInstance().getLayout());
//                cl.show(Ticker.getInstance(), Ticker.ADV);
//                if (isVisible) {
//                    Client.getInstance().mnu_ticker_ActionPerformed();
//                }
//                advanced = select;
//                custom = deselect;

                //------------------- new --
                IS_ADVANCED_SELECTED = true;
                IS_CUSTOM_SELECTED = false;
                selectCustomTicker(false);
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                    selectAdvancedTicker(true);
                }else{
                     IS_CUSTOM_SELECTED = true;
                }
                if (!Advanced_Ticker_Selector) {
                    CommonSettings.EQ_UP = true;
                    CommonSettings.DERI_UP = true;
                    CommonSettings.INT_UP = true;
                    Advanced_Ticker_Selector = true;
                }

                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                    CommonSettings.advancedPanelSetting();
                    CommonSettings.deselectingAdvancedTickers();
                }
                CardLayout cl = (CardLayout) (Ticker.getInstance().getLayout());
                cl.show(Ticker.getInstance(), Ticker.ADV);
                    Client.getInstance().mnu_ticker_ActionPerformed();
                /* HolderPanel.getInstance().getPreferredSize();
                TickerFrame.setSizeOnFrame();
                Ticker.getInstance().updateUI();
                HolderPanel.getInstance().updateUI();
                Client.getInstance().getDesktop().revalidate();*/
                advanced = select;
                custom = deselect;
                lastSelected = tickerAdv;
                UpperPanelSettings.DEF_RELOD = false;
                MiddlePanelSettings.DEF_RELOD = false;
                LowerPanelSettings.DEF_RELOD = false;
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                    CommonSettings.enablingAdvancedTickers();
                }


            } else if (advancedd == 0) {
                IS_ADVANCED_SELECTED = false;
                selectAdvancedTicker(false);
                selectCustomTicker(false);
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                    CommonSettings.deselectingAdvancedTickers();
                    CommonSettings.advancedPanelDeSetting();
                }
                advanced = deselect;
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                    CommonSettings.enablingAdvancedTickers();
                }
            }

        } else if (customd == 1) {
            lastSelected = tickerCust;
            IS_CUSTOM_SELECTED = true;
            IS_ADVANCED_SELECTED = false;
            if (iscusVisible) {
                selectCustomTicker(true);
            }
            selectAdvancedTicker(false);
            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                CommonSettings.advancedPanelDeSetting();
                CommonSettings.deselectingAdvancedTickers();
            }
            CardLayout cl = (CardLayout) (Ticker.getInstance().getLayout());
            cl.show(Ticker.getInstance(), Ticker.CUS);
            if (iscusVisible) {
                TradeFeeder.setVisible(true);
                Client.getInstance().mnu_ticker_ActionPerformed();
            }
            custom = select;
            advanced = deselect;
            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                CommonSettings.enablingAdvancedTickers();
            }
//            SharedSettings.sharedSettings_On_by_button();
        }
    }


    public static void sharedSettings_On_by_button() {
        if (lastSelected.equals(tickerAdv)) {
            IS_ADVANCED_SELECTED = true;
            IS_CUSTOM_SELECTED = false;
            selectCustomTicker(false);
            selectAdvancedTicker(true);

            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                CommonSettings.advancedPanelSetting();
                CommonSettings.deselectingAdvancedTickers();
            }
            CardLayout cl = (CardLayout) (Ticker.getInstance().getLayout());
            cl.show(Ticker.getInstance(), Ticker.ADV);
            Client.getInstance().mnu_ticker_ActionPerformed();
            /* HolderPanel.getInstance().getPreferredSize();
            TickerFrame.setSizeOnFrame();
            Ticker.getInstance().updateUI();
            HolderPanel.getInstance().updateUI();
            Client.getInstance().getDesktop().revalidate();*/
            advanced = select;
            custom = deselect;
            lastSelected = tickerAdv;
        } else {
            IS_CUSTOM_SELECTED = true;
            IS_ADVANCED_SELECTED = false;
            selectAdvancedTicker(false);
            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                CommonSettings.advancedPanelDeSetting();
                CommonSettings.deselectingAdvancedTickers();
            }
            CardLayout cl = (CardLayout) (Ticker.getInstance().getLayout());
            cl.show(Ticker.getInstance(), Ticker.CUS);
            Client.getInstance().mnu_ticker_ActionPerformed();
            selectCustomTicker(true);
            custom = select;
            advanced = deselect;
            lastSelected = tickerCust;
        }

        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
            CommonSettings.enablingAdvancedTickers();
        }
    }

    public static void sharedSettings_Off_by_button() {
        if (lastSelected.equals(tickerAdv)) {
            IS_ADVANCED_SELECTED = false;
            selectAdvancedTicker(false);
            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                CommonSettings.deselectingAdvancedTickers();
                CommonSettings.advancedPanelDeSetting();
            }
            Client.getInstance().mnu_ticker_ActionPerformed();
            advanced = deselect;
        } else {
            IS_CUSTOM_SELECTED = false;
            selectCustomTicker(false);
            custom = deselect;
        }
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
            CommonSettings.enablingAdvancedTickers();
        }
    }

    public static void switchOff_Advanced_TIcker() {
        IS_ADVANCED_SELECTED = false;
        selectAdvancedTicker(false);
        CommonSettings.deselectingAdvancedTickers();
        Client.getInstance().mnu_ticker_ActionPerformed();
        CommonSettings.EQ_UP = true;
        CommonSettings.DERI_UP = true;
        CommonSettings.INT_UP = true;

        advanced = deselect;
    }

    public static void showAdvancedTicker() {
        if (!DetachableRootPane.IS_DETACHED) {
            IS_ADVANCED_SELECTED = true;
            IS_CUSTOM_SELECTED = false;
            selectCustomTicker(false);
            selectAdvancedTicker(true);
            if (!Advanced_Ticker_Selector) {
                CommonSettings.EQ_UP = true;
                CommonSettings.DERI_UP = true;
                CommonSettings.INT_UP = true;
                Advanced_Ticker_Selector = true;
            }
            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                CommonSettings.advancedPanelSetting();
            }
            CardLayout cl = (CardLayout) (Ticker.getInstance().getLayout());
            cl.show(Ticker.getInstance(), Ticker.ADV);

            /*if (!UpperPanelSettings.DEF_RELOD) {
                PanelSetter.upperTicker = true;
                PanelSetter.middleTicker = true;
                PanelSetter.lowerTicker = true;
            }*/

            Client.getInstance().mnu_ticker_ActionPerformed();
            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                CommonSettings.deselectingAdvancedTickers();
            }
            advanced = select;
            custom = deselect;
            lastSelected = tickerAdv;
        } else {
            radioD.setSelected(false);
        }
        UpperPanelSettings.DEF_RELOD = false;
        MiddlePanelSettings.DEF_RELOD = false;
        LowerPanelSettings.DEF_RELOD = false;
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
            CommonSettings.enablingAdvancedTickers();
        }
    }

    public static void showClassicTicker() {
        if (!DetachableRootPane.IS_DETACHED) {
            IS_CUSTOM_SELECTED = true;
            IS_ADVANCED_SELECTED = false;
            selectAdvancedTicker(false);
            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_ADVANCE_TICKER)) {
                CommonSettings.advancedPanelDeSetting();
                CommonSettings.deselectingAdvancedTickers();
            }
            CardLayout cl = (CardLayout) (Ticker.getInstance().getLayout());
            cl.show(Ticker.getInstance(), Ticker.CUS);
            Client.getInstance().mnu_ticker_ActionPerformed();

            selectCustomTicker(true);
            custom = select;
            advanced = deselect;
            lastSelected = tickerCust;
        } else {
            radioE.setSelected(false);
        }
    }
}
