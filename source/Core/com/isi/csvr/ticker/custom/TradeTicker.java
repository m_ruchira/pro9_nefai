/**
 * @author   <b>Bandula Priyadarshana</b>
 * @version 1.0 Copyright (c) 2000 ISI
 */

package com.isi.csvr.ticker.custom;

import com.isi.csvr.shared.*;
import com.isi.csvr.DetachableRootPane;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.Application;
import com.isi.csvr.ticker.GlobalSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.util.TimeZone;

/**
 * <P>Implements the Stock Ticker for displaying the real time stock details
 * in a web page.
 * There are two modes of operation.
 * 1.  Trading mode - Displays the traded details
 * 2.  Message Mode - Displays the message details
 * When the Market is closed
 */
public class TradeTicker extends JPanel implements ApplicationListener {
    public final static int SCROLL_PAUSED = 0;
    public final static int SCROLL_TO_LEFT = 1;
    public final static int SCROLL_TO_RIGHT = 2;

    //##########################################################################
    // Variables Decleration
    //##########################################################################
    public static boolean IS_TICKER_RESIZED;
    public static boolean IS_FRAME_RESIZED;
    private static boolean isNewEntryAdded = false; // Check for added new Entry
    private static TradeFeeder feeder;
    public static DynamicArray tradeObjectList;
    public static final String SYN_FRAME = "RESI_FRAME";
    public static int PIXEL_INCREMENT;
    /*  Holds the details for scrolling direction
     *      0 : Pause Scrolling
     *      1 : Left
     *      2 : Right
     *      3 : Change of direction Left
     *      4 : Change of direction Right
     */
    private static int scrollingDirection = SCROLL_TO_RIGHT;  // Default Scrolling dirn is Right to Left


    public static final String SYN_CHECK = "PAINTANDDRAW";
    public static final String PAINT_CHECK = "PAINTANDRESIZE";
    private BufferedImage textImage;
    private static Graphics2D textG;
    public static TradeTicker self;
    private int sleepingInterval;    // Sets the sleeping time during displaying
    public static int Ticker_Width_Before;
    private boolean isTickerDisposed;    // Is still
    public static boolean NEW_ITEM = false;
    private static Thread feederThread;
    public static boolean FIRST_RESIZED = false;
    public static boolean exchange_Changed_IsDefault = false;
    public static int msgLength = 0;
    //##########################################################################
    // Methods Definitions
    //##########################################################################

    /**
     * Constructor
     */
    public TradeTicker() {
        try {
            if (Ticker.CURRENT_TICKER_SPEED == 1)
//            sleepingInterval = 10;
                sleepingInterval = 6;

            else if (Ticker.CURRENT_TICKER_SPEED == 2)
//            sleepingInterval = 5;
                sleepingInterval = 3;

            else if (Ticker.CURRENT_TICKER_SPEED == 3)
                sleepingInterval = 2;         // Default sleep interval during drawing
            //timeLag             = 5;           // Set the time lag interval
            tradeObjectList = new DynamicArray();

            if (GlobalSettings.ENGLISH_VERSION) {
                scrollingDirection = SCROLL_TO_LEFT;
            } else {
                scrollingDirection = SCROLL_TO_RIGHT;
            }

            initTradeTicker();
            isTickerDisposed = false;

            // DO the feeding for the TradeTicker.
            feeder = new TradeFeeder(this);
            feederThread = new Thread(feeder, "Trade Feeder");
            feederThread.start();
            feederThread.setPriority(Thread.MAX_PRIORITY - 1);
//            feederThread.setPriority(Thread.MIN_PRIORITY);

            Application.getInstance().addApplicationListener(this);
            self = this;
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Error Occured at the Constructor " + e);
        }
    }

    public static TradeTicker getInstance() {
        return self;
    }

    private void initTradeTicker() {
        GlobalSettings.reloadFont(this);
        adjustControls();
    }

    public synchronized void adjustControls() {
        try {
//            g = this.getGraphics();
            textImage = null;
            textImage = new BufferedImage(GlobalSettings.TICKER_WIDTH, GlobalSettings.STOCK_TICKER_HEIGHT, BufferedImage.TYPE_INT_RGB);
            textG = textImage.createGraphics();
            //this.createImage(GlobalSettings.TICKER_WIDTH, GlobalSettings.STOCK_TICKER_HEIGHT);
            //textG = (Graphics2D) textImage.getGraphics();  // Graphics object for DoubleBuffering
            //System.out.println("==>" + (textG == (Graphics2D) textImage.getGraphics()));
            //System.out.println("++>" + (textG.equals((Graphics2D) textImage.getGraphics())));
        } catch (Exception e) {
            System.out.println("Error at TradeTicker " + e);
            e.printStackTrace();
        }
    }

    public void update(Graphics g) {
        paint(g);
    }

    /*public void run() {
        initTradeTicker();
        isTickerDisposed = false;

        // DO the feeding for the TradeTicker.
        feeder = new TradeFeeder(this);
        SharedMethods.printLine("-------------> Feeder start" , true);
        feederThread = new Thread(feeder, "Trade Feeder");
        feederThread.start();
    }*/

    public void paint(Graphics g) {
        // Draw the Offscreen painted Image on to the Applet
        try {
            synchronized (SYN_CHECK) {
//                g.drawImage(textImage, 0, 0, GlobalSettings.TICKER_WIDTH, GlobalSettings.STOCK_TICKER_HEIGHT, this);
                if (TradeTicker.exchange_Changed_IsDefault) {
                    g.drawImage(textImage, 0, 0, GlobalSettings.TICKER_WIDTH, GlobalSettings.STOCK_TICKER_HEIGHT, this);
                } else
                if (!TradeTicker.exchange_Changed_IsDefault && TradeFeeder.getMode().equals(TWTypes.TickerFilter.MODE_ALL)) {
                    g.drawImage(textImage, 0, 0, GlobalSettings.TICKER_WIDTH, GlobalSettings.STOCK_TICKER_HEIGHT, this);
                    g.setFont(GlobalSettings.bigFont);
                    g.setColor(GlobalSettings.TICKER_MSG_COLOR);
                    msgLength = GlobalSettings.symbolFm.stringWidth(Language.getString("TICKER_NO_DATA") + ExchangeStore.getSharedInstance().getSelectedExchange().getDescription());
                    g.drawString(Language.getString("TICKER_NO_DATA") + "" + " " + ExchangeStore.getSharedInstance().getSelectedExchange().getDescription(), (GlobalSettings.TICKER_WIDTH - msgLength) / 2, GlobalSettings.QTY_DISPLAY_HEIGHT);
                } else {
                    g.drawImage(textImage, 0, 0, GlobalSettings.TICKER_WIDTH, GlobalSettings.STOCK_TICKER_HEIGHT, this);
                }
            }

        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Errors occurred while drawing Image " + e);
        }
    }

    /**
     * <P>This will draw the Tiker tape for the TradingTicker...
     */
    private void drawTickers() {  //synchronized
        try {
            int listLength = 0;
            int addedElemLength = 0;
            int counter = 0;

            if (isTickerDisposed) return;

            if (getIsAdded() == true) {
                if (scrollingDirection != SCROLL_PAUSED) {   // Check whether the Ticker is Paused
                    SymbolHolder readTradeObject = null;
                    listLength = tradeObjectList.size() - 1;
                    readTradeObject = (SymbolHolder) tradeObjectList.get(listLength);
                    addedElemLength = readTradeObject.getElementLength();
                    readTradeObject = null;

                    GlobalSettings.CONTINUE_LOOP = true; // Commented on 30-12-02

                    while ((counter < addedElemLength) &&
                            GlobalSettings.CONTINUE_LOOP) {
                        try {
                            if (TradeTicker.exchange_Changed_IsDefault || !TradeFeeder.getMode().equals(TWTypes.TickerFilter.MODE_ALL.toString())) {
                                doScrolling(counter);//, textImage.getGraphics());
                            } else {
                                tradeObjectList.clear();
                            }

                            if (TradeFeeder.getQueue().size() > 0) { // 4 //todo changed
                                if (TradeTicker.exchange_Changed_IsDefault || !TradeFeeder.getMode().equals(TWTypes.TickerFilter.MODE_ALL)) {

                                } else if (tradeObjectList.size() == 0) {
                                    TradeFeeder.getQueue().clear();
                                }

//                                scrollingDirection = SCROLL_TO_LEFT;

                                if (Ticker.CURRENT_TICKER_SPEED == 1) {
//                                    GlobalSettings.UNIT_INCREMENT = 1;
                                    TradeTicker.PIXEL_INCREMENT = 1;
                                } else if (Ticker.CURRENT_TICKER_SPEED == 2) {
//                                       GlobalSettings.UNIT_INCREMENT = 2;
                                    TradeTicker.PIXEL_INCREMENT = 2;

                                } else if (Ticker.CURRENT_TICKER_SPEED == 3) {
//                                       GlobalSettings.UNIT_INCREMENT = 4;
                                    TradeTicker.PIXEL_INCREMENT = 4;

                                }

                            }

                            /*  Force the current Thread to sleep for a given interval
                            *  of time proportional to the number of records present
                            */
                            Thread.sleep(sleepingInterval);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        counter = counter + PIXEL_INCREMENT;
                    } // end while Inner
                    setIsAdded(false);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }// end drawTickers methood

    /**
     * <P>Do the Right to Left or Left to Right scrolling of the ticker tape
     */
    private void doScrolling(int counter) { // synchronized
        int startingXPos = 0;        // Holds the starting X position
        int loopCounter = 0;        // Holds the loop Position
        int widthOfLast = 0;        // Holds the loop Position

        SymbolHolder readTradeObject = null;

        if (scrollingDirection != SCROLL_PAUSED) {  // Ticker is not paused
            //textImage.flush();
            textG.setColor(GlobalSettings.TICKER_BACKGROUND);
            textG.fillRect(0, 0, getWidth(), getHeight());
        } else
            return;

        loopCounter = tradeObjectList.size() - 1;
        while ((loopCounter >= 0) && GlobalSettings.CONTINUE_LOOP) {
            NEW_ITEM = true;
            if (isTickerDisposed) break;

            // Read each Objects from the List
            readTradeObject = (SymbolHolder) tradeObjectList.get(loopCounter);

            switch (readTradeObject.getScrollingDir()) {
                case 1:    // Left Direction
                case 3:    // Scrolling direction is change to left
                    if (readTradeObject != null) {
                        if ((readTradeObject.getDisplayStatus() == 0) || (readTradeObject.getDisplayStatus() == 1)) {
                            if (!IS_TICKER_RESIZED) {

                                if (IS_FRAME_RESIZED) {
                                    startingXPos = readTradeObject.getEarlierXPos() - DetachableRootPane.FRAME_GAP;
                                } else {
                                    startingXPos = readTradeObject.getEarlierXPos();
                                }
                            }
                            if (IS_TICKER_RESIZED) {

                                int tickerGap = Ticker_Width_Before - GlobalSettings.TICKER_WIDTH;
//                                int tickerGap = Ticker_Width_Before- getWidth();

                                startingXPos = readTradeObject.getEarlierXPos() - tickerGap;
                            }
//                            if (ExchangeStore.getSharedInstance().getExchange(readTradeObject.getExchange()).getMarketStatus() == Constants.OPEN) {
                            readTradeObject.draw(startingXPos, scrollingDirection, textG);
//                            }
//                            startingXPos = startingXPos - GlobalSettings.UNIT_INCREMENT; //readTradeObject.getTradeAreaLength();
                            startingXPos = startingXPos - PIXEL_INCREMENT; //readTradeObject.getTradeAreaLength();

                            // Handling the change of direction request
                            widthOfLast = readTradeObject.getElementLength();
                            if (startingXPos == -widthOfLast) {
                                tradeObjectList.remove(loopCounter);

                                /*if (changeOfDirectionRequested) {
                                    changeDirection();
                                    changeOfDirectionRequested = false;
                                    setSleepInterval(sleepingInterval + timeLag);

                                    GlobalSettings.CONTINUE_LOOP = false;
                                    break;
                                } */
                            }
                        } else if (readTradeObject.getDisplayStatus() == 2) {
                            tradeObjectList.remove(loopCounter);
                        }
                    }
                    break;

                case 2:    // Right Direction
                case 4:    // Scrolling direction is change to Right
                    if (readTradeObject != null) {
                        if ((readTradeObject.getDisplayStatus() == 0) || (readTradeObject.getDisplayStatus() == 1)) {

                            /*
                              if(!IS_TICKER_RESIZED)  {

                                if(IS_FRAME_RESIZED){
                                    startingXPos = readTradeObject.getEarlierXPos() + DetachableRootPane.FRAME_GAP;
                                }
                                 else{
                                       startingXPos = readTradeObject.getEarlierXPos();
                                   }
                            }
                            if(IS_TICKER_RESIZED){

                                int tickerGap = Ticker_Width_Before- GlobalSettings.TICKER_WIDTH;
//                                int tickerGap = Ticker_Width_Before- getWidth();
                                int elementGap =readTradeObject.getElementLength();
                                startingXPos = readTradeObject.getEarlierXPos() + tickerGap ;
                            }

                            */
                            startingXPos = readTradeObject.getEarlierXPos(); // ((Integer)startPosList.at(loopCounter)).intValue();
//                            if (ExchangeStore.getSharedInstance().getExchange(readTradeObject.getExchange()).getMarketStatus() == Constants.OPEN) {
                            readTradeObject.draw(startingXPos, scrollingDirection, textG);
//                            }
//                            startingXPos = startingXPos + GlobalSettings.UNIT_INCREMENT; //readTradeObject.getTradeAreaLength();
                            startingXPos = startingXPos + PIXEL_INCREMENT; //readTradeObject.getTradeAreaLength();

                            // Handling the change of direction request
                            if ((startingXPos) == (GlobalSettings.TICKER_WIDTH)) {
                                tradeObjectList.remove(loopCounter);
                            }
                        } else if (readTradeObject.getDisplayStatus() == 2) {
                            tradeObjectList.remove(loopCounter);
                        }
                    }
                    break;
            }
            readTradeObject = null;
            loopCounter--;
        }
        IS_TICKER_RESIZED = false;
        IS_FRAME_RESIZED = false;
        repaint();
        TickerFrame.IS_DATA_COME = false;
        Ticker.IS_FRAME_DATA_COME = false;
    }

    /**
     * <P>Do the painting of the ticker tape when its not scrolling
     */
    private void stayStill(int counter, Graphics g) {// synchronized

        int loopCounter = 0;            // Holds the loop Position
        int startingXPosition = 0;      // Holds the starting X position

        SymbolHolder readTradeObject = null;

        if (scrollingDirection != SCROLL_PAUSED) {  // Ticker is not paused
            g.setColor(GlobalSettings.TICKER_BACKGROUND);
            g.fillRect(0, 0, getWidth(), getHeight());
        }

        loopCounter = tradeObjectList.size() - 1;

        while (loopCounter >= 0) {
            // Read each Objects from the List
            readTradeObject = (SymbolHolder) tradeObjectList.get(loopCounter);
            if (readTradeObject != null) {
                if (scrollingDirection == SCROLL_TO_LEFT) {
                    if (!IS_TICKER_RESIZED) {
                        if (IS_FRAME_RESIZED) {
                            startingXPosition = readTradeObject.getEarlierXPos() - DetachableRootPane.FRAME_GAP;

                        } else {
                            startingXPosition = readTradeObject.getEarlierXPos();
                        }
                    }
                    if (IS_TICKER_RESIZED) {
                        int tickerGap = Ticker_Width_Before - GlobalSettings.TICKER_WIDTH;
//                                int tickerGap = Ticker_Width_Before- getWidth();

                        startingXPosition = readTradeObject.getEarlierXPos() - tickerGap;
                    }
//                    if (ExchangeStore.getSharedInstance().getExchange(readTradeObject.getExchange()).getMarketStatus() == Constants.OPEN) {
                    readTradeObject.draw(startingXPosition, counter, textG);
//                    }

                } else if (scrollingDirection == SCROLL_TO_RIGHT) {

                    /*
                  if(!IS_TICKER_RESIZED)  {
                                   if(IS_FRAME_RESIZED){
                                    startingXPosition = readTradeObject.getEarlierXPos() + DetachableRootPane.FRAME_GAP;

                                }
                                  else{
                                       startingXPosition = readTradeObject.getEarlierXPos();
                                   }
                            }
                            if(IS_TICKER_RESIZED){

                                int tickerGap = Ticker_Width_Before- GlobalSettings.TICKER_WIDTH;

                             startingXPosition = readTradeObject.getEarlierXPos() + tickerGap;

                            }

                    */
                    startingXPosition = readTradeObject.getEarlierXPos();
//                    if (ExchangeStore.getSharedInstance().getExchange(readTradeObject.getExchange()).getMarketStatus() == Constants.OPEN) {
                    readTradeObject.draw(startingXPosition, counter, textG);
//                    }

                }
            }
            readTradeObject = null;
            loopCounter--;
        }

        IS_TICKER_RESIZED = false;
        IS_FRAME_RESIZED = false;
        repaint();
        TickerFrame.IS_DATA_COME = false;
        Ticker.IS_FRAME_DATA_COME = false;
    }

    /**
     * Maintains the Ticker while keeping the scrolling of the ticker.
     * This acts in FIFO basis very much similar to a PIPE.
     * <P>Once a value is displayed and passes a boundary (Right or Left depending
     * on the scrolling direction) then it'll automatically removes from the list.
     * Meanwhile it adds the newly entered value to the list.
     *
     * @param symbol     : New Symbol to be added.
     * @param price      : New price to be added.
     * @param quantity   : New quantity to be added.
     * @param status     : New status to be added.
     * @param instrument
     */
    public void createNewTradeTicker(String exchange, String symbol, String price,
                                     String quantity, int status,
                                     String change, String perChange,
                                     int splits, int instrument) {
        NEW_ITEM = true;
        if (!symbol.equals("")) {
            SymbolHolder lastObject = null;
            SymbolHolder tradeObject = new SymbolHolder();
            tradeObject.setData(symbol, feeder.getCompanyName(exchange, symbol, instrument), price, quantity, change, perChange, splits, status, exchange, instrument);

            // Assign the Scrolling Direction for the newly added element
            tradeObject.setScrollingDir(scrollingDirection);

            // Assign the Starting Position for the added element
            if (scrollingDirection == SCROLL_TO_LEFT) {

                if (tradeObjectList.size() > 0) {
                    lastObject = (SymbolHolder) tradeObjectList.get(tradeObjectList.size() - 1);
                    if (lastObject.getEarlierXPos() < (GlobalSettings.TICKER_WIDTH - lastObject.getElementLength())) {
                        tradeObject.setEarlierXPos(GlobalSettings.TICKER_WIDTH);
                    } else {
                        tradeObject.setEarlierXPos(lastObject.getEarlierXPos() + lastObject.getElementLength());
                    }
                } else {
                    tradeObject.setEarlierXPos(GlobalSettings.TICKER_WIDTH);
                }
            } else if (scrollingDirection == SCROLL_TO_RIGHT) {
                if (tradeObjectList.size() > 0) {
                    lastObject = (SymbolHolder) tradeObjectList.get(tradeObjectList.size() - 1);
                    if (lastObject.getEarlierXPos() > 0) {
                        tradeObject.setEarlierXPos(0 - tradeObject.getElementLength());
                    } else {
                        tradeObject.setEarlierXPos(lastObject.getEarlierXPos() - tradeObject.getElementLength());
                    }
                } else {

                    tradeObject.setEarlierXPos(0 - tradeObject.getElementLength());
                }
            }
            tradeObjectList.add(tradeObject);
            lastObject = null;

        }   // End Checking nulls
        setIsAdded(true);
        drawTickers();
    }// end method

    /**
     * <P>Adds message details to the ticker<BR>
     * Maintains the Ticker while keeping the scrolling.
     * This acts in FIFO basis very much similar to a PIPE. Implemented by using
     * the DynamicArray object.
     * <P>Once a value is displayed and passes a boundary (Right or Left depending
     * on the scrolling direction) then it'll automatically removes from the list.
     * Meanwhile it adds the newly entered value to the list.
     *
     * @param message : New message to be added.
     * @param status  : New status to be added.
     */
    public void createNewScrollTicker(String message, String status) {

        if ((message != null) && (!message.equals(""))) {
            SymbolHolder lastObject = null;
            SymbolHolder tradeObject = new SymbolHolder();
            tradeObject.setData("     " + message, status);

            // Assign the Scrolling Direction for the newly added element
            tradeObject.setScrollingDir(scrollingDirection);

            // Assign the Starting Position for the added element
            if (scrollingDirection == SCROLL_TO_LEFT) {
                if (tradeObjectList.size() > 0) {
                    lastObject = (SymbolHolder) tradeObjectList.get(tradeObjectList.size() - 1);
                    if (lastObject.getEarlierXPos() < (GlobalSettings.TICKER_WIDTH - lastObject.getElementLength()))
                        tradeObject.setEarlierXPos(GlobalSettings.TICKER_WIDTH);
                    else {
                        tradeObject.setEarlierXPos(lastObject.getEarlierXPos() + lastObject.getElementLength());
                    }
                } else
                    tradeObject.setEarlierXPos(GlobalSettings.TICKER_WIDTH);
            } else if (scrollingDirection == SCROLL_TO_RIGHT) {
                if (tradeObjectList.size() > 0) {
                    lastObject = (SymbolHolder) tradeObjectList.get(tradeObjectList.size() - 1);
                    if (lastObject.getEarlierXPos() > 0)
                        tradeObject.setEarlierXPos(0 - tradeObject.getElementLength());
                    else {
                        tradeObject.setEarlierXPos(lastObject.getEarlierXPos() - tradeObject.getElementLength());
                    }
                } else
                    tradeObject.setEarlierXPos(0 - tradeObject.getElementLength());
            }
            tradeObjectList.add(tradeObject);
            lastObject = null;
        }   // End Checking nulls
        setIsAdded(true);
        drawTickers();
    }

    private static void setIsAdded(boolean flag) {
        isNewEntryAdded = flag;
    }// end method

    private static boolean getIsAdded() {
        return isNewEntryAdded;
    }// end method

    public static void setTickerDisposed(boolean isTickerDisposed) {
        if (isTickerDisposed) {
            feederThread.stop();
            feederThread = null;
            feeder = null;
        }
    }

    /**
     * Sets the sleep interval value according to the number of retrieved new records.
     * Normally the speed is proportional to the recordset size. If the record set size is
     * greater then the speed is high. Otherwise scroll in a lesser speed.
     * To implement this it modifies the value of the sleeping time of the create Ticker
     * thread.
     *
     * @param sleepInterval : New value for the sleeping interval
     */
    public void setSleepInterval(int sleepInterval) {
        sleepingInterval = sleepInterval;
    }

    /**
     * Redraw the Ticker if there's no scrolling present.
     */
    public void redrawTicker() {
        try {
            FIRST_RESIZED = true;
            stayStill(0, textG);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTickerTape() {
        try {
            tradeObjectList.clear();
            textG.clearRect(0, 0, getWidth(), getHeight());
            textG.setColor(GlobalSettings.TICKER_BACKGROUND);
            textG.fillRect(0, 0, getWidth(), getHeight());
            repaint();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    public synchronized void reloadFont() {

    public void reloadFont() {
        int xPos = 0; // GlobalSettings.TICKER_WIDTH;
        SymbolHolder tradeObject = null;

        try {
            tradeObject = (SymbolHolder) tradeObjectList.get(tradeObjectList.size() - 1);
            tradeObject.revalidate();
            xPos = tradeObject.getEarlierXPos();
            //if (xPos > (GlobalSettings.TICKER_WIDTH - tradeObject.getElementLength())) {
            if (scrollingDirection == SCROLL_TO_LEFT) {
                xPos = GlobalSettings.TICKER_WIDTH - tradeObject.getElementLength();
                tradeObject.setEarlierXPos(xPos);
            } else {
                xPos = 0; //GlobalSettings.TICKER_WIDTH - tradeObject.getElementLength();
                tradeObject.setEarlierXPos(xPos);
            }

            //for (int i=1; i<tradeObjectList.size(); i++) {
            if (tradeObjectList.size() > 1) {
                for (int i = tradeObjectList.size() - 2; i >= 0; i--) {
                    tradeObject = (SymbolHolder) tradeObjectList.get(i);
                    tradeObject.revalidate();
                    if (scrollingDirection == SCROLL_TO_LEFT) {
                        //xPos -= prevElemWidth;
                        xPos -= tradeObject.getElementLength(); // prevElemWidth;
                        tradeObject.setEarlierXPos(xPos); //GlobalSettings.TICKER_WIDTH);
                    } else if (scrollingDirection == SCROLL_TO_RIGHT) {
                        xPos += tradeObject.getElementLength(); // prevElemWidth;
                        tradeObject.setEarlierXPos(xPos); //GlobalSettings.TICKER_WIDTH);
                    }

                    tradeObject = null;
                }
            }
        } catch (Exception ex) {
            //ex.printStackTrace();
        }
        GlobalSettings.CONTINUE_LOOP = false;
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
        exchange_Changed_IsDefault = true;
        if (exchange != null) {
            if (!exchange.isDefault()) {
                exchange_Changed_IsDefault = false;
                if (TradeFeeder.getMode().equals(TWTypes.TickerFilter.MODE_ALL)) {
                    TradeFeeder.activateSummaryTickerUpdator(false);
                    clearTickerTape();
                }
                /* TradeFeeder.tradeQueue.clear();
                textImage.flush();*/
                //            TradeFeeder.clearOnlineData();
//                clearTickerTape();

                //              msgLength = GlobalSettings.smallFm.stringWidth(Language.getString("TICKER_NO_DATA"));

            } else {
                if (Settings.isShowSummaryTicker()) {
                TradeFeeder.activateSummaryTickerUpdator(true);
                }
                exchange_Changed_IsDefault = true;
                clearTickerTape();


            }
        }
    }
}