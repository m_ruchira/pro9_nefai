package com.isi.csvr.ticker.custom;

import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.ticker.GlobalSettings;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 25, 2005
 * Time: 2:52:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class SummaryTickerUpdator extends Thread {

    private boolean active = true;

    public SummaryTickerUpdator() {

        super("Summary Ticker");
        System.out.println("+++++++++++++++++++++++ttttttttttttttttttt++++++++++++++++++++++++++++");

        start();
    }

    public void deactivate() {
        try {
            active = false;
//            super.interrupt();
            super.stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void run() {
        int tickerStatus;
        boolean played = false;
        while (active) {
            Symbols[] symbols = TradeFeeder.getFilter();
            try {
                if (TradeFeeder.isVisible()) {
                    for (Symbols symbolsObj : symbols) {
                        played = false;
                        String[] keys = symbolsObj.getSymbols();
                        for (String key : keys) {
                            try {
                                Stock stock = DataStore.getSharedInstance().getStockObject(key);
                                if (stock != null && stock.isSymbolEnabled()) {
                                    //if (true) continue;
                                    Exchange exch = ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(key));
                                    if (((!exch.isDefault()) || ((exch.getMarketStatus() == Constants.OPEN)||(exch.getMarketStatus() == Constants.TRADING_AT_LAST))) && stock.isLastTradeUpdated()) {
                                        if ((!exch.hasSubMarkets()) || ((exch.hasSubMarkets()) && (!exch.isUserSubMarketBreakdown()))) {
                                            stock.unsetLastTradeUpdated();
                                            TickerObject oTickerData = new TickerObject();
                                            if (GlobalSettings.isChangeMode) {
                                                if (stock.getChange() == 0)
                                                    tickerStatus = Constants.TICKER_STATUS_NOCHANGE;
                                                else if (stock.getChange() > 0)
                                                    tickerStatus = Constants.TICKER_STATUS_UP;
                                                else
                                                    tickerStatus = Constants.TICKER_STATUS_DOWN;
                                            } else {
                                                if (stock.getPercentChange() == 0)
                                                    tickerStatus = Constants.TICKER_STATUS_NOCHANGE;
                                                else if (stock.getPercentChange() > 0)
                                                    tickerStatus = Constants.TICKER_STATUS_UP;
                                                else
                                                    tickerStatus = Constants.TICKER_STATUS_DOWN;
                                            }
                                            if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                                                oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                        stock.getTradeQuantity(), (float) stock.getChange(),
                                                        (float) stock.getPercentChange(), tickerStatus, 0, stock.getInstrumentType());
                                            } else {
                                                oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                        stock.getVolume(), (float) stock.getChange(),
                                                        (float) stock.getPercentChange(), tickerStatus, 0, stock.getInstrumentType());
                                            }
                                            if (TradeTicker.exchange_Changed_IsDefault || !TradeFeeder.getMode().equals(TWTypes.TickerFilter.MODE_ALL.toString())) {
                                                TradeFeeder.addData(oTickerData);
                                            } else {
                                                TradeFeeder.getQueue().clear();
                                                TradeTicker.tradeObjectList.clear();
                                            }
                                            played = true;
                                            delay(TradeFeeder.getSummaryTickerDelay());
                                        } else {
                                            if ((stock.getMarketID() == null) || (stock.getMarketID().equals(Constants.DEFAULT_MARKET)) || ((stock.getMarketID() != null) &&
                                                    (ExchangeStore.getSharedInstance().getMarket(exch.getSymbol(), stock.getMarketID()).isShowInTicker()))) {
                                                stock.unsetLastTradeUpdated();
                                                TickerObject oTickerData = new TickerObject();
                                                if (GlobalSettings.isChangeMode) {
                                                    if (stock.getChange() == 0)
                                                        tickerStatus = Constants.TICKER_STATUS_NOCHANGE;
                                                    else if (stock.getChange() > 0)
                                                        tickerStatus = Constants.TICKER_STATUS_UP;
                                                    else
                                                        tickerStatus = Constants.TICKER_STATUS_DOWN;
                                                } else {
                                                    if (stock.getPercentChange() == 0)
                                                        tickerStatus = Constants.TICKER_STATUS_NOCHANGE;
                                                    else if (stock.getPercentChange() > 0)
                                                        tickerStatus = Constants.TICKER_STATUS_UP;
                                                    else
                                                        tickerStatus = Constants.TICKER_STATUS_DOWN;
                                                }
                                                //oTickerData.setData(stock.getExchange(), stock.getSymbol(), stock.getLastTradeValue(), Bug ID <#0032> must use symbol code
                                                oTickerData.setData(stock.getExchange(), stock.getSymbolCode(), stock.getLastTradeValue(),
                                                        stock.getTradeQuantity(), (float) stock.getChange(),
                                                        (float) stock.getPercentChange(), tickerStatus, 0, stock.getInstrumentType());
//                                                System.out.println("--------------else add");

                                                if (TradeTicker.exchange_Changed_IsDefault || !TradeFeeder.getMode().equals(TWTypes.TickerFilter.MODE_ALL.toString())) {
                                                    TradeFeeder.addData(oTickerData);
                                                } else {
                                                    TradeFeeder.getQueue().clear();
                                                    TradeTicker.tradeObjectList.clear();
                                                }
                                                played = true;
                                                delay(TradeFeeder.getSummaryTickerDelay());
                                            }
                                        }
                                    }
                                }
                                stock = null;
                            } catch (Exception e) {
                                System.out.println("Error in key " + key); //todo fix the market code issue
                                e.printStackTrace();
                                delay(100);
                            }
                        }
                        if (!played) { // nothing played in this round
                            delay(1000);
                        }
                        keys = null;
                    }
                } else {
                    delay(2000);
                }
            } catch (Exception e) {
//                e.printStackTrace();
                delay(1000);
            }

            delay(TradeFeeder.getSummaryTickerDelay());
        }
    }

    private void delay(long time) {
        try {
            // System.out.println("now selected this---------------------"+time);
            Thread.sleep(time);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
