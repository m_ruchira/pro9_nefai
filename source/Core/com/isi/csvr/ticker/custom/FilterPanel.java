package com.isi.csvr.ticker.custom;

import com.isi.csvr.event.WatchlistListener;
import com.isi.csvr.shared.*;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.watchlist.WatchListStore;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.TWCheckBoxMenuItem;
import com.isi.csvr.TWMenu;
import com.isi.csvr.ticker.advanced.UpperPanelSettings;
import com.isi.util.ColumnLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 2, 2005
 * Time: 9:04:35 AM
 */
public class FilterPanel extends JPanel implements MouseListener, TWTypes, Themeable {
    private TWCustomCheckBox checkNone;
    private TWCustomCheckBox checkWatchlist;
    private TWCustomCheckBox checkClassicView;
    private JLabel okButton;
    private JPanel btnPanel;

    private JPanel watchlistPanel;
    private JPanel classicViewtPanel;
    private TWMenu popupMenu;
    private TWTypes.TickerFilter mode;
    private Symbols[] filter;
    private boolean watchListMode = true;
    public static Color FILTER_PANEL_COLOR = Color.white;

    public FilterPanel(TWMenu popupMenu) {
        this.popupMenu = popupMenu;
        mode = TradeFeeder.getMode();
        filter = TradeFeeder.getFilter();
        createUI();

        this.setBackground(UpperPanelSettings.FILTER_PANEL_HEADER_COLOR);
        Theme.registerComponent(this);

        // initial settings
//        changeState(checkWatchlist, watchlistPanel, false);

        //WatchListManager.getInstance().addWatchlistListener(this);
    }

    private void createUI() {
        ColumnLayout layout = new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE);
        setLayout(layout);
        //setPreferredSize(new Dimension(200,300));
        //setBorder(BorderFactory.createEmptyBorder(2, 10, 2, 10));

        checkNone = new TWCustomCheckBox(Language.getString("PLAY_ALL"), SwingConstants.LEADING, TWCustomCheckBox.CHECK_ONLY);
        checkNone.addMouseListener(this);
        checkNone.setBackground(UpperPanelSettings.FILTER_PANEL_HEADER_COLOR);
        if (Ticker.IS_DEFAULT) {
            add(checkNone);
        }
        checkWatchlist = new TWCustomCheckBox(Language.getString("MY_STOCKS"), SwingConstants.LEADING, TWCustomCheckBox.CHECK_ONLY);
        checkWatchlist.addMouseListener(this);
        checkWatchlist.setBackground(UpperPanelSettings.FILTER_PANEL_HEADER_COLOR);
        add(checkWatchlist);
        watchlistPanel = new JPanel();
        watchlistPanel.setLayout(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));
        watchlistPanel.setBackground(UpperPanelSettings.FILTER_PANEL_HEADER_COLOR);
        watchlistPanel.setForeground(Theme.getColor("LABEL_FGCOLOR"));
        watchlistPanel.setBorder(BorderFactory.createEmptyBorder(2, 20, 2, 20));
        add(watchlistPanel);
        checkClassicView = new TWCustomCheckBox(Language.getString("MIST_VIEW"), SwingConstants.LEADING, TWCustomCheckBox.CHECK_ONLY);
        checkClassicView.addMouseListener(this);
//        add(checkClassicView);
        classicViewtPanel = new JPanel();
        classicViewtPanel.setLayout(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));
        classicViewtPanel.setBorder(BorderFactory.createEmptyBorder(2, 20, 2, 20));
//        add(classicViewtPanel);
        btnPanel = new JPanel();
        okButton = new JLabel("  " + Language.getString("OK") + "  ", SwingUtilities.CENTER);
        okButton.setBorder(BorderFactory.createLineBorder(Theme.getColor("LABEL_FGCOLOR")));
        okButton.addMouseListener(this);
        btnPanel.setBackground(UpperPanelSettings.FILTER_PANEL_HEADER_COLOR);
        btnPanel.add(okButton);
        add(btnPanel);
        GUISettings.applyOrientation(this);

        try {
            WatchListStore[] watchlists = WatchListManager.getInstance().getStores();
            for (WatchListStore watchlist : watchlists) {
                TWCustomCheckBox item = new TWCustomCheckBox(watchlist.getCaption(), SwingConstants.LEADING, TWCustomCheckBox.CHECK_UNCHECK);
                item.setForeground(Theme.getColor("LABEL_FGCOLOR"));
                item.setTag(watchlist.getId());
                if (watchlist.getListType() == WatchListStore.NORMAL_LIST_TABLE_TYPE) {
                    watchlistPanel.add(item);

                } else if (watchlist.getListType() == WatchListStore.CLASSIC_LIST_TABLE_TYPE) {
                    classicViewtPanel.add(item);
                }
                if (filter != null) {
                    for (Symbols symbols : filter) {
                        try {
                            if (((WatchListStore) symbols).getId().equals(watchlist.getId())) {
                                item.setSelected(true);
                                if (watchlist.getListType() == WatchListStore.CLASSIC_LIST_TABLE_TYPE) {
                                    watchListMode = false;
                                }
                            }
                        } catch (Exception e) {
                            // class cast exception may occur. Ignored
                        }
                    }
                }
                GUISettings.applyOrientation(watchlistPanel);
                GUISettings.applyOrientation(classicViewtPanel);
            }
        } catch (Exception e) {
            e.printStackTrace();

        }

        if (mode == TWTypes.TickerFilter.MODE_ALL) {
            checkNone.setSelected(true);
            checkWatchlist.setSelected(false);
            checkClassicView.setSelected(false);
            changeWatchlistState(false);
            changeClassicListState(false);
        } else {
            if (watchListMode) {
                checkClassicView.setSelected(false);
                checkNone.setSelected(false);
                checkWatchlist.setSelected(true);
                changeWatchlistState(true);
                changeClassicListState(false);

            } else {
                checkNone.setSelected(false);
                checkWatchlist.setSelected(false);
                checkClassicView.setSelected(true);
                changeWatchlistState(false);
                changeClassicListState(true);
            }
        }

    }

    public JPanel getWatchlistPanel() {
        return watchlistPanel;
    }

    public void mouseClicked(MouseEvent e) {

        if (e.getSource().equals(checkWatchlist)) {
//            changeState(checkWatchlist, watchlistPanel, true);
            checkNone.setSelected(false);
            checkClassicView.setSelected(false);
            changeWatchlistState(true);
            setWatchlistState(true);
            changeClassicListState(false);
            setClassicListState(false);
            mode = TWTypes.TickerFilter.MODE_WATCHLIST;
            watchListMode = true;
        } else if (e.getSource().equals(checkNone)) {
//            changeState(checkWatchlist, watchlistPanel, false);
            checkWatchlist.setSelected(false);
            checkClassicView.setSelected(false);
            changeWatchlistState(false);
            changeClassicListState(false);
            changeWatchlistState(false);
            setWatchlistState(false);
            setClassicListState(false);
            mode = TWTypes.TickerFilter.MODE_ALL;
            watchListMode = false;
        } else if (e.getSource().equals(okButton)) {
            getParent().setVisible(false);
            popupMenu.getParent().setVisible(false);
            applyFilter();
//        } else{
//            selectPanel((Component)e.getSource());
        } else if (e.getSource().equals(checkClassicView)) {
            checkNone.setSelected(false);
            checkWatchlist.setSelected(false);
            changeClassicListState(true);
            changeWatchlistState(false);
            setWatchlistState(false);
            setClassicListState(true);
            mode = TWTypes.TickerFilter.MODE_WATCHLIST;
            watchListMode = false;
        }
    }

    /*public void setMode(TickerFilter mode) {
        this.mode = mode;
        if (mode == TickerFilter.MODE_ALL) {
            checkNone.setSelected(true);
            changeState(checkWatchlist, watchlistPanel, false);
        }
        if (mode == TickerFilter.MODE_WATCHLIST) {
            checkNone.setSelected(false);
            checkWatchlist.setSelected(true);
            changeState(checkWatchlist, watchlistPanel, true);
        }

        applyFilter();
    }

    public void unsetMode(TickerFilter mode) {
        if (mode == TickerFilter.MODE_ALL) {
            checkNone.setSelected(false);
        }
        if (mode == TickerFilter.MODE_WATCHLIST) {
            checkWatchlist.setSelected(false);
            changeState(checkWatchlist, watchlistPanel, false);
        }

//        applyFilter();
    }*/

    private void applyFilter() {
        if (mode == TickerFilter.MODE_ALL) {
            Exchange exg = ExchangeStore.getSharedInstance().getSelectedExchange();
            Symbols[] symbols;
            if ((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)) {
                symbols = new Symbols[exg.getSubMarketCount()];
                for (int i = 0; i < exg.getSubMarketCount(); i++) {
                    symbols[i] = DataStore.getSharedInstance().getSymbolsObject(exg.getSymbol(), exg.getSubMarkets()[i].getMarketID());
                }
            } else {
                symbols = new Symbols[1];
                symbols[0] = DataStore.getSharedInstance().getSymbolsObject(ExchangeStore.getSelectedExchangeID());
            }
            if (!exg.isDefault()) {
                TradeFeeder.activateSummaryTickerUpdator(false);
                TradeTicker.getInstance().clearTickerTape();
            }
            TradeFeeder.setFilter(mode, symbols);
            Settings.setTickerFilterData("");
        } else if (mode == TickerFilter.MODE_WATCHLIST) {
            applyWatchlistFilter();
            Exchange exg = ExchangeStore.getSharedInstance().getSelectedExchange();
            if (!exg.isDefault()) {
                TradeFeeder.activateSummaryTickerUpdator(true);
                TradeTicker.getInstance().clearTickerTape();
            }

        }
        Settings.setTickerFilter(mode.toString());
    }

    public void applyWatchlistFilter() {

        Component[] items = null;
        if (watchListMode) {
            items = watchlistPanel.getComponents();

        } else {
            items = classicViewtPanel.getComponents();
        }
        Symbols[] symbols = new Symbols[0];
        ArrayList<Symbols> templist = new ArrayList<Symbols>();
        StringBuilder selectedIDs = new StringBuilder();

        for (int i = 0; i < items.length; i++) {
            TWCustomCheckBox item = (TWCustomCheckBox) items[i];
            if (item.isSelected()) {
                WatchListStore watchListStore = WatchListManager.getInstance().getStore(item.getTag());
                templist.add(watchListStore);
                selectedIDs.append(watchListStore.getId());
                selectedIDs.append(",");
                watchListStore = null;
            }
            item = null;
        }

        symbols = templist.toArray(symbols);
        TradeFeeder.setFilter(mode, symbols);
        Settings.setTickerFilterData(selectedIDs.toString());
    }

    private void changeWatchlistState(boolean state) {
        Component[] items = watchlistPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            TWCustomCheckBox item = (TWCustomCheckBox) items[i];
            item.setEnabled(state);
        }
//        if (state == false) {
//            parent.setSelected(false);
//        }
        watchlistPanel.repaint();
    }

    private void changeClassicListState(boolean state) {
        Component[] items = classicViewtPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            TWCustomCheckBox item = (TWCustomCheckBox) items[i];
            item.setEnabled(state);
        }
//        if (state == false) {
//            parent.setSelected(false);
//        }
        classicViewtPanel.repaint();
    }

    private void setClassicListState(boolean state) {
        Component[] items = classicViewtPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            TWCustomCheckBox item = (TWCustomCheckBox) items[i];
            item.setSelected(state);
        }
        classicViewtPanel.repaint();
    }


    private void setWatchlistState(boolean state) {
        Component[] items = watchlistPanel.getComponents();
        for (int i = 0; i < items.length; i++) {
            TWCustomCheckBox item = (TWCustomCheckBox) items[i];
            item.setSelected(state);
        }
//        if (state == false) {
//            parent.setSelected(false);
//        }
        watchlistPanel.repaint();

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    /*public void symbolAdded(String key, String listID) {
        if (mode == TickerFilter.MODE_WATCHLIST) {
            applyFilter();
        }
    }

    public void symbolRemoved(String key, String listID) {
        if (mode == TickerFilter.MODE_WATCHLIST) {
            applyFilter();
        }
    }

    public void watchlistAdded(String listID) {

    }

    public void watchlistRemoved(String listID) {
        if (mode == TickerFilter.MODE_WATCHLIST) {
            applyFilter();
        }
    }*/

    public void applyTheme() {
        try {
            this.FILTER_PANEL_COLOR = Theme.getColor("FILTER_PANEL_HEADER_BGCOLOR");
        } catch (Exception e) {
            FILTER_PANEL_COLOR = Color.BLUE;
        }
    }
}
