/**
 * @author   <b>Bandula Priyadarshana</b>
 * @version 1.0 Copyright (c) 2000 ISI
 */
package com.isi.csvr.ticker;

import com.isi.csvr.Client;
import com.isi.csvr.shared.*;

import javax.swing.*;
import java.awt.*;

/**
 * This is an Abstract class, so it does not mean to be instantiated.
 */
public class GlobalSettings {
    public final static int LEFT_SEPERATOR = 30; //50; //30;
    public final static int RIGHT_SEPERATOR = 30; //10; //5;

    public final static int MENU_IMAGE_HEIGHT = 40; //48;
    public final static int MENU_IMAGE_WIDTH = 20;
  
    public final static int MENU_AREA_HEIGHT = 48;   //50;
    public final static int MENU_AREA_WIDTH = 20; //25;   //100;
    public final static int MENU_SEPERATOR_WIDTH = 0; //5;

    public static int UNIT_INCREMENT = 1;        // No of pixel Positions per a move
    public static boolean ENGLISH_VERSION = true; //true;
    public static boolean CONTINUE_LOOP = false;
    public static boolean isSymbolShowing = false;

    public static boolean isChangeMode = true;

    public static int STOCK_TICKER_HEIGHT = 90; //42; //30; // 300
    public static int SYMBOL_DISPLAY_HEIGHT = 12;
    public static int QTY_DISPLAY_HEIGHT = 24;
    public static int DETAIL_DISPLAY_HEIGHT = 36;

    public static int MAX_MIN_RECT_HEIGHT = 3;
    public static int MAX_MIN_RECT_ALLOWENCE = 5;
    public static int MAX_RECT_DISPLAY_HEIGHT = 1;
    public static int MIN_RECT_DISPLAY_HEIGHT = 86;

    public static int FONT_BIG = 16; //15
    public static int FONT_MEDIUM = 12;  //12
    public static int FONT_SMALL = 11;    //11
    public static int FONT_STYLE = Font.BOLD;
    public static String FONT_TYPE = "Arial";

    public static FontMetrics symbolFm;
    public static FontMetrics smallFm;
    public static Font bigFont;
    public static Font smallFont;

    public final static String IMAGE_PATH = "images/ticker/";
    public final static String MENU_IMAGE_FILE = "icon_buttons";

    public final static String UP = "U";
    public final static String DOWN = "D";
    public final static String NOCHANGE = "N";
    public final static String SMALL = "S";
    public final static String VALNULL = "";

    public static Color UP_COLOR = Color.green.darker();
    public static Color DOWN_COLOR = Color.red;
    public static Color NO_CHANGE_COLOR = Color.black;
    public static Color NULL_COLOR = Color.black;
    public static Color SMALL_TRADE_COLOR = Color.black;
    public static Color TICKER_MIN_PRICE_COLOR = Color.black;
    public static Color TICKER_MAX_PRICE_COLOR = Color.black;
    public static Color TICKER_MSG_COLOR = Color.green.darker();

    public static Color TICKER_BACKGROUND = Color.white;
    public static Color TICKER_SEPERATOR = new Color(51, 102, 153);

//    public static int TICKER_WIDTH = 600;
    public static int TICKER_WIDTH = (int)Client.getInstance().getDesktop().getSize().getWidth();

     public static int TICKER_HEIGHT = 55;

    // This will remove later...
    public static String IMAGE_LIST;

     public static void adjustTickerSize(int width) { // synchronized

//        if (width > 200) {
        if (width > 0) {
            TICKER_WIDTH = width;// - MENU_AREA_WIDTH;
           // System.out.println("if ticker"+TICKER_WIDTH);

        }

         else {
//            TICKER_WIDTH = 1366;// - MENU_AREA_WIDTH;
            TICKER_WIDTH = (int)Client.getInstance().getDesktop().getSize().getWidth();// - MENU_AREA_WIDTH;
        }


    }

    public static void adjustTickerHeight(){
//        STOCK_TICKER_HEIGHT = 67;
    }

    /*public static void reloadFont(JPanel parentPanel) {
        bigFont = new TWFont(FONT_TYPE, FONT_STYLE, FONT_BIG);
        symbolFm = parentPanel.getFontMetrics(new TWFont(FONT_TYPE, FONT_STYLE, FONT_BIG));
        if (Language.isLTR()) {
            smallFont = new TWFont(FONT_TYPE, FONT_STYLE, FONT_SMALL);
            smallFm = parentPanel.getFontMetrics(new TWFont(FONT_TYPE, FONT_STYLE, FONT_SMALL));
        } else {
            smallFont = new TWFont(FONT_TYPE, FONT_STYLE, FONT_SMALL);
            smallFm = parentPanel.getFontMetrics(new TWFont(FONT_TYPE, FONT_STYLE, FONT_SMALL));
        }

        SYMBOL_DISPLAY_HEIGHT = symbolFm.getMaxAscent();
        QTY_DISPLAY_HEIGHT = symbolFm.getMaxAscent() + symbolFm.getMaxDescent() + smallFm.getMaxAscent(); // + 10; // + symbolFm.getHeight();
        DETAIL_DISPLAY_HEIGHT = QTY_DISPLAY_HEIGHT + smallFm.getMaxAscent() + 3;

        STOCK_TICKER_HEIGHT = symbolFm.getHeight() + 2 * smallFm.getHeight() + 3;// + 10; // + smallFm.getAscent() * 2; //3 + 10;
    }*/

    public static void reloadFont(JPanel parentPanel) {
        bigFont = new TWFont(FONT_TYPE, FONT_STYLE, FONT_BIG);
        symbolFm = parentPanel.getFontMetrics(new TWFont(FONT_TYPE, FONT_STYLE, FONT_BIG));
        if (Language.isLTR()) {
            smallFont = new TWFont(FONT_TYPE, FONT_STYLE, FONT_SMALL);
            smallFm = parentPanel.getFontMetrics(new TWFont(FONT_TYPE, FONT_STYLE, FONT_SMALL));
        } else {
            smallFont = new TWFont(FONT_TYPE, FONT_STYLE, FONT_SMALL);
            smallFm = parentPanel.getFontMetrics(new TWFont(FONT_TYPE, FONT_STYLE, FONT_SMALL));
        }

        setMaxMinRectvalues();
        SYMBOL_DISPLAY_HEIGHT = symbolFm.getMaxAscent() + MAX_MIN_RECT_ALLOWENCE ;
        QTY_DISPLAY_HEIGHT = symbolFm.getMaxAscent() + symbolFm.getMaxDescent() + smallFm.getMaxAscent() + MAX_MIN_RECT_ALLOWENCE; // + 10; // + symbolFm.getHeight();
        DETAIL_DISPLAY_HEIGHT = QTY_DISPLAY_HEIGHT + smallFm.getMaxAscent() + 3 + MAX_MIN_RECT_ALLOWENCE;

        STOCK_TICKER_HEIGHT = symbolFm.getHeight() + 2 * smallFm.getHeight() + (2 * MAX_MIN_RECT_ALLOWENCE ) + 3 ;// + 10; // + smallFm.getAscent() * 2; //3 + 10;
        MIN_RECT_DISPLAY_HEIGHT = STOCK_TICKER_HEIGHT - MAX_MIN_RECT_HEIGHT - 1;
    }

    private static void setMaxMinRectvalues(){
        if(FONT_BIG < 14  ){
            MAX_MIN_RECT_ALLOWENCE = 6;
            MAX_MIN_RECT_HEIGHT = 4;
        }else if(FONT_BIG >= 14  && FONT_BIG <= 24){
            MAX_MIN_RECT_ALLOWENCE = 7;
            MAX_MIN_RECT_HEIGHT = 5;
        }else if(FONT_BIG > 24 ){
            MAX_MIN_RECT_ALLOWENCE = 8;
            MAX_MIN_RECT_HEIGHT = 6;
         }
    }

}
