package com.isi.csvr.symbolfilter;

import javax.swing.*;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 18, 2003
 * Time: 2:23:22 PM
 * To change this template use Options | File Templates.
 */
public class FunctionTableModel implements TableModel {

    private FunctionStore store;
    private String [] columns;
    private JTable table;

    public FunctionTableModel(JTable table, FunctionStore store, String [] columns) {
        this.store = store;
        this.table = table;
        this.columns = columns;
    }

    public int getRowCount() {
        return 10;
    }

    public int getColumnCount() {
        return columns.length;
    }

    public String getColumnName(int columnIndex) {
        return columns[columnIndex];
    }

    public Class getColumnClass(int columnIndex) {
        switch (columnIndex) {
            case 5:
                return Number.class;
            default:
                return String.class;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        if (columnIndex == 6) {
            if (rowIndex < (store.size() - 1))
                return true;
            else
                return false;
        } else {
            return false;
        }
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            switch (columnIndex) {
                case 0:
                    return store.getFunction(rowIndex);
                case 1:
                    return store.getFunction(rowIndex);
                case 2:
                    return ((FunctionBuilderComboItem) store.getExchange(rowIndex)).getDescription();
                case 3:
                    return ((FunctionBuilderComboItem) store.getFunction(rowIndex)).getDescription();
                case 4:
                    return ((FunctionBuilderComboItem) store.getEquality(rowIndex)).getDescription();
                case 5:
                    return new Double((String) store.getValue(rowIndex));
                case 6:
                    if (rowIndex < (store.size() - 1))
                        return ((FunctionBuilderComboItem) store.getOperator(rowIndex)).getDescription();
                    else
                        return "";
            }
        } catch (Exception e) {
            return "";
        }
        return "";
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        try {
            switch (columnIndex) {
                case 2:
                    store.setFunction(rowIndex, aValue);
                    break;
                case 3:
                    store.setEquality(rowIndex, aValue);
                    break;
                case 4:
                    store.setValue(rowIndex, aValue);
                    break;
                default:
                    store.setOperator(rowIndex, aValue);
            }
        } catch (Exception e) {

        }
        table.updateUI();
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }
}
