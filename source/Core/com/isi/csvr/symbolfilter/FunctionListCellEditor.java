package com.isi.csvr.symbolfilter;

import javax.swing.*;
import java.awt.*;
import java.util.EventObject;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class FunctionListCellEditor extends DefaultCellEditor {

    private JComboBox operators;
    private int lastColumn = 0;

    public FunctionListCellEditor(JComboBox operators) {
        super(operators);
        this.operators = operators;
        operators.setSelectedIndex(0);
    }

    public Object getCellEditorValue() {
        return (FunctionBuilderComboItem) operators.getSelectedItem();
    }

    public Component getTableCellEditorComponent(JTable table, Object value,
                                                 boolean isSelected, int row, int column) {
        return operators;
    }

    public boolean isCellEditable(EventObject anEvent) {
        return super.isCellEditable(anEvent);
    }


}