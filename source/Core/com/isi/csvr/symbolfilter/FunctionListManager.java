/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 7, 2003
 * Time: 12:19:06 PM
 * To change this template use Options | File Templates.
 */
package com.isi.csvr.symbolfilter;

import com.isi.csvr.DataDisintegrator;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.UpdateNotifier;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.StringTokenizer;

public class FunctionListManager extends Thread implements ViewConstants {
    private static FunctionListManager ourInstance;
    private DynamicArray stores = null;

    public synchronized static FunctionListManager getInstance() {
        if (ourInstance == null) {
            ourInstance = new FunctionListManager();
        }
        return ourInstance;
    }

    private FunctionListManager() {
        super("FunctionListManager");
        stores = new DynamicArray();
        super.setPriority(Thread.NORM_PRIORITY - 3);
    }

    public FunctionListStore createNewStore(String caption) {
        FunctionListStore store = new FunctionListStore("");
        store.setId("" + SharedMethods.getSerialID());
        store.setCaptions(caption);
        stores.add(store);
        return store;
    }


    /**
     * Returns the portfolio store according to the given id
     *
     * @param id
     * @return selected store or null if not found
     */
    public FunctionListStore getStore(String id) {
        FunctionListStore store = null;
        for (int i = 0; i < stores.size(); i++) {
            store = (FunctionListStore) stores.get(i);
            if (store.getId().equals(id)) {
                return store;
            }
        }
        return null;
    }

    /**
     * Remove the given store from the list
     *
     * @param store
     */
    public void removeStore(FunctionListStore store) {
        stores.remove(store);
        stores.trimToSize();
    }


    /**
     * Returns an array of portfolio stores
     *
     * @return array of portfolio stores
     */
    public Object[] getStores() {
        return stores.getList().toArray();
    }

    /**
     * Add the given store to the list
     *
     * @param store
     */
    public void setStore(FunctionListStore store) {
        stores.add(store);
    }

    /**
     * Create a new store using saved settings
     *
     * @param record
     */
    public void createStore(String record) {

        StringTokenizer fields = new StringTokenizer(record, VC_RECORD_DELIMETER);
        DataDisintegrator pair = new DataDisintegrator();
        pair.setSeperator(VC_FIELD_DELIMETER);
        FunctionListStore store = new FunctionListStore("");
        //store.setId(Settings.get);

        String value;
        while (fields.hasMoreTokens()) {
            try {
                pair.setData(fields.nextToken());
                value = pair.getData();

                switch (SharedMethods.intValue(pair.getTag(), 0)) {
                    case VC_ID:
                        store.setId(value);
                        break;

                    case VC_CAPTIONS:
                        store.setCaptions(value);
                        break;

                    case VC_DEPTH_CALC_VALUE:
                        store.setFunction(value);
                        break;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        stores.add(store);
        store = null;
        fields = null;
        pair = null;
    }


    public void save() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath()+"datastore/functionlist.mdf");
            FunctionListStore store = null;
            for (int i = 0; i < stores.size(); i++) {
                store = (FunctionListStore) stores.get(i);
                out.write(store.toString().getBytes());
                out.write("\r\n".getBytes());
            }
            out.close();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
    }

    public void load() {
        String data;
        try {
            DataInputStream in = new DataInputStream(new FileInputStream(Settings.getAbsolutepath()+"datastore/functionlist.mdf"));
            while (true) {
                data = in.readLine();
                if ((data == null) || (data.trim().equals(""))) break;
                createStore(data);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        start();
    }

    public void run() {
        FunctionListStore list;
        while (true) {
            if (Settings.isConnected()) {
                for (int i = 0; i < stores.size(); i++) {
                    try {
                        list = (FunctionListStore) stores.get(i);
                        if ((list.getParent() != null) && (list.getParent().isVisible())) {
                            list.reFilter();
                            list = null;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                UpdateNotifier.setShapshotUpdated();
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }
        }
    }

}

