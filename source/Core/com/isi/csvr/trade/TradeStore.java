// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.trade;

/**
 * Data store for trade objects
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.datastore.DataStoreInterface;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.*;
import com.isi.csvr.shared.*;
import com.isi.csvr.util.TWList;

import java.util.Enumeration;
import java.util.Hashtable;
import java.util.TimeZone;
import java.util.Collections;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;
import java.io.ObjectInputStream;

public class TradeStore implements DataStoreInterface, ConnectionListener, ExchangeListener, ApplicationListener {
    private static final String SYNC = "TradeStore";

    private static Hashtable<String, FilterableTradeStore> allTradesTable;
    private static TradeSerialComparator serialComparator;
    public static TradeStore self = null;
    private static CompanyTradeStore companyTradeStore;
    private static boolean isinitCalled = false;

    /**
     * Constructor
     */
    public TradeStore() {
        allTradesTable = new Hashtable<String, FilterableTradeStore>();
        self = this;
        companyTradeStore = new CompanyTradeStore();
        serialComparator = new TradeSerialComparator();
        //reset();
//        if(Settings.isOfflineMode()){
        //removed if condition on 12nth june 2009
        loadData();
//        }
//        loadData();
        Application.getInstance().addApplicationListener(this);
        ConnectionNotifier.getInstance().addConnectionListener(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
    }

    public static FilterableTradeStore getAllTradesStore(String exchange) {
        FilterableTradeStore store = allTradesTable.get(exchange);
        if (store == null) {
            store = new FilterableTradeStore(FilterableTradeStore.TYPE_ALL, "ALL", TWList.LIST);
//            store = new FilterableTradeStore(FilterableTradeStore.TYPE_ALL, exchange, TWList.LIST);
            allTradesTable.put(exchange, store);
        }
        return store;
    }


    public static synchronized TradeStore getSharedInstance() {
        return self;
    }

    public static boolean isInitCalled(){
        return isinitCalled;
    }

    public static void init() {
        isinitCalled = true;
        synchronized (SYNC) {
            //todo -------
//            serialComparator = new TradeSerialComparator();

            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exchange = (Exchange) exchanges.nextElement();
                if (exchange.isDefault()) {
                    Trade trade = null;
                    FilterableTradeStore store = getAllTradesStore(exchange.getSymbol());
                    if (store != null) {
                        for (int i = 0; i < store.size(); i++) {
                            trade = store.getTrade(i);
                            if (!(trade.isIndexType())) {
                                companyTradeStore.updateDataStore(exchange.getSymbol(), SharedMethods.getKey(trade.getExchange() ,trade.getSymbol(), trade.getInstrument()), trade);
                                //TradeSummaryStore.getSharedInstance().addTrade(trade);
                            }
                            trade = null;
                        }
                    }
                    store = null;
                }
                exchange = null;
            }
            exchanges = null;
        }
    }

    public void clear(String symbol) {

    }

    public synchronized void clear() {
    }

    public void initialize(String exchange) {
        try {
            getAllTradesStore(exchange).clear(true);
            companyTradeStore.initialize(exchange);
        } catch (Exception e) {

        }
    }

    // Modified By Bandula 01 10 01
    public synchronized static boolean addTrade(Trade trade, boolean isBacklogData) {

        boolean result = false;
//        Trade trade;
        String key;
        try {
            //trade = (Trade) otrade.clone(); 5 May 2005 Uditha
            key = SharedMethods.getKey(trade.getExchange(),trade.getSymbol(), trade.getInstrument());

            if (key != null) {
                if (isBacklogData) {
                    companyTradeStore.addZippedReferance(key, trade);//(Trade)otrade.clone()); 5 May 2005 Uditha
                }
                synchronized (SYNC) {
                    companyTradeStore.addTrade(key, trade);
//                    if (companyTradeStore.getCompanyTradesStore(key).unfilteredSize() > Constants.COMPANY_TIME_AND_SALES){
//                        companyTradeStore.getCompanyTradesStore(key).removeFirstTrade();
//                    }
                    if (!trade.isIndexType()) {
                        boolean sucessfullyAdded;
                        sucessfullyAdded = getAllTradesStore(trade.getExchange()).insert(serialComparator, trade);

                        if (getAllTradesStore(trade.getExchange()).unfilteredSize() > TWControl.ALL_TIME_AND_SALES) {
                            allTradesTable.get(trade.getExchange()).removeFirstTrade();
                            companyTradeStore.getCompanyTradesStore(key).removeFirstTrade();
                        }
                        result = sucessfullyAdded;
                    }
                } //sync
            }
        } catch (Exception e) {
            e.printStackTrace();
            result = false;
        } finally {
            trade = null;
            key = null;
        }
        return result;
    }

    public synchronized long getLastValidTradeSequence(String exchange) {
        TWList<Trade> store = getAllTradesStore(exchange).getStore();
        if (store.size() > 0) {
            if ((store.get(0)).getSequence() != 1) {
                return 0;
            } else {
                long lastSequence = 0;
                for (Trade trade : store) {
                    if ((trade.getSequence() - lastSequence) == 1) {
                        lastSequence = trade.getSequence();
                    } else {
                        break;
                    }
                }
                return lastSequence;
            }
        } else {
            return 0;
        }
    }

    public long getLastSequence(String exchange) {
        try {
        return getAllTradesStore(exchange).getLastTrade().getSequence();
        } catch (Exception e) {
            return 0;
        }
    }

    //public synchronized static void reloadFilteredStore(FilterableTradeStore store) {

    /*synchronized (SYNC) {
        int size = g_oTimeNSales.size();

        store.clear();
        for (int i = 0; i < size; i++) {
            store.insert(serialComparator, g_oTimeNSales.getTrade(i));
        }
        store = null;
    }*/
    //}

    /**
     * Save the hash table as an object to the disk
     */
    public static void saveData() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath()+"datastore/TimeAndSales.mdf");
            ObjectOutputStream oObjOut = new ObjectOutputStream(out);
            oObjOut.writeObject(allTradesTable);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

    }
   public static void clearPreviouslySaved(){
       allTradesTable.clear();
        try {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath()+"datastore/TimeAndSales.mdf");
            ObjectOutputStream oObjOut = new ObjectOutputStream(out);
            oObjOut.writeObject(allTradesTable);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
   }
    /**
     * Loads the saved hash table from the disk as an object
     */
    public void loadData() {
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath()+"datastore/TimeAndSales.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            allTradesTable = (Hashtable<String, FilterableTradeStore>) oObjIn.readObject();
            oIn.close();
        } catch (Exception e) {
            e.printStackTrace();
            allTradesTable = new Hashtable<String, FilterableTradeStore>();

        }
//        updateTradeSummaryStore();
    }

    public static FilterableTradeStore getMarketTimeNSales(String exchange) {
        return getAllTradesStore(exchange);
    }

    /*private void updateTradeSummaryStore() {
        synchronized (SYNC) {
            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exchange = (Exchange) exchanges.nextElement();
                if (exchange.isDefault()) {
                    FilterableTradeStore store = getAllTradesStore(exchange.getSymbol());
                    if (store != null){
                        for (int i = 0; i < store.size(); i++) {
                            trade = store.getTrade(i);
                            if (!(trade.isIndexType())) {
                                TradeSummaryStore.getSharedInstance().addTrade(trade);
                            }
                            trade = null;
                        }
                    }
                }
                exchange = null;
            }
            exchanges = null;
        }
    }*/

    public void twConnected() {
        //clear();
    }

    public void twDisconnected() {

    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {
        companyTradeStore.getExchangeTable(exchange.getSymbol()).clear();
        getAllTradesStore(exchange.getSymbol()).clear(true);
        System.out.println("111111");
    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange,long newDate, long oldDate) {
        initialize(exchange.getSymbol());
    }

    public void exchangeRemoved(Exchange exchange) {
        companyTradeStore.getExchangeTable(exchange.getSymbol()).clear();
        getAllTradesStore(exchange.getSymbol()).clear(true);
    }

    public void exchangesAdded(boolean offlineMode) {

    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
//        loadData();
    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath()+"datastore/TimeAndSales.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            Hashtable<String, FilterableTradeStore> allTrades = (Hashtable<String, FilterableTradeStore>) oObjIn.readObject();
            Enumeration<String> sSymbols = allTrades.keys();
            String symbol;
            FilterableTradeStore data;
            while(sSymbols.hasMoreElements()){
                symbol = sSymbols.nextElement();
                if(!allTradesTable.containsKey(symbol)){
                    allTradesTable.put(symbol,allTrades.get(symbol));
                } else {
                    data = allTrades.get(symbol);
                    Collections.copy(allTradesTable.get(symbol).getUnfilteredStore(),data.getUnfilteredStore());
                    allTradesTable.get(symbol).clearAndRefilter();
                    allTradesTable.get(symbol).setEndDate(data.getEndDate());
                    allTradesTable.get(symbol).setEndPrice(data.getEndPrice());
                    allTradesTable.get(symbol).setFilteredByDate(data.isFilteredByDate());
                    allTradesTable.get(symbol).setFilteredByPrice(data.isFilteredByPrice());
                    allTradesTable.get(symbol).setFilteredBySymbol(data.isFilteredBySymbol());
                    allTradesTable.get(symbol).setStartDate(data.getStartDate());
                    allTradesTable.get(symbol).setStartPrice(data.getStartPrice());
                    allTradesTable.get(symbol).setSymbol(data.getSymbol());
                    allTradesTable.get(symbol).setSymbols(data.getSymbols());
                    allTradesTable.get(symbol).setEndVolume(data.getEndVolume());
                    allTradesTable.get(symbol).setEndTurnover(data.getEndTurnover());
                    allTradesTable.get(symbol).setFilteredByVolume(data.isFilteredByVolume());
                    allTradesTable.get(symbol).setFilteredByTurnover(data.isFilteredByTurnover());
                    allTradesTable.get(symbol).setStartVolume(data.getStartVolume());
                    allTradesTable.get(symbol).setStartTurnover(data.getStartTurnover());
                    // -- Market
                    allTradesTable.get(symbol).setStartPriceMarket(data.getStartPriceMarket());
                    allTradesTable.get(symbol).setStartVolumeMarket(data.getStartVolumeMarket());
                    allTradesTable.get(symbol).setStartTurnoverMarket(data.getStartTurnoverMarket());
                    allTradesTable.get(symbol).setEndPriceMarket(data.getEndPriceMarket());
                    allTradesTable.get(symbol).setEndVolumeMarket(data.getEndVolumeMarket());
                    allTradesTable.get(symbol).setEndTurnoverMarket(data.getEndTurnoverMarket());
                    allTradesTable.get(symbol).setFilteredByPriceMarket(data.isFilteredByPriceMarket());
                    allTradesTable.get(symbol).setFilteredByVolumeMarket(data.isFilteredByVolumeMarket());
                    allTradesTable.get(symbol).setFilteredByTurnoverMarket(data.isFilteredByTurnoverMarket());


                }
            }
            oIn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectedExchangeChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}

