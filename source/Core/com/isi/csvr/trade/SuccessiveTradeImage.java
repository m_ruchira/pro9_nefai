package com.isi.csvr.trade;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.shared.Language;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Apr 17, 2009
 * Time: 1:19:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class SuccessiveTradeImage extends ImageIcon {

    private int height = 10;
    private int width = 8;
    private int imageWidth = 5;
    private int CLEARENCE = 4;

    public SuccessiveTradeImage() {
        imageWidth = width - (CLEARENCE * 2);
    }

    public void setWidth(int width) {
        this.width = width;
        imageWidth = width - (CLEARENCE * 2);
    }

    public void setHeight(int height) {
        this.height = height;
        CLEARENCE = height / 4;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        g.setColor(Theme.TNS_SUCCESSIVE_TRDES_COLOR);
        if (Language.isLTR()) {
            g.fillRect(0, 2, width - 4, height - 4);
        } else {
            g.fillRect(x + 4, y + 2, width - 4, height - 4);
        }
    }

    public int getIconHeight() {
        return height;
    }

    public int getIconWidth() {
        if (Language.isLTR()) {
            return width - 2;
        } else {
            return 10;
        }
    }

}
