package com.isi.csvr.trade;

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class TradeSummaryModel extends CommonTable
        implements TableModel, CommonTableInterface {
    private ArrayList dataArray;
    private static final DoubleTransferObject doubleTransferObject = new DoubleTransferObject();


    public TradeSummaryModel() {
    }

    public void setDataStore(ArrayList dataArray) {
        this.dataArray = dataArray;
    }

    /* --- Table Modal's metods start from here --- */
    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        if (dataArray != null)
            return dataArray.size();
        else
            return 0;
    }

    public void setSymbol(String symbol) {

    }

    public Object getValueAt(int iRow, int iCol) {
        SummaryTrade oTrade = (SummaryTrade) dataArray.get(dataArray.size() - iRow - 1);

        switch (iCol) {
            case 0:
                return Double.valueOf("" + oTrade.getTradePrice());
            case 1:
                return Double.valueOf("" + oTrade.getTradeQuantity());
            case 2:
                return Double.valueOf("" + oTrade.getNoOfTrades());
            case 3:
                return Double.valueOf("" + oTrade.getCashInnoOfTrades());
            case 4:
                return Double.valueOf("" + oTrade.getCashInVloume());
            case 5:
                return oTrade.getCashInTurnover();
            case 6:
                return Double.valueOf("" + oTrade.getCashoutnoOfTrades());
            case 7:
                return Double.valueOf("" + oTrade.getCashoutVolume());
            case 8:
                return oTrade.getCashoutTurnover();
            case 9:
                return oTrade.getNetValue();
            case 10:
                return oTrade.getPercentage();

            default:
                return "";
        }
    }

    public String getColumnName(int iCol) {
        if (isLTR()) {
            return super.getViewSettings().getColumnHeadings()[iCol];
        } else {
            return super.getViewSettings().getColumnHeadings()[super.getViewSettings().getColumnHeadings().length - 1 - iCol];
        }
    }

    public Class getColumnClass(int iCol) {
        switch (iCol) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 4:
            case 5:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
                return Double.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public static int compare(Object o1, Object o2, int sortColumn, int sortOrder) {
        SummaryTrade rec1 = (SummaryTrade) o1;
        SummaryTrade rec2 = (SummaryTrade) o2;
        switch (sortColumn) {
            case 1:
                return compareValues(rec1.getTradeQuantity(), rec2.getTradeQuantity(), sortOrder);
            case 3:
                return compareValues(rec1.getCashInnoOfTrades(), rec2.getCashInnoOfTrades(), sortOrder);
            case 4:
                return compareValues(rec1.getCashInVloume(), rec2.getCashInVloume(), sortOrder);
            case 6:
                return compareValues(rec1.getCashoutnoOfTrades(), rec2.getCashoutnoOfTrades(), sortOrder);
            case 7:
                return compareValues(rec1.getCashoutVolume(), rec2.getCashoutVolume(), sortOrder);
            case 10:
                return compareValues(rec1.getPercentage(), rec2.getPercentage(), sortOrder);
            default:
                return 0;
        }
    }

    private static int compareValues(double val1, double val2, int sortOrder) {
        if (Double.isNaN(val1) && Double.isNaN(val2)) {
            return 0;
        } else if (Double.isNaN(val1)) {
            return 1;
        } else if (Double.isNaN(val2)) {
            return -1;
        }
        if (val1 > val2)
            return sortOrder;
        else if (val1 == val2)
            return 0;
        else
            return sortOrder * -1;
    }
}