// Copyright (c) 2000 ISI
package com.isi.csvr.trade;

import com.isi.csvr.ExchangeMap;
import com.isi.csvr.cachestore.Cacheble;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Stock;

import java.io.Serializable;


/**
 * A Class class.
 * <p/>
 *
 * @author ISI
 */
public class Trade implements Serializable, Cloneable, Cacheble {
    private byte[] symbol;
    private String exchangeCode;
    private int instrument;
    private long tradeTime;
    private float tradePrice;
    private long tradeQuantity;
    private int tick = Settings.TICK_NOCHANGE;
    private float g_dNetChange;
    private float g_dPrecentNetChange;
    private long g_lSequence;
    private boolean g_bIndexTrade = false;
    private int splits;
    private byte oddlot;
    private float tradeVWAP;
    private double turnover;
    private long volume;
    private double settlementPrice = 0.00;
    private double accruedInterest=0.00;

    private String ticketNumber = null;
    private String sellOrderNumber = null;
    private String buyOrderNumber = null;
    public String marketID = "NA";
    public String marketCenter = null;
    public String sellerCode = null;
    public char sellerType;
    public String buyerCode = null;
    public char buyerType;
    public int side = Constants.TRADE_SIDE_UNKNOWN;

    public static final char BATE_SYMBOL = 'A';
    public static final char BATE_SPLITS = 'B';
    public static final char BATE_NET_CHANGE = 'C';
    public static final char BATE_TRADE_TIME = 'D';
    public static final char BATE_SEQUENCE = 'E';
    public static final char BATE_PER_NET_CHANGE = 'F';
    public static final char BATE_TRADE_PRICE = 'G';
    public static final char BATE_TRADE_QTY = 'H';
    public static final char BATE_ODDLOT = 'I';
    public static final char BATE_TICK = 'J';
    public static final char BATE_TICKET_NO = 'K';
    public static final char BATE_SELL_ORDER_NO = 'L';
    public static final char BATE_BUY_ORDER_NO = 'M';
    public static final char BATE_MARKET_ID = 'N';
    public static final char BATE_TRADE_VWAP = 'O';
    public static final char BATE_MARKET_CENTER = 'P';
    public static final char BATE_SELLER_CODE = 'Q';
    public static final char BATE_SELLER_TYPE = 'R';
    public static final char BATE_BUYER_CODE = 'S';
    public static final char BATE_BUYER_TYPE = 'T';
    public static final char BATE_SETTLEMENT_PRICE = 'a';
    public static final char BATE_ACCRUED_INTEREST = 'b';
    private long snapShotUpdatedtime = 0l;

    private int tickPrevClosed = Settings.TICK_NOCHANGE;
    double previousClosed = 0.0d;
    double open = 0.0d;
    private Stock stock;
    private double priceChange = 0;

    //    private double tradePriceChange = 0.00d;
    float prevPrice;
//    private long updatedtime=0l;

    /**
     * Constructor
     */
    public Trade(String exchangeCode, String symbol, int instrument) {
        this.exchangeCode = exchangeCode;
        this.symbol = symbol.getBytes();
        this.instrument = instrument;
//        System.out.println("Time n Sales : "+symbol);
//        ObjectWatcher.register(this);
    }

    public void setExchangeCode(String exchangeCode) {
        this.exchangeCode = exchangeCode;
    }

    public void setMarketCenter(String marketCenter) {
        this.marketCenter = marketCenter;
    }

    public void setSymbol(String sSymbol) {
        this.symbol = sSymbol.getBytes();
    }

    public String getExchangeCode() {
        return exchangeCode;
    }

    public String getExchange() {
        return ExchangeMap.getSymbolFor(exchangeCode);
    }

    public int getInstrument() {
        return instrument;
    }

    public void setInstrument(int instrument) {
        this.instrument = instrument;
    }

    public void setTick(int tick) {
        this.tick = tick;
    }

    public int getTick() {
        return this.tick;
    }

    public String getSymbol() {
        return new String(this.symbol);
    }

    public long getTradeTime() {
        return this.tradeTime;
    }

    public void setTradeTime(long lTime) {
        this.tradeTime = lTime;
    }

    public float getTradePrice() {
        return this.tradePrice;
    }

    public void setTradePrice(float dPrice) {
        this.tradePrice = dPrice;
    }

    public long getTradeQuantity() {
        return this.tradeQuantity;
    }

    public void setTradeQuantity(long lQuantity) {
        this.tradeQuantity = lQuantity;
    }

    public long getSequence() {
        return g_lSequence;
    }

    public void setSequence(long lSequence) {
        g_lSequence = lSequence;
    }

    public float getNetChange() {
        return g_dNetChange;
    }

    public void setNetChange(float dNetChange) {
        g_dNetChange = dNetChange;
    }

    public float getPrecentNetChange() {
        return g_dPrecentNetChange;
    }

    public void setPrecentNetChange(float dPrecentNetChange) {
        g_dPrecentNetChange = dPrecentNetChange;
    }

    public float getTradeVWAP() {
        return tradeVWAP;
    }

    public void setTradeVWAP(float tradeVWAP) {
        this.tradeVWAP = tradeVWAP;
    }

    public double getTurnover() {
        return (tradePrice * tradeQuantity);
    }

    public void setTurnover(double turnover) {
        this.turnover = turnover;
    }

    private void setSettlementPrice(double value) {
        this.settlementPrice = value;
    }

    private void setAccruedAmount(double value) {
        this.accruedInterest = value;
    }

    public double getSettlementPrice() {
        return this.settlementPrice;
    }

    public double getAccruedInterest() {
        return this.accruedInterest;
    }

    public String toString() {
        return getTradeTime() + "," +
                getTradePrice() + "," +
                getTradeQuantity() + "," +
                getNetChange() + "," +
                getPrecentNetChange();
    }

    /*public void setIndexType(boolean type) {
        g_bIndexTrade = type;
    }*/

    public boolean isIndexType() {
        return g_bIndexTrade;
    }

    public String toMetaStockString() {
        return SharedMethods.toMetaStockString(this);
    }

    public String toTradeString() {
        return SharedMethods.toAllTradeString(this);
    }

    public int getSplits() {
        return splits;
    }

    public void setSplits(int splits) {
        this.splits = splits;
    }

    public byte getOddlot() {
        return oddlot;
    }

    public void setOddlot(byte oddlot) {
        this.oddlot = oddlot;
    }

    public String setData(String[] data, int decimalFactor) throws Exception {

        if (data == null)
            return "";
        if (stock == null) {
            stock = DataStore.getSharedInstance().getStockObject(getExchange(), new String(this.symbol), this.instrument);
            if (!(stock == null)) {
                previousClosed = stock.getPreviousClosed();
                open = stock.getTodaysOpen();
            }
        }
        char tag;
        splits = 1;
        oddlot = 0;
//        float prevPrice = tradePrice;
        prevPrice = tradePrice;
        int prevTick = tick;
        float prev_netchange = g_dNetChange;
        boolean setTrade = false;
        tick = Settings.TICK_NOCHANGE;
        for (int i = 0; i < data.length; i++) {
            tag = data[i].charAt(0);
            switch (tag) {
                case 'A':
                    symbol = data[i].substring(1).getBytes();
                    break;
                case 'B':
                    splits = Integer.parseInt(data[i].substring(1));
                    break;
                case 'C':
                    g_dNetChange = (float) SharedMethods.getDouble(data[i].substring(1)) / decimalFactor;
                    break;
                case 'D':
                    tradeTime = SharedMethods.TRADE_DATE_FORMAT.parse(data[i].substring(1)).getTime() +
                            ExchangeStore.getSharedInstance().getExchange(getExchange()).getMarketDate();
                    break;
                case 'E':
                    g_lSequence = SharedMethods.getLong(data[i].substring(1));
                    break;
                case 'F':
                    g_dPrecentNetChange = (float) SharedMethods.getDouble(data[i].substring(1)) / decimalFactor;
                    break;
                case 'G':
//                    prevPrice = tradePrice;
                    tradePrice = (float) SharedMethods.getDouble(data[i].substring(1)) / decimalFactor;
                    setTrade = true;
                    //calculation of tick based on last trade price
                    if (tradePrice > prevPrice) {
                        setTick(Settings.TICK_UP);
                    } else if (tradePrice < prevPrice) {
                        setTick(Settings.TICK_DOWN);
                    } else {
                        if ((prevTick == Settings.TICK_UP) || (prevTick == Settings.TICK_POSITIVE_EQUAL)) {
                            setTick(Settings.TICK_POSITIVE_EQUAL);
                        } else {
                            setTick(Settings.TICK_NEGATIVE_EQUAL);
                        }
                    }

                    //calculation of tick based on previous closed price
                    if (tradePrice > previousClosed) {
                        setTickPrevClosed(Settings.TICK_UP);
                    } else if (tradePrice < previousClosed) {
                        setTickPrevClosed(Settings.TICK_DOWN);
                    } else {
                        if ((prevTick == Settings.TICK_UP) || (prevTick == Settings.TICK_POSITIVE_EQUAL)) {
                            setTickPrevClosed(Settings.TICK_POSITIVE_EQUAL);
                        } else {
                            setTickPrevClosed(Settings.TICK_NEGATIVE_EQUAL);
                        }
                    }

                    break;
                case 'H':
                    tradeQuantity = SharedMethods.getLong(data[i].substring(1));
                    break;
                case 'I':
                    oddlot = Byte.parseByte(data[i].substring(1));
                    break;
                case 'J':
                    setTradeTick(data[i].charAt(1), prevTick);
                    break;
                case 'N':
                    marketID = data[i].substring(1);
                    break;
                case 'K':
                    ticketNumber = data[i].substring(1);
                    break;
                case 'M':
                    buyOrderNumber = data[i].substring(1);
                    break;
                case 'L':
                    sellOrderNumber = data[i].substring(1);
                    break;
                case 'O':
                    tradeVWAP = (float) SharedMethods.getDouble(data[i].substring(1)) / decimalFactor;
//                    System.out.println("Symbol = "+new String(this.symbol)+" ,VWAP = "+(float) SharedMethods.getDouble(data[i].substring(1)));
                    break;
                case 'P':
                    marketCenter = data[i].substring(1);
                    break;
                case BATE_SELLER_CODE:
                    sellerCode = data[i].substring(1);
                    break;
                case BATE_SELLER_TYPE:
                    sellerType = data[i].substring(1).toCharArray()[0];
                    break;
                case BATE_BUYER_CODE:
                    buyerCode = data[i].substring(1);
                    break;
                case BATE_BUYER_TYPE:
                    buyerType = data[i].substring(1).toCharArray()[0];
                    break;
                case 'U':
                    side = data[i].substring(1).toCharArray()[0];
                    break;
                case 'V':
                    instrument = Integer.parseInt(data[i].substring(1));
                    break;
                case BATE_SETTLEMENT_PRICE:
                    settlementPrice = SharedMethods.getDouble(data[i].substring(1));
                    break;
                case BATE_ACCRUED_INTEREST:
                    accruedInterest = SharedMethods.getDouble(data[i].substring(1));
                    break;
            }
            if (!setTrade) {
                //calculate tick based on last trade price
                if (tradePrice == prevPrice) {
                    if ((prevTick == Settings.TICK_UP) || (prevTick == Settings.TICK_POSITIVE_EQUAL)) {
                        //System.out.println("Setting(1) positive equal");
                        setTick(Settings.TICK_POSITIVE_EQUAL);
                    } else {
                        // System.out.println("Setting(1) negative equal");
                        setTick(Settings.TICK_NEGATIVE_EQUAL);
                    }
                }
                //calculate tick based on previous closed price
                if (tradePrice == previousClosed) {
                    if ((prevTick == Settings.TICK_UP) || (prevTick == Settings.TICK_POSITIVE_EQUAL)) {
                        //System.out.println("Setting(1) positive equal");
                        setTickPrevClosed(Settings.TICK_POSITIVE_EQUAL);
                    } else {
                        // System.out.println("Setting(1) negative equal");
                        setTickPrevClosed(Settings.TICK_NEGATIVE_EQUAL);
                    }
                }
//                prevPrice = tradePrice;
            }
        }

        return getSymbol();
    }

    public String getMarketID() {
        return marketID;
    }

    public void setSellerCode(String sellerCode) {
        this.sellerCode = sellerCode;
    }

    public void setSellerType(char sellerType) {
        this.sellerType = sellerType;
    }

    public void setBuyerCode(String buyerCode) {
        this.buyerCode = buyerCode;
    }

    public void setBuyerType(char buyerType) {
        this.buyerType = buyerType;
    }

    public String getBuyOrderNumber() {
        return buyOrderNumber;
    }

    public String getSellOrderNumber() {
        return sellOrderNumber;
    }

    public String getTicketNumber() {
        return ticketNumber;
    }


    public void setTradeTick(char tick, int prevTick) {
        if (tick == 'U')
            setTick(Settings.TICK_UP);
        else if (tick == 'D')
            setTick(Settings.TICK_DOWN);
        else if (tick == 'N')
            //setTick(Settings.TICK_NOCHANGE);
            if ((prevTick == Settings.TICK_UP) || (prevTick == Settings.TICK_POSITIVE_EQUAL)) {
                setTick(Settings.TICK_POSITIVE_EQUAL);
            } else {
                setTick(Settings.TICK_NEGATIVE_EQUAL);
            }
        else
            setTick(Settings.TICK_INVALID);
    }

    public Object clone() throws CloneNotSupportedException {
//        ObjectWatcher.register(this);
        return super.clone();
    }

    public void setMarketID(String marketID) {
        this.marketID = marketID;
    }

    public void setBuyOrderNumber(String buyOrderNumber) {
        this.buyOrderNumber = buyOrderNumber;
    }

    public void setSellOrderNumber(String sellOrderNumber) {
        this.sellOrderNumber = sellOrderNumber;
    }

    public void setTicketNumber(String ticketNumber) {
        this.ticketNumber = ticketNumber;
    }


    public String getMarketCenter() {
        return marketCenter;
    }


    public String getSellerCode() {
        return sellerCode;
    }

    public char getSellerType() {
        return sellerType;
    }

    public String getBuyerCode() {
        return buyerCode;
    }

    public char getBuyerType() {
        return buyerType;
    }

    public void setSide(int side) {
        this.side = side;
    }

    public int getSide() {
        return side;
    }

    protected void finalize() throws Throwable {
//        ObjectWatcher.unregister(this);
        super.finalize();
    }

    public synchronized void setSnapShotUpdatedTime(long sUpatedTime) {
        snapShotUpdatedtime = sUpatedTime;
    }

    public synchronized long getSnapShotUpdatedTime() {
        return snapShotUpdatedtime;
    }

    public String getXML() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("<TimeNSalesData>");
        buffer.append("<Symbol=\"").append(getSymbol()).append("\"/>");
        buffer.append("<Exchange=\"").append(getExchange()).append("\"/>");
        buffer.append("<Trade Time=\"").append(getTradeTime()).append("\"/>");
        buffer.append("<Trade Price=\"").append(getTradePrice()).append("\"/>");
        buffer.append("<Trade Quantity=\"").append(getTradeQuantity()).append("\"/>");
        buffer.append("<Sequence=\"").append(getSequence()).append("\"/>");
        buffer.append("<Net Change=\"").append(getNetChange()).append("\"/>");
        buffer.append("<Pct Net Change=\"").append(getPrecentNetChange()).append("\"/>");
        buffer.append("<Trade VWAP=\"").append(getTradeVWAP()).append("\"/>");
        buffer.append("</TimeNSalesData>\n");

        return buffer.toString();
    }

    public String getRecord() {
        return new StringBuilder()
                .append(getExchangeCode()).append(",") /*1x*/
                .append(getSymbol()).append(",")
                .append(getTradeTime()).append(",") /*1x*/
                .append(getTradePrice()).append(",") /*2x*/
                .append(getTradeQuantity()).append(",") /*3x*/
                .append(getNetChange()).append(",") /*4x*/
                .append(getPrecentNetChange()).append(",") /*5x*/
                .append(getOddlot()).append(",") /*6x*/
                .append(getTicketNumber()).append(",") /*7x*/
                .append(getBuyOrderNumber()).append(",") /*8x*/
                .append(getSellOrderNumber()).append(",") /*9x*/
                .append(getMarketID()).append(",") /*10x*/
                .append(getTradeVWAP()).append(",") /*11x*/
                .append(getSide()).append(",")/*12x*/
                .append(getBuyerCode()).append(",") /*13x*/
                .append(getBuyerType()).append(",") /*14x*/
                .append(getSellerCode()).append(",") /*15x*/
                .append(getSellerType()).append(",") /*16x*/
                .append(getInstrument()).append(",") /*17x*/
                .toString();
    }

    public void setRecord(String record) {
        try {
            String[] records = record.split(",");
            setExchangeCode(records[0]);
            setSymbol(records[1]);
            setTradeTime(Long.parseLong(records[2]));
            setTradePrice(Float.parseFloat(records[3]));
            setTradeQuantity(Long.parseLong(records[4]));
            setNetChange(Float.parseFloat(records[5]));
            setPrecentNetChange(Float.parseFloat(records[6]));
            setOddlot(Byte.parseByte(records[7]));
            setTicketNumber(records[8]);
            setBuyOrderNumber(records[9]);
            setSellOrderNumber(records[10]);
            setMarketID(records[11]);
            setTradeVWAP(Float.parseFloat(records[12]));
            setSide(Integer.parseInt(records[13]));
            setBuyerCode(records[14]);
            setBuyerType(records[15].charAt(0));
            setSellerCode(records[16]);
            setSellerType(records[17].charAt(0));
            setInstrument(Integer.parseInt(records[18]));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int compareTo(Object o) {
        return 0;
    }

    public int getTickPrevClosed() {
        return tickPrevClosed;
    }

    public void setTickPrevClosed(int tickPrevClosed) {
        this.tickPrevClosed = tickPrevClosed;
    }

    public double getPreviousTradePrice() {
        if (prevPrice == 0.00d)
            return open;
        else
            return prevPrice;
    }

    public double getPriceChange() {
        return priceChange;
    }

    public void setPriceChange(double priceChange) {
        this.priceChange = priceChange;
    }
}