package com.isi.csvr.trade;

import java.io.Serializable;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class SummaryTrade implements Serializable {// extends Trade {
    private int noOfTrades = 0;
    private float tradePrice;
    private long tradeQuantity;

    private int cashInnoOfTrades = 0;
    private long cashInVloume;
    private double cashInTurnover;
    private int cashoutnoOfTrades = 0;
    private long cashoutVolume;
    private double cashoutTurnover;

    private double netValue;
    private double percentage;

    public SummaryTrade(float tradePrice, long tradeQuantity, int noOfTrades) {
        this.noOfTrades = noOfTrades;
        this.tradePrice = tradePrice;
        this.tradeQuantity = tradeQuantity;
//        ObjectWatcher.register(this);
    }

   // public SummaryTrade(float tradePrice, long tradeQuantity, int noOfTrades, int cashinTrades, long cashinQuantity, int cashoutTrades, long cashoutQuantity) {
    public SummaryTrade(float tradePrice, long tradeQuantity, int noOfTrades,  int cashoutTrades, long cashoutQuantity,int cashinTrades, long cashinQuantity) {  //sequence changed  	 SAUMUBMKT-668 
        this.noOfTrades = noOfTrades;
        this.tradePrice = tradePrice;
        this.tradeQuantity = tradeQuantity;
        this.cashInnoOfTrades = cashinTrades;
        this.cashInVloume = cashinQuantity;
        this.cashoutnoOfTrades = cashoutTrades;
        this.cashoutVolume = cashoutQuantity;
//        ObjectWatcher.register(this);
    }


    public int getNoOfTrades() {
        return noOfTrades;
    }

    public float getTradePrice() {
        return tradePrice;
    }

    public long getTradeQuantity() {
        return tradeQuantity;
    }

    public long getCashInnoOfTrades() {
        return cashInnoOfTrades;
    }

    public long getCashInVloume() {
        return cashInVloume;
    }

    public double getCashInTurnover() {
      //  cashInTurnover = cashInnoOfTrades * cashInVloume;
        cashInTurnover = tradePrice * cashInVloume;
        return cashInTurnover;
    }

    public long getCashoutnoOfTrades() {
        return cashoutnoOfTrades;
    }

    public long getCashoutVolume() {
        return cashoutVolume;
    }

    public double getCashoutTurnover() {
        //cashoutTurnover = cashoutnoOfTrades * cashoutVolume;
        cashoutTurnover = tradePrice * cashoutVolume;
        return cashoutTurnover;
    }

    public double getNetValue() {
        netValue = cashInTurnover - cashoutTurnover;
        return netValue;
    }


    public double getPercentage() {
        percentage = (cashInTurnover) / (cashInTurnover + cashoutTurnover);
        return percentage;
    }

    protected void finalize() throws Throwable {
//        ObjectWatcher.unregister(this);
        super.finalize();
    }
}
