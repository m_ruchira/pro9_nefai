package com.isi.csvr.trade;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public interface TradeListener {
    public void newTradeAdded(Object reference, FilterableTradeStore store, Trade trade);
}