package com.isi.csvr.trade;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 12, 2004
 * Time: 9:41:51 AM
 * To change this template use File | Settings | File Templates.
 */
public class AllTradesSorter implements Comparator {


    private int mode;

    public AllTradesSorter(int mode) {
        this.mode = mode;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public int compare(Object o1, Object o2) {
        Trade trade1 = (Trade) o1;
        Trade trade2 = (Trade) o2;

        switch (mode) {
            case DetailedTradeStore.MODE_SYMBOL:
                if (trade2.getSymbol().compareTo(trade1.getSymbol()) == 0) {
                    if (trade1.getTradeTime() > trade2.getTradeTime()) {
                        return 1;
                    } else if (trade1.getTradeTime() < trade2.getTradeTime()) {
                        return -1;
                    } else {
                        return 0;
                    }
                } else {
                    return trade2.getSymbol().compareTo(trade1.getSymbol());
                }
            case DetailedTradeStore.MODE_TIME:
                if (trade1.getTradeTime() == trade2.getTradeTime()) {
                    return trade2.getSymbol().compareTo(trade1.getSymbol());
                } else {
                    if (trade1.getTradeTime() > trade2.getTradeTime()) {
                        return 1;
                    } else if (trade1.getTradeTime() < trade2.getTradeTime()) {
                        return -1;
                    } else {
                        return trade2.getSymbol().compareTo(trade1.getSymbol());
                    }
                }
            default:
                return 0;
        }
    }

}
