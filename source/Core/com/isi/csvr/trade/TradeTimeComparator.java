package com.isi.csvr.trade;

import java.util.Comparator;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TradeTimeComparator implements Comparator {

    public TradeTimeComparator() {
    }

    public int compare(Object o1, Object o2) {
        Trade t1 = (Trade) o1;
        Trade t2 = (Trade) o2;

        if (t1.getTradeTime() > t2.getTradeTime()) {
            return 1;
        } else if (t1.getTradeTime() < t2.getTradeTime()) {
            return -1;
        } else {
            return 0;
        }
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}