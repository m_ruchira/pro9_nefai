package com.isi.csvr.trade;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 24, 2003
 * Time: 9:18:43 AM
 * To change this template use Options | File Templates.
 */
public class TradeSerialComparator implements Comparator {

    public int compare(Object o1, Object o2) {
        Trade t1 = (Trade) o1;
        Trade t2 = (Trade) o2;
        //todo
        if (t1.getSequence() > t2.getSequence()) {
            return 1;
        } else if (t1.getSequence() < t2.getSequence()) {
            return -1;
        } else {
            return 0;
        }
//        return 1;
    }
}
