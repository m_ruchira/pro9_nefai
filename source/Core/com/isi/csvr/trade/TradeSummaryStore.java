package com.isi.csvr.trade;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.Application;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.DynamicArray;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class TradeSummaryStore implements ExchangeListener, ApplicationListener {
    private static TradeSummaryStore selfRef;

    private Hashtable<String, Hashtable<String, ArrayList<SummaryTrade>>> summaryTable;

    private TradeSummaryStore() {
        summaryTable = new Hashtable<String, Hashtable<String, ArrayList<SummaryTrade>>>();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        Application.getInstance().addApplicationListener(this);
    }

    public static synchronized TradeSummaryStore getSharedInstance() {
        if (selfRef == null)
            selfRef = new TradeSummaryStore();
        return selfRef;
    }

    public void clear(String exchange) {
        try {
            Hashtable<String, ArrayList<SummaryTrade>> exchangeStore = summaryTable.get(exchange);
            Enumeration<ArrayList<SummaryTrade>> elements = exchangeStore.elements();
            while (elements.hasMoreElements()) {
                ArrayList<SummaryTrade> list = elements.nextElement();
                list.clear();
                list.trimToSize();
                list = null;
            }
            elements = null;
            exchangeStore = null;
        } catch (Exception e) {
            //ignore
        }
    }

    private Hashtable<String, ArrayList<SummaryTrade>> getExchangeTable(String exchange) {
        try {
            Hashtable<String, ArrayList<SummaryTrade>> exchangeStore = summaryTable.get(exchange);
            if (exchangeStore == null) {
                exchangeStore = new Hashtable<String, ArrayList<SummaryTrade>>();
                summaryTable.put(exchange, exchangeStore);
            }
            return exchangeStore;
        } catch (Exception e) {
            //e.printStackTrace();
            return null;
        }
    }

    public ArrayList getSummaryTradesForSymbol(String key) {
        String exchange = SharedMethods.getExchangeFromKey(key);
        String symbol = SharedMethods.getSymbolFromKey(key);
        return getSummaryTradesForSymbol(exchange, symbol);
    }

    public ArrayList<SummaryTrade> getSummaryTradesForSymbol(String exchange, String symbol) {
        Hashtable<String, ArrayList<SummaryTrade>> exchangeTable = getExchangeTable(exchange);
        ArrayList<SummaryTrade> summaryData = exchangeTable.get(symbol);
        if (summaryData == null) {
            summaryData = new ArrayList<SummaryTrade>();
            exchangeTable.put(symbol, summaryData);
        }
        exchangeTable = null;

        return summaryData;
    }

    public void addSummaryTrade(String exchange, String symbol, SummaryTrade summaryTrade) {
        ArrayList<SummaryTrade> tradeArray;

        tradeArray = getSummaryTradesForSymbol(exchange, symbol);
        tradeArray.add(summaryTrade);
        tradeArray = null;
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange,long newDate, long oldDate) {
        clear(exchange.getSymbol());
    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {

    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

     public void save() {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(Settings.getAbsolutepath()+"datastore/tradesummary.mdf"));
            out.writeObject(summaryTable);
            out.close();
            out = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void load(){
//        if (Settings.isOfflineMode()) {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(Settings.getAbsolutepath()+"datastore/tradesummary.mdf"));
            summaryTable = (Hashtable<String, Hashtable<String, ArrayList<SummaryTrade>>>) in.readObject();
            in.close();
            in = null;
        } catch (Exception e) {
            summaryTable = new Hashtable<String, Hashtable<String, ArrayList<SummaryTrade>>>();
            e.printStackTrace();
        }
//    }
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(Settings.getAbsolutepath()+"datastore/tradesummary.mdf"));
            Hashtable<String, Hashtable<String, ArrayList<SummaryTrade>>> summaryTableData = (Hashtable<String, Hashtable<String, ArrayList<SummaryTrade>>>) in.readObject();
            Enumeration<String> sSymbols = summaryTableData.keys();
            Enumeration<String> summaryTrades = null;
//            ArrayList<SummaryTrade> tradeList = null;
            String excahnge;
            String symbol;
            Hashtable<String, ArrayList<SummaryTrade>> summaryTradesFromTemp;
            Hashtable<String, ArrayList<SummaryTrade>> summaryTradesFromTable;
            while(sSymbols.hasMoreElements()){
                excahnge = sSymbols.nextElement();
                if(!summaryTable.containsKey(excahnge)){
                    summaryTable.put(excahnge,summaryTableData.get(excahnge));
                } else {
                    summaryTradesFromTemp = summaryTableData.get(excahnge);
                    summaryTradesFromTable = summaryTable.get(excahnge);
                    summaryTrades = summaryTradesFromTemp.keys();
                    while(summaryTrades.hasMoreElements()){
                        symbol = summaryTrades.nextElement();
                        if(!summaryTradesFromTable.containsKey(symbol)){
                            summaryTable.get(excahnge).put(symbol,summaryTradesFromTemp.get(symbol));
                        } else {
                            Collections.copy(summaryTable.get(excahnge).get(symbol), summaryTradesFromTemp.get(symbol));
                        }
                    }
                }
            }
            in.close();
            in = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void selectedExchangeChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}