package com.isi.csvr.trade;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.util.TWList;
import com.isi.csvr.cachestore.CachedStore;
import com.isi.csvr.cachestore.Cacheble;

import java.io.Serializable;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.Enumeration;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class FilterableTradeStore implements Serializable {

    private static long MILLIES_PER_DAY = 86400000L;

    private String[] symbols;
    private String sectorID = null;
    private TWList<Trade> store = null;
    private TWList<Trade> unfilteredStore = null;
    private CachedStore cacheStore = null;
    private CachedStore unfilteredCacheStore = null;
    private int type = 2;
    private String key = null;

    private long totalVolume = 0;
    private float open = 0;
    private float high = 0;
    private float low = 0;
    private float close = 0;

    private long startDate = 0;  // --- Symbol
    private long endDate = 0;
    private float startPrice = 0;
    private float endPrice = 0;
    private long tradeTime = 0;
    private long startVolume = 0;
    private long endVolume = 0;
    private double startTurnover = 0;
    private double endTurnover = 0;
    private String symbol = null;

    private float startPriceMarket = 0;  //--- Market
    private float endPriceMarket = 0;
    private long startVolumeMarket = 0;
    private long endVolumeMarket = 0;
    private double startTurnoverMarket = 0;
    private double endTurnoverMarket = 0;

    private float abovePriceMarket = 0;  //--- Market
    private float belowPriceMarket = 0;
    private long aboveVolumeMarket = 0;
    private long belowVolumeMarket = 0;
    private double aboveTurnoverMarket = 0;
    private double belowTurnoverMarket = 0;

    private boolean filteredByDate = false;
    private boolean filteredByPrice = false;
    private boolean filteredBySymbol = false;
    private boolean filteredByVolume = false;
    private boolean filteredByTurnover = false;

    private boolean filteredByPriceMarket = false; // -- Market
    private boolean filteredByVolumeMarket = false;
    private boolean filteredByTurnoverMarket = false;

    private boolean matchAll = true;
    private boolean matchAny = false;
    private boolean showSuccessiveTrades = false;

    public final static int TYPE_ALL = 0;
    public final static int TYPE_SECTOR = 1;
    public final static int TYPE_SYMBOL = 2;
    public final static int TYPE_COMPANY = 3;
    public final static int TYPE_DETAILED = 4;

    private boolean inDateRange = false;
    private boolean inSymbolRange = false;
    private boolean inPriceRange = false;
    private boolean inVolumeRange = false;
    private boolean inTurnoverRange = false;

    private boolean inPriceRangeMarket = false; // --Market
    private boolean inVolumeRangeMarket = false;
    private boolean inTurnoverRangeMarket = false;

    public static final String SYN_INSERT = "RESIZED";

    public TradeListener tradeListener;

    public FilterableTradeStore(int type, String key, int listType) {
        this.type = type;
        this.key = key;
        store = new TWList<Trade>(listType);
        unfilteredStore = new TWList<Trade>(listType);
    }

    public FilterableTradeStore(String sectorID, String key, int listType) {
        type = TYPE_SECTOR;
        this.sectorID = sectorID;
        this.key = key;
        store = new TWList<Trade>(listType);
        unfilteredStore = new TWList<Trade>(listType);
    }


    public FilterableTradeStore(int type, String key, int listType, boolean b) {
        this.type = type;
        this.key = key;

        cacheStore = new CachedStore(new Trade("-1", "-1", 0), Constants.READE_MODE_BOTTOM_TO_TOP, "Filter");
        unfilteredCacheStore = new CachedStore(new Trade("-1", "-1", 0), Constants.READE_MODE_BOTTOM_TO_TOP, "UnFilter");

    }

    public void addTradeListener(TradeListener tradeListener) {
        this.tradeListener = tradeListener;
    }

    public String getKey() {
        return key;
    }

    public TWList<Trade> getStore() {
        return store;
    }

    public TWList<Trade> getUnfilteredStore() {
        return unfilteredStore;
    }

    public synchronized CachedStore getCachedStore() {
        return cacheStore;
    }

    public synchronized CachedStore getUnfilteredCachedStore() {
        return unfilteredCacheStore;
    }

    public synchronized void clear(boolean clearMasterData) {
        if (clearMasterData) {
            unfilteredStore.clear();
            //unfilteredStore.trimToSize();
            filteredByDate = false;
            filteredByPrice = false;
            filteredBySymbol = false;
            filteredByVolume = false;
            filteredByTurnover = false;

            //== Market
            filteredByPriceMarket = false;
            filteredByVolumeMarket = false;
            filteredByTurnoverMarket = false;

        }
        store.clear();
        //store.trimToSize();
        open = 0;
        high = 0;
        low = 0;
        close = 0;
        totalVolume = 0;
        if (tradeListener != null)
            this.tradeListener.newTradeAdded(null, this, null); // to clear displays

        if (type == TYPE_DETAILED) {// detail trade store is hige. Please call GC to clear memory
            Runtime.getRuntime().gc();
            Runtime.getRuntime().gc();
        }
    }


    public synchronized void clearCachedStroe(boolean clearMasterData) {
        if (clearMasterData) {
            getUnfilteredCachedStore().clear("UnFilter");
            //unfilteredStore.trimToSize();
            filteredByDate = false;
            filteredByPrice = false;
            filteredBySymbol = false;
            filteredByVolume = false;
            filteredByTurnover = false;

            //== Market
            filteredByPriceMarket = false;
            filteredByVolumeMarket = false;
            filteredByTurnoverMarket = false;
        }
        getCachedStore().clear("Filter");
        //store.trimToSize();
        open = 0;
        high = 0;
        low = 0;
        close = 0;
        totalVolume = 0;
        if (tradeListener != null)
            this.tradeListener.newTradeAdded(null, this, null); // to clear displays

        if (type == TYPE_DETAILED) {// detail trade store is hige. Please call GC to clear memory
            Runtime.getRuntime().gc();
            Runtime.getRuntime().gc();
        }
    }

    public void deleteCachedStore(){
        getUnfilteredCachedStore().deleteCache();
        getCachedStore().deleteCache();
    }

    public synchronized void setSymbols(String[] symbols) {
        this.symbols = symbols;
        Arrays.sort(this.symbols);
    }

    public String[] getSymbols() {
        return symbols;
    }

    public synchronized int size() {
        if (type == TYPE_DETAILED) {
          if (DetailedTradeStore.isWritingCache) {
                return 0;
            } else {
                return cachesize();
            }
        } else {
            return store.size();
        }

    }

    public synchronized int unfilteredSize() {
        if (type == TYPE_DETAILED) {
            return unfilteredCacheSize();
        } else {
            return unfilteredStore.size();
        }
    }

    public int cachesize() {
        return getCachedStore().getRowCount();
    }

    public int unfilteredCacheSize() {
        return getUnfilteredCachedStore().getRowCount();
    }

    public Trade getTrade(int location) {
        try {
            return store.get(location);
        }
        catch (Exception ex) {
            return null;
        }
    }
    public Trade getPreviousTrade(Trade trade,int location){
        String symbol=trade.getSymbol();
        int startPosition=location-1;
        if(startPosition<0){
           startPosition=0;
        }
        int index=0;
        for(int i=startPosition;i>0;startPosition--){
             if(symbol.equals(store.get(i).getSymbol())){
                index=i;
                break;
             }
        }
        return store.get(index);
    }
    public Trade getCachedTrade(int location) {
        try {
            return (Trade) getCachedStore().getCachedObject(location);
        }
        catch (Exception ex) {
            return null;
        }
    }

    public Trade getUnfilteredTrade(int location) {
        try {

            if (type == TYPE_DETAILED) {
                return getLastCachableTrade();
            } else {
                return unfilteredStore.get(location);
            }
        }
        catch (Exception ex) {
            return null;
        }
    }


    public Trade getUnfilteredCachedTrade(int location) {
        try {
            return (Trade) getUnfilteredCachedStore().getCachedObject(location);
        }
        catch (Exception ex) {
            return null;
        }
    }

    public Trade getLastTrade() {
        return unfilteredStore.get(unfilteredStore.size() - 1);
    }

    public Trade getLastCachableTrade() {
        return (Trade) getUnfilteredCachedStore().getLastObject();
    }

    private boolean contains(String symbol) {
        return (Arrays.binarySearch(symbols, symbol) >= 0);
    }


    private void insertToStore(int location, Trade trade) {
        unfilteredStore.add(location, trade);

        if (isFilteredOut(trade)) return;

        store.add(location, trade);
        if (open == 0) {
            open = trade.getTradePrice();
        }
        close = trade.getTradePrice();
        if (high < trade.getTradePrice()) {
            high = trade.getTradePrice();
        }
        if ((low > trade.getTradePrice()) || (low == 0)) {
            low = trade.getTradePrice();
        }
        totalVolume += trade.getTradeQuantity();
        if (this.tradeListener != null) {
            this.tradeListener.newTradeAdded(null, this, trade);
        }
    }


    private void insertToCachedStore(Trade trade) {
        getUnfilteredCachedStore().saveObject(trade, false);
        if (isFilteredOut(trade)) return;

        getCachedStore().saveObject(trade, false);
        if (open == 0) {
            open = trade.getTradePrice();
        }
        close = trade.getTradePrice();
        if (high < trade.getTradePrice()) {
            high = trade.getTradePrice();
        }
        if ((low > trade.getTradePrice()) || (low == 0)) {
            low = trade.getTradePrice();
        }
        totalVolume += trade.getTradeQuantity();
        if (this.tradeListener != null) {
            this.tradeListener.newTradeAdded(null, this, trade);
        }
    }

    public void reload() {
        if (type != TYPE_DETAILED) {
            CompanyTradeStore.getSharedInstance().reloadCompanylTradesFor(key);
        }
        if (isFilteredBySymbol()) {
            DetailedTradeStore.getSharedInstance().getSummaryPanel().setVisible(true);
            DetailedTradeStore.getSharedInstance().getSummaryPanel().setvalues();
        } else {
            DetailedTradeStore.getSharedInstance().getSummaryPanel().setVisible(false);
        }
    }

    private boolean isFilteredOut(Trade trade) {

//	    tradeTime = trade.getTradeTime()% MILLIES_PER_DAY;
        tradeTime = ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(trade.getExchange(), trade.getTradeTime());
        tradeTime %= Constants.ONE_DAY;

        if (isFilteredByDate() || isFilteredByPrice() || isFilteredBySymbol() || isFilteredByVolume() || isFilteredByTurnover()) {

            if (isFilteredBySymbol()) {
                if ((symbol != null) && (symbol.equalsIgnoreCase(trade.getSymbol()))) {
                    inSymbolRange = true;
                } else {
                    inSymbolRange = false;
                }
            } else {
                inSymbolRange = true;
            }
            if (isFilteredByDate()) {
                if ((tradeTime >= startDate) && (tradeTime <= (endDate + 999))) {
                    inDateRange = true;
                } else {
                    inDateRange = false;
                }
            } else {
                inDateRange = true;
            }
            if (isFilteredByPrice()) {
                if ((trade.getTradePrice() >= startPrice) && (trade.getTradePrice() <= endPrice)) {
                    inPriceRange = true;
                } else {
                    inPriceRange = false;
                }
            } else {
                inPriceRange = true;
            }
            if (isFilteredByVolume()) {
                if ((trade.getTradeQuantity() >= startVolume) && (trade.getTradeQuantity() <= endVolume)) {
                    inVolumeRange = true;
                } else {
                    inVolumeRange = false;
                }
            } else {
                inVolumeRange = true;
            }
            if (isFilteredByTurnover()) {
                if ((trade.getTurnover() >= startTurnover) && (trade.getTurnover() <= endTurnover)) {
                    inTurnoverRange = true;
                } else {
                    inTurnoverRange = false;
                }
            } else {
                inTurnoverRange = true;
            }


            return !(inDateRange && inPriceRange && inSymbolRange && inVolumeRange && inTurnoverRange);// &&

        }
        // --- Market
        if (isFilteredByPriceMarket() || isFilteredByVolumeMarket() || isFilteredByTurnoverMarket()) {
            if (isMatchAny()) {
                if (isFilteredByPriceMarket()) {
                    if ((trade.getTradePrice() >= startPriceMarket) && (trade.getTradePrice() <= endPriceMarket)) {
                        inPriceRangeMarket = true;
                    } else {
                        inPriceRangeMarket = false;
                    }
                } else {
                    inPriceRangeMarket = false;
                }
                if (isFilteredByVolumeMarket()) {
                    if ((trade.getTradeQuantity() >= startVolumeMarket) && (trade.getTradeQuantity() <= endVolumeMarket)) {
                        inVolumeRangeMarket = true;
                    } else {
                        inVolumeRangeMarket = false;
                    }
                } else {
                    inVolumeRangeMarket = false;
                }
                if (isFilteredByTurnoverMarket()) {
                    if ((trade.getTurnover() >= startTurnoverMarket) && (trade.getTurnover() <= endTurnoverMarket)) {
                        inTurnoverRangeMarket = true;
                    } else {
                        inTurnoverRangeMarket = false;
                    }
                } else {
                    inTurnoverRangeMarket = false;
                }
                return !(inPriceRangeMarket || inVolumeRangeMarket || inTurnoverRangeMarket);// &&

            } else { //-- Match All
                if (isFilteredByPriceMarket()) {
                    if ((trade.getTradePrice() >= startPriceMarket) && (trade.getTradePrice() <= endPriceMarket)) {
                        inPriceRangeMarket = true;
                    } else {
                        inPriceRangeMarket = false;
                    }
                } else {
                    inPriceRangeMarket = true;
                }
                if (isFilteredByVolumeMarket()) {
                    if ((trade.getTradeQuantity() >= startVolumeMarket) && (trade.getTradeQuantity() <= endVolumeMarket)) {
                        inVolumeRangeMarket = true;
                    } else {
                        inVolumeRangeMarket = false;
                    }
                } else {
                    inVolumeRangeMarket = true;
                }
                if (isFilteredByTurnoverMarket()) {
                    if ((trade.getTurnover() >= startTurnoverMarket) && (trade.getTurnover() <= endTurnoverMarket)) {
                        inTurnoverRangeMarket = true;
                    } else {
                        inTurnoverRangeMarket = false;
                    }
                } else {
                    inTurnoverRangeMarket = true;
                }
                return !(inPriceRangeMarket && inVolumeRangeMarket && inTurnoverRangeMarket);
            }
        } else {
            return false;
        }
    }

    public void fireNewPanelAdded() {
        this.tradeListener.newTradeAdded(null, this, null);
    }

    public float getClose() {
        return close;
    }

    public float getHigh() {
        return high;
    }

    public float getLow() {
        return low;
    }

    public float getOpen() {
        return open;
    }

    public long getTotalVolume() {
        return totalVolume;
    }

    public void setStartDate(long startDate) {
        this.startDate = startDate;// % Constants.ONE_DAY;
    }

    public void setStartPrice(float startPrice) {
        this.startPrice = startPrice;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public void setEndDate(long endDate) {
        this.endDate = endDate;// % Constants.ONE_DAY;
    }

    public void setEndPrice(float endPrice) {
        this.endPrice = endPrice;
    }

    public boolean isFilteredByDate() {
        return filteredByDate;
    }

    public void setFilteredByDate(boolean filtered) {
        this.filteredByDate = filtered;
    }

    public boolean isFilteredByPrice() {
        return filteredByPrice;
    }

    public void setFilteredByPrice(boolean filtered) {
        this.filteredByPrice = filtered;
    }

    public boolean isFilteredBySymbol() {
        return filteredBySymbol;
    }

    public void setFilteredBySymbol(boolean filteredBySymbol) {
        this.filteredBySymbol = filteredBySymbol;
    }

    public boolean isFilteredByVolume() {
        return filteredByVolume;
    }

    public void setFilteredByVolume(boolean filteredByVolume) {
        this.filteredByVolume = filteredByVolume;
    }

    public boolean isFilteredByTurnover() {
        return filteredByTurnover;
    }

    public void setFilteredByTurnover(boolean filteredByTurnover) {
        this.filteredByTurnover = filteredByTurnover;
    }

    //--Market

    public boolean isFilteredByPriceMarket() {
        return filteredByPriceMarket;
    }

    public void setFilteredByPriceMarket(boolean filteredByPriceMarket) {
        this.filteredByPriceMarket = filteredByPriceMarket;
    }

    public boolean isFilteredByVolumeMarket() {
        return filteredByVolumeMarket;
    }

    public void setFilteredByVolumeMarket(boolean filteredByVolumeMarket) {
        this.filteredByVolumeMarket = filteredByVolumeMarket;
    }

    public boolean isFilteredByTurnoverMarket() {
        return filteredByTurnoverMarket;
    }

    public void setFilteredByTurnoverMarket(boolean filteredByTurnoverMarket) {
        this.filteredByTurnoverMarket = filteredByTurnoverMarket;
    }

    public float getAbovePriceMarket() {
        return abovePriceMarket;
    }

    public void setAbovePriceMarket(float abovePriceMarket) {
        this.abovePriceMarket = abovePriceMarket;
    }

    public float getBelowPriceMarket() {
        return belowPriceMarket;
    }

    public void setBelowPriceMarket(float belowPriceMarket) {
        this.belowPriceMarket = belowPriceMarket;
    }

    public long getAboveVolumeMarket() {
        return aboveVolumeMarket;
    }

    public void setAboveVolumeMarket(long aboveVolumeMarket) {
        this.aboveVolumeMarket = aboveVolumeMarket;
    }

    public long getBelowVolumeMarket() {
        return belowVolumeMarket;
    }

    public void setBelowVolumeMarket(long belowVolumeMarket) {
        this.belowVolumeMarket = belowVolumeMarket;
    }

    public double getAboveTurnoverMarket() {
        return aboveTurnoverMarket;
    }

    public void setAboveTurnoverMarket(double aboveTurnoverMarket) {
        this.aboveTurnoverMarket = aboveTurnoverMarket;
    }

    public double getBelowTurnoverMarket() {
        return belowTurnoverMarket;
    }

    public void setBelowTurnoverMarket(double belowTurnoverMarket) {
        this.belowTurnoverMarket = belowTurnoverMarket;
    }

    public boolean isMatchAny() {
        return matchAny;
    }

    public void setMatchAny(boolean matchAny) {
        this.matchAny = matchAny;
    }

    public boolean isMatchAll() {
        return matchAll;
    }

    public void setMatchAll(boolean matchAll) {
        this.matchAll = matchAll;
    }

    public boolean isShowSuccessiveTrades() {
        return showSuccessiveTrades;
    }

    public void setShowSuccessiveTrades(boolean showSuccessiveTrades) {
        this.showSuccessiveTrades = showSuccessiveTrades;
    }

    public long getStartDate() {
        return startDate;
    }

    public float getStartPrice() {
        return startPrice;
    }

    public long getEndDate() {
        return endDate;
    }

    public float getEndPrice() {
        return endPrice;
    }

    public long getStartVolume() {
        return startVolume;
    }

    public void setStartVolume(long startVolume) {
        this.startVolume = startVolume;
    }

    public long getEndVolume() {
        return endVolume;
    }

    public void setEndVolume(long endVolume) {
        this.endVolume = endVolume;
    }

    public double getStartTurnover() {
        return startTurnover;
    }

    public void setStartTurnover(double startTurnover) {
        this.startTurnover = startTurnover;
    }

    public double getEndTurnover() {
        return endTurnover;
    }

    public void setEndTurnover(double endTurnover) {
        this.endTurnover = endTurnover;
    }

    //---- Market ---

    public float getStartPriceMarket() {
        return startPriceMarket;
    }

    public void setStartPriceMarket(float startPriceMarket) {
        this.startPriceMarket = startPriceMarket;
    }

    public float getEndPriceMarket() {
        return endPriceMarket;
    }

    public void setEndPriceMarket(float endPriceMarket) {
        this.endPriceMarket = endPriceMarket;
    }

    public long getStartVolumeMarket() {
        return startVolumeMarket;
    }

    public void setStartVolumeMarket(long startVolumeMarket) {
        this.startVolumeMarket = startVolumeMarket;
    }

    public long getEndVolumeMarket() {
        return endVolumeMarket;
    }

    public void setEndVolumeMarket(long endVolumeMarket) {
        this.endVolumeMarket = endVolumeMarket;
    }

    public double getStartTurnoverMarket() {
        return startTurnoverMarket;
    }

    public void setStartTurnoverMarket(double startTurnoverMarket) {
        this.startTurnoverMarket = startTurnoverMarket;
    }

    public double getEndTurnoverMarket() {
        return endTurnoverMarket;
    }

    public void setEndTurnoverMarket(double endTurnoverMarket) {
        this.endTurnoverMarket = endTurnoverMarket;
    }

    public synchronized boolean insert(Comparator comparator, Trade trade) {
        int location;
        synchronized (SYN_INSERT) {
            location = Collections.binarySearch(store, trade, comparator);
        }
        if (location < 0)
            location = (location + 1) * -1;
        else
            return false; // trade already exists in the list
        switch (type) {
            case TYPE_SECTOR:
                if (DataStore.getSharedInstance().getSectorCode(trade.getSymbol()).equals(sectorID)) {
                    insertToStore(location, trade);
                }
                break;
            case TYPE_SYMBOL:
                if (contains(trade.getSymbol())) {
                    insertToStore(location, trade);
                }
                break;
            case TYPE_ALL:
                insertToStore(location, trade);
                break;
            case TYPE_DETAILED:
                insertToStore(location, trade);
                break;
            case TYPE_COMPANY:
                insertToStore(location, trade);
                //DataStore.getSharedInstance().updateTickMap(trade.getExchange(), trade.getSymbol(), location, (byte) trade.getTick(), store.size());
                break;
        }
        return true;
    }

    public void appendTrade(Trade trade) {
//        insertToCachedStore(cacheStore.getRowCount(), trade);
        insertToCachedStore(trade);
    }

    public void pack(){
        getUnfilteredCachedStore().pack();
        getCachedStore().pack();
    }

    public void removeFirstTrade() {
        Object obj = unfilteredStore.removeFirst();
        store.remove(obj);

    }

    public void refilter() {
        Trade trade;
        for (int i = 0; i < unfilteredStore.size(); i++) {
            trade = unfilteredStore.get(i);
            if (isFilteredOut(trade)) continue;
            synchronized (SYN_INSERT) {
                store.add(trade);
            }
            trade = null;
        }
    }

    public void clearAndRefilter() {
        Trade trade;
//        store.clear();
        for (int i = 0; i < unfilteredStore.size(); i++) {
            trade = unfilteredStore.get(i);
            if (isFilteredOut(trade)) continue;
            store.add(trade);
            if (open == 0) {
                open = trade.getTradePrice();
            }
            close = trade.getTradePrice();
            if (high < trade.getTradePrice()) {
                high = trade.getTradePrice();
            }
            if ((low > trade.getTradePrice()) || (low == 0)) {
                low = trade.getTradePrice();
            }
            totalVolume += trade.getTradeQuantity();
            trade = null;
        }
    }


    public void clearAndRefilterCache() {
        Trade trade;
//        store.clear();
        Enumeration<Cacheble> unfilteredcache = getUnfilteredCachedStore().getAllCachedObjects();
        while (unfilteredcache.hasMoreElements()) {
            trade = (Trade) unfilteredcache.nextElement();
            if (isFilteredOut(trade)) continue;
            getCachedStore().saveObject(trade, false);
            if (open == 0) {
                open = trade.getTradePrice();
            }
            close = trade.getTradePrice();
            if (high < trade.getTradePrice()) {
                high = trade.getTradePrice();
            }
            if ((low > trade.getTradePrice()) || (low == 0)) {
                low = trade.getTradePrice();
            }
            totalVolume += trade.getTradeQuantity();
            trade = null;
        }

        /* for (int i = 0; i < unfilteredStore.size(); i++) {
            trade = unfilteredStore.get(i);
            if (isFilteredOut(trade)) continue;
            store.add(trade);
            if (open == 0) {
                open = trade.getTradePrice();
            }
            close = trade.getTradePrice();
            if (high < trade.getTradePrice()) {
                high = trade.getTradePrice();
            }
            if ((low > trade.getTradePrice()) || (low == 0)) {
                low = trade.getTradePrice();
            }
            totalVolume += trade.getTradeQuantity();
            trade = null;
        }*/
    }

    public int getType() {
        return type;
    }
}