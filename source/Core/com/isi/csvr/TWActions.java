package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.debug.Debug;
import com.isi.csvr.dnd.DraggableTable;
import com.isi.csvr.downloader.HistoryArchiveRegister;
import com.isi.csvr.downloader.HistoryDownloadManager;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.shared.*;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.ui.CustomerStatements;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.trading.datastore.RuleManager;
import com.isi.csvr.util.Console;
import com.isi.csvr.util.MemoryWatch;
import com.isi.csvr.watcher.ObjectWatcher;
import com.isi.csvr.table.*;
import com.isi.csvr.communication.tcp.*;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.profiles.imprt.OpenZipFileTree;
import com.isi.csvr.profiles.export.DataSelectionTree;
import com.isi.csvr.sideBar.SideBar;
import com.isi.csvr.volumewatcher.VolumeWatcherUI;
import com.isi.csvr.cashFlowWatch.CashFlowWatcherUI;
import com.isi.csvr.cashFlowWatch.CashFlowHistory;
import com.isi.csvr.troubleshoot.TroubleshootUI;
import com.isi.csvr.trade.DetailedTradeStore;
import com.isi.csvr.tradebacklog.TradeDownloaderUI;
import com.isi.csvr.ohlcbacklog.OHLCDownloaderUI;
import com.isi.csvr.radarscreen.RadarScreenOptionsDialog;
import com.isi.csvr.customindex.CustomIndexWindow;
import com.isi.csvr.commission.CommissionConfig;
import com.isi.csvr.reports.ReportManager;
import com.isi.csvr.win32.NativeMethods;
import com.isi.csvr.help.TipOfDayWindow;
import com.isi.csvr.help.HelpManager;
import com.mubasher.outlook.Reminder;
import com.mubasher.outlook.ReminderUI;

import javax.swing.*;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.KeyEvent;
import java.util.Enumeration;
import java.util.Hashtable;
import java.lang.management.MemoryPoolMXBean;
import java.lang.management.ManagementFactory;
import java.lang.management.MemoryType;
import java.io.*;


/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class TWActions implements KeyEventDispatcher {

    private Client client;
    private static TWActions selfReference;
    private TWDesktop desktop;
    private boolean tableSelected = false;
    private DraggableTable selectedTable = null;
    private boolean optionDialogClosed = false;
    private static boolean ignoreKeyRelaeseForMessageDialog = false;
    private static boolean ctrl_down;
    private int adminKeyCounter = 1;

    private Hashtable<Integer, Integer> adminTable = new Hashtable<Integer, Integer>();
    private Hashtable<Integer, Integer> normalTable = new Hashtable<Integer, Integer>();
    private Hashtable<Integer, Integer> ctrlTable = new Hashtable<Integer, Integer>();
    private Hashtable<Integer, Integer> shiftTable = new Hashtable<Integer, Integer>();
    private Hashtable<Integer, Integer> altTable = new Hashtable<Integer, Integer>();
    private Hashtable<Integer, Integer> altCtrlTable = new Hashtable<Integer, Integer>();
    private Hashtable<Integer, Integer> altShiftTable = new Hashtable<Integer, Integer>();
    private Hashtable<Integer, Integer> ctrlShiftTable = new Hashtable<Integer, Integer>();


    private Hashtable<String, Integer> idMappingTable = new Hashtable<String, Integer>();
    public static Hashtable<String, String> keyMappingTable = new Hashtable<String, String>();

    private Hashtable<String, Integer> mouseGuesturesTable = new Hashtable<String, Integer>();
    public static Hashtable<String, String> mgMappingsTable = new Hashtable<String, String>();


    //Admin Keys
    private String CONSOLE = "CONSOLE";
    private String MEMORY_WATCH = "MEMORY_WATCH";
    private String DISCONNECT_PRICE = "DISCONNECT_PRICE";
    private String CONNECT_TO_PRICE = "CONNECT_TO_PRICE";
    private String DISCONNECT_TRADE = "DISCONNECT_TRADE";
    private String DISPLAY_THREADS = "DISPLAY_THREADS";
    private String GARBAGE_COLLECTION = "GARBAGE_COLLECTION";
    private String SHOW_ACTIVE_IP = "SHOW_ACTIVE_IP";
    private String SHOW_TABLE_SETTINGS = "SHOW_TABLE_SETTINGS";
    private String SHOW_ADD_REQUESTS = "SHOW_ADD_REQUESTS";
    private String DEBUGGER = "DEBUGGER";
    private String OBJECT_WATCH = "OBJECT_WATCH";
    private String PRINT_STOCKMASTER_VERSION = "PRINT_STOCKMASTER_VERSION";
    private String PRINT_HISTORY_ARCHIVE = "PRINT_HISTORY_ARCHIVE";
    private String PRINT_HISTORY = "PRINT_HISTORY";
    private String DISPLAY_TRADING_RULES = "DISPLAY_TRADING_RULES";
    private String CUSTOMIZATION_MODE = "CUSTOMIZATION_MODE";
    private String ENABLE_LOGGING = "ENABLE_LOGGING";
    // CTRL + SHIFT
    private String FOCUS_PREV_WINDOW = "FOCUS_PREV_WINDOW";
    // CTRL
    private String FOCUS_NEXT_WINDOW = "FOCUS_NEXT_WINDOW";
    private String CLOSE_WINDOW = "CLOSE_WINDOW";
    private String WINDOW_SELECTOR = "WINDOW_SELECTOR";
    private String SHOW_CLOSED_FRAMES = "SHOW_CLOSED_FRAMES";
    private String CONNECT = "CONNECT";
    private String CONNECT_TO_TRADE = "CONNECT_TO_TRADE";
    private String CONFIGURE = "CONFIGURE";
    private String SEARCHNAVIGATOR = "SEARCH_NAVIGATOR";
    private String OPEN_FILE = "OPEN_FILE";
    private String SAVE_WORKSPACE = "SAVE_WORKSPACE";
    private String SAVE_AS = "SAVE_AS";
    private String PAGE_SETUP = "PAGE_SETUP";
    private String EXIT = "EXIT";

    // ALT

    //Function Keys
    private String SHOW_HELP = "SHOW_HELP";
    private String TIME_AND_SALES = "TIME_AND_SALES";
    private String SHOW_CHART = "SHOW_CHART";
    private String SNAP_QUOTE = "SNAP_QUOTE";
    private String DEPTH_BY_PRICE = "DEPTH_BY_PRICE";
    private String DEPTH_BY_ORDER = "DEPTH_BY_ORDER";
    private String MARKET_INDICES = "MARKET_INDICES";
    private String FULL_QUOTE = "FULL_QUOTE";
    private String BUY_SCREEN = "BUY_SCREEN";
    private String SELL_SCREEN = "SELL_SCREEN";
    private String ACCOUNT_FRAME = "ACCOUNT_FRAME";
    private String BLOTTER_FRAME = "BLOTTER_FRAME";
    //escape
    private String CLOSE_FOCUSED_WINDOW = "CLOSE_FOCUSED_WINDOW";


    private String PORTFOLIO_SIMULATOR = "PORTFOLIO_SIMULATOR";
    private String ANNOUNCEMENTS = "ANNOUNCEMENTS";
    private String NEWS = "NEWS";
    private String CURRENCY_RATES = "CURRENCY_RATES";
    private String MARKET_SUMMARY = "MARKET_SUMMARY";
    private String TOP_STOCKS = "TOP_STOCKS";
    private String PRO_CHART = "PRO_CHART";
    private String ALERT_MANAGER = "ALERT_MANAGER";
    private String SIDE_BAR = "SIDE_BAR";
    private String VOLUME_WATCHER = "VOLUME_WATCHER";
    private String CASH_FLOW_ANALYZER = "CASH_FLOW_ANALYZER";
    private String GLOBAL_MARKET_SUMMARY = "GLOBAL_MARKET_SUMMARY";
    private String WHAT_IF_CALC = "WHAT_IF_CALC";
    private String MEDIA_PLAYER = "MEDIA_PLAYER";
    private String OUTLOOK_REMINDER = "OUTLOOK_REMINDER";
    private String HISTORICAL_TIME_N_SALES = "HISTORICAL_TIME_N_SALES";
    private String OHLC_DOWNLOADER = "OHLC_DOWNLOADER";
    private String AUTOUPDATE_METASTOCK = "AUTOUPDATE_METASTOCK";
    private String UPDATE_METASTOCK_DB = "UPDATE_METASTOCK_DB";
    private String OPEN_METASTOCK = "OPEN_METASTOCK";
    private String EXPORT_HISTORY = "EXPORT_HISTORY";
    private String HISTORY_ANALYZER = "HISTORY_ANALYZER";
    private String PERFORMANCE_MAP = "PERFORMANCE_MAP";
    private String TAB_INDICATOR_PROPS = "TAB_INDICATOR_PROPS";
    private String FORMULAR_CREATER = "FORMULAR_CREATER";
    private String CUSTOM_INDEX = "CUSTOM_INDEX";
    private String SCANNER = "SCANNER";
    private String BANDWIDTH_OPTIMIZER = "BANDWIDTH_OPTIMIZER";
    private String AUTO_SCROLLING = "AUTO_SCROLLING";
    private String ARABIC_NUMBERS = "ARABIC_NUMBERS";
    private String SHOW_TAB_PANEL = "SHOW_TAB_PANEL";
    private String SHOW_ALIAS = "SHOW_ALIAS";
    private String COMBINED_ORDER_BOOK = "COMBINED_ORDER_BOOK";
    private String POPUP_ANNOUNCEMENTS = "POPUP_ANNOUNCEMENTS";
    private String SAVE_TIME_N_SALES_ON_EXIT = "SAVE_TIME_N_SALES_ON_EXIT";
    private String TIME_LAG_INDICATOR = "TIME_LAG_INDICATOR";
    private String SYMBOL_TRADING_SUMMARY = "SYMBOL_TRADING_SUMMARY";
    private String DAILY_MARKET_SUMMARY = "DAILY_MARKET_SUMMARY";
    private String CASCADE = "CASCADE";
    private String TILE = "TILE";
    private String TILE_HORIZONTALLY = "TILE_HORIZONTALLY";
    private String TILE_VERTICALLY = "TILE_VERTICALLY";
    private String TRADING_HELP = "TRADING_HELP";
    private String WHAT_IS_NEW = "WHAT_IS_NEW";
    private String QUICK_REF_GUIDE = "QUICK_REF_GUIDE";
    private String HOTKEY_GUIDE = "HOTKEY_GUIDE";
    private String FAQ = "FAQ";
    private String SECURITY_TIPS = "SECURITY_TIPS";
    private String SETTING_UP_PRO = "SETTING_UP_PRO";
    private String ENTITLEMENTS = "ENTITLEMENTS";
    private String BROADCAST_MESSAGE = "BROADCAST_MESSAGE";
    private String CHAT = "CHAT";
    private String ABOUT_PRO = "ABOUT_PRO";
    private String SMART_ALERT = "SMART_ALERT";
    private String SMART_ALERT_SUMMARY = "SMART_ALERT_SUMMARY";

    private String BUY = "BUY";
    private String SELL = "SELL";
    private String RAPID_ORDERS = "RAPID_ORDERS";
    private String ORDER_LIST = "ORDER_LIST";
    private String OPEN_POSITIONS = "OPEN_POSITIONS";
    private String BASKET_ORDERS = "BASKET_ORDERS";
    private String PROGRAMED_ORDERS = "PROGRAMED_ORDERS";
    private String TRADE_PORTFOLIO = "TRADE_PORTFOLIO";
    private String ORDER_SEARCH = "ORDER_SEARCH";
    private String CASH_LOG_SEARCH = "CASH_LOG_SEARCH";
    private String ACCOUNT_SUMMARY = "ACCOUNT_SUMMARY";
    private String CUSTOMER_STATEMENTS = "CUSTOMER_STATEMENTS";
    private String DEPOSIT_NOTIFICATION = "DEPOSIT_NOTIFICATION";
    private String WITHDRAW_REQUEST = "WITHDRAW_REQUEST";
    private String FUND_TRANSFER_LIST = "FUND_TRANSFER_LIST";
    private String CASH_TRANSFER = "CASH_TRANSFER";
    private String SUBSCRIBE_IPO = "SUBSCRIBE_IPO";
    private String STOCK_TRANSFER_REQUEST = "STOCK_TRANSFER_REQUEST";
    private String STOCK_TRANSFER_LIST = "STOCK_TRANSFER_LIST";
    private String MARGINABLE_SYMBOLS = "MARGINABLE_SYMBOLS";
    private String CHANGE_TRADE_PASSWORD = "CHANGE_TRADE_PASSWORD";
    private String CHANGE_USB_PIN = "CHANGE_USB_PIN";
    private String SHOW_TRADE_ALERT_POPUP = "SHOW_TRADE_ALERT_POPUP";
    private String TRADE_TIMEOUT = "TRADE_TIMEOUT";

    private String MUTUAL_FUNDS = "MUTUAL_FUNDS";
    private String GLOBAL_INDEX = "GLOBAL_INDEX";
    private String ECON_CALENDAR = "ECON_CALENDAR";
    private String EARNINGS_CALENDAR = "EARNINGS_CALENDAR";
    private String CASH_FLOW_WATCHER = "CASH_FLOW_WATCHER";
    private String TROUBLESHOOT = "TROUBLESHOOT";
    private String DETAILED_TRADES = "DETAILED_TRADES";
    private String DOWNLOAD_HISTORY = "DOWNLOAD_HISTORY";
    private String CANCEL_DOWNLOAD_HISTORY = "CANCEL_DOWNLOAD_HISTORY";
    private String METASTOCK = "METASTOCK";
    private String MENU_OPTION_BUILDER = "MENU_OPTION_BUILDER";
    private String ANNOUNCEMENT_SEARCH = "ANNOUNCEMENT_SEARCH";
    private String CA_SEARCH = "CA_SEARCH";
    private String EDIT_COMISSION = "EDIT_COMISSION";
    private String SECTOR_MAP = "SECTOR_MAP";
    private String CURRENCIES = "CURRENCIES";
    private String FULL_SCREEN_MODE = "FULL_SCREEN_MODE";
    private String TIP_OF_THE_DAY = "TIP_OF_THE_DAY";
    private String TUTORIAL = "TUTORIAL";
    private String CONTACT_CUSTOMER_SUPPORT = "CONTACT_CUSTOMER_SUPPORT";

    private String WATCH_LIST = "WATCH_LIST";
    private String MIST_VIEW = "MIST_VIEW";
    private String FOREX_WATCHLIST = "FOREX_WATCHLIST";
    private String FUNCTION_WATCHLIST = "FUNCTION_WATCHLIST";
    private String MY_PORTFOLIO = "MY_PORTFOLIO";
    private String CHANGE_PASSWORD = "CHANGE_PASSWORD";
    private String EXPORT_USER_DATA = "EXPORT_USER_DATA";
    private String IMPORT_USER_DATA = "IMPORT_USER_DATA";
    private String SET_DEFAULT_WS = "SET_DEFAULT_WS";

    private String SHOW_HISTORY_CHART = "SHOW_HISTORY_CHART";
    private String CASH_FLOW_DETAILED_QUOTE = "CASH_FLOW_DETAILED_QUOTE";
    private String MARKET_DEPTH_CALCULATOR = "MARKET_DEPTH_CALCULATOR";
    private String SPECIAL_ORDER_BOOK = "SPECIAL_ORDER_BOOK";
    private String PUT_ALL_TO_SAME_LAYER = "PUT_ALL_TO_SAME_LAYER";
    private String PERFORMANCE_HIVE = "PERFORMANCE_HIVE";
    private String SYMBOL_TIME_AND_SALES = "SYMBOL_TIME_AND_SALES";

    private String TRADESTATION = "TRADESTATION";
    private String AUTOUPDATE_TRADESTATION = "AUTOUPDATE_TRADESTATION";
    private String UPDATE_TRADESTATION_DB = "UPDATE_TRADESTATION_DB";
    private String OPEN_TRADESTATION = "OPEN_TRADESTATION";
    private String NEW_PF_CREATION = "NEW_PORTFOLIO_CREATION"; //added by Shanaka - SABB CR


    //Admin Keys
    public final static int INT_CONSOLE = 1;
    public final static int INT_MEMORY_WATCH = 2;
    public final static int INT_DISCONNECT_PRICE = 3;
    public final static int INT_DISCONNECT_TRADE = 4;
    public final static int INT_DISPLAY_THREADS = 5;
    public final static int INT_GARBAGE_COLLECTION = 6;
    public final static int INT_SHOW_ACTIVE_IP = 7;
    public final static int INT_SHOW_TABLE_SETTINGS = 8;
    public final static int INT_SHOW_ADD_REQUESTS = 9;
    public final static int INT_DEBUGGER = 10;
    public final static int INT_OBJECT_WATCH = 11;
    public final static int INT_PRINT_STOCKMASTER_VERSION = 12;
    public final static int INT_PRINT_HISTORY_ARCHIVE = 13;
    public final static int INT_PRINT_HISTORY = 14;
    public final static int INT_DISPLAY_TRADING_RULES = 15;
    public final static int INT_CUSTOMIZATION_MODE = 16;
    public final static int INT_ENABLE_LOGGING = 17;
    // CTRL + SHIFT
    public final static int INT_FOCUS_PREV_WINDOW = 18;
    //CTRL
    public final static int INT_FOCUS_NEXT_WINDOW = 19;
    public final static int INT_CLOSE_WINDOW = 20;
    public final static int INT_WINDOW_SELECTOR = 21;
    public final static int INT_SHOW_CLOSED_FRAMES = 22;
    //ALT

    //FUNCTION KEYS
    public final static int INT_SHOW_HELP = 23;
    public final static int INT_TIME_AND_SALES = 24;
    public final static int INT_SHOW_CHART = 25;
    public final static int INT_SNAP_QUOTE = 26;
    public final static int INT_DEPTH_BY_PRICE = 27;
    public final static int INT_DEPTH_BY_ORDER = 28;
    public final static int INT_MARKET_INDICES = 29;
    public final static int INT_FULL_QUOTE = 30;
    public final static int INT_BUY_SCREEN = 31;
    public final static int INT_SELL_SCREEN = 32;
    public final static int INT_ACCOUNT_FRAME = 33;
    public final static int INT_BLOTTER_FRAME = 34;
    //escape
    public final static int INT_CLOSE_FOCUSED_WINDOW = 36;

    //CTRL
    public final static int INT_CONNECT = 37;
    public final static int INT_CONNECT_TO_TRADE = 38;
    public final static int INT_CONFIGURE = 39;
    public final static int INT_OPEN_FILE = 40;
    public final static int INT_SAVE_WORKSPACE = 41;
    public final static int INT_SAVE_AS = 42;
    public final static int INT_PAGE_SETUP = 43;
    public final static int INT_EXIT = 44;

    public final static int INT_PORTFOLIO_SIMULATOR = 45;
    public final static int INT_ANNOUNCEMENTS = 46;
    public final static int INT_NEWS = 47;
    public final static int INT_CURRENCY_RATES = 48;
    public final static int INT_MARKET_SUMMARY = 49;
    public final static int INT_TOP_STOCKS = 50;
    public final static int INT_PRO_CHART = 51;
    public final static int INT_ALERT_MANAGER = 52;
    public final static int INT_SIDE_BAR = 53;
    public final static int INT_VOLUME_WATCHER = 54;
    public final static int INT_CASH_FLOW_ANALYZER = 55;
    public final static int INT_GLOBAL_MARKET_SUMMARY = 56;
    public final static int INT_WHAT_IF_CALC = 57;
    public final static int INT_MEDIA_PLAYER = 58;
    public final static int INT_OUTLOOK_REMINDER = 59;
    public final static int INT_HISTORICAL_TIME_N_SALES = 60;
    public final static int INT_OHLC_DOWNLOADER = 61;
    public final static int INT_AUTOUPDATE_METASTOCK = 62;
    public final static int INT_UPDATE_METASTOCK_DB = 63;
    public final static int INT_OPEN_METASTOCK = 64;
    public final static int INT_EXPORT_HISTORY = 65;
    public final static int INT_HISTORY_ANALYZER = 66;
    public final static int INT_PERFORMANCE_MAP = 67;
    public final static int INT_TAB_INDICATOR_PROPS = 68;
    public final static int INT_FORMULAR_CREATER = 69;
    public final static int INT_CUSTOM_INDEX = 70;
    public final static int INT_SCANNER = 71;
    public final static int INT_BANDWIDTH_OPTIMIZER = 72;
    public final static int INT_AUTO_SCROLLING = 73;
    public final static int INT_ARABIC_NUMBERS = 74;
    public final static int INT_SHOW_TAB_PANEL = 75;
    public final static int INT_SHOW_ALIAS = 76;
    public final static int INT_COMBINED_ORDER_BOOK = 77;
    public final static int INT_POPUP_ANNOUNCEMENTS = 78;
    public final static int INT_SAVE_TIME_N_SALES_ON_EXIT = 79;
    public final static int INT_TIME_LAG_INDICATOR = 80;
    public final static int INT_SYMBOL_TRADING_SUMMARY = 81;
    public final static int INT_DAILY_MARKET_SUMMARY = 82;
    public final static int INT_CASCADE = 83;
    public final static int INT_TILE = 84;
    public final static int INT_TILE_HORIZONTALLY = 85;
    public final static int INT_TILE_VERTICALLY = 86;
    public final static int INT_TRADING_HELP = 87;
    public final static int INT_WHAT_IS_NEW = 88;
    public final static int INT_QUICK_REF_GUIDE = 89;
    public final static int INT_HOTKEY_GUIDE = 90;
    public final static int INT_FAQ = 91;
    public final static int INT_SECURITY_TIPS = 92;
    public final static int INT_SETTING_UP_PRO = 93;
    public final static int INT_ENTITLEMENTS = 94;
    public final static int INT_BROADCAST_MESSAGE = 95;
    public final static int INT_CHAT = 96;
    public final static int INT_ABOUT_PRO = 97;
    public final static int INT_SEARCH_NAVIGATOR = 98;

    //    public final static int INT_BUY = 98;
    //    public final static int INT_SELL = 99;
    public final static int INT_RAPID_ORDERS = 100;
    public final static int INT_ORDER_LIST = 101;
    public final static int INT_OPEN_POSITIONS = 102;
    public final static int INT_BASKET_ORDERS = 103;
    public final static int INT_PROGRAMED_ORDERS = 104;
    public final static int INT_TRADE_PORTFOLIO = 105;
    public final static int INT_ORDER_SEARCH = 106;
    public final static int INT_CASH_LOG_SEARCH = 107;
    public final static int INT_ACCOUNT_SUMMARY = 108;
    public final static int INT_CUSTOMER_STATEMENTS = 109;
    public final static int INT_DEPOSIT_NOTIFICATION = 110;
    public final static int INT_WITHDRAW_REQUEST = 111;
    public final static int INT_FUND_TRANSFER_LIST = 112;
    public final static int INT_CASH_TRANSFER = 113;
    public final static int INT_SUBSCRIBE_IPO = 114;
    public final static int INT_STOCK_TRANSFER_REQUEST = 115;
    public final static int INT_STOCK_TRANSFER_LIST = 116;
    public final static int INT_MARGINABLE_SYMBOLS = 117;
    public final static int INT_CHANGE_TRADE_PASSWORD = 118;
    public final static int INT_CHANGE_USB_PIN = 119;
    public final static int INT_SHOW_TRADE_ALERT_POPUP = 120;
    public final static int INT_TRADE_TIMEOUT = 121;

    public final static int INT_MUTUAL_FUNDS = 122;
    public final static int INT_GLOBAL_INDEX = 123;
    public final static int INT_ECON_CALENDAR = 124;
    public final static int INT_EARNINGS_CALENDAR = 125;
    public final static int INT_CASH_FLOW_WATCHER = 126;
    public final static int INT_TROUBLESHOOT = 127;
    public final static int INT_DETAILED_TRADES = 128;
    public final static int INT_DOWNLOAD_HISTORY = 129;
    public final static int INT_CANCEL_DOWNLOAD_HISTORY = 130;
    public final static int INT_METASTOCK = 131;
    public final static int INT_MENU_OPTION_BUILDER = 132;
    public final static int INT_ANNOUNCEMENT_SEARCH = 133;
    public final static int INT_CA_SEARCH = 134;
    public final static int INT_EDIT_COMISSION = 135;
    public final static int INT_SECTOR_MAP = 136;
    public final static int INT_CURRENCIES = 137;
    public final static int INT_FULL_SCREEN_MODE = 138;
    public final static int INT_TIP_OF_THE_DAY = 139;
    public final static int INT_TUTORIAL = 140;
    public final static int INT_CONTACT_CUSTOMER_SUPPORT = 141;

    public final static int INT_WATCH_LIST = 142;
    public final static int INT_MIST_VIEW = 143;
    public final static int INT_FOREX_WATCHLIST = 144;
    public final static int INT_FUNCTION_WATCHLIST = 145;
    public final static int INT_MY_PORTFOLIO = 146;
    public final static int INT_CHANGE_PASSWORD = 147;
    public final static int INT_EXPORT_USER_DATA = 148;
    public final static int INT_IMPORT_USER_DATA = 149;
    public final static int INT_SET_DEFAULT_WS = 150;

    public final static int INT_SHOW_HISTORY_CHART = 151;
    public final static int INT_CASH_FLOW_DETAILED_QUOTE = 152;
    public final static int INT_MARKET_DEPTH_CALCULATOR = 153;
    public final static int INT_SPECIAL_ORDER_BOOK = 154;
    public final static int INT_PUT_ALL_TO_SAME_LAYER = 155;
    public final static int INT_PERFORMANCE_HIVE = 156;//todo:
    public final static int INT_SMART_ALERT = 157;
    public final static int INT_SMART_ALERT_SUMMARY = 158;

    public final static int INT_MINI_TRADES = 159; //todo: do the implementation
    public final static int INT_SYMBOL_TNS = 160;
    public final static int INT_TRADESTATION = 161;
    public final static int INT_AUTOUPDATE_TRADESTATION = 162;
    public final static int INT_UPDATE_TRADESTATION_DB = 163;
    public final static int INT_OPEN_TRADESTATION = 164;
    public final static int INT_NEW_PF_CREATION = 165;  //added by Shanaka - SABB CR
    private boolean isFocused = true;
    private boolean isKeyPressClicked = true;
    private int keyCount = 0;


    public TWActions(Client client) {
        this.client = client;
        selfReference = this;
        desktop = (TWDesktop) client.getDesktop();
        setIDMappingTable();
        loadShortCuts();
        loadMouseGestures();
    }

    private void setIDMappingTable() {
        //Admin Keys
        idMappingTable.put(CONSOLE, INT_CONSOLE);
        idMappingTable.put(MEMORY_WATCH, INT_MEMORY_WATCH);
        idMappingTable.put(DISCONNECT_PRICE, INT_DISCONNECT_PRICE);
        idMappingTable.put(DISCONNECT_TRADE, INT_DISCONNECT_TRADE);
        idMappingTable.put(DISPLAY_THREADS, INT_DISPLAY_THREADS);
        idMappingTable.put(GARBAGE_COLLECTION, INT_GARBAGE_COLLECTION);
        idMappingTable.put(SHOW_ACTIVE_IP, INT_SHOW_ACTIVE_IP);
        idMappingTable.put(SHOW_TABLE_SETTINGS, INT_SHOW_TABLE_SETTINGS);
        idMappingTable.put(SHOW_ADD_REQUESTS, INT_SHOW_ADD_REQUESTS);
        idMappingTable.put(DEBUGGER, INT_DEBUGGER);
        idMappingTable.put(OBJECT_WATCH, INT_OBJECT_WATCH);
        idMappingTable.put(PRINT_STOCKMASTER_VERSION, INT_PRINT_STOCKMASTER_VERSION);
        idMappingTable.put(PRINT_HISTORY_ARCHIVE, INT_PRINT_HISTORY_ARCHIVE);
        idMappingTable.put(PRINT_HISTORY, INT_PRINT_HISTORY);
        idMappingTable.put(DISPLAY_TRADING_RULES, INT_DISPLAY_TRADING_RULES);
        idMappingTable.put(CUSTOMIZATION_MODE, INT_CUSTOMIZATION_MODE);
        idMappingTable.put(ENABLE_LOGGING, INT_ENABLE_LOGGING);
        //CTRL + SHIFT
        idMappingTable.put(FOCUS_PREV_WINDOW, INT_FOCUS_PREV_WINDOW);
        //CTRL
        idMappingTable.put(FOCUS_NEXT_WINDOW, INT_FOCUS_NEXT_WINDOW);
        idMappingTable.put(CLOSE_WINDOW, INT_CLOSE_WINDOW);
        idMappingTable.put(WINDOW_SELECTOR, INT_WINDOW_SELECTOR);
        idMappingTable.put(SHOW_CLOSED_FRAMES, INT_SHOW_CLOSED_FRAMES);
        idMappingTable.put(CONNECT, INT_CONNECT);
        idMappingTable.put(CONNECT_TO_TRADE, INT_CONNECT_TO_TRADE);
        idMappingTable.put(CONFIGURE, INT_CONFIGURE);
        idMappingTable.put(SEARCHNAVIGATOR, INT_SEARCH_NAVIGATOR);
        idMappingTable.put(OPEN_FILE, INT_OPEN_FILE);
        idMappingTable.put(SAVE_WORKSPACE, INT_SAVE_WORKSPACE);
        idMappingTable.put(SAVE_AS, INT_SAVE_AS);
        idMappingTable.put(PAGE_SETUP, INT_PAGE_SETUP);
        idMappingTable.put(EXIT, INT_EXIT);

        //ALT

        //FUNCTION
        idMappingTable.put(SHOW_HELP, INT_SHOW_HELP);
        idMappingTable.put(TIME_AND_SALES, INT_TIME_AND_SALES);
        idMappingTable.put(SHOW_CHART, INT_SHOW_CHART);
        idMappingTable.put(SNAP_QUOTE, INT_SNAP_QUOTE);
        idMappingTable.put(DEPTH_BY_PRICE, INT_DEPTH_BY_PRICE);
        idMappingTable.put(DEPTH_BY_ORDER, INT_DEPTH_BY_ORDER);
        idMappingTable.put(MARKET_INDICES, INT_MARKET_INDICES);
        idMappingTable.put(FULL_QUOTE, INT_FULL_QUOTE);
        idMappingTable.put(BUY_SCREEN, INT_BUY_SCREEN);
        idMappingTable.put(SELL_SCREEN, INT_SELL_SCREEN);
        idMappingTable.put(ACCOUNT_FRAME, INT_ACCOUNT_FRAME);
        idMappingTable.put(BLOTTER_FRAME, INT_BLOTTER_FRAME);
        //escape
        idMappingTable.put(CLOSE_FOCUSED_WINDOW, INT_CLOSE_FOCUSED_WINDOW);

        idMappingTable.put(PORTFOLIO_SIMULATOR, INT_PORTFOLIO_SIMULATOR);
        idMappingTable.put(ANNOUNCEMENTS, INT_ANNOUNCEMENTS);
        idMappingTable.put(NEWS, INT_NEWS);
        idMappingTable.put(CURRENCY_RATES, INT_CURRENCY_RATES);
        idMappingTable.put(MARKET_SUMMARY, INT_MARKET_SUMMARY);
        idMappingTable.put(TOP_STOCKS, INT_TOP_STOCKS);
        idMappingTable.put(PRO_CHART, INT_PRO_CHART);
        idMappingTable.put(ALERT_MANAGER, INT_ALERT_MANAGER);
        idMappingTable.put(SIDE_BAR, INT_SIDE_BAR);
        idMappingTable.put(VOLUME_WATCHER, INT_VOLUME_WATCHER);
        idMappingTable.put(CASH_FLOW_ANALYZER, INT_CASH_FLOW_ANALYZER);
        idMappingTable.put(GLOBAL_MARKET_SUMMARY, INT_GLOBAL_MARKET_SUMMARY);
        idMappingTable.put(WHAT_IF_CALC, INT_WHAT_IF_CALC);
        idMappingTable.put(MEDIA_PLAYER, INT_MEDIA_PLAYER);
        idMappingTable.put(OUTLOOK_REMINDER, INT_OUTLOOK_REMINDER);
        idMappingTable.put(HISTORICAL_TIME_N_SALES, INT_HISTORICAL_TIME_N_SALES);
        idMappingTable.put(OHLC_DOWNLOADER, INT_OHLC_DOWNLOADER);
        idMappingTable.put(AUTOUPDATE_METASTOCK, INT_AUTOUPDATE_METASTOCK);
        idMappingTable.put(UPDATE_METASTOCK_DB, INT_UPDATE_METASTOCK_DB);
        idMappingTable.put(OPEN_METASTOCK, INT_OPEN_METASTOCK);
        idMappingTable.put(EXPORT_HISTORY, INT_EXPORT_HISTORY);
        idMappingTable.put(HISTORY_ANALYZER, INT_HISTORY_ANALYZER);
        idMappingTable.put(PERFORMANCE_MAP, INT_PERFORMANCE_MAP);
        idMappingTable.put(TAB_INDICATOR_PROPS, INT_TAB_INDICATOR_PROPS);
        idMappingTable.put(FORMULAR_CREATER, INT_FORMULAR_CREATER);
        idMappingTable.put(CUSTOM_INDEX, INT_CUSTOM_INDEX);
        idMappingTable.put(SCANNER, INT_SCANNER);
        idMappingTable.put(BANDWIDTH_OPTIMIZER, INT_BANDWIDTH_OPTIMIZER);
        idMappingTable.put(AUTO_SCROLLING, INT_AUTO_SCROLLING);
        idMappingTable.put(ARABIC_NUMBERS, INT_ARABIC_NUMBERS);
        idMappingTable.put(SHOW_TAB_PANEL, INT_SHOW_TAB_PANEL);
        idMappingTable.put(SHOW_ALIAS, INT_SHOW_ALIAS);
        idMappingTable.put(COMBINED_ORDER_BOOK, INT_COMBINED_ORDER_BOOK);
        idMappingTable.put(POPUP_ANNOUNCEMENTS, INT_POPUP_ANNOUNCEMENTS);
        idMappingTable.put(SAVE_TIME_N_SALES_ON_EXIT, INT_SAVE_TIME_N_SALES_ON_EXIT);
        idMappingTable.put(TIME_LAG_INDICATOR, INT_TIME_LAG_INDICATOR);
        idMappingTable.put(SYMBOL_TRADING_SUMMARY, INT_SYMBOL_TRADING_SUMMARY);
        idMappingTable.put(DAILY_MARKET_SUMMARY, INT_DAILY_MARKET_SUMMARY);
        idMappingTable.put(CASCADE, INT_CASCADE);
        idMappingTable.put(TILE, INT_TILE);
        idMappingTable.put(TILE_HORIZONTALLY, INT_TILE_HORIZONTALLY);
        idMappingTable.put(TILE_VERTICALLY, INT_TILE_VERTICALLY);
        idMappingTable.put(TRADING_HELP, INT_TRADING_HELP);
        idMappingTable.put(WHAT_IS_NEW, INT_WHAT_IS_NEW);
        idMappingTable.put(QUICK_REF_GUIDE, INT_QUICK_REF_GUIDE);
        idMappingTable.put(HOTKEY_GUIDE, INT_HOTKEY_GUIDE);
        idMappingTable.put(FAQ, INT_FAQ);
        idMappingTable.put(SECURITY_TIPS, INT_SECURITY_TIPS);
        idMappingTable.put(SETTING_UP_PRO, INT_SETTING_UP_PRO);
        idMappingTable.put(ENTITLEMENTS, INT_ENTITLEMENTS);
        idMappingTable.put(BROADCAST_MESSAGE, INT_BROADCAST_MESSAGE);
        idMappingTable.put(CHAT, INT_CHAT);
        idMappingTable.put(ABOUT_PRO, INT_ABOUT_PRO);
//        idMappingTable.put(BUY, INT_BUY);
//        idMappingTable.put(SELL, INT_SELL);
        idMappingTable.put(RAPID_ORDERS, INT_RAPID_ORDERS);
        idMappingTable.put(ORDER_LIST, INT_ORDER_LIST);
        idMappingTable.put(OPEN_POSITIONS, INT_OPEN_POSITIONS);
        idMappingTable.put(BASKET_ORDERS, INT_BASKET_ORDERS);
        idMappingTable.put(PROGRAMED_ORDERS, INT_PROGRAMED_ORDERS);
        idMappingTable.put(TRADE_PORTFOLIO, INT_TRADE_PORTFOLIO);
        idMappingTable.put(ORDER_SEARCH, INT_ORDER_SEARCH);
        idMappingTable.put(CASH_LOG_SEARCH, INT_CASH_LOG_SEARCH);
        idMappingTable.put(ACCOUNT_SUMMARY, INT_ACCOUNT_SUMMARY);
        idMappingTable.put(CUSTOMER_STATEMENTS, INT_CUSTOMER_STATEMENTS);
        idMappingTable.put(DEPOSIT_NOTIFICATION, INT_DEPOSIT_NOTIFICATION);
        idMappingTable.put(WITHDRAW_REQUEST, INT_WITHDRAW_REQUEST);
        idMappingTable.put(FUND_TRANSFER_LIST, INT_FUND_TRANSFER_LIST);
        idMappingTable.put(CASH_TRANSFER, INT_CASH_TRANSFER);
        idMappingTable.put(SUBSCRIBE_IPO, INT_SUBSCRIBE_IPO);
        idMappingTable.put(STOCK_TRANSFER_REQUEST, INT_STOCK_TRANSFER_REQUEST);
        idMappingTable.put(STOCK_TRANSFER_LIST, INT_STOCK_TRANSFER_LIST);
        idMappingTable.put(MARGINABLE_SYMBOLS, INT_MARGINABLE_SYMBOLS);
        idMappingTable.put(CHANGE_TRADE_PASSWORD, INT_CHANGE_TRADE_PASSWORD);
        idMappingTable.put(CHANGE_USB_PIN, INT_CHANGE_USB_PIN);
        idMappingTable.put(SHOW_TRADE_ALERT_POPUP, INT_SHOW_TRADE_ALERT_POPUP);
        idMappingTable.put(TRADE_TIMEOUT, INT_TRADE_TIMEOUT);

        idMappingTable.put(MUTUAL_FUNDS, INT_MUTUAL_FUNDS);
        idMappingTable.put(GLOBAL_INDEX, INT_GLOBAL_INDEX);
        idMappingTable.put(ECON_CALENDAR, INT_ECON_CALENDAR);
        idMappingTable.put(EARNINGS_CALENDAR, INT_EARNINGS_CALENDAR);
        idMappingTable.put(CASH_FLOW_WATCHER, INT_CASH_FLOW_WATCHER);
        idMappingTable.put(TROUBLESHOOT, INT_TROUBLESHOOT);
        idMappingTable.put(DETAILED_TRADES, INT_DETAILED_TRADES);
        idMappingTable.put(DOWNLOAD_HISTORY, INT_DOWNLOAD_HISTORY);
        idMappingTable.put(CANCEL_DOWNLOAD_HISTORY, INT_CANCEL_DOWNLOAD_HISTORY);
        idMappingTable.put(METASTOCK, INT_METASTOCK);
        idMappingTable.put(MENU_OPTION_BUILDER, INT_MENU_OPTION_BUILDER);
        idMappingTable.put(ANNOUNCEMENT_SEARCH, INT_ANNOUNCEMENT_SEARCH);
        idMappingTable.put(CA_SEARCH, INT_CA_SEARCH);
        idMappingTable.put(EDIT_COMISSION, INT_EDIT_COMISSION);
        idMappingTable.put(SECTOR_MAP, INT_SECTOR_MAP);
        idMappingTable.put(CURRENCIES, INT_CURRENCIES);
        idMappingTable.put(FULL_SCREEN_MODE, INT_FULL_SCREEN_MODE);
        idMappingTable.put(TIP_OF_THE_DAY, INT_TIP_OF_THE_DAY);
        idMappingTable.put(TUTORIAL, INT_TUTORIAL);
        idMappingTable.put(CONTACT_CUSTOMER_SUPPORT, INT_CONTACT_CUSTOMER_SUPPORT);

        idMappingTable.put(WATCH_LIST, INT_WATCH_LIST);
        idMappingTable.put(MIST_VIEW, INT_MIST_VIEW);
        idMappingTable.put(FOREX_WATCHLIST, INT_FOREX_WATCHLIST);
        idMappingTable.put(FUNCTION_WATCHLIST, INT_FUNCTION_WATCHLIST);
        idMappingTable.put(MY_PORTFOLIO, INT_MY_PORTFOLIO);
        idMappingTable.put(CHANGE_PASSWORD, INT_CHANGE_PASSWORD);
        idMappingTable.put(EXPORT_USER_DATA, INT_EXPORT_USER_DATA);
        idMappingTable.put(IMPORT_USER_DATA, INT_IMPORT_USER_DATA);
        idMappingTable.put(SET_DEFAULT_WS, INT_SET_DEFAULT_WS);

        idMappingTable.put(SHOW_HISTORY_CHART, INT_SHOW_HISTORY_CHART);
        idMappingTable.put(CASH_FLOW_DETAILED_QUOTE, INT_CASH_FLOW_DETAILED_QUOTE);
        idMappingTable.put(MARKET_DEPTH_CALCULATOR, INT_MARKET_DEPTH_CALCULATOR);
        idMappingTable.put(SPECIAL_ORDER_BOOK, INT_SPECIAL_ORDER_BOOK);
        idMappingTable.put(PUT_ALL_TO_SAME_LAYER, INT_PUT_ALL_TO_SAME_LAYER);
        idMappingTable.put(PERFORMANCE_HIVE, INT_PERFORMANCE_HIVE);
        idMappingTable.put(SYMBOL_TIME_AND_SALES, INT_SYMBOL_TNS);
        idMappingTable.put(NEW_PF_CREATION, INT_NEW_PF_CREATION);

        //TradeStation
        idMappingTable.put(TRADESTATION, INT_TRADESTATION);
        idMappingTable.put(AUTOUPDATE_TRADESTATION, INT_AUTOUPDATE_TRADESTATION);
        idMappingTable.put(UPDATE_TRADESTATION_DB, INT_UPDATE_TRADESTATION_DB);
        idMappingTable.put(OPEN_TRADESTATION, INT_OPEN_TRADESTATION);


    }

    private void clearShortcutTables() {

        adminTable.clear();
        normalTable.clear();
        ctrlTable.clear();
        shiftTable.clear();
        altTable.clear();
        altCtrlTable.clear();
        altShiftTable.clear();
        ctrlShiftTable.clear();
    }

    private void clearMouseGestureTables() {
        mouseGuesturesTable.clear();
    }

    private void loadShortCuts() {

        try {
            BufferedReader reader = new BufferedReader(new FileReader(Settings.SYSTEM_PATH + "/shortcuts.dll"));
//          BufferedReader reader = new BufferedReader(new FileReader("C:/shortcuts4.dll"));
            String str;
            String name = "";
            while ((str = reader.readLine()) != null) {
                try {
                    String[] sData = str.toUpperCase().split("\\=");
                    name = sData[0].trim();
                    int id = idMappingTable.get(name);
                    String value = sData[1].trim();
                    keyMappingTable.put(name, value);
                    String[] data = value.split("\\+");
                    boolean isCtrl = false;
                    boolean isAlt = false;
                    boolean isShift = false;
                    for (int i = 0; i < data.length - 1; i++) {
                        String s = data[i].trim();
                        if (s.equals("CTRL")) {
                            isCtrl = true;
                        } else if (s.equals("ALT")) {
                            isAlt = true;
                        } else if (s.equals("SHIFT")) {
                            isShift = true;
                        }
                    }
                    String key = data[data.length - 1].trim();
                    int keyCode = 0;
                    if (key.equalsIgnoreCase("ESCAPE")) {
                        keyCode = KeyEvent.VK_ESCAPE;
                    } else {
                        keyCode = AWTKeyStroke.getAWTKeyStroke(key).getKeyCode();
                    }
                    if (isCtrl && isAlt && isShift) {
                        adminTable.put(keyCode, id);
                    } else if (isShift && isAlt) {
                        altShiftTable.put(keyCode, id);
                    } else if (isCtrl && isAlt) {
                        altCtrlTable.put(keyCode, id);
                    } else if (isCtrl && isShift) {
                        ctrlShiftTable.put(keyCode, id);
                    } else if (isCtrl) {
                        ctrlTable.put(keyCode, id);
                    } else if (isAlt) {
                        altTable.put(keyCode, id);
                    } else if (isShift) {
                        shiftTable.put(keyCode, id);
                    } else {
                        normalTable.put(keyCode, id);
                    }
                } catch (Exception e) {
                    System.out.println("name ==" + name);
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            reader.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static TWActions getInstance() {
        return selfReference;
    }

    private void loadMouseGestures() {
        try {
            BufferedReader reader = new BufferedReader(new FileReader(Settings.SYSTEM_PATH + "/mousegestures.dll"));
//            BufferedReader reader = new BufferedReader(new FileReader("C:/mousegestures1.dll"));
            String str;
            String name = "";
            while ((str = reader.readLine()) != null) {
                try {
                    String[] sData = str.toUpperCase().split("\\=");
                    name = sData[0].trim();
                    int id = idMappingTable.get(name);
                    String value = sData[1].trim();
                    mgMappingsTable.put(name, value);
                    mouseGuesturesTable.put(value, id);

                } catch (Exception e) {
                    System.out.println("name ==" + name);
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }

        } catch (Exception e) {

        }
    }

    public void performMouseGestures(String gesture) {
        int id = mouseGuesturesTable.get(gesture);
        fireActionPerformed(id, true);
    }

    public static void setIgnoreKeyRelaeseForNextMessageDialog(boolean status) {
        ignoreKeyRelaeseForMessageDialog = status;
    }

    public boolean dispatchKeyEvent(KeyEvent e) {
        if (isKeyPressClicked && ((e.getID() & KeyEvent.KEY_RELEASED) == KeyEvent.KEY_RELEASED)) {
//            System.out.println("key released");
            isFocused = true;
            keyCount--;
            if (keyCount == 0)
                isKeyPressClicked = false;
        } else if (isKeyPressClicked && ((e.getID() & KeyEvent.KEY_TYPED) == KeyEvent.KEY_TYPED)) {
//            System.out.println("key typed");
            isFocused = true;
        } else if (((e.getID() & KeyEvent.KEY_PRESSED) == KeyEvent.KEY_PRESSED)) {
//            System.out.println("key pressed");
            isFocused = true;
            isKeyPressClicked = true;
            keyCount++;
        } else {
            isFocused = isKeyPressClicked;
        }
        if (!isFocused) {
            return false;
        }
        if ((!client.symbolBar.isVisible()) /*&& (!Navigator.isDisabled())*/ && (isActionValid(e))) {
            if (!isMetaDown(e)) {
                if ((Character.isLetter(e.getKeyChar())) || (Character.isDigit(e.getKeyChar()))) {
                    e.consume();
                    if (!isParentDetached(e.getComponent()) /*&& !isParentNonNavigateable(e.getComponent())*/) {
                        client.symbolBar.setVisible(true);
                        client.symbolBar.getKeybordListener().requestFocus();

                        if (!(KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof javax.swing.JTextField)) {
                            client.symbolBar.getKeybordListener().setText("" + e.getKeyChar());
                        } else {
                            client.symbolBar.getKeybordListener().setText("");
                        }
                    }
                    return true;
                }
            }
        }

        synchronized (this) {

            if (((e.getID() & KeyEvent.KEY_RELEASED) == KeyEvent.KEY_RELEASED) && (e.getKeyCode() == KeyEvent.VK_ENTER)) {
                // key released and enter key released
                processEnterKeyForKeyReleasd();
            } else if (((e.getID() & KeyEvent.KEY_RELEASED) == KeyEvent.KEY_RELEASED) && (isActionValid(e))) {
                if (optionDialogClosed) {
                    optionDialogClosed = false;
                } else {
                    return actionPerformed(e);
                }
            } else if (isBlockedKey(e)) {
                return true;
            } else if (isControlledKey(e)) {
                return doControlledAction(e);
            }
        }
        return false;
    }

    /*private String getKeyCodes(KeyEvent e){
        return " rel - " + ((e.getID() & KeyEvent.KEY_RELEASED) == KeyEvent.KEY_RELEASED) +
            " press - " + ((e.getID() & KeyEvent.KEY_PRESSED) == KeyEvent.KEY_PRESSED) +
            " type - " + ((e.getID() & KeyEvent.KEY_TYPED) == KeyEvent.KEY_TYPED);
    }*/

    private void processEnterKeyForKeyReleasd() {

        if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                javax.swing.JButton) {
            // to make sure "invalid symbol" popup does not dissapear immediately after the symbol enter and enter released
            if (ignoreKeyRelaeseForMessageDialog) {
                ignoreKeyRelaeseForMessageDialog = false;
                return;
            } else {
                ((JButton) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner()).doClick();
            }
        } else {
            try {
                // to make sure detail quote does not appear immediately after the symbol enter and enter released
                if (ignoreKeyRelaeseForMessageDialog) {
                    ignoreKeyRelaeseForMessageDialog = false;
                    return;
                } else {
                    String symbol = getEnteredSymbol();
                    if (symbol == null) {
                        client.validateWindowData(client.symbolBar.getSymbol(), "DetailQuote");
                    } else {
                        client.mnuDetailQuoteSymbol(symbol);
                    }
                    client.symbolBar.clear();
                    //client.mnuDetailQuoteSymbol(getEnteredSymbol());
                }
            }
            catch (Exception ex) {
            }
            client.symbolBar.setVisible(false);
        }
    }

    private boolean isControlledKey(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_ENTER: // ENTER KEY
                if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                        com.isi.csvr.dnd.DraggableTable) {
                    return true;
                } else if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                        com.isi.csvr.table.TWTextField) {
                    return false; // this makes the table cell to accept the enter key
                } else if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                        com.isi.csvr.ValidatedCompanySelector) {
                    return false; // this makes the table cell to accept the enter key
                } else if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow() instanceof
                        javax.swing.JDialog) {
                    optionDialogClosed = true;
                    return true;
                }
                break;
            case KeyEvent.VK_ESCAPE: // ESCAPE KEY
                if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                        javax.swing.JDialog) {
                    return true;
                }
                break;
        }
        return false;
    }

    private boolean doControlledAction(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_ENTER: // ENTER KEY
                // if enter key is pressed while main board is in focus, cancell the key stroke
                if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                        com.isi.csvr.dnd.DraggableTable) {
                    return true;
                } else if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow() instanceof
                        NonNavigatable) {
                    return false;
                } else if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow() instanceof
                        javax.swing.JDialog) {
                    return true;
                }
                break;
        }
        return false;
    }

    private boolean isBlockedKey(KeyEvent e) {

        switch (e.getKeyCode()) {
            case KeyEvent.VK_F10: // menu
            case KeyEvent.VK_F4: // close window
            case KeyEvent.VK_F8: // split pane
            case KeyEvent.VK_ESCAPE: // escape
                return true;
        }
        return false;
    }

    private boolean isActionValid(KeyEvent e) {
        try {
            if (client.symbolBar.isVisible()) {
                if ((Character.isLetter(e.getKeyChar())) || (Character.isDigit(e.getKeyChar())))
                    return false;
                else
                    return true;
            }
            if ((e.getKeyChar() == KeyEvent.VK_ESCAPE)) {
                return true;
            }
            if ((KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                    com.isi.csvr.table.TWTextField) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            com.isi.csvr.table.TWTextArea) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            TWFormattedTextField) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            JTextPane) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            javax.swing.JPasswordField) ||
                    ((KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow() instanceof
                            javax.swing.JDialog) && (e.getKeyChar() != KeyEvent.VK_ESCAPE)) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow() instanceof
                            com.isi.csvr.shared.NonNavigatable) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            com.isi.csvr.shared.NonNavigatable) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            com.isi.csvr.chart.FocusableComboEditor) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            JFormattedTextField)
                    ) {
                return false;
            } else
                return true;
        }
        catch (Exception ex) {
            return true;
        }
    }

    public String getaction(int id) {
        switch (id) {
            case KeyEvent.KEY_PRESSED:
                return "P";
            case KeyEvent.KEY_RELEASED:
                return "R";
            case KeyEvent.KEY_TYPED:
                return "T";
            default:
                return "U";
        }
    }

    public boolean processEvent(KeyEvent ke) {
        try {
            int code = ke.getKeyCode();

            switch (code) {

                case KeyEvent.VK_F1:
                    hideSymbolBar();
                    client.showHelp();
                    client.symbolBar.clear();

                    return true;
                case KeyEvent.VK_F2:
                    if (client.symbolBar.isVisible()) {
                        String symbol = getEnteredSymbol();
                        //client.mnu_TimeNSalesSymbol(getEnteredSymbol());
                        if (symbol == null) {
                            client.validateWindowData(client.symbolBar.getSymbol(), "Trades");
                        } else {
                            client.mnu_TimeNSalesSymbol(symbol, Constants.MAINBOARD_TYPE, false, false, LinkStore.LINK_NONE);
                        }
                        hideSymbolBar();
                    } else {
                        if (!isParentDetached(ke.getComponent())) {
                            if (ExchangeStore.getSharedInstance().checkMarketTimeNSalesAvailability()) {
                                client.mnu_MarketTimeNSales_ActionPerformed();
                            }
                        }
                    }
                    return true;
                case KeyEvent.VK_F3: {
//                    String symbol = getEnteredSymbol();
//                    if (symbol == null) {
//                        client.validateWindowData(client.symbolBar.getSymbol(), "Chart");
//                    } else {
//                        ChartInterface.showProChart(symbol);
//                    }
//                    hideSymbolBar();
////                    return true;
                    ChartInterface.showChart(client.symbolBar.getSymbol());
                    hideSymbolBar();
                    return true;
                }
                case KeyEvent.VK_F4: {
                    String symbol = getEnteredSymbol();
                    if (symbol == null) {
                        client.validateWindowData(client.symbolBar.getSymbol(), "SummaryQuote");

                    } else {
                        if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(symbol), Meta.IT_SnapQuote)) {
                            client.showSnapQuote(symbol, false, false, false, LinkStore.LINK_NONE);
                        }
                    }
                    hideSymbolBar();
                    return true;
                }
                case KeyEvent.VK_F5: {
                    String symbol = getEnteredSymbol();
                    if (symbol == null) {
                        client.validateWindowData(client.symbolBar.getSymbol(), "DepthPrice");
                    } else {
                        if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(symbol), Meta.IT_MarketDepthByPrice)) {
                            client.depthByPrice(symbol, false, Constants.MAINBOARD_TYPE);
                        }
                    }
                    hideSymbolBar();
                    return true;
                }
                case KeyEvent.VK_F6: {
                    String symbol = getEnteredSymbol();
                    if (TWControl.isOddLotEnabled()) {
                        if (symbol == null) {
                            client.validateWindowData(client.symbolBar.getSymbol(), "DepthOddLot");
                        } else {
                            client.createDepthOddLot(symbol, false, Constants.MAINBOARD_TYPE, false, LinkStore.LINK_NONE);
                        }


                    } else {

                        if (symbol == null) {
                            client.validateWindowData(client.symbolBar.getSymbol(), "DepthOrder");
                        } else {
                            if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(symbol), Meta.IT_MarketDepthByOrder)) {
                                client.depthByOrder(symbol, false, false, LinkStore.LINK_NONE);
                            }
                        }
                    }
                    hideSymbolBar();
                    return true;
                }
                case KeyEvent.VK_F7:
                    client.mnu_MarketIndices_ActionPerformed();
//                    TradeMethods.getSharedInstance().showBuyScreen(null);
                    hideSymbolBar();
                    return true;
                case KeyEvent.VK_F8: {
                    String key = getEnteredSymbol();

                    if (key == null) {
                        client.validateWindowData(client.symbolBar.getSymbol(), "FullQuote");
                    } else {
//                        System.out.println("in TWActions");
                        if (SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(key), Meta.IT_FullQuote)) {
                            boolean isEnabled = DataStore.getSharedInstance().getStockObject(key).isSymbolEnabled();
                            if (isEnabled) {
//                            MultiDialogWindow.getSharedInstance().setVisible(true);
//                            MultiDialogWindow.getSharedInstance().setSymbol(key);
                                Client.getInstance().showFullQuote(key, false, Constants.FULL_QUOTE_TYPE, false, LinkStore.LINK_NONE);
                            }
                        }
                    }

                    hideSymbolBar();
//                    client.createTradingPortfolioView(true);
                    return true;
                }
                case KeyEvent.VK_F9: { //Bug ID <#0015>
                    try {
                        String symbol = getEnteredSymbol();
                        if (symbol == null) {
                            client.validateWindowData(client.symbolBar.getSymbol(), "TradingBuy");
                        } else {
                            TradeMethods.getSharedInstance().showBuyScreen(symbol);
                        }
                    } catch (Exception e) {
                        //TradeMethods.getSharedInstance().showBuyScreen(null);
                    }
                    hideSymbolBar();
                    return true;
                }
                case KeyEvent.VK_F10: { //Bug ID <#0015>
                    try {
                        String symbol = getEnteredSymbol();
                        if (symbol == null) {
                            client.validateWindowData(client.symbolBar.getSymbol(), "TradingSell");
                        } else {
                            TradeMethods.getSharedInstance().showSellScreen(symbol);
                        }
                    } catch (Exception e) {
//                        TradeMethods.getSharedInstance().showSellScreen(null);
                    }
                    hideSymbolBar();
                    return true;
                    /*String symbol = null;
                    try {
                        symbol = getEnteredSymbol(true);
                        TradeMethods.getSharedInstance().showSellScreen(symbol);
                    } catch (Exception e) {
                        //TradeMethods.getSharedInstance().showSellScreen(null);
                    }
                    hideSymbolBar();
                    return true;*/
                }
                case KeyEvent.VK_F11: {
                    TradeMethods.getSharedInstance().showAccountFrame();
                    break;
                }
                case KeyEvent.VK_F12: {
                    client.mnu_Bloter_ActionPerformed();
                    return true;
                }
                case KeyEvent.VK_ESCAPE: {
                    ChartInterface.fireESCActionForGraphFrame();
                    if (client.symbolBar.isVisible()) {
                        client.symbolBar.clear();
                        client.symbolBar.setVisible(false);
                        return true;
                    } else if (client.symbolSearchBar.isVisible()) {
                        client.symbolSearchBar.clear();
                        client.symbolSearchBar.setVisible(false);
                        return true;
                    } else if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow() instanceof
                            javax.swing.JDialog) {
                        if (!(KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow() instanceof NonEscapable)) {
                            JDialog dialog = (JDialog) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow();
                            if (dialog.getDefaultCloseOperation() == JDialog.DISPOSE_ON_CLOSE) {
                                dialog.dispose();
                            } else if (dialog.getDefaultCloseOperation() != JDialog.DO_NOTHING_ON_CLOSE) {
                                dialog.setVisible(false);
                            }
                        }
                        return true;
                    } else {
                        if (GUISettings.isAPopupMenuVisible()) {
                            GUISettings.hideCurrentPopup();
                            return true;
                        } else {
                            JInternalFrame frame = desktop.getSelectedFrame();
                            if (!(frame instanceof NonEscapable) && (((TWDesktopInterface) frame).getDesktopItemType() != TWDesktopInterface.BOARD_TYPE)) {
                                ((TWDesktopInterface) frame).closeWindow();
                            }
                            frame = null;
                            return true;
                        }
                    }
                }
            }
        } catch (Exception ex) {
            client.symbolBar.clear();
            ex.printStackTrace();
        }
        return false;
    }

    private boolean isParentDetached(Component invoker) {
        try {
            JFrame parent = (JFrame) SwingUtilities.getAncestorOfClass(JFrame.class, invoker);
            if (parent.getRootPane() instanceof DetachableRootPane) {
                return true;
            }
        } catch (Exception e) {
            //e.printStackTrace();
        }
        return false;
    }

    private boolean isParentNonNavigateable(Component invoker) {
        try {
            if ((KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow() instanceof
                    com.isi.csvr.shared.NonNavigatable) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            com.isi.csvr.shared.NonNavigatable) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            JFormattedTextField) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            com.isi.csvr.table.TWTextField) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            com.isi.csvr.table.TWTextArea) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            TWFormattedTextField) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            JTextPane) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            javax.swing.JPasswordField) ||
                    (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof
                            com.isi.csvr.chart.FocusableComboEditor)) {
                return true;
            } else
                return false;
        }
        catch (Exception ex) {
        }
        return false;
    }

    public boolean actionPerformed(KeyEvent e) {
        int code = e.getKeyCode();
        boolean ctrlDown = ((e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) ==
                KeyEvent.CTRL_DOWN_MASK);
        boolean altDown = ((e.getModifiersEx() & KeyEvent.ALT_DOWN_MASK) ==
                KeyEvent.ALT_DOWN_MASK) ||
                ((e.getModifiersEx() & KeyEvent.ALT_MASK) == KeyEvent.ALT_MASK);
        boolean shiftDown = ((e.getModifiersEx() & KeyEvent.SHIFT_DOWN_MASK) == KeyEvent.SHIFT_DOWN_MASK) ||
                ((e.getModifiersEx() & KeyEvent.SHIFT_MASK) == KeyEvent.SHIFT_MASK);

        if (shiftDown && ctrlDown && altDown) {
            if (!isParentDetached(e.getComponent())) {
//                 return adminKeyDown(e);
                try {
                    int id = adminTable.get(e.getKeyCode());
                    return fireActionPerformed(id, false);
                } catch (Exception e1) {
                    return false;
                }
            } else {
                return true;
            }

        } else if (shiftDown && ctrlDown) {
            if (!isParentDetached(e.getComponent())) {
//                 return processCtrl_Shift_Key(code);
                try {
                    int id = ctrlShiftTable.get(e.getKeyCode());
                    return fireActionPerformed(id, false);
                } catch (Exception e1) {
                    return false;
                }
            } else {
                return true;
            }
        } else if (ctrlDown) {
            if (!isParentDetached(e.getComponent())) {
//                return processCTRL_Key(code);
                int id = 0;
                try {
                    id = ctrlTable.get(e.getKeyCode());
                    System.out.println(((TWDesktopInterface) this.desktop.getSelectedFrame()).getWindowType());
                    if ((((TWDesktopInterface) this.desktop.getSelectedFrame()).getWindowType()) == Meta.WT_MarketView) {
                        return fireActionPerformed(id, false);
                    } else if (((TWDesktopInterface) this.desktop.getSelectedFrame()).getWindowType() == Meta.WT_WatchList) {
                        return fireActionPerformed(id, false);
                    } else if (((TWDesktopInterface) this.desktop.getSelectedFrame()).getWindowType() == Meta.WT_FilteredWatchList) {
                        return fireActionPerformed(id, false);
                    } else if (((TWDesktopInterface) this.desktop.getSelectedFrame()).getWindowType() == Meta.WT_SectorView) {
                        return fireActionPerformed(id, false);
                    }

                    return false;
                } catch (Exception e1) {
                    if (id == INT_SEARCH_NAVIGATOR)
                        SharedMethods.showMessage(Language.getString("ERROR_WATCHLIST_NOT_SELECTED"), JOptionPane.ERROR_MESSAGE);
                    return false;
                }
            } else {
                return true;
            }
        } else if (altDown) {
//            return processALT_Key(code);
            try {
                int id = altTable.get(e.getKeyCode());
                return fireActionPerformed(id, false);
            } catch (Exception e1) {
                return false;
            }
//            return true;
//        } else if (shiftDown) {
            //return processShift_Key(code);
        } else {
            if (!isParentDetached(e.getComponent())) {
//                return processEvent(e);
                try {
                    int id = normalTable.get(e.getKeyCode());
                    return fireActionPerformed(id, false);
                } catch (Exception e1) {
                    client.symbolBar.clear();
                    return false;
                }
            } else {
                return true;
            }
        }

    }

    private boolean fireActionPerformed(int id, boolean isFromMouseGesture) {

        switch (id) {
            // Admin keys
            case INT_CONSOLE:
                if (Settings.isConsoleAllowed())
                    Console.getSharedInstance().activate();
                return true;
            case INT_MEMORY_WATCH:
                MemoryWatch memoryWatch = new MemoryWatch(Client.getInstance().getFrame());
                memoryWatch.activate();
                showHeap();
                return true;
            case INT_DISCONNECT_PRICE:
                client.disconnectFromServer();
                return true;
            case INT_DISCONNECT_TRADE:
                client.disconnectFromTradingServer();
                return true;
            case INT_DISPLAY_THREADS:
                displayThreads();
                return true;
            case INT_GARBAGE_COLLECTION:
                Runtime.getRuntime().gc();
                Runtime.getRuntime().gc();
                return true;
            case INT_SHOW_ACTIVE_IP:
                String data = null;
                try {
                    data = "Active IP = " + Settings.getActiveIP() + " \n";
                    data += "Symbol Driven IP = " + InternationalConnector.getActiveIP() + " \n";
                    data += "Regional Content Server IP = " + Settings.getRegionalContentProviderIP() + " \n";
                    data += "Global Content Server IP = " + Settings.getGlobalContentProviderIP() + " \n";
                    Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
                    while (exchanges.hasMoreElements()) {
                        Exchange exchange = exchanges.nextElement();
                        data += exchange.getSymbol() + " Realtime path = " + exchange.getPath() + " , Content IP = " + exchange.getContentCMIP() + "\n";
                    }
                } catch (Exception e1) {
                    data = "Active IP = " + Settings.getActiveIP() + " \n";
                }
                new ShowMessage(data, "I");
                return true;
            case INT_SHOW_TABLE_SETTINGS:
                showTableSettings(KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner());
                return true;
            case INT_SHOW_ADD_REQUESTS:
                System.out.println("------- Preferences -------");
                DataStore.getSharedInstance().printPreferences();
                return true;
            case INT_DEBUGGER:
                try {
                    Debug debug = new Debug();
                    debug.show();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                return true;
            case INT_OBJECT_WATCH:
                System.out.println(ObjectWatcher.dump());
                return true;
            case INT_PRINT_STOCKMASTER_VERSION:
                showStockMasterVersions();
                return true;
            case INT_PRINT_HISTORY_ARCHIVE:
                historyArchiveRegister();
                return true;
            case INT_PRINT_HISTORY:
                HistoryDownloadManager.getSharedInstance().print();
                return true;
            case INT_DISPLAY_TRADING_RULES:
                RuleManager.getSharedInstance().showRules();
                return true;
            case INT_CUSTOMIZATION_MODE:
                if (Settings.isDeveloperMode()) {
                    Client.getInstance().enableCustomViewSettings();
                    Settings.enableCustomViewSettings();
                    SharedMethods.showMessage("<HTML><H1>Customisation Mode", JOptionPane.INFORMATION_MESSAGE);
                }
                return true;
            case INT_ENABLE_LOGGING:
                startLogging();
                return true;

            // CTRL + SHIFT
            case INT_FOCUS_PREV_WINDOW:
                try {
                    desktop.focusPreviousWindow(desktop.getListPosition(desktop.getSelectedFrame(), false, false), false);
                }
                catch (Exception ex1) {
                    desktop.focusNextWindow(9999, false);
                }
                return true;

            //CTRL
            case INT_FOCUS_NEXT_WINDOW:
                try {
                    desktop.focusNextWindow(desktop.getListPosition(desktop.getSelectedFrame(), false), false);
                }
                catch (Exception ex1) {
                    desktop.focusNextWindow(9999, false);
                }
                return true;
            case INT_CLOSE_WINDOW: {

                JInternalFrame frame = desktop.getSelectedFrame();
                ((TWDesktopInterface) frame).closeWindow();
                frame = null;
                return true;
            }

            case INT_WINDOW_SELECTOR:
                WindowSelectorDialog ws = new WindowSelectorDialog();
                if (ws.isListNull()) {
                    ws.dispose();
                    ws = null;
                } else {
                    ws.setVisible(true);
                }
                return true;
            case INT_SHOW_CLOSED_FRAMES:
                System.out.println("Undo action raised.....");
                TWDesktop.showClossedFrames();
                return true;
            //ALT

            // FUNCTION KEYS

            case INT_SHOW_HELP:
                hideSymbolBar();
                client.showHelp();
                client.symbolBar.clear();
                return true;
            case INT_TIME_AND_SALES:
                if (client.symbolBar.isVisible() || isFromMouseGesture) {
                    String symbol = null;
                    try {
                        symbol = getEnteredSymbol();
                    } catch (Exception e) {
                        symbol = null;
                    }
                    //client.mnu_TimeNSalesSymbol(getEnteredSymbol());
                    if (symbol == null) {
                        client.validateWindowData(client.symbolBar.getSymbol(), "Trades");
                    } else {
                        client.mnu_TimeNSalesSymbol(symbol, Constants.MAINBOARD_TYPE, false, false, LinkStore.LINK_NONE);
                    }
                    hideSymbolBar();
                } else {
                    // if (!isParentDetached(ke.getComponent())) {
                    if (ExchangeStore.getSharedInstance().checkMarketTimeNSalesAvailability()) {
                        client.mnu_MarketTimeNSales_ActionPerformed();
                    }
                    //  }
                }
                return true;

            /*  String selectedsymbol = null;
               try {
                   selectedsymbol = Client.getInstance().getSelectedSymbol();
               } catch (Exception e) {
                   selectedsymbol = null;
               }
           if(selectedsymbol!= null){
               client.mnu_TimeNSalesSymbol(selectedsymbol, Constants.MAINBOARD_TYPE, false, false, LinkStore.LINK_NONE);
           }else{
               if (ExchangeStore.getSharedInstance().checkMarketTimeNSalesAvailability()) {
                   client.mnu_MarketTimeNSales_ActionPerformed();
               }
           }
           return true;*/
            case INT_SHOW_CHART:
                try {
                    //ChartInterface.showChart(client.symbolBar.getSymbol());
                    String key = null;
                    try {
                        key = getEnteredSymbol();
                    } catch (Exception e) {
                        key = null;
                    }
                    if (key == null) {
                        try {
                            key = DataStore.getSharedInstance().findKeyFromDataStore(client.symbolBar.getSymbol());
                        } catch (Exception e) {
                            key = null;
                        }
                    }
                    if (key == null) {
                        key = client.getSelectedSymbol();
                    }
                    client.showChart(key, null, null, true, false);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                hideSymbolBar();
                return true;
            case INT_SNAP_QUOTE: {
                String symbol = null;
                try {
                    symbol = getEnteredSymbol();
                } catch (Exception e) {
                    symbol = null;
                }
                if (symbol == null) {
                    client.validateWindowData(client.symbolBar.getSymbol(), "SummaryQuote");

                } else {
                    if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(symbol), Meta.IT_SnapQuote)) {
                        client.showSnapQuote(symbol, false, false, false, LinkStore.LINK_NONE);
                    }
                }
                hideSymbolBar();
                return true;
            }

            case INT_DEPTH_BY_PRICE: {
                String symbol = null;
                try {
                    symbol = getEnteredSymbol();
                } catch (Exception e) {
                    symbol = null;
                }
                if (symbol == null) {
                    client.validateWindowData(client.symbolBar.getSymbol(), "DepthPrice");
                } else {
                    if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(symbol), Meta.IT_MarketDepthByPrice)) {
                        client.depthByPrice(symbol, false, Constants.MAINBOARD_TYPE);
                    }
                }
                hideSymbolBar();
                return true;
            }

            case INT_DEPTH_BY_ORDER: {
                String symbol = null;
                try {
                    symbol = getEnteredSymbol();
                } catch (Exception e) {
                    symbol = null;
                }
                if (TWControl.isOddLotEnabled()) {
                    if (symbol == null) {
                        client.validateWindowData(client.symbolBar.getSymbol(), "DepthOddLot");
                    } else {
                        client.createDepthOddLot(symbol, false, Constants.MAINBOARD_TYPE, false, LinkStore.LINK_NONE);
                    }


                } else {

                    if (symbol == null) {
                        client.validateWindowData(client.symbolBar.getSymbol(), "DepthOrder");
                    } else {
                        if (ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(symbol), Meta.IT_MarketDepthByOrder)) {
                            client.depthByOrder(symbol, false, false, LinkStore.LINK_NONE);
                        }
                    }
                }
                hideSymbolBar();
                return true;
            }
            case INT_MARKET_INDICES:
                client.mnu_MarketIndices_ActionPerformed();
                hideSymbolBar();
                return true;
            case INT_FULL_QUOTE: {
                String key = null;
                try {
                    key = getEnteredSymbol();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                if (key == null) {
                    client.validateWindowData(client.symbolBar.getSymbol(), "FullQuote");
                } else {
                    if (SharedMethods.isValidIinformationType(SharedMethods.getExchangeFromKey(key), Meta.IT_FullQuote)) {
                        boolean isEnabled = DataStore.getSharedInstance().getStockObject(key).isSymbolEnabled();
                        if (isEnabled) {
                            Client.getInstance().showFullQuote(key, false, Constants.FULL_QUOTE_TYPE, false, LinkStore.LINK_NONE);
                        }
                    }
                }
                hideSymbolBar();
                return true;
            }
            case INT_BUY_SCREEN:
                try {
                    String symbol = getEnteredSymbol();
                    if (symbol == null) {
                        client.validateWindowData(client.symbolBar.getSymbol(), "TradingBuy");
                    } else {
                        TradeMethods.getSharedInstance().showBuyScreen(symbol);
                    }
                } catch (Exception e) {
                    TradeMethods.getSharedInstance().showBuyScreen(null);
                }
                hideSymbolBar();
                return true;
            case INT_SELL_SCREEN:
                try {
                    String symbol = getEnteredSymbol();
                    if (symbol == null) {
                        client.validateWindowData(client.symbolBar.getSymbol(), "TradingSell");
                    } else {
                        TradeMethods.getSharedInstance().showSellScreen(symbol);
                    }
                } catch (Exception e) {
                    TradeMethods.getSharedInstance().showSellScreen(null);
                }
                hideSymbolBar();
                return true;
            case INT_ACCOUNT_FRAME:
                TradeMethods.getSharedInstance().showAccountFrame();
                return true;
            case INT_BLOTTER_FRAME:
                client.mnu_Bloter_ActionPerformed();
                return true;
            case INT_CLOSE_FOCUSED_WINDOW: {
                ChartInterface.fireESCActionForGraphFrame();
                if (client.symbolBar.isVisible()) {
                    client.symbolBar.clear();
                    client.symbolBar.setVisible(false);
                    return true;
                } else if (client.symbolSearchBar.isVisible()) {
                    client.symbolSearchBar.clear();
                    client.symbolSearchBar.setVisible(false);
                    return true;

                } else if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow() instanceof
                        javax.swing.JDialog) {
                    if (!(KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow() instanceof NonEscapable)) {
                        JDialog dialog = (JDialog) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusedWindow();
                        if (dialog.getDefaultCloseOperation() == JDialog.DISPOSE_ON_CLOSE) {
                            dialog.dispose();
                        } else if (dialog.getDefaultCloseOperation() != JDialog.DO_NOTHING_ON_CLOSE) {
                            dialog.setVisible(false);
                        }
                    }
                    return true;
                } else {
                    if (GUISettings.isAPopupMenuVisible()) {
                        GUISettings.hideCurrentPopup();
                        return true;
                    } else {
                        JInternalFrame frame = desktop.getSelectedFrame();
                        if (!(frame instanceof NonEscapable) && (((TWDesktopInterface) frame).getDesktopItemType() != TWDesktopInterface.BOARD_TYPE)) {
                            ((TWDesktopInterface) frame).closeWindow();
                        }
                        frame = null;
                        return true;
                    }
                }
            }
            case INT_CONNECT:
                if (Settings.isConnected()) {
//                    client.disconnectActionPerformed(); to be implemented
                } else {
                    client.connectActionPerformed(true);
                }
                return true;
            case INT_CONNECT_TO_TRADE:
                client.mnu_TradingConnect_ActionPerformed(true);
                return true;
            case INT_CONFIGURE:
                client.mnu_Config_ActionPerformed();
                return true;
            case INT_OPEN_FILE:
                client.mnuOpen_ActionPerformed();
                return true;
            case INT_SAVE_WORKSPACE:
                client.mnuSave_ActionPerformed();
                return true;
            case INT_SAVE_AS:
                client.mnuSaveAs_ActionPerformed();
                return true;
            case INT_PAGE_SETUP:
                client.mnuShowPageFormat();
                return true;
            case INT_EXIT:
                client.systemExit_ActionPerformed();
                return true;
            case INT_WATCH_LIST:
                client.createNewView(true, null);
                return true;
            case INT_MIST_VIEW:
                client.createNewMistView(true, null);
                return true;
            case INT_FOREX_WATCHLIST:
                client.createNewForexView(true, null);
                return true;
            case INT_FUNCTION_WATCHLIST:
                client.createNewFilteredView(true, null);
                return true;
            case INT_EXPORT_USER_DATA:
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_IMPORT_EXPORT_USER_DATA)) {
                    DataSelectionTree ExSelect = new DataSelectionTree();
                    ExSelect.setVisible(true);
                } else {
                    new ShowMessage("<html><b>" + Language.getString("EXPORT_USER_DATA") + "&nbsp;&nbsp;</b><br>" +
                            Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
                }
                return true;
            case INT_IMPORT_USER_DATA:
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_IMPORT_EXPORT_USER_DATA)) {
                    if (Settings.hasImporteddata()) {
                        String str = Language.getString("MSG_RESTART_TO_APPLY_IMPORT_PREVIOUS");
                        SharedMethods.showMessage(str, JOptionPane.WARNING_MESSAGE);

                    } else {
                        OpenZipFileTree ImSelect = new OpenZipFileTree();
                        (ImSelect.getObject()).setVisible(true);
                    }
                } else {
                    new ShowMessage("<html><b>" + Language.getString("IMPORT_USER_DATA") + "&nbsp;&nbsp;</b><br>" +
                            Language.getString("MSG_NOT_SUBSCRIBED_TO_SEE_INFORMATION_SYMBOL") + "</html>", "E");
                }
                return true;
//            case INT_BUY:
//                client.doTransaction(TradeMeta.BUY, null, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
//                return true;
//            case INT_SELL:
//                client.doTransaction(TradeMeta.SELL, null, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
//                return true;
            case INT_RAPID_ORDERS:
                client.mnuRapidOrderActionPerformed();
                return true;
            case INT_ORDER_LIST:
                client.mnu_Bloter_ActionPerformed();
                return true;
            case INT_OPEN_POSITIONS:
                client.mnuOpenPositionsActionPerformed();
                return true;
            case INT_BASKET_ORDERS:
                client.mnu_OrderQ_ActionPerformed();
                return true;
            case INT_PROGRAMED_ORDERS:
                client.mnu_ProgOrders_ActionPerformed();
                return true;
            case INT_TRADE_PORTFOLIO:
                client.createTradingPortfolioView(true);
                return true;
            case INT_ORDER_SEARCH:
                TradeMethods.getSharedInstance().showOrderSearchWindow();
                return true;
            case INT_CASH_LOG_SEARCH:
                TradeMethods.getSharedInstance().showCashLogSearchWindow();
                return true;
            case INT_ACCOUNT_SUMMARY:
                TradeMethods.getSharedInstance().showAccountFrame();
                return true;
            case INT_CUSTOMER_STATEMENTS:
                if (CustomerStatements.getSharedInstance() != null) {
                    CustomerStatements.getSharedInstance().show();
                } else {
                    CustomerStatements customerStatements = new CustomerStatements();
                    customerStatements.show();
                }
                return true;
            case INT_DEPOSIT_NOTIFICATION:
                TradeMethods.getSharedInstance().showDepositeFrame();
                return true;
            case INT_WITHDRAW_REQUEST:
                TradeMethods.getSharedInstance().showWithdrawalRequest(false); // default transfer mode
                return true;
            case INT_FUND_TRANSFER_LIST:
                TradeMethods.getSharedInstance().showFundTransferFrame();
                return true;
            case INT_CASH_TRANSFER:
                TradeMethods.getSharedInstance().showCashTransferFrame();
                return true;
            case INT_SUBSCRIBE_IPO:
                TradeMethods.getSharedInstance().showIPOSubscribeFrame();
                return true;
            case INT_STOCK_TRANSFER_REQUEST:
                TradeMethods.getSharedInstance().showStockTransferWindow();
                return true;
            case INT_STOCK_TRANSFER_LIST:
                client.mnu_StockTrans_ActionPerformed();
                return true;

            case INT_MARGINABLE_SYMBOLS:
                TradeMethods.getSharedInstance().showMarginableSymbolFrame();
                return true;

            case INT_CHANGE_TRADE_PASSWORD:
                client.changeTradePass(null);
                return true;
            case INT_CHANGE_USB_PIN:
                TradeMethods.getSharedInstance().changeUSBPW(false);
                return true;
            case INT_SHOW_TRADE_ALERT_POPUP:
                client.getMenus().setTradeAlertPopup(!client.getMenus().isShowTradeAlertPopupSelected());
                TradingShared.setShowOrderNotoficationPopup(client.getMenus().isShowTradeAlertPopupSelected());
                return true;
            case INT_PORTFOLIO_SIMULATOR:
                client.createPortfolioView(true);
                return true;
            case INT_ANNOUNCEMENTS:
                client.toggleAnnouncementWindow();
                return true;
            case INT_MUTUAL_FUNDS:
                client.mnu_MutualFund_ActionPerformed();
                return true;
            case INT_NEWS:
                client.mnu_News_ActionPerformed();
                return true;
            case INT_CURRENCY_RATES:
                client.showCurrencyFrame();
                return true;
            case INT_MARKET_SUMMARY:
                client.mnu_ExchangeSummary_ActionPerformed();
                return true;
            case INT_GLOBAL_INDEX:
                client.mnu_GlobalIndex_ActionPerformed();
                return true;
            case INT_TOP_STOCKS:
                client.mnu_topStocks();
                return true;
            case INT_PRO_CHART:
                if (ChartInterface.isFirstTimeChartLoading()) {
                    final WorkInProgressIndicator in = new WorkInProgressIndicator(WorkInProgressIndicator.CHART_LOADING_IN_PROGRESS);
                    in.setVisible(true);
                    new Thread() {
                        public void run() {
                            try {
                                client.showProChart();
                            } catch (Exception e) {
                                e.printStackTrace();  //defalt action
                            }

                            in.dispose();
                            ChartInterface.setFirstTimeChartLoading(false);
                        }
                    }.start();
                } else {
                    client.showProChart();
                }
                return true;
            case INT_ECON_CALENDAR: {
                Client.getInstance().getBrowserFrame().setText("");
                Client.getInstance().getBrowserFrame().setContentType("text/html");
                Client.getInstance().getBrowserFrame().setTitle(Language.getString("ECON_CALENDAR"));
                Client.getInstance().getBrowserFrame().showLoading(Constants.DOC_TYPE_FINANCIAL_CALENDERS);
                String request = Meta.CALENDER_REQUEST + Meta.DS + Meta.ECONOMIC_CALENDER + Meta.EOL;
                SendQFactory.addContentData(Constants.CONTENT_PATH_SECONDARY, request, null);
                SharedMethods.updateGoogleAnalytics("EconomicCalendar");

            }
            return true;
            case INT_EARNINGS_CALENDAR: {
                Client.getInstance().getBrowserFrame().setText("");
                Client.getInstance().getBrowserFrame().setContentType("text/html");
                Client.getInstance().getBrowserFrame().setTitle(Language.getString("EARNINGS_CALENDAR"));
                Client.getInstance().getBrowserFrame().showLoading(Constants.DOC_TYPE_FINANCIAL_CALENDERS);
                String request = Meta.CALENDER_REQUEST + Meta.DS + Meta.EARNINGS_CALENDER + Meta.EOL;
                SendQFactory.addContentData(Constants.CONTENT_PATH_SECONDARY, request, null);
                SharedMethods.updateGoogleAnalytics("EarningsCalendar");
            }
            return true;
            case INT_ALERT_MANAGER:
                client.mnu_AlertManager();
                return true;
            case INT_SIDE_BAR:
                SideBar.getSharedInstance().controlSideBar();
                return true;
            case INT_VOLUME_WATCHER:
                VolumeWatcherUI.getSharedInstance().showVolumeWatcher(true);
                return true;
            case INT_CASH_FLOW_WATCHER:
                CashFlowWatcherUI.getSharedInstance().showCashFlow(true);
                return true;
            case INT_CASH_FLOW_ANALYZER:
                CashFlowHistory.getSharedInstance().showSummaryWindow(true);
                return true;
            case INT_GLOBAL_MARKET_SUMMARY:
                client.createGlobalMarketSummary(false);
                return true;

            case INT_TROUBLESHOOT:
                TroubleshootUI.getSharedInstance().setVisible(true);
                return true;
            case INT_DETAILED_TRADES:
                DetailedTradeStore.getSharedInstance();
                client.mnu_FullTimeNSales_ActionPerformed();
                return true;
            case INT_WHAT_IF_CALC:
                client.openWhatifCalculatorframe();
                ((TWDesktop) Client.getInstance().getDesktop()).refreshDesktop();
                return true;
            case INT_OUTLOOK_REMINDER:
                if (Reminder.checkAvailability()) {
                    ReminderUI rUI = new ReminderUI(Client.getInstance().getFrame());
                    rUI.showDiaog();
                } else {
                    new ShowMessage(Language.getString("MSG_NO_OUTLOOK_SUPPORT"), "E");
                }
                return true;
            case INT_HISTORICAL_TIME_N_SALES:
                TradeDownloaderUI.getSharedInstance().show();
                return true;
            case INT_OHLC_DOWNLOADER:
                OHLCDownloaderUI.getSharedInstance().show();
                return true;
            case INT_DOWNLOAD_HISTORY:
                //check
                client.mnu_download_ActionPerformed(true, null);
                return true;
            case INT_CANCEL_DOWNLOAD_HISTORY:
                client.getMenus().cancelHistoryDownload();
                return true;
            case INT_METASTOCK:
                //check
                try {
                    Runtime.getRuntime().exec(Settings.META_STOCK_APP_PATH);
                    SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.MetaStock, "Open");
                } catch (Exception e) {
                    return false;
                }
                return true;
            case INT_AUTOUPDATE_METASTOCK:
                client.openMetaStockAutoUpdateFrame();
                return true;
            case INT_UPDATE_METASTOCK_DB:
                client.updateMetaStockDB();
                return true;
            case INT_OPEN_METASTOCK:
                new Thread("Open Metastock Folder") {
                    public void run() {
                        try {
                            Desktop.getDesktop().open(new File(Settings.META_STOCK_DB_PATH));
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }.start();
                return true;
            case INT_TRADESTATION:
                //check
                try {
                    Runtime.getRuntime().exec(Settings.TRADE_STATION_BAT_PATH);
                } catch (Exception e) {
                    return false;
                }
                return true;
            case INT_AUTOUPDATE_TRADESTATION:
                client.openTradeStationAutoUpdateFrame();
                return true;
            case INT_UPDATE_TRADESTATION_DB:
                client.updateTradeStationDB();
                return true;
            case INT_OPEN_TRADESTATION:
                new Thread("Open TradeStation Folder") {
                    public void run() {
                        try {
                            Desktop.getDesktop().open(new File(Settings.TRADE_SATION_DB_PATH));
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }.start();
                return true;
            case INT_EXPORT_HISTORY:
                client.mnu__Export_ActionPerformed();
                return true;
            case INT_HISTORY_ANALYZER:
                client.showNetCalculator();
                return true;
            case INT_MENU_OPTION_BUILDER:
                client.mnu_FindOptionSymbol(true);
                return true;
            case INT_ANNOUNCEMENT_SEARCH:
                client.showAnnouncementSearchPanel();
                return true;

            case INT_PERFORMANCE_MAP:
                client.showFullHeatMap();
                return true;
            case INT_TAB_INDICATOR_PROPS:
                RadarScreenOptionsDialog radarScreenOptionsDialog = new RadarScreenOptionsDialog(
                        Client.getInstance().getFrame());
                radarScreenOptionsDialog.setLocationRelativeTo(Client.getInstance().getFrame());
                radarScreenOptionsDialog.setVisible(true);
                return true;
            case INT_FORMULAR_CREATER:
                client.createNewFormula();
                return true;
            case INT_CUSTOM_INDEX:
                CustomIndexWindow.getSharedInstance().setVisible(true);
                return true;
            case INT_SCANNER:
                client.showScanner();
                return true;
            case INT_CA_SEARCH:
                Client.getInstance().showCASearchPanel();
                return true;
            case INT_EDIT_COMISSION:
                new CommissionConfig();
                return true;
            case INT_SECTOR_MAP:
                client.toggleSectorMapWindow();
                return true;
            case INT_BANDWIDTH_OPTIMIZER:
                client.mnu_SelectedSymbols_ActionPerformed();
                return true;
            case INT_AUTO_SCROLLING:
                client.getMenus().setAutoScrollingStatus(!client.getMenus().isAutoScrollingSelected());
                Settings.setAutoScrolling();
                return true;
            case INT_ARABIC_NUMBERS:
                client.getMenus().setArabicNumbers(!client.getMenus().isArabicNumbersSelected());
                new ShowMessage(UnicodeUtils.getNativeString(Language.
                        getString("MSG_CHANGE_NUMBER_SYSTEM")), "I");
                client.showArabicNumbers(client.getMenus().isArabicNumbersSelected());
                return true;
            case INT_FULL_SCREEN_MODE:
                client.getMenus().setFullScreenMode(!client.getMenus().isFullScreenModeSelected());
                client.toggleScreen(client.getMenus().isFullScreenModeSelected());
                return true;
            case INT_SHOW_TAB_PANEL:
                client.getMenus().setShowTabPanel(!client.getMenus().isShowTabPanelSelected());
                Settings.setTabIndex(client.getMenus().isShowTabPanelSelected());
                client.addTabIndexpanel(client.getMenus().isShowTabPanelSelected());
                return true;
            case INT_SHOW_ALIAS:
                client.getMenus().setShowAlies(!client.getMenus().isShowAliasSelected());
                client.showAlias(client.getMenus().isShowAliasSelected());
                return true;
            case INT_COMBINED_ORDER_BOOK:
                client.getMenus().setCombinedOrderBook(!client.getMenus().isCombinedOrderBookSelected());
                Settings.setShowCombinedOrderBook(client.getMenus().isCombinedOrderBookSelected());
                return true;
            case INT_POPUP_ANNOUNCEMENTS:
                boolean status = client.getMenus().isPopupAnnouncementsSelected();
                client.getMenus().setPopupAnnouncements(!status);
                Settings.setPopupAnnounceWindow(client.getMenus().isPopupAnnouncementsSelected());
                if (client.getMenus().getDoNotAutoPopupAnnouncements() != null) {
                    client.getMenus().setDoNotAutoPopupAnnouncementsSelected(status);
                }
                return true;
            case INT_SAVE_TIME_N_SALES_ON_EXIT:
                client.getMenus().setSaveTradesOnExit(!client.getMenus().isSaveTradesOnExitSelected());
                Settings.setSaveTimeNSales(client.getMenus().isSaveTradesOnExitSelected());
                return true;
            case INT_TIME_LAG_INDICATOR:
                LatancyIndicator.getSharedInstance().setVisible(true);
                return true;

            case INT_SYMBOL_TRADING_SUMMARY:
                ReportManager.showSymbolTradingReport();
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Reports, "SymbolTradingSummary");
                return true;
            case INT_DAILY_MARKET_SUMMARY:
                ReportManager.showDailyMarketReport();
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Reports, "DailyMarketSummary");
                return true;
            case INT_CASCADE:
                client.mnu_cascadeWindows();
                return true;
            case INT_TILE:
                client.mnu_TileAllWindows();
                return true;
            case INT_TILE_HORIZONTALLY:
                client.mnu_TileAllWindowsH();
                return true;
            case INT_TILE_VERTICALLY:
                client.mnu_TileAllWindowsV();
                return true;

            case INT_TIP_OF_THE_DAY:
                new TipOfDayWindow().showUI();
                return true;
            case INT_TRADING_HELP:
                NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_TRADE"));
                return true;
            case INT_WHAT_IS_NEW:
                NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_WHATS_NEW"));
                return true;
            case INT_QUICK_REF_GUIDE:
                NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_QRG"));
                return true;
            case INT_TUTORIAL:
                NativeMethods.showTutorial();
                return true;
            case INT_HOTKEY_GUIDE:
                NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_HOTKEY_GUIDE"));
                return true;
            case INT_FAQ:
                NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_FAQ"));
                return true;
            case INT_SECURITY_TIPS:
                NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_SECURITY_TIPS"));
                return true;
            case INT_SETTING_UP_PRO:
                NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_SETTING_UP"));
                return true;
            case INT_ENTITLEMENTS:
                client.showEntitlements(true);
                return true;
            case INT_BROADCAST_MESSAGE:
                client.toggleBroadcastMessageWindow();
                return true;
            case INT_CHAT:
                client.openChatSupportFrame();
                return true;
            case INT_CONTACT_CUSTOMER_SUPPORT:
                client.openCustomerSupportFrame();
                return true;
            case INT_ABOUT_PRO:
                AboutBox aboutBox = AboutBox.getSharedInstance();
                aboutBox.show();
                return true;
            case INT_SEARCH_NAVIGATOR:
                if (client.isMainBoardSelected()) {
                    client.symbolSearchBar.setVisible(true);
                }
                return true;
            case INT_SHOW_HISTORY_CHART: {
                String key = client.getSelectedSymbol();
                client.showChart(key, null, null, false, false);
                return true;
            }
            case INT_CASH_FLOW_DETAILED_QUOTE: {
                String key = client.getSelectedSymbol();
                if (client.isValidInfoType(key, Meta.IT_CashFlow, true)) {
                    client.showCashFlowDetailQuote(key, false, false, false, LinkStore.LINK_NONE);
                }
                return true;
            }
            case INT_MARKET_DEPTH_CALCULATOR: {
                String key = client.getSelectedSymbol();
                if (client.isValidInfoType(key, Meta.IT_DepthCalculator, true)) {
                    client.mnu_MDepth_Calculator(null, TWTypes.TradeSides.BUY, false, false, LinkStore.LINK_NONE);
                }
            }
            return true;
            case INT_SPECIAL_ORDER_BOOK: {
                String key = client.getSelectedSymbol();
                if (client.isValidInfoType(key, Meta.IT_SpecialOrderBook, true) && ExchangeStore.getSharedInstance().checkFeatureAvailability(SharedMethods.getExchangeFromKey(key), Meta.IT_SpecialOrderBook)) {
                    client.depthSpecialByOrder(key, false, false, LinkStore.LINK_NONE);
                } else {
                    String message = Language.getString("FEATURE_NOT_AVAILABLE");
                    message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(key)).getDescription());
                    SharedMethods.showMessage(message, JOptionPane.INFORMATION_MESSAGE);
                }
            }
            return true;
            case INT_PUT_ALL_TO_SAME_LAYER:
                return true;
            case INT_SYMBOL_TNS:
                String selectedsymbol = null;
                try {
                    selectedsymbol = Client.getInstance().getSelectedSymbol();
                } catch (Exception e) {
                    selectedsymbol = null;
                }
                if (selectedsymbol != null) {
                    client.mnu_TimeNSalesSymbol(selectedsymbol, Constants.MAINBOARD_TYPE, false, false, LinkStore.LINK_NONE);
                } else {
                    if (ExchangeStore.getSharedInstance().checkMarketTimeNSalesAvailability()) {
                        client.mnu_MarketTimeNSales_ActionPerformed();
                    }
                }
                return true;
            case INT_NEW_PF_CREATION:
                TradeMethods.getSharedInstance().showOrderSearchWindow();
                return true;

            default:
                return false;
        }
    }

    public boolean adminKeyDown(KeyEvent e) {
        switch (e.getKeyCode()) {
            case KeyEvent.VK_C:// console window
                if (Settings.isConsoleAllowed())
                    Console.getSharedInstance().activate();
                return true;
            case KeyEvent.VK_M: // memory watch
                MemoryWatch memoryWatch = new MemoryWatch(Client.getInstance().getFrame());
                memoryWatch.activate();
                showHeap();
                return true;
            case KeyEvent.VK_R: // reset price connection
                client.disconnectFromServer();
                return true;
            case KeyEvent.VK_T: // reset trading connection
                client.disconnectFromTradingServer();
                return true;
            case KeyEvent.VK_H: // reset trading connection
                displayThreads();
                return true;
            case KeyEvent.VK_G: // call gc
                Runtime.getRuntime().gc();
                Runtime.getRuntime().gc();
                return true;
            case KeyEvent.VK_I: // show the active price connection ip
                String data = null;
                try {
                    data = "Active IP = " + Settings.getActiveIP() + " \n";
                    data += "Symbol Driven IP = " + InternationalConnector.getActiveIP() + " \n";
                    data += "Regional Content Server IP = " + Settings.getRegionalContentProviderIP() + " \n";
                    data += "Global Content Server IP = " + Settings.getGlobalContentProviderIP() + " \n";
                    Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
                    while (exchanges.hasMoreElements()) {
                        Exchange exchange = exchanges.nextElement();
                        data += exchange.getSymbol() + " Realtime path = " + exchange.getPath() + " , Content IP = " + exchange.getContentCMIP() + "\n";
                    }
                } catch (Exception e1) {
                    data = "Active IP = " + Settings.getActiveIP() + " \n";
                }
                new ShowMessage(data, "I");
                return true;
            case KeyEvent.VK_END: //show settings of the active internal frame
                showTableSettings(KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner());
                return true;
            case KeyEvent.VK_P: // show symbol preferences
                System.out.println("------- Preferences -------");
                DataStore.getSharedInstance().printPreferences();
                return true;
            case KeyEvent.VK_D: // show table preferences
                try {
                    Debug debug = new Debug();
                    debug.show();
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                return true;
            case KeyEvent.VK_O: // show object counters
                System.out.println(ObjectWatcher.dump());
                return true;
            case KeyEvent.VK_V: // show object counters
                showStockMasterVersions();
                return true;
            case KeyEvent.VK_A: // show object counters
                historyArchiveRegister();
                return true;
            case KeyEvent.VK_B: // show object counters
                HistoryDownloadManager.getSharedInstance().print();
                return true;
            case KeyEvent.VK_U: // show object counters
                RuleManager.getSharedInstance().showRules();
                return true;
            case KeyEvent.VK_0: // start customization mode
                if (Settings.isDeveloperMode()) {
                    Client.getInstance().enableCustomViewSettings();
                    Settings.enableCustomViewSettings();
                    SharedMethods.showMessage("<HTML><H1>Customisation Mode", JOptionPane.INFORMATION_MESSAGE);
                }
                return true;
            case KeyEvent.VK_L: // log mode
                startLogging();
                return true;
        }
        return false;
    }

    private boolean isMetaDown(KeyEvent e) {
        ctrl_down = ((e.getModifiersEx() & KeyEvent.CTRL_DOWN_MASK) == KeyEvent.CTRL_DOWN_MASK);
        boolean altDown = ((e.getModifiersEx() & KeyEvent.ALT_DOWN_MASK) == KeyEvent.ALT_DOWN_MASK) ||
                ((e.getModifiersEx() & KeyEvent.ALT_MASK) == KeyEvent.ALT_MASK);
        return (ctrl_down | altDown);
    }

    /*private void checkCTRLStatus(KeyEvent e){
		ctrl_down = ((e.getModifiersEx()& KeyEvent.CTRL_DOWN_MASK )==KeyEvent.CTRL_DOWN_MASK);
    }*/

    private boolean processALT_Key(int code) {
        try {
            switch (code) {
                case KeyEvent.VK_W:
                    WindowSelectorDialog ws = new WindowSelectorDialog();
                    if (ws.isListNull()) {
                        ws.dispose();
                        ws = null;
                    } else {
                        ws.setVisible(true);
                    }
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return false;
    }

    private boolean processCtrl_Shift_Key(int code) {
        try {
            switch (code) {
                case KeyEvent.VK_TAB:
                    try {
                        desktop.focusPreviousWindow(desktop.getListPosition(desktop.getSelectedFrame(), false, false), false);
                    }
                    catch (Exception ex1) {
                        desktop.focusNextWindow(9999, false);
                    }
                    return true;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return false;
    }

    private boolean processCTRL_Key(int code) {
        try {
            switch (code) {
                case KeyEvent.VK_TAB:
                    try {
                        desktop.focusNextWindow(desktop.getListPosition(desktop.getSelectedFrame(), false), false);
                    }
                    catch (Exception ex1) {
                        desktop.focusNextWindow(9999, false);
                    }
                    return true;
                case KeyEvent.VK_Q:
                    JInternalFrame frame = desktop.getSelectedFrame();
                    ((TWDesktopInterface) frame).closeWindow();
                    frame = null;
                    break;
                case KeyEvent.VK_W:
                    WindowSelectorDialog ws = new WindowSelectorDialog();
                    if (ws.isListNull()) {
                        ws.dispose();
                        ws = null;
                    } else {
                        ws.setVisible(true);
                    }
                    break;
                case KeyEvent.VK_Z:
                    System.out.println("Undo action raised.....");
                    TWDesktop.showClossedFrames();
                    break;

            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private String getEnteredSymbol() throws Exception {
        return getEnteredSymbol(false);
    }

    private String getEnteredSymbol(boolean ignoreNonDefaultExchanges) throws Exception {
        String symbol = null;
        String exchange = null;
        int instrument = -1;
        if (client.symbolBar.getSymbol().equals("")) {
            getFocusOwner();
            if (tableSelected) {
                try {
                    int row = selectedTable.getSelectedRow();
                    symbol = ((StringTransferObject) selectedTable.getModel().getValueAt(row, -1)).getValue();
                    exchange = ((StringTransferObject) selectedTable.getModel().getValueAt(row, -2)).getValue();
                    instrument = (int) ((LongTransferObject) selectedTable.getModel().getValueAt(row, -11)).getValue();
                } catch (Exception ex) {
                    ex.printStackTrace();
                }
            }
            if ((symbol != null) && (!symbol.trim().equals("")) && (exchange != null) && (!exchange.trim().equals(""))) {
                return SharedMethods.getKey(exchange, symbol, instrument);
            }
            throw new Exception("No Symbol");

        } else {
            if ((ExchangeStore.isNoneDefaultExchangesAvailable())) { // must validate from server
                if (ignoreNonDefaultExchanges) {
                    if (!isValidSymbol(client.symbolBar.getSymbol())) {
                        showInvalidSymbolMessage(client.symbolBar.getSymbol());
                        throw new Exception("Invalid Symbol");
                    } else {
                        return client.symbolBar.getSymbol();
                    }
                } else {
                    return null;
                }
            } else {
//                String key = SymbolMaster.getExchangeForSymbol(client.symbolBar.getSymbol(), false);
                String key = DataStore.getSharedInstance().findKeyFromDataStore(client.symbolBar.getSymbol());
                if (key == null) {
                    showInvalidSymbolMessage(client.symbolBar.getSymbol());
                    throw new Exception("Invalid Symbol");
                } else {
                    return key;
//                    return client.symbolBar.getSymbol();
                }
                /*if (!isValidSymbol(client.symbolBar.getSymbol())) {
                    throw new Exception("Invalid Symbol");
                } else {
                    return client.symbolBar.getSymbol();
                }*/
            }
        }
    }

    private void showInvalidSymbolMessage(String symbol) {
        JOptionPane.showMessageDialog(client.getFrame(), Language.getString("INVALID_SYMBOL") + " " +
                symbol, Language.getString("ERROR"), JOptionPane.OK_OPTION);
    }

    private boolean isValidSymbol(String symbol) {
        if (DataStore.getSharedInstance().findKeyFromDataStore(symbol.toUpperCase()) == null) {
            showInvalidSymbolMessage(symbol);
            /*JOptionPane.showMessageDialog(client.getFrame(), Language.getString("INVALID_SYMBOL") +
                    symbol, Language.getString("ERROR"), JOptionPane.OK_OPTION);*/
            return false;
        }
        return true;
    }

    private void hideSymbolBar() {
        client.symbolBar.setVisible(false);
    }

    private void getFocusOwner() {
        if (KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner() instanceof com.isi.csvr.dnd.DraggableTable) {
            tableSelected = true;
            selectedTable = (DraggableTable) KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
        } else {
            tableSelected = false;
        }
    }

    private void showChart() throws Exception {
        if (client.symbolBar.getSymbol().equals("")) {
            String symbol = Client.getInstance().getSelectedSymbol();

            if (!symbol.equals(""))
                client.showChart(symbol, null, null);
            symbol = null;
        } else if (!isValidSymbol(client.symbolBar.getSymbol())) {
            throw new Exception("Invalid Symbol");
        } else {
            String symbol = DataStore.getSharedInstance().findKey(client.symbolBar.getSymbol());
            client.showChart(symbol, null, null);
        }
    }

    public static boolean isDragPermited() {
        return ctrl_down;
    }

    public static boolean isDragPermited(Object source, DraggableTable dt) {
        try {
            if ((source != dt)) {
                return true;
            } else {
                return ctrl_down;
            }
        } catch (Exception e) {
            return ctrl_down;
        }
    }

    public static void setDragBlocked() {
        ctrl_down = false;
    }

    private void showTableSettings(Component component) {
        if (component instanceof JTable) {
//            if ((adminKeyCounter % 3) == 0) {
            JTable table = (JTable) component;
            int colCount = table.getColumnCount();
            int colIndex = 0;
            StringBuilder stringBuilder = new StringBuilder();
            stringBuilder.append("Order   : ");
            for (int i = 0; i < colCount; i++) {
                TableColumn oCol = table.getColumn("" + i);
                stringBuilder.append(i);
                stringBuilder.append(",");
                stringBuilder.append(oCol.getWidth());
                stringBuilder.append(",");
                stringBuilder.append(table.convertColumnIndexToView(i));
                stringBuilder.append(",");

                if (oCol.getWidth() > 0) {
                    colIndex += Math.pow(2, i);
                }
                oCol = null;
            }

            stringBuilder.append("\nSelected : ");
            stringBuilder.append(colIndex);

            Component parent = component.getParent();
            int index = 0;
            while (true) {
                index++;
                try {
                    if (parent instanceof JInternalFrame) {
                        break;
                    }
                    parent = parent.getParent();
                } catch (Exception e) {
                    parent = null;
                    break;
                }

                if (index >= 200) { // safety break
                    break;
                }
            }
            if (parent != null) {
                stringBuilder.append("\nFrame Size : ");
                stringBuilder.append(parent.getSize());
                stringBuilder.append("\nFrame Location : ");
                stringBuilder.append(parent.getLocation());
                stringBuilder.append("\n Body Font : ");
                stringBuilder.append(table.getFont());
                stringBuilder.append("\n Header Font : ");
                stringBuilder.append(table.getTableHeader().getFont());
            }


            JTextArea textArea = new JTextArea(stringBuilder.toString());
            textArea.setPreferredSize(new Dimension(500, 200));
            textArea.setLineWrap(true);

            JOptionPane.showMessageDialog(client.getFrame(), textArea);
            //}
            adminKeyCounter++;
        } else {
            Point point = MouseInfo.getPointerInfo().getLocation();
            SwingUtilities.convertPointFromScreen(point, component);
            Component comp = SwingUtilities.getDeepestComponentAt(component, (int) point.getX(), (int) point.getY());
            System.out.println(comp);
            //JOptionPane.showMessageDialog(client.getFrame(), "Image : " + ((TWMenuItem)component).getIconFile());
        }
    }

    private void displayThreads() {
        Thread[] threads = new Thread[Thread.activeCount()];

        Thread.enumerate(threads);
        System.out.println("----------- Threads ----------");
        for (int i = 0; i < threads.length; i++) {
            System.out.printf("%2d %s\n", i + 1, threads[i].getName());
        }
        System.out.println("------------------------------");
    }

    private void showStockMasterVersions() {
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            //if (exchange.isDefault()) {
            System.out.println("Exchange         " + exchange.getSymbol());
            System.out.println("Stock Master     " + SymbolMaster.getVersion(exchange.getSymbol()));
            System.out.println("Last Eod Date    " + exchange.getLastEODDate());
            System.out.println("Current Eod Date " + exchange.getCurrentEODDate());
            System.out.println("Country          " + exchange.getCountry());
            System.out.println("Market Date      " + SharedMethods.toFulldateFormat(exchange.getMarketDate()));
            System.out.println("Time Zone        " + exchange.getTimeZoneID());
            System.out.println("After EOD        " + exchange.hasEodRun());
            System.out.println("CCM IP           " + exchange.getContentCMIP());
            System.out.println("-------------------------------");
            //}
            exchange = null;
        }

    }

    private void historyArchiveRegister() {
        System.out.println(HistoryArchiveRegister.getSharedInstance().toString());
    }

    private void showHeap() {
        try {
            double totHInit = 0;
            double totHCommit = 0;
            double totHUsed = 0;
            double totHMax = 0;
            double totMInit = 0;
            double totMCommit = 0;
            double totMUsed = 0;
            double totMMax = 0;

            java.util.List<MemoryPoolMXBean> mpools = ManagementFactory.getMemoryPoolMXBeans();

            System.out.printf("-------------------------------------------\n\r");
            System.out.printf("Name                init  used  comitd  max\n\r");
            System.out.printf("-------------------------------------------\n\r");
            for (MemoryPoolMXBean mp : mpools) {
                System.out.printf("%-18s %5.1f %5.1f %5.1f %6.1f\n\r", mp.getName(), mp.getUsage().getInit() / 1048576D, mp.getUsage().getUsed() / 1048576D, mp.getUsage().getCommitted() / 1048576D, mp.getUsage().getMax() / 1048576D);
                if (mp.getType() == MemoryType.HEAP) {
                    totHInit += mp.getUsage().getInit();
                    totHUsed += mp.getUsage().getUsed();
                    totHCommit += mp.getUsage().getCommitted();
                    totHMax += mp.getUsage().getMax();
                } else {
                    totMInit += mp.getUsage().getInit();
                    totMUsed += mp.getUsage().getUsed();
                    totMCommit += mp.getUsage().getCommitted();
                    totMMax += mp.getUsage().getMax();
                }
            }
            System.out.printf("-------------------------------------------\n\r");
            System.out.printf("%-18s %5.1f %5.1f %5.1f %6.1f\n\r", "Total Heap", totHInit / 1048576D, totHUsed / 1048576D, totHCommit / 1048576D, totHMax / 1048576D);
            System.out.printf("%-18s %5.1f %5.1f %5.1f %6.1f\n\r", "Total NonHeap", totMInit / 1048576D, totMUsed / 1048576D, totMCommit / 1048576D, totMMax / 1048576D);
            System.out.printf("-------------------------------------------\n\r");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean logingOn = false;

    private void startLogging() {
        if (!logingOn) {
            try {
                if (Settings.isDeveloperMode()) {
                    System.setOut(new PrintStream(new FileOutputStream("out.txt")));
                    System.setErr(new PrintStream(new FileOutputStream("err.txt")));
                    logingOn = true;
                    Toolkit.getDefaultToolkit().beep();
                }
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
        }
    }

    public String getActionKeyForID(int id) {
        if (idMappingTable.containsValue(id)) {
            Enumeration<String> keys = idMappingTable.keys();
            String key;
            String function;
            while (keys.hasMoreElements()) {
                key = keys.nextElement();
                if (idMappingTable.get(key) == id) {
                    function = key;
                    String acc = keyMappingTable.get(function);
                    return acc;
                }
            }
        }

        return "";

/*
        if (adminTable.containsValue(id)) {
            Enumeration<Integer> keys = adminTable.keys();
            int key;
            while (keys.hasMoreElements()) {
                key = keys.nextElement();
                if (adminTable.get(key) == id) {
                    return "CTRL + ALT + SHIFT + " + KeyEvent.getKeyText(key);
                }

            }
        } else if (altShiftTable.containsValue(id)) {
            Enumeration<Integer> keys = altShiftTable.keys();
            int key;
            while (keys.hasMoreElements()) {
                key = keys.nextElement();
                if (altShiftTable.get(key) == id) {
                    return "ALT + SHIFT + " + KeyEvent.getKeyText(key);
                }
            }
        } else if (altCtrlTable.containsValue(id)) {
            Enumeration<Integer> keys = altCtrlTable.keys();
            int key;
            while (keys.hasMoreElements()) {
                key = keys.nextElement();
                if (altCtrlTable.get(key) == id) {
                    return "CTRL + ALT + " + KeyEvent.getKeyText(key);
                }
            }
        } else if (ctrlShiftTable.containsValue(id)) {
            Enumeration<Integer> keys = ctrlShiftTable.keys();
            int key;
            while (keys.hasMoreElements()) {
                key = keys.nextElement();
                if (ctrlShiftTable.get(key) == id) {
                    return "CTRL + SHIFT + " + KeyEvent.getKeyText(key);
                }
            }
        } else if (ctrlTable.containsValue(id)) {
            Enumeration<Integer> keys = ctrlTable.keys();
            int key;
            while (keys.hasMoreElements()) {
                key = keys.nextElement();
                if (ctrlTable.get(key) == id) {
                    return "CTRL + " + KeyEvent.getKeyText(key);
                }
            }
        } else if (altTable.containsValue(id)) {
            Enumeration<Integer> keys = altTable.keys();
            int key;
            while (keys.hasMoreElements()) {
                key = keys.nextElement();
                if (altTable.get(key) == id) {
                    return "ALT + " + KeyEvent.getKeyText(key);
                }
            }
        } else if (shiftTable.containsValue(id)) {
            Enumeration<Integer> keys = shiftTable.keys();
            int key;
            while (keys.hasMoreElements()) {
                key = keys.nextElement();
                if (shiftTable.get(key) == id) {
                    return "SHIFT + " + KeyEvent.getKeyText(key);
                }
            }
        } else if (normalTable.containsValue(id)) {
            Enumeration<Integer> keys = normalTable.keys();
            int key;
            while (keys.hasMoreElements()) {
                key = keys.nextElement();
                if (normalTable.get(key) == id) {
                    return " " + KeyEvent.getKeyText(key);
                }
            }
        }

        return "";
*/
    }

    public void saveShortCuts() throws IOException {
        FileOutputStream oOut = new FileOutputStream(Settings.SYSTEM_PATH + "/shortcuts.dll");
//        FileOutputStream oOut = new FileOutputStream("C:/shortcuts4.dll");
        Enumeration keys = keyMappingTable.keys();
        while (keys.hasMoreElements()) {
            String tag = (String) keys.nextElement();
            String shortKey = TWActions.keyMappingTable.get(tag);
            shortKey = shortKey.replace(' ', '_');
            oOut.write((tag + "=" + shortKey + "\n").getBytes());
        }
        oOut.close();
        clearShortcutTables();
        loadShortCuts();

    }

    public void saveMouseGestures() {

        try {
            FileOutputStream oOut = new FileOutputStream(Settings.SYSTEM_PATH + "/mousegestures.dll");
//            FileOutputStream oOut = new FileOutputStream("C:/mousegestures1.dll");
            Enumeration keys = mgMappingsTable.keys();
            while (keys.hasMoreElements()) {
                String tag = (String) keys.nextElement();
                String shortKey = TWActions.mgMappingsTable.get(tag);
                oOut.write((tag + "=" + shortKey + "\n").getBytes());

            }
            oOut.close();
            clearMouseGestureTables();
            loadMouseGestures();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }


}