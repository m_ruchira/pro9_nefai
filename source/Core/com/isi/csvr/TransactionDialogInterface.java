package com.isi.csvr;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 5, 2005
 * Time: 11:30:47 AM
 */
public interface TransactionDialogInterface {
    void setSymbol(String key);

    void notifyInvalidSymbol(String symbol);
}
