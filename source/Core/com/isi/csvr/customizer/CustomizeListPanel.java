package com.isi.csvr.customizer;

import com.isi.csvr.Client;
import com.isi.csvr.FontChooser;
import com.isi.csvr.TableSorter;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.Table;
import com.isi.csvr.table.TWTableRenderer;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.TableThemeSettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.Vector;
import java.util.ArrayList;
//import com.isi.csvr.table.Table;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author Bandula
 * @version 1.0
 */

public class CustomizeListPanel extends JPanel implements MouseListener, ActionListener, CustomizerConstats {

    public static final byte COLUMN_FOREGROUND = 0;
    public static final byte COLUMN_BACKGROUND = 2;
    public static final byte COLUMN_GRID_COLOR = 4;


    private int WIDTH = 210; //185;

    private JColorChooser colorChooser;
    private JDialog colorDialog;
    private FontChooser fontChooser;
    private JDialog parent;
    private CommonTableSettings target;
    private Table table;
    private SmartTable smartTable;
    private JPanel centerPanel = new JPanel(new BorderLayout());
    private JPanel centerNorthPanel;
    private JList colorConfigList;
    private JScrollPane scrollPane;
    private Vector dataModel;
    private CustomizeListRenderer renderer;

    private JPanel fontPanel = new JPanel();
    private JButton btnBodyFont = new JButton();
    private JButton btnHeadingFont = new JButton();
    private JLabel lblBodyFont = new JLabel();
    private JLabel lblHeadingFont = new JLabel();
    private JLabel lblGridColor = new JLabel();
    private JButton btnCLose = new JButton();
    private JCheckBox chkEnable = new JCheckBox();
//    private JCheckBox chkGrid = new JCheckBox();
    private ArrayList<TWComboItem> gridTypes;
    private TWComboBox comGrid;
    private JLabel lblGrid = new JLabel();

    private Font oldFont;
    private Font newBodyFont;
    private Font newHeadingFont;
    private Color oldColor;
    private Color newColor;
    private CustomizerRecord selectedObject;
    private byte selectedMode;
    private int selectedIndex;
    private int oldBodyGap;
    private int newBodyGap;
    private JLabel lblBodyGap = new JLabel();
    private TWComboBox btnBodyGap = null;

    public CustomizeListPanel(JDialog parent) {
        try {
            this.parent = parent;
            jbInit();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.setLayout(new BorderLayout());

        JLabel previewPane = new JLabel(Language.getString("CLIENT"));
        previewPane.setFont(new TWFont("Arial", Font.BOLD, 20));
        colorChooser = new JColorChooser(Color.red);
        colorChooser.setPreviewPanel(previewPane);
        colorDialog = JColorChooser.createDialog(Client.getInstance().getFrame(), Language.getString("SELECT_COLOR"), true, colorChooser, this, this);

        fontChooser = new FontChooser(Client.getInstance().getFrame(), true);
        fontPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        fontPanel.setBorder(BorderFactory.createEtchedBorder());
        fontPanel.setPreferredSize(new Dimension(WIDTH, 140));   // 70
        btnBodyFont.setPreferredSize(new Dimension(20, 20));
        btnBodyFont.setActionCommand("BodyFont");
        btnBodyFont.setText("...");
        btnBodyFont.setActionCommand("FONT_BODY");
        btnBodyFont.addActionListener(this);
        btnHeadingFont.setPreferredSize(new Dimension(20, 20));
        btnHeadingFont.setToolTipText("");
        btnHeadingFont.setActionCommand("HeadFont");
        btnHeadingFont.setText("...");
        btnHeadingFont.setActionCommand("FONT_HEADING");
        btnHeadingFont.addActionListener(this);
        lblBodyFont.setPreferredSize(new Dimension(205, 20));
        lblBodyFont.setText(Language.getString("BODY_FONT"));
        lblHeadingFont.setPreferredSize(new Dimension(205, 20));
        lblHeadingFont.setText(Language.getString("HEADING_FONT"));

        lblBodyGap.setPreferredSize(new Dimension(180, 20));
        lblBodyGap.setText(Language.getString("BODY_GAP"));
        ArrayList elements = new ArrayList();
        for(int i=0; i<11; i++){
            elements.add(i);
        }
        TWComboModel model = new TWComboModel(elements);
        btnBodyGap = new TWComboBox(model);
        btnBodyGap.setPreferredSize(new Dimension(50, 20));
        btnBodyGap.setToolTipText("");
        btnBodyGap.setActionCommand("BODY_GAP");
        btnBodyGap.addActionListener(this);
//        lblBodyFont.setHorizontalAlignment(JLabel.CENTER);
//        lblHeadingFont.setHorizontalAlignment(JLabel.CENTER);

        chkEnable.setPreferredSize(new Dimension(WIDTH, 25));
        chkEnable.setText(Language.getString("ENABLE_CUSTOMIZER_CUSTOM_SETTINGS"));
        chkEnable.addActionListener(this);
        chkEnable.setActionCommand("ENABLE");

        JLabel nullLabel1 = new JLabel(" ");

//        chkGrid.setPreferredSize(new Dimension(WIDTH, 20));
//        chkGrid.setText(Language.getString("ENABLE_GRID"));
//        chkGrid.setActionCommand("ENABLE_GRID");
//        chkGrid.addActionListener(this);

        lblGrid.setText(Language.getString("GRID"));
        lblGrid.setFont(lblGrid.getFont().deriveFont(Font.BOLD));
        lblGrid.setPreferredSize(new Dimension(110, 20));
        gridTypes = new ArrayList<TWComboItem>();
        gridTypes.add(new TWComboItem(TableThemeSettings.GRID_BOTH,Language.getString("CO_BOTH")));
        gridTypes.add(new TWComboItem(TableThemeSettings.GRID_VERTICAL,Language.getString("VERTICLE")));
        gridTypes.add(new TWComboItem(TableThemeSettings.GRID_HORIZONTAL, Language.getString("HORIZONTAL")));
        gridTypes.add(new TWComboItem(TableThemeSettings.GRID_NONE,Language.getString("NONE")));
        comGrid = new TWComboBox(new TWComboModel(gridTypes));
        comGrid.setPreferredSize(new Dimension(100, 20));
        comGrid.setActionCommand("GRID_TYPE");
        comGrid.setSelectedIndex(0);
        comGrid.addActionListener(this);

        lblGridColor.setPreferredSize(new Dimension(60, 20));
        lblGridColor.addMouseListener(this);
        lblGridColor.setOpaque(true);
        lblGridColor.setBorder(BorderFactory.createLineBorder(Color.black));

//        this.add(fontPanel, null);
        fontPanel.add(lblBodyFont, null);
        fontPanel.add(btnBodyFont, null);
        fontPanel.add(lblHeadingFont, null);
        fontPanel.add(btnHeadingFont, null);
        fontPanel.add(lblBodyGap, null);
        fontPanel.add(btnBodyGap, null);
        fontPanel.add(chkEnable, null);
        //fontPanel.add(nullLabel1, null);
//        fontPanel.add(chkGrid, null);

        fontPanel.add(lblGrid);
        fontPanel.add(comGrid);
//        colorPanel.add(btnGrid);

        fontPanel.add(lblGridColor, null);

        centerNorthPanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 2));
        JLabel lblName = new JLabel(Language.getString("SETTING"));
        lblName.setHorizontalAlignment(JLabel.CENTER);

        JLabel lblBGColor = new JLabel(Language.getString("CELL_COLOR"));
        lblBGColor.setHorizontalAlignment(JLabel.CENTER);
        JLabel lblFGColor = new JLabel(Language.getString("FONT_COLOR"));
        lblFGColor.setHorizontalAlignment(JLabel.CENTER);
//        JLabel lblBGColor2 = new JLabel(Language.getString("CELL_COLOR"));
//        lblBGColor2.setHorizontalAlignment(JLabel.CENTER);
//        JLabel lblFGColor2 = new JLabel(Language.getString("FONT_COLOR"));
//        lblFGColor2.setHorizontalAlignment(JLabel.CENTER);

        lblName.setPreferredSize(new Dimension(150, 20));
        lblBGColor.setPreferredSize(new Dimension(60, 20));
        lblFGColor.setPreferredSize(new Dimension(60, 20));
//        lblBGColor2.setPreferredSize(new Dimension(60, 20));
//        lblFGColor2.setPreferredSize(new Dimension(60, 20));

        centerNorthPanel.add(lblName);
        centerNorthPanel.add(lblBGColor);
        centerNorthPanel.add(lblFGColor);
//        centerNorthPanel.add(lblBGColor2);
//        centerNorthPanel.add(lblFGColor2);

        dataModel = new Vector();

        colorConfigList = new JList(dataModel);
        colorConfigList.addMouseListener(this);
        renderer = new CustomizeListRenderer();
        colorConfigList.setCellRenderer(renderer);
//        colorConfigList.setCellRenderer(new CustomizeListRenderer());
        scrollPane = new JScrollPane(colorConfigList);
//        centerPanel.add(scrollPane, BorderLayout.CENTER);

        centerPanel.add(centerNorthPanel, BorderLayout.NORTH);
        centerPanel.add(scrollPane, BorderLayout.CENTER);

        JPanel closePanel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 2, 5));
        btnCLose.setText(Language.getString("CLOSE"));
        btnCLose.addActionListener(this);
        btnCLose.setActionCommand("CLOSE");
        closePanel.add(btnCLose);

        this.add(fontPanel, BorderLayout.NORTH);
        this.add(centerPanel, BorderLayout.CENTER);
        this.add(closePanel, BorderLayout.SOUTH);

        this.setPreferredSize(new Dimension(300, 400));

        GUISettings.applyOrientation(this);
    }

    public void setTarget(Table sTable) {
        this.table = sTable;
        this.smartTable = sTable.getSmartTable();
        this.target = (CommonTableSettings) smartTable.getTableSettings();
        loadInitialSettings();
    }

    private void loadInitialSettings() {
        dataModel.clear();
        Customizeable customizeable;
        if (smartTable.getModel() instanceof TableSorter) {
            customizeable = ((Customizeable) ((TableSorter) smartTable.getModel()).getModel());
        } else {
            customizeable = ((Customizeable) smartTable.getModel());
        }

        int type;
        CustomizerRecord record;
        CustomizerRecord[] customizerRecords = customizeable.getCustomizerRecords();
        for (int i = 0; i < customizerRecords.length; i++) {
            record = customizerRecords[i];
            dataModel.addElement(record);
            type = record.getType();
            switch (type) {
                case FIELD_BASIC_ROW1 :
//                    applyInitialColor(record.getBgColor1(),target.getRowColor1BG());
                    if (isNullColor(target.getRowColor1BG()))
                        target.setRowColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getRowColor1BG());
//                    applyInitialColor(record.getFgColor1(),target.getRowColor1FG());
                    if (isNullColor(target.getRowColor1FG()))
                        target.setRowColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getRowColor1FG());
                    break;
                case FIELD_BASIC_ROW2 :
//                    applyInitialColor(record.getBgColor1(),target.getRowColor2BG());
                    if (isNullColor(target.getRowColor2BG()))
                        target.setRowColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getRowColor2BG());
//                    applyInitialColor(record.getFgColor1(),target.getRowColor2FG());
                    if (isNullColor(target.getRowColor2FG()))
                        target.setRowColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getRowColor2FG());
                    break;
                case FIELD_BID_ROW1 :
//                    applyInitialColor(record.getBgColor(),target.getBidColor1BG());
                    if (isNullColor(target.getBidColor1BG()))
                        target.setBidColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getBidColor1BG());
//                    applyInitialColor(record.getFgColor(),target.getBidColor1FG());
                    if (isNullColor(target.getBidColor1FG()))
                        target.setBidColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getBidColor1FG());
                    break;
                case FIELD_BID_ROW2 :
//                    applyInitialColor(record.getBgColor(),target.getBidColor2BG());
                    if (isNullColor(target.getBidColor2BG()))
                        target.setBidColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getBidColor2BG());
//                    applyInitialColor(record.getFgColor(),target.getBidColor2FG());
                    if (isNullColor(target.getBidColor2FG()))
                        target.setBidColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getBidColor2FG());
                    break;
                case FIELD_ASK_ROW1 :
//                    applyInitialColor(record.getBgColor(),target.getAskColor1BG());
                    if (isNullColor(target.getAskColor1BG()))
                        target.setAskColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getAskColor1BG());
//                    applyInitialColor(record.getFgColor(),target.getAskColor1FG());
                    if (isNullColor(target.getAskColor1FG()))
                        target.setAskColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getAskColor1FG());
                    break;
                case FIELD_ASK_ROW2 :
//                    applyInitialColor(record.getBgColor(),target.getAskColor2BG());
                    if (isNullColor(target.getAskColor2BG()))
                        target.setAskColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getAskColor2BG());
//                    applyInitialColor(record.getFgColor(),target.getAskColor2FG());
                    if (isNullColor(target.getAskColor2FG()))
                        target.setAskColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getAskColor2FG());
                    break;
                case FIELD_SELECTED_ROW :
//                    applyInitialColor(record.getBgColor(),target.getSelectedColumnBG());
                    if (isNullColor(target.getSelectedColumnBG()))
                        target.setSelectedColumnBG(record.getBgColor());
                    else
                        record.setBgColor(target.getSelectedColumnBG());
//                    applyInitialColor(record.getFgColor(),target.getSelectedColumnFG());
                    if (isNullColor(target.getSelectedColumnFG()))
                        target.setSelectedColumnFG(record.getFgColor());
                    else
                        record.setFgColor(target.getSelectedColumnFG());
                    break;
                case FIELD_HEADER_COLOR_ROW :
//                    applyInitialColor(record.getBgColor(),target.getHeaderColorBG());
                    if (isNullColor(target.getHeaderColorBG()))
                        target.setHeaderColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getHeaderColorBG());
//                    applyInitialColor(record.getFgColor(),target.getHeaderColorFG());
                    if (isNullColor(target.getHeaderColorFG()))
                        target.setHeaderColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getHeaderColorFG());
                    break;
                case FIELD_SMALLTRADE_ROW :
                    record.setBgColor(null);
//                    applyInitialColor(record.getFgColor(),target.getSmallTradeFG());
                    if (isNullColor(target.getSmallTradeFG()))
                        target.setSmallTradeFG(record.getFgColor());
                    else
                        record.setFgColor(target.getSmallTradeFG());
                    break;
                case FIELD_VALUE_UP_ROW :
//                    applyInitialColor(record.getBgColor(),target.getCellHighLightedUpBG());
                    if (isNullColor(target.getCellHighLightedUpBG()))
                        target.setCellHighLightedUpBG(record.getBgColor());
                    else
                        record.setBgColor(target.getCellHighLightedUpBG());
//                    applyInitialColor(record.getFgColor(),target.getCellHighLightedUpFG());
                    if (isNullColor(target.getCellHighLightedUpFG()))
                        target.setCellHighLightedUpFG(record.getFgColor());
                    else
                        record.setFgColor(target.getCellHighLightedUpFG());
                    break;
                case FIELD_VALUE_DOWN_ROW :
                    if (isNullColor(target.getCellHighLightedDownBG()))
                        target.setCellHighLightedDownBG(record.getBgColor());
                    else
                        record.setBgColor(target.getCellHighLightedDownBG());
                    if (isNullColor(target.getCellHighLightedDownFG()))
                        target.setCellHighLightedDownFG(record.getFgColor());
                    else
                        record.setFgColor(target.getCellHighLightedDownFG());
                    break;
                case FIELD_POSITIVE_CHANGE_ROW :
                    record.setBgColor(null);
                    if (isNullColor(target.getPositiveChangeFG()))
                        target.setPositiveChangeFG(record.getFgColor());
                    else
                        record.setFgColor(target.getPositiveChangeFG());
                    break;
                case FIELD_NEGATIVE_CHANGE_ROW :
                    record.setBgColor(null);
                    if (isNullColor(target.getNegativeChangeFG()))
                        target.setNegativeChangeFG(record.getFgColor());
                    else
                        record.setFgColor(target.getNegativeChangeFG());
                    break;
                case FIELD_BUY:
                    if (isNullColor(target.getBuyColorBG()))
                        target.setBuyColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getBuyColorBG());
                    if (isNullColor(target.getBuyColorFG()))
                        target.setBuyColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getBuyColorFG());
                    break;
                case FIELD_SELL:
                    if (isNullColor(target.getSellColorBG()))
                        target.setSellColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getSellColorBG());
                    if (isNullColor(target.getSellColorFG()))
                        target.setSellColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getSellColorFG());
                    break;
                case FIELD_OPBAL:
                    if (isNullColor(target.getOpBalColorBG()))
                        target.setOpBalColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getOpBalColorBG());
                    if (isNullColor(target.getOpBalColorFG()))
                        target.setOpBalColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getOpBalColorFG());
                    break;
                case FIELD_DIVIDAND:
                    if (isNullColor(target.getDivColorBG()))
                        target.setDivColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getDivColorBG());
                    if (isNullColor(target.getDivColorFG()))
                        target.setDivColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getDivColorFG());
                    break;
                case FIELD_NEW_ANNOUNCEMENT:
                    if (isNullColor(target.getNewAnnouncementFG()))
                        target.setNewAnnouncementFG(record.getFgColor());
                    else
                        record.setFgColor(target.getNewAnnouncementFG());
                    break;
                case FIELD_COMBINED_ASK_ROW1:
                    if (isNullColor(target.getCombinedAskColor1BG()))
                        target.setCombinedAskColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getCombinedAskColor1BG());
                    if (isNullColor(target.getCombinedAskColor1FG()))
                        target.setCombinedAskColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getCombinedAskColor1FG());
                    break;
                case FIELD_COMBINED_ASK_ROW2:
                    if (isNullColor(target.getCombinedAskColor2BG()))
                        target.setCombinedAskColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getCombinedAskColor2BG());
                    if (isNullColor(target.getCombinedAskColor2FG()))
                        target.setCombinedAskColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getCombinedAskColor2FG());
                    break;
                case FIELD_COMBINED_BID_ROW1:
                    if (isNullColor(target.getCombinedBidColor1BG()))
                        target.setCombinedBidColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getCombinedBidColor1BG());
                    if (isNullColor(target.getCombinedBidColor1FG()))
                        target.setCombinedBidColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getCombinedBidColor1FG());
                    break;
                case FIELD_COMBINED_BID_ROW2:
                    if (isNullColor(target.getCombinedBidColor2BG()))
                        target.setCombinedBidColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getCombinedBidColor2BG());
                    if (isNullColor(target.getCombinedBidColor2FG()))
                        target.setCombinedBidColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getCombinedBidColor2FG());
                    break;
                //change start
                case FIELD_COMBINED_PRICE_ROW1:
                    if (isNullColor(target.getCombinedPriceColor1BG()))
                        target.setCombinedPriceColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getCombinedPriceColor1BG());
                    if (isNullColor(target.getCombinedPriceColor1FG()))
                        target.setCombinedPriceColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getCombinedPriceColor1FG());
                    break;
                case FIELD_COMBINED_PRICE_ROW2:
                    if (isNullColor(target.getCombinedPriceColor2BG()))
                        target.setCombinedPriceColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getCombinedPriceColor2BG());
                    if (isNullColor(target.getCombinedPriceColor2FG()))
                        target.setCombinedPriceColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getCombinedPriceColor2FG());
                    break;
                //change end
                case FIELD_COMBINED_SELECTED_ROW:
                    if (isNullColor(target.getCombinedSelectedColorBG()))
                        target.setCombinedSelectedColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getCombinedSelectedColorBG());
                    if (isNullColor(target.getCombinedSelectedColorFG()))
                        target.setCombinedSelectedColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getCombinedSelectedColorFG());
                    break;
                case FIELD_DEPOSITE_ROW1:
                    if (isNullColor(target.getDepositeColor1BG()))
                        target.setDepositeColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getDepositeColor1BG());
                    if (isNullColor(target.getDepositeColor1FG()))
                        target.setDepositeColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getDepositeColor1FG());
                    break;
                case FIELD_DEPOSITE_ROW2:
                    if (isNullColor(target.getDepositeColor2BG()))
                        target.setDepositeColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getDepositeColor2BG());
                    if (isNullColor(target.getDepositeColor2FG()))
                        target.setDepositeColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getDepositeColor2FG());
                    break;
                case FIELD_WITHDRAW_ROW1:
                    if (isNullColor(target.getWithdrawColor1BG()))
                        target.setWithdrawColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getWithdrawColor1BG());
                    if (isNullColor(target.getWithdrawColor1FG()))
                        target.setWithdrawColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getWithdrawColor1FG());
                    break;
                case FIELD_WITHDRAW_ROW2:
                    if (isNullColor(target.getWithdrawColor2BG()))
                        target.setWithdrawColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getWithdrawColor2BG());
                    if (isNullColor(target.getWithdrawColor2FG()))
                        target.setWithdrawColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getWithdrawColor2FG());
                    break;
                //change start
                case FIELD_TIMENSALES_ROW1:
                    if (isNullColor(target.getTimeNSalesRowColor1BG()))
                        target.setTimeNSalesRowColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getTimeNSalesRowColor1BG());
                    if (isNullColor(target.getTimeNSalesRowColor1FG()))
                        target.setTimeNSalesRowColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getTimeNSalesRowColor1FG());
                    break;
                case FIELD_TIMENSALES_ROW2:
                    if (isNullColor(target.getTimeNSalesRowColor2BG()))
                        target.setTimeNSalesRowColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getTimeNSalesRowColor2BG());
                    if (isNullColor(target.getTimeNSalesRowColor2FG()))
                        target.setTimeNSalesRowColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getTimeNSalesRowColor2FG());
                    break;
                case FIELD_TIMENSALES_SELECTED_ROW:
                    if (isNullColor(target.getTimeNSalesSelectedColorBG()))
                        target.setTimeNSalesSelectedColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getTimeNSalesSelectedColorBG());
                    if (isNullColor(target.getTimeNSalesSelectedColorFG()))
                        target.setTimeNSalesSelectedColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getTimeNSalesSelectedColorFG());
                    break;
                 case FIELD_DEP_BY_ORD_ASK_ROW1:
                    if (isNullColor(target.getDepthOrderAskColor1BG()))
                        target.setDepthOrderAskColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getDepthOrderAskColor1BG());
                    if (isNullColor(target.getDepthOrderAskColor1FG()))
                        target.setDepthOrderAskColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getDepthOrderAskColor1FG());
                    break;
                case FIELD_DEP_BY_ORD_ASK_ROW2:
                    if (isNullColor(target.getDepthOrderAskColor2BG()))
                        target.setDepthOrderAskColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getDepthOrderAskColor2BG());
                    if (isNullColor(target.getDepthOrderAskColor2FG()))
                        target.setDepthOrderAskColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getDepthOrderAskColor2FG());
                    break;
                case FIELD_DEP_BY_ORD_BID_ROW1:
                    if (isNullColor(target.getDepthOrderBidColor1BG()))
                        target.setDepthOrderBidColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getDepthOrderBidColor1BG());
                    if (isNullColor(target.getDepthOrderBidColor1FG()))
                        target.setDepthOrderBidColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getDepthOrderBidColor1FG());
                    break;
                case FIELD_DEP_BY_ORD_BID_ROW2:
                    if (isNullColor(target.getDepthOrderBidColor2BG()))
                        target.setDepthOrderBidColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getDepthOrderBidColor2BG());
                    if (isNullColor(target.getDepthOrderBidColor2FG()))
                        target.setDepthOrderBidColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getDepthOrderBidColor2FG());
                    break;
                case FIELD_DEP_BY_ORD_SELECTED_ROW:
                    if (isNullColor(target.getDepthOrderSelectedColorBG()))
                        target.setDepthOrderSelectedColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getDepthOrderSelectedColorBG());
                    if (isNullColor(target.getDepthOrderSelectedColorFG()))
                        target.setDepthOrderSelectedColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getDepthOrderSelectedColorFG());
                    break;
                case FIELD_CONDITIONAL_BUY:
                    if (isNullColor(target.getConditionBuyColorBG()))
                        target.setConditionBuyColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getConditionBuyColorBG());
                    if (isNullColor(target.getConditionBuyColorFG()))
                        target.setConditionBuyColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getConditionBuyColorFG());
                    break;
                case FIELD_CONDITIONAL_SELL:
                    if (isNullColor(target.getConditionSellColorBG()))
                        target.setConditionSellColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getConditionSellColorBG());
                    if (isNullColor(target.getConditionSellColorFG()))
                        target.setConditionSellColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getConditionSellColorFG());
                    break;
                //change end
                case FIELD_DEPOSITE:
                    if (isNullColor(target.getDeposColorBG()))
                        target.setDeposColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getDeposColorBG());
                    if (isNullColor(target.getDeposColorFG()))
                        target.setDeposColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getDeposColorFG());
                    break;
                case FIELD_WITHDRAW:
                    if (isNullColor(target.getWithdraColorBG()))
                        target.setWithdraColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getWithdraColorBG());
                    if (isNullColor(target.getWithdraColorFG()))
                        target.setWithdraColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getWithdraColorFG());
                    break;
                case FIELD_CHARGES:
                    if (isNullColor(target.getChargesColorBG()))
                        target.setChargesColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getChargesColorBG());
                    if (isNullColor(target.getChargesColorFG()))
                        target.setChargesColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getChargesColorFG());
                    break;
                case FIELD_REFUNDS:
                    if (isNullColor(target.getRefundColorBG()))
                        target.setRefundColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getRefundColorBG());
                    if (isNullColor(target.getRefundColorFG()))
                        target.setRefundColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getRefundColorFG());
                    break;
                case FIELD_NEWS_HOT_NEWS:
                    if (isNullColor(target.getNewsHotNewsColorFG()))
                        target.setNewsHotNewsColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getNewsHotNewsColorFG());
                    break;
                case FIELD_NEWS_TEMPORARY_NEWS:
                    if (isNullColor(target.getNewsTemporaryNewsColorBG()))
                        target.setNewsTemporaryNewsColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getNewsTemporaryNewsColorBG());
                    break;
                case FIELD_OPTION_CHAIN_STRIKE_ROW1:
                    if (isNullColor(target.getOptionChainStrikeColor1BG()))
                        target.setOptionChainStrikeColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getOptionChainStrikeColor1BG());
                    if (isNullColor(target.getOptionChainStrikeColor1FG()))
                        target.setOptionChainStrikeColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getOptionChainStrikeColor1FG());
                    break;
                case FIELD_OPTION_CHAIN_STRIKE_ROW2:
                    if (isNullColor(target.getOptionChainStrikeColor2BG()))
                        target.setOptionChainStrikeColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getOptionChainStrikeColor2BG());
                    if (isNullColor(target.getOptionChainStrikeColor2FG()))
                        target.setOptionChainStrikeColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getOptionChainStrikeColor2FG());
                    break;
                case FIELD_OPTION_CHAIN_CALLS_ROW1:
                    if (isNullColor(target.getOptionChainCallColor1BG()))
                        target.setOptionChainCallColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getOptionChainCallColor1BG());
                    if (isNullColor(target.getOptionChainCallColor1FG()))
                        target.setOptionChainCallColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getOptionChainCallColor1FG());
                    break;
                case FIELD_OPTION_CHAIN_CALLS_ROW2:
                    if (isNullColor(target.getOptionChainCallColor2BG()))
                        target.setOptionChainCallColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getOptionChainCallColor2BG());
                    if (isNullColor(target.getOptionChainCallColor2FG()))
                        target.setOptionChainCallColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getOptionChainCallColor2FG());
                    break;
                case FIELD_OPTION_CHAIN_PUTS_ROW1:
                    if (isNullColor(target.getOptionChainPutColor1BG()))
                        target.setOptionChainPutColor1BG(record.getBgColor());
                    else
                        record.setBgColor(target.getOptionChainPutColor1BG());
                    if (isNullColor(target.getOptionChainPutColor1FG()))
                        target.setOptionChainPutColor1FG(record.getFgColor());
                    else
                        record.setFgColor(target.getOptionChainPutColor1FG());
                    break;
                case FIELD_OPTION_CHAIN_PUTS_ROW2:
                   if (isNullColor(target.getOptionChainPutColor2BG()))
                        target.setOptionChainPutColor2BG(record.getBgColor());
                    else
                        record.setBgColor(target.getOptionChainPutColor2BG());
                    if (isNullColor(target.getOptionChainPutColor2FG()))
                        target.setOptionChainPutColor2FG(record.getFgColor());
                    else
                        record.setFgColor(target.getOptionChainPutColor2FG());
                    break;
                 case FIELD_PFDETAILS_ROW:
                    if (isNullColor(target.getPfDetailColorBG()))
                        target.setPfDetailColorBG(record.getBgColor());
                    else
                        record.setBgColor(target.getPfDetailColorBG());
                    if (isNullColor(target.getPfDetailColorFG()))
                        target.setPfDetailColorFG(record.getFgColor());
                    else
                        record.setFgColor(target.getPfDetailColorFG());
                    break;
            }
        }

        if (target.isGridOn()){
            for (TWComboItem item: gridTypes){
                if (item.getId().equals(""+target.getGridType())){
                    comGrid.setSelectedItem(item);
                    break;
                }
            }
        }
//        chkGrid.setSelected(target.isGridOn()); // todo
        if(target.isGridOn()){
            lblGridColor.setBackground(target.getGridColor());
            smartTable.setGridColor(target.getGridColor());
        }

        if (table.getSmartTable().isCuatomThemeEnabled()) {
            chkEnable.setSelected(true);
            comGrid.setEnabled(true);
        } else {
            chkEnable.setSelected(false);
            comGrid.setEnabled(false);
        }

        comGrid.setEnabled(chkEnable.isSelected());
        if (chkEnable.isSelected()){
            lblGridColor.setBackground(target.getGridColor());
        } else {
            lblGridColor.setBackground(Theme.getColor("BACKGROUND_COLOR"));
        }

        oldBodyGap = smartTable.getBodyGap();
        btnBodyGap.setSelectedItem(oldBodyGap);
        colorConfigList.setEnabled(table.getSmartTable().isCuatomThemeEnabled());
    }

    private boolean isNullColor(Color color) {
        if ((color == null) || (color.getRGB() == -2)) {
            return true;
        } else {
            return false;
        }

    }

    public void applyInitialColor(Color source, Color ref) {
        if (ref != null) {
            source = ref;
        } else {
            ref = Color.red;
        }
    }

    public void applyTheme() {
//        colorConfigList.setBackground(UIManager.getColor("List.background")); // Theme.getColor(""));
//        scrollPane.setBackground(UIManager.getColor("List.background")); // Theme.getColor(""));
        SwingUtilities.updateComponentTreeUI(this);
        SwingUtilities.updateComponentTreeUI(colorChooser);
        SwingUtilities.updateComponentTreeUI(fontChooser);
        SwingUtilities.updateComponentTreeUI(colorDialog);
        renderer.setForegroundColor();
//        setBorders();
    }

     public void setBorders() {
//        Component c;
//        int ncomponents = colorPanel.getComponentCount();
//        for (int i = 0; i < ncomponents; ++i) {
//            c = colorPanel.getComponent(i);
//            if (c instanceof JButton) {
//                ((JButton) c).setBorder(BorderFactory.createLineBorder(Theme.getColor("LABEL_FGCOLOR")));
//            }
//        }
//        c = null;
    }

    public Font getBodyFont() {
        return newBodyFont;
    }

    public Font getHeadingFont() {
        return newHeadingFont;
    }

    /**
     * Invoked when the mouse button has been clicked (pressed
     * and released) on a component.
     */
    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == lblGridColor) {
            if (chkEnable.isSelected()) {
                selectedMode = COLUMN_GRID_COLOR;
                showColorChoser(target.getGridColor());
            }
        } else if (e.getSource() == colorConfigList) {
            if (!chkEnable.isSelected())
                return;

            selectedIndex = colorConfigList.getSelectedIndex();
            CustomizeListRenderer selObj = (CustomizeListRenderer) colorConfigList.getCellRenderer().getListCellRendererComponent(colorConfigList, colorConfigList.getSelectedValue(), colorConfigList.getSelectedIndex(), true, false);
            if ((e.getX() >= selObj.getFGLabel().getBounds().x) && (e.getX() <= (selObj.getFGLabel().getBounds().x + selObj.getFGLabel().getBounds().width)))
            {
                if (!selObj.getFGLabel().isEnabled())
                    return;
                selectedObject = (CustomizerRecord) dataModel.elementAt(colorConfigList.getSelectedIndex());
                selectedMode = COLUMN_FOREGROUND;
                showColorChoser(selectedObject.getFgColor());
            } else
            if ((e.getX() >= selObj.getBGLabel().getBounds().x) && (e.getX() <= (selObj.getBGLabel().getBounds().x + selObj.getBGLabel().getBounds().width)))
            {
                if (!selObj.getBGLabel().isEnabled())
                    return;
                selectedObject = (CustomizerRecord) dataModel.elementAt(colorConfigList.getSelectedIndex());
                selectedMode = COLUMN_BACKGROUND;
                showColorChoser(selectedObject.getBgColor());
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equalsIgnoreCase("OK")) {
            newColor = colorChooser.getColor();
            if (oldColor != newColor) {
                if (selectedMode == COLUMN_GRID_COLOR) {
                    target.setGridColor(newColor);
                    lblGridColor.setBackground(newColor);
                    smartTable.setGridColor(newColor);
                } else {
                    switch (selectedObject.getType()) {
                        case FIELD_BASIC_ROW1 :
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setRowColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                target.setRowColor1FG(newColor);
                                selectedObject.setFgColor(newColor);
                            }
                            break;
                        case FIELD_BASIC_ROW2 :
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setRowColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                target.setRowColor2FG(newColor);
                                selectedObject.setFgColor(newColor);
                            }
                            break;
                        case FIELD_BID_ROW1 :
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setBidColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                target.setBidColor1FG(newColor);
                                selectedObject.setFgColor(newColor);
                            }
                            break;
                        case FIELD_BID_ROW2 :
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setBidColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                target.setBidColor2FG(newColor);
                                selectedObject.setFgColor(newColor);
                            }
                            break;
                        case FIELD_ASK_ROW1 :
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setAskColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                target.setAskColor1FG(newColor);
                                selectedObject.setFgColor(newColor);
                            }
                            break;
                        case FIELD_ASK_ROW2 :
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setAskColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                target.setAskColor2FG(newColor);
                                selectedObject.setFgColor(newColor);
                            }
                            break;
                        case FIELD_SELECTED_ROW :
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setSelectedColumnBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                target.setSelectedColumnFG(newColor);
                                selectedObject.setFgColor(newColor);
                            }
                            break;
                        case FIELD_HEADER_COLOR_ROW :
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setHeaderColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                target.setHeaderColorFG(newColor);
                                selectedObject.setFgColor(newColor);
                            }
                            applyHeaderColor();
                            break;
                        case FIELD_SMALLTRADE_ROW :
                            if (selectedMode == COLUMN_FOREGROUND) {
                                target.setSmallTradeFG(newColor);
                                selectedObject.setFgColor(newColor);
                            }
                            break;
                        case FIELD_VALUE_UP_ROW :
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setCellHighLightedUpBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                target.setCellHighLightedUpFG(newColor);
                                selectedObject.setFgColor(newColor);
                            }
                            break;
                        case FIELD_VALUE_DOWN_ROW :
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setCellHighLightedDownBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                target.setCellHighLightedDownFG(newColor);
                                selectedObject.setFgColor(newColor);
                            }
                            break;
                        case FIELD_POSITIVE_CHANGE_ROW :
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setPositiveChangeFG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
//                                if (target.getTableType() == target.PORTFOLIO_TRANS_HISTORY_TABLE)
//                                    target.setOpBalColorFG(newColor);
                                target.setPositiveChangeFG(newColor);
                                selectedObject.setFgColor(newColor);
                            }
                            break;
                        case FIELD_NEGATIVE_CHANGE_ROW :
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setNegativeChangeFG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setNegativeChangeFG(newColor);
                            }
                            break;
                        case FIELD_BUY:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setBuyColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setBuyColorFG(newColor);
                            }
                            break;
                        case FIELD_SELL:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setSellColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setSellColorFG(newColor);
                            }
                            break;
                        case FIELD_OPBAL:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setOpBalColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setOpBalColorFG(newColor);
                            }
                            break;
                        case FIELD_DIVIDAND:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setDivColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setDivColorFG(newColor);
                            }
                            break;
                        case FIELD_NEW_ANNOUNCEMENT:
                            if (selectedMode == COLUMN_FOREGROUND) {
                                target.setNewAnnouncementFG(newColor);
                                selectedObject.setFgColor(newColor);
                            }
                            break;
                        case FIELD_COMBINED_ASK_ROW1:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setCombinedAskColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setCombinedAskColor1FG(newColor);
                            }
                            break;
                        case FIELD_COMBINED_ASK_ROW2:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setCombinedAskColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setCombinedAskColor2FG(newColor);
                            }
                            break;
                        case FIELD_COMBINED_BID_ROW1:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setCombinedBidColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setCombinedBidColor1FG(newColor);
                            }
                            break;
                        case FIELD_COMBINED_BID_ROW2:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setCombinedBidColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setCombinedBidColor2FG(newColor);
                            }
                            break;
                        //change start
                        case FIELD_COMBINED_PRICE_ROW1:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setCombinedPriceColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setCombinedPriceColor1FG(newColor);
                            }
                            break;
                        case FIELD_COMBINED_PRICE_ROW2:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setCombinedPriceColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setCombinedPriceColor2FG(newColor);
                            }
                            break;
                        //change end
                        case FIELD_COMBINED_SELECTED_ROW:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setCombinedSelectedColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setCombinedSelectedColorFG(newColor);
                            }
                            break;
                        case FIELD_DEPOSITE_ROW1:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setDepositeColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setDepositeColor1FG(newColor);
                            }
                            break;
                        case FIELD_DEPOSITE_ROW2:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setDepositeColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setDepositeColor2FG(newColor);
                            }
                            break;
                        case FIELD_WITHDRAW_ROW1:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setWithdrawColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setWithdrawColor1FG(newColor);
                            }
                            break;
                        case FIELD_WITHDRAW_ROW2:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setWithdrawColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setWithdrawColor2FG(newColor);
                            }
                            break;
                         //change start
                        case FIELD_TIMENSALES_ROW1:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setTimeNSalesRowColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setTimeNSalesRowColor1FG(newColor);
                            }
                            break;
                        case FIELD_TIMENSALES_ROW2:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setTimeNSalesRowColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setTimeNSalesRowColor2FG(newColor);
                            }
                            break;
                        case FIELD_TIMENSALES_SELECTED_ROW:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setTimeNSalesSelectedColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setTimeNSalesSelectedColorFG(newColor);
                            }
                            break;
                        case FIELD_DEP_BY_ORD_ASK_ROW1:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setDepthOrderAskColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setDepthOrderAskColor1FG(newColor);
                            }
                            break;
                        case FIELD_DEP_BY_ORD_ASK_ROW2:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setDepthOrderAskColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setDepthOrderAskColor2FG(newColor);
                            }
                            break;
                        case FIELD_DEP_BY_ORD_BID_ROW1:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setDepthOrderBidColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setDepthOrderBidColor1FG(newColor);
                            }
                            break;
                        case FIELD_DEP_BY_ORD_BID_ROW2:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setDepthOrderBidColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setDepthOrderBidColor2FG(newColor);
                            }
                            break;
                        case FIELD_DEP_BY_ORD_SELECTED_ROW:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setDepthOrderSelectedColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setDepthOrderSelectedColorFG(newColor);
                            }
                            break;
                        case FIELD_CONDITIONAL_BUY:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setConditionBuyColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setConditionBuyColorFG(newColor);
                            }
                            break;
                        case FIELD_CONDITIONAL_SELL:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setConditionSellColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setConditionSellColorFG(newColor);
                            }
                            break;
                        //change end
                        case FIELD_DEPOSITE:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setDeposColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setDeposColorFG(newColor);
                            }
                            break;
                        case FIELD_WITHDRAW:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setWithdraColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setWithdraColorFG(newColor);
                            }
                            break;
                        case FIELD_CHARGES:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setChargesColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setChargesColorFG(newColor);
                            }
                            break;
                        case FIELD_REFUNDS:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setRefundColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setRefundColorFG(newColor);
                            }
                            break;
                        case FIELD_NEWS_HOT_NEWS:
                            if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setNewsHotNewsColorFG(newColor);
                            }
                            break;
                        case FIELD_NEWS_TEMPORARY_NEWS:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setNewsTemporaryNewsColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            }
                            break;
                        case FIELD_OPTION_CHAIN_STRIKE_ROW1:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setOptionChainStrikeColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setOptionChainStrikeColor1FG(newColor);
                            }
                            break;
                        case FIELD_OPTION_CHAIN_STRIKE_ROW2:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setOptionChainStrikeColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setOptionChainStrikeColor2FG(newColor);
                            }
                            break;
                        case FIELD_OPTION_CHAIN_CALLS_ROW1:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setOptionChainCallColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setOptionChainCallColor1FG(newColor);
                            }
                            break;
                        case FIELD_OPTION_CHAIN_CALLS_ROW2:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setOptionChainCallColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setOptionChainCallColor2FG(newColor);
                            }
                            break;
                        case FIELD_OPTION_CHAIN_PUTS_ROW1:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setOptionChainPutColor1BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setOptionChainPutColor1FG(newColor);
                            }
                            break;
                        case FIELD_OPTION_CHAIN_PUTS_ROW2:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setOptionChainPutColor2BG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setOptionChainPutColor2FG(newColor);
                            }
                            break;
                        case FIELD_PFDETAILS_ROW:
                            if (selectedMode == COLUMN_BACKGROUND) {
                                target.setPfDetailColorBG(newColor);
                                selectedObject.setBgColor(newColor);
                            } else if (selectedMode == COLUMN_FOREGROUND) {
                                selectedObject.setFgColor(newColor);
                                target.setPfDetailColorFG(newColor);
                            }
                            break;

                    }
                }
                smartTable.updateUI();
            }
        } else if (e.getActionCommand().equals("FONT_BODY")) {
            oldFont =  table.getTable().getFont(); // target.getBodyFont();
            newBodyFont = fontChooser.showDialog(Language.getString("SELECT_FONT"), smartTable.getFont()); // target.getBodyFont());
            SharedMethods.changeTableFont(oldFont,newBodyFont,table,smartTable,target);
//            if ((newBodyFont != null) && (!newBodyFont.equals(oldFont))) {
//                target.setBodyFont(newBodyFont);
//                smartTable.setBodyFont();
//                try {
//                    ((CommonTable) smartTable.getModel()).getViewSettings().setFont(newBodyFont);
//                } catch (Exception ex) {
//                    try {
//                        table.getModel().getViewSettings().setFont(newBodyFont);
//                    } catch (Exception ex1) {
//                    }
//                }
//                if (smartTable.isUseSameFontForHeader()) {
//                    target.setHeadingFont(newBodyFont);
//                    ((CommonTable) smartTable.getModel()).getViewSettings().setHeaderFont(newBodyFont);
//                    smartTable.updateHeaderUI();
//                }
//                smartTable.adjustColumnWidthsToFit(40);
//                table.setFontResized(true);
//                //todo - removed  16/01/2007
//                if ((table.getWindowType() == ViewSettingsManager.DETAIL_QUOTE) ||
//                        (table.getWindowType() == ViewSettingsManager.SNAP_QUOTE) ||
//                        (table.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW) ||
//                        (table.getWindowType() == ViewSettingsManager.DETAIL_QUOTE_MFUND)){
//
//                    table.packFrame();
//                }
//            }else{
//                table.setFontResized(false);
//            }
//            try {
//                ((TWTableRenderer)table.getTable().getDefaultRenderer(Object.class)).propertyChanged(TWTableRenderer.PROPERTY_FONT_CHANGED);
//            } catch (Exception e1) {}
//            try {
//                ((TWTableRenderer)table.getTable().getDefaultRenderer(Number.class)).propertyChanged(TWTableRenderer.PROPERTY_FONT_CHANGED);
//            } catch (Exception e1) {}
//            try {
//                ((TWTableRenderer)table.getTable().getDefaultRenderer(Boolean.class)).propertyChanged(TWTableRenderer.PROPERTY_FONT_CHANGED);
//            } catch (Exception e1) {}
        } else if (e.getActionCommand().equals("FONT_HEADING")) {
            oldFont = target.getHeadingFont();
            newHeadingFont = fontChooser.showDialog(Language.getString("SELECT_FONT"), smartTable.getTableHeader().getFont()); // target.getHeadingFont());
            if ((newHeadingFont != null) && (newHeadingFont != oldFont)) {
                target.setHeadingFont(newHeadingFont);
                try {
                    ((CommonTable) smartTable.getModel()).getViewSettings().setHeaderFont(newHeadingFont);
                } catch (Exception ex) {
                    try {
                        table.getModel().getViewSettings().setHeaderFont(newHeadingFont);
                    } catch (Exception ex1) {
//                        ex1.printStackTrace();
                    }
//                    ex.printStackTrace();
                }
                smartTable.updateHeaderUI();
            }
        } else if (e.getActionCommand().equals("CLOSE")) {
            table.getModel().getViewSettings().putProperty(ViewConstants.CS_TABLE_SETTINGS, target.getWorkspaceString());
//            parent.setVisible(false);
            parent.dispose();
        } else if (e.getActionCommand().equals("GRID_TYPE")) {
            String type =  ((TWComboItem)comGrid.getSelectedItem()).getId();
            target.setGridType(Integer.parseInt(type));
        } else if (e.getActionCommand().equals("ENABLE_GRID")) {
            applyTableGrid();
        } else if (e.getActionCommand().equals("ENABLE")) {
            smartTable.setCustomThemeEnabled(chkEnable.isSelected());
            target.setCustomThemeEnabled(chkEnable.isSelected());
            colorConfigList.setEnabled(chkEnable.isSelected());
            table.getModel().getViewSettings().putProperty(ViewConstants.CS_CUSTOMIZED, chkEnable.isSelected());
            comGrid.setEnabled(chkEnable.isSelected());
            if (chkEnable.isSelected()){
                lblGridColor.setBackground(target.getGridColor());
                target.setGridColor(target.getGridColor());
                String type =  ((TWComboItem)comGrid.getSelectedItem()).getId();
                target.setGridType(Integer.parseInt(type));
            } else {
                lblGridColor.setBackground(Theme.getColor("BACKGROUND_COLOR"));
            }
            if (chkEnable.isSelected()) {
                table.getModel().getViewSettings().putProperty(ViewConstants.CS_TABLE_SETTINGS, target.getWorkspaceString());
            } else {
                table.getModel().getViewSettings().removeProperty(ViewConstants.CS_TABLE_SETTINGS);
            }
            applyHeaderColor();
            smartTable.updateUI();
        } else if (e.getActionCommand().equals("BODY_GAP")) {
            oldBodyGap = smartTable.getBodyGap();
            newBodyGap = Integer.parseInt(""+btnBodyGap.getSelectedItem());
            if (oldBodyGap != newBodyGap) {
                target.setBodyGap(newBodyGap);
                smartTable.setBodyGap(newBodyGap);
                smartTable.updateUI();
            }
        }
        oldColor = null;
        newColor = null;
        selectedObject = null;
    }

    /*private void loadTableSetings(){
        try {
            String[] values = table.getModel().getViewSettings().getProperty(ViewConstants.CS_TABLE_SETTINGS).split(",");
            int i=0;
            int colorValue;
            while ( i < colorConfigList.getModel().getSize()) {
                CustomizerRecord record = (CustomizerRecord)colorConfigList.getModel().getElementAt(i);
                colorValue = Integer.parseInt(values[i]);
                if (colorValue == -1)
                    record.setFgColor(null);
                else
                    record.setFgColor(new Color(colorValue));
                colorValue = Integer.parseInt(values[i++]);
                if (colorValue == -1)
                    record.setBgColor(null);
                else
                    record.setBgColor(new Color(colorValue));
                record = null;
                i++;
            }
        } catch (Exception e) {
            // if settings not saved
        }
    }*/

    private void applyHeaderColor() {
        if (chkEnable.isSelected()) {
            smartTable.getTableHeader().setBackground(target.getHeaderColorBG());
            smartTable.getTableHeader().setForeground(target.getHeaderColorFG());
            smartTable.updateHeaderUI();
        } else {
            smartTable.getTableHeader().setBackground(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"));
            smartTable.getTableHeader().setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));
            smartTable.updateHeaderUI();
        }
    }

    private void applyTableGrid() {
        /*try { //todo
            target.setGridOn(chkGrid.isSelected());
        } catch (Exception e) {
            e.printStackTrace();
        }
        smartTable.setGdidOn(chkGrid.isSelected());
        if (chkGrid.isSelected()) {
            smartTable.setIntercellSpacing(new Dimension(1, 1));
            lblGridColor.setBackground(target.getGridColor());
            smartTable.setGridColor(target.getGridColor());
        } else {
            smartTable.setIntercellSpacing(new Dimension(0, 0));
            lblGridColor.setBackground(this.getBackground());
        }*/
    }

    private void showColorChoser(Color color) {
        oldColor = color;
        colorChooser.setColor(oldColor);
        disableResetButton();
        colorDialog.show();
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     */
    public void mousePressed(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button has been released on a component.
     */
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Invoked when the mouse enters a component.
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Invoked when the mouse exits a component.
     */
    public void mouseExited(MouseEvent e) {
    }

//    public void setTable(Table table) {
//        this.table = table;
//    }

    private void disableResetButton(){

        Component[] comp= colorDialog.getContentPane().getComponents();

        for(int i=0; i<comp.length; i++){

            if(comp[i] instanceof JPanel){

                JPanel btnpan = (JPanel)comp[i];
                Component[] btncomps = btnpan.getComponents();
                String resetString = UIManager.getString("ColorChooser.resetText");

                for(int j = 0; j<btncomps.length; j++){

                    if(btncomps[j] instanceof JButton && ((JButton)btncomps[j]).getText().equals(resetString)){
                        btncomps[j].setVisible(false);
                    }
                }
            }
        }
    }

}