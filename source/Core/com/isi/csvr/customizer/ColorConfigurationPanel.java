package com.isi.csvr.customizer;

import com.isi.csvr.Client;
import com.isi.csvr.ClientTable;
import com.isi.csvr.FontChooser;
import com.isi.csvr.WorkInProgressIndicator;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.theme.CustomThemeInterface;
import com.isi.csvr.theme.TableThemeSettings;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Feb 26, 2009
 * Time: 1:46:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class ColorConfigurationPanel extends JPanel implements ActionListener, Themeable {

    private JPanel upPanel;
    private JPanel downPanel;
    private JPanel upPanelLeft;
    private TableCuztomizerUI parent;

    private TableThemeSettings target;

    private Color oldColor;
    private Color newColor;
    private JButton source;
    private String lastCommand = "";
    private JColorChooser colorChooser;
    private Font oldFont;
    private Font newBodyFont;
    private Font newHeadingFont;
    private FontChooser fontChooser;
    private JDialog colorDialog;

    private JLabel lblGrid = new JLabel();
    private TWComboBox comGrid;
    private ArrayList<TWComboItem> gridTypes;
    private JButton btnGrid = new JButton();

    private JButton btnBodyFont = new JButton();
    private JButton btnHeadingFont = new JButton();
    private JLabel lblBodyFont = new JLabel();
    private JLabel lblHeadingFont = new JLabel();
    private JLabel lblBodyGap = new JLabel();
    private TWComboBox btnBodyGap = null;
    private Font bodyFont;
    private Font headingFont;
    private int inibodyGap = 2;

    private int bodyGap = 2;


    private JRadioButton thisWindowOnly;
    private JRadioButton allOpenedWindows;
    private JRadioButton allWindows;

    private JButton btnFgRow1 = new JButton();
    private JLabel lblRow1 = new JLabel();
    private JButton btnBgRow1 = new JButton();

    private JButton btnTradeFG1 = new JButton();
    private JLabel lblTrade1 = new JLabel();
    private JButton btnTradeBG1 = new JButton();

    private JButton btnBidBG1 = new JButton();
    private JLabel lblBid = new JLabel();
    private JButton btnBidFG1 = new JButton();

    private JButton btnAskFG1 = new JButton();
    private JLabel lbl = new JLabel();
    private JButton btnAskBG1 = new JButton();

    //----------even  panel
    private JButton btnFgRow2 = new JButton();
    private JLabel lblRow2 = new JLabel();
    private JButton btnBgRow2 = new JButton();

    private JButton btnTradeFG2 = new JButton();
    private JLabel lblTrade2 = new JLabel();
    private JButton btnTradeBG2 = new JButton();

    private JButton btnBidFG2 = new JButton();
    private JLabel lblBid2 = new JLabel();
    private JButton btnBidBG2 = new JButton();

    private JButton btnAskFG2 = new JButton();
    private JLabel lblAsk2 = new JLabel();
    private JButton btnAskBG2 = new JButton();

    //======= Advance panel 1 =========

    private JButton btnDownFG = new JButton();
    private JButton btnDownBG = new JButton(); //== Highl8 down


    private JButton btnUpFg = new JButton();//==Highl8 up
    private JButton btnUpBg = new JButton();

    private JButton btnSelectedFG = new JButton();
    private JButton btnSelectedBG = new JButton();

    private JButton btnChangeUpFG = new JButton();
    private JButton btnChangeDownFG = new JButton();

    //=========== Advance Panel 2==========

    private JButton btnMinFG = new JButton();
    private JButton btnMinBG = new JButton();
    private JButton btnMaxFG = new JButton();
    private JButton btnMaxBG = new JButton();

    private JButton btnWeek52LowFG = new JButton();
    private JButton btnWeek52LowBG = new JButton();
    private JButton btnWeek52HighFG = new JButton();
    private JButton btnWeek52HighBG = new JButton();

    //    private int BUTTON_WIDTH = 60;
    private int BUTTON_WIDTH = 40;

    private boolean isPanelVisible = false;
    private boolean isPanelVisibleOnExit = false;

    private JCheckBox chkEnable = new JCheckBox();

    private TWButton okBtn;
    private TWButton cancelBtn;

    private TWTabbedPane customizerTabPanel;

    private JPanel advanceHead1Panel;
    private JPanel advanceHead2Panel;
    private JPanel advanceHead3Panel;
    private JPanel advanceHead4Panel;
    private JPanel advanceCheckBoxPanel;

    private JCheckBox chkMinEnable = new JCheckBox();
    private JCheckBox chkMaxEnable = new JCheckBox();
    private JCheckBox chkWk52LowEnable = new JCheckBox();
    private JCheckBox chkWk52HighEnable = new JCheckBox();

    private boolean isMinEnableOnExit = false;
    private boolean isMaxEnableOnExit = false;
    private boolean isWk52LowEnableOnExit = false;
    private boolean isWk52HighEnableOnExit = false;

    private JPanel labelOdPanel;
    private JPanel oddPanel;

    private JPanel labelEvenPanel;
    private JPanel evenPanel;

    private JPanel panellabel;
    private JPanel p1;
    private JPanel p2;

    private Hashtable<String, Color> initialColorMap = new Hashtable<String, Color>();


    public ColorConfigurationPanel(TableCuztomizerUI parent) {
        this.parent = parent;
        createUI();
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
    }

    public void createUI() {
        upPanel = new JPanel();
        downPanel = new JPanel();
        GUISettings.applyOrientation(upPanel);
        GUISettings.applyOrientation(downPanel);
        createLowerPanel();

//        upPanel.setPreferredSize(new Dimension(580, 140));
        upPanel.setPreferredSize(new Dimension(520, 140));

        upPanel.setLayout(new FlexGridLayout(new String[]{"55%", "45%"}, new String[]{"20", "80", "30", "5"}, 2, 2));

        p1 = new JPanel();
        p1.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20"}, 5, 0));

        p1.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        JLabel font = new JLabel(Language.getString("FONT_SETTINGS"));
        font.setAlignmentX(JLabel.LEFT_ALIGNMENT);
        p1.add(font);
        upPanel.add(p1);

        p2 = new JPanel();
        p2.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20"}, 5, 0));
        p2.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        JLabel apply = new JLabel(Language.getString("TC_APPLY"));
        apply.setAlignmentX(JLabel.LEFT_ALIGNMENT);
        p2.add(apply);
        upPanel.add(p2);

        upPanel.add(createUpLeftPanel());
        upPanel.add(createSaveModePanel());

        upPanel.add(createConfigPanel());
        upPanel.add(createButtonPanel());
        upPanel.add(new JLabel());
        upPanel.add(new JLabel());
        setLayout(new BorderLayout());

        add(upPanel, BorderLayout.NORTH);
        if (isPanelVisible) {
            add(downPanel, BorderLayout.SOUTH);
        }

        fontChooser = new FontChooser(Client.getInstance().getFrame(), true);
        JLabel previewPane = new JLabel(Language.getString("WINDOW_TITLE_CLIENT"));
        previewPane.setFont(new TWFont("Arial", Font.BOLD, 20));
        colorChooser = new JColorChooser(Color.red);
        colorChooser.setPreviewPanel(previewPane);
        colorDialog = JColorChooser.createDialog(Client.getInstance().getFrame(), Language.getString("SELECT_COLOR"), true, colorChooser, this, this);
    }

    private void createLowerPanel() {
        customizerTabPanel = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "nw");
        downPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 2, 2));
        downPanel.add(customizerTabPanel);
        customizerTabPanel.addTab(Language.getString("TC_BASIC"), createBasicDataPanel());
        customizerTabPanel.addTab(Language.getString("TC_ADVANCE"), createAdvanceDataPanel());

        customizerTabPanel.selectTab(0);
        downPanel.setPreferredSize(new Dimension(400, 230));
        GUISettings.applyOrientation(customizerTabPanel);

    }

    private JPanel createAdvanceDataPanel() {
        JPanel advancePanel = new JPanel();
        advancePanel.setBorder(BorderFactory.createEtchedBorder());
//        advancePanel.setLayout(new FlexGridLayout(new String[]{"130", "2", "150", "2", "110", "2", "150"}, new String[]{"190"}, 2, 2));
        advancePanel.setLayout(new FlexGridLayout(new String[]{"25%", "2", "28%", "2", "20%", "2", "28%"}, new String[]{"100%"}, 2, 2));

        advancePanel.add(createAdvancePanel1());
        advancePanel.add(new JSeparator(1));
        advancePanel.add(createAdvancePanel2());
        advancePanel.add(new JSeparator(1));
//        advancePanel.add(createAdvanceCheckBoxPanel());
        advancePanel.add(createAdvancePanel3());
        advancePanel.add(new JSeparator(1));
        advancePanel.add(createAdvancePanel4());

        return advancePanel;

    }

    private JPanel createAdvanceCheckBoxPanel() {
        JPanel panel1 = new JPanel();
//        panel1.setLayout(new FlexGridLayout(new String[]{"120"}, new String[]{"20", "160"}, 2, 2));
        panel1.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "160"}, 2, 2));

        advanceCheckBoxPanel = new JPanel();
        advanceCheckBoxPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20"}, 0, 0));
        advanceCheckBoxPanel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        panel1.add(advanceCheckBoxPanel);

        JPanel setPanel1 = new JPanel();
        setPanel1.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "30", "30", "30", "30"}, 2, 2));

//        JLabel cellHLDown = new JLabel(Language.getString("TC_MIN"));
//        cellHLDown.setPreferredSize(new Dimension(100, 30));
        setPanel1.add(chkMinEnable);

        JLabel cellHLUp = new JLabel(Language.getString("TC_MAX"));
        cellHLUp.setPreferredSize(new Dimension(100, 30));
        setPanel1.add(chkMaxEnable);

        JLabel userSelect = new JLabel(Language.getString("TC_52WK_LOW"));
        userSelect.setPreferredSize(new Dimension(100, 30));
        setPanel1.add(chkWk52LowEnable);

        JLabel positiveNet = new JLabel(Language.getString("TC_52WK_HIGH"));
        positiveNet.setPreferredSize(new Dimension(100, 30));
        setPanel1.add(chkWk52HighEnable);

        JLabel negativeNet = new JLabel("");
//        negativeNet.setPreferredSize(new Dimension(100, 30));
        setPanel1.add(negativeNet);

        panel1.add(setPanel1);

        return panel1;
    }

    private JPanel createAdvancePanel4() {
        JPanel panel2 = new JPanel();
//        panel2.setLayout(new FlexGridLayout(new String[]{"150"}, new String[]{"20", "160"}, 2, 2));
        panel2.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "160"}, 2, 2));

        advanceHead4Panel = new JPanel();
//        advanceHead4Panel.setLayout(new FlexGridLayout(new String[]{"60", "20", "60"}, new String[]{"20"}, 2, 0));
        advanceHead4Panel.setLayout(new FlexGridLayout(new String[]{"40%", "20%", "40%"}, new String[]{"20"}, 2, 0));

        advanceHead4Panel.add(new JLabel(Language.getString("TC_CELL_COLOR")));
        advanceHead4Panel.add(new JLabel());
        advanceHead4Panel.add(new JLabel(Language.getString("TC_FONT_COLOR")));

        advanceHead4Panel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        panel2.add(advanceHead4Panel);

        JPanel oddSetPanel = new JPanel();
        oddSetPanel.setLayout(new FlexGridLayout(new String[]{"40%", "20%", "40%"}, new String[]{"20", "20", "20", "20", "20"}, 2, 10));

        btnMinFG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnMinFG.setActionCommand("COLOR_FG_MIN");
        btnMinFG.addActionListener(this);
        btnMinBG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnMinBG.setActionCommand("COLOR_BG_MIN");
        btnMinBG.addActionListener(this);

        oddSetPanel.add(btnMinBG);
        oddSetPanel.add(new JLabel());
        oddSetPanel.add(btnMinFG);

        btnMaxFG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnMaxFG.setActionCommand("COLOR_FG_MAX");
        btnMaxFG.addActionListener(this);
        btnMaxBG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnMaxBG.setActionCommand("COLOR_BG_MAX");
        btnMaxBG.addActionListener(this);

        oddSetPanel.add(btnMaxBG);
        oddSetPanel.add(new JLabel());
        oddSetPanel.add(btnMaxFG);

        btnWeek52LowFG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnWeek52LowFG.setActionCommand("COLOR_FG_WK52_LOW");
        btnWeek52LowFG.addActionListener(this);
        btnWeek52LowBG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnWeek52LowBG.setActionCommand("COLOR_BG_WK52_LOW");
        btnWeek52LowBG.addActionListener(this);

        oddSetPanel.add(btnWeek52LowBG);
        oddSetPanel.add(new JLabel());
        oddSetPanel.add(btnWeek52LowFG);

        btnWeek52HighFG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnWeek52HighFG.setActionCommand("COLOR_FG_WK52_HIGH");
        btnWeek52HighFG.addActionListener(this);
        btnWeek52HighBG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnWeek52HighBG.setActionCommand("COLOR_BG_WK52_HIGH");
        btnWeek52HighBG.addActionListener(this);

        oddSetPanel.add(btnWeek52HighBG);
        oddSetPanel.add(new JLabel());
        oddSetPanel.add(btnWeek52HighFG);

        panel2.add(oddSetPanel);
        return panel2;
    }

    private JPanel createAdvancePanel3() {
        JPanel panel1 = new JPanel();
//        panel1.setLayout(new FlexGridLayout(new String[]{"120"}, new String[]{"20", "160"}, 2, 2));
        panel1.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "160"}, 2, 2));

        advanceHead3Panel = new JPanel();
        advanceHead3Panel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20"}, 0, 0));
        advanceHead3Panel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        panel1.add(advanceHead3Panel);

        JPanel setPanel1 = new JPanel();
//        setPanel1.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "30", "30", "30", "30"}, 2, 2));
        setPanel1.setLayout(new FlexGridLayout(new String[]{"25%", "75%"}, new String[]{"30", "30", "30", "30", "30"}, 0, 2));


        chkMinEnable.addActionListener(this);
        chkMinEnable.setActionCommand("MIN_ENABLE");
        setPanel1.add(chkMinEnable);
        JLabel cellHLDown = new JLabel(Language.getString("TC_MIN"));
//        cellHLDown.setEnabled(target.isMinEnabled());
        cellHLDown.setPreferredSize(new Dimension(100, 30));
        setPanel1.add(cellHLDown);
//        chkMinEnable.setPreferredSize(new Dimension(100, 30));


        chkMaxEnable.addActionListener(this);
        chkMaxEnable.setActionCommand("MAX_ENABLE");
        setPanel1.add(chkMaxEnable);
        JLabel cellHLUp = new JLabel(Language.getString("TC_MAX"));
        cellHLUp.setPreferredSize(new Dimension(100, 30));
        setPanel1.add(cellHLUp);


        chkWk52LowEnable.addActionListener(this);
        chkWk52LowEnable.setActionCommand("WK52LOW_ENABLE");
        setPanel1.add(chkWk52LowEnable);
        JLabel userSelect = new JLabel(Language.getString("TC_52WK_LOW"));
        userSelect.setPreferredSize(new Dimension(100, 30));
        setPanel1.add(userSelect);


        chkWk52HighEnable.addActionListener(this);
        chkWk52HighEnable.setActionCommand("WK52HIGH_ENABLE");
        setPanel1.add(chkWk52HighEnable);
        JLabel positiveNet = new JLabel(Language.getString("TC_52WK_HIGH"));
        positiveNet.setPreferredSize(new Dimension(100, 30));
        setPanel1.add(positiveNet);

        setPanel1.add(new JLabel(""));
        JLabel negativeNet = new JLabel("");
        negativeNet.setPreferredSize(new Dimension(100, 30));
        setPanel1.add(negativeNet);

        panel1.add(setPanel1);

        return panel1;
    }

    private JPanel createAdvancePanel2() {
        JPanel panel2 = new JPanel();
        panel2.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "160"}, 2, 2));

        advanceHead2Panel = new JPanel();
//        advanceHead2Panel.setLayout(new FlexGridLayout(new String[]{"60", "20", "60"}, new String[]{"20"}, 0, 0));
        advanceHead2Panel.setLayout(new FlexGridLayout(new String[]{"40%", "20%", "40%"}, new String[]{"20"}, 2, 0));

        advanceHead2Panel.add(new JLabel(Language.getString("TC_CELL_COLOR")));
        advanceHead2Panel.add(new JLabel());
        advanceHead2Panel.add(new JLabel(Language.getString("TC_FONT_COLOR")));

        advanceHead2Panel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        panel2.add(advanceHead2Panel);

        JPanel oddSetPanel = new JPanel();
//        oddSetPanel.setLayout(new FlexGridLayout(new String[]{"60", "20", "60"}, new String[]{"20", "20", "20", "20", "20"}, 2, 10));
        oddSetPanel.setLayout(new FlexGridLayout(new String[]{"40%", "20%", "40%"}, new String[]{"20", "20", "20", "20", "20"}, 2, 10));


        btnDownFG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnDownFG.setActionCommand("COLOR_FG_DOWN");
        btnDownFG.addActionListener(this);
        btnDownBG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnDownBG.setActionCommand("COLOR_BG_DOWN");
        btnDownBG.addActionListener(this);

        oddSetPanel.add(btnDownBG);
        oddSetPanel.add(new JLabel());
        oddSetPanel.add(btnDownFG);


        btnUpFg.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnUpFg.setActionCommand("COLOR_FG_UP");
        btnUpFg.addActionListener(this);
        btnUpBg.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnUpBg.setActionCommand("COLOR_BG_UP");
        btnUpBg.addActionListener(this);

        oddSetPanel.add(btnUpBg);
        oddSetPanel.add(new JLabel());
        oddSetPanel.add(btnUpFg);

        btnSelectedFG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnSelectedFG.setActionCommand("COLOR_FG_SELECTED");
        btnSelectedFG.addActionListener(this);
        btnSelectedBG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnSelectedBG.setActionCommand("COLOR_BG_SELECTED");
        btnSelectedBG.addActionListener(this);

        oddSetPanel.add(btnSelectedBG);
        oddSetPanel.add(new JLabel());
        oddSetPanel.add(btnSelectedFG);

        btnChangeUpFG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnChangeUpFG.setActionCommand("COLOR_FG_CHANGE_UP");
        btnChangeUpFG.addActionListener(this);

        oddSetPanel.add(new JLabel());
        oddSetPanel.add(new JLabel());
        oddSetPanel.add(btnChangeUpFG);

        btnChangeDownFG.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnChangeDownFG.setActionCommand("COLOR_FG_CHANGE_DOWN");
        btnChangeDownFG.addActionListener(this);

        oddSetPanel.add(new JLabel());
        oddSetPanel.add(new JLabel());
        oddSetPanel.add(btnChangeDownFG);

        panel2.add(oddSetPanel);
        return panel2;
    }


    private JPanel createAdvancePanel1() {
        JPanel panel1 = new JPanel();
//        panel1.setLayout(new FlexGridLayout(new String[]{"130"}, new String[]{"20", "160"}, 2, 2));
        panel1.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "160"}, 2, 2));

        advanceHead1Panel = new JPanel();
//        advanceHead1Panel.setLayout(new FlexGridLayout(new String[]{"130"}, new String[]{"20"}, 0, 0));
        advanceHead1Panel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20"}, 0, 0));

        advanceHead1Panel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        panel1.add(advanceHead1Panel);

        JPanel setPanel1 = new JPanel();
//        setPanel1.setLayout(new FlexGridLayout(new String[]{"120"}, new String[]{"30", "30", "30", "30", "30"}, 2, 2));
        setPanel1.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "30", "30", "30", "30"}, 2, 2));

        JLabel cellHLDown = new JLabel(Language.getString("CELL_HIGHLIGHT_DOWN"));
        cellHLDown.setPreferredSize(new Dimension(120, 30));
        setPanel1.add(cellHLDown);

        JLabel cellHLUp = new JLabel(Language.getString("CELL_HIGHLIGHT_UP"));
        cellHLUp.setPreferredSize(new Dimension(120, 30));
        setPanel1.add(cellHLUp);

        JLabel userSelect = new JLabel(Language.getString("SELECTED_CELLS"));
        userSelect.setPreferredSize(new Dimension(120, 30));
        setPanel1.add(userSelect);

        JLabel positiveNet = new JLabel(Language.getString("TC_POSITIVE_NET_CHANGE"));
        positiveNet.setPreferredSize(new Dimension(120, 30));
        setPanel1.add(positiveNet);

        JLabel negativeNet = new JLabel(Language.getString("TC_NEGATIVE_NET_CHANGE"));
        negativeNet.setPreferredSize(new Dimension(120, 30));
        setPanel1.add(negativeNet);

        panel1.add(setPanel1);

        return panel1;


    }

    private JPanel createBasicDataPanel() {
        JPanel basicPanel = new JPanel();

        basicPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "170"}, 5, 2));
//        basicPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"15%", "85%"}, 5, 2));

        basicPanel.add(createGridPanel());
        basicPanel.add(createBasicColorPanel());

        return basicPanel;
    }

    private JPanel createBasicColorPanel() {
        JPanel basicMainPanel = new JPanel();
        basicMainPanel.setBorder(BorderFactory.createEtchedBorder());
        basicMainPanel.setLayout(new FlexGridLayout(new String[]{"34%", "2", "33%", "2", "33%"}, new String[]{"160"}, 2, 0));
//        basicMainPanel.setLayout(new FlexGridLayout(new String[]{"34%", "2", "33%", "2", "33%"}, new String[]{"100%"}, 2, 0));

        basicMainPanel.add(createTableSettingsPanel());
        basicMainPanel.add(new JSeparator(1));
        basicMainPanel.add(createOddPanel());
        basicMainPanel.add(new JSeparator(1));
        basicMainPanel.add(createEvenPanel());


        return basicMainPanel;
    }

    private JPanel createEvenPanel() {
        JPanel panel3 = new JPanel();
        panel3.setPreferredSize(new Dimension(150, 170));
        panel3.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "130"}, 6, 2));
        evenPanel = new JPanel();
        evenPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"15", "15"}, 0, 0));


        JLabel evn = new JLabel(Language.getString("TC_EVEN"));
        evn.setHorizontalAlignment(JLabel.CENTER);
        evenPanel.add(evn);

        evenPanel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        labelEvenPanel = new JPanel();
        labelEvenPanel.setLayout(new FlexGridLayout(new String[]{"40%", "20%", "40%"}, new String[]{"15"}, 2, 0));

        labelEvenPanel.add(new JLabel(Language.getString("TC_CELL_COLOR")));
        labelEvenPanel.add(new JLabel());
        labelEvenPanel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        JLabel bulk = new JLabel();
        labelEvenPanel.add(new JLabel(Language.getString("TC_FONT_COLOR")));
        evenPanel.add(labelEvenPanel);
        panel3.add(evenPanel);

        JPanel evenSetPanel = new JPanel();
        evenSetPanel.setLayout(new FlexGridLayout(new String[]{"40%", "20%", "40%"}, new String[]{"20", "20", "20", "20"}, 2, 10));

        //============= Row=====

        btnFgRow2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgRow2.setActionCommand("COLOR_FG_ROW2");
        btnFgRow2.addActionListener(this);
        btnBgRow2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgRow2.setActionCommand("COLOR_BG_ROW2");
        btnBgRow2.addActionListener(this);

        evenSetPanel.add(btnBgRow2);
        evenSetPanel.add(lblRow2);
        evenSetPanel.add(btnFgRow2);
        //================ Last trade=====

        btnTradeFG2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnTradeFG2.setActionCommand("COLOR_FG_TRADE2");
        btnTradeFG2.addActionListener(this);
        btnTradeBG2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnTradeBG2.setActionCommand("COLOR_BG_TRADE2");
        btnTradeBG2.addActionListener(this);

        evenSetPanel.add(btnTradeBG2);
        evenSetPanel.add(lblTrade2);
        evenSetPanel.add(btnTradeFG2);

        //============= Bid ===============

        btnBidFG2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBidFG2.setActionCommand("COLOR_FG_BID2");
        btnBidFG2.addActionListener(this);
        btnBidBG2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBidBG2.setActionCommand("COLOR_BG_BID2");
        btnBidBG2.addActionListener(this);

        evenSetPanel.add(btnBidBG2);
        evenSetPanel.add(lblBid2);
        evenSetPanel.add(btnBidFG2);

        //====================Offer==========

        btnAskFG2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnAskFG2.setActionCommand("COLOR_FG_ASK2");
        btnAskFG2.addActionListener(this);
        btnAskBG2.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnAskBG2.setActionCommand("COLOR_BG_ASK2");
        btnAskBG2.addActionListener(this);

        evenSetPanel.add(btnAskBG2);
        evenSetPanel.add(lblAsk2);
        evenSetPanel.add(btnAskFG2);

        panel3.add(evenSetPanel);
        return panel3;
    }

    private JPanel createOddPanel() {
        JPanel panel2 = new JPanel();
        panel2.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "130"}, 6, 2));
        oddPanel = new JPanel();
        oddPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"15", "15"}, 0, 0));

        JLabel n = new JLabel(Language.getString("TC_ODD"));
        n.setHorizontalAlignment(JLabel.CENTER);
        oddPanel.add(n);

        oddPanel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        labelOdPanel = new JPanel();
        labelOdPanel.setLayout(new FlexGridLayout(new String[]{"40%", "20%", "40%"}, new String[]{"15"}, 2, 0));

        labelOdPanel.add(new JLabel(Language.getString("TC_CELL_COLOR")));
        labelOdPanel.add(new JLabel());
        labelOdPanel.add(new JLabel(Language.getString("TC_FONT_COLOR")));
        labelOdPanel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        oddPanel.add(labelOdPanel);

        panel2.add(oddPanel);

        JPanel oddSetPanel = new JPanel();
        oddSetPanel.setLayout(new FlexGridLayout(new String[]{"40%", "20%", "40%"}, new String[]{"20", "20", "20", "20"}, 2, 10));

        //============= Row=====

        btnFgRow1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnFgRow1.setActionCommand("COLOR_FG_ROW1");
        btnFgRow1.addActionListener(this);

        btnBgRow1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBgRow1.setActionCommand("COLOR_BG_ROW1");
        btnBgRow1.addActionListener(this);

        oddSetPanel.add(btnBgRow1);
        oddSetPanel.add(lblRow1);
        oddSetPanel.add(btnFgRow1);
        //================ Last trade=====

        btnTradeFG1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnTradeFG1.setActionCommand("COLOR_FG_TRADE1");
        btnTradeFG1.addActionListener(this);
        btnTradeBG1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnTradeBG1.setActionCommand("COLOR_BG_TRADE1");
        btnTradeBG1.addActionListener(this);

        oddSetPanel.add(btnTradeBG1);
        oddSetPanel.add(lblTrade1);
        oddSetPanel.add(btnTradeFG1);

        //========= Bid ==
        btnBidBG1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBidBG1.setActionCommand("COLOR_BG_BID1");
        btnBidBG1.addActionListener(this);
        btnBidFG1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnBidFG1.setActionCommand("COLOR_FG_BID1");
        btnBidFG1.addActionListener(this);

        oddSetPanel.add(btnBidBG1);
        oddSetPanel.add(lblBid);
        oddSetPanel.add(btnBidFG1);

        //========Offer===========

        btnAskBG1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnAskBG1.setActionCommand("COLOR_BG_ASK1");
        btnAskBG1.addActionListener(this);
        btnAskFG1.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnAskFG1.setActionCommand("COLOR_FG_ASK1");
        btnAskFG1.addActionListener(this);

        oddSetPanel.add(btnAskBG1);
        oddSetPanel.add(lbl);
        oddSetPanel.add(btnAskFG1);

        panel2.add(oddSetPanel);
        return panel2;
    }

    private JPanel createTableSettingsPanel() {
        JPanel panel1 = new JPanel();
        panel1.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "130"}, 5, 0));

        panellabel = new JPanel();
        panellabel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30"}, 5, 0));
        panellabel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        JLabel tabSet = new JLabel(Language.getString("TC_TABLE_SETTINGS"));
        tabSet.setAlignmentX(JLabel.LEFT_ALIGNMENT);
        panellabel.add(tabSet);
        panel1.add(panellabel);

        JPanel setLabelPanel = new JPanel();
        setLabelPanel.setLayout(new FlexGridLayout(new String[]{"80"}, new String[]{"30", "30", "30", "30"}, 2, 2));

        setLabelPanel.setPreferredSize(new Dimension(60, 120));
        JLabel rowLabel = new JLabel(Language.getString("ROW"));
        rowLabel.setPreferredSize(new Dimension(50, 30));
        setLabelPanel.add(rowLabel);

        JLabel lastTrade = new JLabel(Language.getString("LAST_TRADE"));
        lastTrade.setPreferredSize(new Dimension(50, 30));
        setLabelPanel.add(lastTrade);

        JLabel bidLabel = new JLabel(Language.getString("BID"));
        bidLabel.setPreferredSize(new Dimension(50, 30));
        setLabelPanel.add(bidLabel);

        JLabel offerLabel = new JLabel(Language.getString("OFFER"));
        offerLabel.setPreferredSize(new Dimension(50, 30));
        setLabelPanel.add(offerLabel);

        panel1.add(setLabelPanel);

        return panel1;
    }

    private JPanel createGridPanel() {

        JPanel gridPanel = new JPanel();
        gridPanel.setPreferredSize(new Dimension(270, 20));
//        lblGrid.setPreferredSize(new Dimension(105, 20));
        lblGrid.setPreferredSize(new Dimension(90, 20));

        lblGrid.setText(Language.getString("TC_GRID"));

        gridTypes = new ArrayList<TWComboItem>();
        gridTypes.add(new TWComboItem(TableThemeSettings.GRID_BOTH, Language.getString("CO_BOTH")));
        gridTypes.add(new TWComboItem(TableThemeSettings.GRID_VERTICAL, Language.getString("VERTICLE")));
        gridTypes.add(new TWComboItem(TableThemeSettings.GRID_HORIZONTAL, Language.getString("HORIZONTAL")));
        gridTypes.add(new TWComboItem(TableThemeSettings.GRID_NONE, Language.getString("NONE")));
        comGrid = new TWComboBox(new TWComboModel(gridTypes));
        comGrid.setPreferredSize(new Dimension(100, 20));
        comGrid.setActionCommand("GRID_TYPE");
        comGrid.setSelectedIndex(0);
        comGrid.addActionListener(this);

        /*btnGrid = new JButton() {
            public void setEnabled(boolean b) {
                super.setEnabled(b);
                SharedMethods.printLine("Grid " + b, true);
            }
        };*/
        btnGrid = new JButton();
//        btnGrid.setPreferredSize(new Dimension(BUTTON_WIDTH, 20));
        btnGrid.setPreferredSize(new Dimension(60, 20));

        btnGrid.setActionCommand("COLOR_GRID");
        btnGrid.addActionListener(this);

        gridPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        gridPanel.setBorder(BorderFactory.createEtchedBorder());
        gridPanel.setPreferredSize(new Dimension(WIDTH, 385));

        gridPanel.add(lblGrid);
        gridPanel.add(comGrid);
        gridPanel.add(btnGrid);

        return gridPanel;

    }

    private JPanel createButtonPanel() {

        JPanel btnPanel = new JPanel();
        btnPanel.setPreferredSize(new Dimension(200, 30));
        btnPanel.setLayout(new FlexGridLayout(new String[]{"20%", "35%", "5%", "35%"}, new String[]{"20"}, 2, 2));

        okBtn = new TWButton(Language.getString("OK"));
        okBtn.setPreferredSize(new Dimension(30, 20));
        okBtn.setText(Language.getString("OK"));
        okBtn.addActionListener(this);
        okBtn.setActionCommand("CLOSE");
        cancelBtn = new TWButton(Language.getString("CANCEL"));
        cancelBtn.setPreferredSize(new Dimension(30, 20));

        cancelBtn.addActionListener(this);
        cancelBtn.setActionCommand("CANCEL");
        btnPanel.add(new JLabel());
        btnPanel.add(okBtn);
        btnPanel.add(new JLabel());

        btnPanel.add(cancelBtn);
        return btnPanel;

    }

    private JPanel createConfigPanel() {
        chkEnable.setText(Language.getString("ENABLE_CUSTOMIZER_CUSTOM_SETTINGS"));
        chkEnable.addActionListener(this);
        chkEnable.setActionCommand("ENABLE");

        JPanel configPanel = new JPanel();
        configPanel.setPreferredSize(new Dimension(200, 35));
        configPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20"}, 2, 4));
        configPanel.add(chkEnable);
        return configPanel;

    }

    private JPanel createUpLeftPanel() {
        upPanelLeft = new JPanel();
        upPanelLeft.setPreferredSize(new Dimension(200, 70));

        upPanelLeft.setLayout(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"20", "20", "20"}, 2, 2));

        lblBodyFont.setPreferredSize(new Dimension(180, 20));
        lblBodyFont.setText(Language.getString("MAIN_BOARD_FONT"));

        btnBodyFont.setPreferredSize(new Dimension(50, 20));
        btnBodyFont.setActionCommand("BodyFont");
//        btnBodyFont.setText(Theme.getInstance().getFont().getFontName() + "," + Theme.getInstance().getFont().getSize() + "," + getBodyStyle(Theme.getInstance().getFont().getStyle()));
        btnBodyFont.setActionCommand("FONT_BODY");
        btnBodyFont.addActionListener(this);

        lblHeadingFont.setPreferredSize(new Dimension(180, 20));
        lblHeadingFont.setText(Language.getString("HEADING_FONT"));

        btnHeadingFont.setPreferredSize(new Dimension(20, 20));
        btnHeadingFont.setToolTipText("");
        btnHeadingFont.setActionCommand("HeadFont");
        btnHeadingFont.setText("");
        btnHeadingFont.setActionCommand("FONT_HEADING");
        btnHeadingFont.addActionListener(this);

        lblBodyGap.setPreferredSize(new Dimension(180, 20));
        lblBodyGap.setText(Language.getString("BODY_GAP"));

        ArrayList elements = new ArrayList();
        for (int i = 0; i < 11; i++) {
            elements.add(i);
        }
        TWComboModel model = new TWComboModel(elements);
        btnBodyGap = new TWComboBox(model);
        btnBodyGap.setAlignmentY(SwingConstants.EAST);
        btnBodyGap.setPreferredSize(new Dimension(20, 20));
        btnBodyGap.setToolTipText("");
        btnBodyGap.setActionCommand("BODY_GAP");
        btnBodyGap.addActionListener(this);
        btnBodyGap.setRenderer(new DefaultListCellRenderer() {
            public Component getListCellRendererComponent(JList list,
                                                          Object value,
                                                          int index,
                                                          boolean isSelected,
                                                          boolean cellHasFocus) {
                JLabel lbl = (JLabel) super.getListCellRendererComponent(
                        list, value, index, isSelected, cellHasFocus);
                lbl.setHorizontalAlignment(JLabel.RIGHT);
                return lbl;
            }
        });

        upPanelLeft.add(lblBodyFont);
        upPanelLeft.add(btnBodyFont);
        upPanelLeft.add(lblHeadingFont);
        upPanelLeft.add(btnHeadingFont);
        upPanelLeft.add(lblBodyGap);
        upPanelLeft.add(btnBodyGap);
        return upPanelLeft;
    }

    public JPanel createSaveModePanel() { //radio button Panel
        thisWindowOnly = new JRadioButton(Language.getString("THIS_WINDOW_ONLY"));
        allOpenedWindows = new JRadioButton(Language.getString("ALL_OPENED"));// + " " + Language.getString("CUTOMIZER_APPLY_MESSAGE"));
        allWindows = new JRadioButton(Language.getString("ALL_WINDOWS"));// + " " + Language.getString("CUTOMIZER_APPLY_MESSAGE"));
        thisWindowOnly.setSelected(true);
        ButtonGroup savingMode = new ButtonGroup();
        savingMode.add(thisWindowOnly);
        savingMode.add(allOpenedWindows);
        savingMode.add(allWindows);
        JPanel radioBtnPanel = new JPanel();
        radioBtnPanel.setPreferredSize(new Dimension(200, 70));
        radioBtnPanel.setLayout(new FlexGridLayout(new String[]{"3%", "94%", "3%"}, new String[]{"33%", "33%", "34%"}, 0, 0));
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(thisWindowOnly);
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(allOpenedWindows);
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(new JLabel());
        radioBtnPanel.add(allWindows);
        radioBtnPanel.add(new JLabel());
        GUISettings.applyOrientation(radioBtnPanel);
        return radioBtnPanel;
    }

    public void initialSettingForWatch() {
        target.setCustomThemeEnabled(isPanelVisible);
        comGrid.setEnabled(isPanelVisible);
        btnGrid.setEnabled(isPanelVisible);
        if (isPanelVisible) {
            loadInitialSettings(target);
        }
        /* must enable the grid */
        String type = ((TWComboItem) comGrid.getSelectedItem()).getId();
        target.setGdidOn(Integer.parseInt(type));
        target.updateUI();
    }


    public String getBodyStyle(int style) {
        switch (style) {
            case 0:
                return "Plain";
            case 1:
                return "Bold";
            case 2:
                return "Italic";
            default:
                return "Bold";
        }
    }

    public void setInitialDataMap(String id, Color iniColor) {
        initialColorMap.put(id, iniColor);
    }

    public Color getColorFromCommand(String com) {
        return initialColorMap.get(com);
    }

    public Dimension getPreferredSize() {
        Dimension dim;
        if (isPanelVisible) {
            dim = new Dimension(upPanel.getPreferredSize().width, (downPanel.getPreferredSize().height + upPanel.getPreferredSize().height)); //dataTable.getPreferredSize().height+ dataTable.getTable().getTableHeader().getHeight()

        } else {
            dim = new Dimension(upPanel.getPreferredSize().width, (upPanel.getPreferredSize().height));
        }

        return dim;
    }

    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getActionCommand().equals("FONT_BODY")) {
                oldFont = target.getBodyFont();
                newBodyFont = fontChooser.showDialog(Language.getString("SELECT_FONT"), target.getBodyFont());
                if (newBodyFont != oldFont) {
                    target.setBodyFont(newBodyFont);
                    target.updateUI();
                }
                btnBodyFont.setText(getBodyFont().getFontName() + "," + getBodyFont().getSize() + "," + getBodyStyle(getBodyFont().getStyle()));

            } else if (e.getActionCommand().equals("FONT_HEADING")) {
                oldFont = target.getHeadingFont();
                newHeadingFont = fontChooser.showDialog(Language.getString("SELECT_FONT"), target.getHeadingFont());
                if ((newHeadingFont != null) && (newHeadingFont != oldFont)) {
                    target.setHeadingFont(newHeadingFont);
                    target.updateHeaderUI();
                }
                btnHeadingFont.setText(getHeadingFont().getFontName() + "," + getHeadingFont().getSize() + "," + getBodyStyle(getHeadingFont().getStyle()));

            } else if (e.getActionCommand().equals("BODY_GAP")) {
                bodyGap = Integer.parseInt("" + btnBodyGap.getSelectedItem());
                target.setBodyGap(bodyGap);
                target.updateUI();
            } else if (e.getActionCommand().equals("GRID_TYPE")) {
                add(downPanel, BorderLayout.SOUTH);
                String type = ((TWComboItem) comGrid.getSelectedItem()).getId();
                target.setGdidOn(Integer.parseInt(type));
                target.updateUI();
            } else if (e.getActionCommand().equals("CANCEL")) {
                target.setCustomThemeEnabled(isPanelVisibleOnExit);
                comGrid.setEnabled(isPanelVisibleOnExit);
                btnGrid.setEnabled(isPanelVisibleOnExit);
                target.setBodyFont(bodyFont);
                target.setHeadingFont(headingFont);
                target.setBodyGap(inibodyGap);

                target.setMinEnabled(isMinEnableOnExit);
                target.setMaxEnabled(isMaxEnableOnExit);
                target.setWk52LowEnabled(isWk52LowEnableOnExit);
                target.setWk52HighEnabled(isWk52HighEnableOnExit);

                newBodyFont = bodyFont;
                newHeadingFont = headingFont;
                bodyGap = inibodyGap;

                resetDataOnCancel();
                parent.setVisible(false);
            } else if (e.getActionCommand().equals("CLOSE")) {
                if (thisWindowOnly.isSelected()) {
                    bodyGap = Integer.parseInt("" + btnBodyGap.getSelectedItem());
                    target.setBodyGap(bodyGap);
                    target.updateUI();
                }
                if (allWindows.isSelected()) {

                    final WorkInProgressIndicator workIn = new WorkInProgressIndicator(WorkInProgressIndicator.APPLYING_TABLE_SETTINGS);
                    new Thread("TableSettings Loading Thread indicator") {
                        public void run() {
                            workIn.setVisible(true);
                        }
                    }.start();
                    new Thread("TableSettings Loading Thread") {
                        public void run() {
                            applySettingsForAllWindows();
                            workIn.dispose();
                    }
                    }.start();
                }
                if (allOpenedWindows.isSelected()) {
                    final WorkInProgressIndicator workIn = new WorkInProgressIndicator(WorkInProgressIndicator.APPLYING_TABLE_SETTINGS);
                    new Thread("TableSettings Loading Thread indicator") {
                        public void run() {
                            workIn.setVisible(true);
                    }
                    }.start();
                    new Thread("TableSettings Loading Thread") {
                        public void run() {
                            applySettingsForOpenedWindows();
                            workIn.dispose();
                                }
                    }.start();

                }
                setIniDataOnClose();
                parent.setVisible(false);
            } else if (e.getActionCommand().equals("ENABLE")) {
                isPanelVisible ^= true;
                //            isPanelVisible = !isPanelVisible;
                target.setCustomThemeEnabled(isPanelVisible);
                comGrid.setEnabled(isPanelVisible);
                btnGrid.setEnabled(isPanelVisible);
                btnGrid.setBackground(target.getTableGridColor());
                target.setTableGridColor(target.getTableGridColor());
                target.updateUI();

                //            if (isPanelVisible) {
                //                loadInitialSettings(target);
                //            }
                /* must enable the grid */
                String type = ((TWComboItem) comGrid.getSelectedItem()).getId();
                target.setGdidOn(Integer.parseInt(type));
                target.updateUI();
                parent.frameResising(getPreferredSize());
                if (isPanelVisible) {
                    add(downPanel, BorderLayout.SOUTH);
                } else {
                    this.remove(1);
                }
                updateUI();
                doLayout();
                repaint();
                parent.pack();
                parent.doLayout();
            } else if (e.getActionCommand().equals("MIN_ENABLE")) {
                target.setMinEnabled(chkMinEnable.isSelected());
                if (target.isMinEnabled()) {
                    btnMinBG.setEnabled(true);
                    btnMinFG.setEnabled(true);
                    btnMinBG.setBackground(target.getMinBGColor());
                    btnMinFG.setBackground(target.getMinFGColor());
                } else {
                    btnMinBG.setEnabled(false);
                    btnMinFG.setEnabled(false);
                    btnMinBG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
                    btnMinFG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
                }
                target.updateUI();

            } else if (e.getActionCommand().equals("MAX_ENABLE")) {
                target.setMaxEnabled(chkMaxEnable.isSelected());
                if (target.isMaxEnabled()) {
                    btnMaxBG.setEnabled(true);
                    btnMaxFG.setEnabled(true);
                    btnMaxBG.setBackground(target.getMaxBGColor());
                    btnMaxFG.setBackground(target.getMaxFGColor());
                } else {
                    btnMaxBG.setEnabled(false);
                    btnMaxFG.setEnabled(false);
                    btnMaxBG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
                    btnMaxFG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
                }
                target.updateUI();

            } else if (e.getActionCommand().equals("WK52LOW_ENABLE")) {
                target.setWk52LowEnabled(chkWk52LowEnable.isSelected());
                if (target.isWk52LowEnabled()) {
                    btnWeek52LowBG.setEnabled(true);
                    btnWeek52LowFG.setEnabled(true);
                    btnWeek52LowBG.setBackground(target.getWeek52LowBGColor());
                    btnWeek52LowFG.setBackground(target.getWeek52LowFGColor());
                } else {
                    btnWeek52LowBG.setEnabled(false);
                    btnWeek52LowFG.setEnabled(false);
                    btnWeek52LowBG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
                    btnWeek52LowFG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
                }
                target.updateUI();

            } else if (e.getActionCommand().equals("WK52HIGH_ENABLE")) {
                target.setWk52HighEnabled(chkWk52HighEnable.isSelected());
                if (target.isWk52HighEnabled()) {
                    btnWeek52HighBG.setEnabled(true);
                    btnWeek52HighFG.setEnabled(true);
                    btnWeek52HighBG.setBackground(target.getWeek52HighBGColor());
                    btnWeek52HighFG.setBackground(target.getWeek52HighFGColor());
                } else {
                    btnWeek52HighBG.setEnabled(false);
                    btnWeek52HighFG.setEnabled(false);
                    btnWeek52HighBG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
                    btnWeek52HighFG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
                }
                target.updateUI();

            } else if (e.getActionCommand().startsWith("COLOR")) {
                source = (JButton) e.getSource();
                oldColor = source.getBackground();
                colorChooser.setColor(oldColor);
                lastCommand = e.getActionCommand();
                colorDialog.show();
            } else if (e.getActionCommand().equalsIgnoreCase("OK")) {
                newColor = colorChooser.getColor();

                if (oldColor != newColor) {
                    source.setBackground(newColor);
                    if (lastCommand.equals("COLOR_FG_UP")) {
                        target.setUpFGColor(newColor);

                    } else if (lastCommand.equals("COLOR_BG_UP")) {
                        target.setUpBGColor(newColor);

                    } else if (lastCommand.equals("COLOR_FG_DOWN")) {
                        target.setDownFGColor(newColor);

                    } else if (lastCommand.equals("COLOR_BG_DOWN")) {
                        target.setDownBGColor(newColor);

                    } else if (lastCommand.equals("COLOR_FG_ROW2")) {
                        target.setRow2FGColor(newColor);

                    } else if (lastCommand.equals("COLOR_BG_ROW2")) {
                        target.setRow2BGColor(newColor);

                    } else if (lastCommand.equals("COLOR_BG_ROW1")) {
                        target.setRow1BGColor(newColor);

                    } else if (lastCommand.equals("COLOR_FG_ROW1")) {
                        target.setRow1FGColor(newColor);

                    } else if (lastCommand.equals("COLOR_BG_ASK2")) {
                        target.setAskBGColor2(newColor);

                    } else if (lastCommand.equals("COLOR_FG_ASK2")) {
                        target.setAskFGColor2(newColor);

                    } else if (lastCommand.equals("COLOR_FG_BID2")) {
                        target.setBidFGColor2(newColor);

                    } else if (lastCommand.equals("COLOR_BG_BID2")) {
                        target.setBidBGColor2(newColor);

                    } else if (lastCommand.equals("COLOR_BG_ASK1")) {
                        target.setAskBGColor1(newColor);

                    } else if (lastCommand.equals("COLOR_FG_ASK1")) {
                        target.setAskFGColor1(newColor);

                    } else if (lastCommand.equals("COLOR_FG_BID1")) {
                        target.setBidFGColor1(newColor);

                    } else if (lastCommand.equals("COLOR_BG_BID1")) {
                        target.setBidBGColor1(newColor);

                    } else if (lastCommand.equals("COLOR_FG_TRADE1")) {
                        target.setTradeFGColor1(newColor);

                    } else if (lastCommand.equals("COLOR_BG_TRADE1")) {
                        target.setTradeBGColor1(newColor);

                    } else if (lastCommand.equals("COLOR_FG_TRADE2")) {
                        target.setTradeFGColor2(newColor);

                    } else if (lastCommand.equals("COLOR_BG_TRADE2")) {
                        target.setTradeBGColor2(newColor);

                    } else if (lastCommand.equals("COLOR_FG_CHANGE_DOWN")) {
                        target.setChangeDownFGColor(newColor);

                    } else if (lastCommand.equals("COLOR_FG_CHANGE_UP")) {
                        target.setChangeUPFGColor(newColor);

                    } else if (lastCommand.equals("COLOR_FG_SELECTED")) {
                        target.setHighlighterFGColor(newColor);

                    } else if (lastCommand.equals("COLOR_BG_SELECTED")) {
                        target.setHighlighterBGColor(newColor);

                    } else if (lastCommand.equals("COLOR_GRID")) {
                        target.setTableGridColor(newColor);

                    } else if (lastCommand.equals("COLOR_FG_MIN")) {
                        target.setMinFGColor(newColor);

                    } else if (lastCommand.equals("COLOR_BG_MIN")) {
                        target.setMinBGColor(newColor);

                    } else if (lastCommand.equals("COLOR_FG_MAX")) {
                        target.setMaxFGColor(newColor);

                    } else if (lastCommand.equals("COLOR_BG_MAX")) {
                        target.setMaxBGColor(newColor);

                    } else if (lastCommand.equals("COLOR_FG_WK52_HIGH")) {
                        target.setWeek52HighFGColor(newColor);

                    } else if (lastCommand.equals("COLOR_BG_WK52_HIGH")) {
                        target.setWeek52HighBGColor(newColor);

                    } else if (lastCommand.equals("COLOR_FG_WK52_LOW")) {
                        target.setWeek52LowFGColor(newColor);

                    } else if (lastCommand.equals("COLOR_BG_WK52_LOW")) {
                        target.setWeek52LowBGColor(newColor);

                    }
                    target.updateUI();
                }

            }
        } catch (NumberFormatException e1) {

        }
    }

    private void applySettingsForAllWindows(){
        if (isPanelVisible) {
            applyGridToAll(Integer.parseInt(((TWComboItem) comGrid.getSelectedItem()).getId()));
        }
        JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
        for (int i = 0; i < frames.length; i++) {
            if (frames[i] instanceof ClientTable)
                applySettingsForTable((ClientTable)frames[i]);
        }

        frames = null;

    }

    private void applySettingsForOpenedWindows(){
        if (isPanelVisible) {
            applyGridToAll(Integer.parseInt(((TWComboItem) comGrid.getSelectedItem()).getId()));
        }
        JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
        for (int i = 0; i < frames.length; i++) {
            if (frames[i] instanceof ClientTable){
                if(frames[i].isVisible())
                    applySettingsForTable((ClientTable)frames[i]);
            }
        }

        frames = null;

    }

    private void applySettingsForTable(ClientTable frame){
        loadInitialSettings(target);
        TableThemeSettings table = (TableThemeSettings) frame.getTable();
        table.setCustomThemeEnabled(isPanelVisible);
        table.setMinEnabled(chkMinEnable.isSelected());
        table.setMaxEnabled(chkMaxEnable.isSelected());
        table.setWk52LowEnabled(chkWk52LowEnable.isSelected());
        table.setWk52HighEnabled(chkWk52HighEnable.isSelected());
        applyColorChanges(table);
        table.setHeadingFont(target.getHeadingFont());
        table.updateHeaderUI();
        table.setBodyFont(target.getBodyFont());
        table.setBodyGap(target.getBodyGap());
        frame.getViewSettings().setFont(target.getBodyFont());
        FontMetrics oMetrices = null;
        try {
            if(frame.isIcon()){
                    frame.setIcon(false);
                    oMetrices = frame.getTable().getGraphics().getFontMetrics();
                    frame.setIcon(true);
            }
            else
                oMetrices = frame.getTable().getGraphics().getFontMetrics();

            frame.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + target.getBodyGap());
        } catch (Exception e) {
           System.out.println("Failed to set body gap");
        }
        table.updateUI();
        table = null;
    }

    public void setTarget(TableThemeSettings targetOrig) {
        this.target = targetOrig;
        loadInitialSettings(target);
    }

    private void loadInitialSettings(TableThemeSettings target) {

        ((CustomThemeInterface) ((JTable) target).getDefaultRenderer(Object.class)).getCurrentData(target);
        chkEnable.setSelected(target.isCuatomThemeEnabled());
        isPanelVisible = target.isCuatomThemeEnabled();
        chkEnable.setSelected(target.isCuatomThemeEnabled());
        isPanelVisibleOnExit = target.isCuatomThemeEnabled();

        if (target.isGdidOn()) {
            for (TWComboItem item : gridTypes) {
                if (item.getId().equals("" + target.getGdidType())) {
                    comGrid.setSelectedItem(item);
                    break;
                }
            }
        }

        btnBodyFont.setText(target.getBodyFont().getFontName() + "," + target.getBodyFont().getSize() + "," + getBodyStyle(target.getBodyFont().getStyle()));
        btnHeadingFont.setText(target.getHeadingFont().getFontName() + "," + target.getHeadingFont().getSize() + "," + getBodyStyle(target.getHeadingFont().getStyle()));
        newBodyFont = target.getBodyFont();
        newHeadingFont = target.getHeadingFont();

        bodyFont = target.getBodyFont();
        headingFont = target.getHeadingFont();
        inibodyGap = target.getBodyGap();

        btnBgRow1.setBackground(target.getRow1BGColor());
        btnFgRow1.setBackground(target.getRow1FGColor());
        btnTradeBG1.setBackground(target.getTradeBGColor1());
        btnTradeFG1.setBackground(target.getTradeFGColor1());
        btnBidBG1.setBackground(target.getBidBGColor1());
        btnBidFG1.setBackground(target.getBidFGColor1());
        btnAskBG1.setBackground(target.getAskBGColor1());
        btnAskFG1.setBackground(target.getAskFGColor1());


        btnBgRow2.setBackground(target.getRow2BGColor());
        btnFgRow2.setBackground(target.getRow2FGColor());
        btnTradeBG2.setBackground(target.getTradeBGColor2());
        btnTradeFG2.setBackground(target.getTradeFGColor2());
        btnBidBG2.setBackground(target.getBidBGColor2());
        btnBidFG2.setBackground(target.getBidFGColor2());
        btnAskBG2.setBackground(target.getAskBGColor2());
        btnAskFG2.setBackground(target.getAskFGColor2());


        btnUpBg.setBackground(target.getUpBGColor());
        btnUpFg.setBackground(target.getUpFGColor());
        btnDownBG.setBackground(target.getDownBGColor());
        btnDownFG.setBackground(target.getDownFGColor());

        btnSelectedBG.setBackground(target.getHighlighterBGColor());
        btnSelectedFG.setBackground(target.getHighlighterFGColor());

        btnChangeDownFG.setBackground(target.getChangeDownFGColor());
        btnChangeUpFG.setBackground(target.getChangeUPFGColor());

        isMaxEnableOnExit = target.isMaxEnabled();
        chkMaxEnable.setSelected(target.isMaxEnabled());
        if (target.isMaxEnabled()) {
            btnMaxBG.setEnabled(true);
            btnMaxFG.setEnabled(true);
            btnMaxBG.setBackground(target.getMaxBGColor());
            btnMaxFG.setBackground(target.getMaxFGColor());
        } else {
            btnMaxBG.setEnabled(false);
            btnMaxFG.setEnabled(false);
            btnMaxBG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
            btnMaxFG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
        }

        isMinEnableOnExit = target.isMinEnabled();
        chkMinEnable.setSelected(target.isMinEnabled());
        if (target.isMinEnabled()) {
            btnMinBG.setEnabled(true);
            btnMinFG.setEnabled(true);
            btnMinBG.setBackground(target.getMinBGColor());
            btnMinFG.setBackground(target.getMinFGColor());
        } else {
            btnMinBG.setEnabled(false);
            btnMinFG.setEnabled(false);
            btnMinBG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
            btnMinFG.setBackground(Theme.getColor("BACKGROUND_COLOR"));

        }

        isWk52LowEnableOnExit = target.isWk52LowEnabled();
        chkWk52LowEnable.setSelected(target.isWk52LowEnabled());

        if (target.isWk52LowEnabled()) {
            btnWeek52LowBG.setEnabled(true);
            btnWeek52LowFG.setEnabled(true);
            btnWeek52LowBG.setBackground(target.getWeek52LowBGColor());
            btnWeek52LowFG.setBackground(target.getWeek52LowFGColor());
        } else {
            btnWeek52LowBG.setEnabled(false);
            btnWeek52LowFG.setEnabled(false);
            btnWeek52LowBG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
            btnWeek52LowBG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
        }

        isWk52HighEnableOnExit = target.isWk52HighEnabled();
        chkWk52HighEnable.setSelected(target.isWk52HighEnabled());

        if (target.isWk52HighEnabled()) {
            btnWeek52HighBG.setEnabled(true);
            btnWeek52HighFG.setEnabled(true);
            btnWeek52HighBG.setBackground(target.getWeek52HighBGColor());
            btnWeek52HighFG.setBackground(target.getWeek52HighFGColor());
        } else {
            btnWeek52HighBG.setEnabled(false);
            btnWeek52HighFG.setEnabled(false);
            btnWeek52HighBG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
            btnWeek52HighFG.setBackground(Theme.getColor("BACKGROUND_COLOR"));
        }

        btnBodyGap.setSelectedItem(target.getBodyGap());
        bodyGap = target.getBodyGap();

        if (isPanelVisible) {
            btnGrid.setBackground(target.getTableGridColor());
            target.setTableGridColor(target.getTableGridColor());
            target.updateUI();
            btnGrid.setEnabled(true);
        } else {
            btnGrid.setBackground(Theme.getColor("BACKGROUND_COLOR"));
            btnGrid.setEnabled(true);
        }
        setIniDataOnClose();
        parent.frameResising(getPreferredSize());
        updateUI();
        doLayout();
        repaint();
        parent.pack();
        parent.doLayout();

    }


    private void loadInitialSettingsOnCancel(TableThemeSettings target) {

        ((CustomThemeInterface) ((JTable) target).getDefaultRenderer(Object.class)).getCurrentData(target);
        chkEnable.setSelected(target.isCuatomThemeEnabled());
        isPanelVisible = target.isCuatomThemeEnabled();
        chkEnable.setSelected(target.isCuatomThemeEnabled());

        if (target.isGdidOn()) {
            for (TWComboItem item : gridTypes) {
                if (item.getId().equals("" + target.getGdidType())) {
                    comGrid.setSelectedItem(item);
                    break;
                }
            }
        }


        btnBodyFont.setText(target.getBodyFont().getFontName() + "," + target.getBodyFont().getSize() + "," + getBodyStyle(target.getBodyFont().getStyle()));
        btnHeadingFont.setText(target.getHeadingFont().getFontName() + "," + target.getHeadingFont().getSize() + "," + getBodyStyle(target.getHeadingFont().getStyle()));
        btnBgRow1.setBackground(target.getRow1BGColor());
        btnFgRow1.setBackground(target.getRow1FGColor());
        btnTradeBG1.setBackground(target.getTradeBGColor1());
        btnTradeFG1.setBackground(target.getTradeFGColor1());
        btnBidBG1.setBackground(target.getBidBGColor1());
        btnBidFG1.setBackground(target.getBidFGColor1());
        btnAskBG1.setBackground(target.getAskBGColor1());
        btnAskFG1.setBackground(target.getAskFGColor1());


        btnBgRow2.setBackground(target.getRow2BGColor());
        btnFgRow2.setBackground(target.getRow2FGColor());
        btnTradeBG2.setBackground(target.getTradeBGColor2());
        btnTradeFG2.setBackground(target.getTradeFGColor2());
        btnBidBG2.setBackground(target.getBidBGColor2());
        btnBidFG2.setBackground(target.getBidFGColor2());
        btnAskBG2.setBackground(target.getAskBGColor2());
        btnAskFG2.setBackground(target.getAskFGColor2());

//         comGrid.setEnabled(target.isCuatomThemeEnabled());

        btnUpBg.setBackground(target.getUpBGColor());
        btnUpFg.setBackground(target.getUpFGColor());
        btnDownBG.setBackground(target.getDownBGColor());
        btnDownFG.setBackground(target.getDownFGColor());

        btnSelectedBG.setBackground(target.getHighlighterBGColor());
        btnSelectedFG.setBackground(target.getHighlighterFGColor());

        btnChangeDownFG.setBackground(target.getChangeDownFGColor());
        btnChangeUpFG.setBackground(target.getChangeUPFGColor());

        btnMaxBG.setBackground(target.getMaxBGColor());
        btnMaxFG.setBackground(target.getMaxFGColor());
        btnMinBG.setBackground(target.getMinBGColor());
        btnMinFG.setBackground(target.getMinFGColor());

        btnWeek52LowBG.setBackground(target.getWeek52LowBGColor());
        btnWeek52LowFG.setBackground(target.getWeek52LowFGColor());
        btnWeek52HighBG.setBackground(target.getWeek52HighBGColor());
        btnWeek52HighFG.setBackground(target.getWeek52HighFGColor());

        btnBodyGap.setSelectedItem(target.getBodyGap());
        bodyGap = target.getBodyGap();


        if (isPanelVisible) {
            btnGrid.setBackground(target.getTableGridColor());
            target.setTableGridColor(target.getTableGridColor());
            target.updateUI();
            btnGrid.setEnabled(true);
        } else {
            btnGrid.setBackground(Theme.getColor("BACKGROUND_COLOR"));
            btnGrid.setEnabled(true);
        }
        parent.frameResising(getPreferredSize());
        updateUI();
        doLayout();
        repaint();
        parent.pack();
        parent.doLayout();

    }

    public void applyTheme() {
        try {
            TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND = Theme.getColor("CUSTOMIZER_HEADING_BG");
        } catch (Exception e) {
            TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND = Color.GRAY;
        }
        advanceHead1Panel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        advanceHead2Panel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        advanceHead3Panel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        advanceHead4Panel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
//        advanceCheckBoxPanel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);

        labelOdPanel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        oddPanel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);

        labelEvenPanel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        evenPanel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);

        panellabel.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);

        p1.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);
        p2.setBackground(TableCuztomizerUI.CUSTOMIZER_HEADING_BACKGROUND);

        SwingUtilities.updateComponentTreeUI(this);
        SwingUtilities.updateComponentTreeUI(colorChooser);
        SwingUtilities.updateComponentTreeUI(fontChooser);
        SwingUtilities.updateComponentTreeUI(colorDialog);
        setBorders();
    }

    public void applyGridToAll(int gridType) {
        JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
        for (int i = 0; i < frames.length; i++) {
            if (frames[i] instanceof ClientTable) {
                TableThemeSettings table = (TableThemeSettings) ((ClientTable) frames[i]).getTable();
                table.setGdidOn(gridType); //todo grid change
//                table.setGdidOn(TableThemeSettings.GRID_HORIZONTAL); //todo grid change
                table.updateUI();
                table = null;
            }
        }
        frames = null;
    }

    public void applyGridToAllOpened(int gridType) {
        JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
        for (int i = 0; i < frames.length; i++) {
            if (frames[i].isVisible()) {
                if (frames[i] instanceof ClientTable) {
                    TableThemeSettings table = (TableThemeSettings) ((ClientTable) frames[i]).getTable();
                    table.setGdidOn(gridType); //todo grid change
//                    table.setGdidOn(TableThemeSettings.GRID_HORIZONTAL); //todo grid change
                    table.updateUI();
                    table = null;
                }
            }
        }
        frames = null;
    }

    public void applyColorChanges(TableThemeSettings target) {

        target.setUpBGColor(this.target.getUpBGColor());
        target.setUpFGColor(this.target.getUpFGColor());
        target.setDownFGColor(this.target.getDownFGColor());
        target.setDownBGColor(this.target.getDownBGColor());
        target.setRow2FGColor(this.target.getRow2FGColor());
        target.setRow2BGColor(this.target.getRow2BGColor());
        target.setRow1BGColor(this.target.getRow1BGColor());
        target.setRow1FGColor(this.target.getRow2FGColor());
        target.setAskBGColor2(this.target.getAskBGColor2());
        target.setAskFGColor2(this.target.getAskFGColor2());
        target.setBidFGColor2(this.target.getBidFGColor2());
        target.setBidBGColor2(this.target.getBidBGColor2());
        target.setAskBGColor1(this.target.getAskBGColor1());
        target.setAskFGColor1(this.target.getAskFGColor1());
        target.setBidFGColor1(this.target.getBidFGColor1());
        target.setBidBGColor1(this.target.getBidBGColor1());
        target.setTradeFGColor1(this.target.getTradeFGColor1());
        target.setTradeBGColor1(this.target.getTradeBGColor1());
        target.setTradeFGColor2(this.target.getTradeFGColor2());
        target.setTradeBGColor2(this.target.getTradeBGColor2());
        target.setChangeDownFGColor(this.target.getChangeDownFGColor());
        target.setChangeUPFGColor(this.target.getChangeUPFGColor());
        target.setHighlighterFGColor(this.target.getHighlighterFGColor());
        target.setHighlighterBGColor(this.target.getHighlighterBGColor());
        target.setMinBGColor(this.target.getMinBGColor());
        target.setMinFGColor(this.target.getMinFGColor());
        target.setMaxBGColor(this.target.getMaxBGColor());
        target.setMaxFGColor(this.target.getMaxFGColor());
        target.setWeek52LowBGColor(this.target.getWeek52LowBGColor());
        target.setWeek52LowFGColor(this.target.getWeek52LowFGColor());
        target.setWeek52HighBGColor(this.target.getWeek52HighBGColor());
        target.setWeek52HighFGColor(this.target.getWeek52HighFGColor());

        if (target.isGdidOn()) {
            target.setTableGridColor(this.target.getTableGridColor());
        }

    }

    public void setBorders() {
        /*  Component c;
      int ncomponents = colorPanel.getComponentCount();
      for (int i = 0; i < ncomponents; ++i) {
          c = colorPanel.getComponent(i);
          if (c instanceof JButton) {
              ((JButton) c).setBorder(BorderFactory.createLineBorder(Theme.getColor("LABEL_FGCOLOR")));
          }
      }
      c = null;*/
    }

    public void resetDataOnCancel() {
        try {

        target.setUpFGColor(getColorFromCommand("COLOR_FG_UP"));

        target.setUpBGColor(getColorFromCommand("COLOR_BG_UP"));

        target.setDownFGColor(getColorFromCommand("COLOR_FG_DOWN"));

        target.setDownBGColor(getColorFromCommand("COLOR_BG_DOWN"));

        target.setRow2FGColor(getColorFromCommand("COLOR_FG_ROW2"));

        target.setRow2BGColor(getColorFromCommand("COLOR_BG_ROW2"));

        target.setRow1BGColor(getColorFromCommand("COLOR_BG_ROW1"));

        target.setRow1FGColor(getColorFromCommand("COLOR_FG_ROW1"));

        target.setAskBGColor2(getColorFromCommand("COLOR_BG_ASK2"));

        target.setAskFGColor2(getColorFromCommand("COLOR_FG_ASK2"));

        target.setBidFGColor2(getColorFromCommand("COLOR_FG_BID2"));

        target.setBidBGColor2(getColorFromCommand("COLOR_BG_BID2"));

        target.setAskBGColor1(getColorFromCommand("COLOR_BG_ASK1"));

        target.setAskFGColor1(getColorFromCommand("COLOR_FG_ASK1"));

        target.setBidFGColor1(getColorFromCommand("COLOR_FG_BID1"));

        target.setBidBGColor1(getColorFromCommand("COLOR_BG_BID1"));

        target.setTradeFGColor1(getColorFromCommand("COLOR_FG_TRADE1"));

        target.setTradeBGColor1(getColorFromCommand("COLOR_BG_TRADE1"));

        target.setTradeFGColor2(getColorFromCommand("COLOR_FG_TRADE2"));

        target.setTradeBGColor2(getColorFromCommand("COLOR_BG_TRADE2"));

        target.setChangeDownFGColor(getColorFromCommand("COLOR_FG_CHANGE_DOWN"));

        target.setChangeUPFGColor(getColorFromCommand("COLOR_FG_CHANGE_UP"));

        target.setHighlighterFGColor(getColorFromCommand("COLOR_FG_SELECTED"));

        target.setHighlighterBGColor(getColorFromCommand("COLOR_BG_SELECTED"));

        target.setTableGridColor(getColorFromCommand("COLOR_GRID"));

        target.setMinFGColor(getColorFromCommand("COLOR_FG_MIN"));

        target.setMinBGColor(getColorFromCommand("COLOR_BG_MIN"));

        target.setMaxFGColor(getColorFromCommand("COLOR_FG_MAX"));

        target.setMaxBGColor(getColorFromCommand("COLOR_BG_MAX"));

        target.setWeek52HighFGColor(getColorFromCommand("COLOR_FG_WK52_HIGH"));

        target.setWeek52HighBGColor(getColorFromCommand("COLOR_BG_WK52_HIGH"));

        target.setWeek52LowFGColor(getColorFromCommand("COLOR_FG_WK52_LOW"));

        target.setWeek52LowBGColor(getColorFromCommand("COLOR_BG_WK52_LOW"));

        } catch (Exception ex) {

        }

    }

    public void setIniDataOnClose() {

        try {

        setInitialDataMap("COLOR_FG_UP", target.getUpFGColor());

        setInitialDataMap("COLOR_BG_UP", target.getUpBGColor());

        setInitialDataMap("COLOR_FG_DOWN", target.getDownFGColor());

        setInitialDataMap("COLOR_BG_DOWN", target.getDownBGColor());

        setInitialDataMap("COLOR_FG_ROW2", target.getRow2FGColor());

        setInitialDataMap("COLOR_BG_ROW2", target.getRow2BGColor());

        setInitialDataMap("COLOR_BG_ROW1", target.getRow1BGColor());

        setInitialDataMap("COLOR_FG_ROW1", target.getRow1FGColor());

        setInitialDataMap("COLOR_BG_ASK2", target.getAskBGColor2());

        setInitialDataMap("COLOR_FG_ASK2", target.getAskFGColor2());

        setInitialDataMap("COLOR_FG_BID2", target.getBidFGColor2());

        setInitialDataMap("COLOR_BG_BID2", target.getBidBGColor2());

        setInitialDataMap("COLOR_BG_ASK1", target.getAskBGColor1());

        setInitialDataMap("COLOR_FG_ASK1", target.getAskFGColor1());

        setInitialDataMap("COLOR_FG_BID1", target.getBidFGColor1());

        setInitialDataMap("COLOR_BG_BID1", target.getBidBGColor1());

        setInitialDataMap("COLOR_FG_TRADE1", target.getTradeFGColor1());

        setInitialDataMap("COLOR_BG_TRADE1", target.getTradeBGColor1());

        setInitialDataMap("COLOR_FG_TRADE2", target.getTradeFGColor2());

        setInitialDataMap("COLOR_BG_TRADE2", target.getTradeBGColor2());

        setInitialDataMap("COLOR_FG_CHANGE_DOWN", target.getChangeDownFGColor());

        setInitialDataMap("COLOR_FG_CHANGE_UP", target.getChangeUPFGColor());

        setInitialDataMap("COLOR_FG_SELECTED", target.getHighlighterFGColor());

        setInitialDataMap("COLOR_BG_SELECTED", target.getHighlighterBGColor());

        setInitialDataMap("COLOR_GRID", target.getTableGridColor());

        setInitialDataMap("COLOR_FG_MIN", target.getMinFGColor());

        setInitialDataMap("COLOR_BG_MIN", target.getMinBGColor());

        setInitialDataMap("COLOR_FG_MAX", target.getMaxFGColor());

        setInitialDataMap("COLOR_BG_MAX", target.getMaxBGColor());

        setInitialDataMap("COLOR_FG_WK52_HIGH", target.getWeek52HighFGColor());

        setInitialDataMap("COLOR_BG_WK52_HIGH", target.getWeek52HighBGColor());

        setInitialDataMap("COLOR_FG_WK52_LOW", target.getWeek52LowFGColor());

        setInitialDataMap("COLOR_BG_WK52_LOW", target.getWeek52LowBGColor());

            //      }
        } catch (Exception ex) {

        }

    }

    public Font getBodyFont() {
        return newBodyFont;
    }

    public int getBodyGap() {
        return bodyGap;
    }

    public Font getHeadingFont() {
        return newHeadingFont;
    }
}
