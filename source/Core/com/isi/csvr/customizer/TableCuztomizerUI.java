package com.isi.csvr.customizer;

import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.Client;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.event.Application;
import com.isi.csvr.theme.TableThemeSettings;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Mar 6, 2009
 * Time: 2:12:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class TableCuztomizerUI extends JDialog implements ActionListener {
    public static TableCuztomizerUI self;
    private JPanel panel1 = new JPanel();
    public static Color CUSTOMIZER_HEADING_BACKGROUND = Color.GRAY;

    private ColorConfigurationPanel colorConfigPanel;


    public static TableCuztomizerUI getSharedInstance() {
        if (self == null) {
            self = new TableCuztomizerUI();
        }
        return self;
    }

    private TableCuztomizerUI(Frame frame, String title, boolean modal) {
        super(frame, title, modal);
        try {
            jbInit();
            pack();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private TableCuztomizerUI() {
        this(Client.getInstance().getFrame(), Language.getString("CUSTOMIZER"), true);
        //colorConfigPanel.setTarget(target);
        setLocationRelativeTo(Client.getInstance().getFrame());
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        colorConfigPanel.registerKeyboardAction(this, "ESC", KeyStroke.getKeyStroke("released ESCAPE"), JComponent.WHEN_IN_FOCUSED_WINDOW);//KeyEvent.VK_ESCAPE,0
    }


    public void show(TableThemeSettings target) {
        colorConfigPanel.setTarget(target);
        colorConfigPanel.applyTheme();
        show();
    }

    public Font getBodyFont() {
        return colorConfigPanel.getBodyFont();
    }

    public int getBodyGap() {
        return colorConfigPanel.getBodyGap();
    }

    public Font getHeadingFont() {
        return colorConfigPanel.getHeadingFont();
    }

    private void jbInit() throws Exception {
        panel1.setLayout(new FlowLayout());
         GUISettings.applyOrientation(this);
        this.setResizable(false);
        colorConfigPanel = new ColorConfigurationPanel(this);
        getContentPane().add(colorConfigPanel);
    }

    public void actionPerformed(ActionEvent e) {
        setVisible(false);
    }

    public void frameResising(Dimension preferredSize) {
        this.setPreferredSize(new Dimension(preferredSize.width, preferredSize.height + GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT + 8));
    }

}

