package com.isi.csvr.customizer;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;

/**
 * <p>Title: TW Test Projects</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: ISI</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class CustomizeListRenderer extends JPanel implements ListCellRenderer {

    private JLabel lblFieldLabel;
    private JLabel lblFieldFG;
    private JLabel lblFieldBG;
    private Color foreground;

    public CustomizeListRenderer() {
        this.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 2));
        lblFieldLabel = new JLabel();
        lblFieldFG = new JLabel();
        lblFieldBG = new JLabel();

        lblFieldLabel.setPreferredSize(new Dimension(150, 20));
        lblFieldFG.setPreferredSize(new Dimension(60, 20));
        lblFieldBG.setPreferredSize(new Dimension(60, 20));

        lblFieldFG.setOpaque(true);
        lblFieldBG.setOpaque(true);

//        lblFieldBG.setBorder(BorderFactory.createLineBorder(Color.black));
//        lblFieldFG.setBorder(BorderFactory.createLineBorder(Color.black));
        this.setBackground(UIManager.getColor("List.background"));
        foreground = UIManager.getColor("Label.foreground");
        this.add(lblFieldLabel);
        this.add(lblFieldBG);
        this.add(lblFieldFG);

        GUISettings.applyOrientation(this);
    }

    public void setForegroundColor(){
        foreground = UIManager.getColor("Label.foreground");
    }


    public Component getListCellRendererComponent(JList list, Object value,
                                                  int index, boolean isSelected, boolean hasFocus) {
        CustomizerRecord dataObject = (CustomizerRecord) value;
        boolean isEnabled = list.isEnabled();
        lblFieldLabel.setText(dataObject.getDescription());
//        lblFieldLabel.setForeground(Color.black);
//        lblFieldLabel.setForeground(UIManager.getColor("Label.foreground"));
        lblFieldLabel.setForeground(foreground);

        if (dataObject.getBgColor() != null) {
            lblFieldBG.setBackground(dataObject.getBgColor());
            lblFieldBG.setEnabled(true);
            lblFieldBG.setBorder(BorderFactory.createLineBorder(Color.black));
        } else {
            lblFieldBG.setEnabled(false);
            lblFieldBG.setBackground(list.getBackground());
            lblFieldBG.setBorder(BorderFactory.createEmptyBorder());
        }
        if (dataObject.getFgColor() != null) {
            lblFieldFG.setBackground(dataObject.getFgColor());
            lblFieldFG.setEnabled(true);
            lblFieldFG.setBorder(BorderFactory.createLineBorder(Color.black));
        } else {
            lblFieldFG.setEnabled(false);
            lblFieldFG.setBackground(list.getBackground());
            lblFieldFG.setBorder(BorderFactory.createEmptyBorder());
        }
        if (!isEnabled) {
            lblFieldBG.setBackground(list.getBackground());
            lblFieldFG.setBackground(list.getBackground());
        }
        if (isSelected) {
            lblFieldLabel.setFont(Theme.getDefaultFont(Font.BOLD, 12)); // new TWFont("Arial", Font.BOLD, 12));
            if (lblFieldBG.isEnabled())
                lblFieldBG.setBorder(BorderFactory.createLineBorder(Color.red));
            if (lblFieldFG.isEnabled())
                lblFieldFG.setBorder(BorderFactory.createLineBorder(Color.red));
        } else {
            lblFieldLabel.setFont(Theme.getDefaultFont(Font.PLAIN, 12)); // new TWFont("Arial", Font.PLAIN, 12));
//            lblFieldBG.setBorder(BorderFactory.createLineBorder(Color.black));
//            lblFieldFG.setBorder(BorderFactory.createLineBorder(Color.black));
        }

//        lblFieldLabel.setEnabled(isEnabled);
//        lblFieldBG.setEnabled(isEnabled);
//        lblFieldFG.setEnabled(isEnabled);
        return this;
    }

    public JLabel getFGLabel() {
        return lblFieldFG;
    }

    public JLabel getBGLabel() {
        return lblFieldBG;
    }
}