// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr;

/**
 * Creates the main menu and the tool bar for the gui
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.announcement.AnnouncementStore;
import com.isi.csvr.cashFlowWatch.CashFlowHistory;
import com.isi.csvr.cashFlowWatch.CashFlowWatcherUI;
import com.isi.csvr.chart.options.ChartOptionsWindow;
import com.isi.csvr.chart.radarscreen.ui.RadarScreenWindow;
import com.isi.csvr.commission.CommissionConfig;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.customformular.FormularEditor;
import com.isi.csvr.customindex.CustomIndexWindow;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.TimeZoneMap;
import com.isi.csvr.downloader.ui.DownloadProgressView;
import com.isi.csvr.event.CurrencyListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.event.VASMenuListener;
import com.isi.csvr.help.HelpItem;
import com.isi.csvr.help.HelpManager;
import com.isi.csvr.help.TipOfDayWindow;
import com.isi.csvr.ie.BrowserManager;
import com.isi.csvr.ie.IEBrowser;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.iframe.TWWindowMenuItem;
import com.isi.csvr.iframe.TWWindowMenuItemSorter;
import com.isi.csvr.middlewarehandler.MWMenuCreationListener;
import com.isi.csvr.middlewarehandler.MWMenuCreationNotifier;
import com.isi.csvr.middlewarehandler.MWTradingHandler;
import com.isi.csvr.ohlcbacklog.OHLCDownloaderUI;
import com.isi.csvr.plugin.PluginMenuLoader;
import com.isi.csvr.profiles.export.DataSelectionTree;
import com.isi.csvr.profiles.imprt.OpenZipFileTree;
import com.isi.csvr.radarscreen.RadarScreenOptionsDialog;
import com.isi.csvr.reports.ReportManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.sideBar.SideBar;
import com.isi.csvr.smartalerts.SmartAlertSummeryWindow;
import com.isi.csvr.smartalerts.SmartAlertWindow;
import com.isi.csvr.table.IndexPanel;
import com.isi.csvr.table.StretchedIndexPanel;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.trade.DetailedTradeStore;
import com.isi.csvr.tradebacklog.TradeDownloaderUI;
import com.isi.csvr.trading.BrokerConfig;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.positionMoniter.IntraDayPositionMoniterUI;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.trading.ui.CustomerStatements;
import com.isi.csvr.trading.ui.NewPFCreationWindow;
import com.isi.csvr.troubleshoot.TroubleshootUI;
import com.isi.csvr.volumewatcher.VolumeWatcherUI;
import com.isi.csvr.win32.NativeMethods;
import com.mubasher.outlook.Reminder;
import com.mubasher.outlook.ReminderUI;

import javax.swing.*;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.event.TreeExpansionEvent;
import javax.swing.event.TreeExpansionListener;
import java.awt.*;
import java.awt.Color;
import java.awt.Component;
import java.awt.FlowLayout;
import java.awt.Dimension;
import java.awt.BorderLayout;
import java.awt.event.*;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Enumeration;
import java.util.Hashtable;

import com.isi.csvr.middlewarehandler.MWTradingHandler;
import com.isi.csvr.middlewarehandler.MWMenuCreationListener;
import com.isi.csvr.middlewarehandler.MWMenuCreationNotifier;

public class Menus extends JMenu implements ActionListener, MenuListener, TreeExpansionListener, MouseListener, ExchangeListener, CurrencyListener, VASMenuListener, TradingConnectionListener, MWMenuCreationListener {

    /* Main menu */
    private TWMenuBar menuBar = new TWMenuBar();
    private JMenu mnuSystem;//= new JMenu();
    private JMenu mnuTrade;
    private JMenu mnuMWTrade;
    private JMenu mnuWebTrade;
    private JMenu mnuWatchLists;// = new JMenu();
    private JMenu mnuView;//= new JMenu();
    private JMenu mnuCustomize;// = new JMenu();
    private JMenu mnuTools;// = new JMenu();
    private JMenu mnuAnalysis;
    private JMenu mnuReports;//= new JMenu();
    private JMenu mnuVAS;
    private JMenu mnuWindows;//= new JMenu();
    private JMenu mnuHelp;// = new JMenu();
    //    private JMenu mnuWindow = new JMenu();
    private JMenu themesMenu;
    private TWMenu mnuWorkspaces = new TWMenu();
    private TWMenu mnuMyStocks = new TWMenu();
    private TWMenu mnuNewItems = new TWMenu(Language.getString("NEW"), "new.gif");
    private TWMenu mnuLangSelection;
    private TWMenu mnuTimeOutSelection;
    private TWMenu mnuOnlineTrading = null;
    private TWMenu mnuTradingHistory = null;
    private TWMenu mnuValuation = null;
    private TWMenu mnuServices = null;

    public JPopupMenu langList;//=new JPopupMenu();
    private TWMenuItem mnuHelpContents;
    private TWMenuItem mnuTipOfDay;
    private TWMenuItem mnuAddOrder;
    private TWMenuItem mnuPendingOrders;
    private TWMenuItem mnuOrderHistory;
    private TWMenuItem mnuExecutions;
    private TWMenuItem mnuInvoices;
    private TWMenuItem mnuPosition;
    private TWMenuItem mnuStatement;
    private TWMenuItem mnuFundTransaction;
    private TWMenuItem mnuAccountServices;
    private TWMenuItem mnuReportsServices;
    private TWMenuItem mnuDownloadsServices;

    private TWMenuItem mnuTradeHelpContents;
    private TWMenuItem mnuHelpWhatzNew;
    private TWMenuItem mnuHelpAbout;
    private TWMenuItem mnuHelpQRG;
    private TWMenuItem mnuHelpSettingUp;
    private TWMenuItem mnuTutorial;
    private TWMenuItem mnuOhlcDownloadTrades;
    private TWMenuItem mnuChatWithCST;
    private TWMenuItem mnuExport;
    private TWMenuItem mnuSectorMap;
    private TWMenuItem mnuDownloadTrades;
    private TWMenuItem mnuDownloadTodaysTrades;
    private TWCheckBoxMenuItem mnuArabicNumbers;
    private TWCheckBoxMenuItem mnuAlias;
    private TWCheckBoxMenuItem mnuFullScreen;
    private TWCheckBoxMenuItem mnuShowTabs;
    private TWCheckBoxMenuItem mnuShowCombinedOrderBook;
    private TWCheckBoxMenuItem mnuPopupAnnouncmWindow;
    private TWCheckBoxMenuItem mnuSaveTradesOnExit;
    private TWCheckBoxMenuItem mnuShowMinMax;
    private TWCheckBoxMenuItem mnuShow52WkHighLow;
    //    TWCheckBoxMenuItem mnuSummaryTicker;
    private TWMenuItem mnuConnect;
    private TWMenuItem mnuTradeConnect;
    private TWMenuItem g_mnuTicker;
    private TWMenuItem mnuExit;
    private TWMenuItem mnuConfig;
    private TWSeparator exportSeperator;
    private TWMenuItem mnuImportData;
    private TWMenuItem mnuExportData;
    private TWMenuItem mnuMakeDefault;
    private TWMenuItem mnuChangePass;
    private TWMenuItem mnuChangeUSBPin;
    private TWMenuItem mnuSave;
    private TWMenuItem mnuSaveAs;
    private TWMenuItem mnuPageFormat;
    private TWMenuItem mnuOpen;
    private TWMenuItem mnuNewMyStock;
    private TWMenuItem mnuMistView;
    private TWMenuItem mnuForexView;
    private TWMenuItem mnuCustomFormular;
    private TWMenuItem mnuOutlook;
    private TWMenuItem mnuNewFilteredList;
    private TWMenuItem mnuNewMyPortfolio;
    private TWMenuItem mnuAnnounceSearch;
    private TWCheckBoxMenuItem mnuSetFrameLayer;
    private TWMenuItem what_ifCalc;
    private TWMenuItem g_mnuOptionSymbolBuilder;
    private TWMenuItem autoUpdateMetaStock;
    private TWMenuItem autoUpdateTradeStation;
    //    TWMenuItem mnuNewsSearch;
    //    private TWMenuItem mnuNewsSearchNew;
    private TWMenuItem mnuAlerts;
    private TWMenuItem mnuReport1;
    private TWMenuItem mnuReport2;
    private TWMenuItem mnuReport3;


    private TWMenuItem mnuTimenSales;
    private TWMenuItem mnuAnnouncements;
    private TWMenuItem mnuPortfolio;
    private TWMenuItem mnuMutualFunds;
    private TWMenuItem mnuNews;
    private TWMenuItem mnuPerformaceHive;
    private TWMenuItem mnuSubscriptions;
    private TWMenuItem g_mnuExchangeSummary; // Added on 02-07-02
    //    TWMenuItem g_mnuMarketSummary;
    private TWMenuItem g_mnuMarketIndices; // Added on 02-07-02
    private TWMenuItem g_mnuGlobalIndex; // Added on 02-07-02
    private TWMenuItem mnuBroadcastMessages;
    private TWMenuItem currency;
    private TWMenuItem g_mnuHistoryAnalyzer;
    private TWMenuItem g_mnuBrokerList;
    private TWMenuItem g_mnuTopStocks;
    private TWMenuItem g_mnuTopBrokers;
    private TWMenuItem g_mnuGraph;
    private TWMenuItem mnuEconomicsCalendar;
    private TWMenuItem mnuEarningsCalendar;
    private TWMenuItem g_mnuDowload;
    private TWMenuItem g_mnuCancelDowload;
    private TWMenuItem mnuUpdateMetaStockHistoryDB;
    private TWMenuItem mnuOpenMetastockFolder;
    private TWMenuItem mnuOpenMetaStoc;

    private TWMenuItem mnuUpdateTradeStationHistoryDB;
    private TWMenuItem mnuOpenTradeStationFolder;
    private TWMenuItem mnuOpenTradeStation;

    private TWMenuItem mnuCascadeWindows;
    private TWMenuItem mnuTileWindows;
    private TWMenuItem mnuTileWindowsVertical;
    private TWMenuItem mnuTileWindowsHorizontal;
    private TWMenuItem mnuMarketMap;
    private TWMenuItem mnuStMarketMap;
    private TWMenuItem mnuCommission;
    private TWMenu mnuTimeZones;
    private TWMenuItem mnuSelectedSymbols;
    private TWCheckBoxMenuItem mnuChangeLayer;
    private TWMenu mnuCurrencies;
    private TWMenuItem mnuTimeLagIndicator;
    //    TWMenuItem reloadVas;
    private TWCheckBoxMenuItem g_mnuAutoScroll;
    private TWMenuItem mnuRadarScreenProperties;
    private TWMenuItem mnuCustomIndex;
    private TWMenuItem mnuScanner;
    private TWMenuItem mnuRadarScreen;
    private TWMenuItem mnuCASearch;
    private TWMenuItem mnuMultiDialog;
    private TWMenuItem mnuTroubleshoot;

    private JLabel subscription;
    private JLabel subscriptionDoInvest;
    private JLabel tradeUserName;


    private ToolBarButton g_btnConnect;
    private ToolBarButton g_btnTradeConnect;
    private ToolBarButton g_btnExit;
    private ToolBarButton g_btnNews;
    private ToolBarButton g_btnBrowser;
    private ToolBarButton g_btnTimeNSales;
    private ToolBarButton g_btnCustTicker;
    private ToolBarButton g_btnExportHistory;
    private ToolBarButton btnDownload;
    private ToolBarButton btnTicker;
    private ToolBarButton btnLanguage;
    private ToolBarButton btnUniChart;
    private ToolBarButton btnMetaStock;
    //    ToolBarButton g_btnHidepanel;
    private JButton g_btnHidepanel;

    private JLabel lblHideToolBar;
    private JLabel lblShowToolBar;
    private JLabel lblShowDownloads;
    private JLabel lblStockNetLogo;
    private static TWImageLabel lblPriceConnectionStatus;
    private static TWImageLabel lblTradeConnectionStatus;
    private static TWImageLabel lblUSConnectionStatus;
    private Client client;
    private Hashtable g_oWindowList = new Hashtable();
    private String g_sCurrentThemeID;

    private TWWindowMenuItemSorter windowItemComparator;
    private TWMenuItem mnuKeyFunctions;
    public TWFrame keyWindow;
    private TWMenuItem mnuBuy;
    private TWMenuItem mnuSell;
    private TWMenuItem mnuRapidOrder;
    private TWMenuItem mnuMiniTrade;
    //    private TWMenuItem mnuBracketOrder;
    private TWMenuItem mnuTradesPending;
    private TWMenuItem mnuOpenPositions;
    private TWMenuItem mnuOrderQ;
    private TWMenuItem progOrders;
    private TWMenuItem mnuTradePortfolio;
    private TWMenuItem mnuCash;
    private TWMenuItem mnuDepositNOtification;
    private TWMenuItem mnuWithdrawalReq;
    private TWMenuItem mnuFundTransWin;
    private TWMenuItem mnuCashTransReq;
    private TWMenu tradeServices;
    private TWMenuItem mnuIPO;
    TWMenu mnuCalcSelection;
    private TWMenuItem mnuMarginableSymbolsUI;
    //    private TWMenuItem mnuTradeHistory;
    private TWMenuItem mnuChangeTradePass;
    private TWMenuItem mnuDUInvestURL;
    private TWMenuItem mnuSearchOrders;
    private TWMenuItem mnuNewPFCreation;
    private TWMenuItem mnuTodayTrades;
    private TWMenuItem mnuCashLogOrders;
    private TWMenuItem mnuAccount;
    private TWMenuItem mnuIntradyPositionMoniter;
    private TWMenu currencyMenu;
    private TWMenu watchlistFilterMenu;
    private WatchlistFilterUI watchlistFilterUI;
    private TWMenuItem mnuCustomerSupport;
    private JCheckBox chkDoNotAutoPopupAnnouncements;

    //selection panel popup
    private JPopupMenu selectionPopup;
    private TWRadioButtonMenuItem mainIndexPanel;
    private TWRadioButtonMenuItem strechedIndexpanel;
    private TWRadioButtonMenuItem symbolPanel;
    private TWRadioButtonMenuItem graphIndex;
    private TWMenuItem sidebar;
    public static boolean isShowTabIndex = true;
    private TWMenuItem volumeWatcher;
    private TWMenuItem cashFlowWatcher;
    private TWMenuItem cashFlowWatcherHistory;
    private TWMenuItem globalMarket;
    private TWMenuItem mnuSmartAlerts;
    private TWMenuItem mnuSmartAlertSummary;
    private TWMenu mnuSmartAlert;

    private TWMenu mnuPreferences;
    private TWMenu mnuDownloadManager;
    private TWMenu mnuMetaStock;
    private TWMenu mnuTradeStation;
    private TWMenuItem mnuAppSettings;
    private TWMenuItem mnuChartSettings;
    private TWSeparator duSeperator = new TWSeparator();

    public static boolean isShowMinMax = true;
    public static boolean isShow52WkhighLow = true;
    public static boolean is52WeekHighlightRows = true;
    public static boolean isMinMaxHighlightRows = true;


    final TWCheckBoxMenuItem mnuShowTradeAlertPopup = new TWCheckBoxMenuItem(Language.getString("SHOW_TRADE_ALERT_POPUP"), "popupTradealert.gif", true);

    /**
     * Constructor
     */
    public Menus(Client oClient) {
        client = oClient;
        windowItemComparator = new TWWindowMenuItemSorter();
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        MWMenuCreationNotifier.getInstance().addMenuCreationListener(this);
    }

    public void createSystemMenu() {
        mnuNewItems = new TWMenu(Language.getString("NEW"), "new.gif");

        mnuNewMyStock = new TWMenuItem(Language.getString("WATCH_LIST"), true);
        mnuNewMyStock.setAcceleratorID(TWActions.INT_WATCH_LIST);
        mnuNewMyStock.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.createNewView(true, null);
            }
        });
        mnuNewMyStock.setEnabled(false);
        mnuNewItems.add(mnuNewMyStock);

        mnuMistView = new TWMenuItem(Language.getString("MIST_VIEW"), true);
        mnuMistView.setAcceleratorID(TWActions.INT_MIST_VIEW);
        mnuMistView.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.createNewMistView(true, null);
            }
        });
        mnuMistView.setEnabled(false);
//        mnuNewItems.add(mnuMistView);

        mnuForexView = new TWMenuItem(Language.getString("FOREX_WATCHLIST"), true);
        mnuForexView.setAcceleratorID(TWActions.INT_FOREX_WATCHLIST);
        mnuForexView.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.createNewForexView(true, null);
            }
        });
        mnuForexView.setEnabled(false);
        mnuForexView.setVisible(false);
        mnuNewItems.add(mnuForexView);


        mnuNewFilteredList = new TWMenuItem(Language.getString("FUNCTION_WATCHLIST"), true);
        mnuNewFilteredList.setAcceleratorID(TWActions.INT_FUNCTION_WATCHLIST);
        mnuNewFilteredList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.createNewFilteredView(true, null);
            }
        });
        //mnuNewFilteredList.setVisible(false);
        mnuNewFilteredList.setEnabled(false);
        mnuNewItems.add(mnuNewFilteredList);

        mnuNewMyPortfolio = new TWMenuItem(Language.getString("MY_PORTFOLIO"), true);
        mnuNewMyPortfolio.setAcceleratorID(TWActions.INT_MY_PORTFOLIO);
        mnuNewMyPortfolio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.createNewPortfolio(null, null, null);
            }
        });
        mnuNewMyPortfolio.setEnabled(false);
        //mnuNewItems.add(mnuNewMyPortfolio);

        mnuConnect = new TWMenuItem(Language.getString("CONNECT"), true);
//        mnuConnect.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_N,KeyEvent.CTRL_DOWN_MASK));
        mnuConnect.setAcceleratorID(TWActions.INT_CONNECT);
        mnuConnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (Settings.isConnected()) {
//                    client.disconnectActionPerformed(); to be implemented
                } else {
                    client.connectActionPerformed(true);
                }
            }
        });

        mnuTradeConnect = new TWMenuItem(Language.getString("CONNECT_TO_BROKER"), "connecttobroker.gif", true);
//        mnuTradeConnect.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_B,KeyEvent.CTRL_DOWN_MASK));
        mnuTradeConnect.setAcceleratorID(TWActions.INT_CONNECT_TO_TRADE);
        mnuTradeConnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_TradingConnect_ActionPerformed(true);
            }
        });
        mnuTradeConnect.setEnabled(false);

        mnuChangePass = new TWMenuItem(Language.getString("CHANGE_PASSWORD"), true);
        mnuChangePass.setAcceleratorID(TWActions.INT_CHANGE_PASSWORD);
        mnuChangePass.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_mnuChangePass_ActionPerformed();
            }
        });


        mnuConfig = new TWMenuItem(Language.getString("CONFIGURE"), true);
//        mnuConfig.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_F, KeyEvent.CTRL_DOWN_MASK));
        mnuConfig.setAcceleratorID(TWActions.INT_CONFIGURE);
        mnuConfig.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_Config_ActionPerformed();
            }
        });
        if ((Settings.getBooleanItem("CONFIG_ALLOWED"))) {
            mnuConfig.setEnabled(true);
        } else {
            mnuConfig.setEnabled(false);
        }

        exportSeperator = new TWSeparator();

        mnuExportData = new TWMenuItem(Language.getString("EXPORT_USER_DATA"), "exportuserdata.gif", true);
        mnuExportData.setAcceleratorID(TWActions.INT_EXPORT_USER_DATA);
        mnuExportData.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DataSelectionTree ExSelect = new DataSelectionTree();
                ExSelect.setVisible(true);
            }
        });


        mnuImportData = new TWMenuItem(Language.getString("IMPORT_USER_DATA"), "importuserdata.gif", true);
        mnuImportData.setAcceleratorID(TWActions.INT_IMPORT_USER_DATA);
        mnuImportData.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (Settings.hasImporteddata()) {
                    String str = Language.getString("MSG_RESTART_TO_APPLY_IMPORT_PREVIOUS");
                    SharedMethods.showMessage(str, JOptionPane.WARNING_MESSAGE);
                } else {
                    try {
                        OpenZipFileTree ImSelect = new OpenZipFileTree();
                        (ImSelect.getObject()).setVisible(true);
                    } catch (Exception e1) {
//                        e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        });


        mnuOpen = new TWMenuItem(Language.getString("OPEN_FILE"), true);
//       mnuOpen.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_O, KeyEvent.CTRL_DOWN_MASK));
        mnuOpen.setAcceleratorID(TWActions.INT_OPEN_FILE);
        mnuOpen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnuOpen_ActionPerformed();
            }
        });
        mnuOpen.setEnabled(false);


        mnuSave = new TWMenuItem(Language.getString("SAVE"), true);
//        mnuSave.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_S, KeyEvent.CTRL_DOWN_MASK));
        mnuSave.setAcceleratorID(TWActions.INT_SAVE_WORKSPACE);
        mnuSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnuSave_ActionPerformed();
            }
        });
        mnuSave.setEnabled(false);


        mnuSaveAs = new TWMenuItem(Language.getString("SAVE_AS"), true);
//        mnuSaveAs.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_E, KeyEvent.CTRL_DOWN_MASK));
        mnuSaveAs.setAcceleratorID(TWActions.INT_SAVE_AS);
        mnuSaveAs.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnuSaveAs_ActionPerformed();
            }
        });
        mnuSaveAs.setEnabled(false);


        mnuPageFormat = new TWMenuItem(Language.getString("PAGE_SETUP"), "pagesetup.gif", true);
//        mnuPageFormat.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_P, KeyEvent.CTRL_DOWN_MASK));
        mnuPageFormat.setAcceleratorID(TWActions.INT_PAGE_SETUP);
        mnuPageFormat.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnuShowPageFormat();
            }
        });


        mnuMakeDefault = new TWMenuItem(Language.getString("SET_DEFAULT_WS"), "makedefaultwsp.gif", true);
        mnuMakeDefault.setAcceleratorID(TWActions.INT_SET_DEFAULT_WS);
        mnuMakeDefault.setEnabled(false);
        mnuMakeDefault.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnuMakeDefault_ActionPerformed();
            }
        });

        //--

        mnuExit = new TWMenuItem(Language.getString("EXIT"), true);
//        mnuExit.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_T, KeyEvent.CTRL_DOWN_MASK));
        mnuExit.setAcceleratorID(TWActions.INT_EXIT);
        mnuExit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.systemExit_ActionPerformed();
            }
        });


        mnuSystem.add(mnuNewItems);
        if (Settings.isTCPMode()) {
            mnuSystem.add(mnuConnect);
            mnuSystem.add(mnuTradeConnect);
            mnuChangePass.setEnabled(false);
//            mnuChangePass.setVisible(false);
            mnuSystem.add(mnuChangePass);
        }

        mnuSystem.add(mnuConfig);
        mnuSystem.add(exportSeperator);
        mnuSystem.add(mnuExportData);
        mnuSystem.add(mnuImportData);
        mnuSystem.add(new TWSeparator());
        mnuSystem.add(mnuOpen);
        mnuSystem.add(mnuSave);
        mnuSystem.add(mnuSaveAs);
        mnuSystem.add(mnuPageFormat);
        mnuSystem.add(mnuMakeDefault);
        mnuSystem.add(new TWSeparator());
        mnuSystem.add(mnuExit);

    }

    private void createTradeMenu() {

        mnuBuy = new TWMenuItem(Language.getString("BUY"), "buy.gif", true);
        mnuBuy.setAcceleratorID(TWActions.INT_BUY_SCREEN);
        mnuBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.doTransaction(TradeMeta.BUY, null, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
            }
        });


        mnuSell = new TWMenuItem(Language.getString("SELL"), "sell.gif", true);
        mnuSell.setAcceleratorID(TWActions.INT_SELL_SCREEN);
        mnuSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.doTransaction(TradeMeta.SELL, null, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
            }
        });


        mnuRapidOrder = new TWMenuItem(Language.getString("RAPID_ORDEERS"), "rapidOrder.gif", true);
        mnuRapidOrder.setAcceleratorID(TWActions.INT_RAPID_ORDERS);
        mnuRapidOrder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnuRapidOrderActionPerformed();
            }
        });


        mnuMiniTrade = new TWMenuItem(Language.getString("MINI_TRADE"), "minitradesum.gif", true);
        mnuMiniTrade.setAcceleratorID(TWActions.INT_MINI_TRADES);
        mnuMiniTrade.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.doMiniTradeSymbol("", System.currentTimeMillis() + "");
            }
        });

//        mnuBracketOrder = new TWMenuItem(Language.getString("BASKET_ORDERS"), "bracketorders.gif");
//        mnuBracketOrder.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//            }
//        });


        mnuTradesPending = new TWMenuItem(Language.getString("BLOTTER"), "orderlist.gif", true);
        mnuTradesPending.setAcceleratorID(TWActions.INT_ORDER_LIST);
        mnuTradesPending.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_Bloter_ActionPerformed();
            }
        });


        mnuOpenPositions = new TWMenuItem(Language.getString("OPENPOSITIONS"), "openpositions.gif", true);
        mnuOpenPositions.setAcceleratorID(TWActions.INT_OPEN_POSITIONS);
        mnuOpenPositions.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnuOpenPositionsActionPerformed();
            }
        });
//        if (BrokerConfig.isOPenPositionsAvailable()) {

//        }

        mnuOrderQ = new TWMenuItem(Language.getString("BASKET_ORDERS"), "basketorders.gif", true);
        mnuOrderQ.setAcceleratorID(TWActions.INT_BASKET_ORDERS);
        mnuOrderQ.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_OrderQ_ActionPerformed();
            }
        });


        progOrders = new TWMenuItem(Language.getString("PROG_ORDERS"), true);
        progOrders.setAcceleratorID(TWActions.INT_PROGRAMED_ORDERS);
        progOrders.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_ProgOrders_ActionPerformed();
            }
        });


        mnuTradePortfolio = new TWMenuItem(Language.getString("TRADE_PORTFOLIO"), "portfolio.gif", true);
        mnuTradePortfolio.setAcceleratorID(TWActions.INT_TRADE_PORTFOLIO);
        mnuTradePortfolio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.createTradingPortfolioView(true);
            }
        });


        mnuSearchOrders = new TWMenuItem(Language.getString("ORDER_SEARCH"), "ordersearch.gif", true);
        mnuSearchOrders.setAcceleratorID(TWActions.INT_ORDER_SEARCH);
        mnuSearchOrders.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().showOrderSearchWindow();
            }
        });

        //added by Shanaka - New portfolio creation - SABB CR
        mnuNewPFCreation = new TWMenuItem(Language.getString("ADDITIONAL_PORTFOLIO_TITLE"), "portfolio_create.gif", true);
        mnuNewPFCreation.setAcceleratorID(TWActions.INT_ORDER_SEARCH);
        mnuNewPFCreation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().showNewPFCreationWindow();
            }
        });


        mnuTodayTrades = new TWMenuItem(Language.getString("TODAY_TRADES"), "ordersearch.gif", true);
   //     mnuTodayTrades.setAcceleratorID(TWActions.INT_ORDER_SEARCH);
        mnuTodayTrades.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().showTodayTradeWindow();
            }
        });


        mnuCashLogOrders = new TWMenuItem(Language.getString("CASHLOG_SEARCH"), "cashlogsearch.gif", true);
        mnuCashLogOrders.setAcceleratorID(TWActions.INT_CASH_LOG_SEARCH);
        mnuCashLogOrders.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().showCashLogSearchWindow();
            }
        });


        mnuAccount = new TWMenuItem(Language.getString("ACCOUNT_SUMMARY"), "accountsummary.gif", true);
        mnuAccount.setAcceleratorID(TWActions.INT_ACCOUNT_SUMMARY);
        mnuAccount.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().showAccountFrame();
            }
        });


        mnuCash = new TWMenuItem(Language.getString("CUSTOMER_STATEMENTS"), "customerstatement.gif", true);
        mnuCash.setAcceleratorID(TWActions.INT_CUSTOMER_STATEMENTS);
        mnuCash.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (CustomerStatements.getSharedInstance() != null) {
                    CustomerStatements.getSharedInstance().show();
                } else {
                    CustomerStatements customerStatements = new CustomerStatements();
                    customerStatements.show();
                }
            }
        });


        mnuDepositNOtification = new TWMenuItem(Language.getString("FUND_TRANSFER_NOTIFICATION"), "fundtransfer.gif", true);
        mnuDepositNOtification.setAcceleratorID(TWActions.INT_DEPOSIT_NOTIFICATION);
        mnuDepositNOtification.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().showDepositeFrame();
            }
        });


        mnuWithdrawalReq = new TWMenuItem(Language.getString("WITHDRAWAL_REQUEST"), "withdrawalreq.gif", true);
        mnuWithdrawalReq.setAcceleratorID(TWActions.INT_WITHDRAW_REQUEST);
        mnuWithdrawalReq.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().showWithdrawalRequest(false); // default transfer mode
            }
        });


        mnuFundTransWin = new TWMenuItem(Language.getString("FUND_TRANSFER_LIST"), "fundtransferlist.gif", true);
        mnuFundTransWin.setAcceleratorID(TWActions.INT_FUND_TRANSFER_LIST);
        mnuFundTransWin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().showFundTransferFrame();
            }
        });


        mnuCashTransReq = new TWMenuItem(Language.getString("CASH_TRANSFER"), "cashtransfer.gif", true);
        mnuCashTransReq.setAcceleratorID(TWActions.INT_CASH_TRANSFER);
        mnuCashTransReq.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().showCashTransferFrame();
            }
        });

        tradeServices = new TWMenu(Language.getString("TRADE_SERVICES"), "tradeservices.gif");

        mnuIPO = new TWMenuItem(Language.getString("SUBSCRIBE_IPO"), "ipo.gif", true);
        mnuIPO.setAcceleratorID(TWActions.INT_SUBSCRIBE_IPO);
        mnuIPO.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().showIPOSubscribeFrame();
            }
        });

        TWMenuItem mnuStockTransReq = new TWMenuItem(Language.getString("STOCK_TRANSFER_REQUEST"), "stocktranfersreq.gif", true);
        mnuStockTransReq.setAcceleratorID(TWActions.INT_STOCK_TRANSFER_REQUEST);
        mnuStockTransReq.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().showStockTransferWindow();
            }
        });

        TWMenuItem mnuStockTransList = new TWMenuItem(Language.getString("STOCK_TRANSFER_LIST"), "stocktransferlist.gif", true);
        mnuStockTransList.setAcceleratorID(TWActions.INT_STOCK_TRANSFER_LIST);
        mnuStockTransList.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_StockTrans_ActionPerformed();
            }
        });


        mnuMarginableSymbolsUI = new TWMenuItem(Language.getString("MARGINABLE_SYMBOLS"), "margin.gif", true);
        mnuMarginableSymbolsUI.setAcceleratorID(TWActions.INT_MARGINABLE_SYMBOLS);
        mnuMarginableSymbolsUI.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().showMarginableSymbolFrame();
            }
        });

        mnuChangeTradePass = new TWMenuItem(Language.getString("CHANGE_TRADE_PASSWORD"), "changepassword.gif", true);
        mnuChangeTradePass.setAcceleratorID(TWActions.INT_CHANGE_TRADE_PASSWORD);
        mnuChangeTradePass.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.changeTradePass(null);
            }
        });

        mnuDUInvestURL = new TWMenuItem(Language.getString("SUBSCRIPTION_URL_DUINVEST"), "duinvest.gif", true);
//        mnuDUInvestURL.setAcceleratorID(TWActions.INT_CHANGE_TRADE_PASSWORD);
        mnuDUInvestURL.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                Client.getInstance().getWebBrowserFrame().setURL(TradeMethods.doInvestClicked(), Language.getString("SUBSCRIPTION_URL_DUINVEST"));
                BrowserManager.getInstance().navigate(Language.getString("SUBSCRIPTION_URL_DUINVEST"), TradeMethods.doInvestClicked(), Language.getString("SUBSCRIPTION_URL_DUINVEST"));
            }
        });
        mnuDUInvestURL.setEnabled(false);
        mnuDUInvestURL.setVisible(false);
        duSeperator.setVisible(false);

        mnuChangeUSBPin = new TWMenuItem(Language.getString("CHANGE_USB_PIN"), "usbpassword.gif", true);
        mnuChangeUSBPin.setAcceleratorID(TWActions.INT_CHANGE_USB_PIN);
        mnuChangeUSBPin.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().changeUSBPW(false);
            }
        });
        mnuChangeUSBPin.setEnabled(false);
        mnuChangeUSBPin.setVisible(false);

        mnuIntradyPositionMoniter = new TWMenuItem(Language.getString("TRADE_INTRADAY_POSITION_MONITER"), "tradeintrady.gif", true);
        //  mnuIntradyPositionMoniter.setAcceleratorID(TWActions.INT_CHANGE_USB_PIN);
        mnuIntradyPositionMoniter.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //IntraDayPositionMoniterUI.getSharedInstance().setVisible(true);
            }
        });

        /*
            Added by Chandika for KSA v9 mtr solution
         */
        mnuCalcSelection = new TWMenu(Language.getString("AFTER_MARKET_VALUATION"), "aftermarketvaluationmethod.gif");
        createAfterMarketValuationMenu();
        mnuCalcSelection.addMenuListener(this);
        mnuCalcSelection.setVisible(false);


        mnuShowTradeAlertPopup.setAcceleratorID(TWActions.INT_SHOW_TRADE_ALERT_POPUP);
        mnuShowTradeAlertPopup.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradingShared.setShowOrderNotoficationPopup(mnuShowTradeAlertPopup.isSelected());
            }
        });

        //ading trade time out option
        mnuTimeOutSelection = new TWMenu(Language.getString("TRADE_TIMEOUT"), "timeOutselection.gif");
        createTimeoutMenu();

        mnuTrade.addMenuListener(new MenuListener() {
            public void menuSelected(MenuEvent e) {
                mnuShowTradeAlertPopup.setSelected(TradingShared.isShowOrderNotoficationPopup());
            }

            public void menuDeselected(MenuEvent e) {
            }

            public void menuCanceled(MenuEvent e) {
            }
        });

        mnuTrade.add(mnuBuy);
        mnuTrade.add(mnuSell);
        mnuTrade.add(mnuRapidOrder);
        if (!TWControl.isMiniTradeDisabled())
            mnuTrade.add(mnuMiniTrade);
        mnuTrade.add(mnuTradesPending);
        mnuTrade.add(mnuOpenPositions);
        if(!TWControl.hideBasketOrders()) {
            mnuTrade.add(mnuOrderQ);
        }

        mnuTrade.add(mnuTradePortfolio);
        mnuTrade.add(mnuSearchOrders);
        mnuTrade.add(mnuAccount);
        if(TWControl.showExecutedTradeReport()) {
            mnuTrade.add(mnuTodayTrades);
        }
        if(TWControl.showAditionalPortfolio()) {
            mnuTrade.add(mnuNewPFCreation);
        }

        if(!TWControl.hideIntradyPositionMoniter())
            mnuTrade.add(mnuIntradyPositionMoniter);
        mnuTrade.add(mnuDepositNOtification);
        mnuTrade.add(mnuWithdrawalReq);
        mnuTrade.add(mnuFundTransWin);
        if(!TWControl.isCashTransferMenuItemDisabled()){
        mnuTrade.add(mnuCashTransReq);
        mnuTrade.add(mnuCash);
        }
        if(!TWControl.hideIntradyPositionMoniter())
        mnuTrade.add(mnuCalcSelection);
        mnuTrade.add(tradeServices);

//        if (BrokerConfig.isMarginableSymbolsAvailable()) {
        mnuTrade.add(mnuMarginableSymbolsUI);
//        }
        mnuTrade.add(duSeperator);
        mnuTrade.add(mnuDUInvestURL);
        mnuTrade.add(new TWSeparator());
        mnuTrade.add(mnuChangeTradePass);
        mnuTrade.add(mnuChangeUSBPin);
        mnuTrade.add(new TWSeparator());
        mnuTrade.add(mnuShowTradeAlertPopup);

    }

    private void createViewMenu() {
        g_mnuTicker = new TWMenuItem(Language.getString("TICKER"), "ticker.gif");
        g_mnuTicker.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TradeTicker)) {    //Settings.isTradeTickerMode()
                    if (ExchangeStore.isValidIinformationType(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), Meta.IT_SymbolTimeAndSales)) {
                        if (!Settings.isShowSummaryTicker()) {
                            String requestID = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol();
                            Client.getInstance().addMarketTradeRequest(requestID, ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());
                        }
                    }
                }
                client.mnu_ticker_ActionPerformed();
            }
        });


        mnuPortfolio = new TWMenuItem(Language.getString("PORTFOLIO_SIMULATOR"), true);
        mnuPortfolio.setAcceleratorID(TWActions.INT_PORTFOLIO_SIMULATOR);
        mnuPortfolio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.createPortfolioView(true);
            }
        });

        mnuAnnouncements = new TWMenuItem(Language.getString("ANNOUNCEMENTS"), true);
        mnuAnnouncements.setAcceleratorID(TWActions.INT_ANNOUNCEMENTS);
        mnuAnnouncements.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.toggleAnnouncementWindow();
            }
        });


        mnuMutualFunds = new TWMenuItem(Language.getString("MUTUAL_FUND"), true);
        mnuMutualFunds.setAcceleratorID(TWActions.INT_MUTUAL_FUNDS);
        mnuMutualFunds.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_MutualFund_ActionPerformed();
            }
        });


        mnuNews = new TWMenuItem(Language.getString("NEWS"), "news.gif", true);
        mnuNews.setAcceleratorID(TWActions.INT_NEWS);
        mnuNews.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_News_ActionPerformed();
//                InternationalConnector.getSharedInstance().sendInitialRequests();
            }
        });

        /*g_mnuBrokerList = new TWMenuItem(Language.getString("BROKERS_TITLE"));
   g_mnuBrokerList.addActionListener(new ActionListener() {
       public void actionPerformed(ActionEvent e) {
           Client.getInstance().mnu_BrokerList_ActionPerformed();
       }
   });
        */

        currency = new TWMenuItem(Language.getString("CURRENCY_RATES"), "currencytable.gif", true);
        currency.setAcceleratorID(TWActions.INT_CURRENCY_RATES);
        currency.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.showCurrencyFrame();
            }
        });


        mnuTimenSales = new TWMenuItem(Language.getString("MARKET_TIME_N_SALES"), true);
        mnuTimenSales.setAcceleratorID(TWActions.INT_TIME_AND_SALES);
        mnuTimenSales.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_MarketTimeNSales_ActionPerformed();
            }
        });


        g_mnuExchangeSummary = new TWMenuItem(Language.getString("MARKET_SUMMARY"), true);
        g_mnuExchangeSummary.setAcceleratorID(TWActions.INT_MARKET_SUMMARY);
        g_mnuExchangeSummary.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_ExchangeSummary_ActionPerformed();
            }
        });

        /*g_mnuMarketSummary = new TWMenuItem(Language.getString("MARKET_SUMMARY"));
        g_mnuMarketSummary.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_MarketSummary_ActionPerformed();
            }
        });
        */

        g_mnuMarketIndices = new TWMenuItem(Language.getString("MARKET_INDICES"), "marketindices.gif", true);
        g_mnuMarketIndices.setAcceleratorID(TWActions.INT_MARKET_INDICES);
        g_mnuMarketIndices.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_MarketIndices_ActionPerformed();
            }
        });

        g_mnuGlobalIndex = new TWMenuItem(Language.getString("GLOBAL_INDEX"), "globalIndex.gif", true);
        g_mnuGlobalIndex.setAcceleratorID(TWActions.INT_GLOBAL_INDEX);
        g_mnuGlobalIndex.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_GlobalIndex_ActionPerformed();
            }
        });


        g_mnuTopStocks = new TWMenuItem(Language.getString("TOP_STOCKS"), true);
        g_mnuTopStocks.setAcceleratorID(TWActions.INT_TOP_STOCKS);
        g_mnuTopStocks.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_topStocks();
            }
        });

        /*g_mnuTopBrokers = new TWMenuItem(Language.getString("TOP_BROKERS"));
                g_mnuTopBrokers.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        client.mnu_topBrokers();
                    }
                });
        */


        mnuEconomicsCalendar = new TWMenuItem(Language.getString("ECON_CALENDAR"), "econcalender.gif", true);
        mnuEconomicsCalendar.setAcceleratorID(TWActions.INT_ECON_CALENDAR);
        mnuEconomicsCalendar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().getBrowserFrame().setText("");
                Client.getInstance().getBrowserFrame().setContentType("text/html");
                Client.getInstance().getBrowserFrame().setTitle(Language.getString("ECON_CALENDAR"));
                Client.getInstance().getBrowserFrame().showLoading(Constants.DOC_TYPE_FINANCIAL_CALENDERS);
                String request = Meta.CALENDER_REQUEST + Meta.DS + Meta.ECONOMIC_CALENDER + Meta.EOL;
                SendQFactory.addContentData(Constants.CONTENT_PATH_SECONDARY, request, null);
                SharedMethods.updateGoogleAnalytics("EconomicCalendar");
            }
        });


        mnuEarningsCalendar = new TWMenuItem(Language.getString("EARNINGS_CALENDAR"), "earningscalender.gif", true);
        mnuEarningsCalendar.setAcceleratorID(TWActions.INT_EARNINGS_CALENDAR);
        mnuEarningsCalendar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().getBrowserFrame().setText("");
                Client.getInstance().getBrowserFrame().setContentType("text/html");
                Client.getInstance().getBrowserFrame().setTitle(Language.getString("EARNINGS_CALENDAR"));
                Client.getInstance().getBrowserFrame().showLoading(Constants.DOC_TYPE_FINANCIAL_CALENDERS);
                String request = Meta.CALENDER_REQUEST + Meta.DS + Meta.EARNINGS_CALENDER + Meta.EOL;
                SendQFactory.addContentData(Constants.CONTENT_PATH_SECONDARY, request, null);
                SharedMethods.updateGoogleAnalytics("EarningsCalendar");
            }
        });


        mnuAlerts = new TWMenuItem(Language.getString("ALERT_MANAGER"), "alertmanager.gif", true);
        mnuAlerts.setAcceleratorID(TWActions.INT_ALERT_MANAGER);
        mnuAlerts.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_AlertManager();
            }
        });
        mnuAlerts.setVisible(false);
        mnuAlerts.setEnabled(false);


        sidebar = new TWMenuItem(Language.getString("SIDE_BAR"), "sidebar.gif", true);
        sidebar.setAcceleratorID(TWActions.INT_SIDE_BAR);
        sidebar.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                SideBar.getSharedInstance().controlSideBar();
            }
        });


        cashFlowWatcher = new TWMenuItem(Language.getString("CASHFLOW_WATCHER"), "cashflowview.gif", true);
        cashFlowWatcher.setAcceleratorID(TWActions.INT_CASH_FLOW_WATCHER);
        cashFlowWatcher.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CashFlowWatcherUI.getSharedInstance().showCashFlow(true);
            }
        });

        cashFlowWatcherHistory = new TWMenuItem(Language.getString("CASHFLOW_ANALYZER"), "cfhistory.gif", true);
        cashFlowWatcherHistory.setAcceleratorID(TWActions.INT_CASH_FLOW_ANALYZER);
        cashFlowWatcherHistory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CashFlowHistory.getSharedInstance().showSummaryWindow(true);
                // CashFlowHistorySummaryUI.getSharedInstance().showWindow(true);
            }
        });

        //global market summary
        globalMarket = new TWMenuItem(Language.getString("GLOBAL_MARKET_SUMMARY"), "globalsummaryview.gif", true);
        globalMarket.setAcceleratorID(TWActions.INT_GLOBAL_MARKET_SUMMARY);
        globalMarket.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.createGlobalMarketSummary(false);

            }
        });

//        mnuView.add(g_mnuTicker);
//        mnuView.add(CommonSettings.getTickerMenu());
        mnuView.add(SharedSettings.getTickerMenu());
//        mnuView.add(mnuPortfolio);
        mnuView.add(mnuAnnouncements);
        mnuView.add(mnuMutualFunds);
        mnuView.add(mnuNews);
//        mnuView.add(mnuPerformaceHive); removed
//        mnuView.add(g_mnuBrokerList);
        mnuView.add(currency);
        mnuView.add(mnuTimenSales);
        mnuView.add(g_mnuExchangeSummary);
//        mnuView.add(g_mnuMarketSummary);
        mnuView.add(g_mnuMarketIndices);
//        mnuView.add(g_mnuGlobalIndex );
        mnuView.add(g_mnuTopStocks);
//        mnuView.add(g_mnuTopBrokers);
//        mnuView.add(g_mnuGraph);    //moved to analysis menu
//        if (TWControl.getBooleanItem("ECON_CALENDAR")){
        mnuView.add(mnuEconomicsCalendar);
//        }
//        if (TWControl.getBooleanItem("EARNINGS_CALENDAR")){
        mnuView.add(mnuEarningsCalendar);
//        }
        if (Settings.isTCPMode()) {
            // mnuView.add(mnuAlerts);
        }
        mnuView.add(sidebar);
//        mnuView.add(volumeWatcher);    //moved to analysis menu
//        mnuView.add(cashFlowWatcher);  //moved to analysis menu
//        mnuView.add(cashFlowWatcherHistory);
        mnuView.add(globalMarket);

    }

    private void createCustomizeMenu() {
        themesMenu = new TWMenu(Language.getString("THEMES"), "themes.gif");
        createThemeMenu();
        themesMenu.addMenuListener(this);
        mnuCustomize.add(themesMenu);

        mnuWorkspaces.setText(Language.getString("MENU_WORKSPACES"));
        mnuCustomize.add(mnuWorkspaces);
        mnuWorkspaces.addMenuListener(this);


        createPreferencesMenu();
        mnuCustomize.add(mnuPreferences);

    }

    private void createPreferencesMenu() {
        mnuPreferences = new TWMenu(Language.getString("PREFERENCES"), "preferences.gif");

        mnuSelectedSymbols = new TWMenuItem(Language.getString("BANDWIDTH_OPTIMIZER"), "bandwidthop.gif", true);
        mnuSelectedSymbols.setAcceleratorID(TWActions.INT_BANDWIDTH_OPTIMIZER);
        mnuSelectedSymbols.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (Settings.isConnected()) {
                    client.mnu_SelectedSymbols_ActionPerformed();
                } else {
                    SharedMethods.showMessage(Language.getString("BANDWIDTH_OP_OFFLINE_MSG"), JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        mnuTimeZones = new TWMenu(Language.getString("TIME_ZONES"), "timezone.gif");

        mnuLangSelection = new TWMenu(Language.getString("CHANGE_LANGUAGE"), "changelanguage.gif");
        createLanguagesMenu();
        mnuLangSelection.addMenuListener(this);

        g_mnuAutoScroll = new TWCheckBoxMenuItem(Language.getString("AUTO_SCROLL"), "autoscroll.gif", true); //autoscroll.gif
        g_mnuAutoScroll.setAcceleratorID(TWActions.INT_AUTO_SCROLLING);
        g_mnuAutoScroll.setSelected(false);
        g_mnuAutoScroll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Settings.setAutoScrolling();
                //g_oClient.showNetCalculator();
            }
        });
        if (Settings.isScrollingOn())
            g_mnuAutoScroll.setState(true);

//        mnuArabicNumbers = new JCheckBoxMenuItem(Language.getString("ARABIC_NUMBERS"), new ImageIcon("images/Theme" + Theme.getID() + "/menu/" + "cellselect.gif")); //arabicnumbers
        mnuArabicNumbers = new TWCheckBoxMenuItem(Language.getString("ARABIC_NUMBERS"), "arabicnumbers.gif", true); //arabicnumbers
        if (Settings.isShowArabicNumbers())
            mnuArabicNumbers.setSelected(true);
        mnuArabicNumbers.setAcceleratorID(TWActions.INT_ARABIC_NUMBERS);
        mnuArabicNumbers.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Thread threadNumChange = new Thread("Menus-changing_number_format_thread") {
                    public void run() {
                        new ShowMessage(UnicodeUtils.getNativeString(Language.getString("MSG_CHANGE_NUMBER_SYSTEM")), "I");
                        client.showArabicNumbers(mnuArabicNumbers.isSelected());
                    }
                };
                threadNumChange.start();
            }
        });

        mnuShowTabs = new TWCheckBoxMenuItem(Language.getString("SHOW_TAB_PANEL"), "showtabs.gif", true);
        mnuShowTabs.setAcceleratorID(TWActions.INT_SHOW_TAB_PANEL);
        mnuShowTabs.setSelected(Settings.isFullScreenMode());
        mnuShowTabs.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Settings.setTabIndex(mnuShowTabs.isSelected());
                client.addTabIndexpanel(mnuShowTabs.isSelected());
                isShowTabIndex = mnuShowTabs.isSelected();
            }
        });
        mnuShowTabs.setSelected(Settings.isTabIndex());


        mnuAlias = new TWCheckBoxMenuItem(Language.getString("SHOW_ALIAS"), "alias.gif", true); //arabicnumbers
        mnuAlias.setAcceleratorID(TWActions.INT_SHOW_ALIAS);
        if (Settings.isShowAlias())
            mnuAlias.setSelected(true);
        mnuAlias.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                new ShowMessage(UnicodeUtils.getNativeString(Language.getString("MSG_CHANGE_NUMBER_SYSTEM")), "I");
                client.showAlias(mnuAlias.isSelected());
            }
        });


        mnuShowCombinedOrderBook = new TWCheckBoxMenuItem(Language.getString("COMBINED_ORDER_BOOK"), "combOrderBook.gif", true);
        mnuShowCombinedOrderBook.setAcceleratorID(TWActions.INT_COMBINED_ORDER_BOOK);
        mnuShowCombinedOrderBook.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Settings.setShowCombinedOrderBook(mnuShowCombinedOrderBook.isSelected());
            }
        });
        mnuShowCombinedOrderBook.setSelected(Settings.isShowCombinedOrderBook());


        mnuPopupAnnouncmWindow = new TWCheckBoxMenuItem(Language.getString("POPUP_ANNOUNCE_WINDOW"), "popupAnnouncements.gif", true);
        mnuPopupAnnouncmWindow.setActionCommand("POPUP_ANNOUNCE_WINDOW");
        mnuPopupAnnouncmWindow.setAcceleratorID(TWActions.INT_POPUP_ANNOUNCEMENTS);
        mnuPopupAnnouncmWindow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Settings.setPopupAnnounceWindow(mnuPopupAnnouncmWindow.isSelected());
                if (chkDoNotAutoPopupAnnouncements != null) {
                    chkDoNotAutoPopupAnnouncements.setSelected(!mnuPopupAnnouncmWindow.isSelected());
                }
            }
        });
        mnuPopupAnnouncmWindow.setSelected(Settings.isPopupAnnounceWindow());


        mnuSaveTradesOnExit = new TWCheckBoxMenuItem(Language.getString("SAVE_TRADES_ON_EXIT"), "saveTnS.gif", true);
        mnuSaveTradesOnExit.setAcceleratorID(TWActions.INT_SAVE_TIME_N_SALES_ON_EXIT);
        mnuSaveTradesOnExit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Settings.setSaveTimeNSales(mnuSaveTradesOnExit.isSelected());
            }
        });
        mnuSaveTradesOnExit.setSelected(Settings.isSaveTimeNSales());

        mnuShowMinMax = new TWCheckBoxMenuItem(Language.getString("SHOW_MIN_MAX"), "showMinMax.gif", true);
//        mnuShowMinMax.setAcceleratorID(TWActions.INT_SHOW_TAB_PANEL);
        isShowMinMax = Settings.getBooleanItem("MINMAX_ENABLE");
        isMinMaxHighlightRows = Settings.getBooleanItem("IS_MIN_MAX_HIGHLIGHT_ROWS");
        mnuShowMinMax.setSelected(Settings.getBooleanItem("MINMAX_ENABLE"));
        mnuShowMinMax.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Settings.saveItem("MINMAX_ENABLE", "" + mnuShowMinMax.isSelected());
                isShowMinMax = mnuShowMinMax.isSelected();
            }
        });

        mnuShow52WkHighLow = new TWCheckBoxMenuItem(Language.getString("SHOW_52WK_HIGHLOW"), "show52WkHighLow.gif", true);
//        mnuShowMinMax.setAcceleratorID(TWActions.INT_SHOW_TAB_PANEL);
        isShow52WkhighLow = Settings.getBooleanItem("52WEEK_HIGHLOW_ENABLE");
        is52WeekHighlightRows = Settings.getBooleanItem("IS_52_WEEK_HIGHLIGHT_ROWS");
        mnuShow52WkHighLow.setSelected(Settings.getBooleanItem("52WEEK_HIGHLOW_ENABLE"));
        mnuShow52WkHighLow.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Settings.saveItem("52WEEK_HIGHLOW_ENABLE", "" + mnuShow52WkHighLow.isSelected());
                isShow52WkhighLow = mnuShow52WkHighLow.isSelected();
            }
        });
        // newly added
        mnuAppSettings = new TWMenuItem(Language.getString("APPLICATION_SETTINGS"), "optionswindow.gif", true);
        mnuAppSettings.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_AppSettingss_ActionPerformed();
            }
        });
        mnuChartSettings = new TWMenuItem(Language.getString("CHART_OPTIONS"), "graph_set_defaults.GIF");
        mnuChartSettings.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ChartOptionsWindow.getSharedInstance(0).setVisible(true);
            }
        });
        mnuPreferences.add(mnuSelectedSymbols);
//        mnuPreferences.add(mnuTimeZones); todo:removed
        mnuPreferences.add(mnuLangSelection);
        mnuPreferences.add(new TWSeparator());
//        mnuPreferences.add(g_mnuAutoScroll); todo:removed
//        mnuPreferences.add(mnuArabicNumbers); todo:removed
//        mnuPreferences.add(mnuShowTabs); todo:removed
//        mnuPreferences.add(mnuAlias);  todo:removed
//        mnuPreferences.add(mnuShowMinMax); todo:removed
//        mnuPreferences.add(mnuShow52WkHighLow); todo:removed
//        if (TWControl.isCombinedorderBookEnable()) {
//            mnuPreferences.add(mnuShowCombinedOrderBook);  todo:removed
//        }
//        if (TWControl.isAutoPopupAnnouncementMenuVisible()) {
//            mnuPreferences.add(mnuPopupAnnouncmWindow);  todo:removed
//        }
//        if (TWControl.isSaveTimeandSalesonExitEnable()) {
//            mnuPreferences.add(mnuSaveTradesOnExit);  todo:removed
//        }
//        mnuPreferences.add(new TWSeparator());
        mnuPreferences.add(mnuAppSettings);
        mnuPreferences.add(mnuChartSettings);
    }

    private void createToolsMenu() {
        mnuTroubleshoot = new TWMenuItem(Language.getString("TROUBLESHOOT"), "troubleshoot.gif", true);
        mnuTroubleshoot.setAcceleratorID(TWActions.INT_TROUBLESHOOT);
        mnuTroubleshoot.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                TroubleshootUI.getSharedInstance().setVisible(true);
            }
        });

        mnuDownloadTodaysTrades = new TWMenuItem(Language.getString("DETAILED_TRADES"), true);
        mnuDownloadTodaysTrades.setAcceleratorID(TWActions.INT_DETAILED_TRADES);
        mnuDownloadTodaysTrades.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                DetailedTradeStore.getSharedInstance();
                client.mnu_FullTimeNSales_ActionPerformed();
            }
        });

        what_ifCalc = new TWMenuItem(Language.getString("WHAT_IF_CALC"), "whatifcalc.gif", true);
        what_ifCalc.setAcceleratorID(TWActions.INT_WHAT_IF_CALC);
        what_ifCalc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.openWhatifCalculatorframe();
                ((TWDesktop) Client.getInstance().getDesktop()).refreshDesktop();

            }
        });

        mnuOutlook = new TWMenuItem(Language.getString("OUTLOOK_MENU"), true);
        mnuOutlook.setAcceleratorID(TWActions.INT_OUTLOOK_REMINDER);
        mnuOutlook.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (Reminder.checkAvailability()) {
                    ReminderUI rUI = new ReminderUI(Client.getInstance().getFrame());
                    rUI.showDiaog();
                } else {
                    new ShowMessage(Language.getString("MSG_NO_OUTLOOK_SUPPORT"), "E");
                }
            }
        });
        mnuSmartAlert = new TWMenu(Language.getString("ALERTS"), "smartalert.gif");
        mnuSmartAlerts = new TWMenuItem(Language.getString("SET_ALERT"), "smartalert.gif");
        mnuSmartAlerts.setAcceleratorID(TWActions.INT_SMART_ALERT);
        mnuSmartAlerts.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if ((!Settings.isConnected())) {
                    new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                    return;
                }
                if (!SmartAlertWindow.getSharedInstance().isVisible()) {
                    SmartAlertWindow.getSharedInstance().clearWindow();
                    SmartAlertWindow.getSharedInstance().setVisible(true);
                    //
                } else {
                    SmartAlertWindow.getSharedInstance().clearWindow();
                    SmartAlertWindow.getSharedInstance().setVisible(false);
                }
            }
        });
        mnuSmartAlert.add(mnuSmartAlerts);

        mnuSmartAlertSummary = new TWMenuItem(Language.getString("SMART_ALERT_SUMMARY"), "smartalertsummary.gif", true);
        mnuSmartAlertSummary.setAcceleratorID(TWActions.INT_SMART_ALERT_SUMMARY);
        mnuSmartAlertSummary.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!SmartAlertSummeryWindow.getSharedInstance().isVisible()) {
                    SmartAlertSummeryWindow.getSharedInstance().setVisible(true);
                } else {
                    SmartAlertSummeryWindow.getSharedInstance().setVisible(false);
                }
            }
        });
        mnuSmartAlert.add(mnuSmartAlertSummary);


        g_mnuCancelDowload = new TWMenuItem(Language.getString("CANCEL_DOWNLOAD_HISTORY"), true);
        g_mnuCancelDowload.setAcceleratorID(TWActions.INT_CANCEL_DOWNLOAD_HISTORY);
        g_mnuCancelDowload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                cancelHistoryDownload();
            }
        });
        g_mnuCancelDowload.setVisible(false);
        g_mnuCancelDowload.setEnabled(false);

        mnuOpenMetaStoc = new TWMenuItem(Language.getString("METASTOCK"), "metastock.gif", true);
        mnuOpenMetaStoc.setAcceleratorID(TWActions.INT_METASTOCK);
        mnuOpenMetaStoc.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                startMetaStock();
            }
        });
        mnuOpenMetaStoc.setVisible(false);

        mnuOpenTradeStation = new TWMenuItem(Language.getString("TRADESTATION"), "tradeStation.gif", true);
        mnuOpenTradeStation.setAcceleratorID(TWActions.INT_TRADESTATION);
        mnuOpenTradeStation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                startTradeStation();
            }
        });
        mnuOpenTradeStation.setVisible(false);
        mnuOpenTradeStation.setEnabled(false);

        autoUpdateMetaStock = new TWMenuItem(Language.getString("METASTOCK_AUTOUPDATE"), "autometastock.gif", true);
        autoUpdateMetaStock.setAcceleratorID(TWActions.INT_AUTOUPDATE_METASTOCK);
        autoUpdateMetaStock.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.openMetaStockAutoUpdateFrame();

            }
        });

        autoUpdateTradeStation = new TWMenuItem(Language.getString("TRADESTATION_AUTOUPDATE"), "autoTradeStation.gif", true);
        autoUpdateTradeStation.setAcceleratorID(TWActions.INT_AUTOUPDATE_TRADESTATION);
        autoUpdateTradeStation.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.openTradeStationAutoUpdateFrame();

            }
        });


        mnuExport = new TWMenuItem(Language.getString("EXPORT_HISTORY"), true);
        mnuExport.setAcceleratorID(TWActions.INT_EXPORT_HISTORY);
        mnuExport.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu__Export_ActionPerformed();
            }
        });


        g_mnuOptionSymbolBuilder = new TWMenuItem(Language.getString("MENU_OPTION_BUILDER"), "optsymbolbuilder.gif", true);
        g_mnuOptionSymbolBuilder.setAcceleratorID(TWActions.INT_MENU_OPTION_BUILDER);
        g_mnuOptionSymbolBuilder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_FindOptionSymbol(true);
            }
        });

        mnuAnnounceSearch = new TWMenuItem(Language.getString("ANNOUNCEMENT_SEARCH"), true);
        mnuAnnounceSearch.setAcceleratorID(TWActions.INT_ANNOUNCEMENT_SEARCH);
        mnuAnnounceSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.showAnnouncementSearchPanel();
            }
        });
        mnuAnnounceSearch.setVisible(false);
        mnuAnnounceSearch.setEnabled(false);


        mnuRadarScreenProperties = new TWMenuItem(Language.getString("RADAR_SCREEN_DIALOG_TITLE"), "tabularprop.gif", true);
        mnuRadarScreenProperties.setAcceleratorID(TWActions.INT_TAB_INDICATOR_PROPS);
        mnuRadarScreenProperties.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                RadarScreenOptionsDialog radarScreenOptionsDialog = new RadarScreenOptionsDialog(
                        Client.getInstance().getFrame());
                radarScreenOptionsDialog.setLocationRelativeTo(Client.getInstance().getFrame());
                radarScreenOptionsDialog.setVisible(true);
            }
        });

        mnuCustomFormular = new TWMenuItem(Language.getString("FORMULAR_CREATER"), true);
        mnuCustomFormular.setAcceleratorID(TWActions.INT_FORMULAR_CREATER);
        mnuCustomFormular.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.createNewFormula();
            }
        });
        mnuCustomFormular.setEnabled(true);

        mnuCustomIndex = new TWMenuItem(Language.getString("CUSTOM_INDEX"), "customindex.gif", true);
        mnuCustomIndex.setAcceleratorID(TWActions.INT_CUSTOM_INDEX);
        mnuCustomIndex.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CustomIndexWindow.getSharedInstance().showCustomIndexWindow(true);
            }
        });


        mnuRadarScreen = new TWMenuItem(Language.getString("RADAR_WATCHLIST"), "radarscreen.gif");
        mnuRadarScreen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //RadarSettingsWindow.getSharedInstance().setVisible(true);
                RadarScreenWindow.getSharedInstance().showWindow();
            }
        });

        mnuCASearch = new TWMenuItem(Language.getString("MENU_CA_SEARCH"), true);
        mnuCASearch.setAcceleratorID(TWActions.INT_CA_SEARCH);
        mnuCASearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //g_oClient.showNewsSearchPanel();
//                SearchedCAStore store = new SearchedCAStore("MAIN");
//                store.sendRequest();
//                JOptionPane.showMessageDialog(Client.getInstance().getFrame(),
//                    "casearch", "sathyajith", JOptionPane.OK_OPTION);
                Client.getInstance().showCASearchPanel();


            }
        });

        mnuMultiDialog = new TWMenuItem(Language.getString("MULTI_DIALOG_MENU"));
        mnuMultiDialog.setVisible(false);
        mnuMultiDialog.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                new MultiDialogWindow();
//                System.out.println("in main menu");
                // MultiDialogWindow.getSharedInstance().setVisible(true);
            }
        });

        mnuCommission = new TWMenuItem(Language.getString("EDIT_COMMISSION"), true);
        mnuCommission.setAcceleratorID(TWActions.INT_EDIT_COMISSION);
        mnuCommission.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new CommissionConfig();
            }
        });
        mnuCommission.setVisible(false);
        mnuCommission.setEnabled(false);

        mnuSectorMap = new TWMenuItem(Language.getString("SECTORMAP"), true);
        mnuSectorMap.setAcceleratorID(TWActions.INT_SECTOR_MAP);
        mnuSectorMap.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.toggleSectorMapWindow();

            }
        });


        mnuChangeLayer = new TWCheckBoxMenuItem(Language.getString("PUT_ALL_TO_SAME_LAYER"), "changelayer.gif", true);
        mnuChangeLayer.setAcceleratorID(TWActions.INT_PUT_ALL_TO_SAME_LAYER);
        mnuChangeLayer.setSelected(Settings.isPutAllToSameLayer());
        mnuChangeLayer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Settings.setPutAllToSameLayer(!Settings.isPutAllToSameLayer());
                client.putAllToSameLayerActionPerformed();
            }
        });


        mnuKeyFunctions = new TWMenuItem(Language.getString("KEY_FUNCTIONS"));
        mnuKeyFunctions.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                client.mnu_KeyFunction_ActionPerformed();
            }
        });

        mnuCurrencies = new TWMenu(Language.getString("CURRENCIES"), null);


        mnuFullScreen = new TWCheckBoxMenuItem(Language.getString("FULL_SCREEN_MODE"), "fullscreen.gif", true);
        mnuFullScreen.setAcceleratorID(TWActions.INT_FULL_SCREEN_MODE);
        mnuFullScreen.setSelected(Settings.isFullScreenMode());
        mnuFullScreen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.toggleScreen(mnuFullScreen.isSelected());
            }
        });


        mnuTimeLagIndicator = new TWMenuItem(Language.getString("TIME_LAG_INDICATOR_WINDOW"), "latencywindow.gif", true);
        mnuTimeLagIndicator.setAcceleratorID(TWActions.INT_TIME_LAG_INDICATOR);
        mnuTimeLagIndicator.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                LatancyIndicator.getSharedInstance().setVisible(true);
            }
        });
        mnuTimeLagIndicator.setVisible(false);
        mnuTimeLagIndicator.setEnabled(false);

//        mnuTools.add(mnuTroubleshoot);
        /*if (Settings.isTCPMode()) {
            mnuTools.add(mnuDownloadTodaysTrades);
        }*/
        //mnuTools.add(mnuSetFrameLayer);

        mnuTools.add(what_ifCalc);
        mnuTools.add(mnuOutlook);
//        mnuTools.add(mnuDownloadTrades);  //moved to download manager menu
//        mnuTools.add(mnuOhlcDownloadTrades);    //moved to download manager menu
        if (Settings.isTCPMode()) {
//            mnuTools.add(g_mnuDowload);      //moved to download manager menu
        }

        createDownloadManagerMenu();

        mnuTools.add(mnuPortfolio);
        mnuTools.add(mnuSmartAlert);
        mnuTools.add(mnuDownloadManager);

        mnuTools.add(mnuExport);
        mnuTools.add(mnuCustomFormular);
        mnuTools.add(mnuCustomIndex);

        createMetaStockMenu();
        createTradeStationMenu();
        mnuTools.add(mnuMetaStock);
        mnuTools.add(mnuTradeStation);

        mnuTools.add(mnuRadarScreenProperties);


        mnuTools.add(g_mnuCancelDowload);
//        mnuTools.add(mnuOpenMetaStoc);
//        mnuTools.add(autoUpdateMetaStock);
//        mnuTools.add(mnuUpdateMetaStockHistoryDB);
//        mnuTools.add(mnuOpenMetastockFolder);
        mnuTools.add(mnuExport);
//        mnuTools.add(g_mnuHistoryAnalyzer);
//        mnuTools.add(g_mnuOptionSymbolBuilder);
        /*if (Settings.isTCPMode()) {
            mnuTools.add(mnuAnnounceSearch);
            //mnuTools.add(mnuNewsSearchNew);
        }*/
//        mnuTools.add(mnuNewsSearch);
//        mnuTools.add(mnuMarketMap);  // moved to analysis menu
        //todo - remove for temparary purpose 25/10/2006
//        mnuTools.add(mnuScanner);    //moved to analysis menu
        //sath
        //mnuTools.add(mnuCASearch);
        //commented only for anagram INdia
        //by chandika
        //   mnuTools.add(mnuMultiDialog);
//        mnuTools.add(mnuRadarScreen);
//        mnuTools.add(mnuCommission);
//        mnuTools.add(mnuSectorMap);
//        mnuTools.add(mnuSelectedSymbols); //Moved to preferences menu
//        mnuTools.add(mnuTimeZones);  //Moved to preferences menu
//        mnuTools.add(mnuKeyFunctions);
//        mnuTools.add(mnuCurrencies);
//        mnuTools.add(new TWSeparator());
//        mnuTools.add(g_mnuAutoScroll); //Moved to preferences menu
        if (TWControl.isArabicNumbersEnable()) {
//            mnuTools.add(mnuArabicNumbers);  //Moved to preferences menu
        }
        if (TWControl.isFullScreenEnable()) {
            mnuTools.add(mnuFullScreen);
        }
//        mnuTools.add(mnuShowTabs); //Moved to preferences menu
//        mnuTools.add(mnuAlias);    //Moved to preferences menu
        if (TWControl.isCombinedorderBookEnable()) {
//            mnuTools.add(mnuShowCombinedOrderBook);    //Moved to preferences menu
        }
        if (TWControl.isAutoPopupAnnouncementMenuVisible()) {
//            mnuTools.add(mnuPopupAnnouncmWindow);   //Moved to preferences menu
        }

        if (TWControl.isSaveTimeandSalesonExitEnable()) {
//            mnuTools.add(mnuSaveTradesOnExit);  //Moved to preferences menu
        }
//        mnuTools.add(mnuChangeLayer); //removed
        if (Settings.isTCPMode()) {
            mnuTools.add(mnuTimeLagIndicator);
        }
//        mnuTools.add(getLanguagesMenu());
//        mnuTools.add(mnuLangSelection); //moved to preferences menu
    }

    private void createDownloadManagerMenu() {
        mnuDownloadManager = new TWMenu(Language.getString("DOWNLOAD_MANAGER"), "downloadmanager.gif");

        mnuDownloadTrades = new TWMenuItem(Language.getString("TIME_N_SALES_HISTORY"), true);
        mnuDownloadTrades.setAcceleratorID(TWActions.INT_HISTORICAL_TIME_N_SALES);
        mnuDownloadTrades.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeDownloaderUI.getSharedInstance().show();
            }
        });

        mnuOhlcDownloadTrades = new TWMenuItem(Language.getString("OHLC_DOWNLOADER"), true);
        mnuOhlcDownloadTrades.setAcceleratorID(TWActions.INT_OHLC_DOWNLOADER);
        mnuOhlcDownloadTrades.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                OHLCDownloaderUI.getSharedInstance().show();
            }
        });

        g_mnuDowload = new TWMenuItem(Language.getString("DOWNLOAD_HISTORY"), true);
        g_mnuDowload.setAcceleratorID(TWActions.INT_DOWNLOAD_HISTORY);
        g_mnuDowload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_download_ActionPerformed(true, g_mnuDowload);
            }
        });
//        g_mnuDowload.setVisible(false);
//        g_mnuDowload.setEnabled(false);

        mnuDownloadManager.add(mnuDownloadTrades);
        mnuDownloadManager.add(mnuOhlcDownloadTrades);
        if (Settings.isTCPMode()) {
            mnuDownloadManager.add(g_mnuDowload);
        }

    }

    private void createTradeStationMenu() {
        mnuTradeStation = new TWMenu(Language.getString("MNU_TRADESTATION"), "autoTradeStation.gif");
        mnuTradeStation.setVisible(false);

        mnuUpdateTradeStationHistoryDB = new TWMenuItem(Language.getString("UPDATE_TRADESTATION_HISTORY_DB"), "tradeStationdb.gif", true);
        mnuUpdateTradeStationHistoryDB.setAcceleratorID(TWActions.INT_UPDATE_TRADESTATION_DB);
        mnuUpdateTradeStationHistoryDB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.updateTradeStationDB();
            }
        });
        mnuUpdateTradeStationHistoryDB.setVisible(false);
        mnuUpdateTradeStationHistoryDB.setEnabled(false);

        mnuOpenTradeStationFolder = new TWMenuItem(Language.getString("OPEN_TRADESTATION_FOLDER"), "tradeStationFolder.gif", true);
        mnuOpenTradeStationFolder.setAcceleratorID(TWActions.INT_OPEN_TRADESTATION);
        mnuOpenTradeStationFolder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new Thread("Open TradeStation Folder") {
                    public void run() {
                        try {
                            Desktop.getDesktop().open(new File(Settings.TRADE_SATION_DB_PATH));
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }.start();
            }
        });
        mnuOpenTradeStationFolder.setVisible(false);
        mnuOpenTradeStationFolder.setEnabled(false);

        mnuTradeStation.add(mnuOpenTradeStation);
        mnuTradeStation.add(mnuUpdateTradeStationHistoryDB);
        mnuTradeStation.add(autoUpdateTradeStation);
        mnuTradeStation.add(mnuOpenTradeStationFolder);
    }

    private void createMetaStockMenu() {
        mnuMetaStock = new TWMenu(Language.getString("MNU_METASTOCK"), "autometastock.gif");

        mnuUpdateMetaStockHistoryDB = new TWMenuItem(Language.getString("UPDATE_METASTOCK_HISTORY_DB"), "metastockdb.gif", true);
        mnuUpdateMetaStockHistoryDB.setAcceleratorID(TWActions.INT_UPDATE_METASTOCK_DB);
        mnuUpdateMetaStockHistoryDB.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.updateMetaStockDB();
            }
        });
        mnuUpdateMetaStockHistoryDB.setVisible(false);
        mnuUpdateMetaStockHistoryDB.setEnabled(false);

        mnuOpenMetastockFolder = new TWMenuItem(Language.getString("OPEN_METASTOCK_FOLDER"), "metastockfolder.gif", true);
        mnuOpenMetastockFolder.setAcceleratorID(TWActions.INT_OPEN_METASTOCK);
        mnuOpenMetastockFolder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new Thread("Open Metastock Folder") {
                    public void run() {
                        try {
                            Desktop.getDesktop().open(new File(Settings.META_STOCK_DB_PATH));
                        } catch (IOException e1) {
                            e1.printStackTrace();
                        }
                    }
                }.start();
            }
        });
        mnuOpenMetastockFolder.setVisible(false);
        mnuOpenMetastockFolder.setEnabled(false);

        mnuMetaStock.add(mnuOpenMetaStoc);
        mnuMetaStock.add(mnuUpdateMetaStockHistoryDB);
        mnuMetaStock.add(autoUpdateMetaStock);
        mnuMetaStock.add(mnuOpenMetastockFolder);


    }

    private void createAnalysisMenu() {
        g_mnuGraph = new TWMenuItem(Language.getString("UNICHARTS"), true);
        g_mnuGraph.setAcceleratorID(TWActions.INT_PRO_CHART);
        g_mnuGraph.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                if (ChartInterface.isFirstTimeChartLoading()) {
                    final WorkInProgressIndicator in = new WorkInProgressIndicator(WorkInProgressIndicator.CHART_LOADING_IN_PROGRESS);
                    in.setVisible(true);
                    new Thread() {
                        public void run() {
                            try {
                                client.showProChart();
                            } catch (Exception e) {
                                e.printStackTrace();  //defalt action
                            }

                            in.dispose();
                            ChartInterface.setFirstTimeChartLoading(false);
                        }
                    }.start();
                } else {
                    client.showProChart();
                }
            }
        });

        volumeWatcher = new TWMenuItem(Language.getString("VOLUME_WATCHER"), "volumewatchview.gif", true);
        volumeWatcher.setAcceleratorID(TWActions.INT_VOLUME_WATCHER);
        volumeWatcher.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                VolumeWatcherUI.getSharedInstance().showVolumeWatcher(true);
            }
        });

        //cash flow

        g_mnuHistoryAnalyzer = new TWMenuItem(Language.getString("HISTORY_ANALYZER"), true);
        g_mnuHistoryAnalyzer.setAcceleratorID(TWActions.INT_HISTORY_ANALYZER);
        g_mnuHistoryAnalyzer.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.showNetCalculator();
            }
        });

        mnuMarketMap = new TWMenuItem(Language.getString("POPUP_HEAT_MAP_FOR_VIEW"), true);
        mnuMarketMap.setAcceleratorID(TWActions.INT_PERFORMANCE_MAP);
        mnuMarketMap.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.showFullHeatMap();
            }
        });

        mnuPerformaceHive = new TWMenuItem(Language.getString("PERFORMANCE_HIVE"), "news.gif", true);
        mnuPerformaceHive.setAcceleratorID(TWActions.INT_PERFORMANCE_HIVE);
        mnuPerformaceHive.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_PerformaceHive_ActionPerformed();
            }
        });

        mnuScanner = new TWMenuItem(Language.getString("SCANNER"), "scanner.gif", true);
        mnuScanner.setAcceleratorID(TWActions.INT_SCANNER);
        mnuScanner.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.showScanner();
            }
        });


        mnuAnalysis.add(g_mnuGraph);
        mnuAnalysis.add(volumeWatcher);
        mnuAnalysis.add(cashFlowWatcherHistory);
        mnuAnalysis.add(g_mnuHistoryAnalyzer);
        mnuAnalysis.add(mnuMarketMap);
//        mnuAnalysis.add(mnuPerformaceHive);   //removed
        mnuAnalysis.add(mnuScanner);
        mnuAnalysis.add(mnuRadarScreen);
//        mnuAnalysis.add(mnuNewPFCreation);


    }

    private void createReportsMenu() {


        mnuReport1 = new TWMenuItem(Language.getString("SYMBOL_TRADING_SUMMARY"), true); //Language.getString("MENU_TOOLS_EXPORT"));
        mnuReport1.setAcceleratorID(TWActions.INT_SYMBOL_TRADING_SUMMARY);
        mnuReport1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //if (repGen == null)
//                ReportGenerator repGen = new ReportGenerator(ReportGenerator.REPORT_SYMBOL_TRADING_SUMMARY);
                ReportManager.showSymbolTradingReport();
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Reports, "SymbolTradingSummary");
            }
        });


        mnuReport2 = new TWMenuItem(Language.getString("DAILY_MKT_SUMMARY"), true); //Language.getString("MENU_TOOLS_EXPORT"));
        mnuReport2.setAcceleratorID(TWActions.INT_DAILY_MARKET_SUMMARY);
        mnuReport2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                ReportManager.showDailyMarketReport();
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Reports, "DailyMarketSummary");
            }
        });

     /*   mnuTodayTrades = new TWMenuItem("google", true); //Language.getString("MENU_TOOLS_EXPORT"));
            //   mnuTodayTrades.setAcceleratorID(TWActions.INT_DAILY_MARKET_SUMMARY);
               mnuTodayTrades.addActionListener(new ActionListener() {
                   public void actionPerformed(ActionEvent e) {
                       String[] url = {"www.tycoonegypt.com/Login.aspx"};
                        Client.getInstance().getWebBrowserFrame().setURL("www.tycoonegypt.com/Login.aspx", "google");
                    //   OpenURL(url);
                  //     ReportManager.showDailyMarketReport();
                  //     SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Reports, "DailyMarketSummary");
                   }
               });*/



        mnuReports.add(mnuReport1);
        mnuReports.add(mnuReport2);
     //   mnuReports.add(mnuTodayTrades);

    }


    private void createWindowsMenu() {


        mnuWindows.addMenuListener(this);

        mnuCascadeWindows = new TWMenuItem(Language.getString("CASCADE"), "cascade.gif", true);
        mnuCascadeWindows.setAcceleratorID(TWActions.INT_CASCADE);
        mnuCascadeWindows.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_cascadeWindows();
            }
        });


        mnuTileWindows = new TWMenuItem(Language.getString("TILE"), "tile.gif", true);
        mnuTileWindows.setAcceleratorID(TWActions.INT_TILE);
        mnuTileWindows.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_TileAllWindows();
            }
        });


        mnuTileWindowsHorizontal = new TWMenuItem(Language.getString("TILE_HORIZONTAL"), "tileH.gif", true);
        mnuTileWindowsHorizontal.setAcceleratorID(TWActions.INT_TILE_HORIZONTALLY);
        mnuTileWindowsHorizontal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_TileAllWindowsH();
            }
        });


        mnuTileWindowsVertical = new TWMenuItem(Language.getString("TILE_VERTICAL"), "tileV.gif", true);
        mnuTileWindowsVertical.setAcceleratorID(TWActions.INT_TILE_VERTICALLY);
        mnuTileWindowsVertical.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_TileAllWindowsV();
            }
        });

        mnuWindows.add(mnuCascadeWindows);
        mnuWindows.add(mnuTileWindows);
        mnuWindows.add(mnuTileWindowsHorizontal);
        mnuWindows.add(mnuTileWindowsVertical);

    }

    /*    private void createHelpMenu() {

        mnuHelp.addMenuListener(this);

//        mnuTipOfDay = new TWMenuItem(Language.getString("TIP_OF_THE_DAY_MENU"), true);
        mnuTipOfDay = new TWMenuItem(Language.getString("TIP_OF_THE_DAY_MENU"), "tipdaymenu.gif");
        mnuTipOfDay.setAcceleratorID(TWActions.INT_TIP_OF_THE_DAY);
        mnuTipOfDay.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new TipOfDayWindow().showUI();
            }
        });

        mnuHelpContents = new TWMenuItem(Language.getString("HELP_GENERAL"), true);
        mnuHelpContents.setAcceleratorID(TWActions.INT_SHOW_HELP);
        mnuHelpContents.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //g_oClient.mnu_cascadeWindows();
                NativeMethods.showHelp();
            }
        });


        mnuTradeHelpContents = new TWMenuItem(Language.getString("HELP_TRADE"), true);
        mnuTradeHelpContents.setAcceleratorID(TWActions.INT_TRADING_HELP);
        mnuTradeHelpContents.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //g_oClient.mnu_cascadeWindows();
                NativeMethods.showTradeHelp();
            }
        });


        mnuHelpWhatzNew = new TWMenuItem(Language.getString("WHATZ_NEW"), true);
        mnuHelpWhatzNew.setAcceleratorID(TWActions.INT_WHAT_IS_NEW);
        mnuHelpWhatzNew.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NativeMethods.showWhatzNewHelp();
            }
        });


        mnuHelpQRG = new TWMenuItem(Language.getString("HELP_QRG"), true);
        mnuHelpQRG.setAcceleratorID(TWActions.INT_QUICK_REF_GUIDE);
        mnuHelpQRG.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NativeMethods.showQRG();
            }
        });


        mnuTutorial = new TWMenuItem(Language.getString("TUTORIAL"), "tutorial.gif", true);
        mnuTutorial.setAcceleratorID(TWActions.INT_TUTORIAL);
        mnuTutorial.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NativeMethods.showTutorial();
            }
        });


        TWMenuItem mnuHKGuide = new TWMenuItem(Language.getString("HOT_KEY_GUIDE"), "hotkeyguide.gif", true);
        mnuHKGuide.setAcceleratorID(TWActions.INT_HOTKEY_GUIDE);
        mnuHKGuide.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NativeMethods.showHotKeyGuide();
            }
        });


        TWMenuItem mnuFAQ = new TWMenuItem(Language.getString("HELP_FAQ"), "faq.gif", true);
        mnuFAQ.setAcceleratorID(TWActions.INT_FAQ);
        mnuFAQ.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NativeMethods.showFAQ();
            }
        });


        TWMenuItem mnuSecTips = new TWMenuItem(Language.getString("HELP_SECURITY_TIPS"), "sectips.gif", true);
        mnuSecTips.setAcceleratorID(TWActions.INT_SECURITY_TIPS);
        mnuSecTips.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NativeMethods.showSecTips();
            }
        });


        mnuHelpSettingUp = new TWMenuItem(Language.getString("SETTING_UP_HELP"), "settingup.gif", true);
        mnuHelpSettingUp.setAcceleratorID(TWActions.INT_SETTING_UP_PRO);
        mnuHelpSettingUp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NativeMethods.showSettingUpHelp();
            }
        });


        mnuSubscriptions = new TWMenuItem(Language.getString("ENTITLEMENTS"), "entitlements.gif", true);
        mnuSubscriptions.setAcceleratorID(TWActions.INT_ENTITLEMENTS);
        mnuSubscriptions.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.showEntitlements(true);
            }
        });

        mnuBroadcastMessages = new TWMenuItem(Language.getString("BROADCAST_MESSAGE"), true);
        mnuBroadcastMessages.setAcceleratorID(TWActions.INT_BROADCAST_MESSAGE);
        mnuBroadcastMessages.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.toggleBroadcastMessageWindow();
            }
        });


        mnuChatWithCST = new TWMenuItem(Language.getString("CHAT_WITH_CUSTOMER_SUPPORT"), "chatsupport.gif", true);
        mnuChatWithCST.setAcceleratorID(TWActions.INT_CHAT);
        mnuChatWithCST.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.openChatSupportFrame();
            }
        });

        mnuChatWithCST.setVisible(false);
        mnuChatWithCST.setEnabled(false);

        mnuCustomerSupport = new TWMenuItem(Language.getString("CONTACT_CUSTOMER_SUPPORT"), "customersupport.gif", true);
        mnuCustomerSupport.setAcceleratorID(TWActions.INT_CONTACT_CUSTOMER_SUPPORT);
        mnuCustomerSupport.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.openCustomerSupportFrame();
            }
        });


        mnuHelpAbout = new TWMenuItem(Language.getString("HELP_ABOUT"), true);
        mnuHelpAbout.setAcceleratorID(TWActions.INT_ABOUT_PRO);
        mnuHelpAbout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AboutBox aboutBox = AboutBox.getSharedInstance();
                aboutBox.show();
            }
        });


        if (HelpManager.isKeyAvailable("TIP_OF_THE_DAY")) {
            mnuHelp.add(mnuTipOfDay);
        }

        if (HelpManager.isKeyAvailable("HELP_GENERAL")) {
            mnuHelp.add(mnuHelpContents);
        }

        if (HelpManager.isKeyAvailable("HELP_TRADE")) {
            mnuHelp.add(mnuTradeHelpContents);
        }


        if (HelpManager.isKeyAvailable("HELP_WHATS_NEW")) {
            mnuHelp.add(mnuHelpWhatzNew);
        }

        if (HelpManager.isKeyAvailable("HELP_QRG")) {
            mnuHelp.add(mnuHelpQRG);
        }
        File file = new File("Tutorial/Tutorial.exe");
        if (file.exists())
            mnuHelp.add(mnuTutorial);
        file = null;

        if (HelpManager.isKeyAvailable("HELP_HOTKEY_GUIDE")) {
            mnuHelp.add(mnuHKGuide);
        }

        if (HelpManager.isKeyAvailable("HELP_FAQ")) {
            mnuHelp.add(mnuFAQ);
        }

        if (isHelpFileAvailable("TRSecTip")) {
            mnuHelp.add(mnuSecTips);
        }

        if (HelpManager.isKeyAvailable("HELP_SETTING_UP")) {
            mnuHelp.add(mnuHelpSettingUp);
        }

        mnuHelp.add(mnuSubscriptions);

        mnuHelp.add(mnuBroadcastMessages);

        if (Settings.isTCPMode()) {
            if (TWControl.isCSChatEnable()) {
                mnuHelp.add(mnuChatWithCST);
            }
            mnuHelp.add(mnuCustomerSupport);
        }
        mnuHelp.add(mnuHelpAbout);
    }*/

    private void createHelpMenu() {
        mnuHelp.addMenuListener(this);

        mnuTipOfDay = new TWMenuItem(Language.getString("TIP_OF_THE_DAY_MENU"), "tipdaymenu.gif");
        mnuTipOfDay.setAcceleratorID(TWActions.INT_TIP_OF_THE_DAY);
        mnuTipOfDay.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new TipOfDayWindow().showUI();
            }
        });
        if (HelpManager.isTodAvailable()) {
            mnuHelp.add(mnuTipOfDay);
        }

        for (final HelpItem hItem : HelpManager.getHelpItems()) {
            TWMenuItem helpItem = new TWMenuItem(UnicodeUtils.getNativeString((hItem.getMenuLabel())), hItem.getImagePath(), true);
            if (hItem.getAccrId() != 0) {
                helpItem.setAcceleratorID(hItem.getAccrId());
            }
            helpItem.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    NativeMethods.showHelpItem(hItem.getFileName());
                }
            });
            if (hItem.isDisplayed()) {
                mnuHelp.add(helpItem);
            }
        }
        mnuTutorial = new TWMenuItem(Language.getString("TUTORIAL"), "tutorial.gif", true);
        mnuTutorial.setAcceleratorID(TWActions.INT_TUTORIAL);
        mnuTutorial.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NativeMethods.showTutorial();
            }
        });

        mnuSubscriptions = new TWMenuItem(Language.getString("ENTITLEMENTS"), "entitlements.gif", true);
        mnuSubscriptions.setAcceleratorID(TWActions.INT_ENTITLEMENTS);
        mnuSubscriptions.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.showEntitlements(true);
            }
        });

        mnuBroadcastMessages = new TWMenuItem(Language.getString("BROADCAST_MESSAGE"), true);
        mnuBroadcastMessages.setAcceleratorID(TWActions.INT_BROADCAST_MESSAGE);
        mnuBroadcastMessages.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.toggleBroadcastMessageWindow();
            }
        });


        mnuChatWithCST = new TWMenuItem(Language.getString("CHAT_WITH_CUSTOMER_SUPPORT"), "chatsupport.gif", true);
        mnuChatWithCST.setAcceleratorID(TWActions.INT_CHAT);
        mnuChatWithCST.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.openChatSupportFrame();
            }
        });

        mnuChatWithCST.setVisible(false);
        mnuChatWithCST.setEnabled(false);

        mnuCustomerSupport = new TWMenuItem(Language.getString("CONTACT_CUSTOMER_SUPPORT"), "customersupport.gif", true);
        mnuCustomerSupport.setAcceleratorID(TWActions.INT_CONTACT_CUSTOMER_SUPPORT);
        mnuCustomerSupport.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.openCustomerSupportFrame();
            }
        });

        mnuHelpAbout = new TWMenuItem(Language.getString("HELP_ABOUT"), "helpabout.gif", true);
        mnuHelpAbout.setAcceleratorID(TWActions.INT_ABOUT_PRO);
        mnuHelpAbout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AboutBox aboutBox = AboutBox.getSharedInstance();
                aboutBox.show();
            }
        });
        File file = new File("Tutorial/Tutorial.exe");
        if (file.exists())
            mnuHelp.add(mnuTutorial);
        file = null;
        mnuHelp.add(mnuSubscriptions);

        if(TWControl.isAdministratorMessageEnable()){
            mnuHelp.add(mnuBroadcastMessages);
        }

        if (Settings.isTCPMode()) {
            if (TWControl.isCSChatEnable()) {
                mnuHelp.add(mnuChatWithCST);
            }
            mnuHelp.add(mnuCustomerSupport);
        }
        mnuHelp.add(mnuHelpAbout);
    }

    /**
     * Create the main menu
     */
    public JMenuBar createManinMenu() {
        menuBar.setLayout(new BoxLayout(menuBar, BoxLayout.LINE_AXIS));

        mnuSystem = new JMenu();
        mnuSystem.setText(Language.getString("FILE_MNU"));
        mnuSystem.addMenuListener(this);
        mnuSystem.getPopupMenu().setLightWeightPopupEnabled(false);
        mnuSystem.setOpaque(false);

        mnuMyStocks.setText(Language.getString("MY_STOCKS"));
        TWMenuItem mnuCreate = new TWMenuItem(Language.getString("CREATE"));
        mnuCreate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //mnu_CreateView_ActionPerformed();
            }
        });
        mnuMyStocks.add(mnuCreate);
        TWMenuItem mnuAddRemove = new TWMenuItem(Language.getString("ADD_REMOVE"));
        mnuSystem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //mnu_AddRemove_ActionPerformed();
            }
        });
        mnuMyStocks.add(mnuAddRemove);
        TWMenuItem mnuDelete = new TWMenuItem(Language.getString("DELETE"));
        mnuSystem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //mnu_Delete_ActionPerformed();
            }
        });
        mnuMyStocks.add(mnuDelete);


        mnuTrade = new JMenu(Language.getString("MUBASHER_TRADE_MENU"));
        mnuTrade.setEnabled(false);
        mnuTrade.getPopupMenu().setLightWeightPopupEnabled(false);

        mnuWatchLists = new JMenu();
        mnuWatchLists.setText(Language.getString("WATCH_LIST"));
        mnuWatchLists.getPopupMenu().setLightWeightPopupEnabled(false);
        mnuWatchLists.addMenuListener(this);
        mnuWatchLists.setOpaque(false);

        mnuView = new JMenu();
        mnuView.setText(Language.getString("VIEW"));
        mnuView.addMenuListener(this);
        mnuView.getPopupMenu().setLightWeightPopupEnabled(false);
        mnuView.setOpaque(false);

        mnuCustomize = new JMenu();
        mnuCustomize.setText(Language.getString("CUSTOMIZE"));
        mnuCustomize.addMenuListener(this);
        mnuCustomize.getPopupMenu().setLightWeightPopupEnabled(false);
        mnuCustomize.setOpaque(false);

        mnuTools = new JMenu();
        mnuTools.setText(Language.getString("TOOLS"));
        mnuTools.addMenuListener(this);
        mnuTools.setOpaque(false);
        mnuTools.getPopupMenu().setLightWeightPopupEnabled(false);

        mnuStMarketMap = new TWMenuItem("Stretched Marketmap");
        mnuStMarketMap.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.showStretchedHeatMap();
            }
        });

        mnuAnalysis = new JMenu(Language.getString("MNU_ANALYSIS"));
        mnuAnalysis.addMenuListener(this);
        mnuAnalysis.setOpaque(false);
        mnuAnalysis.getPopupMenu().setLightWeightPopupEnabled(false);

        mnuReports = new JMenu(Language.getString("REPORTS"));
        mnuReports.setOpaque(false);
        mnuReports.getPopupMenu().setLightWeightPopupEnabled(false);

        mnuVAS = new JMenu(Language.getString("VAS"));
        mnuVAS.setVisible(false);
        mnuVAS.getPopupMenu().setLightWeightPopupEnabled(false);

        mnuWindows = new JMenu();
        mnuWindows.setText(Language.getString("WINDOWS"));
        mnuWindows.setOpaque(false);
        mnuWindows.getPopupMenu().setLightWeightPopupEnabled(false);

        mnuHelp = new JMenu();
        mnuHelp.setText(Language.getString("HELP"));
        mnuHelp.setOpaque(false);
        mnuHelp.getPopupMenu().setLightWeightPopupEnabled(false);

        loadWorkspaces();
        createSystemMenu();
        createTradeMenu();
        createViewMenu();
        createCustomizeMenu();
        createToolsMenu();
        createAnalysisMenu();
        createReportsMenu();
        createWindowsMenu();
        createHelpMenu();
        createMWTradingMenu();
        createWebTradingMenu();

        if (Settings.getSubscriptionURL() != null) {
            subscription = new JLabel(Language.getString("SUBSCRIPTION_URL")) {
                public void updateUI() {
                    super.updateUI();
                    try {
                        subscription.setFont(mnuHelp.getFont());
                        subscription.setForeground(mnuHelp.getForeground());
                    } catch (Exception e) {
                    }
                }

                public void paint(Graphics g) {
                    ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                    super.paintComponent(g);
                }
            };
            subscription.addMouseListener(new MouseAdapter() {

                public void mouseClicked(MouseEvent e) {
//                    Client.getInstance().getWebBrowserFrame().setURL(Settings.getSubscriptionURL());
                    Client.getInstance().getWebBrowserFrame().setURL(Settings.getSubscriptionURL(), Language.getString("SUBSCRIPTION_URL"));
                }
            });

            subscription.setCursor(new Cursor(Cursor.HAND_CURSOR));

        }

        subscriptionDoInvest = new JLabel("  " + Language.getString("SUBSCRIPTION_URL_DUINVEST") + "  ") {
            public void updateUI() {
                super.updateUI();
                try {
                    subscriptionDoInvest.setFont(mnuHelp.getFont());
                    subscriptionDoInvest.setForeground(mnuHelp.getForeground());
                } catch (Exception e) {
                }
            }

            public void paint(Graphics g) {
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                super.paintComponent(g);
            }
        };
        subscriptionDoInvest.addMouseListener(new MouseAdapter() {

            public void mouseClicked(MouseEvent e) {
//                    Client.getInstance().getWebBrowserFrame().setURL(Settings.getSubscriptionURL());
                Client.getInstance().getWebBrowserFrame().setURL(TradeMethods.doInvestClicked(), Language.getString("SUBSCRIPTION_URL_DUINVEST"));
            }
        });

        subscriptionDoInvest.setCursor(new Cursor(Cursor.HAND_CURSOR));


        lblShowToolBar = new JLabel();
        lblShowToolBar.setBorder(null);
        lblShowToolBar.setVisible(false);
        lblShowToolBar.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                setShowHideButtons(client.hideShowToolBar());
            }
        });


        lblShowDownloads = new JLabel();
        lblShowDownloads.setBorder(null);
        lblShowDownloads.setVisible(false);
        lblShowDownloads.addMouseListener(new MouseAdapter() {
            public void mouseEntered(MouseEvent e) {
                DownloadProgressView.getSharedInstance().setVisible(true);
            }

            public void mouseExited(MouseEvent e) {
                DownloadProgressView.getSharedInstance().setVisible(false);
            }
        });

        //todo - added by Dilum - 05/11/2007
        String str = Language.getString("TRADEUSER_MESSAGE");
        str = str.replaceAll("[TRADE_USER]", TradingShared.getTradeUserName());
        tradeUserName = new JLabel(str) {
            public void updateUI() {
                super.updateUI();
                try {
                    tradeUserName.setFont(mnuHelp.getFont());
                    tradeUserName.setForeground(mnuHelp.getForeground());
                } catch (Exception e) {
                }
            }

            public void paint(Graphics g) {
                ((Graphics2D) g).setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                super.paintComponent(g);
            }
        };

        tradeUserName.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
        tradeUserName.setVisible(false);
        tradeUserName.setMaximumSize(new Dimension(300, 20));


        DownloadProgressView.getSharedInstance().setReferenceComponent(lblShowDownloads);


        lblStockNetLogo = new JLabel();
        lblStockNetLogo.setOpaque(true);

        lblPriceConnectionStatus = new TWImageLabel();
        lblPriceConnectionStatus.setToolTipText(Language.getString("PRICE_DISCONNECTED"));
        lblTradeConnectionStatus = new TWImageLabel();
        lblTradeConnectionStatus.setToolTipText(Language.getString("TRADE_DISCONNECTED"));
        lblUSConnectionStatus = new TWImageLabel();
        lblUSConnectionStatus.setToolTipText(Language.getString("US_CONNECTION"));

//        client.setPriceConnectionDown();
//        client.setTradeConnectionDown();
//        client.setUSConnectionDown();
        client.setPriceConnectionBulbStatus();
        client.setTradeConnectionBulbStatus();

        checkMenuAuthentication();

        try {
            ArrayList<JComponent> items = PluginMenuLoader.loadMenu();
            if (items != null) {
                for (JComponent item : items) {
                    addVasMenuItem(item);
                    /*if (item instanceof JMenuItem){
                        final String cLass = (String)item.getClientProperty("CLASS");
                        ((JMenuItem)item).addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                try {
                                    if (cLass != null){
                                        Plugin plugin = (Plugin)Class.forName(cLass).newInstance();
                                        plugin.execute();
                                    }
                                } catch (Exception e1) {
                                    e1.printStackTrace();
                                }
                            }
                        });
                    }*/
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        menuBar.add(mnuSystem);
        //menuBar.add(mnuMyStocks);
        if (Settings.isTCPMode()) {
            mnuTrade.setOpaque(false);
            menuBar.add(mnuTrade);
            menuBar.add(mnuMWTrade); //todo:added for middleware trading functionality
            menuBar.add(mnuWebTrade);
        }
        menuBar.add(mnuWatchLists);
        menuBar.add(mnuView);
        menuBar.add(mnuCustomize);
        menuBar.add(mnuTools);
        menuBar.add(mnuAnalysis);
        menuBar.add(mnuReports);
        if (Settings.isTCPMode()) {
            mnuVAS.setOpaque(false);
            menuBar.add(mnuVAS);
        }
        menuBar.add(mnuWindows);
        menuBar.add(mnuHelp);

        menuBar.add(Box.createHorizontalStrut(10));
        if (Settings.getSubscriptionURL() != null) {
            menuBar.add(subscription);
        }
        subscriptionDoInvest.setVisible(false);
        //  menuBar.add(subscriptionDoInvest);
        menuBar.add(lblShowToolBar);
        menuBar.add(lblShowDownloads);
        menuBar.add(Box.createHorizontalStrut(10));
        //menuBar.add(tradeUserName);
        menuBar.add(Box.createHorizontalGlue());

        menuBar.add(Box.createHorizontalGlue());
        menuBar.add(lblStockNetLogo);
        menuBar.add(lblPriceConnectionStatus);

        /*JLabel gap2 = new JLabel();
        gap2.setPreferredSize(new Dimension(5, 5));
        menuBar.add(gap2);
        menuBar.add(lblUSConnectionStatus);*/

        JLabel gap = new JLabel();
        gap.setPreferredSize(new Dimension(5, 5));
        menuBar.add(gap);
        if (Settings.isTCPMode() && !TWControl.isEnableWebLogin()) {
            menuBar.add(lblTradeConnectionStatus);
        }

        return menuBar;
    }

    public JMenuBar getMenuBar() {
        return menuBar;
    }


    public void createTimeZoneMenu() {
        mnuTimeZones.removeAll();
        String activZone = Settings.getItem("CURRENT_TIME_ZONE");
        if (activZone.equalsIgnoreCase("NULL")) {
            activZone = null;
        }

        ButtonGroup buttonGroup = new ButtonGroup();
        TWRadioButtonMenuItem menuItem = new TWRadioButtonMenuItem(Language.getString("DEFAULT"));
        if (activZone == null) {
            menuItem.setSelected(true);
        }
        menuItem.setActionCommand("T," + "NULL");
        buttonGroup.add(menuItem);
        mnuTimeZones.add(menuItem);
        menuItem.addActionListener(this);

        Enumeration zoneIDs = TimeZoneMap.getInstance().getZoneIDs();
        while (zoneIDs.hasMoreElements()) {
            String zoneID = (String) zoneIDs.nextElement();
            menuItem = new TWRadioButtonMenuItem(TimeZoneMap.getInstance().getZoneDescription(zoneID));
            if ((activZone != null) && (activZone.equals(zoneID))) {
                menuItem.setSelected(true);
            }
            menuItem.setActionCommand("T," + zoneID);
            buttonGroup.add(menuItem);
            mnuTimeZones.add(menuItem);
            menuItem.addActionListener(this);
        }
        zoneIDs = null;
        GUISettings.applyOrientation(mnuTimeZones);
    }

    //private void addExchangesToWatchlistFilter(){
    /*watchlistFilterUI.clrarExchanges();
  Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
  while (exchanges.hasMoreElements()) {
      Exchange exchange = (Exchange) exchanges.nextElement();
      watchlistFilterUI.addExchange(exchange.getSymbol(),  exchange.getDescription());
      exchange = null;
  }
  exchanges = null;*/
    //}

    //private void addCurrencyToWatchlistFilter(){
    /*watchlistFilterUI.clearCurrency();
    Enumeration<String> ids = CurrencyStore.getSharedInstance().getCurrencyIDs();
    while (ids.hasMoreElements()) {
        String id =  ids.nextElement();
        watchlistFilterUI.addCurrency(id, CurrencyStore.getSharedInstance().getCurrencyDescription(id));
        id = null;
    }
    watchlistFilterUI.updateCurrencyPanel();*/
    //}

    public void addCurruncies(Exchange exchange, TWMenu menu) {
    }

    /*public void createCurrencyMenu() {
        mnuCurrencies.removeAll();

        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            TWMenu menu = new TWMenu(exchange.getDescription(), null);
            addCurruncies(exchange, menu);
            mnuCurrencies.add(menu);
            menu = null;
            exchange = null;
        }
        exchanges = null;
    }*/


    public void loadMenuItems() {
        mnuWindows.removeAll();
        //mnuWindows.setLayout(new FlowLayout(FlowLayout.RIGHT));
        mnuWindows.add(mnuCascadeWindows);
        mnuWindows.add(mnuTileWindows);
        mnuWindows.add(mnuTileWindowsHorizontal);
        mnuWindows.add(mnuTileWindowsVertical);

        Object[] list = ((TWDesktop) client.getDesktop()).getWindows();
        Object[] recentlyClossedWindows = ((TWDesktop) client.getDesktop()).getRecentlyClossedWindows();
        if (list == null) return;

        int prevType = -1;
        Arrays.sort(list, windowItemComparator);
        TWDesktopInterface frame = null;
        for (int i = 0; i < list.length; i++) {
            frame = (TWDesktopInterface) list[i];
            if (i == 0) {
                mnuWindows.add(new LabeledSeperator(Language.getString("OPEN_WINDOWS")));
            }
            //if (frame.getWindowType() != Meta.WT_SectorView) continue;

            // do not list invisible UNDIFINED_TYPE frames
            //if (frame.getDesktopItemType() == TWDesktopInterface.POPUP_TYPE) {
            if (!((JInternalFrame) frame).isVisible()) continue;

            if (prevType != frame.getDesktopItemType()) {
//                mnuWindows.add(new LabeledSeperator(Language.getString("OPEN_WINDOWS")));
            }

            TWWindowMenuItem item = new TWWindowMenuItem(frame);
            item.setComponentOrientation(mnuWindows.getComponentOrientation());
            item.addActionListener(this);
            mnuWindows.add(item);
            prevType = frame.getDesktopItemType();
            item = null;
        }
        //mnuWindows.add(new JSeparator());
        if (recentlyClossedWindows.length > 0) {
            mnuWindows.add(new LabeledSeperator(Language.getString("RECENTLY_CLOSED")));
        }

//        mnuWindows.add(new JLabel("<html><b>Recently Closed"));
        for (int i = 0; i < recentlyClossedWindows.length; i++) {
            frame = (TWDesktopInterface) recentlyClossedWindows[i];
            TWWindowMenuItem item = new TWWindowMenuItem(frame);
            item.setComponentOrientation(mnuWindows.getComponentOrientation());
            item.addActionListener(this);
            mnuWindows.add(item);
            item = null;
        }
        frame = null;

        GUISettings.applyOrientation(mnuWindows);
    }

    public void loadMenuImages(String sThemeID) {
        //MPro Menu
        mnuNewItems.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/new.gif"));
        mnuNewMyStock.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/mystocks.gif"));
        mnuMistView.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/classicview.gif"));
        mnuForexView.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/forexview.gif"));
        mnuNewFilteredList.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/condiwatchlist.gif"));
        mnuCustomFormular.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/customformula.gif"));
        mnuOutlook.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/outlook.gif"));
        mnuCustomIndex.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/customindex.gif"));
        //sath todo :set icon for casearch
        mnuMultiDialog.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/combinewindow.gif"));
        mnuConnect.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/connect.gif"));
        mnuChangePass.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/changepw.gif"));
        mnuOpen.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/open.gif"));
        mnuSave.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/savewsp.gif"));
        mnuSaveAs.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/saveas.gif"));
        mnuExit.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/exit.gif"));
        mnuConfig.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/config.gif"));
        mnuRadarScreenProperties.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/tabularprop.gif"));
        // View
        mnuAnnouncements.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/announ.gif"));
        mnuBroadcastMessages.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/broadmessage.gif"));
        mnuPortfolio.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/portfoliosim.gif"));
        mnuTimenSales.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/time&sales.gif"));
        g_mnuExchangeSummary.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/marketsummary.gif"));
//        g_mnuMarketSummary.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/marketsummary.gif"));
        g_mnuTopStocks.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/topstock.gif"));
        //g_mnuTopBrokers.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/topstock.gif"));
        g_mnuGraph.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/menugraph.gif"));

        // Customize
        themesMenu.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/theme.gif"));
        mnuWorkspaces.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/workspaces.gif"));

        // tools
        mnuExport.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/exporthistory.gif"));
        mnuDownloadTrades.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/hitoricaltimeandsales.gif"));
        mnuOhlcDownloadTrades.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/hitoricalohlc.gif"));
        mnuDownloadTodaysTrades.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/detailedtimeandsales.gif"));
        g_mnuDowload.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/downloadhistory.gif"));
        g_mnuHistoryAnalyzer.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/historyanalyser.gif"));
        mnuTimeZones.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/timezone.gif"));
        mnuMarketMap.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/mktmap.gif"));
        mnuStMarketMap.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/mktmap.gif"));
        mnuAnnounceSearch.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/announsearch.gif"));
        mnuOpenMetaStoc.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/metastock.gif"));
        mnuKeyFunctions.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/keyconfig.gif"));

//        mnuCascadeWindows.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/cascade.gif"));
//        mnuTileWindows.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/tile.gif"));

        //help
        mnuSubscriptions.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/entitlements.gif"));
//        mnuHelpContents.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/helpcontents.gif"));
//        mnuTradeHelpContents.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/helptrade.gif"));
//        mnuHelpWhatzNew.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/helpwhatsnew.gif"));
        mnuHelpAbout.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/helpabout.gif"));
//        mnuHelpQRG.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/helpquickref.gif"));
//        mnuHelpSettingUp.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/settingup.gif"));
        mnuChatWithCST.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/chatsupport.gif"));
        mnuReport1.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/report.gif"));
        mnuReport2.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/report.gif"));
        mnuReport3.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/report.gif"));
        //if(ToolBar.getInstance().set_to_newView){
        //     g_btnHidepanel.setIcon(new ImageIcon("images/Theme"+ Theme.getID() + "/showpanel.gif"));
        //} else{
        //    g_btnHidepanel.setIcon(new ImageIcon("images/Theme"+ Theme.getID() + "/hidepanel.gif"));
        //}
    }

    public void loadButtonImages(String sThemeID) {
        g_sCurrentThemeID = sThemeID;
        g_btnExit.setIcon(new ImageIcon("images/Theme" + sThemeID + "/exit.gif"));
        g_btnExit.setRollOverIcon(new ImageIcon("images/Theme" + sThemeID + "/exit_R.gif"));
        g_btnExit.updateUI();
        /*if (Settings.isConnected())
            g_btnConnect.setIcon(new ImageIcon("images/Theme" + sThemeID + "/connect.gif"));
        else
            g_btnConnect.setIcon(new ImageIcon("images/Theme" + sThemeID + "/connect.gif"));
        g_btnConnect.updateUI();*/
        if (TradingShared.isConnected() || MWTradingHandler.getSharedInstance().isVisible()) {
            g_btnTradeConnect.setIcon(new ImageIcon("images/Theme" + sThemeID + "/tradedisconnect.gif"));
            g_btnTradeConnect.setRollOverIcon(new ImageIcon("images/Theme" + sThemeID + "/tradedisconnect_R.gif"));
        } else {
            g_btnTradeConnect.setIcon(new ImageIcon("images/Theme" + sThemeID + "/tradeconnect.gif"));
            g_btnTradeConnect.setRollOverIcon(new ImageIcon("images/Theme" + sThemeID + "/tradeconnect_R.gif"));
        }
        g_btnTradeConnect.updateUI();
        g_btnNews.setIcon(new ImageIcon("images/Theme" + sThemeID + "/Announce.gif"));
        g_btnNews.setRollOverIcon(new ImageIcon("images/Theme" + sThemeID + "/Announce_R.gif"));
        g_btnNews.updateUI();
        /*
        removed for cairo
	    g_btnBrowser.setIcon(new ImageIcon("images/Theme"
            + iThemeID +  "/Messaging.gif"));
        g_btnBrowser.updateUI();*/
        g_btnTimeNSales.setIcon(new ImageIcon("images/Theme" + sThemeID + "/Time_sales.gif"));
        g_btnTimeNSales.setRollOverIcon(new ImageIcon("images/Theme" + sThemeID + "/Time_sales_R.gif"));
        g_btnTimeNSales.updateUI();
        g_btnExportHistory.setIcon(new ImageIcon("images/Theme" + sThemeID + "/Export_history.gif"));
        g_btnExportHistory.setRollOverIcon(new ImageIcon("images/Theme" + sThemeID + "/Export_history_R.gif"));
        g_btnExportHistory.updateUI();
        btnDownload.setIcon(new ImageIcon("images/Theme" + sThemeID + "/Download_history.gif"));
        btnDownload.setRollOverIcon(new ImageIcon("images/Theme" + sThemeID + "/Download_history_R.gif"));
        btnDownload.updateUI();
        /*btnXL.setIcon(new ImageIcon("images/Theme"
            + iThemeID +  "/xl.gif"));
        btnXL.updateUI();*/
        /*btnConfigXL.setIcon(new ImageIcon("images/Theme"
            + iThemeID +  "/xlConf.gif"));
        btnConfigXL.updateUI();*/
        lblStockNetLogo.setIcon(new ImageIcon("images/Theme" + sThemeID + "/mubasherLogo_" + Language.getLanguageTag() + ".gif"));
        lblStockNetLogo.updateUI();
        lblHideToolBar.setIcon(new ImageIcon("images/Theme" + sThemeID + "/hide.gif"));
        lblHideToolBar.updateUI();
        lblShowToolBar.setIcon(new ImageIcon("images/Theme" + sThemeID + "/show.gif"));
        lblShowToolBar.updateUI();
        lblShowDownloads.setIcon(new ImageIcon("images/Theme" + sThemeID + "/downloading.gif"));
        lblShowDownloads.updateUI();
//        btnPrint.setIcon(new ImageIcon("images/Theme"
//            + sThemeID +  "/print.gif"));
//        btnPrint.updateUI();
        btnTicker.setIcon(new ImageIcon("images/Theme" + sThemeID + "/ticker.gif"));
        btnTicker.setRollOverIcon(new ImageIcon("images/Theme" + sThemeID + "/ticker_R.gif"));
        btnTicker.updateUI();
        btnLanguage.setIcon(new ImageIcon("images/Theme"
                + sThemeID + "/language.gif"));
//        btnLanguage.setIcon(new ImageIcon(Theme.getTheamedImagePath("Chat")));
        btnLanguage.setRollOverIcon(new ImageIcon("images/Theme" + sThemeID + "/language_R.gif"));
        btnLanguage.updateUI();
        btnUniChart.setIcon(new ImageIcon("images/Theme" + sThemeID + "/unicharts.gif"));
        btnUniChart.setRollOverIcon(new ImageIcon("images/Theme" + sThemeID + "/unicharts_R.gif"));
        btnUniChart.updateUI();
        btnMetaStock.setIcon(new ImageIcon("images/Theme" + sThemeID + "/metastock.gif"));
        btnMetaStock.setRollOverIcon(new ImageIcon("images/Theme" + sThemeID + "/metastock_R.gif"));
        btnMetaStock.updateUI();
        /*if(ToolBar.getInstance().set_to_newView){
            g_btnHidepanel.setIcon(new ImageIcon("images/Theme" + sThemeID + "/hidepanel.gif"));
        } else{
            g_btnHidepanel.setIcon(new ImageIcon("images/Theme" + sThemeID + "/showpanel.gif"));
        }*/
        g_btnHidepanel.updateUI();

        FormularEditor.getSharedInstance().loadImages(sThemeID);
//        CustomIndexWindow.getSharedInstance().loadImages(sThemeID);

    }

    public void loadTheme() {
        Theme.applyFGColor(mnuSystem, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuTrade, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuWebTrade, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuWatchLists, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuView, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuCustomize, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuTools, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuReports, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuVAS, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuWindows, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuHelp, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(mnuAnalysis, "MAIN_MENU_FGCOLOR");
//        Theme.applyFGColor(mnuWebTrade, "MAIN_MENU_FGCOLOR");

        mnuBuy.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        mnuSell.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
        lblStockNetLogo.setBackground(Theme.getColor("MENU_LOGO_BGCOLOR"));
        mnuTimeZones.updateUI();
        if (subscription != null) {
            subscription.setFont(mnuHelp.getFont());
            subscription.setForeground(mnuHelp.getForeground());
        }
        if (subscriptionDoInvest != null) {
            subscriptionDoInvest.setFont(mnuHelp.getFont());
            subscriptionDoInvest.setForeground(mnuHelp.getForeground());
        }
        if (selectionPopup != null) {
            SwingUtilities.updateComponentTreeUI(selectionPopup);
        }
    }

    public void setTradeConnectionEnabled(boolean status) {
        if(TWControl.isEnableWebLogin()){
            mnuWebTrade.setEnabled(status);
        } else{
            mnuTrade.setEnabled(status);
        }
        unlockTradeServerConnection();
        mnuChangeUSBPin.setEnabled(TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_USB);
        mnuChangeUSBPin.setVisible(TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_USB);
        mnuDUInvestURL.setEnabled(TradeMethods.isDuInvestURLAvailable());
        mnuDUInvestURL.setVisible(TradeMethods.isDuInvestURLAvailable());
        duSeperator.setVisible(TradeMethods.isDuInvestURLAvailable());
        if (TradingShared.isConnected() || MWTradingHandler.getSharedInstance().isVisible()) {
            g_btnTradeConnect.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/tradedisconnect.gif"));
            g_btnTradeConnect.setRollOverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/tradedisconnect_R.gif"));
            g_btnTradeConnect.setToolTipText(TradingShared.getMessageString("DISCONECT_FROM_BROKER"));
//            mnuTradeConnect.setText(TradingShared.getMessageString("DISCONECT_FROM_BROKER"));
            mnuTradeConnect.setText(Language.getString("DISCONECT_FROM_BROKER_NEW"));
        } else {
            g_btnTradeConnect.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/tradeconnect.gif"));
            g_btnTradeConnect.setRollOverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/tradeconnect_R.gif"));
            g_btnTradeConnect.setToolTipText(Language.getString("CONNECT_TO_BROKER"));
            mnuTradeConnect.setText(Language.getString("CONNECT_TO_BROKER"));
        }                                                                                                
        if (TWControl.isSingleSingnOn()) {
            if (BrokerConfig.getSharedInstance().canChangeTradingPWinSSO()) {
                mnuChangeTradePass.setVisible(true);
            } else {
                mnuChangeTradePass.setVisible(false);
            }
        } else {
            mnuChangeTradePass.setVisible(true);
        }
        g_btnTradeConnect.updateUI();
    }

    private void switchLanguage(String newID) {


        String[] saLanguageIDs = Language.getLanguageIDs();
        if (saLanguageIDs.length <= 2) {
            for (int i = 0; i < saLanguageIDs.length; i++) {
                if (!Language.getLanguageTag().equals(saLanguageIDs[i])) {
                    String result = getLanguageSwitchResponceType(saLanguageIDs[i]);
                    if ((result.equals("OK")) || (result.equals("SAVE"))) {
                        Settings.saveItem("LANGUAGE", saLanguageIDs[i]);
                        Settings.resetCurrentDay();
                    }

                    if (result.equals("SAVE")) {
                        client.exitApplication(true);
                    }
                    //new ShowMessage(UnicodeUtils.getNativeString(Language.getString("MSG_CHANGE_LANGUAGE")), "I");
                }
            }
        } else {
            String result = getLanguageSwitchResponceType(newID);
            if ((result.equals("OK")) || (result.equals("SAVE"))) {
                Settings.saveItem("LANGUAGE", newID);
                Settings.resetCurrentDay();
            }

            if (result.equals("SAVE")) {
                client.exitApplication(true);
            }
        }
    }

    public void setTradeTimeout(String mode) {
        int timeoutMode = Integer.parseInt(mode);
        Settings.setTradingTimeoutMode(timeoutMode);

    }

    private String getLanguageSwitchResponceType(String newLang) {
        final StringBuffer result = new StringBuffer();
        TWButton save = new TWButton("<HTML><DIV ALIGN\\=CENTER>" + Language.getStringForLanguage(Language.getLanguageTag(), "ALL_LANG_SAVE_N_EXIT") + "<BR>" + Language.getStringForLanguage(newLang, "ALL_LANG_SAVE_N_EXIT") + "</DIV>");
        TWButton ok = new TWButton("<HTML><DIV ALIGN\\=CENTER>" + Language.getStringForLanguage(Language.getLanguageTag(), "ALL_LANG_OK") + "<BR>" + Language.getStringForLanguage(newLang, "ALL_LANG_OK") + "</DIV>");
        TWButton cancel = new TWButton("<HTML><DIV ALIGN\\=CENTER>" + Language.getStringForLanguage(Language.getLanguageTag(), "ALL_LANG_CANCEL") + "<BR>" + Language.getStringForLanguage(newLang, "ALL_LANG_CANCEL") + "</DIV>");

        Object options[] = {ok, save, cancel};

        JOptionPane pane = new JOptionPane();
        pane.setMessage("<HTML><DIV ALIGN\\=CENTER>" + Language.getStringForLanguage(Language.getLanguageTag(), "MSG_CHANGE_LANGUAGE") + "<BR>" + Language.getStringForLanguage(newLang, "MSG_CHANGE_LANGUAGE") + "</DIV>");
        pane.setOptions(options);
        pane.setMessageType(JOptionPane.INFORMATION_MESSAGE);

        final JDialog exitDialog = pane.createDialog(client.getFrame(), Language.getString("INFORMATION"));
        exitDialog.setModal(true);

        ok.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                AnnouncementStore.getSharedInstance().clearAll();
                result.append("OK");
                exitDialog.dispose();
            }
        });

        save.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AnnouncementStore.getSharedInstance().clearAll();
                result.append("SAVE");
                exitDialog.dispose();
            }
        });

        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                result.append("CANCEL");
                exitDialog.dispose();
            }
        });

        exitDialog.setVisible(true);

        return result.toString();
    }

    public void loadWorkspaces() {
        File file = null;
        //int  index = 0;

        try {
            File[] fileList = null;
            file = new File(Settings.getAbsolutepath() + "workspaces\\");
            fileList = file.listFiles();
            mnuWorkspaces.removeAll();

            if (fileList.length > 0) {
                ButtonGroup group = new ButtonGroup();
                for (int i = 0; i < fileList.length; i++) {
                    try {
                        //final int index = i;
                        if (!fileList[i].isDirectory()) {
                            final String fileName = fileList[i].getName();
                            if ((fileName.split("\\.")[1].equals("wsp")) || (fileName.split("\\.")[1].equals("wspx"))) {
                                TWRadioButtonMenuItem mnuWSNameR = new TWRadioButtonMenuItem();
                                mnuWSNameR.setText(fileName.split("\\.")[0]);
                                TWMenuItem mnuWSName = new TWMenuItem(fileName.split("\\.")[0]);
                                mnuWSNameR.addActionListener(new ActionListener() {
                                    //                                mnuWSName.addActionListener(new ActionListener() {
                                    public void actionPerformed(ActionEvent e) {
                                        final WorkInProgressIndicator workIn = new WorkInProgressIndicator(WorkInProgressIndicator.APPLYING_WORKSPACE);
                                        new Thread("Workspace Loading Thread indicator") {
                                            public void run() {
                                                workIn.setVisible(true);
                                            }
                                        }.start();
                                        new Thread("Workspace Loading Thread") {
                                            public void run() {
                                                client.openWorkSpace(Settings.getAbsolutepath() + "workspaces\\" + fileName);
                                                workIn.dispose();
                                            }
                                        }.start();
                                        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Workspace, "Load");
                                    }
                                });
                                mnuWorkspaces.add(mnuWSNameR);
                                group.add(mnuWSNameR);
                                String path1 = new File(Settings.getAbsolutepath() + "workspaces\\" + fileName).getCanonicalPath();
                                String path2 = new File(client.getSelectedWorkspace()).getCanonicalPath();
                                if (path1.equals(path2)) {
                                    mnuWSNameR.setSelected(true);
                                }
                            }

                        }
                    } catch (Exception e) {

                    }
                }
            }
            GUISettings.applyOrientation(mnuWorkspaces);
        } catch (Exception e) {
        }
    }

    /**
     * returns the languages sub menu.
     * Items in the menu depends on the number of
     * languages listed in Langage.properties file
     */
    public void createLanguagesMenu() {
        String[] saLanguages;
        String[] saLanguageIDs;
        mnuLangSelection.removeAll();

        ButtonGroup oGroup = new ButtonGroup();
        saLanguages = Language.getLanguageCaptions();
        saLanguageIDs = Language.getLanguageIDs();

        for (int i = 0; i < saLanguages.length; i++) {
            TWRadioButtonMenuItem item = new TWRadioButtonMenuItem(saLanguages[i]);
            oGroup.add(item);
            mnuLangSelection.add(item);
            item.setActionCommand(saLanguageIDs[i]);
            if (saLanguageIDs[i].equals(Language.getSelectedLanguage())) {
                item.setSelected(true);
            }
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    switchLanguage(e.getActionCommand());
                }
            });
            if (i == 0) {
                item.setSelected(true);
            }
        }
        if (saLanguages.length <= 1) {
            mnuLangSelection.setVisible(false);
        }
        GUISettings.applyOrientation(mnuLangSelection);
    }

    public void createTimeoutMenu() {
        //time out times 1,5,10,30
        mnuTimeOutSelection.removeAll();
        int[] timeOutArray = new int[]{0, 1, 5, 10, 30};
        String[] timeOutArrayIDs = new String[]{Language.getString("TRADE_TIMEOUT_NEVER"),
                Language.getString("TRADE_TIMEOUT_ONE"),
                Language.getString("TRADE_TIMEOUT_FIVE"),
                Language.getString("TRADE_TIMEOUT_TEN"),
                Language.getString("TRADE_TIMEOUT_THIRTY")};
        ButtonGroup oGroup = new ButtonGroup();

        for (int i = 0; i < timeOutArray.length; i++) {
            TWRadioButtonMenuItem item = new TWRadioButtonMenuItem(timeOutArrayIDs[i]);
            oGroup.add(item);
            mnuTimeOutSelection.add(item);
            item.setActionCommand("" + i);
//            if (saLanguageIDs[i].equals(Language.getSelectedLanguage())) {
//                item.setSelected(true);
//            }
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setTradeTimeout(e.getActionCommand());
                }
            });
            if (i == Settings.getTradingTimeoutMode()) {
                item.setSelected(true);
            }
        }
//        if (saLanguages.length <= 1) {
//            mnuLangSelection.setVisible(false);
//        }
        GUISettings.applyOrientation(mnuTimeOutSelection);
    }

    /**
     * Create the tool bar
     */
    public JPanel createToolbar() {
        JPanel oToolBar = new JPanel();
        oToolBar.setOpaque(false);

        oToolBar.setLayout(new FlowLayout(FlowLayout.LEADING, 2, 2));
        g_btnExit = new ToolBarButton();
        g_btnExit.setFocusable(false);
        //g_btnExit.setContentAreaFilled(false);
        g_btnExit.setPreferredSize(new Dimension(30, 30));
        g_btnExit.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        g_btnExit.setToolTipText(Language.getString("EXIT"));
        g_btnExit.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.systemExit_ActionPerformed();
            }
        });
        oToolBar.add(g_btnExit);

        g_btnConnect = new ToolBarButton();
        g_btnConnect.setFocusable(false);
        g_btnConnect.setPreferredSize(new Dimension(30, 30));
        g_btnConnect.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        g_btnConnect.setToolTipText(Language.getString("CONNECT"));
        g_btnConnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.connectActionPerformed(true);
            }
        });

        g_btnTradeConnect = new ToolBarButton() {
            @Override
            public void setEnabled(boolean status) {
                super.setEnabled(status);
            }
        };
        g_btnTradeConnect.setFocusable(false);
        g_btnTradeConnect.setEnabled(false);
        g_btnTradeConnect.setPreferredSize(new Dimension(30, 30));
        g_btnTradeConnect.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        g_btnTradeConnect.setToolTipText(Language.getString("CONNECT_TO_BROKER"));
        g_btnTradeConnect.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_TradingConnect_ActionPerformed(true);
            }
        });
        oToolBar.add(g_btnTradeConnect);

        g_btnNews = new ToolBarButton();
        g_btnNews.setFocusable(false);
        g_btnNews.setPreferredSize(new Dimension(30, 30));
        g_btnNews.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        if (TWControl.isNewOnToolBar()) {
            g_btnNews.setToolTipText(Language.getString("NEWS"));
            g_btnNews.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    client.mnu_News_ActionPerformed();
                }
            });
        } else {
            g_btnNews.setToolTipText(Language.getString("ANNOUNCEMENTS"));
            g_btnNews.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    client.toggleAnnouncementWindow();
                }
            });
        }
        oToolBar.add(g_btnNews);

        g_btnTimeNSales = new ToolBarButton();
        g_btnTimeNSales.setFocusable(false);
        g_btnTimeNSales.setPreferredSize(new Dimension(30, 30));
        g_btnTimeNSales.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        g_btnTimeNSales.setToolTipText(Language.getString("TIME_AND_SALES"));
        g_btnTimeNSales.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_MarketTimeNSales_ActionPerformed();
            }
        });
//        if (TWControl.isMarketTnSToolbarBtnvisible()) {

//            if (ExchangeStore.getSharedInstance().defaultCount() > 1) {
//                if (ExchangeStore.getSharedInstance().checkMarketTimeNSalesAvailability()) {
        oToolBar.add(g_btnTimeNSales);
//                }
//            }
//        }

        g_btnExportHistory = new ToolBarButton();
        g_btnExportHistory.setFocusable(false);
        g_btnExportHistory.setPreferredSize(new Dimension(30, 30));
        g_btnExportHistory.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        g_btnExportHistory.setToolTipText(Language.getString("EXPORT_HISTORY"));
        g_btnExportHistory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu__Export_ActionPerformed();
            }
        });
        g_btnExportHistory.setEnabled(false);

        btnDownload = new ToolBarButton();
        btnDownload.setFocusable(false);
        btnDownload.setEnabled(false);
        btnDownload.setPreferredSize(new Dimension(30, 30));
        btnDownload.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        btnDownload.setToolTipText(Language.getString("DOWNLOAD_HISTORY"));
        btnDownload.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.mnu_download_ActionPerformed(true, g_mnuDowload);
            }
        });

        btnTicker = new ToolBarButton();
        btnTicker.setFocusable(false);
        btnTicker.setEnabled(true);
        btnTicker.setPreferredSize(new Dimension(30, 30));
        btnTicker.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        btnTicker.setToolTipText(Language.getString("TICKER"));
        btnTicker.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TradeTicker)) { //  Settings.isTradeTickerMode()
                    if (ExchangeStore.isValidIinformationType(ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol(), Meta.IT_SymbolTimeAndSales)) {
                        if (!Settings.isShowSummaryTicker()) {
                            String requestID = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol();
                            Client.getInstance().addMarketTradeRequest(requestID, ExchangeStore.getSharedInstance().getSelectedExchange().getSymbol());
                        }
                    }
                }
                // client.mnu_ticker_ActionPerformed();
                client.mnu_ticker_ActionPerformed_by_Button();
            }
        });
        oToolBar.add(btnTicker);

        btnUniChart = new ToolBarButton();
        btnUniChart.setFocusable(false);
        btnUniChart.setEnabled(true);
        btnUniChart.setPreferredSize(new Dimension(30, 30));
        btnUniChart.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        btnUniChart.setToolTipText(Language.getString("UNICHARTS"));
        btnUniChart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                client.showProChart();
            }
        });
        oToolBar.add(btnUniChart);

        btnMetaStock = new ToolBarButton();
        btnMetaStock.setFocusable(false);
        btnMetaStock.setEnabled(true);
        btnMetaStock.setPreferredSize(new Dimension(30, 30));
        btnMetaStock.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
        btnMetaStock.setToolTipText(Language.getString("METASTOCK"));
        btnMetaStock.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                startMetaStock();
            }
        });
//        oToolBar.add(btnMetaStock);
        if (!Settings.isMetaStockInstalled()) {
            btnMetaStock.setEnabled(false);
        }

        btnLanguage = new ToolBarButton();
        btnLanguage.setFocusable(false);
        btnLanguage.setEnabled(true);
        btnLanguage.setPreferredSize(new Dimension(30, 30));
        btnLanguage.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
//        btnLanguage.setToolTipText(Language.getString("MUBASHER_CHAT"));
        btnLanguage.setToolTipText(Language.getString("SWITCH_LANGUAGE"));
        btnLanguage.addMouseListener(this);
//        btnLanguage.addActionListener(new ActionListener() {
//            public void actionPerformed(ActionEvent e) {
//                if (Settings.isConnected()) {
//                    MubasherChat.getSharedInstance().displayMChat();
//                }
//            }
//        });
//        if (TWControl.isPtoPChatEnable()) {
        if (Language.getStartMessages().length != 1) {
            oToolBar.add(btnLanguage);
        }
//        }

        lblHideToolBar = new JLabel();
        lblHideToolBar.setFocusable(false);
        lblHideToolBar.setBorder(null);
        lblHideToolBar.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                setShowHideButtons(client.hideShowToolBar());
            }
        });

        oToolBar.add(lblHideToolBar);

        return oToolBar;
    }

    private void startMetaStock() {
        try {
            Runtime.getRuntime().exec(Settings.META_STOCK_APP_PATH);
            SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.MetaStock, "Open");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void startTradeStation() {
        try {
            Process p = Runtime.getRuntime().exec(Settings.TRADE_STATION_BAT_PATH);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Sets the connected to the server status for the gui
     */
    public void setConnectedStatus() {
        g_btnExportHistory.setEnabled(true);
        btnDownload.setEnabled(true);
        //mnuAnnounceSearch.setVisible(false);
        g_mnuDowload.setVisible(true);
        g_mnuDowload.setEnabled(true);
        g_mnuCancelDowload.setVisible(false);
        g_mnuCancelDowload.setEnabled(false);
        lockServerConnection();
        //mnuAnnounceSearch.setVisible(true);
        //mnuNewsSearch.setVisible(true);
    }

    /**
     * Sets the disconnected from the server status for the gui
     */
    public void setDisConnectedStatus() {
        mnuConnect.setText(Language.getString("CONNECT"));
        g_btnConnect.setIcon(new ImageIcon("images/Theme"
                + g_sCurrentThemeID + "/connect.gif"));
        g_btnConnect.setToolTipText(Language.getString("CONNECT"));
        btnDownload.setEnabled(false);
        g_btnExportHistory.setEnabled(false);
        mnuAnnounceSearch.setVisible(false);
        mnuAnnounceSearch.setEnabled(false);
//        mnuNewsSearch.setVisible(false);
//        mnuNewsSearch.setEnabled(false);
        g_mnuDowload.setVisible(false);
        g_mnuDowload.setEnabled(false);
        g_mnuCancelDowload.setVisible(false);
        g_mnuCancelDowload.setEnabled(false);
//        mnuNewsSearchNew.setVisible(false);
//        mnuNewsSearchNew.setEnabled(false);
        mnuTimeLagIndicator.setVisible(false);
        //Settings.setAllowedWindowTypes("");
        checkMenuAuthentication();
        //mnuAnnounceSearch.setVisible(false);
    }

    /**
     * Returns the news menu
     */
    public TWMenuItem getNewsMenu() {
        return mnuAnnouncements;
    }

    public JCheckBoxMenuItem getArabicNumbersMenu() {
        return mnuArabicNumbers;
    }

    public JCheckBoxMenuItem getAliasMenu() {
        return mnuAlias;
    }

    public void actionPerformed(ActionEvent e) {

        if (e.getSource() instanceof TWRadioButtonMenuItem && !(e.getSource() == mainIndexPanel || e.getSource() == strechedIndexpanel || e.getSource() == symbolPanel || e.getSource() == graphIndex)) { // time zone or currency
            TWRadioButtonMenuItem item = (TWRadioButtonMenuItem) e.getSource();
            String[] comandData = item.getActionCommand().split(",");
            if (comandData[0].equals("T")) { // time zone data
                setTimeZone(comandData[1]);
            } else if (comandData[0].equals("C")) { // currency data
                Client.getInstance().setTableCurrency(comandData[1]);
            }
            comandData = null;
            item = null;
        } else if (e.getSource() == mainIndexPanel) {
            // IndexPanel.getInstance().graphMode = false;
            IndexPanel.getInstance().setGraphMode(false);
            ToolBar.getInstance().addToolbarPanel();
        } else if (e.getSource() == strechedIndexpanel) {
//            StretchedIndexPanel.getInstance().activate();
            StretchedIndexPanel.getInstance().refresh();
            StretchedIndexPanel.getInstance().adjustLabelWiths();
            ToolBar.getInstance().addNewIndexPanel();
        } else if (e.getSource() == symbolPanel) {
            ToolBar.getInstance().addSymbolPanel();
        } else if (e.getSource() == graphIndex) {
            //   IndexPanel.getInstance().graphMode = true;
            IndexPanel.getInstance().setGraphMode(true);
            ToolBar.getInstance().panelSelection = ToolBar.PANEL_MODES.GraphMode; // graphIndexMode =3
            ToolBar.getInstance().addToolbarPanel();
        } else {
            TWWindowMenuItem item = (TWWindowMenuItem) e.getSource();
            if (item.getFrame() instanceof com.isi.csvr.chart.ChartFrame) {
                Client.getInstance().showProChart();
                TWDesktop.removeReopendWindow(item.getFrame());
            } else if (item.getFrame() instanceof com.isi.csvr.PortfolioTable) {
                if (Client.getInstance().isValidSystemWindow(Meta.IT_Portfolio, true)) {
                    try {
                        item.getFrame().setIcon(false);
                    } catch (Exception ex) {
                    }
                    item.getFrame().show();
                    try {
                        item.getFrame().setSelected(true);
                    } catch (Exception ex) {
                    }
                }
            } else {
                try {
                    item.getFrame().setIcon(false);
                } catch (Exception ex) {
                }
                item.getFrame().show();
                TWDesktop.removeReopendWindow(item.getFrame());
                try {
                    item.getFrame().setSelected(true);
                } catch (Exception ex) {
                }
            }
        }
    }

    private void setTimeZone(String zoneId) {
        if (zoneId.equals("NULL")) {
            zoneId = null;
        }
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            exchange.setActiveTimeZone(zoneId);
            exchange = null;
        }
        exchanges = null;

        if (zoneId == null) {
            Settings.saveItem("CURRENT_TIME_ZONE", "NULL");
        } else {
            Settings.saveItem("CURRENT_TIME_ZONE", zoneId);
        }
    }

    public void removeFormMenu(TWMenuItem oMenuItem) {
        mnuWindows.remove(oMenuItem);
        g_oWindowList.remove(oMenuItem);
    }

    public void setShowHideButtons(boolean bVisible) {
        if (bVisible) {
            lblHideToolBar.setVisible(true);
            lblShowToolBar.setVisible(false);
        } else {
            lblHideToolBar.setVisible(false);
            lblShowToolBar.setVisible(true);
        }
    }

    public void enableDownloadButton() {
        btnDownload.setEnabled(true);
    }

    public void setEnableMakeDefaultWS(boolean status) {
        mnuMakeDefault.setEnabled(status);
    }

    public TWImageLabel getPriceConnectiontBulb() {
        return lblPriceConnectionStatus;
    }

    public TWImageLabel getTradeConnectiontBulb() {
        return lblTradeConnectionStatus;
    }

    public TWImageLabel getUSConnectiontBulb() {
        return lblUSConnectionStatus;
    }

    public void lockServerConnection() {
        g_btnConnect.setEnabled(false);
        mnuConnect.setEnabled(false);
        if (Settings.isSingleSignOnMode() && ((TWControl.getSSOType() == TWControl.SSO_TYPES.Web) || (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct))) {
            g_btnConnect.setVisible(false);
            mnuConnect.setVisible(false);
        }
    }

    public void unlockServerConnection() {
        g_btnConnect.setEnabled(true);
        mnuConnect.setEnabled(true);
        if (!(Settings.isSingleSignOnMode() && ((TWControl.getSSOType() == TWControl.SSO_TYPES.Web) || (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)))) {
            g_btnConnect.setVisible(true);
            mnuConnect.setVisible(true);
        }
        mnuConnect.updateUI();
    }

    public void lockTradeServerConnection() {
        g_btnTradeConnect.setEnabled(false);
        g_btnTradeConnect.updateUI();
        mnuTradeConnect.setEnabled(false);
        mnuTradeConnect.updateUI();
    }

    public void unlockTradeServerConnection() {
        g_btnTradeConnect.setEnabled(!(Settings.isSingleSignOnMode() && TWControl.getSSOType() == TWControl.SSO_TYPES.Web));     //  && TWControl.getSSOType() == TWControl.SSO_TYPES.Web
//        g_btnTradeConnect.setEnabled(!(Settings.isSingleSignOnMode() && ((TWControl.getSSOType() == TWControl.SSO_TYPES.Web) || ((TWControl.getSSOType() == TWControl.SSO_TYPES.Direct) && ))));     //  && TWControl.getSSOType() == TWControl.SSO_TYPES.Web
        g_btnTradeConnect.updateUI();

        if (Settings.isSingleSignOnMode() && TWControl.getSSOType() == TWControl.SSO_TYPES.Web) {     // && TWControl.getSSOType() == TWControl.SSO_TYPES.Web
            mnuTradeConnect.setVisible(false);
        } else {
            // mnuTradeConnect.setEnabled(false);
            //todo disbaled for Saudi Production requirement
            mnuTradeConnect.setEnabled(true);
        }
        mnuTradeConnect.updateUI();

        System.out.println("Trade Button repainted");
    }

    public TWMenuItem getDownloadButton() {
        return g_mnuDowload;
    }

    /**
     * Invoked when a menu is selected.
     *
     * @param e a MenuEvent object
     */
    public void menuSelected(MenuEvent e) {
        //loadMenuItems();
        if (e.getSource() == mnuWatchLists) {
            if (mnuWatchLists.getItemCount() == 0) {
                mnuWatchLists.getPopupMenu().setLayout(new TWTreeMenuLayout());
                JScrollPane treeScroll = new JScrollPane(client.getTree());
                treeScroll.setBorder(BorderFactory.createEmptyBorder());
                GUISettings.applyOrientation(treeScroll);
                mnuWatchLists.add(treeScroll);
            }
        } else if (e.getSource() == mnuWindows) {
            loadMenuItems();
        } else if (e.getSource() == mnuTools) {
            if (Settings.isShowAlias())
                mnuAlias.setSelected(true);
            else
                mnuAlias.setSelected(false);
        } else if (e.getSource() == mnuWorkspaces) {
            loadWorkspaces();
        } else if (e.getSource() == mnuLangSelection) {
            createLanguagesMenu();
        } else if (e.getSource() == themesMenu) {
            createThemeMenu();
        }
    }

    public void cancelHistoryDownload() {
//        if (FileDownloader.isMaualHistoryRequest()) {
//            clearCancelHistoryRequest();
//            FileDownloader.getInstance().removeManualHistoryDownloadRequest();
//        }
    }

    public void clearCancelHistoryRequest() {
        g_mnuDowload.setVisible(true);
        g_mnuDowload.setEnabled(true);
        g_mnuCancelDowload.setVisible(false);
        g_mnuCancelDowload.setEnabled(false);
    }

    public void setHistoryDownload() {
//        if (FileDownloader.isMaualHistoryRequest()) {
//            g_mnuDowload.setVisible(false);
//            g_mnuDowload.setEnabled(false);
//            g_mnuCancelDowload.setVisible(true);
//            g_mnuCancelDowload.setEnabled(true);
//        }
    }

    public void checkMenuAuthentication() {
        mnuSave.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_SaveWSP));
        mnuSaveAs.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_SaveWSP));

        mnuChangePass.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_ChangePassword));
        mnuChangePass.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_ChangePassword));
        mnuOpen.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_OpenWSP));
        mnuWorkspaces.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_OpenWSP));
        mnuNewMyStock.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_NewWatchList));
        mnuMistView.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_NewWatchList));
//        mnuForexView.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_NewForexWatchList));
        mnuNewMyPortfolio.setEnabled(client.isValidSystemWindow(Meta.IT_Portfolio, true));
        //mnuNewFilteredList.setVisible(client.isValidSystemWindow(Meta.IT_FunctionBuilder, true));
        if ((ExchangeStore.getSharedInstance().defaultCount() > 0)) {
            mnuNewFilteredList.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_FunctionBuilder));
            mnuNewFilteredList.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_FunctionBuilder));
        } else {
            mnuNewFilteredList.setVisible(false);
        }
        if ((Settings.getBooleanItem("CONFIG_ALLOWED")) || ExchangeStore.isValidSystemFinformationType(Meta.IT_Config)) {
            mnuConfig.setEnabled(true);
        } else {
            mnuConfig.setEnabled(false);
        }
        mnuAnnounceSearch.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_AnnouncementSearch));
        mnuAnnounceSearch.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_AnnouncementSearch));

        mnuTimeLagIndicator.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_LatencyWindow));

        mnuEconomicsCalendar.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_EconomicCalendar));
        mnuEarningsCalendar.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_Earningscalendar));


        g_mnuExchangeSummary.setVisible(ExchangeStore.getSharedInstance().defaultCount() > 0);
        mnuAnnouncements.setVisible(ExchangeStore.getSharedInstance().defaultCount() > 0);
        mnuTimenSales.setVisible(ExchangeStore.getSharedInstance().defaultCount() > 0);
        g_btnTimeNSales.setVisible(ExchangeStore.getSharedInstance().defaultCount() > 0);

//        mnuNewsSearch.setVisible(Settings.isValidWindow(Meta.IT_News));
//        mnuNewsSearch.setEnabled(Settings.isValidWindow(Meta.IT_News));

//        mnuNewsSearch.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_News));
//        mnuNewsSearch.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_News));

        mnuNews.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_News));
        mnuNews.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_News));

//        mnuNewsSearchNew.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_News));
//        mnuNewsSearchNew.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_News));

//        mnuScanner.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_VolumnWatch_View));
//        mnuScanner.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_VolumnWatch_View));

        volumeWatcher.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_VolumnWatch_View));
        volumeWatcher.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_VolumnWatch_View));

        //cashFlowWatcher.setVisible(ExchangeStore.isValidIinformationType((Meta.IT_CashFlow));
        //cashFlowWatcher.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_CashFlow));

        cashFlowWatcherHistory.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_CashFlow_History));
        cashFlowWatcherHistory.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_CashFlow_History));

        globalMarket.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_Global_Market_Summary));
        globalMarket.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_Global_Market_Summary));

        currency.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_Currency_Window));
        currency.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_Currency_Window));


        mnuMutualFunds.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.WT_MutualFund));
        mnuMutualFunds.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.WT_MutualFund));
        mnuAlerts.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_Alerts));
//        mnuAlerts.setEnabled(Settings.isValidWindow(Meta.IT_Alerts));
        mnuDownloadTrades.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_TimeAndSalesHistory));
        mnuOhlcDownloadTrades.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_OHLCHistory));
//        g_mnuDowload.setVisible(Settings.isConnected());
//        mnuDownloadTodaysTrades.setVisible(Settings.isConnected());
        mnuDownloadTodaysTrades.setEnabled(Settings.isConnected());
        mnuMarketMap.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap) && ExchangeStore.isDefaultExchangesAvailable());
        mnuMarketMap.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketMap) && ExchangeStore.isDefaultExchangesAvailable());

        exportSeperator.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_IMPORT_EXPORT_USER_DATA));
        mnuExportData.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_IMPORT_EXPORT_USER_DATA));
        mnuImportData.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_IMPORT_EXPORT_USER_DATA));

        mnuCommission.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_Portfolio) || ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketDepthByPrice)
                || ExchangeStore.isValidSystemFinformationType(Meta.IT_MarketDepthByOrder));

        if (Settings.isMetaStockInstalled()) {
            if (btnMetaStock != null) {
                btnMetaStock.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock));
                btnMetaStock.repaint();
            }
            mnuOpenMetaStoc.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock));
        }
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock)) {
            mnuUpdateMetaStockHistoryDB.setVisible(true);
            mnuOpenMetastockFolder.setVisible(true);
        }

        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TRADE_STATION)) {
            mnuUpdateTradeStationHistoryDB.setVisible(true);
            mnuOpenTradeStationFolder.setVisible(true);
            mnuTradeStation.setVisible(true);
            mnuOpenTradeStation.setVisible(Settings.isTradeStationInstalled());
        }

//        Settings.CHAT_SERVER_IP = "85.208.254.92";
//        Settings.CHAT_SERVER_PORT  = 9011;
        if (!Settings.CHAT_SERVER_IP.equals("") && Settings.CHAT_SERVER_PORT != 0) {
            mnuChatWithCST.setEnabled(ExchangeStore.isValidSystemFinformationType(Meta.IT_Chat_Support));
            mnuChatWithCST.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_Chat_Support));
        }
        mnuScanner.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_SCANNER));
        mnuOutlook.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_OUTLOOK_REMINDER));
        enableExchangeRelatedMenus();
        g_mnuTopStocks.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_Topstock));
        g_mnuMarketIndices.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_Market_indices));
        mnuRadarScreen.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_Strategy_Watchlist));
        mnuCustomerSupport.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_CustomerSupport_email));
        g_mnuExchangeSummary.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_Market_Summary));
        mnuSelectedSymbols.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer));
        mnuSmartAlert.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_Alerts));

        PluginMenuLoader.validateMenu();
    }

    public JMenu getCurrencyPopup() {
        if (currencyMenu == null) {
            currencyMenu = new TWMenu(Language.getString("CURRENCY"), "Currency.gif");
        }
        return currencyMenu;
    }

    public void enableExchangeRelatedMenus() {
        if (ExchangeStore.getSharedInstance().defaultCount() < 1) {
            mnuTimenSales.setVisible(false);
            g_btnTimeNSales.setVisible(false);
            what_ifCalc.setVisible(false);
            g_mnuHistoryAnalyzer.setVisible(false);
            mnuScanner.setVisible(false);
            mnuCustomIndex.setVisible(false);
            mnuTimenSales.setVisible(false);
            mnuDownloadTrades.setVisible(false);
            mnuOhlcDownloadTrades.setVisible(false);
            mnuDownloadTodaysTrades.setVisible(false);
            g_mnuDowload.setVisible(false);
            g_mnuCancelDowload.setVisible(false);
            mnuTimeLagIndicator.setVisible(false);
            cashFlowWatcher.setVisible(false);
            cashFlowWatcherHistory.setVisible(false);
            mnuOpenMetaStoc.setVisible(false);
            mnuUpdateMetaStockHistoryDB.setVisible(false);
            autoUpdateMetaStock.setVisible(false);
            autoUpdateTradeStation.setVisible(false);
            mnuOpenMetastockFolder.setVisible(false);
            mnuExport.setVisible(false);
            mnuRadarScreenProperties.setVisible(false);
            mnuRadarScreen.setVisible(false);
            mnuReports.setVisible(false);
            //    mnuKeyFunctions.setVisible(false);
            if (btnMetaStock != null) {
                btnMetaStock.setEnabled(false);
            }
            mnuTimeLagIndicator.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_LatencyWindow));
            mnuMultiDialog.setVisible(false);
        } else {
            validateCashFlowWatch();
            //  cashFlowWatcher.setVisible(true);
            mnuReports.setVisible(true);
            //  mnuCustomerSupport.setVisible(true);
            mnuTimenSales.setVisible(true);
            g_btnTimeNSales.setVisible(true);
            g_mnuExchangeSummary.setVisible(true);
            mnuDownloadTrades.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_TimeAndSalesHistory));
            mnuOhlcDownloadTrades.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_OHLCHistory));
            mnuDownloadTodaysTrades.setVisible(Settings.isConnected());
            g_mnuDowload.setVisible(Settings.isConnected());
            g_mnuCancelDowload.setVisible(false);
            mnuTimeLagIndicator.setVisible(ExchangeStore.isValidSystemFinformationType(Meta.IT_LatencyWindow));
            mnuExport.setVisible(true);
            //   mnuKeyFunctions.setVisible(true);
            mnuRadarScreenProperties.setVisible(true);
            mnuRadarScreen.setVisible(true);
//            mnuScanner.setVisible(true);
            mnuCustomIndex.setVisible(true);
            what_ifCalc.setVisible(true);
            g_mnuHistoryAnalyzer.setVisible(true);
            cashFlowWatcher.setVisible(true);
            // mnuMultiDialog.setVisible(true);
            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock)) {
                if (Settings.isMetaStockInstalled()) {
                    mnuOpenMetaStoc.setVisible(true);
                }
                mnuUpdateMetaStockHistoryDB.setVisible(true);
                autoUpdateMetaStock.setVisible(true);
                mnuOpenMetastockFolder.setVisible(true);
            } else {
                mnuUpdateMetaStockHistoryDB.setVisible(false);
                autoUpdateMetaStock.setVisible(false);
                mnuOpenMetaStoc.setVisible(false);
                mnuOpenMetastockFolder.setVisible(false);
            }

            //TradeStation
            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TRADE_STATION)) {
                mnuOpenTradeStation.setVisible(Settings.isTradeStationInstalled());
                mnuUpdateTradeStationHistoryDB.setVisible(true);
                mnuOpenTradeStation.setVisible(true);
                autoUpdateTradeStation.setVisible(true);
                mnuOpenTradeStationFolder.setVisible(true);
                mnuTradeStation.setVisible(true);
            } else {
                mnuUpdateTradeStationHistoryDB.setVisible(false);
                autoUpdateTradeStation.setVisible(false);
                mnuOpenTradeStation.setVisible(false);
                mnuOpenTradeStationFolder.setVisible(false);
                mnuTradeStation.setVisible(false);
            }


            /*if (ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock)) {
                mnuUpdateMetaStockHistoryDB.setVisible(true);
            }*/
            if (ExchangeStore.getSharedInstance().checkMarketTimeNSalesAvailability()) {
                mnuTimenSales.setVisible(true);
                g_btnTimeNSales.setVisible(true);
            } else {
                mnuTimenSales.setVisible(false);
                g_btnTimeNSales.setVisible(false);
            }
        }
        if (Settings.isSingleSignOnMode()) {
            mnuChangePass.setEnabled(false);
            mnuChangePass.setVisible(false);
        }
    }

    private void validateCashFlowWatch() {
        Enumeration<Exchange> en = ExchangeStore.getSharedInstance().getExchanges();
        while (en.hasMoreElements()) {
            Exchange ex = en.nextElement();
            if (ex != null && ex.isDefault()) {
                boolean enable = ExchangeStore.isValidIinformationType(ex.getSymbol(), Meta.IT_CashFlow);
                if (enable) {
                    cashFlowWatcher.setVisible(true);
                    break;
                }
            }

        }
    }


    public JMenu createWatchlistFilterPopup() {
        if (watchlistFilterMenu == null) {
            watchlistFilterMenu = new TWMenu(Language.getString("FILTER"), "filter.gif");
        }
        watchlistFilterMenu.setLayout(new FlowLayout());
        watchlistFilterUI = new WatchlistFilterUI();
        watchlistFilterMenu.add(watchlistFilterUI);
        return watchlistFilterMenu;
    }

    public TWMenu getWatchlistFilterMenu() {
        return watchlistFilterMenu;
    }

    public WatchlistFilterUI getWatchlistFilterUI() {
        return watchlistFilterUI;
    }

    public JPanel createHideButton(int arrowType) {
        DownArrow da = null;
        if (arrowType == 0) {
            da = new DownArrow();
        } else if (arrowType == 1) {
            da = new DownArrow(Color.white);
        }
        JPanel hideButton = new JPanel();
        hideButton.setOpaque(false);
        hideButton.setPreferredSize(new Dimension(18, 34));
        // hideButton.setLayout(new FlexFlowLayout(new String[]{"100%","18"}));
        //   hideButton.setLayout(new FlexGridLayout(new String[]{"100%","18"},new String[]{"34"},0,0));
        hideButton.setLayout(new BorderLayout(1, 0));
//        JButton btnShowPanels = new JButton(new DownArrow());
//        hideButton.setLayout(new FlexGridLayout(new String[] {"100%"},new String[] {"100%"},1,1));
//        hideButton.add(btnShowPanels);
        //=================================================
        g_btnHidepanel = new JButton(da); //new ToolBarButton();
        g_btnHidepanel.setFocusable(false);
        g_btnHidepanel.setContentAreaFilled(false);

        //  g_btnHidepanel.setPreferredSize(new Dimension(18, 18));
        //  g_btnHidepanel.setMaximumSize(new Dimension(18, 18));
        g_btnHidepanel.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
//        g_btnHidepanel.setIcon(new ImageIcon("images/Theme"+ Theme.getID() + "/hidepanel.gif"));
        g_btnHidepanel.setToolTipText(Language.getString("PANEL_SELECTION"));
        g_btnHidepanel.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                GUISettings.showPopup(createSelectionPopup(), (Component) e.getSource(), e.getX(), e.getY());
//                if(ToolBar.getInstance().set_to_newView){
////                    g_btnHidepanel.setIcon(new ImageIcon("images/Theme"+ Theme.getID() + "/hidepanel.gif"));
//                    ToolBar.getInstance().addToolbarPanel();
//                } else{
////                    g_btnHidepanel.setIcon(new ImageIcon("images/Theme"+ Theme.getID() + "/showpanel.gif"));
//                    StretchedIndexPanel.getInstance().activate();
//                    StretchedIndexPanel.getInstance().refresh();
//                    StretchedIndexPanel.getInstance().adjustLabelWiths();
//                    ToolBar.getInstance().addNewIndexPanel();
//                }
            }
        });

        g_btnHidepanel.setVisible(true);
        g_btnHidepanel.setOpaque(false);
        JPanel panel = new JPanel();
        panel.setOpaque(false);
        //  panel.setPreferredSize(new Dimension(18,34));
        //   panel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        // g_btnHidepanel.setBorder(BorderFactory.createLineBorder(Color.RED));
        hideButton.add(panel, BorderLayout.CENTER);
        if (Language.isLTR()) {
            hideButton.add(g_btnHidepanel, BorderLayout.EAST);
        } else {
            hideButton.add(g_btnHidepanel, BorderLayout.WEST);
        }
        hideButton.setVisible(true);
        return hideButton;
    }

    public JPopupMenu createSelectionPopup() {
        if (selectionPopup == null) {
            selectionPopup = new JPopupMenu();
            ButtonGroup group = new ButtonGroup();
            mainIndexPanel = new TWRadioButtonMenuItem(Language.getString("MAIN_INDEX_PANEL"));
            mainIndexPanel.addActionListener(this);
            selectionPopup.add(mainIndexPanel);
            group.add(mainIndexPanel);
            strechedIndexpanel = new TWRadioButtonMenuItem(Language.getString("STRETCHED_INDEX_PANEL"));
            strechedIndexpanel.addActionListener(this);
            selectionPopup.add(strechedIndexpanel);
            group.add(strechedIndexpanel);
            symbolPanel = new TWRadioButtonMenuItem(Language.getString("SYMBOL_PANEL"));
            symbolPanel.addActionListener(this);
            selectionPopup.add(symbolPanel);
            group.add(symbolPanel);
            graphIndex = new TWRadioButtonMenuItem(Language.getString("INDEX_GRAPH_PANEL"));
            graphIndex.addActionListener(this);
            selectionPopup.add(graphIndex);
            group.add(graphIndex);

            GUISettings.applyOrientation(selectionPopup);
        }
        validateSelectionPopup();
        return selectionPopup;
    }

    /*private final int mainIndexMode=0;
    private final int graphIndexMode=3;
    private final int stretchedIndexMode=1;
    private final int symbolIndexMode=2;*/

    public void validateSelectionPopup() {
        if (ToolBar.panelSelection == ToolBar.PANEL_MODES.MainIndex) {
            mainIndexPanel.setSelected(true);
            if (!mainIndexPanel.isSelected()) {
//                mainIndexPanel.setSelected(true);
            }
            //  symbolPanel.setVisible(true);
            //  strechedIndexpanel.setVisible(true);
            //  graphIndex.setVisible(true);
        }
        if (ToolBar.panelSelection == ToolBar.PANEL_MODES.StretchedPanel) {
            strechedIndexpanel.setSelected(true);
            if (!strechedIndexpanel.isSelected()) {
//                strechedIndexpanel.setVisible(false);
            }
            // symbolPanel.setVisible(true);
            // mainIndexPanel.setVisible(true);
            //graphIndex.setVisible(true);
        }
        if (ToolBar.panelSelection == ToolBar.PANEL_MODES.SymbolPanel) {
            symbolPanel.setSelected(true);
            if (!symbolPanel.isSelected()) {
//            symbolPanel.setVisible(false);
            }
            //  mainIndexPanel.setVisible(true);
            // strechedIndexpanel.setVisible(true);
            //  graphIndex.setVisible(true);
        }
        if (ToolBar.panelSelection == ToolBar.PANEL_MODES.GraphMode) {
            graphIndex.setSelected(true);
            if (!graphIndex.isSelected()) {
//            graphIndex.setVisible(false);
            }
            // symbolPanel.setVisible(true);
            //  mainIndexPanel.setVisible(true);
            // strechedIndexpanel.setVisible(true);
        }
    }

    private void createThemeMenu() {
        themesMenu.removeAll();
        final String[][] themes = Theme.getInstance().getThemes();

        ButtonGroup group = new ButtonGroup();
        for (int j = 0; j < themes.length; j++) {
            final int iMenu = j;
            TWRadioButtonMenuItem mnuThemeR = new TWRadioButtonMenuItem();
            mnuThemeR.setText(themes[iMenu][2]);
            mnuThemeR.setIcon(new ImageIcon(themes[iMenu][3]));
            TWMenuItem mnuTheme = new TWMenuItem(themes[iMenu][2], new ImageIcon(themes[iMenu][3])); // + (j+1)));
            mnuThemeR.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    final WorkInProgressIndicator workIn = new WorkInProgressIndicator(WorkInProgressIndicator.APPLYING_THEME);
                    new Thread("Theme changing Thread Indicator") {
                        public void run() {
                            workIn.setVisible(true);

                        }
                    }.start();
                    new Thread("Theme changing Thread") {
                        public void run() {
                            client.themeChanged(themes[iMenu][0]);
                            workIn.dispose();
                            SharedMethods.updateGoogleAnalytics("ChangeTheme");
                        }
                    }.start();

                }
            });
            GUISettings.applyOrientation(mnuThemeR);
            themesMenu.add(mnuThemeR);
            group.add(mnuThemeR);
            if (themes[iMenu][0].equals(Theme.getID())) {
                mnuThemeR.setSelected(true);
            }
        }

    }

    public void createCurrencyPopup() {
        currencyMenu.removeAll();
        ButtonGroup buttonGroup = new ButtonGroup();

        TWRadioButtonMenuItem menuItem = new TWRadioButtonMenuItem(Language.getString("DEFAULT"));
        menuItem.setActionCommand("C,*");
        menuItem.addActionListener(this);
        currencyMenu.add(menuItem);
        buttonGroup.add(menuItem);
        menuItem = null;
        ArrayList<String> currencyIDs;
        currencyIDs = CurrencyStore.getSharedInstance().getSortedCurrencyIDs();
        for (String description : currencyIDs) {
            String[] data = description.split(Constants.KEY_SEPERATOR_CHARACTER);
            menuItem = new TWRadioButtonMenuItem(data[0]);
            menuItem.setActionCommand("C," + data[1]);
            menuItem.addActionListener(this);
            currencyMenu.add(menuItem);
            buttonGroup.add(menuItem);
            menuItem = null;
        }
//        Enumeration<String> ids = CurrencyStore.getSharedInstance().getCurrencyIDs();
//        while (ids.hasMoreElements()) {
//            String id = ids.nextElement();
//            menuItem = new TWRadioButtonMenuItem(CurrencyStore.getSharedInstance().getCurrencyDescription(id));
//            menuItem.setActionCommand("C," + id);
//            menuItem.addActionListener(this);
//            currencyMenu.add(menuItem);
//            buttonGroup.add(menuItem);
//            menuItem = null;
//        }

        // Arrays.sort(currencyMenu.getModel().);
        GUISettings.applyOrientation(currencyMenu);
    }

    public void setAnnouncementAutoPopupMode(boolean status) {
        mnuPopupAnnouncmWindow.setSelected(status);
    }

    public void setTabIndexVisibleMode(boolean status) {
        mnuShowTabs.setSelected(status);

    }

    public void addAnnouncementAutoPopupModeListener(JCheckBox item) {
        chkDoNotAutoPopupAnnouncements = item;
    }

    public JLabel getSnapshotBulb() {
        return lblPriceConnectionStatus;
    }

    public void closeTreePopup() {
        mnuWatchLists.setSelected(false);
        mnuWatchLists.getPopupMenu().setVisible(false);
    }

    /**
     * Invoked when the menu is deselected.
     *
     * @param e a MenuEvent object
     */
    public void menuDeselected(MenuEvent e) {

    }

    /**
     * Invoked when the menu is canceled.
     *
     * @param e a MenuEvent object
     */
    public void menuCanceled(MenuEvent e) {

    }

    /**
     * Called whenever an item in the tree has been expanded.
     */
    public void treeExpanded(TreeExpansionEvent event) {
        try {
            mnuWatchLists.getPopupMenu().pack();
        } catch (Exception ex) {
        }
    }

    /**
     * Called whenever an item in the tree has been collapsed.
     */
    public void treeCollapsed(TreeExpansionEvent event) {
        try {
            mnuWatchLists.getPopupMenu().pack();
        } catch (Exception ex) {
        }
    }

    /**
     * Invoked when the mouse button has been clicked (pressed
     * and released) on a component.
     */
    public void mouseClicked(MouseEvent e) {
        langList = new JPopupMenu();
        if (e.getSource() == btnLanguage) {
            String[] saLanguages;
            String[] saLanguageIDs;
            saLanguages = Language.getLanguageCaptions();
            saLanguageIDs = Language.getLanguageIDs();

            if (saLanguageIDs.length > 2) {
                for (int i = 0; i < saLanguageIDs.length; i++) {
                    String id = saLanguageIDs[i];
                    if (id.equals(Language.getSelectedLanguage())) continue;
                    TWMenuItem item = new TWMenuItem(saLanguages[i]);
                    item.setFont(new Font(item.getFont().getName(), Font.PLAIN, 11));
                    langList.add(item);
                    item.setActionCommand(id);
                    item.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            Settings.saveItem("LANGUAGE", e.getActionCommand());
                            switchLanguage(e.getActionCommand());
                        }
                    });
                }
                GUISettings.applyOrientation(langList);
                GUISettings.showPopup(langList, (Component) e.getSource(), e.getX(), e.getY());
            } else {
                switchLanguage(null);
            }
        }

    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     */
    public void mousePressed(MouseEvent e) {
    }

    /**
     * Invoked when a mouse button has been released on a component.
     */
    public void mouseReleased(MouseEvent e) {
    }

    /**
     * Invoked when the mouse enters a component.
     */
    public void mouseEntered(MouseEvent e) {
    }

    /**
     * Invoked when the mouse exits a component.
     */
    public void mouseExited(MouseEvent e) {
        closeTreePopup();
    }

    public void setMetaStockUpdateInProgress(boolean inprogress) {
        if (mnuUpdateMetaStockHistoryDB.isVisible()) {
            mnuUpdateMetaStockHistoryDB.setEnabled(!inprogress);
        }
    }

    public void setTradeStationUpdateInProgress(boolean inprogress) {
        if (mnuUpdateTradeStationHistoryDB.isVisible()) {
            mnuUpdateTradeStationHistoryDB.setEnabled(!inprogress);
        }
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        createTimeZoneMenu();
        if ((!offlineMode) && (ExchangeStore.getSharedInstance().defaultCount() == 0)) {
            client.removeFuntionTreeNode();
        }
        //addExchangesToWatchlistFilter();
        //createCurrencyMenu();
    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {
        //createCurrencyMenu();
    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {

    }

    public void currencyAdded() {
    }

    /*-- VAS Menu listener*/

    public void VASDataDownloadFailed() {
    }

    public void VASDataDownloadStarted() {
    }

    public void VASDataDownloadSucess(ArrayList<JComponent> menus, ArrayList<JComponent> links) {
        mnuVAS.removeAll();
//        if (Settings.isTCPMode()) {
//            mnuVAS.add(mnuNewsSearchNew);
//        }
        //mnuVAS.add(reloadVas);

        for (JComponent item : menus) {
            addVasMenuItem(item);
        }
        SwingUtilities.updateComponentTreeUI(mnuVAS);
        SwingUtilities.updateComponentTreeUI(mnuTools);
        SwingUtilities.updateComponentTreeUI(mnuReports);
        SwingUtilities.updateComponentTreeUI(mnuView);
        SwingUtilities.updateComponentTreeUI(mnuHelp);

        // remove links from the table popup
        JPopupMenu popup = client.getTablePopup();
        Component[] items = popup.getComponents();
        for (int i = 0; i < items.length; i++) {
            if (items[i] instanceof JMenuItem) {
                JMenuItem menuitem = (JMenuItem) items[i];
                if (menuitem.getText().equals("VAS_START")) {
                    while (true) {
                        TWMenuItem nextitem = (TWMenuItem) items[i + 1];
                        if (!nextitem.getText().equals("VAS_END")) {
                            popup.remove(nextitem);
                        } else {
                            break;
                        }
                    }
                    break;
                }
            }
        }

        client.getVasMenuItemList().clear();

        // add new items to the table popup
        items = popup.getComponents();
        for (int i = 0; i < items.length; i++) {
            if (items[i] instanceof JMenuItem) {
                JMenuItem menuitem = (JMenuItem) items[i];
                if (menuitem.getText().equals("VAS_START")) { // find the location of the menu start
                    for (Object link : links) {
                        if (link instanceof TWSeparator) {
                            client.getTablePopup().add((TWSeparator) link, ++i);
                        } else if (link instanceof JMenuItem) {
                            client.getTablePopup().add((JMenuItem) link, ++i);
                            client.getVasMenuItemList().add((JMenuItem) link);
                        }
                    }
                    break;
                }
            }
        }

        mnuVAS.setVisible(true);
        SwingUtilities.updateComponentTreeUI(popup);
    }

    public void VASDataDownloadSucess(String id, ArrayList<String> links) {
        // not implemented for vas menu
    }

    private void addVasMenuItem(JComponent item) {
        int location = -1;
        try {
            location = Integer.parseInt((String) item.getClientProperty("SEQUENCE"));
        } catch (Exception e) {
        }
        try {
            if (location >= 0) {
                if (item.getClientProperty("LOCATION").equals("SERVICES")) {
                    mnuVAS.add(item, location);
                } else if (item.getClientProperty("LOCATION").equals("REPORTS")) {
                    mnuReports.add(item, location);
                } else if (item.getClientProperty("LOCATION").equals("TOOLS")) {
                    mnuTools.add(item, location);
                } else if (item.getClientProperty("LOCATION").equals("ANALYSIS")) {
                    mnuAnalysis.add(item, location);
                } else if (item.getClientProperty("LOCATION").equals("VIEW")) {
                    mnuView.add(item, location);
                } else if (item.getClientProperty("LOCATION").equals("HELP")) {
                    mnuHelp.add(item, location);
                } else if (item.getClientProperty("LOCATION").equals("POPUP")) {
                    client.getTablePopup().add(item, location);
                } else {
                    mnuVAS.add(item, location);
                }
            } else {
                if (item.getClientProperty("LOCATION").equals("SERVICES")) {
                    mnuVAS.add(item);
                } else if (item.getClientProperty("LOCATION").equals("REPORTS")) {
                    mnuReports.add(item);
                } else if (item.getClientProperty("LOCATION").equals("TOOLS")) {
                    mnuTools.add(item);
                } else if (item.getClientProperty("LOCATION").equals("ANALYSIS")) {
                    mnuAnalysis.add(item);
                } else if (item.getClientProperty("LOCATION").equals("VIEW")) {
                    mnuView.add(item);
                } else if (item.getClientProperty("LOCATION").equals("HELP")) {
                    mnuHelp.add(item);
                } else if (item.getClientProperty("LOCATION").equals("TRADE")) {
                    mnuTrade.add(item);
                } else if (item.getClientProperty("LOCATION").equals("POPUP")) {
                    client.getTablePopup().add(item);
                } else {
                    mnuVAS.add(item);
                }
            }
        } catch (Exception e) {
            mnuVAS.add(item);
        }
        /*if (item instanceof JSeparator){
            mnuVAS.add(item);
        } else if (item instanceof TWMenuItem){
            mnuVAS.add((TWMenuItem)item);
        } else {
            mnuVAS.add((TWMenu)item);
        }*/
    }


    public void tradeServerConnected() {
        String str = Language.getString("TRADEUSER_MESSAGE");
        str = str.replaceAll("\\[TRADE_USER\\]", TradingShared.getTrader().getTraderName(Constants.PATH_PRIMARY));
        tradeUserName.setText(str);
        tradeUserName.setVisible(true);
        if (TradingShared.getTrader().getPortfolioCount() > 1) {
            mnuCashTransReq.setVisible(true);
        } else {
            mnuCashTransReq.setVisible(false);
        }
        mnuMarginableSymbolsUI.setVisible(BrokerConfig.getSharedInstance().isMarginableSymbolsAvailable(Constants.PATH_PRIMARY) && TradingShared.getTrader().isMarginEnabled());
        mnuOpenPositions.setVisible(BrokerConfig.getSharedInstance().isOPenPositionsAvailable(Constants.PATH_PRIMARY));
        mnuDepositNOtification.setVisible(BrokerConfig.getSharedInstance().isDepositWindowAvailable(Constants.PATH_PRIMARY));
        //Added by chandika for the SHUAA requirement
        mnuCashTransReq.setVisible(BrokerConfig.getSharedInstance().isCashTransferAvailable(Constants.PATH_PRIMARY));
        mnuCashLogOrders.setVisible(BrokerConfig.getSharedInstance().isCashLogSearchAvailable(Constants.PATH_PRIMARY));
        mnuCash.setVisible(BrokerConfig.getSharedInstance().isCustomerStatementAvailable(Constants.PATH_PRIMARY));
        mnuIntradyPositionMoniter.setVisible(BrokerConfig.getSharedInstance().isIntradayPositionMonitorAvailable(Constants.PATH_PRIMARY));

        mnuWithdrawalReq.setVisible(BrokerConfig.getSharedInstance().isWithdrawalWindowAvailable(Constants.PATH_PRIMARY));
        boolean isFundTrnsVisable = BrokerConfig.getSharedInstance().isWithdrawalWindowAvailable(Constants.PATH_PRIMARY) ||
                BrokerConfig.getSharedInstance().isDepositWindowAvailable(Constants.PATH_PRIMARY);
        mnuFundTransWin.setVisible(isFundTrnsVisable);
        tradeServices.setVisible(TradingShared.isTradeServicesEnable(Constants.PATH_PRIMARY));
        tradeServices.setEnabled(TradingShared.isTradeServicesEnable(Constants.PATH_PRIMARY));
        if (!TradeMethods.isDuInvestURLAvailable()) {
            mnuDUInvestURL.setVisible(false);
            duSeperator.setVisible(false);
            subscriptionDoInvest.setVisible(true);
        }
        mnuCalcSelection.setVisible(TWControl.isEnableAfterMarketValuation());

    }

    public void tradeSecondaryPathConnected() {
        if (TradingShared.getTrader().getPortfolioCount() > 1) {
            mnuCashTransReq.setVisible(true);
        } else {
            mnuCashTransReq.setVisible(false);
        }
        //Added by chandika for the SHUAA requirement
        mnuCashTransReq.setVisible(BrokerConfig.getSharedInstance().isCashTransferAvailable(Constants.PATH_SECONDARY));
        mnuCashLogOrders.setVisible(BrokerConfig.getSharedInstance().isCashLogSearchAvailable(Constants.PATH_SECONDARY));
        mnuCash.setVisible(BrokerConfig.getSharedInstance().isCustomerStatementAvailable(Constants.PATH_SECONDARY));

        if (!mnuMarginableSymbolsUI.isVisible()) {
            mnuMarginableSymbolsUI.setVisible(BrokerConfig.getSharedInstance().isMarginableSymbolsAvailable(Constants.PATH_SECONDARY) && TradingShared.getTrader().isMarginEnabled());
        }
        if (!mnuOpenPositions.isVisible()) {
            mnuOpenPositions.setVisible(BrokerConfig.getSharedInstance().isOPenPositionsAvailable(Constants.PATH_SECONDARY));
        }
        if (!mnuDepositNOtification.isVisible()) {
            mnuDepositNOtification.setVisible(BrokerConfig.getSharedInstance().isDepositWindowAvailable(Constants.PATH_SECONDARY));
        }
        if (!mnuWithdrawalReq.isVisible()) {
            mnuWithdrawalReq.setVisible(BrokerConfig.getSharedInstance().isDepositWindowAvailable(Constants.PATH_SECONDARY));
        }
        if (!mnuFundTransWin.isVisible()) {
            boolean isFundTrnsVisable = BrokerConfig.getSharedInstance().isDepositWindowAvailable(Constants.PATH_SECONDARY) ||
                    BrokerConfig.getSharedInstance().isDepositWindowAvailable(Constants.PATH_SECONDARY);
            mnuFundTransWin.setVisible(isFundTrnsVisable);
        }
        if (!tradeServices.isVisible()) {
            tradeServices.setVisible(TradingShared.isTradeServicesEnable(Constants.PATH_SECONDARY));
            tradeServices.setEnabled(TradingShared.isTradeServicesEnable(Constants.PATH_SECONDARY));
        }
        if (TWControl.isEnableAfterMarketValuation()) {
            mnuCalcSelection.setVisible(true);
        }
        if (!mnuCalcSelection.isVisible()) {
            mnuCalcSelection.setVisible(TWControl.isEnableAfterMarketValuation());
        }

    }

    public void tradeServerDisconnected() {
        tradeUserName.setVisible(false);
        subscriptionDoInvest.setVisible(false);
        mnuCalcSelection.setVisible(false);
    }

    public TWMenuItem getForexMenuItem() {
        return mnuForexView;
    }

    private boolean isHelpFileAvailable(String fileName) {
        return true;
//        try {
//            File file = new File("./help/" + fileName + "_" + Language.getLanguageTag() +".chm");
//            return (file.exists());
//        } catch (Exception e) {
//            return false;
//        }
    }

    private boolean isHelpFileWhatsNewAvailable(String fileName) {
        try {
            File file = new File("./help/" + fileName + "_" + Language.getLanguageTag() + ".chm");
            return (file.exists());
        } catch (Exception e) {
            return false;
        }
    }

    //TradeHelp_EN

    private boolean isTradeHelpAvailable(String fileName) {
        try {
            File file = new File("./help/" + fileName + "_" + Language.getLanguageTag() + ".chm");
            return (file.exists());
        } catch (Exception e) {
            return false;
        }
    }

    /*public static boolean isTipOfDayAvailable() {
        try {
            File file = new File("help/Tip of the Day/" + Language.getSelectedLanguage());
            return (file.listFiles()[2].exists());
        } catch (Exception e) {
            return false;
        }
    }*/

    public static boolean isTipOfDayAvailable(String fileName) {
        try {
            File file = new File("./help/" + fileName + "_" + Language.getLanguageTag() + ".chm");
            return (file.exists());
        } catch (Exception e) {
            return false;
        }
    }

    public void setArabicNumbers(boolean status) {
        mnuArabicNumbers.setSelected(status);
    }

    public boolean isArabicNumbersSelected() {
        return mnuArabicNumbers.isSelected();
    }

    public void setFullScreenMode(boolean status) {
        mnuFullScreen.setSelected(status);
    }

    public boolean isFullScreenModeSelected() {
        return mnuFullScreen.isSelected();
    }

    public void setShowTabPanel(boolean status) {
        mnuShowTabs.setSelected(status);
    }

    public boolean isShowTabPanelSelected() {
        return mnuShowTabs.isSelected();
    }

    public void setShowAlies(boolean status) {
        mnuAlias.setSelected(status);
    }

    public boolean isShowAliasSelected() {
        return mnuAlias.isSelected();
    }

    public void setCombinedOrderBook(boolean status) {
        mnuShowCombinedOrderBook.setSelected(status);
    }

    public boolean isCombinedOrderBookSelected() {
        return mnuShowCombinedOrderBook.isSelected();
    }

    public void setPopupAnnouncements(boolean status) {
        mnuPopupAnnouncmWindow.setSelected(status);
    }

    public boolean isPopupAnnouncementsSelected() {
        return mnuPopupAnnouncmWindow.isSelected();
    }

    public void setSaveTradesOnExit(boolean status) {
        mnuSaveTradesOnExit.setSelected(status);
    }

    public boolean isSaveTradesOnExitSelected() {
        return mnuSaveTradesOnExit.isSelected();
    }

    public void setAutoScrollingStatus(boolean status) {
        g_mnuAutoScroll.setSelected(status);
    }

    public boolean isAutoScrollingSelected() {
        return g_mnuAutoScroll.isSelected();
    }


    public void setTradeAlertPopup(boolean status) {
        mnuShowTradeAlertPopup.setSelected(status);
    }

    public boolean isShowTradeAlertPopupSelected() {
        return mnuShowTradeAlertPopup.isSelected();
    }

    public JCheckBox getDoNotAutoPopupAnnouncements() {
        return chkDoNotAutoPopupAnnouncements;
    }

    public void setDoNotAutoPopupAnnouncementsSelected(boolean status) {
        chkDoNotAutoPopupAnnouncements.setSelected(status);
    }

    public void updateMarginableSymbolsMenu(boolean ismarginenabled) {
        mnuMarginableSymbolsUI.setVisible(ismarginenabled);
    }

    public void mwTradeMenuCreated() {
        mnuTrade.setVisible(false);
        mnuMWTrade.setVisible(true);

        g_btnTradeConnect.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/tradedisconnect.gif"));
        g_btnTradeConnect.setRollOverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/tradedisconnect_R.gif"));
        g_btnTradeConnect.setToolTipText(TradingShared.getMessageString("DISCONECT_FROM_BROKER") + ":" + MWTradingHandler.getSharedInstance().getBrokerId());
        mnuTradeConnect.setText(TradingShared.getMessageString("DISCONECT_FROM_BROKER") + ":" + MWTradingHandler.getSharedInstance().getBrokerId());
        g_btnTradeConnect.updateUI();
        System.out.println("########################################################## menu creation fired!!" + MWTradingHandler.getSharedInstance().getBrokerId());
    }

    public void mwServerDisconnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    private void createMWTradingMenu() {
        mnuMWTrade = new JMenu(Language.getString("MUBASHER_TRADE"));
        mnuMWTrade.setVisible(false);
        mnuMWTrade.getPopupMenu().setLightWeightPopupEnabled(false);
        mnuMWTrade.setOpaque(false);
    }

    private void createWebTradingMenu() {

        mnuWebTrade = new JMenu(Language.getString("MUBASHER_TRADE_MENU"));
        mnuWebTrade.setVisible(false);
        mnuWebTrade.getPopupMenu().setLightWeightPopupEnabled(false);
        mnuWebTrade.setOpaque(false);

        mnuWebTrade.add(new TWSeparator());
        mnuOnlineTrading = new TWMenu(Language.getString("ONLINE_TRADING"),"rapidOrder.gif");
        mnuOnlineTrading.setOpaque(false);
        mnuWebTrade.add(mnuOnlineTrading);

        TWMenuItem menu1 = new TWMenuItem("Login");
//        mnuWebTrade.add(menu1);
        menu1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BrowserManager.getInstance().navigate("LOGIN3","https://online.globalinv.com.eg/Login.aspx?ReturnUrl=%2fAccount.aspx","LOGIN3");
//                BrowserManager.getInstance().navigate("LOGIN3","\\Templates\\Login\\login.html","LOGIN3");//Templates\Login\login.html
            }
        });

        mnuAddOrder = new TWMenuItem(Language.getString("ADD_ORDER"), "trade.gif", false);
        mnuOnlineTrading.add(mnuAddOrder);
        mnuAddOrder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BrowserManager.getInstance().navigate("LOGIN3",TWControl.getRequestURL("NEW_ORDER"),Language.getString("ADD_ORDER"));  //Templates\Login\login.html
            }
        });
        mnuPendingOrders = new TWMenuItem(Language.getString("PENDING_ORDERS"), "orderlist.gif", false);
        mnuOnlineTrading.add(mnuPendingOrders);
        mnuPendingOrders.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BrowserManager.getInstance().navigate("LOGIN3", TWControl.getRequestURL("PENDING_ORDERS"), Language.getString("PENDING_ORDERS"));  //Templates\Login\login.html
            }
        });

        mnuWebTrade.add(new TWSeparator());
        mnuTradingHistory = new TWMenu(Language.getString("TRADING_HISTORY"), "fundtransfer.gif");
        mnuTradingHistory.setOpaque(false);
        mnuWebTrade.add(mnuTradingHistory);


        mnuOrderHistory = new TWMenuItem(Language.getString("ORDERS"), "ordersearch.gif", false);
        mnuTradingHistory.add(mnuOrderHistory);
        mnuOrderHistory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BrowserManager.getInstance().navigate("LOGIN3",TWControl.getRequestURL("ORDERS"),Language.getString("ORDERS"));  //Templates\Login\login.html
            }
        });
        mnuExecutions = new TWMenuItem(Language.getString("EXECUTIONS"), "companyfinancial.gif");
        mnuTradingHistory.add(mnuExecutions);
        mnuExecutions.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BrowserManager.getInstance().navigate("LOGIN3",TWControl.getRequestURL("EXECUTIONS"),Language.getString("EXECUTIONS"));  //Templates\Login\login.html
            }
        });
        mnuInvoices = new TWMenuItem(Language.getString("INVOICES"), "fundtransferlist.gif", false);
        mnuTradingHistory.add(mnuInvoices);
        mnuInvoices.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BrowserManager.getInstance().navigate("LOGIN3",TWControl.getRequestURL("INVOICES"),Language.getString("INVOICES"));  //Templates\Login\login.html
            }
        });

        mnuWebTrade.add(new TWSeparator());
        mnuValuation = new TWMenu(Language.getString("VALUATION"), "portfoliosim.gif");
        mnuValuation.setOpaque(false);
        mnuWebTrade.add(mnuValuation);

        mnuPosition = new TWMenuItem(Language.getString("POSITION"), "portfolio.gif", false);
        mnuValuation.add(mnuPosition);
        mnuPosition.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BrowserManager.getInstance().navigate("LOGIN3",TWControl.getRequestURL("POSITION"),Language.getString("POSITION"));  //Templates\Login\login.html
            }
        });
        mnuStatement = new TWMenuItem(Language.getString("STATEMENT"), "accountsummary.gif", false);
        mnuValuation.add(mnuStatement);
        mnuStatement.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BrowserManager.getInstance().navigate("LOGIN3",TWControl.getRequestURL("STATEMENT"),Language.getString("STATEMENT"));  //Templates\Login\login.html
            }
        });
        mnuFundTransaction = new TWMenuItem(Language.getString("FUND_TRANSACTION"),"fundtransfer.gif", false);
        mnuValuation.add(mnuFundTransaction);
        mnuFundTransaction.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                BrowserManager.getInstance().navigate("LOGIN3",TWControl.getRequestURL("FUND_TRANSACTION"),Language.getString("FUND_TRANSACTION"));
            }
        });

        mnuWebTrade.add(new TWSeparator());
        mnuServices = new TWMenu(Language.getString("SERVICES"), "companyprofile.gif");
        mnuServices.setOpaque(false);
        mnuWebTrade.add(mnuServices);

        mnuAccountServices = new TWMenuItem(Language.getString("ACCOUNT"), "changepassword.gif", false);
        mnuServices.add(mnuAccountServices);
        mnuAccountServices.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BrowserManager.getInstance().navigate("LOGIN3",TWControl.getRequestURL("ACCOUNT"),Language.getString("ACCOUNT"));  //Templates\Login\login.html
            }
        });
        mnuReportsServices = new TWMenuItem(Language.getString("REPORTS"), "report.gif", false);
        mnuServices.add(mnuReportsServices);
        mnuReportsServices.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BrowserManager.getInstance().navigate("LOGIN3",TWControl.getRequestURL("REPORTS"),Language.getString("REPORTS"));  //Templates\Login\login.html
            }
        });
        mnuDownloadsServices = new TWMenuItem(Language.getString("DOWNLOADS"), "downloadhistory.gif", false);
        mnuServices.add(mnuDownloadsServices);
        mnuDownloadsServices.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                BrowserManager.getInstance().navigate("LOGIN3",TWControl.getRequestURL("DOWNLOADS"),Language.getString("DOWNLOADS"));  //Templates\Login\login.html
            }
        });

        mnuWebTrade.add(new TWSeparator());

        GUISettings.applyOrientation(mnuOnlineTrading);
        GUISettings.applyOrientation(mnuTradingHistory);
        GUISettings.applyOrientation(mnuValuation);
        GUISettings.applyOrientation(mnuServices);
        GUISettings.applyOrientation(mnuWebTrade);

    }

    private String loadTemplate(String language) {
        StringBuilder buffer = new StringBuilder();
        byte[] temp = new byte[1000];
        String template = "";
        int count = 0;
        try {
            if ((language == null) || (language.equals(""))){
                language = Language.getLanguageTag();
            }
            InputStream in = new FileInputStream(".\\Templates\\Login\\login.html");
            while (true) {
                count = in.read(temp);
                if (count == -1) break;
                if (count > 0) {
                    String str = new String(temp, 0, count);
                    buffer.append(str);
                    str = null;
                }
            }
            template = buffer.toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            template = "";
        }
        return  template;
    }

    public JMenu getMnuTrade() {
        return mnuTrade;
    }

    public JMenu getMnuMWTrade() {
        return mnuMWTrade;
    }

    public JMenu getWebTrade() {
        return mnuWebTrade;
    }

    public void createAfterMarketValuationMenu() {
        mnuCalcSelection.removeAll();

        ButtonGroup oGroup = new ButtonGroup();
        String[] langTags = {Language.getString("USE_LAST_PRICE"), Language.getString("USE_CLOSE_PRICE")};
        String[] IDs = {"0", "1"};

        for (int i = 0; i < langTags.length; i++) {
            final TWRadioButtonMenuItem item = new TWRadioButtonMenuItem(langTags[i]);
            oGroup.add(item);
            mnuCalcSelection.add(item);
            item.setActionCommand(IDs[i]);
            if (Integer.parseInt(IDs[i]) == (Settings.getAfterMarketValuationMode())) {
                item.setSelected(true);
            }
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Settings.setAfterMarketValuationMode(Integer.parseInt(item.getActionCommand()));
                }
            });

        }
        GUISettings.applyOrientation(mnuCalcSelection);
    }
}
