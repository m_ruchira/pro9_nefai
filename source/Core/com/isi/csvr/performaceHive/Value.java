package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:36:55 AM
 * To change this template use File | Settings | File Templates.
 */

import java.io.Serializable;

public abstract class Value
        implements Comparable, Serializable {

    public Value() {
    }

    public abstract double getValue();

    public abstract String getLabel();

    public abstract void setValue(double d);

    public abstract void setLabel(String s);

    public int hashCode() {
        int result = super.hashCode();
        long temp = Double.doubleToLongBits(getValue());
        result = 31 * result + (int) (temp ^ temp >>> 32);
        return result;
    }

    public boolean equals(Object obj) {
        if (this == obj)
            return true;
        if (!super.equals(obj))
            return false;
        if (getClass() != obj.getClass())
            return false;
        Value other = (Value) obj;
        return Double.doubleToLongBits(getValue()) == Double.doubleToLongBits(other.getValue());
    }

    public int compareTo(Object value) {
        if (value != null && (value instanceof Value)) {
            Value value2 = (Value) value;
            if (getValue() < value2.getValue())
                return -1;
            else
                return getValue() <= value2.getValue() ? 0 : 1;
        } else {
            throw new IllegalArgumentException();
        }
    }
}