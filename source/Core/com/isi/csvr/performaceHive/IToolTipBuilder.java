package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:52:21 AM
 * To change this template use File | Settings | File Templates.
 */
import javax.swing.JToolTip;

public interface IToolTipBuilder
{

    public abstract JToolTip getToolTip();
}