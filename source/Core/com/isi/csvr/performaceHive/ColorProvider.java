package com.isi.csvr.performaceHive;

/**
 * Created by IntelliJ IDEA.
 * User: dilini
 * Date: Jul 30, 2009
 * Time: 11:51:32 AM
 * To change this template use File | Settings | File Templates.
 */

import java.awt.Color;
import java.io.Serializable;
import javax.swing.JPanel;

public abstract class ColorProvider
        implements Serializable {

    public ColorProvider() {
    }

    public abstract Color getColor(Value value);
}
