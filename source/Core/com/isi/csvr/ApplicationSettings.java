package com.isi.csvr;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.awt.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Set;
import java.util.TimeZone;

import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.shared.*;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.datastore.TimeZoneMap;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.announcement.AnnouncementStore;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.chart.ChartSettings;
import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.GraphDataManager;
import com.isi.csvr.chart.ComboItem;
import com.isi.util.FlexGridLayout;

/**
 * Created by IntelliJ IDEA.
 * User: janithk
 * Date: Dec 2, 2009
 * Time: 10:49:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class ApplicationSettings extends InternalFrame implements ActionListener, TradingConnectionListener {
    private static ApplicationSettings self = null;
    private TWTabbedPane userTab;
    //private final int WINDOW_WIDTH = 450;
    private final int WINDOW_WIDTH = 480;
    private final int WINDOW_HEIGHT = 390;
    private TWButton btnOk;
    private TWButton btnCancel;
    private JCheckBox chkDualScr;
    private JRadioButton rbRight;
    private JRadioButton rbLeft;
    private TWComboBox scrollingCombo;
    private String[] scrollDataArray = {Language.getString("1_SEC"), Language.getString("2_SEC"),
            Language.getString("5_SEC"), Language.getString("AUTO_SCROLL_10_SEC"),
            Language.getString("AUTO_SCROLL_15_SEC")};

    private JCheckBox chkAutoScroll;
    private JCheckBox chkArabicNumbers;
    private JCheckBox chkShowTabs;
    private JCheckBox chkAlias;
    private JCheckBox chkCombOrderBook;
    private JCheckBox chkPopupAnnouncements;
    private JCheckBox chkSaveTradesOnExit;
    private JCheckBox chkShowMinMax;
    private JCheckBox chkShow52WeekHL;
    private JCheckBox chkOpenInProChart;
    private JCheckBox chkShowTradeAlertPopup;
    private JCheckBox chkkeepOrderWindowOpen;
    private JCheckBox chkPutToSameLayer;
    private JCheckBox chkKeepOrderWindowConfirmationWindow;


    private TWComboBox cmbTimeZones;
    private TWComboBox cmbLanguages;
    private TWComboBox cmbNewsIndicator;

    private ArrayList<TWComboItem> lstTimeZones;
    private ArrayList<TWComboItem> lstLanguages;
    private ArrayList<TWComboItem> lstNewsIndicator;

    private String selectedLangId;
    private boolean autoScrollState;
    private boolean showArabicNumbers;

    private static TimeZone tzone;

    //--------- shashika -------------
    private TWTabbedPane tabbedpane;

    private JRadioButton rb52WeekHighlightRows;
    private JRadioButton rb52WeekDisplayIcons;

    private JRadioButton rbMinMaxHighlightRows;
    private JRadioButton rbMinMaxDisplayIcons;

    public ApplicationSettings() {
        this.setTitle(Language.getString("APPLICATION_SETTINGS"));
        this.setSize(WINDOW_WIDTH, WINDOW_HEIGHT);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setClosable(true);
        this.setIconifiable(true);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        this.hideTitleBarMenu();
        createUI();
        Client.getInstance().getDesktop().add(this);
        GUISettings.applyOrientation(this);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
    }

    public static ApplicationSettings getInstance() {
        if (self == null) {
            self = new ApplicationSettings();
        }
        return self;
    }

    private void createUI() {
        //JPanel pnlMain = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"30%", "40%", "20%"}, 2, 2));
        JPanel pnlMain = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "25"}, 2, 2));
        userTab = new TWTabbedPane();
        userTab.addTab(Language.getString("PRICE_TAB"), createPricePanel());
        if (Settings.isSingleSignOnMode()) {
            userTab.addTab(Language.getString("TRADE_TAB"), createTradePanel());
        }
        userTab.setSelectedIndex(0);

//        pnlMain.add(userTab); todo:use tabbed view when seperating price/trade related settings
        /*pnlMain.add(createUpperPanel());
        pnlMain.add(createMidCommonPanel());
        pnlMain.add(createDualScrPanel());*/
//        pnlMain.add(createAutoScrollPanel());

        //----------- shashika -----------
        tabbedpane = new TWTabbedPane();
        tabbedpane.addTab(Language.getString("MARKET_DATA"), createMarketDataPanel());
        tabbedpane.addTab(Language.getString("TRADE"), createTradeOptionsPanel());
        tabbedpane.addTab(Language.getString("GENERAL"), createGeneralPanel());
        tabbedpane.setSelectedIndex(0);
        pnlMain.add(tabbedpane);

        pnlMain.add(createButtonPanel());
        initializeSettings();
        this.add(pnlMain);
    }

    private JPanel createMarketDataPanel() {
        JPanel pnlMarket = new JPanel(new FlexGridLayout(new String[]{"45%", "30%", "25%"}, new String[]{"25", "75", "75", "25", "25"}, 5, 10));
        chkPopupAnnouncements = new JCheckBox(Language.getString("POPUP_ANNOUNCE_WINDOW"));
        chkPopupAnnouncements.addActionListener(this);

        JLabel lblNewsIndicator = new JLabel(Language.getString("NEWS_INDICATOR_EXPIRY_LABEL"));
        lstNewsIndicator = new ArrayList<TWComboItem>();
        cmbNewsIndicator = new TWComboBox(new TWComboModel(lstNewsIndicator));
        cmbNewsIndicator.addActionListener(this);
        createNewsIndicatorExpiryTime();

        // --------- MIn Max --------------

        JPanel pnlMinMax = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "20", "20"}, 0, 0));
        chkShowMinMax = new JCheckBox(Language.getString("SHOW_MIN_MAX"));
        chkShowMinMax.addActionListener(this);
        rbMinMaxHighlightRows = new JRadioButton(Language.getString("HIGHLIGHT_ROWS"));
        rbMinMaxDisplayIcons = new JRadioButton(Language.getString("DISPLAY_ICONS"));
        ButtonGroup grpMinMax = new ButtonGroup();
        grpMinMax.add(rbMinMaxHighlightRows);
        grpMinMax.add(rbMinMaxDisplayIcons);

        pnlMinMax.add(chkShowMinMax);
        pnlMinMax.add(rbMinMaxHighlightRows);
        pnlMinMax.add(rbMinMaxDisplayIcons);

        //------- 52 week High Low -----------------
        JPanel pnl52Week = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "20", "20"}, 0, 0));
        chkShow52WeekHL = new JCheckBox(Language.getString("SHOW_52WK_HIGHLOW"));
        chkShow52WeekHL.addActionListener(this);
        rb52WeekHighlightRows = new JRadioButton(Language.getString("HIGHLIGHT_ROWS"));
        rb52WeekDisplayIcons = new JRadioButton(Language.getString("DISPLAY_ICONS"));
        ButtonGroup grp52Week = new ButtonGroup();
        grp52Week.add(rb52WeekHighlightRows);
        grp52Week.add(rb52WeekDisplayIcons);

        pnl52Week.add(chkShow52WeekHL);
        pnl52Week.add(rb52WeekHighlightRows);
        pnl52Week.add(rb52WeekDisplayIcons);

        //----------------------------------------

        chkSaveTradesOnExit = new JCheckBox(Language.getString("SAVE_TRADES_ON_EXIT"));
        chkSaveTradesOnExit.addActionListener(this);

        chkCombOrderBook = new JCheckBox(Language.getString("COMBINED_ORDER_BOOK"));
        chkCombOrderBook.addActionListener(this);

        pnlMarket.add(chkPopupAnnouncements);
        pnlMarket.add(lblNewsIndicator);
        pnlMarket.add(cmbNewsIndicator);
        pnlMarket.add(pnlMinMax);
        pnlMarket.add(new JLabel());
        pnlMarket.add(new JLabel());
        pnlMarket.add(pnl52Week);
        pnlMarket.add(new JLabel());
        pnlMarket.add(new JLabel());
        pnlMarket.add(chkSaveTradesOnExit);
        pnlMarket.add(new JLabel());
        pnlMarket.add(new JLabel());
        pnlMarket.add(chkCombOrderBook);

        return pnlMarket;
    }

    private JPanel createTradeOptionsPanel() {
        JPanel pnlTrade = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"25", "25", "25"}, 5, 10));

        chkkeepOrderWindowOpen = new JCheckBox(Language.getString("KEEP_ORDER_WINDOW_OPEN"));
        chkkeepOrderWindowOpen.setEnabled(TradingShared.isConnected());
        chkkeepOrderWindowOpen.addActionListener(this);

        chkShowTradeAlertPopup = new JCheckBox(Language.getString("SHOW_TRADE_ALERT_POPUP"));
        chkShowTradeAlertPopup.setEnabled(TradingShared.isConnected());
        chkShowTradeAlertPopup.addActionListener(this);

        chkKeepOrderWindowConfirmationWindow = new JCheckBox(Language.getString("KEEP_ORDER_WINDOW_CONFIRMATION_DIALOG_OPEN"));
        chkKeepOrderWindowConfirmationWindow.setEnabled(TradingShared.isConnected());
        chkKeepOrderWindowConfirmationWindow.addActionListener(this);

        pnlTrade.add(chkkeepOrderWindowOpen);
        pnlTrade.add(chkShowTradeAlertPopup);
        if(TWControl.isNefaie()){
        pnlTrade.add(chkKeepOrderWindowConfirmationWindow);
        }    
        return pnlTrade;
    }

    private JPanel createGeneralPanel() {
        JPanel pnlGeneral = new JPanel(new FlexGridLayout(new String[]{"45%", "30%", "25%"}, new String[]{"25", "25", "25", "25","25", "75"}, 5, 10));

        chkAutoScroll = new JCheckBox(Language.getString("AUTO_SCROLL"));
        chkAutoScroll.addActionListener(this);
        if (Settings.isScrollingOn())
            chkAutoScroll.setSelected(true);
        chkArabicNumbers = new JCheckBox(Language.getString("ARABIC_NUMBERS"));
        chkArabicNumbers.addActionListener(this);
        chkShowTabs = new JCheckBox(Language.getString("SHOW_TAB_PANEL"));
        chkShowTabs.addActionListener(this);
        chkAlias = new JCheckBox(Language.getString("SHOW_ALIAS"));
        chkAlias.addActionListener(this);

        lstTimeZones = new ArrayList<TWComboItem>();
        cmbTimeZones = new TWComboBox(new TWComboModel(lstTimeZones));
        cmbTimeZones.addActionListener(this);
        createTimeZonesList();

        lstLanguages = new ArrayList<TWComboItem>();
        cmbLanguages = new TWComboBox(new TWComboModel(lstLanguages));
        cmbLanguages.addActionListener(this);
        createLanguagesList();

        chkPutToSameLayer =  new JCheckBox(Language.getString("PUT_ALL_TO_SAME_LAYER"));
        chkPutToSameLayer.addActionListener(this);

        JLabel lblTimeZones = new JLabel(Language.getString("TIME_ZONES"));
        JLabel lblLanguages = new JLabel(Language.getString("SELECT_LANGUAGE"));
        JLabel lblAutoScroll = new JLabel(Language.getString("AUTO_SCROLL_INTERVAL"));

        scrollingCombo = new TWComboBox(scrollDataArray);

        pnlGeneral.add(chkAutoScroll);
        pnlGeneral.add(lblAutoScroll);
        pnlGeneral.add(scrollingCombo);
        pnlGeneral.add(chkArabicNumbers);
        pnlGeneral.add(lblLanguages);
        pnlGeneral.add(cmbLanguages);
        pnlGeneral.add(chkShowTabs);
        pnlGeneral.add(lblTimeZones);
        pnlGeneral.add(cmbTimeZones);
        pnlGeneral.add(chkAlias);
        pnlGeneral.add(new JLabel());
        pnlGeneral.add(new JLabel());
        pnlGeneral.add(chkPutToSameLayer);
        pnlGeneral.add(new JLabel());
        pnlGeneral.add(new JLabel());
        pnlGeneral.add(createDualScrPanel());

        return pnlGeneral;
    }

    private JPanel createTradePanel() {
        JPanel pnlTrade = new JPanel();
        return pnlTrade;
    }

    private JPanel createPricePanel() {
        JPanel pnlPrice = new JPanel();
        pnlPrice.setBorder(new OvalBorder(5, 5, Color.gray, Color.gray));
        return pnlPrice;
    }

    private JPanel createUpperPanel() {
        JPanel pnlUpper = new JPanel(new FlexGridLayout(new String[]{"150", "150", "100%"}, new String[]{"20", "20", "20", "20"}, 2, 2));
        lstTimeZones = new ArrayList<TWComboItem>();
        cmbTimeZones = new TWComboBox(new TWComboModel(lstTimeZones));
        cmbTimeZones.addActionListener(this);
        createTimeZonesList();

        lstLanguages = new ArrayList<TWComboItem>();
        cmbLanguages = new TWComboBox(new TWComboModel(lstLanguages));
        cmbLanguages.addActionListener(this);
        createLanguagesList();

        lstNewsIndicator = new ArrayList<TWComboItem>();
        cmbNewsIndicator = new TWComboBox(new TWComboModel(lstNewsIndicator));
        cmbNewsIndicator.addActionListener(this);
        createNewsIndicatorExpiryTime();

        JLabel lblTimeZones = new JLabel(Language.getString("TIME_ZONES"));
        JLabel lblLanguages = new JLabel(Language.getString("SELECT_LANGUAGE"));
        JLabel lblAutoScroll = new JLabel(Language.getString("AUTO_SCROLL_INTERVAL"));
        JLabel lblNewsIndicator = new JLabel(Language.getString("NEWS_INDICATOR_EXPIRY_LABEL"));

        scrollingCombo = new TWComboBox(scrollDataArray);

        pnlUpper.add(lblTimeZones);
        pnlUpper.add(cmbTimeZones);
        pnlUpper.add(new JLabel());
        pnlUpper.add(lblLanguages);
        pnlUpper.add(cmbLanguages);
        pnlUpper.add(new JLabel());
        pnlUpper.add(lblAutoScroll);
        pnlUpper.add(scrollingCombo);
        pnlUpper.add(new JLabel());
        pnlUpper.add(lblNewsIndicator);
        pnlUpper.add(cmbNewsIndicator);
        pnlUpper.add(new JLabel());

        pnlUpper.setBorder(new OvalBorder(5, 5, Color.gray, Color.gray));
        return pnlUpper;
    }

    private JPanel createButtonPanel() {
        JPanel pnlButton = new JPanel(new FlexGridLayout(new String[]{"100%", "80", "80"}, new String[]{"100%"}, 2, 2));
        btnOk = new TWButton(Language.getString("OK"));
        btnCancel = new TWButton(Language.getString("CANCEL"));
        btnOk.addActionListener(this);
        btnCancel.addActionListener(this);

        pnlButton.add(new JLabel());
        pnlButton.add(btnOk);
        pnlButton.add(btnCancel);
        return pnlButton;
    }

    private JPanel createDualScrPanel() {
        JPanel pnlDualScr = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "20", "20"}, 0, 0));
        chkDualScr = new JCheckBox(Language.getString("DUAL_SCREEN_MODE"));
        rbLeft = new JRadioButton(Language.getString("LEFT_SCREEN_POPUP"));
        rbLeft.setSelected(true);
        rbRight = new JRadioButton(Language.getString("RIGHT_SCREEN_POPUP"));
        ButtonGroup group = new ButtonGroup();
        group.add(rbLeft);
        group.add(rbRight);
        pnlDualScr.add(chkDualScr);
        pnlDualScr.add(rbLeft);
        pnlDualScr.add(rbRight);
        if (Settings.isMultiScreenEnvr()) {
            chkDualScr.setEnabled(true);
            chkDualScr.setSelected(Settings.isDualScreenMode());
            if (Settings.isLeftAlignPopups()) {
                rbLeft.setEnabled(true);
                rbLeft.setSelected(true);
                rbRight.setEnabled(true);
                rbRight.setSelected(false);
            } else {
                rbRight.setEnabled(true);
                rbRight.setSelected(true);
                rbLeft.setEnabled(true);
                rbLeft.setSelected(false);
            }
        } else {
            chkDualScr.setEnabled(false);
            rbLeft.setEnabled(false);
            chkDualScr.setSelected(Settings.isDualScreenMode());
            rbRight.setEnabled(false);
        }
        //pnlDualScr.setBorder(new OvalBorder(5, 5, Color.gray, Color.gray));
        return pnlDualScr;
    }

    private JPanel createMidCommonPanel() {
        JPanel pnlMidCommon = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"20", "20", "20", "20", "20","20"/*, "20","20","20","20"*/}, 2, 2));
        pnlMidCommon.setBorder(new OvalBorder(5, 5, Color.gray, Color.gray));

        chkAutoScroll = new JCheckBox(Language.getString("AUTO_SCROLL"));
        chkAutoScroll.addActionListener(this);
        if (Settings.isScrollingOn())
            chkAutoScroll.setSelected(true);
        chkArabicNumbers = new JCheckBox(Language.getString("ARABIC_NUMBERS"));
        chkArabicNumbers.addActionListener(this);
        chkShowTabs = new JCheckBox(Language.getString("SHOW_TAB_PANEL"));
        chkShowTabs.addActionListener(this);
        chkAlias = new JCheckBox(Language.getString("SHOW_ALIAS"));
        chkAlias.addActionListener(this);
        chkCombOrderBook = new JCheckBox(Language.getString("COMBINED_ORDER_BOOK"));
        chkCombOrderBook.addActionListener(this);
        chkPopupAnnouncements = new JCheckBox(Language.getString("POPUP_ANNOUNCE_WINDOW"));
        chkPopupAnnouncements.addActionListener(this);
        chkSaveTradesOnExit = new JCheckBox(Language.getString("SAVE_TRADES_ON_EXIT"));
        chkSaveTradesOnExit.addActionListener(this);
        chkShowMinMax = new JCheckBox(Language.getString("SHOW_MIN_MAX"));
        chkShowMinMax.addActionListener(this);
        chkShow52WeekHL = new JCheckBox(Language.getString("SHOW_52WK_HIGHLOW"));
        chkShow52WeekHL.addActionListener(this);
//        chkOpenInProChart = new JCheckBox(Language.getString("OPEN_GRAPH_IN_PRO_CHART"));
//        chkOpenInProChart.addActionListener(this);
        chkShowTradeAlertPopup = new JCheckBox(Language.getString("SHOW_TRADE_ALERT_POPUP"));
        chkShowTradeAlertPopup.setEnabled(TradingShared.isConnected());
        chkShowTradeAlertPopup.addActionListener(this);

        chkkeepOrderWindowOpen = new JCheckBox(Language.getString("KEEP_ORDER_WINDOW_OPEN"));
        chkkeepOrderWindowOpen.setEnabled(TradingShared.isConnected());
        chkkeepOrderWindowOpen.addActionListener(this);

        chkPutToSameLayer =  new JCheckBox(Language.getString("PUT_ALL_TO_SAME_LAYER"));
        chkPutToSameLayer.setSelected(Settings.isPutAllToSameLayer());
        chkPutToSameLayer.addActionListener(this);

        pnlMidCommon.add(chkAutoScroll);
        pnlMidCommon.add(chkArabicNumbers);
        pnlMidCommon.add(chkShowTabs);
        pnlMidCommon.add(chkAlias);
        pnlMidCommon.add(chkShowMinMax);
        pnlMidCommon.add(chkShow52WeekHL);
//        pnlMidCommon.add(chkOpenInProChart);
        if (TWControl.isCombinedorderBookEnable()) {
            pnlMidCommon.add(chkCombOrderBook);
        }
        if (TWControl.isAutoPopupAnnouncementMenuVisible()) {
            pnlMidCommon.add(chkPopupAnnouncements);
        }
        if (TWControl.isSaveTimeandSalesonExitEnable()) {
            pnlMidCommon.add(chkSaveTradesOnExit);
        }
            pnlMidCommon.add(chkShowTradeAlertPopup);
        pnlMidCommon.add(chkkeepOrderWindowOpen);
        pnlMidCommon.add(chkPutToSameLayer);
        
        return pnlMidCommon;
    }

    private JPanel createAutoScrollPanel() {
        JPanel pnlAutoScroll = new JPanel(new FlexGridLayout(new String[]{"150", "150", "100%"}, new String[]{"20"}, 2, 2));
        JLabel lblAutoScroll = new JLabel(Language.getString("AUTO_SCROLL_INTERVAL"));
        scrollingCombo = new TWComboBox(scrollDataArray);
        pnlAutoScroll.add(lblAutoScroll);
        pnlAutoScroll.add(scrollingCombo);
        pnlAutoScroll.add(new JLabel());
        pnlAutoScroll.setBorder(new OvalBorder(5, 5, Color.gray, Color.gray));
        return pnlAutoScroll;
    }

    public void initializeSettings() {
        try {
            chkAutoScroll.setSelected(Settings.isScrollingOn());
            autoScrollState = Settings.isScrollingOn();
            chkArabicNumbers.setSelected(Settings.isShowArabicNumbers());
            showArabicNumbers = Settings.isShowArabicNumbers();
            chkShowTabs.setSelected(Settings.isTabIndex());
            chkAlias.setSelected(Settings.isShowAlias());
            chkCombOrderBook.setSelected(Settings.isShowCombinedOrderBook());
            chkPopupAnnouncements.setSelected(Settings.isPopupAnnounceWindow());
            chkSaveTradesOnExit.setSelected(Settings.isSaveTimeNSales());

            boolean isMinMaxEnabled = Settings.getBooleanItem("MINMAX_ENABLE");
            
            if(!rbMinMaxHighlightRows.isEnabled()){
            rbMinMaxHighlightRows.setSelected(Settings.getBooleanItem("IS_MIN_MAX_HIGHLIGHT_ROWS"));
            }
            if(!rbMinMaxDisplayIcons.isEnabled()){
            rbMinMaxDisplayIcons.setSelected(!Settings.getBooleanItem("IS_MIN_MAX_DISPLAY_ICONS"));
            }


            chkShowMinMax.setSelected(isMinMaxEnabled);

            boolean is52WeekEnabled = Settings.getBooleanItem("52WEEK_HIGHLOW_ENABLE");
            rb52WeekHighlightRows.setSelected(Settings.getBooleanItem("IS_52_WEEK_HIGHLIGHT_ROWS"));
            rb52WeekDisplayIcons.setSelected(!Settings.getBooleanItem("IS_52_WEEK_HIGHLIGHT_ROWS"));
            chkShow52WeekHL.setSelected(is52WeekEnabled);

            chkShowTradeAlertPopup.setSelected(TradingShared.isShowOrderNotoficationPopup());
//            chkkeepOrderWindowOpen.setSelected(TradingShared.isKeepOrderWindowOpen());
            chkkeepOrderWindowOpen.setSelected(Settings.isKeepOrderWindowOpen());
            chkPutToSameLayer.setSelected(Settings.isPutAllToSameLayer());
            chkKeepOrderWindowConfirmationWindow.setSelected(Settings.getBooleanItem("SHOW_ORDER_WINDOW_CONFIRMATION_WINDOW", true));
            switch (Settings.autoScrollInterval) {
                case 10:
                    scrollingCombo.setSelectedIndex(0);
                    break;
                case 20:
                    scrollingCombo.setSelectedIndex(1);
                    break;
                case 50:
                    scrollingCombo.setSelectedIndex(2);
                    break;
                case 100:
                    scrollingCombo.setSelectedIndex(3);
                    break;
                case 150:
                    scrollingCombo.setSelectedIndex(4);
                    break;
            }
            //dual screen mode
            if (Settings.isMultiScreenEnvr()) {
                chkDualScr.setEnabled(true);
                chkDualScr.setSelected(Settings.isDualScreenMode());
                if (Settings.isLeftAlignPopups()) {
                    rbLeft.setEnabled(true);
                    rbLeft.setSelected(true);
                    rbRight.setEnabled(true);
                    rbRight.setSelected(false);
                } else {
                    rbRight.setEnabled(true);
                    rbRight.setSelected(true);
                    rbLeft.setEnabled(true);
                    rbLeft.setSelected(false);
                }
            } else {
                chkDualScr.setEnabled(false);
                rbLeft.setEnabled(false);
                chkDualScr.setSelected(Settings.isDualScreenMode());
                rbRight.setEnabled(false);
            }

            if (is52WeekEnabled) {
                rb52WeekHighlightRows.setEnabled(true);
                rb52WeekDisplayIcons.setEnabled(true);
            } else {
                rb52WeekHighlightRows.setEnabled(false);
                rb52WeekDisplayIcons.setEnabled(false);
            }

            if (isMinMaxEnabled) {
                rbMinMaxHighlightRows.setEnabled(true);
                rbMinMaxDisplayIcons.setEnabled(true);
            } else {
                rbMinMaxHighlightRows.setEnabled(false);
                rbMinMaxDisplayIcons.setEnabled(false);
            }

            createTimeZonesList();
            createLanguagesList();
            createNewsIndicatorExpiryTime();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == chkShow52WeekHL) {
            rb52WeekHighlightRows.setEnabled(chkShow52WeekHL.isSelected());
            rb52WeekDisplayIcons.setEnabled(chkShow52WeekHL.isSelected());
        }
        else if (e.getSource() == chkShowMinMax) {
            rbMinMaxHighlightRows.setEnabled(chkShowMinMax.isSelected());
            rbMinMaxDisplayIcons.setEnabled(chkShowMinMax.isSelected());
        } 
        else if (e.getSource() == btnOk) {
            okPressed();
        } else if (e.getSource() == btnCancel) {
            cancelPressed();
        }
    }

    private void startArabicNumberTread() {
        Thread threadNumChange = new Thread("Menus-changing_number_format_thread") {
            public void run() {
                new ShowMessage(UnicodeUtils.getNativeString(Language.getString("MSG_CHANGE_NUMBER_SYSTEM")), "I");
                Client.getInstance().showArabicNumbers(chkArabicNumbers.isSelected());
            }
        };
        threadNumChange.start();
    }

    private void okPressed() {
        try {
            saveSettings();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        //change time zone
        try {
            setTimeZone(((TWComboItem) cmbTimeZones.getSelectedItem()).getId());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (chkArabicNumbers.isSelected() != showArabicNumbers) {
            startArabicNumberTread();
        }
        //change language
        if (!((TWComboItem) cmbLanguages.getSelectedItem()).getId().equalsIgnoreCase(selectedLangId)) {
            try {
                switchLanguage(((TWComboItem) cmbLanguages.getSelectedItem()).getId());
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        this.setVisible(false);
    }

    private void cancelPressed() {
        this.setVisible(false);
    }

    private void saveSettings() {
        //******************************settings that are just saved without initial check*****************************
        //show Alias
        Client.getInstance().showAlias(chkAlias.isSelected());
        //show hide tabs
        Settings.setTabIndex(chkShowTabs.isSelected());
        Client.getInstance().addTabIndexpanel(chkShowTabs.isSelected());
        Menus.isShowTabIndex = chkShowTabs.isSelected();

        //auto scroll
        if (chkAutoScroll.isSelected() != autoScrollState) {
            Settings.setAutoScrolling();
        }
        //combined order book
        Settings.setShowCombinedOrderBook(chkCombOrderBook.isSelected());
        //popup announcements
        Settings.setPopupAnnounceWindow(chkPopupAnnouncements.isSelected());
        if (Client.getInstance().getMenus().getDoNotAutoPopupAnnouncements() != null) {
            Client.getInstance().getMenus().getDoNotAutoPopupAnnouncements().setSelected(!chkPopupAnnouncements.isSelected());
        }
//        Client.getInstance().getMenus().getDoNotAutoPopupAnnouncements().setSelected(!chkPopupAnnouncements.isSelected());
        //save time and sales
        Settings.setSaveTimeNSales(chkSaveTradesOnExit.isSelected());
        //show min max
        Settings.saveItem("MINMAX_ENABLE", "" + chkShowMinMax.isSelected());
        Menus.isShowMinMax = chkShowMinMax.isSelected();
        // show 52 wk HL
        Settings.saveItem("52WEEK_HIGHLOW_ENABLE", "" + chkShow52WeekHL.isSelected());
        Menus.isShow52WkhighLow = chkShow52WeekHL.isSelected();

        Settings.saveItem("IS_52_WEEK_HIGHLIGHT_ROWS", "" + rb52WeekHighlightRows.isSelected());
        Menus.is52WeekHighlightRows = rb52WeekHighlightRows.isSelected();

        Settings.saveItem("IS_MIN_MAX_HIGHLIGHT_ROWS", "" + rbMinMaxHighlightRows.isSelected());
        Menus.isMinMaxHighlightRows = rbMinMaxHighlightRows.isSelected();


        //show trade alert popup
        TradingShared.setShowOrderNotoficationPopup(chkShowTradeAlertPopup.isSelected());
        //save auto scroll interval
        saveAutoScrollInterval();
        saveNewsIndicatorExpiryTime();
        //dual screen stuff
        if (chkDualScr.isEnabled()) {
            rbLeft.setEnabled(chkDualScr.isSelected());
            rbRight.setEnabled(chkDualScr.isSelected());
            Settings.setDualScreenMode(chkDualScr.isSelected());
            Settings.setLeftAlignPopups(rbLeft.isSelected());
        }
        //order window open status
//        TradingShared.setKeepOrderWindowOpen(chkkeepOrderWindowOpen.isSelected());
        Settings.setKeepOrderWindowOpen(chkkeepOrderWindowOpen.isSelected());

        Settings.setPutAllToSameLayer(chkPutToSameLayer.isSelected());

        Settings.setItem("SHOW_ORDER_WINDOW_CONFIRMATION_WINDOW", "" + chkKeepOrderWindowConfirmationWindow.isSelected());

        Client.getInstance().putAllToSameLayerActionPerformed();

        //*************************************************************************************************************

    }

    private void saveNewsIndicatorExpiryTime(){
        int id = Integer.parseInt(((TWComboItem)cmbNewsIndicator.getSelectedItem()).getId());
        Settings.setNewsIndicatorExpiryTime(id);
    }

    private void saveAutoScrollInterval() {
        switch (scrollingCombo.getSelectedIndex()) {
            case 0:
                Settings.autoScrollInterval = 10;
                break;
            case 1:
                Settings.autoScrollInterval = 20;
                break;
            case 2:
                Settings.autoScrollInterval = 50;
                break;
            case 3:
                Settings.autoScrollInterval = 100;
                break;
            case 4:
                Settings.autoScrollInterval = 150;
                break;
        }

        Settings.setItem("AUTO_SCROLL_INTERVAL", Settings.autoScrollInterval + "");
    }

    private void createTimeZonesList() {
        lstTimeZones.clear();
        cmbTimeZones.removeActionListener(this);
        String activZone = Settings.getItem("CURRENT_TIME_ZONE");
        if (activZone.equalsIgnoreCase("NULL")) {
            activZone = null;
        }
        TWComboItem cmbItem = new TWComboItem("NULL", Language.getString("DEFAULT"));

        lstTimeZones.add(cmbItem);
        ArrayList<String> zoneIDs = TimeZoneMap.getInstance().getSortedZoneIds();
        for (String zoneID:zoneIDs) {
            TimeZoneMap.getInstance().getGMTTimeFormat(zoneID);
            TWComboItem cmbItem1 = new TWComboItem(zoneID, TimeZoneMap.getInstance().getZoneDescription(zoneID)+" " + TimeZoneMap.getInstance().getGMTTimeFormat(zoneID));
            lstTimeZones.add(cmbItem1);
        }
       
        zoneIDs = null;
        if (activZone == null) {
            cmbTimeZones.setSelectedIndex(0);
        } else {
            for (TWComboItem item : lstTimeZones) {
                if (item.getId().equalsIgnoreCase(activZone)) {
                    cmbTimeZones.setSelectedItem(item);
                    break;
                }
            }
        }
        cmbTimeZones.addActionListener(this);
    }

    private void createLanguagesList() {
        lstLanguages.clear();
        cmbLanguages.removeActionListener(this);
        String[] saLanguages;
        String[] saLanguageIDs;

        saLanguages = Language.getLanguageCaptions();
        saLanguageIDs = Language.getLanguageIDs();

        for (int i = 0; i < saLanguages.length; i++) {
            TWComboItem item = new TWComboItem(saLanguageIDs[i], saLanguages[i]);
            lstLanguages.add(item);
        }
        for (TWComboItem cmbItem : lstLanguages) {
            if (cmbItem.getId().equalsIgnoreCase(Language.getSelectedLanguage())) {
                cmbLanguages.setSelectedItem(cmbItem);
                selectedLangId = cmbItem.getId();
                break;
            }
        }
        cmbLanguages.addActionListener(this);
    }

    private void createNewsIndicatorExpiryTime(){
        lstNewsIndicator.clear();
        cmbNewsIndicator.removeActionListener(this);
        lstNewsIndicator.add(new TWComboItem(-1, Language.getString("NEWS_INDICATOR_NEVER")));
        lstNewsIndicator.add(new TWComboItem(10, Language.getString("NEWS_INDICATOR_10_MIN")));
        lstNewsIndicator.add(new TWComboItem(20, Language.getString("NEWS_INDICATOR_20_MIN")));
        lstNewsIndicator.add(new TWComboItem(30, Language.getString("NEWS_INDICATOR_30_MIN")));
        lstNewsIndicator.add(new TWComboItem(60, Language.getString("NEWS_INDICATOR_1_HOUR")));
        lstNewsIndicator.add(new TWComboItem(240, Language.getString("NEWS_INDICATOR_4_HOURS")));
        for (TWComboItem cmbItem : lstNewsIndicator) {
            if (cmbItem.getId().equals(""+Settings.getNewsIndicatorExpiryTime())) {
                cmbNewsIndicator.setSelectedItem(cmbItem);
                break;
            }
        }
        cmbNewsIndicator.addActionListener(this);
    }

    private void switchLanguage(String newID) {
        String[] saLanguageIDs = Language.getLanguageIDs();
        if (saLanguageIDs.length <= 2) {
            for (int i = 0; i < saLanguageIDs.length; i++) {
                if (!Language.getLanguageTag().equals(saLanguageIDs[i])) {
                    String result = getLanguageSwitchResponceType(saLanguageIDs[i]);
                    if ((result.equals("OK")) || (result.equals("SAVE"))) {
                        Settings.saveItem("LANGUAGE", saLanguageIDs[i]);
                        Settings.resetCurrentDay();
                    }

                    if (result.equals("SAVE")) {
                        Client.getInstance().exitApplication(true);
                    }
                    //new ShowMessage(UnicodeUtils.getNativeString(Language.getString("MSG_CHANGE_LANGUAGE")), "I");
                }
            }
        } else {
            String result = getLanguageSwitchResponceType(newID);
            if ((result.equals("OK")) || (result.equals("SAVE"))) {
                Settings.saveItem("LANGUAGE", newID);
                Settings.resetCurrentDay();
            }

            if (result.equals("SAVE")) {
                Client.getInstance().exitApplication(true);
            }
        }
    }

    private String getLanguageSwitchResponceType(String newLang) {
        final StringBuffer result = new StringBuffer();
        TWButton save = new TWButton("<HTML><DIV ALIGN\\=CENTER>" + Language.getStringForLanguage(Language.getLanguageTag(), "ALL_LANG_SAVE_N_EXIT") + "<BR>" + Language.getStringForLanguage(newLang, "ALL_LANG_SAVE_N_EXIT") + "</DIV>");
        TWButton ok = new TWButton("<HTML><DIV ALIGN\\=CENTER>" + Language.getStringForLanguage(Language.getLanguageTag(), "ALL_LANG_OK") + "<BR>" + Language.getStringForLanguage(newLang, "ALL_LANG_OK") + "</DIV>");
        TWButton cancel = new TWButton("<HTML><DIV ALIGN\\=CENTER>" + Language.getStringForLanguage(Language.getLanguageTag(), "ALL_LANG_CANCEL") + "<BR>" + Language.getStringForLanguage(newLang, "ALL_LANG_CANCEL") + "</DIV>");

        Object options[] = {ok, save, cancel};

        JOptionPane pane = new JOptionPane();
        pane.setMessage("<HTML><DIV ALIGN\\=CENTER>" + Language.getStringForLanguage(Language.getLanguageTag(), "MSG_CHANGE_LANGUAGE") + "<BR>" + Language.getStringForLanguage(newLang, "MSG_CHANGE_LANGUAGE") + "</DIV>");
        pane.setOptions(options);
        pane.setMessageType(JOptionPane.INFORMATION_MESSAGE);
        GUISettings.applyOrientation(pane);
        final JDialog exitDialog = pane.createDialog(Client.getInstance().getFrame(), Language.getString("INFORMATION"));
        exitDialog.setModal(true);

        ok.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
//                AnnouncementStore.getSharedInstance().clearAll();
                result.append("OK");
                exitDialog.dispose();
            }
        });

        save.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                AnnouncementStore.getSharedInstance().clearAll();
                result.append("SAVE");
                exitDialog.dispose();
            }
        });

        cancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                result.append("CANCEL");
                exitDialog.dispose();
            }
        });

        exitDialog.setVisible(true);

        return result.toString();
    }

    private void setTimeZone(String zoneId) {
        if (zoneId.equals("NULL")) {
            zoneId = null;
        }
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            exchange.setActiveTimeZone(zoneId);
            exchange = null;
        }
        exchanges = null;

        if (zoneId == null) {
            Settings.saveItem("CURRENT_TIME_ZONE", "NULL");
        } else {
            Settings.saveItem("CURRENT_TIME_ZONE", zoneId);
        }
    }

    @Override
    public void setVisible(boolean value) {
        if (value) {
            try {
                initializeSettings();
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        super.setVisible(value);
    }

    public void tradeServerConnected() {
        if(chkShowTradeAlertPopup != null){
            chkShowTradeAlertPopup.setEnabled(true);
        }
        if(chkkeepOrderWindowOpen != null){
            chkkeepOrderWindowOpen.setEnabled(true);
        }
        if(chkKeepOrderWindowConfirmationWindow != null){
            chkKeepOrderWindowConfirmationWindow.setEnabled(true);
        }
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        if(chkShowTradeAlertPopup != null){
            chkShowTradeAlertPopup.setEnabled(false);
        }
        if(chkkeepOrderWindowOpen != null){
            chkkeepOrderWindowOpen.setEnabled(false);
        }
        if(chkKeepOrderWindowConfirmationWindow != null){
            chkKeepOrderWindowConfirmationWindow.setEnabled(false);
        }
    }
}
