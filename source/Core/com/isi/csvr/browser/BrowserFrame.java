// Copyright (c) 2000 Home
package com.isi.csvr.browser;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.print.PrintManager;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.table.Table;
import com.mubasher.outlook.Reminder;
import com.mubasher.outlook.ReminderUI;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.event.CaretListener;
import javax.swing.event.CaretEvent;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.beans.PropertyVetoException;


/**
 * A subclass of JInternalFrame.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class BrowserFrame extends JInternalFrame
        implements WindowWrapper, HyperlinkListener, ActionListener, Themeable, ClipboardOwner, TWDesktopInterface, MouseListener {
    private static JEditorPane browserPane;
    private ViewSetting g_oViewSettings;
    private JPopupMenu popupMenu;
    private Clipboard clip;
    private JPanel bottomPanel;
    private JLabel botomLabel;
    private String moreInforText = null;
    private int selStart = 1;
    private int selEnd = 1;
    private String newsTitle="";
    private String body="";
//    private
    //private Announcement ann;
    //public static boolean isAnnouncement =false;
    //---------------------------------------------------------------------------------------------------------------------------------------------------------
    public static int showType;

    /**
     * Constructs a new instance.
     */
    public BrowserFrame() {
        super();
        try {
            clip = getToolkit().getSystemClipboard();
            jbInit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void show() {
        throw new RuntimeException("Do not call this method");
    }

    public void show(int type) {
        if (isIcon) {
            try {
                setIcon(false);
            } catch (PropertyVetoException e) {
            }
        }
        showType = type;
        if ((showType == Constants.DOC_TYPE_ANNOUNCEMENT) && (moreInforText != null)) {//&& (moreInforText!=null)
            bottomPanel.setVisible(true);
        } else {
            bottomPanel.setVisible(false);
        }
        //ann= announcement;
        super.show();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void bringToFont() {
        try {
            setSelected(true);
        } catch (PropertyVetoException e) {
        }
    }
//---------------------------------------------------------------------------------------------------------------------------------------------------------

    /**
     * Sets the view settings object for this table
     */
    public void setViewSettings(ViewSetting oViewSettings) {
        g_oViewSettings = oViewSettings;
        g_oViewSettings.setParent(this);
        try {
            g_oViewSettings.setLayer(this.getLayer());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        applySettings();
    }

    @Override
    public void setLayer(int layer) {
        try {
            if((g_oViewSettings.getLayer() == -1))
                g_oViewSettings.setLayer(layer);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        super.setLayer(layer);    //To change body of overridden methods use File | Settings | File Templates.
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {
        this.getContentPane().setLayout(new BorderLayout());
        browserPane = new JEditorPane();
        browserPane.setEditable(false);
        browserPane.setContentType("text/html");
        browserPane.addHyperlinkListener(this);
//        browserPane = new BrowserPane(this);
        JScrollPane scrollPaneBrowser = new JScrollPane(browserPane);
        this.getContentPane().add(scrollPaneBrowser, BorderLayout.CENTER);
//        browserPane.loadTheme();
        //this.setTitle(Language.getString("BROWSER"));
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
        bottomPanel = new JPanel();
        bottomPanel.setLayout(new FlowLayout(FlowLayout.LEADING));
        bottomPanel.setSize(this.getWidth(), 20);
        botomLabel = new JLabel(Language.getString("FOR_MORE_INFO"));
        botomLabel.setForeground(Theme.getColor("BROWSER_BOTTOM_HYPERLINK_FG_COLOR"));
        botomLabel.addMouseListener(this);
//        botomLabel.setBackground(Color.WHITE);
        bottomPanel.add(botomLabel);
        bottomPanel.setOpaque(true);
        bottomPanel.setBackground(Theme.getColor("BROWSER_BOTTOM_HYPERLINK_BG_COLOR"));
//        botomLabel.
        this.getContentPane().add(bottomPanel, BorderLayout.SOUTH);
        popupMenu = new JPopupMenu();
        popupMenu.setBackground(Color.GRAY);
        JMenuItem menuPrint = new JMenuItem(Language.getString("PRINT"));
        menuPrint.setForeground(Color.BLACK);
        menuPrint.setActionCommand("PRINT");
        popupMenu.add(menuPrint);
        menuPrint.addActionListener(this);
        JMenuItem menuCopy = new JMenuItem(Language.getString("COPY"));
        menuCopy.setForeground(Color.BLACK);
        menuCopy.setActionCommand("COPY");
        menuCopy.addActionListener(this);
        popupMenu.add(menuCopy);
        browserPane.setComponentPopupMenu(popupMenu);
        Theme.registerComponent(this);


    }
//-------------------------------------------------------------------------------------------------------------------------------------------------------------------

    public void mouseClicked(MouseEvent e) {
        try {
            Runtime.getRuntime().exec("start " + moreInforText);
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
        this.setCursor(new Cursor(Cursor.HAND_CURSOR));
    }

    public void mouseExited(MouseEvent e) {
        this.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
    }

    public void applyTheme() {
        botomLabel.setForeground(Theme.getColor("BROWSER_BOTTOM_HYPERLINK_FG_COLOR"));
        bottomPanel.setBackground(Theme.getColor("BROWSER_BOTTOM_HYPERLINK_BG_COLOR"));
        SwingUtilities.updateComponentTreeUI(this);
        SwingUtilities.updateComponentTreeUI(popupMenu);
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme"+Theme.getID()+"/InternalIcon.gif")));
    }

    public void setFont(Font font) {
        browserPane.setFont(font);
    }

    public void setContentType(String type) {
        browserPane.setContentType(type);
    }

    public void showLoading(int type) {
        try {
            setContentType("text/html");
            int count;
            byte[] temp = new byte[1000];
            StringBuffer buffer = new StringBuffer();
            InputStream in = new FileInputStream(".\\Templates\\loading_" + Language.getLanguageTag() + ".htm");
            while (true) {
                count = in.read(temp);
                if (count == -1) break;
                if (count > 0) {
                    String str = new String(temp, 0, count);
                    buffer.append(str);
                    str = null;
                }
            }
            String template = buffer.toString();
            template = template.replaceAll("\\[PATH\\]", ((String) System.getProperties().get("user.dir")).replaceAll("\\\\", "/"));
//            System.out.println(template);
            setText(template);
            show(type);
            bringToFont();
            template = null;
            temp = null;
            buffer = null;
            requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String toString() {
        return this.getTitle();
    }

    public void setURL(String url) {
        try {

            browserPane.setPage(url);
            browserPane.updateUI();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void setText(String text) {
        try {
            browserPane.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
            browserPane.setText(text);
            browserPane.updateUI();
//            browserPane.repaint();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setMoreInfoURL(String text) {
        moreInforText = text;
    }

    public void setWIndowTitle(String title) {
        super.setTitle(title);
    }

    public void actionPerformed(ActionEvent e) {
        SharedMethods.printLine("BrowserFrame:actionPerformed ", false);
        if (e.getActionCommand().equals("PRINT")) {
            Object[] printable = new Object[1];
            printable[0] = browserPane;

            SharedMethods.printTable(printable, PrintManager.PRINT_JEDITORPANE, "");
        } else if (e.getActionCommand().equals("COPY")) {
            try {
                String selected = browserPane.getSelectedText();
                if (selected == null || selected.isEmpty()) {
                    StringSelection text = new StringSelection(browserPane.getDocument().getText(0, browserPane.getDocument().getLength()));
                    clip.setContents(text, this);
                } else {
                    StringSelection text = new StringSelection(selected);
                    clip.setContents(text, this);
                }

            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }
    }

    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
//            JEditorPane pane = (JEditorPane) e.getSource();
            if (e instanceof HTMLFrameHyperlinkEvent) {
                /*HTMLFrameHyperlinkEvent evt = (HTMLFrameHyperlinkEvent) e;
                HTMLDocument doc = (HTMLDocument) pane.getDocument();
                doc.processHTMLFrameHyperlinkEvent(evt);*/
            } else {
                if (e.getDescription().startsWith("--BODY")) { // announcement body request
                    String url = e.getDescription();
                    String[] data = url.split("/");
                    SendQFactory.addAnnouncementBodyRequest(data[1], data[2], data[3]);
                    showLoading(Constants.DOC_TYPE_ANNOUNCEMENT);
                } else if (e.getDescription().startsWith("--TRANSLATE")) { // announcement body request
                    String url = e.getDescription();
                    String[] data = url.split("/");
                    String id = data[1];
                    String from = data[2];
                    String to = data[3];
                    if (showType == Constants.DOC_TYPE_ANNOUNCEMENT){
                        SharedMethods.translateAnnouncement(id, from, to);
                    } else if (showType == Constants.DOC_TYPE_NEWS){
                        SharedMethods.translateNews(id, from, to);
                    }
                } else if (e.getDescription().endsWith("[OUTLOOK]")) {
                    if (Reminder.checkAvailability()) {
                        ReminderUI rUI = new ReminderUI(Client.getInstance().getFrame());
                        rUI.showDiaog(newsTitle, Language.getString("LOCATION_UNKNOWN"), body);
                    } else {
                        new ShowMessage(Language.getString("MSG_NO_OUTLOOK_SUPPORT"), "E");
                    }
                } else {
                    try {
                        Runtime.getRuntime().exec("start " + e.getURL().toString());
                    } catch (IOException e1) {
                        e1.printStackTrace();
                    }
                }
                /*try {
                    pane.setPage(e.getURL());
                } catch (Throwable t) {
                    t.printStackTrace();
                }*/
            }
        }

    }

    /* Table wrapper methods */
    public void applySettings() {
        this.setLocation(g_oViewSettings.getLocation());
        this.setSize(g_oViewSettings.getSize());
        if (g_oViewSettings.isVisible()) {
            if (Settings.isPutAllToSameLayer() && (getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, g_oViewSettings.getIndex());
            } else {
                this.getDesktopPane().setLayer(this, g_oViewSettings.getLayer(), g_oViewSettings.getIndex());
            }
            this.setVisible(true);
            try {
                this.setSelected(true);
            } catch (Exception e) {
            }
        } else {
//            this.getDesktopPane().setLayer(this, this.getLayer(), 0);
            if (Settings.isPutAllToSameLayer() && (getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
            } else {
                this.getDesktopPane().setLayer(this, g_oViewSettings.getLayer(), 0);
            }
        }

        try {
            switch (g_oViewSettings.getStyle()) {
                case ViewSettingsManager.STYLE_ICONIFIED:
                    this.setIcon(true);
                    break;
                case ViewSettingsManager.STYLE_MAXIMIZED:
                    this.setMaximum(true);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public ViewSetting getViewSetting(){
        return g_oViewSettings;
    }

    public boolean isWindowVisible() {
        return isVisible();
    }

    public int getWindowStyle() {
        if (isMaximum())
            return ViewSettingsManager.STYLE_MAXIMIZED;
        else if (isIcon())
            return ViewSettingsManager.STYLE_ICONIFIED;
        else
            return ViewSettingsManager.STYLE_NORMAL;
    }

    public JTable getTable1() {
        return null;
    }

    public JTable getTable2() {
        return null;
    }

    public JTable getTabFoler(String id) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getZOrder() {
        return getDesktopPane().getPosition(this);
    }


    public void printTable() {
    }

    public boolean isTitleVisible() {
        return true;
    }

    public void setTitleVisible(boolean status) {
    }

    public void windowClosing() {
        Theme.unRegisterComponent(this);
    }

    public Object getDetachedFrame() {
        return null;
    }

    public boolean isDetached() {
        return false;
    }
    /* -- */

    public void lostOwnership(Clipboard clipboard, Transferable contents) {

    }

    public void setDesktopIndex(int index) {
    }

    public int getDesktopIndex() {
        return 0;
    }

    public void closeWindow() {
    }

    public int getDesktopItemType() {
        return 0;
    }

    public void setWindowType(int type) {
    }

    public int getWindowType() {
        return 0;
    }

    public void setBody(String body) {
        this.body = body;
    }

    public void setNewsTitle(String newsTitle) {
        this.newsTitle = newsTitle;
    }
}

