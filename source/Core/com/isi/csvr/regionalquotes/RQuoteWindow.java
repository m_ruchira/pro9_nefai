package com.isi.csvr.regionalquotes;

import com.isi.csvr.*;
import com.isi.csvr.communication.tcp.InternationalConnector;
import com.isi.csvr.table.*;
import com.isi.csvr.datastore.*;
import com.isi.csvr.iframe.*;
import com.isi.csvr.properties.*;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.border.EtchedBorder;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.*;
import java.util.Collections;
import java.util.Arrays;
import java.util.Comparator;

public class RQuoteWindow extends JPanel implements Themeable,
                                    ComponentListener, ActionListener,
                                    DropTargetListener, MouseListener, WindowWrapper {

    private final int PANEL_WIDTH   = 800; //400; //350;
    private final int PANEL_HEIGHT  = 400; //250; //160;

    private boolean     isActive         = true;
    private Thread      g_oThread;
    private String      sKey             = null;
    private String      baseSymbol       = null;

    private ViewSettingsManager vsManager;
    private ViewSetting         oRQuoteViewSetting;
    private RegionalQuoteModel rQuoteModel;
    private InternalFrame       parent;
//    private CommonTableRenderer renderer;

    private JTextField  txtSymbol       = new JTextField();
	private TWMenu workSpacePopup  = new TWMenu();         // PopupMenu for the Workspaces
    private JLabel      lblDescr        = new JLabel();
    private Table       rQuoteTable;

    private Color bgColor;
    private static int population = 0;
    private String symbol = null;

    public RQuoteWindow(String sKey, ViewSettingsManager vsManagerIn) {
        vsManager = vsManagerIn;
        Theme.registerComponent(this);
        this.sKey  = sKey;
        baseSymbol = sKey;
        createLayout();
    }

    private void createLayout() {
        applyTheme();
        this.addComponentListener(this);
        this.setLayout(new BorderLayout());
        this.setBackground(bgColor); //Theme.getColor("BACKGROUND_COLOR"));

        // #####################################################################
        //      Create North Panel
        // #####################################################################
        JPanel configPanel = new JPanel();
        configPanel.setLayout(new FlowLayout(SwingConstants.CENTER , 5, 2));
        //configPanel.setBackground(Color.red);
        JLabel lbAddSymbol = new JLabel();
        lbAddSymbol.setFont(new java.awt.Font("Dialog", Font.BOLD, 10));
        lbAddSymbol.setText(Language.getString("POPUP_ADD_SYMBOLS"));

        lblDescr.setFont(new java.awt.Font("Dialog", Font.BOLD, 12));
        lblDescr.setHorizontalAlignment(SwingConstants.CENTER);
        lblDescr.setBorder(new EtchedBorder(EtchedBorder.RAISED));

        lbAddSymbol.setPreferredSize(new Dimension(70, 18));   // 275
        txtSymbol.setPreferredSize(new Dimension(70, 18));   // 275

        configPanel.add(lbAddSymbol);
        configPanel.add(txtSymbol);
        configPanel.add(lblDescr);

        // #####################################################################
        //      Create the Center Panel
        // #####################################################################
        rQuoteTable = new Table();

        try {
            // Create the Calls Model & VeiwSetting
            Stock stock = DataStore.getSharedInstance().getStockObject(sKey);
            if((stock != null) && (stock.getMarketCenter() != null)){
                String symbol = SharedMethods.getSymbolFromKey(sKey);
                if(symbol.endsWith("."+stock.getMarketCenter())){
                    sKey = SharedMethods.getKey(SharedMethods.getExchangeFromKey(sKey),symbol.substring(0, symbol.lastIndexOf("."+stock.getMarketCenter())),SharedMethods.getInstrumentTypeFromKey(sKey));
                }
            }
            rQuoteModel = new RegionalQuoteModel(sKey);
//            rQuoteModel.setDataStore(DataStore.getSharedInstance().getRegQuoteStore(sKey));
            oRQuoteViewSetting = vsManager.getRQuotesView(ViewSettingsManager.REGIONAL_QUOTE_VIEW + "|" + baseSymbol);
            if (oRQuoteViewSetting == null) {
                oRQuoteViewSetting = vsManager.getSymbolView("REGIONAL_QUOTES");
                oRQuoteViewSetting = oRQuoteViewSetting.getObject();
                oRQuoteViewSetting.setID(baseSymbol);
                GUISettings.setColumnSettings(oRQuoteViewSetting, TWColumnSettings.getItem("REGIONAL_QUOTE"));
                //todo added by Dilum
                SharedMethods.applyCustomViewSetting(oRQuoteViewSetting);
                DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oRQuoteViewSetting.getType(),oRQuoteViewSetting);
                vsManager.putRQuotesView(ViewSettingsManager.REGIONAL_QUOTE_VIEW + "|" +
                                        baseSymbol, oRQuoteViewSetting);
            }
            oRQuoteViewSetting.setParent(parent);
            rQuoteModel.setViewSettings(oRQuoteViewSetting);
            rQuoteTable.setModel(rQuoteModel);
			rQuoteTable.getTable().addMouseListener(this);
            rQuoteModel.setTable(rQuoteTable); //, renderer);  // Add the renderer...
            rQuoteModel.applyColumnSettings();

        } catch(Exception e) {
        	e.printStackTrace();
        }

        //this.add(configPanel, BorderLayout.NORTH);
        this.add(rQuoteTable, BorderLayout.CENTER);
        rQuoteTable.setSortingEnabled();

    	try {
            int tableWidth = (oRQuoteViewSetting.getSize().width-10);
            resizeTables(tableWidth, PANEL_HEIGHT);
        } catch(Exception e) {
            e.printStackTrace();
        }
        incPopulation();
        String sym=SharedMethods.getSymbolFromKey(baseSymbol);
        String ex=SharedMethods.getExchangeFromKey(baseSymbol);
        int instrumentType=SharedMethods.getInstrumentTypeFromKey(baseSymbol);
//        InternationalConnector.getSharedInstance().addAddRequest(ex,sym,Meta.REGIONALQUOTE);
        DataStore.getSharedInstance().addSymbolRequest(ex, sym, instrumentType, Meta.MESSAGE_TYPE_REGIONAL_QUOTE);
    }

    private void createWSPopUp() {

        try {
            String      wsName       = null;
            JMenuItem   wsItemMenu  = null;
            Object[] windowList = ((TWDesktop)Client.getInstance().getDesktop()).getWindows();
            if (windowList == null)
                return;
            ClientTable nodeObject  = null;
            ViewSetting oSettings   = null;

            workSpacePopup.setText(Language.getString("POPUP_ADD_TO_MAINBOARD"));
/*
            for (int i = 0; i < windowList.length; i++) {
                JInternalFrame frame = (JInternalFrame) windowList[i];
                if (!(frame instanceof ClientTable)) {
                    continue;
                }
                wsName      = frame.getTitle();
                oSettings   = ((ClientTable)frame).getViewSettings();
                wsItemMenu = new JMenuItem(wsName);
                wsItemMenu.addActionListener(this);
                wsItemMenu.setActionCommand("0" + oSettings.getID());

                workSpacePopup.add(wsItemMenu);
            }*/
            

//            Collections.sort(windowList, new  WindowComparator());
            Arrays.sort(windowList, new  WindowComparator());
            for (int i = 0; i < windowList.length; i++) {
                try {
                    JInternalFrame frame = (JInternalFrame) windowList[i];
                    if (!(frame instanceof ClientTable)) {
                        continue;
                    }
                    wsName = frame.getTitle();
                    oSettings = ((ClientTable) frame).getViewSettings();
                    if(oSettings.isCutomType()){
                        wsItemMenu = new TWMenuItem(wsName);
                        wsItemMenu.addActionListener(this);
                        wsItemMenu.setActionCommand(oSettings.getID());
                        workSpacePopup.add(wsItemMenu);
                    }
                    frame = null;

                } catch (Exception ex) {
                }
            }
            windowList = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//    private void validateSymbol(String symbol) {
//        String symbolOld = sKey.substring(sKey.indexOf(ESPConstants.KEY_SEPERATOR_CHARACTER)+1, sKey.length());
//        if ((symbol.length() > 0) && (!symbolOld.equals(symbol))) {
//            txtSymbol.setText("");
//            SymbolValidator.validateSymbol(false, ESPConstants.IndRegionalQuoteWindow, Meta.QUOTE, symbol, this);
//        }
//        symbolOld = null;
//    }

    private void resizeTables(int width, int height) {
        lblDescr.setPreferredSize(new Dimension(width-160, 18));
        lblDescr.updateUI();
    }

    /**
    * Returns the view setting object for this table
    */
    public ViewSetting getViewSettings() {
    	return oRQuoteViewSetting;
    }

    public Table getTable(){
        return rQuoteTable;
    }

    /**
    * Returns the Window Type for the window
    */
    public byte getWindowType() {
        return ViewSettingsManager.REGIONAL_QUOTE_VIEW;
    }

    public void setParent(InternalFrame parentIn,String caption) {
        //System.out.println("Set Parent");
        parent = parentIn;
        //parent.setTitle(Language.getString("WINDOW_TITLE_OPTIONS"));
        oRQuoteViewSetting.setParent(parent);
        parent.setTitle(Language.getString(oRQuoteViewSetting.getCaptionID()) + " " + caption); // + Language.getString("GDR_MESSAGE"));
        //parent.setTable2(putTable);
        parent.setTable(rQuoteTable);

        createWSPopUp();
        rQuoteTable.getPopup().setMenu(workSpacePopup);
//        rQuoteTable.getPopup().setPrintTarget(rQuoteTable.getTable());
    }

    public void killThread(){
        isActive = false;
        Theme.unRegisterComponent(this);
    }

    public String getCurrentSymbol() {
        return baseSymbol;
    }

    public void connectionEstablished(){};

    /**
    * Sleep the thread for a give time period
    */
    private void Sleep(long lDelay){
    	try {
        	g_oThread.sleep(lDelay);
        } catch(Exception e) {
        }
    }

    public void applyTheme() {
        bgColor         = Theme.getColor("BACKGROUND_COLOR");
//        fgColor         = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
//        selectedBGColor = Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR");
//        selectedFGColor = Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR");

        try {

            rQuoteTable.getTable().setBackground(bgColor);

            //renderer.reload();
        } catch (Exception e) {
        }
        updateUI();
    }

    protected void finalize() throws Throwable{
        //System.out.println("Finalysing DQuote ");
        //Theme.unRegisterComponent(this);
    }

    public void setSymbol(String sKeyIn) {
        String symbol = null;
		String exchange = null;
		String[] keyValues;
        //System.out.println("Set Regional Quote symbol " + sKey);
        //todo
//        if (sKey != null)
//            RQuoteStore.removeRQuote(sKey);
//        sKey = sKeyIn;
//        RQuoteStore.requestRQuote(sKey);
//        rQuoteModel.setDataStore(RQuoteStore.getRQuotesForSymbol(sKey));

        lblDescr.setText(DataStore.getSharedInstance().getCompanyName(baseSymbol));
//		keyValues = sKeyIn.split(Constants.KEY_SEPERATOR_CHARACTER);
		exchange = SharedMethods.getExchangeFromKey(sKeyIn);
        symbol =SharedMethods.getSymbolFromKey(sKeyIn);//sKey.substring(sKey.indexOf(ESPConstants.KEY_SEPERATOR_CHARACTER)+1, sKey.length());
        //parent.setTitle(Language.getString(oRQuoteViewSetting.getCaptionID()) + " " + symbol +" - " + exchange);
        txtSymbol.setText(symbol);
        symbol = null;
		keyValues = null;
		exchange = null;
        lblDescr.updateUI();
    }

	public void actionPerformed(ActionEvent e) {
       String  command = e.getActionCommand();
        if(this.symbol == null){
            return;
        }
//        Client.getInstance().addSymbolToWatchList(symbol, command, Meta.MESSAGE_TYPE_REGIONAL_QUOTE);
        Client.getInstance().addSymbolToWatchList(symbol, command, SharedMethods.getInstrumentTypeFromKey(symbol));
    }

    // MouseListener Interface Methods
    public void mouseClicked(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e){
        Point location = e.getPoint();
        int row = rQuoteTable.getTable().rowAtPoint(location);
        symbol = SharedMethods.getKey (((String)rQuoteTable.getTable().getModel().getValueAt(row, 1)),((String)rQuoteTable.getTable().getModel().getValueAt(row, 0)), Integer.parseInt((String)rQuoteTable.getTable().getModel().getValueAt(row, -1)));
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    // ComponentListener Methods
    public void componentResized(ComponentEvent e) {
        int newWidth = this.getBounds().width;
        resizeTables(newWidth, this.getBounds().getBounds().height);
    }

    public void componentMoved(ComponentEvent e) {
    }

    public void componentShown(ComponentEvent e) {
    }

    public void componentHidden(ComponentEvent e) {
    }

    // Drag and Drop Listeners
    public void dragEnter(DropTargetDragEvent dtde){
    }

    public void dragOver(DropTargetDragEvent dtde){
    }

    public void dropActionChanged(DropTargetDragEvent dtde){
    }

    public void dragExit(DropTargetEvent dte){
    }

    public void drop (DropTargetDropEvent event) {
        String key = null;
        // Check if the drag came form prit button
        try {
            key = (String)event.getTransferable().getTransferData(DataFlavor.stringFlavor);
            if (!key.equals("Print")) {
                //System.out.println("Dropped object accepted! " + key);
                event.getDropTargetContext().dropComplete(true);
                if (key != null)
                    this.setSymbol(key);
            } else {
                event.rejectDrop(); // not from the print button. reject drop
                //System.out.println("Dropped object rejected! " + key);
                return;
            }
        } catch(Exception e) {
            e.printStackTrace();
            event.rejectDrop();
            return;
        }
        key = null;
    }

    public synchronized static int getPopulation() {
        return population;
    }

    public synchronized static void incPopulation() {
        RQuoteWindow.population++;
    }

    public synchronized static void decPopulation() {
        RQuoteWindow.population--;
    }

    public int getWindowStyle() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getZOrder() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isWindowVisible() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public JTable getTable1() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public JTable getTable2() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public JTable getTabFoler(String id) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applySettings() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void printTable() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isTitleVisible() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setTitleVisible(boolean status) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowClosing() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isDetached() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object getDetachedFrame() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
    class WindowComparator implements Comparator{

        public int compare(Object o1, Object o2) {
            JInternalFrame frame1 = (JInternalFrame) o1;
            JInternalFrame frame2 = (JInternalFrame) o2;
            return frame1.getTitle().compareTo(frame2.getTitle());  //To change body of implemented methods use File | Settings | File Templates.
        }
    }
}