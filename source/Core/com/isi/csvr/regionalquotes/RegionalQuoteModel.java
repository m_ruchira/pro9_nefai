// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.regionalquotes;

/**
 * A Class class.
 * <P>
 * @author Bandula Priyadarshana
 */

import com.isi.csvr.shared.*;
import com.isi.csvr.table.*;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.*;

public class RegionalQuoteModel extends CommonTable implements TableModel , CommonTableInterface {

    private ArrayList<String> g_oList;
    private Stock   g_oRQuote;
    String key;
	/**
	* Constructor
	*/
	public RegionalQuoteModel(String key) {
        this.key=key;
        g_oList= DataStore.getSharedInstance().getRegQuoteStore(key);
    }

    protected void finalize() throws Throwable {
        g_oList = null;
    }

    public void setDataStore(ArrayList oQueue) {
		g_oList= DataStore.getSharedInstance().getRegQuoteStore(this.key);
    }

    /* --- Table Model's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {

        if (g_oList != null)
    		return g_oList.size();
        else
        	return 0;
    }

    public Object getValueAt(int iRow, int iCol) {

        g_oRQuote = null;
        g_oRQuote = DataStore.getSharedInstance().getStockObject(g_oList.get(iRow));

        switch(iCol) {
            case -1:
                return ""+ g_oRQuote.getInstrumentType();
            case 0:
                return g_oRQuote.getSymbol();
            case 1:
//                return g_oRQuote.getExchange();
                return ExchangeStore.getSharedInstance().getExchange(g_oRQuote.getExchange().trim()).getDisplayExchange(); // To display exchange
            case 2:
//                return g_oRQuote.getMarketCenter();
                return Settings.getMarketCenter(g_oRQuote.getMarketCenter(),0);
			case 3:
//                return g_oRQuote.getMarketCenter();
                return Settings.getMarketCenter(g_oRQuote.getMarketCenter(),1);
            case 4:
                return "" + g_oRQuote.getLastTradeValue();
//                return g_oRQuote.getLastTrade();
            case 5:
                return "" + g_oRQuote.getTradeQuantity();
            case 6:
                return "" + g_oRQuote.getLastTradeTime();
            case 7:
                return "" + g_oRQuote.getChange();
            case 8:
                return "" + g_oRQuote.getBestBidPrice();
            case 9:
                return "" + g_oRQuote.getBestBidQuantity();
            case 10:
                return "" + g_oRQuote.getBestAskPrice();
            case 11:
                return "" + g_oRQuote.getBestAskQuantity();
            case 12:
//                return "" + g_oRQuote.getLastTradeValue();
                return "" + g_oRQuote.getCurrentPrice();
            case 13:
                return "" + g_oRQuote.getVolume();
            case 14:
                return "" + g_oRQuote.getPreviousClosed();
            case 15:
                return "" + g_oRQuote.getTodaysClose();
            case 16:
				return "" + g_oRQuote.getTodaysOpen();
			case 17:
				return "" + g_oRQuote.getHigh();
			case 18:
				return "" + g_oRQuote.getLow();
			case 19:
				return "" + g_oRQuote.getOpenInterest();
            default:
                return "";
        }
    }


    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getColumnName(int iCol) {
	    return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        try  {

            return String.class;
        } catch (Exception ex)  {
            return Object.class;
            //return Number.class;
        }
        //return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
    	return false;
    }

    public void setValueAt(Object aValue, int rowIndex,int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

}
