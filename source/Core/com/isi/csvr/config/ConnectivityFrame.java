package com.isi.csvr.config;


import info.clearthought.layout.TableLayout;
import info.clearthought.layout.TableLayoutConstraints;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.*;

import com.isi.csvr.shared.*;
/*
 * Created by JFormDesigner on Thu Nov 26 11:50:24 IST 2009
 */


/**
 * @author Uditha Nagahawatta
 * @Modified by Chandika Hewage
 */
public class ConnectivityFrame extends JDialog{
    public ConnectivityFrame(Frame owner) {
        super(owner);
        initComponents();
    }

    public ConnectivityFrame(Frame owner, String title, boolean model) {
        super(owner, title, model);
        try {
            initComponents();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    private void thisWindowClosing(WindowEvent e) {

    }


    private void initComponents() {
        // Generated using JFormDesigner Evaluation license - Uditha N
        panel1 = new JPanel();
        label1 = new JLabel();
        txtMarketServers = new JTextField();
        label2 = new JLabel();
        label2.setEnabled(TWControl.getSSOType() == TWControl.SSO_TYPES.Direct);//todo: change made for disabling broker settings for price only versions
        txtBrokerSerevrs = new JTextField();
        txtBrokerSerevrs.setEnabled(TWControl.getSSOType() == TWControl.SSO_TYPES.Direct);//todo: change made for disabling broker settings for price only versions
        label3 = new JLabel();
        txtUserName = new JTextField();
        label4 = new JLabel();
        txtPassword = new TWPasswordField(this, false, false);
        panel2 = new JPanel();
        chkSocks = new JCheckBox();
        label5 = new JLabel();
        txtSocksIP = new JTextField();
        txtSocksIP.setEnabled(false);
        txtSocksPort = new JTextField();
        txtSocksPort.setDocument(txtSoPxyFormatter);
        txtSocksPort.setEnabled(false);
        chkWeb = new JCheckBox();
        label6 = new JLabel();
        txtWebIP = new JTextField();
        txtWebIP.setEnabled(false);
        txtWebPort = new JTextField();
        txtWebPort.setDocument(txtWePxyFormatter);
        txtWebPort.setEnabled(false);
        panel3 = new JPanel();
        btnOk = new JButton();
        btnCancel = new JButton();
        txtPassword.setUsernameFiled(txtUserName);
        //======== this ========
        setSize(300, 285);
        setTitle(Language.getString("CONFIGURE"));
        addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                thisWindowClosing(e);
            }
        });
        Container contentPane = getContentPane();
        contentPane.setLayout(new TableLayout(new double[][]{{1, TableLayout.FILL, 0}, {TableLayout.PREFERRED, TableLayout.PREFERRED, TableLayout.PREFERRED}}));
        ((TableLayout) contentPane.getLayout()).setVGap(5);
        //======== panel1 ========
        {
            panel1.setBorder(new TitledBorder(null, Language.getString("CONNECTIVITY"), TitledBorder.LEADING, TitledBorder.TOP));
            panel1.setLayout(new TableLayout(new double[][]{{5, 100, 5, TableLayout.FILL, 2}, {0, 23, 5, 23, 5, 23, 5, 23, 2}}));

            //---- label1 ----
            label1.setText(Language.getString("MARKET_DATA"));
            panel1.add(label1, new TableLayoutConstraints(1, 1, 1, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
            panel1.add(txtMarketServers, new TableLayoutConstraints(3, 1, 3, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

            //---- label2 ----
            label2.setText(Language.getString("BROKER"));
            panel1.add(label2, new TableLayoutConstraints(1, 3, 1, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
            panel1.add(txtBrokerSerevrs, new TableLayoutConstraints(3, 3, 3, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

            //---- label3 ----
            label3.setText(Language.getString("USER_NAME"));
            panel1.add(label3, new TableLayoutConstraints(1, 5, 1, 5, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
            panel1.add(txtUserName, new TableLayoutConstraints(3, 5, 3, 5, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

            //---- label4 ----
            label4.setText(Language.getString("PASSWORD"));
            panel1.add(label4, new TableLayoutConstraints(1, 7, 1, 7, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
            panel1.add(txtPassword, new TableLayoutConstraints(3, 7, 3, 7, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
        }
        contentPane.add(panel1, new TableLayoutConstraints(1, 0, 1, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

        //======== panel2 ========
        {
            panel2.setBorder(new TitledBorder(null, Language.getString("PROXY"), TitledBorder.LEADING, TitledBorder.TOP));
            panel2.setLayout(new TableLayout(new double[][]{{25, 81, 5, TableLayout.FILL, 70, 2}, {0, 23, 5, 23, 2}}));
            panel2.add(chkSocks, new TableLayoutConstraints(0, 1, 0, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

            //---- label5 ----
            label5.setText(Language.getString("SOCKS"));
            panel2.add(label5, new TableLayoutConstraints(1, 1, 1, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
            panel2.add(txtSocksIP, new TableLayoutConstraints(3, 1, 3, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
            panel2.add(txtSocksPort, new TableLayoutConstraints(4, 1, 4, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
            panel2.add(chkWeb, new TableLayoutConstraints(0, 3, 0, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

            //---- label6 ----
            label6.setText(Language.getString("WEB"));
            panel2.add(label6, new TableLayoutConstraints(1, 3, 1, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
            panel2.add(txtWebIP, new TableLayoutConstraints(3, 3, 3, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
            panel2.add(txtWebPort, new TableLayoutConstraints(4, 3, 4, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
        }
        contentPane.add(panel2, new TableLayoutConstraints(1, 1, 1, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

          //======== panel3 ========
        {
            panel3.setLayout(new TableLayout(new double[][] {
                {TableLayout.FILL, 80, 5, 80, 9},
                {0, TableLayout.PREFERRED, 5}}));
            //---- btnOk ----
            btnOk.setText(Language.getString("OK"));
            panel3.add(btnOk, new TableLayoutConstraints(1, 1, 1, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

            //---- btnCancel ----
            btnCancel.setText(Language.getString("CANCEL"));
            panel3.add(btnCancel, new TableLayoutConstraints(3, 1, 3, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
        }
        contentPane.add(panel3, new TableLayoutConstraints(1, 2, 1, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
        setLocationRelativeTo(getOwner());
        GUISettings.applyOrientation(this);
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Uditha N
    public JPanel panel1;
    public JLabel label1;
    public JTextField txtMarketServers;
    public JLabel label2;
    public JTextField txtBrokerSerevrs;
    public JLabel label3;
    public JTextField txtUserName;
    public JLabel label4;
    public TWPasswordField txtPassword;
    public JPanel panel2;
    public JCheckBox chkSocks;
    public JLabel label5;
    public JTextField txtSocksIP;
    public JTextField txtSocksPort;
    public JCheckBox chkWeb;
    public JLabel label6;
    public JTextField txtWebIP;
    public JTextField txtWebPort;
    public JPanel panel3;
    public JButton btnOk;
    public JButton btnCancel;
    public Thread thread;
    public int intialConnectionMode;
    public boolean active = true;
    public boolean okPressed = false;

    private ValueFormatter txtSoPxyFormatter = new ValueFormatter(ValueFormatter.POSITIVE_INTEGER, 6L);
    private ValueFormatter txtWePxyFormatter = new ValueFormatter(ValueFormatter.POSITIVE_INTEGER, 6L);
}



