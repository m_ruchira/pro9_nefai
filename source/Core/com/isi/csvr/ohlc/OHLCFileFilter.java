package com.isi.csvr.ohlc;

import java.io.File;
import java.io.FileFilter;
import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 9, 2005
 * Time: 1:46:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class OHLCFileFilter implements FileFilter, Comparator {

    // file filter for the history files root
    public boolean accept(File pathname) {
        if (pathname.isDirectory()) {
            return true;
        } else {
            return false;
        }
    }

    public int compare(Object o1, Object o2) {
        File f1 = (File) o1;
        File f2 = (File) o2;

        // sort in descending order
        //return f1.getName().toUpperCase().compareTo(f2.getName().toUpperCase()); //Bug Id <#0006>
        return f2.getName().toUpperCase().compareTo(f1.getName().toUpperCase());
    }
}
