package com.isi.csvr.ohlc;

import com.isi.csvr.shared.DynamicArray;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 05-Aug-2008 Time: 13:46:44 To change this template use File | Settings
 * | File Templates.
 */
public class IntradayGraphDataLoader extends Thread {
     private DynamicArray ohlcList;
    private String exchange;
    private String symbol;
    private int instrument;
    private String currentdate;


    public IntradayGraphDataLoader(DynamicArray ohlcList, String exchange, String symbol, int instruemnt,String cdate) {
        super("Intrday GraphData Loader");
        this.ohlcList = ohlcList;
        this.exchange = exchange;
        this.symbol   = symbol;
        this.instrument   = instruemnt;
        this.currentdate=cdate;
    }



    public void run() {
        OHLCStore.getInstance().loadIntradayDataToOHLCGraph(ohlcList, exchange, symbol, instrument,currentdate);
        ohlcList = null;
    }
}
