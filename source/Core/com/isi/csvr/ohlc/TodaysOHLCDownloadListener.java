package com.isi.csvr.ohlc;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Nov 20, 2006
 * Time: 12:38:56 PM
 * To change this template use File | Settings | File Templates.
 */
public interface TodaysOHLCDownloadListener {

    void OHLCBacklogDownloadCompleted(String exchange);

    void OHLCBacklogDownloadStarted(String exchange);
}
