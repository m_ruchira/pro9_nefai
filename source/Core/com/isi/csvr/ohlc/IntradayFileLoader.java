package com.isi.csvr.ohlc;

import com.isi.csvr.shared.DynamicArray;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 25, 2005
 * Time: 9:15:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class IntradayFileLoader extends Thread {
    private DynamicArray ohlcList;
    private String exchange;
    private String symbol;
    private int instrument;
    private String currentdate;

    public IntradayFileLoader(DynamicArray ohlcList, String exchange, String symbol, int instruemnt) {
        super("Intrday History Loader");
        this.ohlcList = ohlcList;
        this.exchange = exchange;
        this.symbol   = symbol;
        this.instrument   = instruemnt;
    }
  



    public void run() {
        OHLCStore.getInstance().loadIntradayHistryForSymbol(ohlcList, exchange, symbol, instrument);
        ohlcList = null;
    }
}
