package com.isi.csvr.tabbedpane;

import java.util.EventObject;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Dec 18, 2007
 * Time: 10:41:29 AM
 */
public class TWTabEvent extends EventObject {

    public static int STATE_SELECTED = 0;
//    public static int STATE_DESELECTED = 1; currently not supported
    public static int STATE_ADDED = 2;
    public static int STATE_REMOVED = 3;
    public static int STATE_HIGHLIGHTED = 4;
    private int state;
    /**
     * Constructs a prototypical Event.
     *
     * @param source The object on which the Event initially occurred.
     * @param state can be STATE_SELECTED, STATE_ADDED or STATE_REMOVED
     * @throws IllegalArgumentException if source is null.
     */
    public TWTabEvent(Object source, int state) {
        super(source);
        this.state = state;
    }

    /**
     * returns the reson for this event
     * @return STATE_SELECTED, STATE_ADDED or STATE_REMOVED
     */
    public int getState() {
        return state;
    }
}
