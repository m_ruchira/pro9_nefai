package com.isi.csvr;

import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.dnd.DraggableTable;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.mist.MISTModel;
import com.isi.csvr.mist.MISTTable;
import com.isi.csvr.mist.MISTTableSettings;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.theme.TableThemeSettings;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.plaf.metal.MetalComboBoxButton;
import javax.swing.plaf.metal.MetalScrollButton;
import java.awt.*;
import java.awt.event.*;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: udayaa Date: May 12, 2008 Time: 11:06:33 AM
 */
public class TableMenuToolbar extends Window implements AWTEventListener, MouseListener, ItemListener, ActionListener {

    private JPopupMenu parentpopup;
    private int xp = 0;
    private int yp = 0;
    private int width = 218;
    private int height = 50;
    private JPanel wPannel;
    private JPanel topPannel;
    private JPanel btmPannel;
    private boolean visable;
    private Color bgColor;
    private Color fgColor;
    private Color comboBorderColor;
    private Color borderColor;
    private int borderThickness;

    private ClientTable g_oTempTable;
    private boolean mistViewSelected = false;
    private MISTTable g_oMistTable;
    private Menus g_oMenus;
    private boolean mainboardSelected;
    private boolean portfolioSelected;
    private PortfolioTable g_oPortfolioTable;
    private TableThemeSettings target;
    private MISTTableSettings misttarget;
    private Font oldFont;
    private Font newFont;

    //decimal menu
    private JPopupMenu mnuDecimalPlaces;
    private JPopupMenu mnuCurrency;
    private JPopupMenu mnuFilter;
    private WatchlistFilterUI filterui;
    private JPopupMenu mnuSellSelect;
    private JPopupMenu mnuCopy;
    private JPopupMenu mnuLinkExcel;
    private TWRadioButtonMenuItem mnuDefaultDecimal;
    private TWRadioButtonMenuItem mnuNoDecimal;
    private TWRadioButtonMenuItem mnuOneDecimal;
    private TWRadioButtonMenuItem mnuTwoDecimal;
    private TWRadioButtonMenuItem mnuThrDecimal;
    private TWRadioButtonMenuItem mnuFouDecimal;

    //buttons
    private TWEXComboBox fontSelect;
    private ComboRenderer fontRenderer;
    private TWComboBox textSizeSelect;

    private ToolBarButton tbtnDecimaChange;
    private ToolBarButton tbtnResize;
    private ToolBarButton tbtnCopy;
    private ToolBarButton tbtnLinkExcell;
    private ToolBarButton tbtnCurrency;
    private ToolBarButton tbtnColSettings;
    private ToolBarButton tbtnCellselection;
    private ToolBarButton tbtnCustomize;
    private ToolBarButton tbtnFilter;
    private ToolBarButton tbtnBoald;
    private ToolBarButton tbtnItalic;
    private ToolBarButton tbtnTxtSzUp;
    private ToolBarButton tbtnTxtSzDn;
    private ToolBarButton tbtnhideHeader;

    private ToolBarButton tbtnDefault;
    public static boolean FIRST_TIME_LOAD = true;

    String[] sizeList = new String[]
            {"2", "4", "6", "8", "10", "12", "14", "16", "18", "20", "22", "24", "30", "36", "48", "72"};

    public TableMenuToolbar(Frame owner) {
        super(owner);
        bgColor = Theme.getColor("MENU_TOOLBAR_BGCOLOR");
        fgColor = Theme.getColor("MENU_TOOLBAR_FGCOLOR");
        comboBorderColor = Theme.getColor("MENU_TOOLBAR_COMBO_BORDER_COLOR");
        borderColor = Theme.getColor("MENU_TOOLBAR_BORDER_COLOR");
        borderThickness = Theme.getInt("MENU_TOOLBAR_BORDER_THICKNESS", 1);
        width = Constants.TABLE_MENU_TOOLBAR_WIDTH + 2;
        initPopupWindowUI();
        System.out.println("New toolbar created");
    }

    public void setParentJPopup(JPopupMenu pop) {
        parentpopup = pop;
    }

    public void setVisible(boolean b) {

        if (b) {
            checkFilter();
            checkCurrency();
            checkExcell();
            setSelectedIndexes();
            addListeners();
            visable = b;

            if (parentpopup != null) {
                Point p = parentpopup.getLocation();

                SwingUtilities.convertPointToScreen(p, parentpopup);
                if (parentpopup.getWidth() != 0) {
                }
                if (p.getX() != 0 && p.getY() != 0) {
                    xp = (int) p.getX();
                    yp = (int) p.getY();
                }
                setSize(width, height);
                setLocation(xp, yp - (height + 5));         // display window 3 pixels above the popup menu
            }
            GUISettings.applyOrientation(this);
        }
        super.setVisible(b);    //To change body of overridden methods use File | Settings | File Templates.
    }


    public void initPopupWindowUI() {
        wPannel = new JPanel(new BorderLayout(2, 5));
        wPannel.setBackground(bgColor/*Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR2")*/);

        topPannel = new JPanel(new FlowLayout(FlowLayout.LEFT, 1, 0));
        topPannel.setPreferredSize(new Dimension(150, 20));
        topPannel.setOpaque(false);
        fillTopPanel();

        btmPannel = new JPanel(new FlowLayout(FlowLayout.LEFT, 1, 0));
        btmPannel.setPreferredSize(new Dimension(150, 20));
        btmPannel.setOpaque(false);
        fillBtmPanel();

        wPannel.setBorder(BorderFactory.createCompoundBorder(BorderFactory.createLineBorder(borderColor, borderThickness), BorderFactory.createEmptyBorder(2, 2, 2, 2)));
        wPannel.add(topPannel, BorderLayout.NORTH);
        wPannel.add(btmPannel, BorderLayout.SOUTH);
        add(wPannel);
    }

    public void eventDispatched(AWTEvent event) {
        Object obj = event.getSource();

        if (this.isVisible()) {
            if (event.getID() == 502 && (obj instanceof JMenuItem)) {       // to hide window when main popup exits
                this.setVisible(false);
                removeListeners();
            } else if (event.getID() == 500 && !(obj instanceof JComboBox || obj == this || obj instanceof MetalComboBoxButton || obj instanceof ToolBarButton || obj instanceof MetalScrollButton || obj instanceof TWCustomCheckBox)) {     // mouse pressed not inside the window
                this.setVisible(false);
                removeListeners();

            } else if (event.getID() == 501 && (obj instanceof JButton && !(obj instanceof MetalComboBoxButton || obj instanceof TWButton || obj instanceof MetalScrollButton))) {        // internal frame minimized,maximized
                this.setVisible(false);
                removeListeners();
            } else if (event.getID() == 206 && obj instanceof TWFrame) {
                this.setVisible(false);
                removeListeners();
            } else if (event.getID() == 100 && (obj.equals(Client.getInstance().getFrame()) || obj instanceof InternalFrame)) {
                this.setVisible(false);
                removeListeners();
            }
        }
    }

    private void removeListeners() {

        try {
            Toolkit.getDefaultToolkit().removeAWTEventListener(this);
            Toolkit.getDefaultToolkit().removeAWTEventListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addListeners() {
        Toolkit.getDefaultToolkit().addAWTEventListener(this, AWTEvent.MOUSE_EVENT_MASK);
        Toolkit.getDefaultToolkit().addAWTEventListener(this, AWTEvent.WINDOW_EVENT_MASK);
    }


    public void fillTopPanel() {
        fontRenderer = new ComboRenderer();
        Integer p[] = {1, 2, 3, 4,};
        fontSelect = new TWEXComboBox(getFontList());
        fontSelect.setExtendedMode(false);
        fontSelect.setPreferredSize(new Dimension(65, 19));

        fontSelect.setExtendedMode(true);
        fontSelect.setToolTipText(Language.getString("FONT"));

        fontSelect.addItemListener(this);
        fontSelect.setMaximumRowCount(15);
        fontSelect.setRenderer(fontRenderer);
        fontSelect.setBackground(bgColor);
        fontSelect.setForeground(fgColor);
        fontSelect.setArrowButtonIconColor(fontSelect.getForeground());
        fontSelect.setFocusable(false);
        fontSelect.setBorder(BorderFactory.createLineBorder(comboBorderColor));
        GUISettings.clearBorders(fontSelect);
        SwingUtilities.updateComponentTreeUI(fontSelect);

        textSizeSelect = new TWComboBox(sizeList);
        textSizeSelect.setPreferredSize(new Dimension(44, 19));
        textSizeSelect.setToolTipText(Language.getString("SIZE"));
        textSizeSelect.addItemListener(this);
        textSizeSelect.setMaximumRowCount(16);
        textSizeSelect.setFocusable(false);
        textSizeSelect.setBackground(bgColor);
        textSizeSelect.setForeground(fgColor);
        textSizeSelect.setArrowButtonIconColor(textSizeSelect.getForeground());
        textSizeSelect.setBorder(BorderFactory.createLineBorder(comboBorderColor));
        GUISettings.clearBorders(textSizeSelect);

        tbtnTxtSzUp = new ToolBarButton();
        tbtnTxtSzUp.setPreferredSize(new Dimension(20, 19));
        tbtnTxtSzUp.setToolTipText(Language.getString("INCREASE_FONT"));                     //"Increase Font Size"
        tbtnTxtSzUp.addMouseListener(this);
        tbtnTxtSzUp.setIcon(getIconFromString("in-font"));
        tbtnTxtSzUp.setRollOverIcon(getIconFromString("in-font-mouseover"));

        tbtnTxtSzDn = new ToolBarButton();
        tbtnTxtSzDn.setPreferredSize(new Dimension(20, 19));
        tbtnTxtSzDn.setToolTipText(Language.getString("DECREASE_FONT"));           //"Decrease Font Size"
        tbtnTxtSzDn.addMouseListener(this);
        tbtnTxtSzDn.setIcon(getIconFromString("de-font"));
        tbtnTxtSzDn.setRollOverIcon(getIconFromString("de-font-mouseover"));


        tbtnBoald = new ToolBarButton();
        tbtnBoald.setPreferredSize(new Dimension(20, 19));
        tbtnBoald.setToolTipText(Language.getString("BOLD"));
        tbtnBoald.setIcon(getIconFromString("bold"));
        tbtnBoald.setRollOverIcon(getIconFromString("bold-mouseover"));
        tbtnBoald.setToggledIcon(getIconFromString("bold-mouseover"));
        tbtnBoald.setToggleEnabled(true);
        tbtnBoald.addMouseListener(this);

        tbtnItalic = new ToolBarButton();
        tbtnItalic.setPreferredSize(new Dimension(15, 19));
        tbtnItalic.setToolTipText(Language.getString("ITALIC"));
        tbtnItalic.addMouseListener(this);
        tbtnItalic.setIcon(getIconFromString("italic"));
        tbtnItalic.setRollOverIcon(getIconFromString("italic-mouseover"));
        tbtnItalic.setToggledIcon(getIconFromString("italic-mouseover"));
        tbtnItalic.setToggleEnabled(true);
        if (!Language.isLTR()) {
            tbtnItalic.setEnabled(false);
            tbtnItalic.removeMouseListener(this);
        }

        tbtnDefault = new ToolBarButton();
        tbtnDefault.setPreferredSize(new Dimension(19, 19));
        tbtnDefault.setToolTipText(Language.getString("DEFAULT"));
        tbtnDefault.addMouseListener(this);
        tbtnDefault.setIcon(getIconFromString("default"));
        tbtnDefault.setRollOverIcon(getIconFromString("default-mouseover"));
        tbtnDefault.setToggledIcon(getIconFromString("default-mouseover"));
        tbtnDefault.setToggleEnabled(true);

        topPannel.add(fontSelect);
        topPannel.add(textSizeSelect);
        topPannel.add(tbtnTxtSzUp);
        topPannel.add(tbtnTxtSzDn);
        topPannel.add(tbtnBoald);
        topPannel.add(tbtnItalic);
        topPannel.add(tbtnDefault);
    }

    public void fillBtmPanel() {
        tbtnCopy = new ToolBarButton();
        tbtnCopy.setPreferredSize(new Dimension(20, 19));
        tbtnCopy.setToolTipText(Language.getString("COPY"));
        tbtnCopy.addMouseListener(this);
        tbtnCopy.setIcon(getIconFromString("copy"));
        tbtnCopy.setRollOverIcon(getIconFromString("copy-mouseover"));

        tbtnLinkExcell = new ToolBarButton();
        tbtnLinkExcell.setPreferredSize(new Dimension(20, 19));
        tbtnLinkExcell.setToolTipText(Language.getString("LINK_TO_XL"));
        tbtnLinkExcell.addMouseListener(this);
        tbtnLinkExcell.setIcon(getIconFromString("link-excel"));
        tbtnLinkExcell.setRollOverIcon(getIconFromString("link-excel-mouseover"));

        tbtnCurrency = new ToolBarButton();
        tbtnCurrency.setPreferredSize(new Dimension(20, 19));
        tbtnCurrency.setToolTipText(Language.getString("CURRENCY"));
        tbtnCurrency.addMouseListener(this);
        tbtnCurrency.setIcon(getIconFromString("currency"));
        tbtnCurrency.setRollOverIcon(getIconFromString("currency-mouseover"));

        tbtnDecimaChange = new ToolBarButton();
        tbtnDecimaChange.setPreferredSize(new Dimension(20, 19));
        tbtnDecimaChange.setToolTipText(Language.getString("POPUP_DECIMAL_PLACES"));
        tbtnDecimaChange.addMouseListener(this);
        tbtnDecimaChange.setIcon(getIconFromString("decimal"));
        tbtnDecimaChange.setRollOverIcon(getIconFromString("decimal-mouseover"));

        tbtnFilter = new ToolBarButton();
        tbtnFilter.setPreferredSize(new Dimension(20, 19));
        tbtnFilter.setToolTipText(Language.getString("FILTER"));
        tbtnFilter.addMouseListener(this);
        tbtnFilter.setIcon(getIconFromString("filter"));
        tbtnFilter.setRollOverIcon(getIconFromString("filter-mouseover"));

        tbtnCustomize = new ToolBarButton();
        tbtnCustomize.setPreferredSize(new Dimension(20, 19));
        tbtnCustomize.setToolTipText(Language.getString("CUSTOMIZE"));
        tbtnCustomize.addMouseListener(this);
        tbtnCustomize.setIcon(getIconFromString("customize"));
        tbtnCustomize.setRollOverIcon(getIconFromString("customize-mouseover"));

        tbtnColSettings = new ToolBarButton();
        tbtnColSettings.setPreferredSize(new Dimension(20, 19));
        tbtnColSettings.setToolTipText(Language.getString("COLUMN_SETTINGS"));
        tbtnColSettings.addMouseListener(this);
        tbtnColSettings.setIcon(getIconFromString("column"));
        tbtnColSettings.setRollOverIcon(getIconFromString("column-mouseover"));

        tbtnCellselection = new ToolBarButton();
        tbtnCellselection.setPreferredSize(new Dimension(20, 19));
        tbtnCellselection.setToolTipText(Language.getString("SELECTION_MODE"));
        tbtnCellselection.addMouseListener(this);
        tbtnCellselection.setIcon(getIconFromString("cell-selection"));
        tbtnCellselection.setRollOverIcon(getIconFromString("cell-selection-mouseover"));

        tbtnResize = new ToolBarButton();
        tbtnResize.setPreferredSize(new Dimension(20, 19));
        tbtnResize.setToolTipText(Language.getString("ADJUST_WIDTHS"));
        tbtnResize.addMouseListener(this);
        tbtnResize.setIcon(getIconFromString("resize"));
        tbtnResize.setRollOverIcon(getIconFromString("resize-mouseover"));

        tbtnhideHeader = new ToolBarButton();
        tbtnhideHeader.setPreferredSize(new Dimension(20, 19));
        tbtnhideHeader.setToolTipText(Language.getString("SHOWHIDE_TITLEBAR"));
        tbtnhideHeader.setIcon(getIconFromString("hide-title"));
        tbtnhideHeader.setRollOverIcon(getIconFromString("hide-title-mouseover"));
        tbtnhideHeader.setToggledIcon(getIconFromString("hide-title-mouseover"));
        tbtnhideHeader.setToggleEnabled(true);
        tbtnhideHeader.addMouseListener(this);

        btmPannel.add(tbtnCopy);
        btmPannel.add(tbtnLinkExcell);
        btmPannel.add(tbtnCurrency);
        btmPannel.add(tbtnDecimaChange);
        btmPannel.add(tbtnFilter);
        btmPannel.add(tbtnCustomize);
        btmPannel.add(tbtnColSettings);
        btmPannel.add(tbtnCellselection);
        btmPannel.add(tbtnResize);
        btmPannel.add(tbtnhideHeader);
    }

    public Vector getFontList() {
        String[] fontList = GraphicsEnvironment.getLocalGraphicsEnvironment().getAvailableFontFamilyNames();
        Vector comboList = new Vector();
        String test = Language.getString("LANG");
        for (int i = 0; i < fontList.length; i++) {
            Font fnt = new Font(fontList[i], Font.PLAIN, 12);
            if (fnt.canDisplayUpTo(test) == -1) {
                comboList.add(fontList[i]);
            }
        }
        return comboList;
    }

    public void setG_oTempTable(ClientTable g_oTempTable) {
        this.g_oTempTable = g_oTempTable;
        if (g_oTempTable != null) {
            target = (TableThemeSettings) g_oTempTable.getTable();
            oldFont = target.getBodyFont();
        }
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource().equals(tbtnCopy)) {
            try {
                mnuCopy.removeAll();
            } catch (Exception e1) {
            }
            TWMenuItem men1 = Client.getInstance().getCopyMenu();
            TWMenuItem men2 = Client.getInstance().getCopyWithHeadMenu();
            men1.updateUI();
            men2.updateUI();
            mnuCopy = new JPopupMenu();
            mnuCopy.add(men1);
            mnuCopy.add(men2);
            GUISettings.applyOrientation(mnuCopy);
            mnuCopy.show(tbtnCopy, 0, 22);
        } else if (e.getSource().equals(tbtnLinkExcell)) {

            if (mnuLinkExcel == null) {
                TWMenuItem men1 = Client.getInstance().getLinkMenu();
                TWMenuItem men2 = Client.getInstance().getLinkMenuWithHeaders();
                men1.updateUI();
                men2.updateUI();
                mnuLinkExcel = new JPopupMenu();
                mnuLinkExcel.add(men1);
                mnuLinkExcel.add(men2);
            }
            GUISettings.applyOrientation(mnuLinkExcel);
            mnuLinkExcel.show(tbtnLinkExcell, 0, 22);
        } else if (e.getSource().equals(tbtnCurrency)) {
            if (mnuCurrency == null) {
                JMenu men = g_oMenus.getCurrencyPopup();
                mnuCurrency = new JPopupMenu();
                int c = men.getItemCount();
                for (int i = 0; i < c; i++) {
                    TWRadioButtonMenuItem itm = (TWRadioButtonMenuItem) men.getItem(0);
                    GUISettings.applyOrientation(itm);
                    mnuCurrency.add(itm);
                }
            }
            GUISettings.applyOrientation(mnuCurrency);
            mnuCurrency.show(tbtnCurrency, 0, 22);
        } else if (e.getSource().equals(tbtnDecimaChange)) {
            if (mnuDecimalPlaces == null) {
                createDecimalMenu();
            }
            setDecimalPlaces(((DraggableTable) g_oTempTable.getTable()).getDecimalPlaces());
            mnuDecimalPlaces.show(tbtnDecimaChange, 0, 22);
        } else if (e.getSource().equals(tbtnFilter)) {

            if ((!mistViewSelected && g_oTempTable != null && g_oTempTable.isCustomType()) || (mistViewSelected && g_oMistTable != null)) {
                if (mnuFilter == null) {
                    mnuFilter = new JPopupMenu();
                    if (filterui == null) {
                        filterui = new WatchlistFilterUI();
                    }
                    if (!mistViewSelected) {
                        filterui.setClientTable(g_oTempTable);
                    } else {
                        filterui.setMISTTable(g_oMistTable);
                    }
                    filterui.populateUI();
                }

                if (!mistViewSelected) {
                    filterui.setClientTable(g_oTempTable);
                } else {
                    filterui.setMISTTable(g_oMistTable);
                }
                GUISettings.applyOrientation(mnuFilter);
                filterui.populateUI();
                mnuFilter.add(filterui);
                mnuFilter.show(tbtnFilter, 0, 22);
            } else {

                String sym = Client.getInstance().getSelectedSymbol();
                Symbols symbols;
                if (!mistViewSelected) {
                    symbols = g_oTempTable.getSymbols();
                } else {
                    symbols = g_oMistTable.getViewSettings().getSymbols();
                }
                MarketWatchFilter mWatchFilter = new MarketWatchFilter(Client.getInstance().getFrame(), g_oTempTable, symbols);
                mWatchFilter.setVisible(true);
                g_oTempTable.getTable().updateUI();
            }

        } else if (e.getSource().equals(tbtnCustomize)) {
            Client.getInstance().showCustomizer();
        } else if (e.getSource().equals(tbtnColSettings)) {
            Client.getInstance().g_mnuTreePopSettings_Selected();
        } else if (e.getSource().equals(tbtnCellselection)) {

            if (mnuSellSelect == null) {
                TWMenu men = Client.getInstance().getCellSelectionPopup();
                mnuSellSelect = new JPopupMenu();
                int c = men.getItemCount();
                for (int i = 0; i < c; i++) {
                    TWMenuItem itm = (TWMenuItem) men.getItem(0);
                    itm.updateUI();
                    mnuSellSelect.add(itm);
                }
            }
            GUISettings.applyOrientation(mnuSellSelect);
            mnuSellSelect.show(tbtnCellselection, 0, 22);

        } else if (e.getSource().equals(tbtnResize)) {
            resizecolumns();
            this.setVisible(false);
        } else if (e.getSource().equals(tbtnTxtSzUp)) {
            if (mistViewSelected) {
                int size1 = misttarget.getBodyFont().getSize();
                int index = textSizeSelect.getSelectedIndex();
                if (size1 < 72) {
                    int nwsize = Integer.parseInt((textSizeSelect.getItemAt(index + 1)).toString());
                    System.out.println("{} " + nwsize);
                    newFont = new Font(oldFont.getFamily(), oldFont.getStyle(), nwsize);
                    misttarget.setBodyFont(newFont);
                    g_oMistTable.getTable().getSmartTable().setBodyFont();
                    g_oMistTable.getTable().getModel().getViewSettings().setFont(newFont);
                    oldFont = newFont;
                    setSelectedIndexes();
                    g_oMistTable.getTable().getSmartTable().updateUI();
                }
            } else {
                int size1 = target.getBodyFont().getSize();
                int index = textSizeSelect.getSelectedIndex();

                if (size1 < 72) {
                    int nwsize = Integer.parseInt((textSizeSelect.getItemAt(index + 1)).toString());
                    System.out.println("{} " + nwsize);
                    newFont = new Font(oldFont.getFamily(), oldFont.getStyle(), nwsize);
                    target.setBodyFont(newFont);
                    target.updateUI();
                    oldFont = newFont;
                    setSelectedIndexes();
                    resizecolumns();
                    g_oTempTable.getViewSettings().setFont(newFont);
                    FontMetrics oMetrices = g_oTempTable.getTable().getGraphics().getFontMetrics();
                    g_oTempTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + target.getBodyGap());
                    target.updateUI();
                    g_oTempTable.getTable().updateUI();
                }
            }

        } else if (e.getSource().equals(tbtnTxtSzDn)) {
            if (mistViewSelected) {

                int size1 = misttarget.getBodyFont().getSize();
                int index = textSizeSelect.getSelectedIndex();

                if (size1 > 2) {
                    int nwsize = Integer.parseInt((textSizeSelect.getItemAt(index - 1)).toString());
                    System.out.println("{} " + nwsize);
                    newFont = new Font(oldFont.getFamily(), oldFont.getStyle(), nwsize);
                    misttarget.setBodyFont(newFont);
                    g_oMistTable.getTable().getSmartTable().setBodyFont();
                    g_oMistTable.getTable().getModel().getViewSettings().setFont(newFont);
                    oldFont = newFont;
                    setSelectedIndexes();
                    g_oMistTable.getTable().getSmartTable().updateUI();
                }
            } else {

                int size1 = target.getBodyFont().getSize();
                int index = textSizeSelect.getSelectedIndex();

                if (size1 > 2) {
                    int nwsize = Integer.parseInt((textSizeSelect.getItemAt(index - 1)).toString());
                    System.out.println("{} " + nwsize);
                    newFont = new Font(oldFont.getFamily(), oldFont.getStyle(), nwsize);
                    target.setBodyFont(newFont);
                    target.updateUI();
                    oldFont = newFont;
                    setSelectedIndexes();
                    resizecolumns();
                    g_oTempTable.getViewSettings().setFont(newFont);
                    FontMetrics oMetrices = g_oTempTable.getTable().getGraphics().getFontMetrics();
                    g_oTempTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + target.getBodyGap());
                    target.updateUI();
                    g_oTempTable.getTable().updateUI();
                }
            }

        } else if (e.getSource().equals(tbtnBoald)) {
            if (mistViewSelected) {
                oldFont = misttarget.getBodyFont();
            } else {
                oldFont = target.getBodyFont();
            }

            if (oldFont.isBold() && oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.ITALIC, oldFont.getSize());
            } else if (oldFont.isBold() && !oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.PLAIN, oldFont.getSize());
            } else if (!oldFont.isBold() && oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.BOLD + Font.ITALIC, oldFont.getSize());
            } else if (!oldFont.isBold() && !oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.BOLD, oldFont.getSize());
            }

            if (mistViewSelected) {
                misttarget.setBodyFont(newFont);
                g_oMistTable.getTable().getSmartTable().setBodyFont();
                g_oMistTable.getTable().getModel().getViewSettings().setFont(newFont);
                g_oMistTable.getTable().getSmartTable().updateUI();

            } else {
                target.setBodyFont(newFont);
                target.updateUI();
                g_oTempTable.getViewSettings().setFont(newFont);
            }
            oldFont = newFont;

        } else if (e.getSource().equals(tbtnItalic)) {

            if (mistViewSelected) {
                oldFont = misttarget.getBodyFont();
            } else {
                oldFont = target.getBodyFont();
            }
            if (oldFont.isBold() && oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.BOLD, oldFont.getSize());
            } else if (oldFont.isBold() && !oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.BOLD + Font.ITALIC, oldFont.getSize());
            } else if (!oldFont.isBold() && oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.PLAIN, oldFont.getSize());
            } else if (!oldFont.isBold() && !oldFont.isItalic()) {
                newFont = new Font(oldFont.getFamily(), Font.ITALIC, oldFont.getSize());
            }

            if (mistViewSelected) {
                misttarget.setBodyFont(newFont);
                g_oMistTable.getTable().getSmartTable().setBodyFont();
                g_oMistTable.getTable().getModel().getViewSettings().setFont(newFont);
                g_oMistTable.getTable().getSmartTable().updateUI();

            } else {
                target.setBodyFont(newFont);
                target.updateUI();
                g_oTempTable.getViewSettings().setFont(newFont);
            }
            oldFont = newFont;
        } else if (e.getSource().equals(tbtnDefault)) {
            newFont = Theme.getInstance().getMainBoardDefaultFont();

            if (mistViewSelected) {
                misttarget.setBodyFont(newFont);
                g_oMistTable.getTable().getSmartTable().setBodyFont();
                g_oMistTable.getTable().getModel().getViewSettings().setFont(newFont);
                g_oMistTable.getTable().getSmartTable().updateUI();
            } else {
                target.setBodyFont(newFont);
                target.updateUI();
                g_oTempTable.getViewSettings().setFont(newFont);
                FontMetrics oMetrices = g_oTempTable.getTable().getGraphics().getFontMetrics();
                g_oTempTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + target.getBodyGap());
                target.updateUI();
                g_oTempTable.getTable().updateUI();
            }
            oldFont = newFont;
            textSizeSelect.setSelectedItem("" + 14);
            fontSelect.setSelectedItem(Theme.getFontName("DEFAULT_FONT"));
        } else if (e.getSource().equals(tbtnhideHeader)) {
            if (mistViewSelected) {
                WindowWrapper frame = (WindowWrapper) g_oMistTable.getTable().getModel().getViewSettings().getParent();
                frame.setTitleVisible(!frame.isTitleVisible());
            } else {
                g_oTempTable.setTitleVisible(!g_oTempTable.isTitleVisible());
                g_oTempTable.getViewSettings().setHeaderVisible(g_oTempTable.isTitleVisible());
                g_oTempTable.adjustHideShowTitleMenu();
            }
            this.setVisible(false);
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void resizecolumns() {
        if (mainboardSelected) {
            ((DraggableTable) g_oTempTable.getTable()).adjustColumnWidthsToFit();
        } else if (portfolioSelected) {
            ((SmartTable) g_oPortfolioTable.getTable().getTable()).adjustColumnWidthsToFit(10);
        } else if (mistViewSelected) {
            ((SmartTable) g_oMistTable.getTable().getTable()).adjustColumnWidthsToFitinMIST(10);
        } else if (Client.getInstance().isVolumeWatcherSelected() || Client.getInstance().cashFlowWatcherSelected) {
            ((DraggableTable) g_oTempTable.getTable()).adjustColumnWidthsToFit();
        }

    }

    public void setMistViewSelected(boolean mistViewSelected) {
        this.mistViewSelected = mistViewSelected;
        tbtnLinkExcell.setEnabled(!mistViewSelected);
        tbtnLinkExcell.setVisible(!mistViewSelected);
        tbtnColSettings.setEnabled(!mistViewSelected);
        tbtnColSettings.setVisible(!mistViewSelected);
        tbtnDecimaChange.setEnabled(!mistViewSelected);
        tbtnDecimaChange.setVisible(!mistViewSelected);
        tbtnFilter.setEnabled(!mistViewSelected);
        tbtnFilter.setVisible(!mistViewSelected);

    }

    public void setG_oMistTable(MISTTable g_oMistTable) {
        this.g_oMistTable = g_oMistTable;

        if (g_oMistTable != null) {
            misttarget = (MISTTableSettings) (g_oMistTable.getTable().getSmartTable().getTableSettings());
            mistViewSelected = true;
            oldFont = misttarget.getBodyFont();
        }
    }

    public void setG_oDefaultTable(ClientTable g_oMistTable) {
        if (FIRST_TIME_LOAD) {
            if (g_oMistTable != null) {
                target = (TableThemeSettings) g_oTempTable.getTable();
                FIRST_TIME_LOAD = false;
            }
        }
    }

    public void setG_oMenus(Menus g_oMenus) {
        this.g_oMenus = g_oMenus;
    }

    public void setMainboardSelected(boolean mainboardSelected) {
        this.mainboardSelected = mainboardSelected;
    }

    public void setPortfolioSelected(boolean portfolioSelected) {
        this.portfolioSelected = portfolioSelected;
    }

    public void setG_oPortfolioTable(PortfolioTable g_oPortfolioTable) {
        this.g_oPortfolioTable = g_oPortfolioTable;
    }

    private void createDecimalMenu() {
        mnuDecimalPlaces = new JPopupMenu();
        ButtonGroup oGroup = new ButtonGroup();

        mnuDefaultDecimal = new TWRadioButtonMenuItem(Language.getString("DEFAULT_DECIMAL"));
        mnuDecimalPlaces.add(mnuDefaultDecimal);
        mnuDefaultDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuDefaultDecimal);
        mnuNoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_NO_DECIMAL"));
        mnuDecimalPlaces.add(mnuNoDecimal);
        mnuNoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.NO_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuNoDecimal);
        mnuOneDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_ONE_DECIMAL"));
        mnuDecimalPlaces.add(mnuOneDecimal);
        mnuOneDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.ONE_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuOneDecimal);
        mnuTwoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_TWO_DECIMAL"));
        mnuDecimalPlaces.add(mnuTwoDecimal);
        mnuTwoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.TWO_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuTwoDecimal);
        mnuThrDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_THREE_DECIMAL"));
        mnuDecimalPlaces.add(mnuThrDecimal);
        mnuThrDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.THREE_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuThrDecimal);
        mnuFouDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_FOUR_DECIMAL"));
        mnuDecimalPlaces.add(mnuFouDecimal);
        mnuFouDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.FOUR_DECIMAL_PLACES);
            }
        });
        oGroup.add(mnuFouDecimal);
        GUISettings.applyOrientation(mnuDecimalPlaces);
    }

    private void setDecimalPlaces(byte places) {

        if (!mistViewSelected) {
            ((DraggableTable) g_oTempTable.getTable()).setDecimalPlaces(places);
            g_oTempTable.updateGUI();
            g_oTempTable.updateUI();
            g_oTempTable.getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, places);
        } else {
            ((MISTModel) g_oMistTable.getTable().getModel()).setDecimalPlaces(places);
            g_oMistTable.getTable().updateUI();
            g_oMistTable.getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, places);
        }
        if (places == Constants.NO_DECIMAL_PLACES) {
            mnuNoDecimal.setSelected(true);
        } else if (places == Constants.ONE_DECIMAL_PLACES) {
            mnuOneDecimal.setSelected(true);
        } else if (places == Constants.TWO_DECIMAL_PLACES) {
            mnuTwoDecimal.setSelected(true);
        } else if (places == Constants.THREE_DECIMAL_PLACES) {
            mnuThrDecimal.setSelected(true);
        } else if (places == Constants.FOUR_DECIMAL_PLACES) {
            mnuFouDecimal.setSelected(true);
        } else if (places == Constants.UNASSIGNED_DECIMAL_PLACES) {
            mnuDefaultDecimal.setSelected(true);
        }
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource() == textSizeSelect) {
            if (e.getStateChange() == e.SELECTED) {
                int size = Integer.parseInt(textSizeSelect.getSelectedItem().toString());
                if (size != oldFont.getSize()) {
                    newFont = new Font(oldFont.getName(), oldFont.getStyle(), size);
                    if (mistViewSelected) {
                        misttarget.setBodyFont(newFont);
                        g_oMistTable.getTable().getSmartTable().setBodyFont();
                        g_oMistTable.getTable().getModel().getViewSettings().setFont(newFont);
                        g_oMistTable.getTable().getSmartTable().updateUI();
                    } else {
                        target.setBodyFont(newFont);
                        target.updateUI();
                        g_oTempTable.getViewSettings().setFont(newFont);
                        FontMetrics oMetrices = g_oTempTable.getTable().getGraphics().getFontMetrics();
                        g_oTempTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + target.getBodyGap());
                        target.updateUI();
                        g_oTempTable.getTable().updateUI();
                    }
                    oldFont = newFont;
                }
            }

        } else if (e.getSource() == fontSelect) {

            if (e.getStateChange() == e.SELECTED) {
                String type = fontSelect.getSelectedItem().toString();
                if (!oldFont.getFamily().equals(type)) {

                    newFont = new Font(type, oldFont.getStyle(), oldFont.getSize());
                    if (!(newFont.canDisplayUpTo("TEST") == -1)) {
                        JOptionPane.showMessageDialog(this, Language.getString("FONT_WARNING_MSG"), Language.getString("WARNING"), JOptionPane.OK_OPTION);
                    }
                    if (mistViewSelected) {
                        misttarget.setBodyFont(newFont);
                        g_oMistTable.getTable().getSmartTable().setBodyFont();
                        g_oMistTable.getTable().getModel().getViewSettings().setFont(newFont);
                        g_oMistTable.getTable().getSmartTable().updateUI();
                    } else {
                        target.setBodyFont(newFont);
                        target.updateUI();
                        g_oTempTable.getViewSettings().setFont(newFont);
                        FontMetrics oMetrices = g_oTempTable.getTable().getGraphics().getFontMetrics();
                        g_oTempTable.getTable().setRowHeight(oMetrices.getMaxAscent() + oMetrices.getMaxDescent() + target.getBodyGap());
                        target.updateUI();
                        g_oTempTable.getTable().updateUI();
                    }
                    oldFont = newFont;
                }
            }
        }
    }

    public void setSelectedIndexes() {

        Font fnt;
        if (mistViewSelected) {
            fnt = misttarget.getBodyFont();
        } else {
            fnt = target.getBodyFont();
        }
        String family = fnt.getFamily();
        int sz = fnt.getSize();
        if (fnt.isBold()) {
            tbtnBoald.setSwitchOn(true);

        } else {
            tbtnBoald.setSwitchOn(false);
        }

        if (Language.isLTR()) {
            if (fnt.isItalic()) {
                tbtnItalic.setSwitchOn(true);
            } else {
                tbtnItalic.setSwitchOn(false);
            }
        }
        textSizeSelect.setSelectedItem("" + sz);
        fontSelect.setSelectedItem(family);
        try {
            ViewSetting vSetting;
            if (mistViewSelected) {
                vSetting = g_oMistTable.getTable().getModel().getViewSettings();

            } else {
                vSetting = g_oTempTable.getViewSettings();
            }

            WindowWrapper frame = (WindowWrapper) vSetting.getParent();
            if (!frame.isTitleVisible()) {
                tbtnhideHeader.setSwitchOn(true);
                tbtnhideHeader.setToolTipText(Language.getString("SHOW_TITLEBAR"));
            } else {
                tbtnhideHeader.setSwitchOn(false);
                tbtnhideHeader.setToolTipText(Language.getString("HIDE_TITLEBAR"));
            }
            frame = null;
        } catch (Exception e) {
        }
    }


    public Icon getIconFromString(String iconFile) {
        Icon icn = null;
        try {
            icn = new ImageIcon(Theme.getTheamedImagePath("/menu/popupwindow/" + iconFile));
        } catch (Exception ex) {

        }
        return icn;
    }

    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj.equals(tbtnBoald)) {
            oldFont = target.getBodyFont();
            if (oldFont.isBold()) {
                newFont = new Font(oldFont.getFamily(), Font.PLAIN, oldFont.getSize());
            } else {
                newFont = new Font(oldFont.getFamily(), Font.BOLD, oldFont.getSize());
            }
            target.setBodyFont(newFont);
            target.updateUI();
            oldFont = newFont;
        }
    }

    public class ComboRenderer extends JLabel implements ListCellRenderer {

        public ComboRenderer() {
            setOpaque(true);
        }

        public Component getListCellRendererComponent(JList list, Object value, int index, boolean isSelected, boolean cellHasFocus) {
            String item = (String) value;
            setText(item);
            if (isSelected) {
                setBackground(list.getSelectionBackground());
                setForeground(list.getSelectionForeground());
            } else {
                setBackground(list.getBackground());
                setForeground(list.getForeground());
            }
            setFont(new TWFont(item, Font.PLAIN, 20, item));

            return this;
        }
    }

    public class TWEXComboBox extends TWComboBox {
        int height = 4;

        public TWEXComboBox(Vector<?> items) {
            super(items);
        }

        public Dimension getPopupSize() {
            int rows = getModel().getSize();
            int width = 0;
            for (int i = 0; i < rows; i++) {
                String item = getModel().getElementAt(i).toString();
                Font fnt = new Font(item, Font.PLAIN, 20);

                width = Math.max(width, super.getFontMetrics(fnt).stringWidth(item));
                height = Math.max(height, super.getFontMetrics(fnt).getHeight());
                item = null;
            }

            if (width < super.getSize().getWidth()) {
                return new Dimension((int) super.getSize().getWidth(), (int) super.getSize().getHeight());
            } else {
                return new Dimension(width + 20, (int) super.getSize().getHeight());
            }
        }


        public void calculateMaxNoOfRowsToDisplay() {
            Toolkit oToolkit = Toolkit.getDefaultToolkit();
            Dimension screenSize = oToolkit.getScreenSize();

            int scrh = screenSize.height;
            int avhight = scrh - (yp - 3);
            int displayablecount = avhight / height;

            int rows = getModel().getSize();
            if (displayablecount > rows) {
                setMaximumRowCount(rows);
            } else {
                setMaximumRowCount(displayablecount);
            }
        }
    }

    public void dispose() {
        super.dispose();
        try {
            Toolkit.getDefaultToolkit().removeAWTEventListener(this);
            Toolkit.getDefaultToolkit().removeAWTEventListener(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void checkFilter() {

        if (!mistViewSelected && g_oTempTable != null && (g_oTempTable.isCustomType() || g_oTempTable.isMarketType())) {
            tbtnFilter.setVisible(true);
            tbtnFilter.setEnabled(true);
        } else if (mistViewSelected && g_oMistTable != null) {
            tbtnFilter.setVisible(true);
            tbtnFilter.setEnabled(true);
        } else {
            tbtnFilter.setVisible(false);
            tbtnFilter.setEnabled(false);
        }
    }

    private void checkCurrency() {
        try {
            if (!mistViewSelected && g_oTempTable != null && ((g_oTempTable.isVolumeWatcherType()) || (g_oTempTable.isCashFlowWatcherType()))) {
                tbtnCurrency.setEnabled(false);
                tbtnCurrency.setVisible(false);
            } else {
                tbtnCurrency.setEnabled(true);
                tbtnCurrency.setVisible(true);
            }
        } catch (Exception e) {

        }
    }

    private void checkExcell() {
        try {
            if (!mistViewSelected && g_oTempTable != null && g_oTempTable.isVolumeWatcherType()) {
                tbtnLinkExcell.setEnabled(false);
                tbtnLinkExcell.setVisible(false);
            } else if (!mistViewSelected) {
                tbtnLinkExcell.setEnabled(true);
                tbtnLinkExcell.setVisible(true);
            }
        } catch (Exception e) {
        }
    }
}

