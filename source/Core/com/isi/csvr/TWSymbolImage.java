package com.isi.csvr;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Stock;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA. User: uditha Date: Oct 9, 2007 Time: 1:46:55 PM
 */
public class TWSymbolImage implements Icon {

    private ImageIcon announcementNew;
    private ImageIcon announcementOld;
    private ImageIcon newsNew;
    private ImageIcon newsOld;
    private ImageIcon eventNew;
    private ImageIcon eventOld;
    private ImageIcon img52WeekHigh;
    private ImageIcon img52WeekLow;
    private ImageIcon imgMin;
    private ImageIcon imgMax;
    private int width;
    private int height;

    int annaouncementStatus;
    int newsStatus;
    int eventStatus;
    int newsEventStatus;
    int status52Week;
    int minMaxStatus;


    public TWSymbolImage() {
        announcementNew = new ImageIcon("images/common/status_gn.gif");
        announcementOld = new ImageIcon("images/common/status_yn.gif");
        newsNew = new ImageIcon("images/common/status_ng.gif");
        newsOld = new ImageIcon("images/common/status_ny.gif");
        eventNew = new ImageIcon("images/common/status_eventNew.gif");
        eventOld = new ImageIcon("images/common/status_eventOld.gif");

        img52WeekHigh = new ImageIcon("images/common/52weekHigh.gif");
        img52WeekLow = new ImageIcon("images/common/52weekLow.gif");

        imgMin = new ImageIcon("images/common/min.gif");
        imgMax = new ImageIcon("images/common/max.gif");
    }

    public int getIconHeight() {
        if (height <= 0) {
            try {
                height = Math.max(announcementNew.getIconHeight(), announcementOld.getIconHeight());
                height = Math.max(height, newsNew.getIconHeight());
                height = Math.max(height, newsOld.getIconHeight());
            } catch (Exception e) {
            }
        }
        return height;//height;
    }

    public int getIconWidth() {
        if ((annaouncementStatus == Constants.ITEM_NOT_AVAILABLE) &&
                (newsStatus == Constants.ITEM_NOT_AVAILABLE) &&
                (eventStatus == Constants.ITEM_NOT_AVAILABLE) &&
                (newsEventStatus == Constants.ITEM_NOT_AVAILABLE) &&
                (status52Week == Constants.ITEM_NOT_AVAILABLE)&&
                (minMaxStatus == Constants.ITEM_NOT_AVAILABLE)) {
            width = 1;
        } else {
            width = 0;
            if (annaouncementStatus != Constants.ITEM_NOT_AVAILABLE) {
                width += announcementNew.getIconWidth();
            }
            if (newsStatus != Constants.ITEM_NOT_AVAILABLE) {
                width += newsNew.getIconWidth();
            }
            if (eventStatus != Constants.ITEM_NOT_AVAILABLE) {
                width += eventNew.getIconWidth();
            }
            if (newsEventStatus != Constants.ITEM_NOT_AVAILABLE) {
                width += newsNew.getIconWidth();
            }
            if (status52Week != Constants.ITEM_NOT_AVAILABLE) {
                width += img52WeekHigh.getIconWidth();
            }
            if (minMaxStatus != Constants.ITEM_NOT_AVAILABLE) {
                width += imgMin.getIconWidth();
            }
        }
        return width;//width;
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {
        if (annaouncementStatus == Constants.ITEM_NOT_READ) {
            g.drawImage(announcementNew.getImage(), x, y, null);
            x = x + announcementNew.getIconWidth();
        } else if (annaouncementStatus == Constants.ITEM_READ) {
            g.drawImage(announcementOld.getImage(), x, y, null);
            x = x + announcementOld.getIconWidth() + 2;
        }

        if (newsStatus == Constants.ITEM_NOT_READ) {
            g.drawImage(newsNew.getImage(), x, y, null);
            x = x + newsNew.getIconWidth();
        } else if (newsStatus == Constants.ITEM_READ) {
            g.drawImage(newsOld.getImage(), x, y, null);
            x = x + newsOld.getIconWidth() + 2;
        }

        if (eventStatus == Constants.ITEM_NOT_READ) {
            g.drawImage(eventNew.getImage(), x, y, null);
        } else if (eventStatus == Constants.ITEM_READ) {
            g.drawImage(eventOld.getImage(), x, y, null);
        }

        if (newsEventStatus == Constants.ITEM_NOT_READ) {
            g.drawImage(newsNew.getImage(), x, y, null);
            x = x + newsNew.getIconWidth();
        } else if (newsEventStatus == Constants.ITEM_READ) {
            g.drawImage(newsOld.getImage(), x, y, null);
            x = x + newsOld.getIconWidth() + 2;
        }

        //------ 52 week High Low
        if (status52Week == Stock.BEYOND52WEEKHIGH) {
            g.drawImage(img52WeekHigh.getImage(), x, y, null);
            x = x + img52WeekHigh.getIconWidth();
        } else if (status52Week == Stock.BEYOND52WEEKLOW) {
            g.drawImage(img52WeekLow.getImage(), x, y, null);
            x = x + img52WeekLow.getIconWidth() + 2;
        }

        //-------- Min Max
        if (minMaxStatus == Stock.MIN) {
            g.drawImage(imgMin.getImage(), x, y, null);
            x = x + imgMin.getIconWidth();
        } else if (minMaxStatus == Stock.MAX) {
            g.drawImage(imgMax.getImage(), x, y, null);
            x = x + imgMax.getIconWidth() + 2;
        }
    }

    public void setAnnouncementStatus(int status) {
        annaouncementStatus = status;
    }

    public void setNewsStatus(int status) {
        newsStatus = status;
    }

    public void setEventStatus(int status) {
        eventStatus = status;
    }

    public void setNewsEventStatus(int status) {
        newsEventStatus = status;
    }

    public void set52WeekStatus(int status) {
        status52Week = status;
    }
    public void setMinMaxStatus(int status) {
        minMaxStatus = status;
    }
}
