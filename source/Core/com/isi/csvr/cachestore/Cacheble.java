package com.isi.csvr.cachestore;

public interface Cacheble extends Cloneable, Comparable {
	public String getRecord();

	public void setRecord(String record);

	public Object clone() throws CloneNotSupportedException;

	public long getSequence();

	public int compareTo(Object o);
}
