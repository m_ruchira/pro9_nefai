package com.isi.csvr.cachestore;

public class UniqueID {
	private static long current = System.currentTimeMillis();

	static public synchronized long get() {
		return current++;
	}
}

