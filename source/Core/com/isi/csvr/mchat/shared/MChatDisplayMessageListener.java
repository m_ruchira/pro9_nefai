package com.isi.csvr.mchat.shared;

import com.isi.csvr.mchat.client.MChatSmiliesFrame;
import com.isi.csvr.mchat.client.MChatChatWindow;
import com.isi.csvr.mchat.history.MChatHistoryChatCreate;

import javax.swing.*;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import javax.swing.text.Element;
import javax.swing.text.html.HTML;
import javax.swing.text.html.HTMLDocument;
import javax.swing.text.html.HTMLEditorKit;
import javax.swing.text.html.StyleSheet;
import javax.swing.text.html.parser.ParserDelegator;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import java.util.StringTokenizer;

public class MChatDisplayMessageListener extends JTextPane {

    private MChatHistoryChatCreate ch = null;
    private MChatChatWindow MChatChatWindow = null;

    public MChatDisplayMessageListener(MChatChatWindow MChatChatWindow) {
        try {
            HTMLEditorKit htmlkit = new HTMLEditorKit();
            ch = new MChatHistoryChatCreate();

            StyleSheet styles = htmlkit.getStyleSheet();
            StyleSheet ss = new StyleSheet();

            ss.addStyleSheet(styles);

            HTMLDocument doc = new HTMLDocLinkDetector(ss);
            setEditorKit(htmlkit);
            setDocument(doc);

            this.MChatChatWindow = MChatChatWindow;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected class HTMLDocLinkDetector extends HTMLDocument {
        StyleSheet ss = null;

        public HTMLDocLinkDetector(StyleSheet ss) {
            super(ss);
            this.ss = ss;
            setAsynchronousLoadPriority(4);
            setTokenThreshold(100);
            setParser(new ParserDelegator());
        }

        protected boolean isLink(Element e) {
            return (e.getAttributes().getAttribute(HTML.Tag.A) != null);
        }

        protected synchronized void computeLinks(Element e) throws BadLocationException {
            int caretPos = getCaretPosition();
            try {
                if (isLink(e))
                    correctLink(e);
                else
                    createLink(e);
            } catch (IOException ex) {
                ex.printStackTrace();
            }
            try {
                setCaretPosition(getLength());
            } catch (Exception e1) {
                e1.printStackTrace();
            }
            try {
                MChatChatWindow.setScrollBar();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        }

        protected void correctLink(Element e) throws BadLocationException,
                IOException {

            int length = e.getEndOffset() - e.getStartOffset();

            boolean endOfDoc = e.getEndOffset() == getLength() + 1;

            if (endOfDoc)
                length--;

            String text = getText(e.getStartOffset(), length);

            setOuterHTML(e, text);

            Matcher spaceMatcher = Pattern.compile("(\\s+)$").matcher(text);

            if (spaceMatcher.find()) {
                String endingSpaces = spaceMatcher.group(1);
                insertString(Math.max(getLength(), e.getEndOffset()),
                        endingSpaces, null);
            }
        }

        protected void createLink(Element e) throws BadLocationException,
                IOException {

            boolean isFileLink = false;
            int caretPos = getCaretPosition();

            int startOffset = e.getStartOffset();
            int length = e.getEndOffset() - e.getStartOffset();

            boolean endOfDoc = e.getEndOffset() == getLength() + 1;
            // to avoid catching the final '\n' of the document.

            if (endOfDoc)
                length--;
            else if (getLength() + 1 > e.getEndOffset()) {
                length = getLength() - startOffset;
            } else {
                length = e.getEndOffset() - startOffset;
            }

            String text = null;
            try {
                text = getText(startOffset, length);
            } catch (BadLocationException e1) {
            }

            String[] sepFS = null;
            int iTag = 0;
            String sValue = null;
            String message = null;
            String nickName = null;
            String fontName = "";
            String fontSize = "12";
            boolean italic = false;
            boolean bold = false;
            String dateTime = "";
            boolean isOffline = false;
            if (MChatLanguage.isLTR) {
                fontName = "Tahoma";
            }
            try {
                if (text != null && !text.equals("") && text.length() > 0) {
                    try {
                        sepFS = separateByFS(text.trim(), MChatMeta.FS);
                        for (int j = 0; j < sepFS.length; j++) {
                            try {
                                iTag = Integer.parseInt(getTag(sepFS[j], MChatMeta.DS));
                                sValue = getData(sepFS[j], MChatMeta.DS);
                                if (iTag != 0 && sValue != null) {
                                    switch (iTag) {
                                        case MChatMeta.DISPLAY_MESSEGE:
                                            try {
                                                String[] data = sValue.split(" ");
                                                for (int i = 0; i < data.length; i++) {
                                                    String smileImg = MChatSmiliesFrame.getSharedInstance().getSmiliesRef(data[i]);
                                                    if (smileImg != null) {
                                                        isFileLink = true;
                                                        if (message != null) {
                                                            message = message + " " + smileImg;
                                                        } else {
                                                            message = smileImg + " ";
                                                        }
                                                    } else {
                                                        if (message != null) {
                                                            message = message + " " + data[i];
                                                        } else {
                                                            message = data[i] + " ";
                                                        }
                                                    }
                                                }
                                            } catch (Exception e1) {
//                                                e1.printStackTrace();
                                            }
                                            if (sValue.indexOf("file:") >= 0) {
                                                // file link
                                                try {
                                                    message = "&nbsp;&nbsp;<a href='" + sValue + "'>" + sValue.split("file:///")[1] + "</a>";
                                                } catch (Exception e1) {
//                                                    e1.printStackTrace();
                                                }
                                                isFileLink = true;
                                            }
                                            break;
                                        case MChatMeta.N_NAME:
                                            try {
                                                nickName = sValue;
                                            } catch (Exception e1) {
//                                                e1.printStackTrace();
                                            }
                                            break;
                                        case MChatMeta.DISPLAY_MESSEGE_FONT_NAME:
                                            try {
                                                fontName = sValue;
                                            } catch (Exception ex) {
//                                                ex.printStackTrace();
                                            }
                                            break;
                                        case MChatMeta.DISPLAY_MESSEGE_FONT_ITALIC:
                                            try {
                                                italic = (Integer.parseInt(sValue) == 1);
                                            } catch (Exception exp) {
//                                                exp.printStackTrace();
                                            }
                                            break;
                                        case MChatMeta.DISPLAY_MESSEGE_FONT_BOLD:
                                            try {
                                                bold = (Integer.parseInt(sValue) == 1);
                                            } catch (Exception exp) {
//                                                exp.printStackTrace();
                                            }
                                            break;
                                        case MChatMeta.DISPLAY_MESSEGE_FONT_SIZE:
                                            try {
                                                fontSize = sValue;
                                            } catch (Exception exp) {
//                                                exp.printStackTrace();
                                            }
                                            break;
                                        case MChatMeta.DATE_TIME:
                                            try {
                                                dateTime = sValue;
                                            } catch (Exception exp) {
//                                                exp.printStackTrace();
                                            }
                                            break;
                                        case MChatMeta.IS_OFFLINE_MESSAGE:
                                            try {
                                                isOffline = sValue.equalsIgnoreCase("1");
                                            } catch (Exception exp) {
//                                                exp.printStackTrace();
                                            }
                                            break;
                                        default:
                                            break;
                                            //seperate font parametes
                                    }
                                }
                            } catch (NumberFormatException e1) {
                            }
                        }
                    } catch (Exception e1) {
                    }
                }
            } catch (Exception ew) {
                sValue = null;
            }
//            message = message.replaceAll("<br>","\n");
            if (message != null) {
                if (message.length() > 0) {

                    try {
//                        ch.writeHistorytFile(MChatChatWindow.getReceiver() + ".txt", message, nickName, MChatChatWindow.getReceiver());
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
//                    ss.addRule("body {font-family:arial;font-size:12pt}");
//                    ss.addRule("p {font-family:arial;margin:2}");
                    //<span style="font-size:36;font-family:Bell MT Italic">poddapro : alo

                    /*                   text = "<span style='font-size:" + StyleConstants.getFontSize(DataStore.chatTextAttributes) + ";"
                                               + " font-family:" + StyleConstants.getFontFamily(DataStore.chatTextAttributes) + ";"
                            + " color:#545454"+"'>&nbsp;&nbsp;" + nickName  + " "+ Language.getString("SAYS")+ "</span><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                            "&nbsp;&nbsp;&nbsp;&nbsp;" +
                                                   "<span style='font-size:" + StyleConstants.getFontSize(DataStore.chatTextAttributes) + ";"
                                               + " font-family:" + StyleConstants.getFontFamily(DataStore.chatTextAttributes) + ";"+
                                               " color:#0660bd"+"'>" + message+ "</span>";
                    */
                    try {
                        if (!isFileLink) {
//                            StringTokenizer st = new StringTokenizer(message, "<br>");
//                            String record = null;
//                            while (st.hasMoreTokens()) {
//                                record = st.nextToken();
//
//                            }
                            message = message.replaceAll("<br>", "\r\n");
                            message = message.replaceAll("<", "&lt ");
                            message = message.replaceAll(">", " &gt");
                            message = message.replaceAll("\r\n", "<br>&nbsp;&nbsp;&nbsp;");
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    if (isOffline) {
                        text = "<span style='font-size:" + fontSize + ";"
                                + " font-family:" + fontName + ";"
                                + " color:#545454" + "'>&nbsp;&nbsp;<em>" + nickName + " " + MChatLanguage.getString("SENT") + /*" " + dateTime +*/ "</span><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                                "&nbsp;&nbsp;&nbsp;&nbsp;" +
                                "<span style='font-size:" + fontSize + ";"
                                + " font-family:" + fontName + ";" +
                                " color:#545454" + "'>" + "<em>" + message + "</span>";

                    } else {
                        text = "<span style='font-size:" + fontSize + ";"
                                + " font-family:" + fontName + ";";
/*
                        if (italic) {
                            text = text + "font-style: italic;";
                        } else if (bold) {
                            text = text + "font-weight: bold;";
                        }
*/
                        text = text + " color:#545454" + "'>&nbsp;&nbsp;" + nickName + " " + MChatLanguage.getString("SAYS") + "</span><br>&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;" +
                                "&nbsp;&nbsp;&nbsp;&nbsp;" +
                                "<span style='font-size:" + fontSize + ";"
                                + " font-family:" + fontName + ";";
                        if (italic) {
                            text = text + "font-style: italic;";
                        } else if (bold) {
                            text = text + "font-weight: bold;";
                        }
                        text = text + " color:#0660bd" + "'>" + message + "</span>";
                    }
                } else {
                    text = null;
                }
            }

            Matcher matcher = Pattern.compile(
                    "(?i)(\\b(http://|https://|www.|ftp://|file:/|mailto:)\\S+)(\\s+)")
                    .matcher(text);

            if (matcher.find() && !isFileLink) {
                String url = matcher.group(1);
                String prefix = matcher.group(2);
                String endingSpaces = matcher.group(3);

                Matcher dotEndMatcher = Pattern.compile("([\\W&&[^/]]+)$")
                        .matcher(url);

                //Ending non alpha characters like [.,?%] shouldn't be included
                // in the url.
                String endingDots = "";

                if (dotEndMatcher.find()) {
                    endingDots = dotEndMatcher.group(1);
                    url = dotEndMatcher.replaceFirst("");
                }


                text = matcher.replaceFirst("<a href='" + url + "'>" + url
                        + "</a> " + endingDots + endingSpaces);
                //System.out.println("after "+text);

//                insertString(0,text, FontSelector.getAttributeSet());
                setOuterHTML(e, text);

                // insert initial spaces ignored by the html
                Matcher spaceMatcher = Pattern.compile("^(\\s+)").matcher(text);

                if (spaceMatcher.find()) {
                    String initialSpaces = spaceMatcher.group(1);
                    insertString(startOffset, initialSpaces, null);
                }

                // insert final spaces ignored by the html
                spaceMatcher = Pattern.compile("(\\s+)$").matcher(text);

                if (spaceMatcher.find()) {
                    String extraSpaces = spaceMatcher.group(1);
                    int endoffset = e.getEndOffset();
                    if (extraSpaces.charAt(extraSpaces.length() - 1) == '\n') {
                        extraSpaces = extraSpaces.substring(0, extraSpaces
                                .length() - 1);
                        endoffset--;
                    }
                    try {
                        insertString(Math.max(getLength(), endoffset), extraSpaces,
                                null);
                    } catch (BadLocationException e1) {
                        e1.printStackTrace();
                    }
                }
            } else {
                try {
                    setOuterHTML(e, text);
                } catch (BadLocationException e1) {
                    e1.printStackTrace();
                }
            }
        }

        public void remove(int offs, int len) throws BadLocationException {
            super.remove(offs, len);
            Element e = getCharacterElement(offs - len);
            computeLinks(e);
        }

        public void insertString(int offs, String str, AttributeSet a)
                throws BadLocationException {
            str = str.replaceAll("\r", "<br>");
            str = str.replaceAll("\n", "&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;&nbsp;");
            str = str + "\n";
            super.insertString(offs, str, a);
            Element e = getCharacterElement(offs);
            try {
                computeLinks(e);
            } catch (BadLocationException e1) {
                e1.printStackTrace();
            }
        }

        private String[] separateByFS(String sFrame, String seperator) {
            if (sFrame == null)
                return null;
            String[] s = null;
            try {
                s = sFrame.split(seperator);
            } catch (Exception e) {
            }
            return s;
        }

        public String getTag(String sFrame, String sSeperator) {
            String sTag = null;
            int index = 0;
            if (sFrame == null)
                return null;
            try {
                index = sFrame.indexOf(sSeperator);
                if (index > 0) {
                    sTag = sFrame.substring(0, index + 1);
                }
            } catch (Exception e) {
                index = 0;
                sTag = null;
            }
            return sTag.trim();
        }

        public String getData(String sFrame, String sSeperator) {
            String sData = null;
            int index = 0;
            if (sFrame == null)
                return null;
            try {
                index = sFrame.indexOf(sSeperator);
                if (index > 0) {
                    sData = sFrame.substring(index + 1).trim();
                }
            } catch (Exception e) {
                index = 0;
                sData = null;
            }
            return sData;
        }
    }
}
