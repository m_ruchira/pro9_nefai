package com.isi.csvr.mchat.shared;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.mchat.datastore.MChatDataStore;

import java.awt.*;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

public class MChatSettings {
    private static Properties g_oProperties;
    private static String sSettingsFile;
    public static int serverRTPort = 0;
    public static int serverFilePort = 0;
    public static int voiceServerPort = 0;
    public static String serverIP = null;
    public static String serverFileIP = null;
    public static String voiceServerIP = null;
    public static String MUBASHER_ID = null;
    public static String NICK_NAME = null;
    public static String APP_MODE = "1";
    private static String sData;
    public static String IMAGE_PATH;
    public static boolean isConsoleOutEnabled = true;
    public static boolean isRememberME = false;
    public static boolean isAutoSign = true;
    public static boolean isSearchable = true;
    public static boolean isVoiceEnable = false;
    public static boolean isFirstTime = false;
    public static boolean isAuthenticated = false;
    public static boolean isConnected = true;
    public static boolean isOfflineWarningFormDisabled = false;
    public static int onlineUserCount = 0;
    public static int FILE_ACCEPT = 9;
    public static int FILE_SEND = 10;
    public static int VOICE_BLOCK_SIZE = 512;
    public static SimpleDateFormat dateFormat = new SimpleDateFormat("dd/MM/yyyy");
    public static SimpleDateFormat timeFormat = new SimpleDateFormat("HH:mm:ss");
    public static Calendar cal = Calendar.getInstance(TimeZone.getTimeZone("GMT"));
    private static boolean IS_FILE_TRANSFER_PROGRESS = false;

    public static void Load(String file) {
        InputStream oIn;
        try {
            sSettingsFile = file;
            g_oProperties = new Properties();
            oIn = new FileInputStream(sSettingsFile);
            g_oProperties.load(oIn);
            oIn.close();
        }
        catch (Exception e) {
            System.out.println("SERVER EXIT LOADING " + sSettingsFile);
        }
        oIn = null;
    }

    public static synchronized void setFileTranferInProgress(boolean flag) {
        IS_FILE_TRANSFER_PROGRESS = flag;
    }

    public static boolean isFileTranferInProgress() {
        return IS_FILE_TRANSFER_PROGRESS;
    }

    public static String getString(String s) {
        try {
            sData = g_oProperties.getProperty(s);
            if (sData == null)
                throw new NullPointerException();
            return sData;
        }
        catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static int getInt(String s) {
        try {
            sData = g_oProperties.getProperty(s);
            return (Integer.parseInt(sData));
        }
        catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    private static long getLong(String s) {
        try {
            sData = g_oProperties.getProperty(s);
            return (Long.parseLong(sData));
        }
        catch (Exception e) {
            System.out.println("SERVER EXIT LOADING SETTINGS INT " + s + " : " + e.toString());
            return 0;
        }
    }

    public static void setProperty(String key, String value) {
        g_oProperties.setProperty(key, value);
    }

    public static void save() {
        OutputStream out;
        try {
            out = new FileOutputStream(sSettingsFile);
            g_oProperties.store(out, "Server Configurations. Do not change manually");
            out.close();
            MChatSettings.Load(sSettingsFile);
        }
        catch (Exception e) {
        }
        out = null;
    }

    public static String getDate() {
        try {
            return dateFormat.format(Calendar.getInstance().getTime());
        } catch (Exception e) {
            return "";
        }
    }

    public static String getTime() {
        try {
            Date now = new Date(com.isi.csvr.shared.Settings.getLocalTimeFor(System.currentTimeMillis()));
            int a = Calendar.getInstance().get(Calendar.ZONE_OFFSET);
            return DateFormat.getTimeInstance(DateFormat.SHORT).format(now);
//            return timeFormat.format(Calendar.getInstance(TimeZone.getTimeZone("GMT")).getTime());
        } catch (Exception e) {
            return "";
        }
    }

    public static String getUserID() {
        return MUBASHER_ID;
    }

    public static Color getColor() {
        return Theme.getColor("MCHAT_BG_COLOR");
    }

    public static void destroySelf() {
        isFirstTime = false;
        isAutoSign = true;
        isAuthenticated = false;
        isFirstTime = false;
        isConnected = false;
        MUBASHER_ID = null;
        NICK_NAME = null;
        g_oProperties = null;
        sSettingsFile = null;
        onlineUserCount = 0;
    }
}
