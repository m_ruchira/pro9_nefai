package com.isi.csvr.mchat.shared;

public interface Disposable {
    public void destroySelf();
}
