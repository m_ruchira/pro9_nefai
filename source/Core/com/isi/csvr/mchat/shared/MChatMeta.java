package com.isi.csvr.mchat.shared;

import javax.swing.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.TimeZone;

public class MChatMeta {
    public static final String EOL = "\n";
    public static final String FS = "\u001C";
    public static final String HS = "\u001D";
    public static final String DS = "\u0002";
    public static final String HDS = "\u000F";
    public static final String END = "\u001E";
    public static final String HEND = "\u001B";

    public static final String USERNAME = "1";
    public static final String NICK_NAME = "21";
    public static final String APP_MODE = "601";
    public static final String NEW_USER_FNAME = "30";
    public static final String NEW_USER_LNAME = "31";
    public static final String USER_ID = "12";
    public static final String STATUS = "11";

    //    public static final String CONTACT_LIST  ="2";
    public static final String CONTACT_LIST = "8";
    public static final String CHAT = "3";
    public static final String CONVERSATION = "44";
    public static final String CONVERSATION_ORIGINATOR = "55";
    public static final String CONVERSATION_LIST = "56";
    public static final String CONVERSATION_MESSAGE = "57";
    public static final String DES_ID = "4";
    public static final String TEXT_MESSAGE = "5";
    public static final String SOURCE_ID = "6";

    public static final String MSG_ID = "100";
    public static final String INIT_REQUEST = "7";
    public static final String REGISTER_REQUEST = "28";

    public static final String TEXT_CHAT = "10";
    public static final String ADD_CONTACT = "15";
    public static final String REMOVE_CONTACT = "32";
    public static final String GET_NICK_NAME = "43";
    public static final String STATE_CHANGE = "9";
    public static final String PERSONAL_MESSAGE_CHANGE = "58";
    public static final String ADD_CONTACT_INVITATION = "39";
    public static final String SEARCH_CONTACT = "22";
    public static final String SEARCH_STRING = "23";
    public static final String SEARCH_SUB_TYPE = "33";

//    public static final String REGISTERED_USER = "28";
    public static final String LOGOUT_REQUEST = "17";
    public static final String NUDGE_REQUEST = "26";

    public static final String PULSE = "25";
    public static final String ONLINE_COUNT = "42";
    public static final String NUDGE_CONTACT = "26";
    public static final String APPEARING_STATUS = "50";
    public static final String PERSONAL_MESSAGE = "51";
    public static final String EDIT_DISPLAY_NAME = "123";
    public static final String PERSONAL_SHOWMETO_ALL = "53";
    public static final String PERSONAL_INVITAION = "54";

    public static String MUBASHER_ID;
    public static String DISPLAY_ID;
    public static String FIRST_NAME;
    public static String LAST_NAME;
    public static boolean isLTR;
    public static String LANGUAGE_ID;
    public static final Color MCHAT_COLOR = new Color(15067115);
    public static final Color ONLINE_COLOR = new Color(0,153,0);
    public static final Color DOM_NAME_FONT_COLOR=new Color(9821183);

    public static final String NO_SEARCH_RESULTS_FOUND = "1000";
    public static final String ERROR_INVALID_NICKNAME = "1001";
    public static final String NICK_NAME_NOT_EXIST = "1002";
    public static final String ERROR_NEW_USER = "1003";
    public static final String ERROR_IN_REGISTRATION = "1004";
    public static final String REGISTRATION_SUCSESS = "1005";
    public static final String ALREADY_REGISTERED = "1006";
    public static final String ADD_CONTACT_ERROR = "1007";
    public static final String ADD_CONTACT_SUCCESS = "1008";
    public static final String REMOVE_CONTACT_SUCCESS = "1028";
    public static final String REMOVE_CONTACT_FAILED = "1027";

    public static boolean IS_SENDING_LOGOUT = false;
    public static boolean IS_FINISH_LOGOUT = false;
    public static final String ADD_NEW_CONTACT = "16";
    public static final String NEW_ADD_RQUEST = "16";
    public static final String FILE_SEND_REQUEST = "1";
    public static final String FILE_ACCEPT_REQUEST = "2";
    public static final String FILE_REJECT_REQUEST = "3";

    public static final String FILE_TRANSFER = "60";
    public static final String FILE_REJECT = "62";
    public static final String FILE_ACCEPTED = "63";
    public static final String FILE_REQUEST_ID = "20";
    public static final String FILE_SOURCE_ID = "1";
    public static final String FILE_NAME = "21";
    public static final String FILE_SIZE = "22";
    public static final String FILE_NICK_NAME = "7";
    public static final String FILE_LINK_START = "\u0026";

    public static final short DISPLAY_MESSEGE = 1981;
    public static final short DISPLAY_MESSEGE_FONT_SIZE = 1983;
    public static final short DISPLAY_MESSEGE_FONT_ITALIC = 1986;
    public static final short DISPLAY_MESSEGE_FONT_BOLD = 1987;
    public static final short DISPLAY_MESSEGE_FONT_NAME = 1984;
    public static final short DISPLAY_MESSEGE_FONT_COLOR = 1985;
    public static final short N_NAME = 1982;
    public static final int BYTE_LEN = 200;
    public static final int VOICE_LEN = 52;
    public static Font defaultFont = new Font("Tahoma", Font.PLAIN, 11);
    public static Font smallFont = new Font("Tahoma", Font.PLAIN, 10);
    public static Font defaultFontBold = new Font("Tahoma", Font.BOLD, 11);
    public static Font defaultFontItalic = new Font("Tahoma", Font.ITALIC, 11);

    public static final String ONLINE_SYMBOL = "1";
    public static final String OFFLINE_SYMBOL = "2";
    public static final String PENDING_SYMBOL = "5";
    public static final String OFFLINE_TAG = "0";
    public static final String BUSY_SYMBOL = "3";
    public static final String AWAY_SYMBOL = "4";
    public static long maximumFile = 104857600;
    public static final short IS_OFFLINE_MESSAGE = 102;
    public static final short DATE_TIME = 101;

    public static final String VOICE_CALL_REQUEST                 = "1";
    public static final String VOICE_CALL_ACCEPT_REQUEST          = "2";
    public static final String VOICE_CALL_REJECT_REQUEST          = "3";
    public static final String VOICE_CALL_TIME_OUT_RESPONSE       = "4";

    public static final String  VOICE_CALL = "70";
    public static final String  VOICE_CALL_ACCEPTED = "71";
    public static final String  VOICE_CALL_REJECT = "72";


}
