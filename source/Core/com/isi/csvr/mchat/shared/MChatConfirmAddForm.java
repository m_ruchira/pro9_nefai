package com.isi.csvr.mchat.shared;

import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MChatConfirmAddForm extends SmartFrame {
    private JTextArea requestBanner = null;
    private JPanel addCancelRequestPane = null;
    private JPanel bannerPane = null;
    private JButton requestAdd = null;
    private JButton requestCancel = null;
    private JLabel emptyLbl = null;
    private JLabel emptyLbl_1 = null;
    public String requestMessage = null;
    public String chatID = null;

    public MChatConfirmAddForm() {
        super(true);
        try {
//            Image im1 = getToolkit().getImage("res\\mchat\\ChatIcon.png");
//            setIconImage(im1);
            Image im1 = getToolkit().getImage(MChatSettings.IMAGE_PATH + "ChatIcon.png");
            setIconImage(im1);
            setTitle(MChatLanguage.getString("ADD_CONTACT"));
            requestMessage = MChatLanguage.getString("CONFIRM_ADD");
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            if (MChatLanguage.isLTR) {
                setTitileFont(MChatMeta.defaultFont);
            }
            setDefaultImageIcon();
            setTitleColor();
        } catch (Exception e) {
        }
    }

    public void closeButtonActionPerformed() {
        rejectBuddyAction();
    }

    public void initializeComponents(String buddy, String id, String fName, String lName, String invite) {
        try {
            chatID = id;
            setSmartLayout((new FlexGridLayout(new String[]{"100%"}, new String[]{"70%", "30%"}, 1, 1)));
            bannerPane = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));

            requestBanner = new JTextArea();
            requestBanner.setEditable(false);
            requestBanner.setLineWrap(true);
            requestBanner.setWrapStyleWord(true);
            requestBanner.setText("\n");
            requestBanner.setText("\n");
            requestBanner.append(" " + buddy + " " + requestMessage);
            requestBanner.append("\n");
            requestBanner.append("\n");
            requestBanner.append(" " + MChatLanguage.getString("FNAME") + " : " + fName);
            requestBanner.append("\n");
            requestBanner.append("\n");
            requestBanner.append(" " + MChatLanguage.getString("LNAME") + " : " + lName);
            requestBanner.append("\n");
            requestBanner.append("\n");
            if (invite != null) {
                requestBanner.append(" " + MChatLanguage.getString("PERSONAL_INVITE_MESSAGE") + " : " + invite);
            }
            requestBanner.append("\n");
            requestBanner.setForeground(Color.white);
            if (MChatLanguage.isLTR) {
                requestBanner.setFont(MChatMeta.defaultFont);
            }

            requestBanner.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            bannerPane.add(requestBanner);

            addCancelRequestPane = new JPanel(new FlexGridLayout(new String[]{"25%", "25%", "25%", "25%"}, new String[]{"100%"}, 1, 1));
            addCancelRequestPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));

            emptyLbl = new JLabel();
            addCancelRequestPane.add(emptyLbl);

            requestAdd = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/add-but.jpg"));
            requestAdd.setBorder(BorderFactory.createEmptyBorder());
            requestAdd.setContentAreaFilled(false);
            requestAdd.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/add-but-up.jpg"));
            requestAdd.setFocusPainted(false);

            requestCancel = new JButton(MChatLanguage.getString("REJECT"));
            requestCancel = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/Reject-but.jpg"));
            requestCancel.setBorder(BorderFactory.createEmptyBorder());
            requestCancel.setContentAreaFilled(false);
            requestCancel.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/Reject-but-up.jpg"));
            requestCancel.setFocusPainted(false);

            emptyLbl_1 = new JLabel();
            addCancelRequestPane.add(requestAdd);
            addCancelRequestPane.add(requestCancel);
            addCancelRequestPane.setForeground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            addCancelRequestPane.setForeground(MChatMeta.MCHAT_COLOR);
            addCancelRequestPane.add(emptyLbl_1);

            getSmartContentPanel().add(bannerPane);
            getSmartContentPanel().add(addCancelRequestPane);
            Rectangle confirmRect = getGraphicsConfiguration().getBounds();
            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((int) dim.getWidth() - (this.getWidth() + 800), (int) dim.getHeight() - (this.getHeight() + 600));
            setVisible(true);
            setSize(500, 240);
            setMinSize(500, 240);
            setResizable(false);

            requestAdd.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        addBuddyAction();
                    } catch (Exception e1) {
                    }
                }
            });
            requestCancel.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    rejectBuddyAction();
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void addBuddyAction() {
        try {
            MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createAddNewRequest(chatID));
            this.dispose();
        } catch (Exception e1) {
        }
    }

    private void rejectBuddyAction() {
        try {
            MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createRejectNewRequest(chatID));
            this.dispose();
        } catch (Exception e1) {
        }
    }
}
