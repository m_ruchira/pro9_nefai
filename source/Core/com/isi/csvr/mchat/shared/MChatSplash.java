package com.isi.csvr.mchat.shared;

import com.isi.csvr.mchat.client.*;
import com.isi.csvr.mchat.datastore.MChatDataStore;

import javax.swing.*;

public class MChatSplash extends Thread implements Disposable {
    JLabel label = null;
    boolean isActive = true;
    int i = 0;
    private static MChatSplash self = null;
    private boolean isLoading = false;

    public MChatSplash() {
        try {
            start();
        } catch (Exception e) {
        }
    }

    public static MChatSplash getSharedInstance(){
        if(self == null){
            self = new MChatSplash();
        }
        return self;
    }

    public synchronized void setLoadingImage(boolean flag){
        isLoading = flag;
        if(isLoading){

        }
    }

    public void reset(){
        try {
            stop();
        } catch (Exception e) {
        }
        self = null;
    }

    public void run() {
        String message = null;
        while (isActive) {
            MChatSearchForm.getSharedInstance().setVisible(false);
            MChatChatForm.getSharedInstance().setVisible(false);
            MChat.getSharedInstance().showLoginPanel();
            if (i <= 3) {
                i++;
                message = MChatLanguage.getString("RECONNECT")+" "+(4-i);
                MChatLoginForm.getSharedInstance().giveMsg(message,1);
            } else {
                System.out.println("<DEBUG> Automatic Login");
                i = 0;
                if (MChatClient.getSharedInstance().isConnected()) {
                    if ((MChatMeta.MUBASHER_ID != null) && (MChatMeta.DISPLAY_ID != null)) {
                        MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.loginUserRequest(MChatMeta.MUBASHER_ID, MChatMeta.DISPLAY_ID, MChatSettings.APP_MODE));
                    }
                    try {
                        synchronized (this) {
                            wait();
                        }
                    } catch (Exception e) {
                       e.printStackTrace();
                    }
                }
            }
            try {
                sleep(2000);
            } catch (InterruptedException e) {

            }
        }
    }

    public void destroySelf() {
        try {
            stop();
        } catch (Exception e) {
            e.printStackTrace();
        }
        self = null;
    }
}
