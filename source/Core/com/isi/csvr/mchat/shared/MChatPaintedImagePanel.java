package com.isi.csvr.mchat.shared;

import javax.swing.*;
import javax.imageio.ImageIO;
import java.awt.*;
import java.awt.image.BufferedImage;
import java.awt.geom.AffineTransform;
import java.io.File;
import java.io.IOException;

public class MChatPaintedImagePanel extends JPanel {
    private String  imagePath = null;
    private double scale = 1;
    private BufferedImage image;

    public MChatPaintedImagePanel(String  imagePath, double scale){
        this.imagePath = imagePath;
        this.scale = scale;
        try {
            image = ImageIO.read(new File(this.imagePath));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    protected void paintComponent(Graphics g) {
        try {
            super.paintComponent(g);
            Graphics2D g2 = (Graphics2D) g;
            g2.setRenderingHint(RenderingHints.KEY_INTERPOLATION,
                    RenderingHints.VALUE_INTERPOLATION_BICUBIC);
            int w = getWidth();
            int h = getHeight();
            int iw = image.getWidth();
            int ih = image.getHeight();
            double x = (w - scale * iw) / 2;
            double y = (h - scale * ih) / 2;
            AffineTransform at = AffineTransform.getTranslateInstance(x, y);
            at.scale(scale, scale);
            g2.drawRenderedImage(image, at);
            repaint();
        } catch (Exception e) {
        }
    }
}
