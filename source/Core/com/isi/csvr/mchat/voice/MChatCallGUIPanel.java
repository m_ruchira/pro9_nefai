package com.isi.csvr.mchat.voice;

import com.isi.util.FlexGridLayout;
import com.isi.csvr.mchat.shared.MChatLanguage;
import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.mchat.shared.MChatSettings;
import com.isi.csvr.shared.GUISettings;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MChatCallGUIPanel extends JPanel {

    private JPanel iconPanel = null;
    private JPanel fileButtonpanel = null;
    private JTextPane displayArea = null;
    private JButton noButton = null;
    private String source = null;
    private Icon icon = null;
    private JLabel jlabel = null;


    public MChatCallGUIPanel(String fileSource) {
        try {
            this.source = fileSource;

            setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"15", "30"}, 1, 1));
            setOpaque(false);

            iconPanel = new JPanel();
            iconPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 1, 1));
            iconPanel.setOpaque(false);

            icon = new ImageIcon("images/mchat/mchat_en/Calling.gif");
            jlabel = new JLabel(MChatLanguage.getString("CALLING_2"));
            jlabel.setIcon(icon);
            if (MChatLanguage.isLTR) {
                jlabel.setFont(MChatMeta.defaultFont);
            }

            iconPanel.add(jlabel);

            noButton = new JButton("<HTML><u>" + MChatLanguage.getString("HANG_UP") + "</u><HTML>");
            noButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    jlabel.setText(MChatLanguage.getString("CALL_END"));
                    jlabel.setForeground(Color.red);
                    jlabel.setIcon(null);
                    fileButtonpanel.setVisible(false);
                }
            });
            noButton.setBorder(BorderFactory.createEmptyBorder());
            noButton.setContentAreaFilled(false);
            noButton.setForeground(Color.blue);
            noButton.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
            if (MChatLanguage.isLTR) {
                noButton.setFont(MChatMeta.defaultFont);
            }

            fileButtonpanel = new JPanel();
            fileButtonpanel.setLayout(new FlexGridLayout(new String[]{"70"}, new String[]{"100%"}, 1, 1));
            fileButtonpanel.setOpaque(false);
            fileButtonpanel.add(noButton);

            add(iconPanel);
            add(fileButtonpanel);
            GUISettings.applyOrientation(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JLabel getJlabel() {
        return jlabel;
    }


    public void setDisplayArea(JTextPane displayArea) {
        this.displayArea = displayArea;
    }

    public JTextPane getDisplayArea() {
        return displayArea;
    }
}


