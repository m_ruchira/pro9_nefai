package com.isi.csvr.mchat.datastore;

import com.isi.csvr.mchat.client.MChatChatWindow;
import com.isi.csvr.mchat.client.MChatMessageProcessor;
import com.isi.csvr.mchat.shared.MChatQueue;
import com.isi.csvr.mchat.shared.Disposable;

import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyleConstants;
import java.awt.*;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Vector;


public class MChatDataStore {
    private static MChatQueue inQue;
    private static MChatQueue outQue;
    private static MChatDataStore self = null;
    private static MChatMessageProcessor processor;
    private ArrayList<String> txtTemplates = null;
    private static Hashtable<String, MChatChatWindow> chatWindowTable = null;
    public static SimpleAttributeSet chatTextAttributes = null;
    private static String inputText = null;
    public static Vector<Disposable> disposableObjects = null;

    public MChatDataStore() {
        try {
            inQue = new MChatQueue();
            outQue = new MChatQueue();
            processor = MChatMessageProcessor.getSharedInstance();
            chatTextAttributes = new SimpleAttributeSet();
            chatWindowTable = new Hashtable<String, MChatChatWindow>();
            StyleConstants.setForeground(chatTextAttributes, Color.gray);
            StyleConstants.setItalic(chatTextAttributes, true);
            StyleConstants.setFontFamily(chatTextAttributes, "Tahoma");
            StyleConstants.setFontSize(chatTextAttributes, 13);
            disposableObjects = new Vector<Disposable>();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void addDisposableListener(Disposable object) {
        disposableObjects.add(object);
    }

    public void disposeAll() {
        for (Disposable dis : disposableObjects) {
            try {
                dis.destroySelf();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public String getInputText() {
        return inputText;
    }

    public void resetInputText() {
        inputText = "";
    }

    public void addInputText(String text) {
        inputText = text;
    }

    public static void addChatWindow(String user, MChatChatWindow cw) {
        try {
            chatWindowTable.put(user, cw);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*   public void addFileServerSender(String key,FileServerSender sender){
            try {
                fileSenderTable.put(key,sender);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        public FileServerSender getFileSender(String key){
           return fileSenderTable.get(key);
        }
    */
    public static void removeChatWindow(String user) {
        try {
            chatWindowTable.remove(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static MChatChatWindow getChatWindow(String user) {
        return chatWindowTable.get(user);
    }

    public MChatQueue getInQue() {
        return inQue;
    }

    public MChatQueue getOutQue() {
        return outQue;
    }

    public static MChatDataStore getSharedInstance() {
        if (self == null) {
            self = new MChatDataStore();
        }
        return self;
    }

    public void addToInque(String message) {
        inQue.add(message);
        notifyObservers();
    }

    public String getFromInque() {
        if (inQue.size() > 0) {
            return ((String) inQue.pop());
        }
        return null;
    }

    private void notifyObservers() {
        processor.processMessages();
    }

    public ArrayList<String> getTxtTemplates() {
        return txtTemplates;
    }

    public void loadTextTemplates() {
        /*   txtTemplates = new ArrayList();
        FileInputStream fIn = null;
        try {
            fIn = new FileInputStream("images/mchat/mchat_en/chatTxtTemplates_"+com.isi.csvr.shared.Language.getSelectedLanguage()+".txt");
        } catch (FileNotFoundException e) {
            try {
                fIn = new FileInputStream("images/mchat/mchat_en/chatTxtTemplates_EN.txt");
            } catch (FileNotFoundException e1) {
                e1.printStackTrace();
            }
            e.printStackTrace();
        }

        int ch;
        StringBuffer buffer = new StringBuffer();
        String line;

        while (true) {
            try {
                ch = fIn.read();
                if (ch == -1) {
                    fIn.close();
                    break;
                } else if ((char) ch == 'Z') {
                    fIn.close();
                    break;
                } else if ((char) ch != '\n') {
                    buffer.append((char) ch);
                } else {
                    line = buffer.toString();
                    try {
                        txtTemplates.add(line);
                    } catch (Exception e) {
                        System.out.println("<ERROR> Text Templates file line error = " + line);
                    }
                    buffer = new StringBuffer();
                }
            } catch (IOException e) {
                e.printStackTrace();
            }
        }*/
    }

    public void size() {
//        System.out.println(chatWindowTable.size());
    }

    public static void destroy(){
        chatWindowTable = null;
        chatTextAttributes = null;
        processor = null;
        inQue = null;
        outQue = null;
        disposableObjects= null;
        self = null;
    }
}
