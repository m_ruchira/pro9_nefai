package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class MChatRemoveContactForm extends SmartFrame {
    private JPanel buttonPane = null;
    private JPanel labelPane=null;
    private JButton closeButton = null;
    private JButton cancelButton=null;
    private JLabel messageLabel = null;
    public String requestMessage = null;
    private String contact=null;
    private String id = null;

    public MChatRemoveContactForm(String contact, String id) {
        super(true);
        try {
            setTitle(MChatLanguage.getString("REMOVE_CONTACT"));
            setTitleColor();
            setTitileFont(MChatMeta.defaultFont);
            setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
            setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));
            this.contact = contact;
            this.id = id;
            initializeComponents();
        } catch (Exception e) {
        }
    }

    public void initializeComponents() {
        try {
            Image im1 = getToolkit().getImage(MChatSettings.IMAGE_PATH + "ChatIcon.png");
            setIconImage(im1);
            setSmartLayout((new FlexGridLayout(new String[]{"100%"}, new String[]{"65", "30", "100%"}, 0, 0)));

            messageLabel = new JLabel("  "+MChatLanguage.getString("REMOVE") + " " + contact + " " + MChatLanguage.getString("FROM_CONTACT_LIST"));
            messageLabel.setForeground(Color.white);

            if (MChatLanguage.isLTR) {
                messageLabel.setFont(MChatMeta.defaultFont);
            }

            labelPane = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
            labelPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));

            labelPane.add(messageLabel);

            buttonPane = new JPanel(new FlexGridLayout(new String[]{"100%","80", "5", "80"}, new String[]{"100%"}, 0, 0));
            buttonPane.setBackground(Theme.getColor("MCHAT_FOOTER_COLOR"));

            closeButton = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/ok-but.jpg"));
            closeButton.setBorder(BorderFactory.createEmptyBorder());
            closeButton.setContentAreaFilled(false);
            closeButton.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/ok-but-up.jpg"));
            closeButton.setFocusPainted(false);

            cancelButton = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/cancel-but.jpg"));
            cancelButton.setBorder(BorderFactory.createEmptyBorder());
            cancelButton.setContentAreaFilled(false);
            cancelButton.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/cancel-but-up.jpg"));
            cancelButton.setFocusPainted(false);

            buttonPane.add(new JLabel());
            buttonPane.add(closeButton);
            buttonPane.add(new JLabel());
            buttonPane.add(cancelButton);


            getSmartContentPanel().add(labelPane);
            getSmartContentPanel().add(buttonPane);

            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
            setLocation((int) dim.getWidth() - (this.getWidth() + 440), (int) dim.getHeight() - (this.getHeight() + 340));
            setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            setVisible(true);
            setMinSize(420, 140);
            setSize(420, 140);
            setResizable(true);

            closeButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        if (!contact.equalsIgnoreCase("")) {
                             MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createRemoveContactRequest(id));
                            MChatChatWindow.closeTab();
                         }
                        dispose();
                    } catch (Exception e1) {
                    }
                }
            });

            cancelButton.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        dispose();
                    } catch (Exception e1) {
                    }
                }
            });
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

