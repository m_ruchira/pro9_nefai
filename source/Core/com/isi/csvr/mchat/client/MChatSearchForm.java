package com.isi.csvr.mchat.client;

import com.isi.csvr.mchat.datastore.MChatDataStore;
import com.isi.csvr.mchat.datastore.MChatContactObject;
import com.isi.csvr.mchat.shared.*;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableColumn;
import javax.swing.table.TableCellRenderer;
import java.awt.*;
import java.awt.event.*;
import java.util.Enumeration;

public class MChatSearchForm extends SmartFrame implements Disposable{
    private JPanel searchPanel = null;
    private JRadioButton searchID;
    private JRadioButton searchName;
    private JRadioButton searchSName;
    public static JTextField searchMessage;
    private JButton doSearch;
    private JPanel textBoxPane;
    private JPanel searchPane;
    private JPanel tablePane;
    private JPanel addCancelPane;
    private JPanel searchStringPanel;
    private ButtonGroup searchBy;
    private JScrollPane scrollSearch;
    private JProgressBar searchProgress;
    public JPanel progressPanel;
    private JButton searchAdd;
    private JButton searchBack;
    public static JTable tableShowContacts;
    private DefaultTableModel contactSearchModel = null;
    private JTextArea errorTxtArea = null;
    private static MChatSearchForm self = null;
    private boolean searchInProgress;
    public static JLabel onlineImage = new JLabel();
    public static JLabel id = new JLabel();
    public static JLabel firstName = new JLabel();
    public static JLabel lastName = new JLabel();


    public MChatSearchForm() {
        super(true);
        try {
            setTitle(MChatLanguage.getString("SEARCH"));
            setSize(450, 300);
            setMinSize(450, 300);
            setLocationRelativeTo(com.isi.csvr.Client.getInstance().getFrame());
            setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
            Image im1 = getToolkit().getImage(MChatSettings.IMAGE_PATH + "ChatIcon.png");
            setIconImage(im1);
            setSmartLayout(new BorderLayout());
            setTitileFont(MChatMeta.defaultFontBold);
            setTitleColor();
            setDefaultImageIcon();
            searchPanel = new JPanel();
            searchPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"40", "0", "100%", "35", "3"}, 2, 2));
            searchPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

            errorTxtArea = new JTextArea("");
            errorTxtArea.setLineWrap(true);
            errorTxtArea.setEditable(false);
            errorTxtArea.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            final ImageIcon icon = new ImageIcon(MChatSettings.IMAGE_PATH + "/loader.gif");
            final String searching = MChatLanguage.getString("SEARCHING");

            errorTxtArea = new JTextArea("") {
                public void paint(Graphics g) {
                    setForeground(Color.WHITE);
                    if (MChatLanguage.isLTR) {
                        setFont(MChatMeta.defaultFont);
                    }
                    super.paint(g);
                    if (isSearchInProgress()) {
                        FontMetrics fontMetrics = searchMessage.getFontMetrics(getFont());
                        g.drawString(searching, (fontMetrics.stringWidth(searching)) / 2,
                                getHeight() / 2);
                        g.drawImage(icon.getImage(),
                                (fontMetrics.stringWidth(searching)) / 2 + fontMetrics.stringWidth(searching) + 10,
                                (getHeight() - icon.getImage().getHeight(this)) / 2, this);
                        fontMetrics = null;
                    }
                }
            };
            errorTxtArea.setLineWrap(true);
            errorTxtArea.setEditable(false);
            errorTxtArea.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            errorTxtArea.setText("");

            contactSearchModel = new DefaultTableModel();

            searchPane = new JPanel((new FlexGridLayout(new String[]{"25%", "40%", "100%"}, new String[]{"100%"}, 0, 0)));
            searchPane.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

            searchID = new JRadioButton(MChatLanguage.getString("ID"), true);
            searchName = new JRadioButton(MChatLanguage.getString("FNAME"));
            searchSName = new JRadioButton(MChatLanguage.getString("LNAME"));
            searchID.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            searchName.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            searchSName.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            searchBy = new ButtonGroup();

            searchPane.add(searchID);
            searchPane.add(searchName);
            searchPane.add(searchSName);
            searchBy.add(searchName);
            searchBy.add(searchID);
            searchBy.add(searchSName);

            textBoxPane = new JPanel((new FlexGridLayout(new String[]{"5", "155", "100", "100%"}, new String[]{"100%"}, 0, 5)));
            textBoxPane.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

            searchMessage = new JTextField();
            searchMessage.setText("");
            searchMessage.setBorder(BorderFactory.createEmptyBorder());
            searchMessage.setBackground(Color.white);
            searchMessage.setForeground(Color.black);

            searchStringPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"5","100%","5"}, 0, 0));
            searchStringPanel.setOpaque(false);
            searchStringPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

            JPanel dummyPanel = new JPanel(new FlexGridLayout(new String[]{"6", "100%"}, new String[]{"100%"}, 0, 0));
            dummyPanel.setOpaque(false);
            dummyPanel.setBackground(Color.white);

            JTextField dummyField = new JTextField();
            dummyField.setBackground(Color.white);
            dummyField.setBorder(BorderFactory.createEmptyBorder());
            dummyField.setEditable(false);

            dummyPanel.add(dummyField);
            dummyPanel.add(searchMessage);

            searchStringPanel.add(new JLabel());
            searchStringPanel.add(dummyPanel);
            searchStringPanel.add(new JLabel());

            textBoxPane.add(new JLabel());
            textBoxPane.add(searchStringPanel);

            try {
                doSearch = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/search-but.jpg"));
                doSearch.setContentAreaFilled(false);
                doSearch.setBorder(BorderFactory.createEmptyBorder());
                doSearch.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/search-but.jpg"));

            } catch (Exception e) {
            }
            textBoxPane.add(doSearch);
            textBoxPane.add(errorTxtArea);
            JLabel empty1 = new JLabel(MChatLanguage.getString("SEARCH"));
//            textBoxPane.add(empty1);

            progressPanel = new JPanel((new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0)));
            searchProgress = new JProgressBar();
            searchProgress.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            searchProgress.setIndeterminate(true);
            progressPanel.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

            tablePane = new JPanel((new FlexGridLayout(new String[]{"5","100%","5"}, new String[]{"100%"}, 0, 0)));
            tablePane.setBackground(Theme.getColor("MCHAT_BG_COLOR"));

            scrollSearch = new JScrollPane();
            scrollSearch.getViewport().setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
            scrollSearch.setBorder(BorderFactory.createEmptyBorder());
            scrollSearch.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            tablePane.add(new JLabel());
            tablePane.add(scrollSearch);
            tablePane.add(new JLabel());

            tableShowContacts = new JTable(contactSearchModel) {
                public boolean isCellEditable(int row, int col) {
                    return false;
                }
            };
           /*  if(tableShowContacts.getRowCount() > 0){
                 tableShowContacts.removeAll();
                 tableShowContacts.repaint();
             }*/
            Dimension d = new Dimension(0, 0);
            tableShowContacts.setShowGrid(false);
            tableShowContacts.setRowHeight(20);
            tableShowContacts.setIntercellSpacing(d);
            tableShowContacts.getTableHeader().setBackground(Theme.getColor("MCHAT_HEADER_BACKGROUND_COLOR"));

            TableColumn proIDCol = null;
            TableColumn statusImage = null;
            try {
                contactSearchModel.setDataVector(new Object[0][0], new Object[]{MChatLanguage.getString("STATUS"), MChatLanguage.getString("ID"), MChatLanguage.getString("FNAME"), MChatLanguage.getString("LNAME"), MChatLanguage.getString("PRO_ID")});
                tableShowContacts.setBackground(Color.WHITE);
                tableShowContacts.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
                tableShowContacts.getTableHeader().setReorderingAllowed(false);
                if (MChatLanguage.isLTR) {
                    tableShowContacts.setFont(MChatMeta.defaultFont);
                    tableShowContacts.getTableHeader().setFont(MChatMeta.defaultFont);
                    tableShowContacts.getTableHeader().setForeground(Color.WHITE);
                }
                proIDCol = tableShowContacts.getColumnModel().getColumn(4);
                proIDCol.setMinWidth(0);
                proIDCol.setMaxWidth(0);
                proIDCol.setResizable(false);

                statusImage = tableShowContacts.getColumnModel().getColumn(0);
                statusImage.setMinWidth(0);
                statusImage.setMaxWidth(50);
                statusImage.setResizable(false);
            } catch (Exception e) {
                e.printStackTrace();
            }

            tableShowContacts.addMouseListener(new MouseListener() {
                public void mouseClicked(MouseEvent e) {
                    if (e.isMetaDown()) {
                        try {
                            JPopupMenu popup = new JPopupMenu();
                            popup.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
                            popup.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR"));
                            JMenuItem menuItem = new JMenuItem(MChatLanguage.getString("ADD"));
                            if (MChatLanguage.isLTR) {
                                menuItem.setFont(MChatMeta.defaultFont);
                            }
                            menuItem.addActionListener(new ActionListener() {
                                public void actionPerformed(ActionEvent e) {
                                    sendPersonalInvitation();
                                }
                            });
                            popup.add(menuItem);
                            MChat.getSharedInstance().add(popup);
                            popup.setLocation(e.getLocationOnScreen());
                            int selectedRow = tableShowContacts.getSelectedRow();
                            if ((selectedRow != -1)) {
                                popup.show(tableShowContacts, e.getX(), e.getY());
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    } else if (e.getClickCount() == 2) {
                        sendPersonalInvitation();
                    }
                }

                public void mousePressed(MouseEvent e) {
                }

                public void mouseReleased(MouseEvent e) {
                }

                public void mouseEntered(MouseEvent e) {
                }

                public void mouseExited(MouseEvent e) {
                }
            });

            try {
                scrollSearch.add(tableShowContacts);
                scrollSearch.setViewportView(tableShowContacts);
            } catch (Exception e) {
                e.printStackTrace();
            }

            addCancelPane = new JPanel((new FlexGridLayout(new String[]{"100%", "80", "3", "80"}, new String[]{"100%"}, 1, 1)));
            addCancelPane.setBackground(Theme.getColor("MCHAT_BG_COLOR"));
            searchAdd = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/add-but.jpg"));
            searchAdd.setBorder(BorderFactory.createEmptyBorder());
            searchAdd.setContentAreaFilled(false);
            searchAdd.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/add-but-up.jpg"));
            searchAdd.setFocusPainted(false);

            searchAdd.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    sendPersonalInvitation();
                }
            });

            searchBack = new JButton(new ImageIcon(MChatSettings.IMAGE_PATH + "/close-but.jpg"));
            searchBack.setContentAreaFilled(false);
            searchBack.setBorder(BorderFactory.createEmptyBorder());
            searchBack.setRolloverIcon(new ImageIcon(MChatSettings.IMAGE_PATH + "/close-but-up.jpg"));
            searchBack.setFocusPainted(false);

            searchBack.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setVisible(false);
                }
            });

//            JLabel emptylbl = new JLabel();
//            addCancelPane.add(emptylbl);
            addCancelPane.add(new JLabel());
            addCancelPane.add(searchAdd);
            addCancelPane.add(new JLabel());
            addCancelPane.add(searchBack);

            doSearch.setFocusPainted(false);
            searchMessage.addKeyListener(new KeyListener() {
                public void keyTyped(KeyEvent e) {

                }

                public void keyPressed(KeyEvent e) {
                    if (e.getKeyCode() == KeyEvent.VK_ENTER) {
                        try {
                            searchProgress.setVisible(true);
                            errorTxtArea.setText("");
                            String subType = "";
                            if (searchID.isSelected()) {
                                subType = "1";
                            } else if (searchName.isSelected()) {
                                subType = "2";
                            } else if (searchSName.isSelected()) {
                                subType = "3";
                            }
                            subType = "0"; //first release
                            String text = searchMessage.getText().trim();
                            if (!text.equalsIgnoreCase("")) {
//                            progressPanel.add(searchProgress);
                                setSearchInProgress(true);
                                errorTxtArea.repaint();
//                            setVisible(true);
                                MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.searchContacts(subType, text));
                            } else {
                                giveMsg(MChatLanguage.getString("SEARCH_EMPTY"), 1);
                            }
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }

                }

                public void keyReleased(KeyEvent e) {

                }
            });
            doSearch.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    try {
                        searchProgress.setVisible(true);
                        errorTxtArea.setText("");
                        String subType = "";
                        if (searchID.isSelected()) {
                            subType = "1";
                        } else if (searchName.isSelected()) {
                            subType = "2";
                        } else if (searchSName.isSelected()) {
                            subType = "3";
                        }
                        subType = "0"; //first release
                        String text = searchMessage.getText().trim();
                        if (!text.equalsIgnoreCase("")) {
//                            progressPanel.add(searchProgress);
                            setSearchInProgress(true);
                            errorTxtArea.repaint();
//                            setVisible(true);
                            MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.searchContacts(subType, text));
                        } else {
                            giveMsg(MChatLanguage.getString("SEARCH_EMPTY"), 1);
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }

            });

            doSearch.setToolTipText(MChatLanguage.getString("SEARCH_NEW_CONTACTS"));
            searchAdd.setToolTipText(MChatLanguage.getString("ADD_CONTACT"));
            searchBack.setToolTipText(MChatLanguage.getString("CLOSE_SEARCH_FORM"));

            try {
                searchPanel.add(textBoxPane);
                searchPanel.add(progressPanel);
                searchPanel.add(tablePane);
                searchPanel.add(addCancelPane);
                searchPanel.add(new JLabel());
            } catch (Exception e) {
                e.printStackTrace();
            }

/*
            try {
                GUISettings.applyOrientation(searchPanel);
            } catch (Exception e) {
            }
*/

            getSmartContentPanel().add(searchPanel, BorderLayout.CENTER);
            GUISettings.applyOrientation(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void emptyRenderer() {
        try {
            final JPanel comp = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
            comp.add(onlineImage);
            comp.add(id);
            comp.add(firstName);
            comp.add(lastName);

            TableCellRenderer renderer = new TableCellRenderer() {
                public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
                    try {
                        tableShowContacts = table;
                        tableShowContacts.getTableHeader().setBackground(Theme.getColor("MCHAT_HEADER_BACKGROUND_COLOR"));
                        tableShowContacts.getTableHeader().setForeground(Color.WHITE);
                        tableShowContacts.setShowHorizontalLines(true);
                        tableShowContacts.setShowGrid(true);
                        tableShowContacts.setGridColor(Color.white);
                        comp.removeAll();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    comp.setBorder(BorderFactory.createEmptyBorder());
                    try {
                        if (isSelected && column == 1) {
                            JLabel label = new JLabel((String) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.LEADING);
                            label.setFont(MChatMeta.defaultFont);
                            label.setForeground(Color.black);
                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            return comp;
                        } else if (!isSelected && column == 1) {
                            JLabel label = new JLabel((String) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.LEADING);
                            label.setFont(MChatMeta.defaultFont);
                            label.setForeground(Color.black);
//                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            return comp;
                        } else if (isSelected && column == 0) {
                            JLabel label = new JLabel((ImageIcon) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.CENTER);
                            label.setFont(MChatMeta.defaultFont);
//                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            return comp;
                        } else if (!isSelected && column == 0) {
                            JLabel label = new JLabel((ImageIcon) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.CENTER);
                            label.setFont(MChatMeta.defaultFont);
//                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            return comp;
                        } else if (!isSelected && column == 2) {
                            JLabel label = new JLabel((String) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.LEADING);
                            label.setFont(MChatMeta.defaultFont);
//                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            return comp;
                        } else if (isSelected && column == 2) {
                            JLabel label = new JLabel((String) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.LEADING);
                            label.setForeground(Theme.getColor("MCHAT_SELECTED_FONT_COLOR"));
                            label.setFont(MChatMeta.defaultFont);
//                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            return comp;
                        } else if (!isSelected && column == 3) {
                            JLabel label = new JLabel((String) table.getValueAt(row, column));
//                            label.setHorizontalAlignment(JLabel.LEADING);
                            label.setFont(MChatMeta.defaultFont);
//                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            return comp;
                        } else if (isSelected && column == 3) {
                            JLabel label = new JLabel((String) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.LEADING);
                            label.setForeground(Theme.getColor("MCHAT_SELECTED_FONT_COLOR"));
                            label.setFont(MChatMeta.defaultFont);
//                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            return comp;
                        } else if (!isSelected && column == 4) {
                            JLabel label = new JLabel((String) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.LEADING);
                            label.setFont(MChatMeta.defaultFont);
//                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            return comp;
                        } else if (isSelected && column == 4) {
                            JLabel label = new JLabel((String) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.LEADING);
                            label.setForeground(Theme.getColor("MCHAT_SELECTED_FONT_COLOR"));
                            label.setFont(MChatMeta.defaultFont);
//                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            return comp;
                        } else {
                            JLabel label = new JLabel((String) table.getValueAt(row, column));
                            label.setHorizontalAlignment(JLabel.LEADING);
                            label.setForeground(Color.black);
                            label.setFont(MChatMeta.defaultFont);
//                            comp.add(label);
                            comp.setBackground(Theme.getColor("MCHAT_CONTACT_LIST_COLOR"));
                            return comp;
                        }
                    } catch (Exception e) {
                        return new JPanel();
                    }
                }
            };
//                    renderer.setHorizontalAlignment(JLabel.RIGHT);
//                    renderer.setFont(Meta.defaultFont);
            tableShowContacts.getColumnModel().getColumn(0).setCellRenderer(renderer);
            tableShowContacts.getColumnModel().getColumn(1).setCellRenderer(renderer);
            tableShowContacts.getColumnModel().getColumn(2).setCellRenderer(renderer);
            tableShowContacts.getColumnModel().getColumn(3).setCellRenderer(renderer);
            tableShowContacts.getColumnModel().getColumn(4).setCellRenderer(renderer);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

/*
    private void add() {
        try {
            errorTxtArea.setText("");
            String newID = null;
            String nickName = null;
            String lName = null;
            String fName = null;
            try {
                newID = (String) tableShowContacts.getValueAt(tableShowContacts.getSelectedRow(), 4);
                nickName = (String) tableShowContacts.getValueAt(tableShowContacts.getSelectedRow(), 1);
                lName = (String) tableShowContacts.getValueAt(tableShowContacts.getSelectedRow(), 2);
                fName = (String) tableShowContacts.getValueAt(tableShowContacts.getSelectedRow(), 3);
                addNewContact(newID, nickName, lName, fName);
            } catch (Exception e1) {
                if (tableShowContacts.getRowCount() == 0) {
                    giveMsg(MChatLanguage.getString("SEARCH_FOR_CONTACTS"), 1);
                } else {
                    giveMsg(MChatLanguage.getString("SELECT_CONTACTS"), 1);
                }
            }
        } catch (Exception e) {
        }
    }
*/

    public static MChatSearchForm getSharedInstance() {
        if (self == null) {
            self = new MChatSearchForm();
        }
        return self;
    }

    public boolean isSearchInProgress() {
        return searchInProgress;
    }

    public void setSearchInProgress(boolean searchInProgress) {
        this.searchInProgress = searchInProgress;
    }

/*    public void addNewContact(String newproID, String nickName, String lName, String fName) {
        try {
            Enumeration e1 = MChat.contactObjects.keys();
            boolean isAlreadyContact = false;
            while (e1.hasMoreElements()) {
                if (newproID.equals(e1.nextElement())) {
                    isAlreadyContact = true;
                    break;
                }
            }
            if ((!isAlreadyContact)
                *//*&& (!newproID.equalsIgnoreCase(Meta.MUBASHER_ID))*//*) {
//                if (!pendingAdds.contains(newproID)) {
                MChatDataStore.getSharedInstance().getOutQue().add(MChatSharedMethods.createAddContactRequest(newproID));
                giveMsg(MChatLanguage.getString("SUCCESSFULLY_ADD_CONTACTS"), 1);
                try {
                    MChatContactObject MChatContact = new MChatContactObject();
                    MChatContact.userID = newproID;
                    if (MChatContact.userID.equalsIgnoreCase("null")) {
                        return;
                    }
                    MChatContact.nickName = nickName;
                    MChatContact.status = "0";
                    MChatContact.lName = lName;
                    MChatContact.fName = fName;
                    MChatContact.status = "0";
                    MChatContact.personalMessage = "";
                    MChat.getSharedInstance().contactObjects.put(MChatContact.userID, MChatContact);
                    MChat.contactPanel.removeAll();
                    MChat.getSharedInstance().processContactList();
                    MChat.getSharedInstance().populateContacts();
                    MChat.contactPanel.repaint();
                    MChat.getSharedInstance().setVisible(true);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                giveMsg(MChatLanguage.getString("ADD_MESSEGE_SENT"), 1);
            } else {
                giveMsg(MChatLanguage.getString("CONTACT_EXIST"), 1);
            }
        } catch (Exception e) {
        }
    }*/

    public void giveMsg(String errorMsg, int msgType) {
        try {
            if (msgType == 1) {
                errorTxtArea.setForeground(Color.RED);
            } else {
                errorTxtArea.setForeground(Color.BLACK);
            }
            errorTxtArea.setText(errorMsg);
        } catch (Exception e) {

        }
    }

    public DefaultTableModel getSearchTableModel() {
        return contactSearchModel;
    }

    public JTable getSearchResultTable() {
        return tableShowContacts;
    }

    public JProgressBar getProgressBar() {
        return searchProgress;
    }

    private void sendPersonalInvitation() {
        try {
            Enumeration e1 = MChat.contactObjects.keys();
            boolean isAlreadyContact = false;
            String newProID = (String) tableShowContacts.getValueAt(tableShowContacts.getSelectedRow(), 4);
            while (e1.hasMoreElements()) {
                if (newProID.equals(e1.nextElement())) {
                    isAlreadyContact = true;
                    break;
                }
            }
            if ((!isAlreadyContact)) {
                String newID = null;
                String nickName = null;
                String fName = null;
                String lName = null;
                try {
                    newID = (String) tableShowContacts.getValueAt(tableShowContacts.getSelectedRow(), 4);
                    nickName = (String) tableShowContacts.getValueAt(tableShowContacts.getSelectedRow(), 1);
                    fName = (String) tableShowContacts.getValueAt(tableShowContacts.getSelectedRow(), 1);
                    lName = (String) tableShowContacts.getValueAt(tableShowContacts.getSelectedRow(), 1);
                    MChatPersonalInvitationForm in = new MChatPersonalInvitationForm();
                    in.initializeComponents(newID, nickName, fName, lName);
                } catch (Exception e2) {
                    e2.printStackTrace();
                }
            } else {
                giveMsg(MChatLanguage.getString("CONTACT_EXIST"), 1);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void destroySelf() {
        searchMessage = null;
        tableShowContacts = null;
        onlineImage = null;
        id = null;
        firstName = null;
        lastName = null;
        self = null;
    }
}
