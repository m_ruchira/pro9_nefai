package com.isi.csvr.mchat.history;

import javax.swing.table.AbstractTableModel;
import javax.swing.*;
import java.util.Vector;


public class MChatHistoryDataSet extends AbstractTableModel {
    private static final int DATE = 0;
    private static final int TIME = 1;
    private static final int FROM = 2;
    private static final int TO = 3;
    private static final int MESSAGE = 4;
    public Vector colNames = null;
    public Vector colDataStore = null;
    public Vector colSettings = null;
    public MChatHistoryColumnData colData = new MChatHistoryColumnData();
    public String[] col_Names;
    public Class[] colTypes;
    public int rowCount = 0;
    public int colCount = 0;
    public int columnCount = 0;

    public MChatHistoryDataSet() {
        super();
    }

    public void colSettings(Vector col) {
        colSettings = col;
    }

    public void setDataVector(Vector vec) {
        colDataStore = vec;
//        System.out.println("Inner Size " + colDataStore.size());
    }


    public void loadColumnNames() {
        try {
            this.setColumnName();
            col_Names = new String[getColCount()];
            for (int p = 0; p < col_Names.length; p++) {
                col_Names[p] = ((MChatHistoryTableSettings) (getColumnName().get(p))).getColName();
            }
            this.loadColumnTypes();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void loadColumnTypes() {
        try {
            colTypes = new Class[getColCount()];
            for (int j = 0; j < colTypes.length; j++) {
                colTypes[0] = String.class;
                colTypes[1] = String.class;
                colTypes[2] = String.class;
                colTypes[3] = String.class;
                colTypes[4] = JTextArea.class;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getColumnCount() {
        return columnCount;
    }

    public int getRowCount() {
        return rowCount = colDataStore.size();
    }

    public synchronized void setValueAt(Object value, int row, int col) {
        try {
            MChatHistoryDataStore historyData = (MChatHistoryDataStore) (colDataStore.elementAt(row));
            switch (col) {
                case DATE:
                    historyData.setData((String) value);
                    break;
                case TIME:
                    historyData.setTime((String) value);
                    break;
                case FROM:
                    historyData.setPersonFrom((String) value);
                    break;
                case TO:
                    historyData.setPersonTo((String) value);
                    break;
                case MESSAGE:
                    historyData.setMessage((JTextArea) value);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getColumnName(int col) {
        return col_Names[col];
    }

    public Class getColumnClass(int col) {
        return colTypes[col];
    }

    public synchronized Object getValueAt(int row, int col) {
        MChatHistoryDataStore historyData = (MChatHistoryDataStore) (colDataStore.elementAt(row));
        try {
            switch (col) {
                case DATE:
                    return historyData.getDate();
                case TIME:
                    return historyData.getTime();
                case FROM:
                    return historyData.getPersonFrom();
                case TO:
                    return historyData.getPersonTo();
                case MESSAGE:
                    return historyData.getMessage();
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public int getColCount() {
        return colCount;
    }

    public void setColCount() {
        colCount = colData.colCountMax;
        this.setColumnCount();
    }

    public void setColumnName() {
        colNames = colData.getColInfo();
    }

    public Vector getColumnName() {
        return colNames;
    }

    public void setColumnCount() {
        columnCount = colData.colCountMax;
    }
}
