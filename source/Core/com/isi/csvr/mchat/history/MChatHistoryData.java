package com.isi.csvr.mchat.history;

import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.mchat.shared.MChatStringTokenizer;
import com.isi.csvr.shared.UnicodeUtils;

import javax.swing.*;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.util.Vector;


public class MChatHistoryData {

    public String historyString = null;
    public int rowCount;
    public Vector data = new Vector();
    public JTextArea txtArea = null;

    public MChatHistoryData() {

    }

    public Vector getResult(String fileName) {
        try {
            data.clear();
            BufferedReader in = null;
            try {
                int tok = 0;
                MChatStringTokenizer st = null;
                int count = 0;
                String ap = "";
                File file = new File("history/mchat/" + fileName + ".txt");
                try {
                    FileInputStream fileIn = new FileInputStream(file);
                    while ((tok = fileIn.read()) != -1) {
                        ap = ap + (char) tok;
                        count++;
                    }
                    fileIn.close();
                    try {
                        st = new MChatStringTokenizer(ap, MChatMeta.HDS);
                        while (st.hasMoreElements()) {
                            String str = (String) st.nextToken();
                            MChatStringTokenizer next = new MChatStringTokenizer(str, MChatMeta.HEND, false);
                            while (next.hasMoreElements()) {
                                String endbreak = (String) next.nextToken();
                                MChatStringTokenizer fs = new MChatStringTokenizer(endbreak, MChatMeta.HS, false);
                                while (fs.hasMoreElements()) {
                                    MChatHistoryDataStore history = new MChatHistoryDataStore();
                                    txtArea = new JTextArea();
                                    history.date = (String) fs.nextElement();
                                    history.time = (String) fs.nextElement();
                                    history.from = (String) fs.nextElement();
                                    history.to = (String) fs.nextElement();
                                    String msg =  (String) fs.nextElement();
//                                    UnicodeUtils.getNativeStringFromCompressed(msg.trim());
                                    txtArea.append(UnicodeUtils.getNativeStringFromCompressed(msg.trim()));
                                    history.message = txtArea;
                                    data.addElement(history);
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } catch (Exception e) {
                throw new RuntimeException("String is not in right format!");
            }
            rowCount = data.size();
//            System.out.println("First rowcount " + rowCount);
        } catch (RuntimeException e) {
            e.printStackTrace();
        }
        return data;
    }

    public void setData(String history) {
        this.historyString = history;
    }

    public String getData() {
        return this.historyString;
    }

    public int getSearchRowCount() {
        return rowCount;
    }

    public void setMessageTextArea(JTextArea txt) {
        this.txtArea = txt;
    }

    public JTextArea getMessageTextArea(String msg) {
        txtArea.setText(msg);
        return txtArea;
    }
}
