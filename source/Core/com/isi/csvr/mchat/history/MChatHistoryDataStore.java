package com.isi.csvr.mchat.history;

import javax.swing.*;

public class MChatHistoryDataStore {

    public String date = null;
    public String time = null;
    public String from = null;
    public String to = null;
    public JTextArea message = new JTextArea();

    public String getDate() {
        return date;
    }

    public void setData(String data) {
        date = data;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getPersonFrom() {
        return from;
    }

    public void setPersonFrom(String from) {
        this.from = from;
    }

    public String getPersonTo() {
        return to;
    }

    public void setPersonTo(String to) {
        this.to = to;
    }

    public JTextArea getMessage() {
        return message;
    }

    public void setMessage(JTextArea msg) {
        message = msg;
    }
}
