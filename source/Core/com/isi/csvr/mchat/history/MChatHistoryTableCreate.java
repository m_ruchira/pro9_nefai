package com.isi.csvr.mchat.history;

import com.isi.csvr.mchat.shared.MChatLanguage;
import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.mchat.shared.MChatSettings;
import com.isi.csvr.mchat.shared.SmartFrame;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.StringSelection;
import java.awt.event.*;

public class MChatHistoryTableCreate extends SmartFrame implements TableCellRenderer {

    private JPanel tablePane = null;
    private JTable historyTable = null;
    private JScrollPane scrollSearch = null;
    private MChatHistoryDataSet dataSet = null;
    private JPopupMenu menu = null;
    private Clipboard system = null;
    private MouseEvent event = null;
    private int vColIndex = 0;
    private int width = 80;
    private JTextArea historyTxt = null;
    private JPopupMenu textAreaMenu = null;
    private boolean isMaximized = false;
    private boolean isMinimized = false;
    private int innerRow = 0;
    private int innerColumn = 0;
    private static Point origin = new Point();
    private JMenuItem menuMax = null;
    private JMenuItem menuMin = null;
    private JMenuItem menuCopy = null;
    private JScrollPane scrol = null;
    private JPanel pane = null;

    public MChatHistoryTableCreate() {
        super(true);
        try {
            setTitileFont(MChatMeta.defaultFontBold);
            setTitleColor();
            setTitle(MChatLanguage.getString("HISTORY_TITLE"));
            tablePane = new JPanel((new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0)));
            setSmartLayout((new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0)));
            scrollSearch = new JScrollPane();
            menu = new JPopupMenu();
            menu.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
            menu.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR")) ;
            tablePane.add(scrollSearch);
            historyTable = new JTable();
            historyTable.setFont(MChatMeta.defaultFontBold);
            historyTable.getTableHeader().setBackground(Theme.getColor("MCHAT_HEADER_BACKGROUND_COLOR"));
            historyTable.getTableHeader().setForeground(Color.WHITE);
            historyTable.setIntercellSpacing(new Dimension(0, 0));
            historyTable.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
//            historyTable.seti
            historyTable.getTableHeader().setReorderingAllowed(false);
            historyTable.setShowGrid(false);
            historyTable.setGridColor(Color.WHITE);
            scrollSearch.add(historyTable);
            scrollSearch.setViewportView(historyTable);
            historyTable.addMouseListener(new JTableButtonMouseListener(historyTable));
            tablePane.addMouseListener(new JTablePaneMouseListener(tablePane));
            setVisible(true);
            setLocation(100, 60);
            setSize(800, 600);
            setDefaultImageIcon();
            getSmartContentPanel().add(tablePane);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        JPanel comp;
        historyTable = table;
        int n = historyTable.getModel().getRowCount();
        comp = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        try {
            for (int i = 0; i < n; i++) {
                if (column != 4) {
                    if (isSelected && row % 2 == 0) {
                        JLabel jbl = new JLabel();
                        jbl.setFont(MChatMeta.defaultFont);
                        jbl.setVerticalAlignment(SwingConstants.TOP);
                        jbl.setText((String) historyTable.getValueAt(row, column));
                        comp.add(jbl);
                        comp.setBackground(new Color(0, 231, 69));
                        return comp;
                    } else if (!isSelected && row % 2 == 1) {
                        JLabel jbl = new JLabel();
                        jbl.setFont(MChatMeta.defaultFont);
                        jbl.setVerticalAlignment(SwingConstants.TOP);
                        jbl.setText((String) historyTable.getValueAt(row, column));
                        comp.add(jbl);
                        comp.setBackground(new Color(255, 200, 200));
                        return comp;
                    } else if (isSelected && row % 2 == 1) {
                        JLabel jbl = new JLabel();
                        jbl.setFont(MChatMeta.defaultFont);
                        jbl.setVerticalAlignment(SwingConstants.TOP);
                        jbl.setText((String) historyTable.getValueAt(row, column));
                        comp.add(jbl);
                        comp.setBackground(new Color(0, 231, 69));
                        return comp;
                    } else if (!isSelected && row % 2 == 0) {
                        JLabel jbl = new JLabel();
                        jbl.setFont(MChatMeta.defaultFont);
                        jbl.setVerticalAlignment(SwingConstants.TOP);
                        jbl.setText((String) historyTable.getValueAt(row, column));
                        comp.add(jbl);
                        comp.setBackground(new Color(255, 248, 255));
                        return comp;
                    } else {
                        JLabel jbl = new JLabel();
                        jbl.setFont(MChatMeta.defaultFont);
                        jbl.setVerticalAlignment(SwingConstants.TOP);
                        jbl.setText((String) historyTable.getValueAt(row, column));
                        comp.add(jbl);
                        comp.setBackground(Color.WHITE);
                        return comp;
                    }
                } else {
                    JTextArea txt = null;
                    if (isSelected && row % 2 == 0) {
                        txt = (JTextArea) historyTable.getValueAt(row, column);
                        txt.setFont(MChatMeta.defaultFont);
                        txt.setBackground(new Color(0, 231, 69));
                        txt.setLineWrap(true);
                        txt.setWrapStyleWord(true);
                        return txt;
                    } else if (!isSelected && row % 2 == 1) {
                        txt = (JTextArea) historyTable.getValueAt(row, column);
                        txt.setFont(MChatMeta.defaultFont);
                        txt.setBackground(new Color(255, 200, 200));
                        txt.setLineWrap(true);
                        txt.setWrapStyleWord(true);
                        return txt;
                    } else if (isSelected && row % 2 == 1) {
                        txt = (JTextArea) historyTable.getValueAt(row, column);
                        txt.setFont(MChatMeta.defaultFont);
                        txt.setBackground(new Color(0, 231, 69));
                        txt.setLineWrap(true);
                        txt.setWrapStyleWord(true);
                        return txt;
                    } else if (!isSelected && row % 2 == 0) {
                        txt = (JTextArea) historyTable.getValueAt(row, column);
                        txt.setFont(MChatMeta.defaultFont);
                        txt.setBackground(new Color(255, 248, 255));
                        txt.setLineWrap(true);
                        txt.setWrapStyleWord(true);
                        return txt;
                    } else {
                        txt = (JTextArea) historyTable.getValueAt(row, column);
                        txt.setFont(MChatMeta.defaultFont);
                        txt.setBackground(Color.WHITE);
                        txt.setLineWrap(true);
                        txt.setWrapStyleWord(true);
                        return txt;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return comp;
    }

    private class TextAreaRenderer implements TableCellRenderer {
        JTable tab = null;

        public TextAreaRenderer(JTable table) {
            tab = table;
        }

        public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
            JTextArea txt = (JTextArea) value;
            return (JTextArea) value;
        }
    }

    class JTablePaneMouseListener implements MouseListener {
        private JPanel innerPane;

        public JTablePaneMouseListener(JPanel panel) {
            innerPane = panel;
        }

        public void mouseClicked(MouseEvent e) {

        }

        public void mousePressed(MouseEvent e) {

        }

        public void mouseReleased(MouseEvent e) {

        }

        public void mouseEntered(MouseEvent e) {

        }

        public void mouseExited(MouseEvent e) {

        }
    }

    class JTableButtonMouseListener implements MouseListener {
        private JTable innerTable;
        private JFrame frame;

        public JTableButtonMouseListener(JTable table) {
            innerTable = table;
        }

        private void copySelect(MouseEvent e) {
            try {
                TableColumnModel columnModel = innerTable.getColumnModel();
                int column = columnModel.getColumnIndexAtX(e.getX());
                int row = e.getY() / innerTable.getRowHeight();
                Object value;

                if (row >= innerTable.getRowCount() || row < 0 ||
                        column >= innerTable.getColumnCount() || column < 0)
                    return;

                value = innerTable.getValueAt(row, column);
                StringBuffer sbf = new StringBuffer();
                if ((value instanceof JTextArea)) {
                    sbf.append((((JTextArea) value).getSelectedText()));
                    sbf.append("\n");
                } else {
                    sbf.append((((String) value)));
                    sbf.append("\n");
                }
                StringSelection stsel = new StringSelection(sbf.toString());
                system = Toolkit.getDefaultToolkit().getSystemClipboard();
                system.setContents(stsel, stsel);
                innerTable.repaint();
            } catch (HeadlessException e1) {
                e1.printStackTrace();
            }
        }

        private void viewHistory(MouseEvent e) {
            try {
                TableColumnModel columnModel = innerTable.getColumnModel();
                int column = columnModel.getColumnIndexAtX(e.getX());
                int row = e.getY() / innerTable.getRowHeight();
                Object value;
                if (row >= innerTable.getRowCount() || row < 0 ||
                        column >= innerTable.getColumnCount() || column < 0)
                    return;
                value = innerTable.getValueAt(row, column);
                StringBuffer sbf = new StringBuffer();
                if ((value instanceof JTextArea)) {
                    getToolTip(((JTextArea) value).getText(), e.getXOnScreen(), e.getYOnScreen(), (JTextArea) value);
                } else {
                    getToolTip(((String) value), e.getXOnScreen(), e.getYOnScreen(), (JTextArea) innerTable.getValueAt(row, 4));
                }

            } catch (HeadlessException e1) {
                e1.printStackTrace();
            }
        }


        private void copyTextArea(JTextArea textArea) {
            try {

                StringBuffer sbf = new StringBuffer();
                if ((textArea instanceof JTextArea)) {
                    sbf.append(textArea.getText());
                    sbf.append("\n");
                } else {
                    return;
                }
                StringSelection stsel = new StringSelection(sbf.toString());
                system = Toolkit.getDefaultToolkit().getSystemClipboard();
                system.setContents(stsel, stsel);
                innerTable.repaint();
            } catch (HeadlessException e1) {
                e1.printStackTrace();
            }
        }

        private class MouseDragAdapter implements MouseListener, MouseMotionListener {
            JFrame frame;

            public MouseDragAdapter(JFrame frame) {
                this.frame = frame;
            }

            public void mousePressed(MouseEvent e) {
                origin.x = e.getPoint().x;
                origin.y = e.getPoint().y;
            }

            public void mouseDragged(MouseEvent e) {
                try {
                    final Point mp = e.getPoint();
                    final Point fp = frame.getLocationOnScreen();
                    SwingUtilities.invokeLater(new Runnable() {
                        public void run() {
                            frame.setLocation(fp.x + mp.x - origin.x,
                                    fp.y + mp.y - origin.y);
                        }
                    });
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }

            public void mouseClicked(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
                origin.x = 0;
                origin.y = 0;
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }

            public void mouseMoved(MouseEvent e) {
            }
        }


        private void getToolTip(String input, int col, int row, JTextArea tArea) {

            ImageIcon max = new ImageIcon(MChatSettings.IMAGE_PATH + "\\btn_maximize.gif");
            ImageIcon min = new ImageIcon(MChatSettings.IMAGE_PATH + "\\btn_minimize.gif");
            ImageIcon copy = new ImageIcon(MChatSettings.IMAGE_PATH + "\\copy.gif");
            menuMax = new JMenuItem(MChatLanguage.getString("MAXIMISE"), max);
            menuMin = new JMenuItem(MChatLanguage.getString("MINIMISE"), min);
            menuCopy = new JMenuItem(MChatLanguage.getString("COPY"), copy);

            menuMin.setEnabled(false);
            isMinimized = true;
            historyTxt = new JTextArea();
            textAreaMenu = new JPopupMenu();
            textAreaMenu.setForeground(Theme.getColor("MCHAT_POPUP_FORGROUND_COLOR"));
            textAreaMenu.setBackground(Theme.getColor("MCHAT_POPUP_BACKGROUND_COLOR")) ;
            int adjust = 20;
            innerColumn = col;
            innerRow = row;
            frame = new JFrame();
            if (input.length() < 50) {

            } else {
                try {
                    pane = (JPanel) frame.getContentPane();
                    historyTxt.setText(input);
                    scrol = new JScrollPane();
                    scrol.setViewportView(historyTxt);
                    historyTxt.setLineWrap(true);
                    historyTxt.setWrapStyleWord(true);
                    historyTxt.setEditable(false);
                    historyTxt.setCaretPosition(0);
                    pane.add(scrol);
//                    frame.add(pane);
                    frame.setSize(400, 200);
                    frame.setLocation(col, row + adjust);
                    frame.setAlwaysOnTop(true);
                    frame.setResizable(true);
                    frame.setUndecorated(true);
                    historyTxt.setBackground(new Color(253, 254, 226));
                    frame.setVisible(true);
//                    Border border = scrol.getBorder();
//                    Border margin = new EmptyBorder(4, 4, 4, 4);
//                    scrol.setBorder(new CompoundBorder(border, margin));
                    scrol.setBorder(BorderFactory.createLineBorder(Color.BLACK  , 2));
                    WindowFocusListener listener = new WindowFocusListener() {
                        public void windowGainedFocus(WindowEvent e) {
                        }

                        public void windowLostFocus(WindowEvent e) {
                            frame.setVisible(false);
                        }
                    };
                    frame.addWindowFocusListener(listener);
                } catch (HeadlessException e) {
                    e.printStackTrace();
                } catch (SecurityException e) {
                    e.printStackTrace();
                }

                try {
                    historyTxt.addMouseListener(new MouseListener() {
                        public void mouseClicked(MouseEvent e) {

                            event = e;
                            textAreaMenu.removeAll();
                            try {
                                if (e.isMetaDown()) {
                                    menuMax.enable(true);
                                    menuMin.enable(false);

                                    menuCopy.addActionListener(new ActionListener() {
                                        public void actionPerformed(ActionEvent e) {
                                            copyTextArea(historyTxt);
                                        }
                                    });

                                    menuMax.addActionListener(new ActionListener() {
                                        public void actionPerformed(ActionEvent e) {
                                            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                                            frame.setSize((int) dim.getWidth() - 50, (int) dim.getHeight() - 50);
                                            frame.setLocation((int) dim.getWidth() - (frame.getWidth() + 20), (int) dim.getHeight() - (frame.getHeight() + 20));
                                            isMaximized = true;
                                            menuMin.setEnabled(true);
                                            menuMax.setEnabled(false);
                                        }
                                    });

                                    menuMin.addActionListener(new ActionListener() {
                                        public void actionPerformed(ActionEvent e) {
                                            frame.setSize(400, 200);
                                            frame.setLocation(innerColumn, innerRow + 20);
                                            isMinimized = true;
                                            menuMin.setEnabled(false);
                                            menuMax.setEnabled(true);
                                        }
                                    });

                                    textAreaMenu.add(menuCopy);
                                    textAreaMenu.add(menuMax);
                                    textAreaMenu.add(menuMin);
                                    textAreaMenu.show(historyTxt, e.getX(), e.getY());
                                } else {
                                    if (e.getClickCount() == 2) {
                                        if (isMaximized) {
                                            Dimension dim = Toolkit.getDefaultToolkit().getScreenSize();
                                            frame.setSize((int) dim.getWidth() - 50, (int) dim.getHeight() - 50);
                                            frame.setLocation((int) dim.getWidth() - (frame.getWidth() + 20), (int) dim.getHeight() - (frame.getHeight() + 20));
                                            isMinimized = true;
                                            isMaximized = false;
                                            menuMin.setEnabled(true);
                                            menuMax.setEnabled(false);

                                        } else if (isMinimized) {
                                            frame.setSize(400, 200);
                                            frame.setLocation(innerColumn, innerRow + 20);
                                            isMaximized = true;
                                            isMinimized = false;
                                            menuMin.setEnabled(false);
                                            menuMax.setEnabled(true);
                                        }
                                    }
                                }
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }

                        public void mousePressed(MouseEvent e) {

                        }

                        public void mouseReleased(MouseEvent e) {

                        }

                        public void mouseEntered(MouseEvent e) {

                        }

                        public void mouseExited(MouseEvent e) {

                        }
                    });
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        public void mouseClicked(MouseEvent e) {
            try {
                event = e;
                menu.removeAll();
                if (e.isMetaDown()) {
                    ImageIcon copy = new ImageIcon(MChatSettings.IMAGE_PATH + "\\copy.gif");
                    JMenuItem menuCopy = new JMenuItem(MChatLanguage.getString("COPY"), copy);
                    menuCopy.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            copySelect(event);
                        }
                    });
                    menu.add(menuCopy);
                    menu.show(historyTable, e.getX(), e.getY());
                } else {
                    try {
                        viewHistory(e);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            } catch (Exception e1) {
                e1.printStackTrace();
            }

        }

        public void mouseEntered(MouseEvent e) {

        }

        public void mouseExited(MouseEvent e) {

        }

        public void mousePressed(MouseEvent e) {

        }

        public void mouseReleased(MouseEvent e) {

        }
    }


    public void initializeHistoryTable() {

        try {
            try {
                historyTable.setModel(this.getDataModel());
                TableColumn firstCol = historyTable.getColumnModel().getColumn(vColIndex);
                firstCol.setMinWidth(width);
                firstCol.setMaxWidth(width);
                firstCol.setPreferredWidth(width);
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                TableColumn secondCol = historyTable.getColumnModel().getColumn(vColIndex + 1);
                secondCol.setMinWidth(width);
                secondCol.setMaxWidth(width);
                secondCol.setPreferredWidth(width);
            } catch (Exception e) {
                e.printStackTrace();
            }

            try {
                TableColumn fifthCol = historyTable.getColumnModel().getColumn(vColIndex + 4);
                fifthCol.setMinWidth(width + 300);
                fifthCol.setMaxWidth(width + 300);
                fifthCol.setPreferredWidth(width + 300);
            } catch (Exception e) {
                e.printStackTrace();
            }

//            historyTable.getColumn("Message").setCellRenderer(new TextAreaRenderer(historyTable));
            historyTable.getColumn(MChatLanguage.getString("HISTORY_MESSAGE")).setCellRenderer(new TextAreaRenderer(historyTable));

            for (int i = 0; i < historyTable.getModel().getColumnCount(); i++) {
                historyTable.getColumnModel().getColumn(i).setCellRenderer(this);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setDataModel(MChatHistoryDataSet data) {
        dataSet = data;
    }

    public MChatHistoryDataSet getDataModel() {
        return dataSet;
    }
}
