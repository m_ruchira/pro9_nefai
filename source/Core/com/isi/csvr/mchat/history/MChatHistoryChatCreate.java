package com.isi.csvr.mchat.history;

import com.isi.csvr.mchat.shared.MChatMeta;
import com.isi.csvr.shared.UnicodeUtils;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class MChatHistoryChatCreate {

    private static MChatHistoryChatCreate self = null;

    public void writeHistorytFile(String fileName, String message, String from, String to) {
        try {
            String path = "history/mchat/" + fileName;
            File file = new File(path);
            DateFormat dateFormatTime = new SimpleDateFormat("HH:mm:ss");
            DateFormat dateFormatDate = new SimpleDateFormat("yyyy/MM/dd");
            Date date = new Date();
            if (file.exists()) {
                try {
                    FileWriter fw = new FileWriter(path, true);
                    fw.write(MChatMeta.HDS + dateFormatDate.format(date) + MChatMeta.HS + dateFormatTime.format(date) + MChatMeta.HS + from + MChatMeta.HS + to + MChatMeta.HS + UnicodeUtils.getCompressedUnicodeString(message)/*message*/ + MChatMeta.HEND);
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            } else {
                try {
                    file.createNewFile();
                    FileWriter fw = new FileWriter(path);
                    fw.write(MChatMeta.HDS + dateFormatDate.format(date) + MChatMeta.HS + dateFormatTime.format(date) + MChatMeta.HS + from + MChatMeta.HS + to + MChatMeta.HS + UnicodeUtils.getCompressedUnicodeString(message)/*message*/ + MChatMeta.HEND);
                    fw.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static MChatHistoryChatCreate getSharedInstance() {
        if (self == null) {
            self = new MChatHistoryChatCreate();
        }
        return self;
    }
}
