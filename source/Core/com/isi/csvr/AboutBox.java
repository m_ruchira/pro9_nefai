package com.isi.csvr;

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.win32.NativeMethods;

import javax.swing.*;
import javax.swing.event.HyperlinkEvent;
import javax.swing.event.HyperlinkListener;
import javax.swing.text.html.HTMLFrameHyperlinkEvent;
import java.awt.*;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0 
 */

public class AboutBox
        extends InternalFrame implements HyperlinkListener {
    private BorderLayout borderLayout1 = new BorderLayout(0, 0);
    private JEditorPane pane;
    private static AboutBox self = null;
    private String title;

    public static AboutBox getSharedInstance(){
        if(self==null){
            self=new AboutBox();
        }
        return self;
    }
    private AboutBox(String title) {
        super(false);
        this.title = title;
        //super(frame, title, modal);
        try {
            jbInit();
            loadTemplate();
            pack();
            setLocationRelativeTo(Client.getInstance().getDesktop());
            Client.getInstance().getDesktop().add(this);
            //pane.paint(Client.getInstance().getGraphics());
            // pane.createEditorKitForContentType()
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public AboutBox() {
        this(Language.getString("HELP_ABOUT"));
    }

    /*public static AboutBox getSharedInstance() {
        if (self == null) {
            self = new AboutBox();
            Client.getInstance().getDesktop().add(self);
        }
        return self;
    }*/

    private void jbInit() throws Exception {
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        setTitle(title);
        setClosable(true);
        pane = new JEditorPane();
        pane.setEditable(false);
        getContentPane().setLayout(borderLayout1);
        getContentPane().add(pane, BorderLayout.CENTER);
        pane.setOpaque(true);
        pane.setFocusable(false);

        pane.setContentType("text/html");
        pane.addHyperlinkListener(this);
        setResizable(false);
        setOrientation();
        setDefaultCloseOperation(HIDE_ON_CLOSE);
    }

    private void loadTemplate() {
        StringBuffer buffer = new StringBuffer();
        String template;

        byte[] temp = new byte[5000];
        int count = 0;

        try {
            InputStream in = new FileInputStream(".\\Templates\\About_twt_" + Language.getLanguageTag() + ".htm");
            while (true) {
                count = in.read(temp);
                if (count == -1) {
                    break;
                }
                if (count > 0) {
                    String str = new String(temp, 0, count);
                    buffer.append(str);
                    str = null;
                }
            }
            template = buffer.toString();
            template = template.replaceAll("\\[PATH\\]",
                    (System.getProperties().get("user.dir") + "/Templates/").replaceAll("\\\\", "/"));
            template = template.replaceFirst("\\[VER\\]", Settings.TW_VERSION);
//            template = template.replaceFirst("\\[DATE\\]",System.getProperty("BuildDate"));
            template = template.replaceFirst("\\[DATE\\]", NativeMethods.getBuildDate());
            pane.setText(template);
            buffer = null;
            in = null;
            temp = null;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            template = "";
        }
    }



    public void hyperlinkUpdate(HyperlinkEvent e) {
        if (e.getEventType() == HyperlinkEvent.EventType.ACTIVATED) {
            JEditorPane pane = (JEditorPane) e.getSource();
            if (e instanceof HTMLFrameHyperlinkEvent) {
            } else {
                try {
                    if (e.getURL().toString().toUpperCase().startsWith("HTTP://AGGREEMENT")) {
                        this.hide();
                        this.setVisible(false);
                        SwingUtilities.invokeLater(new Thread("AboutBox") {
                            public void run() {
                                (new EULAWindow(Client.getInstance().getFrame(), false)).show();
                            }
                        });
                    } else {
                        Runtime.getRuntime().exec("start " + e.getURL());
                    }
                }
                catch (IOException ex) {
                }
            }
        }
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}