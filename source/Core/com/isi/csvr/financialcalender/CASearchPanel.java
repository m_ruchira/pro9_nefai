package com.isi.csvr.financialcalender;

import com.isi.csvr.Client;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.calendar.DatePicker;
//import com.isi.csvr.netcalculator.DatePicker;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.text.DecimalFormat;
import java.util.Calendar;
import java.util.Vector;
import java.util.Enumeration;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class CASearchPanel extends JDialog implements ActionListener,
		DateSelectedListener, WindowFocusListener, Themeable{

	private final int FROM_DATE = 0;
	private final int TO_DATE 	= 1;

    private JPanel 		ListPanel;
	private JCheckBox 	chkDates;
	private JButton 	btnFromDate;
	private JButton 	btnToDate;
	private JButton     btnSearch;
	private JPanel 		lowerControlPanel;
	private DatePicker datePicker;
	private JLabel		lblFromDate;
	private JLabel		lblToDate;
	private JList 		lstSources;
	private JTextField 	txtSymbol;
	//private JTextField  txtKeyWord;
	private JPanel      controlPanel;

	private String		toDate;
	private String 		fromDate;
	private int			dateSelector;
    private int         category = -1;
	private JComboBox 	cmbCategories;

	private Object[] 	hours;
    private boolean     firstTime = true;

    public CASearchPanel(JFrame parent) {
		super(parent, Language.getString("WINDOW_TITLE_CA_SEARCH_PANEL"));
        try {
			setModal(true);
            jbInit();
        }
        catch(Exception ex) {
            ex.printStackTrace();
        }
    }
    void jbInit() throws Exception {
        //this.setMaximumSize(new Dimension(500, //));
        //this.setMinimumSize(new Dimension(500, 100));
        //this.setPreferredSize(new Dimension(500, 100));

		Theme.registerComponent(this);

		hours = new Object[24];
		for (int i = 0; i < 24; i++) {
			hours[i] = "" + i;
		}

		datePicker = new DatePicker(this,true);
		datePicker.getCalendar().addDateSelectedListener(this);
		//datePicker.setse
		this.setSize(new Dimension(610, 70));
		//this.setLocationRelativeTo(Client.getInstance());
        //GUISettings.setLocationRelativeTo(this,Client.getInstance().getDesktop());
        this.getContentPane().setLayout(new FlowLayout(FlowLayout.LEADING,0,0));

		((JComponent)this.getContentPane()).registerKeyboardAction(this,"ESCAPE",KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE,0),JComponent.WHEN_IN_FOCUSED_WINDOW);



		//

		/* ---------------- Blank  panel ------------------- */
		//JPanel blankPanel = new JPanel();
		//blankPanel.setPreferredSize(new Dimension(400,20));


		controlPanel = new JPanel();
		controlPanel.setLayout(new FlowLayout(FlowLayout.CENTER,0,0));
		controlPanel.setPreferredSize(new Dimension(600,100));

		/* ---------- upper control panel ------------------ */
		JPanel upperControlPanel = new JPanel();

		//upperControlPanel.setBackground(Color.red);
		upperControlPanel.setPreferredSize(new Dimension(600,40));
		upperControlPanel.setLayout(new FlowLayout(FlowLayout.LEADING,5,10));

		JLabel lblSymbol = new JLabel("Symbol");
		lblSymbol.setPreferredSize(new Dimension(60,20));
		upperControlPanel.add(lblSymbol);

		txtSymbol = new JTextField();
		txtSymbol.setPreferredSize(new Dimension(130,20));
		upperControlPanel.add(txtSymbol);

//		Object[] list = {"And","Or"};
//		JComboBox lstAndOr = new JComboBox(list);
//		lstAndOr.setPreferredSize(new Dimension(60,20));
//		upperControlPanel.add(lstAndOr);

		JLabel lblKeyWord = new JLabel("Category");
		lblKeyWord.setPreferredSize(new Dimension(70,20));
		upperControlPanel.add(lblKeyWord);

//		txtKeyWord = new JTextField();
//		txtKeyWord.setPreferredSize(new Dimension(100,20));
//		upperControlPanel.add(txtKeyWord);
        String[] categories = new String[4];
        categories[0] = Language.getString("LBL_CA_TYPE_ANY");
        categories[1] = Language.getString("LBL_CA_TYPE_GENERAL");
        categories[2] = Language.getString("LBL_CA_TYPE_SPLIT");
        categories[3] = Language.getString("LBL_CA_TYPE_DIVIDEND");
        cmbCategories = new JComboBox(categories);
        cmbCategories.setPreferredSize(new Dimension(130,20));
		upperControlPanel.add(cmbCategories);
        cmbCategories.addActionListener(this);
        cmbCategories.setActionCommand("CAT");

		btnSearch = new JButton("Search");
		upperControlPanel.add(btnSearch);
		btnSearch.setActionCommand("SEARCH");
		btnSearch.addActionListener(this);

		JButton btnAdvanced = new JButton("Advanced");
		upperControlPanel.add(btnAdvanced);
		btnAdvanced.setActionCommand("ADVANCED");
		btnAdvanced.addActionListener(this);

		/* ---------- lower control panel ------------------ */
		lowerControlPanel = new JPanel();
		//lowerControlPanel.setBackground(Color.gray);
		lowerControlPanel.setPreferredSize(new Dimension(600,60));
		lowerControlPanel.setLayout(new FlowLayout(FlowLayout.LEADING,5,0));
		//lowerControlPanel.setOpaque(true);;
		//lowerControlPanel.setBackground(Color.red);

		//ListPanel.setMaximumSize(new Dimension(100, 100));
		//ListPanel.setMinimumSize(new Dimension(100, 100));
		ListPanel 		= new JPanel();
		ListPanel.setPreferredSize(new Dimension(240, 60));
		ListPanel.setOpaque(true);
		//ListPanel.setBackground(Color.red);
		ListPanel.setLayout(new BorderLayout(0,0));

		JLabel lblSources = new JLabel("Exchanges");


		ListPanel.add(lblSources,BorderLayout.NORTH);
		lstSources = new JList();
		JScrollPane scrollPane = new JScrollPane(lstSources);
		//lstSources.setPreferredSize(new Dimension(100,100));
		ListPanel.add(scrollPane,BorderLayout.CENTER);

        //sath
        loadExchanges();
        JPanel datePanel = new JPanel();
		datePanel.setLayout(new FlowLayout(FlowLayout.LEADING,10,0));
		datePanel.setPreferredSize(new Dimension(350,60));


		/*chkDates = new JCheckBox();
		lowerControlPanel.add(chkDates);
		chkDates.setActionCommand("CHK");
		chkDates.addActionListener(this);*/

		JLabel lblFrom = new JLabel("From");
		lblFrom.setPreferredSize(new Dimension(80,20));
		//upperControlPanel.add(lblFrom);
		datePanel.add(lblFrom);

		btnFromDate = new JButton();
		lblFromDate = new JLabel();
		datePanel.add(getDatePanel("FD",btnFromDate,lblFromDate));

		JLabel lblFromHr = new JLabel("");
		lblFromHr.setPreferredSize(new Dimension(100,20));
		//upperControlPanel.add(lblFromHr);
		datePanel.add(lblFromHr);

//		cmbFromHour = new JComboBox(hours);
//		cmbFromHour.setPreferredSize(new Dimension(50,20));
//		cmbFromHour.setActionCommand("FROMHR");
//		cmbFromHour.addActionListener(this);
//		datePanel.add(cmbFromHour);

		JLabel lblTo = new JLabel("To");
		lblTo.setPreferredSize(new Dimension(80,20));
		datePanel.add(lblTo);
		//lowerControlPanel.add(lblTo);

		btnToDate = new JButton();
		lblToDate = new JLabel();
		datePanel.add(getDatePanel("TD",btnToDate,lblToDate));

		JLabel lblToHr = new JLabel("");
		lblToHr.setPreferredSize(new Dimension(100,20));
		//upperControlPanel.add(lblFromHr);
		datePanel.add(lblToHr);

//		cmbToHour = new JComboBox(hours);
//		cmbToHour.setPreferredSize(new Dimension(50,20));
//		cmbToHour.setActionCommand("TOHR");
//		cmbToHour.addActionListener(this);
//		datePanel.add(cmbToHour);

		//controlPanel.add(blankPanel);
		lowerControlPanel.setVisible(false);
		lowerControlPanel.add(ListPanel);
		lowerControlPanel.add(datePanel);
		controlPanel.add(upperControlPanel);
		controlPanel.add(lowerControlPanel);

       //this.getContentPane().add(ListPanel);
		this.getContentPane().add(controlPanel);

		setDefaultDateSettings();
		this.setResizable(false);

		//addWindowFocusListener(this);

		txtSymbol.registerKeyboardAction(this,"SEARCH",KeyStroke.getKeyStroke(KeyEvent.VK_ENTER,0),JComponent.WHEN_IN_FOCUSED_WINDOW);
		//loadNewsSources();
    }

	public void applyTheme(){
		SwingUtilities.updateComponentTreeUI(controlPanel);
	}

	public void show(){
        if (firstTime){
            GUISettings.setLocationRelativeTo(this,Client.getInstance().getDesktop());
            firstTime = false;
        }
		SwingUtilities.updateComponentTreeUI(this);
		super.show();

	}

	public void loadExchanges(){

		Vector newsSourcesVec;
    	//if (lstSources.getModel().getSize() >0) return; // already loaded

       //sath
       newsSourcesVec = new Vector();
       newsSourcesVec.add("All");
       Enumeration enumExchanges = ExchangeStore.getSharedInstance().getExchanges();
       while (enumExchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) enumExchanges.nextElement();
           newsSourcesVec.add(exchange.toString());
           System.out.println("sathex: " + exchange.toString());
            exchange = null;
        }
       	lstSources.setListData(newsSourcesVec.toArray());
		//sath
        //	lstSources.updateUI();
		//if (lstSources.getModel().getSize() <= 0){

			/*if ((newsSources != null) && (newsSources.length>0)){
				String[] activeNewsSources = new String[newsSources.length+1];
				System.arraycopy(newsSources,0,activeNewsSources,1,newsSources.length);
				activeNewsSources[0] = "All";
				lstSources.setListData(activeNewsSources);
				lstSources.updateUI();
			}*/



		//}
    	newsSourcesVec = null;
	}

	public JPanel getDatePanel(String type, final JButton arrowButton, final JLabel lblDate){

		JPanel datePanel = new JPanel(new FlowLayout(FlowLayout.CENTER,0,0));
		datePanel.setPreferredSize(new Dimension(100,20));
		datePanel.setBorder(BorderFactory.createEtchedBorder());
		//lblDate = new JPanel();


		lblDate.setPreferredSize(new Dimension(78,16));
		lblDate.setOpaque(true);
		datePanel.add(lblDate);
		arrowButton.setIcon(new DownArrow());
		arrowButton.setBorder(null);
		arrowButton.setActionCommand(type);
		arrowButton.addActionListener(this);
		arrowButton.setPreferredSize(new Dimension(18,16));
		datePanel.add(arrowButton);
		//fromDatePanel.addMouseListener(this);
		//panel3.add(fromDatePanel);
        //mainPanel.add(panel3);
		return datePanel;
	}

	//public static void main(String[] args) {
		//NewsSearchPanel panel = new NewsSearchPanel();
		//panel.show();
	//}

	//sath
    private void creteRequest(){

        String sources = gettSelectedNewsSources();
        String sSelectedSymbol = txtSymbol.getText();

        FinancialCalenderStore.getSharedInstance().reset();
        FinancialCalenderStore.getSharedInstance().PrepareRequest(sources, sSelectedSymbol,""+category,fromDate,toDate);
        FinancialCalenderStore.getSharedInstance().sendCommonCARequest();
        Client.getInstance().mnu_CorporateAction_ActionPerformed(sSelectedSymbol);
    }

	public String getAllExchanges(){
		StringBuffer sources = new StringBuffer();
		for (int i = 1; i < lstSources.getModel().getSize(); i++) {
			sources.append(lstSources.getModel().getElementAt(i));
			if (i != lstSources.getModel().getSize()-1){
				sources.append(",");
			}
		}

		return sources.toString();
	}

	private String gettSelectedNewsSources(){
		StringBuffer sources = new StringBuffer();

		int[] selection = lstSources.getSelectedIndices();
		if((selection.length ==0) || (selection[0] ==0)){ // 'all' item is selected
			for (int i = 1; i < lstSources.getModel().getSize(); i++) {
				sources.append(lstSources.getModel().getElementAt(i));
				if (i != lstSources.getModel().getSize()-1){
					sources.append(",");
				}
			}

		}else{
			for (int i = 0; i < selection.length; i++) {
				sources.append(lstSources.getModel().getElementAt(selection[i]));
				if (i != selection.length-1){
					sources.append(",");
				}
			}
		}
		SharedMethods.printLine("sources " + sources.toString());
		return sources.toString();
	}

	private void search(){

        if(isValidInput()){
			creteRequest();
			hide();
		}else{
			SharedMethods.printLine("invalid input");
		}
	}

	private boolean isValidInput(){
		/*if(isEmpty(txtKeyWord) && isEmpty(txtSymbol)){
	  		return false;
		}else{
			return true;
		}*/
		return true;
	}

	private boolean isEmpty(JTextField component){
		return component.getText().trim().equals("");
	}

	 public void dateSelected(Object source, int iYear,int iMonth,int iDay){
		 //SharedMethods.printLine(""+iDay);
		 DecimalFormat numberFormat = new DecimalFormat("00");

		iMonth++;
		 switch(dateSelector){
			 case FROM_DATE:
		 		lblFromDate.setText("" + iYear  + "/" + iMonth + "/" +  iDay);
				fromDate = "" + iYear  + "" + numberFormat.format(iMonth)  + "" +
			   		numberFormat.format(iDay);// + numberFormat.format(cmbFromHour.getSelectedIndex());
				break;
			case TO_DATE:
				lblToDate.setText("" + iYear  + "/" + iMonth + "/" +  iDay);
				toDate = "" + iYear  + "" + numberFormat.format(iMonth)  + "" +
			 		numberFormat.format(iDay);// + numberFormat.format(cmbToHour.getSelectedIndex());
				break;
		 }

		 datePicker.setVisible(false);
	 }

//	 private String modifyFromHour(String request){
//		 DecimalFormat numberFormat = new DecimalFormat("00");
//		 return request.substring(0,request.length()-2) + numberFormat.format(cmbFromHour.getSelectedIndex());
//	 }
//
//	 private String modifyToHour(String request){
//		 DecimalFormat numberFormat = new DecimalFormat("00");
//		 return request.substring(0,request.length()-2) + numberFormat.format(cmbToHour.getSelectedIndex());
//	 }

	 public void setDefaultDateSettings(){
		 Calendar calendar = Calendar.getInstance();

		 this.dateSelector = TO_DATE;
		 dateSelected(null,calendar.get(Calendar.YEAR),
					  calendar.get(Calendar.MONTH),
					  calendar.get(Calendar.DATE));
		 calendar.add(Calendar.MONTH,-1);
		 this.dateSelector = FROM_DATE;
		 dateSelected(null,calendar.get(Calendar.YEAR)-5,
					  calendar.get(Calendar.MONTH),
					  calendar.get(Calendar.DATE));


		 calendar = null;
	 }


	public void actionPerformed(ActionEvent e){
		if (e.getActionCommand().equals("FD")){

			this.dateSelector = FROM_DATE;
			JComponent source = (JComponent)e.getSource();
			Point location = source.getParent().getLocation();
			location.move(-80,20);
			SwingUtilities.convertPointToScreen(location,source);
			datePicker.setLocation(location);
			datePicker.show();
		}else if (e.getActionCommand().equals("TD")){
			this.dateSelector = TO_DATE;
			JComponent source = (JComponent)e.getSource();
			Point location = source.getParent().getLocation();
			location.move(-80,20);
			SwingUtilities.convertPointToScreen(location,source);
			datePicker.setLocation(location);
			datePicker.show();
		}else if (e.getActionCommand().equals("ADVANCED")){
			setDefaultDateSettings();
			this.setSize(new Dimension(610, 130));
			lowerControlPanel.setVisible(true);
			((JButton)e.getSource()).setActionCommand("SIMPLE");
		}else if (e.getActionCommand().equals("SIMPLE")){
			this.setSize(new Dimension(610, 70));
			lowerControlPanel.setVisible(false);
			((JButton)e.getSource()).setActionCommand("ADVANCED");
		}else if (e.getActionCommand().equals("ESCAPE")){
			this.setVisible(false);
		}else if (e.getActionCommand().equals("SEARCH")){

            search();
//sath
        }else if (e.getActionCommand().equals("CAT")){
            switch (cmbCategories.getSelectedIndex()){
//                case 0:
//                    category = Meta.CA_ANY;
//                    break;
//                case 1:
//                    category = Meta.CA_GENERAL;
//                    break;
//                case 2:
//                    category = Meta.CA_SPLIT;
//                    break;
//                case 3:
//                    category = Meta.CA_DIDIVEND;
//                    break;
//                default:
//                    category = Meta.CA_ANY;
            }
//		}else if (e.getActionCommand().equals("FROMHR")){
//			fromDate = modifyFromHour(fromDate);
//		}else if (e.getActionCommand().equals("TOHR")){
//			toDate = modifyToHour(toDate);
		}
	}

	/**
	 * Invoked when the Window is set to be the focused Window, which means
	 * that the Window, or one of its subcomponents, will receive keyboard
	 * events.
	 */
	public void windowGainedFocus(WindowEvent e){
//		SymbolBar.setDisabled(true);
	}

	/**
	 * Invoked when the Window is no longer the focused Window, which means
	 * that keyboard events will no longer be delivered to the Window or any of
	 * its subcomponents.
	 */
    public void windowLostFocus(WindowEvent e){
//		SymbolBar.setDisabled(false);
		setVisible(false);
	}

}