package com.isi.csvr.forex;

import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.TWFormattedTextField;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.Client;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.datastore.IPOTableRecord;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.ChangeListener;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.dnd.*;
import java.awt.event.*;
import java.util.*;
import java.io.FileInputStream;
import java.io.File;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;
import java.text.NumberFormat;

/**
 * Created by IntelliJ IDEA. User: Chandika Hewage Date: Sep 1, 2007 Time: 12:52:29 PM To change this template use File
 * | Settings | File Templates.
 */

public class FXSymbolView extends JPanel implements ActionListener, MouseListener, DragSourceListener, DragGestureListener, DropTargetListener, KeyListener, ChangeListener, PropertyChangeListener, Themeable {
    /**
     * This is the main UI Component of the forex board
     */
    private JPanel upperMain;
    private JPanel upperTop;
    private JPanel upperPanel;
    private JPanel middlePanel;
    private JPanel lowerPanel;
    private JPanel lowerFirst;
    private JPanel lowerSecond;
    private JPanel quantityPanel;
    public FXButton bidBtn;
    public FXButton askBtn;
    private TWButton closeBtn;
    private TWButton addLotSizesBtn;
    public JPanel dragLabel;
    private JButton dowwnArrow;
    public TWComboBox firstPart;
    public TWComboBox secondPart;
    public TWComboBox currencyCombo;
    private TWComboModel fxModel;
    private ArrayList<TWComboItem> fxArray;
    private JLabel iconLabel;
    private JLabel currencyLabel = new JLabel();
    private JLabel netAmtLabel;
    private JSpinner blockSpinner;
    private TWFormattedTextField qtyField;
    private TWFormattedTextField ownedAmount;
    public TWFormattedTextField tf;
    private NumberFormat amountFormat;
    private double amount = 100000;
    FXObject fxObject;
    private double buyValue;
    private double sellValue;
    public String key;
    String[] allSymbols;
    TWDecimalFormat blockSizeFormat = new TWDecimalFormat("###,##0");
    SmartProperties prop;
    public long selectedBlockSize;
    public JPopupMenu rightClickPopup;
    public TWMenuItem removeInstrument;
    ForexBoard parent;
    public String startingKey;
    Color fxColor = new Color(185, 249, 185);
    public static Hashtable pipHash = new Hashtable();
    private Comparator comparator;
    private Vector sortedSymbols = new Vector();
    private long blockSpinnerDefault = 100000;

    long[] arrayBlockSizes = new long[9];

    private Color blockItemSelectedColor;
    private Color blockItemFGColor;
    public JLabel timeLabel;
    private long  netVal=0;
    private long  netExp=0;

    public FXSymbolView(ForexBoard parent) {
        this.parent = parent;
        loadAllSymbols();
        this.setBorder(BorderFactory.createEtchedBorder());
        this.setLayout(new BorderLayout());
        this.add(createUpperPanel(""), BorderLayout.NORTH);
        currencyCombo.setSelectedIndex(0);
        String symbol=((TWComboItem)currencyCombo.getSelectedItem()).getId();

        this.key = SharedMethods.getKey("FORX", symbol, Meta.INSTRUMENT_FOREX);
        this.startingKey = SharedMethods.getKey("FORX", symbol, Meta.INSTRUMENT_FOREX);
        this.add(createMiddlePanel(), BorderLayout.CENTER);
        this.add(createLowerPanel(), BorderLayout.SOUTH);
        this.setBackground(Color.white);
        this.selectedBlockSize = Long.parseLong((blockSpinner.getValue() + "").split("\\.")[0]);
        currencyLabel.setText(((TWComboItem)currencyCombo.getSelectedItem()).getId().substring(0,3));
        this.addMouseListener(this);
        this.setPreferredSize(new Dimension(145, 160));
        blockItemSelectedColor = Theme.getColor("FOREX_POPUP_SELECTED");
        blockItemFGColor = Theme.getColor("FOREX_POPUP_FG");
        Theme.registerComponent(this);

    }

    public FXSymbolView(ForexBoard parent, String left, String right) {
        this.parent = parent;
        loadAllSymbols();
        this.setBorder(BorderFactory.createEtchedBorder());
        this.setLayout(new BorderLayout());
        this.add(createUpperPanel(left+right), BorderLayout.NORTH);
        this.key = SharedMethods.getKey("FORX", left + right, Meta.INSTRUMENT_FOREX);
        this.startingKey = SharedMethods.getKey("FORX", "" + firstPart.getSelectedItem() + secondPart.getSelectedItem(), Meta.INSTRUMENT_FOREX);
        this.add(createMiddlePanel(), BorderLayout.CENTER);
        this.add(createLowerPanel(), BorderLayout.SOUTH);
        this.setBackground(Color.white);
        this.selectedBlockSize = Long.parseLong((blockSpinner.getValue() + "").split("\\.")[0]);
        currencyLabel.setText(left);
        this.addMouseListener(this);
        this.setPreferredSize(new Dimension(145, 160));
        blockItemSelectedColor = Theme.getColor("FOREX_POPUP_SELECTED");
        blockItemFGColor = Theme.getColor("FOREX_POPUP_FG");
        Theme.registerComponent(this);
    }

    public JPanel createUpperPanel(String symbol) {
        upperMain = new JPanel();
        upperMain.setPreferredSize(new Dimension(145, 28));
        upperMain.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"10", "100%"}));
        upperTop = new JPanel();
        closeBtn = new TWButton();
        closeBtn.addActionListener(this);
        dragLabel = new JPanel();
        dragLabel.setCursor(new Cursor(Cursor.HAND_CURSOR));
//        upperTop.setBackground(Color.BLUE);
        upperTop.setPreferredSize(new Dimension(145, 10));
        upperTop.setLayout(new FlexGridLayout(new String[]{"100%", "10"}, new String[]{"100%"}));
        upperTop.add(dragLabel);
        upperTop.add(closeBtn);
        upperPanel = new JPanel();
        firstPart = new TWComboBox();
        firstPart.addActionListener(this);
        secondPart = new TWComboBox();
        secondPart.addActionListener(this);
        fxArray=new ArrayList<TWComboItem>();
        fxModel=new TWComboModel(fxArray);
        currencyCombo = new TWComboBox(fxModel);
        currencyCombo.addActionListener(this);
        timeLabel=new JLabel();
        timeLabel.setOpaque(true);
        timeLabel.setHorizontalAlignment(SwingConstants.CENTER);
        iconLabel = new JLabel("[  ]");
        upperPanel.setLayout(new FlexGridLayout(new String[]{"100%", "60"}, new String[]{"100%"}));    //"5","70","100%","70","5"
//        upperPanel.add(new JLabel());
        upperPanel.add(currencyCombo);
        upperPanel.add(timeLabel);
//        upperPanel.add(new JLabel());
//        upperPanel.add(iconLabel);
        populateForexCombo(symbol);
        upperPanel.setPreferredSize(new Dimension(145, 25));
        upperMain.add(upperTop);
        upperMain.add(upperPanel);
        loadButtonImages();
        return upperMain;
    }

    public JPanel createMiddlePanel() {
        middlePanel = new JPanel();
        bidBtn = new FXButton(Language.getString("TRADE_SIDE_BUY"), this.key);
        bidBtn.setOpaque(true);
        bidBtn.setBackground(fxColor);
        bidBtn.addActionListener(this);
        askBtn = new FXButton(Language.getString("SELL"), this.key);
        askBtn.setOpaque(true);
        askBtn.setBackground(fxColor);
        askBtn.addActionListener(this);
        middlePanel.setLayout(new FlexGridLayout(new String[]{"70", "100%", "70"}, new String[]{"100%"}));     //"5","70","100%","70","5"
//        middlePanel.add(new JLabel());
        middlePanel.add(askBtn);
        middlePanel.add(new JLabel());
        middlePanel.add(bidBtn);
//        middlePanel.add(new JLabel());
        middlePanel.setPreferredSize(new Dimension(145, 65));
        bidBtn.setPreferredSize(new Dimension(70, 65));
        askBtn.setPreferredSize(new Dimension(70, 65));
        return middlePanel;
    }

    public JPanel createLowerPanel() {
        lowerPanel = new JPanel();
        lowerFirst = new JPanel();
        currencyLabel.setText(firstPart.getSelectedItem() + "");
        quantityPanel = new JPanel();
        quantityPanel.setLayout(new FlexGridLayout(new String[]{"85%", "15%"}, new String[]{"100%"}));
        quantityPanel.setBorder(BorderFactory.createEtchedBorder());
        dowwnArrow = new JButton(new DownArrow());
        dowwnArrow.setBorderPainted(false);
        blockSpinner = new JSpinner(new SpinnerNumberModel(CustomSettingsStore.getSharedInstance().getSettingsObject(this.key).getAmtArray()[0],
                                                           CustomSettingsStore.getSharedInstance().getSettingsObject(this.key).getDefaultAmmount(),
                                                           CustomSettingsStore.getSharedInstance().getSettingsObject(this.key).getAmtArray()[5],
                                                           CustomSettingsStore.getSharedInstance().getSettingsObject(this.key).getUnitIncrement()));
        amountFormat = NumberFormat.getNumberInstance();
        tf = new TWFormattedTextField(amountFormat);
        tf.setValue(new Double(amount));
        tf.setColumns(10);
        tf.addPropertyChangeListener("value", this);
        tf.setHorizontalAlignment(SwingConstants.RIGHT);
//        tf.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));
        tf.setEditable(true);
//        tf.setText("" + blockSpinnerDefault);
        tf.setBorder(BorderFactory.createEmptyBorder());
        blockSpinner.setEditor(tf);
        tf.setBackground(Color.white);
        blockSpinner.setBorder(BorderFactory.createEmptyBorder());
        quantityPanel.add(blockSpinner);
        quantityPanel.add(dowwnArrow);
//        tf =new TWTextField();
//        tf.setEditable(true);
//        tf.setBackground(Color.white);
//        tf.addKeyListener(this);
        lowerFirst.setLayout(new FlexGridLayout(new String[]{"30", "100", "100%"}, new String[]{"100%"}));  //"5","30","100%","90","20","5"
//        lowerFirst.add(new JLabel());
        addLotSizesBtn = new TWButton();
        addLotSizesBtn.addActionListener(this);
        lowerFirst.add(currencyLabel);
        lowerFirst.add(quantityPanel);
        lowerFirst.add(addLotSizesBtn);
//        lowerFirst.add(new JLabel());
        lowerSecond = new JPanel();
        ownedAmount = new TWFormattedTextField(amountFormat);
        netAmtLabel = new JLabel("Net");
        ownedAmount.setHorizontalAlignment(SwingConstants.RIGHT);
        ownedAmount.setEditable(false);
        lowerSecond.setLayout(new FlexGridLayout(new String[]{"30", "100%", "110"}, new String[]{"100%"}));    //"5","30","100%","110","5"
//        lowerSecond.add(new JLabel());
        lowerSecond.add(netAmtLabel);
        lowerSecond.add(new JLabel());
        lowerSecond.add(ownedAmount);
//        lowerSecond.add(new JLabel());
        lowerPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"50%", "50%"}));
        lowerPanel.add(lowerFirst);
        /**
         * Net Amount panel removed, It may be usefull in near future
         */
        lowerPanel.add(lowerSecond);
        lowerPanel.setPreferredSize(new Dimension(145, 36));
        dowwnArrow.addActionListener(this);
        blockSpinner.addChangeListener(this);
        return lowerPanel;
    }

    private void reloadBlockSpinner() {
        blockSpinner.setModel(new SpinnerNumberModel(CustomSettingsStore.getSharedInstance().getSettingsObject(this.key).getAmtArray()[0],
                                                           CustomSettingsStore.getSharedInstance().getSettingsObject(this.key).getDefaultAmmount(),
                                                           CustomSettingsStore.getSharedInstance().getSettingsObject(this.key).getAmtArray()[5],
                                                           CustomSettingsStore.getSharedInstance().getSettingsObject(this.key).getUnitIncrement()));
    }

    public void populateForexCombo(String id) {
//        try {
//
//            Enumeration e = sortedSymbols.elements();//keys();
//            while (e.hasMoreElements()) {
//                String first = (String) e.nextElement();
//                firstPart.addItem(first);
//                first = null;
//
//            }
//
//        } catch (Exception e) {
//
//        }
//        loadSecondarySymbol("" + firstPart.getItemAt(0));
//        secondPart.setSelectedIndex(0);
        fxArray.add(new TWComboItem("EURJPY","EUR-JPY"));
        fxArray.add(new TWComboItem("EURUSD","EUR-USD"));
        fxArray.add(new TWComboItem("AUDUSD","AUD-USD"));
        fxArray.add(new TWComboItem("EURCHF","EUR-CHF"));
        fxArray.add(new TWComboItem("USDGBP","USD-GBP"));
        fxArray.add(new TWComboItem("USDJPY","USD-JPY"));
        if(!id.equals("")){

        currencyCombo.setSelectedItem(getSelectedItemForKey(id));

        }else{


    }
        loadSecondarySymbol("EUR");
        loadSecondarySymbol("AUD");
        loadSecondarySymbol("USD");
        loadSecondarySymbol("GBP");

    }
    private TWComboItem getSelectedItemForKey(String id){
                TWComboItem item;
            for (int i = 0; i < fxArray.size(); i++) {
                item = (TWComboItem) fxArray.get(i);
                if ((item.getId().equals(id))){
                    return item;
                }
            }
            return null;
    }
    public void loadAllSymbols() {
        prop = new SmartProperties("=");
        try {
            prop = new SmartProperties("=");
            prop.loadCompressed(Settings.CONFIG_DATA_PATH+"/forex.msf");
            sortSymbols();
        } catch (Exception s_e) {
            s_e.printStackTrace();
        }
    }

    public void sortSymbols() {
        sortedSymbols = new Vector(prop.keySet());
        Collections.sort(sortedSymbols);

    }

    public synchronized void loadSecondarySymbol(String symbolID) {
        String id = symbolID;
        secondPart.removeAllItems();
        String[] secondaryArray = null;
        String[] pipSeparated = null;
        try {
            secondaryArray = getList(prop.getProperty(symbolID));
            Arrays.sort(secondaryArray);
            if (secondaryArray != null) {
                if ((secondaryArray.length > 0)) {
                    for (int i = 0; i < secondaryArray.length; i++) {
                        String splitPattern = "~";
                        pipSeparated = secondaryArray[i].split(splitPattern);
//                        secondPart.addItem(pipSeparated[0]);
                        pipHash.put(SharedMethods.getKey("FORX", symbolID, Meta.INSTRUMENT_FOREX) + pipSeparated[0], pipSeparated[1]);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
//        currencyLabel.setText(id);
//        secondPart.updateUI();

    }

    public void actionPerformed(ActionEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getSource() == bidBtn) {
            try {
                //ForexStore.getSharedInstance().printForexStore();
                FXObject fxo = (FXObject) ForexStore.forexStore.get(this.key);
                BlockObect bxo = (BlockObect) fxo.getBlockObject(selectedBlockSize);
                BestBuySellObject bbxo = (BestBuySellObject) fxo.getBestObject(selectedBlockSize);
                System.out.println("Parameter Testing : " + getSymbol(this.key) + " , " + Integer.parseInt(blockSizeTrimer(tf.getText())) + " , " + fxo.getForexQuoteID() + " , " + selectedBlockSize + " ,newly added : " + bbxo.getBuyQuoteId() + " , " + bbxo.getBuyBrokerId() + " , " + bbxo.getBuyValue() + ", buy value : " + fxo.getBuyValForGivenBlock(selectedBlockSize));
                if (TradingShared.isConnected()) {
                    TradeMethods.getSharedInstance().sendNewOrder(true, false, 0, ((TWComboItem) parent.portfolioCombo.getSelectedItem()).getId(), "FORX", getSymbol(this.key),
                            1, TradeMeta.ORDER_TYPE_QUOTED, fxo.getSellValForGivenBlock(fxo.getBlockSizeForGivenQuantity(Long.parseLong(blockSizeTrimer(tf.getText())))), 0, "*",
                            Integer.parseInt(blockSizeTrimer(tf.getText())), (short) 0, 0, -1, 0, false, 0, null, Meta.INSTRUMENT_QUOTE, -1, null, -1, null, null, null, -1, fxo.getForexQuoteID(), bbxo.getBuyQuoteId(), bbxo.getBuyBrokerId(), bbxo.getBuyValue(),null,0,0);
                } else {
                    JOptionPane.showMessageDialog(Client.getInstance().getDesktop(), Language.getString("TRADE_CONNECT_ERROR"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                    return;
                }
                fxo = null;
                bxo = null;
            } catch (Exception e1) {
                System.out.println("Forex feed unavailable");
            }
        } else if (e.getSource() == askBtn) {
            try {
                FXObject fxo = (FXObject) ForexStore.forexStore.get(this.key);
                BlockObect bxo = (BlockObect) fxo.getBlockObject(selectedBlockSize);
                BestBuySellObject bbxo = (BestBuySellObject) fxo.getBestObject(selectedBlockSize);
                System.out.println("Parameter Testing : " + getSymbol(this.key) + " , " + Integer.parseInt(blockSizeTrimer(tf.getText())) + " , " + fxo.getForexQuoteID() + " , " + selectedBlockSize + " ,newly added : " + bbxo.getBuyQuoteId() + " , " + bbxo.getBuyBrokerId() + " , " + bbxo.getBuyValue());
                if (TradingShared.isConnected()) {
                    TradeMethods.getSharedInstance().sendNewOrder(true, false, 0, ((TWComboItem) parent.portfolioCombo.getSelectedItem()).getId(), "FORX", getSymbol(this.key),
                            2, TradeMeta.ORDER_TYPE_QUOTED, fxo.getBuyValForGivenBlock(fxo.getBlockSizeForGivenQuantity(Long.parseLong(blockSizeTrimer(tf.getText())))), 0, "*",
                            Integer.parseInt(blockSizeTrimer(tf.getText())), (short) 0, 0, -1, 0, false, 0, null, Meta.INSTRUMENT_QUOTE, -1, null, -1, null, null, null, -1, fxo.getForexQuoteID(), bbxo.getSellQuoteId(), bbxo.getSellBrokerId(), bbxo.getSellValue(),null,0,0);
                } else {
                    JOptionPane.showMessageDialog(Client.getInstance().getDesktop(), Language.getString("TRADE_CONNECT_ERROR"), Language.getString("ERROR"), JOptionPane.ERROR_MESSAGE);
                    return;
                }
                fxo = null;
                bxo = null;
            } catch (Exception e1) {
                System.out.println("Forex feed unavailable");
            }
        } else if (e.getSource() == firstPart) {
            secondPart.removeAllItems();
            loadSecondarySymbol("" + firstPart.getSelectedItem());
            if ((firstPart.getSelectedItem() != null) && (secondPart.getSelectedItem() != null)) {
                this.key = SharedMethods.getKey("FORX", "" + firstPart.getSelectedItem() + secondPart.getSelectedItem(), Meta.INSTRUMENT_FOREX);
                String symbol = "" + firstPart.getSelectedItem() + secondPart.getSelectedItem();
                if ((this.startingKey != null) && (this.key != null)) {
                    if (!(this.key.equals(this.startingKey))) {
                        if (Settings.isConnected()) {
                            DataStore.getSharedInstance().removeSymbolRequest(this.startingKey);
                            ForexBoard.getInstance().removeSymbolsFromWatchListStore(this.startingKey);
                            String requestID = "ForexSearch:" + parent.watchListID + System.currentTimeMillis();
                            SendQFactory.addValidateRequest(symbol, requestID, null,0);
                            this.startingKey = this.key;
                        }

                    }
                }
            }

        } else if (e.getSource() == secondPart) {
            if ((firstPart.getSelectedItem() != null) && (secondPart.getSelectedItem() != null)) {
                this.key = SharedMethods.getKey("FORX", "" + firstPart.getSelectedItem() + secondPart.getSelectedItem(), Meta.INSTRUMENT_FOREX);
                String symbol = "" + firstPart.getSelectedItem() + secondPart.getSelectedItem();
                if ((this.startingKey != null) && (this.key != null)) {
                    if (!(this.key.equals(this.startingKey))) {
                        if (Settings.isConnected()) {
                            DataStore.getSharedInstance().removeSymbolRequest(this.startingKey);
                            ForexBoard.getInstance().removeSymbolsFromWatchListStore(this.startingKey);
                            String requestID = "ForexSearch:" + parent.watchListID + System.currentTimeMillis();
                            SendQFactory.addValidateRequest(symbol, requestID, null,0);
                            this.startingKey = this.key;
                        }
                    }
                }
            }
        } else if (e.getSource() == dowwnArrow) {
            Point location = new Point(lowerFirst.getX(), (lowerFirst.getY()) + lowerFirst.getBounds().height);
            SwingUtilities.convertPointToScreen(location, lowerFirst.getParent());
            JPopupMenu jl = createBlockSizeList();
            jl.setLocation(location);
            jl.setPreferredSize(new Dimension(lowerFirst.getWidth(), 130));
            jl.show(lowerFirst, 2, lowerFirst.getHeight() + 2);
            this.selectedBlockSize = Long.parseLong((blockSpinner.getValue() + "").split("\\.")[0]);
        } else if (e.getSource() == blockSpinner) {
            this.selectedBlockSize = Long.parseLong((blockSpinner.getValue() + "").split("\\.")[0]);
        } else if (e.getSource() == closeBtn) {
            parent.removeFXView(this);
        } else if (e.getSource().equals(addLotSizesBtn)) {
            Point location = new Point(lowerFirst.getX(), (lowerFirst.getY()) + lowerFirst.getBounds().height);
            SwingUtilities.convertPointToScreen(location, lowerFirst.getParent());
            CustomLotSizeWindow clsw = CustomLotSizeWindow.getSharedInstance();
            clsw.setKey(this.key);
            clsw.setData(this.key);
            clsw.setLocationRelativeTo(Client.getInstance().getFrame());
            clsw.setVisible(true);
            reloadBlockSpinner();
        }else if(e.getSource().equals(currencyCombo)){
            TWComboItem  item=(TWComboItem)currencyCombo.getSelectedItem();
            this.key = SharedMethods.getKey("FORX", "" + item.getId(), Meta.INSTRUMENT_FOREX);
                String symbol =item.getId();
                if ((this.startingKey != null) && (this.key != null)) {
                    if (!(this.key.equals(this.startingKey))) {
                        if (Settings.isConnected()) {
                            DataStore.getSharedInstance().removeSymbolRequest(this.startingKey);
                            ForexBoard.getInstance().removeSymbolsFromWatchListStore(this.startingKey);
                            String requestID = "ForexSearch:" + parent.watchListID + System.currentTimeMillis();
                            SendQFactory.addValidateRequest(symbol, requestID, null,0);
                            this.startingKey = this.key;
                            currencyLabel.setText(symbol.substring(0,3));
                        }
                    }
                }


        }
    }

    public String[] getList(String sID) {
        String[] saCols = null;
        int i = 0;
        try {
            StringTokenizer oTokens = new StringTokenizer(sID, ",");
            saCols = new String[oTokens.countTokens()];

            i = 0;
            while (oTokens.hasMoreTokens()) {
                saCols[i] = oTokens.nextToken();
                if (saCols[i].equals(".")) {
                    saCols[i] = "";
                }
                i++;
            }
            oTokens = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return saCols;

    }

    public JPopupMenu createBlockSizeList() {
        JPopupMenu blockPopup = new JPopupMenu("Block Popup");
        blockPopup.applyComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
        arrayBlockSizes = CustomSettingsStore.getSharedInstance().getSettingsObject(this.key).getAmtArray();
//        arrayBlockSizes[0]=1000000;
//        arrayBlockSizes[1]=1500000;
//        arrayBlockSizes[2]=2000000;
//        arrayBlockSizes[3]=2500000;
//        arrayBlockSizes[4]=3000000;
//        arrayBlockSizes[5]=3500000;
//        arrayBlockSizes[6]=4000000;
//        arrayBlockSizes[7]=4500000;
//        arrayBlockSizes[8]=5000000;
        try {
            for (int i = 0; i < arrayBlockSizes.length; i++) {
                final JMenuItem blockItem = new JMenuItem();
//                 blockItem.setBackground(blockItemSelectedColor);
                blockItem.setForeground(blockItemFGColor);
                blockItem.applyComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
                blockItem.setBorder(BorderFactory.createEmptyBorder());
                blockItem.setText(amountFormat.format(arrayBlockSizes[i]));
                blockItem.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        tf.setValue(Long.parseLong(blockSizeTrimer(blockItem.getText())));
                        selectedBlockSize = Long.parseLong(blockSizeTrimer(blockItem.getText()));
                        blockSpinner.setValue(Double.parseDouble(blockSizeTrimer(blockItem.getText())));
                    }
                });
                blockPopup.add(blockItem);
            }

        } catch (Exception e) {
            System.out.println("Error while loading block sizes...");
            e.printStackTrace();
        }
/*        try{
            FXObject fxo=ForexStore.getSharedInstance().getForexObject(this.key);
            ArrayList<BlockObect> blocks=fxo.getBlockObjects();
            for (BlockObect blockObect: blocks){
                 final JMenuItem blockItem=new JMenuItem ();
//                 final TWFormattedTextField blockItem=new TWFormattedTextField (amountFormat);

                 blockItem.applyComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
                 blockItem.setBorder(BorderFactory.createEmptyBorder());
                 blockItem.setText(amountFormat.format(blockObect.getForexBlockSize()));
//                 blockItem.setValue(blockObect.getForexBlockSize());
                 blockItem.addActionListener(new java.awt.event.ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        tf.setValue(Long.parseLong(blockSizeTrimer(blockItem.getText())));
                        selectedBlockSize=Long.parseLong(blockSizeTrimer(blockItem.getText()));
                      }
                });
                blockPopup.add(blockItem);
//                GUISettings.applyOrientation(blockPopup);
            }
        }catch(Exception e){
            System.out.println("Error while loading block sizes...");
            e.printStackTrace();

    }*/
        return blockPopup;
    }

    public void createRightClickPopup() {
        rightClickPopup = new JPopupMenu();
        removeInstrument = new TWMenuItem("Remove Instrument");
        rightClickPopup.add(removeInstrument);
        removeInstrument.addActionListener(this);
        GUISettings.applyOrientation(rightClickPopup);
    }

    public String blockSizeTrimer(String longString) {
        String splitpatern = ",";
        String[] parts = longString.split(splitpatern);
        String blocksize = "";
        for (int i = 0; i < parts.length; i++) {
            blocksize = blocksize + parts[i];
        }
        return blocksize.trim();
    }

    /**
     * This method use to add symbol validate requests while its loading from watchlists
     *
     * @param left
     * @param right
     */
    public void setInitialSymbol(String left, String right) {
        firstPart.setSelectedItem("" + left);
        secondPart.setSelectedItem("" + right);
        if (Settings.isConnected()) {
            try {
                String requestID = "ForexSearch:" + parent.watchListID + System.currentTimeMillis();
                SendQFactory.addValidateRequest(left + right, requestID, null,0);
            } catch (Exception e) {

            }
        }
    }

    public String getSymbol(String key) {
        return SharedMethods.getSymbolFromKey(key);

    }

    public long getNetVal() {
        return netVal;
    }

    public void setNetVal(long netVal) {
        this.netVal = netVal;
    }

    public long getNetExp() {
        return netExp;
    }

    public void setNetExp(long netExp) {
        this.netExp = netExp;
    }

    public void mouseClicked(MouseEvent e) {

    }

    public void loadButtonImages() {
        String sThemeID = Theme.getID();
        closeBtn.setIcon(new ImageIcon("images/Theme" + sThemeID + "/forexCloseBtn.gif"));
        closeBtn.updateUI();
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dragEnter(DragSourceDragEvent dsde) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dragOver(DragSourceDragEvent dsde) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dropActionChanged(DragSourceDragEvent dsde) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dragExit(DragSourceEvent dse) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dragDropEnd(DragSourceDropEvent dsde) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dragEnter(DropTargetDragEvent dtde) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dragOver(DropTargetDragEvent dtde) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dropActionChanged(DropTargetDragEvent dtde) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dragExit(DropTargetEvent dte) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void drop(DropTargetDropEvent dtde) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void dragGestureRecognized(DragGestureEvent dge) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getSource() == tf) {
            System.out.println("Value typed : " + selectedBlockSize);
        }
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        int code = e.getKeyCode();
        if (code == KeyEvent.VK_ENTER) {
            e.consume();
        }
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void stateChanged(ChangeEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getSource() == blockSpinner) {
            tf.setValue(blockSpinner.getValue());
//            tf.setText(blockSpinner.getValue() + "");
            selectedBlockSize = Long.parseLong((blockSpinner.getValue() + "").split("\\.")[0]);
        }
    }

    public void propertyChange(PropertyChangeEvent evt) {
        //To change body of implemented methods use File | Settings | File Templates.
        Object source = evt.getSource();
        if (source == tf) {
            amount = ((Number) tf.getValue()).doubleValue();
        }
    }
    public JLabel getTimeLable(){
        return timeLabel;
    }
    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
        blockItemSelectedColor = Theme.getColor("FOREX_POPUP_SELECTED");
        blockItemFGColor = Theme.getColor("FOREX_POPUP_FG");
    }

    public void updatenetAmmount(long text) {
        ownedAmount.setValue(new Long(text));
    }

    class ListComparator implements Comparator {

        public int compare(Object o1, Object o2) {
            return ((String) o1).compareToIgnoreCase((String) o2);
        }

    }

    // alpha composite
//    public void paintComponent(Graphics g) {
//        super.paintComponent(g);
//        Graphics2D g2d = (Graphics2D) g;
////        for (int i = 0; i < 11; i++) {
//            drawSquares(g2d, 6 * 0.1F);
//            g2d.translate(deltaX, 0);
////        }
//    }
//    private AlphaComposite makeComposite(float alpha) {
//    int type = AlphaComposite.SRC_ATOP;
//    return(AlphaComposite.getInstance(type, alpha));
//  }
//
//  private void drawSquares(Graphics2D g2d, float alpha) {
//    Composite originalComposite = g2d.getComposite();
//    g2d.setPaint(Color.blue);
//    g2d.fill(blueSquare);
//    g2d.setComposite(makeComposite(alpha));
//    g2d.setPaint(Color.red);
//    g2d.fill(redSquare);
//    g2d.setComposite(originalComposite);
//  }
//  private static int gap=10, width=60, offset=20,
//                     deltaX=gap+width+offset;
//  private Rectangle
//    blueSquare = new Rectangle(gap+offset, gap+offset, width, width),
//    redSquare = new Rectangle(gap, gap, width, width);
}
