package com.isi.csvr.forex;

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.Client;
import com.isi.csvr.trading.datastore.Transaction;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.table.TWTextField;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 23-Nov-2007 Time: 10:53:51 To change this template use File | Settings
 * | File Templates.
 */
public class ForexCancelOrder extends InternalFrame implements ActionListener {
    public JPanel upperPanel;
    public JPanel midlePanel;
    public JPanel lowerPanel;
    public JPanel quantityPanel;
    private JLabel symbolLabel;
    private JLabel quantityLabel;
    private JLabel typeLabel;
    private JLabel priceLabel;
    private JLabel pfLabel;
    private TWTextField symbolField;
    private TWTextField priceField;
    public TWComboBox typeCombo;
    public TWComboBox portfolioCombo;
    private JButton dowwnArrow;
    private TWTextField tf;
    private JSpinner blockSpinner;
    private TWButton cancelBtn;
    private TWButton closeBtn;

    //Transaction Object Related
    String orderID;
    public static ForexCancelOrder self;
    Transaction oldTransaction;

   public static ForexCancelOrder getSharedInstance(Transaction tr) {
        if (self == null) {
            self = new ForexCancelOrder(tr);
        }
        self.dispose();
        self=null;
        self=new ForexCancelOrder(tr);
        return self;
    }

    public ForexCancelOrder(Transaction tr) {
        super();
        this.oldTransaction=tr;
        this.setTitle(Language.getString("FX_ORDER_CANCEL"));
        this.setSize(290, 196);
        this.setClosable(true);
        this.setIconifiable(true);
        this.setResizable(false);
        this.setLayer(GUISettings.TOP_LAYER);
        Client.getInstance().getDesktop().add(this);
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%","40"}, 0, 0));
        upperPanel = new JPanel();
        midlePanel = new JPanel();
        lowerPanel = new JPanel();
        quantityPanel=new JPanel();
        symbolLabel = new JLabel(Language.getString("CUS_INDEX_SYMBOL"));
        priceLabel = new JLabel(Language.getString("PRICE"));
        pfLabel = new JLabel(Language.getString("PORTFOLIO"));
        quantityLabel = new JLabel(Language.getString("WHAT_IF_CALC_QTY"));
        typeLabel = new JLabel(Language.getString("TYPE"));
        symbolField=new TWTextField();
        priceField=new TWTextField();
        typeCombo=new TWComboBox();
        typeCombo=new TWComboBox();
        cancelBtn=new TWButton(Language.getString("CANCEL_ORDER"));
        closeBtn=new TWButton(Language.getString("CLOSE"));
        portfolioCombo=new TWComboBox();
        cancelBtn.addActionListener(this);
        closeBtn.addActionListener(this);
        this.add(createUpperPanel());
        this.add(createMiddlePanel());
        populateTypeCombo();
        GUISettings.applyOrientation(this);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setData(tr);

    }

    public JPanel createUpperPanel(){
        upperPanel.setLayout(new FlexGridLayout(new String[]{"50%", "110", "10", "130", "50%"}, new String[]{"20", "20", "20", "20", "20", "100%"}, 5, 5));
        upperPanel.add(new JLabel());
        upperPanel.add(symbolLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(symbolField);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(quantityLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(createQuantityPanel());
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(typeLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(typeCombo);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(priceLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(priceField);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(pfLabel);
        upperPanel.add(new JLabel());
        upperPanel.add(portfolioCombo);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        GUISettings.applyOrientation(upperPanel);

        return upperPanel;

    }
    public JPanel createMiddlePanel(){
        midlePanel.setLayout(new FlexGridLayout(new String[]{"50%", "30", "110", "110", "50%"}, new String[]{"25"}, 5, 5));
        midlePanel.add(new JLabel());
        midlePanel.add(new JLabel());
        midlePanel.add(closeBtn);
        midlePanel.add(cancelBtn);
        midlePanel.add(new JLabel());
        return midlePanel;
    }
    public void createLowerPanel(){

    }
        public JPanel createQuantityPanel() {
        quantityPanel = new JPanel();
        quantityPanel.setLayout(new FlexGridLayout(new String[]{"85%", "15%"}, new String[]{"100%"}));
        dowwnArrow = new JButton(new DownArrow());
        dowwnArrow.setBorderPainted(false);
        blockSpinner = new JSpinner(new SpinnerNumberModel(1000000, 1000000, 3000000, 500000));
        tf = new TWTextField();
        tf.setHorizontalAlignment(SwingConstants.RIGHT);
        tf.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));
        tf.setEditable(true);
        blockSpinner.setEditor(tf);
        tf.setBackground(Color.white);
        quantityPanel.add(blockSpinner);
        quantityPanel.add(dowwnArrow);
        dowwnArrow.addActionListener(this);
        return quantityPanel;

    }
    public void setData(Transaction tr){
        if(tr!=null){
            symbolField.setText(tr.getSymbol());
            tf.setText(tr.getOrderQuantity()+"");
            typeCombo.addItem(tr.getType());
            priceField.setText(tr.getPrice()+"");
            portfolioCombo.addItem(tr.getPortfolioNo());
            setOrderType(tr.getType(),0,tr);
            symbolField.setEnabled(false);
            priceField.setEnabled(false);
            tf.setEnabled(false);
            portfolioCombo.setEnabled(false);
            dowwnArrow.setEnabled(false);

        }

    }
      public void populateTypeCombo() {
        typeCombo.addItem(new TWComboItem("1", Language.getString("ORDER_TYPE_MARKET")));
        typeCombo.addItem(new TWComboItem("2", Language.getString("ORDER_TYPE_LIMIT")));
        typeCombo.addItem(new TWComboItem("3", Language.getString("ORDER_TYPE_QUOTE")));
    }
        private void setOrderType(char type, int side, Transaction tr) {
        try {
            switch (type) {
                case'D':
                    typeCombo.setSelectedIndex(2);
                    typeCombo.setEnabled(false);
                    break;
                case'1':
                    typeCombo.setSelectedIndex(0);
                    typeCombo.setEnabled(false);
                    break;
                case'2':
                    typeCombo.setSelectedIndex(1);
                    typeCombo.setEnabled(false);
                    break;
                case 3:
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }
    public void actionPerformed(ActionEvent e){
        if(e.getSource().equals(cancelBtn)){
            try {
                TradeMethods.cancelOrder(this.oldTransaction);
            } catch (Exception e1) {
                System.out.println("Cancel Forex order failed....");
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            self=null;
            this.dispose();

        }
        if(e.getSource().equals(closeBtn)){
            self=null;
            this.dispose();

        }

    }
    public void internalFrameClosing(InternalFrameEvent e) {
        this.dispose();
        self=null;

    }
}
