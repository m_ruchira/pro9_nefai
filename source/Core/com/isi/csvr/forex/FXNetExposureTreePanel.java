package com.isi.csvr.forex;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.properties.SmartProperties;

import javax.swing.*;
import java.util.Vector;
import java.awt.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 03-Jul-2008 Time: 13:14:30 To change this template use File | Settings
 * | File Templates.
 */
public class FXNetExposureTreePanel extends JPanel {
    private Vector componetVector;
    private JTree tree;
    private JScrollPane scrollPane;
    private Object[] rootNodes;
    private Vector rootVector;

    public FXNetExposureTreePanel(Vector components){
        this.componetVector=components;
        this.setLayout(new BorderLayout());
        createUI();

    }
        public void createUI() {
        rootNodes=loadForexSymbols();
        rootVector = new ForexVector("Root", null, rootNodes);
        tree = new JTree(rootVector);
        tree.putClientProperty("JTree.lineStyle", "Vertical");
        tree.setToggleClickCount(1);
        JScrollPane scrollPane = new JScrollPane(tree);
        this.add(scrollPane, BorderLayout.CENTER);
        GUISettings.applyOrientation(this);
        tree.updateUI();
        tree.repaint();
        this.repaint();
        this.updateUI();
        this.repaint();
    }
        private Object[] loadForexSymbols() {
        Vector tempVector=new Vector();
        for(int i=0;i<componetVector.size();i++){
            String forexKey=((String)componetVector.get(i));
            tempVector.add(new ForexVector(forexKey,"",new Object[]{"Test"}));
//            System.out.println("FX Test : "+forexSymbolList);
        }
        return tempVector.toArray();
    }
}
