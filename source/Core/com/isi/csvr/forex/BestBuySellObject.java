package com.isi.csvr.forex;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 30-Apr-2008 Time: 12:23:47 To change this template use File | Settings
 * | File Templates.
 */
public class BestBuySellObject {
    public String buyQuoteId;
    public String sellQuoteId;
    public String buyBrokerId;
    public String sellBrokerId;
    public double buyValue;
    public double sellValue;
    public long blockSize;

    public BestBuySellObject(){

    }

    public long getBlockSize() {
        return blockSize;
    }

    public void setBlockSize(long blockSize) {
        this.blockSize = blockSize;
    }

    public String getBuyQuoteId() {
        return buyQuoteId;
    }

    public void setBuyQuoteId(String buyQuoteId) {
        this.buyQuoteId = buyQuoteId;
    }

    public String getSellQuoteId() {
        return sellQuoteId;
    }

    public void setSellQuoteId(String sellQuoteId) {
        this.sellQuoteId = sellQuoteId;
    }

    public String getBuyBrokerId() {
        return buyBrokerId;
    }

    public void setBuyBrokerId(String buyBrokerId) {
        this.buyBrokerId = buyBrokerId;
    }

    public String getSellBrokerId() {
        return sellBrokerId;
    }

    public void setSellBrokerId(String sellBrokerId) {
        this.sellBrokerId = sellBrokerId;
    }

    public double getBuyValue() {
        return buyValue;
    }

    public void setBuyValue(double buyValue) {
        this.buyValue = buyValue;
    }

    public double getSellValue() {
        return sellValue;
    }

    public void setSellValue(double sellValue) {
        this.sellValue = sellValue;
    }
}
