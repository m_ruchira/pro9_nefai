package com.isi.csvr.chat;

import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Chandika Hewage
 * Date: Aug 28, 2006
 * Time: 9:52:31 AM
 * To change this template use File | Settings | File Templates.
 */
public class CustomerPulse extends Thread{
//    protected DataOutputStream o;
    CustomerMain parent;
    boolean isActive = true;
    int seconds;
//    Timer timer;

    public CustomerPulse(CustomerMain parent, int seconds) {
        super("Chat Pulse Thread");
        this.parent = parent;
        this.seconds = seconds;
        this.start();
    }

    public void run() {
        isActive = true;
        while (isActive) {
//            if(CustomerMain.isServerConnected){
                if(!CustomerMain.isTimeExpired){
                    Date now = new Date();
                    if (CustomerMain.lastupDate != null) {
//                        System.out.println("Sending pulse==>customer");
                    //                MessageFormater mf = new MessageFormater();
                        long dif = now.getTime() - CustomerMain.lastupDate.getTime();
                        long seconds = dif / 1000;
                        if (seconds >= 15) {
                           parent.write(MessageFormater.makeCustomerPulse());
                        } else {
//                            System.out.println("No need to send pulse");
                        }
                    } else {
                        System.out.println("Not started communication yet");
                    }

                    try {
                        Thread.sleep(5000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }else{
                    break;
                }
//            }
        }
    }

    public void exitThread(){
        isActive = false;
        try {
            this.interrupt();
            parent = null;
//            this.stop();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

}
