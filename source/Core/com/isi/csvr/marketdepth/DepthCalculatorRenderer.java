// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.marketdepth;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.table.ExchangeFormatInterface;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

public class DepthCalculatorRenderer extends TWBasicTableRenderer {

    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private String[] g_asMarketStatus = new String[4];

    private String g_sNA = Language.getString("NA");

    private static Color g_oUpFGColor;
    private static Color g_oDownFGColor;
    private static Color g_oUpBGColor;
    private static Color g_oDownBGColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color g_oDownColor;
    private static Color g_oUpColor;
    private static Color g_oHeaderBGColor;
    private static Color g_oHeaderFGColor;

    private Color upColor;
    private Color downColor;
    private Color headerFG;
    private Color headerBG;

    private Color foreground, background;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oQuantityFormat;

    private long longValue;

    public DepthCalculatorRenderer() {
        reload();
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
        try {
            g_asMarketStatus[0] = Language.getString("STATUS_PREOPEN");
            g_asMarketStatus[1] = Language.getString("STATUS_OPEN");
            g_asMarketStatus[2] = Language.getString("MARKET_CLOSE");
            g_asMarketStatus[3] = Language.getString("PRECLOSE");
        } catch (Exception e) {
            g_asMarketStatus[0] = g_sNA;
            g_asMarketStatus[1] = g_sNA;
            g_asMarketStatus[2] = g_sNA;
            g_asMarketStatus[3] = g_sNA;
        }

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
    }

    public void propertyChanged(int property) {
    }

    public static void reloadForPrinting() {
        g_oUpFGColor = Color.black;
        g_oDownFGColor = Color.black;
        g_oUpBGColor = Color.white;
        g_oDownBGColor = Color.white;
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
        g_oDownColor = Color.black;
        g_oUpColor = Color.black;
        g_oHeaderBGColor = Color.white;
        g_oHeaderFGColor = Color.black;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oUpFGColor = Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR");
            g_oDownFGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR");
            g_oUpBGColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownBGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            g_oDownColor = g_oDownBGColor;
            g_oUpColor = g_oUpBGColor;
            g_oHeaderBGColor = Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR");
            g_oHeaderFGColor = Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR");

        } catch (Exception e) {
            g_oUpBGColor = Color.green;
            g_oDownBGColor = Color.red;
            g_oUpFGColor = Color.black;
            g_oDownFGColor = Color.black;
            g_oDownColor = Color.red;
            g_oUpColor = Color.green;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        float[] adPrice;
        long[] aalQuantity;

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
            upColor = sett.getPositiveChangeFG();
            downColor = sett.getNegativeChangeFG();
            headerBG = sett.getHeaderColorBG();
            headerFG = sett.getHeaderColorFG();
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
            upColor = g_oUpColor;
            downColor = g_oDownColor;
            headerBG = g_oHeaderBGColor;
            headerFG = g_oHeaderFGColor;
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        int iRendID = getRenderingID(row, column);

        try {
            lblRenderer.setIcon(null);

            switch (row) {
                case 2:
                    lblRenderer.setToolTipText((String) table.getModel().getValueAt(-1, 0));
                    break;
                default:
                    lblRenderer.setToolTipText(null);
            }

            if (oPriceFormat == null) {
                oPriceFormat = (((ExchangeFormatInterface) table.getModel()).getDecimalFormat());
            }

            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setBackground(background);
                    lblRenderer.setForeground(foreground);
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // Normal Title
                    lblRenderer.setBackground(background);
                    lblRenderer.setForeground(foreground);
                    lblRenderer.setText(" " + value + " ");
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 2: // Title
                    lblRenderer.setBackground(headerBG);
                    lblRenderer.setForeground(headerFG);
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 3: // PRICE
                    double dvalue = toDoubleValue(value);
                    if ((dvalue == Long.MAX_VALUE) || (Double.isNaN(dvalue)) || (Double.isInfinite(dvalue))||(dvalue<0))
                        lblRenderer.setText(g_sNA);
                    else
                        lblRenderer.setText(oPriceFormat.format(dvalue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // QUANTITY
                    longValue = toLongValue(value);
                    if ((longValue == Long.MAX_VALUE))
                        lblRenderer.setText(g_sNA);
                    else
                        lblRenderer.setText(oQuantityFormat.format(longValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 5: // CHANGE
                    double dValue = toDoubleValue(value);
                    lblRenderer.setText(oPriceFormat.format(dValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (dValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (dValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 'P': // PRICE
                    adPrice = (float[]) value;
                    lblRenderer.setText(oPriceFormat.format(adPrice[0]));
                    if (adPrice.length > 1) {
                        if (adPrice[0] > adPrice[1]) {
                            lblRenderer.setBackground(g_oUpBGColor);
                            lblRenderer.setForeground(g_oUpFGColor);
                        } else if (adPrice[0] < adPrice[1]) {
                            lblRenderer.setForeground(g_oDownFGColor);
                            lblRenderer.setBackground(g_oDownBGColor);
                        }
                        adPrice[1] = adPrice[0];
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'q': // Quantity with coloured bg
                    aalQuantity = (long[]) value;
                    lblRenderer.setText(oQuantityFormat.format(aalQuantity[0]));
                    if (aalQuantity.length > 1) {
                        if (aalQuantity[0] > aalQuantity[1]) {
                            lblRenderer.setBackground(g_oUpBGColor);
                            lblRenderer.setForeground(g_oUpFGColor);
                        } else if (aalQuantity[0] < aalQuantity[1]) {
                            lblRenderer.setForeground(g_oDownFGColor);
                            lblRenderer.setBackground(g_oDownBGColor);
                        } else
                            aalQuantity[1] = aalQuantity[0];
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 'Q': // PRICE
                    longValue = toLongValue(value);
                    if ((longValue == 0) || (Double.isNaN(longValue)) || (Double.isInfinite(longValue))) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText(oQuantityFormat.format(longValue));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            lblRenderer.setText("");
        }

        adPrice = null;
        aalQuantity = null;

        return lblRenderer;
    }

    private int getRenderingID(int iRow, int iCol) {
        switch (iRow) {
            case 0:
                return 2;
            case 1:
                switch (iCol) {
                    case 0:
                        return 1;
                    case 1:
                        return 3;
                    case 2:
                        return 1;
                    case 3:
                        return 3;
                }
            case 2:
                switch (iCol) {
                    case 0:
                        return 1;
                    case 1:
                        return 3;
                    case 2:
                        return 1;
                    case 3:
                        return 3;
                }

            case 3:
                switch (iCol) {
                    case 0:
                        return 1;
                    case 1:
                        return 4;
                    case 2:
                        return 1;
                    case 3:
                        return 4;
                }
            case 4:
                switch (iCol) {
                    case 0:
                        return 1;
                    case 1:
                        return 3;
                    case 2:
                        return 1;
                    case 3:
                        return 3;
                }
            case 5:
                return 2;
            case 6:
                switch (iCol) {
                    case 0:
                        return 1;
                    case 1:
                        return 3;
                    case 2:
                        return 1;
                    case 3:
                        return 3;
                }

            case 7:
                switch (iCol) {
                    case 0:
                        return 1;
                    case 1:
                        return 'Q';
                    case 2:
                        return 1;
                    case 3:
                        return 'Q';
                }
            case 8:
            case 9:
            case 10:
                switch (iCol) {
                    case 0:
                        return 1;
                    case 1:
                        return 3;
                    case 2:
                        return 1;
                    case 3:
                        return 3;
                }
                /* case 9:
                switch(iCol) {
                    case 0:
                        return 1;
                    case 1:
                        return 3;
                    case 2:
                        return 1;
                    case 3:
                        return 3;
                }
            case 10:
                switch(iCol) {
                    case 0:
                        return 1;
                    case 1:
                        return 'V';
                    case 2:
                        return 1;
                    case 3:
                        return 'V';
                }*/
            case 11:
                return 2;
            case 12:
                switch (iCol) {
                    case 0:
                        return 1;
                    case 1:
                        return 3;
                    case 2:
                        return 1;
                    case 3:
                        return 3;
                }
            case 13:
                switch (iCol) {
                    case 0:
                        return 1;
                    case 1:
                        return 3;
                }
            case 14:
                switch (iCol) {
                    case 0:
                        return 1;
                    case 1:
                        return 3;
                }
        }
        return 1;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private int toIntValue(Object oValue) throws Exception {
        return Integer.parseInt((String) oValue);
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return Double.parseDouble((String) oValue);
    }


}