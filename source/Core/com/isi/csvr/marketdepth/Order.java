package com.isi.csvr.marketdepth;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 20, 2005
 * Time: 4:31:51 PM
 */
public class Order {
    private double price;
    private long quantity;

    public Order(double price, long quantity) {
        this.price = price;
        this.quantity = quantity;
    }

    public double getPrice() {
        return price;
    }

    public long getQuantity() {
        return quantity;
    }
}
