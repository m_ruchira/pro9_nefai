package com.isi.csvr.marketdepth;

import com.isi.csvr.datastore.DataStoreInterface;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class DepthStore implements DataStoreInterface {

    private Hashtable tree = null;
    private static DepthStore self = null;

    private DepthStore() {
        tree = new Hashtable();
    }

    public static synchronized DepthStore getInstance() {
        if (self == null) {
            self = new DepthStore();
        }
        return self;
    }

    public synchronized DepthObject getDepthFor(String symbol) {
        DepthObject depth = (DepthObject) tree.get(symbol);
        if (depth == null) {
            depth = new DepthObject();
            tree.put(symbol, depth);
        }
        return depth;
    }

    /*
     * Clear all depth objcts (initialize)
     */

    public void clear(String symbol) {
    }

    public void clear() {
        Enumeration depthObjects = tree.elements();

        while (depthObjects.hasMoreElements()) {
            DepthObject depthObject = (DepthObject) depthObjects.nextElement();
            depthObject.clear();
            depthObject = null;
        }
        depthObjects = null;
    }
    public String getXMLFor(String symbol,String depthMode){
        DepthObject depth = (DepthObject) tree.get(symbol);
        if (depth == null) {
            depth = new DepthObject();
            tree.put(symbol, depth);
        }
        return depth.getXML(symbol,depthMode);
    }
}