package com.isi.csvr.marketdepth;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 23, 2008
 * Time: 1:25:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class OddLot {

    public static final byte BID = 1;
    public static final byte ASK = 2;
    private double price;
    private String description;
    private long quantity;
    private byte priceType;
    private int depth_sequance;
    private long client_sequance;



    public OddLot(double price) {
        this.price = price;
    }

    public OddLot() {
    }

    public double getPrice() {
        return price;
    }
    public double getPrice(byte type) {
        if(type == priceType){
        return price;
        }else{
            return 0d;
        }
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public byte getPriceType() {
        return priceType;
    }

    public void setPriceType(byte priceType) {
        this.priceType = priceType;
    }

    public long getQuantity() {
        return quantity;
    }

    public void setQuantity(long quantity) {
        this.quantity = quantity;
    }

    public int getDepth_sequance() {
        return depth_sequance;
    }

    public void setDepth_sequance(int depth_sequance) {
        this.depth_sequance = depth_sequance;
    }

    public long getClientSequance() {
        return client_sequance;
    }

    public void setClientSequance(long client_sequance) {
        this.client_sequance = client_sequance;
    }
}
