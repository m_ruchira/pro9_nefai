package com.isi.csvr.marketdepth;

import com.isi.csvr.shared.Language;

import javax.swing.*;
import java.awt.*;
import java.text.DecimalFormat;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 10, 2003
 * Time: 3:52:49 PM
 * To change this template use Options | File Templates.
 */
public class TotalQuantityIndicator extends JPanel {
    private JLabel lblValue;
    private String label;
    private DecimalFormat format;

    public TotalQuantityIndicator(String label) {
        this.label = label;
        createUI();
    }

    private void createUI() {
        format = new DecimalFormat("#,##0");
        setLayout(new BoxLayout(this, BoxLayout.LINE_AXIS));
        JLabel lbl = new JLabel(label);
        add(lbl);
        lbl.setFont(lbl.getFont().deriveFont(Font.BOLD));
        lblValue = new JLabel(Language.getString("NA"), JLabel.TRAILING);
        add(Box.createHorizontalGlue());
        add(lblValue);
        lblValue.setFont(lblValue.getFont().deriveFont(Font.BOLD));
    }

    public void setValue(long value, long splits) {
        if (Language.isLTR())
            lblValue.setText(format.format(value) + " (" + format.format(splits) + ")");
        else
            lblValue.setText("(" + format.format(splits) + ") " + format.format(value) );
        lblValue.repaint();
    }

    public Dimension getMinimumSize() {
        return new Dimension(0,0);
    }

}
