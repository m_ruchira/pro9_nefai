package com.isi.csvr.marketdepth;

import com.isi.csvr.datastore.DataStoreInterface;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;

import java.util.Hashtable;
import java.util.Enumeration;
import java.util.Collections;
import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 23, 2008
 * Time: 5:56:15 PM
 * To change this template use File | Settings | File Templates.
 */
public class OddLotStore implements DataStoreInterface {
    private Hashtable tree = null;
    private static OddLotStore self = null;
    private long clientNo = 0;

    private OddLotStore() {
        tree = new Hashtable();
    }

    public static OddLotStore getSharedInstance() {
        if (self == null) {
            self = new OddLotStore();
        }
        return self;
    }


    public synchronized DynamicArray getOddLotFor(String symbol) {
        DynamicArray oddLotList = (DynamicArray) tree.get(symbol);

        if (oddLotList == null) {
            oddLotList = new DynamicArray(1);
            oddLotList.setComparator(new OddLotComparator());
            tree.put(symbol, oddLotList);
        }
        return oddLotList;
    }

    public void removeBidOddLotObject(String key, int depth){
        try {
            DynamicArray oddLotList = (DynamicArray) tree.get(key);
            int index = -1;
            for (int i = 0; i < oddLotList.size(); i++) {
                OddLot oddlot= (OddLot)oddLotList.get(i);
                if((oddlot.getDepth_sequance()==depth) && (oddlot.getPriceType() == OddLot.BID)){
                   index = i;
                }
            }
            oddLotList.remove(index);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void removeAskOddLotObject(String key, int depth){
        try {
            DynamicArray oddLotList = (DynamicArray) tree.get(key);
            int index = -1;
            for (int i = 0; i < oddLotList.size(); i++) {
                OddLot oddlot= (OddLot)oddLotList.get(i);
                if((oddlot.getDepth_sequance()==depth) && (oddlot.getPriceType() == OddLot.ASK)){
                   index = i;
                }
            }
            oddLotList.remove(index);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void clear(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void clear() {

        Enumeration dynamicarrays = tree.elements();
        while (dynamicarrays.hasMoreElements()) {
            DynamicArray darray = (DynamicArray) dynamicarrays.nextElement();
            darray.clear();
            darray = null;
        }
        dynamicarrays = null;
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void populateOddlotStore(String symbol, String data, int decimalFactor) {

        System.out.println("Symbol = " + symbol);

        System.out.println("Data  +77777&&&& = " + data);
        DynamicArray oddLotList = getOddLotFor(symbol);
        OddLot bidoddlot = null;
        OddLot askoddlot = null;
        String[] oddLotRecords = data.split(Meta.FD);
        char tag;
        int depth = -1;
        for (int i = 0; i < oddLotRecords.length; i++) {
            tag = oddLotRecords[i].charAt(0);

            switch (tag) {
                case '4':
                    if(bidoddlot != null){
                        if((bidoddlot.getPrice() <=0) || (bidoddlot.getQuantity() <=0)){
                            removeBidOddLotObject(symbol,depth);
                        }
                    }
                    if(askoddlot != null){
                        if((askoddlot.getPrice() <=0) || (askoddlot.getQuantity() <=0)){
                            removeAskOddLotObject(symbol,depth);
                        }
                    }
                    depth = toInt(oddLotRecords[i].substring(1));
                    bidoddlot = getBidOddLotObject(symbol,depth);
                    askoddlot = getAskOddLotObject(symbol,depth);
                    try {
                        String descrip = DataStore.getSharedInstance().getStockObject(symbol).getShortDescription();
                        bidoddlot.setDescription(descrip);
                        askoddlot.setDescription(descrip);
                    } catch (Exception e) {
                        bidoddlot.setDescription("");
                        askoddlot.setDescription("");
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    break;
                case '5':
                    askoddlot.setPrice(SharedMethods.getDouble(oddLotRecords[i].substring(1)) / decimalFactor);
                    break;
                case '6':
                    bidoddlot.setPrice(SharedMethods.getDouble(oddLotRecords[i].substring(1)) / decimalFactor);
                    break;
                case '7':
                    bidoddlot.setPrice(SharedMethods.getDouble(oddLotRecords[i].substring(1)) / decimalFactor);
                    askoddlot.setPrice(SharedMethods.getDouble(oddLotRecords[i].substring(1)) / decimalFactor);
                    break;
                case '8':
                    askoddlot.setQuantity(SharedMethods.getLong(oddLotRecords[i].substring(1)));
                    break;
                case '9':
                     bidoddlot.setQuantity(SharedMethods.getLong(oddLotRecords[i].substring(1)));
                    break;
                case ':':
                     bidoddlot.setQuantity(SharedMethods.getLong(oddLotRecords[i].substring(1)));
                     askoddlot.setQuantity(SharedMethods.getLong(oddLotRecords[i].substring(1)));
                    break;
            }

            if(bidoddlot != null && bidoddlot.getQuantity()>0 && bidoddlot.getPrice()>0 && (!listContainsObj(bidoddlot,oddLotList)) ){
                 oddLotList.insert(bidoddlot);
            }
            if(askoddlot != null && askoddlot.getQuantity()>0 && askoddlot.getPrice()>0 && (!listContainsObj(askoddlot,oddLotList)) ){
                 oddLotList.insert(askoddlot);
            }
        }
        if(bidoddlot != null){
            if((bidoddlot.getPrice() <=0) || (bidoddlot.getQuantity() <=0)){
                removeBidOddLotObject(symbol,depth);
            }
        }
        if(askoddlot != null){
            if((askoddlot.getPrice() <=0) || (askoddlot.getQuantity() <=0)){
                removeAskOddLotObject(symbol,depth);
            }
        }
        Collections.sort(oddLotList.getList(),new OddLotComparator());
    }

    private int toInt(String sValue) {
        try {
            return Integer.parseInt(sValue);
        } catch (Exception e) {
            return 0;
        }
    }

    private OddLot getBidOddLotObject(String key,int depth){
        OddLot lot =null;
        DynamicArray oddLotList = getOddLotFor(key);
        if(!oddLotList.isEmpty()){
            for (int i = 0; i < oddLotList.size(); i++) {
                OddLot oddlot= (OddLot)oddLotList.get(i);
                if((oddlot.getDepth_sequance()==depth) && (oddlot.getPriceType() == OddLot.BID)){
                    return oddlot;
                }
            }
            lot = new OddLot();
            lot.setClientSequance(clientNo);
            clientNo ++;
            lot.setPriceType(OddLot.BID);
            lot.setDepth_sequance(depth);
            oddLotList.insert(lot);
            return lot;

        }else{
            lot = new OddLot();
            lot.setDepth_sequance(depth);
            lot.setClientSequance(clientNo);
            clientNo ++;
            lot.setPriceType(OddLot.BID);
            oddLotList.insert(lot);
            return lot;
        }
    }

    private OddLot getAskOddLotObject(String key,int depth){
        OddLot lot =null;
        DynamicArray oddLotList = getOddLotFor(key);
        if(!oddLotList.isEmpty()){
            for (int i = 0; i < oddLotList.size(); i++) {
                OddLot oddlot= (OddLot)oddLotList.get(i);
                if((oddlot.getDepth_sequance()==depth) && (oddlot.getPriceType() == OddLot.ASK)){
                    return oddlot;
                }
            }
            lot = new OddLot();
            lot.setClientSequance(clientNo);
            clientNo ++;
            lot.setPriceType(OddLot.ASK);
            lot.setDepth_sequance(depth);
//            oddLotList.insert(lot);
            return lot;

        }else{
            lot = new OddLot();
            lot.setDepth_sequance(depth);
            lot.setClientSequance(clientNo);
            clientNo ++;
            lot.setPriceType(OddLot.ASK);
//            oddLotList.insert(lot);
            return lot;
        }
    }

    private boolean listContainsObj(OddLot oddlot, DynamicArray oddLotList){
         for (int i = 0; i < oddLotList.size(); i++) {
            OddLot oddlot1= (OddLot)oddLotList.get(i);
            if((oddlot.getDepth_sequance()==oddlot1.getDepth_sequance()) && ((oddlot.getPriceType()==oddlot1.getPriceType()))){
                return true;
            }
        }
        return false;
    }

}
