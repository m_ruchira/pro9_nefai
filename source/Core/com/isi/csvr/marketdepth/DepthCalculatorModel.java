// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.marketdepth;

/**
 * A Class class.
 * <P>
 * @author Bandula
 */

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.commission.CommissionSettings;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.portfolio.CommissionObject;
import com.isi.csvr.portfolio.PFStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.shared.TWTypes.TradeSides;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.TableUpdateListener;
import com.isi.csvr.trading.datastore.TPlusStore;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class DepthCalculatorModel extends CommonTable implements TableModel,
        CommonTableInterface, TableModelListener, TableUpdateListener{

    public static int MODE_PRICE = 0;
    public static int MODE_QUANTITY = 1;
    public int mode = MODE_QUANTITY;

    private String key = null;
    private String symbol = null;
    private String exchange = null;
    private long enteredQty = 0;
    private String comissionID="";
    private DepthCalculatorWindow parent;

    private int noOfOrders = 0;
    private double totCommission =0;
    private double vatAmount = 0;
    private long totQty = 0;
    private double totValue = 0;
    private double orderCommission = 0;
    private long orderQty = 0;
    private double orderValue = 0;
    private double allTotValue = 0;
    private long allTotVolume = 0;
    private double watp = 0;
    private double wap =0;
    private boolean createOrders = false;
    private boolean isCalculated = false;
    private boolean isActive = true;

    private final int roundOffValue = 1;

    private TradeSides side;
    private List<Order> orders;
    private String selectedPortfolio = "";
    //change start
    private Stock stock;

    /**
     * Constructor
     */
    public DepthCalculatorModel(TWTypes.TradeSides side, DepthCalculatorWindow parent) {
        this.side = side;
        this.parent = parent;
        orders  = Collections.synchronizedList(new LinkedList<Order>()); // new LinkedList<Order>();
    }

    public void tableChanged(TableModelEvent e) {
    }

    public TWTypes.TradeSides getSide() {
        return side;
    }

    public void setSide(TWTypes.TradeSides side) {
        this.side = side;
        initValues();
    }

    public void setSymbol(String symbolIn) {
        try {
            key = symbolIn;
            exchange = SharedMethods.getExchangeFromKey(key);
            symbol = SharedMethods.getSymbolFromKey(key);
            //change start
            stock = DataStore.getSharedInstance().getStockObject(key);
            this.updateGUI();
        } catch (Exception e) {
        }
    }

    public void setPortfolio(String portfolio){
        //change start
        if(portfolio==null){
            selectedPortfolio = "";
        }
        if((portfolio!=null)&&(!selectedPortfolio.equals(portfolio))){
            this.selectedPortfolio = portfolio;
            createOrders = true;
//            doCalculated();
        }
    }

    public void setParent(DepthCalculatorWindow parentIn) {
        parent = parentIn;
    }

    public void setQuantity(long value, int mode,String comissionID) { //String sValue) {
        try {
            enteredQty = value;
            this.mode = mode;
            this.comissionID = comissionID;
            initValues();
            this.updateGUI();
        } catch (Exception e) {
        }
    }

    public List<Order> getOrders() {
//        doCalculate(true);
        createOrders = true;
        doCalculated();
        return orders;
    }

    /* --- Table Model methods start from here --- */
    public int getColumnCount() {
        return 2; //super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        return 15;
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            if (key == null)
                return "";
            Stock stock = DataStore.getSharedInstance().getStockObject(key);
            if (stock == null)
                return "";
            if (side == TradeSides.SELL) {
                switch (iRow) {
                    case -1:
                        return Language.getString("TIP_WABP");
                    case 0:
                        return Language.getString("MARKET_VALUES");
                    case 1:
                        switch (iCol) {
                            case 0:
                                return Language.getString("BEST_BID");
                            case 1:

                                return "" + stock.getBestBidPrice();

                        }
                    case 2:
                        switch (iCol) {
                            case 0:
                                return Language.getString("WABP");
                            case 1:
                                return "" + wap;
                        }
                    case 3:
                        switch (iCol) {
                            case 0:
                                return Language.getString("TOTAL_BID_QUANTITY_SHORT");
                            case 1:
                                return "" + allTotVolume;
                        }
                    case 4:
                        switch (iCol) {
                            case 0:
                                return Language.getString("TOTAL_BID_VALUE");
                            case 1:
                                return "" + allTotValue;
                        }
                        // calculated values
                    case 5:
                        return Language.getString("CALCULATED_VALUES");
                    case 6:
                        switch (iCol) {
                            case 0:
                                return Language.getString("WATBP");
                            case 1:
                                return "" + watp;
                        }
                    case 7:
                        switch (iCol) {
                            case 0:
                                return Language.getString("NO_OF_SHARES");
                            case 1:
                                return "" + totQty;
                        }
                    case 8:
                        switch (iCol) {
                            case 0:
                                return Language.getString("VALUE");
                            case 1:
                                return "" + (totValue);
                        }
                    case 9:
                        switch (iCol) {
                            case 0:
                                return Language.getString("COMMISSION");
                            case 1:
                                return "" + totCommission;
                        }
                    case 10:
                        switch (iCol) {
                            case 0:
                                return Language.getString("TOTAL_VALUE");
                            case 1:
                                return "" + (totValue - totCommission);
                        }
                        // current market values
                    case 11:
                        return Language.getString("CURRENT_MARKET");
                    case 12:
                        switch (iCol) {
                            case 0:
                                return Language.getString("LAST");
                            case 1:
                                return "" + stock.getLastTradeValue();
                        }
                    case 13:
                        switch (iCol) {
                            case 0:
                                return Language.getString("PCT_CHANGE");
                            case 1:
                                return "" + stock.getPercentChange();
                        }
                }
            } else {
                switch (iRow) {
                    case -1:
                        return Language.getString("TIP_WAAP");
                    case 0:
                        return Language.getString("MARKET_VALUES");
                    case 1:
                        switch (iCol) {
                            case 0:
                                return Language.getString("BEST_ASK");
                            case 1:

                                    return "" + stock.getBestAskPrice();

                        }
                    case 2:
                        switch (iCol) {
                            case 0:
                                return Language.getString("WAAP");
                            case 1:
                                return "" + wap;
                        }
                    case 3:
                        switch (iCol) {
                            case 0:
                                return Language.getString("TOTAL_ASK_QUANTITY_SHORT");
                            case 1:
                                return "" + allTotVolume;
                        }
                    case 4:
                        switch (iCol) {
                            case 0:
                                return Language.getString("TOTAL_OFFER_VALUE");
                            case 1:
                                return "" + allTotValue;
                        }
                        // calculated valus
                    case 5:
                        return Language.getString("CALCULATED_VALUES");
                    case 6:
                        switch (iCol) {
                            case 0:
                                return Language.getString("WATAP");
                            case 1:
                                return "" + watp;
                        }
                    case 7:
                        switch (iCol) {
                            case 0:
                                return Language.getString("NO_OF_SHARES");
                            case 1:
                                return "" + totQty;
                        }
                    case 8:
                        switch (iCol) {
                            case 0:
                                return Language.getString("VALUE");
                            case 1:
                                return "" + totValue;
                        }
                    case 9:
                        switch (iCol) {
                            case 0:
                                return Language.getString("COMMISSION");
                            case 1:
                                return "" + totCommission;
                        }
                    case 10:
                        switch (iCol) {
                            case 0:
                                return Language.getString("VAT");
                            case 1:
                                return "" + getVat();
                        }
                    case 11:
                        return Language.getString("CURRENT_MARKET");
                        // current market values
                    case 12:
                        switch (iCol) {
                            case 0:
                                return Language.getString("TOTAL_VALUE");
                            case 1:
                                return "" + (totValue + totCommission + getVat());
                        }
                    case 13:
                        switch (iCol) {
                            case 0:
                                return Language.getString("LAST");
                            case 1:
                                return "" + stock.getLastTradeValue();
                        }
                    case 14:
                        switch (iCol) {
                            case 0:
                                return Language.getString("PCT_CHANGE");
                            case 1:
                                return "" + stock.getPercentChange();
                        }
                }
            }
            return "";
        } catch (Exception e) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        //if ((row ==  0) && ((col == 0) || (col == 1) || (col == 2))) {
        //    return true;
        //}
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public void tableUpdating() {
//        doCalculate(false);
//        createOrders = false;
//        doCalculated();
    }

    public void doCalculated() {
        DynamicArray list = null;
        BidAsk bidAsk = null;
        long currAmount =0;
        int i = 0;
        boolean isFirstTime = true;
//        const round =

        int       localNoOfOrders      = 0;
        double    localTotCommission   = 0;
        long      localTotQty          = 0;
        double    localTotValue        = 0;
        double    localOrderCommission = 0;
        long      localOrderQty        = 0;
        double    localOrderValue      = 0;
        double    localAllTotValue     = 0;
        long      localAllTotVolume    = 0;
        double    localWatp            = 0;
        double    localWap             = 0;
        isCalculated = false;
        isActive = true;
//        System.out.println("isActivec = "+ isActive);

//        double maxOrderVal = 0;
        long maxOrderQty   = 0;
//        double maxOrderComm= 0;
//        double minOrderVal = 0;
        long minOrderQty   = 0;
//        double minOrderComm= 0;
        double midOrderVal = 0;
        long midOrderQty   = 0;
        double midOrderComm= 0;
        boolean isWhileLoop = true;
        synchronized (orders) {
            if (createOrders)
                orders.clear();
            //change start
//        stock = DataStore.getSharedInstance().getStockObject(key);
            if(side == TradeSides.SELL){
                try {
                    list = DepthStore.getInstance().getDepthFor(key).getPriceList(DepthObject.BID);
                    currAmount = enteredQty;
                    for (i = 0; i < list.size(); i++) {
                        if(!isActive){
                            return;
                        }
                        bidAsk = (BidAsk) list.get(i);
                        localAllTotValue += bidAsk.getPrice() * bidAsk.getQuantity(); // (Math.round(((double) bidAsk.getPrice() * bidAsk.getQuantity())*100000)/100000);
                        localAllTotVolume += bidAsk.getQuantity();
                        if((isFirstTime)&&(currAmount >0)){
                            localNoOfOrders ++;
                            localOrderQty = bidAsk.getQuantity();
                            localOrderValue = bidAsk.getPrice()* bidAsk.getQuantity(); //(Math.round(((double) bidAsk.getPrice()* bidAsk.getQuantity())*100000)/100000);
                            localOrderCommission = getCommission(bidAsk.getPrice(), localOrderQty); // SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio); //(Math.round((SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio))*100000)/100000);
                            if(mode==MODE_PRICE) {
                                if((Math.round((localTotValue + localOrderValue)*1000)/1000)< (Math.round((currAmount + localOrderCommission + localTotCommission)*1000)/1000)){
                                    localTotValue += localOrderValue;
                                    localTotCommission += localOrderCommission;
                                    localTotQty += localOrderQty;
                                    if (createOrders) {
                                        orders.add(new Order(bidAsk.getPrice(), bidAsk.getQuantity()));
                                    }
                                }else{
                                    maxOrderQty = localOrderQty;
                                    long qty = (long)Math.ceil((currAmount - localTotCommission-localTotValue)/bidAsk.getPrice());
                                    minOrderQty = qty;
                                    while(isWhileLoop){
                                        if(!isActive){
                                            return;
                                        }
                                        if((maxOrderQty - minOrderQty) <= 25){
                                            for(long k = minOrderQty; k <= maxOrderQty; k++){
                                                localOrderQty = k;
                                                localOrderValue =  bidAsk.getPrice()*(localOrderQty); // (Math.round(((double) bidAsk.getPrice()*(localOrderQty))*100000)/100000);
                                                localOrderCommission = getCommission(bidAsk.getPrice(), localOrderQty); // SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio); // (Math.round((SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio))*100000)/100000);
                                                if((Math.round((localTotValue + localOrderValue)*1000)/1000) > (Math.round((currAmount + localOrderCommission + localTotCommission)*1000)/1000)){
                                                    localTotQty += localOrderQty;
                                                    localTotValue += localOrderValue;
                                                    localTotCommission += localOrderCommission; // SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty , stock.getMarketID(),selectedPortfolio);
                                                    if (createOrders) {
                                                        orders.add(new Order(bidAsk.getPrice(), localOrderQty));
                                                    }
                                                    isWhileLoop = false;
                                                    break;
                                                }
                                            }
                                        }else{
                                            midOrderQty = (long)Math.ceil((minOrderQty +(maxOrderQty - minOrderQty)/2));
                                            midOrderVal = bidAsk.getPrice()*midOrderQty; //(Math.round(((double) bidAsk.getPrice()*midOrderQty)*100000)/100000);
                                            midOrderComm = getCommission(bidAsk.getPrice(), midOrderQty); // SharedMethods.getCommission(exchange, bidAsk.getPrice(), midOrderQty, stock.getMarketID(),selectedPortfolio); // (Math.round((SharedMethods.getCommission(exchange, bidAsk.getPrice(), midOrderQty, stock.getMarketID(),selectedPortfolio))*100000)/100000);
                                            if((Math.round((localTotValue + midOrderVal)*1000)/1000)< (Math.round((currAmount + midOrderComm + localTotCommission)*1000)/1000)){
            //                                            minOrderComm = midOrderComm;
                                                minOrderQty = midOrderQty;
            //                                            minOrderVal  = midOrderVal;
                                            }else{
                                                maxOrderQty = midOrderQty;
            //                                            maxOrderComm = midOrderComm;
            //                                            maxOrderVal = midOrderVal;
                                            }
                                        }
                                        try {
                                            Thread.sleep(1);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                            if(!isActive){
                                                return;
                                            }
                                        }
                                    }
            //                                long qty = (long)Math.ceil((currAmount-localTotCommission -localTotValue)/bidAsk.getPrice());
            //                                for(long j= qty; j<=bidAsk.getQuantity(); j++){
            //                                    if(!isActive){
            //                                        return;
            //                                    }
            //                                    localOrderQty = j;
            //                                    localOrderValue = (double) bidAsk.getPrice()*(localOrderQty);
            //                                    localOrderCommission = SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio);
            //                                    if((localTotValue + localOrderValue) > (currAmount + localTotCommission + localOrderCommission)){
            //                                        localTotQty += localOrderQty;
            //                                        localTotValue += localOrderValue;
            //                                        localTotCommission += localOrderCommission; //SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty , stock.getMarketID(),selectedPortfolio);
            //                                        if (createOrders) {
            //                                            orders.add(new Order(bidAsk.getPrice(), localOrderQty));
            //                                        }
            //                                        break;
            //                                    }
            //                                    if(((j-qty)%500)==0){
            //                                        try {
            //                                            Thread.sleep(2);
            //                                        } catch (InterruptedException e) {
            //                                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            //                                        }
            //                                    }
            //                                }
                                    isFirstTime = false;
                                }
                            }else{
                                if((localTotQty + localOrderQty) < currAmount){
                                    localTotQty += localOrderQty;
                                    localTotValue += localOrderValue;
                                    localTotCommission +=localOrderCommission;
                                    if (createOrders) {
                                        orders.add(new Order(bidAsk.getPrice(), bidAsk.getQuantity()));
                                    }
                                }else{
                                    localOrderQty = currAmount - localTotQty;
                                    localOrderValue = bidAsk.getPrice()*(localOrderQty); // (Math.round(((double) bidAsk.getPrice()*(localOrderQty))*100000)/100000);
                                    localTotQty += localOrderQty;
                                    localTotValue += localOrderValue;
                                    localTotCommission += getCommission(bidAsk.getPrice(), localOrderQty); // SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio); // (Math.round((SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio))*100000)/100000);
                                    if (createOrders) {
                                        orders.add(new Order(bidAsk.getPrice(), localOrderQty));
                                    }
                                    isFirstTime = false;
                                }
                            }
                        }
                    bidAsk = null;
                    }
                    if (localAllTotVolume != 0) {
                        localWap = localAllTotValue / localAllTotVolume;
                    } else {
                        localWap = Double.NaN;
                    }
                    if(localTotQty != 0){
                        localWatp = localTotValue / localTotQty;
            //                    localTotValue = localTotValue - localTotCommission;
                    } else{
                        localWatp = Double.NaN;
                    }
                } catch( Exception e){
                    e.printStackTrace();
                }
            }else {
                try {
                    list = DepthStore.getInstance().getDepthFor(key).getPriceList(DepthObject.ASK);
                    currAmount = enteredQty;
                    for (i = 0; i < list.size(); i++) {
                        if(!isActive){
                            return;
                        }
                        bidAsk = (BidAsk) list.get(i);
                        localAllTotValue += bidAsk.getPrice() * bidAsk.getQuantity(); // (Math.round(((double) bidAsk.getPrice() * bidAsk.getQuantity())*100000)/100000);
                        localAllTotVolume += bidAsk.getQuantity();
                        if((isFirstTime)&&(currAmount >0)){
                            localNoOfOrders ++;
                            localOrderQty = bidAsk.getQuantity();
                            localOrderValue = bidAsk.getPrice()* bidAsk.getQuantity(); // (Math.round(((double) bidAsk.getPrice()* bidAsk.getQuantity())*100000)/100000);
                            localOrderCommission = getCommission(bidAsk.getPrice(), localOrderQty); // SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio); // (Math.round((SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio))*100000)/100000);
                            if(mode==MODE_PRICE) {
                                if((Math.round((localTotValue + localTotCommission + localOrderValue + localOrderCommission)*1000)/1000)<= currAmount){
                                    localTotValue += localOrderValue;
                                    localTotCommission += localOrderCommission;
                                    localTotQty += localOrderQty;
                                    if (createOrders) {
                                        orders.add(new Order(bidAsk.getPrice(), bidAsk.getQuantity()));
                                    }
                                }else{
                                    minOrderQty = 1;
                                    if(bidAsk.getPrice()<=0){
                                        continue;
                                    }
                                    long qty = (long)Math.ceil((currAmount-localTotCommission-localTotValue)/bidAsk.getPrice());
                                    maxOrderQty =  qty + 1;
                                    while(isWhileLoop){
                                        if(!isActive){
                                            return;
                                        }
                                        if((maxOrderQty - minOrderQty) <= 25){
                                            for(long k = maxOrderQty; k >= minOrderQty; k--){
            //                                            System.out.println("max Qty = "+ maxOrderQty + " ,  min Qty = "+ minOrderQty);
                                                localOrderQty = k;
                                                localOrderValue = bidAsk.getPrice()*(localOrderQty); // (Math.round(((double) bidAsk.getPrice()*(localOrderQty))*100000)/100000);
                                                localOrderCommission = getCommission(bidAsk.getPrice(), localOrderQty); // SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio); // (Math.round((SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio))*100000)/100000);
                                                if(((Math.round((localTotValue + localTotCommission + localOrderValue + localOrderCommission)*1000)))<= (currAmount*1000)){
                                                    localTotQty += localOrderQty;
                                                    localTotValue += localOrderValue;
                                                    localTotCommission += localOrderCommission; //SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty , stock.getMarketID(),selectedPortfolio);
                                                    if (createOrders) {
                                                        orders.add(new Order(bidAsk.getPrice(), localOrderQty));
                                                    }
                                                    isWhileLoop = false;
                                                    break;
                                                }else if(k==1 && (((Math.round((localTotValue + localTotCommission + localOrderValue + localOrderCommission)*1000)))> (currAmount*1000)) ){
                                                  isWhileLoop = false;
                                                    break;  
                                                }

                                            }
                                        }else{
                                            midOrderQty = (long)Math.ceil((minOrderQty +(maxOrderQty - minOrderQty )/2));
                                            midOrderVal =  bidAsk.getPrice()*midOrderQty; // (Math.round(((double) bidAsk.getPrice()*midOrderQty)*100000)/100000);
                                            midOrderComm = getCommission(bidAsk.getPrice(), midOrderQty); // SharedMethods.getCommission(exchange, bidAsk.getPrice(), midOrderQty, stock.getMarketID(),selectedPortfolio); //(Math.round((SharedMethods.getCommission(exchange, bidAsk.getPrice(), midOrderQty, stock.getMarketID(),selectedPortfolio))*100000)/100000);
                                            if((Math.round((localTotValue + localTotCommission + midOrderVal + midOrderComm)*1000)/1000)<= currAmount){
            //                                            minOrderComm = midOrderComm;
                                                minOrderQty = midOrderQty;
            //                                            minOrderVal  = midOrderVal;
                                            }else{
                                                maxOrderQty = midOrderQty;
            //                                            maxOrderComm = midOrderComm;
            //                                            maxOrderVal = midOrderVal;
                                            }
                                        }
                                        try {
                                            Thread.sleep(1);
                                        } catch (InterruptedException e) {
                                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                            if(!isActive){
                                                return;
                                            }
                                        }
                                    }
            //                                long qty = (long)Math.ceil((currAmount-localTotCommission -localTotValue)/bidAsk.getPrice());
            //                                for(long j = qty+1; j>=0; j--){
            //                                    if(!isActive){
            //                                        return;
            //                                    }
            //                                    localOrderQty = j;
            //                                    localOrderValue = (double) bidAsk.getPrice()*(localOrderQty);
            //                                    localOrderCommission = SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio);
            //                                    if((localTotValue + localTotCommission + localOrderValue + localOrderCommission) <= currAmount){
            //                                        localTotQty += localOrderQty;
            //                                        localTotValue += localOrderValue;
            //                                        localTotCommission += localOrderCommission ; //SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty , stock.getMarketID(),selectedPortfolio);
            //                                        if (createOrders) {
            //                                            orders.add(new Order(bidAsk.getPrice(), localOrderQty));
            //                                        }
            //                                        break;
            //                                    }
            //                                    if(((qty+1-j)%500)==0){
            //                                        try {
            //                                            Thread.sleep(2);
            //                                        } catch (InterruptedException e) {
            //                                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            //                                        }
            //                                    }
            //                                }
                                    isFirstTime = false;
                                }
                            } else{
                                if((localTotQty + localOrderQty) < currAmount){
                                    localTotQty += localOrderQty;
                                    localTotValue += localOrderValue;
                                    localTotCommission +=localOrderCommission;
                                    if (createOrders) {
                                        orders.add(new Order(bidAsk.getPrice(), bidAsk.getQuantity()));
                                    }
                                }else{
                                    localOrderQty = currAmount - localTotQty;
                                    localOrderValue = bidAsk.getPrice()*(localOrderQty); // (Math.round(((double) bidAsk.getPrice()*(localOrderQty))*100000)/100000);
                                    localTotQty += localOrderQty;
                                    localTotValue += localOrderValue;
                                    localTotCommission += getCommission(bidAsk.getPrice(), localOrderQty); // SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio); // (Math.round((SharedMethods.getCommission(exchange, bidAsk.getPrice(), localOrderQty, stock.getMarketID(),selectedPortfolio))*100000)/100000);
                                    if (createOrders) {
                                        orders.add(new Order(bidAsk.getPrice(), localOrderQty));
                                    }
                                    isFirstTime = false;
                                }
                            }
                        }
                        bidAsk = null;
                    }
                    if (localAllTotVolume != 0) {
                        localWap = localAllTotValue / localAllTotVolume;
                    } else {
                        localWap = Double.NaN;
                    }
                    if(localTotQty != 0){
                       localWatp = localTotValue / localTotQty;
                    } else{
                        localWatp = Double.NaN;
                    }

                }catch( Exception e){
                    e.printStackTrace();
                }
            }
        }
        this.noOfOrders         = localNoOfOrders;
        this.totCommission      = localTotCommission;
        this.totQty             = localTotQty;
        this.totValue           = localTotValue;
        this.orderCommission    = localOrderCommission;
        this.orderQty           = localOrderQty;
        this.orderValue         = localOrderValue;
        this.allTotValue        = localAllTotValue;
        this.allTotVolume       = localAllTotVolume;
        this.watp               = localWatp;
        this.wap               =  localWap;
        isCalculated = true;
    }

    private double calculateComission(double totval, double comission){
        if(!comissionID.isEmpty()){
            CommissionObject obj = PFStore.getInstance().getCommission(Long.parseLong(comissionID));
            if(obj != null){
                double calcval = (totval *obj.getPercentage())/100;
                if(calcval > obj.getMinPrice()){
                    return calcval;
                }else{
                    return obj.getMinPrice();
                }

            }

        }
        return comission;
    }

    public boolean isCalculated(){
        return isCalculated;
    }

    public void setActive(boolean isActive){
        this.isActive = isActive;
    }

    public void initValues(){
        noOfOrders      = 0;
        totCommission   = 0;
        totQty          = 0;
        totValue        = 0;
        orderCommission = 0;
        orderQty        = 0;
        orderValue      = 0;
        allTotValue     = 0;
        allTotVolume    = 0;
        watp            = 0;
        wap             = 0;
    }

    public long getTotalShares(){
        return totQty;
    }

    //change start
    public double calculateCommissionWithoutTrading(double price, long qty) {
        double commission = 0f;
        commission = (price * qty) * CommissionSettings.getSharedInstance().getPctCommission();
        commission = commission / 100;
        commission = Math.max(commission, CommissionSettings.getSharedInstance().getMinCommission());
        return commission;
    }

    public double getCommission(double price, long qty){
        if(!comissionID.isEmpty() && !comissionID.equals(Language.getString("BROKER"))){
            CommissionObject obj = PFStore.getInstance().getCommission(Long.parseLong(comissionID));
            if(obj != null){
                double calcval = (price*qty *obj.getPercentage())/100;
                if(calcval > obj.getMinPrice()){
                    return calcval;
                }else{
                    return obj.getMinPrice();
                }

            }else{
                return 0;
            }

        }else{

        if(!selectedPortfolio.equals("")){
            if(TradingShared.isExchangeAllowedToTrading(exchange, selectedPortfolio)){
                return SharedMethods.getCommission(exchange, price, qty, stock.getMarketID(),selectedPortfolio,key, TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(),"0"), CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));

            }else{
                return calculateCommissionWithoutTrading(price, qty);
            }
        }else{
            return calculateCommissionWithoutTrading(price, qty);
        }
    }
    }

    public double getVat() {
        if(!selectedPortfolio.equals("")){
            if(TradingShared.isExchangeAllowedToTrading(exchange, selectedPortfolio)){
                return SharedMethods.vatAmount(totCommission);

            }else{
                return totCommission * 0.05;
            }
        }else{
            return totCommission * 0.05;
        }
    }
    //change end
}

