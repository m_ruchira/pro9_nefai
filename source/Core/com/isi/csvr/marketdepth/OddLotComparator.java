package com.isi.csvr.marketdepth;

import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 24, 2008
 * Time: 9:36:18 AM
 * To change this template use File | Settings | File Templates.
 */
public class OddLotComparator implements Comparator{
    public int compare(Object o1, Object o2) {

        OddLot od1 = (OddLot)o1;
        OddLot od2 = (OddLot)o2;

        if(od1.getQuantity() >od2.getQuantity()){
            return 1;
        }else if(od1.getQuantity() == od2.getQuantity()){

            if(od1.getPrice()>od2.getPrice()){
                return -1;
            }else{                   // todo if prices become equal
                return 1;
            }


        }else{
            return -1;
        }



    }
}
