package com.isi.csvr.marketdepth;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.dde.DDELinkInterface;
import com.isi.csvr.shared.BidAsk;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;

public class DepthPriceModel extends CommonTable
        implements TableModel, CommonTableInterface, ClipboardOwner, DDELinkInterface {

    private String g_sSymbol;
    private byte type;
    private TotalQuantityIndicator indicator;
    private Clipboard clip;

    /**
     * Constructor
     */
    public DepthPriceModel(TotalQuantityIndicator indicator, String sSymbol, byte type) {
        g_sSymbol = sSymbol;
        this.type = type;
        this.indicator = indicator;
        clip = Toolkit.getDefaultToolkit().getSystemClipboard();
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        try {
            DepthObject depth = DepthStore.getInstance().getDepthFor(g_sSymbol);
            int rowCount = depth.getPriceListSize(type);
            if (rowCount == 0) {
                updateIndicator();
            }
            return rowCount;
        } catch (Exception e) {
            return 0;
        }
    }

    public void setSymbol(String symbol) {
        g_sSymbol = symbol;
    }

    public Object getValueAt(int iRow, int iCol) {

        try {
            DepthObject depth = DepthStore.getInstance().getDepthFor(g_sSymbol);
            BidAsk bidAsk = (BidAsk) depth.getPriceList(type).get(iRow);

            switch (iCol) {
                case 0:
                    return "" + (bidAsk.getDepthSequance() + 1);// (iRow + 1);
                case 1:
                    return "" + bidAsk.getPrice();
                case 2:
                    updateIndicator(); // set the total qty
                    return "" + bidAsk.getQuantity();
                case 3:
                    return "" + bidAsk.getSplits();
                default:
                    return "0";
            }
        } catch (Exception e) {
            return "0";
        }
    }

    private void updateIndicator() {
        if (indicator != null) {
            Stock stock = DataStore.getSharedInstance().getStockObject(g_sSymbol);
            if (stock != null) {
                if (type == DepthObject.ASK)
                    indicator.setValue(stock.getTotalAskQty(), stock.getNoOfAsks());
                else
                    indicator.setValue(stock.getTotalBidQty(), stock.getNoOfBids());
            }
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
            case 1:
            case 2:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] customizerRecords = new CustomizerRecord[4];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, null, Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));    //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")
        if (type == DepthObject.BID) {
            customizerRecords[1] = new CustomizerRecord(Language.getString("BID_ROW_1"), FIELD_DEP_BY_ORD_BID_ROW1, Theme.getColor("DEPTH_BY_ORDER_BID_BGCOLOR1"), Theme.getColor("DEPTH_BY_ORDER_BID_FGCOLOR1"));
            customizerRecords[2] = new CustomizerRecord(Language.getString("BID_ROW_2"), FIELD_DEP_BY_ORD_BID_ROW2, Theme.getColor("DEPTH_BY_ORDER_BID_BGCOLOR2"), Theme.getColor("DEPTH_BY_ORDER_BID_FGCOLOR2"));
        } else {
            customizerRecords[1] = new CustomizerRecord(Language.getString("OFFER_ROW_1"), FIELD_DEP_BY_ORD_ASK_ROW1, Theme.getColor("DEPTH_BY_ORDER_ASK_BGCOLOR1"), Theme.getColor("DEPTH_BY_ORDER_ASK_FGCOLOR1"));
            customizerRecords[2] = new CustomizerRecord(Language.getString("OFFER_ROW_2"), FIELD_DEP_BY_ORD_ASK_ROW2, Theme.getColor("DEPTH_BY_ORDER_ASK_BGCOLOR2"), Theme.getColor("DEPTH_BY_ORDER_ASK_FGCOLOR2"));
        }
        customizerRecords[3] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_DEP_BY_ORD_SELECTED_ROW, Theme.getColor("DEPTH_BY_ORDER_SELECTED_BGCOLOR"), Theme.getColor("DEPTH_BY_ORDER_SELECTED_FGCOLOR"));
        return customizerRecords;
    }

    public void getDDEString(Table table, boolean withHeadings) {

        int col;
        int cols;
        int row = table.getTable().getSelectedRow();
        int rows = table.getTable().getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        if (withHeadings)
            buffer.append(copyHeaders(table));
        for (int r = row; r < (rows + row); r++) {
            for (int c = 0; c < cols; c++) {
                int modelIndex = table.getTable().convertColumnIndexToModel(col + c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    buffer.append("=MRegionalDdeServer|'");
                    if (type == DepthObject.ASK)
                        buffer.append("PA");
                    else
                        buffer.append("PB");
                    buffer.append(g_sSymbol);
                    buffer.append("'!'");
                    buffer.append(modelIndex + (r * 100));
                    buffer.append("'");
                    buffer.append("*1");

                    if (c != (cols - 1))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
    }

    public String copyHeaders(Table table) {
        int col;
        int cols;
        //int row = this.getSelectedRow();
        //int rows= this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        for (int c = 0; c < cols; c++) {
            int modelIndex = table.getTable().convertColumnIndexToModel(col + c);
            if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                buffer.append((String) table.getTable().getColumn("" + modelIndex).getHeaderValue());
                if (c != (cols - 1))  // do not append the tab char to the last item of the row
                    buffer.append("\t");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {

    }

    public long getTradeQty(int index) {
        System.out.println("selected index : " + index);
        long qty = 0;
        for (int i = index; i >= 0; i--) {
            try {
                DepthObject depth = DepthStore.getInstance().getDepthFor(g_sSymbol);
                BidAsk bidAsk = (BidAsk) depth.getPriceList(type).get(i);
                qty = qty + bidAsk.getQuantity();
            } catch (Exception e) {
                qty = 0;
            }
        }
        return qty;
    }

     public double getTradePrice(int index) {
        System.out.println("selected index : " + index);
        double price = 0;
            try {
                DepthObject depth = DepthStore.getInstance().getDepthFor(g_sSymbol);
                BidAsk bidAsk = (BidAsk) depth.getPriceList(type).get(index);
                price = bidAsk.getPrice();
            } catch (Exception e) {
                price = 0;
            }
        return price;
    }

      public long getSelectedTradeQty(int index) {
        System.out.println("selected index : " + index);
        long qty = 0;

            try {
                DepthObject depth = DepthStore.getInstance().getDepthFor(g_sSymbol);
                BidAsk bidAsk = (BidAsk) depth.getPriceList(type).get(index);
                qty = qty + bidAsk.getQuantity();
            } catch (Exception e) {
                qty = 0;

        }
        return qty;
    }
}