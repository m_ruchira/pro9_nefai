package com.isi.csvr.topstocks;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Oct 22, 2003
 * Time: 10:23:44 AM
 * To change this template use Options | File Templates.
 */
public class TopStocksTypeComboItem {
    private int type;
    private String caption;
//    private boolean gainers;

    public TopStocksTypeComboItem(String caption, int type) {
        this.caption = caption;
        this.type = type;
//        this.gainers = gainers;
    }

    public String getCaption() {
        return caption;
    }

    public int getType() {
        return type;
    }

    public String toString() {
        return caption;
    }

//    public boolean isGainers() {
//        return gainers;
//    }

    public boolean equals(Object obj) {
        try {
            TopStocksTypeComboItem item = (TopStocksTypeComboItem) obj;
            return (type == item.getType());
        } catch (Exception e) {
            return false;
        }
    }

}
