package com.isi.csvr.portfolio;

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.TransactionDialogInterface;
import com.isi.csvr.Client;
import com.isi.csvr.tabbedpane.TWTabEvent;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.calendar.DatePicker;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.commission.CommissionSettings;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexNullLayout;

import javax.swing.*;
import javax.swing.SwingUtilities;
import javax.swing.JComboBox;
import javax.swing.JOptionPane;
import javax.swing.JTextField;
import javax.swing.JPanel;
import javax.swing.BorderFactory;
import javax.swing.JLabel;
import javax.swing.JComponent;
import javax.swing.table.TableColumn;
import java.awt.*;
import java.awt.event.*;
import java.util.*;


/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class TransactionDialog extends InternalFrame implements ActionListener,
        DateSelectedListener, FocusListener, Runnable, ItemListener,
        MouseListener, KeyListener, ExchangeListener, TransactionDialogInterface {

    private JPanel basePanel = new JPanel();
    private JPanel centerPanel = new JPanel();
    private JPanel transactionPanel = new JPanel();
    private JPanel quoteSummaryPanel = new JPanel();
    private JPanel buttonPanel = new JPanel();
    private JPanel datePanel = new JPanel();
    private JPanel memoPanel = new JPanel();
    private JPanel advSellPanel = new JPanel();

    private MemoField memoField; //       = new MemoField();
    //    private JComboBox cmbExchanges;
    private TWTextField txtExchanges;
//    private TWComboModel exchangeModel;
    //    private ArrayList<TWComboItem> exchanges = new ArrayList<TWComboItem>();
    private TWTextField txtSymbol = new TWTextField();
    private JComboBox cmbPortfolios = new JComboBox();
    private JComboBox cmbTxType = new JComboBox();
    private JComboBox CommissionCombo = new JComboBox();
    private ArrayList<String> CommissionItems;
    //    private JTextField  txtDate         = new JTextField();
    private TWTextField txtQty = new TWTextField();
    private TWTextField txtPrice = new TWTextField();
    private TWTextField txtCommission = new TWTextField();
    //    private JTextField  txtMemo         = new JTextField();
    private JLabel lblTotal = new JLabel();
    private JLabel lblTotalDesc = new JLabel();
    private JLabel lblExchanges = new JLabel();
    private JLabel lblSymbol = new JLabel();
    private JLabel lblPortfolios = new JLabel();
    private JLabel lblTxnType = new JLabel();
    private JLabel lblDate = new JLabel();
    private JLabel lblQty = new JLabel();
    private JLabel lblSharePrice = new JLabel();
    private JLabel lblCommissionCombo = new JLabel();
    private JLabel lblCommission = new JLabel();
    private JLabel lblFromDate = new JLabel();
    private JLabel lblMemo = new JLabel();
    private JLabel lblAvailCashBal = new JLabel();
    private JLabel lblAvailCashBalDesc = new JLabel();
    private JLabel lblMemoVoid;
//    private JLabel	lblMemoDesc     = new JLabel();

    private JLabel lblLastTradeDesc = new JLabel();
    private JLabel lblChangeDesc = new JLabel();
    private JLabel lblBidDesc = new JLabel();
    private JLabel lblOfferDesc = new JLabel();
    private JLabel lblVolumeDesc = new JLabel();
    private JLabel lblCurrDesc = new JLabel();
    private JLabel lblLastTrade = new JLabel();
    private JLabel lblChange = new JLabel();
    private JLabel lblBid = new JLabel();
    private JLabel lblOffer = new JLabel();
    private JLabel lblVolume = new JLabel();
    private JLabel lblCurrency = new JLabel();
    private JLabel lblCompName = new JLabel();
    private JLabel lblCompNameDesc = new JLabel();

    private TWButton btnSearch = new TWButton();
    private TWButton btnAdvTraction = new TWButton();
    private TWButton btnCal = new TWButton();
    private TWButton btnGetPrice = new TWButton();
    private TWButton btnOK = new TWButton();
    private TWButton btnCancel = new TWButton();
    private TWButton btnAddAnother = new TWButton();
    private TWButton btnFromDate = new TWButton();
    private TWButton btnMemo = new TWButton();

    private DatePicker datePicker;

    private int buttonPanelHeight = 30;
    private int quotePanelHeight = 80;
    private int advBuyPanelHeight = 100;
    private int width;
    private int height;
    private int selectedPFID;
    private String sKey;
    private String prevEnteredText;
    private String lastEnteredSymbol;
    private boolean continueLoop = true;
    private boolean isEditMode;
    private boolean isTextChanged;
    private Thread selfThread;
    private Color upColor;
    private Color downColor;
    private PortfolioWindow parent;
    private TransactRecord record;
    private boolean showAdvSell = false;
    private boolean isFIFO = false;
    ArrayList symbolSpecificBuys;

    private Table advanceSellTable;
    private AdvaneSellTableModel advSellModel;


    private ArrayList<String> requestRegister = new ArrayList<String>();

    private int oldQty;
    private double oldPrice;
    private double oldCommission;

    private boolean isInternallyValidatedSymbol;
    private TWDecimalFormat priceFormatter;
    private int symbolDecimals = 2;

    public TransactionDialog(Frame frame, String title) {

        try {
            setTitle(title);
            selectedPFID = 1;           // Temporarily
            lastEnteredSymbol = "";
            priceFormatter = new TWDecimalFormat("###0.00");
            jbInit();
            lblCompName.setHorizontalAlignment(JLabel.LEADING);
            lblCompName.setFont(Theme.getDefaultFont(Font.BOLD, 12));
            addListeners();
            loadThemeColors();
            loadPortflioList();
            loadCommisionList();
            GUISettings.applyOrientation(this);
            pack();
            lblFromDate.setText(PFUtilities.getFormattedDate(new Date()));
            txtCommission.setHorizontalAlignment(JTextField.RIGHT);
            txtPrice.setHorizontalAlignment(JTextField.RIGHT);
            txtQty.setHorizontalAlignment(JTextField.RIGHT);
            ExchangeStore.getSharedInstance().addExchangeListener(this);
            btnGetPrice.setEnabled(false);
            this.addInternalFrameListener(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public TransactionDialog() {
        this(null, "");
    }


    public void setRecord(TransactRecord record) {
        try {
            this.record = record;
            txtSymbol.setEnabled(false);
            btnSearch.setEnabled(false);
            lblSymbol.setEnabled(false);
            lblCompName.setEnabled(false);
            lblExchanges.setEnabled(false);
            cmbPortfolios.setEnabled(false);
            btnFromDate.setEnabled(false);

            cmbTxType.setEnabled(false);
            //txtSymbol.setText(SharedMethods.getSymbolFromKey(record.getSKey()));
            this.setSymbol(record.getSKey());
            cmbPortfolios.setSelectedItem(PFStore.getInstance().getPortfolio(record.getPfID()).getName());
            txtQty.setText(record.getQuantity() + "");


            String curr = record.getCurrency();
            String price = (record.getPrice()) + "";
            if (record.getTxnType() == PFUtilities.DEPOSITE || record.getTxnType() == PFUtilities.WITHDRAWAL
                    || record.getTxnType() == PFUtilities.DIVIDEND || record.getTxnType() == PFUtilities.REFUNDS
                    || record.getTxnType() == PFUtilities.CHARGES || record.getTxnType() == PFUtilities.OP_BAL) {
                int decimal = 0;
                decimal = CurrencyStore.getSharedInstance().getDecimalPlaces(curr);

                TWDecimalFormat formater = SharedMethods.getDecimalFormatNoCommaNoSpace(decimal);
                price = formater.format(record.getPrice());
                btnGetPrice.setEnabled(false);
            }
            setPriceFormat(record.getSKey());
            //     txtPrice.setText((record.getPrice()) +"");

            txtPrice.setText(priceFormatter.format(record.getPrice()) + "");
            String str = Language.getString("SHARE_PRICE");
            if (Language.isLTR()) {
                str = str + " ( " + curr + " ) ";
            } else {
                str = " ( " + curr + " ) " + str;
            }
            lblSharePrice.setText(str);
            oldQty=0;
            oldCommission=0;
            oldPrice=0;
            calculateTotal();


            oldQty = record.getQuantity();
            oldCommission = record.getBrokerage();
            oldPrice = record.getPrice();
            if (record.getMemo() != null) {
                memoField.setText(record.getMemo() + "");
                this.setMemoDescription(record.getMemo() + "");
            }
            int txnType = record.getTxnType();
            switch (txnType) {
                case PFUtilities.BUY:
                    cmbTxType.setSelectedIndex(0);
                    break;
                case PFUtilities.SELL:
                    cmbTxType.setSelectedIndex(1);
                    break;
                case PFUtilities.OP_BAL:
                    cmbTxType.setSelectedIndex(2);
                    break;
                case PFUtilities.DIVIDEND:
                    cmbTxType.setSelectedIndex(3);
                    break;
                case PFUtilities.DEPOSITE:
                    cmbTxType.setSelectedIndex(4);
                    break;
                case PFUtilities.WITHDRAWAL:
                    cmbTxType.setSelectedIndex(5);
                    break;
                case PFUtilities.CHARGES:
                    cmbTxType.setSelectedIndex(6);
                    break;
                case PFUtilities.REFUNDS:
                    cmbTxType.setSelectedIndex(7);
                    break;
            }
            loadCommisionList();
            try {

                // CommissionCombo.setSelectedItem(record.getComissionID());
                setSelectedCommision(record.getComissionID());
            } catch (Exception e) {

                CommissionCombo.setSelectedIndex(CommissionCombo.getItemCount() - 1);
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            setComissionText(record.getBrokerage());
            // txtCommission.setText(record.getBrokerage() + "");

            Date date = new Date(record.getTxnDate());
            lblFromDate.setText(PFUtilities.getFormattedDate(date));
            date = null;
            isEditMode = true;
            this.setTitle(Language.getString("EDIT_TRANSACTION"));
            if (record.getTxnType() == PFUtilities.SELL) {
                validateAdvaceBuy();
            }
        } catch (Exception ex) {
            isEditMode = false;
        }
    }

    private void setComissionText(double comission) {
        // double comission = Double.parseDouble(val);
        txtCommission.setText("" + (float) ((int) (comission * 100)) / (float) 100);

    }

    public void setParent(PortfolioWindow parent) {
        this.parent = parent;
    }

    public void disableSymbolField() {

        txtSymbol.setEnabled(false);
        txtSymbol.setEditable(false);

        btnSearch.setEnabled(false);
        btnAddAnother.setEnabled(false);

    }

    private void jbInit() throws Exception {
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        this.setTitle(Language.getString("ADD_TXN"));
        this.setResizable(true);
        this.setClosable(true);
        basePanel.setLayout(new BorderLayout());
        centerPanel.setLayout(new FlexNullLayout());
        transactionPanel.setLayout(new FlexNullLayout());
        transactionPanel.setBorder(BorderFactory.createTitledBorder(""));  //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR").brighter(), Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR")));
        advSellPanel.setBorder(BorderFactory.createTitledBorder(""));
        advanceSellTable = new Table(false);
        advanceSellTable.addFocusListener(this);

        advSellModel = new AdvaneSellTableModel();
        createAdvanceSellTable();
        quoteSummaryPanel.setLayout(new FlexNullLayout());
//        quoteSummaryPanel.setBorder(BorderFactory.createEtchedBorder());  //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR").brighter(), Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR")));
        quoteSummaryPanel.setBorder(BorderFactory.createTitledBorder(""));  //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR").brighter(), Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR")));

        datePicker = new DatePicker(Client.getInstance().getFrame(), true);
        datePicker.getCalendar().addDateSelectedListener(this);

//        exchangeModel = new TWComboModel(exchanges);
//        cmbExchanges = new JComboBox(exchangeModel);
        txtExchanges = new TWTextField();
        txtExchanges.setEditable(false);

        buttonPanel.setLayout(new FlowLayout(FlowLayout.CENTER, 10, 4));
        btnOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (saveTransaction()) {
                    closeDialog();
                }
            }
        });
        btnAddAnother.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (saveTransaction()) {
                    initializeDataInputs();
                }
            }
        });
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (!isEditMode) {
                    unregisterSymbol();
                }
                closeDialog();
            }
        });
        buttonPanel.add(btnOK);
        buttonPanel.add(btnAddAnother);
        buttonPanel.add(btnCancel);

        memoField = new MemoField(this);
        datePanel.setLayout(new FlowLayout(FlowLayout.LEADING, 0, 0));
        datePanel.add(getDatePanel("FD", btnFromDate, lblFromDate));

        memoPanel.setLayout(new FlowLayout(FlowLayout.LEADING, 0, 0));
        //memoPanel.add(getMemoPanel("MEMO", btnMemo));
        getMemoPanel("MEMO", btnMemo);

        defineComponentBounds();
        setComponentLabels();
        addCenterPanelComponents();
        addInputMasks();

        btnSearch.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                searchSymbol();
            }
        });

        btnGetPrice.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                searchMarketPrice();
                calculateCommission();
            }
        });

        btnAdvTraction.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                showAdvSell ^= true;
//                showAdvSell = !showAdvSell;
                advBuyClicked();
            }
        });
        btnAdvTraction.setEnabled(false);

        if (PortfolioInterface.getPortfolioVersion() == PFUtilities.TADAWUL_VERSION)
            btnGetPrice.setVisible(true);
        else
            btnGetPrice.setVisible(false);
        basePanel.add(centerPanel, BorderLayout.CENTER);
        basePanel.add(buttonPanel, BorderLayout.SOUTH);
        getContentPane().add(basePanel);

        //todo - 21/12/2006
        memoField.setBackground(Theme.getColor("PORTF_SIMU_MEMO_BG_COLOR"));

//        loadExchanges();
    }

    /*private void loadExchanges() {
        exchanges.clear();
        if (ExchangeStore.getSharedInstance().count() > 0) {
            Enumeration records = ExchangeStore.getSharedInstance().getExchanges();
            while (records.hasMoreElements()) {
                Exchange exchange = (Exchange) records.nextElement();
                exchanges.add(new TWComboItem(exchange.getSymbol(), exchange.getDescription()));
                exchange = null;
            }
            records = null;
        }
        try {
            cmbExchanges.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        cmbExchanges.updateUI();
    }*/

    private void addListeners() {
        txtSymbol.addFocusListener(this);
        txtSymbol.addKeyListener(this);
        txtQty.addFocusListener(this);
        txtPrice.addFocusListener(this);
        txtCommission.addFocusListener(this);

        cmbPortfolios.addItemListener(this);
        CommissionCombo.addItemListener(this);
        cmbTxType.addItemListener(this);
    }

    private void addInputMasks() {
//        ValueFormatter
        txtQty.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));
        txtPrice.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
        txtCommission.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL));
    }


    private void defineComponentBounds() {
        try {
            int lblWidth1 = 140;
            int lblWidth2 = 110;
            int lblWidth3 = 120;
            int xPos1 = 10;
            int xPos2 = xPos1 + lblWidth1 + 5;
            int xPos3 = xPos2 + lblWidth2 + 5;
            int fieldHeight = 20;
            int curYPos = 10;
            transactionPanel.setLayout(new FlexNullLayout());
            lblCompNameDesc.setBounds(new Rectangle(xPos1 + 5, curYPos, lblWidth1, fieldHeight));
            lblCompName.setBounds(new Rectangle(xPos2 + 5, curYPos, lblWidth2 + lblWidth3, fieldHeight));
            curYPos += fieldHeight + 5;

            lblExchanges.setBounds(new Rectangle(xPos1 + 5, curYPos, lblWidth1, fieldHeight));
            txtExchanges.setBounds(new Rectangle(xPos2 + 5, curYPos, lblWidth2, fieldHeight));
            curYPos += fieldHeight + 5;

            lblSymbol.setBounds(new Rectangle(xPos1 + 5, curYPos, lblWidth1, fieldHeight));
            txtSymbol.setBounds(new Rectangle(xPos2 + 5, curYPos, lblWidth2, fieldHeight));
            btnSearch.setBounds(new Rectangle(xPos3 + 5, curYPos, lblWidth3, fieldHeight));
            curYPos += fieldHeight + 5;

            lblTxnType.setBounds(new Rectangle(xPos1 + 5, curYPos, lblWidth1, fieldHeight));
            cmbTxType.setBounds(new Rectangle(xPos2 + 5, curYPos, lblWidth2, fieldHeight));
            curYPos += fieldHeight + 5;

            lblPortfolios.setBounds(new Rectangle(xPos1 + 5, curYPos, lblWidth1, fieldHeight));
            cmbPortfolios.setBounds(new Rectangle(xPos2 + 5, curYPos, lblWidth2, fieldHeight));
            curYPos += fieldHeight + 5;

            height = curYPos;
            curYPos = 10;

            lblDate.setBounds(new Rectangle(xPos1, curYPos, lblWidth1, fieldHeight));
//        txtDate.setBounds(new Rectangle(xPos2, curYPos, lblWidth2, fieldHeight));
//        btnCal.setBounds(new Rectangle(xPos3, curYPos, 20, fieldHeight));
            datePanel.setBounds(new Rectangle(xPos2, curYPos, lblWidth2, fieldHeight));
            curYPos += fieldHeight + 5;

            lblQty.setBounds(new Rectangle(xPos1, curYPos, lblWidth1, fieldHeight));
            txtQty.setBounds(new Rectangle(xPos2, curYPos, lblWidth2, fieldHeight));
            btnAdvTraction.setBounds(new Rectangle(xPos3, curYPos, lblWidth3, fieldHeight));
//        btnSearch.setBounds(new Rectangle(xPos3, curYPos, 60, fieldHeight));
            curYPos += fieldHeight + 5;

            lblSharePrice.setBounds(new Rectangle(xPos1, curYPos, lblWidth1, fieldHeight));
            txtPrice.setBounds(new Rectangle(xPos2, curYPos, lblWidth2, fieldHeight));
            btnGetPrice.setBounds(new Rectangle(xPos3, curYPos, lblWidth3, fieldHeight));
            curYPos += fieldHeight + 5;
            if (showAdvSell) {
                advanceSellTable.setVisible(true);
                advanceSellTable.setBounds(new Rectangle(3, curYPos, (xPos3 + lblWidth3), advBuyPanelHeight));
                // advanceSellTable.updateGUI();
                // advanceSellTable.updateUI();
                curYPos += advBuyPanelHeight + 5;
            } else {

                advanceSellTable.setVisible(false);
            }

            lblCommissionCombo.setBounds(new Rectangle(xPos1, curYPos, lblWidth1, fieldHeight));
            CommissionCombo.setBounds(new Rectangle(xPos2, curYPos, lblWidth2, fieldHeight));
            curYPos += fieldHeight + 5;

            lblCommission.setBounds(new Rectangle(xPos1, curYPos, lblWidth1, fieldHeight));
            txtCommission.setBounds(new Rectangle(xPos2, curYPos, lblWidth2, fieldHeight));
            curYPos += fieldHeight + 5;

            lblMemo.setBounds(new Rectangle(xPos1, curYPos, lblWidth1, fieldHeight));
            memoPanel.setBounds(new Rectangle(xPos2, curYPos, lblWidth2, fieldHeight));
            curYPos += fieldHeight + 5;

            lblTotalDesc.setBounds(new Rectangle(xPos1, curYPos, lblWidth1, fieldHeight));
            lblTotal.setBounds(new Rectangle(xPos2 + 50, curYPos, lblWidth2 + 20, fieldHeight));
            curYPos += fieldHeight + 5;

            lblAvailCashBalDesc.setBounds(new Rectangle(xPos1, curYPos, lblWidth1 + 120, fieldHeight));
            lblAvailCashBal.setBounds(new Rectangle(xPos2 + 50, curYPos, lblWidth2 + 20, fieldHeight));

            curYPos += fieldHeight + 5;
            width = xPos3 + lblWidth3 + 10;
            transactionPanel.setBounds(new Rectangle(5, height, width - 5, curYPos));
            height += curYPos;


            curYPos = 5;
            lblWidth1 = 80;
            fieldHeight = 18;
            xPos2 = xPos1 + lblWidth1 + 5;
            xPos3 = xPos2 + lblWidth1 + 5;
            int xPos4 = xPos3 + lblWidth1 + 5;

            if (!showAdvSell) {
                lblLastTradeDesc.setBounds(new Rectangle(xPos1, curYPos, lblWidth1, fieldHeight));
                lblLastTrade.setBounds(new Rectangle(xPos2, curYPos, lblWidth1, fieldHeight));

                lblChangeDesc.setBounds(new Rectangle(xPos3, curYPos, lblWidth1, fieldHeight));
                lblChange.setBounds(new Rectangle(xPos4, curYPos, lblWidth1, fieldHeight));
                curYPos += fieldHeight + 5;

                lblBidDesc.setBounds(new Rectangle(xPos1, curYPos, lblWidth1, fieldHeight));
                lblBid.setBounds(new Rectangle(xPos2, curYPos, lblWidth1, fieldHeight));

                lblOfferDesc.setBounds(new Rectangle(xPos3, curYPos, lblWidth1, fieldHeight));
                lblOffer.setBounds(new Rectangle(xPos4, curYPos, lblWidth1, fieldHeight));
                curYPos += fieldHeight + 5;

                lblVolumeDesc.setBounds(new Rectangle(xPos1, curYPos, lblWidth1, fieldHeight));
                lblVolume.setBounds(new Rectangle(xPos2, curYPos, lblWidth1, fieldHeight));

                lblCurrDesc.setBounds(new Rectangle(xPos3, curYPos, lblWidth1, fieldHeight));
                lblCurrency.setBounds(new Rectangle(xPos4, curYPos, lblWidth1, fieldHeight));
                curYPos += fieldHeight + 5;

                quoteSummaryPanel.setBounds(new Rectangle(5, height, width - 5, quotePanelHeight));
                height += quotePanelHeight + 5;
            }
//        btnOK.setBounds(new Rectangle(5, 5, 80, 20));
//        btnCancel.setBounds(new Rectangle(95, 5, 80, 20));
            btnOK.setPreferredSize(new Dimension(80, 20));
            btnAddAnother.setPreferredSize(new Dimension(120, 20));
            btnCancel.setPreferredSize(new Dimension(80, 20));

            centerPanel.setBounds(new Rectangle(width, height));
//        buttonPanel.setBounds(new Rectangle(width, buttonPanelHeight+30));
            buttonPanel.setPreferredSize(new Dimension(width, buttonPanelHeight));
            basePanel.setPreferredSize(new Dimension(width, height + buttonPanelHeight + quotePanelHeight));
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void setComponentLabels() {
        cmbTxType.addItem(Language.getString("BUY"));
        cmbTxType.addItem(Language.getString("SELL"));
        cmbTxType.addItem(Language.getString("OPENING_BAL"));
        cmbTxType.addItem(Language.getString("DIVIDEND"));
        cmbTxType.addItem(Language.getString("DEPOSITES"));
        cmbTxType.addItem(Language.getString("WITHDROWALS"));
        cmbTxType.addItem(Language.getString("CHARGES"));
        cmbTxType.addItem(Language.getString("REFUNDS"));
        lblCompNameDesc.setText(Language.getString("COMPANY_NAME")); //"Symbol");
        lblExchanges.setText(Language.getString("EXCHANGE")); //"Symbol");
        lblSymbol.setText(Language.getString("SYMBOL")); //"Symbol");
        btnGetPrice.setText(Language.getString("GET_MARKET_PRICE"));
        btnSearch.setText(Language.getString("SEARCH")); //"Search");
        btnAdvTraction.setText(Language.getString("ADV_TRANSACTION"));
        lblPortfolios.setText(Language.getString("ADD_TO_PORTFOLIO")); //"Add to Portfolio");
        lblTxnType.setText(Language.getString("TRANSACTION_TYPE")); //"Transaction Type");
        lblDate.setText(Language.getString("DATE")); //"Date");
        lblQty.setText(Language.getString("QUANTITY")); //"Quantity");
        lblSharePrice.setText(Language.getString("SHARE_PRICE")); //"Share Price");
        lblCommission.setText(Language.getString("TOTAL_COMMISSION")); //"Commission");
        lblCommissionCombo.setText(Language.getString("COMMISSION_RATE"));
        lblMemo.setText(Language.getString("MEMO_HINT")); //"Memo (max 60 chars)");
        lblTotalDesc.setText(Language.getString("TOTAL"));   //"Total");

        btnOK.setText(Language.getString("OK"));
        btnAddAnother.setText(Language.getString("ADD_ANOTHER"));
        btnCancel.setText(Language.getString("CANCEL"));
        lblAvailCashBalDesc.setText(Language.getString("AVAILABLE_CASH_BALANCE"));

//        lblLastTradeDesc.setText(Language.getString("LAST"));
        lblChangeDesc.setText(Language.getString("CHANGE"));
        lblOfferDesc.setText(Language.getString("OFFER"));
        lblBidDesc.setText(Language.getString("BID"));
        lblVolumeDesc.setText(Language.getString("VOLUME"));
        lblCurrDesc.setText(Language.getString("CURRENCY"));

        lblLastTrade.setText(PFUtilities.getConvertedData("0.00"));
        lblChange.setText(PFUtilities.getConvertedData("0.00"));
        lblOffer.setText(PFUtilities.getConvertedData("0.00"));
        lblBid.setText(PFUtilities.getConvertedData("0.00"));
        lblVolume.setText(PFUtilities.getConvertedData("0.00"));

        if (PFStore.isVWAPMode()) {
            lblLastTradeDesc.setText(Language.getString("VWAP"));
        } else {
            lblLastTradeDesc.setText(Language.getString("LAST"));
        }

    }

    private void addCenterPanelComponents() {
        centerPanel.add(lblCompNameDesc);
        centerPanel.add(lblCompName);
        centerPanel.add(lblExchanges);
        centerPanel.add(txtExchanges);
        centerPanel.add(lblSymbol);
        centerPanel.add(txtSymbol);
        centerPanel.add(btnSearch);
        centerPanel.add(lblPortfolios);
        centerPanel.add(cmbPortfolios);
        centerPanel.add(lblTxnType);
        centerPanel.add(cmbTxType);
        GUISettings.applyOrientation(centerPanel);
        transactionPanel.add(lblDate);
        transactionPanel.add(datePanel);
        transactionPanel.add(lblQty);
        transactionPanel.add(txtQty);
        transactionPanel.add(btnAdvTraction);
        transactionPanel.add(lblSharePrice);
        transactionPanel.add(txtPrice);
        transactionPanel.add(btnGetPrice);
        transactionPanel.add(advanceSellTable);
        transactionPanel.add(lblCommissionCombo);
        transactionPanel.add(CommissionCombo);
        transactionPanel.add(lblCommission);
        transactionPanel.add(txtCommission);
        transactionPanel.add(lblMemo);
        transactionPanel.add(memoPanel); // getMemoPanel("MEMO", btnMemo));
        transactionPanel.add(lblTotalDesc);
        transactionPanel.add(lblTotal);
        transactionPanel.add(lblAvailCashBalDesc);
        transactionPanel.add(lblAvailCashBal);
        centerPanel.add(transactionPanel);
        //  centerPanel.add(advSellPanel);
        centerPanel.add(quoteSummaryPanel);

        lblLastTrade.setHorizontalAlignment(JLabel.TRAILING);
        lblChange.setHorizontalAlignment(JLabel.TRAILING);
        lblBid.setHorizontalAlignment(JLabel.TRAILING);
        lblOffer.setHorizontalAlignment(JLabel.TRAILING);
        lblVolume.setHorizontalAlignment(JLabel.TRAILING);
        lblCurrency.setHorizontalAlignment(JLabel.TRAILING);
        lblAvailCashBal.setHorizontalAlignment(JLabel.TRAILING);
        lblTotal.setHorizontalAlignment(JLabel.TRAILING);

        quoteSummaryPanel.add(lblLastTradeDesc);
        quoteSummaryPanel.add(lblLastTrade);
        quoteSummaryPanel.add(lblChangeDesc);
        quoteSummaryPanel.add(lblChange);
        quoteSummaryPanel.add(lblBidDesc);
        quoteSummaryPanel.add(lblBid);
        quoteSummaryPanel.add(lblOfferDesc);
        quoteSummaryPanel.add(lblOffer);
        quoteSummaryPanel.add(lblVolumeDesc);
        quoteSummaryPanel.add(lblVolume);
        quoteSummaryPanel.add(lblCurrDesc);
        quoteSummaryPanel.add(lblCurrency);
    }

    public void showDialog() {
        this.setPreferredSize(new Dimension(width + 10, height + buttonPanelHeight + 30));
        centerUI();
        this.setVisible(true);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
    }

    public Dimension getPreferredSize() {
        return new Dimension(new Dimension(width + 10, height + buttonPanelHeight + 30));
    }

    private void centerUI() {
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        this.setBounds((int) (oDm.getWidth() / 2 - (this.getWidth() / 2)),
                (int) (oDm.getHeight() / 2 - (this.getHeight() / 2)),
                this.getWidth(), this.getHeight());
    }

    private void closeDialog() {
        this.exitFromLoop();
        resetAndClearFIFOArrayList();
        // finalizeWindow();
        super.doDefaultCloseAction();
        this.dispose();
    }

    public JPanel getDatePanel(String type, final TWButton arrowButton, final JLabel lblDate) {
        JPanel datePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        datePanel.setPreferredSize(new Dimension(110, 20));
        datePanel.setBorder(BorderFactory.createEtchedBorder());

        lblDate.setPreferredSize(new Dimension(88, 16));
        lblDate.setOpaque(true);
        datePanel.add(lblDate);
        arrowButton.setIcon(new DownArrow());
        arrowButton.setBorder(null);
        arrowButton.setActionCommand(type);
        arrowButton.addActionListener(this);
        arrowButton.setPreferredSize(new Dimension(18, 16));
        datePanel.add(arrowButton);
        //fromDatePanel.addMouseListener(this);
        //panel3.add(fromDatePanel);
        //mainPanel.add(panel3);
        return datePanel;
    }

    public JPanel getMemoPanel(String type, final TWButton arrowButton) {
        memoPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        //todo - 21/12/2006
        memoPanel.setPreferredSize(new Dimension(100, 20));
//        memoPanel.setPreferredSize(new Dimension(110, 20));

//        memoPanel.setBorder(BorderFactory.createEmptyBorder());
        memoPanel.setBorder(BorderFactory.createEtchedBorder());

        lblMemoVoid = new JLabel("");
        lblMemoVoid.addMouseListener(this);
        //todo - 21/12/2006
//        lblMemoVoid.setPreferredSize(new Dimension(86, 16));
        lblMemoVoid.setPreferredSize(new Dimension(87, 16));

        lblMemoVoid.setOpaque(true);
        memoPanel.add(lblMemoVoid);
        arrowButton.setIcon(new DownArrow());
        arrowButton.setBorder(null);
        arrowButton.setActionCommand(type);
        arrowButton.addActionListener(this);
        //todo - 21/12/2006
//        arrowButton.setPreferredSize(new Dimension(18, 16));
        arrowButton.setPreferredSize(new Dimension(18, 16));

        memoPanel.add(arrowButton);
        return memoPanel;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("FD")) {
//            this.dateSelector = FROM_DATE;
            JComponent source = (JComponent) e.getSource();
            Point location = source.getParent().getLocation();
            location.move(-80, 20);
            SwingUtilities.convertPointToScreen(location, source);
            datePicker.setLocation(location);
            datePicker.show();
        } else if (e.getActionCommand().equals("MEMO")) {
            showMemo();
        }
    }

    private void showMemo() {
        int popWidth = 0;
        if (Language.isLTR()) {
            popWidth = (int) memoField.getPreferredSize().getWidth();
        }
        memoField.show(memoPanel, 0, btnMemo.getHeight() - 4);
    }

    public void dateSelected(Object source, int iYear, int iMonth, int iDay) {
        boolean isValidDate = false;
//        DecimalFormat numberFormat = new DecimalFormat("00");
        Calendar cal = Calendar.getInstance();
        cal.setTimeInMillis(System.currentTimeMillis());

        int iCurYear = cal.get(Calendar.YEAR);
        int iCurMonth = cal.get(Calendar.MONTH);
        int iCurDay = cal.get(Calendar.DAY_OF_MONTH);

        if (iCurYear > iYear) {
            isValidDate = true;
        } else if (iCurYear == iYear) {
            if (iCurMonth > iMonth) {
                isValidDate = true;
            } else if (iCurMonth == iMonth) {
                if (iCurDay >= iDay) {
                    isValidDate = true;
                }
            }
        }

        if (!isValidDate) {
//            System.out.println("Future Dates are not allowed!");
            new ShowMessage(Language.getString("MSG_FUTURE_DATES_NOT_ALLOWED"), "I");
        } else {
            iMonth++;
//            lblFromDate.setText("" + iYear + "/" + iMonth + "/" + iDay);
            lblFromDate.setText(PFUtilities.getConvertedData("" + iDay + "/" + iMonth + "/" + iYear));
            datePicker.setVisible(false);
        }
        cal = null;
//        numberFormat = null;
    }

    private void searchSymbol() {
        try {
            sKey = SharedMethods.searchSymbols(getTitle(), true, true)[0]; // symbols.getSymbols()[0];
            setSymbol(sKey);
            //txtSymbol.setText(SharedMethods.getSymbolFromKey(sKey));
            //String exchange = SharedMethods.getExchangeFromKey(sKey);

            //selectExchange(exchange);
        } catch (Exception ex) {
        }
    }

    /*private void selectExchange(String exchange){
        try {
            for(TWComboItem item: exchanges){
                if (item.getId().equals(exchange)){
                    cmbExchanges.setSelectedItem(item);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public void focusGained(FocusEvent e) {
    }

    public void setSymbol(String key) {
        if (key != null) {
            String exchange = SharedMethods.getExchangeFromKey(key);
            String symbol = SharedMethods.getSymbolFromKey(key);
            Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, SharedMethods.getInstrumentTypeFromKey(key));
            symbolDecimals = stock.getDecimalCount();
            txtExchanges.setText(ExchangeStore.getSharedInstance().getExchange(exchange).getDisplayExchange());

            this.sKey = key;
            if (!isEditMode)
                requestRegister.add(sKey); // no need to add to the request register in edit mode
            lastEnteredSymbol = sKey;
            lblCompName.setText(PortfolioInterface.getCompanyName(sKey));
            txtSymbol.setText(symbol);
            updateStockPanel(sKey);

            btnAddAnother.setEnabled(true);
            btnOK.setEnabled(true);
        }

        txtSymbol.setEnabled(true);
        btnSearch.setEnabled(true);
        btnGetPrice.setEnabled(true);
        lblSymbol.setEnabled(true);
        lblCompName.setEnabled(true);
        lblExchanges.setEnabled(true);
        validateAdvaceBuy();
    }

    public void notifyInvalidSymbol(String symbol) {
        btnAddAnother.setEnabled(true);
        btnOK.setEnabled(true);
        txtSymbol.setText("");
        txtSymbol.setEnabled(true);
        btnSearch.setEnabled(true);
        lblSymbol.setEnabled(true);
        lblCompName.setEnabled(true);
        lblExchanges.setEnabled(true);
        //txtSymbol.requestFocus();
//        txtSymbol.setSelectionStart(0);
//        txtSymbol.setSelectionEnd(txtSymbol.getText().length());
//        txtSymbol.requestFocus();
    }

    public void focusLost(FocusEvent e) {
        if (e.getSource() == txtSymbol) {
            String symbol = txtSymbol.getText().trim().toUpperCase();
            if (symbol.length() > 0) {
                if (isTextChanged) {
                    btnAddAnother.setEnabled(false);
                    btnOK.setEnabled(false);
                    txtSymbol.setEnabled(false);
                    btnSearch.setEnabled(false);
                    lblSymbol.setEnabled(false);
                    lblCompName.setEnabled(false);
                    lblExchanges.setEnabled(false);
                    PortfolioInterface.validateSymbol(symbol, "PF_SIM_DLG", this);

                } else {
                    if (PFStore.isContainedSymbol(sKey)) {
                        applySelectedSymbol(sKey);
                        validateAdvaceBuy();
                    }
                }
                isTextChanged = false;
            }
        } else if ((e.getSource() == txtPrice) || (e.getSource() == txtQty)) {
            calculateCommission();
            calculateTotal();
            validateAdvaceBuy();
            if (isFIFO && showAdvSell) {
                advanceSellTable.updateUI();
            }
        } else if (e.getSource() == txtCommission) {
            calculateTotal();
            if (isFIFO && showAdvSell) {
                advanceSellTable.updateUI();
            }

        } else if (isFIFO && showAdvSell && e.getSource() == advanceSellTable) {
            advanceSellTable.updateUI();

        }
    }

    /*private String createKey() {
        String exchange = txtExchanges.getText();
        String symbol = (txtSymbol.getText().trim()).toUpperCase();
        return SharedMethods.getKey(exchange,symbol);
    }*/

    public void calculateCommission() {

        /*double price = 0f;
        long qty = 0;
        double commission = 0f;

        if (Settings.isPortfolioSimulatorAutoCommission()) {
            try {
                price = Double.parseDouble(txtPrice.getText());
            } catch (NumberFormatException ex) {
                price = 0f;
            }

            try {
                qty = Long.parseLong(txtQty.getText());
            } catch (NumberFormatException ex) {
                qty = 0;
            }

            int txnType = cmbTxType.getSelectedIndex() + 1;
            if (txnType != PFUtilities.DIVIDEND) {
                try {
                    Interpreter interpreter = new Interpreter();
                    interpreter.set("price", price);
                    interpreter.set("quantity", qty);
                    commission = (Double) interpreter.source("rules/" + txtExchanges.getText() + "/pfsim.txt");
                    interpreter = null;
                } catch (Exception e) { // could not eveluate the rule or rule not found
                    e.printStackTrace();
                }
                if (commission > 0) {
                    txtCommission.setText("" + (float) ((int) (commission * 100)) / (float) 100);// commission);
                }
            } else {
                commission = 0;
                txtCommission.setText("0");
            }


        }*/


        double price = 0f;
        long qty = 0;
        double commission = 0f;
        CommissionObject comObj = null;
        boolean manual = false;
        String curr = "";
        String pfCurr = "";

        try {
            if (PFStore.getInstance().getCommissionList().size() > 0) {
                try {
                    price = Double.parseDouble(txtPrice.getText());
                } catch (NumberFormatException ex) {
                    price = 0f;
                }

                try {
                    qty = Long.parseLong(txtQty.getText());
                } catch (NumberFormatException ex) {
                    qty = 0;
                }

                try {
                    comObj = PFStore.getInstance().getCommission(((TWComboItem) CommissionCombo.getItemAt(CommissionCombo.getSelectedIndex())).getValue());
                    if (comObj == null) {
                        manual = ((TWComboItem) CommissionCombo.getItemAt(CommissionCombo.getSelectedIndex())).getId().equals(Language.getString("MANAUL"));
                    }
                } catch (Exception e) {

                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

                int txnType = cmbTxType.getSelectedIndex() + 1;
                if (comObj != null && (txnType == PFUtilities.BUY) || (txnType == PFUtilities.SELL) || (txnType == PFUtilities.OP_BAL)) {
                    commission = (price * qty) * comObj.getPercentage();
                    commission = commission / 100;
                    //                try {
                    //                    curr = PortfolioInterface.getStockObject(sKey).getCurrencyCode();
                    //                    pfCurr= PFStore.getInstance().getPortfolio(cmbPortfolios.getSelectedIndex()).getCurrency();
                    //                } catch(Exception e) {
                    //                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    //                }

                    //   commission = convertToSelectedCurrency(curr,commission);
                    commission = Math.max(commission, comObj.getMinPrice());


                    if (commission > 0) {
                        txtCommission.setText("" + (float) ((int) (commission * 100)) / (float) 100);// commission);
                        //  lblCommission   Language.getString("TOTAL_COMMISSION")

                    }
                } else if (!manual) {
                    commission = 0;
                    txtCommission.setText("0");
                }
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private double convertFromSelectedCurrency(String tgtCurr, double value) {
        return value * CurrencyStore.getRate(PFStore.getInstance().getPortfolio(cmbPortfolios.getSelectedIndex()).getCurrency(), tgtCurr);
    }

    private void applySelectedSymbol(String sKey) {
        isInternallyValidatedSymbol = true;
        lastEnteredSymbol = sKey;
        lblCompName.setText(PortfolioInterface.getCompanyName(sKey));
        txtSymbol.setText(SharedMethods.getSymbolFromKey(sKey));//PortfolioInterface.getSymbolFromKey(sKey));
        updateStockPanel(sKey);
    }

    private void calculateTotal() {
        double oldTotal = 0d;
        double total = 0d;
        double price = 0d;
        double qty = 0d;
        double commission = 0d;
        String curr = null;
        try {
            curr = PortfolioInterface.getStockObject(sKey).getCurrencyCode();
        } catch (Exception e) {
            curr = PFStore.getInstance().getPortfolio(cmbPortfolios.getSelectedIndex()).getCurrency();
        }
        try {
            price = Double.parseDouble(txtPrice.getText());
        } catch (NumberFormatException ex) {
            price = 0d;
        }
        try {
            commission = Double.parseDouble(txtCommission.getText());
        } catch (NumberFormatException ex) {
            commission = 0d;
        }
        try {
            qty = Integer.parseInt(txtQty.getText());
        } catch (NumberFormatException ex) {
            qty = 0;
        }
        int txnType = cmbTxType.getSelectedIndex() + 1;
        switch (txnType) {
            case PFUtilities.BUY:
            case PFUtilities.OP_BAL:
                total = convertToSelectedCurrency(curr, (price * qty + commission));
                if (isEditMode)
                //    oldTotal = oldPrice * oldQty + oldCommission;
                break;
            case PFUtilities.SELL:
                total = convertToSelectedCurrency(curr, (price * qty - commission));
                if (isEditMode)
                    oldTotal = oldPrice * oldQty - oldCommission;
                break;
            case PFUtilities.DIVIDEND:
            case PFUtilities.DEPOSITE:
            case PFUtilities.WITHDRAWAL:
            case PFUtilities.CHARGES:
            case PFUtilities.REFUNDS:
                total = convertToSelectedCurrency(curr, price);
                if (isEditMode)
                    oldTotal = oldPrice;
                break;
        }
//        (value/(ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sKey)).getPriceModificationFactor())));
//         //Bug ID <#0050> - convert  KSE fill amount to KWD amount
        try {
            if ((ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sKey)).getPriceModificationFactor()) > 0) {
                total = (total / (ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sKey)).getPriceModificationFactor()));
                //oldTotal = (oldTotal / (ExchangeStore.getSharedInstance().getExchange(txtExchanges.getText()).getPriceModificationFactor()));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (isEditMode) {
           // lblTotal.setText(PFUtilities.getNormalizedPrice(total - oldTotal));
            lblTotal.setText(PFUtilities.getNormalizedPrice(total));
        } else
            lblTotal.setText(PFUtilities.getNormalizedPrice(total));
    }

    private void loadThemeColors() {
        upColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
//        upfgColor  	= Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR");
        downColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
//        downfgColor	= Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR");
//        fgColor     = Theme.getColor("LABEL_FGCOLOR");
        //fd1Color    = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
    }

    private void updateStockPanel(String sKey) {
        try {
            if (sKey == null)
                return;
            Stock stock = PortfolioInterface.getStockObject(sKey);
            if (stock == null)
                return;
            if (PFStore.isVWAPMode()) {
                lblLastTradeDesc.setText(Language.getString("VWAP"));
                if (PortfolioInterface.getVWAP(stock) > 0) {
                    lblLastTrade.setText(PFUtilities.getNormalizedPrice(PortfolioInterface.getVWAP(stock),symbolDecimals));
                } else {
                    lblLastTrade.setText(PFUtilities.getNormalizedPrice(PortfolioInterface.getPreviousClosed(stock),symbolDecimals));
                }
            } else {
                lblLastTradeDesc.setText(Language.getString("LAST"));
                if (PortfolioInterface.getLastTrade(stock) > 0) {
                    lblLastTrade.setText(PFUtilities.getNormalizedPrice(PortfolioInterface.getLastTrade(stock),symbolDecimals));
                } else {
                    lblLastTrade.setText(PFUtilities.getNormalizedPrice(PortfolioInterface.getPreviousClosed(stock),symbolDecimals));
                }
            }
//            if (PortfolioInterface.getLastTrade(stock) > 0&&PFStore.isVWAPMode()) {
//                lblLastTrade.setText(PFUtilities.getNormalizedPrice((float) PortfolioInterface.getVWAP(stock)));
//            } else {
//                lblLastTrade.setText(PFUtilities.getNormalizedPrice((float) PortfolioInterface.getLastTrade(stock)));
//            }
//            if (PortfolioInterface.getLastTrade(stock) > 0) {
//                lblLastTrade.setText(PFUtilities.getNormalizedPrice(PortfolioInterface.getLastTrade(stock)));
//            } else {
//                lblLastTrade.setText(PFUtilities.getNormalizedPrice(PortfolioInterface.getPreviousClosed(stock)));
//            }
            if (PortfolioInterface.getChange(stock)[0] == PortfolioInterface.DEFAULT_DOUBLE_VALUE) {
                lblChange.setText(Language.getString("NA"));
//            lblChange.setHorizontalAlignment(JLabel.TRAILING);
            } else {
                lblChange.setText(PFUtilities.getNormalizedPrice(PortfolioInterface.getChange(stock)[0],symbolDecimals));
                if (PortfolioInterface.getChange(stock)[0] > 0) {
                    lblChange.setForeground(upColor);
                } else if (PortfolioInterface.getChange(stock)[0] < 0) {
                    lblChange.setForeground(downColor);
                } else {
                    lblChange.setForeground(lblChangeDesc.getForeground());
                }
            }
//        lblChange.setText(oPriceFormat.format(stock.getChange()[0]));
            lblBid.setText(PFUtilities.getNormalizedPrice(PortfolioInterface.getBidPrice(stock),symbolDecimals));
            lblOffer.setText(PFUtilities.getNormalizedPrice(PortfolioInterface.getAskPrice(stock),symbolDecimals));
            lblVolume.setText(PFUtilities.getNormalizedQty(PortfolioInterface.getVolume(stock)));
            if (PortfolioInterface.getCurrency(stock) != null) {
                lblCurrency.setText(PortfolioInterface.getCurrency(stock));
            } else {
                lblCurrency.setText(Language.getString("NA"));
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void run() {
        while (continueLoop) {
            if (sKey != null)
                updateStockPanel(sKey);
            try {
                Thread.sleep(1000);
            } catch (Exception ex) {
//                ex.printStackTrace();
            }
        }
    }

    public void setCurrentThread(Thread thread) {
        this.selfThread = thread;
    }

    public void exitFromLoop() {
        continueLoop = false;
        try {
            selfThread.interrupt();
        } catch (Exception ex) {
        }
    }

    private boolean isInputsAreValid() {
        boolean success = false;
        if ((cmbTxType.getSelectedIndex() == 4) || (cmbTxType.getSelectedIndex() == 5)
                || (cmbTxType.getSelectedIndex() == 6) || (cmbTxType.getSelectedIndex() == 7)) {
            if (lblFromDate.getText().length() > 0) {
                if (txtPrice.getText().length() > 0) {
                    if (cmbTxType.getSelectedIndex() == 5) {
                        PortfolioRecord pfRecord = PFStore.getInstance().getPortfolio((PFStore.getInstance().getPortfolio(cmbPortfolios.getSelectedIndex()).getPfID()));
                        if (Float.parseFloat(txtPrice.getText()) > (pfRecord.getCashBalance() + oldPrice)) {
                            int option = SharedMethods.showConfirmMessage(Language.getString("MSG_PRICE_WITHDRAW_AMOUNT_EXCEED"), JOptionPane.WARNING_MESSAGE,
                                    JOptionPane.YES_NO_OPTION);
                            if (option == JOptionPane.YES_OPTION) {
                                success = true;
                            } else {
                                return false;
                            }
//                            new ShowMessage(Language.getString("MSG_PRICE_WITHDRAW_AMOUNT_EXCEED"), "I");
                        } else {
                            success = true;
                        }
                    } else {
                        success = true;
                    }
                } else {
                    new ShowMessage(Language.getString("MSG_PRICE_CANNOT_BE_BLANK"), "I");
                    return false;
                }
            } else {
                new ShowMessage(Language.getString("MSG_TX_DATE_CANNOT_BE_BLANK"), "I");
                return false;
            }
        } else {
            if (txtSymbol.getText().length() > 0) {
                if (lblFromDate.getText().length() > 0) {
                    if ((txtQty.getText().length() > 0) || (cmbTxType.getSelectedIndex() == 3)) {
                        if (txtPrice.getText().length() > 0) {
                            success = true;
                        } else {
                            new ShowMessage(Language.getString("MSG_PRICE_CANNOT_BE_BLANK"), "I");
                            return false;
                        }
                    } else {
                        new ShowMessage(Language.getString("MSG_QTY_CANNOT_BE_BLANK"), "I");
                        return false;
                    }
                } else {
                    new ShowMessage(Language.getString("MSG_TX_DATE_CANNOT_BE_BLANK"), "I");
                    return false;
                }
            } else {
                new ShowMessage(Language.getString("MSG_SYMBOL_CANNOT_BE_BLANK"), "I");
                return false;
            }
        }
        if (isFIFO && showAdvSell) {
            if (advanceSellTable.getTable().isEditing()) {
                new ShowMessage(Language.getString("MSG_PLEASE_EXIT_FROM_EDIT_MODE"), "I");
                return false;
            }

        }
        return success;
    }

    private synchronized boolean saveTransaction() {
        byte txnType = 0;
        int quantity = 0;
        double price = 0f;
        double amount = 0f;
        double oldAmount = 0f;
        double brokerage = 0f;
        PortfolioRecord pfRecord = null;
        Calendar cal = Calendar.getInstance();
        String curr = null;
        String comID = "Manual";
        try {
            curr = PortfolioInterface.getStockObject(sKey).getCurrencyCode();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (isInputsAreValid()) {
            if (!isEditMode || (record == null)) {
                record = new TransactRecord();
                record.setSKey(sKey);
            }
            // Collect entered Transaction details
            txnType = (byte) (cmbTxType.getSelectedIndex() + 1);
            try {
                quantity = Integer.parseInt(txtQty.getText());
                if (quantity < 0) {
                    new ShowMessage(Language.getString("INVALID_QUANTITY"), "I");
                    return false;
                }
            } catch (NumberFormatException ex2) {
                quantity = 0;
            }
            try {
                if (!((TWComboItem) CommissionCombo.getItemAt(CommissionCombo.getSelectedIndex())).getValue().equals(Language.getString("MANUAL"))) {

                    calculateCommission();
                }
                comID = ((TWComboItem) CommissionCombo.getItemAt(CommissionCombo.getSelectedIndex())).getValue();
                brokerage = Double.parseDouble(txtCommission.getText());
            } catch (Exception ex) {
                brokerage = 0.00d;
            }
            price = Double.parseDouble(txtPrice.getText());
            try {
                StringTokenizer st = new StringTokenizer(lblFromDate.getText().trim(), "/", false);
                int date = Integer.parseInt(st.nextToken());
                int month = Integer.parseInt(st.nextToken()) - 1;
                int year = Integer.parseInt(st.nextToken());
                st = null;
                Calendar cal2 = Calendar.getInstance();
                if ((cal2.get(Calendar.YEAR) == year) && (cal2.get(Calendar.MONTH) == month) && (cal2.get(Calendar.DATE) == date)) {
                    cal.setTime(new Date());
                } else {
                    cal.set(year, month, date, 23, 59, 59);
                }
            } catch (Exception ex1) {
                ex1.printStackTrace();
                new ShowMessage(Language.getString("MSG_INVALID_DATE_FORMAT"), "I");
                return false;
            }

            if (!isEditMode) {
                record.setPfID(PFStore.getInstance().getPortfolio(cmbPortfolios.getSelectedIndex()).getPfID());
            }
            pfRecord = PFStore.getInstance().getPortfolio(record.getPfID());
            if (curr == null) {
                curr = pfRecord.getCurrency();
            }

            switch (txnType) {
                case PFUtilities.BUY:      // Check for available Cash Balance
                    //Bug ID <#0050> - convert  KSE fill amount to KWD amount
                    //amount = convertToSelectedCurrency(curr, ((quantity * price) + brokerage));
                    amount = convertToSelectedCurrency(curr, ((quantity * price)));
                    amount = (amount / (ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sKey)).getPriceModificationFactor()));
                    amount = amount + convertToSelectedCurrency(curr, brokerage);
                    if (isEditMode) {

                        if (quantity < oldQty && (oldQty - quantity) > record.getRemainingHolding()) {
                            String sMesg = "<html>" + Language.getString("MSG_AVAILABLE_AMOUNT_TODEC") + record.getSoldHolding() +
                                    "<br>" + "&nbsp;:&nbsp;<b>" +
                                    Language.getString("MSG_CANNOT_DELETE_REASON") + "</b>&nbsp;"
                                    + "&nbsp;&nbsp;<b>"
                                    + "</b></html>";
                            new ShowMessage(sMesg, "I");
                            return false;


                        }


                        oldAmount = convertToSelectedCurrency(curr, (oldQty * oldPrice + oldCommission));
                        //Bug ID <#0050> - convert  KSE fill amount to KWD amount
                        oldAmount = (oldAmount / (ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sKey)).getPriceModificationFactor()));
                        amount = amount - oldAmount;
                    }
                    if (amount > pfRecord.getCashBalance()) {
                        String sMesg = "<html>" + Language.getString("MSG_EXCEEDED_CASH_LIMIT_WARNING") +
                                "<br>" + Language.getString("MSG_AVAILABLE_CASH_AMOUNT") + "&nbsp;:&nbsp;<b>" +
                                PFUtilities.getNormalizedPrice(pfRecord.getCashBalance(),symbolDecimals) + ",</b>&nbsp;" +
                                Language.getString("TRANSACTION_VALUE") + "&nbsp;:&nbsp;<b>" +
                                PFUtilities.getNormalizedPrice(amount,symbolDecimals) + "</b></html>";
                        new ShowMessage(sMesg, "I");
                        return false;
                    } else {
                        pfRecord.setCashBalance(pfRecord.getCashBalance() - amount);
                    }
                    break;
                case PFUtilities.SELL:     // Check for available qty
                    int availQty = PFStore.getInstance().getAvailableQuantity(record.getPfID(), record.getSKey());
//                    if (isEditMode) {
//                        quantity = quantity - oldQty;
//                    }
                    if (availQty < quantity) {
                        String sMesg = "<html>" + Language.getString("MSG_EXCEEDS_STOCKS_LIMIT_WARNING") +
                                "<br>" + Language.getString("MSG_AVAILABLE_CASH_AMOUNT") + "&nbsp;:&nbsp;<b>" +
                                PFUtilities.getNormalizedQty(availQty) + ",</b>&nbsp;" +
                                Language.getString("SALE_QTY") + "&nbsp;:&nbsp;<b>" +
                                PFUtilities.getNormalizedQty(quantity) + "</b></html>";
                        new ShowMessage(sMesg, "I");
                        return false;
                    } else {
                        if (isFIFO) {
                            ArrayList fifoList = validateExecuteFIFOSell(quantity);
                            if (fifoList != null && !fifoList.isEmpty()) {


                                for (int i = 0; i < fifoList.size(); i++) {

                                    TransactRecord rec = (TransactRecord) fifoList.get(i);
                                    if (isEditMode) {
                                        rec.editSellQuantity(record.getId(), rec.getSellTempVal());
                                        record.putBuyQty(rec.getId(), rec.getSellTempVal());
                                    } else {
                                        rec.sellQuantity(record.getId(), rec.getSellTempVal());
                                        record.putBuyQty(rec.getId(), rec.getSellTempVal());
                                    }
                                }

                                amount = convertToSelectedCurrency(curr, (quantity * price - brokerage));
                                amount = (amount / (ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sKey)).getPriceModificationFactor()));
                                if (isEditMode) {
                                    oldAmount = convertToSelectedCurrency(curr, (oldQty * oldPrice - oldCommission));
                                    amount = amount - oldAmount;
                                }
                                pfRecord.setCashBalance(pfRecord.getCashBalance() + amount);


                            } else if (!showAdvSell) {
                                ExecuteHiddenFIFOSellInWAMode(quantity, record.getId());
                                amount = convertToSelectedCurrency(curr, (quantity * price - brokerage));
                                amount = (amount / (ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sKey)).getPriceModificationFactor()));
                                if (isEditMode) {
                                    oldAmount = convertToSelectedCurrency(curr, (oldQty * oldPrice - oldCommission));
                                    amount = amount - oldAmount;
                                }
                                pfRecord.setCashBalance(pfRecord.getCashBalance() + amount);

                            } else {

//                                String sMesg = "<html>" + "Special" +Language.getString("MSG_DIFFERENT_QTY") +
//                                "<br>" + Language.getString("MSG_AVAILABLE_CASH_AMOUNT") + "&nbsp;:&nbsp;<b>" +
//                                PFUtilities.getNormalizedQty(availQty) + ",</b>&nbsp;" +
//                                Language.getString("SALE_QTY") + "&nbsp;:&nbsp;<b>" +
//                                PFUtilities.getNormalizedQty(quantity) + "</b></html>";
                                new ShowMessage(Language.getString("MSG_DIFFERENT_QTY"), "I");
                                return false;

                            }


                        } else {
                            amount = convertToSelectedCurrency(curr, (quantity * price - brokerage));
                            amount = (amount / (ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sKey)).getPriceModificationFactor()));
                            if (isEditMode) {
                                oldAmount = convertToSelectedCurrency(curr, (oldQty * oldPrice - oldCommission));
                                amount = amount - oldAmount;
                            }
                            pfRecord.setCashBalance(pfRecord.getCashBalance() + amount);
                            if (isEditMode) {
                                ExecuteHiddenFIFOSellEditInWAMode(quantity, record.getId());
                            } else {
                                ExecuteHiddenFIFOSellInWAMode(quantity, record.getId());
                            }
                        }
                    }
                    break;
                case PFUtilities.OP_BAL:
                    break;
                case PFUtilities.DIVIDEND:
                case PFUtilities.DEPOSITE:
                case PFUtilities.REFUNDS:
                    if (isEditMode) {
                        amount = price - oldPrice;
                    } else
                        amount = price;
                    pfRecord.setCashBalance(pfRecord.getCashBalance() + amount);
                    break;
                case PFUtilities.WITHDRAWAL:
                case PFUtilities.CHARGES:
                    if (isEditMode) {
                        amount = price - oldPrice;
                    } else
                        amount = price;
                    pfRecord.setCashBalance(pfRecord.getCashBalance() - amount);
                    break;
            }

            if (isEditMode) {
                //   PFStore.getInstance().deleteTransaction(record.getId(),false);
                //  record = null;
                //   record = new TransactRecord();
                record.setSKey(sKey);
                record.setPfID(PFStore.getInstance().getPortfolio(cmbPortfolios.getSelectedIndex()).getPfID());
            } else {
                for (int i = 0; i < requestRegister.size(); i++) {
                    System.out.println(sKey + " " + requestRegister.get(i));
//                     if (!sKey.equals(requestRegister.get(i))){ // do not remove the selected symbol
                    PortfolioInterface.removeSymbol(requestRegister.get(i));
//                     }
                }
                requestRegister.clear();
            }
            record.setCurrency(curr);
            record.setTxnType(txnType);
            record.setQuantity(quantity);
            record.setBrokerage(brokerage);
            record.setPrice(price);
            record.setTxnDate(cal.getTimeInMillis());
            record.setComissionID(comID);

            cal = null;

            if (memoField.getText().length() > 0)
                record.setMemo(memoField.getText());

            if (!isEditMode) {
                PFStore.getInstance().addTransaction(record);
            }
//            if (!isEditMode && !isInternallyValidatedSymbol)
//                PortfolioInterface.removeSymbol(record.getSKey());

            isEditMode = false;
            parent.createFilteredTransactionList(null, PFUtilities.NONE, null, null);
            parent.setFilterDescription(false, null, null, null, PFUtilities.NONE);
            parent.createValuationList();
            parent.updateValuationLists();
            parent.displayCashBalance();

            if (parent.isAllPannelSelected()) {

                parent.tabStareChanged(new TWTabEvent(this, TWTabEvent.STATE_SELECTED));
            }

            record = null;
            System.out.println("Transaction saved");
            return true;
        } else
            return false;
    }

    public void createTotalLabel() {
        PortfolioRecord pfRecord = PFStore.getInstance().getPortfolio(cmbPortfolios.getSelectedIndex());
        String str = Language.getString("TOTAL");
        if (Language.isLTR()) {
            str = str + " ( " + pfRecord.getCurrency() + " ) ";
        } else {
            str = " ( " + pfRecord.getCurrency() + " ) " + str;
        }
        lblTotalDesc.setText(str);
        pfRecord = null;
        str = null;
    }


    private void initializeDataInputs() {
//        if (sKey != null) {
//            QuoteStore.removeSymbol(sKey);    // Remove temporarily
        lastEnteredSymbol = null;
        sKey = null;
//        }
        txtSymbol.setEnabled(true);
        btnSearch.setEnabled(true);
        lblSymbol.setEnabled(true);
        lblCompName.setEnabled(true);
        lblExchanges.setEnabled(true);
        cmbPortfolios.setEnabled(true);
        cmbTxType.setEnabled(true);

        record = null;
        isEditMode = false;

        txtSymbol.setText("");
        int index= cmbTxType.getSelectedIndex();
        cmbTxType.setSelectedIndex(0);
        txtQty.setText("");
        txtPrice.setText("");
        txtCommission.setText("");
        memoField.setText("");
        lblCompName.setText("");
        lblMemoVoid.setText("");

        lblFromDate.setText(PFUtilities.getFormattedDate(new Date()));
//        lblFromDate.setText("");
        lblLastTrade.setText(PFUtilities.getNormalizedPrice(0,symbolDecimals)); // "0.00");
        lblChange.setText(PFUtilities.getNormalizedPrice(0,symbolDecimals)); // "0.00");
        lblBid.setText(PFUtilities.getNormalizedPrice(0,symbolDecimals)); // "0.00");
        lblOffer.setText(PFUtilities.getNormalizedPrice(0,symbolDecimals)); // "0.00");
        lblVolume.setText(PFUtilities.getNormalizedQty(0)); // "0");
        lblTotal.setText(PFUtilities.getNormalizedPrice(0)); // "0.00");

        displayAvailableCashBalance();
        resetAndClearFIFOArrayList();
        validateAdvaceBuy();
         cmbTxType.setSelectedIndex(index);
    }

    private void loadPortflioList() {
        PortfolioRecord record = null;
        for (int i = 0; i < PFStore.getInstance().getPortfolioList().size(); i++) {
            record = (PortfolioRecord) PFStore.getInstance().getPortfolioList().get(i);
            cmbPortfolios.addItem(record.getName());
//            if (record.isDefault())
//                cmbPortfolios.setSelectedItem(record.getName());


            record = null;
        }

    }

    public void setSelectedPF(long pfid) {
        cmbPortfolios.setSelectedItem(PFStore.getInstance().getPortfolio(pfid).getName());

    }

    public void setSelectedCommision(long pfid) {
        CommissionObject obj = PFStore.getInstance().getCommission(PFStore.getInstance().getPortfolio(pfid).getCommisionName());
        if (obj != null && CommissionItems.contains(obj.getId())) {
            int pos = CommissionItems.indexOf(obj.getId());
            try {
                CommissionCombo.setSelectedIndex(pos);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

    }

    public void setSelectedCommision(String id) {
        if (id != null && CommissionItems.contains(id)) {
            int pos = CommissionItems.indexOf(id);
            try {
                CommissionCombo.setSelectedIndex(pos);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

    }

    private void loadCommisionList() {
        CommissionCombo.removeAllItems();
        CommissionObject commision = null;
        ArrayList<String> itemlist = PFStore.getInstance().getCommissionList();
        CommissionItems = new ArrayList<String>();
        for (int i = 0; i < itemlist.size(); i++) {
            commision = PFStore.getInstance().getCommission(itemlist.get(i));

            //CommissionCombo.addItem(commision.getId());
            TWComboItem item = new TWComboItem(commision.getTimeStamp() + "", commision.getId());
            //CommissionCombo.addItem(new TWComboItem(commision.getTimeStamp()+"",commision.getId()));
            CommissionCombo.addItem(item);
            CommissionItems.add(item.getValue());

        }
        CommissionCombo.addItem(new TWComboItem(Language.getString("MANUAL"), Language.getString("MANUAL")));
        if (CommissionCombo.getItemCount() > 0) {
            try {
                CommissionCombo.setSelectedIndex(CommissionCombo.getItemCount() - 1);
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

    }

    private double convertToSelectedCurrency(String baseCurr, double value) {
        return value * CurrencyStore.getRate(baseCurr, PFStore.getInstance().getPortfolio(cmbPortfolios.getSelectedIndex()).getCurrency());
    }

    private void displayAvailableCashBalance() {
        PortfolioRecord pfRecord = PFStore.getInstance().getPortfolio(cmbPortfolios.getSelectedIndex());
        lblAvailCashBal.setText(PFUtilities.getNormalizedPrice(pfRecord.getCashBalance()));
        String str = Language.getString("AVAILABLE_CASH_BALANCE");
        if (Language.isLTR()) {
            str = str + " ( " + pfRecord.getCurrency() + " ) ";
        } else {
            str = " ( " + pfRecord.getCurrency() + " ) " + str;
        }
        lblAvailCashBalDesc.setText(str);
        str = null;

        String str2 = Language.getString("TOTAL");
        if (Language.isLTR()) {
            str2 = str2 + " ( " + pfRecord.getCurrency() + " ) ";
        } else {
            str2 = " ( " + pfRecord.getCurrency() + " ) " + str2;
        }
        lblTotalDesc.setText(str2);
        str2 = null;
        pfRecord = null;
    }


    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            if (e.getSource() == cmbPortfolios) {
                displayAvailableCashBalance();
            } else if (e.getSource() == CommissionCombo) {
                calculateCommission();
            } else if (e.getSource() == cmbTxType) {
                if (cmbTxType.getSelectedIndex() == 3) {
                    setComponentStatus(false);
                } else if (cmbTxType.getSelectedIndex() > 3) {
                    setComponentStatus(false);
                    disableSymbolStatus();
                } else {
                    setComponentStatus(true);
                }

                validateAdvaceBuy();
            }
        }
    }

    private void disableSymbolStatus() {
        txtSymbol.setText("");
        lblCompName.setText("");
        txtExchanges.setText("");
        txtSymbol.setEnabled(false);
        btnSearch.setEnabled(false);
        txtQty.setText("");
        lblSymbol.setEnabled(false);
        lblCompName.setEnabled(false);
        lblExchanges.setEnabled(false);
    }


    private void setComponentStatus(boolean status) {
        txtSymbol.setEnabled(true);
        btnSearch.setEnabled(true);
        lblSymbol.setEnabled(true);
        lblCompName.setEnabled(true);
        lblExchanges.setEnabled(true);
        lblQty.setEnabled(status);
        lblCommission.setEnabled(status);
        lblCommissionCombo.setEnabled(status);
        btnGetPrice.setEnabled(status);
        txtQty.setEnabled(status);
        txtCommission.setEnabled(status);
        CommissionCombo.setEnabled(status);
        if (status) {
//            txtQty.setBackground(Color.white);
//            txtCommission.setBackground(Color.white);
            lblSharePrice.setText(Language.getString("SHARE_PRICE"));
        } else {
            txtQty.setBackground(centerPanel.getBackground());
            txtCommission.setBackground(centerPanel.getBackground());
            String str = "";
            String curr = PFStore.getInstance().getPortfolio(cmbPortfolios.getSelectedIndex()).getCurrency();
            if (cmbTxType.getSelectedIndex() == 3) {
                str = Language.getString("CASH_DIVIDENDS");
            } else if (cmbTxType.getSelectedIndex() == 4) {
                str = Language.getString("DEPOSITES");
            } else if (cmbTxType.getSelectedIndex() == 5) {
                str = Language.getString("WITHDROWALS");
            } else if (cmbTxType.getSelectedIndex() == 6) {
                str = Language.getString("CHARGES");
            } else if (cmbTxType.getSelectedIndex() == 7) {
                str = Language.getString("REFUNDS");
            }
            if (Language.isLTR()) {
                str = str + " ( " + curr + " ) ";
            } else {
                str = " ( " + curr + " ) " + str;
            }
            lblSharePrice.setText(str);
            txtCommission.setText("0");
        }
    }

    private void searchMarketPrice() {
        if (sKey != null) {
            setPriceFormat(sKey);
            if (lblFromDate.getText().length() > 0) {
                StringTokenizer st = new StringTokenizer(lblFromDate.getText(), "/", false);
                String sDay = st.nextToken();
                String sMonth = st.nextToken();
                String sYear = st.nextToken();
                if (sDay.length() == 1)
                    sDay = "0" + sDay;
                if (sMonth.length() == 1)
                    sMonth = "0" + sMonth;
                st = null;
//                double value = 0;
                try {
                    String sDate = sYear.trim() + sMonth.trim() + sDay.trim();
                    String str = Language.getString("SHARE_PRICE");
                    String curr = "";
                    if ((SharedMethods.isCurrentDay(SharedMethods.getExchangeFromKey(sKey), sDate)) || (SharedMethods.isFutureDay(SharedMethods.getExchangeFromKey(sKey), sDate))) {
                        double value = 0;
                        if (PFStore.isVWAPMode()) {
                            Stock stock = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(sKey), txtSymbol.getText(), SharedMethods.getInstrumentTypeFromKey(sKey));
                            value = stock.getAvgTradePrice();
                            curr = stock.getCurrencyCode();
                            if (Language.isLTR()) {
                                str = str + " ( " + curr + " ) ";
                            } else {
                                str = " ( " + curr + " ) " + str;
                            }
                            lblSharePrice.setText(str);
                            setPriceFormat(sKey);
                            if (value > 0) {
                                txtPrice.setText(priceFormatter.format((value)));
                            } else {
                                txtPrice.setText(priceFormatter.format((stock.getPreviousClosed())));
                            }
                        } else {
                            Stock stock = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(sKey), txtSymbol.getText(), SharedMethods.getInstrumentTypeFromKey(sKey));
                            value = stock.getLastTradeValue();
                            curr = stock.getCurrencyCode();
                            if (Language.isLTR()) {
                                str = str + " ( " + curr + " ) ";
                            } else {
                                str = " ( " + curr + " ) " + str;
                            }
                            setPriceFormat(sKey);
                            lblSharePrice.setText(str);
                            if (value > 0) {
                                txtPrice.setText(priceFormatter.format((value)));
                            } else {
                                txtPrice.setText(priceFormatter.format((stock.getPreviousClosed())));
                            }
//                            value = DataStore.getSharedInstance().getStockObject(txtExchanges.getText(), txtSymbol.getText()).getLastTradeValue();
                        }
//                        txtPrice.setText("" + (value/(ExchangeStore.getSharedInstance().getExchange(txtExchanges.getText()).getPriceModificationFactor())));
                    } else {
//                        value = PortfolioInterface.getMarketPriceForDay(sKey, sDate);
                        setPriceFormat(sKey);
                        txtPrice.setText(priceFormatter.format((PortfolioInterface.getMarketPriceForDay(sKey, sDate))));
                        curr = PortfolioInterface.getCurrency(PortfolioInterface.getStockObject(sKey));
                        if (Language.isLTR()) {
                            str = str + " ( " + curr + " ) ";
                        } else {
                            str = " ( " + curr + " ) " + str;
                        }
                        lblSharePrice.setText(str);
//                        txtPrice.setText("" + (value/(ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(sKey)).getPriceModificationFactor())));
                    }

                    sDay = null;
                    sMonth = null;
                    sYear = null;
                } catch (Exception ex) {
                    setPriceFormat(sKey);
                    txtPrice.setText(PFUtilities.getNormalizedPrice(0)); // "0.00");
                }
                calculateTotal();
            } else {
                new ShowMessage(Language.getString("MSG_TX_DATE_CANNOT_BE_BLANK"), "I");
            }
        } else {
            new ShowMessage(Language.getString("MSG_SYMBOL_CANNOT_BE_BLANK"), "I");
        }
    }

    public void setMemoDescription(String text) {
        lblMemoVoid.setText(text);
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource() == lblMemoVoid) {
            showMemo();
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mouseDragged(MouseEvent e) {
    }

    private void unregisterSymbol() {
        try {
            /*if (lastEnteredSymbol != null) {
                System.out.println("Removing Symbol " + lastEnteredSymbol);
                if (!PFStore.isContainedKey(lastEnteredSymbol))
                    PortfolioInterface.removeSymbol(lastEnteredSymbol);
            }*/
            for (int i = 0; i < requestRegister.size(); i++) {
                System.out.println("removing request " + (String) requestRegister.get(i));
                PortfolioInterface.removeSymbol((String) requestRegister.get(i));
            }
            requestRegister.clear();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void keyTyped(KeyEvent e) {
        if (e.getSource() == txtSymbol)
            isTextChanged = true;
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        //loadExchanges();
    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void setPriceFormat(String skeyy) {

        priceFormatter = SharedMethods.getDecimalFormatNoComma(skeyy);


    }

    private void validateAdvaceBuy() {

        boolean isTxnBuyType = false;
        boolean isStockInPf = false;

        isTxnBuyType = (cmbTxType.getSelectedIndex() == 1);
        if (isTxnBuyType) {
            isStockInPf = loadAdvanceSellTableModel();

        }

        if (isTxnBuyType && isStockInPf) {
            btnAdvTraction.setEnabled(true);


        } else {
            btnAdvTraction.setEnabled(false);
            showAdvSell = false;
            defineComponentBounds();
            this.pack();
        }


    }

    private boolean loadAdvanceSellTableModel() {
        if (sKey == null || sKey.isEmpty()) {
            return false;
        }

        if (advSellModel == null) {
            advSellModel = new AdvaneSellTableModel();
        }
        if (symbolSpecificBuys == null) {
            symbolSpecificBuys = new ArrayList();
        }

        ArrayList transactionRecords = PFStore.getInstance().getFilteredTransactionList();
        // ArrayList symbolSpecific = new ArrayList();
        TransactRecord trRec = null;
        symbolSpecificBuys.clear();

        for (int i = 0; i < transactionRecords.size(); i++) {

            trRec = (TransactRecord) transactionRecords.get(i);
            if (trRec.getSKey() != null && trRec.getSKey().equals(sKey) && (trRec.getTxnType() == PFUtilities.BUY || trRec.getTxnType() == PFUtilities.OP_BAL)) {
                if (isEditMode) {

                    if (trRec.getRemainingHolding() > 0) {
                        symbolSpecificBuys.add(trRec);
                    } else {

                        Integer val = trRec.getSpecificSellQty(record.getId());
                        if (val != null) {

                            symbolSpecificBuys.add(trRec);
                        }


                    }


                } else {
                    if (trRec.getRemainingHolding() > 0) {
                        symbolSpecificBuys.add(trRec);
                    }
                }

            }


        }

        if (!symbolSpecificBuys.isEmpty()) {

            advSellModel.setDataStore(symbolSpecificBuys);
            advSellModel.setEditMode(isEditMode);
            if (isEditMode) {
                advSellModel.setEditingTrId(record.getId());
                setSelltempValsInTrnsForEdit();
            }
            return true;
        } else {
            return false;
        }

    }


    private void createAdvanceSellTable() {

        ViewSetting oSettings;
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings = (ViewSetting) Client.getInstance().getViewSettingsManager().getPortfolioView(1280, "6");
            if (oSettings == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        //String sColOrder = Settings.getItem("Gio");
        GUISettings.setColumnSettings(oSettings, PortfolioInterface.getColumnSetting("PORTFOLIO_ADVANCED_TRAN_COLUMNS"));

        //  advanceSellTable = new Table(false);
        oSettings.setTableNumber(1);
        //   advSellModel = new AdvaneSellTableModel();
//        advSellModel.setEditMode(isEditMode);
//        if(isEditMode){
//            advSellModel.setEditingTrId(record.getId());
//            setSelltempValsInTrnsForEdit();
//        }
        advSellModel.setViewSettings(oSettings);

        advanceSellTable.setModel(advSellModel);
        advSellModel.setAdvTradingPortfolioTable(advanceSellTable);

        //  advSellPanel.add(advanceSellTable);
        advanceSellTable.getTable().getTableHeader().setReorderingAllowed(false);
        advanceSellTable.getTable().setFillsViewportHeight(true);
        advanceSellTable.setHeaderPopupActive(false);
        TableColumn column = advanceSellTable.getTable().getColumnModel().getColumn(3);
        TWTextField celltext = new TWTextField();
        celltext.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));
        celltext.setHorizontalAlignment(JLabel.RIGHT);


        column.setCellEditor(new DefaultCellEditor(celltext));


    }

    public void setSelltempValsInTrnsForEdit() {

        for (int i = 0; i < symbolSpecificBuys.size(); i++) {

            TransactRecord tr = (TransactRecord) symbolSpecificBuys.get(i);

            Integer val = tr.getSpecificSellQty(record.getId());
            if (val != null) {

                tr.setSellTempVal(val);

            }


        }


    }

    private void advBuyClicked() {


        try {
            if (showAdvSell) {
                loadAdvanceSellTableModel();
            }
                defineComponentBounds();
                advanceSellTable.getTable().updateUI();
                this.repaint();
                this.pack();



        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public void setSellValuationMode(boolean isFIFO) {
        this.isFIFO = isFIFO;
        if (!isFIFO) {
            btnAdvTraction.setVisible(false);
            showAdvSell = false;
            defineComponentBounds();
        } else {
            btnAdvTraction.setVisible(true);
            defineComponentBounds();
            this.pack();

        }

    }

    public void resetAndClearFIFOArrayList() {

        if (symbolSpecificBuys != null && !symbolSpecificBuys.isEmpty()) {
            TransactRecord tr = null;

            for (int i = 0; i < symbolSpecificBuys.size(); i++) {

                tr = (TransactRecord) symbolSpecificBuys.get(i);
                tr.setSellTempVal(0);
            }
            symbolSpecificBuys.clear();
        }


    }

    private ArrayList validateExecuteFIFOSell(int totQty) {

        int fifoQty = 0;
        ArrayList selectedBuys = new ArrayList();
        if (symbolSpecificBuys != null && !symbolSpecificBuys.isEmpty()) {
            int entries = symbolSpecificBuys.size();
            for (int i = 0; i < entries; i++) {

                TransactRecord tr = (TransactRecord) symbolSpecificBuys.get(i);
                if (tr.getSellTempVal() > 0) {
                    selectedBuys.add(tr);
                } else if (isEditMode) {
                    Integer val = tr.getSpecificSellQty(record.getId());
                    if (val != null) {

                        selectedBuys.add(tr);

                    }


                }
                fifoQty = fifoQty + tr.getSellTempVal();

            }

            if (fifoQty == totQty) {

                return selectedBuys;


            } else {
                return null;
            }


        } else {
            return null;
        }


    }


    public boolean finalizeWindow() {

        resetAndClearFIFOArrayList();
        this.exitFromLoop();
        showAdvSell = false;
        if (parent != null) {
            parent.addTxnWindowClosed();
        }

        return super.finalizeWindow();


    }

    public void clearSymbolspecificBuys() {

        resetAndClearFIFOArrayList();
        if (isEditMode && isFIFO) {
            showAdvSell = false;
            defineComponentBounds();

        }
        this.pack();

    }


    private void ExecuteHiddenFIFOSellInWAMode(int qty, long trID) {

        if (symbolSpecificBuys != null && !symbolSpecificBuys.isEmpty()) {

            int size = symbolSpecificBuys.size();
            int i = 0;
            while (qty > 0) {

                TransactRecord tr = (TransactRecord) symbolSpecificBuys.get(i);
                int availableqty = tr.getRemainingHolding();

                if (availableqty >= qty) {

                    tr.sellQuantity(trID, qty);
                    record.putBuyQty(tr.getId(), qty);
                    qty = 0;
                    break;
                } else if (availableqty > 0 && availableqty < qty) {

                    tr.sellQuantity(trID, availableqty);
                    record.putBuyQty(tr.getId(), qty);
                    qty = qty - availableqty;
                    i = i + 1;

                } else {

                    i = i + 1;
                }

                if (i == size) {
                    break;
                }
            }
        }
    }


    private void ExecuteHiddenFIFOSellEditInWAMode(int qty, long trID) {

        if (symbolSpecificBuys != null && !symbolSpecificBuys.isEmpty()) {

            int change = qty - oldQty;
            if (change > 0) {

                ExecuteHiddenFIFOSellInWAMode(change, trID);

            } else {

                UndoFIFOSell(trID);
                ExecuteHiddenFIFOSellInWAMode(qty, trID);


            }
        }
    }

    public void UndoFIFOSell(long trId) {

        if (symbolSpecificBuys != null && !symbolSpecificBuys.isEmpty()) {

            int size = symbolSpecificBuys.size();
            int tempOldqty = oldQty;

            for (int i = 0; i < size; i++) {

                TransactRecord tr = (TransactRecord) symbolSpecificBuys.get(i);
                Integer val = tr.getSpecificSellQty(trId);

                if (val != null) {

                    tr.deleteSellQuantity(trId);


                }


            }


        }

//        if(record.getTxnType()== PFUtilities.SELL){
//
//         ArrayList transactionRecords = PFStore.getInstance().getFilteredTransactionList();
//            ArrayList tempsymbolSpecificBuys = new ArrayList();
//       // ArrayList symbolSpecific = new ArrayList();
//        TransactRecord trRec= null;
//        symbolSpecificBuys.clear();
//         String tempKey = record.getSKey();
//         long id = record.getId();
//
//        for(int i=0; i<transactionRecords.size(); i++){
//
//            trRec = (TransactRecord) transactionRecords.get(i);
//             if(trRec.getSKey()!= null && trRec.getSKey().equals(tempKey) && trRec.getTxnType() == PFUtilities.BUY ){
//
//
//                  if(trRec.getRemainingHolding()>0) {
//                        symbolSpecificBuys.add(trRec);
//                    }else{
//
//                          Integer val = trRec.getSpecificSellQty(record.getId());
//                          if(val != null){
//
//                              symbolSpecificBuys.add(trRec);
//                          }
//
//
//                      }
//
//
//             }
//
//
//
//
//
//        }
//
//
//        }

    }

}
