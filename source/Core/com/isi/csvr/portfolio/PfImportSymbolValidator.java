package com.isi.csvr.portfolio;

import com.isi.csvr.SymbolMaster;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.shared.SharedMethods;

import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 11, 2008
 * Time: 8:03:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class PfImportSymbolValidator implements Runnable, PFImportDialogInterface {

   private Hashtable<Long,StringTableRowObj> temprowObjects ;
    private ArrayList<PriliminaryTxnObject> neumericValidated;
    private ArrayList<PriliminaryTxnObject> symbolValidated;
     private Hashtable<String,ArrayList> validationList;
    private int replyCount=0;
    private int expectedCount=0;


    public PfImportSymbolValidator(Hashtable<Long, StringTableRowObj> temprowObjects, ArrayList<PriliminaryTxnObject> neumericValidated, ArrayList<PriliminaryTxnObject> symbolValidated) {
        this.temprowObjects = temprowObjects;
        this.neumericValidated = neumericValidated;
        this.symbolValidated = symbolValidated;
        validationList = new Hashtable<String, ArrayList>();
    }


    public void run() {

        validateSymbols();
        long count = System.currentTimeMillis();
        boolean timeout = false;
        while((expectedCount>replyCount) &&(!timeout)){
            try {
                Thread.sleep(3000);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

           if(System.currentTimeMillis() > count+(1000*120) ){
               timeout = true;
               System.out.println("PF Import Symbol Validator Timed Out");
           }
        }

        PFImportDataStore.getSharedInstance().setNeumaricVal(false);
        PFImportDataStore.getSharedInstance().setCompletedPhase(PFImportDataStore.VALIDATION_SYMBOL_COMPLETE);


    }




    private void validateSymbols(){

        invalidateAll();
        loadValidationList();
        validateAllMktSymbols();


    }

    private void invalidateAll(){
        Enumeration en = temprowObjects.keys();
        while(en.hasMoreElements()){
            Long id = (Long)en.nextElement();

            StringTableRowObj row = temprowObjects.get(id);
            row.setValidStatus(StringTableRowObj.INVALID_SYMBOL);
        }
    }
    private void validateAllMktSymbols(){

        Enumeration en = validationList.keys();
        long timestamp = System.currentTimeMillis();
        while (en.hasMoreElements()){
            String cmpkey = (String)en.nextElement();
            String key =cmpkey.split("~")[1];
            String exchange =cmpkey.split("~")[0];
            PFImportDataStore.validateSymbolNoExchange(key,"PF_IMPRT_DLG",exchange,this, timestamp);
            timestamp = timestamp +1 ;
//            String symbol = SymbolMaster.getExchangeForSymbol(key, false);
//            if(symbol != null){
//
//                ArrayList list = validationList.get(cmpkey);
//                validateTempRowObjects(list);
//            }

        }


    }

    private void validateTempRowObjects(ArrayList list, int instrument, String key ){

        if(!list.isEmpty()){
            for(int j=0; j<list.size(); j++ ){

                Long id = (Long)list.get(j);
                temprowObjects.get(id).setValidStatus(StringTableRowObj.VALID);
                PriliminaryTxnObject txn= getObjectFromneumericValidated(id);
                txn.setInstrumentType(instrument);
                txn.setSKey(key);
                if(txn != null){
                    symbolValidated.add(txn);           // the original order is changed
                }
            }
        }
    }


    private PriliminaryTxnObject getObjectFromneumericValidated(Long id){
          for (PriliminaryTxnObject txn : neumericValidated) {
              if(txn.getID() == id){
                  return txn;
              }

          }
          return null;
    }

    private void loadValidationList(){
        for(int i=0; i<neumericValidated.size(); i++){
             PriliminaryTxnObject txn =neumericValidated.get(i);
            String key = txn.getExchange()+"~"+txn.getSymbol();
            Exchange ex = ExchangeStore.getSharedInstance().getExchange(txn.getExchange());
            if(ex!= null && ex.isUserSubMarketBreakdown()&& ex.hasSubMarkets() ) {

                //key = key +"`R";
            }
            if(!validationList.containsKey(key)){
                ArrayList list= new ArrayList();
                list.add(txn.getID());
                validationList.put(key,list);
            }else{
                ((ArrayList) validationList.get(key)).add(txn.getID());
            }
        }
        expectedCount = validationList.size();
    }


    public void setSymbol(String key, int instrument) {
             if (key != null){
                 String symbolWOsub="";
                String exchange = SharedMethods.getExchangeFromKey(key);
                String symbol = SharedMethods.getSymbolFromKey(key);
                String cumkey = exchange+"~"+symbol;
                 Exchange ex  = ExchangeStore.getSharedInstance().getExchange(exchange);
                if(ex!= null && ex.isUserSubMarketBreakdown()&& ex.hasSubMarkets() ) {
                    if(symbol.contains("`")){
                        symbolWOsub = symbol.split("`")[0];
                    }else{
                        symbolWOsub = symbol;
                    }

                }

               String cumKeyWOsub =exchange+"~"+symbolWOsub;
                if(validationList.containsKey(cumkey)){
                   ArrayList list = validationList.get(cumkey);
                   validateTempRowObjects(list,instrument,key);

                }else if(validationList.containsKey(cumKeyWOsub)){
                    String keyWOsub = SharedMethods.getKey(exchange,symbolWOsub,instrument);
                     ArrayList list = validationList.get(cumKeyWOsub);
                      validateTempRowObjects(list,instrument,key);
                }
                 replyCount +=1;


            }
        }

    public void setData(Object data) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void notifyInvalidSymbol(String symbol) {
        replyCount+=1;
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
