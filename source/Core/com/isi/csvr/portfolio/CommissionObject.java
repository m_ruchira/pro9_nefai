package com.isi.csvr.portfolio;

import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Sep 1, 2008
 * Time: 11:09:13 AM
 * To change this template use File | Settings | File Templates.
 */
public class CommissionObject {
    private String id;
    private double minPrice;
    private double percentage;
    private long timeStamp;

    public CommissionObject() {
    }

    public CommissionObject(String id) {
        this.id = id;
        timeStamp = System.currentTimeMillis();
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public double getMinPrice() {
        return minPrice;
    }

    public void setMinPrice(double minPrice) {
        this.minPrice = minPrice;
    }

    public double getPercentage() {
        return percentage;
    }

    public void setPercentage(double percentage) {
        this.percentage = percentage;
    }

    public long getTimeStamp() {
        return timeStamp;
    }

     public void retrieveCommission(String sData){
        try {
            StringTokenizer st = new StringTokenizer(sData, "|", false);
            id = st.nextToken();
            minPrice = Double.parseDouble(st.nextToken());
            percentage = Double.parseDouble(st.nextToken());
            timeStamp = Long.parseLong(st.nextToken());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String toString(byte fileType){
        String delim = "";
        switch (fileType) {
            case PFUtilities.DATA :
                delim = "|";
                break;
            case PFUtilities.TXT :
                delim = "\t";
                break;
            case PFUtilities.CSV :
                delim = ",";
                break;
            case PFUtilities.QIF :
                delim = "|";
                break;
        }
        return PFUtilities.COMMISSION_RECORD + delim + id + delim + minPrice + delim + percentage + delim+ timeStamp+ "\n";
    }
}
