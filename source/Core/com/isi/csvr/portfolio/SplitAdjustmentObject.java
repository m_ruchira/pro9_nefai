package com.isi.csvr.portfolio;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Aug 26, 2008
 * Time: 2:37:04 PM
 * To change this template use File | Settings | File Templates.
 */
public class SplitAdjustmentObject {
    private long portfolioID;
    private String portfolioName;
    private long transactID;
    private long SplitID;
    private double oldPrice;
    private long oldQty;
    private double newPrice;
    private long newQty;

    public SplitAdjustmentObject(long transactID) {
        this.transactID = transactID;
    }

    public long getTransactID() {
        return transactID;
    }

    public void setTransactID(long transactID) {
        this.transactID = transactID;
    }

    public long getSplitID() {
        return SplitID;
    }

    public void setSplitID(long splitID) {
        SplitID = splitID;
    }

    public double getOldPrice() {
        return oldPrice;
    }

    public void setOldPrice(double oldPrice) {
        this.oldPrice = oldPrice;
    }

    public long getOldQty() {
        return oldQty;
    }

    public void setOldQty(long oldQty) {
        this.oldQty = oldQty;
    }

    public double getNewPrice() {
        return newPrice;
    }

    public void setNewPrice(double newPrice) {
        this.newPrice = newPrice;
    }

    public long getNewQty() {
        return newQty;
    }

    public void setNewQty(long newQty) {
        this.newQty = newQty;
    }

    public long getPortfolioID() {
        return portfolioID;
    }

    public void setPortfolioID(long portfolioID) {
        this.portfolioID = portfolioID;
        portfolioName = PFStore.getInstance().getPortfolioName(portfolioID);
    }

    public String getPortfolioName() {
        return portfolioName;
    }
}
