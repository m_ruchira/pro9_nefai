package com.isi.csvr.portfolio;

import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.print.attribute.PrintRequestAttribute;
import javax.print.attribute.HashPrintRequestAttributeSet;
import javax.print.attribute.PrintRequestAttributeSet;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.print.PrinterJob;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public final class PFReportFrame extends JFrame implements ActionListener, Themeable {

    private static PFReportFrame pR = null;

    JPanel toolPanel;
    JPanel backPanel;
    private static ReportPanel reportPanel = null; // = new ReportPanel(pfWindow);
    JScrollPane reportArea;
    TWButton btnClose;
    TWButton btnPrint;

    private PFReportFrame(PortfolioWindow pfWindow) {

        btnClose = new TWButton(Language.getString("CLOSE"));
        btnPrint = new TWButton(Language.getString("PRINT"));
        toolPanel = new JPanel();
        backPanel = new JPanel(new FlowLayout(FlowLayout.LEADING));
        reportArea = new JScrollPane(backPanel);

        reportPanel = new ReportPanel(pfWindow);
        this.getContentPane().setLayout(new BorderLayout(0, 0));
        toolPanel.setLayout(new FlowLayout(FlowLayout.LEADING, 2, 2));
        toolPanel.setPreferredSize(new Dimension(48, 24));
        Dimension dimRep = Toolkit.getDefaultToolkit().getScreenSize();
        reportPanel.setPreferredSize(new Dimension(dimRep.width, dimRep.height));
    //    reportPanel.setMinimumSize(new Dimension(reportPanel.PREFERRED_WIDTH, dimRep.height));
//												   (int)Math.round(Settings.getPageFormat().getImageableWidth()),
//												   (int)Math.round(Settings.getPageFormat().getImageableHeight())));
        btnClose.setPreferredSize(new Dimension(100, 20));
        btnPrint.setPreferredSize(new Dimension(100, 20));
        btnClose.addActionListener(this);
        btnPrint.addActionListener(this);
        this.getContentPane().add(toolPanel, BorderLayout.NORTH);
        this.getContentPane().add(reportArea, BorderLayout.CENTER);
        backPanel.add(reportPanel);
        reportPanel.setBackground(Color.WHITE);
        backPanel.setBackground(Color.WHITE);
        toolPanel.add(btnClose);
        toolPanel.add(btnPrint);
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
        GUISettings.applyOrientation(this);
    }

    public static synchronized PFReportFrame showReport(PortfolioWindow pfWindow) {
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.PortfolioSimulator, "Report");
        if (pR == null) {
            pR = new PFReportFrame(pfWindow);
            Theme.registerComponent(pR);
        }
        pR.setSize(Toolkit.getDefaultToolkit().getScreenSize());
        pR.setTitle(Language.getString("PORTFOLIO_REPORT_TITLE"));
        reportPanel.updateReport();

       // pR.setVisible(true);
        if (pR.getExtendedState() == JFrame.ICONIFIED){
            pR.setExtendedState(JFrame.NORMAL);
        }
        reportPanel.repaint();
     //   reportPanel.updateUI();
        pR.setVisible(true);
        //reportPanel.updateUI();
       
        return pR;
    }

    public Object clone() throws CloneNotSupportedException {
        if (true) {
            throw new CloneNotSupportedException();
        }
        return null;
    }

    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj == btnClose) {
            this.setVisible(false);
        } else if (obj == btnPrint) {
            printPanel();
        }
    }

    public void printPanel() {
        PrinterJob job = PrinterJob.getPrinterJob();
      // job.getPageFormat()
        PrintRequestAttributeSet printAttributes = new HashPrintRequestAttributeSet();
     //   job.setPrintable(reportPanel, Settings.getPageFormat());
        if (job.printDialog(printAttributes)) {
             job.getPageFormat(printAttributes) ;
            job.setPrintable(reportPanel, job.getPageFormat(printAttributes));
            try {
                job.print();
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }

    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(getContentPane());
        SwingUtilities.updateComponentTreeUI(reportPanel);
        btnClose.updateUI();
        btnPrint.updateUI();
        reportArea.updateUI();
        toolPanel.updateUI();
    }

}