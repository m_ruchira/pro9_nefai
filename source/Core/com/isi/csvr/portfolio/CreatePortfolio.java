package com.isi.csvr.portfolio;

import com.isi.csvr.ShowMessage;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.shared.*;
import com.isi.util.FlexNullLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.Enumeration;
import java.util.ArrayList;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class CreatePortfolio extends JDialog {

    private JPanel centerPanel = new JPanel();
    private JPanel southPanel = new JPanel();
    private JLabel lblPFName = new JLabel();
    private JLabel lblCurrency = new JLabel();
    private JLabel lblCashBalance = new JLabel();
    private JLabel lblCommission = new JLabel();
    private JTextField txtPFName = new JTextField();
    private JTextField txtOpeningBalance = new JTextField();
    private JComboBox cmbCurrency = new JComboBox();
   private JComboBox CommissionCombo = new JComboBox();
    private TWButton btnCreate = new TWButton();
    private TWButton btnCancel = new TWButton();
    private JCheckBox chkIsDefault = new JCheckBox();

    private int buttonPanelHeight = 30;
    private int width;
    private int height;
    private PortfolioRecord record;
    private boolean isEditMode;
    private PortfolioWindow parent;

    public CreatePortfolio(Component ref, PortfolioWindow invoker) {
        super((Window)SwingUtilities.getAncestorOfClass(Window.class,ref), "", Dialog.ModalityType.APPLICATION_MODAL);
        try {
            this.parent = invoker;
            jbInit();
//            pack();
            GUISettings.applyOrientation(this);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

//    public CreatePortfolio() {
//        this(null, "", false, null);
//    }

    private void jbInit() throws Exception {
        getContentPane().setLayout(new BorderLayout());
        centerPanel.setLayout(new FlexNullLayout()); // new GridLayout(3, 2, 2, 4));
        southPanel.setLayout(new FlowLayout(1, 2, 4));

        txtOpeningBalance.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        setComponentLabels();
        defineComponentBounds();
        loadCommisionList();
        txtPFName.setDocument(new LimitedLengthDocument(20));

        centerPanel.add(lblPFName);
        centerPanel.add(txtPFName);
        centerPanel.add(lblCurrency);
        centerPanel.add(cmbCurrency);
        centerPanel.add(lblCashBalance);
        centerPanel.add(txtOpeningBalance);
        centerPanel.add(lblCommission);
        centerPanel.add(CommissionCombo);
        centerPanel.add(chkIsDefault);


        btnCreate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (createPortfolio())
                    closeDialog();
            }
        });

        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closeDialog();
            }
        });

        southPanel.add(btnCreate);
        southPanel.add(btnCancel);

        getContentPane().add(centerPanel, BorderLayout.CENTER);
        getContentPane().add(southPanel, BorderLayout.SOUTH);
        populateCombos();
        if (PFStore.getInstance().getPortfolioList().size() == 0) {
            chkIsDefault.setSelected(true);
        }
    }

    private void setComponentLabels() {
        String cashDescr = Language.getString("MSG_ENTER_CASH_BALANCE");
        lblPFName.setText(Language.getString("PORTFOLIO_NAME"));
        lblCurrency.setText(Language.getString("CURRENCY"));
        lblCommission.setText(Language.getString("COMMISSION_RATE"));
        lblCashBalance.setText(cashDescr);
        lblCashBalance.setHorizontalAlignment(JTextField.LEADING);

        btnCreate.setText(Language.getString("CREATE"));
        btnCancel.setText(Language.getString("CANCEL"));

        chkIsDefault.setText(Language.getString("SET_AS_DEFAULT_PORTFOLIO"));

        this.setTitle(Language.getString("CREATE_PORTFOLIO"));
    }

    private void defineComponentBounds() {
        int lblWidth1 = 120;
        int lblWidth2 = 150;
//        int lblWidth3 = 110;
        int xPos1 = 10;
        int xPos2 = xPos1 + lblWidth1 + 5;
//        int xPos3 = xPos2 + lblWidth2 + 5;
        int fieldHeight = 20;
        int curYPos = 10;

        lblPFName.setBounds(new Rectangle(xPos1 + 5, curYPos, lblWidth1, fieldHeight));
        txtPFName.setBounds(new Rectangle(xPos2 + 5, curYPos, lblWidth2, fieldHeight));
        curYPos += fieldHeight + 5;

        lblCurrency.setBounds(new Rectangle(xPos1 + 5, curYPos, lblWidth1, fieldHeight));
        cmbCurrency.setBounds(new Rectangle(xPos2 + 5, curYPos, lblWidth1, fieldHeight));
        curYPos += fieldHeight + 5;

        lblCashBalance.setBounds(new Rectangle(xPos1 + 5, curYPos, lblWidth1, fieldHeight + 50));
        txtOpeningBalance.setBounds(new Rectangle(xPos2 + 5, curYPos + 15, lblWidth2, fieldHeight));
        curYPos += fieldHeight + 45;

        lblCommission.setBounds(new Rectangle(xPos1 + 5, curYPos, lblWidth1, fieldHeight));
        CommissionCombo.setBounds(new Rectangle(xPos2 + 5, curYPos, lblWidth2, fieldHeight));
        curYPos += fieldHeight + 10;

        chkIsDefault.setBounds(new Rectangle(xPos1 + 5, curYPos, 300, fieldHeight));
//        txtOpeningBalance.setBounds(new Rectangle(xPos2+5, curYPos+15, lblWidth2, fieldHeight));
        curYPos += fieldHeight + 5;

        width = xPos2 + lblWidth2 + 20;
        centerPanel.setBounds(new Rectangle(5, xPos1, width - 5, curYPos));
        southPanel.setPreferredSize(new Dimension(width, buttonPanelHeight));
        height += curYPos + buttonPanelHeight * 2;

        btnCreate.setPreferredSize(new Dimension(80, 20));
        btnCancel.setPreferredSize(new Dimension(80, 20));
    }

    private void populateCombos() {
        String sItem = null;
        Enumeration currencyIDs = CurrencyStore.getSharedInstance().getCurrencyIDs();
        while (currencyIDs.hasMoreElements()) {
            sItem = (String) currencyIDs.nextElement();
            cmbCurrency.addItem(sItem);
            sItem = null;

        }
        currencyIDs = null;
        try {
            cmbCurrency.setSelectedItem(PFStore.getInstance().getBaseCurrency());
        } catch (Exception ex) {
        }
    }

    public void showDialog() {
        this.setSize(new Dimension(width, height));
        this.setResizable(false);
        centerUI();
        this.setVisible(true);
    }

    private void centerUI() {
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        this.setBounds((int) (oDm.getWidth() / 2 - (this.getWidth() / 2)),
                (int) (oDm.getHeight() / 2 - (this.getHeight() / 2)),
                this.getWidth(), this.getHeight());
    }

    private void closeDialog() {
//        this.exitFromLoop();
        this.dispose();
    }

    public void setRecord(PortfolioRecord record) {
        try {
            TWDecimalFormat format = new TWDecimalFormat("###.###");

            this.record = record;
            cmbCurrency.setEnabled(false);
            try {
                cmbCurrency.setSelectedItem(record.getCurrency());
            } catch (Exception ex) {
            }
//            cmbPortfolios.setSelectedItem(PFStore.getInstance().getPortfolio(record.getPfID()).getName());
            txtPFName.setText(record.getName());

            txtOpeningBalance.setText(format.format(record.getCashBalance()));
            isEditMode = true;
            btnCreate.setText(Language.getString("UPDATE"));
            chkIsDefault.setSelected(record.isDefault());
            this.setTitle(Language.getString("EDIT_PORTFOLIO"));
            txtOpeningBalance.setEnabled(false);
            CommissionCombo.setSelectedItem(record.getCommisionName());
        } catch (Exception ex) {
            isEditMode = false;
        }
    }

    private boolean createPortfolio() {
//        PortfolioRecord record = null;
        String pfName = txtPFName.getText();
        double cashBalance = 0;

        if (pfName.trim().equals("")) {
            new ShowMessage(Language.getString("MSG_NAME_CANNOT_BE_BLANK"), "I");
            return false;
        }
        if (isEditMode && (record != null) && (!record.getName().equals(pfName))) {
            if (PFStore.getInstance().isPortfolioNameExists(pfName)) {
                new ShowMessage(Language.getString("MSG_NAME_ALREADY_EXISTS"), "I");
                return false;
            }
        }
        if (!isEditMode) {
            if (PFStore.getInstance().isPortfolioNameExists(pfName)) {
                new ShowMessage(Language.getString("MSG_NAME_ALREADY_EXISTS"), "I");
                return false;
            }
        }
        try {
            cashBalance = Double.parseDouble(txtOpeningBalance.getText());
        } catch (NumberFormatException ex) {
            cashBalance = 0;
        }
        if (!isEditMode && cashBalance < 0) {
            new ShowMessage(Language.getString("PF_MSG_CASH_CANNOT_BE_NEGATIVE"), "I");
            return false;
        }
        if (!isEditMode && txtOpeningBalance.getText().trim().equals("")) {
            new ShowMessage(Language.getString("ENTER_VALID_CASH_BALANCE_FOR_THE_PORTFOLIO"), "I");
            return false;
        }
        if (!isEditMode || (record == null)) {
            record = new PortfolioRecord();
        }
//            record = new PortfolioRecord();
        record.setName(pfName);
        record.setCashBalance(cashBalance);

        record.setCommisionName((String)CommissionCombo.getSelectedItem());

        if (!isEditMode) {
            record.setCurrency((String) cmbCurrency.getSelectedItem());
            PFStore.getInstance().addPortfolio(record);
//            } else {
//                if (cashBalance > record.getCashBalance())
//                    record.setCashBalance(cashBalance);
//                else if ((record.getCashBalance() - cashBalance) < 0)
        }
//            PortfolioWindow.setCashBalance(record.getCurrency(), cashBalance);

        if ((PFStore.getInstance().getPortfolioList().size() == 0) || chkIsDefault.isSelected()) {
            PFStore.getInstance().setDefaultPortfolio(record.getPfID());
        }
        isEditMode = false;
        parent.loadNewlyAddedPortfolio(record.getPfID(), record);
        record = null;
        System.out.println("Portfolio Created, " + pfName);
        return true;
    }

    private void loadCommisionList(){
        CommissionCombo.removeAllItems();
        CommissionObject commision = null;
        ArrayList<String> itemlist= PFStore.getInstance().getCommissionList();
        for(int i=0; i<itemlist.size(); i++){
            commision = PFStore.getInstance().getCommission(itemlist.get(i));
            CommissionCombo.addItem(commision.getId());

        }
        CommissionCombo.addItem(Language.getString("MANUAL"));
        if(CommissionCombo.getItemCount()>0){
            try {
                CommissionCombo.setSelectedIndex(CommissionCombo.getItemCount()-1 );
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

    }
}