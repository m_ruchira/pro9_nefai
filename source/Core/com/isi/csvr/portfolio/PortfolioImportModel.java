package com.isi.csvr.portfolio;

import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 9, 2008
 * Time: 9:00:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class PortfolioImportModel  extends CommonTable
        implements TableModel, CommonTableInterface {

    private Hashtable<Integer, StringTableRowObj> datastore;
    private boolean reloadedModel = false;

    public PortfolioImportModel() {
    }

    public void setDatastore(Hashtable datastore) {
        this.datastore = datastore;
    }

    public void setReloadedModel(boolean reloadedModel) {
        this.reloadedModel = reloadedModel;
    }

    public int getRowCount() {
        if(datastore!= null){
            return datastore.size();
        }else{
            return 0;
        }
          //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        if(columnIndex==0 && !reloadedModel){
            return rowIndex+1+"";
        }
        if(rowIndex < getRowCount() && columnIndex>=0){
            StringTableRowObj rowobj = datastore.get(rowIndex);
            if(reloadedModel){
              return (rowobj.getCellValue(columnIndex)).toString();  
            }else{
            return  (rowobj.getCellValue(columnIndex-1)).toString();
            }
           // return "values";
        }else if(columnIndex==-1){
            StringTableRowObj rowobj = datastore.get(rowIndex);
            Boolean b = rowobj.getValidStatus()==0;
            return b;

        }else if(columnIndex==-2){
            StringTableRowObj rowobj = datastore.get(rowIndex);
            return rowobj.getStatusMessage();

        }else if(columnIndex==-3){
             StringTableRowObj rowobj = datastore.get(rowIndex);
            if(reloadedModel){
            return rowobj.getInvalidColNumber();
            }else{
               return rowobj.getInvalidColNumber()+1;  
            }

        }else if(columnIndex ==-4){
            return  datastore.get(rowIndex);
        }
        else{
            return "";
        }

//        try {
//            StringTableRowObj rowobj = datastore.get(rowIndex);
//           // return  (rowobj.getCellValue(columnIndex)).toString();
//            return  "values";
//        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            return "";
//        }


    }

    public void setSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        /*try {
            return getValueAt(0, iCol).getClass();
                 } catch (Exception e) {
            return Object.class;
                 }*/


                return String.class;

    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }
}
