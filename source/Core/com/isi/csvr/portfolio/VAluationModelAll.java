package com.isi.csvr.portfolio;

import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;

import javax.swing.table.TableModel;
import javax.swing.event.TableModelListener;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Aug 27, 2008
 * Time: 8:44:55 AM
 * To change this template use File | Settings | File Templates.
 */
public class VAluationModelAll   extends CommonTable implements TableModel, CommonTableInterface {

    private PFStore pfStore;
    private ArrayList dataStore;
    private PortfolioWindow pfWindow;
    private String sCash;
    public boolean isCashIncluded = false;


       public VAluationModelAll(ArrayList dataStore) {
        this.dataStore = dataStore;
        pfStore = PFStore.getInstance();
        sCash = Language.getString("CASH");
    }



    public void setSymbol(String symbol) {

    }

     public void includeCashBalance(boolean status) {
        isCashIncluded = status;
    }

    public void setPFWindow(PortfolioWindow pfWindow) {
        this.pfWindow = pfWindow;
    }

    /* --- Table Model metods start from here --- */
    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        if(dataStore!= null){
            return dataStore.size() ;
        }else{
            return 0;
        }

    }

    public Object getValueAt(int row, int col) {

       
        try {
             ValuationRecordAll record = (ValuationRecordAll) dataStore.get(row);
            if(col ==-3) {
               return  (Integer) record.getRecordType();
            }
            if (record.getRecordType() ==ValuationRecordAll.VALUATIONREC_TYPE) { // only transaction rows

                if (record == null) {
                    return "";
                }

                Stock stock = PortfolioInterface.getStockObject(record.getSKey());
                if (stock == null) {
                    switch (col) {
                        case -1:     // KEY
                            return record.getSKey();
                        case 0:     // SYMBOL
                            return SharedMethods.getSymbolFromKey(record.getSKey());
                        case 1:     // EXCHANGE CODE
                            return ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(record.getSKey()).trim()).getDisplayExchange();
                        default:
                            return "";
                    }
                }
                String exchange = stock.getExchange();

                switch (col) {
                    case -4:
                        return stock.getDecimalCount();
                    case -13:
                        return stock.isSymbolEnabled();
                    case -1:
                        return record.getSKey();
                    case 0:     // SYMBOL
                        return PortfolioInterface.getSymbol(stock);
                    case 1:     // EXCHANGE CODE
                        return ExchangeStore.getSharedInstance().getExchange(exchange).getDisplayExchange();
                    case 2:     // COMPANY NAME
                        if (PortfolioInterface.getCompanyName(stock) == null)
                            return "";
                        else
                            return PortfolioInterface.getCompanyName(stock);
                    case 3:     // CURRENCY
                        if (PortfolioInterface.getCurrency(stock) == null)
                            return "";
                        else
                            return PortfolioInterface.getCurrency(stock);
                    case 7:     // EXCHANGE RATE
//                        return CurrencyStore.getCurrency(PortfolioInterface.getCurrency(stock)
//                            + "|" + PFStore.getBaseCurrency()) + "";
                        return CurrencyStore.getRate(PortfolioInterface.getCurrency(stock),
                                PFStore.getBaseCurrency());
                    case 5:     // HOLDING
                        return (long)record.getHolding();
                    case 6:     // AVERAGE COST
                        if(!PFStore.isFIFOSellMode())
                        //return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), record.getAvgCost(),true);
                        return record.getAvgCost();
                        else
                        return "N/A";
                    case 4:     // LATEST PRICE
                         if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                          //  return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),(float) PortfolioInterface.getLastTrade(stock),true);
                             return  PortfolioInterface.getLastTrade(stock);
                         else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                            //return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), PortfolioInterface.getVWAP(stock),true);
                             return (double)PortfolioInterface.getVWAP(stock);
                         else
                            //return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock),true);
                            return  PortfolioInterface.getPreviousClosed(stock);
                    case 8:     // TOTAL COST
                        return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), record.getCumCost(),true);
//                        return new Long((long)convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), record.getTotCost()));
                    case 9:     // MARKET VALUE
                        if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0)){
                            return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
                                record.getHolding() * PortfolioInterface.getLastTrade(stock) + record.getCashDividends(),true);

                        }else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0)) {
                           return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
                                record.getHolding() * PortfolioInterface.getVWAP(stock) + record.getCashDividends(),true);
                        } else {
                           return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
                                   record.getHolding() *PortfolioInterface.getPreviousClosed(stock) + record.getCashDividends(),true);
                        }


//                    case 9:     // MARKET VALUE
//                        if (stock.getLastTradeValue() > 0)
//                            return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
//                                    record.getHolding() * PortfolioInterface.getLastTrade(stock) +
//                                            record.getCashDividends());
//                        else
//                            return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
//                                    record.getHolding() * PortfolioInterface.getPreviousClosed(stock) +
//                                            record.getCashDividends());
                    case 10:    //  GAIN LOSS TODAY
                        return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), (record.getHolding() * PortfolioInterface.getChange(stock)[0]),true);
                    case 11:    //  % GAIN LOSS TODAY
//                        return new Float(convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((record.getHolding() * PortfolioInterface.getChange(stock)[0]) / record.getTotCost()))*100);
                        if(record.getCumCost()>0){
                        return new Double(((record.getHolding() * PortfolioInterface.getChange(stock)[0]) / record.getCumCost()) * 100);
                        }else{
                            return "";
                        }
                    case 12: {  //  GAIN LOSS OVERALL
                        double marketValue = 0D;
                        if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                           marketValue = (record.getHolding() * PortfolioInterface.getLastTrade(stock));
                       else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                           marketValue = ((record.getHolding() * PortfolioInterface.getVWAP(stock)));
                       else
                           marketValue = (record.getHolding() * PortfolioInterface.getPreviousClosed(stock));
                        marketValue += record.getCashDividends();
                        return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), (marketValue - record.getCumCost()),true);
//                        if (stock.getLastTradeValue() > 0&&PFStore.isVWAPMode())
//                            marketValue = ((record.getHolding() * (float)PortfolioInterface.getVWAP(stock)));
//                        else
//                            marketValue = (record.getHolding() * PortfolioInterface.getLastTrade(stock));
//                        marketValue += record.getCashDividends();
//                        return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), (marketValue - record.getCumCost()));
                    }
//                    case 12: {  //  GAIN LOSS OVERALL
//                        float marketValue = 0f;
//                        if (stock.getLastTradeValue() > 0)
//                            marketValue = ((record.getHolding() * PortfolioInterface.getLastTrade(stock)));
//                        else
//                            marketValue = (record.getHolding() * PortfolioInterface.getPreviousClosed(stock));
//                        marketValue += record.getCashDividends();
//                        return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), (marketValue - record.getCumCost()));
//                    }
//                    case 13: {  //  % GAIN LOSS OVERALL
//                        float marketValue = 0f;
//                        if (stock.getLastTradeValue() > 0)
//                            marketValue = (record.getHolding() *
//                                    PortfolioInterface.getLastTrade(stock));
//                        else
//                            marketValue = (record.getHolding() *
//                                    PortfolioInterface.getPreviousClosed(stock));
//                        marketValue += record.getCashDividends();
//                        return ((marketValue - record.getCumCost()) / record.getCumCost()) * 100;
////                        return new Float(convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((marketValue - record.getTotCost()) / record.getTotCost()))*100);
//                    }
                    case 13: {  //  % GAIN LOSS OVERALL
                        double marketValue = 0f;
                        if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                           marketValue = (record.getHolding() * PortfolioInterface.getLastTrade(stock));
                        else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                           marketValue = (record.getHolding() * PortfolioInterface.getVWAP(stock));
                        else
                           marketValue = (record.getHolding() * PortfolioInterface.getPreviousClosed(stock));
                        marketValue += record.getCashDividends();
                        if(record.getCumCost()>0){
                        return ((marketValue - record.getCumCost()) / record.getCumCost()) * 100;
                        }else{
                            return "";
                        }
//                        if (stock.getLastTradeValue() > 0&&PFStore.isVWAPMode())
//                            marketValue = (record.getHolding() * (float)PortfolioInterface.getVWAP(stock));
//                        else
//                            marketValue = (record.getHolding() * PortfolioInterface.getLastTrade(stock));
//                        marketValue += record.getCashDividends();
//                        return ((marketValue - record.getCumCost()) / record.getCumCost()) * 100;
//                        return new Float(convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((marketValue - record.getTotCost()) / record.getTotCost()))*100);
                    }
                    case 14:   //  % OF PORTFOLIO
                        return getPercOfPortfolio(record);
                    case 15:   //  CASH DIVIDENDS
                        return record.getCashDividends();
                    case 16:   //  Company code
                        return PortfolioInterface.getCompanyCode(stock);
                    case 17:  // Realized Gain Loss
                        return getRelizedGainLoss(record);
                    case 18:  // Total Gain Loss
                        return getRelizedGainLoss(record) + getUnrealizedGainLoss(stock, record);
                    default:
                        return "";
                }
            } else if (record.getRecordType()== ValuationRecordAll.PF_TOTAL_CASH_TYPE) {
                long pfId= record.getPfID();
                switch (col) {
                     case -13:
                        return true;
                    case 2:     // COMPANY NAME
                        return sCash + " (" + PFStore.getBaseCurrency() + ")";
                    case 8:             // CUMULATIVE TOTAL COST
                        return pfWindow.getCashBlanceForPortfolio(pfId);
                    case 9:             // CUMULATIVE MARKET VALUE
                        return pfWindow.getCashBlanceForPortfolio(pfId);
                    case 14:            //  % OF PORTFOLIO
                        return getPercOfPortfolio( record);
//                    case 15:   //  CASH DIVIDENDS
//                        return new Float(getTotalCashDividends());
                    default:
                        return "";
                }
            } else if (record.getRecordType()== ValuationRecordAll.PF_TOTAL_TYPE) {
                   double marketValue = getMarketValue(record);
                   double cumelativeTotCost=getCumulativeTotalCost(record);
                   double cumGainLossToday=getCumulativeGainLoss(record);
                   double cumGainLossAll=getCumGainLossOverall(record);
                   double totDividends=getTotalCashDividends(record);
                   double totRealizedGL=getCummRelizedGainLoss(record);
                   record.setValuation(marketValue);
                   record.setCumCost(cumelativeTotCost);
                   record.setPortfolioTotal(cumelativeTotCost);
                   record.setTotGainLoss(cumGainLossAll);
                   record.setCashDividends(totDividends);
                   record.setTodayGainLoss(cumGainLossToday);
                   record.setRealisedGain(totRealizedGL);
                
                switch (col) {
                     case -13:
                        return true;
                    case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        return "";
                    case 7:
                        if (Language.isLTR()) {
                            return (PFStore.getInstance().getPortfolio(record.getPfID())).getName() + " " + Language.getString("PF_TOTAL_SUMMARY");
                        } else {
                            return Language.getString("PF_TOTAL_SUMMARY")+ " "+ (PFStore.getInstance().getPortfolio(record.getPfID())).getName() ;
                        }
                    case 8:         // CUMULATIVE TOTAL COST
                        return getCumulativeTotalCost(record);
//                        return new Long((long)getCumulativeTotalCost());
                    case 9:         // CUMULATIVE MARKET VALUE
                        return getMarketValue(record);
                    case 10:        // CUMULATIVE GAIN LOSS TODAY
                        return getCumulativeGainLoss(record);
                    case 11: {       // CUMULATIVE % GAIN LOSS TODAY
                        double cumCost = getCumulativeTotalCost(record);
                        if (cumCost == 0)
                            return 0f;
                        else
                            return (getCumulativeGainLoss(record) / cumCost) * 100;
//                        return new Float(getCumulativePercentGainLoss());
                    }
                    case 12:        // CUMULATIVE GAIN LOSS OVERALL
                        return getCumGainLossOverall(record);
                    case 13: {       // CUMULATIVE % GAIN LOSS OVERALL
//                        return new Float(getCumPercentGainLossOverall());
                        double cumCost = getCumulativeTotalCost(record);
                        if (cumCost == 0)
                            return 0f;
                        else
                            return (getCumGainLossOverall(record) / cumCost) * 100;
                    }
                    case 14:        //  % OF PORTFOLIO
                        return new Double("100");
                    case 15:   //  CASH DIVIDENDS
                        return getTotalCashDividends(record);
                    case 17:  // Realized Gain Loss
                        return getCummRelizedGainLoss(record);
                      case 18:  // Realized Gain Loss
                        return getCummRelizedGainLoss(record) + getCumGainLossOverall(record);
                    default:
                        return "";
                }
            }else if (record.getRecordType()== ValuationRecordAll.VERTUAL_HEADETR_TYPE) {
                  String heading="";
                try {
                    heading= getViewSettings().getColumnHeadings()[col];
                } catch (Exception e) {
                }
                return heading;

            }else if (record.getRecordType()== ValuationRecordAll.PF_GRAND_TOTAL_TYPE) {


                switch (col) {
                     case -13:
                        return true;
                   case 0:
                    case 1:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        return "";
                    case 7:
                        return pfWindow.getTableCurrencyString();

                    case 8:         // CUMULATIVE TOTAL COST
                        return getGrandCumulativeTotalCost();
//                        return new Long((long)getCumulativeTotalCost());
                    case 9:         // CUMULATIVE MARKET VALUE
                        return getGrandMarketValue();
                    case 10:        // CUMULATIVE GAIN LOSS TODAY
                        return getGrandCumulativeGainLoss();
                    case 11: {       // CUMULATIVE % GAIN LOSS TODAY
                        double cumCost = getGrandCumulativeTotalCost();
                        if (cumCost == 0)
                            return 0f;
                        else
                            return (getGrandCumulativeGainLoss() / cumCost) * 100;
//                        return new Float(getCumulativePercentGainLoss());
                    }
                    case 12:        // CUMULATIVE GAIN LOSS OVERALL
                        return getGrandCumGainLossOverall();
                    case 13: {       // CUMULATIVE % GAIN LOSS OVERALL
//                        return new Float(getCumPercentGainLossOverall());
                        double cumCost = getGrandCumulativeTotalCost();
                        if (cumCost == 0)
                            return 0f;
                        else
                            return (getGrandCumGainLossOverall() / cumCost) * 100;
                    }
                    case 14:        //  % OF PORTFOLIO
                        return new Double("100");
                    case 15:   //  CASH DIVIDENDS
                        return getGrandCashDividends();
                     case 17:  // Realized Gain Loss
                        return getGrandCummRelizedGainLoss();
                      case 18:  // Realized Gain Loss
                        return getGrandCummRelizedGainLoss() + getGrandCumGainLossOverall();
                    default:
                        return "";
                }



            }else if (record.getRecordType()== ValuationRecordAll.EMPTY_ROW){
                return "";

            }else if (record.getRecordType()== ValuationRecordAll.PFNAME_ROW){
                if(col==0){
                    return  (PFStore.getInstance().getPortfolio(record.getPfID())).getName();
                } else{
                    return "";
                }

            }
            else {
                return "";
            }
        } catch (Exception e) {
//            e.printStackTrace();
            return "";
        }
    }

    public double getCummRelizedGainLoss(ValuationRecordAll allrec) {
        double cumRealized=0d;
         ArrayList tempdataStore = getValuationListForPortfolio(allrec);
        ValuationRecord record = null;
        for (int i = 0; i < tempdataStore.size(); i++) {
            record = (ValuationRecord) tempdataStore.get(i);
            cumRealized += record.getRealizedGain();
            record = null;
        }
        return cumRealized;

    }

     public double getRelizedGainLoss(ValuationRecord record){
        return record.getRealizedGain();
    }

     public double getUnrealizedGainLoss(Stock stock, ValuationRecord record){
        double marketValue = 0D;
        if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
           marketValue = (record.getHolding() * PortfolioInterface.getLastTrade(stock));
       else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
           marketValue = ((record.getHolding() * PortfolioInterface.getVWAP(stock)));
       else
           marketValue = (record.getHolding() * PortfolioInterface.getPreviousClosed(stock));
        marketValue += record.getCashDividends();
        return convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), (marketValue - record.getCumCost()),true);
    }

   public double getGrandCummRelizedGainLoss(){
        double grandtorelgain = 0f;
        Stock stock = null;
        ArrayList totlist = getFilteredValuatioAllRecordList(ValuationRecordAll.PF_TOTAL_TYPE);
        for(int i=0; i<totlist.size(); i++){

            ValuationRecordAll record = (ValuationRecordAll) totlist.get(i);
            grandtorelgain +=record.getRealisedGain();

        }
       return grandtorelgain;


   }
    public double  getGrandCumulativeTotalCost(){
       double grandtotcost = 0f;
        Stock stock = null;
        ArrayList totlist = getFilteredValuatioAllRecordList(ValuationRecordAll.PF_TOTAL_TYPE);
        for(int i=0; i<totlist.size(); i++){

            ValuationRecordAll record = (ValuationRecordAll) totlist.get(i);
            grandtotcost +=record.getPortfolioTotal();

        }
       return grandtotcost;
    }

    public double getGrandMarketValue(){

         double grandMktValue = 0f;
        Stock stock = null;
        ArrayList totlist = getFilteredValuatioAllRecordList(ValuationRecordAll.PF_TOTAL_TYPE);
        for(int i=0; i<totlist.size(); i++){

            ValuationRecordAll record = (ValuationRecordAll) totlist.get(i);
            grandMktValue +=record.getValuation();

        }
       return grandMktValue;


    }

    public double getGrandCumGainLossOverall(){
           double grandCumLoss = 0f;
        Stock stock = null;
        ArrayList totlist = getFilteredValuatioAllRecordList(ValuationRecordAll.PF_TOTAL_TYPE);
        for(int i=0; i<totlist.size(); i++){

            ValuationRecordAll record = (ValuationRecordAll) totlist.get(i);
            grandCumLoss +=record.getTotGainLoss();

        }
       return grandCumLoss;



    }

    public double getGrandCumulativeGainLoss(){

            double grandCumLoss = 0f;
        Stock stock = null;
        ArrayList totlist = getFilteredValuatioAllRecordList(ValuationRecordAll.PF_TOTAL_TYPE);
        for(int i=0; i<totlist.size(); i++){

            ValuationRecordAll record = (ValuationRecordAll) totlist.get(i);
            grandCumLoss +=record.getTodayGainLoss();

        }
       return grandCumLoss;


    }

    public float getGrandCashDividends(){

       float grandDividends = 0f;
        Stock stock = null;
        ArrayList totlist = getFilteredValuatioAllRecordList(ValuationRecordAll.PF_TOTAL_TYPE);
        for(int i=0; i<totlist.size(); i++){

            ValuationRecordAll record = (ValuationRecordAll) totlist.get(i);
            grandDividends +=record.getCashDividends();

        }
       return grandDividends;

    }

    public float getTotalCashDividends(ValuationRecordAll allrec) {
        float totMValue = 0f;
        ArrayList tempdataStore = getValuationListForPortfolio(allrec);
        ValuationRecord record = null;
        for (int i = 0; i < tempdataStore.size(); i++) {
            record = (ValuationRecord) tempdataStore.get(i);
            totMValue += record.getCashDividends();
            record = null;
        }
        return totMValue;
    }

    public double getPercOfPortfolio(ValuationRecordAll allrec) {
        double mValue = 0f;
        double selMValue = 0f;
        double totMValue = 0f;
        Stock stock = null;
        long pfId = allrec.getPfID();
        if(allrec.getRecordType() == ValuationRecordAll.VALUATIONREC_TYPE){
            ArrayList localdataStore = getValuationListForPortfolio(allrec);
//            pfStore.createValuationList(pfStore.getPortfolioIDList(),null);
//            localdataStore = pfStore.getValuationList();
            ValuationRecord record = null;
            for (int i = 0; i < localdataStore.size(); i++) {
                record = (ValuationRecord) localdataStore.get(i);
                stock = PortfolioInterface.getStockObject(record.getSKey());
                if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                    mValue = record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),
                            PortfolioInterface.getLastTrade(stock) + record.getCashDividends(),true);
                else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                    mValue = record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),
                            PortfolioInterface.getVWAP(stock) + record.getCashDividends(),true);
                else
                    mValue = record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),
                            PortfolioInterface.getPreviousClosed(stock) + record.getCashDividends(),true);
    //            if (stock.getLastTradeValue() > 0)
    //                mValue = record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),
    //                        PortfolioInterface.getLastTrade(stock) + record.getCashDividends());
    //            else
    //                mValue = record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),
    //                        PortfolioInterface.getPreviousClosed(stock) + record.getCashDividends());
                if (record.getSKey() == allrec.getSKey()){
                    selMValue = mValue;
                }
                totMValue += mValue;
                record = null;
                stock = null;
            }
        }
        if (isCashIncluded ) {
            if (allrec.getRecordType() == ValuationRecordAll.PF_TOTAL_CASH_TYPE){
                selMValue = pfWindow.getCashBlanceForPortfolio(pfId);
            }
            totMValue += pfWindow.getCashBlanceForPortfolio(pfId);
        }
        if (totMValue == 0)
            return 0f;
        else
            return (selMValue / totMValue) * 100;
    }

/*
        public float getPercOfPortfolio(int row) {
            float mValue    = 0f;
            float selMValue = 0f;
            float totMValue = 0f;
            Stock stock  = null;
            ValuationRecord record = null;
            for (int i = 0; i < dataStore.size(); i++) {
                record = (ValuationRecord)dataStore.get(i);
                stock  = PortfolioInterface.getStockObject(record.getSKey());
                if (stock.getLastTrade()[0] > 0)
                    mValue += record.getHolding() * convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock)[0]);
                else
                    mValue += record.getHolding() * convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock)[0]);
                if (row == i)
                    selMValue = mValue;
                totMValue += mValue;
                record = null;
                stock  = null;
            }
            if (isCashIncluded) {
                if (row == (getRowCount() - 2))
                    selMValue = pfWindow.getCashBalanceForSelectedPFs();
                totMValue += pfWindow.getCashBalanceForSelectedPFs();
            }
            return (selMValue / totMValue) * 100;
        }
*/

    public double getCumulativeTotalCost(ValuationRecordAll allrec) {
        double totCost = 0f;
        Stock stock = null;
        ArrayList tempdataStore = getValuationListForPortfolio(allrec);
        ValuationRecord record = null;
        for (int i = 0; i < tempdataStore.size(); i++) {
            record = (ValuationRecord) tempdataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            totCost += convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), record.getCumCost(),true);
            record = null;
        }
        if (isCashIncluded)
            totCost += pfWindow.getCashBlanceForPortfolio(allrec.getPfID());
        return totCost;
    }

    public double getMarketValue(ValuationRecordAll allrec) {
        double mValue = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        ArrayList tempdataStore = getValuationListForPortfolio(allrec);
        for (int i = 0; i < tempdataStore.size(); i++) {
            record = (ValuationRecord) tempdataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock),true);
            else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getVWAP(stock),true);
            else
                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock),true);
//            if (stock.getLastTradeValue() > 0)
//                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock));
//            else
//                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock));
            record = null;
            stock = null;
        }
        if (isCashIncluded)
            mValue += pfWindow.getCashBlanceForPortfolio(allrec.getPfID());
        return mValue;
    }

    public double getCumGainLossOverall(ValuationRecordAll allrec) {
        double mValue = 0f;
        double cumValue = 0f;
        Stock stock = null;
        ValuationRecord record = null;
         ArrayList tempdataStore = getValuationListForPortfolio(allrec);
        for (int i = 0; i < tempdataStore.size(); i++) {
            record = (ValuationRecord) tempdataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                mValue = record.getHolding() * PortfolioInterface.getLastTrade(stock);
            else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                mValue = record.getHolding() * PortfolioInterface.getVWAP(stock);
            else
                mValue = record.getHolding() * PortfolioInterface.getPreviousClosed(stock);
//            if (stock.getLastTradeValue() > 0)
//                mValue = record.getHolding() * PortfolioInterface.getLastTrade(stock);
//            else
//                mValue = record.getHolding() * PortfolioInterface.getPreviousClosed(stock);
//            mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), (mValue - record.getTotCost()));
            cumValue += convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), (mValue - record.getCumCost()),true);
            record = null;
            stock = null;
        }
        return cumValue;
    }
/*
        public float getCumGainLossOverall() {
            float mValue = 0f;
            Stock stock  = null;
            ValuationRecord record = null;
            for (int i = 0; i < dataStore.size(); i++) {
                record = (ValuationRecord)dataStore.get(i);
                stock  = PortfolioInterface.getStockObject(record.getSKey());
                if (stock.getLastTrade()[0] > 0)
                    mValue += record.getHolding() * PortfolioInterface.getLastTrade(stock)[0];
                else
                    mValue += record.getHolding() * PortfolioInterface.getPreviousClosed(stock)[0];
                mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), (mValue - record.getTotCost()));
                record = null;
                stock  = null;
            }
            return mValue;
        }
*/

    public float getCumPercentGainLossOverall(ValuationRecordAll allrec) {
        double mValue = 0f;
        float cumValue = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        ArrayList tempdataStore = getValuationListForPortfolio(allrec);
        for (int i = 0; i < tempdataStore.size(); i++) {
            record = (ValuationRecord) tempdataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            if (PortfolioInterface.getLastTrade(stock) > 0)
                mValue = record.getHolding() * PortfolioInterface.getLastTrade(stock);
            else
                mValue = record.getHolding() * PortfolioInterface.getPreviousClosed(stock);
//            mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((mValue - record.getTotCost()) / record.getTotCost()));
            if (record.getCumCost() != 0)
                cumValue += ((mValue - record.getCumCost()) / record.getCumCost()) * 100;
            record = null;
            stock = null;
        }
        return cumValue;
    }

/*
        public float getCumPercentGainLossOverall() {
            float mValue = 0f;
            float cumValue = 0f;
            Stock stock  = null;
            ValuationRecord record = null;
            for (int i = 0; i < dataStore.size(); i++) {
                record = (ValuationRecord)dataStore.get(i);
                stock  = PortfolioInterface.getStockObject(record.getSKey());
                if (PortfolioInterface.getLastTrade(stock)[0] > 0)
                    mValue += record.getHolding() * PortfolioInterface.getLastTrade(stock)[0];
                else
                    mValue += record.getHolding() * PortfolioInterface.getPreviousClosed(stock)[0];
//            mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((mValue - record.getTotCost()) / record.getTotCost()));
                cumValue += ((mValue - record.getTotCost()) / record.getTotCost()) * 100;
                record = null;
                stock  = null;
            }
            return cumValue;
        }
     */

    public double getCumulativeGainLoss(ValuationRecordAll allrec) {
        double totCost = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        ArrayList tempdataStore = getValuationListForPortfolio(allrec);
        for (int i = 0; i < tempdataStore.size(); i++) {
            record = (ValuationRecord) tempdataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            totCost += convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), record.getHolding() * PortfolioInterface.getChange(stock)[0],true);
            record = null;
        }
        return totCost;
    }

    public float getCumulativePercentGainLoss(ValuationRecordAll allrec) {
        float totCost = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        ArrayList tempdataStore = getValuationListForPortfolio(allrec);
        for (int i = 0; i < tempdataStore.size(); i++) {
            record = (ValuationRecord) tempdataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            if (record.getCumCost() != 0)
                totCost += (record.getHolding() * PortfolioInterface.getChange(stock)[0] / record.getCumCost()) * 100;
//            totCost += convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), (record.getHolding() * PortfolioInterface.getChange(stock)[0] / record.getTotCost()));
            record = null;
        }
        return totCost;
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        /*try {
            return getValueAt(0, iCol).getClass();
                 } catch (Exception e) {
            return Object.class;
                 }*/
//        switch (iCol) {
//            case 0:
//            case 1:
//            case 2:
//            case 3:
//            case 16:
//                return String.class;
//            case 4:
//            case 6:
//            case 7:
//            case 8:
//            case 9:
//            case 10:
//            case 11:
//            case 12:
//            case 13:
//            case 14:
//                return Double.class;
//            case 5:
//                return Long.class;
//            case 15:
//                return Float.class;
//            default:
//                return Object.class;
//        }
        return Object.class;
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object value, int rowIndex, int column) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }

    private double convertToSelectedCurrency(String exchangeCode,String sCurrency, double value, boolean applyPriceModificationFactor ) {
        //return value * (float) CurrencyStore.getRate(sCurrency, pfStore.getInstance().getBaseCurrency());
        if(applyPriceModificationFactor){
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
            if (exchange != null)
                return value * (double) CurrencyStore.getRate(sCurrency, PFStore.getBaseCurrency()) / exchange.getPriceModificationFactor();
            else
                return value * (double) CurrencyStore.getRate(sCurrency, PFStore.getBaseCurrency());
        } else {
            return value * (double) CurrencyStore.getRate(sCurrency, PFStore.getBaseCurrency());
        }
    }


    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return Double.parseDouble((String) oValue);
    }

    private int toIntValue(Object oValue) throws Exception {
        return Integer.parseInt((String) oValue);
    }

    private float toFloatValue(Object oValue) throws Exception {
        return Float.parseFloat((String) oValue);
    }

    public double getMarketValueForSymbol(String sKey) {
        double marketValue = 0f;
        ValuationRecord valRecord = null;
        ArrayList list = PFStore.getInstance().getValuationList();
        Stock stock = PortfolioInterface.getStockObject(sKey);
        if (stock == null)
            return 0f;

        for (int i = 0; i < list.size(); i++) {
            valRecord = (ValuationRecord) list.get(i);
            if (valRecord.getSKey().equals(sKey)) {
                if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                    marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), valRecord.getHolding() * PortfolioInterface.getLastTrade(stock),true);
                else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                    marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), valRecord.getHolding() * PortfolioInterface.getVWAP(stock),true);
                else
                    marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), valRecord.getHolding() * PortfolioInterface.getPreviousClosed(stock),true);
//                if (stock.getLastTradeValue() > 0)
//                    marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), valRecord.getHolding() * PortfolioInterface.getLastTrade(stock));
//                else
//                    marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), valRecord.getHolding() * PortfolioInterface.getPreviousClosed(stock));
                break;
            }
            valRecord = null;
        }
        valRecord = null;
        list = null;
        stock = null;

        return marketValue;
    }

    public double getMktValuePerShareForSymbol(String sKey) {
        double marketValue = 0f;
        Stock stock = PortfolioInterface.getStockObject(sKey);
        if (stock == null)
            return 0f;
        if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
            marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock),true);
        else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
            marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getVWAP(stock),true);
        else
            marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock),true);
//        if (stock.getLastTradeValue() > 0)
//            marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock));
//        else
//            marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock));

        stock = null;
        return marketValue;
    }

    public float getMarketValue(boolean isCashIncludedLocal) {
        float mValue = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            record = (ValuationRecord) dataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock),true);
            else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getVWAP(stock),true);
            else
                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock),true);
//            if (stock.getLastTradeValue() > 0)
//                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock));
//            else
//                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock));
            record = null;
            stock = null;
        }
        if (isCashIncludedLocal)
            mValue += pfWindow.getCashBalanceForSelectedPFs();
        return mValue;
    }


    private ArrayList getValuationListForPortfolio(ValuationRecordAll allrec){
        ArrayList list =null;
        try {

            long pfId = allrec.getPfID();
            long[]  pfIDS = new long[1];
            pfIDS[0] = pfId;
            pfStore.createValuationList(pfIDS,null);
            list = pfStore.getValuationList();
        } catch (Exception e) {
            list = new ArrayList();
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return list;

    }

    private ArrayList getFilteredValuatioAllRecordList(int type){

        ArrayList list = new ArrayList();
        for(int i=0; i<dataStore.size(); i++){

            ValuationRecordAll record = (ValuationRecordAll) dataStore.get(i);

            if(record.getRecordType() == type){
                list.add(record);
            }
        }

        return list;
    }

    public ValuationRecordAll getRecord(int row){

        return (ValuationRecordAll) dataStore.get(row);
    }
}