package com.isi.csvr.portfolio;


import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.SmartFileChooser;
import com.isi.csvr.shared.*;
import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.io.File;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 5, 2008
 * Time: 9:57:01 AM
 * To change this template use File | Settings | File Templates.
 */
public class PortfolioImportUI extends InternalFrame {

   private ArrayList portfolioNameList;
   private static int PORTFOLIO_SELECT=1;
   private static String PORTFOLIO_SELECT_STRING="portfolio";
   private static int FILETYPE_SELECT=2;
   private static String FILETYPE_SELECT_STRING="filetype";

   public static int EXCHANGE_MODE_MULTIPLE=1;
   public static int EXCHANGE_MODE_SINGLE=2;
   private String selectedPanel = "portfolio";

   private JPanel crdPanel;
   private JPanel topPanel1;
   private JPanel topPanel2;
   private JPanel middelPanel1;
   private JPanel middelPanel2;
   private JPanel portfolioSelPanel;
   private JPanel importFileTypePanel;
   private JPanel bottomPanel;

   //top Panel components
    private TWTextField pfName;
    private TWComboBox currTyepe;
    private TWTextField cashBal;
    private TWComboBox imprtType;
    private TWComboBox cmbExchangeMode;
    private TWTextField browse;

    private JLabel lblpfName;
    private JLabel lblcurrTyepe;
    private JLabel lblcashBal;
    private JLabel lblimprtType;
    private JLabel lblbrowse;
    private JLabel lblExchangeMode;
    private TWButton downBtn;

    private JCheckBox useExsisting;
    private JComboBox selPortfolio;

    private JLabel lbluseExsisting;
    private JLabel lblselPortfolio;

    //bottom panel components
    private TWButton next;
    private TWButton back;
    private TWButton cancel;

    private SmartFileChooser chooser;
    private File selFile;
    private long selpfId=0l;
    private long createdlpfId=0l;
    private boolean isEditMode;
    private boolean newPfCreated;


    public PortfolioImportUI() {

        initUI();
    }

    private void initUI(){
       initComponents();
        setLables();
       addCompToPanels();
        addListeners();
        loadPortfolios();
        loadCurrency();
        loadFileTypes();
        this.setTitle(Language.getString("IMPORT_FILE"));
         this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(false);
        this.setIconifiable(true);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
    //   this.setPreferredSize(new Dimension(300,300));
        this.setSize(320,200);
       this.setLayout(new BorderLayout());
       this.getContentPane().add(crdPanel, BorderLayout.CENTER);
        this.getContentPane().add(bottomPanel, BorderLayout.SOUTH);
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        GUISettings.applyOrientation(this);
      // Client.getInstance().getDesktop().add(this);
     //   setLocationRelativeTo(Client.getInstance().getDesktop());
    //    this.setVisible(true);



    }

    
    private void initComponents(){
      crdPanel = new JPanel(new CardLayout(1,1));
      topPanel1 = new JPanel();
      topPanel2 = new JPanel();
      middelPanel1 = new JPanel();
      middelPanel2 = new JPanel();
      bottomPanel = new JPanel();
      portfolioSelPanel = new JPanel();
      importFileTypePanel = new JPanel();

      pfName = new TWTextField();
      currTyepe = new TWComboBox();
      cashBal = new TWTextField(new ValueFormatter(ValueFormatter.DECIMAL),"",20);
      imprtType = new TWComboBox();
      browse = new TWTextField();
      lblpfName = new JLabel();
      lblcurrTyepe = new JLabel();
      lblcashBal = new JLabel();
      lblimprtType = new JLabel();
      lblbrowse = new JLabel();
        lblExchangeMode = new JLabel();
      downBtn = new TWButton(new DownArrow());

      useExsisting = new JCheckBox();
      selPortfolio = new JComboBox();
      lblselPortfolio = new JLabel();

       next= new TWButton();
       cancel = new TWButton();
       back = new TWButton();


        cmbExchangeMode = new TWComboBox();
        TWComboItem multiple = new TWComboItem(EXCHANGE_MODE_MULTIPLE, Language.getString("PF_IMPORT_EXCHANGE_MODE_MULTIPLE"));
        TWComboItem single = new TWComboItem(EXCHANGE_MODE_SINGLE, Language.getString("PF_IMPORT_EXCHANGE_MODE_SINGLE"));
        cmbExchangeMode.addItem(multiple);
        cmbExchangeMode.addItem(single);
        cmbExchangeMode.setSelectedIndex(0);




    }
    private void setLables(){
        lblpfName.setText(Language.getString("PORTFOLIO_NAME"));
        lblcurrTyepe.setText(Language.getString("CURRENCY"));
        lblcashBal.setText(Language.getString("CASH_BALANCE"));
        lblimprtType.setText(Language.getString("IMPORT_TYPE"));
        lblbrowse.setText(Language.getString("BROWSE"));
        useExsisting.setText(Language.getString("USE_EXSISTING"));
        lblselPortfolio.setText(Language.getString("PORTFOLIO_NAME"));
        next.setText(Language.getString("NEXT"));
        cancel.setText(Language.getString("CANCEL"));
        back.setText(Language.getString("BACK"));
        lblExchangeMode.setText(Language.getString("PF_IMPORT_EXCHANGE_MODE"));
    }
    private void addCompToPanels(){
        topPanel1.setLayout(new FlexGridLayout(new String[]{"120", "180"}, new String[]{"22","22","22","22","24"},4,4,true, true));
        topPanel1.setSize(300,120);
        topPanel1.add(lblpfName);
        topPanel1.add(pfName);
        topPanel1.add(lblcurrTyepe);
        topPanel1.add(currTyepe);
        topPanel1.add(lblcashBal);
        topPanel1.add(cashBal);
        topPanel1.add(useExsisting );
        topPanel1.add(new JLabel(" "));
        topPanel1.add(lblselPortfolio);
        topPanel1.add(selPortfolio);
       // topPanel1.setBorder(BorderFactory.createLineBorder(Color.RED));
        topPanel1.setOpaque(true);

        topPanel2.setLayout(new FlexGridLayout(new String[]{"100", "180"}, new String[]{"22","24","24"},4,15,true, true));
        topPanel2.add(lblimprtType);
        topPanel2.add(imprtType);
        topPanel2.add(lblbrowse);
        JPanel panel = new JPanel(new FlowLayout(FlowLayout.LEFT));
        
        panel.setLayout(new FlexGridLayout(new String[]{"160", "20"}, new String[]{"24"},0,0,true, true));
       // browse.setPreferredSize(new Dimension(160,20));
        //downBtn.setPreferredSize(new Dimension(20,20));
        panel.setPreferredSize(new Dimension(180,24));
        panel.add(browse);
        panel.add(downBtn);
        topPanel2.add(panel);
        topPanel2.add(lblExchangeMode);
        topPanel2.add(cmbExchangeMode);

//        middelPanel1.setLayout(new FlexGridLayout(new String[]{"100", "180"}, new String[]{"22","24"},4,4,true, true));
//        middelPanel1.add(useExsisting);
//        middelPanel1.add(new JLabel(""));
//        middelPanel1.add(lblselPortfolio);
//        middelPanel1.add(selPortfolio);
       if(Language.isLTR()){
        bottomPanel.setLayout(new FlowLayout(FlowLayout.RIGHT));
        bottomPanel.add(cancel);
         bottomPanel.add(back);
        bottomPanel.add(next);
       }else{
          bottomPanel.setLayout(new FlowLayout(FlowLayout.LEFT));
        bottomPanel.add(next);
         bottomPanel.add(back);
        bottomPanel.add(cancel); 
       }


        portfolioSelPanel.setLayout(new BoxLayout(portfolioSelPanel, BoxLayout.Y_AXIS));
        portfolioSelPanel.add(topPanel1);
      //  portfolioSelPanel.add(middelPanel1);

        importFileTypePanel.setLayout(new BoxLayout(importFileTypePanel, BoxLayout.Y_AXIS));
        importFileTypePanel.add(topPanel2);

       // crdPanel.add(portfolioSelPanel,PORTFOLIO_SELECT);
      //  crdPanel.add(importFileTypePanel,FILETYPE_SELECT);
        portfolioSelPanel.setOpaque(true);
        importFileTypePanel.setOpaque(true);
        crdPanel.add(PORTFOLIO_SELECT_STRING,portfolioSelPanel);
        crdPanel.add(FILETYPE_SELECT_STRING,importFileTypePanel) ;

        CardLayout cl = (CardLayout) (crdPanel.getLayout());
        cl.show(crdPanel, PORTFOLIO_SELECT_STRING);
        back.setEnabled(false);
        selPortfolio.setEnabled(false);

        GUISettings.applyOrientation(portfolioSelPanel);
        GUISettings.applyOrientation(importFileTypePanel);

    }

    public void showwindow(){
         this.setVisible(true);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());

    }

    private void loadPortfolios(){
         PortfolioRecord record = null;
        for (int i = 0; i < PFStore.getInstance().getPortfolioList().size(); i++) {
            record = (PortfolioRecord) PFStore.getInstance().getPortfolioList().get(i);
            selPortfolio.addItem(record.getName());
            if (record.isDefault())
                selPortfolio.setSelectedItem(record.getName());

            record = null;
        }

    }

    private void loadFileTypes(){
        imprtType.addItem(Language.getString("CSV_FORMAT"));

        
    }

    private void loadCurrency(){

        String sItem = null;
        Enumeration currencyIDs = CurrencyStore.getSharedInstance().getCurrencyIDs();
        while (currencyIDs.hasMoreElements()) {
            sItem = (String) currencyIDs.nextElement();
            currTyepe.addItem(sItem);
            sItem = null;

        }
        currencyIDs = null;
        try {
            currTyepe.setSelectedItem(PFStore.getInstance().getBaseCurrency());
            GUISettings.applyOrientation(currTyepe);
        } catch (Exception ex) {
        }
    }

    private void addListeners(){

        next.addActionListener(this);
        back.addActionListener(this);
        cancel.addActionListener(this);
        useExsisting.addActionListener(this);
        downBtn.addMouseListener(this);


    }

    public void actionPerformed(ActionEvent e) {

        if(e.getSource()==next){
            if(selectedPanel==PORTFOLIO_SELECT_STRING && validatePfData()){
            back.setEnabled(true);
            isEditMode = false;
            setSelectedPannel(FILETYPE_SELECT);

            }else{

                boolean success= setSelectedDataToStore();
                if(success){
                    this.dispose();
                }
            }

        }else if(e.getSource()==back){
            if(selectedPanel== FILETYPE_SELECT_STRING){
                back.setEnabled(false);
                setSelectedPannel(PORTFOLIO_SELECT);
                isEditMode = true;
                enabletextBoxes();
            }
        }else if(e.getSource()== useExsisting){
          enabletextBoxes();
        }
        else if(e.getSource()== cancel){

            this.dispose();

        }


    }

    private void enabletextBoxes(){
        if(useExsisting.isSelected()){
                pfName.setEnabled(false);
                currTyepe.setEnabled(false);
                cashBal.setEnabled(false);
                selPortfolio.setEnabled(true);
            }else if(!useExsisting.isSelected() && isEditMode && newPfCreated){
                 pfName.setEnabled(true);
                currTyepe.setEnabled(false);
                cashBal.setEnabled(false);
                selPortfolio.setEnabled(true);
            }
            else{
                 pfName.setEnabled(true);
                currTyepe.setEnabled(true);
                cashBal.setEnabled(true);
                selPortfolio.setEnabled(false);

            }

    }

    public void mouseClicked(MouseEvent e) {
        if(e.getSource()== downBtn){

            selFile = getSelecedFile();
            if(selFile!=null){

                browse.setText(selFile.getPath());

            }

        }



    }

    public void setSelectedPannel(int panleid){
        if(panleid==FILETYPE_SELECT){
            selectedPanel = FILETYPE_SELECT_STRING;
        }else if(panleid == PORTFOLIO_SELECT){
            selectedPanel = PORTFOLIO_SELECT_STRING;

        }
        CardLayout cl = (CardLayout) (crdPanel.getLayout());
        cl.show(crdPanel, selectedPanel);
    }


    private File getSelecedFile(){

        chooser = new SmartFileChooser(Settings.getAbsolutepath());
        GUISettings.localizeFileChooserHomeButton(chooser);
        chooser.addChoosableFileFilter(new MyFilterTree());
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setDialogTitle(Language.getString("IMPORT_USER_DATA"));
        int option = chooser.showOpenDialog(Client.getInstance().getFrame());
        if (option == JFileChooser.CANCEL_OPTION) {
            return null;
        } else if (option == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile();
        }else{
            return null;
        }

    }

    private boolean validatePfData(){
        if(useExsisting.isSelected()){
            try {
                selpfId= PFStore.getInstance().getPortfolio(selPortfolio.getSelectedIndex()).getPfID();
                return true;
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return false;
            }


        } else{

          return validateAndCreateNewPf();

        }

    }

    private boolean validateAndCreateNewPf(){
        PortfolioRecord record=null;
        if(isEditMode && newPfCreated){

            record = PFStore.getInstance().getPortfolio(createdlpfId);
        }
        String pfName = this.pfName.getText();
        float cashBalance = 0;

        if (pfName.equals("")) {
            new ShowMessage(Language.getString("MSG_NAME_CANNOT_BE_BLANK"), "I");
            return false;
        }
        if (isEditMode && (record != null) && (!record.getName().equals(pfName))) {
            if (PFStore.getInstance().isPortfolioNameExists(pfName)) {
                new ShowMessage(Language.getString("MSG_NAME_ALREADY_EXISTS"), "I");
                return false;
            }
        }
        if (!isEditMode || !newPfCreated) {
            if (PFStore.getInstance().isPortfolioNameExists(pfName)) {
                new ShowMessage(Language.getString("MSG_NAME_ALREADY_EXISTS"), "I");
                return false;
            }
        }
         if ((!isEditMode || !newPfCreated) && cashBalance < 0) {
            new ShowMessage(Language.getString("PF_MSG_CASH_CANNOT_BE_NEGATIVE"), "I");
            return false;
        }

        try {
            cashBalance = Float.parseFloat(cashBal.getText());
        } catch (NumberFormatException ex) {
            cashBalance = 0;
        }

        if ((!isEditMode&& !newPfCreated) || (record == null)) {
            record = new PortfolioRecord();
        }
        record.setName(pfName);
        record.setCashBalance(cashBalance);
        if (!isEditMode || (isEditMode && !newPfCreated)) {
            PFStore.getInstance().addPortfolio(record);
            record.setCurrency((String)currTyepe.getSelectedItem());
            newPfCreated = true;
             createdlpfId= record.getPfID();
        }
            selpfId = record.getPfID();

        PortfolioImport.getSharedInstance().loadNewlyAddedPortfolio(record.getPfID(),record);
        return true;

    }

    private boolean setSelectedDataToStore(){

      if(selpfId !=0l && selFile != null){
          int excgType =Integer.parseInt (((TWComboItem)cmbExchangeMode.getSelectedItem()).getId());
          PortfolioImport.getSharedInstance().setDataFromStartImport(selpfId,selFile, (String)imprtType.getSelectedItem(), excgType);
          return true;
      }


      return false;


    }


    class MyFilterTree extends javax.swing.filechooser.FileFilter {
    public boolean accept(File file) {
        String filename = file.getName();
        String ext = getFileExtention();
        if (filename.endsWith(ext)) {
            return true;
        } else if (file.isDirectory()) {
            return true;
        } else {
            return false;
        }
    }

    public String getDescription() {
        return "*"+ getFileExtention();
    }

    private String getFileExtention(){
        String selected = (String)imprtType.getSelectedItem();
        if(selected == Language.getString("CSV_FORMAT")){
            return ".csv";
        }else{
            return ".*";
        }
    }
}

    public void closeWindow() {
//		if (getDefaultCloseOperation() == HIDE_ON_CLOSE){
//			setVisible(false);
//		}else{
//			dispose();
//		}
        doDefaultCloseAction();
    }
    public int getDesktopItemType() {
        return BOARD_TYPE;
    }
}
