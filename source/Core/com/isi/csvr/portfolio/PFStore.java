package com.isi.csvr.portfolio;

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWDateFormat;

import java.io.*;
import java.util.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public final class PFStore {

    public static String transFileName = Settings.getAbsolutepath() + "datastore/txnHistory.dat";
    private static ArrayList<PortfolioRecord> portfolioList;         // Contains the portfolio list
    private static ArrayList<TransactRecord> transactionRecords;    // Contains the whole list of transactions
    private static ArrayList filteredTransactions;  // Contains the whole list of transactions
    private static ArrayList valuationRecords;      // Contains calculated record details for the
    // selected Portfolio(s). These records may get
    // change due to the post transactions associated
    // with the selected portfolio(s).
    private static ArrayList unregisteredSymbols;   // Contains list of unregisterd symbols
    private static ArrayList allValuationRecords;  //contains all the possible valuation records as ValuationrecordAll type object
    private static String baseCurrency;//    = PortfolioInterface.DEFAULT_CURRENCY; //"USD"; // "USD"; //
    private static PFStore selfRef = null;
    //    private static  int     lastTransactionID = 0;
    private Properties prop;
    private static boolean VWAPMode = false;
    private static boolean FIFOSellMode = false;
    private static boolean SHOWZEROHOLDINGS = false;
    private static Hashtable<String, SplitRecord> splitStore = null;
    private static Hashtable<Long, CommissionObject> commissionStore = null;
    private static ArrayList<String> commissionList = null;
    private long lastSplitID = 0;

    private PFStore() {
        portfolioList = new ArrayList<PortfolioRecord>();
        transactionRecords = new ArrayList<TransactRecord>();
        filteredTransactions = new ArrayList();
        valuationRecords = new ArrayList(); //(20, 0.9f);
        splitStore = new Hashtable<String, SplitRecord>();
        commissionStore = new Hashtable<Long, CommissionObject>();
        allValuationRecords = new ArrayList<ValuationRecordAll>();
        commissionList = new ArrayList<String>();
        loadPortfolioFile();
        baseCurrency = prop.getProperty("PORTFOLIO_SIMULATOR_BASE_CURRENCY");
        VWAPMode = Boolean.parseBoolean(prop.getProperty("PORTFOLIO_SIMULATOR_VWAPMODE"));
        FIFOSellMode = Boolean.parseBoolean(prop.getProperty("PORTFOLIO_SIMULATOR_FIFO_SELL_MODE"));
        try {
            loadTransactions(transFileName); // Load the saved translations from the flat file.
        } catch (Exception ex) {
        }
    }

    public void loadPortfolioFile() {
        prop = new Properties();
        try {
            FileInputStream oIn = new FileInputStream(Settings.SYSTEM_PATH + "/portfolio.dll");
            prop.load(oIn);
            oIn.close();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static boolean isVWAPMode() {
        return VWAPMode;
    }

    public static boolean isFIFOSellMode() {
        return FIFOSellMode;


    }

    public void setFIFOSellMode(boolean FIFOSellMode) {
        PFStore.FIFOSellMode = FIFOSellMode;
        prop.setProperty("PORTFOLIO_SIMULATOR_FIFO_SELL_MODE", "" + FIFOSellMode);
        try {
            FileOutputStream oOut = new FileOutputStream(Settings.SYSTEM_PATH + "/portfolio.dll");
            prop.store(oOut, "");
            oOut.close();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static boolean isSymbolSpecificTransaction(int type) {
        switch (type) {
            case PFUtilities.BUY:
            case PFUtilities.SELL:
            case PFUtilities.OP_BAL:
            case PFUtilities.DIVIDEND:
                return true;
            case PFUtilities.DEPOSITE:
            case PFUtilities.WITHDRAWAL:
            case PFUtilities.CHARGES:
            case PFUtilities.REFUNDS:
                return false;
            default:
                return false;
        }
    }

    public void setVWAPMode(boolean newVWAPMode) {
        VWAPMode = newVWAPMode;
        prop.setProperty("PORTFOLIO_SIMULATOR_VWAPMODE", "" + newVWAPMode);
        try {
            FileOutputStream oOut = new FileOutputStream(Settings.SYSTEM_PATH + "/portfolio.dll");
            prop.store(oOut, "");
            oOut.close();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static synchronized PFStore getInstance() {
        if (selfRef == null)
            selfRef = new PFStore();
        return selfRef;
    }

    public ArrayList getPortfolioList() {
        return portfolioList;
    }

    public ArrayList getTransactionList() {
        return transactionRecords;
    }

    public ArrayList getFilteredTransactionList() {
        return filteredTransactions;
    }

    public void addPortfolio(PortfolioRecord record) {
        portfolioList.add(record);
    }

    public boolean isSufficientQuantityAvailable(long pfID, String sKey, int qty) {
        int availQty = 0;
        TransactRecord pfRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if (isSymbolSpecificTransaction(pfRec.getTxnType())) {
                if ((pfRec.getPfID() == pfID) && (pfRec.getSKey().equals(sKey))) {
                    switch (pfRec.getTxnType()) {
                        case PFUtilities.BUY:
                        case PFUtilities.OP_BAL:
                            availQty += pfRec.getQuantity();
                            break;
                        case PFUtilities.SELL:
                            availQty -= pfRec.getQuantity();
                            break;
                    }
                }
            }
            pfRec = null;
        }
        if (availQty >= qty)
            return true;
        else
            return false;
    }

    public int getAvailableQuantity(long pfID, String sKey) {
        int availQty = 0;
        TransactRecord pfRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if (isSymbolSpecificTransaction(pfRec.getTxnType())) {
                if ((pfRec.getPfID() == pfID) && (pfRec.getSKey().equals(sKey))) {
                    switch (pfRec.getTxnType()) {
                        case PFUtilities.BUY:
                        case PFUtilities.OP_BAL:
                            availQty += pfRec.getRemainingHolding();
                            break;
                        case PFUtilities.SELL:
                            //  availQty -= pfRec.getQuantity();
                            break;
                    }
                }
            }
            pfRec = null;
        }
        return availQty;
    }

    public boolean isPortfolioNameExists(String name) {
        PortfolioRecord pfRec = null;
        for (int i = 0; i < portfolioList.size(); i++) {
            pfRec = (PortfolioRecord) portfolioList.get(i);
            if (pfRec.getName().equalsIgnoreCase(name)) {
                return true;
            }
            pfRec = null;
        }
        return false;
    }

    public PortfolioRecord getPortfolio(int index) {
//        PortfolioRecord pfRec  = null;
//        for (int i = 0; i < portfolioList.size(); i++) {
//            pfRec = (PortfolioRecord)portfolioList.get(i);
//            if (pfRec.getPfID() == pfID) {
//                break;
//            }
//            pfRec = null;
//        }
        try {
            return (PortfolioRecord) portfolioList.get(index);
        } catch (Exception ex) {
            return null;
        }
    }

    public PortfolioRecord getPortfolio(long pfID) {
        PortfolioRecord pfRec = null;
        for (int i = 0; i < portfolioList.size(); i++) {
            pfRec = (PortfolioRecord) portfolioList.get(i);
            if (pfRec.getPfID() == pfID) {
                break;
            }
            pfRec = null;
        }
        return pfRec;
    }

    public String getPortfolioName(long pfID) {
        PortfolioRecord pfRec = null;
        for (int i = 0; i < portfolioList.size(); i++) {
            pfRec = (PortfolioRecord) portfolioList.get(i);
            if (pfRec.getPfID() == pfID) {
//                System.out.println("PF NAME " + pfID + " " + pfRec.getName());
                return pfRec.getName();
            }
            pfRec = null;
        }
        return "";
    }

    public void deletePortfolio(long pfID) {
        String sKey = null;
        ArrayList<String> sKeys = new ArrayList<String>();
        TransactRecord pfRec = null;
        PortfolioRecord portflio = null;
//System.out.println("Deleting PF " + pfID);
        for (int i = 0; i < portfolioList.size(); i++) {
            portflio = (PortfolioRecord) portfolioList.get(i);
            if (portflio.getPfID() == pfID) {
                portfolioList.remove(i);
                break;
            }
            portflio = null;
        }
        portflio = null;

        // First remove from the Transaction log
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if (pfRec.getPfID() == pfID) {
                if (isSymbolSpecificTransaction(pfRec.getTxnType())) {
                    sKeys.add(pfRec.getSKey());
                }
//                sKey = pfRec.getSKey();
                transactionRecords.remove(i);
//                break;
            }
            pfRec = null;
        }

        // Second remove from the filtered Transaction log
        for (int i = 0; i < filteredTransactions.size(); i++) {
            pfRec = (TransactRecord) filteredTransactions.get(i);
            if (pfRec.getPfID() == pfID) {
                filteredTransactions.remove(i);
//                break;
            }
            pfRec = null;
        }
        pfRec = null;

        ValuationRecord valRec = null;
        try {
            for (int i = 0; i < valuationRecords.size(); i++) {
                valRec = (ValuationRecord) valuationRecords.get(i);
                if (sKeys.contains(valRec.getSKey())) {     // valRec.getSKey().equals(sKey)) {
                    valuationRecords.remove(i);
                    PortfolioInterface.removeSymbol(sKey);  // Added on 13-05-03
                }
                valRec = null;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        sKeys = null;
        sKey = null;

//        createFilteredTransactionList(null, PFRecord.NONE, null, null);
    }

    public ValuationRecord getValuationRecord(String sKey) {
        ValuationRecord valRec = null;
        for (int i = 0; i < valuationRecords.size(); i++) {
            valRec = (ValuationRecord) valuationRecords.get(i);
            if (valRec.getSKey().equals(sKey)) {
                break;
            }
            valRec = null;
        }
        return valRec; //(ValuationRecord)valuationRecords.get(sKey);
    }

    public ArrayList getValuationList() {
        return valuationRecords;
    }

    public synchronized void addTransaction(TransactRecord record) {
        int noOfRecords = transactionRecords.size();
        boolean isAdded = false;
        TransactRecord oldRec = null;
        for (int i = 0; i < noOfRecords; i++) {
            oldRec = transactionRecords.get(i);
            if (oldRec.getTxnDate() >= record.getTxnDate()) {
                transactionRecords.add(i, record);
                isAdded = true;
                break;
            } else if (oldRec.getTxnDate() < record.getTxnDate()) {
                continue;
            }
            oldRec = null;
        }
        if (!isAdded) {
            transactionRecords.add(record);
            isAdded = false;
        }

//        record.getTxnDate()
//        System.out.println("Adding record " + record.getSKey() + " " + record.getPrice() + " " + record.getQuantity());
//        try {
//            transactionRecords.add(record);
//            createFilteredTransactionList(null, PFRecord.NONE, null, null);

//        } catch (Exception ex) {
//            ex.printStackTrace();
//        }
    }

    public TransactRecord getTransaction(long txnID) {
        TransactRecord pfRec = null;
        for (int i = 0; i < filteredTransactions.size(); i++) {
            pfRec = (TransactRecord) filteredTransactions.get(i);
            if (pfRec.getId() == txnID) {
                break;
            }
            pfRec = null;
        }
        return pfRec;
    }

    public static double convertToSelectedCurrency(String baseCurrency, double value) {
        return value * CurrencyStore.getRate(baseCurrency, PFStore.getBaseCurrency());

    }

    public static double convertToSelectedPortfolioCurrency(String trnsCurr, String pfCurr, double value) {
        return value * CurrencyStore.getRate(trnsCurr, pfCurr);
    }

    private void undoTransaction(TransactRecord record) {
        PortfolioRecord pfRecord = this.getPortfolio(record.getPfID());
        double cashBalance = pfRecord.getCashBalance();
        String pfCurr = pfRecord.getCurrency();
        float pModifyFact = 1.0f;
        String exchg = SharedMethods.getExchangeFromKey(record.getSKey());
        if (exchg != null && (!exchg.isEmpty())) {
            pModifyFact = ExchangeStore.getSharedInstance().getExchange(exchg).getPriceModificationFactor();
        }

        switch (record.getTxnType()) {
            case PFUtilities.OP_BAL:
                break;
            case PFUtilities.BUY:
                cashBalance += convertToSelectedPortfolioCurrency(record.getCurrency(), pfCurr, record.getBrokerage()) + (convertToSelectedPortfolioCurrency(record.getCurrency(), pfCurr, record.getPrice()) * record.getQuantity()) / pModifyFact;
                break;
            case PFUtilities.SELL:
                cashBalance -= ((convertToSelectedPortfolioCurrency(record.getCurrency(), pfCurr, record.getPrice()) * record.getQuantity()) / pModifyFact) - convertToSelectedPortfolioCurrency(record.getCurrency(), pfCurr, record.getBrokerage());
                //    cashBalance -= (convertToSelectedCurrency(record.getCurrency(),record.getPrice()) * record.getQuantity() - record.getBrokerage()) ;
                break;
            case PFUtilities.DIVIDEND:
            case PFUtilities.DEPOSITE:
            case PFUtilities.REFUNDS:
                cashBalance -= convertToSelectedPortfolioCurrency(record.getCurrency(), pfCurr, record.getPrice());
                break;
            case PFUtilities.WITHDRAWAL:
            case PFUtilities.CHARGES:
                cashBalance += convertToSelectedPortfolioCurrency(record.getCurrency(), pfCurr, record.getPrice());
                break;
        }
        pfRecord.setCashBalance(cashBalance);

        pfRecord = null;
        record = null;
    }

    public double getWeightedAvgCostForTheTransaction(long txnID) {
        double wAvgCost = 0f;
        double value = 0f;
        double totCost = 0f;
        long holding = 0;

        TransactRecord record = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            record = transactionRecords.get(i);
            if (record.getId() < txnID) {
                switch (record.getTxnType()) {
                    case PFUtilities.OP_BAL:
                    case PFUtilities.BUY:
                        totCost = record.getQuantity() * record.getPrice() + record.getBrokerage();
                        holding += record.getQuantity();
                        value += totCost;
                        wAvgCost = value / holding;
                        break;
                    case PFUtilities.SELL:
                        holding -= record.getQuantity();
                        value -= wAvgCost * record.getQuantity();
                        break;
                }
            } else if (record.getId() == txnID) {
                break;
            }
            record = null;
        }

        return wAvgCost;
    }

    public TransactRecord deleteTransaction(long txnID, boolean reduceCashBalance) {
        TransactRecord record = null;
        // First remove from the Transaction log
        for (int i = 0; i < transactionRecords.size(); i++) {
            TransactRecord pfRec = (TransactRecord) transactionRecords.get(i);
            if (pfRec.getId() == txnID) {
                if (reduceCashBalance) {
                    undoTransaction((TransactRecord) transactionRecords.remove(i));
                } else {
                    transactionRecords.remove(i);
                }
                record = pfRec;
                break;
            }
            pfRec = null;
        }

        // Second remove from the filtered Transaction log
        for (int i = 0; i < filteredTransactions.size(); i++) {
            TransactRecord pfRec = (TransactRecord) filteredTransactions.get(i);
            if (pfRec.getId() == txnID) {
                filteredTransactions.remove(i);
                break;
            }
            pfRec = null;
        }

        /* Removed 5 May 2005 Uditha
        ValuationRecord valRec = null;
        for (int i = 0; i < valuationRecords.size(); i++) {
            valRec = (ValuationRecord)valuationRecords.get(i);
            if (valRec.getSKey().equals(sKey)) {
                valuationRecords.remove(i);
                PortfolioInterface.removeSymbol(sKey);  // Added on 13-05-03
            }
            valRec = null;
        }*/
        return record;
    }

    public synchronized void createFilteredTransactionList(long[] pfIDs, String sKey, byte type, Date startDate, Date endDate) {
        boolean isPFSelected = false;
        boolean isNotRelevant = false;
        TransactRecord pfRec = null;

        if (pfIDs == null)
            return;
        filteredTransactions.clear();
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = (TransactRecord) transactionRecords.get(i);

            for (int j = 0; j < pfIDs.length; j++) {

                if (pfRec.getPfID() == pfIDs[j]) {
                    isPFSelected = true;
                    break;
                }
            }
            if (isPFSelected) {
                // Apply the conditions to filter the history records
                if ((sKey != null) && (!sKey.equalsIgnoreCase(pfRec.getSKey())))
                    isNotRelevant = true;
                if ((type != PFUtilities.NONE) && (type != pfRec.getTxnType()))
                    isNotRelevant = true;
                if (startDate != null) {
                    // To apply the date filter startDate should not be NULL. Other wise date filter is not applied.
                    // If teh endDate is NULL, current time is taken as the end date.
                    if (endDate == null)
                        endDate = new Date();
                    Date recordedDate = new Date(pfRec.getTxnDate());
//                    if (recordedDate.before(startDate) || recordedDate.after(endDate))
                    if (PFUtilities.isPriorDate(recordedDate, startDate) || PFUtilities.isPriorDate(endDate, recordedDate))
                        isNotRelevant = true;
                    recordedDate = null;
                } else {
                    if (endDate == null)
                        endDate = new Date();
                    Date recordedDate = new Date(pfRec.getTxnDate());
//                    if (recordedDate.after(endDate))
                    if (PFUtilities.isPriorDate(endDate, recordedDate))
                        isNotRelevant = true;
                    recordedDate = null;
                }
                // Ignore the current record
                if (isNotRelevant) {
                    isNotRelevant = false;
                    pfRec = null;
                    isPFSelected = false;
                    continue;
                } else {
                    filteredTransactions.add(pfRec);
                    pfRec = null;
                }
                isPFSelected = false;
            }
        }
    }

    /*  public synchronized void createValuationList(long[] pfIDs, Date endDate) {
    //        public void createValuationList(int[] pfIDs, String sKey, byte type, Date startDate, Date endDate) {
            boolean isNewRecord = false;
            boolean isPFSelected = false;
            boolean isNotRelevant = false;
            TransactRecord pfRec = null;
            ValuationRecord valRec = null;

            if (pfIDs == null)
                return;

    //System.out.println("creating ValuationList");
            valuationRecords.clear();
            for (int i = 0; i < transactionRecords.size(); i++) {
                pfRec = (TransactRecord) transactionRecords.get(i);
                if((pfRec.getTxnType()==PFUtilities.BUY) || (pfRec.getTxnType()==PFUtilities.SELL) ||
                        (pfRec.getTxnType()==PFUtilities.OP_BAL) || (pfRec.getTxnType()==PFUtilities.DIVIDEND)){
                    // Apply the conditions to filter the history records
        //            if ((sKey != null) && (!sKey.equalsIgnoreCase(pfRec.getSKey())))
        //                isNotRelevant = true;
        //            if ((type != PFRecord.NONE) && (type != pfRec.getTxnDate()))
        //                isNotRelevant = true;
        //            if (startDate != null) {
                    // To apply the date filter startDate should not be NULL. Other wise date filter is not applied.
                    // If teh endDate is NULL, current time is taken as the end date.
                    if (endDate == null)
                        endDate = new Date();
                    Date recordedDate = new Date(pfRec.getTxnDate());
        //                if (recordedDate.before(startDate) || recordedDate.after(endDate))
                    if (recordedDate.after(endDate))
                        isNotRelevant = true;
                    recordedDate = null;
        //            }
                    // Ignore the current record
                    if (isNotRelevant) {
                        isNotRelevant = false;
                        continue;
                    }

                    for (int j = 0; j < pfIDs.length; j++) {
                        if (pfRec.getPfID() == pfIDs[j]) {
                            isPFSelected = true;
                            break;
                        }
                    }

                    if (isPFSelected) {
        //                ValuationRecord valRec = null;
        //                boolean isKeyFound;
                        for (int i2 = 0; i2 < valuationRecords.size(); i2++) {
                            valRec = (ValuationRecord) valuationRecords.get(i2);
                            if (valRec.getSKey().equals(pfRec.getSKey())) {
                                break;
                            }
                            valRec = null;
                        }

                        if (valRec == null) {
        //                if (valuationRecords.containsKey(pfRec.getSKey())) {
        //                    valRec = (ValuationRecord)valuationRecords.get(pfRec.getSKey());
        //                } else {
                            isNewRecord = true;
                            valRec = new ValuationRecord();
                            valRec.setSKey(pfRec.getSKey());
                            valRec.setCurrency(pfRec.getCurrency());

                            switch (pfRec.getTxnType()) {
                                case PFUtilities.OP_BAL :
                                case PFUtilities.BUY :
                                    valRec.setHolding(pfRec.getRemainingHolding() );                      // Holding = Qty
                                   // valRec.setCumCost(pfRec.getQuantity() * pfRec.getPrice() + pfRec.getBrokerage());   // Holding * Price + Brokerage
                                    if(FIFOSellMode){
                                       //valRec.setCumCost(valRec.getCumCost() - pfRec.getSoldHolding()  * pfRec.getPrice());
                                       valRec.setCumCost(pfRec.getRemainingHolding()  * pfRec.getPrice());
                                    }else{
                                       valRec.setCumCost(pfRec.getQuantity() * pfRec.getPrice() + pfRec.getBrokerage());
                                    }
                                    //   valRec.setAvgCost(pfRec.getPrice());                        // Tot Cost / Holding
                                    valRec.setAvgCost(valRec.getCumCost() / valRec.getHolding());
                                    break;
                                case PFUtilities.SELL :
                                   // valRec.setHolding(0 - pfRec.getQuantity());                      // Holding = Qty
        //                            valRec.setTotCost(pfRec.getQuantity() * getWeightedAvgCostForTheTransaction(pfRec.getId()));
        //                            valRec.setAvgCost(valRec.getAvgCost());
                                    if(!FIFOSellMode){
                                    valRec.setCumCost(0 - pfRec.getQuantity() * pfRec.getPrice());//  - pfRec.getBrokerage());   // Holding * Price - Brokerage
                                    }
                                        valRec.setAvgCost(pfRec.getPrice());                        // Tot Cost / Holding
                                    break;
                                case PFUtilities.DIVIDEND :
        //                            System.out.println("Adding DIVIDEND for " + pfRec.getSKey());
                                    valRec.setCashDividends(pfRec.getPrice());
                                    break;
                            }
        //                    valRec.setHolding(pfRec.getHolding());                      // Holding = Qty
        //                    valRec.setTotCost(pfRec.getHolding() * pfRec.getPrice());   // Holding * Price
        //                    valRec.setAvgCost(pfRec.getPrice());                        // Tot Cost / Holding

                            // Have to calculate the other field values
                            valuationRecords.add(valRec);
        //                    valuationRecords.put(pfRec.getSKey(), valRec);
                        }
                        if (!isNewRecord) {
                            switch (pfRec.getTxnType()) {
                                case PFUtilities.OP_BAL :
                                case PFUtilities.BUY :
                                    valRec = calculateBuyTransactions(valRec, pfRec);
                                    break;
                                case PFUtilities.SELL :
                                    valRec = calculateSellTransactions(valRec, pfRec);
                                    break;
                                case PFUtilities.DIVIDEND :
                                    valRec.setCashDividends(pfRec.getPrice() + valRec.getCashDividends());
                                    break;
                            }
                        }
                        if (valRec.getHolding() == 0) {
                            for (int i2 = 0; i2 < valuationRecords.size(); i2++) {
                                if (valRec.getSKey().equals(((ValuationRecord) valuationRecords.get(i2)).getSKey())) {
                                    valuationRecords.remove(i2);
                                    break;
                                }
                            }
                        }
                        valRec = null;
                        isNewRecord = false;
                        isPFSelected = false;
                    }
                    pfRec = null;
                }
    //            System.out.println("valuationRecords size = " + valuationRecords.size());
            }
        }

        private ValuationRecord calculateBuyTransactions(ValuationRecord valRec, TransactRecord pfRec) {
            valRec.setHolding(valRec.getHolding() + pfRec.getRemainingHolding());
           // valRec.setCumCost(valRec.getCumCost() + pfRec.getQuantity() * pfRec.getPrice() + pfRec.getBrokerage());
            if(FIFOSellMode){
               //valRec.setCumCost(valRec.getCumCost() - pfRec.getSoldHolding()  * pfRec.getPrice());
               valRec.setCumCost(pfRec.getRemainingHolding()  * pfRec.getPrice() + pfRec.getBrokerage());
            }else{
                valRec.setCumCost(valRec.getCumCost() + pfRec.getQuantity() * pfRec.getPrice() + pfRec.getBrokerage());
            }
             valRec.setAvgCost(valRec.getCumCost() / valRec.getHolding());
            return valRec;
        }

        private ValuationRecord calculateSellTransactions(ValuationRecord valRec, TransactRecord pfRec) {
          //  valRec.setHolding(valRec.getHolding() - pfRec.getQuantity());
    //        valRec.setTotCost(valRec.getAvgCost() * valRec.getHolding());     // it's not changing, since it's a sell transaction

           if(!FIFOSellMode){
            valRec.setCumCost(valRec.getCumCost() - pfRec.getQuantity() * valRec.getAvgCost()); // - pfRec.getBrokerage());
           }else{

           }
    //        valRec.setTotCost(valRec.getTotCost() - pfRec.getQuantity() * getWeightedAvgCostForTheTransaction(pfRec.getId())); // - pfRec.getBrokerage());
    //        valRec.setTotCost(pfRec.getQuantity() * getWeightedAvgCostForTheTransaction(pfRec.getId()));
    //        valRec.setTotCost(valRec.getTotCost() - pfRec.getHolding() * pfRec.getPrice() + pfRec.getBrokerage());
    //        valRec.setAvgCost(valRec.getTotCost() / valRec.getHolding());     // it's not changing, since it's a sell transaction
            return valRec;
        }
    */


    public synchronized void createValuationList(long[] pfIDs, Date endDate) {
//        public void createValuationList(int[] pfIDs, String sKey, byte type, Date startDate, Date endDate) {
        boolean isPFSelected = false;
        boolean isNotRelevant = false;
        TransactRecord pfRec = null;
        ValuationRecord valRec = null;

        if (pfIDs == null)
            return;

//System.out.println("creating ValuationList");
        valuationRecords.clear();
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if ((pfRec.getTxnType() == PFUtilities.BUY) || (pfRec.getTxnType() == PFUtilities.SELL) ||
                    (pfRec.getTxnType() == PFUtilities.OP_BAL) || (pfRec.getTxnType() == PFUtilities.DIVIDEND)) {
                // Apply the conditions to filter the history records
                //            if ((sKey != null) && (!sKey.equalsIgnoreCase(pfRec.getSKey())))
                //                isNotRelevant = true;
                //            if ((type != PFRecord.NONE) && (type != pfRec.getTxnDate()))
                //                isNotRelevant = true;
                //            if (startDate != null) {
                // To apply the date filter startDate should not be NULL. Other wise date filter is not applied.
                // If teh endDate is NULL, current time is taken as the end date.
                if (endDate == null) {
                    endDate = new Date();
                }
                Date recordedDate = new Date(pfRec.getTxnDate());
                //                if (recordedDate.before(startDate) || recordedDate.after(endDate))
                if (recordedDate.after(endDate)) {
                    isNotRelevant = true;
                }
                recordedDate = null;
                //            }
                // Ignore the current record
                if (isNotRelevant) {
                    isNotRelevant = false;
                    continue;
                }

                for (int j = 0; j < pfIDs.length; j++) {
                    if (pfRec.getPfID() == pfIDs[j]) {
                        isPFSelected = true;
                        break;
                    }
                }
                if (isPFSelected) {
                    for (int i2 = 0; i2 < valuationRecords.size(); i2++) {
                        valRec = (ValuationRecord) valuationRecords.get(i2);
                        if (valRec.getSKey().equals(pfRec.getSKey())) {
                            break;
                        }
                        valRec = null;
                    }

                    if (valRec == null) {
                        valRec = new ValuationRecord();
                        valRec.setSKey(pfRec.getSKey());
                        valRec.setCurrency(pfRec.getCurrency());
                        valRec.setFIFOSell(FIFOSellMode);
                        valuationRecords.add(valRec);
                    }
                    switch (pfRec.getTxnType()) {
                        case PFUtilities.OP_BAL:
                        case PFUtilities.BUY:
                            valRec = calculateBuyTransactions(valRec, pfRec);
                            break;
                        case PFUtilities.SELL:
                            valRec = calculateSellTransactions(valRec, pfRec);
                            break;
                        case PFUtilities.DIVIDEND:
                            valRec.setCashDividends(pfRec.getPrice() + valRec.getCashDividends());
                            break;
                    }
//                    if (!SHOWZEROHOLDINGS && valRec.getHolding() == 0) {
//                        for (int i2 = 0; i2 < valuationRecords.size(); i2++) {
//                            if (valRec.getSKey().equals(((ValuationRecord) valuationRecords.get(i2)).getSKey())) {
//                                valuationRecords.remove(i2);
//                                break;
//                            }
//                        }
//                    }
                    valRec = null;
                    isPFSelected = false;
                }
                pfRec = null;
            }
//            System.out.println("valuationRecords size = " + valuationRecords.size());
        }

        if (!SHOWZEROHOLDINGS) {
//            for (int i2 = 0; i2 < valuationRecords.size(); i2++) {
//                ValuationRecord rec = (ValuationRecord) valuationRecords.get(i2);
//                if (rec.getHolding() == 0) {
//                    valuationRecords.remove(i2);
//                }
//            }
            int elements = valuationRecords.size();
            for (int firstLoop = 0; firstLoop < elements; firstLoop++) {
            for (int i2 = 0; i2 < valuationRecords.size(); i2++) {
                ValuationRecord rec = (ValuationRecord) valuationRecords.get(i2);
                if (rec.getHolding() == 0) {
                    valuationRecords.remove(i2);
                        break;
                    }
                }
            }
        }
    }

    private ValuationRecord calculateBuyTransactions(ValuationRecord valRec, TransactRecord pfRec) {
        valRec.setHolding(valRec.getHolding() + pfRec.getRemainingHolding());
        valRec.setRealizedGain(valRec.getRealizedGain() + pfRec.getRealizedGain());
        // valRec.setCumCost(valRec.getCumCost() + pfRec.getRemainingHolding() * pfRec.getPrice() + pfRec.getBrokerage());
        if (FIFOSellMode) {
            // valRec.setCumCost(valRec.getCumCost() - pfRec.getSoldHolding()  * pfRec.getPrice());
            valRec.setCumCost(valRec.getCumCost() + pfRec.getRemainingHolding() * pfRec.getPrice() + pfRec.getBrokerage());
        } else if(pfRec.getRemainingHolding() != 0){
            calcAvgCost(valRec, pfRec);


        }
           valRec.setAvgCost(valRec.getCumCost() / valRec.getHolding());
        return valRec;
    }

    private ValuationRecord calculateSellTransactions(ValuationRecord valRec, TransactRecord pfRec) {
//        valRec.setHolding(valRec.getHolding() - pfRec.getQuantity());
        //    valRec.setRealizedGain(valRec.getRealizedGain() + pfRec.getRealizedGain());
       if(!FIFOSellMode){
            valRec.setCumCost(valRec.getCumCost() - pfRec.getQuantity() * valRec.getAvgCost()); // - pfRec.getBrokerage());
       }
        return valRec;
    }

    public void calcAvgCost(ValuationRecord valRec, TransactRecord pfRec) {

        double thisRecVAlue = pfRec.getTransactionCost();
        int thisRecQty = pfRec.getQtyforWA();

        valRec.setTotCostForWACalculation(valRec.getTotCostForWACalculation() + thisRecVAlue);
        valRec.setTotQtyWACalculation(valRec.getTotQtyWACalculation() + thisRecQty);
    }

    private double getRealizedGainLossForValuation(ValuationRecord valRec) {
        return 0;
    }

    public void clear() {
        transactionRecords.clear();
        valuationRecords.clear();
        portfolioList.clear();
        filteredTransactions.clear();
    }

    public static String getBaseCurrency() {
        return baseCurrency;
    }

    public void setBaseCurrency(String newBaseCurrency) {
        baseCurrency = newBaseCurrency;
        prop.setProperty("PORTFOLIO_SIMULATOR_BASE_CURRENCY", newBaseCurrency);
        try {
            FileOutputStream oOut = new FileOutputStream(Settings.SYSTEM_PATH + "/portfolio.dll");
            prop.store(oOut, "");
            oOut.close();
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
//        prop.getProperty("PORTFOLIO_SIMULATOR_BASE_CURRENCY");
        //Settings.setItem("PORTFOLIO_SIMULATOR_BASE_CURRENCY", newBaseCurrency);
    }

    public PortfolioRecord getDefaultPortfolio() {
        PortfolioRecord record = null;
        for (int i = 0; i < portfolioList.size(); i++) {
            record = (PortfolioRecord) portfolioList.get(i);
            if (record.isDefault()) {
                return record;
            }
            record = null;
        }
        return null;
    }

    public void setDefaultPortfolio(long pfID) {
        PortfolioRecord record = null;
        for (int i = 0; i < portfolioList.size(); i++) {
            record = (PortfolioRecord) portfolioList.get(i);
            if (record.getPfID() == pfID) {
                record.setDefaultPortfolio(true);
            } else {
                record.setDefaultPortfolio(false);
            }
            record = null;
        }
    }

    public static synchronized void saveTransactions(String fileName) {
        TransactRecord record = null;
        PortfolioRecord pfRecord = null;
        File file = null;
        FileOutputStream fOut = null;

        try {
            file = new File(fileName);
            fOut = new FileOutputStream(file, false);

            for (int i = 0; i < portfolioList.size(); i++) {
                pfRecord = (PortfolioRecord) portfolioList.get(i);
                if (pfRecord != null)
                    fOut.write((UnicodeUtils.getUnicodeString(pfRecord.toString(PFUtilities.DATA)) + "\n").getBytes());
                pfRecord = null;
            }

            for (int i = 0; i < transactionRecords.size(); i++) {
                record = (TransactRecord) transactionRecords.get(i);
                if (record != null)
                    fOut.write(record.toString(PFUtilities.DATA).getBytes());
                record = null;
            }
            SplitRecord split = null;
            try {
                Enumeration<SplitRecord> splits = splitStore.elements();
                split = null;
                while (splits.hasMoreElements()) {
                    split = splits.nextElement();
                    if (split != null) {
                        fOut.write(split.toString(PFUtilities.DATA).getBytes());
                    }
                    split = null;
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            try {
                Enumeration<CommissionObject> comms = commissionStore.elements();
                CommissionObject comm = null;
                while (comms.hasMoreElements()) {
                    comm = comms.nextElement();
                    if (comm != null) {
                        fOut.write(comm.toString(PFUtilities.DATA).getBytes());
                    }
                    comm = null;
                }
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            fOut.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        file = null;
        fOut = null;
    }

    public void loadTransactions(String sFileName) {
        byte type = 0;
        int transID = 0;
        boolean isdone = false;
        String sLine = null;
        TransactRecord record = null;
        SplitRecord split = null;
        CommissionObject comm = null;
        File file = null;
        FileInputStream fIn = null;
        InputStreamReader inputR = null;
        BufferedReader in = null;
        PortfolioRecord pfRecord = null;

        try {
            file = new File(sFileName);
            fIn = new FileInputStream(file);
            inputR = new InputStreamReader(fIn);
            in = new BufferedReader(inputR);
            isdone = false;

            clear();    // Initialize all the datastores before loading new transactions
            while (!isdone) {
                sLine = in.readLine();
                if (sLine != null) {
//                    System.out.println("> " + sLine);
                    sLine = UnicodeUtils.getNativeString(sLine);
                    type = Byte.parseByte(sLine.substring(0, sLine.indexOf("|")));
                    sLine = sLine.substring(sLine.indexOf("|") + 1);
                    switch (type) {
                        case PFUtilities.PORTFOLIO_RECORD:
                            pfRecord = new PortfolioRecord();
                            pfRecord.retrievePortfolio(sLine);
                            portfolioList.add(pfRecord);
                            pfRecord = null;
                            break;
                        case PFUtilities.TRANSACTION_RECORD:
                            record = new TransactRecord();
                            record.retrieveTransaction(sLine);
                            transactionRecords.add(record);
                            record = null;
                            break;
                        case PFUtilities.SPLIT_RECORD:
                            split = new SplitRecord();
                            split.retrieveSplit(sLine);
                            splitStore.put("" + split.getId(), split);
                            record = null;
                            break;
                        case PFUtilities.COMMISSION_RECORD:
                            comm = new CommissionObject();
                            comm.retrieveCommission(sLine);
                            commissionStore.put(comm.getTimeStamp(), comm);
                            commissionList.add(comm.getId());
                            record = null;
                            break;
                    }
                } else {
                    isdone = true;
                }
            }
            in.close();
            inputR.close();

//            for (int i = 0; i < transactionRecords.size(); i++) {
//                record = (PFRecord)transactionRecords.get(i);
//                if (record != null)
//                    fIn.write(record.toString(PFRecord.TXT).getBytes());
//                record = null;
//            }
            fIn.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
//        createFilteredTransactionList(null, PFRecord.NONE, null, null);
        file = null;
        fIn = null;
    }

    public void clearAllStores() {
        portfolioList.clear();
        transactionRecords.clear();
        filteredTransactions.clear();
        valuationRecords.clear();
        if (unregisteredSymbols != null) {
            unregisteredSymbols.clear();
        }
        allValuationRecords.clear();
        splitStore.clear();
        commissionStore.clear();
        commissionList.clear();

    }

    public static boolean isContainedSymbol(String symbol) {
        TransactRecord valRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            valRec = (TransactRecord) transactionRecords.get(i);
            if (PortfolioInterface.getSymbolFromKey(valRec.getSKey()).equals(symbol)) {
                valRec = null;
                return true;
            }
            valRec = null;
        }
        return false;
    }

    public static boolean isContainedKey(String sKey) {
        try {
            TransactRecord valRec = null;
            if (transactionRecords == null) {
                return false;
            }
            for (int i = 0; i < transactionRecords.size(); i++) {
                valRec = (TransactRecord) transactionRecords.get(i);
                SharedMethods.printLine("PFStore: isContainedKey " + sKey + " " + valRec.getSKey(), false);
                if (valRec.getSKey().equals(sKey)) {
                    valRec = null;
                    return true;
                }
                valRec = null;
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return false;
    }

    public static String getKeyForValidSymbol(String symbol) {
        TransactRecord valRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            valRec = (TransactRecord) transactionRecords.get(i);
            if (PortfolioInterface.getSymbolFromKey(valRec.getSKey()).equals(symbol)) {
//                valRec = null;
                return valRec.getSKey();
            }
            valRec = null;
        }
        return null;
    }

    public static String[] getCurrentSymbolList() {


        String[] list = new String[valuationRecords.size()];
        ValuationRecord valRec = null;
//        System.out.println("valuationRecords " + valuationRecords.size());
        for (int i = 0; i < valuationRecords.size(); i++) {
            valRec = (ValuationRecord) valuationRecords.get(i);
            list[i] = valRec.getSKey();
//System.out.println("---- " + list[i]);
            valRec = null;
        }
        return list;
    }

    public static String[] getAllSymbolList() {
        String[] newArray = null;
        ArrayList newList = new ArrayList();
        TransactRecord valRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            valRec = (TransactRecord) transactionRecords.get(i);
            if (!newList.contains(valRec.getSKey()) && (valRec.getTxnType() == PFUtilities.SELL || valRec.getTxnType() == PFUtilities.BUY)) {
                newList.add(valRec.getSKey());
            }
//            if (PortfolioInterface.getSymbolFromKey(valRec.getSKey()).equals(symbol)) {
//                return valRec.getSKey();
//            }
            valRec = null;
        }

        newArray = new String[newList.size()];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = (String) newList.get(i);
        }
        newList = null;
        return newArray;
    }

    public void unregisterSymbols() {
        TransactRecord pfRec = null;
        if (filteredTransactions != null) {
            unregisteredSymbols = new ArrayList();
            for (int i = 0; i < filteredTransactions.size(); i++) {
                pfRec = (TransactRecord) filteredTransactions.get(i);
                PortfolioInterface.removeSymbol(pfRec.getSKey());
                unregisteredSymbols.add(pfRec.getSKey());
                pfRec = null;
            }
            pfRec = null;
        }
    }

    public void registerSymbols() {
        if (unregisteredSymbols != null) {
            for (int i = 0; i < unregisteredSymbols.size(); i++) {
                PortfolioInterface.setSymbol(true, (String) unregisteredSymbols.get(i));
            }
            unregisteredSymbols = null;
        }
    }

    public SplitRecord createSplitRecord(long id) {
        SplitRecord record = null;
        try {
            record = splitStore.get("" + id);
            if (record != null) {
                return record;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        id = ++lastSplitID;
        record = new SplitRecord(id);
        splitStore.put("" + id, record);
        return record;
    }

    public SplitRecord getSplitRecord(long id) {
        try {
            return splitStore.get("" + id);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

    public void removeSplitRecord(long id) {
        try {
            splitStore.remove("" + id);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public ArrayList<SplitAdjustmentObject> getRecordsForSplitAdjustment(SplitRecord split) {
        ArrayList<SplitAdjustmentObject> list = new ArrayList<SplitAdjustmentObject>();
        SplitAdjustmentObject obj = null;
        for (TransactRecord record : transactionRecords) {
            try {
                //    if(record.getSKey().equals(split.getKey()) && (record.getTxnType() == PFUtilities.BUY) && (record.getTxnDate() < split.getStartDate()) && (record.getRemainingHolding()>0)){
                if (record.getSKey().equals(split.getKey()) && (record.getTxnType() == PFUtilities.BUY) && compareDatesForSplitAdjustments(split.getStartDate(), record.getTxnDate()) && (record.getRemainingHolding() > 0)) {
                    obj = new SplitAdjustmentObject(record.getId());
                    obj.setOldPrice(record.getPrice());
                    obj.setOldQty(record.getRemainingHolding());
                    obj.setPortfolioID(record.getPfID());
                    obj.setSplitID(split.getId());
                    long qty = (long) (record.getRemainingHolding() * split.getSecondFactor() / split.getFirstFactor());
                    obj.setNewQty(qty);
                    obj.setNewPrice(record.getRemainingHolding() * record.getPrice() / qty);
                    list.add(obj);
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            record = null;
        }
        return list;
    }


    private boolean compareDatesForSplitAdjustments(long splitDate, long txndate) {

        if (splitDate > txndate) {
            return true;
        } else {
            TWDateFormat formatter = new TWDateFormat("yyyy-MM-dd");
            String split = formatter.format(splitDate);
            String dt = formatter.format(txndate);
            if (split.equals(dt)) {
                return true;
            }
            return false;
        }

    }

    public void adjustSplits(ArrayList<SplitAdjustmentObject> store, SplitRecord split) {
        File file = null;
        FileOutputStream fOut = null;
        String fileName = BackUpDaemon.autoBackUpLocation + "./" + split.getId() + "_" + split.getStartDate() + "_" + split.getKey();
        TransactRecord transRecord = null;
        try {
            //  file = new File(fileName);
            //  fOut = new FileOutputStream(file, false);
            //  fOut.write(split.toString(PFUtilities.DATA).getBytes());
            for (SplitAdjustmentObject obj : store) {
                try {
                    transRecord = PFStore.getInstance().getTransaction(obj.getTransactID());
                    lockSellsEntriesOfBuyTxn(transRecord);
                    //       fOut.write(transRecord.toString(PFUtilities.DATA).getBytes());
                    createAdjustedTransactionForSplit(transRecord, obj);
                    //   transRecord.setPrice(obj.getNewPrice());
                    //    transRecord.setRemainingHolding((int)obj.getNewQty());
                    transRecord.appendSplitData(split.getId(), obj.getOldPrice(), obj.getOldQty());
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            //   fOut.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        file = null;
        fOut = null;
    }

    public void revertSplitAdjustment(SplitRecord split) {
        File file = null;
        FileInputStream fIn = null;
        String fileName = BackUpDaemon.autoBackUpLocation + "./" + split.getId() + "_" + split.getStartDate() + "_" + split.getKey();
        TransactRecord transRecord = null;
        InputStreamReader inputR = null;
        BufferedReader in = null;
        boolean isdone = false;
        TransactRecord record;
        String sLine = null;
        try {
            file = new File(fileName);
            fIn = new FileInputStream(file);
            inputR = new InputStreamReader(fIn);
            in = new BufferedReader(inputR);
            isdone = false;
            sLine = in.readLine();
            split.retrieveSplit(sLine);
            while (!isdone) {
                sLine = in.readLine();
                if (sLine != null) {
                    record = new TransactRecord();
                    record.retrieveTransaction(sLine);
                    transactionRecords.add(record);
                    record = null;
                } else {
                    isdone = true;
                }
            }
            in.close();
            inputR.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        file = null;
    }

    public static boolean isShowZeroHoldings() {
        return SHOWZEROHOLDINGS;
    }

    public static void setShowZeroHoldings(boolean showZeroHoldings) {
        PFStore.SHOWZEROHOLDINGS = showZeroHoldings;
    }

    public ArrayList getAllValuationRecords() {
        return allValuationRecords;
    }


    private void lockSellsEntriesOfBuyTxn(TransactRecord record) {

        if (record.getTxnType() == PFUtilities.BUY) {
            record.LockSellTransactions();
        }
    }

    private boolean createAdjustedTransactionForSplit(TransactRecord oldrec, SplitAdjustmentObject split) {
        int holding = oldrec.getQuantity();
        int soldholding = oldrec.getSoldHolding();
        int remainingholding = oldrec.getRemainingHolding();
        TransactRecord newRec = new TransactRecord();
        if (soldholding == 0 && remainingholding == holding) {
            oldrec.setQuantity((int) split.getNewQty());
            oldrec.setPrice(split.getNewPrice());
            return true;
        }
        if (holding == soldholding + remainingholding) {

            try {
                oldrec.setQuantity(soldholding);
                newRec.setQuantity(soldholding);
                newRec.setSKey(oldrec.getSKey());
                newRec.setTxnDate(oldrec.getTxnDate());
                newRec.setQuantity((int) split.getNewQty());
                newRec.setPrice(split.getNewPrice());
                newRec.setCurrency(oldrec.getCurrency());
                newRec.setMemo(oldrec.getMemo());
                newRec.setPfID(oldrec.getPfID());
                newRec.setTxnType(PFUtilities.BUY);
                PFStore.getInstance().addTransaction(newRec);
                return true;
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return false;
            }


        }

        return false;


    }

    public void createAllValuationList(boolean isCashIncluded) {


        allValuationRecords.clear();
        boolean isfirstEmpty = false;
        int firstVHeader = 0;         // if the first tab is empty there will be 2 headers (1actual header and  1V header) to prevent this
        if (portfolioList.size() >= 0) {
            int size = portfolioList.size();
            for (int i = 0; i < portfolioList.size(); i++) {

                long[] pfId = new long[1];
                pfId[0] = ((PortfolioRecord) portfolioList.get(i)).getPfID();

                createValuationList(pfId, null);
                if (i == 0) {
                    isfirstEmpty = valuationRecords.isEmpty();
                    if (!isfirstEmpty) {
                        firstVHeader = 1;
                    }
                }

                if (i > 0 && !valuationRecords.isEmpty() && firstVHeader >= 1) {
                    ValuationRecordAll empty = new ValuationRecordAll(ValuationRecordAll.EMPTY_ROW);
                    allValuationRecords.add(empty);
                    ValuationRecordAll vheader = new ValuationRecordAll(ValuationRecordAll.VERTUAL_HEADETR_TYPE);
                    allValuationRecords.add(vheader);


                }

                if (!valuationRecords.isEmpty()) {
                    firstVHeader += 1;
                    ValuationRecordAll namerow = new ValuationRecordAll(ValuationRecordAll.PFNAME_ROW);
                    namerow.setPfID(pfId[0]);
                    //   allValuationRecords.add(namerow);
                    for (int j = 0; j < valuationRecords.size(); j++) {
                        ValuationRecordAll allrec = new ValuationRecordAll(ValuationRecordAll.VALUATIONREC_TYPE);
                        allrec.setSuperValues((ValuationRecord) valuationRecords.get(j));
                        allrec.setPfID(pfId[0]);
                        allValuationRecords.add(allrec);

                    }

                    if (isCashIncluded) {
                        ValuationRecordAll cashrec = new ValuationRecordAll(ValuationRecordAll.PF_TOTAL_CASH_TYPE);
                        cashrec.setPfID(pfId[0]);
                        allValuationRecords.add(cashrec);
                    }
                    ValuationRecordAll totrec = new ValuationRecordAll(ValuationRecordAll.PF_TOTAL_TYPE);
                    totrec.setPfID(pfId[0]);
                    allValuationRecords.add(totrec);

                    if (i < size - 1) {
//                        ValuationRecordAll empty = new ValuationRecordAll(ValuationRecordAll.EMPTY_ROW);
//                        allValuationRecords.add(empty);
//                        ValuationRecordAll vheader = new ValuationRecordAll(ValuationRecordAll.VERTUAL_HEADETR_TYPE);
//                        allValuationRecords.add(vheader);
                    } else if (i == size - 1) {
//                        ValuationRecordAll empty = new ValuationRecordAll(ValuationRecordAll.EMPTY_ROW);
//                        allValuationRecords.add(empty);
//                        ValuationRecordAll grandTot = new ValuationRecordAll(ValuationRecordAll.PF_GRAND_TOTAL_TYPE);
//                        allValuationRecords.add(grandTot);
                    }
                }

                if (i == size - 1) {
                    ValuationRecordAll empty = new ValuationRecordAll(ValuationRecordAll.EMPTY_ROW);
                    allValuationRecords.add(empty);
                    ValuationRecordAll grandTot = new ValuationRecordAll(ValuationRecordAll.PF_GRAND_TOTAL_TYPE);
                    allValuationRecords.add(grandTot);

                }
            }
        }
    }


    public long[] getPortfolioIDList() {

        long[] list = new long[portfolioList.size()];
        for (int i = 0; i < portfolioList.size(); i++) {
            list[i] = ((PortfolioRecord) portfolioList.get(i)).getPfID();
        }

        return list;
    }

    public CommissionObject addCommission(String id) {
//        CommissionObject comm = null;
//        try {
//            comm = commissionStore.get(id);
//            if(comm == null){
//                comm = new CommissionObject(id);
//                commissionStore.put(id, comm);
//                commissionList.add(id);
//                Collections.sort(commissionList);
//            }
//        } catch (Exception e) {
//            comm = new CommissionObject(id);
//            commissionStore.put(id, comm);
//            commissionList.add(id);
//            Collections.sort(commissionList);
//        }
//        return comm;
        return null;
    }

    public void addCommission(CommissionObject object) {
        if (object != null) {
            commissionStore.put(object.getTimeStamp(), object);
            commissionList.add(object.getId());
            Collections.sort(commissionList);
        }

    }

    public boolean isNameValid(String name) {
        return commissionList.contains(name);

    }

    public CommissionObject getCommission(Long timestamp) {

        try {
            return commissionStore.get(timestamp);
        } catch (Exception e) {
            return null;
        }
    }

    public CommissionObject getCommission(String id) {

        Enumeration<Long> enu = commissionStore.keys();
        while (enu.hasMoreElements()) {

            Long timeStamp = enu.nextElement();
            CommissionObject obj = commissionStore.get(timeStamp);
            if (obj.getId().equals(id)) {
                return obj;
            }

        }

        return null;
    }

    public void removeCommission(String id, long tstamp) {
        try {
            commissionStore.remove(tstamp);
            commissionList.remove(id);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void editCommision(CommissionObject obj, String oldName) {

        commissionList.remove(oldName);
        commissionList.add(obj.getId());
        Collections.sort(commissionList);

    }


    public ArrayList<String> getCommissionList() {
        return commissionList;
    }

    public Enumeration<Long> getCommissionListIDs() {
        return commissionStore.keys();
    }


    public Hashtable<Long, CommissionObject> getCommissionStore() {
        return commissionStore;
    }
}