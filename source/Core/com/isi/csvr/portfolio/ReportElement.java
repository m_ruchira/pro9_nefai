package com.isi.csvr.portfolio;


/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2003</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public class ReportElement {

    private String title;
    private String[] elements;
    private double[] values;
    private double[] percents;
    private float total;
    private String description;

    public ReportElement(String title, String[] elements, double[] values, String desc) {
        this.title = title;
        this.elements = elements;
        this.values = values;
        this.percents = calculatePercents(values);
        this.description = desc;
    }

    double[] calculatePercents(double[] values) {
        total = 0;
        double[] perc = new double[values.length];
        for (int i = 0; i < values.length; i++) {
            total += values[i];
        }
        for (int i = 0; i < values.length; i++) {
            if(total!=0){
                perc[i] = values[i] * 100f / total;
            }
        }
        return perc;
    }

    public String getTitle() {
        return title;
    }

    public String[] getElements() {
        return elements;
    }

    public double[] getValues() {
        return values;
    }

    public double[] getPercents() {
        return percents;
    }

    public float getTotal() {
        return total;
    }

    public String getDescription() {
        return description;
    }

}


