package com.isi.csvr.portfolio;

import com.isi.csvr.shared.Settings;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class BackUpDaemon implements Runnable {

    private static boolean isAutoBackUpEnabled;
    private static long backupInterval = PFUtilities.EVERY_10_MINUTE;
    public static String autoBackUpLocation;

    private boolean isActive;

    public BackUpDaemon() {
        isAutoBackUpEnabled = Settings.getBooleanItem("PF_IS_AUTO_BACKUP_ENABLED");
        System.out.println("isAutoBackUpEnabled = " + isAutoBackUpEnabled);
        autoBackUpLocation = Settings.getItem("PF_BACKUP_PATH");
        if ((autoBackUpLocation == null) || (autoBackUpLocation.length() == 0))
            autoBackUpLocation = PFUtilities.backUpPath; //PFStore.getInstance().transFileName;

        try {
            backupInterval = Long.parseLong(Settings.getItem("PF_BACKUP_INTERVAL"));
        } catch (Exception ex) {
            backupInterval = PFUtilities.EVERY_10_MINUTE;
        }
    }

    public void run() {
        isActive = true;
        while (isActive) {
            try {
                Thread.sleep(backupInterval);
                if (isAutoBackUpEnabled)
                    doBackUp();
            } catch (Exception ex) {
            }
        }
    }

    public void setBackUpInterval(long interval) {
        backupInterval = interval;
//        System.out.println(" backupInterval = " + backupInterval);
    }

    public static long getBackUpInterval() {
        return backupInterval;
    }

    public void setAutoBackUpStatus(boolean status) {
        System.out.println("Set isAutoBackUpEnabled = " + isAutoBackUpEnabled);
        isAutoBackUpEnabled = status;
    }

    public static boolean getAutoBackUpStatus() {
        System.out.println(">> isAutoBackUpEnabled = " + isAutoBackUpEnabled);
        return isAutoBackUpEnabled;
    }

    public static void setBackUpLoaction(String sLocation) {
//        System.out.println("Location = " + sLocation);
        autoBackUpLocation = sLocation;
    }

    public static String getBackUpLoaction() {
        return autoBackUpLocation;
    }

    private void doBackUp() {
        System.out.println("Backing up the current transactions!");
        PFStore.saveTransactions(autoBackUpLocation + "./txnBackUp.dat"); // PFStore.getInstance().transFileName);
    }

    public void exitFromLoop() {
        isActive = false;
//        saveConfiguration();
    }

    public static void saveConfiguration() {
        String sValue = null;
        if (isAutoBackUpEnabled)
            sValue = "TRUE";
        else
            sValue = "FALSE";
        Settings.saveItem("PF_IS_AUTO_BACKUP_ENABLED", sValue);
        sValue = null;

        if ((autoBackUpLocation == null) || (autoBackUpLocation.length() == 0))
            sValue = PFUtilities.backUpPath; //"./PortfolioData"; //PFStore.getInstance().transFileName;
        else
            sValue = autoBackUpLocation;
        System.out.println("PF_BACKUP_PATH = " + sValue);
        Settings.saveItem("PF_BACKUP_PATH", sValue);

        Settings.saveItem("PF_BACKUP_INTERVAL", backupInterval + "");
    }
}