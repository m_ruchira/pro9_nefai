// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.portfolio;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;

public class ValuationModel
        extends CommonTable
        implements TableModel, CommonTableInterface {

    private PFStore pfStore;
    private ArrayList dataStore;
//    private ValuationModel  selfReference; // Holds the self reference
    public boolean isCashIncluded = false;
    private PortfolioWindow pfWindow;
    private String sCash;
    //private String  baseCurrency = "SR";

    /**
     * Constructor
     */
    public ValuationModel(ArrayList dataStore) {
        this.dataStore = dataStore;
//        selfReference = this;
        pfStore = PFStore.getInstance();
        sCash = Language.getString("CASH");
    }

    public void includeCashBalance(boolean status) {
        isCashIncluded = status;
    }

    public void setSymbol(String symbol) {

    }

    public void setPFWindow(PortfolioWindow pfWindow) {
        this.pfWindow = pfWindow;
    }

    /* --- Table Model metods start from here --- */
    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
//        if (Settings.isValidSystemWindow(ESPConstants.PortfolioWindow)) {
        if (isCashIncluded)
            return dataStore.size() + 2;
        else
            return dataStore.size() + 1;
//        } else {
//            return 0;
//        }
    }

    public Object getValueAt(int row, int col) {
        //float conversionRate = CurrencyStore.getCurrency("SR|" + getBaseCurrency());
        int extraRows = 1;
        if (isCashIncluded)
            extraRows = 2;
        try {
            if (row < (getRowCount() - extraRows)) { // only transaction rows
                ValuationRecord record = (ValuationRecord) dataStore.get(row);
                if (record == null) {
                    return "";
                }
          

                Stock stock = PortfolioInterface.getStockObject(record.getSKey());
                if (stock == null) {
                    switch (col) {
                        case -1:     // KEY
                            return record.getSKey();
                        case 0:     // SYMBOL
                            return SharedMethods.getSymbolFromKey(record.getSKey());
                        case 1:     // EXCHANGE CODE
//                            return SharedMethods.getExchangeFromKey(record.getSKey());
                            return ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(record.getSKey()).trim()).getDisplayExchange(); // To display exchange

                        default:
                            return "";
                    }
                }
                String exchange = stock.getExchange();


                switch (col) {
                    case -4:
                        return stock.getDecimalCount();
                    case -13:
                        return stock.isSymbolEnabled();
                    case -1:
                        return record.getSKey();
                    case 0:     // SYMBOL
                        return PortfolioInterface.getSymbol(stock);
                    case 1:     // EXCHANGE CODE
                        return ExchangeStore.getSharedInstance().getExchange(exchange).getDisplayExchange();
                    case 2:     // COMPANY NAME
                        if (PortfolioInterface.getCompanyName(stock) == null)
                            return "";
                        else
                            return PortfolioInterface.getCompanyName(stock);
                    case 3:     // CURRENCY
                        if (PortfolioInterface.getCurrency(stock) == null)
                            return "";
                        else
                            return PortfolioInterface.getCurrency(stock);
                    case 7:     // EXCHANGE RATE
//                        return CurrencyStore.getCurrency(PortfolioInterface.getCurrency(stock)
//                            + "|" + PFStore.getBaseCurrency()) + "";
                        return CurrencyStore.getRate(PortfolioInterface.getCurrency(stock),
                                PFStore.getBaseCurrency());
                    case 5:     // HOLDING
                        return (long)record.getHolding();
                    case 6:     // AVERAGE COST
                        if(!PFStore.isFIFOSellMode())
                       // return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), record.getAvgCost(),true);
                        return  record.getAvgCost();
                        else
                        return "N/A";
                    case 4:     // LATEST PRICE
                         if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                         //   return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),(float) PortfolioInterface.getLastTrade(stock),true);
                            return  PortfolioInterface.getLastTrade(stock);
                        else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                          //  return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), PortfolioInterface.getVWAP(stock),true);
                            return (double)PortfolioInterface.getVWAP(stock);
                        else
                         //   return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock),true);
                            return  PortfolioInterface.getPreviousClosed(stock);
                    case 8:     // TOTAL COST
                        return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), record.getCumCost(),true);
//                        return new Long((long)convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), record.getTotCost()));
                    case 9:     // MARKET VALUE
                        if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0)){
                            return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
                                record.getHolding() * PortfolioInterface.getLastTrade(stock) + record.getCashDividends(),true);

                        }else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0)) {
                           return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
                                record.getHolding() * PortfolioInterface.getVWAP(stock) + record.getCashDividends(),true);
                        } else {
                           return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
                                   record.getHolding() *PortfolioInterface.getPreviousClosed(stock) + record.getCashDividends(),true);
                        }


//                    case 9:     // MARKET VALUE
//                        if (stock.getLastTradeValue() > 0)
//                            return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
//                                    record.getHolding() * PortfolioInterface.getLastTrade(stock) +
//                                            record.getCashDividends());
//                        else
//                            return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
//                                    record.getHolding() * PortfolioInterface.getPreviousClosed(stock) +
//                                            record.getCashDividends());
                    case 10:    //  GAIN LOSS TODAY
                        return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), (record.getHolding() * PortfolioInterface.getChange(stock)[0]),true);
                    case 11:    //  % GAIN LOSS TODAY
//                        return new Float(convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((record.getHolding() * PortfolioInterface.getChange(stock)[0]) / record.getTotCost()))*100);
                        if(record.getCumCost()>0){
                            return new Double(((record.getHolding() * PortfolioInterface.getChange(stock)[0]) / record.getCumCost()) * 100);
                        }else{
                            return "";
                        }
                    case 12: {  //  GAIN LOSS OVERALL
                        double marketValue = 0D;
                        if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                           marketValue = (record.getHolding() * PortfolioInterface.getLastTrade(stock));
                       else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                           marketValue = ((record.getHolding() * PortfolioInterface.getVWAP(stock)));
                       else
                           marketValue = (record.getHolding() * PortfolioInterface.getPreviousClosed(stock));
                        marketValue += record.getCashDividends();
                        return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), (marketValue - record.getCumCost()),true);
//                        if (stock.getLastTradeValue() > 0&&PFStore.isVWAPMode())
//                            marketValue = ((record.getHolding() * (float)PortfolioInterface.getVWAP(stock)));
//                        else
//                            marketValue = (record.getHolding() * PortfolioInterface.getLastTrade(stock));
//                        marketValue += record.getCashDividends();
//                        return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), (marketValue - record.getCumCost()));
                    }
//                    case 12: {  //  GAIN LOSS OVERALL
//                        float marketValue = 0f;
//                        if (stock.getLastTradeValue() > 0)
//                            marketValue = ((record.getHolding() * PortfolioInterface.getLastTrade(stock)));
//                        else
//                            marketValue = (record.getHolding() * PortfolioInterface.getPreviousClosed(stock));
//                        marketValue += record.getCashDividends();
//                        return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), (marketValue - record.getCumCost()));
//                    }
//                    case 13: {  //  % GAIN LOSS OVERALL
//                        float marketValue = 0f;
//                        if (stock.getLastTradeValue() > 0)
//                            marketValue = (record.getHolding() *
//                                    PortfolioInterface.getLastTrade(stock));
//                        else
//                            marketValue = (record.getHolding() *
//                                    PortfolioInterface.getPreviousClosed(stock));
//                        marketValue += record.getCashDividends();
//                        return ((marketValue - record.getCumCost()) / record.getCumCost()) * 100;
////                        return new Float(convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((marketValue - record.getTotCost()) / record.getTotCost()))*100);
//                    }
                    case 13: {  //  % GAIN LOSS OVERALL
                        double marketValue = 0f;
                        if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                           marketValue = (record.getHolding() * PortfolioInterface.getLastTrade(stock));
                        else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                           marketValue = (record.getHolding() * PortfolioInterface.getVWAP(stock));
                        else
                           marketValue = (record.getHolding() * PortfolioInterface.getPreviousClosed(stock));
                        marketValue += record.getCashDividends();
                        if(record.getCumCost()>0){
                        return ((marketValue - record.getCumCost()) / record.getCumCost()) * 100;
                        }else{
                            return "";
                        }
//                        if (stock.getLastTradeValue() > 0&&PFStore.isVWAPMode())
//                            marketValue = (record.getHolding() * (float)PortfolioInterface.getVWAP(stock));
//                        else
//                            marketValue = (record.getHolding() * PortfolioInterface.getLastTrade(stock));
//                        marketValue += record.getCashDividends();
//                        return ((marketValue - record.getCumCost()) / record.getCumCost()) * 100;
//                        return new Float(convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((marketValue - record.getTotCost()) / record.getTotCost()))*100);
                    }
                    case 14:   //  % OF PORTFOLIO
                        return getPercOfPortfolio(row);
                    case 15:   //  CASH DIVIDENDS
                        return record.getCashDividends();
                    case 16:   //  Company code
                        return PortfolioInterface.getCompanyCode(stock);
                    case 17:  // Realized Gain Loss
                        return getRelizedGainLoss(record);
                    case 18:  // Realized Gain Loss
                        return getRelizedGainLoss(record) + getUnrealizedGainLoss(stock, record);
                    default:
                        return "";
                }
            } else if (isCashIncluded && (row == (getRowCount() - 2))) {
                switch (col) {
                    case -13:
                        return true;
                    case 2:     // COMPANY NAME
                        return sCash + " (" + PFStore.getBaseCurrency() + ")";
                    case 8:             // CUMULATIVE TOTAL COST
                        return pfWindow.getCashBalanceForSelectedPFs();
                    case 9:             // CUMULATIVE MARKET VALUE
                        return pfWindow.getCashBalanceForSelectedPFs();
                    case 14:            //  % OF PORTFOLIO
                        return getPercOfPortfolio(row);
//                    case 15:   //  CASH DIVIDENDS
//                        return new Float(getTotalCashDividends());
                    default:
                        return "";
                }
            } else if (row == (getRowCount() - 1)) {
                switch (col) {
                     case -13:
                        return true;
                    case 0:
                    case 2:
                    case 3:
                    case 4:
                    case 5:
                    case 6:
                        return "";
                    case 7:
                    
                        return pfWindow.getTableCurrencyString();
                    case 8:         // CUMULATIVE TOTAL COST
                        return getCumulativeTotalCost();
//                        return new Long((long)getCumulativeTotalCost());
                    case 9:         // CUMULATIVE MARKET VALUE
                        return getMarketValue();
                    case 10:        // CUMULATIVE GAIN LOSS TODAY
                        return getCumulativeGainLoss();
                    case 11: {       // CUMULATIVE % GAIN LOSS TODAY
                        double cumCost = getCumulativeTotalCost();
                        if (cumCost == 0)
                            return 0f;
                        else
                            return (getCumulativeGainLoss() / cumCost) * 100;
//                        return new Float(getCumulativePercentGainLoss());
                    }
                    case 12:        // CUMULATIVE GAIN LOSS OVERALL
                        return getCumGainLossOverall();
                    case 13: {       // CUMULATIVE % GAIN LOSS OVERALL
//                        return new Float(getCumPercentGainLossOverall());
                        double cumCost = getCumulativeTotalCost();
                        if (cumCost == 0)
                            return 0f;
                        else
                            return (getCumGainLossOverall() / cumCost) * 100;
                    }
                    case 14:        //  % OF PORTFOLIO
                        return new Double("100");
                    case 15:   //  CASH DIVIDENDS
                        return getTotalCashDividends();
                     case 17:  // Realized Gain Loss
                        return getCummRelizedGainLoss();
                      case 18:  // Realized Gain Loss
                        return getCummRelizedGainLoss() + getCumGainLossOverall();
                    default:
                        return "";
                }
            } else {
                return "";
            }
        } catch (Exception e) {
//            e.printStackTrace();
            return "";
        }
    }


     public double getCummRelizedGainLoss(){
         double gainloss = 0d;
          ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            record = (ValuationRecord) dataStore.get(i);
            gainloss += record.getRealizedGain();
            record = null;
        }
        return gainloss;

    }

    public double getUnrealizedGainLoss(Stock stock, ValuationRecord record){
        double marketValue = 0D;
        if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
           marketValue = (record.getHolding() * PortfolioInterface.getLastTrade(stock));
       else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
           marketValue = ((record.getHolding() * PortfolioInterface.getVWAP(stock)));
       else
           marketValue = (record.getHolding() * PortfolioInterface.getPreviousClosed(stock));
        marketValue += record.getCashDividends();
        return convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), (marketValue - record.getCumCost()),true);
    }

    public float getTotalCashDividends() {
        float totMValue = 0f;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            record = (ValuationRecord) dataStore.get(i);
            totMValue += record.getCashDividends();
            record = null;
        }
        return totMValue;
    }

    public double getPercOfPortfolio(int row) {
        double mValue = 0f;
        double selMValue = 0f;
        double totMValue = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            record = (ValuationRecord) dataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                mValue = record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),
                        PortfolioInterface.getLastTrade(stock) + record.getCashDividends(),true);
            else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                mValue = record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),
                        PortfolioInterface.getVWAP(stock) + record.getCashDividends(),true);
            else
                mValue = record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),
                        PortfolioInterface.getPreviousClosed(stock) + record.getCashDividends(),true);
//            if (stock.getLastTradeValue() > 0)
//                mValue = record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),
//                        PortfolioInterface.getLastTrade(stock) + record.getCashDividends());
//            else
//                mValue = record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),
//                        PortfolioInterface.getPreviousClosed(stock) + record.getCashDividends());
            if (row == i)
                selMValue = mValue;
            totMValue += mValue;
            record = null;
            stock = null;
        }
        if (isCashIncluded) {
            if (row == (getRowCount() - 2))
                selMValue = pfWindow.getCashBalanceForSelectedPFs();
            totMValue += pfWindow.getCashBalanceForSelectedPFs();
        }
        if (totMValue == 0)
            return 0f;
        else
            return (selMValue / totMValue) * 100;
    }

    public double getRelizedGainLoss(ValuationRecord record){
        return record.getRealizedGain();
    }

/*
        public float getPercOfPortfolio(int row) {
            float mValue    = 0f;
            float selMValue = 0f;
            float totMValue = 0f;
            Stock stock  = null;
            ValuationRecord record = null;
            for (int i = 0; i < dataStore.size(); i++) {
                record = (ValuationRecord)dataStore.get(i);
                stock  = PortfolioInterface.getStockObject(record.getSKey());
                if (stock.getLastTrade()[0] > 0)
                    mValue += record.getHolding() * convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock)[0]);
                else
                    mValue += record.getHolding() * convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock)[0]);
                if (row == i)
                    selMValue = mValue;
                totMValue += mValue;
                record = null;
                stock  = null;
            }
            if (isCashIncluded) {
                if (row == (getRowCount() - 2))
                    selMValue = pfWindow.getCashBalanceForSelectedPFs();
                totMValue += pfWindow.getCashBalanceForSelectedPFs();
            }
            return (selMValue / totMValue) * 100;
        }
*/

    public double getCumulativeTotalCost() {
        double totCost = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            record = (ValuationRecord) dataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            totCost += convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), record.getCumCost(),true);
            record = null;
        }
        if (isCashIncluded)
            totCost += pfWindow.getCashBalanceForSelectedPFs();
        return totCost;
    }

    public double getMarketValue() {
        double mValue = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            record = (ValuationRecord) dataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock),true);
            else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getVWAP(stock),true);
            else
                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock),true);
//            if (stock.getLastTradeValue() > 0)
//                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock));
//            else
//                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock));
            record = null;
            stock = null;
        }
        if (isCashIncluded)
            mValue += pfWindow.getCashBalanceForSelectedPFs();
        return mValue;
    }

    public double getCumGainLossOverall() {
        double mValue = 0f;
        double cumValue = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            record = (ValuationRecord) dataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                mValue = record.getHolding() * PortfolioInterface.getLastTrade(stock);
            else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                mValue = record.getHolding() * PortfolioInterface.getVWAP(stock);
            else
                mValue = record.getHolding() * PortfolioInterface.getPreviousClosed(stock);
//            if (stock.getLastTradeValue() > 0)
//                mValue = record.getHolding() * PortfolioInterface.getLastTrade(stock);
//            else
//                mValue = record.getHolding() * PortfolioInterface.getPreviousClosed(stock);
//            mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), (mValue - record.getTotCost()));
            cumValue += convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), (mValue - record.getCumCost()),true);
            record = null;
            stock = null;
        }
        return cumValue;
    }
/*
        public float getCumGainLossOverall() {
            float mValue = 0f;
            Stock stock  = null;
            ValuationRecord record = null;
            for (int i = 0; i < dataStore.size(); i++) {
                record = (ValuationRecord)dataStore.get(i);
                stock  = PortfolioInterface.getStockObject(record.getSKey());
                if (stock.getLastTrade()[0] > 0)
                    mValue += record.getHolding() * PortfolioInterface.getLastTrade(stock)[0];
                else
                    mValue += record.getHolding() * PortfolioInterface.getPreviousClosed(stock)[0];
                mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), (mValue - record.getTotCost()));
                record = null;
                stock  = null;
            }
            return mValue;
        }
*/

    public float getCumPercentGainLossOverall() {
        double mValue = 0f;
        float cumValue = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            record = (ValuationRecord) dataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            if (PortfolioInterface.getLastTrade(stock) > 0)
                mValue = record.getHolding() * PortfolioInterface.getLastTrade(stock);
            else
                mValue = record.getHolding() * PortfolioInterface.getPreviousClosed(stock);
//            mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((mValue - record.getTotCost()) / record.getTotCost()));
            if (record.getCumCost() != 0)
                cumValue += ((mValue - record.getCumCost()) / record.getCumCost()) * 100;
            record = null;
            stock = null;
        }
        return cumValue;
    }

/*
        public float getCumPercentGainLossOverall() {
            float mValue = 0f;
            float cumValue = 0f;
            Stock stock  = null;
            ValuationRecord record = null;
            for (int i = 0; i < dataStore.size(); i++) {
                record = (ValuationRecord)dataStore.get(i);
                stock  = PortfolioInterface.getStockObject(record.getSKey());
                if (PortfolioInterface.getLastTrade(stock)[0] > 0)
                    mValue += record.getHolding() * PortfolioInterface.getLastTrade(stock)[0];
                else
                    mValue += record.getHolding() * PortfolioInterface.getPreviousClosed(stock)[0];
//            mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((mValue - record.getTotCost()) / record.getTotCost()));
                cumValue += ((mValue - record.getTotCost()) / record.getTotCost()) * 100;
                record = null;
                stock  = null;
            }
            return cumValue;
        }
     */

    public double getCumulativeGainLoss() {
        double totCost = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            record = (ValuationRecord) dataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            totCost += convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), record.getHolding() * PortfolioInterface.getChange(stock)[0],true);
            record = null;
        }
        return totCost;
    }

    public float getCumulativePercentGainLoss() {
        float totCost = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            record = (ValuationRecord) dataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            if (record.getCumCost() != 0)
                totCost += (record.getHolding() * PortfolioInterface.getChange(stock)[0] / record.getCumCost()) * 100;
//            totCost += convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), (record.getHolding() * PortfolioInterface.getChange(stock)[0] / record.getTotCost()));
            record = null;
        }
        return totCost;
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        /*try {
            return getValueAt(0, iCol).getClass();
                 } catch (Exception e) {
            return Object.class;
                 }*/
        switch (iCol) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 16:
                return String.class;
            case 4:
            case 6:
            case 7:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 17:
            case 18:
                return Double.class;
            case 5:
                return Long.class;
            case 15:
                return Float.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object value, int rowIndex, int column) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }

    private double convertToSelectedCurrency(String exchangeCode,String sCurrency, double value, boolean applyPriceModificationFactor ) {
        //return value * (float) CurrencyStore.getRate(sCurrency, pfStore.getInstance().getBaseCurrency());
        if(applyPriceModificationFactor){
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
            if (exchange != null)
                return value * (double) CurrencyStore.getRate(sCurrency, PFStore.getBaseCurrency()) / exchange.getPriceModificationFactor();
            else
                return value * (double) CurrencyStore.getRate(sCurrency, PFStore.getBaseCurrency());
        } else {
            return value * (double) CurrencyStore.getRate(sCurrency, PFStore.getBaseCurrency());
        }
    }


    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return Double.parseDouble((String) oValue);
    }

    private int toIntValue(Object oValue) throws Exception {
        return Integer.parseInt((String) oValue);
    }

    private float toFloatValue(Object oValue) throws Exception {
        return Float.parseFloat((String) oValue);
    }

    public double getMarketValueForSymbol(String sKey) {
        double marketValue = 0f;
        ValuationRecord valRecord = null;
        ArrayList list = PFStore.getInstance().getValuationList();
        Stock stock = PortfolioInterface.getStockObject(sKey);
        if (stock == null)
            return 0f;

        for (int i = 0; i < list.size(); i++) {
            valRecord = (ValuationRecord) list.get(i);
            if (valRecord.getSKey().equals(sKey)) {
                if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                    marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), valRecord.getHolding() * PortfolioInterface.getLastTrade(stock),true);
                else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                    marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), valRecord.getHolding() * PortfolioInterface.getVWAP(stock),true);
                else
                    marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), valRecord.getHolding() * PortfolioInterface.getPreviousClosed(stock),true);
//                if (stock.getLastTradeValue() > 0)
//                    marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), valRecord.getHolding() * PortfolioInterface.getLastTrade(stock));
//                else
//                    marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), valRecord.getHolding() * PortfolioInterface.getPreviousClosed(stock));
                break;
            }
            valRecord = null;
        }
        valRecord = null;
        list = null;
        stock = null;

        return marketValue;
    }

    public double getMktValuePerShareForSymbol(String sKey) {
        double marketValue = 0f;
        Stock stock = PortfolioInterface.getStockObject(sKey);
        if (stock == null)
            return 0f;
        if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
            marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock),true);
        else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
            marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getVWAP(stock),true);
        else
            marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock),true);
//        if (stock.getLastTradeValue() > 0)
//            marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock));
//        else
//            marketValue = convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock));

        stock = null;
        return marketValue;
    }

    public float getMarketValue(boolean isCashIncludedLocal) {
        float mValue = 0f;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            record = (ValuationRecord) dataStore.get(i);
            stock = PortfolioInterface.getStockObject(record.getSKey());
            if (!PFStore.isVWAPMode() && (stock.getLastTradeValue() > 0))
                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock),true);
            else if(PFStore.isVWAPMode() && (stock.getAvgTradePrice() > 0))
                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getVWAP(stock),true);
            else
                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock),true);
//            if (stock.getLastTradeValue() > 0)
//                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock));
//            else
//                mValue += record.getHolding() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock));
            record = null;
            stock = null;
        }
        if (isCashIncludedLocal)
            mValue += pfWindow.getCashBalanceForSelectedPFs();
        return mValue;
    }
}