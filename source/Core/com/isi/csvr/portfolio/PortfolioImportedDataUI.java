package com.isi.csvr.portfolio;

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.table.Table;
import com.isi.csvr.table.TWEditableHeaderTableColumn;
import com.isi.csvr.shared.*;
import com.isi.csvr.Client;
import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.SortButtonRenderer;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.InternalFrameEvent;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseEvent;
import java.util.*;
import java.math.BigInteger;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 9, 2008
 * Time: 12:21:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class PortfolioImportedDataUI extends InternalFrame implements InternalFrameListener, Themeable {

    private static final String EDIT_TABLE = "edit";
    private static final String UNEDIT_TABE = "unedit";
    private static final String TXN_TABLE = "txn";
    //pannels
    private JPanel topPanel;
    private JPanel tablePanel;
    private JPanel bottomPanel;
    private JPanel parameterPanel;
    private JPanel advParamsPanel;
    private JPanel btnPanel;
    private JPanel messagePanel;

    // components
    private Table editableTable;
    private Table uneditableTable;
    private Table validatedTable;
    private JSpinner startFrom;
    private TWComboBox dateFormat;
    private TWComboBox cmbexchange;
    private JLabel selectExchange;
    private TWButton cancel;
    private TWButton validate;
    private TWButton next;
    private TWButton finalImport;
    private ArrayList<TWComboItem> exchangesList;

    //lables
    private JLabel lblstartFrom;
    private JLabel lbldateFormat;
    //  private JLabel lblError;
    private JTextPane lblError;

    private PortfolioImportModel importmodel;
    private PortfolioImportModel importmodelSymVal;
    private ViewSetting oSettings;
    private String selectedpanel = "";
    TWComboItem[] editableComboItemlist;
    private int exchnageMode;

    public PortfolioImportedDataUI(int exchangemode) {
        exchnageMode = exchangemode;
        initUI();


    }

    private void initUI() {

        initComponents();
        setLables();
        addComponents();
        populateDateCombo();
        addListeners();
        if (Language.isLTR()) {
            this.setTitle(Language.getString("PORTFOLIO_NEUMARIC_VAL") + "    " + Language.getString("PF_IMOPORT_1OF4"));
        } else {
            this.setTitle(Language.getString("PF_IMOPORT_1OF4") + "    " + Language.getString("PORTFOLIO_NEUMARIC_VAL"));
        }

        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(true);
        this.setIconifiable(true);
        this.applyTheme();
        Theme.registerComponent(this);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        this.setSize(800, 350);
        addInternalFrameListener(this);


    }

    public void showwindow() {
        this.setVisible(true);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());

    }

    private void initComponents() {

        topPanel = new JPanel(new FlexGridLayout(new String[]{"680", "200"}, new String[]{"30", "30"}, 4, 4, true, true));
        tablePanel = new JPanel();
        bottomPanel = new JPanel(new FlexGridLayout(new String[]{"90", "90", "90"}, new String[]{"25"}, 4, 4, true, true));
        parameterPanel = new JPanel(new FlexGridLayout(new String[]{"90", "90", "40", "80", "90", "40", "100", "90"}, new String[]{"22"}, 4, 4, true, true));
        advParamsPanel = new JPanel();
        btnPanel = new JPanel(new BorderLayout());
        messagePanel = new JPanel();
        //lblError = new JLabel();
        lblError = new JTextPane();
        lblError.setPreferredSize(new Dimension(500, 60));
        lblError.setEditable(false);
        lblError.setOpaque(false);
        //  lblError.setContentType("text/rtf");
        //   editableTable = new Table();
        validatedTable = new Table();

        //  createEditableTable();
        createValidatedTable();

        startFrom = new JSpinner(new SpinnerNumberModel(1, 1, 10, 1));
        dateFormat = new TWComboBox();
        cancel = new TWButton();
        cancel.setPreferredSize(new Dimension(90, 25));
        validate = new TWButton();
        validate.setPreferredSize(new Dimension(90, 25));
        finalImport = new TWButton();
        finalImport.setPreferredSize(new Dimension(90, 25));
        next = new TWButton();

        exchangesList = new ArrayList<TWComboItem>();
        cmbexchange = new TWComboBox(new TWComboModel(exchangesList));
        SharedMethods.populateExchangesForTWCombo(exchangesList, false);
        selectExchange = new JLabel(Language.getString("PFIMPORT_DEFAULT_EXCHANGE"));
        //cmbexchange.setEnabled(false);

        lbldateFormat = new JLabel();
        lblstartFrom = new JLabel();
    }

    private void setLables() {

        cancel.setText(Language.getString("CANCEL"));
        validate.setText(Language.getString("NEXT"));
        finalImport.setText(Language.getString("IMPORT"));
        next.setText(Language.getString("NEXT"));
        lbldateFormat.setText(Language.getString("DATE_FORMAT"));
        lblstartFrom.setText(Language.getString("START_FROM"));
    }

    private void addComponents() {
        //   parameterPanel.setBorder(BorderFactory.createLineBorder(Color.RED,2));
        //   selectExchange.setBorder(BorderFactory.createLineBorder(Color.BLACK,2));
        parameterPanel.add(lblstartFrom);
        parameterPanel.add(startFrom);
        parameterPanel.add(new JPanel());
        parameterPanel.add(lbldateFormat);
        parameterPanel.add(dateFormat);
        parameterPanel.add(new JPanel());
        parameterPanel.add(selectExchange);
        parameterPanel.add(cmbexchange);
        if (exchnageMode == PortfolioImportUI.EXCHANGE_MODE_MULTIPLE) {
            selectExchange.setEnabled(false);
            cmbexchange.setEnabled(false);
        }

        topPanel.add(parameterPanel);
        topPanel.add(advParamsPanel);
        //  topPanel.add(selectExchange);
        //  topPanel.add(cmbexchange);

        tablePanel.setLayout(new CardLayout(1, 1));


        bottomPanel.add(validate);
        //  bottomPanel.add(next);
        bottomPanel.add(finalImport);
        bottomPanel.add(cancel);
//        messagePanel.setBorder(BorderFactory.createLineBorder(Color.GREEN));
        //   messagePanel.setPreferredSize(new Dimension(200,60));
        // lblError.setPreferredSize(new Dimension(200,60));
        messagePanel.add(lblError);
        lblError.setText(Language.getString("PFVAL_NEUMERIC_MESSAGE"));
        ////   messagePanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        //  btnPanel.setBorder(BorderFactory.createLineBorder(Color.BLACK));
        btnPanel.add(messagePanel, BorderLayout.WEST);
        btnPanel.add(bottomPanel, BorderLayout.EAST);

        //   tablePanel.setBorder(BorderFactory.createLineBorder(Color.blue,3));

        next.setEnabled(false);
        finalImport.setEnabled(false);


        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(topPanel, BorderLayout.NORTH);
        this.getContentPane().add(tablePanel, BorderLayout.CENTER);
        this.getContentPane().add(btnPanel, BorderLayout.SOUTH);


    }

    private void addListeners() {
        next.addActionListener(this);
        validate.addActionListener(this);
        cancel.addActionListener(this);
    }

    private void populateDateCombo() {
        dateFormat.addItem("dd/MM/yyyy");
        dateFormat.addItem("MM/dd/yyyy");
        dateFormat.addItem("yyyy/MM/dd");
    }

    private void createUnEditableTable() {
//        ViewSetting oSettings;
//        try {
//            // Read the default view in the view settings file
//            // get the default view
//            oSettings = (ViewSetting) Client.getInstance().getViewSettingsManager().getPortfolioView(1280, "8").clone();
//            if (oSettings == null)
//                throw (new Exception("View not found"));
////            if (sID == null)
////                oSettings.setID(Settings.NULL_STRING+ System.currentTimeMillis());
////            else
////                oSettings.setID(store.getId());
////            oSettings.setCaptions(sNewName);
//        } catch (Exception e) {
//            e.printStackTrace();
//            return;
//        }

        //String sColOrder = Settings.getItem("Gio");
        //    GUISettings.setColumnSettings(oSettings, PortfolioInterface.getColumnSetting("PORTFOLIO_IMPORT_COLUMNS"));
        //       oSettings.setTableNumber(1);
        final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
        final String searching = Language.getString("SEARCHING");
//        uneditableTable = new Table(){
//            public void paint(Graphics g) {
//                super.paint(g);
//                if (PFImportDataStore.getSharedInstance().isTransVal() || PFImportDataStore.getSharedInstance().isReadingCSV()) {
//                    FontMetrics fontMetrics = editableTable.getFontMetrics(getFont());
//                    g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
//                            (getHeight()) / 2);
//                    g.drawImage(icon.getImage(),
//                            (getWidth() - fontMetrics.stringWidth(searching)) / 2 + fontMetrics.stringWidth(searching) + 10,
//                            (getHeight() - icon.getImage().getHeight(this)) / 2, this);
//                    fontMetrics = null;
//                }
//            }
//        };
        uneditableTable = new Table();
        uneditableTable.setWindowType(ViewSettingsManager.PORTFOLIO_IMPORT);
        //  importmodel = new PortfolioImportModel();
        importmodelSymVal.setViewSettings(oSettings);
        //   oSettings.setParent(this);
        // editableTable.setImportPortfolioEditableHeaderModel(importmodel,oSettings.getColumnHeadings());
        uneditableTable.setModel(importmodelSymVal);
        importmodelSymVal.setTable(uneditableTable);
        Theme.registerComponent(uneditableTable);
//        uneditableTable.getTable().setDefaultRenderer(Object.class, new PortfolioImportRenderer());
//        uneditableTable.getTable().setDefaultRenderer(String.class, new PortfolioImportRenderer());
        uneditableTable.getModel().updateGUI();
        tablePanel.add(UNEDIT_TABE, uneditableTable);
        //  Client.getInstance().getViewSettingsManager().addPortfolioSetting(oSettings);
        uneditableTable.getTable().addMouseListener(this);

    }

    private void createValidatedTable() {

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == next) {
            next.setEnabled(false);
            PFImportDataStore.getSharedInstance().goToNextPhase();

            lblError.setText("");
            lblError.repaint();
            //validate.setEnabled(true);

        } else if (e.getSource() == validate) {
            if (next.isEnabled()) {
                next.setEnabled(false);
            }
            //next.setEnabled(true);
            if (PFImportDataStore.getSharedInstance().getCompletedPhase() == PFImportDataStore.VALIDATION_LOAD_COMPLETE && selectedpanel == EDIT_TABLE) {
                startValidation();
            } else if (PFImportDataStore.getSharedInstance().getCompletedPhase() == PFImportDataStore.VALIDATION_NEUMARIC_COMPLETE && selectedpanel == UNEDIT_TABE) {
                if (Settings.isConnected()) {
                    PFImportDataStore.getSharedInstance().continueValidation();
                } else {
                    new ShowMessage(Language.getString("MSG_PFIMPORT_SYMVAL_ERROR"), "I");
                }
            } else if (selectedpanel == EDIT_TABLE) {
                startValidation();
            } else {
                PFImportDataStore.getSharedInstance().goToNextPhase();
            }
            //validate.setEnabled(false);
        } else if (e.getSource() == cancel) {
            PortfolioImport.getSharedInstance().cancelPressed();
        }


    }

    private void startValidation() {
        JTable table = editableTable.getTable();
        ArrayList orgHeaderlist = new ArrayList();
        String[] hList = new String[table.getColumnModel().getColumnCount() - 1];
        TWEditableHeaderTableColumn col;
        int neededHeadercount = 0;
        for (int i = 1; i < table.getColumnModel().getColumnCount(); i++) {
            col = (TWEditableHeaderTableColumn) table.getColumnModel().getColumn(i);

            TWComboItem val = (TWComboItem) col.getHeaderValue();
            if (!val.getId().equals(HeaderMapper.NA) && orgHeaderlist.contains(val.getId())) {
                PortfolioImport.getSharedInstance().hideMessageFrame();
                new ShowMessage(Language.getString("MSG_DUPLICATE_COLOUMN"), "I");
                return;
            } else {

            }
            orgHeaderlist.add(val.getId());
            hList[i - 1] = val.getValue();
            if (!val.getId().equals(HeaderMapper.NA)) {
                neededHeadercount += 1;
            }

        }
        if (exchnageMode == PortfolioImportUI.EXCHANGE_MODE_SINGLE) {
            if ((TWComboItem) cmbexchange.getSelectedItem() == null) {
                new ShowMessage(Language.getString("MSG_SELECT_EXCHANGE"), "E");
                return;
            }

            hList = adjustArrays(hList, orgHeaderlist);
            boolean success = (hList != null);
            if (!success || neededHeadercount < HeaderMapper.getHeaderCount() - 1) {
                new ShowMessage(Language.getString("MSG_NOTENOUGH_COLOUMN"), "E");
                return;
            }
        }

        if (neededHeadercount < HeaderMapper.getHeaderCount() && exchnageMode == PortfolioImportUI.EXCHANGE_MODE_MULTIPLE) {
            PortfolioImport.getSharedInstance().hideMessageFrame();
            new ShowMessage(Language.getString("MSG_NOTENOUGH_COLOUMN"), "I");
            return;
        }
        PortfolioImport.getSharedInstance().startAutoImportProcess();
        String Dateformat = (String) dateFormat.getSelectedItem();
        int startRow = (Integer) startFrom.getValue() - 1;
        PFImportDataStore.getSharedInstance().StartNeumericalValidation(orgHeaderlist, Dateformat, startRow);
        oSettings.setColumnHeadings(hList);
        adjustViewSettingsForValidationPhase(oSettings, hList.length);
        HeaderMapper.mappedHeadings = Arrays.copyOf(hList, hList.length);


    }

    private String[] adjustArrays(String[] hlist1, ArrayList orgHeaderlist1) {


        try {
            String[] nwHlist = new String[hlist1.length + 1];
            System.arraycopy(hlist1, 0, nwHlist, 0, hlist1.length);
            nwHlist[hlist1.length] = HeaderMapper.getExchangeComboItem().getValue();
            //hlist1 = nwHlist;

            orgHeaderlist1.add(HeaderMapper.getExchangeComboItem().getId());
            PFImportDataStore.getSharedInstance().adjustStringObjectsForSingleExchange(((TWComboItem) cmbexchange.getSelectedItem()).getId());
            return nwHlist;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return null;
        }

    }

    public void setDataStoreForEditTable(Hashtable datastore) {

        importmodel.setDatastore(datastore);
        editableTable.getModel().updateGUI();
        setSelectedHeadingItems();

    }

    public void UpdateModelandCreateTable(int CSVRowItemcount) {

        //  ViewSetting oSettings;
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings = (ViewSetting) Client.getInstance().getViewSettingsManager().getPortfolioView(1280, "8").clone();
            if (oSettings == null)
                throw (new Exception("View not found"));
//            if (sID == null)
//                oSettings.setID(Settings.NULL_STRING+ System.currentTimeMillis());
//            else
//                oSettings.setID(store.getId());
//            oSettings.setCaptions(sNewName);
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        //String sColOrder = Settings.getItem("Gio");
        GUISettings.setColumnSettings(oSettings, PortfolioInterface.getColumnSetting("PORTFOLIO_IMPORT_COLUMNS"));
        oSettings.setTableNumber(1);
        final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
        final String searching = Language.getString("SEARCHING");
        /* editableTable = new Table(){
            public void paint(Graphics g) {
                super.paint(g);
                if (PFImportDataStore.getSharedInstance().isNeumaricVal() || PFImportDataStore.getSharedInstance().isReadingCSV()) {
                    FontMetrics fontMetrics = editableTable.getFontMetrics(getFont());
                    g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
                            (getHeight()) / 2);
                    g.drawImage(icon.getImage(),
                            (getWidth() - fontMetrics.stringWidth(searching)) / 2 + fontMetrics.stringWidth(searching) + 10,
                            (getHeight() - icon.getImage().getHeight(this)) / 2, this);
                    fontMetrics = null;
                }
            }
        };*/
        editableTable = new Table();
        int minColNo = oSettings.getColumnHeadings().length - 1;
        if (exchnageMode == PortfolioImportUI.EXCHANGE_MODE_SINGLE) {
            minColNo = oSettings.getColumnHeadings().length - 2;
        }
        //if(oSettings.getColumnHeadings().length-1> CSVRowItemcount){
        if (minColNo > CSVRowItemcount) {
            new ShowMessage(Language.getString("MSG_PFIMPORT_INSUF_COLS"), "0");
            return;
        }
        //  ViewSetting osetting= importmodel.getViewSettings();
        //   TWComboItem[] itemlist = getTWComboItemList(oSettings);
        editableComboItemlist = getTWComboItemList(oSettings);
        if (exchnageMode == PortfolioImportUI.EXCHANGE_MODE_SINGLE) {
            editableComboItemlist = removeExchangeComboItem(editableComboItemlist);
        }

        adjustViewSetting(oSettings, CSVRowItemcount);

        editableTable.setWindowType(ViewSettingsManager.PORTFOLIO_IMPORT);
        importmodel = new PortfolioImportModel();
        importmodel.setViewSettings(oSettings);
        oSettings.setParent(this);
        editableTable.setImportPortfolioEditableHeaderModel(importmodel, editableComboItemlist);
        //editableTable.setModel(importmodel);
        importmodel.setTable(editableTable);
        Theme.registerComponent(editableTable);
        editableTable.getModel().updateGUI();
        Client.getInstance().getViewSettingsManager().addPortfolioSetting(oSettings);
        editableTable.getTable().addMouseListener(this);

        tablePanel.add(EDIT_TABLE, editableTable);
        showPannel(EDIT_TABLE);
        tablePanel.updateUI();


    }

    public void applyTheme() {
        PortfolioImportRenderer.reload();
        SwingUtilities.updateComponentTreeUI(topPanel);
        SwingUtilities.updateComponentTreeUI(btnPanel);
    }

    private TWComboItem[] removeExchangeComboItem(TWComboItem[] items) {
        TWComboItem[] nwItems = new TWComboItem[items.length - 1];
        ArrayList<TWComboItem> nwheadings = new ArrayList<TWComboItem>();

        for (int i = 0; i < items.length; i++) {
            if (i != 1) {
                nwheadings.add(items[i]);
            }
        }

        for (int j = 0; j < nwheadings.size(); j++) {
            nwItems[j] = nwheadings.get(j);
        }

        return nwItems;

    }


    private TWComboItem[] getTWComboItemList(ViewSetting oSetting) {

        return PFImportDataStore.getSharedInstance().getTWComboItemlist(oSetting.getColumnHeadings());


    }

    private void adjustViewSetting(ViewSetting oSetting, int count) {
        if (exchnageMode == PortfolioImportUI.EXCHANGE_MODE_SINGLE) {
            String[] nwHeaderlist = getExchangeRemovedColHeadings(oSetting.getColumnHeadings());
            oSetting.setColumnHeadings(nwHeaderlist);
            adjustColSettings(oSetting);
            adjustColumnSettings(oSetting);


        }
        //  else{

        String[] nwHeaderlist = getAdjustedColumnHeadings(oSetting.getColumnHeadings(), count);
        oSetting.setColumnHeadings(nwHeaderlist);
        adjustColSettings(oSetting);
        adjustColumnSettings(oSetting);
        //    }


    }

    private void adjustViewSettingsForValidationPhase(ViewSetting oSetting, int count) {
        String[] nwHeaderlist = getAdjustedColumnHeadings(oSetting.getColumnHeadings(), count);
        oSetting.setColumnHeadings(nwHeaderlist);
        adjustColSettings(oSetting);
        adjustColumnSettings(oSetting);
    }

    private String[] getExchangeRemovedColHeadings(String[] original) {
        String[] newheadings = new String[original.length - 1];
        ArrayList<String> nwheadings = new ArrayList<String>();

        for (int i = 0; i < original.length; i++) {
            if (i != 2) {
                nwheadings.add(original[i]);
            }
        }

        for (int j = 0; j < nwheadings.size(); j++) {
            newheadings[j] = nwheadings.get(j);
        }

        return newheadings;

    }

    private String[] getAdjustedColumnHeadings(String[] original, int size) {
        // if (exchnageMode == PortfolioImportUI.EXCHANGE_MODE_MULTIPLE) {
//            if (original.length >= size) { // size is added 1 to allow for the "Number" column
//                return original;
//            }
        if (original.length > size) { // size is added 1 to allow for the "Number" column
            return original;
        } else {
            int oldSz = original.length;
            String[] newheadings = new String[size + 1];
            for (int i = 0; i < newheadings.length; i++) {
                if (i < oldSz) {
                    newheadings[i] = original[i];
                } else {
                    newheadings[i] = "x";
                }

            }

            return newheadings;
        }
        //  }
//        else{
//             if(original.length>= size){
//                return original;
//            }else{
//                int oldSz = original.length;
//                String[] newheadings = new String[size];
//                for(int i=0; i<newheadings.length; i++){
//                    if(i<oldSz){
//                        newheadings[i]= original[i];
//                    }else{
//                        newheadings[i]="x";
//                    }
//
//                }
//
//               return newheadings;
//            }
//        }
        // return null;
    }

    private void adjustColSettings(ViewSetting oSeting) {
        int columncnt = oSeting.getColumnHeadings().length;
        int[][] colSetings = new int[columncnt][3];

        for (int i = 0; i < columncnt; i++) {
            colSetings[i][0] = i;
            colSetings[i][1] = 100;
            colSetings[i][2] = i;
        }

        oSeting.setColumnSettings(colSetings);

    }

    private void adjustColumnSettings(ViewSetting viewSettings) {

        Vector g_oSelected = new Vector();
        BigInteger lSelectedPos = (viewSettings.getColumns().or(viewSettings.getFixedColumns())).and(viewSettings.getHiddenColumns().not());
        BigInteger lFixedPos = viewSettings.getFixedColumns();
        BigInteger lHiddenPos = viewSettings.getHiddenColumns();

        String[] asColumns = viewSettings.getColumnHeadings();
        for (int i = 0; i < (asColumns.length - 1); i++) {
            if (((lHiddenPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))) // make sure the col is not hidden
            {
                if ((lSelectedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) {
                } else {
                    if ((lFixedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))
                        g_oSelected.add(asColumns[i]);
                }
            }
            lSelectedPos = lSelectedPos.shiftRight(1);
            lFixedPos = lFixedPos.shiftRight(1);
            lHiddenPos = lHiddenPos.shiftRight(1);
        }
        g_oSelected.add(asColumns[(asColumns.length - 1)]);
        lSelectedPos = lSelectedPos.shiftRight(1);
        lFixedPos = lFixedPos.shiftRight(1);
        lHiddenPos = lHiddenPos.shiftRight(1);

        Object[] tempArray = g_oSelected.toArray();
        Arrays.sort(tempArray);
        g_oSelected.clear();
        for (int k = 0; k < tempArray.length; k++) {
            g_oSelected.addElement(tempArray[k]);
        }
        tempArray = null;

        BigInteger lCols = BigInteger.valueOf(0);
        for (int i = 0; i < asColumns.length; i++) {
            if (g_oSelected.indexOf(asColumns[i]) != -1) {
                //change start
                lCols = lCols.add(pwr(i));
            }
        }
        viewSettings.setColumns((lCols));

    }

    private BigInteger pwr(int iPwr) {
        BigInteger i = BigInteger.valueOf(1);
        return (i.shiftLeft(iPwr));
    }

    public void reloadModelForSymValPhase(Hashtable filtered) {
        if (Language.isLTR()) {
            this.setTitle(Language.getString("VALIDATED_SYMBOLS") + "    " + Language.getString("PF_IMOPORT_2OF4"));
        } else {
            this.setTitle(Language.getString("PF_IMOPORT_2OF4") + "    " + Language.getString("VALIDATED_SYMBOLS"));
        }
        importmodelSymVal = new PortfolioImportModel();
        importmodelSymVal.setDatastore(filtered);
        importmodelSymVal.setReloadedModel(true);
        //importmodel.setDatastore(filtered);
        createUnEditableTable();
        showPannel(UNEDIT_TABE);
        uneditableTable.getModel().updateGUI();
        disableTopPanel();
        lblError.setText(Language.getString("PFVAL_SYMBOL_MESSAGE"));
        this.remove(topPanel);
        this.doLayout();
        PortfolioImport.getSharedInstance().setReadyForNextPhase(true);

    }

    public void validateButtonsNeumaricComplete() {

        next.setEnabled(true);

    }

    public void validateButtonsSymbolComplete() {

        next.setEnabled(true);

    }

    private void showPannel(String panelid) {
        CardLayout cl = (CardLayout) (tablePanel.getLayout());
        cl.show(tablePanel, panelid);
        selectedpanel = panelid;

    }

    public void mouseClicked(MouseEvent e) {
        if (selectedpanel == EDIT_TABLE && e.getClickCount() > 1) {
            int row = editableTable.getTable().getSelectedRow();
            String msg = (String) editableTable.getModel().getValueAt(row, -2);
            if (msg != null && !msg.equals(Language.getString("VALID"))) {
                new ShowMessage(msg, "I");
                // lblError.setText(msg);
                //  lblError.repaint();
            }

        } else if (selectedpanel == UNEDIT_TABE && e.getClickCount() > 1) {
            int row = uneditableTable.getTable().getSelectedRow();
            String msg = (String) uneditableTable.getModel().getValueAt(row, -2);
            if (msg != null && !msg.equals(Language.getString("VALID"))) {
                new ShowMessage(msg, "I");
                // lblError.setText(msg);
                //   lblError.repaint();
            }

        }
    }

    private void disableTopPanel() {
        startFrom.setEnabled(false);
        dateFormat.setEnabled(false);
    }

    public void showLogicValidatePannel(ArrayList symbolValidated, PortfolioImportLogicUI importedLigicUI) {
        tablePanel.add(TXN_TABLE, importedLigicUI);
        importedLigicUI.setDataStoreInModel(symbolValidated);
        btnPanel.setVisible(false);
        this.remove(btnPanel);
        this.doLayout();
        showPannel(TXN_TABLE);


    }

    private void setSelectedHeadingItems() {
        StringTableRowObj rowobj = (StringTableRowObj) importmodel.getValueAt(0, -4);
        int colcount = importmodel.getColumnCount();

        for (int j = 1; j < colcount; j++) {     // start from second row

            String heading = (String) importmodel.getValueAt(0, j);

            int i = HeaderMapper.getComboItemNumber(heading);
            if (exchnageMode == PortfolioImportUI.EXCHANGE_MODE_SINGLE) {
                i = HeaderMapper.getComboItemNumberSingleExchange(heading);
            }
            try {
                if (i != -1 && i >= 0) {
                    TWEditableHeaderTableColumn col = (TWEditableHeaderTableColumn) editableTable.getTable().getColumnModel().getColumn(j);
                    col.setHeaderValue(editableComboItemlist[i]);


                } else if (i == -1) {
                    TWEditableHeaderTableColumn col = (TWEditableHeaderTableColumn) editableTable.getTable().getColumnModel().getColumn(j);
                    col.setHeaderValue(editableComboItemlist[editableComboItemlist.length - 1]);
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

        }
    }

    public void clickNext() {
        validate.doClick();
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        PortfolioImport.getSharedInstance().UIClosed();
    }

    public void closeWindow() {
//		if (getDefaultCloseOperation() == HIDE_ON_CLOSE){
//			setVisible(false);
//		}else{
//			dispose();
//		}
        doDefaultCloseAction();
    }

    public int getDesktopItemType() {
        return BOARD_TYPE;
    }
}
