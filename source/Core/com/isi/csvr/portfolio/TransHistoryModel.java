// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.portfolio;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.ArrayList;

public class TransHistoryModel
        extends CommonTable
        implements TableModel, CommonTableInterface {

    private ArrayList dataStore;
    private TransHistoryModel selfReference; // Holds the self reference

    /**
     * Constructor
     */
    public TransHistoryModel(ArrayList dataStore) {
        this.dataStore = dataStore;
        selfReference = this;

    }

    /* --- Table Model metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        return dataStore.size();
    }

    public void setSymbol(String symbol) {

    }

    public Object getValueAt(int row, int col) {
        try {
            if (row < (getRowCount())) {
                TransactRecord record = (TransactRecord) dataStore.get(row);
                if (record == null)
                    return "";
                if((record.getTxnType()==PFUtilities.BUY)||(record.getTxnType()==PFUtilities.SELL)||
                        (record.getTxnType()==PFUtilities.OP_BAL)||(record.getTxnType()==PFUtilities.DIVIDEND)){
                        Stock stock = PortfolioInterface.getStockObject(record.getSKey());
                        if (stock == null) {
                            switch (col) {
                                case -4:
                                    try {
                                        return DataStore.getSharedInstance().getStockObject(record.getSKey()).getDecimalCount();
                                    } catch(Exception e) {
                                        return 3;
                                    }
                                case 0:     // SYMBOL
                                    return SharedMethods.getSymbolFromKey(record.getSKey());
                                case 1:     // EXCHANGE CODE
//                                    return SharedMethods.getExchangeFromKey(record.getSKey());
                                return ExchangeStore.getSharedInstance().getExchange(SharedMethods.getExchangeFromKey(record.getSKey()).trim()).getDisplayExchange(); // To display exchange
                                case 2:
                                    return SharedMethods.getSymbolFromKey(record.getSKey());
                                case 6:     // CURRENCY
                                    return record.getCurrency();
                                case 7:     // EXCHANGE RATE
                                    return new Float(CurrencyStore.getRate(PFStore.getBaseCurrency(),
                                            record.getCurrency()));
                                case 9:     // PRICE
                                    return convertToSelectedCurrency(SharedMethods.getExchangeFromKey(record.getSKey()), record.getCurrency(), record.getPrice());
                                case 10:     // COMMISSION
                                    return convertToSelectedCurrency(SharedMethods.getExchangeFromKey(record.getSKey()), record.getCurrency(), record.getBrokerage());
                                case 11:     // TOTAL VALUE OF TRANSACTION
                                    if ((record.getTxnType() == PFUtilities.BUY) || (record.getTxnType() == PFUtilities.OP_BAL))
                                        return convertToSelectedCurrency(SharedMethods.getExchangeFromKey(record.getSKey()), record.getCurrency(), record.getQuantity() * record.getPrice() + record.getBrokerage());
                                    else if (record.getTxnType() == PFUtilities.SELL)
                                        return convertToSelectedCurrency(SharedMethods.getExchangeFromKey(record.getSKey()), record.getCurrency(), record.getQuantity() * record.getPrice() - record.getBrokerage());
                                    else if ((record.getTxnType() == PFUtilities.DIVIDEND)|| (record.getTxnType() == PFUtilities.DEPOSITE) || (record.getTxnType() == PFUtilities.CHARGES)
                                            || (record.getTxnType() == PFUtilities.WITHDRAWAL) || (record.getTxnType() == PFUtilities.REFUNDS))
                                        return convertToSelectedCurrency(SharedMethods.getExchangeFromKey(record.getSKey()), record.getCurrency(), record.getPrice());
                                case 16:     // Company code
                                    return record.getSKey();
                        }
                        } else {
                        switch (col) {
                            case -4:
                                try {
                                    return stock.getDecimalCount();
                                } catch(Exception e) {
                                    return 2;
                                }
                            case 0:     // SYMBOL
                                return PortfolioInterface.getSymbol(stock);
                            case 1:     // EXCHANGE CODE
                                return ExchangeStore.getSharedInstance().getExchange(stock.getExchange()).getDisplayExchange();
                            case 2:     // COMPANY NAME
                                if (PortfolioInterface.getCompanyName(stock) == null)
                                    return "";
                                else
                                    return PortfolioInterface.getCompanyName(stock);
                            case 6:     // CURRENCY
                                if (PortfolioInterface.getCurrency(stock) == null)
                                    return "";
                                else
                                    return PortfolioInterface.getCurrency(stock);
                            case 7:     // EXCHANGE RATE
                                return new Float(CurrencyStore.getRate(PFStore.getBaseCurrency(),
                                        PortfolioInterface.getCurrency(stock)));
                            case 9:     // PRICE
                                return convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), record.getPrice());
                            case 10:     // COMMISSION
                                return convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), record.getBrokerage());
                            case 11:     // TOTAL VALUE OF TRANSACTION
                                if ((record.getTxnType() == PFUtilities.BUY) || (record.getTxnType() == PFUtilities.OP_BAL))
                                    return convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), record.getQuantity() * record.getPrice() + record.getBrokerage());
                                else if (record.getTxnType() == PFUtilities.SELL)
                                    return convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), record.getQuantity() * record.getPrice() - record.getBrokerage());
                                else if ((record.getTxnType() == PFUtilities.DIVIDEND)|| (record.getTxnType() == PFUtilities.DEPOSITE) || (record.getTxnType() == PFUtilities.CHARGES)
                                        || (record.getTxnType() == PFUtilities.WITHDRAWAL) || (record.getTxnType() == PFUtilities.REFUNDS))
                                    return convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), record.getPrice());
                                case 16:     // Company code
                                    return PortfolioInterface.getCompanyCode(stock);

                            }
                        }
                        switch (col) {
                            case 3:     // DATE
                                return record.getTxnDate();
                            case 4:     // TIME
                                return record.getTxnDate();
                            case 5:     // TRANSACTION TYPE
                                return record.getTxnType();
                            case 8:     // QUANTITY
                                return (long)record.getQuantity();
                            case 12:     // TRANSACTION ID
                                return record.getId();
                            case 13:     // PORTFOLIO ID
                                return record.getPfID();
                            case 14:     // MEMO
                                if (record.getMemo() == null)
                                    return "";
                                else
                                    return record.getMemo();
                            case 15:     // PORTFOLIO NAME
                                return PFStore.getInstance().getPortfolioName(record.getPfID());
                            default:
                                return "";
                        }
                    } else{
                        switch (col) {
                            case -4:
                                try {
                                    return DataStore.getSharedInstance().getStockObject(record.getSKey()).getDecimalCount();
                                } catch(Exception e) {
                                    return 2;
                                }
                            case 3:     // DATE
                                return record.getTxnDate();
                            case 4:     // TIME
                                return record.getTxnDate();
                            case 5:     // TRANSACTION TYPE
                                return record.getTxnType();
                            case 6:     // CURRENCY
                                return record.getCurrency();
                            case 7:     // EXCHANGE RATE
                                return new Float(CurrencyStore.getRate(PFStore.getBaseCurrency(), record.getCurrency()));
                            case 8:     // QUANTITY
                                return (long)record.getQuantity();
                            case 9:     // PRICE
                                return convertToSelectedCurrency(SharedMethods.getExchangeFromKey(record.getSKey()), PFStore.getInstance().getPortfolio(record.getPfID()).getCurrency(), record.getPrice());
                            case 10:     // COMMISSION
                                return convertToSelectedCurrency(SharedMethods.getExchangeFromKey(record.getSKey()), record.getCurrency(), record.getBrokerage());
                            case 11:     // TOTAL VALUE OF TRANSACTION
                                return convertToSelectedCurrency(SharedMethods.getExchangeFromKey(record.getSKey()), PFStore.getInstance().getPortfolio(record.getPfID()).getCurrency(), record.getPrice());
                            case 12:     // TRANSACTION ID
                                return record.getId();
                            case 13:     // PORTFOLIO ID
                                return record.getPfID();
                            case 14:     // MEMO
                                if (record.getMemo() == null)
                                    return "";
                                else
                                    return record.getMemo();
                            case 15:     // PORTFOLIO NAME
                                return PFStore.getInstance().getPortfolioName(record.getPfID());
//                            case 16:     // Company code
//                                return PortfolioInterface.getCompanyCode(stock);

                            default:
                                return "";
                    }
                }
            }else{
                return "";
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (iCol) {
            case 0:
            case 1:
            case 2:
            case 6:
            case 14:
            case 15:
            case 16:
                return String.class;
            case 5:
                return Byte.class;
            case 3:
            case 4:
            case 8:
            case 12:
            case 13:
                return Long.class;
            case 7:
            case 9:
            case 10:
            case 11:
                return Float.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object value, int rowIndex, int column) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] customizerRecords = new CustomizerRecord[10];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, null, Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));      //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")
        customizerRecords[1] = new CustomizerRecord(Language.getString("BUY"), FIELD_BUY, Theme.getColor("PORTFOLIO_TABLE_CELL_BUY_BGCOLOR"), Theme.getColor("PORTFOLIO_TABLE_CELL_BUY_FGCOLOR"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("SELL"), FIELD_SELL, Theme.getColor("PORTFOLIO_TABLE_CELL_SELL_BGCOLOR"), Theme.getColor("PORTFOLIO_TABLE_CELL_SELL_FGCOLOR"));
        customizerRecords[3] = new CustomizerRecord(Language.getString("OPENING_BAL"), FIELD_OPBAL, Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR"), Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("DIVIDEND"), FIELD_DIVIDAND, Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR"), Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("DEPOSITES"), FIELD_DEPOSITE, Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR"), Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR"));
        customizerRecords[6] = new CustomizerRecord(Language.getString("WITHDROWALS"), FIELD_WITHDRAW, Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR"), Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR"));
        customizerRecords[7] = new CustomizerRecord(Language.getString("CHARGES"), FIELD_CHARGES, Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR"), Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR"));
        customizerRecords[8] = new CustomizerRecord(Language.getString("REFUNDS"), FIELD_REFUNDS, Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR"), Theme.getColor("PORTFOLIO_TABLE_CELL_OPBAL_BGCOLOR"));
        customizerRecords[9] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_SELECTED_ROW, Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR"), Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR"));
        return customizerRecords;
    }

    private double convertToSelectedCurrency(String exchangeCode,String sCurrency, double value) {
        //return value * (float) CurrencyStore.getRate(sCurrency, pfStore.getInstance().getBaseCurrency());
        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
        if (exchange != null)
            return value * CurrencyStore.getRate(sCurrency, PFStore.getBaseCurrency()) / exchange.getPriceModificationFactor();
        else
            return value * CurrencyStore.getRate(sCurrency, PFStore.getBaseCurrency());
    }

}