package com.isi.csvr.portfolio;

import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.*;
import javax.swing.border.Border;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 11, 2008
 * Time: 3:47:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class PortfolioImportRenderer  extends TWBasicTableRenderer {

    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oQuantityFormat;
    private Color foreground, background;
    private DefaultTableCellRenderer lblRenderer;
   //  private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;

    private String g_sNA = "NA";

    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color importError;

    private byte decimalPlaces = Constants.TWO_DECIMAL_PLACES;
    private byte tableDecimals = Constants.TWO_DECIMAL_PLACES;
    private byte stockDecimals = Constants.TWO_DECIMAL_PLACES;

    public PortfolioImportRenderer() {
        oPriceFormat = new TWDecimalFormat(" ###,##0.000 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
      //  g_asRendIDs = asRendIDs;

        reload();
        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;
        g_iNumberAlign = JLabel.RIGHT;
    }

     public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            importError = Theme.getColor("PF_IMPORT_INVALIDATED");
        } catch (Exception e) {
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
            importError = Color.red;
        }
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
       // g_asRendIDs = asRendIDs;
    }

    public void propertyChanged(int property) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
     public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);

        if (isSelected) {
            foreground = g_oSelectedFG;
            background = g_oSelectedBG;
        } else if (row % 2 == 0) {
            foreground = g_oFG1;
            background = g_oBG1;
        } else {
            foreground = g_oFG2;
            background = g_oBG2;
        }




        Boolean b = (Boolean)table.getModel().getValueAt(row,-1);
        if(!b && !isSelected){
            background =  importError;
        }

         int cl = -4;
         try {
             cl = (Integer) table.getModel().getValueAt(row,-3);
         } catch (Exception e) {
             e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
         }

         if( cl==column){
             ImageIcon icon = new ImageIcon("images/common/untick.gif");
             lblRenderer.setHorizontalTextPosition(g_iStringAlign);
             lblRenderer.setIcon(icon);
         }else{
             lblRenderer.setIcon(null);
         }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);


        try {
                 // SYMBOL
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);



        } catch (Exception e) {
            lblRenderer.setText("");
            //e.printStackTrace();
        }


        return lblRenderer;
    }








}
