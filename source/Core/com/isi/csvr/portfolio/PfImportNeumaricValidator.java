package com.isi.csvr.portfolio;

import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.SymbolMaster;

import java.util.*;
import java.text.SimpleDateFormat;
import java.text.ParseException;
import java.text.DecimalFormat;


/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 10, 2008
 * Time: 2:48:17 PM
 * To change this template use File | Settings | File Templates.
 */
public class PfImportNeumaricValidator  implements Runnable, PFImportDialogInterface{

    private ArrayList<String> headerlist;
    private Hashtable<Integer,StringTableRowObj> rowObjects ;
    private Hashtable<Long,StringTableRowObj> temprowObjects ;
    private ArrayList<PriliminaryTxnObject> neumericValidated;
    private ArrayList<PriliminaryTxnObject> symbolValidated;
    private String dateFormat= "dd/MM/yyyy";
    private SimpleDateFormat formatter;
    private int startRow=0;
    private int count=0;
    private DecimalFormat priceFormat1= new DecimalFormat("###,##0.00");
    private DecimalFormat priceFormat2= new DecimalFormat("###,##0.0000");
    private DecimalFormat qtyFormat= new DecimalFormat("###,##0");
    private boolean isSymbolsValidating=false;

    private ArrayList validationIDList;
    private Hashtable<String,ArrayList> validationList;




    public PfImportNeumaricValidator(ArrayList headerlist, Hashtable rowObjects, String dateFormat, int startRow, ArrayList<PriliminaryTxnObject> neumericValidated, ArrayList<PriliminaryTxnObject> symbolValidated) {
        this.headerlist = headerlist;
        this.rowObjects = rowObjects;
        this.dateFormat = dateFormat;
        this.startRow = startRow;
        this.neumericValidated= neumericValidated;
        temprowObjects = new Hashtable<Long, StringTableRowObj>();
        validationIDList = new ArrayList();
        validationList = new Hashtable<String, ArrayList>();
        this.symbolValidated = symbolValidated;


    }

    public void run() {

        if(checkData()){
            for(int i=startRow; i<rowObjects.size(); i++){
                StringTableRowObj row = rowObjects.get(i);
                if(row != null){
                    validateRow(row,i);
                }
            }

            System.out.println("Neumerical validation Complete");
            if(!neumericValidated.isEmpty()){
                validateExchanges();
            }
  //           validateSymbols();
            if(!ExchangeStore.isNoneDefaultExchangesAvailable()){
                 validateSymbols();
                validateCurrencies();
            }
        }

        PFImportDataStore.getSharedInstance().setNeumaricVal(false);
        if(ExchangeStore.isNoneDefaultExchangesAvailable()){
            PFImportDataStore.getSharedInstance().setCompletedPhase(PFImportDataStore.VALIDATION_NEUMARIC_COMPLETE);
        }else{
           PFImportDataStore.getSharedInstance().setCompletedPhase(PFImportDataStore.VALIDATION_SYMBOL_COMPLETE);
        }

    }

    private void validateRow(StringTableRowObj row, int timeAdj){

        int size = headerlist.size();
        PriliminaryTxnObject txn = new PriliminaryTxnObject();
        for(int i=0; i<size; i++){
            String ID = headerlist.get(i);
            String value = row.getCellValue(i);


            if(ID.equals(HeaderMapper.SYMBOL)){
                txn.setSymbol(value.trim().toUpperCase());
            }else if(ID.equals(HeaderMapper.EXCHANGE)){
                txn.setExchange(value.trim().toUpperCase());
            }else if(ID.equals(HeaderMapper.DATE)){
                try {

                    if(formatter.parse(value).before(new Date(System.currentTimeMillis()))){
                      long timestamp = formatter.parse(value).getTime()+timeAdj;
                      txn.setDate(new Date(timestamp));
                      //txn.setDate(formatter.parse(value));
                    }else{
                       row.setValidStatus(StringTableRowObj.INVALID_DATE_FORMAT );
                       return; 
                    }
                } catch (ParseException e) {
                    row.setValidStatus(StringTableRowObj.INVALID_DATE_FORMAT );
                    return;
                }
            }else if(ID.equals(HeaderMapper.PRICE)){
                try {
                  txn.setPrice((priceFormat1.parse(value)).doubleValue());
                } catch (ParseException e) {
                   row.setValidStatus(StringTableRowObj.INVALID_PRICE_FORMAT);
                   return;
                }
            }else if(ID.equals(HeaderMapper.COMISSION)){
                try{
                    txn.setBrokerage((priceFormat1.parse(value)).doubleValue());
                }catch (ParseException e){
                    row.setValidStatus(StringTableRowObj.INVALID_BROKERAGE_FORMAT);
                    return;
                }
            }else if(ID.equals(HeaderMapper.TYPE)){
                if(value.equalsIgnoreCase("buy")|| value.equalsIgnoreCase("b")){
                    txn.setType(PFUtilities.BUY);
                }else if(value.equalsIgnoreCase("sell")|| value.equalsIgnoreCase("s")){
                    txn.setType(PFUtilities.SELL);
                }else{
                    row.setValidStatus(StringTableRowObj.INVALID_TYPE );
                    return;
                }
            }else if(ID.equals(HeaderMapper.HOLDING)){
                try{
                    txn.setHolding((qtyFormat.parse(value)).intValue());
                }catch (ParseException e){
                    row.setValidStatus(StringTableRowObj.INVALID_HOLDING_FORMAT);
                    return;
                }
            }else if(ID.equals(HeaderMapper.CURRENCY)){
                txn.setCurrency(value);
            }else if(ID.equals(HeaderMapper.MEMO)){
                txn.setMemo(value);
            }else if(ID.equals(HeaderMapper.NA)){

            }
        }
        row.setValidStatus(StringTableRowObj.VALID);
        txn.setRowId(count);
        txn.setID(row.getId());

        neumericValidated.add(txn);
        temprowObjects.put(row.getId(),row);
        row=null;
        count+=1;

    }

    private boolean checkData(){
        if(headerlist != null && headerlist.size()>0 && rowObjects!= null && !rowObjects.isEmpty() && (startRow>=0 && startRow<rowObjects.size())){
            try {
                formatter  = new SimpleDateFormat(dateFormat);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                 return false;
            }
            return true;
        }else{
            return false;
        }
    }

    private void validateExchanges(){
         List<String> expired = ExchangeStore.getSharedInstance().getExpiredExchanges();
         List<String> inactive = ExchangeStore.getSharedInstance().getInactiveExchanges();

        for(int i=0; i<neumericValidated.size(); i++){
            PriliminaryTxnObject txn = neumericValidated.get(i);
            String exchange=txn.getExchange();
            if(ExchangeStore.getSharedInstance().isValidExchange(exchange)){
                if((expired != null && expired.contains(exchange)) || (inactive != null && inactive.contains(exchange))){
                   temprowObjects.get(txn.getID()).setValidStatus(StringTableRowObj.INVALID_EXCHANGE);
                    temprowObjects.remove(txn.getID());
                   neumericValidated.remove(txn);
                }
            } else{
                 temprowObjects.get(txn.getID()).setValidStatus(StringTableRowObj.INVALID_EXCHANGE);
                temprowObjects.remove(txn.getID());
                neumericValidated.remove(txn);
            }
        }


    }

    private void validateCurrencies(){
        for(int i=0; i<neumericValidated.size(); i++){
            PriliminaryTxnObject txn = neumericValidated.get(i);
            String curr=txn.getCurrency();
            if(!CurrencyStore.isValidCurrency(curr)){
                temprowObjects.get(txn.getID()).setValidStatus(StringTableRowObj.INVALID_CURRENCY_FORMAT );
                temprowObjects.remove(txn.getID());
                neumericValidated.remove(txn);

            }
        }
    }

    private void validateSymbols(){
        isSymbolsValidating =true;
        invalidateAll();
        loadValidationList();
        validateDefaultMktSymbols();


    }

    private void invalidateAll(){
        Enumeration en = temprowObjects.keys();
        while(en.hasMoreElements()){
            Long id = (Long)en.nextElement();
            validationIDList.add(id);
            StringTableRowObj row = temprowObjects.get(id);
            row.setValidStatus(StringTableRowObj.INVALID_SYMBOL);
        }
    }

    private void loadValidationList(){
        for(int i=0; i<neumericValidated.size(); i++){
             PriliminaryTxnObject txn =neumericValidated.get(i);
            String key = txn.getExchange()+"~"+txn.getSymbol();
            if(!validationList.containsKey(key)){
                ArrayList list= new ArrayList();
                list.add(txn.getID());
                validationList.put(key,list);
            }else{
                ((ArrayList) validationList.get(key)).add(txn.getID());
            }
        }
    }


    public void setSymbol(String key, int instrument) {
        if (key != null){
            String exchange = SharedMethods.getExchangeFromKey(key);
            String symbol = SharedMethods.getSymbolFromKey(key);



        }

    }

    public void setData(Object data) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void notifyInvalidSymbol(String symbol) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private void validateDefaultMktSymbols(){

        Enumeration en = validationList.keys();
        while (en.hasMoreElements()){
            String cmpkey = (String)en.nextElement();
            String key =cmpkey.split("~")[1];
            String symbol = SymbolMaster.getExchangeForSymbol(key, false);

            if(symbol != null){
                String exchangew = SharedMethods.getExchangeFromKey(symbol);
                if (exchangew.equals(cmpkey.split("~")[0])) {
                ArrayList list = validationList.get(cmpkey);
                validateTempRowObjects(list,symbol );
            }
            }

        }


    }

    private void validateTempRowObjects(ArrayList list , String symbol){

        if(!list.isEmpty()){
            for(int j=0; j<list.size(); j++ ){

                Long id = (Long)list.get(j);
                temprowObjects.get(id).setValidStatus(StringTableRowObj.VALID);
                PriliminaryTxnObject txn= getObjectFromneumericValidated(id);
                txn.setSKey(symbol);
                if(txn != null){
                    symbolValidated.add(txn);           // the original order is changed
                }
            }
        }
    }


    private PriliminaryTxnObject getObjectFromneumericValidated(Long id){
          for (PriliminaryTxnObject txn : neumericValidated) {
              if(txn.getID() == id){
                  return txn;
              }

          }
          return null;
    }
}
