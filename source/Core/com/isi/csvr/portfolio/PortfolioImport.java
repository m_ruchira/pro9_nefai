package com.isi.csvr.portfolio;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.table.TWTextArea;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.GUISettings;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.io.*;
import java.util.Hashtable;
import java.util.ArrayList;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 8, 2008
 * Time: 2:17:11 PM
 * To change this template use File | Settings | File Templates.
 */
public class PortfolioImport implements Runnable {

    private PortfolioImportUI importwindow;
    private PortfolioImportedDataUI importedDataUI;
    private PortfolioImportLogicUI importedLigicUI;
    private static PortfolioImport selfRef;
    private PortfolioWindow pfWindow;
    private long selPfID;
    private File selTextFlie = null;
    private int completedPhase = 0;
    private boolean readyForNextPhase = false;
    private MessageUpdateFrame  messageframe;


    private PortfolioImport() {

    }

    public static PortfolioImport getSharedInstance() {
        if (selfRef == null) {
            selfRef = new PortfolioImport();
        }
        return selfRef;
    }


    public void startImport() {
        selTextFlie = null;
        selPfID = 0l;
        importwindow = new PortfolioImportUI();
        Client.getInstance().getDesktop().add(importwindow);
        importwindow.showwindow();
        messageframe = new MessageUpdateFrame();
    }

    public void setPfWindow(PortfolioWindow pfWindow) {
        this.pfWindow = pfWindow;
    }

    public long getSelPfID() {
        return selPfID;
    }

    public void setSelPfID(long selPfID) {
        this.selPfID = selPfID;
    }

    public File getSelTextFlie() {
        return selTextFlie;
    }

    public void setSelTextFlie(File selTextFlie) {
        this.selTextFlie = selTextFlie;
    }

    public void loadNewlyAddedPortfolio(long pfId, PortfolioRecord rec) {
        pfWindow.loadNewlyAddedPortfolio(pfId, rec);
    }

    public void setDataFromStartImport(long pfid, File selfile, String filetype, int exchangeMode) {
        setSelPfID(pfid);

        setSelTextFlie(selfile);
        importedDataUI = new PortfolioImportedDataUI(exchangeMode);
        PFImportDataStore.getSharedInstance().setImpDataUiRef(importedDataUI);
        PFImportDataStore.getSharedInstance().reload();
        PFImportDataStore.getSharedInstance().loadValuesFromCSV(selfile);
        PFImportDataStore.getSharedInstance().setSelPfId(pfid);
        createLoadTableUI();


    }

    public void createLoadTableUI() {

        //   PortfolioImportLogicUI dataui = new PortfolioImportLogicUI();
        Client.getInstance().getDesktop().add(importedDataUI);
        importedDataUI.showwindow();
    }

    public void fireColCountDetermined(int count) {
        importedDataUI.UpdateModelandCreateTable(count);

    }

    public void fireDataStoreLoaded(Hashtable stringObjList) {
        importedDataUI.setDataStoreForEditTable(stringObjList);
    }

    public void symbolValidateStarted(Hashtable filtered, boolean invalid) {
        importedDataUI.reloadModelForSymValPhase(filtered);
    }

    public void validateButtonsForPhase(int completedPhase) {
        if (completedPhase == PFImportDataStore.VALIDATION_NEUMARIC_COMPLETE && (!PFImportDataStore.getSharedInstance().isNeuValidatedListEmpty())) {
            importedDataUI.validateButtonsNeumaricComplete();
        } else
        if (completedPhase == PFImportDataStore.VALIDATION_SYMBOL_COMPLETE && (!PFImportDataStore.getSharedInstance().isSymValidatedListEmpty())) {
            importedDataUI.validateButtonsSymbolComplete();
        }
    }

    public void LoadLogicValidatorUI(ArrayList symbolValidated) {
        // importedDataUI.setVisible(false);
        importedLigicUI = new PortfolioImportLogicUI();
        //  Client.getInstance().getDesktop().add(importedLigicUI);
        //  importedLigicUI.showwindow();
        importedLigicUI.setDataStoreInModel(symbolValidated);
        importedDataUI.showLogicValidatePannel(symbolValidated, importedLigicUI);
        if (Language.isLTR()) {
            importedDataUI.setTitle(Language.getString("TRANSACTION_VALIDATION") + "    " + Language.getString("PF_IMOPORT_3OF4"));
        } else {
            importedDataUI.setTitle(Language.getString("PF_IMOPORT_3OF4") + "    " + Language.getString("TRANSACTION_VALIDATION"));
        }
         setReadyForNextPhase(true);
    }

    public void setTitleForFinalImport() {
        if (Language.isLTR()) {
            importedDataUI.setTitle(Language.getString("TRANSACTION_VALIDATION") + "    " + Language.getString("PF_IMOPORT_4OF4"));
        } else {
            importedDataUI.setTitle(Language.getString("PF_IMOPORT_4OF4") + "    " + Language.getString("TRANSACTION_VALIDATION"));
        }
    }

    public void validateTransactions() {
        PFImportDataStore.getSharedInstance().validateTransactions();

    }

    public void transactionsValidated(ArrayList cumilativeSymbolWiseList, Hashtable rejectedSymbolWiseTable) {
        importedLigicUI.setValidatedDataStores(cumilativeSymbolWiseList, rejectedSymbolWiseTable);
    }

    public void cancelPressed() {
        PFImportDataStore.getSharedInstance().reload();
        pfWindow.updateSelectedTable();
        if (importedLigicUI != null) {
            //  importedLigicUI.dispose();
            importedLigicUI = null;
        }
        if (importedDataUI != null) {
            importedDataUI.dispose();
            importedDataUI = null;
        }
    }

    public int confirmSkip() {
        String str = Language.getString("SKIP_INVALID");

        int i = SharedMethods.showConfirmMessage(str, JOptionPane.WARNING_MESSAGE);
        return i;
    }
    public void allSymInvalid(){
         String str = Language.getString("PFVAL_NOSYMBOL_EXPORT");
          if(importedLigicUI!= null && importedLigicUI.isVisible() ){
         SharedMethods.showBlockingMessage(str, JOptionPane.ERROR_MESSAGE);
          }

    }

    public String validationComplete(int phase) {
        String str = "";
        switch (phase) {
            case 1:
                str = Language.getString("PFVAL_NEUMERIC_COMPLETE");
                break;
            case 2:
                str = Language.getString("PFVAL_SYMBOL_COMPLETE");
                break;
            default:
                str = "";
                break;
        }

       return str;

    }

    public void startAutoImportProcess() {
        completedPhase = 0;
        Thread t = new Thread(this);
        t.start();
       setReadyForNextPhase(true);

    }

    public void setCompletedPhase(int completedPhase) {
        this.completedPhase = completedPhase;
    }

    public void setReadyForNextPhase(boolean readyForNextPhase) {
        this.readyForNextPhase = readyForNextPhase;
    }

    public void showMessageFrame(){
        if(!messageframe.isVisible() ){
        messageframe.setVisible(true);
        messageframe.setLocationRelativeTo(importedDataUI);
        }
    }
    public void hideMessageFrame(){
        messageframe.setVisible(false);
    }

    public void run() {

        while (importedDataUI != null) {

            if(completedPhase == PFImportDataStore.VALIDATION_LOAD_COMPLETE && readyForNextPhase){
              readyForNextPhase = false;
                showMessageFrame();
                messageframe.setText(Language.getString("PFVAL_NEUMERIC_STARTED"),1);
            }

            if (completedPhase == PFImportDataStore.VALIDATION_NEUMARIC_COMPLETE && readyForNextPhase ) {
                readyForNextPhase = false;
                showMessageFrame();
                messageframe.setText(validationComplete(PFImportDataStore.VALIDATION_NEUMARIC_COMPLETE),1);
                messageframe.setText(Language.getString("PFVAL_SYMBOL_STARTED"),2);
                importedDataUI.clickNext();

            } else if (completedPhase == PFImportDataStore.VALIDATION_SYMBOL_COMPLETE && readyForNextPhase) {
                readyForNextPhase = false;
                showMessageFrame();
                messageframe.setText(validationComplete(PFImportDataStore.VALIDATION_SYMBOL_COMPLETE),2);
                 messageframe.setText(Language.getString("PFVAL_TRANSVAL_STARTED"),3);
                importedLigicUI.clickNext();

            }else if(completedPhase == PFImportDataStore.VALIDATION_CUMILATIVE_COMPLETE && readyForNextPhase){
                showMessageFrame();
                readyForNextPhase =false;
                 messageframe.setText(Language.getString("PFVAL_TRANSVAL_FINISHED"),3);
                importedLigicUI.clickFinalImport();
                messageframe.setText(Language.getString("MSG_IMPORT_SUCCES"),4);

            }

             try {
                Thread.sleep(2000);
            } catch (InterruptedException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }


        }
        hideMessageFrame();
        messageframe.clear();


    }


    private class MessageUpdateFrame extends JDialog{
        TWTextField mesg;
        JLabel lbl1;
        JLabel lbl2;
        JLabel lbl3;
        JLabel lbl4;
        int i=1;


        public MessageUpdateFrame() {
             super(Client.getInstance().getFrame(), true);
            this.setSize(new Dimension(200,100));

            this.setUndecorated(true);
            this.setModal(false);
           // mesg = new TWTextField();
            lbl1 = new JLabel("");
            lbl2 = new JLabel("");
            lbl3 = new JLabel("");
            lbl4 = new JLabel("");
            setLayout(new BorderLayout());
           // new TWTextField()
            JPanel pan = new JPanel();
            pan.setOpaque(true);
            pan.setLayout(new FlexGridLayout(new String[]  {"100%"},new String[] {"25%","25%","25%","25%"},2,2));
            pan.setPreferredSize(new Dimension(200,100));
            pan.setBorder(BorderFactory.createLineBorder(Color.BLACK,3));
            pan.add(lbl1);
            pan.add(lbl2);
            pan.add(lbl3);
            pan.add(lbl4);
            GUISettings.applyOrientation(pan);
           this.add(pan,BorderLayout.CENTER);



        }

        public void setText(String str, int i){
            //mesg.append("> "+str + "\n");
            if(i==1){
            lbl1.setText("> "+str );
            }else if(i==2){
              lbl2.setText("> "+str);
            }else if(i==3){
               lbl3.setText("> "+str); 
            }else if(i==4){
               lbl4.setText("> "+str);
            }

            this.repaint();

        }
        public void clear(){
            lbl1.setText("");
            lbl2.setText("");
            lbl3.setText("");
            lbl4.setText("");
            i=1;
        }
    }

    public void UIClosed(){
        cancelPressed();
    }

}
