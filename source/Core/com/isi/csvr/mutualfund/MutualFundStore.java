/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 12, 2003
 * Time: 9:01:14 AM
 * To change this template use Options | File Templates.
 */
package com.isi.csvr.mutualfund;

import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.event.ProcessListener;

import java.util.Collections;

public class MutualFundStore {
    private static MutualFundStore self;

    private DynamicArray fullStore;
    private DynamicArray filteredStore;
    private DynamicArray managers;
    private DynamicArray currencies;
    private int lastSelectedManagerIndex = 0;
    private int lastSelectedCurrencyIndex = 0;
    private Table mutualFundTable;
    private ProcessListener panel;

    public synchronized static MutualFundStore getInstance() {
        if (self == null) {
            self = new MutualFundStore();
        }
        return self;
    }

    private MutualFundStore() {
        fullStore = new DynamicArray();
        filteredStore = new DynamicArray();
        managers = new DynamicArray();
        currencies = new DynamicArray();
    }

    public void setPanel(ProcessListener panel) {
        this.panel = panel;
    }

    public void setTable(Table table) {
        mutualFundTable = table;
    }

    public synchronized void downloadData() {
        /*panel.processCompleted(false,null);
        while (Settings.isConnected()){
            try {
                xZipFileDownloader downloader = new xZipFileDownloader();
                String url = Meta.ZIPFILE + Meta.DS + Language.getLanguageTag() + "_MutualFund" + ".zip"
                        + Meta.FS + Meta.USER_SESSION + Meta.DS + Settings.getSessionID();
                downloader.downloadFile(url);
                url = null;
                String data = downloader.getStringData();
                data = UnicodeUtils.getNativeString(data); // convert to native font
                clear();
                if (data != null){
                    String[] records = data.split(Meta.RS);
                    for (int i = 0; i < records.length; i++) {
                        String[] fields = records[i].split(Meta.FS);
                        MutualFund mutualFund = new MutualFund(fields[0],fields[1],fields[2],fields[3],
                            fields[4],fields[5],fields[6],fields[7],fields[8]);
                        addRecord(mutualFund);
                        addManager(fields[1]);
                        addCurrency(fields[2]);
                        fields = null;
                    }
                    records = null;
                }
                managers.add(0,Language.getString("ALL"));
                currencies.add(0,Language.getString("ALL"));
                setFilterIndex(lastSelectedManagerIndex, lastSelectedCurrencyIndex);
                if (mutualFundTable != null){
                    mutualFundTable.applyInitialSorting();
                }
                break;
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                    e1.printStackTrace();
                }
            }
        }
        panel.processCompleted(true,null);*/
    }

    private void addManager(String descr) {
        MutualFundField manager = new MutualFundField(descr);
        int index = Collections.binarySearch(managers.getList(), manager);
        if (index < 0) {
            managers.add((index + 1) * -1, manager);
        }
    }

    public void addCurrency(String descr) {
        MutualFundField currency = new MutualFundField(descr);
        int index = Collections.binarySearch(currencies.getList(), currency);
        if (index < 0) {
            currencies.add((index + 1) * -1, currency);
        }
    }

    public void setFilterIndex(int managerindex, int currencyindex) {
        lastSelectedManagerIndex = managerindex;
        lastSelectedCurrencyIndex = currencyindex;
        String manager = managers.get(managerindex).toString();
        String currency = currencies.get(currencyindex).toString();

        if (managerindex == 0) {
            manager = null;
        } else {
            manager = managers.get(managerindex).toString();
        }

        if (currencyindex == 0) {
            currency = null;
        } else {
            currency = currencies.get(currencyindex).toString();
        }

        filteredStore.clear();
        for (int i = 0; i < fullStore.size(); i++) {
            MutualFund mutualFund = (MutualFund) fullStore.get(i);
            if ((isEqual(manager, mutualFund.getManager())) && (isEqual(currency, mutualFund.getCurrency()))) {
                //(mutualFund.getManager().equals(manager)){
                filteredStore.add(mutualFund);
            }
            mutualFund = null;
        }

        /*if ((managerindex == 0) && (currencyindex == 0)){
            filteredStore.clear();
            for (int i = 0; i < fullStore.size(); i++) {
                  filteredStore.add(fullStore.get(i));
            }
        }else{
            filteredStore.clear();
            for (int i = 0; i < fullStore.size(); i++) {
                MutualFund mutualFund =  (MutualFund)fullStore.get(i);
                if (mutualFund.getManager().equals(manager)){
                    filteredStore.add(mutualFund);
                }
                mutualFund = null;
            }
        }*/
    }

    private boolean isEqual(String ref, String obj) {
        try {
            if (ref == null) {
                return true;
            } else {
                return ref.compareToIgnoreCase(obj) == 0;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public void addRecord(MutualFund mutualFund) {
        fullStore.add(mutualFund);
    }

    public MutualFund getRecord(int index) {
        return (MutualFund) filteredStore.get(index);
    }

    public Object getManager(int index) {
        return managers.get(index);
    }

    public int getManagerCount() {
        return managers.size();
    }

    public Object getCurrency(int index) {
        return currencies.get(index);
    }

    public int getCurrencyCount() {
        return currencies.size();
    }

    public void clear() {
        fullStore.clear();
        filteredStore.clear();
        managers.clear();
        currencies.clear();
    }

    public int size() {
        return filteredStore.size();
    }
}

