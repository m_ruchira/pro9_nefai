package com.isi.csvr.brokers;

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: May 23, 2007
 * Time: 3:06:49 PM
 * To change this template use File | Settings | File Templates.
 */
public class TopBrokersModel extends CommonTable implements TableModel, CommonTableInterface {

    private DynamicArray g_oList;
    private TopBrokerObject stock;

    /**
     * Constructor
     */
    public TopBrokersModel() {
    }

    protected void finalize() throws Throwable {
        g_oList = null;
    }

    public void setDataStore(DynamicArray oList) {
        g_oList = oList;
    }

    public void setSymbol(String symbol) {

    }

    /* --- Table Model's methods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        if (g_oList != null)
            return g_oList.size();
        else
            return 0;
    }

    public Object getValueAt(int iRow, int iCol) {
        try {
            stock = null;
            stock = (TopBrokerObject) g_oList.get(iRow);

            switch (iCol) {
                case -1:
                    return "" + stock.getSymbol();
                case 0:
                    return "" + stock.getSymbol();
                case 1:
                    return      stock.getDescription();
                case 2:
                    return "" + stock.getValue();
                case 3:
                    return "" + stock.getVolume();
                case 4:
                    return "" + stock.getTrades();
                default:
                    return "";
            }
        } catch (Exception ex) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        try {
            return getValueAt(0, iCol).getClass();
        } catch (Exception ex) {
            return Object.class;
            //return Number.class;
        }
        //return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }
    /* --- Table Modal's metods end here --- */

}
