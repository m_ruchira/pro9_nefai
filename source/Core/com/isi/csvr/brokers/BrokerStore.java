package com.isi.csvr.brokers;

import com.isi.csvr.util.Decompress;
import com.isi.csvr.shared.*;

import java.io.*;
import java.util.Hashtable;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 21, 2007
 * Time: 2:46:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class BrokerStore {

    private static BrokerStore self = null;
    private Hashtable<String,Broker> store;
    private ArrayList<String> list;

    private BrokerStore(){
        store = new Hashtable<String, Broker>();
        list = new ArrayList<String> ();
    }

    public static BrokerStore getSharedInstance(){
        if(self == null){
            self = new BrokerStore();
        }
        return self;
    }

    public int getBrokerCount(){
        return list.size();
    }
    public ArrayList getBrokers(){
        return list;
    }

    public String getBrokerTypeString(char brokerType){
        switch(brokerType){
            case Constants.BROKER_TYPE_FOREIGN:
                return Language.getString("BROKER_TYPE_FOREIGN");
            case Constants.BROKER_TYPE_DOMESTIC:
                return Language.getString("BROKER_TYPE_DOMESTIC");
            case Constants.BROKER_TYPE_HOUSE_ACC:
                return Language.getString("BROKER_TYPE_HOUSE_ACC");
            default:
                return Language.getString("NA");
        }
    }

    public void addBroker(String id,Broker broker){
        store.put (id,broker);
        if(!list.contains (id)){
            list.add (id);
        }
    }

    public Broker getBroker(String id){
        return store.get(id);
    }

    public Broker getBroker(int index){
        String id = list.get(index);
        return store.get (id);
    }

    public void loadFile(){
        try {
            Decompress decompress = new Decompress();
            String sLangSpecFileName =  Settings.SYSTEM_PATH +"/brokers_" +Language.getSelectedLanguage() +".msf";
            ByteArrayOutputStream out = decompress.setFiles(sLangSpecFileName);
            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            String record = null;

            do {
                record = SharedMethods.readLine(in);
                if (record != null) {
                    record = record.trim();
                    String[] fields = record.split(Meta.DS);
                    Broker broker = getBroker(fields[0]);
                    if(broker == null){
                        broker = new Broker(fields[0]);
                        addBroker (fields[0],broker);
                    }
                    if (broker != null) {
                        String description = Language.getLanguageSpecificString(fields[1]);
                        description = UnicodeUtils.getNativeString(description);
                        broker.setName(description);
                        description = null;
                    }
                    fields = null;
                    broker = null;
                }
            } while (record != null);

            out = null;
            decompress = null;
            in.close();
            in = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
