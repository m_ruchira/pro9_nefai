package com.isi.csvr.communication;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 31, 2004
 * Time: 9:21:15 AM
 * To change this template use File | Settings | File Templates.
 */
public interface SendQInterface {
    void loadOfflineRequests();

    boolean isRequestValidForDespatch(String request);

    void writeData(String data) throws Exception;

    void addData(String sData);

    void clear();

    void addValidateRequest(String symbol, String referenceID, String language);

    void addAddRequest(String exchange, String symbol, int type, int instrumentType);

    void addRemoveRequest(String exchange, String symbol, int type, int instrumentType);

    void addAnnouncementBodyRequest(String exchange, String id, String language);

    void addNewsBodyRequest(String exchange, String id, String language);
}