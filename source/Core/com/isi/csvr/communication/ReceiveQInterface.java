package com.isi.csvr.communication;


import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 30, 2004
 * Time: 3:41:41 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ReceiveQInterface {
    List<String> getReceiveBuffer();
}
