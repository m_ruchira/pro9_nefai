package com.isi.csvr.communication;

import com.isi.csvr.shared.*;
import com.isi.csvr.datastore.*;
import com.isi.csvr.communication.tcp.SendQueue;
import com.isi.csvr.communication.tcp.InternationalConnector;
import com.isi.csvr.communication.tcp.TCPConnector;
import com.isi.csvr.downloader.*;
import com.isi.csvr.bandwidthoptimizer.MainUI;
import com.isi.csvr.bandwidthoptimizer.OptimizedTreeRenderer;
import com.isi.csvr.Client;

import java.io.OutputStream;
import java.util.Enumeration;
import java.util.ArrayList;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 31, 2004
 * Time: 9:17:34 AM
 */
public class SendQFactory {
    private static SendQInterface instance;

    public static SendQInterface createSendQ(OutputStream oOut) {
        instance = new SendQueue(oOut);
        return instance;
    }

    public static boolean isRequestValidForDespatch(String request) {
        if (instance != null)
            return instance.isRequestValidForDespatch(request);
        else
            return true;
    }

    public static void writeData(String data) throws Exception {
        if (instance != null)
            instance.writeData(data);
    }

    public static void addData(int path, String sData) {
        if (path == Constants.PATH_SECONDARY){
            InternationalConnector.getSharedInstance().addData(sData);
        } else {
            if (instance != null){
                instance.addData(sData);
            }
        }
    }

    public static void addContentData(byte path,String sData, String exchange){
         if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        new ContentCMConnector(sData, exchange,path);
    }

    public static void addContentData(String errData,byte path,String sData, String exchange){
           if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
               return;
           }
           new ContentCMConnector(sData, exchange,path,errData);
       }

    public static void addContentData(byte path,String sData, String exchange, int metaType){
        if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        new ContentCMConnector(sData, exchange, path, metaType);
    }

    public static void addContentData(byte path, String sData, String exchange, String ip){
        if ((exchange != null) && (!exchange.equals("*")) && (!exchange.equals("")) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        new ContentCMConnector(sData, exchange,ip, path);
    }
    //added 19/11/2009
    public static void addContentData(byte path, String sData, String exchange, String ip,String errData ){
        if ((exchange != null) && (!exchange.equals("*")) && (!exchange.equals("")) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        new ContentCMConnector(sData, exchange,ip, path,errData);
    }

    public static void addContentData(byte path, String sData, String exchange, String ip, int type, String strToAppend){
        if ((exchange != null) && (!exchange.equals("*")) && (!exchange.equals("")) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        new ContentCMConnector(sData, exchange, ip, type, path, strToAppend,null);
    }

    public static synchronized void addValidateRequest(String symbol, String referenceID, String exchange,int instrumentType) {
//        if (instance != null){
//            instance.addValidateRequest(symbol, referenceID);
//        }
        String request;
        if((exchange == null) || (exchange.equals(""))){
            exchange = "*";
            request = Meta.VALIDATE_SYMBOL + Meta.DS + symbol + Meta.FD + referenceID + Meta.FD + exchange + Meta.FD + Language.getSelectedLanguage()+Meta.EOL;
            exchange = null;
        }else{
            request = Meta.VALIDATE_SYMBOL + Meta.DS + symbol + Meta.FD + referenceID + Meta.FD + exchange + Meta.FD + Language.getSelectedLanguage()+ Meta.FD+instrumentType+Meta.EOL;
        }
        new ContentCMConnector(request, exchange, Constants.CONTENT_PATH_SECONDARY);

    }
      public static synchronized void addBulkValidateRequest(String symbolList, String referenceID) {

        String request = Meta.VALIDATE_SYMBOL_BULK + Meta.DS + referenceID +  Meta.FD + Language.getSelectedLanguage()+ Meta.FD+symbolList+Meta.EOL;
        new ContentCMConnector(request, null, Constants.CONTENT_PATH_SECONDARY);

    }
     public static synchronized void addValidateRequestWithoutInstrumentType(String symbol, String referenceID, String exchange) {
        String  request = Meta.VALIDATE_SYMBOL + Meta.DS + symbol + Meta.FD + referenceID + Meta.FD + exchange + Meta.FD + Language.getSelectedLanguage()+ Meta.EOL;
        new ContentCMConnector(request, null, Constants.CONTENT_PATH_SECONDARY);

    }
    public static synchronized void addAddRequest(String exchange, String symbol, int type, int instrumentType, String marketCenter) {
        if ((exchange != null) && (!exchange.equals("*")) && (!exchange.equals("")) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        int path = Constants.PATH_PRIMARY;
        try {
            if ((exchange != null) && (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == Constants.PATH_SECONDARY)) {
                path = Constants.PATH_SECONDARY;
            }
        } catch (Exception e) {
            e.printStackTrace();
            path = Constants.PATH_PRIMARY;
        }
        addAddRequest(path,exchange, symbol, type, instrumentType, marketCenter);
//        try {
//            if ((exchange != null) && (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == Constants.PATH_SECONDARY)) {
//                InternationalConnector.getSharedInstance().addAddRequest(exchange, symbol, type, instrumentType);
//            } else {
//            if (instance != null)
//                instance.addAddRequest(exchange, symbol, type, instrumentType);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//            if (instance != null)
//                instance.addAddRequest(exchange, symbol, type, instrumentType);
//        }
    }

    public static synchronized void addOptimizationRecord(String exchange, String symbol, int instrumentType){
          if ((exchange != null) && (!exchange.equals("*")) && (!exchange.equals("")) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        int path = Constants.PATH_PRIMARY;
        try {
            if ((exchange != null) && (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == Constants.PATH_SECONDARY)) {
                path = Constants.PATH_SECONDARY;
            }
        } catch (Exception e) {
            e.printStackTrace();
            path = Constants.PATH_PRIMARY;
        }

        int messageType = SharedMethods.getSymbolType(instrumentType);
        addAddOptimizedrecord(path,messageType,exchange, symbol, instrumentType);
    }

     public static synchronized void removeOptimizationRecord(String exchange, String symbol, int instrumentType){
          if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        int path = Constants.PATH_PRIMARY;
        try {
            if ((exchange != null) && (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == Constants.PATH_SECONDARY)) {
                path = Constants.PATH_SECONDARY;
            }
        } catch (Exception e) {
            e.printStackTrace();
            path = Constants.PATH_PRIMARY;
        }
         int messageType = SharedMethods.getSymbolType(instrumentType);

        removeAddOptimizedrecord(path,messageType,exchange, symbol, instrumentType);
    }
    

    //----------------------------- Added by Shanika
     public static synchronized void addOptimizationAllRecord(int key, String exchange, String symbol_instrument) {
        if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        int path = Constants.PATH_PRIMARY;
        try {
            if ((exchange != null) && (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == Constants.PATH_SECONDARY)) {
                path = Constants.PATH_SECONDARY;
            }
        } catch (Exception e) {
            e.printStackTrace();
            path = Constants.PATH_PRIMARY;
        }
        int messageType = SharedMethods.getSymbolType(key);
        addAddOptimizedAllrecord(path, messageType, exchange, symbol_instrument);
    }

     public static synchronized void removeOptimizationAllRecord(Integer key, String exchange, String symbol_instrument){
          if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        int path = Constants.PATH_PRIMARY;
        try {
            if ((exchange != null) && (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == Constants.PATH_SECONDARY)) {
                path = Constants.PATH_SECONDARY;
            }
        } catch (Exception e) {
            e.printStackTrace();
            path = Constants.PATH_PRIMARY;
        }
         int messageType = SharedMethods.getSymbolType(key);

        removeAddOptimizedAllrecord(path,messageType,exchange, symbol_instrument);
    }


    public static synchronized void addAddRequest(int path, String exchange, String symbol, int type, int instrumentType, String marketCenter) {
        try {
//            if (path == Constants.PATH_SECONDARY) {
//                InternationalConnector.getSharedInstance().addAddRequest(exchange, symbol, type, instrumentType);
//            } else {
//                if (instance != null)
//                    instance.addAddRequest(exchange, symbol, type, instrumentType);
//            }
            if ((exchange != null) && (!exchange.equals("*")) && (!exchange.equals("")) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
                return;
            }
            String request;
            if (symbol == null) {
                request = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + type + Meta.FD + exchange;
            } else if (marketCenter == null) {
                request = Meta.ADD_SYMBOL_REQUEST + Meta.DS + type + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType;
            } else {
                try {
                    if(symbol.endsWith("."+marketCenter)){
                        symbol = symbol.substring(0,symbol.lastIndexOf("."+marketCenter));
                    }
                } catch(Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                request = Meta.ADD_SYMBOL_REQUEST + Meta.DS + type + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType + Meta.FD + marketCenter;
            }
            addData(path, request);
//            System.out.println("Test add : "+request);
            //Normal
//             String request;
//        if (symbol == null)
//            request = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + type + Meta.FD + exchange;
//        else
//            request = Meta.ADD_SYMBOL_REQUEST + Meta.DS + type + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType + Meta.FD + marketCenter;
//
//        System.out.println("Add Request " + request);

            //International
            // String request;
//        if (symbol == null) {
//            request = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + type + Meta.FD + exchange;
//        } else {
//            request = Meta.ADD_SYMBOL_REQUEST + Meta.DS + type + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType;
//        }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * ************************* Added By Shanika - For bandwidth optimizer**********************************************************************
     */

    public static synchronized void addAddOptimizedrecord(int path, int key,String exchange, String symbol, int instrumentType) {
        try {

            if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
                return;
            }
            String request = Meta.ADD_OPTIMIZED_REQUEST + Meta.DS + key + Meta.FD + exchange + Meta.FD + symbol + Meta.EOL;
            String valForProp = symbol;

            boolean needToReconnect = false;
            ArrayList<String> totalSymbols = new ArrayList<String>();
            Enumeration eekeys = OptimizedTreeRenderer.selectedSymbols.keys();
            while (eekeys.hasMoreElements()) {
                String skey = (String) eekeys.nextElement();
                Stock st = DataStore.getSharedInstance().getStockObject(skey);
                if (st.getExchange().equals(exchange)) {
                    totalSymbols.add(skey);
                }
            }

            Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exchange);
            ArrayList symbolArray = new ArrayList();
            while (exchageSymbols.hasMoreElements()) {
                symbolArray.add((String) exchageSymbols.nextElement());
            }

            int sizeOfExchangesymbolArry = symbolArray.size();                       //16010DGCXDGO13NOV200874000CA:10
            int sizeOfCollectedSymbolarry = totalSymbols.size();

            if (sizeOfCollectedSymbolarry == sizeOfExchangesymbolArry) {
                String keyToRemove = Meta.REMOVE_OPTIMIZED_REQUEST + Meta.DS + key + Meta.FD + exchange;
                MainUI.prop.remove(exchange);

                if(ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()){
                needToReconnect = true;
                }
            } else if (sizeOfExchangesymbolArry > sizeOfCollectedSymbolarry) {
                if (MainUI.prop.containsKey(exchange)) {
                    MainUI.prop.put(exchange, valForProp);
                } else {
                    Enumeration<Object> pkeys = MainUI.prop.keys();
                    while (pkeys.hasMoreElements()) {
                        String keyOnProp = (String) pkeys.nextElement();
                        if (keyOnProp.equals(exchange)) {
                            MainUI.prop.put(keyOnProp, valForProp);
                        } else {
                            if(ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()){

                            needToReconnect = true;
                            }
                            MainUI.prop.put(exchange, valForProp);
                        }
                    }
                    if (MainUI.prop.size() == 0) {
                        if(ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()){

                        needToReconnect = true;
                        }
                        MainUI.prop.put(exchange, valForProp);
                    }
                }
            }
            if (needToReconnect) {
                MainUI.save_BandWidthOptimized_Symbols();
                MainUI.saveSettings();

                Client.getInstance().disconnectFromServer();
            } else {
                addData(path, request);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

      public static synchronized void removeAddOptimizedrecord(int path,int key, String exchange, String symbol, int instrumentType) {
          try {

              if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
                  return;
              }

              String request = Meta.REMOVE_OPTIMIZED_REQUEST + Meta.DS + key + Meta.FD + exchange + Meta.FD + symbol + Meta.EOL;
              String valForProp = symbol;

              boolean needToReconnect = false;
              if (MainUI.prop.containsKey(exchange)) {
                  MainUI.prop.put(exchange, valForProp);
              } else {
                  Enumeration<Object> pkeys = MainUI.prop.keys();
                  while (pkeys.hasMoreElements()) {
                      String keyOnProp = (String) pkeys.nextElement();
                      if (keyOnProp.equals(exchange)) {
                          MainUI.prop.put(keyOnProp, valForProp);
                      } else {
                          if(ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()){
                          needToReconnect = true;
                          }
                          MainUI.prop.put(exchange, valForProp);
                      }
                  }
                  if (MainUI.prop.size() == 0) {
                      if(ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()){
                      needToReconnect = true;
                      }
                      MainUI.prop.put(exchange, valForProp);
                  }
              }
              if (needToReconnect) {
                  MainUI.save_BandWidthOptimized_Symbols();
                  MainUI.saveSettings();
                  MainUI.selectedSymbols = new Hashtable<String, Boolean>();
                  Client.getInstance().disconnectFromServer();
              } else {
                  addData(path, request);
              }
          } catch (Exception e) {
              e.printStackTrace();
          }
      }


     public static synchronized void addAddOptimizedAllrecord(int path, int key, String exchange, String symbol) {
         try {

             if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
                 return;
             }
             String request = Meta.ADD_OPTIMIZED_REQUEST + Meta.DS + key + Meta.FD + exchange + Meta.FD + symbol + Meta.EOL;
             String valForProp = symbol;

             if (Client.WATCHLIST_SELECT) {

                 boolean needToReconnect = false;
                 ArrayList<String> totalSymbols = new ArrayList<String>();
                 Enumeration eekeys = OptimizedTreeRenderer.selectedSymbols.keys();
                 while (eekeys.hasMoreElements()) {
                     String skey = (String) eekeys.nextElement();
                     Stock st = DataStore.getSharedInstance().getStockObject(skey);
                     if (st.getExchange().equals(exchange)) {
                         totalSymbols.add(skey);
                     }
                 }

                 Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exchange);
                 ArrayList symbolArray = new ArrayList();
                 while (exchageSymbols.hasMoreElements()) {
                     symbolArray.add((String) exchageSymbols.nextElement());
                 }

                 int sizeOfExchangesymbolArry = symbolArray.size();                       //16010DGCXDGO13NOV200874000CA:10
                 // String request = Meta.ADD_OPTIMIZED_REQUEST + Meta.DS + key +Meta.FD + exchange + Meta.FD + symbol+ Meta.EOL;
                 int sizeOfCollectedSymbolarry = totalSymbols.size();


                 if (sizeOfCollectedSymbolarry == sizeOfExchangesymbolArry) {
                     MainUI.prop.remove(exchange);
                     if(ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()){

                        Client.needToReconnect = true;
                     }
                 } else if (sizeOfExchangesymbolArry > sizeOfCollectedSymbolarry) {
                     if (MainUI.prop.containsKey(exchange)) {
                         MainUI.prop.put(exchange, valForProp);
                     } else {
                         Enumeration<Object> pkeys = MainUI.prop.keys();
                         while (pkeys.hasMoreElements()) {
                             String keyOnProp = (String) pkeys.nextElement();
                             if (keyOnProp.equals(exchange)) {
                                 MainUI.prop.put(keyOnProp, valForProp);
                             } else {
                                 if(ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()){

                                    Client.needToReconnect = true;
                                 }
                                 MainUI.prop.put(exchange, valForProp);
                             }
                         }
                         if (MainUI.prop.size() == 0) {
                             if(ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()){

                               Client.needToReconnect = true;
                             }
                             MainUI.prop.put(exchange, valForProp);
                         }
                     }
                 }

              /*  if (needToReconnect) {
                     MainUI.save_BandWidthOptimized_Symbols();
                     MainUI.saveSettings();
                     Client.getInstance().disconnectFromServer();
                 } else {
                     addData(path, request);
                }*/
//                Client.WATCHLIST_SELECT = false;
//                 }
            if(!Client.needToReconnect){
                addData(path, request);
             }
         }

             if (!TCPConnector.isTCP) {
                 MainUI.getSharedInstance().addrequestCollector(path, request);
             }

             if (TCPConnector.isTCP) {
            addData(path, request);
                 TCPConnector.isTCP = false;
             }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }


      public static synchronized  void removeAddOptimizedAllrecord(int path, Integer key, String exchange, String symbol) {
        try {

            if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
                return;
            }
              String request = Meta.REMOVE_OPTIMIZED_REQUEST + Meta.DS + key + Meta.FD + exchange + Meta.FD + symbol + Meta.EOL;
              String valForProp = symbol;

              if (Client.WATCHLIST_SELECT) {
//                boolean needToReconnect = false;
//                if (MainUI.prop.containsKey(exchange)) {
//                    MainUI.prop.put(exchange, valForProp);
//                } else {
                      Enumeration<Object> pkeys = MainUI.prop.keys();
                      while (pkeys.hasMoreElements()) {
                          String keyOnProp = (String) pkeys.nextElement();
//                         if (MainUI.prop.containsKey(exchange)) {
////                    MainUI.prop.put(exchange, valForProp);
////                }
//                        if (keyOnProp.equals(exchange)) {
                    if (MainUI.prop.containsKey(exchange)) {
                              MainUI.prop.put(keyOnProp, valForProp);
                          } else {
                              if(ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()){
                            Client.needToReconnect = true;
                              }
                              MainUI.prop.put(exchange, valForProp);
                          }
                      }
                      if (MainUI.prop.size() == 0) {
                          if(ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()){
                        Client.needToReconnect = true;
                          }
                          MainUI.prop.put(exchange, valForProp);
                      }
//                }
//                if (needToReconnect) {
//                    MainUI.save_BandWidthOptimized_Symbols();
//                    MainUI.saveSettings();
//                    MainUI.selectedSymbols = new Hashtable<String, Boolean>();
//                    Client.getInstance().disconnectFromServer();
//                } else {
//                    addData(path, request);
//                }
                if (!Client.needToReconnect) {
            addData(path, request);
                  }

              }

              if (!TCPConnector.isTCP) {
                  MainUI.getSharedInstance().removerequestCollector(path, request);
              }
              if (TCPConnector.isTCP) {
                  addData(path, request);
                  TCPConnector.isTCP = false;
              }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
//  =================================================================================================================================


    public static synchronized void addAnnouncementBodyRequest(String exchange, String id, String language) {
        if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        String request = Meta.COMPRESSED_ANNOUNCEMENT_BODY_REQUEST + Meta.DS + exchange + Meta.FD + id + Meta.FD + language + Meta.EOL;
        byte path = Constants.CONTENT_PATH_PRIMARY;
//        if ((exchange != null)){
//            path = ExchangeStore.getSharedInstance().getExchange(exchange).getPath();
//        }
        new ContentCMConnector(request, exchange, path);

//        if ((exchange != null) && (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == Constants.PATH_SECONDARY)) {
//            InternationalConnector.getSharedInstance().addAnnouncementRequest(exchange, id, language);
//        } else {
//        if (instance != null)
//            instance.addAnnouncementRequest(exchange, id, language);
//    }
    }

    public static synchronized void addNewsBodyRequest(String exchange, String id, String language,String ip,byte spath) {
        if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        String request = Meta.COMPRESSED_NEWS_BODY_REQUEST + Meta.DS + exchange + Meta.FD + id + Meta.FD + language + Meta.EOL;

        if((ip != null) && (!ip.equals(""))) {
            new ContentCMConnector(request,exchange,ip,spath);
        } else{
            if(spath < 1){
                spath = Constants.CONTENT_PATH_SECONDARY;
            }
            if((exchange == null) || (exchange.equals(""))){
                new ContentCMConnector(request, null, spath);
            } else {
                new ContentCMConnector(request, exchange, spath);
            }
        }
/*
        byte path = spath;
        byte path2 = 4;
        if ((exchange.equals(""))){
            new ContentCMConnector(request,null,ip,path2);
        }else{
            new ContentCMConnector(request,exchange,ip,path);

        }*/
//        if ((exchange != null) && (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == Constants.PATH_SECONDARY)) {
//            InternationalConnector.getSharedInstance().addNewsBodyRequest(exchange, id, language);
//        } else {
//            if (instance != null)
//                instance.addNewsBodyRequest(exchange, id, language);
//        }
    }

    /*public static synchronized void addNewsBodyRequest(String exchange, String id, String language) {
        String request = Meta.COMPRESSED_NEWS_BODY_REQUEST + Meta.DS + exchange + Meta.FD + id + Meta.FD + language + Meta.EOL;
        byte path = Constants.CONTENT_PATH_SECONDARY;
//        if ((exchange != null)){
//            path = ExchangeStore.getSharedInstance().getExchange(exchange).getPath();
//        }
        new ContentCMConnector(request,exchange,path);
//        if ((exchange != null) && (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == Constants.PATH_SECONDARY)) {
//            InternationalConnector.getSharedInstance().addNewsBodyRequest(exchange, id, language);
//        } else {
//            if (instance != null)
//                instance.addNewsBodyRequest(exchange, id, language);
//        }
    }*/
    
    public static void createOpraRequest(String sKey, String month, byte viewType, int chainType){
        if(sKey == null) return;
        String reqString = null;
        String exCode = SharedMethods.getExchangeFromKey(sKey);
        if ((exCode != null) && ((ExchangeStore.getSharedInstance().getExchange(exCode).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exCode).isInactive()))) {
            return;
        }
        String symbol = SharedMethods.getSymbolFromKey(sKey);
        int instrumentType = SharedMethods.getInstrumentTypeFromKey(sKey);
        reqString = Meta.OPTION_REQUEST + Meta.DS + exCode + Meta.FD + symbol +  Meta.FD +  instrumentType+ Meta.FD + chainType ;
        if(month != null){
            reqString = reqString + Meta.FD + viewType + Meta.FD + month;
        }
        addData(ExchangeStore.getSharedInstance().getExchange(exCode).getPath(), reqString);
    }

    public static synchronized void addRemoveRequest(String exchange, String symbol, int type, int instrumentType, String marketCenter) {
        if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        int path = Constants.PATH_PRIMARY;
        try {
            if ((exchange != null) && (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == Constants.PATH_SECONDARY)) {
                path = Constants.PATH_SECONDARY;
            }
        } catch(Exception e) {
            path = Constants.PATH_PRIMARY;
        }
        addRemoveRequest(path, exchange, symbol, type, instrumentType, marketCenter);
    }

    public static synchronized void addRemoveRequest(int path, String exchange, String symbol, int type, int instrumentType, String marketCenter) {
        if ((exchange != null) && ((ExchangeStore.getSharedInstance().getExchange(exchange).isExpired()) || (ExchangeStore.getSharedInstance().getExchange(exchange).isInactive()))) {
            return;
        }
        String request;
        if (symbol == null) {
            request = Meta.REMOVE_EXCHANGE_REQUEST + Meta.DS + type + Meta.FD + exchange;
        } else if (marketCenter == null) {
            request = Meta.REMOVE_SYMBOL_REQUEST + Meta.DS + type + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType;
        } else {
            try {
                if(symbol.endsWith("."+marketCenter)){
                symbol = symbol.substring(0,symbol.lastIndexOf("."+marketCenter));
                }
            } catch(Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            request = Meta.REMOVE_SYMBOL_REQUEST + Meta.DS + type + Meta.FD + exchange + Meta.FD + symbol + Meta.FD + instrumentType + Meta.FD + marketCenter;
        }
        addData(path, request);

    }

    public static void clear() {
        if (instance != null)
            instance.clear();
    }


   
}
