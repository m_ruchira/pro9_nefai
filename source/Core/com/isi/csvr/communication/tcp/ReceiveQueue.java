// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.communication.tcp;


import com.isi.csvr.Client;
import com.isi.csvr.communication.ReceiveQInterface;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.communication.SendQInterface;
import com.isi.csvr.history.HistorySettings;
import com.isi.csvr.shared.*;
import com.isi.util.TypeConvert;

import java.io.ByteArrayInputStream;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.FilterInputStream;
import java.net.Socket;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipEntry;
import java.util.zip.ZipInputStream;

/**
 * Receive buffer for the in-comming data
 * Adds all incomming frames to a Queue.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class ReceiveQueue extends Thread implements ReceiveQInterface {
    private List<String> g_qReceiveBuffer;
    private SendQueue g_oSendQueue;
    private InputStream in;
    private Socket g_oSocket;
    private Client client;
    private long frameCount;

//    private OutputStream fout; //remove

    /**
     * Constructor
     */
    public ReceiveQueue(Socket oSocket, InputStream in, Client client, SendQInterface sendQueue, boolean compressionSupported) {
        super("ReceiveQueue");
        g_qReceiveBuffer =  Collections.synchronizedList(new LinkedList<String>()); //Bug ID <#0004>
        //g_qReceiveBuffer =  new LinkedList<String>();
        g_oSendQueue = (SendQueue) sendQueue;
        this.client = client;
        g_oSocket = oSocket;

        try {
            if (compressionSupported){
                this.in = new CompressedBlockInputStream(in);
            } else {
            this.in = in;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        /*try { //remove
            fout = new FileOutputStream("Debug "+System.currentTimeMillis() + ".txt");
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    public long getFrameCount() {
        return frameCount;
    }

    public void incFrameCount() {
        try {
            this.frameCount++;
        } catch (Exception e) {
        }
    }

    /**
     * run method of the thread.
     */
    public void run() {
        boolean newFrame = true;

        StringBuilder sFrame = new StringBuilder("");
        int value = -1;

        try {
            Client.setStatus(Language.getString("STATUS_LIVE"));
            client.setConnectionStatus(Language.getString("STATUS_LIVE"));


            while (Settings.isConnected()) {
                while ((Settings.isConnected())) {
                    value = in.read();
//                    dump((byte)value);
                    if (newFrame) {
                        if (value == 'Z') { // incomming zip data
                            Client.setStatus(Language.getString("STATUS_RECEVING_SNAPSHOT"));
                            byte[] data = readZip();
                            extractZip(data);
                            Client.setStatusProgress(0);
                            if(Settings.isConnected()){
                                Client.setStatus(Language.getString("STATUS_LIVE"));
                            } else {
                                Client.setStatus(Language.getString("NOT_CONNECTED"));
                            }
                            Client.getInstance().setTitleText();
                            data = null;

                            newFrame = true;
                            continue;
                        }
                        newFrame = false;
                    }
                    try {

                        if (value == -1)
                            throw new Exception("End of Stream");
                        if (value != '\n') {
                            sFrame.append((char) value);
                            continue;
                        }
                    } catch (InterruptedIOException e) {
                        continue;
                    }
                    if ((sFrame != null) && (sFrame.length() > 0)) {
//                        g_qReceiveBuffer.addLast(sFrame.toString());
                        addToBuffer(sFrame.toString());
                        incFrameCount();
                        sFrame = null;
                        sFrame = new StringBuilder("");
                        newFrame = true;
                    }

                }
            }
        } catch (Exception e) {
            SharedMethods.printLine("Exception in ReceiveQ");
            e.printStackTrace();
            Sleep(5000);
            closeConnection();
            HistorySettings.interruptDownload();
            Client.setCanShowDisconnectMessage(true);
            while (!Settings.isAuthenticated()) Sleep(1000);
            client.setConnectionStatus(Language.getString("NOT_CONNECTED"));
            Settings.setDisConnected();
            client.setDisConnectedStatus(false);
        }
        try {
            in.close();
            in = null;
        } catch (Exception e) {
        }
    }

    public byte[] readZip() throws Exception {
        byte[] sizeData = new byte[4];

        for (int i = 0; i < sizeData.length; i++) {
            sizeData[i] = (byte) in.read();
//            dump(sizeData[i]);
        }

        int size = TypeConvert.byteArrayToInt(sizeData, 0, 4);
        sizeData = null;

        System.out.println("Receveing Snapshot. Size " + size);

        byte[] data = new byte[size];
        byte[] btempData = new byte[1000];
        int readcount = 0;
        int iValue;

        Client.setStatusMaxProgress(size);

        while (true) {
            if ((size - readcount) >= 1000) {
                iValue = in.read(btempData);
//                dump((byte)iValue);
                if (iValue == -1) {
                    System.out.println("ReceiveQ Zip Read Disconnected");
                    throw new Exception("IO Stream disconnected.");
                }
                for (int j = 0; j < iValue; j++) {
                    data[readcount + j] = btempData[j];//(byte) iValue;
                }
                readcount = readcount + iValue;
                Client.setStatusProgress(readcount);
                if (readcount == size)
                    break;
            } else {
                Client.setStatusProgress(readcount);
                if (readcount == size)
                    break;
                btempData = new byte[size - readcount];
                iValue = in.read(btempData);
//                dump((byte)iValue);
                if (iValue == -1) {
                    System.out.println("ReceiveQ Zip Read Disconnected");
                    throw new Exception("IO Stream disconnected.");
                }
                for (int j = 0; j < iValue; j++) {
                    data[readcount + j] = btempData[j];
                }
                readcount = readcount + iValue;
                Client.setStatusProgress(readcount);
                if (readcount == size)
                    break;
            }
        }
        btempData = null;
        return data;
    }


    public void extractZip(byte[] array) throws Exception {
        StringBuilder sFrame = new StringBuilder();
        int iValue;
        int rs = Meta.RS.charAt(0);
        int counter = 0;

        ZipInputStream zIn = new ZipInputStream(new ByteArrayInputStream(array));

        ZipEntry oEntry = null;
        sFrame.append(Meta.SNAPSHOT_FRAME);
        sFrame.append(Meta.DS);
        sFrame.append(Meta.YES);
        sFrame.append(Meta.FS);

        while (Settings.isConnected()) {
            oEntry = zIn.getNextEntry();
            if (oEntry != null) {
                while (true) {
                    try {
                        iValue = zIn.read();
                        if (iValue == -1) break; // end of streeam
                        if (iValue != rs) {
                            sFrame.append((char) iValue);
                            continue;
                        }
                    } catch (InterruptedIOException e) {
                        Sleep(10);
                        continue;
                    }
                    if ((sFrame != null) && (sFrame.length() > 0)) {
                        addToBuffer(sFrame.toString());
//                        g_qReceiveBuffer.addLast(sFrame.toString());
                        incFrameCount();
                        counter++;
                        // wait till the analyser buffer clears
                        while (counter > 100) {
                            Sleep(10);
                            counter = 0;
                        }
                        sFrame = null;
                        sFrame = new StringBuilder("");
                        sFrame.append(Meta.SNAPSHOT_FRAME);
                        sFrame.append(Meta.DS);
                        sFrame.append(Meta.YES);
                        sFrame.append(Meta.FS);
                    }

                } //while true
                zIn.closeEntry();
                oEntry = null;
            } else {
                break;
            }
            Sleep(100);
        } // Disconnected
        if ((sFrame != null) && (sFrame.length() > 0)) {
            addToBuffer(sFrame.toString());
//            g_qReceiveBuffer.addLast(sFrame.toString());
            incFrameCount();
            sFrame = null;
            sFrame = new StringBuilder("");
        }


        writeData(Meta.ZIP_STATUS + Meta.DS + Meta.ZIP_RECEIVED);
        // add this frame manually to hide the init dialog
        addToBuffer("" + Meta.ZIP_DATA_AVAILABLE + Meta.DS + Meta.NO);
//        g_qReceiveBuffer.addLast("" + Meta.ZIP_DATA_AVAILABLE + Meta.DS + Meta.NO);
//        g_oSendQueue.loadOfflineRequests();
//        Settings.setZipCompleted();
        zIn.close();
        zIn = null;
    }

    private void closeConnection() {
        try {
            g_oSocket.close();
            g_oSocket = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    /**
     * Writes the given string to the output stream
     */
    private void writeData(String sData) throws Exception {
        SendQFactory.writeData(sData);
        SendQFactory.writeData("\n");
    }

    private void addToBuffer(String frame){  //Bug ID <#0002>
        g_qReceiveBuffer.add(frame);
    }

    /**
     * Returns the receive buffer
     */
    public List<String> getReceiveBuffer() {
        return g_qReceiveBuffer;
    }

    /**
     * Sleeps the thread for the given interval
     */
    private void Sleep(long lDelay) {
        try {
            sleep(lDelay);
        } catch (Exception e) {
        }
    }

    /*private void dump(byte i){ //remove
        try {
            fout.write(i);
            fout.flush();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/
}

