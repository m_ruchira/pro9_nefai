package com.isi.csvr.communication.tcp;

import com.isi.csvr.*;
import com.isi.csvr.TradeStation.TradeStationManager;
import com.isi.csvr.smartalerts.SmartAlertManager;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.alert.AlertManager;
import com.isi.csvr.announcement.AnnouncementDownloadManager;
import com.isi.csvr.announcement.AnnouncementStore;
import com.isi.csvr.bandwidthoptimizer.MainUI;
import com.isi.csvr.bandwidthoptimizer.OptimizedTreeRenderer;
import com.isi.csvr.communication.ConnectorInterface;
import com.isi.csvr.communication.ReceiveQFactory;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.customindex.CustomIndexStore;
import com.isi.csvr.datastore.*;
import com.isi.csvr.downloader.TWUpdateDownloader;
import com.isi.csvr.downloader.TodaysOHLCBacklogDownloadManager;
import com.isi.csvr.downloader.TodaysTradeBacklogDownloadManager;
import com.isi.csvr.event.Application;
import com.isi.csvr.forex.CustomSettingsStore;
import com.isi.csvr.globalIndex.GlobalIndexStore;
import com.isi.csvr.metastock.MetaStockManager;
import com.isi.csvr.mutualfund.MutualFundStore;
import com.isi.csvr.news.NewsProvider;
import com.isi.csvr.news.NewsProvidersStore;
import com.isi.csvr.news.NewsStore;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.*;
import com.isi.csvr.tradebacklog.TradeBacklogStore;
import com.isi.csvr.tradebacklog.TradeDownloaderUI;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.trading.AutoTradeLoginDaemon;
import com.isi.csvr.util.SocketTimer;
import com.isi.csvr.vas.MenuLoader;
import com.isi.csvr.win32.NativeMethods;
import com.mubasher.priceAPI.PriceInfoServer;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.net.URL;
import java.net.URLEncoder;
import java.net.URLConnection;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TCPConnector
        extends Thread implements ConnectorInterface {

    public static final int CONECTING = 0;
    public static final int STOPPED = 1;
    public static final int CONNECTED = 2;
    public static final int IDLE = 3;

    private static final String STATUS_LOCK = "STATUS_LOCK";
    public static ArrayList filteredExchanges;
    public static boolean PRIMARY_IP_FORCED = false;
    ReceiveQueue receiveQueue;
    PulseGenerator pulseGenerator;

    private Client client;
    private Socket socket = null;
    private String user;
    private String pass;
    private SendQueue sendQueue;
    private static FrameAnalyser frameAnalyser;
    //private String windowtypes;
    private OutputStream out;
    private static int connectionStatus = IDLE;
    private IP currentIP;
    private String tradingBrokers;
    private String userid;

    private boolean inSafeMode = false;
    private String safeModeCMID = null;
    private long[] sleepTimes = new long[]{5000, 5000, 30000, 60000, (5 * 60000)};
    private int currentIndex = 0;
    private WorkInProgressIndicator applyOfflineIndicator;
    private boolean validationLock = true;
    private boolean compressionSupported = false;

    public static boolean isTCP = false;
    private String ssoCode = "SSO_";
//    byte[] buffer = new byte[1000];

    public TCPConnector() {
        super("TCPConnector");
        this.client = Client.getInstance();
        PrimarySniffer primarySniffer = new PrimarySniffer();
    }

    public void start() {
        super.start();
    }

    public void update() {
    }

    public void run() {
        while (true) {
            try {
                if (getMode() == IDLE) {
                    if (TWControl.isSingleSingnOn() && TWControl.getSSOType() == TWControl.SSO_TYPES.Direct) {
                        if (!Settings.isConnected() && TradingShared.isConnected()) {
                            setMode(CONECTING);
                            System.err.println("Reconnecting................" + SharedMethods.getMemoryDetails());
                            sleepThread(500); // just in case, another requesr comes in
                            connect();
                        } else {
                            sleepThread(1000);
                        }
                    } else if (!Settings.isConnected()) {
//                    if (!Settings.isConnected() && (TWControl.isSingleSingnOn() && TWControl.getSSOType() == TWControl.SSO_TYPES.Direct && TradingShared.isConnected())) {
                        setMode(CONECTING);
                        System.err.println("Reconnecting................" + SharedMethods.getMemoryDetails());
                        sleepThread(500); // just in case, another requesr comes in
                        connect();
                    }
                } else {
                    sleepThread(1000);
                }
            } catch (Exception e) {
                sleepThread(5000);
            }
        }
    }

    private void connect() {
        synchronized (this) {
            boolean isSocketOpened = false;
            try {
                Client.getInstance().getMenus().lockServerConnection();
                String ip;
                while (true) {
                    Client.setDownloadStatus(""); // clear the download / Server Busy message(s)
                    if (getUserInfo()) {
                        ip = getNextIP();
                        System.out.println("Selected IP " + ip);
                        openSocket(ip);
                        isSocketOpened = true;
                        if (authenticate()) {
                            setMode(CONNECTED);
                            Settings.setConnected();
                            client.setConnectionStatus(Language.getString("STATUS_LIVE"));
                            System.out.println("connected>>>>>>>>" + System.currentTimeMillis());

                            //------------------------- Added by Shanika--- to load the optimized symbols--

                            if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
                                try {
                                    MainUI.loadSymbols();
                                } catch (Exception e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                            }
                            initializeApp();
                            WatchListManager.getInstance().removeSymbolsIfNotValidExchange();

                            try {
                                if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
                                    // --------- Added by Shanika for bandwidthop----
                                    MainUI.stockUpdator();
                                }

                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            Settings.isConnectedFirstTime = true;
                            resetConnectionTime();
                            System.out.println("EXIT FROM INITIALIZEAPP ---------------------------");
                            break;
                        } else {
                            Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
                            if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                                TradingShared.setDisconnected();
                                com.isi.csvr.trading.IPSettings.getSharedInstance().resetIPIndex();
                                Client.getInstance().getAutoTradeLoginDaemon().closeSocket();
                                Client.getInstance().getAutoTradeLoginDaemon().closeSecondarySocket();
                                Client.removeLoyaltyPoints();
                                Client.getInstance().setTitleText();
                                setMode(IDLE);
                                break;
                            }
                        }
                    } else {
                        setMode(STOPPED);
                        Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
                        Client.getInstance().setDisConnectedStatus(true);
                        break;
                    }

                    /*} catch (Exception ex1) {
                        ex1.printStackTrace();
                        closeSocket();
                        Settings.setDisConnected();
                        Client.getInstance().setDisConnectedStatus(isStopped());
                        Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
                    }*/
                    if (!isSocketOpened) {
                        resetConnectionTime();
                        sleepThread(3000);
                    }
                    variableSleepTime();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
//                variableSleepTime();
                if (!isSocketOpened) {
                    resetConnectionTime();
                    sleepThread(3000);
                    System.out.println("set Idle mode from line 183");
                    setMode(IDLE);
                }

                // no ips found exception
                closeSocket();
                Settings.setDisConnected();
                Client.getInstance().setDisConnectedStatus(isStopped());
                Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
                sleepThread(5000);
            }
        }
        /*synchronized (this) {
            try {
                Client.getInstance().getMenus().lockServerConnection();
                String ip;
                boolean isSocketOpened = false;
                while (true) {
                    try {
                        Client.setDownloadStatus(""); // clear the download / Server Busy message(s)
                        if (getUserInfo()) {
                            ip = getNextIP();
                            System.out.println("Selected IP " + ip);
                            openSocket(ip);
                            isSocketOpened = true;
                            if (authenticate()) {
                                setMode(CONNECTED);
                                Settings.setConnected();
                                System.out.println("connected>>>>>>>>"+System.currentTimeMillis());
                                initializeApp();
                                resetConnectionTime();
                                System.out.println("EXIT FROM INITIALIZEAPP ---------------------------");
                                break;
                            } else {
                                Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
                            }
                        } else {
                            setMode(STOPPED);
                            Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
                            Client.getInstance().setDisConnectedStatus(true);
                            break;
                        }

                    } catch (Exception ex1) {
                        ex1.printStackTrace();
                        closeSocket();
                        Settings.setDisConnected();
                        Client.getInstance().setDisConnectedStatus(isStopped());
                        Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
                    }
                    if(!isSocketOpened){
                        resetConnectionTime();
                        sleepThread(3000);
                    }
                    variableSleepTime();
                }
            } catch (Exception ex) {
                ex.printStackTrace();
//                variableSleepTime();
                sleepThread(5000);
                // no ips found exception
                setMode(IDLE);
            }
        }*/
    }

    private void variableSleepTime() {
        int previousMode = getMode();
        currentIndex = currentIndex + 1;
        if (currentIndex > (sleepTimes.length - 1)) {
            currentIndex = sleepTimes.length - 1;
        }
//        System.out.println("################  sleep time = "+sleepTimes[currentIndex]);
        setMode(TCPConnector.STOPPED);
        sleepThread(sleepTimes[currentIndex]);
        setMode(previousMode);
    }

    public void resetConnectionTime() {
        try {
            currentIndex = 0;
            this.interrupt();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void setStopped() {
        try {
            this.interrupt();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        setMode(TCPConnector.STOPPED);
    }

    /* private void setDefaultPassword(){
        if(user.equals(pass)){
            boolean isOkClicked = Client.doChangePass();
            if(!isOkClicked){
                try {
                    socket.close();
                } catch (IOException e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
    }*/

    private String getNextIP() throws Exception {

        currentIP = IPSettings.getNextIP();
        String newip;
        Client.getInstance().setConnectionStatus(Language.getString("OBTAINING_IP"));
        if (currentIP.getType() == IP.DYNAMIC) {
            newip = getDynamicIP(currentIP.getIP());
            if (newip.trim().startsWith("IP:")) {
                newip = newip.substring(3);
                Settings.setLastDynamicIP(newip);
            } else {
                throw new Exception("Error in dynamic ip");
            }
        } else {
            newip = currentIP.getIP();
        }
        return newip;
    }

    private String getDynamicIP(String loadBalancerURL) throws Exception {
        System.out.println("Connecting to Load Balancer " + loadBalancerURL);

        URL url = new URL(loadBalancerURL + "&U=" + user);
        URLConnection connection;

        //  Socket lbSocket;
        String billingCode;
        String proVersion;
        if (url.getPort() > 0) {
            //    lbSocket = TWSocket.createSocket(url.getHost(), url.getPort());
        } else {
            //   lbSocket = TWSocket.createSocket(url.getHost(), 80);
        }
        //  lbSocket.setSoTimeout(30000); // make sure to timeout in 30 secs, if no responce
        //    OutputStream out = lbSocket.getOutputStream();
        billingCode = Settings.getBillingCode();
        proVersion = "" + Settings.TW_VERSION;
        if ((billingCode != null) && (!billingCode.equals(""))) {
            billingCode = "&B=" + billingCode;
        } else {
            billingCode = "";
        }
        if ((proVersion != null) && (!proVersion.equals(""))) {
            proVersion = "&V=" + proVersion;
        } else {
            proVersion = "";
        }
        connection = SharedMethods.getURLConnection(loadBalancerURL + URLEncoder.encode("T=1&U=" + user + billingCode + proVersion, "utf-8"));
//                new URL(loadBalancerURL + URLEncoder.encode("T=1&U=" + user + billingCode +proVersion, "utf-8")+ " HTTP/1.0\r\n");
        // out.write(("GET " + loadBalancerURL + URLEncoder.encode("T=1&U=" + user + billingCode +proVersion, "utf-8")+ " HTTP/1.0\r\n").getBytes());    // "T=1&U=" + user + billingCode+proVersion
        System.out.println(("GET " + loadBalancerURL + URLEncoder.encode("T=1&U=" + user + billingCode + proVersion, "utf-8")));    // "T=1&U=" + user + billingCode+proVersion
        //  out.write("\r\n\r\n".getBytes());
        //    connection = url.openConnection();
        // InputStream in = lbSocket.getInputStream();
        connection.setReadTimeout(30000);
        InputStream in = connection.getInputStream();
        StringBuffer newIP = new StringBuffer();
        char ch = (char) in.read();

        while (true) {
            if ((ch != Meta.ERD) && (ch != (char) -1)) {
                newIP.append(ch);
                ch = (char) in.read();
            } else {
                break;
            }
        }
        int ipLocation = newIP.toString().indexOf("IP:");
        newIP = new StringBuffer(newIP.toString().substring(ipLocation));
        System.out.println("Dynamic IP " + newIP.toString());
//        try {
//            lbSocket.close();
//        } catch (IOException e) {
//        }
        if (newIP.toString().trim().equals(""))
            return null;
        else
            return newIP.toString().trim();
    }

    /*private void vv(String ip) throws Exception {
        SocketAddress addr = new InetSocketAddress("socks.mydomain.com", 1080);
        Proxy proxy = new Proxy(Proxy.Type.SOCKS, addr);
        Socket socket = new Socket(proxy);
        InetSocketAddress dest = new InetSocketAddress("server.foo.com", 1234);
        socket.connect(dest);
    }*/

    /*private void openSocket(String ip) throws Exception {
            Client.getInstance().setConnectionStatus(Language.getString("CONNECTING"));
            System.out.println("-------------connecting to ip " + ip);

            socket = TWSocket.createSocket(ip, Constants.HTTP_PRICE_PORT);
            socket.setSoTimeout(60000);
            Settings.setActiveIP(ip);

            Settings.SERVER_PORT = Constants.HTTP_PRICE_PORT;
            Settings.DOWNLOAD_PORT = Constants.HTTP_DOWNLOAD_PORT;
            System.out.println("-------------connected to socket");
    }*/

    private void openSocket(String ip) throws Exception {
        try {
            Client.getInstance().setConnectionStatus(Language.getString("CONNECTING"));
            System.out.println("-------------connecting to ip " + ip);

            socket = TWSocket.createSocket(ip, Constants.DEFAULT_PRICE_PORT);
            socket.setSoTimeout(30000);
            Settings.setActiveIP(ip);

            Settings.SERVER_PORT = Constants.DEFAULT_PRICE_PORT;
            Settings.DOWNLOAD_PORT = Constants.DEFAULT_DOWNLOAD_PORT;
            System.out.println("-------------connected to socket");
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
            /*try {
                Client.getInstance().setConnectionStatus(Language.getString("CONNECTING"));
                System.out.println("-------------connecting to HTTP ip " + ip);

                socket = TWSocket.createSocket(ip, Constants.HTTP_PRICE_PORT);
                socket.setSoTimeout(30000);
                Settings.setActiveIP(ip);

                Settings.SERVER_PORT = Constants.HTTP_PRICE_PORT;
                Settings.DOWNLOAD_PORT = Constants.HTTP_DOWNLOAD_PORT;
                System.out.println("-------------connected to HTTP socket");
            } catch (Exception e2) {
                throw e2;
            }*/
        }
    }

    /*private void openSocketX(String ip) throws Exception {
        try {
            Client.getInstance().setConnectionStatus(Language.getString("CONNECTING"));
            System.out.println("-------------connecting to ip " + ip);
            socket = TWSocket.createSocket(ip, Constants.DEFAULT_PRICE_PORT);
            socket.setSoTimeout(60000);
            Settings.setActiveIP(ip);
            Settings.SERVER_PORT = Constants.DEFAULT_PRICE_PORT;
            Settings.DOWNLOAD_PORT = Constants.DEFAULT_DOWNLOAD_PORT;
            System.out.println("-------------connected to socket");
        } catch (Exception e) {
            e.printStackTrace();
            try {
                Client.getInstance().setConnectionStatus(Language.getString("CONNECTING"));
                System.out.println("-------------connecting to HTTP ip " + ip);
                socket = TWSocket.createSocket(ip, Constants.HTTP_PRICE_PORT);
                socket.setSoTimeout(60000);
                Settings.setActiveIP(ip);
                Settings.SERVER_PORT = Constants.HTTP_PRICE_PORT;
                Settings.DOWNLOAD_PORT = Constants.HTTP_DOWNLOAD_PORT;
                System.out.println("-------------connected to HTTP socket");
            } catch (Exception e2) {
                throw e2;
            }
        }
    }*/

    private boolean getUserInfo() {
        if (!Settings.isSingleSignOnMode() && !isPasswordSaved()) {
            LoginWindow loginDialog = new LoginWindow(Client.getInstance().getFrame(), Login.MODE_PRICE_SERVERS);
//            Login loginDialog = new Login(Client.getInstance().getFrame(), Login.MODE_PRICE_SERVERS);
            while (true) {
                if (loginDialog.showDialog()) {
                    user = loginDialog.getUserName().trim();
                    pass = loginDialog.getPassword().trim();
                    Settings.setUserDetails(user, pass);
                    resetConnectionTime();
                    loginDialog.dispose();
                    loginDialog = null;
                    return true;
                } else {
//                    Client.getInstance().setTitleText();//Language.getString("STATUS_NOT_CONNECTED"));
//                    Client.getInstance().setConnectionStatus(Language.getString("MSG_PRICE_DISCONNECTION_WARNING"), false);
                    Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"), false);
                    loginDialog = null;

                    if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
                        try {
                            File file = new File(Settings.SYSTEM_PATH + "/optimizedSymbols.dll");
                            if (file.exists()) {
                                MainUI.load_BandWidthOptimized_Symbols();
                            }
                            if (!file.exists()) {
                                MainUI.firstTimeLoaded();
                            }
                        } catch (IOException e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                        MainUI.stockUpdator();
                    }


                    if (!Settings.isConnectedFirstTime && !Settings.isOfflineDataLoaded) {
                        applyOfflineIndicator = new WorkInProgressIndicator(WorkInProgressIndicator.APPLYING_OFFLINE_DATA);
                        applyOfflineIndicator.setVisible(true);
                        Settings.setOfflineMode(true);
                        System.out.println("start of work in indicator");
                        new Thread("Offline Data Thread") {
                            public void run() {
                                Application.getInstance().fireLoadOfflineData();
                                if (applyOfflineIndicator != null) {
                                    applyOfflineIndicator.dispose();
                                }
                                Settings.isOfflineDataLoaded = true;
                            }
                        }.start();
                    }
                    UpdateNotifier.setShapshotUpdated(); // make sure main boards are repainted for offline date
                    return false; // login cancelled by user
                }
            }
        }
        return true;
    }

    private boolean authenticate() throws Exception {

        Client.getInstance().setConnectionStatus(Language.getString("STATUS_AUTHENTICATE"));

        StringBuffer clientInfo = new StringBuffer();
        clientInfo.append(NativeMethods.getOSName());
        clientInfo.append("|");
        clientInfo.append(NativeMethods.getMemorySize());
        clientInfo.append("|");
        clientInfo.append(NativeMethods.getCPUSpeed());
        clientInfo.append("|");
        clientInfo.append(Settings.systemLocale);
        clientInfo.append("|");
        clientInfo.append(NativeMethods.getBuildDate());
        clientInfo.append("|");
        clientInfo.append(NativeMethods.getJetVersion());
        clientInfo.append("|");
        clientInfo.append(NativeMethods.getVolumeSerial());
        clientInfo.append("|");
        clientInfo.append(getFreeDiskSpace());

        String ssoString = "";
        /*if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web)) {
           // if (!Settings.getTradeToken().equals("")) {
            //    ssoString = Meta.SSO_ENABLED + Meta.DS + Meta.YES + Meta.FS +
            //            Meta.INSTITUTION_ID + Meta.DS + Settings.getInstitutionID() + Meta.DS + Settings.getTradeToken();
           // } else {
                ssoString = Meta.SSO_ENABLED + Meta.DS + Meta.YES + Meta.FS +
                        Meta.INSTITUTION_ID + Meta.DS + Settings.getInstitutionID() + Meta.FS + Settings.getSingleSignOnID();
           // }
        } else if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
//            ssoString = Meta.SSO_ENABLED + Meta.DS + Meta.YES + Meta.FS +
//                    Meta.INSTITUTION_ID + Meta.DS + Settings.getInstitutionID() + Meta.DS + Settings.getSingleSignOnID();
            ssoString = Meta.SSO_ENABLED + Meta.DS + Meta.YES + Meta.FS +
                    Meta.INSTITUTION_ID + Meta.DS + Settings.getInstitutionID() + Meta.FS + Settings.getSingleSignOnID();
        }*/

        if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web)) {
           // if (!Settings.getTradeToken().equals("")) {
            //    ssoString = Meta.SSO_ENABLED + Meta.DS + Meta.YES + Meta.FS +
            //            Meta.INSTITUTION_ID + Meta.DS + Settings.getInstitutionID() + Meta.DS + Settings.getTradeToken();
           // } else {
                ssoString = Meta.SSO_ENABLED + Meta.DS + Meta.YES + Meta.FS +
                        Meta.INSTITUTION_ID + Meta.DS + Settings.getInstitutionID();
           // }
        } else if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
//            ssoString = Meta.SSO_ENABLED + Meta.DS + Meta.YES + Meta.FS +
//                    Meta.INSTITUTION_ID + Meta.DS + Settings.getInstitutionID() + Meta.DS + Settings.getSingleSignOnID();
            ssoString = Meta.SSO_ENABLED + Meta.DS + Meta.YES + Meta.FS +
                    Meta.INSTITUTION_ID + Meta.DS + Settings.getInstitutionID();
        }

        String userName;
        if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web)) {
           // if (!Settings.getTradeToken().equals("")) {
           //     userName = Settings.getTradeToken();
           // } else {
                userName = Settings.getSingleSignOnID();
           // }
        } else if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
            userName = Settings.getSingleSignOnID();
        } else {
            userName = SharedMethods.encrypt(user);
        }

        String sessionID;
        if (inSafeMode) {
            sessionID = "*";
        } else {
            sessionID = Settings.getSessionID();
        }

        String sAuthString =
                Meta.AUTHENTICATION_REQUEST_VERSION + Meta.DS + TWControl.getIntItem("AUTH_VERSION", 1)/* Requested by JAM */ + Meta.FS +
                        Meta.USER_NAME + Meta.DS + userName + Meta.FS +
                        Meta.USER_PASSWORD + Meta.DS + SharedMethods.encrypt(pass) + Meta.FS +
                        Meta.USER_VERSION + Meta.DS + Settings.TW_VERSION + Meta.FS +
                        Meta.USER_SESSION + Meta.DS + sessionID + Meta.FS +
                        Meta.CONNECTION_PATH + Meta.DS + Meta.CONNECTION_PATH_PRIMARY + Meta.FS +
                        Meta.USER_INFO + Meta.DS + clientInfo.toString() + Meta.FS +
                        Meta.USER_LANGUAGE + Meta.DS + Language.getSelectedLanguage() + Meta.FS +
                        Meta.COMPRESSESSOPN_SUPPORTED + Meta.DS + Meta.YES + Meta.FS +
                        Meta.USER_TYPE + Meta.DS + Meta.USER_TYPE_NEW_TW + Meta.FS + ssoString + "\n";

        //sAuthString = "150\u00021\u001C20\u0002\u0019,JcttzxJcttz}JcttzQJctt{yJcttbzR\u001C21\u0002\u0019?]v1142]v1143]v1144\u001C23\u00028.84.002\u001C22\u00024B717A37-BA3A-DC89-E040-1CAC510268AC\u001C25\u0002Windows XP|2040|1.69G|US en|17/04/2008|Sun 1.6.0|6C3326F3|6.893\u001C26\u0002EN\u001C24\u00021\u001C";
        out = socket.getOutputStream();
        System.out.println(sAuthString);
        System.out.println("out>>>>>>>>>>>>>" + System.currentTimeMillis());
        out.write(sAuthString.getBytes());
        InputStream in = socket.getInputStream();
        SocketTimer soTimer = new SocketTimer((socket)); // Activate the socket timer
        String g_sAuthResult = readLine(in); // 1 auth reply
        System.out.println("AUTH_RESULT ="+g_sAuthResult);
        soTimer.deactivate(); // deactivate the socket timer
        soTimer = null;

        inSafeMode = false;
        int result = (isAuthenticationValid(g_sAuthResult));
        if (result == Meta.YES) { // authenticated successfully
            System.out.println("Normal Authentication Sucess");
            return true;
        } else if (result == Meta.SAFE_MODE) { // check if safe in mode
            System.out.println("In Safe mode");
            if (Settings.isSingleSignOnMode()) {
                g_sAuthResult = getSavedUserData(ssoCode + Settings.getBillingCode()); // get the saved user data string
            } else {
                g_sAuthResult = getSavedUserData(user);
            }
            System.out.println("Loading saved user");
            if (g_sAuthResult != null) {
                System.out.println("Saved user found");
                inSafeMode = true;
                result = (isAuthenticationValid(g_sAuthResult)); // do the authentication again with saved data
                if (result == Meta.YES) { // authenticated successfully
                    System.out.println("Safe mode authentication sucess");
                    Settings.setTWPath(safeModeCMID);
                    return true;
                } else {
                    System.out.println("Safe mode authentication failed");
                    return false;
                }
            } else {
                System.out.println("Saved user not found");
                return false;
            }
        } else {
//            System.out.println("Authentication failed");
            return false;
        }
    }

    private String getSavedUserData(String user) {
        try {
            SmartProperties userData = new SmartProperties(Meta.RS);
            userData.loadCompressed(Settings.SYSTEM_PATH + "/udb.msf");
            return userData.getProperty(user);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    private float getFreeDiskSpace() {
        try {
            return NativeMethods.getFreeDiskSpace(System.getProperties().getProperty("user.dir").substring(0, 2));
        } catch (Exception e) {
            return -1;
        }
    }

    public String readLine(InputStream in) throws Exception {
        int iValue;
//        int index = 0;
        StringBuilder buffer = new StringBuilder();

        while (true) {
            iValue = in.read();
//            System.out.println("iValue = "+iValue);
            if (iValue == -1)
                throw new Exception("End of Stream");

            if (iValue != '\n') {
                buffer.append((char) iValue);
//                buffer[index] = (byte) iValue;
//                index++;
            } else {
//                String line = new String(buffer, 0, index);
//                return line;
                return buffer.toString();
            }
        }
    }


    private void initializeApp() throws Exception {
        DataStore.getSharedInstance().init(true);
        System.out.println("currency store cleared +++++++++++++++++++++++++++++++");
        CurrencyStore.getSharedInstance().clearCurrencies();
        Settings.setHistoryCondition((short) -1); // make invalid
        Settings.setConnected();
        //todo --------
//        if(!TradeStore.isInitCalled()){
//            TradeStore.init();
//        }
        //todo -------
        TradeBacklogStore.getSharedInstance().clear();
        TradeDownloaderUI.getSharedInstance().setProcessCompleted("", true); // complete any disconnected downloads
        TradeDownloaderUI.getSharedInstance().exchangesAdded(false);

        sendQueue = null;
        sendQueue = (SendQueue) SendQFactory.createSendQ(out);

        receiveQueue = null;
        receiveQueue = (ReceiveQueue) ReceiveQFactory.createReceiveQueue(socket, socket.getInputStream(), client, sendQueue, compressionSupported);
        frameAnalyser = null;
        pulseGenerator = null;
        frameAnalyser = new FrameAnalyser(receiveQueue.getReceiveBuffer());
        Client.getInstance().initializeFrameAnalyser(frameAnalyser);
        frameAnalyser.setClient(client);
        frameAnalyser.addFrame(tradingBrokers);
        frameAnalyser.addFrame(userid);
        pulseGenerator = new PulseGenerator();
        receiveQueue.start();

// Checking the Password and Username, if both are equals then call the change password dialog
        try {
            if (!Settings.isSingleSignOnMode() && user.equals(pass)) {
                String mess = Language.getString("FORCING_PASSWORD");
                SharedMethods.showMessage(mess, JOptionPane.INFORMATION_MESSAGE);
                /*int n = JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                Language.getString("MSG_PASSWORD_AND_USERNAME"),
                Language.getString("ERROR"),
                JOptionPane.OK_OPTION,
                JOptionPane.ERROR_MESSAGE,
                null,
                options,
                options[0]);*/
                boolean isOkClicked = Client.doChangePass();
                if (!isOkClicked) {

                    try {
                        socket.close();
                    } catch (IOException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    setMode(IDLE);
                    Settings.setAuthenticated();
                    return;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        frameAnalyser.start();

        NewsStore.getSharedInstance().clear();

        if (ExchangeStore.getSharedInstance().isSecondaryPathConnectionRequired()) {
            InternationalConnector.getSharedInstance().setReceiveBuffer(receiveQueue.getReceiveBuffer());
            InternationalConnector.getSharedInstance().activate();
            InternationalConnector.getSharedInstance().initiate();
        }
        TodaysTradeBacklogDownloadManager.getSharedInstance().setBuffer(receiveQueue.getReceiveBuffer());
        TodaysOHLCBacklogDownloadManager.getSharedInstance().setBuffer(receiveQueue.getReceiveBuffer());
        AnnouncementDownloadManager.getSharedInstance().download(10);
        CustomIndexStore.getSharedInstance().initializeStore();

        //------------------------------- Added by Shanika -------- To load from file-------------


        filteredExchanges = new ArrayList();

        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
            File files = new File(Settings.SYSTEM_PATH + "/propReq.dll");
            if (files.exists()) {
                MainUI.loadSymbols();
                //---------------------------- To filter exchanges -------
                Enumeration<Object> pkeys = MainUI.prop.keys();
                while (pkeys.hasMoreElements()) {

                    String keyOnProp = (String) pkeys.nextElement();
                    boolean isValid = ExchangeStore.getSharedInstance().isValidExchange(keyOnProp);
                    if (isValid) {
                        filteredExchanges.add(keyOnProp);
                    } else {
                        System.out.println("===========================invalid ex=====================" + keyOnProp);
                    }
                }
            }
        }
        // Request exchange related data
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            try {
                Exchange exchange = (Exchange) exchanges.nextElement();

//                if (exchange.getSymbol().equals("DFM")){
                if (exchange.isExpired()) {
                    DataStore.getSharedInstance().setExpiredStatus(exchange.getSymbol());
                    continue;
                }

                if (exchange.isInactive()) {
//                    DataStore.getSharedInstance().setExpiredStatus(exchange.getSymbol());
//                    continue;
                }

                if (exchange.isDefault()) {

                    try {

                        if (!filteredExchanges.contains(exchange.getSymbol())) {
                            SendQFactory.addAddRequest(exchange.getSymbol(), null, Meta.ALL, -1, null); // request snapshot updates
                            exchange.setExchageMode(true);
                        } else {
                            //===For symbol mode ====
                            exchange.setExchageMode(false);
                            System.out.println("=======================Exchange======================" + exchange.getSymbol());
                            SendQFactory.addAddRequest(exchange.getSymbol(), null, Meta.MARKET, -1, null);  // request market updates
                            SendQFactory.addAddRequest(exchange.getSymbol(), null, Meta.ANNOUNCEMENTS, Meta.ANNOUNCEMENTS, null); // trade history file list
                            SendQFactory.addData(Constants.PATH_PRIMARY, Meta.DAILY_TRADES + Meta.DS + exchange.getSymbol() + Meta.EOL); // trade history file list

                        }
                        //if (exchange.isValidIinformationType(Meta.IT_MetaStock)) {  //Bug ID <#????> metastok subscription is not exchange based
                        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_MetaStock)) {
                            MetaStockManager.startIntrdayUpdator(exchange.getSymbol());
                        }

                        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_TRADE_STATION)) {
                            TradeStationManager.startIntrdayUpdator(exchange.getSymbol());
                        }

                        if (exchange.isValidIinformationType(Meta.IT_MarketTimeAndSales)) {
                            String requestID = Meta.MESSAGE_TYPE_COMBINED_TRADE + "|" + exchange.getSymbol();
                            Client.getInstance().addMarketTradeRequest(requestID, exchange.getSymbol());
                        }

                        /*if (exchange.isValidIinformationType(Meta.IT_SymbolTimeAndSales)) {
                            SendQFactory.addAddRequest(exchange.getSymbol(), null, Meta.COMBINED_TRADE); // register for time and sales
                        }*/
                        SendQFactory.addAddRequest(exchange.getSymbol(), null, Meta.INTRADAYOHLC, -1, null); // register for ohlc
                        //   if(ExchangeStore.isValidIinformationType(exchange.getDisplayExchange(), Meta.IT_TWAP_ClosingVWAP)){
                        String requestID = Meta.MESSAGE_TYPE_PERIODIC_INDICATOR + "|" + exchange.getSymbol();
                        Client.getInstance().addSR6Request(requestID,"TDWL" ,263);
                //        }

                        // request ohlc intraday for indexes
                        Enumeration<String> symbols = DataStore.getSharedInstance().getFilteredList(exchange.getSymbol(), Meta.INSTRUMENT_INDEX);
                        while (symbols.hasMoreElements()) {
                            String symbol = symbols.nextElement();
                            try {
                                SendQFactory.addAddRequest(exchange.getSymbol(), symbol, Meta.INDEXOHLC, Meta.INSTRUMENT_INDEX, null);
                            } catch (Exception e) {
                                System.out.println(symbol);
                                e.printStackTrace();
                            }
                            symbol = null;
                        }
                        symbols = null;
                        SendQFactory.addAddRequest(exchange.getSymbol(), null, Meta.MARKET, -1, null); // request market updates
                    } catch (Exception e) {
                        System.out.println("---------------- Failed for " + exchange.getSymbol());
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                } else {
                    SendQFactory.addAddRequest(exchange.getSymbol(), null, Meta.MARKET, -1, null);  // request market updates
                    SendQFactory.addAddRequest(exchange.getSymbol(), null, Meta.ANNOUNCEMENTS, Meta.ANNOUNCEMENTS, null); // trade history file list
                    SendQFactory.addData(Constants.PATH_PRIMARY, Meta.DAILY_TRADES + Meta.DS + exchange.getSymbol() + Meta.EOL); // trade history file list
                }
                ExchangeStore.getSharedInstance().rebuildAllMarkets();
//            SendQFactory.addAddRequest(exchange.getSymbol(), null, Meta.MARKET); // request market updates
                exchange = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        exchanges = null;

        GlobalIndexStore.getSharedInstance().sendAddSymbolRequests();
        //loading forex custom data...
        CustomSettingsStore.getSharedInstance().load();
        //  AlertManager.getSharedInstance().requestHistory();
        SmartAlertManager.getSharedInstance().requestHistory(userid);

        //==============================================================================================
        /*
       * Sending request to get Expiary dates of Exchanges
       * */
//             String exgExpiaryRequest=Meta.ADD_EXCHANGE_EXPIARYDATE_REQUEST + Meta.DS + Settings.getUserID()+"\n";
//             out.write(exgExpiaryRequest.getBytes());
        SendQFactory.addData(Constants.PATH_PRIMARY, Meta.ADD_EXCHANGE_EXPIARYDATE_REQUEST + Meta.DS + Settings.getUserID());
//             SendQFactory.addData(Constants.PATH_PRIMARY,1000+ Meta.DS + "Sexy Chandika");
        //=============================================================================================

        //===============================================================================================
        try {
            NewsProvidersStore.getSharedInstance().loadProviders();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        Enumeration<NewsProvider> nProviders = NewsProvidersStore.getSharedInstance().getAllProviders();
        while (nProviders.hasMoreElements()) {
            try {
                NewsProvider np = nProviders.nextElement();
                /*if((np.isSelected()) && (np.getPath() == Constants.PATH_PRIMARY)){
                                    SendQFactory.addData(np.getPath(), Meta.NEWS + Meta.DS + np.getID() + Meta.FD + 1); // + Meta.FD + Constants.NEWS_ADD_ID);
                    }*/
                System.out.println("np id and np is selected==" + np.getID() + " , " + np.isSelected());
                if ((np.isSelected())) {
                    SendQFactory.addData(Constants.PATH_PRIMARY, Meta.NEWS + Meta.DS + np.getID() + Meta.FD + 1); // + Meta.FD + Constants.NEWS_ADD_ID);
//					SendQFactory.addData(Constants.PATH_SECONDARY, Meta.NEWS + Meta.DS + np.getID() + Meta.FD + 1); // + Meta.FD + Constants.NEWS_ADD_ID);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        //**********************************************************************************************
//              SendQFactory.addData(Constants.PATH_PRIMARY,   Meta.NEWS + Meta.DS + "MUBASHER.US");
//              SendQFactory.addData(Constants.PATH_SECONDARY, Meta.NEWS + Meta.DS + "MUBASHER");
//              SendQFactory.addData(Constants.PATH_SECONDARY, Meta.NEWS + Meta.DS + "DWJN");
//              SendQFactory.addData(Constants.PATH_SECONDARY, Meta.NEWS + Meta.DS + "MUBASHER.US");
        //**********************************************************************************************
        //===============================================================================================
        //////////////////////////////////////////////////////////////////////////////////////////////////////////////
//        Enumeration<String> newsProviders = NewsProviders.getSharedInstance().getAllowedProviders();
//        Enumeration<String> newsProviders = NewsProviders.getSharedInstance().getNewsProviders();
//        while (newsProviders.hasMoreElements()) {
//            String provider = newsProviders.nextElement();
//            if (NewsProviders.getSharedInstance().isGlobalProvider(provider)) {
//                exchanges = ExchangeStore.getSharedInstance().getExchanges();
//                while (exchanges.hasMoreElements()) {
//                    Exchange exchange = (Exchange) exchanges.nextElement();
//                    SendQFactory.addData(exchange.getPath(), Meta.NEWS + Meta.DS + provider + "." + exchange.getCountry()); // + Meta.FD + Constants.NEWS_ADD_ID);
//                    exchange = null;
//                }
//                exchanges = null;
//            } else {
//               SendQFactory.addData(Constants.PATH_PRIMARY, Meta.NEWS + Meta.DS + provider);
//               SendQFactory.addData(Constants.PATH_SECONDARY, Meta.NEWS + Meta.DS + provider);
//            }
//        }
        ////////////////////////////////////////////////////////////////////////////////////////////////////////////////
        /* change the status o connected */
        client.setConnectedStatus();
        Settings.setAuthenticated();
//        client.getMenus().checkMenuAuthentication();
        MutualFundStore.getInstance().clear();
//        DetailedTradeStore.getSharedInstance().init(); //--- commented shanika -with cachedstore changes

        DataStore.getSharedInstance().sendAddRequests(Constants.PATH_PRIMARY); // register nondefault symbols with server
        setMode(TCPConnector.CONNECTED);
        Application.getInstance().fireApplicationReadyForTransactions();

        //LatancyIndicator.getSharedInstance().sendThresholdRequest();
        LatancyIndicator.getSharedInstance().activate();

        try {
            if ((Settings.isTCPMode()) && (ExchangeStore.isValidSystemFinformationType(Meta.IT_VAS))) {
                if ((!MenuLoader.isInProgress()) && (!MenuLoader.isDownloaded())) {
                    MenuLoader.getSharedInstance().addListener(client.getMenus());
                    MenuLoader.getSharedInstance().start();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        AnnouncementStore.getSharedInstance().applyAnnouncementBulbs(); // reapply bulbs for all announcements

        //sendQueue.addData(Meta.BROADCAST_MSG_HISTORY + Meta.DS + 0 + Meta.FD + Settings.getUserID());

        /* Subsctiption based restrictions apply from here */
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Config)) {
            Settings.saveItem("CONFIG_ALLOWED", "true");
        } else {
            Settings.RemoveItem("CONFIG_ALLOWED");
        }

        sendQueue.loadOfflineRequests();

        MessageList.getSharedInstance().requestHistory();
        client.getMenus().checkMenuAuthentication();

        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Google)) {
            SharedMethods.activateGoogleAnalytics();
        }
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_PriceAPI)) {
            PriceInfoServer.start();
        }
        // sending validated requests
        //These requests are sending only a once a day
        if (!Settings.isOnceADayValidationNeed()) {
            new Thread("Once a Day Validation") {
                public void run() {
                    try {
                        sleep(10000);
                        if (Settings.isConnected()) {
                            sendValidationRequests();
                        }
                    }
                    catch (InterruptedException ex) {
                    }
                }
            }.start();

        }

// ---------------------------- Shanika------------------------------


        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
            try {
                File file = new File(Settings.SYSTEM_PATH + "/optimizedSymbols.dll");
                if (file.exists()) {
                    //      MainUI.getSharedInstance();
                    MainUI.load_BandWidthOptimized_Symbols();
                }
                if (!file.exists()) {

                    MainUI.firstTimeLoaded();
                }
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

// ================================ To send requests =====================================

            for (int p = 0; p < filteredExchanges.size(); p++) {
                String ex = (String) filteredExchanges.get(p);
                Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();

//------  to control symbol request length --------

                try {
                    Enumeration ssskeys = OptimizedTreeRenderer.selectedSymbols.keys();
                    while (ssskeys.hasMoreElements()) {
                        String key = (String) ssskeys.nextElement();
                        boolean workspaceSetting = OptimizedTreeRenderer.selectedSymbols.get(key);
                        Stock st = DataStore.getSharedInstance().getStockObject(key);

                        if (st != null) {
                            if (st.getExchange().equals(ex)) {

                                int instrument_type = st.getInstrumentType();
                                if (exchangeRequests.containsKey(instrument_type)) {
                                    ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(instrument_type);
                                    gettingArrayFromHash.add(st);
                                } else {
                                    ArrayList<Stock> freshArray = new ArrayList<Stock>();
                                    freshArray.add(st);
                                    exchangeRequests.put(instrument_type, freshArray);
                                }
                            }
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

                try {
                    Enumeration<Integer> exkeys = exchangeRequests.keys();
                    while (exkeys.hasMoreElements()) {
                        Integer key = exkeys.nextElement();
                        ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(key);

                        int arraySize = (gettingArrayFromHash.size() / 100);
                        int reqDevider = arraySize + 1;

                        int position = 0;
                        int max = gettingArrayFromHash.size();

                        for (int j = 1; j <= reqDevider; j++) {
                            ArrayList exchangeSymbols = new ArrayList();

                            int currentPos;
                            if ((j * 100) > max) {
                                currentPos = max;
                            } else {
                                currentPos = (j * 100);
                            }
                            for (int i = position; i < currentPos; i++) {
                                Stock fulKey = gettingArrayFromHash.get(i);
                                int messageType = SharedMethods.getSymbolType(fulKey.getInstrumentType());
                                Exchange exx = ExchangeStore.getSharedInstance().getExchange(fulKey.getExchange());
                                if ((exx.isUserSubMarketBreakdown()) && (exx.getSubMarketCount() > 0)) {
                                    exchangeSymbols.add(fulKey.getSymbolCode() + ":" + fulKey.getInstrumentType());
                                } else {
                                    exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
                                }
                                position = position + 1;
                            }
                            String selectedExchangesSymbols = "";
                            for (int f = 0; f < exchangeSymbols.size(); f++) {
                                selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
                            }
                            String exchangeAddRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);

                            isTCP = true;
                            SendQFactory.addOptimizationAllRecord(key, ex, exchangeAddRequest);
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }


            }
            //  ===================================== End of To send saved requests =========================================
        }

        SymbolMaster.updateAlias();
        DataStore.getSharedInstance().sendOfflineRequest();

    }

    public void sendingRequests() {


    }

    private void sendValidationRequests() {
        Hashtable<String, String> symbols = new Hashtable<String, String>();
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/validated.msf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            symbols = (Hashtable<String, String>) oObjIn.readObject();
            oIn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (symbols == null) {
            symbols = new Hashtable<String, String>();
        }
        Enumeration validatedEnum = symbols.keys();
        while (validatedEnum.hasMoreElements()) {
            try {
                String key = (String) validatedEnum.nextElement();
                String symbolName = SharedMethods.getSymbolFromKey(key);
                String exchange = SharedMethods.getExchangeFromKey(key);
                Exchange exg = ExchangeStore.getSharedInstance().getExchange(exchange);
                if ((!exg.isExpired()) && (!exg.isInactive())) {

                    String requestID = "OnceaDayValidation:" + System.currentTimeMillis();
                    DataStore.getSharedInstance().checkSymbolAvailability(symbolName, requestID, exchange, SharedMethods.getInstrumentTypeFromKey(key));
//                    SendQFactory.addValidateRequest(symbolName, requestID, exchange, SharedMethods.getInstrumentTypeFromKey(key));
                    SymbolsRegistry.getSharedInstance().rememberRequest(requestID, this);
                    SimpleDateFormat sdfOutput = new SimpleDateFormat("MM:dd:yyyy");
                    Date dt = new Date();
                    Settings.setCurrentDay(sdfOutput.format(dt));
                } else {
                    // not sending validations
                }
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

    }

    private boolean isPasswordSaved() {
        pass = Settings.getUserPassword();
        user = Settings.getUserName();
        if (TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
            return true;
        }
        if ((pass == null) || (pass.trim().equals(Constants.NULL_STRING))) {
            pass = null;
            return false;
        } else if ((user == null) || (user.trim().equals(Constants.NULL_STRING))) {
            user = null;
            return false;
        } else {
            // if the password is saved take the username from the file too
            user = Settings.getUserName();
            return true;
        }
    }

    private void showExpiryMessages(String date, String days) {
        try {
            boolean critical = false;

            String message = "";

            try {
                if (Integer.parseInt(days) < 7) {
                    critical = true;
                }
            } catch (NumberFormatException e) {
                critical = false;
            }

            if ((date != null) && (!date.equalsIgnoreCase("null"))) {
                date = date.substring(6) + " / " + date.substring(4, 6) + " / " + date.substring(0, 4);
                message = Language.getString("STATUS_ACCOUNT_EXPIRE");
                if (Settings.isShowArabicNumbers()) {
                    message = message.replaceAll("\\[DAY\\]", UnicodeUtils.getArabicNumbers(date));
                } else {
                    message = message.replaceAll("\\[DAY\\]", date);
                }
            }
            Client.setExpiryStatus(message, critical);
            message = null;
        } catch (Exception e) {
        }
    }

    private int isAuthenticationValid(String frame) {
        Object[] options = {
                Language.getString("OK")};

        boolean safeMode = false;
        boolean authenticated = false;
        boolean blocking = false;
        String message = null;
        String exchanges = null;
        String exchangeParams = null;
        String expiredExchanges = null;
        String inactiveExchanges = null;
        String windowTypes = null;
        String expiryDate = null;
        String expiryDays = null;
        String protocolVer = null;

        compressionSupported = false;

        String[] fields = frame.split(Meta.FS);
        for (int i = 0; i < fields.length; i++) {
            String[] data = fields[i].split(Meta.DS);
            try {
                switch (Integer.parseInt(data[Meta.TAG])) {
                    case Meta.AUTHENTICATION_REQUEST_VERSION:
                        protocolVer = data[Meta.VALUE];
                        break;
                    case Meta.AUTH_RESULT:
                        authenticated = (SharedMethods.intValue(data[Meta.VALUE]) == Meta.YES);
                        if (!authenticated) {
                            if (SharedMethods.intValue(data[Meta.VALUE]) == Meta.SAFE_MODE) {
                                safeMode = true;
                            }
                        } else {
                            client.setConnectionStatus(Language.getString("STATUS_LIVE"));
                        }
                        break;
                    case Meta.AUTH_BLOCKING:
                        if (!safeMode) {
                            blocking = (SharedMethods.intValue(data[Meta.VALUE]) == Meta.YES);
                        }
                        break;
                    case Meta.USER_SESSION:
                        if (authenticated) {
                            Settings.setSessionID(data[Meta.VALUE]);
                        }
                        break;
                    case Meta.WINDOW_TYPES:
                        if (!safeMode) {
                            if (authenticated) {
                                windowTypes = data[Meta.VALUE];
                                System.out.println("==> Window Types " + windowTypes);
//                                ExchangeStore.getSharedInstance().addWindowTypes(windowTypes);
                            }
                        }
                        break;
                    case Meta.AUTH_MESSAGE:
                        if (!safeMode) {
                            message = UnicodeUtils.getNativeString(Language.getLanguageSpecificString(data[Meta.VALUE]));
                        }
                        break;
                    case Meta.EXCHANGE_PARAMETERS:
                        if (!safeMode) {
                            exchangeParams = data[Meta.VALUE];
                            System.out.println("==> Exchange Params " + exchangeParams);
                        }
                        break;
                    case Meta.EXCHANGE_LIST:
                        if (!safeMode) {
                            if (authenticated) {
                                exchanges = data[Meta.VALUE];
//                                System.out.println("==> Subscribed Exchanges " + exchanges);
//                                ExchangeStore.getSharedInstance().addExchangeList(exchanges, exchangeParams);
                            }
                        }
                        break;
                    case Meta.TRADING_BROKERS:
                        if (!safeMode) {
                            tradingBrokers = fields[i];
                        }
                        break;
                    case Meta.USERID:
                        if (!safeMode) {
                            userid = fields[i];
                        }
                        break;
                    case Meta.TW_VERSION:
                        if (!(safeMode || inSafeMode)) { // if not safe mode triggerd of already in safe mode do not take the version
                            System.out.println("==> Version Data " + data[Meta.VALUE]);
                            TWUpdateDownloader.setServerTWVersionData(data[Meta.VALUE]);
                        } else {
                            TWUpdateDownloader.setServerTWVersionData(null);
                        }
                        break;
                    case Meta.DAYS_TO_EXPIRE:
                        if (!safeMode) {
                            expiryDays = data[Meta.VALUE];
                        }
                        break;
                    case Meta.EXPIRY_DATE:
                        if (!safeMode) {
                            expiryDate = data[Meta.VALUE];
                        }
                        break;
                    //case Meta.NEWS_SOURCES:
                    case Meta.NEWS_SOURCES_NEW:
                        if (!safeMode) {
                            processNewsSources(data[Meta.VALUE]);
                            System.out.println("==> News Sources " + data[Meta.VALUE]);
                        }
                        break;
                    case Meta.CM_ID:
                        if (safeMode) {
                            safeModeCMID = data[Meta.VALUE];
                        } else {
                            Settings.setTWPath(data[Meta.VALUE]);
                        }
                        System.out.println("==> CM path " + data[Meta.VALUE]);
                        break;
                    case Meta.CHAT_SERVER_DETAILS:
                        if (!safeMode) {
                            String[] chatData = (data[Meta.VALUE]).split(":");
                            Settings.CHAT_SERVER_IP = chatData[0];
                            Settings.CHAT_SERVER_PORT = Integer.parseInt(chatData[1]);
                        } else {
                            Settings.CHAT_SERVER_IP = "";
                            Settings.CHAT_SERVER_PORT = 0;
                        }
                        break;
                    case Meta.BILLING_INSTITUTE:
                        try {
                            System.out.println("Billing Code " + data[Meta.VALUE]);
                            Settings.setBillingCode(data[Meta.VALUE].trim());
                            Settings.setConsoleAllowed(Settings.isDeveloperMode());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case Meta.INSTITUTION_CODE:
                        try {
                            System.out.println("Institution Code " + data[Meta.VALUE]);
                            Settings.setInstitutionCode(data[Meta.VALUE].trim());
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case Meta.SERVER_LANGUAGE_LIST:
                        try {
                            System.out.println("SERVER_LANGUAGE_LIST " + data[Meta.VALUE]);
                            Language.setServerLanguageList(data[Meta.VALUE]);
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                        break;
                    case Meta.IP_SYMBOL_DRIVEN_CM:
                        System.out.println("--> Symbol Driven(tag 114) IP " + data[Meta.VALUE]);
                        InternationalConnector.setIp(data[Meta.VALUE].trim());
                        break;
                    case Meta.IP_GLOBAL_CONTENT_PROVIDER_CM:
                        System.out.println("-->Global Content Provider(tag 116) IP " + data[Meta.VALUE]);
                        Settings.setGlobalContentProviderIP(data[Meta.VALUE].trim());
                        break;
                    case Meta.IP_REGIONAL_CONTENT_PROVIDER_CM:
                        System.out.println("-->Regional Content Provider(tag 118) IP " + data[Meta.VALUE]);
                        Settings.setRegionalContentProviderIP(data[Meta.VALUE].trim());
                        break;
                    case Meta.USER_PAYMENT_TYPE:
                        Settings.setUserPaymentType(Byte.parseByte(data[Meta.VALUE].trim()));
                        System.out.println("User Payment " + data[Meta.VALUE]);
                        break;
                    case Meta.EXPIRED_EXCHANGES:
                        expiredExchanges = data[Meta.VALUE].trim();
                        break;
                    case Meta.CONNECTION_TYPE:
                        Settings.setConnectionType(Integer.parseInt(data[Meta.VALUE].trim()));
                        break;
                    case Meta.COMPRESSESSOPN_SUPPORTED:
                        if (Integer.parseInt(data[Meta.VALUE].trim()) == Meta.YES) {
                            compressionSupported = true;
                        }
                        break;
                    case Meta.INACTIVE_EXCHANGE_LIST:
                        inactiveExchanges = data[Meta.VALUE].trim();
                        break;
                    case Meta.INACTIVE_EXCHANGE_MESSAGE:
                        try {
                            String[] strs = data[Meta.VALUE].trim().split("\\|");
                            final String heading = strs[0];
                            final String body = strs[2];
                            char[] errorType = strs[1].toUpperCase().toCharArray();
                            final int messageType;//= Integer.parseInt(strs[1]);
                            switch (errorType[0]) {
                                case 'E':
                                    messageType = JOptionPane.ERROR_MESSAGE;
                                    break;
                                case 'W':
                                    messageType = JOptionPane.WARNING_MESSAGE;
                                    break;
                                case 'I':
                                    messageType = JOptionPane.INFORMATION_MESSAGE;
                                    break;
                                default:
                                    messageType = JOptionPane.INFORMATION_MESSAGE;

                            }

                            new Thread("Inactive Message") {
                                public void run() {
                                    SharedMethods.showHTMLMessage(UnicodeUtils.getNativeString(heading), UnicodeUtils.getNativeString(body), messageType, JOptionPane.DEFAULT_OPTION);
                                }
                            }.start();
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }

                        break;
                    case Meta.IP_GLOBAL_NEWS_CONTENT_PROVIDER_CM:
                        Settings.setGlobalContentNewsIP(data[Meta.VALUE].trim());
                        break;

                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            data = null;
        }

        boolean versionOK = false;
        if (protocolVer == null) {
            versionOK = false;
        } else {
            String[] versions = protocolVer.split(",");
            for (String version : versions) {
                if (Meta.PROTOCOL_VERSION.equals(version)) {
                    versionOK = true;
                }
            }
        }

        if (!versionOK) {
            SharedMethods.showMessage(Language.getString("MSG_INVALID_PROTOCOL"), JOptionPane.ERROR_MESSAGE);
            authenticated = false;
            setMode(STOPPED);
            Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
            Client.getInstance().setDisConnectedStatus(true);
            Settings.setUserDetails(user, null);
        }

        if (authenticated) {
            /* Check Billing Code validity */
            if (isBillibgCodeLockingOK()) {
                System.out.println("==> Subscribed Exchanges " + exchanges);
                ExchangeStore.getSharedInstance().setExpiredExchanges(expiredExchanges);
                ExchangeStore.getSharedInstance().setInactiveExchanges(inactiveExchanges);
                ExchangeStore.getSharedInstance().addExchangeList(exchanges, exchangeParams, windowTypes);
//                ExchangeStore.getSharedInstance().addWindowTypes(windowTypes); // todo: moved to exchanges added
                ExchangeStore.getSharedInstance().addTradingWindowTypes(null, Constants.PATH_PRIMARY);
                ExchangeStore.getSharedInstance().addTradingWindowTypes(null, Constants.PATH_SECONDARY);
            } else {
                authenticated = false;
                Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
                Client.getInstance().setDisConnectedStatus(true);
                Settings.setUserDetails(user, null);
                SharedMethods.showMessage(Language.getString("MSG_INVALID_BILLING_CODE"), JOptionPane.ERROR_MESSAGE);
            }
        }


        if (safeMode) {
            return Meta.SAFE_MODE;
        }

        showExpiryMessages(expiryDate, expiryDays);
        showExpiredExchanges();
        if (blocking) {
            Settings.setUserDetails(user, null);
            SharedMethods.showHTMLMessage(Language.getString("ERROR"), message, JOptionPane.ERROR_MESSAGE, JOptionPane.DEFAULT_OPTION);
        } else {
//            if (!authenticated && TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct) && ((message != null) && (!message.equals("null")))) {
            if (!authenticated && TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                String errMsg = "";
                if ((message != null) && (!message.equals("null"))) {
                    errMsg = message;
                } else {
                    errMsg = Language.getString("SSO_PRICE_USER_INVALID");
                }
                SharedMethods.showBlockingMessage(errMsg, JOptionPane.ERROR_MESSAGE);

            } else if ((message != null) && (!message.equals("null"))) {
                Client.setDownloadStatus(message);
            }
        }

        if (authenticated) {
            saveUserData(frame);
        }


        if (authenticated) {
            return Meta.YES;
        } else {
            return Meta.NO;
        }
    }

    private void showExpiredExchanges() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                //To change body of implemented methods use File | Settings | File Templates.
                List<String> expiredExchenges = ExchangeStore.getSharedInstance().getExpiredExchanges();
                int exchangeCount = ExchangeStore.getSharedInstance().count();
                int expiredCount = ExchangeStore.getSharedInstance().expiredCount();

                if ((exchangeCount == expiredCount) && (exchangeCount != 0) && (expiredCount != 0)) {
                    SharedMethods.showNonBlockingMessage(Language.getString("MSG_ALL_EXCHANGES_EXPIRED"), JOptionPane.ERROR_MESSAGE);
                } else {
                    String template = Language.getString("MSG_EXCHANGES_EXPIRED");
                    StringBuilder buffer = new StringBuilder();
                    for (String expiredExchange : expiredExchenges) {
                        try {
                            String[] exg = expiredExchange.split(",");
                            for (int k = 0; k < exg.length; k++) {
                                if (!exg[k].equals("")) {
                                    Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exg[k]);
                                    buffer.append("<li>").append(exchange.getDescription()).append("</li>");
                                }
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }
                    String buff = buffer.toString();
                    if (buff.trim().length() > 0) {
                        template = template.replaceAll("\\[EXCHANGE\\]", buff);
                        SharedMethods.showHTMLMessage(null, template, JOptionPane.WARNING_MESSAGE, JOptionPane.DEFAULT_OPTION);
                    }
                }
            }
        });
    }

    private void saveUserData(String frame) {
        SmartProperties userData;
        try {
            userData = new SmartProperties(Meta.RS); // load the existing file
            userData.storeCompressed(new FileOutputStream(Settings.SYSTEM_PATH + "/udb.msf"), "");
        } catch (Exception e) {
            userData = new SmartProperties(Meta.RS); // create new file
        }

        try {
            if (Settings.isSingleSignOnMode()) {
                userData.put(ssoCode + Settings.getBillingCode(), frame);
            } else {
                userData.put(user, frame);
            }
            userData.storeCompressed(new FileOutputStream(Settings.SYSTEM_PATH + "/udb.msf"), "");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void processNewsSources(String data) {
        try {
            NewsProvidersStore.getSharedInstance().addUserSubscribedProviders(data);
            /*String[] providerArray = data.split("\\|");
            NewsProvidersStore.getSharedInstance().clear();
            if (providerArray.length > 0) {
                for (int i = 0; i < providerArray.length; i++) {
                    String[] sources = (providerArray[i]).split(",");
                    try {
                        NewsProvidersStore.getSharedInstance().addServerSupportedProvider(sources[0].toUpperCase(),new ServerSupportedNewsProvider(sources[0].toUpperCase(),sources[1],(byte)Integer.parseInt(sources[2])));
                    } catch (Exception e) {
                        NewsProvidersStore.getSharedInstance().addServerSupportedProvider(sources[0].toUpperCase(),new ServerSupportedNewsProvider(sources[0].toUpperCase(),Settings.getActiveIP(),(byte)Integer.parseInt("1")));
                }
            }
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sleepThread(long time) {
//        System.out.println("sleep Thread=="+time);
        try {
            sleep(time);
        } catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void closeSocket() {
        try {
            if (socket != null)
                socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static final boolean isStopped() {
        return connectionStatus == STOPPED;
    }

    public final int getMode() {
        synchronized (STATUS_LOCK) {
            return connectionStatus;
        }
    }

    public final void setMode(int status) {
        System.out.println("Status ------------- " + status);
        synchronized (STATUS_LOCK) {
            connectionStatus = status;
        }
    }

    public void setIdle() {
        synchronized (STATUS_LOCK) {
            if (connectionStatus == STOPPED) {
                connectionStatus = IDLE;
            }
        }
    }

    private boolean isBillibgCodeLockingOK() {
        String[] billingCodes = TWControl.getLokedBillingCodes();
        if (billingCodes == null) return true; // no locking enforced
        for (int i = 0; i < billingCodes.length; i++) {
            try {
                if (billingCodes[i].trim().equals(Settings.getBillingCode().trim())) {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private class PrimarySniffer extends Thread {

        public PrimarySniffer() {
            super("PrimarySniffer");
            start();
        }

        public void run() {
            String primaryIP;
            System.out.println("===================Primary IP Sniffer Active");
            while (true) {
                if (PRIMARY_IP_FORCED && (Settings.isConnected()) && (currentIP != null) && (currentIP.getIpClass() == IP.SECONDARAY)) {
                    try {
                        primaryIP = IPSettings.getIP(IPSettings.getIpIndex() - 1).getIP();
                        Socket primarySocket = TWSocket.createSocket(primaryIP, Settings.SERVER_PORT);
                        String sAuthString =
                                Meta.USER_NAME + Meta.DS + SharedMethods.encrypt(user) + Meta.FS +
                                        Meta.USER_PASSWORD + Meta.DS + SharedMethods.encrypt(pass) + Meta.FS +
                                        Meta.USER_VERSION + Meta.DS + Settings.TW_VERSION + Meta.FS +
                                        Meta.USER_SESSION + Meta.DS + Settings.getSessionID() + Meta.FS +
                                        Meta.USER_INFO + Meta.DS + "Primary IP Check Only" + Meta.FS +
                                        Meta.USER_TYPE + Meta.DS + Meta.USER_TYPE_NEW_TW + "\n";
                        OutputStream primaryOut = primarySocket.getOutputStream();
                        InputStream primaryIn = primarySocket.getInputStream();
                        primaryOut.write(sAuthString.getBytes());
                        String authResult = readLine(primaryIn);
                        primarySocket.close();
                        primarySocket = null;
                        primaryIn = null;
                        primaryOut = null;
                        boolean sucessfull = isConnectedToServerSucessfully(authResult);
                        if (sucessfull) {
                            currentIP = null;
                            client.disconnectFromServer();
                        }
                    } catch (Exception e) {
                        // ip not found
                        e.printStackTrace();
                    }
                }
                try {
                    Thread.sleep(300000);
                } catch (Exception e) {
                }
            }

        }
    }

    private boolean isConnectedToServerSucessfully(String sFrame) {

        String[] fields = sFrame.split(Meta.FS);
        for (int i = 0; i < fields.length; i++) {
            String[] data = fields[i].split(Meta.DS);
            switch (Integer.parseInt(data[Meta.TAG])) {
                case Meta.AUTH_RESULT:
                    return true;
            }
        }
        return false;

        /*int iResult = 0;
        String sTag = null;
        String sValue = null;

        /* Check if the message type is authentication *
        DataDisintegrator oData = new DataDisintegrator();
        oData.setSeperator(Meta.DS);
        oData.setData(sFrame);
        sTag = oData.getTag();
        sValue = oData.getData();

        if (sTag.equals("" + Meta.AUTH_RESULT)) {
            try {
                iResult = (new Integer(sValue)).intValue();
            } catch (Exception e) {
                return false;
            }
        } else if (sTag.equals("" + Meta.SERVER_BUSY)) {
            return false;
        } else {
            return false;
        }

        /* check the reply type *
        if (iResult == Meta.AUTH_MSG_SUCCESSFUL) {
            return true;
        } else if (iResult == Meta.AUTH_MSG_INVALID_USER) {
            return true;
        } else if (iResult == Meta.AUTH_MSG_INVALID_PW) {
            return true;
        } else if (iResult == Meta.AUTH_MSG_BUSY) {
            return false;
        } else if (iResult == Meta.AUTH_MSG_EXPIRED) {
            return true;
        } else if (iResult == Meta.AUTH_MSG_MULTIPLE) {
            return true;
        } else if (iResult == Meta.AUTH_MSG_KILL) {
            return true;
        } else if (iResult == Meta.AUTH_MSG_NOT_ACTIVATED) {
            return true;
        } else if (sValue.equals("" + Meta.AUTH_MSG_INVALID_TW_VERSION)) {
            return true;
        } else {
            return true;
        }*/
    }

    public double test() {
        int price = 0;
        int quantity = 0;

        if ((price * quantity) <= 20000) {
            return 75;
        } else if ((price * quantity) <= 400000) {
            return ((0.00225 * (price * quantity)) + 30.0);
        } else {
            return ((0.00275 * (price * quantity)) + 10.0);
        }
    }

    public static FrameAnalyser getFrameAnalyser() {
        return frameAnalyser;
    }

}
