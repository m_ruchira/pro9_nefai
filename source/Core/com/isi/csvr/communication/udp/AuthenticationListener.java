package com.isi.csvr.communication.udp;

import com.isi.csvr.Client;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.trading.security.TWSecurityManager;
import com.isi.csvr.trading.shared.TradingShared;

import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;


/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 11, 2004
 * Time: 11:17:16 AM
 */
public class AuthenticationListener implements Runnable {

    public AuthenticationListener() {
    }

    public void run() {

        try {
            // Create the socket and bind it to port 'port'.
            MulticastSocket s = new MulticastSocket(new InetSocketAddress(Settings.SATELLITE_CARD_IP, Settings.SATELLITE_AUTH_PORT));
            s.setReceiveBufferSize(100000);

            // join the multicast group
            s.joinGroup(InetAddress.getByName(Settings.SATELLITE_DATA_IP));
            // Now the socket is set up and we are ready to receive packets

            // Create a DatagramPacket and do a receive
            byte[] buf = new byte[1024];
            DatagramPacket pack = new DatagramPacket(buf, buf.length);
            System.out.println("Authentication listener Ready...");

            while (true) {
                try {
                    if (SatelliteSecurityManager.USB_OK) {
                        s.receive(pack);
                        if (pack == null) continue;

                        if (!Authenticator.USER_VALID) {
                            validateSatUser(pack.getData(), pack.getLength());
                        }
                    } else {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void validateSatUser(byte[] data, int len) {
        try {
            String header = TWSecurityManager.decryptSubscription(data, TradingShared.INITAL_USB_PW);
            if ((header != null) && (header.startsWith("MUBASHER"))) {
                SharedMethods.printLine("User Validated");
                Client.getInstance().setConnectedStatus();
                Client.getInstance().setConnectionStatus(Language.getString("STATUS_LIVE"));
                String deskey = header.substring(8).trim();
                TWSecurityManager.initializePriceDataDecryptor(deskey);
                String userInfo = new String(TWSecurityManager.decryptData(data,
                        TWSecurityManager.PUBLIC_KEY_ENCRYPTED_LENGTH, len -
                        TWSecurityManager.PUBLIC_KEY_ENCRYPTED_LENGTH));
                SharedMethods.printLine(userInfo);
                Authenticator.getSharedInstance().createUser(userInfo);
                UDPConnector.getSharedInstance().initializeApp();

                if (Authenticator.USER_VALID) {
                    UDPConnector.getSharedInstance().activateDataListeners();
                }
                deskey = null;
            } else {
                System.err.println("Unknow User Data");
            }
        } catch (Exception e) {
            System.err.println("Unknow User Data");
        }
    }
}