package com.isi.csvr.communication.udp;

import com.isi.csvr.Client;
import com.isi.csvr.FrameAnalyser;
import com.isi.csvr.announcement.AnnouncementStore;
import com.isi.csvr.communication.ConnectorInterface;
import com.isi.csvr.customindex.CustomIndexStore;
import com.isi.csvr.datastore.CurrencyStore;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.mutualfund.MutualFundStore;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.trade.DetailedTradeStore;
import com.isi.csvr.tradebacklog.TradeBacklogStore;
import com.isi.csvr.tradebacklog.TradeDownloaderUI;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 11, 2004
 * Time: 11:36:36 AM
 * To change this template use File | Settings | File Templates.
 */
public class UDPConnector implements ConnectorInterface {
    private FrameAnalyser frameAnalyser;
    private List<String> receiveBuffer;
    private Hashtable<String, DataListener> dataListeners;
    private Hashtable<String, FileListener> fileListeners;

    public static UDPConnector self = null;

    public static synchronized UDPConnector getSharedInstance() {
        return self;
    }

    public UDPConnector() {
        receiveBuffer = Collections.synchronizedList(new LinkedList<String>());
        dataListeners = new Hashtable<String, DataListener>();
        fileListeners = new Hashtable<String, FileListener>();
        self = this;

        SatelliteSecurityManager securityManager = new SatelliteSecurityManager();
        securityManager.activate();

        ControlListener controlListener = new ControlListener();
        Thread controlThread = new Thread(controlListener, "UDPConnector-ControlListener");
        controlThread.start();

        AuthenticationListener authenticationListener = new AuthenticationListener();
        Thread authThread = new Thread(authenticationListener, "UDPConnector-AuthenticationListener");
        authThread.start();
    }

    public void activateDataListeners() {
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault() && (exchange.getSatelliteDataPort() > 0)) {
                ExchangeStore.getSharedInstance().loadSubMarkets(exchange);
                ExchangeStore.getSharedInstance().loadBrokers(exchange);
                DataListener dataListener = new DataListener(exchange.getSatelliteDataPort(), exchange.getSymbol());
                dataListeners.put(exchange.getSymbol(), dataListener);
                Thread dataThread = new Thread(dataListener, "UDPConnector-DataListener-" + exchange.getSymbol());
                dataThread.start();
            }
            exchange = null;
        }
        exchanges = null;

        Exchange exchange =  ExchangeStore.getSharedInstance().getApplicationExchange();
        DataListener dataListener = new DataListener(exchange.getSatelliteDataPort(), exchange.getSymbol());
        dataListeners.put(exchange.getSymbol(), dataListener);
        Thread dataThread = new Thread(dataListener, "UDPConnector-DataListener-" + exchange.getSymbol());
        dataThread.start();
    }

    private void deavtivateDataListeners() {
        Enumeration<DataListener> datalistenerObjetcs = dataListeners.elements();
        while (datalistenerObjetcs.hasMoreElements()) {
            DataListener dataListener = datalistenerObjetcs.nextElement();
            dataListener.setInavtive();
        }
        datalistenerObjetcs = null;
        dataListeners.clear();
    }

    public void activateFileListener(String exchange, int port) {
        if (!fileListeners.containsKey(exchange)) {
            FileListener fileListener = new FileListener(port, exchange);
            fileListeners.put(exchange, fileListener);
            Thread historyThread = new Thread(fileListener, "UDPConnector-FileListener" + exchange);
            historyThread.start();
        }
    }

    private void deavtivateFileListeners() {
        Enumeration<FileListener> fileistenerObjetcs = fileListeners.elements();
        while (fileistenerObjetcs.hasMoreElements()) {
            FileListener fileListener = fileistenerObjetcs.nextElement();
            fileListener.setInavtive();
        }
        fileistenerObjetcs = null;
        fileListeners.clear();
    }

    public void resetConnection() {
        ExchangeStore.setStopListeningForStockMaster();
        deavtivateDataListeners();
        deavtivateFileListeners();
        Authenticator.USER_VALID = false;
        Client.getInstance().setDisConnectedStatus(false);
        Client.getInstance().setConnectionStatus(Language.getString("NOT_CONNECTED"));
    }

    public void initializeApp() {
        DataStore.getSharedInstance().init(true);
        CurrencyStore.getSharedInstance().clearCurrencies();
        Settings.setConnected();
        Settings.setHistoryCondition((short) -1); // make invalid
        //todo ------
//        if(!TradeStore.isInitCalled()){
//            TradeStore.init();
//        }
        //todo ------
        TradeBacklogStore.getSharedInstance().clear();
        TradeDownloaderUI.getSharedInstance().setProcessCompleted("", true); // complete any disconnected downloads

        Client.getInstance().getMenus().checkMenuAuthentication();
        frameAnalyser = new FrameAnalyser(receiveBuffer);
        //frameAnalyser = UDPConnector.getSharedInstance().getFrameAnalyser();
        Client.getInstance().initializeFrameAnalyser(frameAnalyser);
        frameAnalyser.setClient(Client.getInstance());
        frameAnalyser.start();
        OHLCStore.getInstance().clear();
        AnnouncementStore.getSharedInstance().clearAll();

        /* change the status to connected */
        Settings.setAuthenticated();
        MutualFundStore.getInstance().clear();
        DetailedTradeStore.getSharedInstance().init();
        populateTimeAndSalesList();
        CustomIndexStore.getSharedInstance().initializeStore();
        System.out.println("App initialized");
    }

    private void populateTimeAndSalesList(){
         Enumeration exchangeObjects = ExchangeStore.getSharedInstance().getExchanges();
        while (exchangeObjects.hasMoreElements()) {
            Exchange exchange = (Exchange) exchangeObjects.nextElement();
            TradeBacklogStore.getSharedInstance().updateListFromFiles(exchange.getSymbol());
            exchange = null;
        }
        exchangeObjects = null;
    }

    public List<String> getReceiveBuffer() {
        return receiveBuffer;
    }

    public FrameAnalyser getFrameAnalyser() {
        return frameAnalyser;
    }

    public void closeSocket() {
    }

    public int getMode() {
        return 0;
    }

    public void setMode(int status) {
    }

    public void setIdle() {
    }

    public void start() {

    }

    public void update() {
    }
}
