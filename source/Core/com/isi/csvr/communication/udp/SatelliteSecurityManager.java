package com.isi.csvr.communication.udp;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.trading.security.TWSecurityManager;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jul 6, 2005
 * Time: 2:52:00 PM
 */
public class SatelliteSecurityManager implements Runnable {

    public static boolean USB_OK;

    public void activate() {
        Thread thread = new Thread(this, "SatelliteSecurityManager");
        thread.start();
    }

    public void run() {
        while (true) {
            try {
                if (TWSecurityManager.isUSBConected()) {
                    USB_OK = true;
                } else {
                    USB_OK = false;
                    SharedMethods.showMessage(Language.getString("NO_USB_DEVICE"), JOptionPane.ERROR_MESSAGE);
                }
                Thread.sleep(60000);
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
}
