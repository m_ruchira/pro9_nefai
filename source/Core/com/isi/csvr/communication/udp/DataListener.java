package com.isi.csvr.communication.udp;

import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.trading.security.TWSecurityManager;

import javax.crypto.BadPaddingException;
import javax.crypto.IllegalBlockSizeException;
import java.net.DatagramPacket;
import java.net.InetAddress;
import java.net.InetSocketAddress;
import java.net.MulticastSocket;
import java.util.LinkedList;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 10, 2004
 * Time: 2:13:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class DataListener implements Runnable {

    private int port;
    private boolean active;
    private String exchange;
    private MulticastSocket socket;
    public static int frameCount;

    public DataListener(int port, String exchange) {
        this.port = port;
        this.exchange = exchange;
    }

    public void run() {

        try {
            final byte CR = (byte) '\n';
            boolean firstPacket = false;
            active = true;
            StringBuilder frame = new StringBuilder("");
            List<String> queue = UDPConnector.getSharedInstance().getReceiveBuffer();

            // Create the socket and bind it to port 'port'.
            socket = new MulticastSocket(new InetSocketAddress(Settings.SATELLITE_CARD_IP, port));

            // join the multicast group
            socket.joinGroup(InetAddress.getByName(Settings.SATELLITE_DATA_IP));
            // Now the socket is set up and we are ready to receive packets

            // Create a DatagramPacket and do a receive
            byte[] packetbuf = new byte[1024];
            DatagramPacket pack = new DatagramPacket(packetbuf, packetbuf.length);
            byte[] buf;
            System.out.println(" Data listener Ready " + exchange + " " + port);

            while (active) {
                try {
                    if (SatelliteSecurityManager.USB_OK // user might remove the usb while tw in operation
                            && Authenticator.USER_VALID) {

                        socket.receive(pack);
                        if (pack == null) continue;
                        firstPacket = true;

                        buf = TWSecurityManager.decryptData(packetbuf, 0, pack.getLength());

                        // break frames in packet data
                        for (int i = 0; i < buf.length; i++) {
                            if (buf[i] != CR) {
                                if (buf[i] == Meta.SAT_EOF){
                                    break;
                                }else{
                                    frame.append((char) buf[i]);
                                    continue;
                                }
                            }
                            if ((frame != null) && (frame.length() > 0)) {
                                if (firstPacket) {
                                    queue.add(frame.toString().substring(1));
                                    frameCount ++;
                                    firstPacket = false;
                                } else {
                                    queue.add(frame.toString());
                                    frameCount ++;
                                }
                            }
                            frame = null;
                            frame = new StringBuilder("");
                        }
                        buf = null;
//                        pack = null;
                    } else {
                        try {
                            Thread.sleep(1000);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                } catch (BadPaddingException e) {
                    e.printStackTrace();
                    UDPConnector.getSharedInstance().resetConnection();
                } catch (IllegalBlockSizeException e){
                    e.printStackTrace();
                    UDPConnector.getSharedInstance().resetConnection();
                } catch (Exception e) {
                    e.printStackTrace();
                    try {
                        Thread.sleep(1000);
                    } catch (InterruptedException e2) {}
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setInavtive() {
        try {
            active = false;
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}


/*private void dispatchAdjustedFrames(String exCode, String sFrame) {
        int     bytesWritten        = 0;
        int     remainingDataSize   = 0;
        byte[]  bSplittedFrame      = null;
        String  exchange            = getExchangeForTheFrame(sFrame);

        if (exchange == null) {
            if (exCode != null)
                exchange = exCode;
            else {
                LogManager.addToServerLog("<SatRealTimeDataPusher> Ignored Frame " + sFrame);
                return;
            }
        }

        byte[] bArray = (SatelliteConstants.SAT_FRAME_FIRST_FIELD + sFrame + Meta.EOL).getBytes();

        while (bytesWritten < bArray.length) {
            remainingDataSize = bArray.length - bytesWritten;
            if (remainingDataSize > SatelliteConstants.DATA_PACKET_SIZE) {
                bSplittedFrame = new byte[SatelliteConstants.DATA_PACKET_SIZE];
                System.arraycopy(bArray, bytesWritten, bSplittedFrame, 0, SatelliteConstants.DATA_PACKET_SIZE-1);
                bytesWritten += SatelliteConstants.DATA_PACKET_SIZE-1;
            } else {
                bSplittedFrame = new byte[remainingDataSize];
                System.arraycopy(bArray, bytesWritten, bSplittedFrame, 0, remainingDataSize);
                bytesWritten += remainingDataSize;
            }

            sendEncryptedFrame(exchange, bSplittedFrame);

            bSplittedFrame = null;
        }
    }
}*/
