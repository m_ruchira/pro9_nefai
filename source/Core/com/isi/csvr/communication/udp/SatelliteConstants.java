package com.isi.csvr.communication.udp;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jul 15, 2005
 * Time: 8:56:15 AM
 */
public class SatelliteConstants {
    public static final byte FRAME_BODY = 'D';
    public static final byte FRAME_HEADER = 'H';

    // BackLog Path Header Frame Contents
    public static final int FILE_POSITION_FRAME_ENTRYPTED = 0;    // 1 byte : H - Header, D- Data
    public static final int FILE_POSITION_FRAME_TYPE = 1;    // 1 byte : H - Header, D- Data
    public static final int FILE_POSITION_FILE_TYPE = 2;    // 1 byte : See the listing below
    public static final int FILE_POSITION_FILE_NAME = 3;    // 20 bytes
    public static final int FILE_POSITION_FILE_SIZE = 23;   // 8 bytes
    public static final int FILE_POSITION_FILE_ID_HEADER = 31;   // 8 bytes - Time Stamped value the file created
    public static final int FILE_POSITION_VERSION_HEADER = 39;   // 4 bytes - Version of teh property file
    public static final int FILE_POSITION_MD5_HEADER = 47;   // 32 bytes - MD5 value for the file

    // BackLog Path Data Frame Contents
    public static final int FILE_POSITION_FILE_ID_BODY = 2;    // 8 bytes - Time Stamped value the file created
    public static final int FILE_POSITION_SEQUENCE = 10;    // 8 bytes - Start byte position of the current block of data
    public static final int FILE_POSITION_DATA_LENGTH = 18;   // 4 bytes - Length of the packet data
    public static final int FILE_POSITION_DATA = 22;   // Data Portion

    // Lengths of each portion
    public static final int LENGTH_FILE_NAME = 20;
    public static final int LENGTH_FILE_ID = 8;
    public static final int LENGTH_FILE_SIZE = 8;
    public static final int LENGTH_SEQUENCE = 8;
    public static final int LENGTH_MD5 = 32;
    public static final int LENGTH_DATA = 4;
    public static final int LENGTH_VERSION = 4;

    // File Types
    public static final int FILE_TYPE_ARCHIVE = 1;    // Will be ignored by the SatDM
    public static final int FILE_TYPE_HISTORY = 2;
    public static final int FILE_TYPE_RECENT_HISTORY = 3;    // Will be ignored by the SatDM
    public static final int FILE_TYPE_OHLC_HISTORY = 4;
    public static final int FILE_TYPE_STOCK_MASTER = 5;
    public static final int FILE_TYPE_PROPERTY = 6;
    public static final int FILE_TYPE_AUTO_UPDATE = 7;
    public static final int FILE_TYPE_COMPLETE_HISTORY = 8;
    public static final int FILE_TYPE_ANNOUNCEMENTS = 9;
    public static final int FILE_TYPE_TRADE_SUMMARY = 10;
    public static final int FILE_TYPE_DAILY_TRADE = 11;

    public static final byte ENCRYPTION_NONE = 0;
    public static final byte ENCRYPTION_ON = 1;

    public static final byte CONTROL_TYPE_FORCE_DISCONNECTION = 0;
    public static final byte CONTROL_TYPE_USER_MESSAGES = 1;

// Control Path Frame Contents
    public static int CTRL_POSITION_USER_ID = 0;
    public static int CTRL_POSITION_ENCRYPTION_TYPE = 4;
    public static int CTRL_POSITION_MESG_TYPE = 5;
    public static int CTRL_POSITION_MESG_LENGTH = 6;
    public static int CTRL_POSITION_MESG_BODY = 10;

// Lengths of each portion
    public static int CTRL_LENGTH_USER_ID = 4;
    public static int CTRL_LENGTH_ENCRYPTION_TYPE = 1;
    public static int CTRL_LENGTH_MESG_TYPE = 1;
    public static int CTRL_LENGTH_MESG_LENGTH = 4;


}
