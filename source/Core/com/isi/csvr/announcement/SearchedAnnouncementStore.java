package com.isi.csvr.announcement;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.announcementnew.AnnouncementSearchWindow;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.shared.*;

/**
 * Created by IntelliJ IDEA. User: Uditha Nagahawatta Date: Aug 14, 2003 Time: 11:53:21 AM To change this template use
 * Options | File Templates.
 */
public class SearchedAnnouncementStore {

    private static SearchedAnnouncementStore self = null;
    private DynamicArray store;
    private String selectedLanguage = null;
    private String lastSearchedLanguage;
    private DynamicArray searchIndex;
    private byte currentPage = 0;
    private String searchString;
    private String exchange;
    private String langID;
    private TWButton btnNext;
    private TWButton btnPrev;
    //    private TWButton btnReload;
    private TWButton btnSearch;
//    private JRadioButton rbEnglish;
    //    private JRadioButton rbArabic;
    private int lastSearchedID;
    private boolean haveMoreRecords;
    private boolean searchInProgess;

    private SearchedAnnouncementStore() {
        store = new DynamicArray();
        searchIndex = new DynamicArray();
        btnPrev = new TWButton();
        btnNext = new TWButton();
        btnSearch = new TWButton();
    }

    public static SearchedAnnouncementStore getSharedInstance() {
        if (self == null) {
            self = new SearchedAnnouncementStore();
        }
        return self;
    }

    public void setData(String data) {
        //COUNT  FD HAVE_MORE FD
        // [ SEQUENCE FD DATE_TIME FD HEADLINE ] \n

        int start = 0;
        int end = 0;

        try {
            String[] records = data.split(Meta.FD);

            // header
            short recordCount = Short.parseShort(records[0]);
            haveMoreRecords = records[1].equals("1");
            setSearchInProgress(false);
            Client.getInstance().getDesktop().repaint();
            // data

            Announcement announcement;
            int record = 0;
            int i = 2;
            if (recordCount > 0) {

                while (record < recordCount) {
                    try {
                        String[] fields = records[i].split(Meta.ID);
                        announcement = new Announcement(fields[0]);
                        announcement.setSymbol(fields[1]);
                        announcement.setAnnouncementNo(fields[3]);
                        announcement.setAnnouncementTime(Long.parseLong(fields[4]));
                        announcement.setHeadLine(UnicodeUtils.getNativeString(fields[5]));
                        try {
                            announcement.setLanguage(fields[6]);
                        } catch (Exception e) {
                        }
                        store.add(announcement);
                        if (record == 0) {
                            start = Integer.parseInt(announcement.getAnnouncementNo());
                        }
                        end = Integer.parseInt(announcement.getAnnouncementNo());
                        announcement = null;
                        record++;
                        i++;
                        fields = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                        throw new RuntimeException("Error reading Announcements");
                    }
                }
            } else {
                new ShowMessage(Language.getString("NO_RESULTS_FOUND"), "I");
            }
            records = null;
            announcement = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        AnnouncementSearchWindow.getSharedInstance().setBusyMode(false);
        AnnouncementSearchWindow.getSharedInstance().resetSearchBtn();
        updateSearchIndex(start, end);
        enableButtons();
       // Client.getInstance().setAnnouncementSearchTitle(null, null);
    }

    public void setBody(String body) {
        // CA_BODY + DS + ID + FD + Body
        try {
            String[] bodyData = body.split(Meta.FD);

            Announcement announcement = getSearchedAnnouncement(bodyData[0]);
            if (announcement != null) {
                announcement.setMessage(UnicodeUtils.getNativeStringFromCompressed(bodyData[1]));
                try {
                    announcement.setHeadLine(UnicodeUtils.getNativeStringFromCompressed(bodyData[2]));
                } catch (Exception e) {
                }
                try {
                    announcement.setLanguage(UnicodeUtils.getNativeStringFromCompressed(bodyData[3]));
                } catch (Exception e) {
                }
            }
            announcement = AnnouncementStore.getSharedInstance().getAnnouncement(bodyData[0]);
            if (announcement != null) {
                announcement.setMessage(UnicodeUtils.getNativeStringFromCompressed(bodyData[1]));
                try {
                    announcement.setHeadLine(UnicodeUtils.getNativeStringFromCompressed(bodyData[2]));
                } catch (Exception e) {
                }
                try {
                    announcement.setLanguage(UnicodeUtils.getNativeStringFromCompressed(bodyData[3]));
                } catch (Exception e) {
                }
            }
            bodyData = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Announcement getAnnouncement(int index) {
        return (Announcement) store.get(index);
    }

    public Announcement getSearchedAnnouncement(String id) {
        for (int i = 0; i < store.size(); i++) {
            Announcement announcement = (Announcement) store.get(i);
            if (announcement.getAnnouncementNo().equals(id))
                return announcement;
            announcement = null;
        }
        return null;
    }

    public void showAnnouncement(String id) {
        Announcement announcement = getSearchedAnnouncement(id);
        String body = AnnouncementStore.getSharedInstance().generateTempFile(announcement);
        String company = UnicodeUtils.getNativeString(DataStore.getSharedInstance().getShortDescription(announcement.getKey()));
        Client.getInstance().getBrowserFrame().setContentType("text/html");
        Client.getInstance().getBrowserFrame().setTitle(Language.getString("ANNOUNCEMENT") + " " + company);
        Client.getInstance().getBrowserFrame().setText(body);
        Client.getInstance().getBrowserFrame().show(Constants.DOC_TYPE_ANNOUNCEMENT);
        Client.getInstance().getBrowserFrame().bringToFont();
        announcement = null;
    }

    public DynamicArray getStore() {
        return store;
    }

    public int size() {
        return store.size();
    }

    public void clear() {
        store.clear();
    }

    public boolean isSearchInProgress() {
        return searchInProgess;
    }

    public void setSearchInProgress(boolean searchInProgress) {
        this.searchInProgess = searchInProgress;
    }

    public static SearchedAnnouncementStore getSelf() {
        return self;
    }

    public static void setSelf(SearchedAnnouncementStore self) {
        SearchedAnnouncementStore.self = self;
    }

    public String getSelectedLanguage() {
        return selectedLanguage;
    }

    public void setSelectedLanguage(String selectedLanguage) {
        this.selectedLanguage = selectedLanguage;
    }

    public String getLastSearchedLanguage() {
        return lastSearchedLanguage;
    }

    public void setLastSearchedLanguage(String lastSearchedLanguage) {
        this.lastSearchedLanguage = lastSearchedLanguage;
    }

    private void updateSearchIndex(int start, int end) {
        if (searchIndex.size() - 1 >= currentPage) {
            return; // index is already there
        }

        SearchIndexRecord searchIndexRecord = new SearchIndexRecord();
        searchIndexRecord.start = start;
        searchIndexRecord.end = end;
        searchIndex.add(searchIndexRecord);
    }

    private int getNextID() {
        SearchIndexRecord searchIndexRecord = (SearchIndexRecord) searchIndex.get(currentPage);
        return searchIndexRecord.end;
    }

    private int getPreviousID() {
        SearchIndexRecord searchIndexRecord = (SearchIndexRecord) searchIndex.get(currentPage - 1);
        return searchIndexRecord.start + 1;
    }

    public void doPreviuosSearch() {
        if ((langID!=null)&&(!langID.equals(""))) {
            doSearch(searchString, langID, getPreviousID(), exchange);
        } else {
            doSearch(searchString, getSelectedLanguage(), getPreviousID(), exchange);
        }
//        doSearch(searchString, getSelectedLanguage(), getPreviousID(), exchange);
        currentPage--;
    }

    public void doNextSearch() {
        if ((langID!=null)&&(!langID.equals(""))) {
            doSearch(searchString, langID, getNextID(), exchange);
        } else {
            doSearch(searchString, getSelectedLanguage(), getNextID(), exchange);
        }
//        doSearch(searchString, getSelectedLanguage(), getNextID(), exchange);
        currentPage++;
    }

    public void doReSearch() {
        doSearch(searchString, getSelectedLanguage(), lastSearchedID, exchange);
    }

    public void doSearch(String searchString, String language, int id, String exchange) {
        disableButtons();
        clear();
        this.searchString = searchString;
        this.exchange = exchange;
        this.lastSearchedID = id;
        this.langID=language;
//        if(AnnouncementSearchPanel.getSharedInstance().isConnected()){
        if (Settings.isConnected()) {
            SearchedAnnouncementStore.getSharedInstance().setSearchInProgress(true);
            //Client.getInstance().setAnnouncementSearchTitle(null, Language.getString("SEARCHING"));
//            SendQFactory.addData(Constants.PATH_PRIMARY, searchString + Meta.FD + id + Meta.FD + language);
            SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, searchString + Meta.FD + id + Meta.FD + language + Meta.EOL, exchange);
            setLastSearchedLanguage(language);
            System.out.println(searchString + Meta.FD + id + Meta.FD + language);
        }
    }

    public void initSearch() {
        searchIndex.clear();
        currentPage = 0;
    }

    public void setButtonReferences(TWButton btnNext, TWButton btnPrev /*TWButton btnReload*/,
                                    TWButton btnSearch /*JRadioButton rbEnglish, JRadioButton rbArabic*/) {
        this.btnPrev = btnPrev;
        this.btnNext = btnNext;
        this.btnSearch = btnSearch;


    }

    public void disableButtons() {
        try {
            AnnouncementSearchWindow.getSharedInstance().disableButtons();
        } catch (Exception e) {
            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void enableButtons() {
        if (currentPage == 0) {
            if (haveMoreRecords) {
                AnnouncementSearchWindow.getSharedInstance().enableNextButtons();
            }else{
                AnnouncementSearchWindow.getSharedInstance().disaableNextButtons();
            }
        } else {
            AnnouncementSearchWindow.getSharedInstance().enablePrevButtons();
        }

//        if (haveMoreRecords) {
//            AnnouncementSearchWindow.getSharedInstance().enableNextButtons();
//        } else {
//            AnnouncementSearchWindow.getSharedInstance().enablePrevButtons();
//        }
//        btnReload.setEnabled(true);
        btnSearch.setEnabled(true);
//        rbArabic.setEnabled(true);
//        rbEnglish.setEnabled(true);
    }

    class SearchIndexRecord {
        int start;
        int end;
    }

    public boolean enablePrev() {
        if (currentPage == 0) {
            return false;
        } else {
            return true;
        }
    }

    public boolean enableNext() {
        if (haveMoreRecords) {
            return true;
        } else {
            return false;
        }
    }
}
