package com.isi.csvr;

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Meta;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 8, 2005
 * Time: 10:54:34 AM
 */
public class ExchangeSelectorDialog extends JDialog implements ActionListener {

    private String exchange;
    final static int selectionType = 0;//market Time & Sales

    public ExchangeSelectorDialog(String title) throws HeadlessException {
        super(Client.getInstance().getFrame(), true);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
//        panel.setLayout(new GridLayout(ExchangeStore.getSharedInstance().getd));
        panel.setBorder(BorderFactory.createCompoundBorder(
                GUISettings.getTWFrameBorder(),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        setUndecorated(true);
        setLayout(new BorderLayout());

        JLabel label = new JLabel(title);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(label);

        ArrayList<Exchange> list = new ArrayList<Exchange>();
        SharedMethods.populateExchanges(list, true);
        /*Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            //if (!exchange.isDefault()) {
                list.add(exchange);
            //}
            exchange = null;
        }

        Collections.sort(list);*/

        for (Exchange exchange : list) {
            if (exchange.isDefault()) {
//                if(exchange.isValidIinformationType(Meta.IT_MarketTimeAndSales)){
                JButton button = new JButton(ExchangeStore.getSharedInstance().getDescription(exchange.getSymbol()));
                button.setFont(button.getFont().deriveFont(Font.PLAIN));
                button.setBorderPainted(false);
                button.setContentAreaFilled(false);
                button.setAlignmentX(Component.CENTER_ALIGNMENT);
                button.setActionCommand(exchange.getSymbol());
                button.addActionListener(this);
                button.setCursor(new Cursor(Cursor.HAND_CURSOR));
                panel.add(button);
//                }
            }
            exchange = null;
        }
//        exchanges = null;
        list = null;
        getContentPane().add(panel, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(Client.getInstance().getFrame());
    }

    public ExchangeSelectorDialog(String title, int type) throws HeadlessException {
        super(Client.getInstance().getFrame(), true);
        JPanel panel = new JPanel();
        panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
//        panel.setLayout(new GridLayout(ExchangeStore.getSharedInstance().getd));
        panel.setBorder(BorderFactory.createCompoundBorder(
                GUISettings.getTWFrameBorder(),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        setUndecorated(true);
        setLayout(new BorderLayout());

        JLabel label = new JLabel(title);
        label.setHorizontalAlignment(SwingConstants.CENTER);
        label.setAlignmentX(Component.CENTER_ALIGNMENT);
        panel.add(label);

        ArrayList<Exchange> list = new ArrayList<Exchange>();
        SharedMethods.populateExchanges(list, true);


        for (Exchange exchange : list) {
            if (exchange.isDefault()) {
                if (type == selectionType) {
                    if (exchange.isValidIinformationType(Meta.IT_MarketTimeAndSales)) {
                        JButton button = new JButton(ExchangeStore.getSharedInstance().getDescription(exchange.getSymbol()));
                        button.setFont(button.getFont().deriveFont(Font.PLAIN));
                        button.setBorderPainted(false);
                        button.setContentAreaFilled(false);
                        button.setAlignmentX(Component.CENTER_ALIGNMENT);
                        button.setActionCommand(exchange.getSymbol());
                        button.addActionListener(this);
                        button.setCursor(new Cursor(Cursor.HAND_CURSOR));
                        panel.add(button);
                    }

                }else{

                    JButton button = new JButton(ExchangeStore.getSharedInstance().getDescription(exchange.getSymbol()));
                    button.setFont(button.getFont().deriveFont(Font.PLAIN));
                    button.setBorderPainted(false);
                    button.setContentAreaFilled(false);
                    button.setAlignmentX(Component.CENTER_ALIGNMENT);
                    button.setActionCommand(exchange.getSymbol());
                    button.addActionListener(this);
                    button.setCursor(new Cursor(Cursor.HAND_CURSOR));
                    panel.add(button);
                }
//                if(exchange.isValidIinformationType(Meta.IT_MarketTimeAndSales)){
//                }
            }
            exchange = null;
        }
//        exchanges = null;
        list = null;
        getContentPane().add(panel, BorderLayout.CENTER);
        pack();
        setLocationRelativeTo(Client.getInstance().getFrame());
    }

    public String getExchange() {
        setVisible(true);
        return exchange;
    }

    public void actionPerformed(ActionEvent e) {
        exchange = e.getActionCommand();
        dispose();
    }

}
