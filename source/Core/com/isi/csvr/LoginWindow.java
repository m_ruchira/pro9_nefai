package com.isi.csvr;

import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.shared.*;
import com.isi.csvr.shared.IPSettings;
import com.isi.csvr.trading.datastore.Brokers;
import com.isi.csvr.trading.datastore.Broker;
import com.isi.csvr.trading.*;
import com.isi.csvr.middlewarehandler.MWTradingHandler;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.Enumeration;
import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: Aug 31, 2009
 * Time: 9:49:02 AM
 * To change this template use File | Settings | File Templates.
 */
public class LoginWindow extends JDialog
        implements ActionListener, WindowListener, KeyListener, Themeable {

    public static final int MODE_PRICE_SERVERS = 0;
    public static final int MODE_TRADING_SERVERS = 1;
    private JPanel jPanel1;
    private JLabel lblUser = new JLabel();
    private JLabel lblPass = new JLabel();
    private JLabel lblBroker = new JLabel();
    private TWComboBox cmbBrokers;
    private JTextField txtUser = new JTextField();
    private TWPasswordField txtPassK;
    private TWButton btnOk = new TWButton();
    private TWButton btnCancel = new TWButton();
    private boolean g_bStatus;
    private int mode;
    private static boolean tradeLoginSucess = false;
    int buttonOffset = 0;
    private JCheckBox saveUsername = new JCheckBox();
    private JCheckBox virtualKeyBoard = new JCheckBox();
    private JCheckBox savePriceUNAndPWD = new JCheckBox();
    private JButton linkToConfig;

    private JPanel lowerPanel;
    private JPanel upperPanel;
    private JPanel mainPanel;
    private ImageIcon headerImg;
    private ImageIcon backgroundImg;
    private JPanel dataPanel;
    private JPanel buttonPanel;
    private boolean isUserSaved=false;
    private final short MW_DIRECT_LOGIN = 1;

    public LoginWindow(Frame parent, String title, int mode, boolean modal) {
        super(parent, title, modal);
        this.setResizable(false);
        try {
            if (mode == MODE_TRADING_SERVERS) {
                setTitle(Language.getString("TRADE_LOGIN"));
            } else {
                setTitle(Language.getString("USER_LOGIN"));
            }
            this.mode = mode;
            createUI();
            pack();
            Theme.registerComponent(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructs a new non-modal unparented instance with a blank title.
     */
    public LoginWindow(Frame oParent, int mode) {
        this(oParent, "", mode, true);

    }

    public void createUI() {
        headerImg = new ImageIcon("images/theme" + Theme.getID() + "/login-header_"+Language.getLanguageTag()+".jpg");
        backgroundImg = new ImageIcon("images/theme" + Theme.getID() + "/login-bg.jpg");
        mainPanel = new JPanel();
        upperPanel = new JPanel() {
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(headerImg.getImage(), 0, 0, null);
            }
        };
        lowerPanel = new JPanel() {
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(backgroundImg.getImage(), 50, 0, null);
            }
        };

        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"105", "100%"}, 0, 0));
        mainPanel.add(upperPanel);
        mainPanel.add(lowerPanel);
        createDataPanel(lowerPanel);
        getContentPane().add(mainPanel);


    }

    public void createDataPanel(JPanel panel) {
        dataPanel = panel;
        dataPanel.setOpaque(true);
        lblUser.setText(Language.getString("USER"));
        lblUser.setFont(new Font("Arial", Font.BOLD, 12));
        lblPass.setText(Language.getString("PASSWORD"));
        lblPass.setFont(new Font("Arial", Font.BOLD, 12));
        linkToConfig = new JButton(Language.getString("LINK_TO_CONFIGURE"));
        linkToConfig.setHorizontalTextPosition(SwingConstants.LEADING);
        linkToConfig.setHorizontalAlignment(SwingConstants.LEADING);
        linkToConfig.setCursor(new Cursor(Cursor.HAND_CURSOR));
        linkToConfig.setFont(new Font("Arial", Font.PLAIN, 12));
        linkToConfig.setBorder(BorderFactory.createEmptyBorder());
        linkToConfig.setOpaque(false);
        linkToConfig.addActionListener(this);
        linkToConfig.setContentAreaFilled(false);

        if (mode == MODE_TRADING_SERVERS) {
            if(!TWControl.isSFCSpecificChangesAddToAccSum()){
                txtPassK = new TWPasswordField(this, TWControl.isUseVirtualKeyBoard(),
                        TWControl.getBooleanItem("VIRTUAL_KEYBOARD_RANDOM"));
                if (!TWControl.isUseVirtualKeyBoard()) {
                    txtPassK.addKeyListener(this);
                }
            }else{
                txtPassK = new TWPasswordField(this, (Settings.getItem("IS_VIRTUAL_KEYBOARD_SAVED").equals("true")),
                        TWControl.getBooleanItem("VIRTUAL_KEYBOARD_RANDOM"));
                if (!Settings.getItem("IS_VIRTUAL_KEYBOARD_SAVED").equals("true")) {
                    txtPassK.addKeyListener(this);
                }
            }
            txtUser.setDocument(new LimitedLengthDocument(20));
            txtUser.addKeyListener(this);
            txtPassK.setUsernameFiled(txtUser);
            lblBroker = new JLabel(Language.getString("BROKER"));
            lblBroker.setFont(new Font("Arial", Font.BOLD, 12));
            cmbBrokers = getBrokerList();
            saveUsername.setText(Language.getString("REMEMBER_USERNAME"));
            saveUsername.setFont(new Font("Arial", Font.PLAIN, 12));
            cmbBrokers.setActionCommand("B");
            virtualKeyBoard.setText(Language.getString("VIRTUAL_KEYBOARD"));
            virtualKeyBoard.setFont(new Font("Arial", Font.PLAIN, 12));
            cmbBrokers.addActionListener(this);
            if(!TWControl.isSFCSpecificChangesAddToAccSum()){
                virtualKeyBoard.setSelected(TWControl.isUseVirtualKeyBoard());
            }
            if ((Settings.getItem("IS_TRADE_USER_SAVED") != null) && (Settings.getItem("IS_TRADE_USER_SAVED").equals("true"))) {
                saveUsername.setSelected(true);
                txtUser.setText(Settings.getTradeUser());
                isUserSaved = true;

            }
            if ((Settings.getItem("IS_VIRTUAL_KEYBOARD_SAVED") != null) && (Settings.getItem("IS_VIRTUAL_KEYBOARD_SAVED").equals("true"))) {
                virtualKeyBoard.setSelected(true);
            }
            try {
                cmbBrokers.setSelectedIndex(Integer.parseInt(Settings.getItem("TRADE_BROKER")));
            } catch (Exception e) {
                e.printStackTrace();
            }
            saveUsername.addActionListener(this);
            virtualKeyBoard.addActionListener(this);
            saveUsername.setOpaque(false);
            virtualKeyBoard.setOpaque(false);
            broker_actionPerformed();
            if (TWControl.isSingleSingnOn()) {
                dataPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"10", "20", "20", "20", "20", "5", "20", "20", "5", "20", "5", "20","20"}, 40, 0));
                dataPanel.add(new JLabel());
                dataPanel.add(lblUser);
                dataPanel.add(txtUser);
                dataPanel.add(lblPass);
                dataPanel.add(txtPassK);
                dataPanel.add(new JLabel());
                dataPanel.add(saveUsername);
                dataPanel.add(virtualKeyBoard);
                dataPanel.add(new JLabel());
                dataPanel.add(linkToConfig);
                dataPanel.add(new JLabel());
                dataPanel.add(createButtonPanel());
                dataPanel.add(new JLabel());
            } else {
                dataPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"10", "20", "20", "20", "20", "20", "20", "5", "20", "20", "5", "20", "5", "20"}, 40, 0));
                dataPanel.add(new JLabel());
                dataPanel.add(lblBroker);
                dataPanel.add(cmbBrokers);
                dataPanel.add(lblUser);
                dataPanel.add(txtUser);
                dataPanel.add(lblPass);
                dataPanel.add(txtPassK);
//                dataPanel.add(lblBroker);
//                dataPanel.add(cmbBrokers);
                dataPanel.add(new JLabel());
                dataPanel.add(saveUsername);
                dataPanel.add(virtualKeyBoard);
                dataPanel.add(new JLabel());
                dataPanel.add(createButtonPanel());
                dataPanel.add(new JLabel());
                dataPanel.add(new JLabel());
            }


        } else {
            dataPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"10", "20", "20", "20", "20", "5", "20", "5", "20", "5", "20"}, 40, 0));
            txtPassK = new TWPasswordField(this, false, false);
            txtPassK.addKeyListener(this);
            txtUser.setDocument(new LimitedLengthDocument(20));
            txtUser.addKeyListener(this);
            txtPassK.setUsernameFiled(txtUser);
            savePriceUNAndPWD.setText(Language.getString("SAVE_USERNAME_AND_PASSWORD"));
            savePriceUNAndPWD.setFont(new Font("Arial", Font.PLAIN, 12));
            savePriceUNAndPWD.setOpaque(false);
            savePriceUNAndPWD.addActionListener(this);
            savePriceUNAndPWD.addKeyListener(this);
            dataPanel.add(new JLabel());
            dataPanel.add(lblUser);
            dataPanel.add(txtUser);
            dataPanel.add(lblPass);
            dataPanel.add(txtPassK);
            dataPanel.add(new JLabel());
            dataPanel.add(savePriceUNAndPWD);
            dataPanel.add(new JLabel());
            dataPanel.add(createButtonPanel());
            dataPanel.add(new JLabel());
            dataPanel.add(linkToConfig);
        }
       GUISettings.applyOrientation(dataPanel);
    }

    private JPanel createButtonPanel() {
        buttonPanel = new JPanel();
        buttonPanel.setOpaque(false);
        JPanel oPanel = (JPanel) this.getContentPane();
        oPanel.registerKeyboardAction(this, "O", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
        oPanel.registerKeyboardAction(this, "C", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
        oPanel.setOpaque(true);

        btnCancel.setActionCommand("C");
        btnCancel.addActionListener(this);
        btnCancel.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    btnCancel_actionPerformed();
            }
        });

        btnCancel.setText(Language.getString("CANCEL"));
        btnOk.setText(Language.getString("OK"));
        btnOk.addActionListener(this);
        btnOk.setActionCommand("O");
        buttonPanel.setLayout(new FlexGridLayout(new String[]{"100%", "80", "10", "80"}, new String[]{"100%"}, 0, 0));
        buttonPanel.add(new JLabel());
        buttonPanel.add(btnOk);
        buttonPanel.add(new JLabel());
        buttonPanel.add(btnCancel);
        GUISettings.applyOrientation(buttonPanel);
        return buttonPanel;

    }

    public String getPassword() {
        String password;

        password = new String(txtPassK.getPassword()).trim();

        return password;
    }

    void btnCancel_actionPerformed() {
        txtUser.setText("");
        txtPassK.setText("");
        g_bStatus = false;
        this.hide();
    }

    /*
    * Returns the password
    */
    public String getUserName() {
        return txtUser.getText();
    }

    private TWComboBox getBrokerList() {
        int brokerCount = Brokers.getSharedInstance().getBrokerCount();
        Enumeration brokers = Brokers.getSharedInstance().getBrokers();
        Object[] brokerList = new Object[brokerCount];
        int i = 0;

        while (brokers.hasMoreElements()) {
            Broker broker = (Broker) brokers.nextElement();
            TWComboItem comboItem = new TWComboItem(broker.getId(), broker.getDecription());
            brokerList[i] = comboItem;
            i++;
            broker = null;
            comboItem = null;
        }
        Arrays.sort(brokerList);
        TWComboBox cmbBrokers = new TWComboBox(brokerList);
        return cmbBrokers;
    }

    private void broker_actionPerformed() {
        TWComboItem comboItem = (TWComboItem) cmbBrokers.getSelectedItem();
        Broker broker = Brokers.getSharedInstance().getBroker(comboItem.getId());
        if(broker.getType() == MW_DIRECT_LOGIN){
            lblUser.setEnabled(false);
            lblPass.setEnabled(false);
            txtUser.setEnabled(false);
            txtPassK.setEnabled(false);
            saveUsername.setEnabled(false);
            virtualKeyBoard.setEnabled(false);
        }else{
            lblUser.setEnabled(true);
            lblPass.setEnabled(true);
            txtUser.setEnabled(true);
            txtPassK.setEnabled(true);
            saveUsername.setEnabled(true);
            virtualKeyBoard.setEnabled(true);
        }
        if (!TWControl.isSingleSingnOn()) {
            Object[] ips = broker.getIPs();
            com.isi.csvr.trading.IPSettings.getSharedInstance().removeIPS();
            for (int i = 0; i < ips.length; i++) {

                com.isi.csvr.trading.IPSettings.getSharedInstance().addIP((String) ips[i]);
            }
        }
        Brokers.getSharedInstance().setSelectedBroker(broker.getId());
        broker = null;
        comboItem = null;

    }

    public boolean showDialog() {
        g_bStatus = false;
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        if (mode == MODE_TRADING_SERVERS) {
            if (!TWControl.isSingleSingnOn()) {
                this.setSize(295, 360);
            } else {
                this.setSize(295, 345);
            }
        } else {
            this.setSize(295, 320);
        }
        this.setLocationRelativeTo(Client.getInstance().getFrame());
        /*this.setBounds((int) (oDm.getWidth() / 2 - 140),
            (int) (oDm.getHeight() / 2 - 90), 280, 230+buttonOffset);*/
        if (isUserSaved) {
            txtPassK.requestFocusInWindow();
            txtPassK.setCarretPosition(0);
        }
        this.setVisible(true);
        return g_bStatus;
    }


    void btnOk_actionPerformed() {
        Broker broker = null;
        if (cmbBrokers != null) {
            TWComboItem comboItem = (TWComboItem) cmbBrokers.getSelectedItem();
            broker = Brokers.getSharedInstance().getBroker(comboItem.getId());
        }
        if (((mode==MODE_TRADING_SERVERS) && (broker.getType() == MW_DIRECT_LOGIN))) {
            MWTradingHandler.getSharedInstance().showWindow(Short.parseShort("1"),"", broker.getIP(0), broker.getDecription());
            this.hide();
        } else {
        String password;
            password = new String(txtPassK.getPassword()).trim();
            if ((password.equals("")) || (txtUser.getText().trim().equals(""))) {
                Object[] options = {Language.getString("OK")};

                int n = JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                        Language.getString("MSG_PASSWORD_AND_USERNAME"),
                        Language.getString("ERROR"),
                        JOptionPane.OK_OPTION,
                        JOptionPane.ERROR_MESSAGE,
                        null,
                        options,
                        options[0]);
            } else {
                g_bStatus = true;
                if (mode == MODE_TRADING_SERVERS) {
                    Settings.setItem("TRADE_BROKER", "" + cmbBrokers.getSelectedIndex());
                    Settings.setItemInBulk("TRADE_USER", txtUser.getText().trim());
                } else {
                    if (savePriceUNAndPWD.isSelected()) {
                        Settings.setItemInBulk("DEFAULT_USER", txtUser.getText().trim());
                        Settings.setItemInBulk("PASSWORD", password);
                    }
                }
                this.hide();
            }
        }
/*        if (((mode==MODE_TRADING_SERVERS) && (broker.getType() != MW_DIRECT_LOGIN)) || mode==MODE_PRICE_SERVERS) {
            String password;
        password = new String(txtPassK.getPassword()).trim();
        if ((password.equals("")) || (txtUser.getText().trim().equals(""))) {
            Object[] options = {Language.getString("OK")};

            int n = JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                    Language.getString("MSG_PASSWORD_AND_USERNAME"),
                    Language.getString("ERROR"),
                    JOptionPane.OK_OPTION,
                    JOptionPane.ERROR_MESSAGE,
                    null,
                    options,
                    options[0]);
        } else {
            g_bStatus = true;
            if (mode == MODE_TRADING_SERVERS) {
                Settings.setItem("TRADE_BROKER", "" + cmbBrokers.getSelectedIndex());
                Settings.setItemInBulk("TRADE_USER", txtUser.getText().trim());
            } else {
                if (savePriceUNAndPWD.isSelected()) {
                    Settings.setItemInBulk("DEFAULT_USER", txtUser.getText().trim());
                    Settings.setItemInBulk("PASSWORD", password);
                }
            }
            this.hide();
        }
        } else {
            MWTradingHandler.getSharedInstance().showWindow(Short.parseShort("1"),"", broker.getIP(0), broker.getDecription());
            this.hide();
        }*/
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("C")) {
            btnCancel_actionPerformed();
        } else if (e.getActionCommand().equals("O")) {
            btnOk_actionPerformed();
        } else if (e.getActionCommand().equals("B")) {
            broker_actionPerformed();
        } else if (e.getSource() == (saveUsername)) {
            if (saveUsername.isSelected()) {
                Settings.setItem("IS_TRADE_USER_SAVED", "true");
            } else {
                Settings.setItem("IS_TRADE_USER_SAVED", "false");
            }
        } else if (e.getSource() == (virtualKeyBoard)) {
            TWControl.setUseVirtualKeyBoard(virtualKeyBoard.isSelected());
            txtPassK.setUseVirtualKeyboard(virtualKeyBoard.isSelected());

            if (virtualKeyBoard.isSelected()) {
                Settings.setItem("IS_VIRTUAL_KEYBOARD_SAVED", "true");
            } else {
                Settings.setItem("IS_VIRTUAL_KEYBOARD_SAVED", "false");
            }

            if (virtualKeyBoard.isSelected()) {
                txtPassK.removeKeyListener(this);
            } else {
                txtPassK.addKeyListener(this);
            }
        } else if (e.getSource() == (savePriceUNAndPWD)) {
            if (savePriceUNAndPWD.isSelected()) {
//                Settings.setItem("DEFAULT_USER",);
            }
        } else if (e.getSource().equals(linkToConfig)) {
            Client.getInstance().mnu_Config_ActionPerformed();
//             this.hide();
        }
    }

    public static boolean isTradeLoginSucess() {
        return tradeLoginSucess;
    }

    public static void setTradeLoginSucess(boolean tradeLoginSucess) {
        LoginWindow.tradeLoginSucess = tradeLoginSucess;
    }

    public void windowOpened(WindowEvent e) {
        if (!txtUser.getText().trim().equals("")) {
            txtUser.transferFocus();
        }
    }

    public void windowClosing(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowClosed(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowIconified(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowDeiconified(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowActivated(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void windowDeactivated(WindowEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyChar() == '\n') {
            e.consume();
            btnOk_actionPerformed();
        }
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        if (cmbBrokers != null) {
            SwingUtilities.updateComponentTreeUI(cmbBrokers);
        }
    }
}
