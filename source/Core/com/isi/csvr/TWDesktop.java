package com.isi.csvr;

import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.chart.ChartFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.ticker.custom.TradeFeeder;
import com.isi.csvr.browser.BrowserFrame;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.Application;
import com.isi.csvr.datastore.Exchange;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ComponentEvent;
import java.awt.event.ComponentListener;
import java.util.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TWDesktop extends JDesktopPane implements Themeable, ComponentListener, ApplicationListener {

    private DynamicArray list;
    private int windowIndex = 0;
    private TWDesktopComparator comparator;
    public static boolean setSame_layer = false;
    private static LinkedList stackFrames;
    private ImageIcon image;


    public TWDesktop() {
        list = new DynamicArray();
        stackFrames = new LinkedList();
        comparator = new TWDesktopComparator(this);
        list.setComparator(comparator);
        Theme.registerComponent(this);
        addComponentListener(this);
        Application.getInstance().addApplicationListener(this); 
    }

    public Component add(Component comp) {
        if (comp instanceof com.isi.csvr.iframe.TWDesktopInterface) {
            list.add(comp);
            if (((TWDesktopInterface) comp).getDesktopIndex() == TWDesktopInterface.UNASSIGNED) {
                ((TWDesktopInterface) comp).setDesktopIndex(windowIndex++);
            }

        } else if (comp instanceof javax.swing.JInternalFrame.JDesktopIcon) {
            TWDesktopInterface c =
                    (TWDesktopInterface) (((javax.swing.JInternalFrame.JDesktopIcon) comp).getInternalFrame());
            ((javax.swing.JInternalFrame.JDesktopIcon) comp).getInternalFrame().setLayer(GUISettings.SYMBOLBAR_LAYER);
            list.add(c);
            if (c.getDesktopIndex() == TWDesktopInterface.UNASSIGNED) {
                c.setDesktopIndex(windowIndex++);
            }
            c = null;
        }
        return super.add(comp);
    }

    public void add(Component comp, Object cons, int i) {
        if (comp instanceof com.isi.csvr.iframe.TWDesktopInterface) {
            list.add(comp);
            if (((TWDesktopInterface) comp).getDesktopIndex() == TWDesktopInterface.UNASSIGNED) {
                ((TWDesktopInterface) comp).setDesktopIndex(windowIndex++);
            }
        } else if (comp instanceof javax.swing.JInternalFrame.JDesktopIcon) {
            TWDesktopInterface c =
                    (TWDesktopInterface) (((javax.swing.JInternalFrame.JDesktopIcon) comp).getInternalFrame());
            ((javax.swing.JInternalFrame.JDesktopIcon) comp).getInternalFrame().setLayer(GUISettings.SYMBOLBAR_LAYER);
            list.add(c);
            if (c.getDesktopIndex() == TWDesktopInterface.UNASSIGNED) {
                c.setDesktopIndex(windowIndex++);
            }
            c = null;
        }
        super.add(comp, cons, i);
    }

    public void remove(Component comp) {
        if (comp instanceof com.isi.csvr.iframe.TWDesktopInterface) {
            list.remove(comp);
        } else if (comp instanceof javax.swing.JInternalFrame.JDesktopIcon) {
            TWDesktopInterface c =
                    (TWDesktopInterface) (((javax.swing.JInternalFrame.JDesktopIcon) comp).getInternalFrame());
            list.remove(c);
            c = null;
        }
        super.remove(comp);
    }

    public int getListPosition(Component comp, boolean closeMode) {
        return getListPosition(comp, closeMode, true);
    }
    
    public int getListPosition(Component comp, boolean closeMode, boolean forward) {
        if (closeMode) {
            comparator.setMode(TWDesktopComparator.CLOSE_MODE);
        } else {
            comparator.setMode(TWDesktopComparator.FOCUS_MODE);
        }
        if (forward){
            comparator.setDirection(TWDesktopComparator.FORWARD_DIRECTION);
        } else {
            comparator.setDirection(TWDesktopComparator.REVERSE_DIRECTION);
        }
        Collections.sort(list.getList(), comparator);
        return list.indexOf(comp);
    }

    public void focusNextWindow(int current, boolean closeMode) {
//        JInternalFrame frame = null;
//
//        if (closeMode) {
//            comparator.setMode(TWDesktopComparator.CLOSE_MODE);
//        } else {
//            comparator.setMode(TWDesktopComparator.FOCUS_MODE);
//        }
//        comparator.setDirection(TWDesktopComparator.FORWARD_DIRECTION);
//        Collections.sort(list.getList(), comparator);
//
//
//        try {
//            for (int i = current + 1; i < list.size(); i++) {
//                frame = (JInternalFrame) list.get(i);
//
//                if ((frame.isVisible()) && (!frame.isIcon())) {  // ignore invisible frames
//                    if (((TWDesktopInterface) frame).getWindowType() != Meta.WT_Ticker) {
//                        frame.setVisible(true);
//                        frame.setSelected(true);
//                        if (list.get(i) instanceof com.isi.csvr.ClientTable) {
//                            ((ClientTable) list.get(i)).getTable().requestFocus();
//                        }
//                        return;
//                    }
//                }
//            }
//        }
//        catch (Exception ex) {
//            ex.printStackTrace();
//        }
//
//        try {
//            for (int i = 0; i < current; i++) {
//                frame = (JInternalFrame) list.get(i);
//                if ((frame.isVisible()) && (!frame.isIcon())) {
//                    if (((TWDesktopInterface) frame).getWindowType() != Meta.WT_Ticker) {
//                        System.out.println(frame.getTitle());
//                        frame.setVisible(true);
//                        frame.setSelected(true);
//                        if (list.get(i) instanceof com.isi.csvr.ClientTable) {
//                            ((ClientTable) list.get(i)).getTable().requestFocus();
//                        }
//                        return;
//                    }
//                }
//            }
//        }
//        catch (Exception ex) {
//            ex.printStackTrace();
//        }

    }

    public void focusPreviousWindow(int current, boolean closeMode) {
        JInternalFrame frame = null;

        comparator.setMode(TWDesktopComparator.FOCUS_MODE);
        comparator.setDirection(TWDesktopComparator.REVERSE_DIRECTION);
        Collections.sort(list.getList(), comparator);

        try {
            for (int i = current + 1; i < list.size(); i++) {
                frame = (JInternalFrame) list.get(i);

                if ((frame.isVisible()) && (!frame.isIcon())) {  // ignore invisible frames
                    if (((TWDesktopInterface) frame).getWindowType() != Meta.WT_Ticker) {
                        frame.setVisible(true);
                        frame.setSelected(true);
                        if (list.get(i) instanceof com.isi.csvr.ClientTable) {
                            ((ClientTable) list.get(i)).getTable().requestFocus();
                        }
                        return;
                    }
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

        try {
            for (int i = 0; i < current; i++) {
                frame = (JInternalFrame) list.get(i);
                if ((frame.isVisible()) && (!frame.isIcon())) {
                    if (((TWDesktopInterface) frame).getWindowType() != Meta.WT_Ticker) {
                        System.out.println(frame.getTitle());
                        frame.setVisible(true);
                        frame.setSelected(true);
                        if (list.get(i) instanceof com.isi.csvr.ClientTable) {
                            ((ClientTable) list.get(i)).getTable().requestFocus();
                        }
                        return;
                    }
                }
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    public Object[] getWindows() {
        return list.getList().toArray();
    }

    public String toString() {
        String buffer = "";
        for (int i = 0; i < list.size(); i++) {
            JInternalFrame frame = (JInternalFrame) list.get(i);
            if (frame.isVisible())
                buffer += (i + " - " + frame.getLayer() + ":" + ((TWDesktopInterface) list.get(i)).getWindowType() + " - " + (frame.getTitle()) + "\n");
        }
        return buffer;
    }

    /**
     * Invoked when the component's position changes.
     */
    public static void doCloseWindow(JInternalFrame frame1) {
        final JInternalFrame frame = frame1;
        if (frame instanceof WindowWrapper) {
            ((WindowWrapper) frame).windowClosing();
        }
        switch (frame.getDefaultCloseOperation()) {
            case JInternalFrame.DO_NOTHING_ON_CLOSE:
                //stackFrames.addFirst(frame);
                break;
            case JInternalFrame.HIDE_ON_CLOSE:
                frame.setVisible(false);
                if (frame.isSelected())
                    try {
                        frame.setSelected(false);
                    }
                    catch (Exception pve) {
                    }
                if (SharedMethods.isInCallTree(TWActions.class)) { // check it it was Escaped
                    if (!stackFrames.contains(frame)) {
                        stackFrames.addFirst(frame);
                    }
                    Thread disposedelaying = new Thread("Window Closing Timer") {
                        public void run() {
                            try {
                                Thread.sleep(30000);
                            } catch (InterruptedException e) {
                            }
                            stackFrames.remove(frame);
                            try {
                                if (!frame.isVisible()) {
                                    frame.setVisible(false);
                                }
                            } catch (Exception e) {
                            }

                        }
                    };
                    disposedelaying.start();
                }
                break;
            case JInternalFrame.DISPOSE_ON_CLOSE:
                if (SharedMethods.isInCallTree(TWActions.class)) { // check it it was Escaped
                    frame.setVisible(false);
                    if (!stackFrames.contains(frame)) {
                        stackFrames.addFirst(frame);
                    }
                    Thread disposedelaying = new Thread("Window Closing Timer") {
                        public void run() {
                            try {
                                Thread.sleep(30000);
                            } catch (InterruptedException e) {
                            }
                            stackFrames.remove(frame);
                            try {
                                if (!frame.isVisible()) {
                                    frame.dispose();
                                }
                            } catch (Exception e) {
                            }

                        }
                    };
                    disposedelaying.start();
                } else {
                    frame.dispose();
                }

            default:
                break;
        }
    }

    public static void showClossedFrames() {
        try {
            JInternalFrame frame = (JInternalFrame) stackFrames.removeFirst();
            frame.setVisible(true);
            frame.show();

        } catch (Exception e) {
            System.out.println("No frame is remaining in the stack......");
        }

    }

    public Object[] getRecentlyClossedWindows() {
        Object[] frameArray = new Object[stackFrames.size()];
        for (int i = 0; i < stackFrames.size(); i++) {
            frameArray[i] = stackFrames.get(i);
        }
        return frameArray;
    }

    public static void removeReopendWindow(JInternalFrame frame) {
        try {
            stackFrames.remove(frame);
        } catch (Exception e) {
            System.out.println("Removing when reopening faile...");

        }

    }

    public static void addWindowTab(InternalFrame iframe) {
        if ((iframe.getDesktopPane() != null) && (!iframe.isDetached()) && !(iframe instanceof NonTabbable)) {
            Client.getInstance().getWindowTab().addTab(iframe.getTitle(), iframe);
            Client.getInstance().getWindowTab().setSelectedComponent(iframe);
            Client.getInstance().getWindowTab().repaint();
            if (Settings.isTabIndex()) {
                Client.getInstance().addTabIndexpanel(Client.getInstance().getWindowTab().getTabCount() > 0);
            }
        }
    }

    public static void addWindowTab(ClientTable iframe) {
        if ((iframe.getDesktopPane() != null) && (!iframe.isDetached()) && !(iframe instanceof NonTabbable)) {
            Client.getInstance().getWindowTab().addTab(iframe.getTitle(), iframe);
            Client.getInstance().getWindowTab().setSelectedComponent(iframe);
            Client.getInstance().getWindowTab().repaint();
            if (Settings.isTabIndex()) {
                Client.getInstance().addTabIndexpanel(Client.getInstance().getWindowTab().getTabCount() > 0);
            }
        }
    }

    public static void updateTabTitles(InternalFrame frame) {
        try {
        for (int i = 0; i < Client.getInstance().getWindowTab().getTabCount(); i++) {
            if (Client.getInstance().getWindowTab().getComponentAt(i).equals(frame)) {
                Client.getInstance().getWindowTab().setTitleAt(i, frame.getTitle());
                break;
            }
        }
        try {
            Client.getInstance().getWindowTab().repaint();
            Client.getInstance().getWindowTab().updateUI();
        } catch (Exception e1) {
        }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static void updateTabTitles(ClientTable frame) {
        try {
        for (int i = 0; i < Client.getInstance().getWindowTab().getTabCount(); i++) {
            if (Client.getInstance().getWindowTab().getComponentAt(i).equals(frame)) {
                Client.getInstance().getWindowTab().setTitleAt(i, frame.getTitle());
                break;
            }
        }
        try {
            Client.getInstance().getWindowTab().repaint();
            Client.getInstance().getWindowTab().updateUI();
        } catch (Exception e1) {
        }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static void removeWindowtab(InternalFrame iframe) {
        try {
            Client.getInstance().getWindowTab().removeTab(iframe);
            if (Client.getInstance().getWindowTab().getTabCount() == 0) {
                Client.getInstance().removeTabIndexPanel();
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.out.println("Tab removing failed....");
        }
    }

    public static void removeWindowtab(ClientTable iframe) {
        try {
            Client.getInstance().getWindowTab().removeTab(iframe);
            if (Client.getInstance().getWindowTab().getTabCount() == 0) {
                Client.getInstance().removeTabIndexPanel();
            }
        } catch (Exception e) {
            System.out.println("Tab removing failed....");
        }
    }

    public static boolean checkInternalFrameAvailability(JInternalFrame iframe) {
        boolean isAvailable = false;
        JInternalFrame[] frameArray = Client.getInstance().getDesktop().getAllFrames();
        for (int i = 0; i < frameArray.length; i++) {
            if (frameArray[i] == iframe) {
                isAvailable = true;
                break;
            } else {
                isAvailable = false;
            }

        }
        return isAvailable;
    }

    public static void setTabForSelectedWindow(InternalFrame iframe) {
        try {
            Client.getInstance().getWindowTab().setSelectedComponent(iframe);
        } catch (Exception e) {
            System.out.println("Componet not found..........");
        }

    }

    public static void setTabForSelectedWindow(ClientTable iframe) {
        try {
            Client.getInstance().getWindowTab().setSelectedComponent(iframe);
        } catch (Exception e) {
            System.out.println("Componet not found..........");
        }

    }

     public static void setTabForSelectedWindow(JInternalFrame iframe) {
        try {
            Client.getInstance().getWindowTab().setSelectedComponent(iframe);
        } catch (Exception e) {
            System.out.println("Componet not found..........");
        }

    }

    public void setDetachedFrames() {
        Object[] list = getWindows();//((TWDesktop) Client.getInstance().getDesktop()).getWindows();
        TWDesktopInterface frame = null;
        for (int i = 0; i < list.length; i++) {
            try {
                frame = (TWDesktopInterface) list[i];
                if ((frame != null) && (frame instanceof InternalFrame)) {
                    InternalFrame iframe = (InternalFrame) frame;
                    System.out.println(iframe.getTitle());
                    if (iframe.getViewSetting().isDetached) {
                        iframe.setVisible(true);
                        ((DetachableRootPane) iframe.getRootPane()).detach();
                    }
                    Point location = iframe.getViewSetting().getLocation();
                    ((JFrame) iframe.getDetachedFrame()).setLocation(location);
                }
            } catch (Exception e) {
//               e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        // Chart frame
        /*ViewSetting defaultSettings = ViewSettingsManager.getSummaryView("UNICHART");
        if (defaultSettings.isDetached) {
            ((InternalFrame) frame).setVisible(true);
            ((DetachableRootPane) ((InternalFrame) frame).getRootPane()).detach();
        }*/
    }

    public void setDetachedClientTables() {
        Object[] list = getWindows();//((TWDesktop) Client.getInstance().getDesktop()).getWindows();
        ClientTableInterface frame = null;
        for (int i = 0; i < list.length; i++) {
            try {
                frame = (ClientTableInterface) list[i];
                if ((frame != null) && (frame instanceof ClientTable)) {
                    ClientTable iframe = (ClientTable) frame;
                    if (iframe.getViewSettings().isDetached) {
                        iframe.setVisible(true);
                        ((DetachableRootPane) iframe.getRootPane()).detach();
                    }
                    Point location = iframe.getViewSettings().getLocation();
                    ((JFrame) iframe.getDetachedFrame()).setLocation(location);
                }
            } catch (Exception e) {
//               System.out.println("Detaching Client tables..........");
//               e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public static boolean isInTabbedPanel(InternalFrame frame) {
        try {
        return Client.getInstance().getWindowTab().contains(frame);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isInTabbedPanel(ClientTable frame) {
        try {
            return Client.getInstance().getWindowTab().contains(frame);
        } catch (Exception e) {
            return false;
        }
    }

     public void applyTheme() {
        image = new ImageIcon(System.getProperties().get("user.dir")+"/images/common/mubasher_en.gif");
    }

   public void refreshDesktop(){
       int iconX=0;
       for(int i=0;i<list.size();i++){
           if (list.get(i) instanceof JInternalFrame) {
                JInternalFrame iframe=(JInternalFrame)list.get(i);
                if (iframe.isIcon()) {
                        iframe.getDesktopIcon().setBounds(iconX, (int)this.getVisibleRect().getHeight()-(int)iframe.getDesktopIcon().getBounds().getHeight(),
                                (int)iframe.getDesktopIcon().getBounds().getWidth(), (int)iframe.getDesktopIcon().getBounds().getHeight());
                        updateUI();
                        iconX = iconX+(int)iframe.getDesktopIcon().getBounds().getWidth();
                }
            }
        }
    }

    public void changeLayers() {
        int iconX = 0;
        for (int i = 0; i < list.size(); i++) {
            if (list.get(i) instanceof InternalFrame) {
                InternalFrame iframe;
                try {
                    iframe = (InternalFrame) list.get(i);
                    ViewSetting oSetting = iframe.getViewSetting();
                    if(oSetting == null){
                        if(iframe.getFrameLayer()  == GUISettings.INTERNAL_DIALOG_LAYER) {
                            continue;
                        }  else if(Settings.isPutAllToSameLayer()){
                            iframe.setLayer(GUISettings.DEFAULT_LAYER);
                            iframe.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
                        } else {
                            iframe.setLayer(iframe.getFrameLayer());
                            iframe.getDesktopPane().setLayer(this, iframe.getFrameLayer(), 0);
                        }
                    } else {
                        if(oSetting.getLayer() == GUISettings.INTERNAL_DIALOG_LAYER){
                            continue;
                        } else if(Settings.isPutAllToSameLayer()){
                            iframe.setLayer(GUISettings.DEFAULT_LAYER);
                            iframe.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, oSetting.getIndex());
                        } else {
                            iframe.setLayer(oSetting.getLayer());
                            iframe.getDesktopPane().setLayer(this, oSetting.getLayer(), oSetting.getIndex());
                        }
                    }
                    /*if (Settings.isPutAllToSameLayer() && (iframe.getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                        iframe.setLayer(GUISettings.DEFAULT_LAYER);
                        iframe.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, iframe.getViewSetting().getIndex());
                    } else {
                        iframe.setLayer(iframe.getViewSetting().getLayer());
                        iframe.getDesktopPane().setLayer(this, iframe.getViewSetting().getLayer(), iframe.getViewSetting().getIndex());
                    }*/
//                    iframe.applySettings();
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            } else if (list.get(i) instanceof ClientTable) {
                ClientTable iframe;
                try {
                    iframe = (ClientTable) list.get(i);
                    if (Settings.isPutAllToSameLayer() && (iframe.getPreferredLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                        iframe.setLayer(GUISettings.DEFAULT_LAYER);
                    } else {
                        iframe.setLayer(iframe.getPreferredLayer());
                    }
//                    iframe.applySettings();
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            } else if (list.get(i) instanceof BrowserFrame) {
                BrowserFrame iframe;
                try {
                    iframe = (BrowserFrame) list.get(i);
                    if (Settings.isPutAllToSameLayer() && (iframe.getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                        iframe.setLayer(GUISettings.DEFAULT_LAYER);
                    } else {
                        iframe.setLayer(iframe.getViewSetting().getLayer());
                    }
//                    iframe.applySettings();
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
    }

    public void checkForDetach(){
        for(int i=0;i<list.size();i++){
           if (list.get(i) instanceof InternalFrame) {
               InternalFrame iframe=(InternalFrame)list.get(i);
               iframe.checkForDetach();
           }else if(list.get(i) instanceof ClientTable){
               ClientTable table = (ClientTable) list.get(i);
               table.checkForDetach();
           }
        }
    }

    public void paint(Graphics g) {

        Color lightColor = Color.gray;
        Color darkColor = Color.white;

        int width = getWidth();
            int height = getHeight();

        if (image.getIconWidth() > 0){
            for (int i = 0; i <= height; i++) {
                g.setColor(new Color((((lightColor.getRed() * (height - i)) + darkColor.getRed() * i) / height),
                        (((lightColor.getGreen() * (height - i)) + darkColor.getGreen() * i) / height),
                        (((lightColor.getBlue() * (height - i)) + darkColor.getBlue() * i) / height)));
                g.drawLine(0, i, width, i);
            }
            g.drawImage(image.getImage(),width - image.getIconWidth(),height - image.getIconHeight(),this);
            super.paintChildren(g);
        }else {
            super.paint(g);
        }
    }

    public void componentResized(ComponentEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        refreshDesktop();
    }

    public void componentMoved(ComponentEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        refreshDesktop();
    }

    public void componentShown(ComponentEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        refreshDesktop();
    }

    public void componentHidden(ComponentEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        refreshDesktop();
    }

    public void fireLayerChanged() {
        changeLayers();
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
        setDetachedFrames();
        setDetachedClientTables();
    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}