package com.isi.csvr.cashFlowWatch;

import com.isi.csvr.shared.*;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.event.ExchangeListener;

import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Date;
import java.io.File;
import java.io.BufferedReader;
import java.io.FileReader;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.text.ParseException;


/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 17, 2008
 * Time: 5:18:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowHistoryDataStore implements ExchangeListener{

    private static CashFlowHistoryDataStore self;
    private Hashtable<String,CashFlowExchgSummary> cfHistoryStore;
    private SimpleDateFormat format ;
    private static String dataHome;
    private ArrayList exchangeList;
    private CashFlowHistoryDataStore() {
        cfHistoryStore = new Hashtable<String, CashFlowExchgSummary>();
        initStoreFolder();
         format = new SimpleDateFormat("yyyyMMdd");
        dataHome = Settings.getAbsolutepath() + "DataStore/CFHistory/";
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        loadExchangeList();
        clearDataFolderOnStart();
    }

    public static CashFlowHistoryDataStore getSharedInstance(){
        if(self==null){
            self = new CashFlowHistoryDataStore();
            return self;
        }else{
            return self;
        }
    }

    public void clearStores(){
        cfHistoryStore.clear();
    }
    private void loadExchangeList(){
        exchangeList = new ArrayList();
        SharedMethods.populateExchanges(exchangeList, true);
    }

    private void initStoreFolder(){
        File file = new File(Settings.getAbsolutepath() + "DataStore/CFHistory/");
        if(!file.exists()){
            file.mkdirs();
        }
    }

    public ArrayList<CashFlowSummaryRecord> getSummaryList(String exchange, String period){
        ArrayList<CashFlowSummaryRecord> list = null;
        if(cfHistoryStore.containsKey(exchange)){
            CashFlowExchgSummary summary = cfHistoryStore.get(exchange);
            list= summary.getSummaryListForPeriod(period);
            if(list != null){
                return list;
            }else{
                return null;
            }
            
        }else{
           int resp= checkForSummaryFile(exchange);
            if(resp==2){
                CashFlowExchgSummary summary = cfHistoryStore.get(exchange);
                list= summary.getSummaryListForPeriod(period);
                if(list != null){
                    return list;
                }else{
                    return null;
                }
            }
        }
        return null;
    }

    private int checkForSummaryFile(String exchange){
        Exchange exg= ExchangeStore.getSharedInstance().getExchange(exchange);
        if(exg==null){
            return 0;
        }
        long time = exg.getMarketDate();
        String foldername= format.format(new Date(time));
        File folder= new File(dataHome +exchange+ "/" + foldername+ "/");
        String mktDateHome =dataHome +exchange+ "/" + foldername+ "/"; 
        if(!folder.exists()){
            return 1;
        }
        File summmary= new File(mktDateHome + "cashflowsummary_"+ exchange.toLowerCase()  +".csv");
        if(summmary.exists()){
            loadSummaryFiletoStore(summmary,exchange);
            return 2;
        }
       return 0;
    }

    public boolean checkIfSummaryFileExsists(String exchange){
         Exchange exg= ExchangeStore.getSharedInstance().getExchange(exchange);
        if(exg==null){
            return false;
        }
        long time = exg.getMarketDate();
        String foldername= format.format(new Date(time));
        File folder= new File(dataHome +exchange+ "/" + foldername+ "/");
        String mktDateHome =dataHome +exchange+ "/" + foldername+ "/";
        File summmary= new File(mktDateHome + "cashflowsummary_"+ exchange.toLowerCase()  +".csv");
        if(summmary.exists()){

            return true;
        }else{
            return false;
        }
    }


    private void loadSummaryFiletoStore(File summary, String exchange){
      ArrayList<String> rowlist = new ArrayList<String>();
      ArrayList<CashFlowSummaryRecord> splittedlist = new ArrayList<CashFlowSummaryRecord>();
        CashFlowExchgSummary summaryRecord = new CashFlowExchgSummary();
        ArrayList<CashFlowSummaryRecord> record7D = new ArrayList<CashFlowSummaryRecord>();
        ArrayList<CashFlowSummaryRecord> record30D= new ArrayList<CashFlowSummaryRecord>();
        ArrayList<CashFlowSummaryRecord> record90D= new ArrayList<CashFlowSummaryRecord>();
        summaryRecord.setExchange(exchange.toUpperCase());
            try {
        BufferedReader in = new BufferedReader(new FileReader(summary));
        String str;
        int i=0;
        while ((str = in.readLine()) != null) {
           rowlist.add(str);
        }
        in.close();
        } catch (IOException e) {
          e.printStackTrace();

        }

        if(!rowlist.isEmpty()){
         //   for(int i=rowlist.size()-1; i>=0; i--){
            for(int i=0; i<rowlist.size(); i++){
                 String rowdata = rowlist.get(i);
                 String[] datalist = rowdata.split(Meta.FS);
                if(datalist!= null && datalist.length==16){
                    //String symbol = datalist[0];
                    validateSummaryEntry(datalist,exchange ,record7D,record30D,record90D );
                }
                datalist= null;
            }
        }
        summaryRecord.setSummaryListForPeriod(record7D, Language.getString("CF_SUMMARY_7D"));
        summaryRecord.setSummaryListForPeriod(record30D, Language.getString("CF_SUMMARY_30D"));
        summaryRecord.setSummaryListForPeriod(record90D, Language.getString("CF_SUMMARY_90D"));
        cfHistoryStore.put(exchange,summaryRecord);
    }

    public ArrayList<CashFlowHistoryRecord> getDetailedSummaryList(String exchange, String sKey){
         ArrayList<CashFlowHistoryRecord> list = null;
        if(cfHistoryStore.containsKey(exchange)){
            CashFlowExchgSummary summary = cfHistoryStore.get(exchange);
            list= summary.getDetailedListForSymbol(sKey);
            if(list != null){
                return list;
            }else{
                int resp= checkForDetailFile(exchange, SharedMethods.getSymbolFromKey(sKey),sKey);
                if(resp==2){
                    summary = cfHistoryStore.get(exchange);
                    list= summary.getDetailedListForSymbol(sKey);
                    if(list != null){
                        return list;
                    }else{
                        return null;
                    }
                }
            }
        }else{
           return null;
        }
        return null;
    }

    private int checkForDetailFile(String exchange, String symbol, String sKey){
         Exchange exg= ExchangeStore.getSharedInstance().getExchange(exchange);
        if(exg==null){
            return 0;
        }
        long time = exg.getMarketDate();
        String foldername= format.format(new Date(time));
        File folder= new File(dataHome +exchange+ "/" + foldername+ "/");
        String mktDateHome =dataHome +exchange+ "/" + foldername+ "/";
        if(!folder.exists()){
            return 1;
        }
        File summmary= new File(mktDateHome + symbol +".csv");
        if(summmary.exists()){
            loadDetailToFileToStore(summmary,exchange,sKey);
            return 2;
        }
       return 0;
    }


    private void loadDetailToFileToStore(File summary, String exchange,String sKey){
       ArrayList<String> rowlist = new ArrayList<String>();
      ArrayList<CashFlowHistoryRecord> splittedlist = new ArrayList<CashFlowHistoryRecord>();
            try {
        BufferedReader in = new BufferedReader(new FileReader(summary));
        String str;
        int i=0;
        while ((str = in.readLine()) != null) {
           rowlist.add(str);
        }
        in.close();
        } catch (IOException e) {
               e.printStackTrace();

        }
      //  String date = System.currentTimeMillis()+"";
        //String[] md = {date,"22.5","2349","67045","145678.86","5000","3600","456980.45"};


        if(!rowlist.isEmpty()){
            for(int i=rowlist.size()-1; i>=0; i--){
                 String rowdata = rowlist.get(i);
                 String[] datalist = rowdata.split(Meta.FS);
                if(datalist!= null && datalist.length>=8){
                    CashFlowHistoryRecord rec = validateDetailEntry(datalist);
                    if(rec != null){
                        splittedlist.add(rec);
                    }
                }
                datalist= null;
            }


        }

        cfHistoryStore.get(exchange).setDetailedListForSymbol(splittedlist,sKey);



    }

    private void validateSummaryEntry(String[] datalist, String exchange,ArrayList list7d,ArrayList list30d,ArrayList list90d){
        CashFlowSummaryRecord record7 = new CashFlowSummaryRecord();
        CashFlowSummaryRecord record30 = new CashFlowSummaryRecord();
        CashFlowSummaryRecord record90 = new CashFlowSummaryRecord();
        String symbol = datalist[0];
        int instrument= SymbolMaster.getInstrumentType(symbol,exchange);
        String sKey = SharedMethods.getKey(exchange,symbol,instrument);
        String shortdes =  DataStore.getSharedInstance().getShortDescription(sKey);

        try {
            record7.setSKey(sKey);
            record7.setDescription(shortdes);
            record7.setExchange(exchange);
            record7.setSymbol(symbol);
            record7.setVolume((long)Double.parseDouble(datalist[1]));
            record7.setTurnover(Double.parseDouble(datalist[2]));
            record7.setPercentage(Double.parseDouble(datalist[3])/100);
            record7.setCashInTurnover(Double.parseDouble(datalist[4]));
            record7.setCashOutTurnover(Double.parseDouble(datalist[5]));
            list7d.add(record7);


            record30.setSKey(sKey);
            record30.setDescription(shortdes);
            record30.setExchange(exchange);
            record30.setSymbol(symbol);
            record30.setVolume((long)Double.parseDouble(datalist[6]));
            record30.setTurnover(Double.parseDouble(datalist[7]));
            record30.setPercentage(Double.parseDouble(datalist[8])/100);
            record30.setCashInTurnover(Double.parseDouble(datalist[9]));
            record30.setCashOutTurnover(Double.parseDouble(datalist[10]));
            list30d.add(record30);

            record90.setSKey(sKey);
            record90.setDescription(shortdes);
            record90.setExchange(exchange);
            record90.setSymbol(symbol);
            record90.setVolume((long)Double.parseDouble(datalist[11]));
            record90.setTurnover(Double.parseDouble(datalist[12]));
            record90.setPercentage(Double.parseDouble(datalist[13])/100);
            record90.setCashInTurnover(Double.parseDouble(datalist[14]));
            record90.setCashOutTurnover(Double.parseDouble(datalist[15]));
            list90d.add(record90);
        } catch (NumberFormatException e) {

            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

     private CashFlowHistoryRecord validateDetailEntry(String[] datalist){
        CashFlowHistoryRecord record = new CashFlowHistoryRecord();

        try {
            record.setDate(format.parse(datalist[0]).getTime());
            record.setLastPrice(Double.parseDouble(datalist[1]));
            record.setCashInOrders(Long.parseLong(datalist[2]));
            record.setCashInVloume(Long.parseLong(datalist[3]));
            record.setCashInTurnover(Double.parseDouble(datalist[4]));
            record.setCashoutOrders(Long.parseLong(datalist[5]));
            record.setCashoutVolume(Long.parseLong(datalist[6]));
            record.setCashoutTurnover(Double.parseDouble(datalist[7]));

            return record;
        } catch (NumberFormatException e) {
            e.printStackTrace();
            return null;
        }catch(ParseException e){
            return null;
        }


    }

    public String getExchangetDataHome(String exchange){
        Exchange exg= ExchangeStore.getSharedInstance().getExchange(exchange);
        if(exg==null){
            return"" ;
        }
        long time = exg.getMarketDate();
        String foldername= format.format(new Date(time));
        return   dataHome +exchange+ "/" + foldername+ "/";
    }
    private String getExchangeHome(String exchange){
        return dataHome+exchange+"/";
    }
    private String getFolderNameForDate(String exchange){
         Exchange exg= ExchangeStore.getSharedInstance().getExchange(exchange);
        if(exg==null){
            return"" ;
        }
        long time = exg.getMarketDate();
        String foldername= format.format(new Date(time));
        return foldername;
    }

    public void loadSelectedSummaryFile(String fpath,String exchange){
        String filename = fpath+ "cashflowsummary_"+ exchange.toLowerCase()  +".csv";
        File file = new File(filename);
        if(file.exists()){
            loadSummaryFiletoStore(file,exchange.toUpperCase());
        }


    }

    public void loadSelectedDetailFile(String fpath,String sKey,String symbol, String exchange){
       String filename = fpath+  symbol  +".csv";
        File file = new File(filename);
        if(file.exists()){
            loadDetailToFileToStore(file,exchange.toUpperCase(),sKey);
        }

    }

    public synchronized void deleteFolders(String exchange, long mktdate){
        String exhome = getExchangeHome(exchange);
        File folder = new File(exhome);
        if(folder.exists()){
           String todayfolder = getFolderNameForDate(exchange);
           File[] list= folder.listFiles();
          if(list!=null && list.length>0){
              for(int i=0; i<list.length; i++){
                  File f= list[i];
                  if(!f.getName().equals(todayfolder)){
                      deleteTree(f);
                     boolean deleted=  f.delete();
                      System.out.println("CF history delete " + deleted);
                  }
              }
          }
        }
    }

    private void deleteTree(File f){
        File[] list = f.listFiles();
        if(list!= null){
            for(int i=0; i<list.length;i++){
                 File file =list[i];
                if(file.isDirectory()){
                 deleteTree(f);
                }
                file.delete();

            }
        }
    }

    public void exchangeAdded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeRemoved(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeUpgraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeDowngraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangesAdded(boolean offlineMode) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangesLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        deleteFolders(exchange.getSymbol(),newDate);
    }

    public void exchangeMasterFileLoaded(String exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTradingInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    private void clearDataFolderOnStart(){
        if(!exchangeList.isEmpty()){
            for(int j=0; j<exchangeList.size(); j++){
                Exchange exchange = ((Exchange)exchangeList.get(j));
                deleteFolders(exchange.getSymbol(),exchange.getMarketDate() );
            }
        }
    }
}
