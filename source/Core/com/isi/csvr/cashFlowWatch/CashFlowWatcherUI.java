package com.isi.csvr.cashFlowWatch;

import com.isi.csvr.ClientTable;
import com.isi.csvr.Client;
import com.isi.csvr.datastore.*;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 17-Apr-2008 Time: 11:45:50 To change this template use File | Settings
 * | File Templates.
 */

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.table.Table;
import com.isi.csvr.shared.*;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.Client;
import com.isi.csvr.ClientTable;
import com.isi.csvr.WorkInProgressIndicator;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.dnd.DraggableTable;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.datastore.*;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.table.AbstractTableModel;
import java.awt.event.ActionListener.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import java.util.Vector;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.*;
import java.beans.PropertyVetoException;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 15-Apr-2008 Time: 08:53:58 To change this template use File | Settings
 * | File Templates.
 */
public class CashFlowWatcherUI implements ActionListener, ExchangeListener {
    private ClientTable volumeTable;
    private JPanel northPanel;
    private ArrayList<TWComboItem> exchanges;
    private ArrayList<TWComboItem> markets;
    private TWComboBox exchangeCombo;
    private TWComboBox marketCombo;
    private ViewSetting g_oSetting;
    public static CashFlowWatcherUI self = null;
    TWComboItem itemSevenday;
    TWComboItem itemThirtyDay;
    TWComboItem itemNinetyDay;
    private String selectedExchange = null;

    private int selectedExchangeID = 441;
    private int selectedMarketID = 442;
    Symbols volumeWatcherSymbols;


    public static CashFlowWatcherUI getSharedInstance() {
        if (self == null) {
            self = new CashFlowWatcherUI();
        }
        return self;
    }

    private CashFlowWatcherUI() {
        g_oSetting = ViewSettingsManager.getSummaryView("CASHFLOW_WATCHER_VIEW");
        g_oSetting.setRenderingIDs(ViewSettingsManager.getMainBoardRenderingIDs());
        setViewSetting();
        createUI();
    }

    private void createUI() {
        northPanel = new JPanel();
        exchanges = new ArrayList<TWComboItem>();
        markets = new ArrayList<TWComboItem>();
        exchangeCombo = new TWComboBox(new TWComboModel(exchanges));
        marketCombo = new TWComboBox(new TWComboModel(markets));
        populateExchangesCombo();
        exchangeCombo.addActionListener(this);
        marketCombo.addActionListener(this);
        northPanel.setLayout(new FlexGridLayout(new String[]{"120", "200","30", "90","200","100%"}, new String[]{"20"}, 3, 3));
        northPanel.setPreferredSize(new Dimension((int) g_oSetting.getSize().getWidth(), 26));
        northPanel.add(new JLabel(Language.getString("SELECT_EXCHANGE")));
        northPanel.add(exchangeCombo);
        northPanel.add(new JLabel());
        northPanel.add(new JLabel(Language.getString("SELECT_MARKET")));
        northPanel.add(marketCombo);
        northPanel.add(new JLabel());

        GUISettings.applyOrientation(northPanel);
        volumeTable = new ClientTable();
        volumeTable.setSize(g_oSetting.getSize());
        volumeTable.setResizable(true);
        volumeTable.setMaximizable(true);
        volumeTable.setIconifiable(true);
        volumeTable.setClosable(true);
        volumeTable.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
        volumeTable.setSortable(true);
        volumeTable.setLinkWindowControl(true);
        volumeTable.setViewSettings(g_oSetting);
        volumeWatcherSymbols = new Symbols();
        volumeTable.setSymbols(volumeWatcherSymbols);

        applySavedSettings();
        volumeTable.createTable(true);
        volumeTable.setVisible(true);
        volumeTable.getTable().addMouseListener(Client.getInstance().getSymbolMouseListener());
        volumeTable.setTypeDescription(Language.getString("CASHFLOW_WATCHER"));
        volumeTable.setNorthPanel(northPanel);
        Client.getInstance().oTopDesktop.add(volumeTable);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        volumeTable.getScrollPane().getViewport().addMouseListener(Client.getInstance().getTableMouseListener());
        volumeTable.updateClientTable();
        volumeTable.setPreferredLayer(GUISettings.TOP_LAYER);
        volumeTable.adjustColumns();
        volumeTable.applySettings();
        volumeTable.updateUI();
        volumeTable.applyTheme();


    }

    public void setViewSetting() {
        try {
            Vector oViewSettingVec = ViewSettingsManager.getViewsVector();
            Object[] views = oViewSettingVec.toArray();
            for (int j = 0; j < views.length; j++) {
                ViewSetting oSettings = ((ViewSetting) views[j]).getObject();
                if (oSettings.getMainType() == ViewSettingsManager.MARKET_VIEW) {
                    if (oSettings.isCashFlowWatcherType()) {
                        g_oSetting = (ViewSetting) oSettings.clone();
                        ViewSettingsManager.addMainBoardSetting(g_oSetting);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void populateExchangesCombo() {
        SharedMethods.populateExchangesForTWCombo(exchanges, true);
        validateExchanges();

    }
     private void populateMarketCombo(String exchange) {
         markets.clear();
        SharedMethods.populateSubMarketsForTWCombo(markets, true,exchange);
//         marketCombo.setSelectedIndex(0);
        validateExchanges();
    }
    private void validateExchanges(){
        if(exchanges!= null && !exchanges.isEmpty()){
            for(int i=0; i<exchanges.size(); i++){
                TWComboItem item = exchanges.get(i);

                if( !(ExchangeStore.isValidIinformationType(item.getId(),Meta.IT_CashFlow)) ){
                    exchanges.remove(i);
                }
            }


        }

    }


    private void applySavedSettings() {
        try {

            String exgID = g_oSetting.getProperty(selectedExchangeID);
            String marketID = g_oSetting.getProperty(selectedMarketID);
            if (exgID != null) {
                 boolean isExchangeSet = false;
                Symbols symbolsObject;
                populateMarketCombo(exgID);
                if((marketID!=null)&&(!marketID.equals(""))){
                  symbolsObject = DataStore.getSharedInstance().getSymbolsObject(exgID, marketID);
                   for (int i = 0; i < markets.size(); i++) {
                    TWComboItem item = markets.get(i);
                    if (item.getId().equals(marketID)) {
                        marketCombo.setSelectedIndex(i);
                        break;
                    }
                }
                }else{
                  symbolsObject = DataStore.getSharedInstance().getSymbolsObject(exgID, null);
                }
                volumeWatcherSymbols.setSymbols(symbolsObject.getSymbols());
                for (int i = 0; i < exchanges.size(); i++) {
                    TWComboItem item = exchanges.get(i);
                    if (item.getId().equals(exgID)) {
                        isExchangeSet = true;
                        exchangeCombo.setSelectedIndex(i);
                        break;
                    }
                }
                if(!isExchangeSet){
                    exchangeCombo.setSelectedIndex(0);
                }
            } else {
                exchangeCombo.setSelectedIndex(0);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void actionPerformed(ActionEvent e) {
        try {
            if (e.getSource().equals(exchangeCombo)) {
                String id = ((TWComboItem) exchangeCombo.getSelectedItem()).getId();
                selectedExchange = id;
                g_oSetting.putProperty(selectedExchangeID, selectedExchange);
                Exchange exg=ExchangeStore.getSharedInstance().getExchange(selectedExchange);
                if((exg.isUserSubMarketBreakdown()) && (exg.getSubMarketCount() > 0)){
                    populateMarketCombo(selectedExchange);
                    marketCombo.setEnabled(true);
                    String subMarketID= null;
                    try {
                        subMarketID = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                    } catch (Exception e1) {
                        marketCombo.setSelectedIndex(0);
                        subMarketID = ((TWComboItem) marketCombo.getSelectedItem()).getId();
                    }
                    g_oSetting.putProperty(selectedMarketID, subMarketID);
                    volumeWatcherSymbols.setSymbols(DataStore.getSharedInstance().getSymbolsObject(selectedExchange, subMarketID).getSymbols());
                }else{
                    volumeWatcherSymbols.setSymbols(DataStore.getSharedInstance().getSymbolsObject(selectedExchange, null).getSymbols());
                    marketCombo.setEnabled(false);
                }
                volumeTable.getTable().updateUI();
            }else if(e.getSource().equals(marketCombo)){
                String id = ((TWComboItem) exchangeCombo.getSelectedItem()).getId();
                selectedExchange = id;
                String subMarketID=((TWComboItem) marketCombo.getSelectedItem()).getId();
                g_oSetting.putProperty(selectedMarketID, subMarketID);
                volumeWatcherSymbols.setSymbols(DataStore.getSharedInstance().getSymbolsObject(selectedExchange, subMarketID).getSymbols());
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void showCashFlow(boolean isVisible) {
        try {
            if (volumeTable.isIcon()){
                volumeTable.setIcon(false);
            } else {
        volumeTable.setVisible(isVisible);
    }
            volumeTable.setSelected(true);
            if (isVisible){
                SharedMethods.updateGoogleAnalytics("ChashFlowView");
            }
        } catch (PropertyVetoException e) {
        }
    }

    public void exchangeAdded(Exchange exchange) {
    }

    public void exchangeRemoved(Exchange exchange) {
    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public void exchangeDowngraded(Exchange exchange) {
    }

    public void exchangesAdded(boolean offlineMode) {
        populateExchangesCombo();
    }

    public void exchangesLoaded() {
    }

    public void exchangeMustInitialize(Exchange exchange,long newDate, long oldDate) {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
    }

    public void exchangeInformationTypesChanged() {
    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void cellSelectionCell(){

        ((DraggableTable) volumeTable.getTable()).setCellSelectionEnabled(true);
        ((DraggableTable) volumeTable.getTable()).setRowSelectionAllowed(true);
        ((DraggableTable) volumeTable.getTable()).setColumnSelectionAllowed(true);

    }

    public void  cellSelectionRow(){

        ((DraggableTable) volumeTable.getTable()).setCellSelectionEnabled(true);
        ((DraggableTable) volumeTable.getTable()).setRowSelectionAllowed(true);
        ((DraggableTable) volumeTable.getTable()).setColumnSelectionAllowed(false);

    }

    private void filterSymbols(){

    }
}