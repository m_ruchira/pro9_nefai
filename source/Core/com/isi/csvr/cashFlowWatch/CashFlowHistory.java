package com.isi.csvr.cashFlowWatch;

import com.isi.csvr.ShowMessage;
import com.isi.csvr.downloader.CFHistoryDownloadManager;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;

import java.beans.PropertyVetoException;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Sep 18, 2008
 * Time: 10:18:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class CashFlowHistory {
    private static CashFlowHistory self;

    private CashFlowHistorySummaryUI summary;
    private boolean summaryFileDl;
    private boolean detailFileDl;
    private String selexchange = "";
    private String selDateRange = "";
    private String selSkey = "";

    private int comboId = 0;

    private CashFlowHistory() {
        CashFlowHistoryDataStore.getSharedInstance();
    }

    public static CashFlowHistory getSharedInstance() {
        if (self == null) {
            self = new CashFlowHistory();
            return self;
        } else {
            return self;
        }
    }

    public boolean isSummaryFileDl() {
        return summaryFileDl;
    }

    public void setSummaryFileDl(boolean summaryFileDl) {
        this.summaryFileDl = summaryFileDl;
    }

    public boolean isDetailFileDl() {
        return detailFileDl;
    }

    public void setDetailFileDl(boolean detailFileDl) {
        this.detailFileDl = detailFileDl;
    }

    public void loadSummaryFile(String exchange, String period, boolean erShow) {
        selexchange = exchange;
        selDateRange = period;
        ArrayList<CashFlowSummaryRecord> list = CashFlowHistoryDataStore.getSharedInstance().getSummaryList(exchange, period);
        if (list != null && !list.isEmpty()) {
            CashFlowHistorySummaryUI.getSharedInstance().setDataLoading(false);
            CashFlowHistorySummaryUI.getSharedInstance().loadDataToModel(list);
        } else {
            if (Settings.isConnected()) {
                if (!CashFlowHistoryDataStore.getSharedInstance().checkIfSummaryFileExsists(exchange)) {  // to stop infinite DL loop if .csv is incompatible
                    CFHistoryDownloadManager.getSharedInstance().addCFHistorySummaryRequest(exchange);
                    CashFlowHistorySummaryUI.getSharedInstance().setDataLoading(true);
                    summaryFileDl = true;
                } else {
                    CashFlowHistorySummaryUI.getSharedInstance().setDataLoading(false);
                }
            } else {
                CashFlowHistorySummaryUI.getSharedInstance().setDataLoading(false);
                if (erShow)
                    new ShowMessage(Language.getString("CF_DISCONNECTED"), "I");
            }
        }
    }

    public void loadDetailFile(String exchange, String sKey) {

        ArrayList<CashFlowHistoryRecord> list = CashFlowHistoryDataStore.getSharedInstance().getDetailedSummaryList(selexchange, sKey);
        if (list != null && !list.isEmpty()) {
            selSkey = sKey;
            CashFlowHistoryDetailUI.getSharedInstance().setDataLoading(false);
            CashFlowHistoryDetailUI.getSharedInstance().loadDataToModel(list);
            // CFHistoryDownloadManager.getSharedInstance().

        } else {
            if (Settings.isConnected()) {
                boolean valid = CFHistoryDownloadManager.getSharedInstance().addCFHistoryDetailRequest(exchange, sKey);
                if (valid) {
                    selSkey = sKey;
                    CashFlowHistoryDetailUI.getSharedInstance().clearStore();
                    CashFlowHistoryDetailUI.getSharedInstance().setDataLoading(true);
                } else {
                    new ShowMessage(Language.getString("CASHFLOWHISTORY_DETAIL_LOAD_ERROR"), "I");
                }
            } else {
                CashFlowHistoryDetailUI.getSharedInstance().setDataLoading(false);
                CashFlowHistoryDetailUI.getSharedInstance().setVisible(false);
                CashFlowHistoryDetailUI.getSharedInstance().finalizeWindow();
                new ShowMessage(Language.getString("CF_DISCONNECTED"), "I");

            }
        }

    }

    public void SummaryFileLoadComplete() {

        loadSummaryFile(selexchange, selDateRange, true);
    }

    public void detailFileLoadComplete() {
        loadDetailFile(selexchange, selSkey);
    }

    public void showDetailWondow(String exchange, String sKey) {
        if (!CashFlowHistoryDetailUI.getSharedInstance().isVisible()) {
            CashFlowHistoryDetailUI.getSharedInstance().showwidow();
        }
        CashFlowHistoryDetailUI.getSharedInstance().adjustTitle(sKey);
        CashFlowHistoryDetailUI.getSharedInstance().setDataLoading(true);
        loadDetailFile(exchange, sKey);
        CashFlowHistoryDetailUI.getSharedInstance().setGraphBasesymbol(sKey);
        // CashFlowHistoryDetailUI.getSharedInstance().setVisible(true);
        try {
            CashFlowHistoryDetailUI.getSharedInstance().setSelected(true);
        } catch (PropertyVetoException e) {

        }
    }

    public void cancellDetailDownloads() {
        CFHistoryDownloadManager.getSharedInstance().cancelDetailDownloads();
    }

    public void cancellSummaryDownloads() {
        CFHistoryDownloadManager.getSharedInstance().cancelSummaryDownloads();
    }

    public void showSummaryWindow(boolean isViasble) {
        CashFlowHistorySummaryUI.getSharedInstance().sortTable();
        CashFlowHistorySummaryUI.getSharedInstance().showWindow(true);


    }

    public void showSummaryFromWSP() {
        CashFlowHistorySummaryUI.getSharedInstance().setLoadedFromWsp(true);
        CashFlowHistorySummaryUI.getSharedInstance().showWindow(true);

    }

    public void stopSerchIconOnThreadInturrupt() {
        try {
            CashFlowHistoryDetailUI.getSharedInstance().setDataLoading(false);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            CashFlowHistorySummaryUI.getSharedInstance().setDataLoading(false);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

}
