package com.isi.csvr.ie;

import com.isi.csvr.Client;
import com.isi.csvr.WorkInProgressIndicator;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWControl;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ie.WebBrowser;
import com.jniwrapper.win32.ie.event.DefaultWebBrowserEventsHandler;
import com.jniwrapper.win32.ie.event.NavigationEventListener;
import com.jniwrapper.win32.ie.event.NewWindowEventHandlerExt;
import com.jniwrapper.win32.ie.event.BrowserDialogEventHandler;
import com.jniwrapper.win32.ui.MessageBox;
import com.jniwrapper.win32.ui.Wnd;


import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;

public class IEBrowser
        extends InternalFrame
        implements NavigationEventListener, Themeable, NewWindowEventHandlerExt {

    private JProgressBar progress;
    private final Browser wb;
    private boolean completed;
    public static boolean connect = false;
    private boolean isLoginWindow = false;
    private boolean isRedirectedFromLogin = false;
    private WorkInProgressIndicator indicator = null;
    private WorkInProgressIndicator indicator2 = null;
    private Thread indicatorThread = null;
    private Thread indicatorThread2 = null;
    private boolean isFirstTIme = true;

    public IEBrowser() {
        progress = new JProgressBar();
        this.setLayout(new BorderLayout());
        wb = new Browser();
        wb.addNavigationListener(this);
        wb.setDialogEventHandler(new BrowserDialogEventHandler() {
            @Override
            public int showDialog(Wnd wnd, String title, String text, int type) {
                if ("JExplorer".equals(title)) {
                    return MessageBox.OK;
                }
                return super.showDialog(wnd, title, text, type);
            }
        });
        wb.setEventHandler(new DefaultWebBrowserEventsHandler() {

     public boolean beforeNavigate(
             WebBrowser webBrowser,
             String url,
             String targetFrameName,
             String postData,
             String headers) {
         System.out.println("url = " + url);
         System.out.println("postData = " + postData);
         System.out.println("headers = " + headers);
         if(url.contains("ctl00$SignOutLinkButton")){
             setVisible(false);
             Client.getInstance().getMenus().getWebTrade().setEnabled(false);
             Client.getInstance().getMenus().unlockTradeServerConnection();
             Dimension dSize = new Dimension(300, 340);
             wb.setPreferredSize(dSize);
             wb.setSize(dSize);
             setAutoscrolls(false);
             setResizable(false);
             setMaximizable(false);
             BrowserManager man = BrowserManager.getInstance();
             IEBrowser br = man.getBrowser("LOGIN3");
             GUISettings.setLocationRelativeTo(br, br.getDesktopPane());
             pack();
             return true;
         } else if (url.contains(TWControl.getRequestURL("LOGIN_URL")) && isRedirectedFromLogin){
             isRedirectedFromLogin = false;
             isFirstTIme = false;
             setVisible(false);
             indicator = new WorkInProgressIndicator(WorkInProgressIndicator.AUTHENTICATION);
             indicatorThread = new Thread("Client-systemExit_ActionPerformed 1") {
                 public void run() {
                     indicator.setVisible(true);

                 }
             };
             indicatorThread.start();
         }  else if (url.contains(TWControl.getRequestURL("LOGIN_URL")) && !isFirstTIme) {
             setVisible(false);
             indicator2 = new WorkInProgressIndicator(WorkInProgressIndicator.INITIALIZIG);
             indicatorThread2 = new Thread("Client-systemExit_ActionPerformed 1") {
                 public void run() {
                     indicator2.setVisible(true);

                 }
             };
             indicatorThread2.start();
         }
         return false;
     }
});
        this.add(wb, BorderLayout.CENTER);
        progress.setMaximum(1000);
        progress.setMinimum(0);
        progress.setForeground(Theme.getColor("BLACK_COLOR"));
        progress.setPreferredSize(new Dimension(1, 10));
        progress.setBorder(BorderFactory.createEmptyBorder());
        this.setSize(new Dimension(1, 1));
        wb.setNewWindowHandler(this);
        this.add(progress, BorderLayout.SOUTH);
        Theme.registerComponent(this);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setVisible(false);
        setResizable(true);
        setIconifiable(true);
        setClosable(true);
        setMaximizable(true);
        setLayer(10);
        hideTitleBarMenu();
        Client.getInstance().getDesktop().add(this);
        GUISettings.applyOrientation(this);
        wb.setSilent(true);
    }

    public void setRedirectedFromLogin(boolean status){
        isRedirectedFromLogin = status;
    }

    public boolean isLoginWindow() {
        return isLoginWindow;
    }

    public void setLoginWindow(boolean loginWindow) {
        isLoginWindow = loginWindow;
    }

    public void setBrowserSize(Dimension size) {
        wb.setPreferredSize(size);
    }

    public void applyTheme() {
        progress.setForeground(Theme.getColor("BLACK_COLOR"));
    }

    public void documentCompleted(WebBrowser webBrowser, String s) {

    }

    public void downloadBegin() {

    }

    public void downloadCompleted() {
    }

    public void onRefresh(){        
    }

    public void entireDocumentCompleted(WebBrowser webBrowser, String s) {

        String path = null;
        if (s.contains(TWControl.getRequestURL("LOGIN_URL"))||s.contains(TWControl.getRequestURL("LOGIN_URL_TWO"))) {
            isLoginWindow=true;
            String params = TWControl.getPostParams();

            Client.getInstance().getMenus().getWebTrade().setEnabled(false);
            String a = webBrowser.getContent();
            String b = a.substring(a.indexOf(TWControl.getRequestURL("PWD_VALIDATOR")));
            String errorMessage = b.substring(b.indexOf(TWControl.getRequestURL("ERROR_MSG_START")) + 14);
            errorMessage = errorMessage.substring(0, errorMessage.toLowerCase().indexOf(TWControl.getRequestURL("ERROR_MSG_END")));
            String status1 = errorMessage.substring(errorMessage.indexOf(TWControl.getRequestURL("ERROR_MSG_STATUS_START")) + 9);
            String status = status1.substring(0, status1.indexOf(TWControl.getRequestURL("ERROR_MSG_STATUS_END")));
            BrowserManager man = BrowserManager.getInstance();
            if (status.trim().equals(TWControl.getRequestURL("ERROR_MSG_STATUS_FIRST_TIME"))) {
                if (!Language.isLTR()) {
                    path = (System.getProperties().get("user.dir") + "\\Templates\\Login\\AR\\login.html");
                } else {
                    path = (System.getProperties().get("user.dir") + "\\Templates\\Login\\EN\\login.html");
                }
                man.navigate("LOGIN3", path, Language.getString("TRADE_LOGIN"), null, params, false);
            } else {
                if (!Language.isLTR()) {
                    path = (System.getProperties().get("user.dir") + "\\Templates\\Login_Error\\AR\\login.html");
                } else {
                    path = (System.getProperties().get("user.dir") + "\\Templates\\Login_Error\\EN\\login.html");
                }
                man.navigate("LOGIN3", path, Language.getString("TRADE_LOGIN"), null, params, false);
            }
            Dimension dSize = new Dimension(300, 340);
            wb.setPreferredSize(dSize);
            wb.setSize(dSize);
            setAutoscrolls(false);
            setResizable(false);
            setMaximizable(false);
            this.setLocationRelativeTo(Client.getInstance().getDesktop());
            this.setPreferredSize(dSize);
            this.setSize(dSize);
            pack();
        } else if (s.contains(TWControl.getRequestURL("NEW_ORDER"))) {
            if (indicatorThread != null) {
                indicatorThread.interrupt();
            }
            if (indicator != null) {
                indicator.dispose();
            }
            if (indicatorThread2 != null) {
                indicatorThread2.interrupt();
            }
            if (indicator2 != null) {
                indicator2.dispose();
            }
            isLoginWindow = false;
            try {
                Client.getInstance().getMenus().getWebTrade().setEnabled(true);
                BrowserManager man = BrowserManager.getInstance();
                IEBrowser br = man.getBrowser("LOGIN3");
                br.setMaximum(true);
                setResizable(true);
                setMaximizable(true);
                Dimension dSize = new Dimension(800, 500);
                wb.setPreferredSize(dSize);
                wb.setSize(dSize);
                wb.setLocation(null);
                this.setPreferredSize(dSize);
                this.setSize(dSize);
                pack();
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else if(s.contains("login.html")) {
            System.out.println("login page loaded");
            Dimension dSize = new Dimension(300, 340);
            wb.setPreferredSize(dSize);
            wb.setSize(dSize);
            setAutoscrolls(false);
            setResizable(false);
            setMaximizable(false);
            this.setLocationRelativeTo(Client.getInstance().getDesktop());
            this.setPreferredSize(dSize);
            this.setSize(dSize);
            pack();
            new Thread(" IE Visible Thread"){
                @Override
                public void run() {
                    try {
                        System.out.println("login thread started");
                        Thread.sleep(2000);
                    } catch (InterruptedException e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    if (indicatorThread != null) {
                        indicatorThread.interrupt();
                    }
                    if (indicator != null) {
                        indicator.dispose();
                    }
                    if (indicatorThread2 != null) {
                        indicatorThread2.interrupt();
                    }
                    if (indicator2 != null) {
                        indicator2.dispose();
                    }
                    isRedirectedFromLogin = true;
                    wb.setVisible(true);
                    BrowserManager.getInstance().setVisible("LOGIN3", true);
                    isFirstTIme = false;
                    System.out.println("login thread ended");
                }
            }.start();
        }

    }

    public void navigationCompleted(WebBrowser webBrowser, String s) {
    }

    public void progressChanged(int /*[in]*/ _Progress, int /*[in]*/ _ProgressMax) {
        progress.setMaximum(_ProgressMax);
        if (_Progress > 0) {
            progress.setValue(_Progress);
        } else {
            progress.setValue(0);
        }
    }

    public synchronized void showLoading(String title) {
        setTitle(title);
        wb.navigate(System.getProperties().get("user.dir")
                + "\\templates\\loading_" + Language.getLanguageTag() + ".htm");
        wb.refresh();
    }

    public synchronized void setURL(String url, String title) {
        setURL(url, title, true);
    }

    public synchronized void setURL(String url, String title, boolean visibility) {
        setTitle(title);
        showLoading(title);
        completed = false;
        setVisible(visibility && !isLoginWindow);
        wb.navigate(url);

    }

    public synchronized void setURL(String url, String postData, String title) {
        setURL(url, postData, title, true);
    }

    public synchronized void setURL(String url, String postData, String title, boolean visibility) {

        setTitle(title);
        showLoading(title);
        completed = false;
        setVisible(visibility && !isLoginWindow);
        wb.navigate(url, postData);
    }

    private synchronized void idle() {
        try {
            setTitle("");
            wb.navigate("about:blank");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void callURL(String url) {
        System.out.println(url);
        try {
            wb.navigate(url);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public boolean isCompleted() {
        return completed;
    }

    @Override
    public void internalFrameClosing(InternalFrameEvent e) {
        try {
            wb.stop();
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        idle();
    }

    public NewWindowAction newWindow(String s, String s1, NewWindowManagerFlags newWindowManagerFlags) {
        return null;
    }

    public NewWindowAction newWindow() {
        return null;
    }
}
