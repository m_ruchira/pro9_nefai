package com.isi.csvr.ie;

import com.isi.csvr.Client;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWControl;
import com.jniwrapper.win32.ie.proxy.ProxyConfiguration;
import com.jniwrapper.win32.ie.Browsers;

import java.util.Hashtable;
import java.awt.*;
import java.beans.PropertyVetoException;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 7, 2009
 * Time: 2:16:01 PM
 * <p/>
 * Manages Internet Explorer browser instances.
 */

public class BrowserManager {
    private static BrowserManager self;
    private Hashtable<String, IEBrowser> browsers;
    private String proxyIP;
    private int proxyPort;
    private boolean proxyChecked;

    /**
     * @return shared singleton instance
     */
    public static synchronized BrowserManager getInstance() {
        if (self == null) {
            self = new BrowserManager();
        }
        return self;
    }

    /**
     * private contructor due to singleton usage
     */
    private BrowserManager() {
        browsers = new Hashtable<String, IEBrowser>();
        checkBrowserProxy();
    }

    /**
     * Navigate URL
     *
     * @param id    of the browser
     * @param url   to be navigated
     * @param title to be dinspayed on top
     */
    public void navigate(String id, String url, String title) {
        navigate(id, url, title, null, true);
    }

    public void navigate(String id, String url, String title, boolean visibility) {
        navigate(id, url, title, null, visibility);
    }

    /**
     * Navigate URL
     *
     * @param id    of the browser
     * @param url   to be navigated
     * @param title to be dinspayed on top
     * @param size  of the browser window
     */
    public void navigate(String id, String url, String title, String size, boolean visibility) {
        if ((id == null) || (id.trim().equals(""))) {
            id = "NULL";
        }

        IEBrowser browser = browsers.get(id.trim());
        if (browser == null) {
            browser = new IEBrowser();
            browsers.put(id, browser);
            try {
                String[] sizes = size.split(",");
                Dimension dSize = new Dimension(Integer.parseInt(sizes[0]), Integer.parseInt(sizes[1]));
                browser.setBrowserSize(dSize);
            } catch (Exception e) { // incorrect size or size not specified
                Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
                if (screen.getWidth() > 850) {
//                    browser.setBrowserSize(new Dimension(810, 500));
                    browser.setBrowserSize(new Dimension(300, 340)); 
                } else {
                    browser.setBrowserSize(new Dimension(760, 500));
                }
            }
            
            browser.pack();
            GUISettings.applyOrientation(browser);
            GUISettings.setLocationRelativeTo(browser, browser.getDesktopPane());
        }
        browser.setRedirectedFromLogin(false);
        browser.setURL(url, title, visibility);
        if (browser.isIcon()) {
            try {
                browser.setIcon(false);
            } catch (PropertyVetoException e) {
            }
        }
        System.out.println("Browser123 "+ id + " set visible = "+ visibility);
        browser.setVisible(visibility);
    }

    public IEBrowser getBrowser(String id){
        if ((id == null) || (id.trim().equals(""))) {
            id = "NULL";
        }
        return browsers.get(id.trim());
    }

    public void removeBrowser(String id){
        browsers.remove(id);
        if(browsers.contains(id)){
            browsers.remove(id);
        }
    }

    public void navigate(String id, String url, String title, String size, String postData) {
        navigate(id, url, title, size, postData, true);
    }

    public void navigate(String id, String url, String title, String size, String postData, boolean visibility) {
        if ((id == null) || (id.trim().equals(""))) {
            id = "NULL";
        }

        IEBrowser browser = browsers.get(id.trim());
        if (browser == null) {
            browser = new IEBrowser();
            browsers.put(id, browser);
            try {
                String[] sizes = size.split(",");
                Dimension dSize = new Dimension(Integer.parseInt(sizes[0]), Integer.parseInt(sizes[1]));
                browser.setBrowserSize(dSize);
            } catch (Exception e) { // incorrect size or size not specified
                Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
                if (screen.getWidth() > 850) {
                    browser.setBrowserSize(new Dimension(300, 340));
                } else {
                    browser.setBrowserSize(new Dimension(760, 500));
                }
            }

            browser.pack();
            GUISettings.setLocationRelativeTo(browser, browser.getDesktopPane());
        }
        browser.setRedirectedFromLogin(false);
        browser.setURL(url,postData, title, visibility);
        if (browser.isIcon()) {
            try {
                browser.setIcon(false);
            } catch (PropertyVetoException e) {
            }
        }
        System.out.println("Browser "+ id + " set visible = "+ visibility);
        browser.setVisible(visibility);
    }

    public void setVisible(String id, boolean visibility){
        IEBrowser browser = getBrowser(id);
        if (browser != null) {
            if (browser.isIcon()) {
                try {
                    browser.setIcon(false);
                } catch (PropertyVetoException e) {
                }
            }
            System.out.println("Browser Set Visible "+ id + " set visible = "+ visibility);
            browser.setVisible(visibility);
        }
    }

    public void showLoading(final String id, final String title, String size) {
        IEBrowser browser = browsers.get(id.trim());
        if (browser == null) {
            browser = new IEBrowser();
            browsers.put(id, browser);
            try {
                String[] sizes = size.split(",");
                Dimension dSize = new Dimension(Integer.parseInt(sizes[0]), Integer.parseInt(sizes[1]));
                browser.setBrowserSize(dSize);
            } catch (Exception e) { // incorrect size or size not specified
                Dimension screen = Toolkit.getDefaultToolkit().getScreenSize();
                if (screen.getWidth() > 850) {
                    browser.setBrowserSize(new Dimension(810, 500));
                } else {
                    browser.setBrowserSize(new Dimension(760, 500));
                }
            }
            browser.pack();
            GUISettings.setLocationRelativeTo(browser, browser.getDesktopPane());
        }
        browser.setRedirectedFromLogin(false);
        browser.showLoading(title);
        System.out.println("Browser "+ id + " set visible = "+ true);
        browser.setVisible(true);
        browser.updateUI();
    }

    /**
     * Check if there is a proxy set in Internet Explorer
     */
    private synchronized void checkBrowserProxy(){
        try {
            if (!proxyChecked){
                // extract global proxy settings
                proxyIP = null;
                ProxyConfiguration globalProxySettings = Browsers.getProxy();

                int type =  globalProxySettings.getConnectionType();
                if (type == ProxyConfiguration.ConnectionType.PROXY) { // proxy check box enabled
                    String proxy = globalProxySettings.getProxy(ProxyConfiguration.ServerType.HTTP);
                    if ((proxy != null) && (!proxy.trim().equals(""))){
                        proxyIP = proxy.split(":")[0];
                        proxyPort = Integer.parseInt(proxy.split(":")[1]);
                    }
                } else { // No proxy
                    proxyIP = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            proxyIP = null;
        } finally{
            proxyChecked = true;
        }
    }

    public String getProxyIP() {
        return proxyIP;
    }

    public int getProxyPort() {
        return proxyPort;
    }

    public boolean isProxyAvailable(){
        return proxyIP != null;
    }
}
