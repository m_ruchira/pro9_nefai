package com.isi.csvr.sectoroverview;

import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWColumnSettings;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.table.Table;
import com.isi.csvr.SortButtonRenderer;
import com.isi.csvr.Client;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.util.ColumnGroup;
import com.isi.csvr.util.GroupableTableHeader;

import javax.swing.event.InternalFrameAdapter;
import javax.swing.event.InternalFrameEvent;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Collection;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Nov 14, 2008
 * Time: 12:27:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class SectorOverviewUI extends InternalFrame implements Themeable {
    private ViewSetting viewSettings;
    private String typeDescription = null;
    private String exchange;
    private Exchange exObject;

    private Hashtable<String, SectorOverviewObject> splitStoreFilled = new Hashtable<String, SectorOverviewObject>();
    private ArrayList<SectorOverviewObject> dataArray = new ArrayList();
    //    private ViewSetting tableSetting;
    private Table table;
    private SectorOverviewModal model;
    private boolean isUISelected = false;

    public static SectorOverviewUpdator feeders;
    SortButtonRenderer renderer;


    public SectorOverviewUI(ViewSetting oViewSettings) {
        viewSettings = oViewSettings;
        viewSettings.setParent(this);

        getContentPane().setLayout(new BorderLayout());
        getDataTable();
        this.setTitle(Language.getString("SECTOR_OVERVIEW"));
        this.setTable(table);
        getContentPane().add(table, BorderLayout.CENTER);
        this.setClosable(true);
        this.setIconifiable(true);
        this.setResizable(true);
        this.setMaximizable(true);
        //----------
        this.setColumnResizeable(true);
        this.setDetachable(true);
        this.setPrintable(true);
        Client.getInstance().getDesktop().add(this);
        this.setLayer(GUISettings.TOP_LAYER);
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
        /* this.addInternalFrameListener(new InternalFrameAdapter() {
            public void internalFrameClosing(InternalFrameEvent e) {
                SectorOverviewUpdator.getSharedInstance().sectorExchangesRemover(getExObject());
            }
        });*/

    }

    public void setViewSettings(ViewSetting oViewSettings) {
        viewSettings = oViewSettings;
        viewSettings.setParent(this);
    }

    public void setTypeDescription(String typeDescription) {
        this.typeDescription = typeDescription;
    }

    public String getTitle() {
        try {
            String currentyTitle = "";

            if (typeDescription != null) {
                currentyTitle = getExchange();
                return typeDescription;
            } else
                return title + currentyTitle;

        } catch (Exception e) {
            return "";
        }
    }

    public String gettingTitle() {
        return typeDescription;
    }

    private Table getDataTable() {
        table = new Table();
        table.addMouseListener(this);
        table.setSortingEnabled();
        model = new SectorOverviewModal(dataArray);
        model.setViewSettings(viewSettings);

        String[] columns = viewSettings.getColumnHeadings().clone();// need to clone headings since the groupable
        viewSettings.setColumnHeadings(columns);
        table.setSortingEnabled();
        table.setGroupableHeaderModel(model);
        model.setTable(table, new SectorOverviewRenderer());
        model.applyColumnSettings();
        renderer = new SortButtonRenderer(true);

        TableColumnModel cm = table.getTable().getColumnModel();
        ColumnGroup cin = new ColumnGroup(Language.getString("SECTOR_DETAILS"));
        GUISettings.setColumnSettings(viewSettings, TWColumnSettings.getItem("SECTOR_OVERVIEW_COLS"));
        cin.add(cm.getColumn(0));
        cin.add(cm.getColumn(1));
        cin.add(cm.getColumn(2));
        cin.add(cm.getColumn(3));
        cin.add(cm.getColumn(4));
        cin.add(cm.getColumn(5));
        cin.add(cm.getColumn(6));
        viewSettings.getColumnHeadings()[0] = Language.getString("SECTOR_DETAILS") + " - " + viewSettings.getColumnHeadings()[0];
        viewSettings.getColumnHeadings()[1] = Language.getString("SECTOR_DETAILS") + " - " + viewSettings.getColumnHeadings()[1];
        viewSettings.getColumnHeadings()[2] = Language.getString("SECTOR_DETAILS") + " - " + viewSettings.getColumnHeadings()[2];
        viewSettings.getColumnHeadings()[3] = Language.getString("SECTOR_DETAILS") + " - " + viewSettings.getColumnHeadings()[3];
        viewSettings.getColumnHeadings()[4] = Language.getString("SECTOR_DETAILS") + " - " + viewSettings.getColumnHeadings()[4];
        viewSettings.getColumnHeadings()[5] = Language.getString("SECTOR_DETAILS") + " - " + viewSettings.getColumnHeadings()[5];
        viewSettings.getColumnHeadings()[6] = Language.getString("SECTOR_DETAILS") + " - " + viewSettings.getColumnHeadings()[6];


        ColumnGroup cout = new ColumnGroup(Language.getString("INDEX_DETAILS"));

        cout.add(cm.getColumn(7));
        cout.add(cm.getColumn(8));
        cout.add(cm.getColumn(9));
        cout.add(cm.getColumn(10));
        cout.add(cm.getColumn(11));
        cout.add(cm.getColumn(12));
        viewSettings.getColumnHeadings()[7] = Language.getString("INDEX_DETAILS") + " - " + viewSettings.getColumnHeadings()[7];
        viewSettings.getColumnHeadings()[8] = Language.getString("INDEX_DETAILS") + " - " + viewSettings.getColumnHeadings()[8];
        viewSettings.getColumnHeadings()[9] = Language.getString("INDEX_DETAILS") + " - " + viewSettings.getColumnHeadings()[9];
        viewSettings.getColumnHeadings()[10] = Language.getString("INDEX_DETAILS") + " - " + viewSettings.getColumnHeadings()[10];
        viewSettings.getColumnHeadings()[11] = Language.getString("INDEX_DETAILS") + " - " + viewSettings.getColumnHeadings()[11];
        viewSettings.getColumnHeadings()[12] = Language.getString("INDEX_DETAILS") + " - " + viewSettings.getColumnHeadings()[12];

        ColumnGroup cflow = new ColumnGroup(Language.getString("CASHFLOW_DETAILS"));
        cflow.add(cm.getColumn(13));
        cflow.add(cm.getColumn(14));
        cflow.add(cm.getColumn(15));
        viewSettings.getColumnHeadings()[13] = Language.getString("CASHFLOW_DETAILS") + " - " + viewSettings.getColumnHeadings()[13];
        viewSettings.getColumnHeadings()[14] = Language.getString("CASHFLOW_DETAILS") + " - " + viewSettings.getColumnHeadings()[14];
        viewSettings.getColumnHeadings()[15] = Language.getString("CASHFLOW_DETAILS") + " - " + viewSettings.getColumnHeadings()[15];


        GroupableTableHeader header = (GroupableTableHeader) (table.getTable().getTableHeader());
        header.addGroup(cin);
        header.addGroup(cout);
        header.addGroup(cflow);

        TableColumnModel colmodel = table.getTable().getColumnModel();
        int n = model.getColumnCount();

        for (int i = 0; i < n; i++) {
            colmodel.getColumn(i).setHeaderRenderer(renderer);
        }
        table.createGroupableTable();


        table.getTable().getTableHeader().setDefaultRenderer(renderer);
        table.getTable().getTableHeader().updateUI();
        table.getTable().getTableHeader().repaint();
        table.getModel().updateGUI();
        model.setTable(table, new SectorOverviewRenderer());
        model.applySettings();
        this.setTable(table);
        table.updateGUI();
        table.getPopup().showLinkMenus();
        return table;
    }

    public boolean isUISelected() {
        return isUISelected;
    }

    public void setUISelected(boolean UISelected) {
        isUISelected = UISelected;
    }

    public Exchange getExObject() {
        return exObject;
    }

    public void setExObject(Exchange exObject) {
        this.exObject = exObject;
    }

    public String getExchange() {
        return exchange;
    }

    public void addData(SectorOverviewObject sob) {
        for (int i = 0; i < dataArray.size(); i++) {
            if (dataArray.get(i).getSectorID().equals(sob.getSectorID())) {
                dataArray.remove(i);
            }
        }
        dataArray.add(sob);
        Collections.sort(dataArray);
//        this.updateGUI();
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;

    }

    public String getTypeDescription() {
        return typeDescription;
    }


    public ArrayList<SectorOverviewObject> getDataArray() {
        return dataArray;
    }

    public void setDataArray(ArrayList<SectorOverviewObject> dataArray) {
        this.dataArray = dataArray;
    }

    public SectorOverviewModal getModel() {
        return model;
    }

    @Override
    public void setVisible(boolean visible) {
        super.setVisible(visible);
        if (this.isVisible()) {
            SectorOverviewUpdator.getSharedInstance().sectorExchangesGatherer(getExObject());

            if (!SectorOverviewUpdator.isWIndowActive) {
                SectorOverviewUpdator.isWIndowActive = true;
                Client.getInstance().startSectorGatherer();
            }
        }
    }


    private void setFont() {
        if (viewSettings.getFont() != null) {
            table.getTable().setFont(viewSettings.getFont());
        }

        if (viewSettings.getHeaderFont() != null) {
            table.getTable().getTableHeader().setFont(viewSettings.getHeaderFont());
            //if (table.isSortable()) {
            for (int i = 0; i < table.getTable().getColumnModel().getColumnCount(); i++) {
                SortButtonRenderer oRend = (SortButtonRenderer) table.getTable().getColumnModel().getColumn(i).getHeaderRenderer();
                if (oRend != null) {
                    oRend.setHeaderFont(viewSettings.getHeaderFont());
                    oRend.updateUI();
                }
                oRend = null;
            }
            table.getTable().getTableHeader().updateUI();
        }
        try {
            table.getTable().updateUI();
        } catch (Exception e) {
        }
    }


    @Override
    public void internalFrameClosing(InternalFrameEvent e) {
        SectorOverviewUpdator.getSharedInstance().sectorExchangesRemover(getExObject());
        Client.getInstance().stopSectorGatherer();
    }

    public void applyTheme() {
        renderer.applyTheme();
    }
}
