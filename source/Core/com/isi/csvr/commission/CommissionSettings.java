package com.isi.csvr.commission;

import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Settings;

import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.Properties;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 19, 2004
 * Time: 9:02:06 AM
 * To change this template use File | Settings | File Templates.
 */
public class CommissionSettings {
    private Properties prop;
    private double pctCommission = 0;
    private double minCommission = 0;
    private boolean autoCommissionAllowed = true;
    private static CommissionSettings self = null;

    public static synchronized CommissionSettings getSharedInstance() {
        if (self == null) {
            self = new CommissionSettings();
        }
        return self;
    }

    private CommissionSettings() {
        try {
            prop = new Properties();
            FileInputStream oIn = new FileInputStream(Settings.SYSTEM_PATH +"/cm.dll");
            prop.load(oIn);
            oIn.close();
            try {
                pctCommission = Double.parseDouble(prop.getProperty("PCT_COMM"));
            } catch (Exception e) {
                pctCommission = 0;
            }
            try {
                minCommission = Double.parseDouble(prop.getProperty("MIN_COMM"));
            } catch (Exception e) {
                minCommission = 0;
            }
            try {
                autoCommissionAllowed = SharedMethods.booleanValue(prop.getProperty("AUTO_COMM"), true);
            } catch (Exception e) {
                minCommission = 0;
            }
        }
        catch (Exception s_e) {
            prop = new Properties();
            pctCommission = .15D;
            minCommission = 15;
            autoCommissionAllowed = true;
        }
    }

    public void save() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.SYSTEM_PATH +"/cm.dll");
            prop.store(out, "Commission Settings");
            out.close();
        }
        catch (Exception s_e) {
        }
    }

    public double getMinCommission() {
        return minCommission;
    }

    public void setMinCommission(double minCommission) {
        this.minCommission = minCommission;
        prop.put("MIN_COMM", "" + minCommission);
    }

    public double getPctCommission() {
        return pctCommission;
    }

    public void setPctCommission(double pctCommission) {
        this.pctCommission = pctCommission;
        prop.put("PCT_COMM", "" + pctCommission);
    }

    public boolean isAutoCommissionAllowed() {
        return autoCommissionAllowed;
    }

    public void setAutoCommissionAllowed(boolean autoCommissionAllowed) {
        this.autoCommissionAllowed = autoCommissionAllowed;
        prop.put("AUTO_COMM", "" + autoCommissionAllowed);
    }

    public double getCommission(double ammount) {
        return Math.max(minCommission, (ammount * pctCommission) / (float) 100);
    }
}
