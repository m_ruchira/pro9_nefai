package com.isi.csvr.heatmap;

import com.isi.csvr.HeatInterface;

import java.awt.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author unascribed
 * @version 1.0
 */

public class HeatRecord {

    private String key;
    private String symbol;
    private double[] value;
    private Color color;
    private Color fontColor;

    public HeatRecord(String key, double[] value) {
        this.key = key;
        this.value = value;
        this.symbol = HeatInterface.getSymbolFromKey(key);
        this.color = Color.white;
        this.fontColor = Color.BLACK;
    }

    public String getKey() {
        return key;
    }

    public String getSymbol() {
        return symbol;
    }

    public double getValue(byte criteria) {
        if (criteria < value.length) {
            return value[criteria];
        } else {
            return value[0];
        }
    }

    public Color getColor() {
        return color;
    }

    public void setColor(Color c) {
        color = c;
    }

    public Color getFontColor() {
        return fontColor;
    }

    public void setFontColor(Color fc) {
        fontColor = fc;
    }
}