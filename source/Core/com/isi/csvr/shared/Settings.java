// Copyright (c) 2000 ISI
package com.isi.csvr.shared;

import com.isi.csvr.Mubasher;
import com.isi.csvr.PulseGenerator;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.win32.NativeMethods;
import com.dfn.mtr.mix.util.ConfigLoader;

import javax.swing.*;
import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.PrinterJob;
import java.io.*;
import java.math.BigInteger;
import java.text.SimpleDateFormat;
import java.util.*;

public class Settings {

    public static int SERVER_PORT = 9006;
    public static int TRADE_ROUTER_PORT = 8999;

    public static int DOWNLOAD_PORT = 9005;
    public static int SECONDARY_SERVER_PORT = 9006;
    public static int CONTENT_SERVER_PORT = 9005;

    public static int MARKET_OPEN = 2;
    public static int MARKET_CLOSE = 3;

    public static final int TICK_UP = 1;
    public static final int TICK_DOWN = -1;
    public static final int TICK_NOCHANGE = 0;
    public static final int TICK_NEGATIVE_EQUAL = -2;
    public static final int TICK_POSITIVE_EQUAL = 2;
    public static final int TICK_INVALID = 999;

    private static Properties prop;
    private static Properties propBulk;          // To keep userData
    private static boolean g_bRun;
    private static boolean g_bConnected = false;
    private static int g_iMarketStatus = -1;
    private static boolean g_bAuthented = false;
    private static short g_iHistoryCondition = -1;

    private static boolean showArabicNumbers = false;
    private static boolean showSummaryTicker = false;
    private static PageFormat g_PageFormat = null;

    private static String sessionID = "*";
    private static String userID = "";
    private static String singleSignOnID = "";

    public static String getTradeToken() {
        return tradeToken;
    }

    public static void setTradeToken(String tradeToken) {
        Settings.tradeToken = tradeToken;
    }

    private static String tradeToken = "";
    private static String ssoId = "";
    private static String institutionID = "";
    private static String brokerInstitutionID = "";
    private static boolean ssoTradingEnabled = false;
    private static String activeIP = null;
    public static String systemLocale;
    private static String twPath;
    private static int alertModes = 0;

    public static long NULL_DATE = (new Date()).getTime();

    // AUTO SCROLL PROPERTIES
    private static boolean combinedOrderBook = false;
    private static boolean popupAnnounceWindow = true;
    private static boolean tabIndex = true;
    private static boolean heatMapValueOnOff = true;
    private static boolean saveTimeNSales = false;
    private static int heatMapMode = 0;
    private static boolean autoUpdateDefaultMarkets;
    private static boolean isScrollingOn = false;
    public static int autoScrollInterval = 1;
    public static boolean symbolPanelOnly = false;
    private static boolean fullScreenMode = false;
    public static boolean detachOnWspLoad = false;

    //private static int[] allowedWindowTypes = null;

    public static String user = null;
    public static String password = null;

    public static String TW_VERSION = "Pro";
    public static String TW_VERSION_NUMBER = "9.10.000.000";
    public static String TW_VERSION_COMPLETE = "0";
    public static String CUSTOMIZATION_VERSION = "";
    public static final int HEART_BEAT = 20;  // in seconds


    public static final String TEMP_PATH = ".\\FlatDatabase\\Temp";
    public static String META_STOCK_APP_PATH;
    public static String TRADE_STATION_BAT_PATH;

    public static final String SYSTEM_PATH = Settings.getAbsolutepath() + "system";
    public static final String CONFIG_DATA_PATH = "config";
    public static final String CHART_DATA_PATH = Settings.getAbsolutepath() + "Charts";
    public static final String EXCHANGES_DATA_PATH = Settings.getAbsolutepath() + "Exchanges";
    public static final String UPDATE_PATH = "./AUTOUPDATE";
    public static final String HISTORY_FLAT_DB_PATH = Settings.getAbsolutepath() + "history";
    public static final String CURRENT_FLAT_DB_PATH = ".\\FlatDatabase\\Current";
    public static final String META_STOCK_DB_PATH = Settings.getAbsolutepath() + "MetaStock";
    public static final String TRADE_SATION_DB_PATH = Settings.getAbsolutepath() + "TradeStation";
    public static final String INTRADAY_OHLC_PATH = Settings.getAbsolutepath() + "OHLC\\";
    public static final String TEMPLATES_PATH="Templates";

    public static final int ANNOUNCEMENT_DB_SIZE = 50;

    public static final int BTW_ANNOUNCEMENT_DB_SIZE = 50;
    public static final int BTW_NEWS_DB_SIZE = 100;           //todo- previouslly it was 50 and change done in 27/11/2007
    public static final int SYMBOL_ANNOUNCEMENT_DB_SIZE = 5;

    public static final int COMMUNICATION_TYPE_TCP = 1;
    public static final int COMMUNICATION_TYPE_UDP = 0;
    public static int COMMUNICATION_TYPE = COMMUNICATION_TYPE_TCP;


    public static int SATELLITE_AUTH_PORT = 9001;
    public static int SATELLITE_CONTROL_PORT = 9000;
    public static int SATELLITE_DATA_PORT = 8001;
    public static int SATELLITE_HISTORY_PORT = 8002;
    public static String SATELLITE_DATA_IP = "127.0.0.1";
    public static String SATELLITE_CARD_IP = "127.0.0.1";

    public static String CHAT_SERVER_IP = "";
    public static int CHAT_SERVER_PORT = 0;

    private static boolean isUSConnectionSuccess = false;

    //    private static boolean portfolioSimulatorAutoCommission = false;
    public static boolean LAST_TIME_SAVE = false;

    private static boolean twUpdateAvailable = false;
    private static TimeZone currentZone;

    private static String datacenterPhone = "";
    private static String datacenterFax = "";
    private static String datacenterURL = "";

    private static String brokerPhone = "";
    private static String brokerFax = "";
    private static String brokerURL = "";
    private static String billingCode = "";
    private static String institutionCode = "";

    private static boolean dualScreenMode = false;
    private static boolean multiScreenEnvr = false;
    private static boolean leftAlignPopups;

    private static String lastDynamicIP = "333.333.333.333";

    private static boolean singleSignOnMode = false;
    private static byte fullQuotePriceType = 1;
    public static BigInteger MAX_BIGINT_VALUE = new BigInteger("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF", 16);
    private static transient boolean showwelcomescreen = true;
    private static transient boolean keepDefaultWSP = true;
    private static boolean isMktDepthStatusLoaded = false;
    public static int BROADCAST_MESS_WIDTH = 350;
    public static int BROADCAST_MESS_HEIGHT = 200;
    public static String RSA_SSO_CODE;
    private static String subscriptionURL;

//    public static String PROFILE_PATH = "";

    private static boolean consoleAllowed = false;
    public static ArrayList<String> autoUpdateEnableMarkets;
    public static ArrayList<String> autoUpdateEnableMarketsTS;
    private static Hashtable<String, String[]> g_oMarketCenters;
    private static Hashtable<String, String> customViewSettings = new Hashtable<String, String>();
    private static SmartProperties customViews;
    private static String globalContentProviderIP = null;
    private static String regionalContentProviderIP = null;
    private static boolean enableTradeTicker = false;

    private static int maximumSymbolCount;
    private static int maximumRQuoteCount;
    private static int maximumOPRACount;

    private static boolean enableCustomViewSettings = false;

    private static byte userPaymentType = 0;
    private static int connectionType = 0;
    public static boolean isBurstRequestMode = false;
    public static boolean isBurstRequestModeSecondary = false;

    public static boolean isOfflineMode = false;
    public static boolean isConnectedFirstTime = false;
    public static boolean couldNotApplyUpdate = false;
    public static boolean isOfflineDataLoaded = false;
    public static boolean isCurrentDay = true;
    public static boolean hasImporteddata = false;
    public static int tradingTimeoutMode = 0; //0=never,1=1 min,2=5 min,3=10 min,4=30 min
    public static String globalContentNewsIp = "";
    public static String reskFreeInt = null;
    public static String indexValuation = null;
    public static String stocksValuation = null;

    private static boolean isPutAllToSameLayer = false;
    private static boolean isKeepOrderWindowOpen = false;
    private static boolean isMultiInstanceMode = false;

    private static ArrayList<JInternalFrame> focusFlowList = new ArrayList<JInternalFrame>();

    private static int newsIndicatorExpiryTime = -1;
    //    private static String mwTradingUrl = "http://203.143.19.41:8080/pioneers/home.p?method=login&thm=sky";
    private static String mwTradingUrl = "";
    public static int afterMarketValuationMode = 0;

    private static boolean isGraphAdjusted = true;

    /**
     * Constructor
     */
    public static void init() {
//         ConfigLoader configloader = new ConfigLoader(); //mix cofiguration load
//         TRADE_ROUTER_PORT = com.dfn.mtr.mix.util.Settings.MIX_END_CLIENT_PORT;
        prop = new Properties();
        propBulk = new Properties();
        try {
            FileInputStream oIn = new FileInputStream(Settings.SYSTEM_PATH + "/st.dll");
            prop.load(oIn);
            FileInputStream oInBulk = new FileInputStream(Settings.SYSTEM_PATH + "/userdata.msf");
            propBulk.load(oInBulk);

            oIn.close();

            try {
                File file = new File(Settings.SYSTEM_PATH + "/customst.dll");
                if (file.exists()) {
                    oIn = new FileInputStream(file);
                    prop.load(oIn);
                    oIn.close();
                    file.deleteOnExit();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            propBulk.setProperty("DEFAULT_USER", SharedMethods.decrypt(propBulk.getProperty("DEFAULT_USER")));
            propBulk.setProperty("TRADE_USER", SharedMethods.decrypt(propBulk.getProperty("TRADE_USER")));
            propBulk.setProperty("PASSWORD", SharedMethods.decrypt(propBulk.getProperty("PASSWORD")));
            password = getItemFromBulk("PASSWORD").trim();
            user = getItemFromBulk("DEFAULT_USER").trim();
            try {
                MAX_BIGINT_VALUE = new BigInteger(getItem("MAX_BIGINT_VALUE"), 16);
            } catch (Exception e) {
                MAX_BIGINT_VALUE = new BigInteger(getItem("FFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFFF"), 16);
            }
            /*if ((user == null) || (user.equals("")) || (password == null) || (password.equals(""))) {
                isOfflineMode = true;
            } else {
                isOfflineMode = false;
            }*/
            loadWelcomeScrStatus();
            loadScrollingStatus();
            loadCustomViewSettings();
            loadNumaralStatus();
            loadAliasStatus();
            loadScreenStatus();
            loadTickerStatus();
            loadSatelliteSettings();
            loadTWVersion();
            loadCustomizationVersion();
            loadMktDepthStatus();
            loadAnnouncePopupStatus();
            loadProxySettings();
            loadWebProxySettings();
            loadWebProxySettingsForHTTPS();
            loadTabIndex();
            loadDefaulfWSPSettings();
//            loadMarketMapValueSowing();
//            loadHeatMapMode();
            loadAutoUpdateDefaultMarkets();
            loadAutoUpdateDefaultMarketsTS();
            loadLastDynamicIP();
            loadDualScreenProperties();
            loadBroadcastMessDialogSize();
            loadMarketCenter();
            loadSSOCode();
            loadConsoleStatus();
            loadBillingCode();
            loadSubscription();
            loadTimeNSalesStatus();
            loadMaximumSymbolCount();
            loadMaximumRQuoteCount();
            loadMaximumOPRACount();
            loadIndexPanelMode();
            loadTradeTickerMode();
            loadTradingTimeoutMode();
            loadIsDetachFramesMode();
            loadPutAllToSameLayer();
            loadWindowOpenStatus();
            loadNewsIndicatorExpiryTime();
            loadIsGraphAdjusted();
            loadMultiInstanceMode();
//            loadFullQuoteProperties();
//            loadCommissionStatus();
        } catch (Exception s_e) {
            s_e.printStackTrace();
            //System.out.println(s_e+" error in loading Settings file");
        }


        try {
            String metaStockPath = NativeMethods.getRegValue("SOFTWARE\\Classes\\MetaStock.Chart\\shell\\open\\command");
            if (metaStockPath != null) {
                String[] metaStockPathInfo = metaStockPath.split("\"");
                META_STOCK_APP_PATH = metaStockPathInfo[1];
                File metaStockApp = new File(META_STOCK_APP_PATH);
                if (!metaStockApp.exists()) {
                    META_STOCK_APP_PATH = null;
                }
                metaStockApp = null;
            } else {
                META_STOCK_APP_PATH = null;
            }
            metaStockPath = null;
        } catch (Exception e) {
            META_STOCK_APP_PATH = null;
        }

        String path = NativeMethods.getTradeStationPath();
        if (path != null) {
            TRADE_STATION_BAT_PATH = writeTradeStationBatFile(path);
        }

        try {
            ConfigLoader configloader = new ConfigLoader(); //mix cofiguration load
            Settings.TRADE_ROUTER_PORT = com.dfn.mtr.mix.util.Settings.MIX_END_CLIENT_PORT;
//            Settings.TRADE_ROUTER_PORT = 8999;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private static String writeTradeStationBatFile(String path) {
        File file = new File(Settings.SYSTEM_PATH + "\\TS.bat");
        String batpath = null;

        boolean status = false;
        if (file.exists()) {
            file.delete();
        }
        try {
            status = file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }

        if (status) {
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(file));
                writer.append("@echo off\r\n");
                writer.append("cd " + path + "Program\r\n");
                writer.append("ORPlat.exe\r\n");
                writer.append("exit");
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
            //file.renameTo(new File(Settings.SYSTEM_PATH + "\\TS.bat"));
            batpath = file.getAbsolutePath();
        }

        file = null;
        return batpath;

    }

    private static void loadCustomViewSettings() {
//        try {
//            DataInputStream oIn = new DataInputStream(new FileInputStream("./system/customviewsettings.msf"));
//            subscriptionURL = oIn.readLine();
//        } catch (Exception e) {
//            subscriptionURL = null;
//        }
        try {
            FileInputStream oIn = new FileInputStream(Settings.SYSTEM_PATH + "/customviewsettings.msf");
            customViews = new SmartProperties(Meta.RS);
            customViews.load(oIn);
            oIn.close();
            oIn = null;
            Enumeration keys = customViews.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                String workspaceSetting = customViews.getProperty(key);
                customViewSettings.put(key, workspaceSetting);
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static Hashtable<String, String> getCustomViewSettings() {
        return customViewSettings;
    }

    public static void putCustomViewSetting(String key, String workspaceString) {
        customViewSettings.put(key, workspaceString);
        saveCustomViewSettings();
    }

    private static void loadConsoleStatus() {
        try {
            consoleAllowed = getItem("CONSOLE_ON").equals("1");
        } catch (Exception e) {
            consoleAllowed = false;
        }

    }

    public static boolean isOfflineMode() {
        return isOfflineMode;
    }

    public static void setOfflineMode(boolean offlineMode) {
        isOfflineMode = offlineMode;
    }

    public static int getMaximumSymbolCount() {
        return maximumSymbolCount;
    }

    public static void setMaximumSymbolCount(int count) {
        maximumSymbolCount = count;
        setItem("QUOTE_LIMIT", "" + count);
    }

    public static void enableCustomViewSettings() {
        enableCustomViewSettings = true;
    }

    public static boolean isCustomViewSettingsEnabled() {
        return enableCustomViewSettings;
    }

    public static void loadMaximumSymbolCount() {
        try {
            maximumSymbolCount = Integer.parseInt(getItem("QUOTE_LIMIT"));
        } catch (Exception e) {
            maximumSymbolCount = 100;
        }
    }

    public static void loadMaximumRQuoteCount() {
        try {
            maximumRQuoteCount = Integer.parseInt(getItem("RQUOTE_LIMIT"));
        } catch (Exception e) {
            maximumRQuoteCount = 100;
        }
    }

    public static void setMaximumRQuoteCount(int maximumRQuoteCount) {
        Settings.maximumRQuoteCount = maximumRQuoteCount;
        setItem("RQUOTE_LIMIT", "" + maximumRQuoteCount);
    }

    public static int getMaximumRQuoteCount() {
        return maximumRQuoteCount;
    }

    public static void loadMaximumOPRACount() {
        try {
            maximumOPRACount = Integer.parseInt(getItem("OPRA_LIMIT"));
        } catch (Exception e) {
            maximumOPRACount = 100;
        }
    }

    public static int getMaximumOPRACount() {
        return maximumOPRACount;
    }

    public static void setMaximumOPRACount(int maximumOPRACount) {
        Settings.maximumOPRACount = maximumOPRACount;
        setItem("OPRA_LIMIT", "" + maximumOPRACount);
    }


    private static void loadBillingCode() {
        try {
            billingCode = getItem("BILLING_CODE");
            if (billingCode == null) {
                billingCode = "";
            }
        } catch (Exception e) {
            billingCode = "";
        }

    }

    private static void loadSSOCode() {
        RSA_SSO_CODE = getItem("RSA_SSO_CODE");
        if (RSA_SSO_CODE == null) {
            RSA_SSO_CODE = "";
        }
    }

    private static void loadSubscription() {
        try {
            DataInputStream oIn = new DataInputStream(new FileInputStream(Settings.CONFIG_DATA_PATH + "/subscription.dll"));
            subscriptionURL = oIn.readLine();
        } catch (Exception e) {
            subscriptionURL = null;
        }
    }

    public static String getSubscriptionURL() {
        //subscriptionURL = subscriptionURL.replaceAll("\\[LANGUAGE\\]", Language.getLanguageTag()); changed due to a bug in server
        try {
            subscriptionURL = subscriptionURL.replaceAll("\\[LANGUAGE\\]", Language.getLanguageTag().toLowerCase());
            return subscriptionURL;
        } catch (Exception e) {
            return null;
        }
    }

    private static void loadMarketCenter() {
        try {
            FileInputStream oIn = new FileInputStream(Settings.CONFIG_DATA_PATH + "/mc.dll");
            SmartProperties marketCenters = new SmartProperties("=");
            marketCenters.load(oIn);
            Enumeration keys = marketCenters.keys();
            String key;
            String element;
            String[] values;
            g_oMarketCenters = new Hashtable<String, String[]>();
            while (keys.hasMoreElements()) {
                key = (String) keys.nextElement();
                element = (String) marketCenters.get(key);
                values = element.split("\\|");
                g_oMarketCenters.put(key.toLowerCase(), values); // description|Code
                key = null;
            }
            marketCenters = null;
            oIn.close();
            oIn = null;
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static String getMarketCenter(String sID, int index) {
        try {
            String sValue = g_oMarketCenters.get(sID.toLowerCase())[index];
            return sValue;
        } catch (Exception e) {
            return Language.getString("NA");
        }
    }

//    public static String getMarketCenter(String code){
//        return g_oMarketCenters.get(code);
//    }

    private static void loadLastDynamicIP() {
        try {
            if (Settings.getBooleanItem("DYNAMIC_IPS_ALLOWED")) {
                String lastIP = Settings.getItem("LAST_DYNAMIC_IP");
                if ((lastIP != null) && (!lastIP.trim().equals(""))) {
                    setLastDynamicIP(lastIP);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void loadDualScreenProperties() {
        try {
            int numScrens = 0;
            try {
                GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();
                GraphicsDevice[] gs = ge.getScreenDevices();
                for (int j = 0; j < gs.length; j++) {
                    numScrens++;
                }
            } catch (Exception e) {
                numScrens++;
            }

            if (numScrens == 1) {
                setDualScreenMode(false);
                setLeftAlignPopups(true);
                multiScreenEnvr = false;
            } else {
                setDualScreenMode(Settings.getBooleanItem("DUAL_SCREEN_MODE"));
                setLeftAlignPopups(Settings.getBooleanItem("DUAL_SCREEN_MODE_LEFT"));
                multiScreenEnvr = true;
            }
        } catch (Exception e) {
            setDualScreenMode(false);
            setLeftAlignPopups(true);
            multiScreenEnvr = false;
        }
    }

    public static void loadFullQuoteProperties(byte type) {
        try {
            Settings.fullQuotePriceType = type;
//            Settings.fullQuotePriceType = Byte.parseByte(Settings.getItem("FULL_QUOTE_MDEPTH_TYPE"));
        } catch (NumberFormatException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static byte getFullQuoteMDepthType() {
        return Settings.fullQuotePriceType;                  
    }

    private static void loadTWVersion() {
        try {
            SmartProperties g_oVersion = new SmartProperties("=");
            g_oVersion.loadCompressed(Settings.CONFIG_DATA_PATH + "/vr.msf");            //Settings.getAbsolutepath()+
            TW_VERSION = g_oVersion.getProperty("VER");
            try {
                TW_VERSION_NUMBER = TW_VERSION.split(" ")[1];
            } catch (Exception e) {
                TW_VERSION_NUMBER = TW_VERSION;
//                e.printStackTrace();
            }
//            TW_VERSION = g_oVersion.getProperty("VER").split(" ")[1];
//            TW_VERSION_COMPLETE = g_oVersion.getProperty("VER");
//            TW_VERSION_COMPLETE = g_oVersion.getProperty("VER");
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (TW_VERSION == null) {
            TW_VERSION = "0";
            TW_VERSION_NUMBER = "0";
        }
    }

    private static void loadCustomizationVersion() {
        try {
            Properties g_oVersion = new Properties();
            g_oVersion.load(new FileInputStream(Settings.CONFIG_DATA_PATH + "/cvr.txt"));    //Settings.getAbsolutepath()+
            CUSTOMIZATION_VERSION = g_oVersion.getProperty("VER");
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    private static void loadSatelliteSettings() {
        try {
            int connectionType = Integer.parseInt(getItem("CONNECTION_MODE"));

            COMMUNICATION_TYPE = connectionType;
            SATELLITE_AUTH_PORT = 9001;
        } catch (Exception e) {
            COMMUNICATION_TYPE = COMMUNICATION_TYPE_TCP;
        }
    }

    private static void loadBroadcastMessDialogSize() {
        try {
            String[] str = getItem("BROADCAST_MESS_SIZE").split(",");
            BROADCAST_MESS_WIDTH = Integer.parseInt(str[0]);
            BROADCAST_MESS_HEIGHT = Integer.parseInt(str[1]);
        } catch (Exception e) {
            BROADCAST_MESS_WIDTH = 380;
            BROADCAST_MESS_HEIGHT = 200;
        }
    }

    private static void loadScrollingStatus() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("AUTO_SCROLL_STATUS"));
            if (status == 1)
                isScrollingOn = true;
            else
                isScrollingOn = false;

            autoScrollInterval = Integer.parseInt(getItem("AUTO_SCROLL_INTERVAL"));
        } catch (Exception ex) {
            isScrollingOn = false;
            autoScrollInterval = 10;
        }
    }

    private static void loadNumaralStatus() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("ARABIC_NUMBERS"));
            if (status == 1)
                showArabicNumbers = true;
            else
                showArabicNumbers = false;

        } catch (Exception ex) {
            showArabicNumbers = false;
        }
    }

    private static void loadAliasStatus() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("SHOW_ALIAS"));
            if (status == 1)
                Stock.symbolAlias = true;
            else
                Stock.symbolAlias = false;

        } catch (Exception ex) {
            Stock.symbolAlias = false;
        }
    }

    private static void loadWelcomeScrStatus() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("SHOW_WELCOME_SCR"));
            if (status == 1)
                showwelcomescreen = true;
            else
                showwelcomescreen = false;

        } catch (Exception ex) {
            showwelcomescreen = false;
        }
//        showwelcomescreen = true;
//        System.out.println("welcome screen loaded in settings");
    }

    private static void loadDefaulfWSPSettings() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("KEEP_REGIONAL_WSP"));
            if (status == 1)
                keepDefaultWSP = true;
            else
                keepDefaultWSP = false;
        } catch (Exception ex) {
            keepDefaultWSP = true;
        }
    }

    public static void loadWindowOpenStatus() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("KEEP_ORDER_WINDOW_OPEN"));
            if (status == 1) {
                isKeepOrderWindowOpen = true;
            } else {
                isKeepOrderWindowOpen = false;
            }
        } catch (NumberFormatException e) {
            isKeepOrderWindowOpen = false;
        }
    }

    public static void loadNewsIndicatorExpiryTime() {
        int status = -1;
        try {
            status = Integer.parseInt(getItem("NEWS_INDICATOR_EXPIRY_TIME"));
        } catch (Exception e) {
        }
        newsIndicatorExpiryTime = status;
    }

    public static int getNewsIndicatorExpiryTime() {
        return newsIndicatorExpiryTime;
    }

    public static void setNewsIndicatorExpiryTime(int newsIndicatorExpiryTime) {
        Settings.newsIndicatorExpiryTime = newsIndicatorExpiryTime;
        setItem("NEWS_INDICATOR_EXPIRY_TIME", "" + newsIndicatorExpiryTime);
        PulseGenerator.setNewsCountDownTime(Settings.getNewsIndicatorExpiryTime() * 60);
    }

    public static boolean isDefaultWSPEnable() {
//        System.out.println("get the sow screen loaded variable");
        return keepDefaultWSP;
    }

    public static boolean isShowWelcomeScreen() {
//        System.out.println("get the sow screen loaded variable");
        return showwelcomescreen;
    }

    public static void setShowWelcomeScreen(boolean status) {
        showwelcomescreen = status;
        if (status) {
            setItem("SHOW_WELCOME_SCR", "1");
        } else {
            setItem("SHOW_WELCOME_SCR", "0");
        }
    }

    private static void loadScreenStatus() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("FULL_SCREEN_MODE"));
            if (status == 1)
                fullScreenMode = true;
            else
                fullScreenMode = false;

        } catch (Exception ex) {
            fullScreenMode = false;
        }
    }

    private static void loadMktDepthStatus() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("SHOW_COMBINED_DEPTH"));
            if (status == 1)
                combinedOrderBook = true;
            else
                combinedOrderBook = false;

            isMktDepthStatusLoaded = true;

        } catch (Exception ex) {
            combinedOrderBook = false;
        }
    }

    public static boolean isMktDepthStatusLoaded() {
        return isMktDepthStatusLoaded;
    }

    public static void loadMultiInstanceMode() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("IS_MULTI_INSTANCE_MODE"));
            if (status == 1)
                isMultiInstanceMode = true;
            else
                isMultiInstanceMode = false;

        } catch (Exception ex) {
            isMultiInstanceMode = false;
        }
    }

    public static boolean isMultiInstanceMode() {
           return isMultiInstanceMode;
       }

    private static void loadAnnouncePopupStatus() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("POPUP_ANNOUNCE_WINDOW"));
            if (status == 1)
                popupAnnounceWindow = true;
            else
                popupAnnounceWindow = false;

        } catch (Exception ex) {
            popupAnnounceWindow = true;
        }
    }

    private static void loadTabIndex() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("TAB_INDEX"));
            if (status == 1)
                tabIndex = true;
            else
                tabIndex = false;

        } catch (Exception ex) {
            tabIndex = true;
        }
    }

    private static void loadMarketMapValueSowing() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("HEATMAP_VALUE_ONF"));
            if (status == 1)
                heatMapValueOnOff = true;
            else
                heatMapValueOnOff = false;

        } catch (Exception ex) {
            heatMapValueOnOff = true;
        }
    }

    private static void loadHeatMapMode() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("HEATMAP_MODE"));
            if (status == 1) {
                heatMapMode = 1;
            } else if (status == 2) {
                heatMapMode = 2;
            } else {
                heatMapMode = 0;
            }

        } catch (Exception ex) {
            heatMapMode = 0;
        }
    }

    private static void loadAutoUpdateDefaultMarkets() {
        try {
            autoUpdateEnableMarkets = new ArrayList<String>();
            String marketList = getItem("METASTOCK_AUTOUPDATE_ENABLE");
            if ((marketList == null) || marketList.equals("NO_MARKETS_SELECTED")) {

            } else {
                StringTokenizer st = new StringTokenizer(marketList, ",");
                while (st.hasMoreTokens()) {
                    String exchange = st.nextToken();
                    autoUpdateEnableMarkets.add(exchange);
                    exchange = null;
                }
            }
        } catch (Exception e) {

        }
    }

    private static void loadAutoUpdateDefaultMarketsTS() {
        try {
            autoUpdateEnableMarketsTS = new ArrayList<String>();
            String marketList = getItem("TRADESTATION_AUTOUPDATE_ENABLE");
            if ((marketList == null) || marketList.equals("NO_MARKETS_SELECTED")) {

            } else {
                StringTokenizer st = new StringTokenizer(marketList, ",");
                while (st.hasMoreTokens()) {
                    String exchange = st.nextToken();
                    autoUpdateEnableMarketsTS.add(exchange);
                    exchange = null;
                }
            }
        } catch (Exception e) {

        }
    }

    public static void loadTickerStatus() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("SUMMARY_TICKER"));
            if (status == 1)
                showSummaryTicker = true;
            else
                showSummaryTicker = false;

        } catch (Exception ex) {
            showSummaryTicker = false;
        }
    }

    public static void loadTradeTickerMode() {
        try {
            enableTradeTicker = Boolean.parseBoolean(getItem("ENABLE_TRADE_TICKER"));
            setEnableTradeTicker(enableTradeTicker);
        } catch (Exception ex) {
            enableTradeTicker = false;
            setEnableTradeTicker(false);
        }
    }

    public static void loadTradingTimeoutMode() {
        try {
            tradingTimeoutMode = Integer.parseInt(getItem("TRADING_TIMEOUT"));
        } catch (NumberFormatException e) {
            tradingTimeoutMode = 0;//set to "never" mode
        }
    }

    public static boolean isTradeTickerMode() {
        return enableTradeTicker;
    }

    public static void loadTimeNSalesStatus() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("SAVE_TNS"));
            if (status == 1)
                saveTimeNSales = true;
            else
                saveTimeNSales = false;

        } catch (Exception ex) {
            saveTimeNSales = false;
        }
    }

    public static void loadIndexPanelMode() {
        try {
            symbolPanelOnly = Boolean.parseBoolean(getItem("SYMBOL_PANEL_ONLY"));

        } catch (Exception ex) {
            symbolPanelOnly = false;
        }

    }

    public static void loadIsDetachFramesMode() {
        try {
            detachOnWspLoad = Boolean.parseBoolean(getItem("DETACH_FRAMES_ON_WSPLOAD"));
        } catch (Exception ex) {
            detachOnWspLoad = true;
        }
    }

    public static void loadPutAllToSameLayer() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("PUT_ALL_TO_SAME_LAYER"));
            isPutAllToSameLayer = (status == 1);
        } catch (Exception ex) {
            isPutAllToSameLayer = false;
        }
    }

    public static void loadIsGraphAdjusted() {
        boolean status;
        try {
            status = Boolean.parseBoolean(getItem("GRAPH_ADJUSTED"));
            isGraphAdjusted = (status == true);
        } catch (Exception ex) {
            isGraphAdjusted = false;
        }
    }

    public static boolean isPutAllToSameLayer() {
        return isPutAllToSameLayer;
    }

    public static boolean isGraphAdjusted() {
        return isGraphAdjusted;
    }

    public static void setPutAllToSameLayer(boolean putAllToSameLayer) {
        isPutAllToSameLayer = putAllToSameLayer;
        if (isPutAllToSameLayer) {
            setItem("PUT_ALL_TO_SAME_LAYER", "1");
        } else {
            setItem("PUT_ALL_TO_SAME_LAYER", "0");
        }
    }

    /*public static void setGraphAdjusted(boolean isAdjusted) {
        isGraphAdjusted = isAdjusted;
        if (isGraphAdjusted) {
            setItem("GRAPH_ADJUSTED", "1");
        } else {
            setItem("GRAPH_ADJUSTED", "0");
        }
    }*/

    public static void loadProxySettings() {
        if (isSocksProxyEnabled()) {
            System.setProperty("socksProxyHost", Settings.getItem("PROXY_SOCKS_IP"));
            System.setProperty("socksProxyPort", Settings.getItem("PROXY_SOCKS_PORT"));
        } else {
            System.setProperty("socksProxyHost", "");
        }
    }

    public static void loadWebProxySettings() {
        if (isWebProxyEnabled()) {
            System.setProperty("http.proxyHost", Settings.getItem("PROXY_WEB_IP"));
            System.setProperty("http.proxyPort", Settings.getItem("PROXY_WEB_PORT"));
        } else {
            System.setProperty("http.proxyHost", "");
        }
    }

    public static void loadWebProxySettingsForHTTPS() {
        if (isWebProxyEnabled()) {
            System.setProperty("https.proxyHost", Settings.getItem("PROXY_WEB_IP"));
            System.setProperty("https.proxyPort", Settings.getItem("PROXY_WEB_PORT"));
        } else {
            System.setProperty("https.proxyHost", "");
        }
    }
    /*public static void loadCommissionStatus() {
        int status = 0;
        try {
            status = Integer.parseInt(getItem("PF_SIM_AUTO_COMM"));
            if (status == 1)
                portfolioSimulatorAutoCommission = true;
            else
                portfolioSimulatorAutoCommission = false;

        } catch (Exception ex) {
            portfolioSimulatorAutoCommission = false;
        }
    }*/

    public static boolean isShowSummaryTicker() {
        return showSummaryTicker;
    }

    public synchronized static void save() {
        try {
            synchronized (prop) {
                //--------------------------------- To save user details to a seperate file ------------------------
                if (LAST_TIME_SAVE) {
                    FileOutputStream oOutBulk = new FileOutputStream(Settings.SYSTEM_PATH + "/userdata.msf");
                    Enumeration keys = prop.keys();
                    while (keys.hasMoreElements()) {
                        String key = (String) keys.nextElement();

                        if (key.equals("DEFAULT_USER")) {

                            propBulk.setProperty("DEFAULT_USER", SharedMethods.encrypt(propBulk.getProperty("DEFAULT_USER")));
                            //  prop.remove("DEFAULT_USER");
                        } else if (key.equals("TRADE_USER")) {

                            propBulk.setProperty("TRADE_USER", SharedMethods.encrypt(propBulk.getProperty("TRADE_USER")));
                            //  prop.remove("TRADE_USER");

                        } else if (key.equals("PASSWORD")) {

                            propBulk.setProperty("PASSWORD", SharedMethods.encrypt(propBulk.getProperty("PASSWORD")));
                            //  prop.remove("PASSWORD");

                        }
                    }
                    propBulk.store(oOutBulk, "");
                    oOutBulk.close();
                    /******************************/
                }

                FileOutputStream oOut = new FileOutputStream(Settings.SYSTEM_PATH + "/st.dll");
//                prop.setProperty("DEFAULT_USER", SharedMethods.encrypt(prop.getProperty("DEFAULT_USER")));
//                prop.setProperty("TRADE_USER", SharedMethods.encrypt(prop.getProperty("TRADE_USER")));
//                prop.setProperty("PASSWORD", SharedMethods.encrypt(prop.getProperty("PASSWORD")));
                prop.store(oOut, "");
                oOut.close();
//                prop.setProperty("DEFAULT_USER", SharedMethods.decrypt(prop.getProperty("DEFAULT_USER")));
//                prop.setProperty("TRADE_USER", SharedMethods.decrypt(prop.getProperty("TRADE_USER")));
//                prop.setProperty("PASSWORD", SharedMethods.decrypt(prop.getProperty("PASSWORD")));
//                prop.store(oOut, "");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    private static void saveCustomViewSettings() {
        try {
            FileOutputStream oOut = new FileOutputStream(Settings.SYSTEM_PATH + "/customviewsettings.msf");
            Enumeration keys = customViewSettings.keys();
            while (keys.hasMoreElements()) {
                String key = (String) keys.nextElement();
                String workspaceSetting = customViewSettings.get(key);
//                customViews.put(key, workspaceSetting);
                oOut.write((key + Meta.RS + workspaceSetting + "\n").getBytes());
            }
//            customViews.store(oOut, "");
            oOut.close();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public static String getItem(String sItem) {
        String sValue = "";

        sValue = prop.getProperty(sItem);

        if (sValue != null)
            return sValue.trim();
        else
            return null;
    }

    public static String getItemFromBulk(String sItem) {
        String sValue = "";

        sValue = propBulk.getProperty(sItem);

        if (sValue != null)
            return sValue.trim();
        else
            return null;

    }

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static int getZipSize() {
//        return zipSize;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

//    public static void setZipSize(int size) {
//        zipSize = size;
//    }

    public static boolean getBooleanItem(String sItem) {
        try {
            String sValue = "";

            sValue = prop.getProperty(sItem);

            return (new Boolean(sValue)).booleanValue();
        } catch (Exception ex) {
            return false;
        }
    }

    public static boolean getBooleanItem(String sItem, boolean defaultValue) {
        try {
            String sValue = "";

            sValue = prop.getProperty(sItem);
            if (sValue == null) {
                return defaultValue;
            } else {
                return (new Boolean(sValue)).booleanValue();
            }
        } catch (Exception ex) {
            return defaultValue;
        }
    }

    public synchronized static void RemoveItem(String sItem) {
        prop.remove(sItem);
        save();
        /*try
        {
            prop.save(new FileOutputStream(new File("st.dll")), "date");
        }catch (Exception s_e)
        {
            //System.out.println("error in RemoveItem"+s_e);
        }*/
    }

    public synchronized static void saveItem(String sTitle, String sItem) {
        prop.put(sTitle, sItem);
        save();
        /*try
        {
            prop.save(new FileOutputStream(new File("st.dll")), "date");
        }catch (Exception s_e)
        {
            //System.out.println("error in SaveItem"+s_e);
        }*/
    }

    public static void setTickerFilter(String type) {
        saveItem("TICKER_FILTER_TYPE", type);
    }

    public static String getTickerFilter() {
        return getItem("TICKER_FILTER_TYPE");
    }

    public static void setTickerFilterData(String data) {
        setItem("TICKER_FILTER_DATA", data);
    }

    public static String getTickerFilterData() {
        return getItem("TICKER_FILTER_DATA");
    }

    public static void setItem(String sTitle, String sItem) {
        prop.put(sTitle, sItem);
    }

    public static void setItemInBulk(String sTitle, String sItem) {
        propBulk.put(sTitle, sItem);
    }

    public static String getLastThemeID() {
        try {
            return getItem("THEME_ID");
        } catch (Exception e) {
            return "0";
        }
    }

    public static void saveTheme(String sID) {
        saveItem("THEME_ID", "" + sID);
    }

    public static void setRunMode() {
        g_bRun = true;
    }

    public static void setExitMode() {
        g_bRun = false;
    }

    public static boolean isRunning() {
        return g_bRun;
    }

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static void setChangingPassword() {
//        g_bChangingPassword = true;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static boolean isPasswordChanged() {
//        return g_bChangingPassword;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

    public static void setConnected() {
        g_bConnected = true;
    }

    public static void setDisConnected() {
        g_bConnected = false;
    }

    public static boolean isConnected() {
        return g_bConnected;
    }

    /*
     *  Checks whether the Market is Closed
     */

    public static boolean isMarketClosed() {
        if (g_iMarketStatus == MARKET_CLOSE)
            return true;
        else
            return false;
    }

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static boolean isMarketOpen() {
//        if (g_iMarketStatus == MARKET_OPEN)
//            return true;
//        else
//            return false;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

    public static int getMarketStatus() {
        return g_iMarketStatus;
    }

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static void setMarketPreOpen() {
//        g_iMarketStatus = Meta.MARKET_PREOPEN;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static void setMarketOpen() {
//        g_iMarketStatus = Meta.MARKET_OPEN;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static void setMarketCosed() {
//        g_iMarketStatus = Meta.MARKET_CLOSE;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static void setMarketPreCosed() {
//        g_iMarketStatus = Meta.MARKET_PRECLOSE;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static boolean isMarketStatusUpdated() {
//        return g_bMarketStatusFound;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static void setMarketStatusValid() {
//        g_bMarketStatusFound = true;
//        //System.out.println(" ***************** ");
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

//    public static void setMarketStatusInvalid() {
//        g_bMarketStatusFound = false;
//    }

    public static boolean isAuthenticated() {
        return g_bAuthented;
    }

    public static void setAuthenticated() {
        g_bAuthented = true;
    }

    public static void setInvalid() {
        g_bAuthented = false;
    }

    public synchronized static void setNewStockMasterAvailable() {
        prop.put("NEW_STOCKMASTER_AVAILABLE", "1");
        save();
    }

    public static boolean isNewStockMasterAvailable() {
        String sStockMaster = null;

        sStockMaster = (String) prop.get("NEW_STOCKMASTER_AVAILABLE");
        if (sStockMaster == null) {
            return false;
        } else if (sStockMaster.equals("1")) {
            return true;
        } else {
            return false;
        }
    }

    public synchronized static void setInvlidateNewStockMaster() {
        prop.put("NEW_STOCKMASTER_AVAILABLE", "0");
        save();
    }

    /**
     * Sets the zip available message
     */
//    public static void setZipAvailable() {
//        g_bZipready = true;
//    }
//
//    /**
//     * Sets the zip file read status
//     */
//    public static void setZipCompleted() {
//        g_bZipready = false;
//    }

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    /**
//     * Checks the zip file status
//     */
//    public static boolean isZipReady() {
//        return g_bZipready;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

//    public static void setShowIntradayChart(boolean bSetting) {
//        g_bIntradayChart = bSetting;
//    }
//
//    public static void setShowAnnouncements(boolean bSetting) {
//        g_bShowAnnouncements = bSetting;
//    }
//
//    public static void setShowOrderBook(boolean bSetting) {
//        g_bShowOrderBook = bSetting;
//    }

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static boolean isShowIntradayChartAllowed() {
//        return g_bIntradayChart;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static boolean isShowAnnouncementsAllowed() {
//        return g_bShowAnnouncements;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static boolean isShowOrderBookAllowed() {
//        return g_bShowOrderBook;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)
    public static void setHistoryCondition(short iCondition) {
        g_iHistoryCondition = iCondition;
    }

    public static short getHistoryCondition() {
        return g_iHistoryCondition;
    }

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static boolean isLogoutSelected() {
//        return g_bLogoutSelected;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static void setLogoutSelected(boolean bStatus) {
//        g_bLogoutSelected = bStatus;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

    public static String getDefaultWorkspace() {
        return getItem("DEFAULT_WS");
    }

    public static void setDefaultWorkspace(String file) {
        setItem("DEFAULT_WS", file);
    }

    public static boolean isDefaultThemeAvailable() {
        return ((getItem("DEFAULT_WS") != null) &&
                (!getItem("DEFAULT_WS").equals("")));
    }

    public static void setPageFormat(PageFormat pageFormat) {
        g_PageFormat = pageFormat;
    }

    public static PageFormat getPageFormat() {
        if (g_PageFormat == null) {
            PrinterJob prnJob = PrinterJob.getPrinterJob();
            g_PageFormat = prnJob.defaultPage();
        }
        return g_PageFormat;
    }

    public static String getSessionID() {
        return sessionID;
    }

    public static void setSessionID(String newSessionID) {
        sessionID = newSessionID;
    }

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static String getSubsciptionID() {
//        return subsciptionID;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

//    public static void setSubsciptionID(String newSubsciptionID) {
//        subsciptionID = newSubsciptionID;
//    }

    public static String getUserID() {
        return userID;
    }

    public static void setUserID(String newUserID) {
        userID = newUserID;
    }

    public static TimeZone getCurrentZone() {
        return currentZone;
    }

    public static long getLocalTimeFor(long time) {
        try {
            return time + currentZone.getOffset(time);
        } catch (Exception e) {
            return time;
        }
    }

    public static void setCurrentZone(TimeZone zone) {
        currentZone = zone;
    }

    public static void setUserDetails(String usrName, String password) {
        Settings.user = usrName;
        Settings.password = password;
        if (isSingleSignOnMode()) { // && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web)
            Settings.setItem("DEFAULT_USER", "");
            Settings.setItem("PASSWORD", "");
        }
    }

//    public static void setTradeUser(String usrName) {
//        Settings.tradeUser = usrName;
//    }

    public static String getUserName() {
        return user;
    }

//    public static String getTradeUser() {
//        return tradeUser;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

    public static String getUserPassword() {
        return password;
    }

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static void setManualDisconnect(boolean status) {
//        isManulaDisconnection = status;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

// --Commented out by Inspection START (3/29/05 8:55 AM):
//    public static boolean isManualDisconnect() {
//        return isManulaDisconnection;
//    }
// --Commented out by Inspection STOP (3/29/05 8:55 AM)

    // #########################################################################
    //      Methods use for auto scrolling Table contents
    // #########################################################################

    public static boolean isScrollingOn() {
        return isScrollingOn;
    }

    public static void setAutoScrolling() {
        int status = 0;
        isScrollingOn ^= true;
//        isScrollingOn = !isScrollingOn;
        if (isScrollingOn)
            status = 1;
        else
            status = 0;
        setItem("AUTO_SCROLL_STATUS", status + "");
    }

    public static String getActiveIP() {
        return activeIP;
    }

    public static void setActiveIP(String newIP) {
        activeIP = newIP;
    }

    public static boolean isShowArabicNumbers() {
        return showArabicNumbers;
    }

    public static void setShowArabicNumbers(boolean status) {
        showArabicNumbers = status;
        if (showArabicNumbers)
            setItem("ARABIC_NUMBERS", "1");
        else
            setItem("ARABIC_NUMBERS", "0");
    }

    public static boolean isShowAlias() {
        return Stock.symbolAlias;
    }

    public static void setShowAlias(boolean status) {
        Stock.symbolAlias = status;
        if (Stock.symbolAlias) {
            setItem("SHOW_ALIAS", "1");
            UpdateNotifier.setShapshotUpdated();
        } else {
            setItem("SHOW_ALIAS", "0");
            UpdateNotifier.setShapshotUpdated();
        }
    }


    public static boolean isFullScreenMode() {
        return fullScreenMode;
    }

    public static void setFullScreenMode(boolean status) {
        fullScreenMode = status;
        if (fullScreenMode)
            setItem("FULL_SCREEN_MODE", "1");
        else
            setItem("FULL_SCREEN_MODE", "0");
    }

    public static void setShowSummaryTicker(boolean status) {
        showSummaryTicker = status;
        if (showSummaryTicker)
            setItem("SUMMARY_TICKER", "1");
        else
            setItem("SUMMARY_TICKER", "0");
    }

    public static void setEnableTradeTicker(boolean mode) {
        if (mode)
            setItem("ENABLE_TRADE_TICKER", "true");
        else
            setItem("ENABLE_TRADE_TICKER", "false");
    }
    /*public static void setAllowedWindowTypes(String sData) {

        try {
            System.out.println(sData);
            String[] types = sData.split("\\|");
            allowedWindowTypes = new int[types.length];
            for (int i = 0; i < types.length; i++) {
                try {
                    allowedWindowTypes[i] = Integer.parseInt(types[i]);
                } catch (NumberFormatException ex) {
//ex.printStackTrace();
                }
            }
            Arrays.sort(allowedWindowTypes);
//setChartMode();

            //setChartMode();

// validate metastock
//            metaStockEnabledMode = isValidWindow(Meta.IT_MetaStock);

            if (!ExchangeStore.isValidSystemFinformationType(Meta.IT_Config)) {
                RemoveItem("CONFIG_ALLOWED");
            }
//            if (isValidWindow(Meta.IT_SymbolTimeAndSales)) {
//                tradeEnabledMode = true;
//            } else {
//                tradeEnabledMode = false;
//            }
            /*if (isValidWindow(Meta.WT_AllowDynamicIPs)) {
                TCPConnector.PRIMARY_IP_FORCED = false;
                IPSettings.resetIPIndex(); // make sure to go from the beginning next time
                setItem("DYNAMIC_IPS_ALLOWED", "true");
                IPSettings.includeDynamicIPs();
            } else {
                if (!sData.equals("")) {
                    setItem("DYNAMIC_IPS_ALLOWED", "false");
                    IPSettings.discardDymanicIPs();
                }
                if (isValidWindow(Meta.WT_ForcePrimaryIP)) {
                    TCPConnector.PRIMARY_IP_FORCED = true;
                } else {
                    TCPConnector.PRIMARY_IP_FORCED = false;
                }
            }*
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

// --Commented out by Inspection START (3/29/05 8:57 AM):
//    public static boolean isTradeEnabledMode() {
//        return tradeEnabledMode;
//    }
// --Commented out by Inspection STOP (3/29/05 8:57 AM)

    /*private static void setChartMode() {
        if (Arrays.binarySearch(allowedWindowTypes, Meta.WT_ProfessionalCharting) >= 0) {
            ChartInterface.setHistoryEnabled(true);
            ChartInterface.setIndicatorsEnabled(true);
        } else if (Arrays.binarySearch(allowedWindowTypes, Meta.WT_SandardCharting) >= 0) {
            ChartInterface.setHistoryEnabled(true);
            ChartInterface.setIndicatorsEnabled(false);
        } else if (Arrays.binarySearch(allowedWindowTypes, Meta.WT_BasicCharting) >= 0) {
            ChartInterface.setHistoryEnabled(false);
            ChartInterface.setIndicatorsEnabled(false);
        } else {
            ChartInterface.setHistoryEnabled(false);
            ChartInterface.setIndicatorsEnabled(false);
        }
    }*/

    /*public static boolean isValidWindow(int type) {

        try {
            return (Arrays.binarySearch(allowedWindowTypes, type) >= 0);
        } catch (Exception ex) {
            return false;
        }

    }*/


    public synchronized static long getTimeOffset(long time) {
        return 0;
    }

    public static int getAlertModes() {
        return alertModes;
    }

    public static void setAlertModes(int modes) {
        alertModes = modes;
    }

    public static void setTWPath(String path) {
        twPath = path;
    }

    public static String getTWPath() {
        return twPath;
    }

    public static String getCMID() {
        try {
            String[] path = twPath.split("\\|");
            return path[path.length - 1];
        } catch (Exception e) {
            return null;
        }
    }

    public static String getTradeArchivePath() {
//        String path = prop.getProperty("TRADE_ARCHIVE");
//        if ((path == null) || (path.trim().equals(""))){
        return Settings.getAbsolutepath() + "trades\\";
//        }else{
//            return path;
//        }
    }

    public static void setTradeArchivePath(String path) {
        prop.put("TRADE_ARCHIVE", path);
    }

    public static boolean isTCPMode() {
        return (Settings.COMMUNICATION_TYPE == Settings.COMMUNICATION_TYPE_TCP);
    }

    public static boolean isSatelliteMode() {
        return (Settings.COMMUNICATION_TYPE == Settings.COMMUNICATION_TYPE_UDP);
    }

    public static int getCommunicationType() {
        return Settings.COMMUNICATION_TYPE;
    }

    public static void setCommunicationType(int communicationType) {
        Settings.COMMUNICATION_TYPE = communicationType;
    }

    public static float getPropertyVersion() {
        try {
            return Float.parseFloat(getItem("PROPERTY_VERSION_" + Language.getSelectedLanguage()));
        } catch (Exception e) {
            return 0;
        }
    }

    public static void setPropertyVersion(float version) {
        setItem("PROPERTY_VERSION_" + Language.getSelectedLanguage(), "" + version);
    }

    public static boolean isMetaStockInstalled() {
        return Settings.META_STOCK_APP_PATH != null;
    }

    public static boolean isTradeStationInstalled() {
        return Settings.TRADE_STATION_BAT_PATH != null;
    }

    public static boolean isTwUpdateAvailable() {
        return twUpdateAvailable;
    }

    public static void setTwUpdateAvailable(boolean twUpdateAvailable) {
        Settings.twUpdateAvailable = twUpdateAvailable;
    }

    public static int getTwVersionInInt() {
        try {
            String[] versionData = TW_VERSION.split(".");
            String sVersion = "";
            for (int i = 0; i < versionData.length; i++) {
                sVersion += versionData[i];
            }

            return Integer.parseInt(sVersion);
        } catch (Exception e) {
            return 0;
        }
    }

    public static String convertIntToTwVersion(int iVer) {
        try {
            String sVersion = "" + iVer;
            int len = sVersion.length();

            return sVersion.substring(0, len - 5) + "." + sVersion.substring(len - 5, len - 3) + "." + sVersion.substring(len - 3);
        } catch (Exception e) {
            return "0.00.000";
        }
    }

    public static String getDatacenterFax() {
        return datacenterFax;
    }

    public static void setDatacenterFax(String newdatacenterFax) {
        datacenterFax = newdatacenterFax;
    }

    public static String getDatacenterPhone() {
        return datacenterPhone;
    }

    public static void setDatacenterPhone(String newdatacenterPhone) {
        datacenterPhone = newdatacenterPhone;
    }

    public static String getDatacenterURL() {
        return datacenterURL;
    }

    public static void setDatacenterURL(String newdatacenterURL) {
        datacenterURL = newdatacenterURL;
    }

    public static String getBrokerFax() {
        return brokerFax;
    }

    public static void setBrokerFax(String newbrokerFax) {
        brokerFax = newbrokerFax;
    }

    public static String getBrokerPhone() {
        return brokerPhone;
    }

    public static void setBrokerPhone(String newbrokerPhone) {
        brokerPhone = newbrokerPhone;
    }

    public static String getBrokerURL() {
        return brokerURL;
    }

    public static void setBrokerURL(String newbrokerURL) {
        brokerURL = newbrokerURL;
    }

    public static boolean isShowCombinedOrderBook() {
        return combinedOrderBook;
    }

    public static void setShowCombinedOrderBook(boolean combinedOrderBook) {
        Settings.combinedOrderBook = combinedOrderBook;
        if (combinedOrderBook)
            setItem("SHOW_COMBINED_DEPTH", "1");
        else
            setItem("SHOW_COMBINED_DEPTH", "0");
        isMktDepthStatusLoaded = true;
    }

    public static boolean isPopupAnnounceWindow() {
        return popupAnnounceWindow;
    }

    public static void setPopupAnnounceWindow(boolean popupAnnounceWindow) {
        Settings.popupAnnounceWindow = popupAnnounceWindow;
        if (popupAnnounceWindow)
            setItem("POPUP_ANNOUNCE_WINDOW", "1");
        else
            setItem("POPUP_ANNOUNCE_WINDOW", "0");

    }

    public static boolean isTabIndex() {
        return tabIndex;
    }

    public static void setTabIndex(boolean tabIndex) {
        Settings.tabIndex = tabIndex;

        if (tabIndex)
            setItem("TAB_INDEX", "1");
        else
            setItem("TAB_INDEX", "0");
    }

    public static boolean isHeatMapValueShowing() {
        return heatMapValueOnOff;
    }

    public static void setHeatMapValueShowing(boolean heatMapVal) {
        Settings.heatMapValueOnOff = heatMapVal;

        if (heatMapValueOnOff)
            setItem("HEATMAP_VALUE_ONF", "1");
        else
            setItem("HEATMAP_VALUE_ONF", "0");
    }

    public static int getHeatMapMode() {
        return heatMapMode;
    }

    public static void setHeatMapMode(int mode) {
        Settings.heatMapMode = mode;

        if (heatMapMode == 1) {
            setItem("HEATMAP_MODE", "1");
        } else if (heatMapMode == 2) {
            setItem("HEATMAP_MODE", "2");
        } else if (heatMapMode == 0) {
            setItem("HEATMAP_MODE", "0");
        }
    }

    public static void setAutoUpdateforSelectedMarkets(boolean autoUpdateDefaults, String markets) {
        Settings.autoUpdateDefaultMarkets = autoUpdateDefaults;
        if (autoUpdateDefaults)
            setItem("METASTOCK_AUTOUPDATE_ENABLE", markets);
        else
            setItem("METASTOCK_AUTOUPDATE_ENABLE", "NO_MARKETS_SELECTED");
    }

    public static void setAutoUpdateforSelectedMarketsTS(boolean autoUpdateDefaults, String markets) {
        if (autoUpdateDefaults)
            setItem("TRADESTATION_AUTOUPDATE_ENABLE", markets);
        else
            setItem("TRADESTATION_AUTOUPDATE_ENABLE", "NO_MARKETS_SELECTED");
    }

    public static String getSingleSignOnID() {
        return singleSignOnID;
    }

    public static void setSingleSignOnID(String singleSignOnID) {
        Settings.singleSignOnID = singleSignOnID;
    }

    public static String getSsoId(){
        return ssoId;
    }

    public static void setSsoId(String id){
        Settings.ssoId = id;
    }

    public static boolean isSingleSignOnMode() {
        return singleSignOnMode;
    }

    public static void setSingleSignOnMode(boolean singleSignOnMode) {
        Settings.singleSignOnMode = singleSignOnMode;
    }

    public static boolean isDualScreenMode() {
        return dualScreenMode;
    }

    public static void setDualScreenMode(boolean dualScreenMode) {
        Settings.dualScreenMode = dualScreenMode;
        setItem("DUAL_SCREEN_MODE", "" + dualScreenMode);
    }

    public static boolean isLeftAlignPopups() {
        return leftAlignPopups;
    }

    public static void setLeftAlignPopups(boolean leftAlignPopups) {
        Settings.leftAlignPopups = leftAlignPopups;
        setItem("DUAL_SCREEN_MODE_LEFT", "" + leftAlignPopups);
    }


    public static String getInstitutionID() {
        return institutionID;
    }

    public static void setInstitutionID(String institutionID) {
        Settings.institutionID = institutionID;
    }

    public static boolean isSsoTradingEnabled() {
        return ssoTradingEnabled;
    }

    public static void setSsoTradingEnabled(boolean ssoTradingEnabled) {
        Settings.ssoTradingEnabled = ssoTradingEnabled;
    }

    /*public static boolean isPortfolioSimulatorAutoCommission() {
        return portfolioSimulatorAutoCommission;
    }

    public static void setPortfolioSimulatorAutoCommission(boolean portfolioSimulatorAutoCommission) {
        Settings.portfolioSimulatorAutoCommission = portfolioSimulatorAutoCommission;
        int status;
        if (portfolioSimulatorAutoCommission)
            status = 1;
        else
            status = 0;
        setItem("PF_SIM_AUTO_COMM", status + "");
    }*/

    public static boolean isSocksProxyEnabled() {
        try {
            return prop.getProperty("USE_SOCKS_PROXY").equals("1");
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isWebProxyEnabled() {
        try {
            return prop.getProperty("USE_WEB_PROXY").equals("1");
        } catch (Exception e) {
            return false;
        }
    }

    public static String getLastDynamicIP() {
        return lastDynamicIP;
    }

    public static void setLastDynamicIP(String lastDynamicIP) {
        Settings.lastDynamicIP = lastDynamicIP;
        prop.put("LAST_DYNAMIC_IP", lastDynamicIP);
    }

    public static synchronized String getTradeUser() {
        synchronized (propBulk) {
            return propBulk.getProperty("TRADE_USER");
        }
    }

    public static boolean isMultiScreenEnvr() {
        return multiScreenEnvr;
    }


    public static boolean isConsoleAllowed() {
        return consoleAllowed;
    }

    public static void setConsoleAllowed(boolean status) {
        Settings.consoleAllowed = status;
        if (status) {
            setItem("CONSOLE_ON", "1");
        } else {
            setItem("CONSOLE_ON", "0");
        }
    }

    public static String getGlobalContentProviderIP() {
        return globalContentProviderIP;
    }

    public static void setGlobalContentProviderIP(String globalContentProviderIP) {
        if ((globalContentProviderIP != null) && (!globalContentProviderIP.equals("")) && (!globalContentProviderIP.equalsIgnoreCase("NULL"))) {
            Settings.globalContentProviderIP = globalContentProviderIP;
        }
    }

    public static String getRegionalContentProviderIP() {
        return regionalContentProviderIP;
    }

    public static void setRegionalContentProviderIP(String regionalContentProviderIP) {
        if ((regionalContentProviderIP != null) && (!regionalContentProviderIP.equals("")) && (!regionalContentProviderIP.equalsIgnoreCase("NULL"))) {
            Settings.regionalContentProviderIP = regionalContentProviderIP;
        }
    }

    public static String getBillingCode() {
        return billingCode;
    }

    public static void setBillingCode(String billingCode) {
        Settings.billingCode = billingCode;
        setItem("BILLING_CODE", billingCode);
    }

    public static String getInstitutionCode() {
        return institutionCode;
    }

    public static void setInstitutionCode(String institutionCode) {
        Settings.institutionCode = institutionCode;
    }

    public static boolean isDeveloperMode() {
        return Settings.getBillingCode().equals("ISI") || Settings.getBillingCode().equals("MDC");
    }


    public static boolean isSaveTimeNSales() {
        return saveTimeNSales;
    }

    public static void setSaveTimeNSales(boolean saveTimeNSales) {
        Settings.saveTimeNSales = saveTimeNSales;
        if (saveTimeNSales) {
            setItem("SAVE_TNS", "1");
        } else {
            setItem("SAVE_TNS", "0");
        }
    }

    public static byte getUserPaymentType() {
        return userPaymentType;
    }

    public static void setUserPaymentType(byte userPaymentType) {
        Settings.userPaymentType = userPaymentType;
    }

    public static boolean isUSConnectionSuccess() {
        return isUSConnectionSuccess;
    }

    public static void setUSConnectionSuccess(boolean USConnectionSuccess) {
        isUSConnectionSuccess = USConnectionSuccess;
    }

    public static int getConnectionType() {
        return connectionType;
    }

    public static void setConnectionType(int connectionType) {
        Settings.connectionType = connectionType;
    }

    public static void setBurstReuestMode(boolean mode) {
        isBurstRequestMode = mode;
    }

    public static boolean isBurstRequestMode() {
        return isBurstRequestMode;
    }

    public static void setBurstReuestModeSecondary(boolean mode) {
        isBurstRequestMode = mode;
    }

    public static boolean isBurstRequestModeSecondary() {
        return isBurstRequestMode;
    }

    public static String getAbsolutepath() {
        return Mubasher.getProDataHome() + "\\";
//        return "C:\\Documents and Settings\\chandika\\Mubasher\\Mubasher Pro\\";
    }

    public static boolean couldNotApplyUpdate() {
        return couldNotApplyUpdate;
    }

    public static void setCouldNotApplyUpdate() {
        Settings.couldNotApplyUpdate = true;
    }

    public static void setCurrentDay(String date) {
        setItem("ONCE_A_DAY_VALIDATION", date);
    }

    public static void resetCurrentDay() {
        setItem("ONCE_A_DAY_VALIDATION", "");
    }

    public static boolean isOnceADayValidationNeed() {
        try {
            SimpleDateFormat sdfOutput = new SimpleDateFormat("MM:dd:yyyy");
            Date dt = new Date();
            if (getItem("ONCE_A_DAY_VALIDATION").equals(sdfOutput.format(dt))) {
                isCurrentDay = true;
            } else {
                isCurrentDay = false;
            }
            return isCurrentDay;
        } catch (Exception e) {
            return false;
        }
    }

    public static void setScreenSize(int width, int height) {
        try {
            prop.setProperty("SCREEN_SIZE", "" + width + "," + height);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static boolean hasImporteddata() {
        return hasImporteddata;
    }

    public static void setHasImporteddata(boolean hasImporteddata) {
        Settings.hasImporteddata = hasImporteddata;
    }

    public static Dimension getScreenSize() {
        try {
            String[] data = prop.getProperty("SCREEN_SIZE").split(",");
            Dimension dim = new Dimension(Integer.parseInt(data[0]), Integer.parseInt(data[1]));
            return dim;
        } catch (Exception e) {
            return null;
        }
    }

    public static int getTradingTimeoutMode() {
        return tradingTimeoutMode;
    }

    public static void setTradingTimeoutMode(int tradingTimeoutMode) {
        Settings.tradingTimeoutMode = tradingTimeoutMode;
        setItem("TRADING_TIMEOUT", "" + tradingTimeoutMode);
    }

    public static String getStocksValuation() {
        if (stocksValuation == null) {
            setStocksValuation(getItem("STOCKS_VALUATION"));
        }
        return stocksValuation;
    }

    public static void setStocksValuation(String stocksValuation) {
        Settings.stocksValuation = stocksValuation;
        setItem("STOCKS_VALUATION", stocksValuation);
    }

    public static String getIndexValuation() {
        if (indexValuation == null) {
            setIndexValuation(getItem("INDEX_VALUATION"));
        }
        return indexValuation;
    }

    public static void setIndexValuation(String indexValuation) {
        Settings.indexValuation = indexValuation;
        setItem("INDEX_VALUATION", indexValuation);
    }

    public static String getReskFreeInt() {
        if (reskFreeInt == null) {
            setReskFreeInt(getItem("RISK_FREE_RATE"));
        }
        return reskFreeInt;
    }

    public static void setReskFreeInt(String reskFreeInt, String indexValuation, String stocksValuation) {
        setReskFreeInt(reskFreeInt);
        setIndexValuation(indexValuation);
        setStocksValuation(stocksValuation);
    }

    private static void setReskFreeInt(String reskFreeInt) {
        Settings.reskFreeInt = reskFreeInt;
        setItem("RISK_FREE_RATE", reskFreeInt);
    }

    public static void setGlobalContentNewsIP(String ip) {
        globalContentNewsIp = ip;
    }

    public static String getGlobalContentNewsIp() {
        return globalContentNewsIp;
    }

    public static boolean hasWriteAccess() {
        try {
            File check = new File(Settings.getAbsolutepath() + "/writeCheck");
            OutputStream out = new FileOutputStream(check);
            out.write(0);
            out.close();
            check.deleteOnExit();
            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    public static String getBrokerInstitutionID() {
        return brokerInstitutionID;
    }

    public static void setBrokerInstitutionID(String brokerInstitutionID) {
        Settings.brokerInstitutionID = brokerInstitutionID;
    }

    public static void setKeepOrderWindowOpen(boolean keepOrderWindowOpen) {
        Settings.isKeepOrderWindowOpen = keepOrderWindowOpen;
        if (isKeepOrderWindowOpen)
            setItem("KEEP_ORDER_WINDOW_OPEN", "1");
        else
            setItem("KEEP_ORDER_WINDOW_OPEN", "0");
    }

    public static boolean isKeepOrderWindowOpen() {
        return isKeepOrderWindowOpen;
    }

    public static ArrayList<JInternalFrame> getFocusFlowList() {
        return focusFlowList;
    }

    public static void addToFocusFlowList(JInternalFrame frame) {
        if (Settings.focusFlowList.size() == 0) {
            Settings.focusFlowList.add(0, frame);
        } else {
            if (Settings.getFormFocusFlowList(frame) == null) {
                Settings.focusFlowList.add(Settings.focusFlowList.size(), frame);
            } else {
                Settings.removeFormFocusFlowList(frame);
                Settings.focusFlowList.add(Settings.focusFlowList.size(), frame);
            }
        }
    }

    public static JInternalFrame getFormFocusFlowList(int index) {
        if (Settings.focusFlowList.size() > 0) {
            return Settings.focusFlowList.get(index);
        } else {
            return null;
        }
    }

    public static JInternalFrame getFormFocusFlowList(JInternalFrame frame) {
        int i = 0;
        for (; i < Settings.focusFlowList.size(); i++) {
            if (Settings.focusFlowList.get(i).equals(frame)) {
                return Settings.focusFlowList.get(i);
            }
        }
        return null;
    }

    public static void removeFormFocusFlowList(int index) {
        Settings.focusFlowList.remove(index);
        Settings.focusFlowList.trimToSize();
    }

    public static void removeFormFocusFlowList(JInternalFrame frame) {
        Settings.focusFlowList.remove(frame);
        Settings.focusFlowList.trimToSize();
    }

    public static String getMwTradingUrl() {
        return mwTradingUrl;
    }

    public static void setMwTradingUrl(String mwTradingUrl) {
        Settings.mwTradingUrl = mwTradingUrl;
    }

    public static int getAfterMarketValuationMode() {
        return afterMarketValuationMode;
    }

    public static void setAfterMarketValuationMode(int stateWiseCalculationsEnable) {
        Settings.afterMarketValuationMode = stateWiseCalculationsEnable;
        setItem("AFTER_MARKET_VALUATION_MODE", "" + stateWiseCalculationsEnable);

    }
}
