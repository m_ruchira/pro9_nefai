package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 3, 2004
 * Time: 2:24:55 PM
 */
public class BooleanTransferObject {
    private boolean value;

    public boolean getValue() {
        return value;
    }

    public BooleanTransferObject setValue(boolean value) {
        this.value = value;
        return this;
    }
}

