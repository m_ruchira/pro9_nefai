package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 2, 2004
 * Time: 2:37:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class IP {

    public static final int DYNAMIC = 0;
    public static final int STATIC = 1;
    public static final int LAST = 2;

    public static final int PRIMARY = 0;
    public static final int SECONDARAY = 1;

    private String ip;
    private int type;
    private int ipClass;

    public IP(String ip, int type, int ipClass) {
        this.ip = ip;
        this.type = type;
        this.ipClass = ipClass;
    }

    public int getType() {
        return type;
    }

    public String getIP() {
        return ip;
    }

    public String toString() {
        return ip;
    }

    public int getIpClass() {
        return ipClass;
    }
}
