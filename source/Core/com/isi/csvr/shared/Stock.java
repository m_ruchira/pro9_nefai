/**
 * @(#)Stock.java Oracle JDeveloper 3.1	May 25 2000
 * Copyright (c) 2000 Integrated Systems International (Pvt)Ltd. All Rights Reserved.
 * CopyrightVersion 1.0_beta
 **/

package com.isi.csvr.shared;

import com.isi.csvr.Client;
import com.isi.csvr.forex.ForexStore;
import com.isi.csvr.news.NewsProvidersStore;
import com.isi.csvr.radarscreen.RadarScreenData;
import com.isi.csvr.radarscreen.RadarScreenInterface;
import com.isi.csvr.datastore.*;

import java.io.Serializable;
import java.util.*;

/**
 * @author Uditha Nagahawatta
 * @version 1.0
 */

/* complete revison made by Nishantha Fernand0 */
/* to include marketdepth, accomadate calculated information from exchanges
/* Version 2.0 */
/* 28th June 2001 */
public class Stock implements Serializable, FunctionStockInterface {

    private static final long serialVersionUID = 123456789123456L;

    public static int MIN = -1;
    public static int MAX = 1;
    public static int NORMAL = 0;

    //indicate limits of 52Weeks High or Low
    public static int BEYOND52WEEKHIGH = 1;
    public static int BEYOND52WEEKLOW = -1;
    public static int NORMAL52WEEK = 0;
    public static int OPEN_ABOVE_CLOSED = 1;

    // Todo - SNE related
    public static final char BATE_DEPTH_ASK_ORDER_NO = '(';
    public static final char BATE_DEPTH_ASK_FILL_FLAG = ')';
    public static final char BATE_DEPTH_ASK_TIFF_FLAG = '^';
    public static final char BATE_DEPTH_ASK_MIN_AMOUNT = '!';
    public static final char BATE_DEPTH_ASK_MKT_CODE = '@';

    public static final char BATE_DEPTH_BID_ORDER_NO = '.';
    public static final char BATE_DEPTH_BID_FILL_FLAG = ',';
    public static final char BATE_DEPTH_BID_TIFF_FLAG = '\'';
    public static final char BATE_DEPTH_BID_MIN_AMOUNT = '/';
    public static final char BATE_DEPTH_BID_MKT_CODE = '\\';

    public static final char BATE_DEPTH_BIDASK_ORDER_NO = '{';
    public static final char BATE_DEPTH_BIDASK_FILL_FLAG = '}';
    public static final char BATE_DEPTH_BIDASK_TIFF_FLAG = '[';
    public static final char BATE_DEPTH_BIDASK_MIN_AMOUNT = ']';
    public static final char BATE_DEPTH_BIDASK_MKT_CODE = '?';

    public static final char BATE_OPTION_TYPE = '?';
    public static final char BATE_EQUITY_SYMBOL = '*';


    public static final int OHLC_MAP_COUNT = 41;
    public static final int TICK_MAP_LENGTH = 10;

    private long lastTradeTime;  // this is the time which received the last trade
    private long lastUpdate;     // this change for any updates made to this stock obj instance
    private boolean lastTradeUpdated = false;
    private boolean lastTradeUpdatedUpper = false;
    private boolean lastTradeUpdatedMiddle = false;
    private boolean lastTradeUpdatedLower = false;

    private int tickMap;

    private String longDescription;
    private String shortDescription;
    private String companyCode;
    private String symbol;
    private String symbolCode;
    private String exchange;
    private String key;
    private double turnoverValue;
    private long turnoverFlag = System.currentTimeMillis();
    private String assetClass;
    private String sectorCode;
    private byte tradeTick = Settings.TICK_NOCHANGE;
    private long lastTradeFlag = System.currentTimeMillis();
    private double lastTradeValue;
    private long tradeQuantity;
    private long tradeQuantityFlag = System.currentTimeMillis();
    private double high;
    private double low;
    private double change;
    private long volume;
    private long volumeFlag = System.currentTimeMillis();
    private double percentChange;
    private double previousClosed;
    private double bestBidPrice;
    private long bestBidPriceFlag = System.currentTimeMillis();
    private double bestAskPrice;
    private long bestAskPriceFlag = System.currentTimeMillis();
    private double closingVWAP;
    private double TWAP;
    private int loosingCategory=-1;

    private int minOrMax = 0;
    private int week52HighLowTracker = 0;

    private double spread = 0;
    private double pctSpread = 0;
    private double range = 0;
    private double pctRange = 0;
    public double bidaskRatio = Double.NaN;// bid/ask
    public double cashFlowRatio = Double.NaN;// bid/ask

    private int instrumentType = Meta.INSTRUMENT_EQUITY;
    private int insTypeTradabeRights = Constants.TRADABLE_RIGHTS;
    private int groupStatus = 0;
    private int lotSize = 0;
    private int bondType = 0;
    private int rateCalcType = 0;
    private int dayCountMethod = 0;
    private int numberOfRecordDays = 0;
    private int interestDayType = 0;
    private int faceValueDimension = 0;
    private int numberOfCashflows = 0;
    private int daysToFirstCashFlow = 0;
    private int daysInFirstrecord = 0;
    private int calcConvention = 0;
    private String firstSubscriptionStartDate = null;
    private String firstSubscriptionEndDate = null;
    private String secondSubscriptionStartDate = null;
    private String secondSubscriptionEndDate = null;

    private int symbolType = Meta.SYMBOL_TYPE_EQUITY;

    private int eventNewsAvailability = Constants.ITEM_NOT_AVAILABLE;

    private int newsTimeoutPeriod = Constants.NEWS_TIME_OUT_TIME;
    private boolean predictedIndex;

    private long timeOffset;
    private long latestPriceUpdsateTime = 0;
    public static transient boolean symbolAlias = false;
    private byte decimalCount = 2;
    private boolean baseSymbolUpdated = false;
    public transient double[] ohlcMapvalues = new double[OHLC_MAP_COUNT + 2];//{23,23.5,23.25,23.25,23.5,23.25,23.5,23.25,23.25,23.25,23.25,23};
//	private double settlementPrice;
    private double currentPrice;
    private String series = null;
    private long snapShotUpdatedtime = 0l;
    private int avgVolumeMode = 0;

    private boolean isSymbolEnabled = false;

    // TOP/TOV/TCP/TCV
    private double top;
    private long tov;
    private double tcp;
    private long tcv;

    private Hashtable<Integer, SymbolEvent> symbolEventHashTable;

    private static final int ID_SYMBOL = 1;
    private static final int ID_EXCHANGE = 2;
    private static final int ID_INSTRUMENT_TYPE = 3;
    private static final int ID_SYMBOL_CODE = 4;
    private static final int ID_SHORT_DESCRYPTION = 5;
    private static final int ID_LONG_DESCRYPTION = 6;
    private static final int ID_SYMBOL_ALIAS = 7;
    private static final int ID_SUB_MARKET = 8;
    private static final int ID_CURRENCY = 10;
    private static final int ID_TURNOVER = 11;
    private static final int ID_VOLUME = 12;
    private static final int ID_LAST_TRADE_TIME = 13;
    private static final int ID_DISPLAY_SYMBOL = 14;
    private static final int ID_BASE_SYMBOL = 15;
    private static final int ID_NET_ASSET = 16;
    private static final int ID_NO_OF_TRADES = 17;
    private static final int ID_LAST_TRADED_DATE = 18;
    private static final int ID_LAST_TRADED_PRICE = 19;
    private static final int ID_TODAYS_CLOSE = 20;
    private static final int ID_TODAYS_OPEN = 21;
    private static final int ID_AVG_TRADE_PRICE = 22;
    private static final int ID_AVG_TRADE_VOLUME = 23;
    private static final int ID_MAX_PRICE = 23;
    private static final int ID_MIN_PRICE = 24;
    private static final int ID_HIGH_PRICE_52WKS = 25;
    private static final int ID_HIGH_PRICE_DATE_52WKS = 26;
    private static final int ID_LOW_PRICE_52WKS = 27;
    private static final int ID_LOW_PRICE_DATE_52WKS = 28;
    private static final int ID_FOREX_CURRENCY = 29;
    private static final int ID_BENCHMARK = 30;
    private static final int ID_PER = 31;
    private static final int ID_LOT_SIZE = 32;
    private static final int ID_FUND_VALUE = 33;
    private static final int ID_MARKET_CAP = 34;
    private static final int ID_FOREX_BASE_SYMBOL = 35;
    private static final int ID_PERFORMANCE_YTD = 36;
    private static final int ID_YIELD = 37;
    private static final int ID_FOREX_APPENDER = 38;
    private static final int ID_FACE_VALUE = 39;
    private static final int ID_FOREX_DEVIATION = 40;
    private static final int ID_PERFORMANCE_3Y = 41;
    private static final int ID_LISTED_SHARES = 42;
    private static final int ID_FOREX_QUOTE_ID = 43;
    private static final int ID_PERFORMANCE_5Y = 44;
    private static final int ID_PAID_SHARES = 45;
    private static final int ID_FOREX_BUY_DATE = 46;
    private static final int ID_MGT_FEE = 47;
    private static final int ID_COUPON_VALUE = 48;
    private static final int ID_FOREX_SELL_DATE = 49;
    private static final int ID_DEADLINE = 50;
    private static final int ID_COUPON_DATE = 51;
    private static final int ID_IS_FOREX_BEST_QUOTE = 52;
    private static final int ID_NET_PROFIT = 53;
    private static final int ID_FOREX_BUY = 54;
    private static final int ID_NET_PROFIT_DATE = 55;
    private static final int ID_FOREX_SELL = 56;
    private static final int ID_EPS = 57;
    private static final int ID_PERFORMANCE_12M = 58;
    private static final int ID_BEST_OFFER_QTY = 59;
    private static final int ID_AMC = 60;
    private static final int ID_COUNTRY = 61;
    private static final int ID_BEST_BID_QTY = 62;
    private static final int ID_RISK_1Y = 63;
    private static final int ID_NO_OF_ASKS = 64;
    private static final int ID_MIN_INIT_PURCHASE = 65;
    private static final int ID_NO_OF_BIDS = 66;
    private static final int ID_FEND_LOAD = 67;
    private static final int ID_TOT_BID_QTY = 68;
    private static final int ID_BEND_LOAD = 69;
    private static final int ID_TOT_ASK_QTY = 70;
    private static final int ID_FUND_SIZE = 71;
    private static final int ID_HIGH_ASKS = 72;
    private static final int ID_SUBS_PURCHASE = 73;
    private static final int ID_LOW_BID = 74;
    private static final int ID_TOT_ASSETS = 75;
    private static final int ID_LAST_ASK_PRICE = 76;
    private static final int ID_TOT_ASSETS_DATE = 77;
    private static final int ID_LAST_BID_PRICE = 78;
    private static final int ID_BENCHMARK_IND_VALUES = 79;
    private static final int ID_LAST_ASK_QTY = 80;
    private static final int ID_LAST_BID_QTY = 81;
    private static final int ID_SIMPLE_AVG_ASK = 82;
    private static final int ID_WEIGHTED_AVG_ASK = 83;
    private static final int ID_SIMPLE_AVG_BID = 84;
    private static final int ID_WEIGHTED_AVG_BID = 85;
    private static final int ID_SHARES_FOR_FOREIGN = 86;
    private static final int ID_OPEN_INTEREST = 87;
    private static final int ID_EXPIRATION_DATE = 88;
    private static final int ID_STRIKE_PRICE = 89;
    private static final int ID_EQUITY_SYMBOL = 90;
    private static final int ID_CONTRACT_HIGH = 91;
    private static final int ID_CONTRACT_LOW = 92;
    private static final int ID_REF_PRICE = 93;
    private static final int ID_ISIN_CODE = 94;
    private static final int ID_PBR = 95;
    private static final int ID_CASH_IN_ORDER = 96;
    private static final int ID_CASH_IN_VOLUME = 97;
    private static final int ID_CASH_IN_TURNOVER = 98;
    private static final int ID_CASH_OUT_ORDER = 99;
    private static final int ID_CASH_OUT_VOLUME = 100;
    private static final int ID_CASH_OUT_TURNOVER = 101;
    private static final int ID_SYMBOL_STATUS = 102;
    private static final int ID_EVENT_AVAILABILITY = 103;
    private static final int ID_NEWS_AVAILABILITY = 104;
    private static final int ID_NEWS_TIMEOUT_PERIOD = 105;
    private static final int ID_BULB_STATUS = 106;
    private static final int ID_CHG_IN_OPEN_INTEREST = 107;
    private static final int ID_MARKET_CENTER = 108;
    private static final int ID_SEVEN_DAY_AVG_VOLUME = 109;
    private static final int ID_THIRTY_DAY_AVG_VOLUME = 110;
    private static final int ID_NINTY_DAY_AVG_VOLUME = 111;
    private static final int ID_ANNUAL_VOLATILITY = 112;
    private static final int ID_DAILY_VOLATILITY = 113;
    private static final int ID_PARAMS = 114;
    private static final int ID_PATH = 115;
    private static final int ID_LAST_TRADE_DATE = 116;
    private static final int ID_ANNOUNCEMENT_AVAILABILITY = 117;
    private static final int ID_UNITS = 118;
    private static final int ID_OPTION_TYPE = 119;
    private static final int ID_SETTLEMENT_PRICE = 120;
    private static final int CHANGE_VWAP_BASE = 122;
    private static final int PER_CHANGE_VWAP_BASE = 123;

    private static final int ID_PREDICTED_INDEX = 124;
    private static final int ID_PREDICTED_INDEX_CHANGE = 125;
    private static final int ID_PREDICTED_INDEX_PCT_CHANGE = 126;

    //byate codes related to BONDS
    private static final int BATE_MF_ISIN_CODE = 127;//	#
    private static final int BATE_MF_CURRENCY = 128;//	B
    private static final int BATE_MF_NAV = 129;//	F
    private static final int BATE_MF_BENCHMARK = 130;//	a
    private static final int BATE_MF_SIZE = 131;//	b
    private static final int BATE_MF_PERFORMACE_YTD = 132;//	c
    private static final int BATE_MF_PERFORMACE_3Y = 133;//	e
    private static final int BATE_MF_PERFORMACE_5Y = 134;//	f
    private static final int BATE_MF_MANAGEMENT_FEE = 135;//	z
    private static final int BATE_MF_DEALING_DEADLINE = 136;//	0
    private static final int BATE_MF_PERFORMACE_12M = 137;//	1
    private static final int BATE_MF_FUND_NAME = 138;//	2
    private static final int BATE_MF_AMC = 139;//	3
    private static final int BATE_MF_COUNTRY = 140;//	4
    private static final int BATE_MF_RISK1Y = 141;//	5
    private static final int BATE_MF_MIN_INIT_PURCHASE = 142;//	6
    private static final int BATE_MF_F_END_LOAD = 143;//	7
    private static final int BATE_MF_B_END_LOAD = 144;//	8
    private static final int BATE_MF_HIGH_ASK = 145;//	u
    private static final int BATE_MF_LOW_BID = 146;//	v
    private static final int BATE_MF_LAST_ASK = 147;//	w
    private static final int BATE_MF_LAST_BID = 148;//	x
    private static final int BATE_MF_LAST_ASK_QTY = 149;//	y
    private static final int BATE_MF_SETTLEMENT_DATE = 150;//		Z  x
    private static final int BATE_MF_ISSUED_AMOUNT = 151;//		r
    private static final int BATE_MF_DATE_OF_ISSUE = 152;//		s
    private static final int BATE_MF_OUT_AMOUNT = 153;//		t
    private static final int BATE_MF_MATURITY_DATE = 154;//		u
    private static final int BATE_MF_COUPON_RATE = 155;//		v
    private static final int BATE_MF_NOMINAL_VALUE = 156;//		w
    private static final int BATE_MF_ISSUE_CURRENCY = 157;//		x
//    private static final int BATE_CURRENCY                  =158;//	B
//    private static final int BATE_MF_LOT_SIZE               =159;//		y
    private static final int BATE_MF_LISTING_DATE = 160;//		z
    private static final int BATE_COUPON_DATE = 161;//	h
    private static final int BATE_ISIN_CODE = 162;//	#
//    private static final int BATE_MF_BOND_TYPE              =163;//		A
    private static final int BATE_MF_TRADED_THROUGH_DATE = 164;//		B
    private static final int BATE_MF_RATE_CALC_TYPE = 165;//		C
    private static final int BATE_MF_DAY_COUNT_METHOD = 166;//		D
    private static final int BATE_MF_LOAN_NUMBER = 167;//		E
    private static final int BATE_MF_EXT_BOND_TYPE = 168;//		F
    private static final int BATE_MF_EARLY_LATE_MATURITY = 169;//		G
    private static final int BATE_MF_MATURITY_VALUE = 170;//		H
    private static final int BATE_MF_YEARLY_COUPON_FREQ = 171;//		I
    private static final int BATE_MF_RECORD_DATE_TYPE = 172;//		J
    private static final int BATE_MF_NO_OF_RECORD_DAYS = 173;//		K
    private static final int BATE_MF_INTEREST_FROM_DATE = 174;//		L
    private static final int BATE_MF_INTEREST_DAY_TYPE = 175;//		M
    private static final int BATE_MF_PREV_COUPON_DATE = 176;//		N
    private static final int BATE_MF_LAST_COUPON_DATE = 177;//		O
    private static final int BATE_MF_COUPON_RECORD_DATE = 178;//		P
    private static final int BATE_FACE_VALUE = 179;//	d
    private static final int BATE_MF_FACE_VALUE_DIMENSION = 180;//		Q
    private static final int BATE_MF_NO_OF_CASHFLOWS = 181;//		R
    private static final int BATE_MF_COUPON_FREQ = 182;//		S
    private static final int BATE_MF_DAY_TO_FIRST_CASHFLOW = 183;//		T
    private static final int BATE_MF_DAY_IN_FIRST_RECORD = 184;//		U
    private static final int BATE_MF_INDEX_COUPON_ADJ = 185;//		V
    private static final int BATE_MF_ACCRUED_INTEREST = 186;//		W
    private static final int BATE_MF_INDEX_FACTOR = 187;//		X
    private static final int BATE_MF_CALC_CONVENTION = 188;//		Y

    private static final int ID_YTD_STOCK_VALUE = 189;
    private static final int ID_GROUP_NAME = 191;
    private static final int ID_GROUP_STATUS = 192;
    private static final int ID_TOP_TCP_VALUE = 193;
    private static final int ID_WEEK_AVG_PRICE = 194;
    private static final int ID_MONTH_AVG_PRICE = 195;
    public static final int BATE_INTRINSIC_VALUE = 196; // '\\'
    private static final int FIRST_SUBSCRIPTION_START_DATE=197;
    private static final int FIRST_SUBSCRIPTION_END_DATE=198;
    private static final int SECOND_SUBSCRIPTION_START_DATE=199;
    private static final int SECOND_SUBSCRIPTION_END_DATE=200;
    private static final int FIRST_SUBSCRIPTION_START_DATE_HIJRI=201;
    private static final int FIRST_SUBSCRIPTION_END_DATE_HIJRI=202;
    private static final int SECOND_SUBSCRIPTION_START_DATE_HIJRI=203;
    private static final int SECOND_SUBSCRIPTION_END_DATE_HIJRI=204;

    // for TOP,TOV, TCP, TCV
    private static final int ID_TOP=205;
    private static final int ID_TOV=206;
    private static final int ID_TCP=207;
    private static final int ID_TCV=208;

    private static final int ID_SHARE_CAPITAL=209;
    private static final int ID_CURRENT_ADJUSTED_VALUE=210;
    private static final int ID_STATIC_MIN=211;
    private static final int ID_STATIC_MAX=212;


    private Hashtable<Integer, String> stringTable = new Hashtable<Integer, String>();
    private Hashtable<Integer, Long> longTable = new Hashtable<Integer, Long>();
    private Hashtable<Integer, Double> doubleTable = new Hashtable<Integer, Double>();
    private PeriodicIndicator periodicIndicator;

    public PeriodicIndicator getPeriodicIndicator(){
      return periodicIndicator ;
    }

    public Stock(String newSymbol, String exchange, int instrument) {
        symbolCode = newSymbol;
        instrumentType = instrument;
        symbolType = SharedMethods.getSymbolType(instrument);
        try {
            symbol = symbolCode.split(Constants.MARKET_SEPERATOR_CHARACTER)[0];
            shortDescription = symbol;
//			longDescription = symbol;
        } catch (Exception e) {
            symbol = newSymbol;
            shortDescription = symbol;
//			longDescription = symbol;
        }
        this.exchange = exchange;
        key = SharedMethods.getKey(exchange, symbolCode, instrumentType);
        symbolEventHashTable = new Hashtable<Integer, SymbolEvent>();
        init();
    }

    public void setDisplaySymbol(String value) {
        if (value != null) {
            stringTable.put(ID_DISPLAY_SYMBOL, value);
        }
    }

    /*private void setExchange(String value){
         stringTable.put(ID_EXCHANGE, value);
     }*/

    /*private void setKey(String value){
         stringTable.put(ID_KEY, value);
     }*/


    public String getSeries() {
        if (series == null) {
            series = "";
        }
        return series;
    }

    public void setSeries(String series) {
        this.series = series;
    }

    public int getGroup() {
        return (instrumentType & 0xff00);
    }

    public void setInstrumentType(int type) {
        this.instrumentType = type;
    }


    public int getInstrumentType() {
        return instrumentType;// & 0x00ff;
    }


    public double[] getOHLCArray() {
        return ohlcMapvalues;
    }

    public void init() {
        assetClass = Constants.DEFAULT_ASSETCLASS;
        sectorCode = Constants.DEFAULT_SECTOR;
        periodicIndicator=new PeriodicIndicator();
        initTickMap();
    }

    public void initTickMap() {
        tickMap = 0;
    }

    private void setNetAssetValue(double value) {
        doubleTable.put(ID_NET_ASSET, value);
    }

    private void setLastTradedDate(long value) {
        longTable.put(ID_LAST_TRADED_DATE, value);
    }

    private void setLastTradedPrice(double value) {
        doubleTable.put(ID_LAST_TRADED_PRICE, value);
    }

    public void setTodaysClose(double value) {
        doubleTable.put(ID_TODAYS_CLOSE, value);
    }

    public void setTodaysOpen(double value) {
        doubleTable.put(ID_TODAYS_OPEN, value);
    }

    private void setAvgTradePrice(double value) {
        doubleTable.put(ID_AVG_TRADE_PRICE, value);
    }

    private void setAvgTradeVolume(long value) {
        longTable.put(ID_AVG_TRADE_VOLUME, value);
    }

    private void setMaxPrice(double value) {
        doubleTable.put(ID_MAX_PRICE, value);
        calculateIsMinOrMax();
    }

    private void setMinPrice(double value) {
        doubleTable.put(ID_MIN_PRICE, value);
        calculateIsMinOrMax();
    }

    private void setHighPriceof52Weeks(double value) {
        doubleTable.put(ID_HIGH_PRICE_52WKS, value);
        calculateIsBeyond52WeekHighorLow();
    }

    private void setHighPriceDateof52Weeks(long value) {
        longTable.put(ID_HIGH_PRICE_DATE_52WKS, value);
    }

    private void setLowPriceof52Weeks(double value) {
        doubleTable.put(ID_LOW_PRICE_52WKS, value);
        calculateIsBeyond52WeekHighorLow();
    }

    private void setLowPriceDateof52Weeks(long value) {
        longTable.put(ID_LOW_PRICE_DATE_52WKS, value);
    }

    private void setForexCurrency(String value) {
        if (value != null) {
            stringTable.put(ID_FOREX_CURRENCY, value);
            (ForexStore.getSharedInstance().getForexObject(exchange, symbol, instrumentType)).setForexCurrency(value);
        }
    }

    private void setBenchmark(String value) {
        if ((value != null)) {
            stringTable.put(ID_BENCHMARK, value);
        }
    }

    private void setPERation(double value) {
        doubleTable.put(ID_PER, value);
    }

    public void setLotSize(long value) {
        longTable.put(ID_LOT_SIZE, value);
    }

    private void setFundValue(double value) {
        doubleTable.put(ID_FUND_VALUE, value);
    }

    private void setMarketCap(double value) {
            doubleTable.put(ID_MARKET_CAP, value / 1000);
    }

    private void setForexBaseSymbol(String value) {
        if (value != null) {
            stringTable.put(ID_FOREX_BASE_SYMBOL, value);
        }
    }

    private void setPerformenceYTD(double value) {
        doubleTable.put(ID_PERFORMANCE_YTD, value);
    }

    private void setYield(double value) {
        doubleTable.put(ID_YIELD, value);
    }

    private void setForexAppender(String value) {
        if (value != null) {
            stringTable.put(ID_FOREX_APPENDER, value);
        }
    }

    private void setFaceValue(double value) {
        doubleTable.put(ID_FACE_VALUE, value);
    }

    private void setYTDStockValue(double value) {
        doubleTable.put(ID_YTD_STOCK_VALUE, value);
    }

    private void setForexDeviation(double value) {
        doubleTable.put(ID_FOREX_DEVIATION, value);
        (ForexStore.getSharedInstance().getForexObject(exchange, symbol, instrumentType)).setForexDeviation(value);
    }

    private void setPerformance3Y(double value) {
        doubleTable.put(ID_PERFORMANCE_3Y, value);
    }

    public void setListedShares(long newListedShares) {
        longTable.put(ID_LISTED_SHARES, newListedShares);
        if ((newListedShares > 0) && (lastTradeValue > 0)) {
            setMarketCap((newListedShares * lastTradeValue) / 1000);
        }
    }

    private void setForexQuoteID(String value) {
        if (value != null) {
            stringTable.put(ID_FOREX_QUOTE_ID, value);
        }
    }

    private void setPerformance5Y(double value) {
        doubleTable.put(ID_PERFORMANCE_5Y, value);
    }

    private void setPaidShares(long value) {
        longTable.put(ID_PAID_SHARES, value);
    }

    private void setForexBuyDate(String value) {
        if (value != null) {
            stringTable.put(ID_FOREX_BUY_DATE, value);
            (ForexStore.getSharedInstance().getForexObject(exchange, symbol, instrumentType)).setForexBuyDate(value);
        }
    }

    private void setMgtFee(double value) {
        doubleTable.put(ID_MGT_FEE, value);
    }

    private void setCouponValue(double value) {
        doubleTable.put(ID_COUPON_VALUE, value);
    }

    private void setForexSellDate(String value) {
        if (value != null) {
            stringTable.put(ID_FOREX_SELL_DATE, value);
            (ForexStore.getSharedInstance().getForexObject(exchange, symbol, instrumentType)).setForexSellDate(value);
        }
    }

    private void setDeadline(long value) {
        longTable.put(ID_DEADLINE, value);
    }

    private void setCouponDate(long value) {
        longTable.put(ID_COUPON_DATE, value);
    }

    private void setForexBestQuote(int value) {
        longTable.put(ID_IS_FOREX_BEST_QUOTE, (long) value);
    }

    private void setNetProfit(double value) {
        doubleTable.put(ID_NET_PROFIT, value);
    }

    private void setForexBuy(double value) {
        doubleTable.put(ID_FOREX_BUY, value);
    }

    private void setNetProfitDate(long value) {
        longTable.put(ID_NET_PROFIT_DATE, value);
    }

    private void setForexSell(double value) {
        doubleTable.put(ID_FOREX_SELL, value);
    }

    private void setEPS(double value) {
        doubleTable.put(ID_EPS, value);
    }

    private void setPerformance12M(double value) {
        doubleTable.put(ID_PERFORMANCE_12M, value);
    }

    private void setBestAskQty(long value) {
        longTable.put(ID_BEST_OFFER_QTY, value);
    }

    private void setAMC(String value) {
        if (value != null) {
            stringTable.put(ID_AMC, value);
        }
    }

    private void setCountry(String value) {
        if (value != null) {
            stringTable.put(ID_COUNTRY, value);
        }
    }

    private void setBestBidQty(long value) {
        longTable.put(ID_BEST_BID_QTY, value);
    }

    private void setRisk1Y(double value) {
        doubleTable.put(ID_RISK_1Y, value);
    }

    private void setNoOfAsks(long value) {
        longTable.put(ID_NO_OF_ASKS, value);
    }

    private void setMinInitPurchase(double value) {
        doubleTable.put(ID_MIN_INIT_PURCHASE, value);
    }

    private void setNoOfBids(long value) {
        longTable.put(ID_NO_OF_BIDS, value);
    }

    private void setFEndLoad(double value) {
        doubleTable.put(ID_FEND_LOAD, value);
    }

    private void setTotBidQty(long value) {
        longTable.put(ID_TOT_BID_QTY, value);
        setBidAskRatio();
    }

    private void setBEndLoad(double value) {
        doubleTable.put(ID_BEND_LOAD, value);
    }

    private void setTotAskQty(long value) {
        longTable.put(ID_TOT_ASK_QTY, value);
        setBidAskRatio();
    }

    private void setFundSize(long value) {
        longTable.put(ID_FUND_SIZE, value);
    }

    private void setHighAsks(double value) {
        doubleTable.put(ID_HIGH_ASKS, value);
    }

    private void setSubsPurchase(double value) {
        doubleTable.put(ID_SUBS_PURCHASE, value);
    }

    private void setLowBid(double value) {
        doubleTable.put(ID_LOW_BID, value);
    }

    private void setTotAssets(double value) {
        doubleTable.put(ID_TOT_ASSETS, value);
    }

    private void setLastAskPrice(double value) {
        doubleTable.put(ID_LAST_ASK_PRICE, value);
    }

    private void setTotAssetsDate(long value) {
        longTable.put(ID_TOT_ASSETS_DATE, value);
    }

    private void setLastBidPrice(double value) {
        doubleTable.put(ID_LAST_BID_PRICE, value);
    }

    private void setBenchmarkIndValues(double value) {
        doubleTable.put(ID_BENCHMARK_IND_VALUES, value);
    }

    private void setLastAskQty(long value) {
        longTable.put(ID_LAST_ASK_QTY, value);
    }

    private void setLastBidQty(long value) {
        longTable.put(ID_LAST_BID_QTY, value);
    }

    private void setSimpleAverageAsk(double value) {
        doubleTable.put(ID_SIMPLE_AVG_ASK, value);
    }

    private void setWeightedAverageAsk(double value) {
        doubleTable.put(ID_WEIGHTED_AVG_ASK, value);
    }

    private void setSimpleAverageBid(double value) {
        doubleTable.put(ID_SIMPLE_AVG_BID, value);
    }

    private void setWeightedAverageBid(double value) {
        doubleTable.put(ID_WEIGHTED_AVG_BID, value);
    }

    private void setSharesForForeigners(long value) {
        longTable.put(ID_SHARES_FOR_FOREIGN, value);
    }

    private void setOpenInterest(long value) {
        longTable.put(ID_OPEN_INTEREST, value);
    }

    public void setExpirationDate(String date) {
        longTable.put(ID_EXPIRATION_DATE, SharedMethods.getExpirationDate(date));
    }

    public void setExpirationDateLong(long date) {
        longTable.put(ID_EXPIRATION_DATE, date);
    }

    public void setStrikePrice(double value) {
        doubleTable.put(ID_STRIKE_PRICE, value);
    }

    public void setEquitySymbol(String value) {
        if (value != null) {
            stringTable.put(ID_EQUITY_SYMBOL, value);
        }
    }

    private void setContractHigh(double value) {
        doubleTable.put(ID_CONTRACT_HIGH, value);
    }

    private void setContractLow(double value) {
        doubleTable.put(ID_CONTRACT_LOW, value);
    }

    private void setRefPrice(double value) {
        doubleTable.put(ID_REF_PRICE, value);
        setTopTcpValue(value);
    }

    public void setISINCode(String value) {
        if (value != null) {
            stringTable.put(ID_ISIN_CODE, value);
        }
    }

    private void setPBRatio(double value) {
        doubleTable.put(ID_PBR, value);
    }

    private void setCashInOrder(long value) {
        longTable.put(ID_CASH_IN_ORDER, value);
    }

    private void setCashInVolume(long value) {
        longTable.put(ID_CASH_IN_VOLUME, value);
    }

    private void setCashInTurnover(double value) {
        doubleTable.put(ID_CASH_IN_TURNOVER, value);
    }

    private void setCashOutOrder(long value) {
        longTable.put(ID_CASH_OUT_ORDER, value);
    }

    private void setCashOutVolume(long value) {
        longTable.put(ID_CASH_OUT_VOLUME, value);
    }

    private void setCashOutTurnover(double value) {
        doubleTable.put(ID_CASH_OUT_TURNOVER, value);
    }

    public void setSymbolStatus(long value) {
        longTable.put(ID_SYMBOL_STATUS, value);
    }

    private void setEventAvailability(long value) {
        longTable.put(ID_EVENT_AVAILABILITY, value);
    }

    public void setNewsAvailability(int value) {
        longTable.put(ID_NEWS_AVAILABILITY, (long) value);
        setBulbStatus(SharedMethods.getStockBulbStatus(value, getAnnouncementAvailability()));
    }

    public void setNewsTimeoutPeriod(long value) {
        longTable.put(ID_NEWS_TIMEOUT_PERIOD, value);
    }

    private void setBulbStatus(int value) {
        longTable.put(ID_BULB_STATUS, (long) value);
    }

    private void setChgInOpenInterest(double value) {
        doubleTable.put(ID_CHG_IN_OPEN_INTEREST, value);
    }

    public void setMarketCenter(String value) {
        if ((value != null) && (value.trim().length() > 0)) {
            stringTable.put(ID_MARKET_CENTER, value);
        }
    }

    private void setSevenDayAvgVolume(long value) {
        longTable.put(ID_SEVEN_DAY_AVG_VOLUME, value);
    }

    private void setThirtyDayAvgVolume(long value) {
        longTable.put(ID_THIRTY_DAY_AVG_VOLUME, value);
    }

    private void setNintyDayAvgVolume(long value) {
        longTable.put(ID_NINTY_DAY_AVG_VOLUME, value);
    }

    private void setAnnualVolatility(double value) {
        doubleTable.put(ID_ANNUAL_VOLATILITY, value);
    }

    private void setDailyVolatility(double value) {
        doubleTable.put(ID_DAILY_VOLATILITY, value);
    }

    public void setParams(String value) {
        if (value != null) {
            stringTable.put(ID_PARAMS, value);
        }
    }

    public void setPath(byte value) {
        longTable.put(ID_PATH, (long) value);
    }

    public void setLastTradeDate(long value) {
        longTable.put(ID_LAST_TRADE_DATE, value);
    }

    public void setAnnouncementAvailability(int status) {
        longTable.put(ID_ANNOUNCEMENT_AVAILABILITY, (long) status);
        setBulbStatus(SharedMethods.getStockBulbStatus(getNewsAvailability(), status));
    }

    public void setMarketID(String value) {
        if (value != null) {
            stringTable.put(ID_SUB_MARKET, value);
        }
    }

    public void setAlias(String value) {
        if (value != null) {
            stringTable.put(ID_SYMBOL_ALIAS, value);
        }
    }

    public void setUnit(String value) {
        if ((value != null) && (value.trim().length() > 0)) {
            stringTable.put(ID_UNITS, value);
        }
    }

    public void setOptionType(long value) {
        longTable.put(ID_OPTION_TYPE, value);
    }

    private void setSettlementPrice(double value) {
        doubleTable.put(ID_SETTLEMENT_PRICE, value);
    }

    private void setIntrinsicValue(double value) {
        doubleTable.put(BATE_INTRINSIC_VALUE, value);
        /*if(insTypeTradabeRights == 66){
            doubleTable.put(BATE_INTRINSIC_VALUE, value);
        } else {
            doubleTable.put(BATE_INTRINSIC_VALUE, Double.NaN);
        }*/
    }

    public void setData(String data, int decimalFactor, boolean fromZip) throws Exception {
        if (data == null)
            return;
        char tag;

        setLastUpdatedTime(Client.getInstance().getMarketTime());
        StringTokenizer tokenizer = new StringTokenizer(data, Meta.FD);
//		for (int i = 0; i < data.length; i++) {
        while (tokenizer.hasMoreElements()) {
//			tag = data[i].charAt(0);
            String element = tokenizer.nextToken();
            tag = element.charAt(0);
            switch (tag) {
                case 'B':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            break;
                        default:
                            setCurrencyCode(element.substring(1));
                            break;
                    }
                    break;
                case 'C':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            break;
                        default:
                            setTurnover(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case 'D':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            break;
                        default:
                            setVolume(SharedMethods.getLong(element.substring(1)));
                            if ((!fromZip) && (getLastTradeValue() > 0)) {
                                lastTradeUpdated = true;
                                lastTradeUpdatedLower = true;
                                lastTradeUpdatedMiddle = true;
                                lastTradeUpdatedUpper = true;
                            }
                            break;
                    }
                    break;
                case 'E':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            lastTradeTime = getTradeTimeForex(element.substring(1));
                            break;
                        default:
                            try {
                                lastTradeTime = getTradeTime(element.substring(1)) + getMarketDate();
                            } catch (Exception e) {
                            }
                            break;
                    }
                    break;
                case 'F':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setNetAssetValue(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                        default:
                            setLastTrade((SharedMethods.getDouble(element.substring(1)) / decimalFactor), fromZip);
                            if ((!fromZip) && (getLastTradeValue() > 0)) {
                                lastTradeUpdated = true;
                                lastTradeUpdatedLower = true;
                                lastTradeUpdatedMiddle = true;
                                lastTradeUpdatedUpper = true;
                            }
                            break;
                    }
                    break;
                case 'G':
                    setTradeQuantity(SharedMethods.getLong(element.substring(1)));
                    break;
                case 'H':
                    setTradeTick("" + element.charAt(1), fromZip);
                    break;
                case 'I':
                    setNoOfTrades((int) SharedMethods.getLong(element.substring(1)));
                    if ((!fromZip) && (getLastTradeValue() > 0)) {
                        lastTradeUpdated = true;
                        lastTradeUpdatedLower = true;
                        lastTradeUpdatedMiddle = true;
                        lastTradeUpdatedUpper = true;
                    }
                    break;
                case 'J':
                    setLastTradedDate(getTradeDate(element.substring(1)));
                    break;
                case 'K':
                    setLastTradedPrice(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'L':
                    setHigh(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'M':
                    setLow(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'N':
                    change = SharedMethods.getDouble(element.substring(1)) / decimalFactor;
                    break;
                case 'O':
                    percentChange = SharedMethods.getDouble(element.substring(1)) / decimalFactor;
                    break;
                case 'P':
                    setPreviousClosed(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'Q':
                    setTodaysClose(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'R':
                    setTodaysOpen(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'S':
                    setAvgTradePrice(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'T':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_INDEX:
                            setPredictedIndex(SharedMethods.getLong(element.substring(1)));
                            break;
                        default:
                            /*if (element.equals("TWAp")) {
                                setTWAP(0);
                            }*/
                            setAvgTradeVolume(SharedMethods.getLong(element.substring(1)));
                            break;
                    }
                    break;
                case 'U':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            setHigh(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                        case Meta.SYMBOL_TYPE_INDEX:
                            setPredictedIndex(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                        default:
                            setMaxPrice(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case 'V':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            setLow(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                        case Meta.SYMBOL_TYPE_INDEX:
                            setPredictedIndexChange(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                        default:
                            setMinPrice(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case 'W':
                    setHighPriceof52Weeks(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'X':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_INDEX:
                            setPredictedIndexPctChange(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                        default:
                            setHighPriceDateof52Weeks(getTradeDate(element.substring(1)));
                            break;
                    }
                    break;
                case 'Y':
                    setLowPriceof52Weeks(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'Z':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setSettlementDate(getTradeDate(element.substring(1)));
                            break;
                        default:
                            setLowPriceDateof52Weeks(getTradeDate(element.substring(1)));
                            break;
                    }
                    break;
                case 'a':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            setForexCurrency(element.substring(1));
                            break;
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setBenchmark(element.substring(1));
                            break;
                        default:
                            setPERation(SharedMethods.getDouble(element.substring(1)));
                            break;
                    }
                    break;
                case 'b':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            setLotSize(SharedMethods.getLong(element.substring(1)));
                            break;
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setFundValue(SharedMethods.getDouble(element.substring(1)));
                            break;
                        default:
                            setMarketCap(SharedMethods.getDouble(element.substring(1)));
                            break;
                    }
                    break;
                case 'c':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            setForexBaseSymbol(element.substring(1));
                            break;
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setPerformenceYTD(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                        default:
                            setYield(SharedMethods.getDouble(element.substring(1)));
                            break;
                    }
                    break;
                case 'd':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            setForexAppender(element.substring(1));
                            break;
                        default:
                            setFaceValue(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case 'e':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            setForexDeviation(SharedMethods.getDouble(element.substring(1)));
                            break;
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setPerformance3Y(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                        default:
                            setListedShares(SharedMethods.getLong(element.substring(1)));
                            break;
                    }
                    break;
                case 'f':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            setForexQuoteID(element.substring(1));
                            break;
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setPerformance5Y(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                        default:
                            setPaidShares(SharedMethods.getLong(element.substring(1)));
                            break;
                    }
                    break;
                case 'g':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            setForexBuyDate(element.substring(1));
                            break;
                        default:
                            setCouponValue(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case 'h':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            setForexSellDate(element.substring(1));
                            break;
                        default:
                            setCouponDate(getTradeDate(element.substring(1)));
                            break;
                    }
                    break;
                case 'i':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            setForexBestQuote(Integer.parseInt(element.substring(1)));
                            break;
                        default:
                            setNetProfit(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case 'j':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            setForexBuy(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                        default:
                            setNetProfitDate(getTradeDate(element.substring(1)));
                            break;
                    }
                    break;
                case 'k':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            setForexSell(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                        default:
                            setEPS(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case 'm':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_FOREX:
                            break;
                        default:
                            setBestAskPrice(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case 'n':
                    setBestAskQty(SharedMethods.getLong(element.substring(1)));
                    break;
                case 'o':
                    setBestBidPrice(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'p':
                    setBestBidQty(SharedMethods.getLong(element.substring(1)));
                    break;
                case 'q':
                    if ((ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketDepthByPrice)) || (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketDepthByOrder))) {
                        setNoOfAsks(SharedMethods.getLong(element.substring(1)));
                    }
                    break;
                case 'r':
                    if ((ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketDepthByPrice)) || (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketDepthByOrder))) {
                        setNoOfBids(SharedMethods.getLong(element.substring(1)));
                    }
                    break;
                case 's':
                    if ((ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketDepthByPrice)) || (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketDepthByOrder))) {
                        setTotBidQty(SharedMethods.getLong(element.substring(1)));
                    }
                    break;
                case 't':
                    if ((ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketDepthByPrice)) || (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketDepthByOrder))) {
                        setTotAskQty(SharedMethods.getLong(element.substring(1)));
                    }
                    break;
                case 'u':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setFundSize(SharedMethods.getLong(element.substring(1)));
                            break;
                        default:
                            setHighAsks(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case 'v':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setSubsPurchase(SharedMethods.getDouble(element.substring(1)));
                            break;
                        default:
                            setLowBid(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case 'w':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setTotAssets(SharedMethods.getDouble(element.substring(1)));
                            break;
                        default:
                            setLastAskPrice(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case 'x':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setTotAssetsDate(SharedMethods.getLong(element.substring(1)));
                            break;
                        default:
                            setLastBidPrice(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case 'y':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setBenchmarkIndValues(SharedMethods.getDouble(element.substring(1)));
                            break;
                        default:
                            setLastAskQty(SharedMethods.getLong(element.substring(1)));
                            break;
                    }
                    break;
                case 'z':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setMgtFee(SharedMethods.getDouble(element.substring(1)));
                            break;
                        default:
                            setLastBidQty(SharedMethods.getLong(element.substring(1)));
                            break;
                    }

                    break;
                case '0':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setDeadline(SharedMethods.getLong(element.substring(1)));
                            break;
                        default:
                            setSimpleAverageAsk(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case '1':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setPerformance12M(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                        default:
                            setWeightedAverageAsk(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case '2':

                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setFundName(element.substring(1));
                            break;
                        default:
                            setSimpleAverageBid(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case '3':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_MUTUALFUND:
                            setAMC(element.substring(1));
                            break;
                        default:
                            setWeightedAverageBid(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                            break;
                    }
                    break;
                case '4':
                    setCountry(element.substring(1));
                    break;
                case '5':
                    setRisk1Y(SharedMethods.getDouble(element.substring(1)));
                    break;
                case '6':
                    setMinInitPurchase(SharedMethods.getDouble(element.substring(1)));
                    break;
                case '7':
                    setFEndLoad(SharedMethods.getDouble(element.substring(1)));
                    break;
                case '8':
                    setBEndLoad(SharedMethods.getDouble(element.substring(1)));
                    break;
                case '`':
                    setSharesForForeigners(SharedMethods.getLong(element.substring(1)));
                    break;
                case '$':
                    setOpenInterest(SharedMethods.getLong(element.substring(1)));
                    break;
                case '%':
                    setExpirationDate(element.substring(1));
                    break;
                case '&':
                    setStrikePrice(SharedMethods.getDouble(element.substring(1)));
                    break;
                case '*':
                    setEquitySymbol(element.substring(1));
                    String[] symbolData = element.substring(1).split("\\|");
                    setEquitySymbol(symbolData[symbolData.length - 1]);
                    symbolData = null;
                    break;
                case '+':
                    setContractHigh(SharedMethods.getDouble(element.substring(1)));
                    break;
                case '-':
                    setContractLow(SharedMethods.getDouble(element.substring(1)));
                    break;
                case '~':
                    setRefPrice(SharedMethods.getDouble(element.substring(1)));
                    break;
                case '#':
                    setISINCode(element.substring(1));
                    break;
                case '@':
                    setPBRatio(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case '[':
                    try {
                        String[] fields = element.substring(1).split(",", 6);
                        if (fields.length == 6) {
                            if (!fields[0].equals("")) {
                                setCashInOrder(SharedMethods.getLong(fields[0]));
                            }
                            if (!fields[1].equals("")) {
                                setCashInVolume(SharedMethods.getLong(fields[1]));
                            }
                            if (!fields[2].equals("")) {
                                setCashInTurnover(SharedMethods.getDouble(fields[2]) / decimalFactor);
                            }
                            if (!fields[3].equals("")) {
                                setCashOutOrder(SharedMethods.getLong(fields[3]));
                            }
                            if (!fields[4].equals("")) {
                                setCashOutVolume(SharedMethods.getLong(fields[4]));
                            }
                            if (!fields[5].equals("")) {
                                setCashOutTurnover(SharedMethods.getDouble(fields[5]) / decimalFactor);
                            }
                        }
                    } catch (Exception e) {
                    }
                    break;
                case ']':
                    processSymbolEvents(element.substring(1));
                    break;
                case ')':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_OPTIONS:
                            setChgInOpenInterest(SharedMethods.getDouble(element.substring(1)));
                            break;
                        default:
                            setMarketCenter(element.substring(1));
                            break;
                    }
                    break;
                case '?':
                    switch (symbolType) {
                        case Meta.SYMBOL_TYPE_OPTIONS:
                            setOptionType(SharedMethods.getLong(element.substring(1)));
                            break;
                        default:
                            break;
                    }
                    break;
                case '\\': // added by Shanaka
                    setIntrinsicValue(SharedMethods.getDouble(element.substring(1)));
                    break;
                case '}':
                    setData2(element, decimalFactor, fromZip);
                    break;
                /*case '{':
                    setTWAP(Float.parseFloat(element.substring(1)));*/
            }
        }
        if (SharedMethods.getSymbolType(instrumentType) == Meta.SYMBOL_TYPE_FOREX) {
            createForexBuySellValues(decimalFactor);
        }
    }

    private void setData2(String data, int decimalFactor, boolean fromZip) throws Exception {
        try {
            if (data == null)
                return;

            char tag;
            setLastUpdatedTime(Client.getInstance().getMarketTime());
            StringTokenizer tokenizer = new StringTokenizer(data, Meta.ID);
            while (tokenizer.hasMoreElements()) {
//		for (int i = 0; i < data.length; i++) {
                String element = tokenizer.nextToken();
                tag = element.charAt(0);
                switch (tag) {
                    case 'a':
                        setSevenDayAvgVolume(SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'b':
                        setThirtyDayAvgVolume(SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'c':
                        setNintyDayAvgVolume(SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'j':
                        setTop(SharedMethods.getDouble(element.substring(1)));
                        break;
                    case 'l':
                        setAnnualVolatility(Double.parseDouble(element.substring(1)));
                        break;
                    case 'm':
                        setDailyVolatility(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                        break;
                    case 'n':
                        switch (instrumentType) {
                            case Meta.INSTRUMENT_INDEX_OPTION:
                                setEquitySymbol(SharedMethods.getKey(this.exchange, element.substring(1), Meta.INSTRUMENT_INDEX));
                                break;
                            default:
                                setEquitySymbol(SharedMethods.getKey(this.exchange, element.substring(1), Meta.INSTRUMENT_EQUITY));
                                break;
                        }
                        break;
                    case 'p':
                        setChgVWAPBase(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                        break;
                    case 'q':
                        setPercentageChgVWAPBase(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                        break;
                    case 'r':
                        issuedAmount(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                        break;
                    case 's':
                        setDateofIssue(getTradeDate(element.substring(1)));
                        break;
                    case 't':
                        setOutstandingAmount(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                        break;
                    case 'u':
                        setMatuarityDate(getTradeDate(element.substring(1)));
                        break;
                    case 'v':
                        setCouponRate(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                        break;
                    case 'w':
                        setNominalValue(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                        break;
                    case 'x':
                        setIssueCurrency(element.substring(1));
                        break;
                    case 'y':
                        setLotSize(SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'z':
                        setListingDate(getTradeDate(element.substring(1)));
                        break;
                    case 'A':
                        setBondType((int) SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'B':
                        tradedThroughDate(getTradeDate(element.substring(1)));
                        break;
                    case 'C':
                        setRateCalcType((int) SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'D':
                        setDayCountMethod((int) SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'E':
                        setLoanNumber(element.substring(1));
                        break;
                    case 'F':
                        setExternalBondType(element.substring(1));
                        break;
                    case 'G':
                        setEarlyLateMatuarity(element.substring(1));
                        break;
                    case 'H':
                        setMatuarityValue(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                        break;
                    case 'I':
                        setYearlyCouponFreq(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                        break;
                    case 'J':
                        setRecordDateType(element.substring(1));
                        break;
                    case 'K':
                        setNumberofRecordDays((int) SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'L':
                        setInterestFromDate(getTradeDate(element.substring(1)));
                        break;
                    case 'M':
                        setInterestDayType((int) SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'N':
                        setPreviousCouponDate(getTradeDate(element.substring(1)));
                        break;
                    case 'O':
                        setLastCouponDate(getTradeDate(element.substring(1)));
                        break;
                    case 'P':
                        setCouponRecordDate(getTradeDate(element.substring(1)));
                        break;
                    case 'Q':
                        setFaceValueDimension((int) SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'R':
                        setNumberofCashflows((int) SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'S':
                        setCouponFrequancy(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                        break;
                    case 'T':
                        setDaysToFirstCashFlow((int) SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'U':
                        setDaysInFirstRecord((int) SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'V':
                        setIndexCouponAdjustments(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                        break;
                    case 'W':
                        setAccuredInterest(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                        break;
                    case 'X':
                        setIndexFactor(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                        break;
                    case 'Y':
                        setCalcConvention((int) SharedMethods.getLong(element.substring(1)));
                        break;
                    case 'Z':
                        setSettlementDate(getTradeDate(element.substring(1)));
                        break;
                    case '0':
                        setDecimalCount(Byte.parseByte("" + SharedMethods.getLong(element.substring(1))));
                        break;
                    case '}':
                        setData3(element, decimalFactor, fromZip);
                        break;

                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    private void setData3(String data, int decimalFactor, boolean fromZip) throws Exception {
        try {
            if (data == null)
                return;

            char tag;
            setLastUpdatedTime(Client.getInstance().getMarketTime());
            StringTokenizer tokenizer = new StringTokenizer(data, Meta.US);
            while (tokenizer.hasMoreElements()) {
                String element = tokenizer.nextToken();
                tag = element.charAt(0);
                switch (tag) {
                    case 'm':
                        setYTDStockValue(SharedMethods.getDouble(element.substring(1)));
                        break;
                    case 'r':
                        setGroupName(element.substring(1));
                        break;
                    case 's':
                        setGroupStatus((int) SharedMethods.getLong(element.substring(1)));
                        break;
                    case 't':
                        setTov(SharedMethods.getLong(element.substring(1)));
                        break;
                    case '6': // have to consider
                        setWeekAvgPrice(SharedMethods.getDouble(element.substring(1)));
                        break;
                    case '7':  // have to consider
                        setMonthAvgPrice(SharedMethods.getDouble(element.substring(1)));
                        break;
                    case '}':
                        setData4(element, decimalFactor, fromZip);
                        break;

                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    private void setData4(String data, int decimalFactor, boolean fromZip) throws Exception {
        try {
            if (data == null)
                return;

            char tag;
            setLastUpdatedTime(Client.getInstance().getMarketTime());
            StringTokenizer tokenizer = new StringTokenizer(data, Meta.EXT3);
            while (tokenizer.hasMoreElements()) {
                String element = tokenizer.nextToken();
                tag = element.charAt(0);
                switch (tag) {
                    case 'R':
                        setTWAP(SharedMethods.getDouble(element.substring(1)));
                        break;
                    case 'S':
                        setClosingVWAP(SharedMethods.getDouble(element.substring(1)));
                        break;
                    case 'X':
                        setLoosingCategory(Integer.parseInt(element.substring(1)));
                        break;
                    case 'W':
                        setTcp(SharedMethods.getDouble(element.substring(1)));
                        break;
                    case 'Z':
                        setTcv(SharedMethods.getLong(element.substring(1)));
                        break;
                    case 's':
                        setShareCapital(SharedMethods.getDouble(element.substring(1)));
                        break;
                    case '7':
                        setStaticMax(SharedMethods.getDouble(element.substring(1)));
                        break;
                    case '8':
                        setStaticMin(SharedMethods.getDouble(element.substring(1)));
                        break;
                    case 'f':
                        setCurrentAdjstVal(SharedMethods.getLong(element.substring(1)));
                        break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }
    private void processSymbolEvents(String data) {
        String[] records = data.split(",");
        for (String record : records) {
            String[] fields = record.split("\\|");
            if (fields[0].equals(Meta.EVENT_TYPE_SYMBOL_STATE)) {
                try {
                    setSymbolStatus((long) SharedMethods.intValue(fields[1]));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (fields[0].equals(Meta.EVENT_TYPE_FINANCIAL_CALENDAR)) {
                try {
                    setEventAvailability((long) Constants.ITEM_NOT_READ);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            } else if (fields[0].equals(Meta.EVENT_TYPE_NEWS_INDICATOR)) {
                try {
                    setNewsAvailability(NewsProvidersStore.getSharedInstance().isSubscribedNewsSouceInclude(fields[1]));
                    setNewsTimeoutPeriod((long) Constants.NEWS_TIME_OUT_TIME);

                } catch (Exception e) {

                }
            }
        }
    }

    private void setBidAskRatio() {
        try {
            long totalBidQty = getTotalBidQty();
            long totalAskQty = getTotalAskQty();
            if ((totalBidQty == 0) && (totalAskQty == 0))
                bidaskRatio = -1;
            else if (totalBidQty == 0)
                bidaskRatio = 0;
            else if (totalAskQty == 0)
                bidaskRatio = Double.POSITIVE_INFINITY;
            else {
                bidaskRatio = (double) totalBidQty / (double) totalAskQty;
            }
        } catch (Exception e) {
            bidaskRatio = -1;
        }
    }

    /* public synchronized void calculateBidAskRange() {
             if (bidaskRatio >= 1) {
                 if (bidaskRatio == Double.POSITIVE_INFINITY){
                     // do nothing
                 } else if (bidaskRatio > ExchangeStore.getSharedInstance().getExchange(exchange).getBidAskMax()) {
                     ExchangeStore.getSharedInstance().getExchange(exchange).setBidAskMax(bidaskRatio);
                 }
             } else {
                 if (bidaskRatio < 0){
                     // do nothing
                 } else if (bidaskRatio < ExchangeStore.getSharedInstance().getExchange(exchange).getBidAskMin()) {
                     ExchangeStore.getSharedInstance().getExchange(exchange).setBidAskMin(bidaskRatio);
                 }
             }
         }

         public void calculateSpreadRange() {
             if (!Double.isInfinite(pctSpread)) {
                 if (pctSpread >= 0) {
                     if (pctSpread > ExchangeStore.getSharedInstance().getExchange(exchange).getSpreadMax()) {
                         ExchangeStore.getSharedInstance().getExchange(exchange).setSpreadMax(pctSpread);
                     }
                 } else {
                     if (pctSpread < ExchangeStore.getSharedInstance().getExchange(exchange).getSpreadMin()) {
                         ExchangeStore.getSharedInstance().getExchange(exchange).setSpreadMin(pctSpread);
                     }
                 }
             }
         }

         public void calculateRangeRange() {
             if ((pctRange >= 0) && (pctRange != Double.POSITIVE_INFINITY)){
                 if (pctRange > ExchangeStore.getSharedInstance().getExchange(exchange).getRangeMax()) {
                     ExchangeStore.getSharedInstance().getExchange(exchange).setRangeMax(pctRange);
                 }
             }
         }

         public void calculateChangeRange() {
             if (percentChange >= 0) {
                 if (percentChange > ExchangeStore.getSharedInstance().getExchange(exchange).getChangeMax()) {
                     ExchangeStore.getSharedInstance().getExchange(exchange).setChangeMax(percentChange);
                 }
             } else {
                 if (percentChange < ExchangeStore.getSharedInstance().getExchange(exchange).getChangeMin()) {
                     ExchangeStore.getSharedInstance().getExchange(exchange).setChangeMin(percentChange);
                 }
             }
         }*/

    private long getMarketDate() {
        long date = -1;
        try {
            date = ExchangeStore.getSharedInstance().getExchange(getExchange()).getMarketDate();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (date < 1) {
            Calendar cl = Calendar.getInstance();
            cl.setTimeInMillis(System.currentTimeMillis());
            cl.set(Calendar.HOUR_OF_DAY, Integer.parseInt("00"));
            cl.set(Calendar.MINUTE, Integer.parseInt("00"));
            cl.set(Calendar.SECOND, Integer.parseInt("00"));
            date = (cl.getTime()).getTime();
        }
        return date;
    }

    private long getTradeTime(String sValue) throws Exception {
        long lTime = 0;
        Calendar oCal = Calendar.getInstance();
        oCal.setTimeInMillis(0);
        oCal.set(Calendar.HOUR, Integer.parseInt(sValue.substring(0, 2)));
        oCal.set(Calendar.MINUTE, Integer.parseInt(sValue.substring(2, 4)));
        oCal.set(Calendar.SECOND, Integer.parseInt(sValue.substring(4)));
        lTime = oCal.getTime().getTime();
        oCal = null;
        return lTime;
    }

    private long getTradeTimeForex(String sValue) throws Exception {
        long lTime = 0;
        try {
            Calendar oCal = Calendar.getInstance();
            oCal.setTimeInMillis(0);
            String time = sValue.split("-")[1];
            oCal.set(Calendar.HOUR, Integer.parseInt(time.substring(0, 2)));
            oCal.set(Calendar.MINUTE, Integer.parseInt(time.substring(3, 5)));
            oCal.set(Calendar.SECOND, Integer.parseInt(time.substring(6)));
            lTime = oCal.getTime().getTime();
            oCal = null;
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return lTime;
    }

    private long getTradeDate(String sValue) throws Exception {
        long lTime = 0;
        Calendar oCal = Calendar.getInstance();
        oCal.setTimeInMillis(0);
        oCal.set(Calendar.YEAR, Integer.parseInt(sValue.substring(0, 4)));
        oCal.set(Calendar.MONTH, (Integer.parseInt(sValue.substring(4, 6)) - 1));
        oCal.set(Calendar.DATE, Integer.parseInt(sValue.substring(6)));
        lTime = oCal.getTime().getTime();
        oCal = null;
        return lTime;
    }

    public double getChange() {
        return change;
    }

    public void setChange(double newChange) {
        change = newChange;
    }

    public double getPredictedIndex() {
        try {
            return doubleTable.get(ID_PREDICTED_INDEX);
        } catch (Exception e) {
            return 0;
        }
    }

    public void setPredictedIndex(double newChange) {
        doubleTable.put(ID_PREDICTED_INDEX, newChange);
    }

    public double getPredictedIndexChange() {
        try {
            return doubleTable.get(ID_PREDICTED_INDEX_CHANGE);
        } catch (Exception e) {
            return 0;
        }
    }

    public void setPredictedIndexChange(double newChange) {
        doubleTable.put(ID_PREDICTED_INDEX_CHANGE, newChange);
    }

    public double getPredictedIndexPctChange() {
        try {
            return doubleTable.get(ID_PREDICTED_INDEX_PCT_CHANGE);
        } catch (Exception e) {
            return 0;
        }
    }

    public void setPredictedIndexPctChange(double newChange) {
        doubleTable.put(ID_PREDICTED_INDEX_PCT_CHANGE, newChange);
    }


    public String getCompanyCode() {
        return companyCode;
    }

    public void setCompanyCode(String newCompanyCode) {
        companyCode = newCompanyCode;
    }


    public double getHigh() {
        return high;
    }

    public void setHigh(double newHigh) {
        high = newHigh;
        range = high - low;
        pctRange = range * 100 / previousClosed;
        calculateIsBeyond52WeekHighorLow();
    }

    public void setLastTrade(double newLastTrade, boolean fromZip) {

        if (!fromZip) {
            if (lastTradeValue > 0) {
                byte prevTick = tradeTick;
                if (this.lastTradeValue > newLastTrade) {
                    tradeTick = (byte) Settings.TICK_DOWN;
                    lastTradeFlag = -1 * System.currentTimeMillis();
                } else if (this.lastTradeValue < newLastTrade) {
                    tradeTick = (byte) Settings.TICK_UP;
                    lastTradeFlag = System.currentTimeMillis();
                } else {
                    if ((prevTick == Settings.TICK_UP) || (prevTick == Settings.TICK_POSITIVE_EQUAL)) {
                        tradeTick = (byte) Settings.TICK_POSITIVE_EQUAL;
                    } else if ((prevTick == Settings.TICK_DOWN) || (prevTick == Settings.TICK_NEGATIVE_EQUAL)) {
                        tradeTick = (byte) Settings.TICK_NEGATIVE_EQUAL;
                    } else {
                        tradeTick = (byte) Settings.TICK_INVALID;
                    }
                }
            } else {
                tradeTick = (byte) Settings.TICK_NOCHANGE;
            }
        }

        if ((getListedShares() > 0) && (lastTradeValue > 0)) {
            setMarketCap((getListedShares() * newLastTrade) / 1000);
        }
        lastTradeValue = newLastTrade;
//        lastTradeValue = newLastTrade;
        setPctSpread();
        calculateIsMinOrMax();
        //calculateIsBeyond52WeekHighorLow();
    }

    private void calculateIsMinOrMax() {
        if ((lastTradeValue == getMinPrice()) && (getNoOfTrades() > 0)) {
            setMinOrMax(MIN);
        } else if ((lastTradeValue == getMaxPrice()) && (getNoOfTrades() > 0)) {
            setMinOrMax(MAX);
        } else {
            setMinOrMax(NORMAL);
        }
    }

    private void calculateIsBeyond52WeekHighorLow() {
        if ((high != 0) && (low != 0) && (getHighPriceOf52Weeks() != 0) && (getLowPriceOf52Weeks() != 0)) {
            if (high >= (getHighPriceOf52Weeks())) {
                set52WeekHighLowTracker(BEYOND52WEEKHIGH);
            } else if (low <= getLowPriceOf52Weeks()) {
                set52WeekHighLowTracker(BEYOND52WEEKLOW);
            } else {
                set52WeekHighLowTracker(NORMAL52WEEK);
            }
        }
    }

    public long getLastTradeTime() {
        return lastTradeTime;
    }

    public long getLastUpdatedTime() {
        return timeOffset + lastUpdate;
    }

    public void setLastUpdatedTime(long newLastUpdatedTime) {
        lastUpdate = newLastUpdatedTime;
    }

    public double getLow() {
        return low;
    }

    public void setLow(double newLow) {
        low = newLow;
        range = high - low;
        pctRange = range * 100 / previousClosed;
        calculateIsBeyond52WeekHighorLow();
    }

    public int getNoOfTrades() {
        long value = 0;
        try {
            value = longTable.get(ID_NO_OF_TRADES);
            return (int) value;
        } catch (Exception e) {
//			e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return 0;
    }

    public void setNoOfTrades(int newNoOfTrades) {
        longTable.put(ID_NO_OF_TRADES, (long) newNoOfTrades);
        calculateIsMinOrMax();
    }

    public double getPercentChange() {
        return percentChange;
    }

    public void setPercentChange(double newPercentChange) {
        percentChange = newPercentChange;
    }

    public double getPreviousClosed() {
        return previousClosed;
    }

    public void setPreviousClosed(double newPreviousClosed) {
        previousClosed = newPreviousClosed;
        pctRange = range * 100 / previousClosed;
    }

    public String getSymbol() {
        return symbol;
    }

    //===================================For the CR-MORMUBMKT-1041==============================================//

    public void setGroupName(String newgroupName) {
        if (newgroupName != null) {
            stringTable.put(ID_GROUP_NAME, newgroupName);
        }
    }

    public String getGroupName() {
        if (stringTable.get(ID_GROUP_NAME) != null) {
            return stringTable.get(ID_GROUP_NAME);
        }
        return (Language.getString("NA"));
    }

    public void setGroupStatus(int value) {
        this.groupStatus = value;
        /*if (this.getGroupName().equals("03"))
            System.out.println("Group : " + this.groupStatus + " , " + System.currentTimeMillis());*/
    }


    public String getGroupStatus() {
        if (this.groupStatus == Meta.MARKET_OPEN)
            return Language.getString("MKT_STATUS_OPEN");
        else if (this.groupStatus == Meta.MARKET_PREOPEN)
            return Language.getString("MKT_STATUS_PRE_OPEN");
        else if (this.isMarketPreClose())
            return Language.getString("MKT_STATUS_PRE_CLOSE");
        else if (this.groupStatus == Meta.TRADING_AT_LAST)
            return Language.getString("MKT_STATUS_TRADE_AT_LAST");
        else if (this.groupStatus == Meta.MARKET_CLOSE)
            return Language.getString("MKT_STATUS_CLOSE");
        else
            return Language.getString("NA");
    }

    //-------------- added by shanaka---

    public void setWeekAvgPrice(double value) {
        doubleTable.put(ID_WEEK_AVG_PRICE, value);
    }

    public double getWeekAvgPrice() {
        try {
            return doubleTable.get(ID_WEEK_AVG_PRICE);
        } catch (Exception e) {
            return 0;
        }
    }

    public void setMonthAvgPrice(double value) {
        doubleTable.put(ID_MONTH_AVG_PRICE, value);
    }

    public double getMonthAvgPrice() {
        try {
            return doubleTable.get(ID_MONTH_AVG_PRICE);
        } catch (Exception e) {
            return 0;
        }
    }

    //------------------

    public boolean isMarketPreClose() {
        if ((this.groupStatus == Meta.MARKET_PRECLOSE) || (this.groupStatus == Meta.PRECLOSE_WITH_TRADES) || (this.groupStatus == Meta.CLOSE_WITH_TRADES))
            return true;
        return false;
    }

    private void setTopTcpValue(double value) {
        doubleTable.put(ID_TOP_TCP_VALUE, value);
        /*if (value != 0.0) {
            doubleTable.put(ID_TOP_TCP_VALUE, value);
        }*/
    }

    public double getTopTcpValue() {
        double value = -1;
        if ((this.groupStatus == Meta.MARKET_PREOPEN) || (this.isMarketPreClose())) {
            try {
                value = doubleTable.get(ID_TOP_TCP_VALUE);
            } catch (Exception e) {
                value = 0;
            }
        }
        /*else{
            value = -1;
        }*/
        return value;
    }

    //=================================================================================//


    public String getSymbolForTopStocks() {
        if (ExchangeStore.getSharedInstance().getExchange(exchange).isUserSubMarketBreakdown()) {
            return symbolCode;
        } else {
            return symbol;
        }
    }

    public String getSymbolCode() {
        return symbolCode;
    }

    public String getExchange() {
        return exchange;
    }

    public String getKey() {
        return key;
    }

    public long getTradeQuantity() {
        return tradeQuantity;
    }

    public void setTradeQuantity(long newTradeQuantity) {
        if (tradeQuantity > newTradeQuantity) {
            tradeQuantityFlag = System.currentTimeMillis() * -1;
        } else if (tradeQuantity < newTradeQuantity) {
            tradeQuantityFlag = System.currentTimeMillis();
        }
        tradeQuantity = newTradeQuantity;
    }

    public long getTradeQuantityFlag() {
        return tradeQuantityFlag;
    }

    public int getTradeTick() {
        return (int) tradeTick;
    }

    public void setTradeTick(String sTradeTick, boolean fromZip) {
        if (fromZip) {
            if (sTradeTick.equals("U"))
                tradeTick = (byte) Settings.TICK_UP;
            else if (sTradeTick.equals("D"))
                tradeTick = (byte) Settings.TICK_DOWN;
            else if (sTradeTick.equals("N"))
                tradeTick = (byte) Settings.TICK_NOCHANGE;
            else
                tradeTick = (byte) Settings.TICK_INVALID;
        }
    }

    public double getTurnover() {
        return turnoverValue;
    }

    public long getTurnoverFlag() {
        return turnoverFlag;
    }

    public void setTurnover(double newTurnover) {
        if (turnoverValue > newTurnover) {
            turnoverFlag = System.currentTimeMillis() * -1;
        } else if (turnoverValue < newTurnover) {
            turnoverFlag = System.currentTimeMillis();
        }
        turnoverValue = newTurnover;
    }

    public long getVolume() {
        return volume;
    }

    public void setVolume(long newVolume) {
        latestPriceUpdsateTime = System.currentTimeMillis();
        if (volume > newVolume) {
            volumeFlag = System.currentTimeMillis() * -1;
        } else if (volume < newVolume) {
            volumeFlag = System.currentTimeMillis();
        }
        volume = newVolume;
//        sevenDayAvgVolume=volume*(Math.random()+0.1);
//        thirtyDayAvgVolume=volume*(Math.random()+0.2);
//        ninetyDayAvgVolume=volume*(Math.random()+0.3);
    }

    public String getOriginalShortDescription() {
        if (shortDescription == null) {
            return symbol;
        } else {
            return shortDescription;
        }
    }

    public String getShortDescription() {
        if (Stock.symbolAlias) {
            String alias = getAlias();
            if ((alias == null) || (alias.equals(""))) {
                if ((shortDescription == null) || (shortDescription.equals(""))) {
                    return symbol;
                } else {
                    return shortDescription;
                }
            } else {
                return alias;
            }
        } else {
            if ((shortDescription == null) || (shortDescription.equals(""))) {
                return symbol;
            } else {
                return shortDescription;
            }
        }
//        if (shortDescription == null) {
//            return symbol;
//        } else {
//            return shortDescription;
//        }
    }


    public void setShortDescription(String newShortDescription) {
        shortDescription = newShortDescription;
    }

    public String getLongDescription() {
        if ((longDescription != null) && !longDescription.equals(Constants.DEFAULT_STRING)) {
            return longDescription;
        } else {
            if ((instrumentType == Meta.INSTRUMENT_OPTION) || (instrumentType == Meta.INSTRUMENT_EQUITY_OPTION) ||
                    (instrumentType == Meta.INSTRUMENT_INDEX_OPTION) || (instrumentType == Meta.INSTRUMENT_FUTURE_OPTION)) {
                try {
                    String des = Language.getString("SYMBOL_DESCRIPTION_BUILDER");
                    if (getEquitySymbol() != null) {
                        des = des.replaceAll("\\[BASE_SYMBOL\\]", getEquitySymbol());
                    } else {
                        des = des.replaceAll("\\[BASE_SYMBOL\\]", "");
                    }
                    des = des.replaceAll("\\[STRIKE_PRICE\\]", "" + getStrikePrice());
                    if (getOptionType() != -1) {
                        des = des.replaceAll("\\[OPTION_TYPE\\]", "" + SharedMethods.getOptionTypeString(getOptionType()));
                    } else {
                        des = des.replaceAll("\\[OPTION_TYPE\\]", "");
                    }

                    TWDateFormat displayDateFormat = new TWDateFormat("MM:yyyy");
                    String date = displayDateFormat.format(getExpirationDate());
                    String[] months = Language.getString("SHORT_MONTH_LIST").split(",");
                    des = des.replaceAll("\\[YEAR\\]", date.split(":")[1]);
                    des = des.replaceAll("\\[SHORT_MONTH\\]", months[Integer.parseInt(date.split(":")[0]) - 1]);
                    longDescription = des;
                    return longDescription;
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                return getDisplaySymbol();
            } else {
                return getDisplaySymbol();
            }
        }
    }

    public void setLongDescription(String newLongDescription) {
        longDescription = newLongDescription;
    }

    public String getAssetClass() {
        return assetClass;
    }

    public void setAssetClass(String newAssetClass) {
        assetClass = newAssetClass;
    }

    public String getSectorCode() {
        return sectorCode;
    }

    public void setSectorCode(String newSectorCode) {
        sectorCode = newSectorCode;
    }

    public double getLastTradeValue() {
        return lastTradeValue;
    }

    public void setLastTradeValue(double lastTradeValue) {
        this.lastTradeValue = lastTradeValue;
    }

    public long getLastTradeFlag() {
        return lastTradeFlag;
    }

    public double getBestAskPrice() {

        return bestAskPrice; //27 Mar
        // SAMA
        //return asks[0].getPrice();
    }

    public void setBestAskPrice(double newBestAskPrice) {
        latestPriceUpdsateTime = System.currentTimeMillis();
        if (bestAskPrice > newBestAskPrice) {
            bestAskPriceFlag = System.currentTimeMillis() * -1;
        } else if (bestAskPrice < newBestAskPrice) {
            bestAskPriceFlag = System.currentTimeMillis();
        }
        bestAskPrice = newBestAskPrice;
        calculateSpread();
        setPctSpread();
    }

    public long getBestAskPriceFlag() {
        return bestAskPriceFlag;
    }

    public double getClosingVWAP() {
        return closingVWAP;
    }

    public void setClosingVWAP(double closingVWAP) {
        this.closingVWAP = closingVWAP;
    }

    public double getTWAP() {
        return TWAP;
    }

    public void setTWAP(double TWAP) {
        this.TWAP = TWAP;
    }

    public int getLoosingCategory() {
        return loosingCategory;
    }

    public void setLoosingCategory(int loosingCategory) {
        this.loosingCategory = loosingCategory;
    }

    public void calculateSpread() {
        if ((bestAskPrice == 0) && (bestBidPrice == 0)) {
            spread = Double.NaN;
        } else if (bestAskPrice == 0) {
            spread = Double.POSITIVE_INFINITY;
        } else if (bestBidPrice == 0) {
            spread = Double.NEGATIVE_INFINITY;
        } else {
            spread = bestAskPrice - bestBidPrice;
        }
    }

    public double getBestBidPrice() {
        return bestBidPrice;
    }

    public void setBestBidPrice(double newBestBidPrice) {
        latestPriceUpdsateTime = System.currentTimeMillis();
        if (bestBidPrice > newBestBidPrice) {
            bestBidPriceFlag = System.currentTimeMillis() * -1;
        } else if (bestBidPrice < newBestBidPrice) {
            bestBidPriceFlag = System.currentTimeMillis();
        }
        bestBidPrice = newBestBidPrice;
        calculateSpread();
        setPctSpread();
    }

    public long getBestBidPriceFlag() {
        return bestBidPriceFlag;
    }

    public double getBidaskRatio() {
        return bidaskRatio;
    }

    public void setBidaskRatio(double newBidaskRatio) {

        bidaskRatio = newBidaskRatio;
    }

    public String getCurrencyCode() {
        return stringTable.get(ID_CURRENCY);
    }

    public void setCurrencyCode(String newCurrencyCode) {
        if (newCurrencyCode != null) {
            stringTable.put(ID_CURRENCY, newCurrencyCode);
        }
    }

    public int getNewsEventAvailability() {
        return eventNewsAvailability;
    }

    public double getSpread() {
        return spread;
    }

    public double getRange() {
        return range;
    }

    public double getPctRange() {
        return pctRange;
    }

    public double getPctSpread() {
        return pctSpread;
    }

    public long getVolumeFlag() {
        return volumeFlag;
    }

    public void setPctSpread() {
        if (lastTradeValue != 0) {
            if ((!Double.isInfinite(spread)) && (!Double.isNaN(spread)))
                this.pctSpread = spread * 100 / lastTradeValue;
            else
                this.pctSpread = spread;
        } else {
            this.pctSpread = Double.NaN;
        }
    }

    public String toString() {
        //if (getTradeQuantity() > 0){
        /*return
                  getSymbol() + Meta.FS +
                  getLongDescription() + Meta.FS +
                  //getLastTradeTime() + "," +
                  getLastTrade()[0] + Meta.FS +
                  getAvgTradePrice() + Meta.FS +
                  getVolume() + Meta.FS +
                  getTurnover() + Meta.FS +
                  getTodaysOpen() + Meta.FS +
                  getHigh() + Meta.FS +
                  getLow() + Meta.FS +
                  getPreviousClosed() + "\r";*/


        return
                getSymbol() + Meta.FS +
                        //getCompanyCode() + Meta.FS +
                        getLongDescription() + Meta.FS +
                        //"�" + Meta.FS +
                        //timeFormat.format(new Date(getLastTradeTime())) + Meta.FS +
                        getLastTradeValue() + Meta.FS +
                        getTradeQuantity() + Meta.FS +
                        getChange() + Meta.FS +
                        getPercentChange() + Meta.FS +
                        getAvgTradePrice() + Meta.FS +
                        getVolume() + Meta.FS +
                        getTurnover() + Meta.FS +
                        getNoOfTrades() + Meta.FS +
                        getAvgTradeVolume() + Meta.FS +
                        getMinPrice() + Meta.FS +
                        getMaxPrice() + Meta.FS +
                        getTodaysOpen() + Meta.FS +
                        getHigh() + Meta.FS +
                        getLow() + Meta.FS +
                        getPreviousClosed() + Meta.FS +
                        getBestBidPrice() + Meta.FS +
                        getBestBidQuantity() + Meta.FS +
                        getTotalBidQty() + Meta.FS +
                        getNoOfBids() + Meta.FS +
                        getLastBidPrice() + Meta.FS +
                        getLowBid() + Meta.FS +
                        getSimpleAverageBid() + Meta.FS +
                        getWAverageBid() + Meta.FS +
                        getBestAskPrice() + Meta.FS +
                        getBestAskQuantity() + Meta.FS +
                        getTotalAskQty() + Meta.FS +
                        getNoOfAsks() + Meta.FS +
                        getLastAskPrice() + Meta.FS +
                        getHighAsk() + Meta.FS +
                        getSimpleAverageAsk() + Meta.FS +
                        getWAverageAsk() + Meta.FS +
                        getBidaskRatio() + Meta.FS +
                        getMarketCap() + Meta.FS +
                        getPER() + Meta.FS +
                        getPBR() + Meta.FS +
                        getYield() + Meta.FS +
                        getHighPriceOf52Weeks() + Meta.FS +
                        getLowPriceOf52Weeks() + "\r";

    }

    public long getLastTradedDate() {
        long value = 0;
        try {
            value = longTable.get(ID_LAST_TRADED_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getLastTradedPrice() {
        double value = 0;
        try {
            value = doubleTable.get(ID_LAST_TRADED_PRICE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getTodaysClose() {
        double value = 0;
        try {
            value = doubleTable.get(ID_TODAYS_CLOSE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getTodaysOpen() {
        double value = 0;
        try {
            value = doubleTable.get(ID_TODAYS_OPEN);
        } catch (Exception e) {
        }
        return value;
    }

    public double getAvgTradePrice() {
        double value = 0;
        try {
            value = doubleTable.get(ID_AVG_TRADE_PRICE);
        } catch (Exception e) {
        }
        return value;
    }

    public long getAvgTradeVolume() {
        long value = 0;
        try {
            value = longTable.get(ID_AVG_TRADE_VOLUME);
        } catch (Exception e) {
        }
        return value;
    }

    public double getMaxPrice() {
        double value = 0;
        try {
            value = doubleTable.get(ID_MAX_PRICE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getMinPrice() {
        double value = 0;
        try {
            value = doubleTable.get(ID_MIN_PRICE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getHighPriceOf52Weeks() {
        double value = 0;
        try {
            value = doubleTable.get(ID_HIGH_PRICE_52WKS);
        } catch (Exception e) {
        }
        return value;
    }

    public long getHighPriceOf52WeeksDate() {
        long value = 0;
        try {
            value = longTable.get(ID_HIGH_PRICE_DATE_52WKS);
        } catch (Exception e) {
        }
        return value;
    }

    public double getLowPriceOf52Weeks() {
        double value = 0;
        try {
            value = doubleTable.get(ID_LOW_PRICE_52WKS);
        } catch (Exception e) {
        }
        return value;
    }

    public long getLowPriceOf52WeeksDate() {
        long value = 0;
        try {
            value = longTable.get(ID_LOW_PRICE_DATE_52WKS);
        } catch (Exception e) {
        }
        return value;
    }

    public String getForexCurrency() {
        return stringTable.get(ID_FOREX_CURRENCY);
    }

    public String getBenchmark() {
        String benchMark = "";
        benchMark = stringTable.get(ID_BENCHMARK);
        if (benchMark == null) {
            benchMark = Language.getString("NA");
        }
        return benchMark;
    }

    public double getPER() {
        double value = 0;
        try {
            value = doubleTable.get(ID_PER);
        } catch (Exception e) {
        }
        return value;
    }

    public long getLotSize() {
        long value = 1;
        try {
            value = longTable.get(ID_LOT_SIZE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getFundValue() {
        double value = 0;
        try {
            value = doubleTable.get(ID_FUND_VALUE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getMarketCap() {
        double value = 0;
        try {
            value = doubleTable.get(ID_MARKET_CAP);
        } catch (Exception e) {
            value = 0.00;
        }
        return value;
    }

    public String getForexBaseValue() {
        return stringTable.get(ID_FOREX_BASE_SYMBOL);
    }

    public double getPerformanceYTD() {
        double value = 0;
        try {
            value = doubleTable.get(ID_PERFORMANCE_YTD);
        } catch (Exception e) {
        }
        return value;
    }

    public double getYield() {
        double value = 0;
        try {
            value = doubleTable.get(ID_YIELD);
        } catch (Exception e) {
        }
        return value;
    }

    public String getForexAppender() {
        return stringTable.get(ID_FOREX_APPENDER);
    }

    public double getFaceValue() {
        double value = 0;
        try {
            value = doubleTable.get(ID_FACE_VALUE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getForexDeviation() {
        double value = 0;
        try {
            value = doubleTable.get(ID_FOREX_DEVIATION);
        } catch (Exception e) {
        }
        return value;
    }

    public double getPerformance3Y() {
        double value = 0;
        try {
            value = doubleTable.get(ID_PERFORMANCE_3Y);
        } catch (Exception e) {
        }
        return value;
    }

    public long getListedShares() {
        long value = 0;
        try {
            value = longTable.get(ID_LISTED_SHARES);
        } catch (Exception e) {
        }
        return value;
    }

    public String getForexQuoteID() {
        return stringTable.get(ID_FOREX_QUOTE_ID);
    }

    public double getPerformance5Y() {
        double value = 0;
        try {
            value = doubleTable.get(ID_PERFORMANCE_5Y);
        } catch (Exception e) {
        }
        return value;
    }

    public long getPaidShares() {
        long value = 0;
        try {
            value = longTable.get(ID_PAID_SHARES);
        } catch (Exception e) {
        }
        return value;
    }

    public String getForexBuyDate() {
        return stringTable.get(ID_FOREX_BUY_DATE);
    }

    public double getMgtFee() {
        double value = 0;
        try {
            value = doubleTable.get(ID_MGT_FEE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getCouponValue() {
        double value = 0;
        try {
            value = doubleTable.get(ID_COUPON_VALUE);
        } catch (Exception e) {
        }
        return value;
    }

    public long getDeadline() {
        long value = 0;
        try {
            value = longTable.get(ID_DEADLINE);
        } catch (Exception e) {
        }
        return value;
    }

    public long getCouponDate() {
        long value = 0;
        try {
            value = longTable.get(ID_COUPON_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    public int getForexBestQuote() {
        long value = 0;
        try {
            value = longTable.get(ID_IS_FOREX_BEST_QUOTE);
        } catch (Exception e) {
        }
        return (int) value;
    }

    public double getNetProfit() {
        double value = 0;
        try {
            value = doubleTable.get(ID_NET_PROFIT);
        } catch (Exception e) {
        }
        return value;
    }

    public double getForexBuy() {
        double value = 0;
        try {
            value = doubleTable.get(ID_FOREX_BUY);
        } catch (Exception e) {
        }
        return value;
    }

    public long getNetProfitDate() {
        long value = 0;
        try {
            value = longTable.get(ID_NET_PROFIT_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    public String getForexSellDate() {
        return stringTable.get(ID_FOREX_SELL_DATE);
    }

    public double getEPS() {
        double value = 0;
        try {
            value = doubleTable.get(ID_EPS);
        } catch (Exception e) {
        }
        return value;
    }

    public double getForexSell() {
        double value = 0;
        try {
            value = doubleTable.get(ID_FOREX_SELL);
        } catch (Exception e) {
        }
        return value;
    }

    public double getPerformance12M() {
        double value = 0;
        try {
            value = doubleTable.get(ID_PERFORMANCE_12M);
        } catch (Exception e) {
        }
        return value;
    }

    public long getBestAskQuantity() {
        long value = 0;
        try {
            value = longTable.get(ID_BEST_OFFER_QTY);
        } catch (Exception e) {
        }
        return value;
    }

    public String getAmc() {
        return stringTable.get(ID_AMC);
    }

    public String getCountry() {
        return stringTable.get(ID_COUNTRY);
    }

    public long getBestBidQuantity() {
        long value = 0;
        try {
            value = longTable.get(ID_BEST_BID_QTY);
        } catch (Exception e) {
        }
        return value;
    }

    public double getRisk1Y() {
        double value = 0;
        try {
            value = doubleTable.get(ID_RISK_1Y);
        } catch (Exception e) {
        }
        return value;
    }

    public int getNoOfAsks() {
        long value = 0;
        try {
            value = longTable.get(ID_NO_OF_ASKS);
        } catch (Exception e) {
        }
        return (int) value;
    }

    public double getMinInitPurchase() {
        double value = 0;
        try {
            value = doubleTable.get(ID_MIN_INIT_PURCHASE);
        } catch (Exception e) {
        }
        return value;
    }

    public int getNoOfBids() {
        long value = 0;
        try {
            value = longTable.get(ID_NO_OF_BIDS);
        } catch (Exception e) {
        }
        return (int) value;
    }

    public double getFEndLoad() {
        double value = -1;
        try {
            value = doubleTable.get(ID_FEND_LOAD);
        } catch (Exception e) {
        }
        return value;
    }

    public long getTotalBidQty() {
        long value = 0;
        try {
            value = longTable.get(ID_TOT_BID_QTY);
        } catch (Exception e) {
        }
        return value;
    }

    public double getBEndLoad() {
        double value = -1;
        try {
            value = doubleTable.get(ID_BEND_LOAD);
        } catch (Exception e) {
        }
        return value;
    }

    public long getTotalAskQty() {
        long value = 0;
        try {
            value = longTable.get(ID_TOT_ASK_QTY);
        } catch (Exception e) {
        }
        return value;
    }

    public long getFundsize() {
        long value = -1;
        try {
            value = longTable.get(ID_FUND_SIZE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getHighAsk() {
        double value = 0;
        try {
            value = doubleTable.get(ID_HIGH_ASKS);
        } catch (Exception e) {
        }
        return value;
    }

    public double getSubsPurchase() {
        double value = -1;
        try {
            value = doubleTable.get(ID_SUBS_PURCHASE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getLowBid() {
        double value = 0;
        try {
            value = doubleTable.get(ID_LOW_BID);
        } catch (Exception e) {
        }
        return value;
    }

    public double getTotAsset() {
        double value = -1;
        try {
            value = doubleTable.get(ID_TOT_ASSETS);
        } catch (Exception e) {
        }
        return value;
    }

    public double getLastAskPrice() {
        double value = 0;
        try {
            value = doubleTable.get(ID_LAST_ASK_PRICE);
        } catch (Exception e) {
        }
        return value;
    }

    public long getTotAssetDate() {
        long value = 0;
        try {
            value = longTable.get(ID_TOT_ASSETS_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getLastBidPrice() {
        double value = 0;
        try {
            value = doubleTable.get(ID_LAST_BID_PRICE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getBenchmarkIndVal() {
        double value = -1;
        try {
            value = doubleTable.get(ID_BENCHMARK_IND_VALUES);
        } catch (Exception e) {
        }
        return value;
    }

    public long getLastAskQuantity() {
        long value = 0;
        try {
            value = longTable.get(ID_LAST_ASK_QTY);
        } catch (Exception e) {
        }
        return value;
    }

    public long getLastBidQuantity() {
        long value = 0;
        try {
            value = longTable.get(ID_LAST_BID_QTY);
        } catch (Exception e) {
        }
        return value;
    }

    public double getSimpleAverageAsk() {
        double value = 0;
        try {
            value = doubleTable.get(ID_SIMPLE_AVG_ASK);
        } catch (Exception e) {
        }
        return value;
    }

    public double getWAverageAsk() {
        double value = 0;
        try {
            value = doubleTable.get(ID_WEIGHTED_AVG_ASK);
        } catch (Exception e) {
        }
        return value;
    }

    public double getSimpleAverageBid() {
        double value = 0;
        try {
            value = doubleTable.get(ID_SIMPLE_AVG_BID);
        } catch (Exception e) {
        }
        return value;
    }

    public double getWAverageBid() {
        double value = 0;
        try {
            value = doubleTable.get(ID_WEIGHTED_AVG_BID);
        } catch (Exception e) {
        }
        return value;
    }

    public long getSharesForForeigners() {
        long value = 0;
        try {
            value = longTable.get(ID_SHARES_FOR_FOREIGN);
        } catch (Exception e) {
        }
        return value;
    }

    public long getOpenInterest() {
        long value = 0;
        try {
            value = longTable.get(ID_OPEN_INTEREST);
        } catch (Exception e) {
        }
        return value;
    }

    public long getExpirationDate() {
        long value = 0;
        try {
            value = longTable.get(ID_EXPIRATION_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    public double getStrikePrice() {
        double value = 0;
        try {
            value = doubleTable.get(ID_STRIKE_PRICE);
        } catch (Exception e) {
        }
        return value;
    }

    public String getOptionBaseSymbol() {
        return stringTable.get(ID_EQUITY_SYMBOL);
    }

    public String getEquitySymbol() {
        return stringTable.get(ID_EQUITY_SYMBOL);
    }

    public double getContractHigh() {
        double value = 0;
        try {
            value = doubleTable.get(ID_CONTRACT_HIGH);
        } catch (Exception e) {
        }
        return value;
    }

    public double getContractLow() {
        double value = 0;
        try {
            value = doubleTable.get(ID_CONTRACT_LOW);
        } catch (Exception e) {
        }
        return value;
    }

    public double getRefPrice() {
        double value = 0;
        try {
            value = doubleTable.get(ID_REF_PRICE);
        } catch (Exception e) {
        }
        return value;
    }

    public String getISINCode() {
        String value = "";
        try {
            value = stringTable.get(ID_ISIN_CODE);
            if (value == null) {
                value = "";
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return value;
    }

    public double getPBR() {
        double value = 0;
        try {
            value = doubleTable.get(ID_PBR);
        } catch (Exception e) {
        }
        return value;
    }

    public long getCashInOrders() {
        long value = 0;
        try {
            value = longTable.get(ID_CASH_IN_ORDER);
        } catch (Exception e) {
        }
        return value;
    }

    public long getSevenDayAvgVolume() {
        long value = 0;
        try {
            value = longTable.get(ID_SEVEN_DAY_AVG_VOLUME);
        } catch (Exception e) {
        }
        return value;
    }

    public long getCashInVolume() {
        long value = 0;
        try {
            value = longTable.get(ID_CASH_IN_VOLUME);
        } catch (Exception e) {
        }
        return value;
    }

    public double getCashInTurnover() {
        double value = 0;
        try {
            value = doubleTable.get(ID_CASH_IN_TURNOVER);
        } catch (Exception e) {
        }
        return value;
    }

    public long getCashOutOrders() {
        long value = 0;
        try {
            value = longTable.get(ID_CASH_OUT_ORDER);
        } catch (Exception e) {
        }
        return value;
    }

    public long getCashOutVolume() {
        long value = 0;
        try {
            value = longTable.get(ID_CASH_OUT_VOLUME);
        } catch (Exception e) {
        }
        return value;
    }

    public double getCashOutTurnover() {
        double value = 0;
        try {
            value = doubleTable.get(ID_CASH_OUT_TURNOVER);
        } catch (Exception e) {
        }
        return value;
    }

    public int getSymbolStatus() {
        long value = 0;
        try {
            value = longTable.get(ID_SYMBOL_STATUS);
        } catch (Exception e) {
        }
        return (int) value;
    }

    public int getNewsAvailability() {
        long value = 0;
        try {
            value = longTable.get(ID_NEWS_AVAILABILITY);
        } catch (Exception e) {
        }
        return (int) value;
    }

    public int getEventAvailability() {
        long value = 0;
        try {
            value = longTable.get(ID_EVENT_AVAILABILITY);
        } catch (Exception e) {
        }
        return (int) value;
    }

    public int getBulbStatus() {
        long value = 0;
        try {
            value = longTable.get(ID_BULB_STATUS);
        } catch (Exception e) {
        }
        return (int) value;
    }

    public int getNewsTimeoutPeriod() {
        long value = Constants.NEWS_TIME_OUT_TIME;
        try {
            value = longTable.get(ID_NEWS_TIMEOUT_PERIOD);
        } catch (Exception e) {
        }
        return (int) value;
    }

    public double getChgInOpenInt() {
        double value = 0;
        try {
            value = doubleTable.get(ID_CHG_IN_OPEN_INTEREST);
        } catch (Exception e) {
        }
        return value;
    }

    public String getMarketCenter() {
        return stringTable.get(ID_MARKET_CENTER);
    }

    public long getThirtyDayAvgVolume() {
        long value = 0;
        try {
            value = longTable.get(ID_THIRTY_DAY_AVG_VOLUME);
        } catch (Exception e) {
        }
        return value;
    }

    public long getNinetyDayAvgVolume() {
        long value = 0;
        try {
            value = longTable.get(ID_NINTY_DAY_AVG_VOLUME);
        } catch (Exception e) {
        }
        return value;
    }

    public double getAnnualVolatility() {
        double value = Double.NaN;
        try {
            value = doubleTable.get(ID_ANNUAL_VOLATILITY);
        } catch (Exception e) {
        }
        return value;
    }

    public double getDailyVolatility() {
        double value = 0;
        try {
            value = doubleTable.get(ID_DAILY_VOLATILITY);
        } catch (Exception e) {
        }
        return value;
    }

    public String getParams() {
        return stringTable.get(ID_PARAMS);
    }

    public byte getPath() {
        long value = 0;
        try {
            value = longTable.get(ID_PATH);
        } catch (Exception e) {
        }
        return (byte) value;
    }

    public long getLastTradeDate() {
        long value = 0;
        try {
            value = longTable.get(ID_LAST_TRADE_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    public int getAnnouncementAvailability() {
        long value = Constants.ITEM_NOT_AVAILABLE;
        try {
            value = longTable.get(ID_ANNOUNCEMENT_AVAILABILITY);
        } catch (Exception e) {
        }
        return (int) value;
    }

    public double getNetAssetValue() {
        double value = 0;
        try {
            value = doubleTable.get(ID_NET_ASSET);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return value;
    }

    public double getIntrinsicValue() {
        double value = Double.NaN;
        try {
            if(instrumentType==66)
            value = doubleTable.get(BATE_INTRINSIC_VALUE);
        } catch (Exception e) {
            e.printStackTrace();
        }
        return value;
    }

    public String getMarketID() {
        return stringTable.get(ID_SUB_MARKET);
    }

    public String getAlias() {
        return stringTable.get(ID_SYMBOL_ALIAS);
    }

    public int getOptionType() {
//        return stringTable.get(ID_OPTION_TYPE);
        long value = -1;
        try {
            value = longTable.get(ID_OPTION_TYPE);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return (int) value;
    }

    public String getUnit() {
        String value = "";
        try {
            value = stringTable.get(ID_UNITS);
            if (value == null) {
                value = "";
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return value;
    }

    public double getSettlementPrice() {
        double value = 0;
        try {
            value = doubleTable.get(ID_SETTLEMENT_PRICE);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return value;
    }

    public int getMinOrMax() {
        return minOrMax;
    }

    public int get52WeekHighLowTracker() {
        return week52HighLowTracker;
    }

    private void setMinOrMax(int minOrMax) {
        this.minOrMax = minOrMax;
    }

    public void set52WeekHighLowTracker(int value) {
        this.week52HighLowTracker = value;
    }

    public boolean isLastTradeUpdated() {
        return lastTradeUpdated;
    }

    public void unsetLastTradeUpdated() {
        this.lastTradeUpdated = false;
    }

    public boolean isLastTradeUpdatedUpper() {
        return lastTradeUpdatedUpper;
    }

    public void setLastTradeUpdatedUpper() {
        this.lastTradeUpdatedUpper = false;
    }

    public boolean isLastTradeUpdatedMiddle() {
        return lastTradeUpdatedMiddle;
    }

    public void setLastTradeUpdatedMiddle() {
        this.lastTradeUpdatedMiddle = false;
    }

    public boolean isLastTradeUpdatedLower() {
        return lastTradeUpdatedLower;
    }

    public void setLastTradeUpdatedLower() {
        this.lastTradeUpdatedLower = false;
    }

//    public byte[] getTickMap() {
//        return tickMap;
//    }

    public int getTickMap() {
        return tickMap;
    }

    public long getTimeOffset() {
        return timeOffset;
    }

    public void setTimeOffset(long timeOffset) {
        this.timeOffset = timeOffset;
    }


    public synchronized void updateTickMap(int location, byte tick) {

        if (location < (TICK_MAP_LENGTH - 1)) {
            int temp;
            if (location == 0) {
                tickMap <<= 2; // shift tick values to the left
                tickMap = tickMap | tick;
            } else {
                temp = (((1 << (location * 2)) - 1));
                temp = temp & tickMap;
                tickMap ^= temp;
                tickMap <<= 2; // shift tick values to the left
                tickMap |= temp;
                tick <<= (location * 2);
                tickMap = tickMap | tick;
            }
        }
    }

    /* Following are for Radar Screen */

    /*
          * 1. Simple Moving average
          */

    public double getSimpleMovingAverage() {
        RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
        return radarScreenData.getRsSimpleMovingAverage().getValue();
    }

    /**
     * 2. Exponential Moving Average
     */
    public double getExponentialMovingAverage() {
        RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
        return radarScreenData.getRsExpMovingAverage().getValue();
    }

    /**
     * 3. Weighted Moving Average
     */
    public double getWeightedMovingAverage() {
        RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
        return radarScreenData.getRsWghtMovingAverage().getValue();
    }

    /**
     * 4. MACD
     */
    public double getMACD() {
        RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
        return radarScreenData.getRsMACD().getValue();
    }

    /**
     * 5. IMI
     */
    public double getIMI() {
        RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
        return radarScreenData.getRsIMI().getValue();
    }

    /**
     * 6. RSI
     */
    public double getRSI() {
        RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
        return radarScreenData.getRsRSI().getValue();
    }

    /**
     * 7. PriceROC
     */
    public double getROC() {
        RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
        return radarScreenData.getRsROC().getValue();
    }

    /**
     * 8. Chaikin AD Oscillator
     */
    public double getChaikinOsc() {
        RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
        return radarScreenData.getRsChaikinOsc().getValue();
    }

    /**
     * 9. Fast Stochastic Oscillator
     */
    public double getFastStochastic() {
        RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
        return radarScreenData.getRsFastStochastic().getValue();
    }

    /**
     * 10. Bollinger Middle
     */
    public double getBollingerMiddle() {
        RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
        return radarScreenData.getRsBollingerMiddle().getValue();
    }

    /**
     * 11. Bollinger Upper
     */
    public double getBollingerUpper() {
        RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
        return radarScreenData.getRsBollingerUpper().getValue();
    }

    /**
     * 12. Bollinger Lower
     */
    public double getBollingerLower() {
        RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
        return radarScreenData.getRsBollingerLower().getValue();
    }

    /**
     * 13. Slow Stochastic
     */
    public double getSlowStochastic() {
        RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
        return radarScreenData.getRsSlowStochastic().getValue();
    }

    public byte getDecimalCount() {
        return decimalCount;
    }

    public void setDecimalCount(byte decimalCount) {
        this.decimalCount = decimalCount;
    }

    public boolean isBaseSymbolUpdated() {
        return baseSymbolUpdated;
    }

    public void setBaseSymbolUpdated(boolean baseSymbolUpdated) {
        this.baseSymbolUpdated = baseSymbolUpdated;
    }

    public static char getBateDepthAskFillFlag() {
        return BATE_DEPTH_ASK_FILL_FLAG;
    }

    public double getCashFlowNet() {
        return getCashInTurnover() - getCashOutTurnover();
    }

    public double getCashFlowPct() {
        double cashInTurnover = getCashInTurnover();
        return (cashInTurnover) / (cashInTurnover + getCashOutTurnover());
    }

    public double getCashFlowRatio() {
        double cashInTurnover = getCashInTurnover();
        double cashOutTurnover = getCashOutTurnover();
        try {
            if ((cashInTurnover == 0) && (cashOutTurnover == 0))
                cashFlowRatio = -1;
            else if (cashInTurnover == 0)
                cashFlowRatio = 0;
            else if (cashOutTurnover == 0)
                cashFlowRatio = Double.POSITIVE_INFINITY;
            else {
                cashFlowRatio = cashInTurnover / cashOutTurnover;
            }
        } catch (Exception e) {
            cashFlowRatio = -1;
        }
        return cashFlowRatio;
    }

    public void createForexBuySellValues(int decimalCorrection) {
        try {
            bestBidPrice = getForexBuy();
            bestAskPrice = getForexSell();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public long pricesDataChangedAt() {
        return latestPriceUpdsateTime;
    }

    public String getRegionalSymbol() {
        return getSymbol() + "." + getMarketCenter();
    }

    public String getXML() {
        TWDecimalFormat format = SharedMethods.getDecimalFormatNoComma(key);
        StringBuilder buffer = new StringBuilder();
        buffer.append("<STOCK>");
        buffer.append("<Symbol value=\"").append(SharedMethods.getSymbolFromKey(key)).append("\"/>");
        buffer.append("<iType value=\"").append(SharedMethods.getInstrumentTypeFromKey(key)).append("\"/>");
        buffer.append("<Description value=\"").append(getShortDescription()).append("\"/>");
        buffer.append("<Exchange value=\"").append(SharedMethods.getExchangeFromKey(key)).append("\"/>");
        buffer.append("<LastTrade value=\"").append(format.format(getLastTradedPrice())).append("\"/>");
        buffer.append("<Change value=\"").append(getChange()).append("\"/>");
        buffer.append("<Bid value=\"").append(format.format(getBestBidPrice())).append("\"/>");
        buffer.append("<BidQty value=\"").append(getBestBidQuantity()).append("\"/>");
        buffer.append("<Offer value=\"").append(format.format(getBestAskPrice())).append("\"/>");
        buffer.append("<OfferQty value=\"").append(getBestAskQuantity()).append("\"/>");
        buffer.append("<Volume value=\"").append(getVolume()).append("\"/>");
        buffer.append("<High value=\"").append(format.format(getHigh())).append("\"/>");
        buffer.append("<Low value=\"").append(format.format(getLow())).append("\"/>");
        buffer.append("<Min value=\"").append(format.format(getMinPrice())).append("\"/>");
        buffer.append("<Max value=\"").append(format.format(getMaxPrice())).append("\"/>");
        buffer.append("</STOCK>\n");

        return buffer.toString();
    }

    public synchronized void setSnapShotUpdatedTime(long sUpatedTime) {
        snapShotUpdatedtime = sUpatedTime;
    }

    public synchronized long getSnapShotUpdatedTime() {
        return snapShotUpdatedtime;
    }

    public void print() {
        if (symbol.equals("AAPL")) {

        }
//        System.out.println(symbol + " " + bestAskPrice);// + " "  + volume);
    }

    public int getAvgVolumeMode() {
        return avgVolumeMode;
    }

    public void setAvgVolumeMode(int avgVolumeMode) {
        this.avgVolumeMode = avgVolumeMode;
    }

    public long getAvgVolume(int mode) {
        if (mode == Constants.VOLUME_WATCHER_SEVENDAY) {
            return getSevenDayAvgVolume();
        } else if (mode == Constants.VOLUME_WATCHER_THIRTYDAY) {
            return getThirtyDayAvgVolume();
        } else if (mode == Constants.VOLUME_WATCHER_NINETYDAY) {
            return getNinetyDayAvgVolume();
        } else {
            return 0;
        }
    }

    public double getPctVolume(int mode) {
        try {
            if ((mode == Constants.VOLUME_WATCHER_SEVENDAY) && (volume > 0)) {
                return ((((double) volume - getSevenDayAvgVolume()) * 100 / getSevenDayAvgVolume()));
            } else if ((mode == Constants.VOLUME_WATCHER_THIRTYDAY) && (volume > 0)) {
                return ((((double) volume - getThirtyDayAvgVolume()) * 100 / getThirtyDayAvgVolume()));
            } else if ((mode == Constants.VOLUME_WATCHER_NINETYDAY) && (volume > 0)) {
                return ((((double) volume - getNinetyDayAvgVolume()) * 100 / getNinetyDayAvgVolume()));
            } else {
                return 0.0;
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return 0.0;
        }
    }

    public long get7DVolume() {
        return getSevenDayAvgVolume();
    }

    public double get7DVolumeChange() {
        if ((volume > 0) && (getSevenDayAvgVolume() > 0)) {
            return ((((double) volume - getSevenDayAvgVolume()) * 100 / getSevenDayAvgVolume()));
        } else {
            return 0.0;
        }
    }

    public long get30DVolume() {
        return getThirtyDayAvgVolume();
    }

    public double get30DVolumeChange() {
        if ((volume > 0) && (getThirtyDayAvgVolume() > 0)) {
            return ((((double) volume - getThirtyDayAvgVolume()) * 100 / getThirtyDayAvgVolume()));
        } else {
            return 0.0;
        }
    }

    public long get90DVolume() {
        return getNinetyDayAvgVolume();
    }

    public double get90DVolumeChange() {
        if ((volume > 0) && (getNinetyDayAvgVolume() > 0)) {
            return ((((double) volume - getNinetyDayAvgVolume()) * 100 / getNinetyDayAvgVolume()));
        } else {
            return 0.0;
        }
    }

    public void decrementTimeUnit() {
        setNewsTimeoutPeriod(getNewsTimeoutPeriod() - 1);
    }

    public void incrementTimeUnit() {
        setNewsTimeoutPeriod(getNewsTimeoutPeriod() + 1);
    }

    public String getBaseSymbol() {
        return stringTable.get(ID_BASE_SYMBOL);
    }

    public void setBaseSymbol(String baseSymbol) {
        if (baseSymbol != null) {
            stringTable.put(ID_BASE_SYMBOL, baseSymbol);
        }
//		this.baseSymbol = baseSymbol;
    }

    public String getDisplaySymbol() {
        try {
            if ((stringTable.get(ID_DISPLAY_SYMBOL) != null)) {
                return stringTable.get(ID_DISPLAY_SYMBOL);
            } else {
                return symbol;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return symbol;
    }

    public double getCurrentPrice() {
        return currentPrice;
    }

    public void setCurrentPrice(double currentPrice) {
        this.currentPrice = currentPrice;
    }


    public class SymbolEvent implements Serializable {
        public int type;
        public int readStatus = Constants.ITEM_NOT_AVAILABLE;
        public String dataString = "";

        public SymbolEvent(int type, int readStatus, String dataString) {
            this.type = type;
            this.readStatus = readStatus;
            this.dataString = dataString;
        }
    }

    public int getSymbolEventStatus(int symEvType) {

        int status = Constants.ITEM_NOT_AVAILABLE;
        if (symbolEventHashTable.containsKey(symEvType)) {
            return symbolEventHashTable.get(symEvType).readStatus;
        }
        return status;
    }

    public SymbolEvent getSymbolEvent(int symEvType) {
        if (symbolEventHashTable.containsKey(symEvType)) {
            return symbolEventHashTable.get(symEvType);
        } else {
            SymbolEvent ev = new SymbolEvent(symEvType, Constants.ITEM_NOT_AVAILABLE, "");
            symbolEventHashTable.put(symEvType, ev);
            return ev;
        }
    }

    public boolean getOtherSymbolEventAvailbility() {
        if (symbolEventHashTable.containsKey(Constants.EVENT_RESEARCH_MATERIAL) || symbolEventHashTable.containsKey(Constants.EVENT_INSIDER_TRADES) || symbolEventHashTable.containsKey(Constants.EVENT_FINANCIAL_CALENDAR)) {
            return true;
        } else {
            return false;
        }
    }

    public Hashtable<Integer, SymbolEvent> getSymbolEventHashTable() {
        return symbolEventHashTable;
    }

    public boolean isSymbolEnabled() {
        /* if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer) && ExchangeStore.getSharedInstance().getExchange(getExchange()).isDefault()) {
            return isSymbolEnabled;
        } else if(ExchangeStore.getSharedInstance().getExchange(getExchange()).isDefault() && ExchangeStore.getSharedInstance().getExchange(getExchange()).isExchageMode()) {
            return true;
        } else {
            return true;
        }*/
        if (ExchangeStore.getSharedInstance().getExchange(getExchange()).isDefault() && ExchangeStore.getSharedInstance().getExchange(getExchange()).isExchageMode()) {
            return true;
        } else if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer) && ExchangeStore.getSharedInstance().getExchange(getExchange()).isDefault()) {
            return isSymbolEnabled;
        } else {
            return true;
        }
    }

    public void setSymbolEnabled(boolean symbolEnabled) {
        if (ExchangeStore.isValidSystemFinformationType(Meta.IT_Bandwidth_Optimizer)) {
            isSymbolEnabled = symbolEnabled;
        } else {
            isSymbolEnabled = true;
        }
    }

    public boolean isPredictedIndex() {
        return predictedIndex;
    }

    private void setPredictedIndex(long value) {
        predictedIndex = value == 1;
    }

    public String getSymbolCodeWithMarkets() {
        try {
            if (symbolCode.contains("`")) {
                return getSymbol() + " (" + (ExchangeStore.getSharedInstance().getMarket(getExchange(), getMarketID())).getDescription().trim() + ")";
            } else {
                return getSymbolCode();
            }
        } catch (Exception e) {
            return getSymbolCode();
        }
    }

    public void setChgVWAPBase(double value) {
        doubleTable.put(CHANGE_VWAP_BASE, value);
    }

    public double getChgVWAPBase() {
        try {
            Double value = doubleTable.get(CHANGE_VWAP_BASE);
            if (value != null) {
                return value;
            }
        } catch (Exception e) {
        }
        return 0;
    }

    //to-do

    public double getYTDStockValue() {
        try {
            Double value = doubleTable.get(ID_YTD_STOCK_VALUE);
            if (value != null) {
                return value;
            }
        } catch (Exception e) {
        }
        return 0;
    }

    public void setPercentageChgVWAPBase(double value) {
        doubleTable.put(PER_CHANGE_VWAP_BASE, value);
    }

    public double getPercentageChgVWAPBase() {
        try {
            Double value = doubleTable.get(PER_CHANGE_VWAP_BASE);
            if (value != null) {
                return value;
            }
        } catch (Exception e) {
        }
        return 0;
    }
    // Getters and Setters of BONDS...........
    //==========================================================================================

    private void setFundName(String value) {
        stringTable.put(BATE_MF_FUND_NAME, value);
    }

    public String getFundName() {
        String value = "";
        try {
            value = stringTable.get(BATE_MF_FUND_NAME);
            if (value == null) {
                value = "";
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return value;
    }

    //==========================================================================================

    private void setSettlementDate(long value) {
        longTable.put(BATE_MF_SETTLEMENT_DATE, value);
    }

    public long getSettlementDate() {
        long value = 0;
        try {
            value = longTable.get(BATE_MF_SETTLEMENT_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    //==========================================================================================

    private void issuedAmount(double value) {
        doubleTable.put(BATE_MF_ISSUED_AMOUNT, value);
    }

    public double getIssuedAmount() {
        double value = Double.NaN;
        try {
            value = doubleTable.get(BATE_MF_ISSUED_AMOUNT);
        } catch (Exception e) {
        }
        return value;
    }

    //==========================================================================================

    private void setDateofIssue(long value) {
        longTable.put(BATE_MF_DATE_OF_ISSUE, value);
    }

    public long getDateofIssue() {
        long value = 0;
        try {
            value = longTable.get(BATE_MF_DATE_OF_ISSUE);
        } catch (Exception e) {
        }
        return value;
    }

    //=========================================================================================

    private void setOutstandingAmount(double value) {
        doubleTable.put(BATE_MF_OUT_AMOUNT, value);
    }

    public double getOutstandingAmount() {
        double value = Double.NaN;
        try {
            value = doubleTable.get(BATE_MF_OUT_AMOUNT);
        } catch (Exception e) {
        }
        return value;
    }

    //=========================================================================================

    private void setMatuarityDate(long value) {
        longTable.put(BATE_MF_MATURITY_DATE, value);
    }

    public long getMatuarityDate() {
        long value = 0;
        try {
            value = longTable.get(BATE_MF_MATURITY_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    //=========================================================================================

    private void setCouponRate(double value) {
        doubleTable.put(BATE_MF_COUPON_RATE, value);
    }

    public double getCouponRate() {
        double value = Double.NaN;
        try {
            value = doubleTable.get(BATE_MF_COUPON_RATE);
        } catch (Exception e) {
        }
        return value;
    }

    //=========================================================================================

    private void setNominalValue(double value) {
        doubleTable.put(BATE_MF_NOMINAL_VALUE, value);
    }

    public double getNominalValue() {
        double value = Double.NaN;
        try {
            value = doubleTable.get(BATE_MF_NOMINAL_VALUE);
        } catch (Exception e) {
        }
        return value;
    }

    //=========================================================================================

    private void setIssueCurrency(String value) {
        stringTable.put(BATE_MF_ISSUE_CURRENCY, value);
    }

    public String getIssueCurrency() {
        String value = "";
        try {
            value = stringTable.get(BATE_MF_ISSUE_CURRENCY);
            if (value == null) {
                value = "";
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return value;
    }

    //========================================================================================

    private void setLotSize(int value) {
        lotSize = value;
    }

    public int getMFLotSize() {
        return lotSize;
    }

    //==========================================================================================

    private void setListingDate(long value) {
        longTable.put(BATE_MF_LISTING_DATE, value);
    }

    public long getListingDate() {
        long value = 0;
        try {
            value = longTable.get(BATE_MF_LISTING_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    //========================================================================================

    private void setBondType(int value) {
        bondType = value;
    }

    public int getBondType() {
        return bondType;
    }

    //========================================================================================

    private void tradedThroughDate(long value) {
        longTable.put(BATE_MF_TRADED_THROUGH_DATE, value);
    }

    public long getTradedThoughDate() {
        long value = 0;
        try {
            value = longTable.get(BATE_MF_TRADED_THROUGH_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    //========================================================================================

    private void setRateCalcType(int value) {
        rateCalcType = value;
    }

    public int getRateCalcType() {
        return rateCalcType;
    }

    //========================================================================================

    private void setDayCountMethod(int value) {
        dayCountMethod = value;
    }

    public int getDayCountMethod() {
        return dayCountMethod;
    }

    //========================================================================================

    private void setLoanNumber(String value) {
        stringTable.put(BATE_MF_LOAN_NUMBER, value);
    }

    public String getLoanNumber() {
        String value = "";
        try {
            value = stringTable.get(BATE_MF_LOAN_NUMBER);
            if (value == null) {
                value = "";
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return value;
    }

    //========================================================================================

    private void setExternalBondType(String value) {
        stringTable.put(BATE_MF_EXT_BOND_TYPE, value);
    }

    public String getExternalBondType() {
        String value = "";
        try {
            value = stringTable.get(BATE_MF_EXT_BOND_TYPE);
            if (value == null) {
                value = "";
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return value;
    }

    //========================================================================================

    private void setEarlyLateMatuarity(String value) {
        stringTable.put(BATE_MF_EARLY_LATE_MATURITY, value);
    }

    public String getEarlyLateMaturity() {
        String value = "";
        try {
            value = stringTable.get(BATE_MF_EARLY_LATE_MATURITY);
            if (value == null) {
                value = "";
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return value;
    }

    //========================================================================================

    private void setMatuarityValue(double value) {
        doubleTable.put(BATE_MF_MATURITY_VALUE, value);
    }

    public double getMaturityValue() {
        double value = Double.NaN;
        try {
            value = doubleTable.get(BATE_MF_MATURITY_VALUE);
        } catch (Exception e) {
        }
        return value;
    }

    //========================================================================================

    private void setYearlyCouponFreq(double value) {
        doubleTable.put(BATE_MF_YEARLY_COUPON_FREQ, value);
    }

    public double getYearlyCouponFreq() {
        double value = Double.NaN;
        try {
            value = doubleTable.get(BATE_MF_YEARLY_COUPON_FREQ);
        } catch (Exception e) {
        }
        return value;
    }

    //========================================================================================

    private void setRecordDateType(String value) {
        stringTable.put(BATE_MF_RECORD_DATE_TYPE, value);
    }

    public String getRecordDateType() {
        String value = "";
        try {
            value = stringTable.get(BATE_MF_RECORD_DATE_TYPE);
            if (value == null) {
                value = "";
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return value;
    }

    //========================================================================================

    private void setNumberofRecordDays(int value) {
        numberOfRecordDays = value;
    }

    public int getNumberOfRecordDays() {
        return numberOfRecordDays;
    }

    //========================================================================================

    private void setInterestFromDate(long value) {
        longTable.put(BATE_MF_INTEREST_FROM_DATE, value);
    }

    public long getInterestFromDate() {
        long value = 0;
        try {
            value = longTable.get(BATE_MF_INTEREST_FROM_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    //========================================================================================

    private void setInterestDayType(int value) {
        interestDayType = value;
    }

    public int getInterestDayType() {
        return interestDayType;
    }

    //========================================================================================

    private void setPreviousCouponDate(long value) {
        longTable.put(BATE_MF_PREV_COUPON_DATE, value);
    }

    public long getPreviousCouponDate() {
        long value = 0;
        try {
            value = longTable.get(BATE_MF_PREV_COUPON_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    //========================================================================================

    private void setLastCouponDate(long value) {
        longTable.put(BATE_MF_LAST_COUPON_DATE, value);
    }

    public long getLastCouponDate() {
        long value = 0;
        try {
            value = longTable.get(BATE_MF_LAST_COUPON_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    //========================================================================================

    private void setCouponRecordDate(long value) {
        longTable.put(BATE_MF_COUPON_RECORD_DATE, value);
    }

    public long getCouponRecordDate() {
        long value = 0;
        try {
            value = longTable.get(BATE_MF_COUPON_RECORD_DATE);
        } catch (Exception e) {
        }
        return value;
    }

    //========================================================================================

    private void setFaceValueDimension(int value) {
        faceValueDimension = value;
    }

    public int getFaceValueDimension() {
        return faceValueDimension;
    }

    //========================================================================================

    private void setNumberofCashflows(int value) {
        numberOfCashflows = value;
    }

    public int getNumberOfCashFlows() {
        return numberOfCashflows;
    }

    //========================================================================================

    private void setCouponFrequancy(double value) {
        doubleTable.put(BATE_MF_COUPON_FREQ, value);
    }

    public double getCouponFrequancy() {
        double value = Double.NaN;
        try {
            value = doubleTable.get(BATE_MF_COUPON_FREQ);
        } catch (Exception e) {
        }
        return value;
    }

    //========================================================================================

    private void setDaysToFirstCashFlow(int value) {
        daysToFirstCashFlow = value;
    }

    public int getDaysToFirstCashFlow() {
        return daysToFirstCashFlow;
    }

    //========================================================================================

    private void setDaysInFirstRecord(int value) {
        daysInFirstrecord = value;
    }

    public int getDaysInFirstRecord() {
        return daysInFirstrecord;
    }

    //========================================================================================

    private void setIndexCouponAdjustments(double value) {
        doubleTable.put(BATE_MF_INDEX_COUPON_ADJ, value);
    }

    public double getIndexCouponAdjustments() {
        double value = Double.NaN;
        try {
            value = doubleTable.get(BATE_MF_INDEX_COUPON_ADJ);
        } catch (Exception e) {
        }
        return value;
    }

    //========================================================================================

    private void setAccuredInterest(double value) {
        doubleTable.put(BATE_MF_ACCRUED_INTEREST, value);
    }

    public double getAccuredInterest() {
        double value = Double.NaN;
        try {
            value = doubleTable.get(BATE_MF_ACCRUED_INTEREST);
        } catch (Exception e) {
        }
        return value;
    }

    //========================================================================================

    private void setIndexFactor(double value) {
        doubleTable.put(BATE_MF_INDEX_FACTOR, value);
    }

    public double getIndexFactor() {
        double value = Double.NaN;
        try {
            value = doubleTable.get(BATE_MF_INDEX_FACTOR);
        } catch (Exception e) {
        }
        return value;
    }

    //========================================================================================

    private void setCalcConvention(int value) {
        calcConvention = value;
    }

    public int getCalcConvention() {
        return calcConvention;
    }

    public String getFirstSubscriptionStartDate() {
        return stringTable.get(FIRST_SUBSCRIPTION_START_DATE);
    }

    public void setFirstSubscriptionStartDate(String value) {
        if ((value != null) && (value.trim().length() > 0)) {
            stringTable.put(FIRST_SUBSCRIPTION_START_DATE, value);
        }
    }

    public String getFirstSubscriptionEndDate() {
        return stringTable.get(FIRST_SUBSCRIPTION_END_DATE);
    }

    public void setFirstSubscriptionEndDate(String value) {
        if ((value != null) && (value.trim().length() > 0)) {
            stringTable.put(FIRST_SUBSCRIPTION_END_DATE, value);
        }
    }

    public String getSecondSubscriptionStartDate() {
        return stringTable.get(SECOND_SUBSCRIPTION_START_DATE);
    }

    public void setSecondSubscriptionStartDate(String value) {
        if ((value != null) && (value.trim().length() > 0)) {
            stringTable.put(SECOND_SUBSCRIPTION_START_DATE, value);
        }
    }

    public String getSecondSubscriptionEndDate() {
        return stringTable.get(SECOND_SUBSCRIPTION_END_DATE);
    }

    public void setSecondSubscriptionEndDate(String value) {
        if ((value != null) && (value.trim().length() > 0)) {
            stringTable.put(SECOND_SUBSCRIPTION_END_DATE, value);
        }
    }

    // Theoretical values getters and setters
    public double getTop() {
        double value = 0;
        try {
            value = doubleTable.get(ID_TOP);
        } catch (Exception e) {
        }
        return value;
    }

    public long getTov() {
        long value = 0;
        try {
            value = longTable.get(ID_TOV);
        } catch (Exception e) {
        }
        return value;
    }

    public double getTcp() {
        double value = 0;
        try {
            value = doubleTable.get(ID_TCP);
        } catch (Exception e) {
        }
        return value;
    }

    public long getTcv() {
        long value = 0;
        try {
            value = longTable.get(ID_TCV);
        } catch (Exception e) {
        }
        return value;
    }

    public double getShareCapital() {
        double value =0;
        try {
            value = doubleTable.get(ID_SHARE_CAPITAL);
        }catch (Exception e) {

        }
        return value;
    }

    public long getCurrentAdjstVal() {
        long value = 0;
        try {
            value = longTable.get(ID_CURRENT_ADJUSTED_VALUE);
        }catch (Exception e) {

        }
        return value;
    }

    public double getStaticMin() {
        double value = 0;
        try {
            value = doubleTable.get(ID_STATIC_MIN);
        }catch (Exception e) {

        }

        return value;
    }

    public double getStaticMax() {
        double value = 0;
        try {
            value = doubleTable.get(ID_STATIC_MAX);
        }catch (Exception e) {

        }

        return value;
    }

    public void setTop(double value) {
        doubleTable.put(ID_TOP, value);
    }

    public void setTov(long value) {
        longTable.put(ID_TOV, value);
    }

    public void setTcp(double value) {
        doubleTable.put(ID_TCP, value);
    }

    public void setTcv(long value) {
        longTable.put(ID_TCV, value);
    }

    public void setShareCapital(double value) {
        doubleTable.put(ID_SHARE_CAPITAL, value);
    }

    public void setCurrentAdjstVal(long value) {
        longTable.put(ID_CURRENT_ADJUSTED_VALUE, value);
    }

    public void setStaticMin(double value) {
        doubleTable.put(ID_STATIC_MIN, value);
    }

    public void setStaticMax(double value) {
        doubleTable.put(ID_STATIC_MAX, value);
    }
}
