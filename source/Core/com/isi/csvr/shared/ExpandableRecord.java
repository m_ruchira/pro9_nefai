package com.isi.csvr.shared;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Iterator;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Nov 17, 2007
 * Time: 5:10:48 PM
 */
public class ExpandableRecord<E> {

    public static final int EXPANDED = 0;
    public static final int COLLAPSED = 1;

    private E baseRecord;
    private List<E> subRecords;
    private int status;
    private String id;

    public ExpandableRecord(String id, E baseRecord) {
        this.id = id;
        this.baseRecord = baseRecord;
        subRecords = Collections.synchronizedList(new ArrayList<E>());
    }

    public synchronized void addRecord(E record){
        subRecords.add(record);
    }

    public synchronized void setRecord(int index, E record){
        subRecords.set(index, record);
    }

    public void setExpanded(){
       status = EXPANDED;
    }

    public void setCollapsed(){
       status = COLLAPSED;
    }

    public boolean isExpanded(){
        return status == EXPANDED;
    }

    public boolean isCollapsed(){
        return status == COLLAPSED;
    }

    public E getBaseRecord() {
        return baseRecord;
    }

    public Iterator<E> getRecords(){
        return subRecords.iterator();
    }

    public String getId() {
        return id;
    }
}
