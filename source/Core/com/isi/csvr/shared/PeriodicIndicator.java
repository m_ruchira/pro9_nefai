package com.isi.csvr.shared;

import com.isi.csvr.Client;
import com.isi.csvr.communication.SendQInterface;
import com.isi.csvr.datastore.ExchangeStore;

import java.util.Date;
import java.util.StringTokenizer;

/**
 * Created by IntelliJ IDEA.
 * User: Nishanthab
 * Date: Mar 3, 2014
 * Time: 3:23:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class PeriodicIndicator  {

    private int EndOfDayIndicator;
    private String PeriodStart;
    private String PeriodStop;
    private double TWAS;
    private double RTWAS;
    private double TWABBid;
    private double TWABAsk;
    private double TVWAQ;
    private double TVOBAskVolume;
    private double TVOBAskValue;
    private double TNOBAsk;
    private double TVOAAskVolume;
    private double TVOAAskValue;
    private double TNOAAsk;
    private double TVOBBidVolume;
    private double TVOBBidValue;
    private double TNOBBid;
    private double TVOABidVolume;
    private double TVOABidValue;
    private double TNOABid;
    private double TVOBVolume;
    private double TVOBValue;
    private double TNOB;
    private double TVOAVolume;
    private double TVOAValue;
    private double TNOA;
    private double TotalOrderCoverage;
    private double BidOrderCoverage;
    private double AskOrderCoverage;
    private double BEVWAPBid;
    private double BEVWAPAsk;
    private double TotalVolumeBid;
    private double TotalVolumeAsk;
    private double BAIBest;
    private double BAIAll;
    private double CIBid;
    private double CIAsk;
    private String LocalRoundTripClasses ;
    private String LocalRoundTripCosts ;
    private String LocalRoundTripCoverages ;
    private String ReferenceRoundTripClasses ;
    private String ReferenceRoundTripCosts ;
    private String ReferenceRoundTripCoverages ;

   /* public static PeriodicIndicator getSharedInstance() {
        if (self == null) {
            self = new PeriodicIndicator();
        }
        return self;
    }*/

    public int getEndOfDayIndicator() {
        return EndOfDayIndicator;
    }

    public void setEndOfDayIndicator(int endOfDayIndicator) {
        EndOfDayIndicator = endOfDayIndicator;
    }

    public String getPeriodStart() {
        return PeriodStart;
    }

    public void setPeriodStart(String periodStart) {
        PeriodStart = periodStart;
    }

    public String getPeriodStop() {
        return PeriodStop;
    }

    public void setPeriodStop(String periodStop) {
        PeriodStop = periodStop;
    }

    public double getTWAS() {
        return TWAS;
    }

    public void setTWAS(double TWAS) {
        this.TWAS = TWAS;
    }

    public double getRTWAS() {
        return RTWAS;
    }

    public void setRTWAS(double RTWAS) {
        this.RTWAS = RTWAS;
    }

    public double getTWABBid() {
        return TWABBid;
    }

    public void setTWABBid(double TWABBid) {
        this.TWABBid = TWABBid;
    }

    public double getTWABAsk() {
        return TWABAsk;
    }

    public void setTWABAsk(double TWABAsk) {
        this.TWABAsk = TWABAsk;
    }

    public double getTVWAQ() {
        return TVWAQ;
    }

    public void setTVWAQ(double TVWAQ) {
        this.TVWAQ = TVWAQ;
    }

    public double getTVOBAskVolume() {
        return TVOBAskVolume;
    }

    public void setTVOBAskVolume(double TVOBAskVolume) {
        this.TVOBAskVolume = TVOBAskVolume;
    }

    public double getTVOBAskValue() {
        return TVOBAskValue;
    }

    public void setTVOBAskValue(double TVOBAskValue) {
        this.TVOBAskValue = TVOBAskValue;
    }

    public double getTNOBAsk() {
        return TNOBAsk;
    }

    public void setTNOBAsk(double TNOBAsk) {
        this.TNOBAsk = TNOBAsk;
    }

    public double getTVOAAskVolume() {
        return TVOAAskVolume;
    }

    public void setTVOAAskVolume(double TVOAAskVolume) {
        this.TVOAAskVolume = TVOAAskVolume;
    }

    public double getTVOAAskValue() {
        return TVOAAskValue;
    }

    public void setTVOAAskValue(double TVOAAskValue) {
        this.TVOAAskValue = TVOAAskValue;
    }

    public double getTNOAAsk() {
        return TNOAAsk;
    }

    public void setTNOAAsk(double TNOAAsk) {
        this.TNOAAsk = TNOAAsk;
    }

    public double getTVOBBidVolume() {
        return TVOBBidVolume;
    }

    public void setTVOBBidVolume(double TVOBBidVolume) {
        this.TVOBBidVolume = TVOBBidVolume;
    }

    public double getTVOBBidValue() {
        return TVOBBidValue;
    }

    public void setTVOBBidValue(double TVOBBidValue) {
        this.TVOBBidValue = TVOBBidValue;
    }

    public double getTNOBBid() {
        return TNOBBid;
    }

    public void setTNOBBid(double TNOBBid) {
        this.TNOBBid = TNOBBid;
    }

    public double getTVOABidVolume() {
        return TVOABidVolume;
    }

    public void setTVOABidVolume(double TVOABidVolume) {
        this.TVOABidVolume = TVOABidVolume;
    }

    public double getTVOABidValue() {
        return TVOABidValue;
    }

    public void setTVOABidValue(double TVOABidValue) {
        this.TVOABidValue = TVOABidValue;
    }

    public double getTNOABid() {
        return TNOABid;
    }

    public void setTNOABid(double TNOABid) {
        this.TNOABid = TNOABid;
    }

    public double getTVOBVolume() {
        return TVOBVolume;
    }

    public void setTVOBVolume(double TVOBVolume) {
        this.TVOBVolume = TVOBVolume;
    }

    public double getTVOBValue() {
        return TVOBValue;
    }

    public void setTVOBValue(double TVOBValue) {
        this.TVOBValue = TVOBValue;
    }

    public double getTNOB() {
        return TNOB;
    }

    public void setTNOB(double TNOB) {
        this.TNOB = TNOB;
    }

    public double getTVOAVolume() {
        return TVOAVolume;
    }

    public void setTVOAVolume(double TVOAVolume) {
        this.TVOAVolume = TVOAVolume;
    }

    public double getTVOAValue() {
        return TVOAValue;
    }

    public void setTVOAValue(double TVOAValue) {
        this.TVOAValue = TVOAValue;
    }

    public double getTNOA() {
        return TNOA;
    }

    public void setTNOA(double TNOA) {
        this.TNOA = TNOA;
    }

    public double getTotalOrderCoverage() {
        return TotalOrderCoverage;
    }

    public void setTotalOrderCoverage(double totalOrderCoverage) {
        TotalOrderCoverage = totalOrderCoverage;
    }

    public double getBidOrderCoverage() {
        return BidOrderCoverage;
    }

    public void setBidOrderCoverage(double bidOrderCoverage) {
        BidOrderCoverage = bidOrderCoverage;
    }

    public double getAskOrderCoverage() {
        return AskOrderCoverage;
    }

    public void setAskOrderCoverage(double askOrderCoverage) {
        AskOrderCoverage = askOrderCoverage;
    }

    public double getBEVWAPBid() {
        return BEVWAPBid;
    }

    public void setBEVWAPBid(double BEVWAPBid) {
        this.BEVWAPBid = BEVWAPBid;
    }

    public double getBEVWAPAsk() {
        return BEVWAPAsk;
    }

    public void setBEVWAPAsk(double BEVWAPAsk) {
        this.BEVWAPAsk = BEVWAPAsk;
    }

    public double getTotalVolumeBid() {
        return TotalVolumeBid;
    }

    public void setTotalVolumeBid(double totalVolumeBid) {
        TotalVolumeBid = totalVolumeBid;
    }

    public double getTotalVolumeAsk() {
        return TotalVolumeAsk;
    }

    public void setTotalVolumeAsk(double totalVolumeAsk) {
        TotalVolumeAsk = totalVolumeAsk;
    }

    public double getBAIBest() {
        return BAIBest;
    }

    public void setBAIBest(double BAIBest) {
        this.BAIBest = BAIBest;
    }

    public double getBAIAll() {
        return BAIAll;
    }

    public void setBAIAll(double BAIAll) {
        this.BAIAll = BAIAll;
    }

    public double getCIBid() {
        return CIBid;
    }

    public void setCIBid(double CIBid) {
        this.CIBid = CIBid;
    }

    public double getCIAsk() {
        return CIAsk;
    }

    public void setCIAsk(double CIAsk) {
        this.CIAsk = CIAsk;
    }

    public String getLocalRoundTripClasses() {
        return LocalRoundTripClasses;
    }

    public void setLocalRoundTripClasses(String localRoundTripClasses) {
        LocalRoundTripClasses = localRoundTripClasses;
    }

    public String getLocalRoundTripCosts() {
        return LocalRoundTripCosts;
    }

    public void setLocalRoundTripCosts(String localRoundTripCosts) {
        LocalRoundTripCosts = localRoundTripCosts;
    }

    public String getLocalRoundTripCoverages() {
        return LocalRoundTripCoverages;
    }

    public void setLocalRoundTripCoverages(String localRoundTripCoverages) {
        LocalRoundTripCoverages = localRoundTripCoverages;
    }

    public String getReferenceRoundTripClasses() {
        return ReferenceRoundTripClasses;
    }

    public void setReferenceRoundTripClasses(String referenceRoundTripClasses) {
        ReferenceRoundTripClasses = referenceRoundTripClasses;
    }

    public String getReferenceRoundTripCosts() {
        return ReferenceRoundTripCosts;
    }

    public void setReferenceRoundTripCosts(String referenceRoundTripCosts) {
        ReferenceRoundTripCosts = referenceRoundTripCosts;
    }

    public String getReferenceRoundTripCoverages() {
        return ReferenceRoundTripCoverages;
    }

    public void setReferenceRoundTripCoverages(String referenceRoundTripCoverages) {
        ReferenceRoundTripCoverages = referenceRoundTripCoverages;
    }

    public void setData(String data, int decimalFactor, boolean fromZip, String exchange) throws Exception {
        if (data == null)
            return;
        char tag;

       // setLastUpdatedTime(Client.getInstance().getMarketTime());
        StringTokenizer tokenizer = new StringTokenizer(data, Meta.FD);
//		for (int i = 0; i < data.length; i++) {
        while (tokenizer.hasMoreElements()) {
//			tag = data[i].charAt(0);
            String element = tokenizer.nextToken();
            tag = element.charAt(0);
            switch (tag) {
                /*case 'A':
                    setEndOfDayIndicator(Integer.parseInt(element.substring(1)) / decimalFactor);
                    break;*/
                case 'B':
                 //   setPeriodStart(""+ ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, Long.parseLong(element.substring(1))));
                    setPeriodStart(element.substring(1));
                    break;
                case 'C':
                    setPeriodStop(element.substring(1));
                    break;
                case 'D':
                    setTWAS(SharedMethods.getDouble(element.substring(1)));
                    break;
                case 'E':
                    setRTWAS(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'F':
                    setTWABBid(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'G':
                    setTWABAsk(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'H':
                    setTVWAQ(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'I':
                   setTVOBAskVolume(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'J':
                    setTVOBAskValue(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'K':
                    setTNOBAsk(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'L':
                    setTVOAAskVolume(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'M':
                    setTVOAAskValue(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'N':
                    setTNOAAsk(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'O':
                    setTVOBBidVolume(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'P':
                    setTVOBBidValue(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'Q':
                    setTNOBBid(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'R':
                    setTVOABidVolume(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'S':
                    setTVOABidValue(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'T':
                    setTNOABid(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'U':
                    setTVOBVolume(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'V':
                    setTVOBValue(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'W':
                    setTNOB(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'X':
                    setTVOAVolume(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'Y':
                    setTVOAValue(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'Z':
                    setTNOA(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'a':
                    setTotalOrderCoverage(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'b':
                    setBidOrderCoverage(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'c':
                    setAskOrderCoverage(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'd':
                    setBEVWAPBid(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'e':
                    setBEVWAPAsk(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'f':
                    setTotalVolumeBid(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'g':
                    setTotalVolumeAsk(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'h':
                    setBAIBest(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'i':
                    setBAIAll(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'j':
                    setCIBid(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
                case 'k':
                    setCIAsk(SharedMethods.getDouble(element.substring(1)) / decimalFactor);
                    break;
              /*  case 'l':
                    setLocalRoundTripClasses(element.substring(1));
                    break;
                case 'm':
                    setLocalRoundTripCosts(element.substring(1));
                    break;
                case 'n':
                    setLocalRoundTripCoverages(element.substring(1));
                    break;
                case 'o':
                    setReferenceRoundTripClasses(element.substring(1));
                    break;
                case 'p':
                    setReferenceRoundTripCosts(element.substring(1));
                    break;
                case 'q':
                    setReferenceRoundTripCoverages(element.substring(1));
                    break;*/
            }
        }
//        if (SharedMethods.getSymbolType(instrumentType) == Meta.SYMBOL_TYPE_FOREX) {
//            createForexBuySellValues(decimalFactor);
//        }

    }

}
