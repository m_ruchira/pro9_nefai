package com.isi.csvr.shared;

import javax.swing.*;
import java.awt.*;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;

public class PrintBuffer extends JDialog
        implements Printable {

    private int m_maxNumPage = 1;
    private JPanel printPanel;

    public PrintBuffer() {
        printPanel = new JPanel(new BorderLayout());
        this.getContentPane().add(printPanel);
        this.pack();
    }

    public void setComponent(Component component) {
        printPanel.add(BorderLayout.CENTER, component);
    }

    public void setHeader(Component header) {
        printPanel.add(BorderLayout.NORTH, header);
    }

    public int print(Graphics pg, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex >= m_maxNumPage) { // || m_bi == null)
            return NO_SUCH_PAGE;
        }

        pg.translate((int) pageFormat.getImageableX(),
                (int) pageFormat.getImageableY());
        int wPage = (int) pageFormat.getImageableWidth();
        int hPage = (int) pageFormat.getImageableHeight();
        int w = printPanel.getWidth();
        int h = printPanel.getHeight();
        if (w == 0 || h == 0)
            return NO_SUCH_PAGE;

        int nCol = Math.max((int) Math.ceil((double) w / wPage), 1);
        int nRow = Math.max((int) Math.ceil((double) h / hPage), 1);
        m_maxNumPage = nCol * nRow;
        int iCol = pageIndex % nCol;
        int iRow = pageIndex / nCol;
        int x = iCol * wPage;
        int y = iRow * hPage;
        int wImage = Math.min(wPage, w - x);
        int hImage = Math.min(hPage, h - y);
        Image bufferImg = printPanel.createImage(printPanel.getWidth(), printPanel.getHeight());
        Graphics g = bufferImg.getGraphics();
        printPanel.paint(g);
        pg.drawImage(bufferImg, 0, 0, wImage, hImage, x, y, x + wImage, y + hImage, this);
        //pg.drawImage(bufferImg, 0, 0, wImage, hImage, x, y, x+wImage, y+hImage, this);
        System.gc();

        return PAGE_EXISTS;
    }

    /*public int print(Graphics g, PageFormat pageFormat,
        int pageIndex) throws PrinterException {
        
         Graphics2D  g2      = (Graphics2D) g;
         g2.setColor(Color.black);
         int fontHeight      = g2.getFontMetrics().getHeight();
         int fontDesent      = g2.getFontMetrics().getDescent();

        JTable tableView    = table;

         //leave room for page number
         double pageHeight   = pageFormat.getImageableHeight()-(fontHeight*2);
         double pageWidth    = pageFormat.getImageableWidth();
         double tableWidth   = (double)tableView.getColumnModel().getTotalColumnWidth();
         double scale        = 1;
         if (tableWidth >= pageWidth) {
            scale =  pageWidth / tableWidth;
        }

         double headerHeightOnPage   = tableView.getTableHeader().getHeight()*scale;
         double tableWidthOnPage     = tableWidth*scale;

         //double oneRowHeight=(tableView.getRowHeight()+tableView.getRowMargin())*scale;
        double oneRowHeight         = (tableView.getRowHeight())*scale;
         int numRowsOnAPage          = (int)((pageHeight-headerHeightOnPage)/oneRowHeight);
         double pageHeightForTable   = oneRowHeight*numRowsOnAPage;
         int totalNumPages           = (int)Math.ceil(((double)tableView.getRowCount())/numRowsOnAPage);
         if(pageIndex>=totalNumPages) {
            return NO_SUCH_PAGE;
         }

         g2.translate(pageFormat.getImageableX(), pageFormat.getImageableY());
        //bottom center
         g2.drawString(getTitle(),(int)pageWidth/2-35, (int)(
             fontHeight-fontDesent));
         g2.drawString("Page: "+(pageIndex+1),(int)pageWidth/2-35, (int)(pageHeight
             +fontHeight+fontHeight-fontDesent));// two font heights. onr for header
                                                // one for footer
         g2.translate(0f,headerHeightOnPage+fontHeight);
         g2.translate(0f,-pageIndex*pageHeightForTable);

         //If this piece of the table is smaller
         //than the size available,
         //clip to the appropriate bounds.
         if (pageIndex + 1 == totalNumPages) {
           int lastRowPrinted = numRowsOnAPage * pageIndex;
           int numRowsLeft = tableView.getRowCount() - lastRowPrinted;
           g2.setClip(0,
             (int)(pageHeightForTable * pageIndex),
             (int) Math.ceil(tableWidthOnPage),
             (int) Math.ceil(oneRowHeight *
                               numRowsLeft));
         }
         //else clip to the entire area available.
         else{
             g2.setClip(0,
             (int)((pageHeightForTable*pageIndex)),
             (int) Math.ceil(tableWidthOnPage),
             (int) Math.ceil(pageHeightForTable));
         }

         g2.scale(scale,scale);
         tableView.paint(g2);
         g2.scale(1/scale,1/scale);
         g2.translate(0f,pageIndex*pageHeightForTable);
         g2.translate(0f, -headerHeightOnPage);
        g2.setClip(0, 0,
           (int) Math.ceil(tableWidthOnPage),
          (int)Math.ceil(headerHeightOnPage));
         /*g2.setClip(0, 0,
           (int) Math.ceil(g_oJScrollPane.getViewport().getWidth()),
          (int)Math.ceil(headerHeightOnPage));
         g2.scale(scale,scale);*
         tableView.getTableHeader().paint(g2);
         //paint header at top

         return Printable.PAGE_EXISTS;
   } */
}