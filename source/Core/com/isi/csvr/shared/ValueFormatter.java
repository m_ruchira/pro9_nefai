package com.isi.csvr.shared;


import javax.swing.text.AttributeSet;
import java.awt.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class ValueFormatter extends javax.swing.text.PlainDocument {
    public final static int LONG = 0;
    public final static int INTEGER = 2;
    public final static int POSITIVE_INTEGER = 6;
    public final static int DECIMAL = 1;
    public final static int POSITIVE_DECIMAL = 7;
    public final static int UPPERCASE = 3;
    public final static int BLANK = 4;
    public final static int NORMAL = 5;

    private int type;
    private long length = 0;
    private int decimalPlaces = 0;
//    private boolean allowNegative;


    public ValueFormatter(int type) {
        //Call the parent constructor
        this(type, -1);
    }

    public ValueFormatter(int type, int decimalPlaces) {
        //Call the parent constructor
        super();
        this.decimalPlaces = decimalPlaces;
        this.type = type;
    }
    /*public ValueFormatter(int type, boolean allowNegative) {
        //Call the parent constructor
        super();
        this.allowNegative = allowNegative;
        this.type = type;
    }*/

    public ValueFormatter(int type, long length) {
        this(type, length, -1);

    }

    public ValueFormatter(int type, long length, int decimalPlaces) {
        //Call the parent constructor
        super();
        this.type = type;
        this.length = length;
        this.decimalPlaces = decimalPlaces;
    }

    /*public ValueFormatter(int type, int length, boolean allowNegative) {
        //Call the parent constructor
        super();
        this.allowNegative = allowNegative;
        this.type = type;
        this.length = length;
    }*/

    public void insertString(int offset, String str, AttributeSet a) throws javax.swing.text.BadLocationException {
        StringBuffer text = new StringBuffer(getText(0, getLength()));

        if (type == NORMAL) {
            super.insertString(offset, str, a);
            return;
        } else if (type == BLANK) {
            if (str.startsWith(Meta.DS)) {
                super.insertString(offset, str.substring(1), a);
            } else {
                return;
            }
        } else {
            text.insert(offset, str);

            if (type == UPPERCASE) {
                if (getLength() < length) {
                    super.insertString(offset, str.toUpperCase(), a);
                } else {
                    Toolkit.getDefaultToolkit().beep();
                }
            } else {
                if ((length > 0) && (getLength() > length)) {
                    Toolkit.getDefaultToolkit().beep();
                    return;
                } else
                if (((type == POSITIVE_DECIMAL) || (type == POSITIVE_INTEGER) || (type == BLANK)) && (text.toString().equals("-"))) {
                    Toolkit.getDefaultToolkit().beep();
//                    super.insertString(offset, str, a);
                    return;
                }
                try {
                    if (Character.isLetter(text.charAt(text.length() - 1))) {
                        throw new Exception("Letter found");
                    }
                    switch (type) {
                        case LONG:
                            if (text.toString().equals("-")) {
                                // do nothing
                            } else {
                                Long.parseLong(text.toString());
                            }
                            //Long.parseLong(text.toString()); //Bug ID <#0021> must allow negative numbers
                            super.insertString(offset, str, a);
                            break;
                        case INTEGER:
                            if ((text.toString().equals("-"))) { //Bug ID <#0021> must allow negative numbers
                                // do nothing
                            } else {
                                Integer.parseInt(text.toString());
                            }
                            //Integer.parseInt(text.toString());
                            super.insertString(offset, str, a);
                            break;
                        case POSITIVE_INTEGER:
                            Integer.parseInt(text.toString());
                            if (Integer.parseInt(text.toString()) < 0) {
                                throw new Exception("Negative values not allowed");
                            }
                            super.insertString(offset, str, a);
                            break;
                        case DECIMAL:
                            if (text.toString().startsWith(".")) {
                                if (checkDecimalPlaces(text.toString())) {
                                    Double.parseDouble("0" + text.toString());
                                } else {
                                    throw new Exception("Too many decimal places");
                                }
                            } else if (text.toString().equals("-")) {
                                // do nothing
                            } else if (text.toString().equals("-.")) {
                                // do nothing
                            } else {
                                if (checkDecimalPlaces(text.toString())) {
                                    Double.parseDouble(text.toString());
                                } else {
                                    throw new Exception("Too many decimal places");
                                }
                            }
                            super.insertString(offset, str, a);
                            break;
                        case POSITIVE_DECIMAL:
                            if (text.toString().startsWith(".")) {
                                if (checkDecimalPlaces(text.toString())) {
                                    Double.parseDouble("0" + text.toString());
                                } else {
                                    throw new Exception("Too many decimal places");
                                }
                            } else {
                                if (Double.parseDouble(text.toString()) < 0) {
                                    throw new Exception("Negative values not allowed");
                                }
                                if (checkDecimalPlaces(text.toString())) {
                                    Double.parseDouble(text.toString());
                                } else {
                                    throw new Exception("Too many decimal places");
                                }
                            }
                            super.insertString(offset, str, a);
                            break;
                    }
                } catch (Exception e) {
                    Toolkit.getDefaultToolkit().beep();
                }
            }
        }
        text = null;
    }

    public boolean checkDecimalPlaces(String str) {
        if (decimalPlaces == -1) {
            return true;
        } else {
            int len = str.indexOf(".");
            String decimal = str.substring(len + 1);
            if(len==-1){
               return true;
            }else{
                  if (decimal.length() <= decimalPlaces) {
                    return true;
            } else {
                    return false;
            }
          }

        }


    }
}