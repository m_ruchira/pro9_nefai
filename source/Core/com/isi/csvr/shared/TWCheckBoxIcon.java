package com.isi.csvr.shared;

import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.plaf.UIResource;
import javax.swing.plaf.metal.MetalLookAndFeel;
import java.io.Serializable;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 29, 2005
 * Time: 3:45:14 PM
 */
public class TWCheckBoxIcon implements Icon, UIResource, Serializable {

    private Icon unselsectedIcon;
    private Icon unselsectedDisabledIcon;
    private Icon selectedIcon;
    private Icon selectedDisabledIcon;

    public TWCheckBoxIcon(Icon unselsected, Icon unselsectedDisabled, Icon selected, Icon selectedDisabled) {
        if (unselsected.getIconHeight() > 0)
            this.unselsectedIcon = unselsected;
        else
            this.unselsectedIcon = null;

        if (unselsectedDisabled.getIconHeight() > 0)
            this.unselsectedDisabledIcon = unselsectedDisabled;
        else
            this.unselsectedDisabledIcon = null;

        if (selected.getIconHeight() > 0)
            this.selectedIcon = selected;
        else
            this.selectedIcon = null;

        if (selectedDisabled.getIconHeight() > 0)
            this.selectedDisabledIcon = selectedDisabled;
        else
            this.selectedDisabledIcon = null;
    }

    public TWCheckBoxIcon(){

          this.unselsectedIcon = null;
          this.unselsectedDisabledIcon = null;
          this.selectedIcon = null;
            this.selectedDisabledIcon = null;


    }

    protected int getControlSize() {
        if (unselsectedIcon != null){
            return Math.max(unselsectedIcon.getIconWidth(), unselsectedIcon.getIconHeight());
        } else {
            return 13;
        }
    }

    public void paintIcon(Component c, Graphics g, int x, int y) {

        ButtonModel cb;
        if (c instanceof AbstractButton) {
            AbstractButton b = (AbstractButton) c;
            cb = b.getModel();
        } else {
            cb = (ButtonModel) c;
        }
        int controlSize = getControlSize();

        if (cb.isSelected()){
            if (cb.isEnabled()) {
                if (selectedIcon == null){
                    g.setColor(Theme.getColor("LABEL_FGCOLOR"));
                    g.drawRect(x, y, controlSize - 1, controlSize - 1);
                    drawCheck(c, g, x, y);
                } else {
                    selectedIcon.paintIcon(c, g, x, y);
                }
            } else {
                if (selectedDisabledIcon == null){
                    g.setColor(MetalLookAndFeel.getControlShadow());
                    g.drawRect(x, y, controlSize - 1, controlSize - 1);
                    drawCheck(c, g, x, y);
                } else {
                    selectedDisabledIcon.paintIcon(c, g, x, y);
                }
            }
        } else {
            if (cb.isEnabled()) {
                if (unselsectedIcon == null){
                    g.setColor(Theme.getColor("LABEL_FGCOLOR"));
                    g.drawRect(x, y, controlSize - 1, controlSize - 1);
                } else {
                    unselsectedIcon.paintIcon(c, g, x, y);
                }
            } else {
                if (unselsectedDisabledIcon == null){
                    g.setColor(MetalLookAndFeel.getControlShadow());
                    g.drawRect(x, y, controlSize - 1, controlSize - 1);
                } else {
                    unselsectedDisabledIcon.paintIcon(c, g, x, y);
                }
            }
        }
    }

    protected void drawCheck(Component c, Graphics g, int x, int y) {
        int controlSize = getControlSize();
        g.fillRect(x + 2, y + 2, controlSize - 4, controlSize - 4);
    }

    public int getIconWidth() {
        return getControlSize();
    }

    public int getIconHeight() {
        return getControlSize();
    }
}
