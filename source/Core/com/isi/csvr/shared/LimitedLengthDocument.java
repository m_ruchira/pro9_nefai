package com.isi.csvr.shared;

import javax.swing.text.AttributeSet;
import java.awt.*;

public class LimitedLengthDocument extends javax.swing.text.PlainDocument {
    private int m_maxLength;

    public LimitedLengthDocument(int max_length) {
        //Call the parent constructor
        super();
        //Set the maximum length of the
        m_maxLength = max_length;
    }

    public void insertString(int offset, String str, AttributeSet a) throws javax.swing.text.BadLocationException {
        if (getLength() < m_maxLength)
            super.insertString(offset, str, a);
        else {
            Toolkit.getDefaultToolkit().beep();
        }
    }
}
