package com.isi.csvr.shared;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class UpdateNotifier {
    private static long shapshotUpdatedTime = 0;
    private static long tradeUpdatedTime = 0;

    public static synchronized void setShapshotUpdated() {
        shapshotUpdatedTime = tradeUpdatedTime++;
        //SharedMethods.printLine("setShapshotUpdated", true);
//        shapshotUpdatedTime = System.currentTimeMillis();
    }

    public static synchronized long getShapshotUpdatedTime(long lastUpdatedTime) {
        if (lastUpdatedTime < shapshotUpdatedTime) {
            return shapshotUpdatedTime;
        } else {
            return lastUpdatedTime;
        }
    }

    /*public static synchronized void setTradeUpdated() {
        tradeUpdatedTime = System.currentTimeMillis();
    }*/

    public static synchronized boolean isTradeUpdatedTime(long lastUpdatedTime) {
        return (lastUpdatedTime < tradeUpdatedTime);
    }


}