package com.isi.csvr.shared;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 30, 2004
 * Time: 10:26:18 AM
 */
public class TWComboItem implements Comparable, Serializable {
    protected String id;
    protected String value;
    protected String type;

    public TWComboItem(String id, String value) {
        this.id = id;
        this.value = value;
    }

    public TWComboItem(int id, String value) {
        this.id = "" + id;
        this.value = value;
    }
       public TWComboItem(String id, String value,String type) {
        this.id = id;
        this.value = value;
        this.type=type;
    }
      public TWComboItem(int id, String value,String type) {
        this.id = "" + id;
        this.value = value;
        this.type=type;
    }

    public String getId() {
        return id;
    }

    public String getValue() {
        return value;
    }
    public String getType(){
        return type;
    }

    public String toString() {
        return value;
    }

    public boolean equals(Object obj) {
        try {
            return id.equals(((TWComboItem) obj).getId());
        } catch (Exception e) {
            return false;
        }
    }

    public int compareTo(Object item) {
        try {
            return value.compareTo(((TWComboItem) item).getValue());
        } catch (Exception e) {
            return -1;
        }
    }
}
