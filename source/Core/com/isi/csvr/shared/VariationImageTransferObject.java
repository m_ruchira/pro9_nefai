package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Oct 16, 2006
 * Time: 5:34:57 PM
 */
public class VariationImageTransferObject implements TransferObject {
    private double value;
    private long flag;
    private boolean blank;
    private String exchange;
    private double[] valuearray;//=new double[13];

    public VariationImageTransferObject() {
    }

    public double getValue() {
        return value;
    }
    public double[] getValueArray() {
        return valuearray;
    }

    public long getFlag() {
        return flag;
    }

    public VariationImageTransferObject setValue(double value) {
        this.value = value;
        return this;
    }
    public VariationImageTransferObject setValueArray(double[] vals) {
        this.valuearray = vals;
        return this;
    }

    public void setFlag(long flag) {
        this.flag = flag;
    }


    public boolean isBlank() {
        return blank;
    }

    public void setBlank(boolean blank) {
        this.blank = blank;
    }


    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }
}
