package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 3, 2004
 * Time: 2:24:55 PM
 */
public class IntTransferObject implements TransferObject {
    private long flag;
    private int value;
    private boolean blank;

    public int getValue() {
        return value;
    }

    public long getFlag() {
        return flag;
    }

    public int getCellValue() {
        return value;
    }

    public IntTransferObject() {
    }

    public void setCellValue(int value){
       this.value=value;
    }

    public IntTransferObject setValue(int value) {
        this.value = value;
        return this;
    }

    public void setFlag(long flag) {
        this.flag = flag;
    }

    public boolean isBlank() {
        return blank;
    }

    public void setBlank(boolean blank) {
        this.blank = blank;
    }
}
