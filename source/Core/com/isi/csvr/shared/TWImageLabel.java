package com.isi.csvr.shared;

import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 2, 2005
 * Time: 10:05:47 AM
 */
public class TWImageLabel extends JLabel implements Themeable {

    private String imageFileName;

    public TWImageLabel() {
        super();
        Theme.registerComponent(this);
    }

    public TWImageLabel(Icon image) {
        super(image);
        Theme.registerComponent(this);
    }

    public TWImageLabel(Icon image, int horizontalAlignment) {
        super(image, horizontalAlignment);
        Theme.registerComponent(this);
    }

    public TWImageLabel(String text) {
        super(text);
        Theme.registerComponent(this);
    }

    public TWImageLabel(String text, int horizontalAlignment) {
        super(text, horizontalAlignment);
        Theme.registerComponent(this);
    }

    public TWImageLabel(String text, Icon icon, int horizontalAlignment) {
        super(text, icon, horizontalAlignment);
        Theme.registerComponent(this);
    }

    /**
     * This special method binds the label to the Theme system where image is changed with
     * each theme change
     *
     * @param imageFileName
     */
    public void setImageFileName(String imageFileName) {
        this.imageFileName = imageFileName;
        setIcon(new ImageIcon("images/theme" + Theme.getID() + "/" + imageFileName));
    }

    public void applyTheme() {
        setIcon(new ImageIcon("images/theme" + Theme.getID() + "/" + imageFileName));
    }
}
