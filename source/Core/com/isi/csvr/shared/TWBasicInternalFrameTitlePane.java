package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 13, 2005
 * Time: 11:15:21 PM
 */

import com.isi.csvr.ClientTable;
import com.isi.csvr.DetachableRootPane;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.mist.MISTTable;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.dnd.DraggableTable;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.table.SmartTable;
import sun.swing.DefaultLookup;
import sun.swing.SwingUtilities2;

import javax.accessibility.AccessibleContext;
import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.plaf.ActionMapUIResource;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.beans.PropertyVetoException;

/**
 * The class that manages a basic title bar
 * <p/>
 * <strong>Warning:</strong>
 * Serialized objects of this class will not be compatible with
 * future Swing releases. The current serialization support is
 * appropriate for short term storage or RMI between applications running
 * the same version of Swing.  As of 1.4, support for long term storage
 * of all JavaBeans<sup><font size="-2">TM</font></sup>
 * has been added to the <code>java.beans</code> package.
 * Please see {@link java.beans.XMLEncoder}.
 *
 * @author David Kloba
 * @author Steve Wilson
 * @version 1.41 01/18/01
 */
public class TWBasicInternalFrameTitlePane extends JComponent {
    protected JMenuBar menuBar;
    protected JButton iconButton;
    protected JButton maxButton;
    protected JButton closeButton;
    protected JButton resizeButton;
    protected JButton detachButton;
    protected JButton printButton;
    protected JButton servicesButton;
    protected JButton linkGroupButton;

    protected JMenu windowMenu;
    protected JInternalFrame frame;

    protected Color selectedTitleColorDark;
    protected Color selectedTitleColorLight;
    protected Color selectedTextColor;
    protected Color notSelectedTitleColorDark;
    protected Color notSelectedTitleColorLight;
    protected Color notSelectedTextColor;

    protected Icon maxIcon;
    protected Icon maxIconRollOver;
    protected Icon minIcon;
    protected Icon minIconRollOver;
    protected Icon iconIcon;
    protected Icon iconIconRollOver;
    protected Icon closeIcon;
    protected Icon closeIconRollOver;
    protected Icon resizeIcon;
    protected Icon resizeIconRollOver;
    protected Icon detachIcon;
    protected Icon detachIconRollOver;
    protected Icon printIcon;
    protected Icon printIconRollOver;
    protected Icon servicesIcon;
    protected Icon linkedIcon;
    protected Icon servicesIconRollOver;

    protected PropertyChangeListener propertyChangeListener;

    protected Action closeAction;
    protected Action maximizeAction;
    protected Action iconifyAction;
    protected Action restoreAction;
    protected Action printAction;
    protected Action linkGroupAction;
    protected Action servicesAction;
    protected Action resizeColsAction;
    protected Action detachColsAction;
    protected Action moveAction;
    protected Action sizeAction;

    protected static final String CLOSE_CMD =
            UIManager.getString("InternalFrameTitlePane.closeButtonText");
    protected static final String ICONIFY_CMD =
            UIManager.getString("InternalFrameTitlePane.minimizeButtonText");
    protected static final String RESTORE_CMD =
            UIManager.getString("InternalFrameTitlePane.restoreButtonText");
    protected static final String MAXIMIZE_CMD =
            UIManager.getString("InternalFrameTitlePane.maximizeButtonText");
    protected static final String MOVE_CMD =
            UIManager.getString("InternalFrameTitlePane.moveButtonText");
    protected static final String SIZE_CMD =
            UIManager.getString("InternalFrameTitlePane.sizeButtonText");
    protected static final String PRINT_CMD = "Print";
    protected static final String SERVICES_CMD = "Services";
    protected static final String RESIZE_CMD = "ResizeCols";
    protected static final String DETACH_CMD = "DetachWindow";
    protected static final String LINKGRP_CMD = "LinkGroup";

    private String closeButtonToolTip;
    private String iconButtonToolTip;
    private String restoreButtonToolTip;
    private String maxButtonToolTip;
    private String printButtonToolTip;
    private String servicesButtonToolTip;
    private String resizeColsButtonToolTip;
    private String detachColsButtonToolTip;
    private String lnkGroupButtonToolTip;
    private Handler handler;
    private boolean printInProgress = false;

    public TWBasicInternalFrameTitlePane(JInternalFrame f) {
        frame = f;
        installTitlePane();
    }

    protected void installTitlePane() {
        installDefaults();
        installListeners();

        createActions();
        enableActions();
        createActionMap();

        setLayout(createLayout());

        assembleSystemMenu();
        createButtons();
        addSubComponents();

    }

    protected void addSubComponents() {
        add(menuBar);
        add(iconButton);
        add(maxButton);
        add(closeButton);
        add(resizeButton);
        add(detachButton);
        add(printButton);
        add(servicesButton);
        add(linkGroupButton);
    }

    protected void createActions() {
        maximizeAction = new MaximizeAction();
        iconifyAction = new IconifyAction();
        closeAction = new CloseAction();
        restoreAction = new RestoreAction();
        printAction = new PrintAction();
        servicesAction = new ServicesAction();
        linkGroupAction = new LinkGroupAction();
        resizeColsAction = new ResizeColsAction();
        detachColsAction = new DetachColsAction();
        moveAction = new MoveAction();
        sizeAction = new SizeAction();
    }

    ActionMap createActionMap() {
        ActionMap map = new ActionMapUIResource();
        map.put("showSystemMenu", new ShowSystemMenuAction(true));
        map.put("hideSystemMenu", new ShowSystemMenuAction(false));
        return map;
    }

    protected void installListeners() {
        if (propertyChangeListener == null) {
            propertyChangeListener = createPropertyChangeListener();
        }
        frame.addPropertyChangeListener(propertyChangeListener);
    }

    protected void uninstallListeners() {
        frame.removePropertyChangeListener(propertyChangeListener);
        handler = null;
    }

    private Icon getIcon(String id){
        try {
            if ((UIManager.getIcon(id).getIconHeight()) > 0){
                return UIManager.getIcon(id);
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    protected void installDefaults() {
        maxIcon = getIcon("InternalFrame.maximizeIcon");
        maxIconRollOver = getIcon("InternalFrame.maximizeIconRollOver");
        minIcon = getIcon("InternalFrame.minimizeIcon");
        minIconRollOver = getIcon("InternalFrame.minimizeIconRollOver");
        iconIcon = getIcon("InternalFrame.iconifyIcon");
        iconIconRollOver = getIcon("InternalFrame.iconifyIconRollOver");
        closeIcon = getIcon("InternalFrame.closeIcon");
        closeIconRollOver = getIcon("InternalFrame.closeIconRollOver");
        printIcon = getIcon("InternalFrame.printIcon");
        printIconRollOver = getIcon("InternalFrame.printIconRollOver");
        servicesIcon = getIcon("InternalFrame.servicesIcon");
        servicesIconRollOver = getIcon("InternalFrame.servicesIconRollOver");
        resizeIcon = getIcon("InternalFrame.resizeIcon");
        resizeIconRollOver = getIcon("InternalFrame.resizeIconRollOver");
        detachIcon = getIcon("InternalFrame.detachIcon");
        detachIconRollOver = getIcon("InternalFrame.detachIconRollOver");

        selectedTitleColorLight = UIManager.getColor("InternalFrame.activeTitleBackgroundLight");
        selectedTitleColorDark = UIManager.getColor("InternalFrame.activeTitleBackgroundDark");
        selectedTextColor = UIManager.getColor("InternalFrame.activeTitleForeground");
        notSelectedTitleColorDark = UIManager.getColor("InternalFrame.inactiveTitleBackgroundDark");
        notSelectedTitleColorLight = UIManager.getColor("InternalFrame.inactiveTitleBackgroundLight");
        notSelectedTextColor = UIManager.getColor("InternalFrame.inactiveTitleForeground");
        setFont(UIManager.getFont("InternalFrame.titleFont"));
        closeButtonToolTip =
                UIManager.getString("InternalFrame.closeButtonToolTip");
        iconButtonToolTip =
                UIManager.getString("InternalFrame.iconButtonToolTip");
        restoreButtonToolTip =
                UIManager.getString("InternalFrame.restoreButtonToolTip");
        maxButtonToolTip =
                UIManager.getString("InternalFrame.maxButtonToolTip");
        printButtonToolTip =
                UIManager.getString("InternalFrame.printButtonToolTip");
        servicesButtonToolTip =
                UIManager.getString("InternalFrame.servicesButtonToolTip");
        resizeColsButtonToolTip =
                UIManager.getString("InternalFrame.resizeColsButtonToolTip");
        detachColsButtonToolTip =
                UIManager.getString("InternalFrame.detachButtonToolTip");
        lnkGroupButtonToolTip =
                 UIManager.getString("InternalFrame.lnkGroupButtonToolTip");
    }


    protected void uninstallDefaults() {
        /*iconButton = null;
        maxButton = null;
        closeButton = null;
        resizeButton = null;
        printButton = null;*/

    }

    protected void createButtons() {
        iconButton = new NoFocusButton("InternalFrameTitlePane.iconifyButtonAccessibleName");
        iconButton.addActionListener(iconifyAction);
        if (iconButtonToolTip != null && iconButtonToolTip.length() != 0) {
            iconButton.setToolTipText(iconButtonToolTip);
        }

        maxButton = new NoFocusButton("InternalFrameTitlePane.maximizeButtonAccessibleName");
        maxButton.addActionListener(maximizeAction);

        closeButton = new NoFocusButton("InternalFrameTitlePane.closeButtonAccessibleName");
        closeButton.addActionListener(closeAction);
        if (closeButtonToolTip != null && closeButtonToolTip.length() != 0) {
            closeButton.setToolTipText(closeButtonToolTip);
        }

        printButton = new NoFocusButton("InternalFrameTitlePane.printButtonAccessibleName");
        printButton.addActionListener(printAction);
        servicesButton = new NoFocusButton("InternalFrameTitlePane.servicesButtonAccessibleName");
        servicesButton.addActionListener(servicesAction);
        linkGroupButton = new LinkGroupButton("InternalFrameTitlePane.linkGroupButtonAccessibleName", LinkStore.LINK_NONE);
        linkGroupButton.addActionListener(linkGroupAction);
        resizeButton = new NoFocusButton("InternalFrameTitlePane.resizeButtonAccessibleName");
        resizeButton.addActionListener(resizeColsAction);
        detachButton = new NoFocusButton("InternalFrameTitlePane.detachButtonAccessibleName");
        detachButton.addActionListener(detachColsAction);

        setButtonIcons();
    }

    protected void setButtonIcons() {
        if (frame.isIcon()) {
            if (minIcon != null) {
                iconButton.setIcon(minIcon);
            }
            if (minIconRollOver != null) {
                iconButton.setRolloverIcon(minIconRollOver);
            }
            if (restoreButtonToolTip != null &&
                    restoreButtonToolTip.length() != 0) {
                iconButton.setToolTipText(restoreButtonToolTip);
            }
            if (maxIcon != null) {
                maxButton.setIcon(maxIcon);
            }
            if (maxIconRollOver != null) {
                maxButton.setRolloverIcon(maxIconRollOver);
            }
            if (maxButtonToolTip != null && maxButtonToolTip.length() != 0) {
                maxButton.setToolTipText(maxButtonToolTip);
            }
        } else if (frame.isMaximum()) {
            if (iconIcon != null) {
                iconButton.setIcon(iconIcon);
            }
            if (iconIconRollOver != null) {
                iconButton.setRolloverIcon(iconIconRollOver);
            }
            if (iconButtonToolTip != null && iconButtonToolTip.length() != 0) {
                iconButton.setToolTipText(iconButtonToolTip);
            }
            if (minIcon != null) {
                maxButton.setIcon(minIcon);
            }
            if (minIconRollOver != null) {
                maxButton.setRolloverIcon(minIconRollOver);
            }
            if (restoreButtonToolTip != null &&
                    restoreButtonToolTip.length() != 0) {
                maxButton.setToolTipText(restoreButtonToolTip);
            }
        } else {
            if (iconIcon != null) {
                iconButton.setIcon(iconIcon);
            }
            if (iconIconRollOver != null) {
                iconButton.setRolloverIcon(iconIconRollOver);
            }
            if (iconButtonToolTip != null && iconButtonToolTip.length() != 0) {
                iconButton.setToolTipText(iconButtonToolTip);
            }
            if (maxIcon != null) {
                maxButton.setIcon(maxIcon);
            }
            if (maxIconRollOver != null) {
                maxButton.setRolloverIcon(maxIconRollOver);
            }
            if (maxButtonToolTip != null && maxButtonToolTip.length() != 0) {
                maxButton.setToolTipText(maxButtonToolTip);
            }
        }
        if (closeIcon != null) {
            closeButton.setIcon(closeIcon);
        }
        if (closeIconRollOver != null) {
            closeButton.setRolloverIcon(closeIconRollOver);
        }

        if (printIcon != null) {
            printButton.setIcon(printIcon);
        }
        if (printIconRollOver != null) {
            printButton.setRolloverIcon(printIconRollOver);
        }

        if (printButtonToolTip != null && printButtonToolTip.length() != 0) {
            printButton.setToolTipText(printButtonToolTip);
        }

        if (servicesIcon != null) {
            servicesButton.setIcon(servicesIcon);
        }
        if (servicesIcon != null) {
            servicesButton.setRolloverIcon(servicesIconRollOver);
        }

        if (servicesButtonToolTip != null && servicesButtonToolTip.length() != 0) {
            servicesButton.setToolTipText(servicesButtonToolTip);
        }
        if(frame instanceof TWTitlePaneInterface){
        ((LinkGroupButton) linkGroupButton).setGroupID(((TWTitlePaneInterface) frame).getLinkGroupIDForTitleBar());
            if (lnkGroupButtonToolTip != null && lnkGroupButtonToolTip.length() != 0) {
            linkGroupButton.setToolTipText(lnkGroupButtonToolTip);
        }
        }
        if (resizeIcon != null) {
            resizeButton.setIcon(resizeIcon);
        }
        if (resizeIconRollOver != null) {
            resizeButton.setRolloverIcon(resizeIconRollOver);
        }
        if (resizeColsButtonToolTip != null && resizeColsButtonToolTip.length() != 0) {
            resizeButton.setToolTipText(resizeColsButtonToolTip);
        }

        if (detachIcon != null) {
            detachButton.setIcon(detachIcon);
        }
        if (detachIconRollOver != null) {
            detachButton.setRolloverIcon(detachIconRollOver);
        }
        if (detachColsButtonToolTip != null && detachColsButtonToolTip.length() != 0) {
            detachButton.setToolTipText(detachColsButtonToolTip);
        }
    }

    protected void assembleSystemMenu() {
        menuBar = createSystemMenuBar();
        windowMenu = createSystemMenu();
        menuBar.add(windowMenu);
        addSystemMenuItems(windowMenu);
        enableActions();
    }

    protected void addSystemMenuItems(JMenu systemMenu) {
        JMenuItem mi = (JMenuItem) systemMenu.add(restoreAction);
        mi.setMnemonic('R');
        mi = (JMenuItem) systemMenu.add(moveAction);
        mi.setMnemonic('M');
        mi = (JMenuItem) systemMenu.add(sizeAction);
        mi.setMnemonic('S');
        mi = (JMenuItem) systemMenu.add(iconifyAction);
        mi.setMnemonic('n');
        mi = (JMenuItem) systemMenu.add(maximizeAction);
        mi.setMnemonic('x');
        systemMenu.add(new JSeparator());
        mi = (JMenuItem) systemMenu.add(closeAction);
        mi.setMnemonic('C');
    }

    protected JMenu createSystemMenu() {
        return new JMenu("    ");
    }

    protected JMenuBar createSystemMenuBar() {
        menuBar = new SystemMenuBar();
        menuBar.setBorderPainted(false);
        return menuBar;
    }

    protected void showSystemMenu() {
        //      windowMenu.setPopupMenuVisible(true);
        //      windowMenu.setVisible(true);
        windowMenu.doClick();
    }

    /*public void paintComponent(Graphics g) {
        paintTitleBackground(g);

        if (frame.getTitle() != null) {
            boolean isSelected = frame.isSelected();
            Font f = g.getFont();
            g.setFont(getFont());
            if (isSelected)
                g.setColor(selectedTextColor);
            else
                g.setColor(notSelectedTextColor);

// Center text vertically.
            FontMetrics fm = SwingUtilities2.getFontMetrics(frame, g);
            int baseline = (getHeight() + fm.getAscent() - fm.getLeading() -
                    fm.getDescent()) / 2;

            int titleX;
            Rectangle r = new Rectangle(0, 0, 0, 0);
            if (frame.isIconifiable())
                r = iconButton.getBounds();
            else if (frame.isMaximizable())
                r = maxButton.getBounds();
            else if (frame.isClosable()) r = closeButton.getBounds();
            int titleW;

            String title = frame.getTitle();
            if (SharedMethods.isLeftToRight(frame)) {
                if (r.x == 0) r.x = frame.getWidth() - frame.getInsets().right;
                titleX = menuBar.getX() + menuBar.getWidth() + 2;
                titleW = r.x - titleX - 3;
                title = getTitle(frame.getTitle(), fm, titleW);
            } else {
                titleX = menuBar.getX() - 2
                        - SwingUtilities2.stringWidth(frame, fm, title);
            }

            SwingUtilities2.drawString(frame, g, title, titleX, baseline);
            g.setFont(f);
        }
    }*/

    /**
     * Invoked from paintComponent.
     * Paints the background of the titlepane.  All text and icons will
     * then be rendered on top of this background.
     *
     * @param g the graphics to use to render the background
     * @since 1.4
     */
    protected void paintTitleBackground(Graphics g) {
        boolean isSelected = frame.isSelected();

        if (isSelected)
            g.setColor(selectedTitleColorDark);
        else
            g.setColor(notSelectedTitleColorDark);
        g.fillRect(0, 0, getWidth(), getHeight());
    }

    protected String getTitle(String text, FontMetrics fm, int availTextWidth) {
        return SwingUtilities2.clipStringIfNecessary(frame, fm, text, availTextWidth);
    }

    /**
     * Post a WINDOW_CLOSING-like event to the frame, so that it can
     * be treated like a regular Frame.
     */
    protected void postClosingEvent(JInternalFrame frame) {
        InternalFrameEvent e = new InternalFrameEvent(frame, InternalFrameEvent.INTERNAL_FRAME_CLOSING);
        // Try posting event, unless there's a SecurityManager.
        if (JInternalFrame.class.getClassLoader() == null) {
            try {
                Toolkit.getDefaultToolkit().getSystemEventQueue().postEvent(e);
                return;
            } catch (SecurityException se) {
                // Use dispatchEvent instead.
            }
        }
        frame.dispatchEvent(e);
    }


    protected void enableActions() {
        restoreAction.setEnabled(frame.isMaximum() || frame.isIcon());
        maximizeAction.setEnabled((frame.isMaximizable() && !frame.isMaximum() && !frame.isIcon()) ||
                (frame.isMaximizable() && frame.isIcon()));
        iconifyAction.setEnabled(frame.isIconifiable() && !frame.isIcon());
        closeAction.setEnabled(frame.isClosable());
        sizeAction.setEnabled(false);
        moveAction.setEnabled(false);
    }

    private Handler getHandler() {
        if (handler == null) {
            handler = new Handler();
        }
        return handler;
    }

    protected PropertyChangeListener createPropertyChangeListener() {
        return getHandler();
    }

    protected LayoutManager createLayout() {
        return getHandler();
    }


    private class Handler implements LayoutManager, PropertyChangeListener {
        //
        // PropertyChangeListener
        //
        public void propertyChange(PropertyChangeEvent evt) {
            String prop = (String) evt.getPropertyName();

            if (prop == JInternalFrame.IS_SELECTED_PROPERTY) {
                repaint();
                return;
            }

            if (prop == JInternalFrame.IS_ICON_PROPERTY ||
                    prop == JInternalFrame.IS_MAXIMUM_PROPERTY) {
                setButtonIcons();
                enableActions();
                return;
            }

            if ("closable" == prop) {
                if ((Boolean) evt.getNewValue() == Boolean.TRUE) {
                    add(closeButton);
                } else {
                    remove(closeButton);
                }
            } else if ("maximizable" == prop) {
                if ((Boolean) evt.getNewValue() == Boolean.TRUE) {
                    add(maxButton);
                } else {
                    remove(maxButton);
                }
            } else if ("iconable" == prop) {
                if ((Boolean) evt.getNewValue() == Boolean.TRUE) {
                    add(iconButton);
                } else {
                    remove(iconButton);
                }
            }
            enableActions();

            revalidate();
            repaint();
        }


        //
        // LayoutManager
        //
        public void addLayoutComponent(String name, Component c) {
        }

        public void removeLayoutComponent(Component c) {
        }

        public Dimension preferredLayoutSize(Container c) {
            return minimumLayoutSize(c);
        }

        public Dimension minimumLayoutSize(Container c) {
            // Calculate width.
            int width = 22;

            if (frame.isClosable()) {
                width += 19;
            }
            if (frame.isMaximizable()) {
                width += 19;
            }
            if (frame.isIconifiable()) {
                width += 19;
            }

            FontMetrics fm = frame.getFontMetrics(getFont());
            String frameTitle = frame.getTitle();
            int title_w = frameTitle != null ? SwingUtilities2.stringWidth(frame, fm, frameTitle) : 0;
            int title_length = frameTitle != null ? frameTitle.length() : 0;

            // Leave room for three characters in the title.
            if (title_length > 3) {
                int subtitle_w = SwingUtilities2.stringWidth(frame, fm, frameTitle.substring(0, 3) + "...");
                width += (title_w < subtitle_w) ? title_w : subtitle_w;
            } else {
                width += title_w;
            }

            // Calculate height.
            Icon icon = frame.getFrameIcon();
            int fontHeight = fm.getHeight();
            fontHeight += 2;
            int iconHeight = 0;
            if (icon != null) {
                // SystemMenuBar forces the icon to be 16x16 or less.
                iconHeight = Math.min(icon.getIconHeight(), 16);
            }
            iconHeight += 2;

            int height = Math.max(fontHeight, iconHeight);

            Dimension dim = new Dimension(width, height);

            // Take into account the border insets if any.
            if (getBorder() != null) {
                Insets insets = getBorder().getBorderInsets(c);
                dim.height += insets.top + insets.bottom;
                dim.width += insets.left + insets.right;
            }
            return dim;
        }

        public void layoutContainer(Container c) {
            boolean leftToRight = SharedMethods.isLeftToRight(frame);

            int w = getWidth();
            int h = getHeight();
            int x;

            int buttonHeight = closeButton.getIcon().getIconHeight();

            Icon icon = frame.getFrameIcon();
            int iconHeight = 0;
            if (icon != null) {
                iconHeight = icon.getIconHeight();
            }
            x = (leftToRight) ? 2 : w - 16 - 2;
            menuBar.setBounds(x, (h - iconHeight) / 2, 16, 16);

            x = (leftToRight) ? w - 16 - 2 : 2;

            if (frame.isClosable()) {
                closeButton.setBounds(x, (h - buttonHeight) / 2, 16, 14);
                x += (leftToRight) ? -(16 + 2) : 16 + 2;
            }

            if (frame.isMaximizable()) {
                maxButton.setBounds(x, (h - buttonHeight) / 2, 16, 14);
                x += (leftToRight) ? -(16 + 2) : 16 + 2;
            }

            if (frame.isIconifiable()) {
                iconButton.setBounds(x, (h - buttonHeight) / 2, 16, 14);
            }
        }
    }

    /**
     * This class should be treated as a &quot;protected&quot; inner class.
     * Instantiate it only within subclasses of <Foo>.
     */
    public class PropertyChangeHandler implements PropertyChangeListener {
        // NOTE: This class exists only for backward compatability. All
        // its functionality has been moved into Handler. If you need to add
        // new functionality add it to the Handler, but make sure this
        // class calls into the Handler.
        public void propertyChange(PropertyChangeEvent evt) {
            getHandler().propertyChange(evt);
        }
    }

    /**
     * This class should be treated as a &quot;protected&quot; inner class.
     * Instantiate it only within subclasses of <Foo>.
     */
    public class TitlePaneLayout implements LayoutManager {
        // NOTE: This class exists only for backward compatability. All
        // its functionality has been moved into Handler. If you need to add
        // new functionality add it to the Handler, but make sure this
        // class calls into the Handler.
        public void addLayoutComponent(String name, Component c) {
            getHandler().addLayoutComponent(name, c);
        }

        public void removeLayoutComponent(Component c) {
            getHandler().removeLayoutComponent(c);
        }

        public Dimension preferredLayoutSize(Container c) {
            return getHandler().preferredLayoutSize(c);
        }

        public Dimension minimumLayoutSize(Container c) {
            return getHandler().minimumLayoutSize(c);
        }

        public void layoutContainer(Container c) {
            getHandler().layoutContainer(c);
        }
    }

    /**
     * This class should be treated as a &quot;protected&quot; inner class.
     * Instantiate it only within subclasses of <Foo>.
     */
    public class CloseAction extends AbstractAction {
        public CloseAction() {
            super(CLOSE_CMD);
        }

        public void actionPerformed(ActionEvent e) {
            if (frame.isClosable()) {
                frame.doDefaultCloseAction();
            }
        }
    } // end CloseAction

    /**
     * This class should be treated as a &quot;protected&quot; inner class.
     * Instantiate it only within subclasses of <Foo>.
     */
    public class MaximizeAction extends AbstractAction {
        public MaximizeAction() {
            super(MAXIMIZE_CMD);
        }

        public void actionPerformed(ActionEvent evt) {
            if (frame.isMaximizable()) {
                if (frame.isMaximum() && frame.isIcon()) {
                    try {
                        frame.setIcon(false);
                    } catch (PropertyVetoException e) {
                    }
                } else if (!frame.isMaximum()) {
                    try {
                        frame.setMaximum(true);
                    } catch (PropertyVetoException e) {
                    }
                } else {
                    try {
                        frame.setMaximum(false);
                    } catch (PropertyVetoException e) {
                    }
                }
            }
        }
    }

    /**
     * This class should be treated as a &quot;protected&quot; inner class.
     * Instantiate it only within subclasses of <Foo>.
     */
    public class IconifyAction extends AbstractAction {
        public IconifyAction() {
            super(ICONIFY_CMD);
        }

        public void actionPerformed(ActionEvent e) {
            if (frame.isIconifiable()) {
                if (!frame.isIcon()) {
                    try {
                        frame.setIcon(true);
                    } catch (PropertyVetoException e1) {
                    }
                } else {
                    try {
                        frame.setIcon(false);
                    } catch (PropertyVetoException e1) {
                    }
                }
            }
        }
    } // end IconifyAction

    /**
     * This class should be treated as a &quot;protected&quot; inner class.
     * Instantiate it only within subclasses of <Foo>.
     */
    public class RestoreAction extends AbstractAction {
        public RestoreAction() {
            super(RESTORE_CMD);
        }

        public void actionPerformed(ActionEvent evt) {
            if (frame.isMaximizable() && frame.isMaximum() && frame.isIcon()) {
                try {
                    frame.setIcon(false);
                } catch (PropertyVetoException e) {
                }
            } else if (frame.isMaximizable() && frame.isMaximum()) {
                try {
                    frame.setMaximum(false);
                } catch (PropertyVetoException e) {
                }
            } else if (frame.isIconifiable() && frame.isIcon()) {
                try {
                    frame.setIcon(false);
                } catch (PropertyVetoException e) {
                }
            }
        }
    }

    public class ResizeColsAction extends AbstractAction {
        public ResizeColsAction() {
            super(RESIZE_CMD);
        }

        public void actionPerformed(ActionEvent evt) {
            setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
            if (frame instanceof ClientTable) {
                ((DraggableTable) (((ClientTable) frame).getTable())).adjustColumnWidthsToFit();
            } else if (frame instanceof WindowWrapper) {
                if(frame instanceof MISTTable){
                        try {
                        ((SmartTable) ((WindowWrapper) frame).getTable1()).adjustColumnWidthsToFitinMIST(40);
                        ((SmartTable) ((WindowWrapper) frame).getTable2()).adjustColumnWidthsToFitinMIST(40);
                    } catch (Exception e) {
                    }
                }else{
                    try {
                        ((SmartTable) ((WindowWrapper) frame).getTable1()).adjustColumnWidthsToFit(40);
                        ((SmartTable) ((WindowWrapper) frame).getTable2()).adjustColumnWidthsToFit(40);
                    } catch (Exception e) {
                    }
                    try {
                        if ((((InternalFrame) frame).getWindowType() == ViewSettingsManager.DETAIL_QUOTE) ||
                                (((InternalFrame) frame).getWindowType() == ViewSettingsManager.COMBINED_WINDOW_VIEW) ||
                                (((InternalFrame) frame).getWindowType() == ViewSettingsManager.DETAIL_QUOTE_CASH_FLOW) ||
                                (((InternalFrame) frame).getWindowType() == ViewSettingsManager.SNAP_QUOTE)||
                                (((InternalFrame) frame).getWindowType() == ViewSettingsManager.DETAIL_QUOTE_FOREX)) {
                            frame.pack();
                            frame.setSize(frame.getWidth() + 4, frame.getHeight() + 4);
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                /*try {
                    ((SmartTable) ((WindowWrapper) frame).getTable1()).adjustColumnWidthsToFit(40);
                    ((SmartTable) ((WindowWrapper) frame).getTable2()).adjustColumnWidthsToFit(40);
                } catch (Exception e) {
                }
                try {
                    if ((((InternalFrame) frame).getWindowType() == ViewSettingsManager.DETAIL_QUOTE) ||
                            //(((InternalFrame) frame).getWindowType() == ViewSettingsManager.CURRENCY_VIEW) ||
                            (((InternalFrame) frame).getWindowType() == ViewSettingsManager.SNAP_QUOTE)) {
                        frame.pack();
                        frame.setSize(frame.getWidth() + 4, frame.getHeight() + 4);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }*/
            }
            try {
                frame.setSelected(true);
            } catch (PropertyVetoException e) {
                e.printStackTrace();
            }
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }
    }

    public class DetachColsAction extends AbstractAction {
        public DetachColsAction() {
            super(DETACH_CMD);
        }

        public void actionPerformed(ActionEvent evt) {
            try {
                ((DetachableRootPane)frame.getRootPane()).detach();
            } catch (Exception e) {
                e.printStackTrace(); 
            }
        }
    }

    public class PrintAction extends AbstractAction {
        public PrintAction() {
            super(PRINT_CMD);
        }

        public void actionPerformed(ActionEvent evt) {
            if (!printInProgress) {
                new Thread() {
                    public void run() {
            if (frame instanceof TWTitlePaneInterface) {
                            printInProgress = true;
                ((TWTitlePaneInterface) frame).printContents();
            }
            try {
                frame.setSelected(true);
                            printInProgress = false;
            } catch (PropertyVetoException e) {
                e.printStackTrace();
            }
        }
                }.start();
            }
        }
    }

    public class ServicesAction extends AbstractAction {
        public ServicesAction() {
            super(SERVICES_CMD);
        }

        public void actionPerformed(ActionEvent evt) {
            if (frame instanceof TWTitlePaneInterface) {
                ((TWTitlePaneInterface) frame).showServices();
            }
            try {
                frame.setSelected(true);
            } catch (PropertyVetoException e) {
                e.printStackTrace();
            }
        }
    }

    public class LinkGroupAction extends AbstractAction {
        public LinkGroupAction() {
            super(LINKGRP_CMD);
        }

        public void actionPerformed(ActionEvent evt) {
            if (frame instanceof TWTitlePaneInterface) {
                ((TWTitlePaneInterface) frame).showLinkGroup();
            }
            try {
                frame.setSelected(true);
            } catch (PropertyVetoException e) {
                e.printStackTrace();
            }
        }
    }

    /**
     * This class should be treated as a &quot;protected&quot; inner class.
     * Instantiate it only within subclasses of <Foo>.
     */
    public class MoveAction extends AbstractAction {
        public MoveAction() {
            super(MOVE_CMD);
        }

        public void actionPerformed(ActionEvent e) {
            // This action is currently undefined
        }
    } // end MoveAction

    /*
     * Handles showing and hiding the system menu.
     */
    private class ShowSystemMenuAction extends AbstractAction {
        private boolean show;    // whether to show the menu

        public ShowSystemMenuAction(boolean show) {
            this.show = show;
        }

        public void actionPerformed(ActionEvent e) {
            if (show) {
                windowMenu.doClick();
            } else {
                windowMenu.setVisible(false);
            }
        }
    }

    /**
     * This class should be treated as a &quot;protected&quot; inner class.
     * Instantiate it only within subclasses of <Foo>.
     */
    public class SizeAction extends AbstractAction {
        public SizeAction() {
            super(SIZE_CMD);
        }

        public void actionPerformed(ActionEvent e) {
            // This action is currently undefined
        }
    } // end SizeAction


    /**
     * This class should be treated as a &quot;protected&quot; inner class.
     * Instantiate it only within subclasses of <Foo>.
     */
    public class SystemMenuBar extends JMenuBar {
        public boolean isFocusTraversable() {
            return false;
        }

        public void requestFocus() {
        }

        public void paint(Graphics g) {
            Icon icon = frame.getFrameIcon();
            if (icon == null) {
                icon = (Icon) DefaultLookup.get(frame, frame.getUI(),
                        "InternalFrame.icon");
            }
            if (icon != null) {
                // Resize to 16x16 if necessary.
                if (icon instanceof ImageIcon && (icon.getIconWidth() > 16 || icon.getIconHeight() > 16)) {
                    Image img = ((ImageIcon) icon).getImage();
                    ((ImageIcon) icon).setImage(img.getScaledInstance(16, 16, Image.SCALE_SMOOTH));
                }
                icon.paintIcon(this, g, 0, 0);
            }
        }

        public boolean isOpaque() {
            return true;
        }
    } // end SystemMenuBar


    private class NoFocusButton extends JButton {
        private String uiKey;

        public NoFocusButton(String uiKey) {
            setFocusPainted(false);
            setMargin(new Insets(0, 0, 0, 0));
            setOpaque(true);
            this.uiKey = uiKey;
        }

        public boolean isFocusTraversable() {
            return false;
        }

        public void requestFocus() {
        }



        public AccessibleContext getAccessibleContext() {
            AccessibleContext ac = super.getAccessibleContext();
            if (uiKey != null) {
                ac.setAccessibleName(UIManager.getString(uiKey));
                uiKey = null;
            }
            return ac;
        }
    }

    private class LinkGroupButton extends NoFocusButton{

        String groupID="";

        private LinkGroupButton(String uiKey, String groupID) {
            super(uiKey);
            this.groupID = groupID;
            this.setIcon(LinkStore.getSharedInstance().getColoredIconForButton(groupID));

        }

        public String getGroupID() {
            return groupID;
        }

        public void setGroupID(String groupID) {
            this.groupID = groupID;
            Icon icn = LinkStore.getSharedInstance().getColoredIconForButton(groupID);
            if(icn!= null){
            this.setIcon(icn);
            this.updateUI();
            }
            
        }
    }

   public void setLinkGroupIDToButton(String id){
     ((LinkGroupButton) linkGroupButton).setGroupID(id);
   }

      // end NoFocusButton

}   // End Title Pane Class

