package com.isi.csvr.shared;

/**
 * @author : Uditha Nagahawatta
 * @date   : 3 / May / 2002
 * @purpose: An array which behaves as a list
 */

import java.io.Serializable;
import java.util.Arrays;
import java.util.Comparator;

public class FixedArrayList implements Serializable {
    private Object[] list = null;
    private int count = 0;    // number of items in the array
    private Comparator comparator;
    private int capacity;
    private boolean listFull = false;

    public FixedArrayList(int capacity, Comparator comparator) {
        list = new Object[capacity];
        this.comparator = comparator;
        this.capacity = capacity;
    }

    /**
     * Clear the list
     */
    public synchronized void clear() {
        count = 0;
        listFull = false;
        Arrays.fill(list, null);
    }

    /**
     * Add an element to the list
     *
     * @param element to be added
     */
    public synchronized void addElemet(Object element) {
        list[count] = element;
        Arrays.sort(list, 0, count + 1, comparator);
        if (count != (capacity - 1)) {
            count++;
        } else {
            listFull = true;
        }
    }

    /**
     * Returns the list as an array
     *
     * @return list
     */
    public synchronized Object[] toArray() {
        return list;
    }

    /**
     * Returns the number if items in the list
     *
     * @return numner of items in the list
     */
    public synchronized int size() {
        if (listFull)
            return capacity;
        else
            return count;
    }

}