package com.isi.csvr.shared;

import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Aug 26, 2008
 * Time: 11:31:58 AM
 */
public class TimePickerPanel extends JPanel implements KeyListener {

    private JTextField txtHours;
    private JTextField txtMins;

    FlexGridLayout layout = new FlexGridLayout(new String[]{"50%", "5", "50%"}, new String[]{"100%"}, 2, 1);

    public JTextField getTxtHours() {
        return txtHours;
    }

    public JTextField getTxtMins() {
        return txtMins;
    }

    public TimePickerPanel() {
        super();
        createUI();
    }

    private void createUI() {

        this.setLayout(layout);
        this.setPreferredSize(new Dimension(60, 20));

        txtHours = new JTextField();
        txtMins = new JTextField();
        txtMins.setBorder(null);

        txtHours.setHorizontalAlignment(JTextField.TRAILING);
        txtMins.setHorizontalAlignment(JTextField.TRAILING);

        this.add(txtHours);
        this.add(new JLabel(":"));
        this.add(txtMins);

        setOpaque(true);
        setBackground(txtHours.getBackground());
        setBorder(txtHours.getBorder());
        txtHours.setBorder(null);

        txtMins.addKeyListener(this);
        txtHours.addKeyListener(this);
    }

    public void setHours(String val) {
        this.txtHours.setText(val);
    }

    public void setMins(String val) {
        this.txtMins.setText(val);
    }

    public void setTime(int hours, int minutes) {
        if ((hours > 0) && (hours < 26)) {
            setHours("" + hours);
        } else {
            setHours("0");
        }
        if ((minutes > 0) && (minutes < 60)) {
            setMins("" + minutes);
        } else {
            setMins("0");
        }
    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getSource() == txtHours) {
            try {
                int hours = 0;
                String text = txtHours.getText().trim();
                if (!text.equals("")) {
                    hours = Integer.parseInt(text);
                    if (!isValidHour(hours)) {
                        txtHours.setText("12");
                    }
                }
            }
            catch (Exception ex) {
                txtHours.setText("12");
            }
        } else if (e.getSource() == txtMins) {
            try {

                int min = 0;
                String text = txtMins.getText().trim();
                if (!text.equals("")) {
                    min = Integer.parseInt(text);
                    if (!isValidMin(min)) {
                        txtMins.setText("00");
                    }
                }


            }
            catch (Exception ex) {
                txtMins.setText("00");
            }
        }
    }

    public boolean isValidHour(int hour) {
        for (int i = 0; i < 24; i++) {
            if (hour == i) {
                return true;
            }
        }
        return false;
    }

    public boolean isValidMin(int min) {
        for (int i = 0; i < 60; i++) {
            if (min == i) {
                return true;
            }
        }
        return false;
    }
}
