package com.isi.csvr.shared;

import com.isi.util.FlexGridLayout;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class LabeledSeperator extends JPanel {

    private Line sep1 = new Line();
    private Line sep2 = new Line();
    private static TWGradientImage gradientImage;

    public LabeledSeperator(String label) {
        String[] widths;
        if (Language.isLTR()) {
            widths = new String[]{""+ (Constants.OUTLOOK_MENU_COLUMN_WIDTH+1), "50%","0","50%"};
        } else {
            widths = new String[]{""+ Constants.OUTLOOK_MENU_COLUMN_WIDTH, "50%","0","50%"};
        }
        String[] heights = {"20"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights);
        this.setLayout(layout);
        this.add(new Gradient());
        this.add(sep1);
        JLabel lbl = new JLabel(" " + label+ " ");
        lbl.setForeground(Theme.getColor("MENU_FGCOLOR"));
        this.add(lbl);
        this.add(sep2);
        gradientImage = new TWGradientImage(Constants.OUTLOOK_MENU_COLUMN_WIDTH, getHeight(), TWGradientImage.MENU);
        setOpaque(true);
        setBackground(Theme.getColor("MENU_BGCOLOR"));
    }

    class Line extends JLabel {
        public void paint(Graphics g) {
            super.paint(g);
            g.setColor(UIManager.getColor("controlDkShadow"));
            g.drawLine(0, getHeight() / 2, getWidth(), getHeight() / 2);
            g.setColor(UIManager.getColor("controlLtHighlight"));
            g.drawLine(0, (getHeight() / 2) + 1, getWidth(), (getHeight()/2)+1);
        }
	}

    class Gradient extends JLabel {
        public void paint(Graphics g) {
            if (Constants.OUTLOOK_MENU_SIDE > 0) { // ltr
                gradientImage.setHeight(getHeight());
//                gradientImage.setWidth(19);
                gradientImage.paintIcon(this, g, 0, 0);
            } else {
                gradientImage.setHeight(getHeight());
                gradientImage.paintIcon(this, g, 0, 0);
            }
        }

        public Dimension getPreferredSize() {
            return new Dimension(100, gradientImage.getIconHeight());
        }
    }
}