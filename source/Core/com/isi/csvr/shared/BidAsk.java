// Copyright (c) 2000 ISI
package com.isi.csvr.shared;

import com.isi.csvr.marketdepth.DepthObject;

import java.io.Serializable;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * A Class class.
 * <p/>
 *
 * @author ISI
 */
public class BidAsk implements Serializable, Cloneable {
    private char transactionType;
    //private String    exchangeCode;
    private String symbol;
    //private long      transactionTime;
    private double price;
    private long quantity;
    private int depth_sequance;
    private int splits;

    public long    orderNo      = 0;
    public int     FILL_flags   = 0;
    public int     TIF_flags    = 0;
    public long    reg_Time     = 0;
    public String  marketCode;

    //private long      sequance;
    //private byte      tradingSession;
    //private short     orderConditions;
    //private byte      tick = ESPConstants.CHARNULL;
    //private String    broker = null;
    //private boolean   oddLot = false;


    /**
     * Constructor with default market depth
     */
    public BidAsk(String symbol) {
        this.symbol = symbol;
        //tick = Settings.TICK_INVALID;
    } // end constructor

    public BidAsk() {
        //tick = Settings.TICK_INVALID;
    } // end constructor

    public static char getASK() {
        return Constants.ASK;
    }

    public static char getBID() {
        return Constants.BID;
    }

    public char getTransactionType() {
        return transactionType;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String sSymbol) {
        symbol = sSymbol;
    }

    public double getPrice() {
        return price;
    }

    public long getQuantity() {
        return quantity;
    }

    public int getDepthSequance() {
        return depth_sequance;
    }

    public void setDepthSequance(int depth_seq) {
        this.depth_sequance = depth_seq;
    }

    public void setQuantity(long qty) {
        this.quantity = qty;
    }

    public void setPrice(double price) {
        //this.price[1] = this.price[0]; 27 Mar
        this.price = price;
    }

    public String getDDEValue(int type, int column) {
        switch (type) {
            case DepthObject.TYTE_PRICE:
                switch (column) {
                    case 1:
                        return "" + getPrice();
                    case 2:
                        return "" + getQuantity();
                    case 3:
                        return "" + getSplits();
                }
            case DepthObject.TYTE_ORDER:
            case DepthObject.TYTE_SPECIAL:
                switch (column) {
                    case 0:
                        return "" + (getDepthSequance() + 1);
                    case 1:
                        return "" + getPrice();
                    case 2:
                        return "" + getQuantity();
                }

        }
        return "";
    }

    public Object clone() throws CloneNotSupportedException {
        return super.clone();
    }

    public int getSplits() {
        return splits;
    }

    public void setSplits(int splits) {
        this.splits = splits;
    }
    public int getDepth_sequance() {
       return depth_sequance;
   }

   public void setDepth_sequance(int depth_sequance) {
       this.depth_sequance = depth_sequance;
   }

   public long getOrderNo() {
       return orderNo;
   }

   public void setOrderNo(long orderNo) {
       this.orderNo = orderNo;
   }

   public String getFILL_flags() {

       return Language.getString("FILL_"+FILL_flags);
   }

   public void setFILL_flags(int FILL_flags) {
       this.FILL_flags = FILL_flags;
   }

   public String getTIF_flags() {
       return Language.getString("TIFF_"+TIF_flags);
   }

   public void setTIF_flags(int TIF_flags) {
       this.TIF_flags = TIF_flags;
   }

   public long getReg_Time() {
       return reg_Time;
       //return reg_Time;
   }

   public void setReg_Time(long reg_Time) {
       this.reg_Time = reg_Time;
   }

   public String getMarketCode() {
       return marketCode;
   }

   public void setMarketCode(String marketCode) {
       this.marketCode = marketCode;
   }

}

