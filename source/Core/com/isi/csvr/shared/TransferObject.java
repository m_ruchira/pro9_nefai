package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Oct 16, 2006
 * Time: 12:45:18 PM
 */
public interface TransferObject {
    long getFlag();

    boolean isBlank();
}
