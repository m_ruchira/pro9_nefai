/**
 * @(#)ESPConstants.java	Oracle JDeveloper 3.1 May 25 2000
 * Copyright (c) 2000 Integrated Systems International (Pvt)Ltd. All Rights Reserved.
 * CopyrightVersion 1.0_beta
 **/

package com.isi.csvr.shared;

import java.io.Serializable;

/**
 * <p>Definitions for all the Constants associated with the EnterpriseStockInformationServer.
 * This contains
 * <pre>Colour schemas</pre>
 * <pre>Server Configuration file names</pre>
 * <pre>Initialization file names</pre>
 * <pre>Predefined Constants</pre>
 *
 * @author
 * @version Oracle JDeveloper 3.1
 */
public class Constants implements Serializable, Cloneable {

    public static enum ThreadTypes {
        DEFAULT, MAIN_BOARDS, TRADING, TIME_N_SALES, SYMBOL_PANEL, INDEX_PANEL}

    public static final int DEFAULT_PRICE_PORT = 9006;
    public static final int HTTP_PRICE_PORT = 80;

    public static final int DEFAULT_DOWNLOAD_PORT = 9005;
    public static final int HTTP_DOWNLOAD_PORT =  443; //8080; 

    public static final String NO_TRANSLATION_ANNOUNCEMENT = "No Translation,";

    public static final byte MARKET_DEPTH_UPPER_LIMIT = 10;
    public static final char CHARNULL = 'Q';

    public static final byte PREOPEN = 1;
    public static final byte OPEN = 2;
    public static final byte CLOSED = 3;
    public static final byte PRECLOSED = 4;
    public static final byte CLOSING_AUCTION = 16;
    public static final byte CLOSING_AUCTION_NEW = 27;
    public static final int PRECLOSE_WITH_TRADES = 18;
    public static final int CLOSE_WITH_TRADES = 19;
    public static final int TRADING_AT_LAST = 20;
    public static final byte DEFAULT_STATUS = -1;

//    public static final String DEFAULT_SECTOR = "UNCLASSIFIED";
    public static final String DEFAULT_SECTOR = Language.getString("SECTOR_UNCLASSIFIED");

    public static final String DEFAULT_ASSETCLASS = "E";
    public static final String DEFAULT_INDEXTYPE = "S";
    public static final String DEFAULT_CURRENCY_CODE = "N/A";
    public static final String APPLICATION_EXCHANGE = "SYS";
    public static final String DEFAULT_MARKET = "NA";
    public static final int DEFAULT_PRICE_CORRECTION_FACTOR = 1;
    public static final int DEFAULT_DECIMAL_COUNT = 2;

    public final static char ASK = 'A';
    public final static char BID = 'B';

    /*public static final int ANNOUCEMENT_NOT_AVAILABLE = 0;
    public static final int ANNOUCEMENT_NOT_READ = 1;
    public static final int ANNOUCEMENT_READ = 2;

    public static final int NEWS_NOT_AVAILABLE = 0;
    public static final int NEWS_NOT_READ = 1;
    public static final int NEWS_READ = 2;*/

    public static final int ITEM_NOT_AVAILABLE = 0;
    public static final int ITEM_NOT_READ = 1;
    public static final int ITEM_READ = 2;

    public static final byte UNASSIGNED_DECIMAL_PLACES = -10;
    public static final byte ONE_DECIMAL_PLACES = 1;
    public static final byte ONE_DECIMAL_PLACES_NOZERO = -1;
    public static final byte TWO_DECIMAL_PLACES = 2;
    public static final byte TWO_DECIMAL_PLACES_NOZERO = -2;
    public static final byte THREE_DECIMAL_PLACES = 3;
    public static final byte THREE_DECIMAL_PLACES_NOZERO = -3;
    public static final byte FOUR_DECIMAL_PLACES = 4;
    public static final byte FOUR_DECIMAL_PLACES_NOZERO = -4;
    public static final byte FIVE_DECIMAL_PLACES = 5;
    public static final byte FIVE_DECIMAL_PLACES_NOZERO = -5;
    public static final byte SIX_DECIMAL_PLACES = 6;
    public static final byte SIX_DECIMAL_PLACES_NOZERO = -6;
    public static final byte SEVEN_DECIMAL_PLACES = 7;
    public static final byte SEVEN_DECIMAL_PLACES_NOZERO = -7;
    public static final byte EIGHT_DECIMAL_PLACES = 8;
    public static final byte EIGHT_DECIMAL_PLACES_NOZERO = -8;
    public static final byte NINE_DECIMAL_PLACES = 9;
    public static final byte NINE_DECIMAL_PLACES_NOZERO = -9;
    public static final byte TEN_DECIMAL_PLACES = 10;
    public static final byte TEN_DECIMAL_PLACES_NOZERO = -10;
    public static final byte NO_DECIMAL_PLACES  = 0;

    public static final byte READE_MODE_TOP_TO_BOTTOM = 0;
	public static final byte READE_MODE_BOTTOM_TO_TOP = 1;

    public static final String PATTERN_NO_DECIMAL = " ###,##0  ";
    public static final String PATTERN_ONE_DECIMAL = " ###,##0.0  ";
    public static final String PATTERN_ONE_DECIMAL_NOZERO = " ###,##0.#  ";
    public static final String PATTERN_TWO_DECIMAL = " ###,##0.00  ";
    public static final String PATTERN_TWO_DECIMAL_NOZERO = " ###,##0.##  ";
    public static final String PATTERN_THREE_DECIMAL = " ###,##0.000  ";
    public static final String PATTERN_THREE_DECIMAL_NOZERO = " ###,##0.###  ";
    public static final String PATTERN_FOUR_DECIMAL = " ###,##0.0000  ";
    public static final String PATTERN_FOUR_DECIMAL_NOZERO = " ###,##0.####  ";
    public static final String PATTERN_FIVE_DECIMAL = " ###,##0.00000  ";
    public static final String PATTERN_FIVE_DECIMAL_NOZERO = " ###,##0.#####  ";
    public static final String PATTERN_SIX_DECIMAL = " ###,##0.000000  ";
    public static final String PATTERN_SIX_DECIMAL_NOZERO = " ###,##0.######  ";
    public static final String PATTERN_SEVEN_DECIMAL = " ###,##0.0000000  ";
    public static final String PATTERN_SEVEN_DECIMAL_NOZERO = " ###,##0.#######  ";
    public static final String PATTERN_EIGHT_DECIMAL = " ###,##0.00000000  ";
    public static final String PATTERN_EIGHT_DECIMAL_NOZERO = " ###,##0.########  ";
    public static final String PATTERN_NINE_DECIMAL = " ###,##0.000000000  ";
    public static final String PATTERN_NINE_DECIMAL_NOZERO = " ###,##0.#########  ";
    public static final String PATTERN_TEN_DECIMAL = " ###,##0.0000000000  ";
    public static final String PATTERN_TEN_DECIMAL_NOZERO = " ###,##0.##########  ";

    public static final String KEY_SEPERATOR_CHARACTER = "~";
    public static final String MARKET_SEPERATOR_CHARACTER = "`";
    public static final String REQUEST_TYPE_SEPERATOR_CHARACTER = "!";
    public static final String MARKET_CENTER_SEPERATOR_CHARACTER = "!";
    public static final float DEFAULT_DOUBLE_VALUE = -9999;
    public static final byte IndGraphWindow = 36;

    public static int OUTLOOK_MENU_COLUMN_WIDTH = 30;
    public static int OUTLOOK_MENU_SIDE = 1;

    public static final int GROUP_STOCK = 0x0100;
    public static final int GROUP_INDCATOR = 0x0200;
    public static final int TYPE_NA = 0x00ff;
    public static final int TYPE_ANY = 0x00ff;

//    public static final int ITYPE_INDEX = 7;
//    public static final int TYPE_EQUITY = 0;
    public static final int TYPE_ORDER = 100;
//    public static final int TYPE_SECTOR = 0x0001;
//    public static final int ITYPE_GLOBAL_INDEX = 124;
    // Sync Locks
    public static final String HISTORY_LOCK = "HISTOEY_LOCK";

    public static final byte CONTENT_PATH_PRIMARY = 1;
    public static final byte CONTENT_PATH_SECONDARY = 2;
    public static final byte CONTENT_PATH_TERTIARY = 3;

    public static final byte PATH_PRIMARY = 1;
    public static final byte PATH_SECONDARY = 2;

    public static final long CELL_HIGHLIGHT_DEALY = 1000; // in millies

    public static final long ONE_DAY = 1000 * 60 * 60 * 24; // in millies

    public static final String FILE_EXTRACT_LOCK   = "FILE_EXTRACT_LOCK";
    public static final String FRAME_ANALYSER_LOCK = "FRAME_ANALYSER_LOCK";

    public static final int DOC_TYPE_ANNOUNCEMENT = 1;
    public static final int DOC_TYPE_COMPANY_PROFILE = 2;
    public static final int DOC_TYPE_NEWS = 3;
    public static final int DOC_TYPE_BROADCAST = 3;
    public static final int DOC_TYPE_COROPARTE_ACTIONS = 4;
    public static final int DOC_TYPE_FINANCIAL_CALENDERS = 5;

    public static final int COMPANY_TIME_AND_SALES = 100;


    public static final int OPENABLE_TRADE_FILE_SIZE = 1536 * 1024;
//    public static final int OPENABLE_TRADE_FILE_SIZE =  1024;

    public static final boolean USE_VIRTUAL_KEYBOARD = false;

    public static final int OK_PRESSED = 1;
    public static final int CANCEL_PRESSED = 2;

    public static final int TICKER_STATUS_SMALLTRADE    = 0;
    public static final int TICKER_STATUS_NOCHANGE        = 1;
    public static final int TICKER_STATUS_UP            = 2;
    public static final int TICKER_STATUS_DOWN          = 3;

    public static final int EXPANSION_STATUS_NO_CHILDREN = 0;
    public static final int EXPANSION_STATUS_EXPANDED    = 1;
    public static final int EXPANSION_STATUS_COLLAPSED      = 2;
    public static final int EXPANSION_STATUS_NA          = 3;


    public static String NULL_STRING = "";

    public static final byte COMPILER_COMPILE_SUCCESS = 0;
    public static final byte COMPILER_COMPILE_FAILED = -1;

    public static final byte MAINBOARD_TYPE = 1;
    public static final byte FULL_QUOTE_TYPE = 2;

    public static final byte VALUATION_MODE_LAST = 1;
    public static final byte VALUATION_MODE_VWAP = 2;

    public static final char BROKER_TYPE_FOREIGN = 'F';
    public static final char BROKER_TYPE_DOMESTIC = 'D';
    public static final char BROKER_TYPE_HOUSE_ACC = 'H';

    public static final char TRADE_SIDE_BUY = 'B';
    public static final char TRADE_SIDE_SELL = 'S';
    public static final char TRADE_SIDE_UNKNOWN = 'N';

    public static final byte IMPORT_TAB_WORKSPACE   = 0;
    public static final byte IMPORT_TAB_WATCHLIST   = 1;
    public static final byte IMPORT_TAB_CUS_COLUMN  = 2;
    public static final byte IMPORT_TAB_CUS_INDEX   = 3;
    public static final byte IMPORT_TAB_OTHERS      = 4;

    public static final String HEATMAP_CONSTANT     = "256|7\\u0019";
     //forex
    public static final int FOREX_MAXMIN_DIVIDER    = 10000;
    //Opra
    public static final  int    NEAR_THE_MONEY_LIMIT = 5;

    public static final  int    NEWS_ADD_ID     = 1;
    public static final  int    NEWS_REMOVE_ID  = 2;

    public static final String DEFAULT_STRING = Language.getString("NA");

    public static final byte OPRA_VIEWTYPE_AMERICAN = 0;
    public static final byte OPRA_VIEWTYPE_EUROPIAN = 1;

    public static final byte USER_PAYMENT_TYPE_TRIAL        = 0;
    public static final byte USER_PAYMENT_TYPE_DEMO         = 1;
    public static final byte USER_PAYMENT_TYPE_PRE_PAID     = 2;
    public static final byte USER_PAYMENT_TYPE_POST_PAID    = 3;
    
    public static final String SHORT_PASSWORD = Language.getString("SHORT_PASSWORD");
    public static final String BAD_PASSWORD = Language.getString("BAD_PASSWORD");
    public static final String GOOD_PASSWORD = Language.getString("GOOD_PASSWORD");
    public static final String STRONG_PASSWORD = Language.getString("STRONG_PASSWORD");

    public static final int STOCK_OPEN_ABOVE_PREV_CLOSED    =  1;
    public static final int STOCK_OPEN_BELOW_PREV_CLOSED    = -1;

    //volume watcher modes
    public static final int VOLUME_WATCHER_SEVENDAY     = 0;
    public static final int VOLUME_WATCHER_THIRTYDAY    = 1;
    public static final int VOLUME_WATCHER_NINETYDAY    = 2;

    //POPUP WINDOW WIDTHS
    public static final int TABLE_MENU_TOOLBAR_WIDTH = 218;
    public static final int COMMON_TABLE_MENU_TOOLBAR_WIDTH = 218;

    //News bulb timeout (minutes)
    public static final int NEWS_TIME_OUT_TIME = 20;


    //Constants for the Stock field
    public static final int EVENT_SYMBOL_STATE =1;
    public static final int EVENT_FINANCIAL_CALENDAR =2;
    public static final int EVENT_NEWS_INDICATOR =3;
    public static final int EVENT_INSIDER_TRADES =4;
    public static final int EVENT_RESEARCH_MATERIAL =5;
    public static final int EVENT_TYPE_SPOT_STATE = 6;

  //Instument Types
    public static String INS_EQUITY = Language.getString("INS_EQUITY");
    public static String INS_OPTION = Language.getString("INS_OPTION");
    public static String INS_MUTUALFUND = Language.getString("INS_MUTUALFUND");
    public static String INS_FOREX = Language.getString("INS_FOREX");
    public static String INS_INDEX = Language.getString("INS_INDEX");
    public static String INS_BONDS = Language.getString("INS_BONDS");
//    public static String INS_ETF = Language.getString("INS_INDEX");

    public static final short BLACK_SCHOLES = 1;
    public static final short COX_ROSS = 0;

    public static final double DEPTH_PRICE_MARKET_ORDER_VALUE = -2;
    public static final double DEPTH_PRICE_MARKET_ON_OPENING_VALUE = -3;

    public static final String HISTORY_DOWNLOADER_EOD_NAME = "EOD_DATE";
    public static int isMSWordAvailable = 0; //0 -not checked ,1 =Available ,2 =not available

    public static final String LIQUIDATION_PRICE_TYPE_LAST="LAST";
    public static final String LIQUIDATION_PRICE_TYPE_MIN="MIN";
    public static final String LIQUIDATION_PRICE_TYPE_MAX="MAX";
    public static final String LIQUIDATION_PRICE_TYPE_BID="BID";
    public static final String LIQUIDATION_PRICE_TYPE_OFFER="OFFER";

    public static final byte LIQUIDATION_PRICE_TYPE_LAST_INT=0;
    public static final byte LIQUIDATION_PRICE_TYPE_MIN_INT=1;
    public static final byte LIQUIDATION_PRICE_TYPE_MAX_INT=2;
    public static final byte LIQUIDATION_PRICE_TYPE_BID_INT=3;
    public static final byte LIQUIDATION_PRICE_TYPE_OFFER_INT=4;



    public static final int WATCHLIST_TYPE_FULL_MARKET_VIEW=0;
    public static final int WATCHLIST_TYPE_EUITY_VIEW=1;
    public static final int WATCHLIST_TYPE_INDEX_VIEW=2;
    public static final int WATCHLIST_TYPE_CASHFLOW_VIEW=3;
    public static final int WATCHLIST_TYPE_GRAPHICAL_VIEW=4;
    public static final int WATCHLIST_TYPE_MUTUAL_FUND_VIEW_=5 ;
    public static final int WATCHLIST_TYPE_BOND_VIEW_=6;
    public static final int WATCHLIST_TYPE_FOREX_VIEW_=7;

    //Used for tick calculation modes in Time and sales
    public static final int TICK_MODE_LAST_TRADE=0;
    public static final int TICK_MODE_PREVIOUS_CLOSED=1;

    //used for double click function in Market TNS
    public static final int SHOW_FULL_QUOTE=0;
    public static final int SHOW_DETAIL_QUOTE=1;
    public static final int SHOW_DEPTH_BY_PRICE=2;
    public static final int SHOW_DEPTH_BY_ORDER=3;
    public static final int SHOW_SYMBOL_TNS=4;
    public static final byte AFTER_MARKET_VALUATION_MODE_LAST_TRADE=0;
    public static final byte AFTER_MARKET_VALUATION_MODE_CLOSE=1;

    //Instrument type for Tradeble rights
    public static final int TRADABLE_RIGHTS = 66;

    public static final String CO_BRANDING_CLIENT_NEFAIE = "NEFAIE";
    public static final String CO_BRANDING_CLIENT_ALKAHIR = "KHAIR";


    public static final int WARRANT = 63;
    public static final int TRUST = 65;
    public static final int RIGHT = 66;
    public static final int INDEX = 7;
    public static final int OPTION = 10;

}
