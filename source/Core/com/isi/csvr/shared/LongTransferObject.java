package com.isi.csvr.shared;

public class LongTransferObject implements TransferObject {
    private long value;
    private long flag;
    private boolean blank;

    public LongTransferObject() {
    }

    public long getValue() {
        return value;
    }

    public long getFlag() {
        return flag;
    }

    public LongTransferObject setValue(long value) {
        this.value = value;
        return this;
    }

    public void setFlag(long flag) {
        this.flag = flag;
    }


    public boolean isBlank() {
        return blank;
    }

    public void setBlank(boolean blank) {
        this.blank = blank;
    }
}