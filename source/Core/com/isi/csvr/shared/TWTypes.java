package com.isi.csvr.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 4, 2005
 * Time: 9:11:24 AM
 */
public interface TWTypes {

    public enum TickerFilter {
        MODE_ALL,MODE_EXCHANGE,MODE_WATCHLIST,MODE_NEWS,MODE_ANNOUNCEMENT,MODE_ALLEXCHANGE,MODE_WATCHEX}

    public enum TradeSides {
        BUY, SELL}
}
