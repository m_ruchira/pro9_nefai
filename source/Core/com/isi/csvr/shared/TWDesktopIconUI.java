package com.isi.csvr.shared;

import com.sun.java.swing.plaf.windows.WindowsInternalFrameTitlePane;

import javax.swing.plaf.basic.BasicDesktopIconUI;
import javax.swing.plaf.ComponentUI;
import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Mar 3, 2009
 * Time: 4:50:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class TWDesktopIconUI extends BasicDesktopIconUI{
    private int width;

    public static ComponentUI createUI(JComponent c) {
        return new TWDesktopIconUI();
    }

    public void installDefaults() {
        super.installDefaults();
        width = UIManager.getInt("DesktopIcon.width");
    }

    public void installUI(JComponent c)   {
	    super.installUI(c);
        c.setOpaque(false);
    }

    // Uninstall the listeners added by the WindowsInternalFrameTitlePane
    public void uninstallUI(JComponent c) {
//        WindowsInternalFrameTitlePane thePane = (WindowsInternalFrameTitlePane)iconPane;
        TWInternalFrameTitlePane thePane = (TWInternalFrameTitlePane)iconPane;
        super.uninstallUI(c);
        thePane.uninstallListeners();
    }

    protected void installComponents() {
//        iconPane = new WindowsInternalFrameTitlePane(frame);
        iconPane = new TWInternalFrameTitlePane(frame, true);
        desktopIcon.setLayout(new BorderLayout());
        desktopIcon.add(iconPane, BorderLayout.CENTER);

	/*if (XPStyle.getXP() != null) {
	    desktopIcon.setBorder(null);
	}*/
    }

    public Dimension getPreferredSize(JComponent c) {
        // Windows desktop icons can not be resized.  Therefore, we should
        // always return the minimum size of the desktop icon. See
        // getMinimumSize(JComponent c).
        return getMinimumSize(c);
    }

    /**
     * Windows desktop icons are restricted to a width of 160 pixels by
     * default.  This value is retrieved by the DesktopIcon.width property.
     */
    public Dimension getMinimumSize(JComponent c) {
        Dimension dim = super.getMinimumSize(c);
        dim.width = width;
        dim.height = iconPane.getPreferredSize().height;
        return dim;
    }
}
