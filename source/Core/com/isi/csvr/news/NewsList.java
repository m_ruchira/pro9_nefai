package com.isi.csvr.news;

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Settings;

import java.io.Serializable;
import java.util.Hashtable;
import java.util.LinkedList;
import java.util.Collections;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 12, 2005
 * Time: 10:13:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class NewsList implements Serializable {
//    private Hashtable companyNewss = null;
    private LinkedList<News> marketNewss = null;
    private DynamicArray filteredMarketNewss = null;
    private NewsComparator sortComparator;
    private NewsComparator searchComparator;
    private String filter = null;
    public static final byte FILTER_TYPE_PROVIDER = 1;
    public static final byte FILTER_TYPE_EXCHANGE = 2;
    private byte filterType = FILTER_TYPE_EXCHANGE;

    public NewsList() {
//        companyNewss = new Hashtable();
        marketNewss = new LinkedList<News>();//Settings.BTW_ANNOUNCEMENT_DB_SIZE);//,
        filteredMarketNewss = new DynamicArray(Settings.BTW_NEWS_DB_SIZE);//,
        sortComparator = new NewsComparator(NewsComparator.TYPE_SORT);
        searchComparator = new NewsComparator(NewsComparator.TYPE_SEARCH);

    }

    public void clear() {
//        companyNewss.clear();
        marketNewss.clear();
        filteredMarketNewss.clear();
    }

    public synchronized void setNews(String symbol, News news) {
        try {
            News oldNews = null;
            int location = Collections.binarySearch(marketNewss, news, searchComparator);
            if (location >= 0) {
                oldNews = marketNewss.get(location);
            }
            if(oldNews != null){
                oldNews.setData(news.getData());
            }
        } catch(Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public synchronized boolean addNews(String symbol, News news) {
        boolean newsAdded = false;

        int location = Collections.binarySearch(marketNewss, news, searchComparator);
        if (location < 0) { // new news item
            location = Collections.binarySearch(marketNewss, news, sortComparator);
        if (location < 0) {
            marketNewss.add(-(location + 1), news);
            }
        } else {
            marketNewss.set(location, news);
        }

        if (marketNewss.size() > Settings.BTW_NEWS_DB_SIZE) {
            marketNewss.remove(marketNewss.size() - 1);
        }

        filteredMarketNewss.insert(news, sortComparator);
        if (filteredMarketNewss.size() > Settings.BTW_NEWS_DB_SIZE) {
            filteredMarketNewss.remove(filteredMarketNewss.size() - 1);
        }
        newsAdded = true;

        return newsAdded;
    }

//    public String getFilter() {
//        return filter;
//    }

    public void setFilter(String filter) {
        this.filter = filter;
    }


    public void setFilterType(byte filterType) {
        this.filterType = filterType;
    }

    public synchronized void applyFilter() {
        filteredMarketNewss.clear();

        for (News item : marketNewss) {
            if ((filter == null) || ((filterType == FILTER_TYPE_PROVIDER) && (item.isContainInCategory(filter)))// (filter.equals(item.getNewsProvider())))
                    || ((filterType == FILTER_TYPE_EXCHANGE) && (filter.equals(item.getExchange())))) {
                filteredMarketNewss.insert(item, sortComparator);
            }
        }
    }

    /*public DynamicArray getSymbolNewss(String symbol) {
        return (DynamicArray) companyNewss.get(symbol);
    }*/

    public LinkedList<News> getLastNewss() {
        return marketNewss;
    }

    public DynamicArray getFilteredNewss() {
        return filteredMarketNewss;
    }

    private boolean isAlreadyIn(String id, Object[] list, int size) {
        News item;
        for (int i = 0; i < size; i++) {
            item = (News) list[i];
            if (item.getNewsID().equals(id))
                return true;
        }
        item = null;
        return false;
    }
}
