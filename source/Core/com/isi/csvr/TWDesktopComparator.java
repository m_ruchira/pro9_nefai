package com.isi.csvr;

import com.isi.csvr.iframe.TWDesktopInterface;

import javax.swing.*;
import java.util.Comparator;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class TWDesktopComparator implements Comparator {

    public static final int CLOSE_MODE = 0;
    public static final int FOCUS_MODE = 1;

    public static final int FORWARD_DIRECTION = 0;
    public static final int REVERSE_DIRECTION = 1;

    private JDesktopPane desktop;
    private int mode = CLOSE_MODE;
    private int direction = FORWARD_DIRECTION;

    public TWDesktopComparator(JDesktopPane desktop) {
        this.desktop = desktop;
    }

    public void setMode(int mode) {
        this.mode = mode;
    }

    public void setDirection(int direction) {
        this.direction = direction;
    }

    public int compare(Object o1, Object o2) {
        TWDesktopInterface t1 = (TWDesktopInterface) o1;
        TWDesktopInterface t2 = (TWDesktopInterface) o2;

        if (mode == CLOSE_MODE) {
            // if (t1.getWindowType() == Meta.WT_Ticker) return -1; // give the ticker lowest priority
            // so that it will be the last to close
            if (desktop.getIndexOf((JInternalFrame) t1) > desktop.getIndexOf((JInternalFrame) t2))
                return 1;
            else if (desktop.getIndexOf((JInternalFrame) t1) < desktop.getIndexOf((JInternalFrame) t2))
                return -1;
            else
                return 0;
        } else {
            if (direction == FORWARD_DIRECTION){
            if (t1.getDesktopIndex() > t2.getDesktopIndex())
                return 1;
            else if (t1.getDesktopIndex() < t2.getDesktopIndex())
                return -1;
            else
                return 0;
            } else {
                if (t1.getDesktopIndex() < t2.getDesktopIndex())
                    return 1;
                else if (t1.getDesktopIndex() > t2.getDesktopIndex())
                    return -1;
                else
                    return 0;
            }
        }

    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}