package com.isi.csvr.announcementnew;

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.shared.*;
import com.isi.csvr.announcement.DateCombo;
import com.isi.csvr.announcement.SearchedAnnouncementStore;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.datastore.*;
import com.isi.csvr.news.NewsProvidersStore;
import com.isi.csvr.news.NewsProvider;
import com.isi.csvr.news.NewsWindow;
import com.isi.csvr.news.SearchedNewsStore;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.*;
import java.text.SimpleDateFormat;

/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: May 4, 2009
 * Time: 11:46:34 AM
 * To change this template use File | Settings | File Templates.
 */
public class AdvanceAnnouncementWindow extends InternalFrame implements ActionListener, Themeable, FocusListener, ConnectionListener, MouseListener, KeyListener, ExchangeListener {
    public static AdvanceAnnouncementWindow self;
    private static final String NULL_EXCHANGE = "";
    private static final String ALL_EXCHANGE = "All";
    private JPanel mainPanel;
    private JPanel upperPanel;
    private JPanel lowerPanel;
    private JPanel criteriaPanel;
    private JPanel periodPanel;
    private int width = 600;
    private int height = 158;
    private TWButton searchBtn;
    private JRadioButton rbPreDef = new JRadioButton(Language.getString("PRE_DEFINED"));
    private JRadioButton rbCustom = new JRadioButton(Language.getString("CUSTOM_NEWS"));
    private ButtonGroup periodGroup = new ButtonGroup();
    private DateCombo fromDateCombo;
    private DateCombo toDateCombo;
    private TWComboModel preDefModel;
    private TWComboBox cmbPredef;
    ArrayList<TWComboItem> predefArray = new ArrayList<TWComboItem>();
    private TWLabel symbolLabel, providerLabel, keywordLabel;
    private TWTextField keywordField;
    private TWTextField symbolField;
    private TWComboBox cmbProvider;
    ArrayList<TWComboItem> providerSearchArray = new ArrayList<TWComboItem>();
    private TWComboModel providerModel;
    private TWButton btnDownArraow;
    private boolean isconnected = true;
    private String exchange = "*";
    private JLabel exchangeLabel, typeLabel, languageLabel, busyLabel;
    private TWComboBox cmbLanguage, cmboExchanges;
    private TWComboBox cmbExchange;
    private TWComboModel exchangesModel;
    private TWComboModel sectorModel;
    private TWComboModel typeModel;
    private TWComboBox typeCombo;
    private TWComboModel mktModel;
    private TWComboBox mktCombo;
    private TWComboBox sectorCombo;
    ArrayList<TWComboItem> exchangesArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> sectorArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> typeArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> mktArray = new ArrayList<TWComboItem>();
    ArrayList<TWComboItem> exchangeArray = new ArrayList<TWComboItem>();

    private String[][] periods = {
            {Language.getString("1_DAY"), "" + Calendar.DATE, "1"},
            {Language.getString("1_WEEK"), "" + Calendar.WEEK_OF_MONTH, "1"},
            {Language.getString("1_MONTH"), "" + Calendar.MONTH, "1"},
            {Language.getString("1_YEAR"), "" + Calendar.YEAR, "1"},
            {Language.getString("ALL"), "" + Calendar.YEAR, "100"}};

    public static AdvanceAnnouncementWindow getSharedInstance() {
        if (self == null) {
            self = new AdvanceAnnouncementWindow();
        }
        return self;
    }

    private AdvanceAnnouncementWindow() {
        this.setSize(width, height);
        this.setTitle(Language.getString("NEWS_AD_SEARCH") + " - " + Language.getString("ANNOUNCEMENTS"));
        this.setResizable(false);
        this.setClosable(true);
        this.setMaximizable(false);
        this.setIconifiable(true);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        Client.getInstance().getDesktop().add(this);
        mainPanel = new JPanel();
        upperPanel = new JPanel();
        lowerPanel = new JPanel();
        criteriaPanel = new JPanel();
        periodPanel = new JPanel();
        sectorModel = new TWComboModel(sectorArray);
        exchangesModel = new TWComboModel(exchangesArray);
        sectorCombo = new TWComboBox(sectorModel);
        mktModel = new TWComboModel(mktArray);
        mktCombo = new TWComboBox(mktModel);
        exchangeLabel = new JLabel(Language.getString("EXCHANGE"));
        typeLabel = new JLabel(Language.getString("TYPE"));
        languageLabel = new JLabel(Language.getString("LANGUAGE_CAPTION"));
        keywordLabel = new TWLabel(Language.getString("KEYWORD"));
        keywordField = new TWTextField();
        cmbLanguage = new TWComboBox();
        cmboExchanges = new TWComboBox(exchangesModel);
        searchBtn = new TWButton(Language.getString("SEARCH"));
        preDefModel = new TWComboModel(predefArray);
        cmbPredef = new TWComboBox(preDefModel);
        searchBtn.addActionListener(this);
        searchBtn.setActionCommand("SEARCH");
        providerModel = new TWComboModel(providerSearchArray);
        cmbProvider = new TWComboBox(providerModel);
        symbolLabel = new TWLabel(Language.getString("SYMBOL"));
        symbolLabel.setOpaque(true);
        providerLabel = new TWLabel(Language.getString("PROVIDER"));
        providerLabel.setOpaque(true);
        keywordLabel = new TWLabel(Language.getString("KEYWORD"));
        keywordLabel.setOpaque(true);
        keywordField = new TWTextField();
        periodGroup.add(rbPreDef);
        periodGroup.add(rbCustom);
        rbPreDef.addFocusListener(this);
        rbPreDef.setOpaque(true);
        rbPreDef.setSelected(true);
        rbCustom.addFocusListener(this);
        rbCustom.setOpaque(true);
        rbPreDef.addActionListener(this);
        rbCustom.addActionListener(this);
        populateExchanges();
        populateLanguageCombo();
        createUI();
        cmboExchanges.setSelectedIndex(0);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        Theme.registerComponent(this);
    }

    private void createUI() {
        this.setLayout(new BorderLayout());
        this.add(mainPanel);
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "30"}, 0, 0));
        mainPanel.add(upperPanel);
        mainPanel.add(lowerPanel);
        upperPanel.setLayout(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"100%"}, 0, 0));
        upperPanel.add(createCriteriaPanel());
        upperPanel.add(createPeriodPanel());
        lowerPanel.setLayout(new FlexGridLayout(new String[]{"100%", "100"}, new String[]{"100%"}, 3, 3));
        lowerPanel.add(new JLabel());
        lowerPanel.add(searchBtn);
        lowerPanel.registerKeyboardAction(this, "SEARCH", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
        searchBtn.addKeyListener(this);
        populateDuration();
        populateProviders();


    }

    private JPanel createCriteriaPanel() {
//        criteriaPanel.setBorder(BorderFactory.createTitledBorder(null,Language.getString("CRITERIA"),0,0,criteriaPanel.getFont().deriveFont(Font.BOLD),Theme.getColor("BORDER_COLOR")));
////        criteriaPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("CRITERIA")));
//        String[] width = {"40%", "60%"};
//        String[] height = {"33%", "33%", "34%"};
//        criteriaPanel.setLayout(new FlexGridLayout(width, height, 2, 3));
//        criteriaPanel.doLayout();
//        criteriaPanel.add(symbolLabel);
//        criteriaPanel.add(createSymbolPanel());
//        criteriaPanel.add(keywordLabel);
//        criteriaPanel.add(keywordField);
//        criteriaPanel.add(providerLabel);
//        criteriaPanel.add(cmbProvider);
//        GUISettings.applyOrientation(criteriaPanel);
//        return criteriaPanel;
        String[] width = {"40%", "60%"};
        String[] height = {"20", "20", "20"};
        criteriaPanel.setLayout(new FlexGridLayout(width, height, 5, 5));
        criteriaPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("CRITERIA")));
        criteriaPanel.doLayout();
        criteriaPanel.add(exchangeLabel);
        criteriaPanel.add(cmboExchanges);
        criteriaPanel.add(typeLabel);
        typeArray.add(new TWComboItem("*", Language.getString("LBL_ALL")));
        typeArray.add(new TWComboItem("sy", Language.getString("SYMBOL")));
        typeArray.add(new TWComboItem("se", Language.getString("SECTOR")));
        typeArray.add(new TWComboItem("mk", Language.getString("MARKET")));
        typeModel = new TWComboModel(typeArray);
        typeCombo = new TWComboBox(typeModel);
        typeCombo.setSelectedIndex(0);
        criteriaPanel.add(createAllPanal());
        typeCombo.addItemListener(new ItemListener() {
            public void itemStateChanged(ItemEvent e) {
                if (e.getStateChange() == ItemEvent.SELECTED) {
                    if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("SY")) {
                        removeItems();
                        criteriaPanel.add(createSymbolPanal());
                        setItems();
                    } else if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("SE")) {
                        removeItems();
                        criteriaPanel.add(createSectorPanal());
                        setItems();

                    } else if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("MK")) {
                        removeItems();
                        criteriaPanel.add(createMktPanal());
                        setItems();
                    } else {
                        removeItems();
                        criteriaPanel.add(createAllPanal());
                        setItems();
                    }
                }
            }
        });
//        criteriaPanel.add(keywordLabel);
//        criteriaPanel.add(keywordField);
        criteriaPanel.add(languageLabel);
        criteriaPanel.add(cmbLanguage);
        cmboExchanges.addActionListener(this);
        GUISettings.applyOrientation(criteriaPanel);
        return criteriaPanel;

    }

    private JPanel createPeriodPanel() {
        periodPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("PERIOD")));
        String[] dateWidths = {"30%", "70%"};
        String[] dateHeights = {"20"};
        fromDateCombo = new DateCombo(Client.getInstance().getFrame());
        fromDateCombo.addFocusListener(this);
        fromDateCombo.setEnable(false);
        JPanel fromPanel = new JPanel(new FlexGridLayout(dateWidths, dateHeights, 0, 2));
        fromPanel.add(new TWLabel(Language.getString("FROM")));
        fromPanel.add(fromDateCombo);
        fromPanel.doLayout();
        toDateCombo = new DateCombo(Client.getInstance().getFrame());
        toDateCombo.addFocusListener(this);
        toDateCombo.setEnable(false);
        JPanel toPanel = new JPanel(new FlexGridLayout(dateWidths, dateHeights, 0, 2));
        toPanel.add(new TWLabel(Language.getString("TO")));
        toPanel.add(toDateCombo);
        toPanel.doLayout();
        periodGroup.add(rbPreDef);
        periodGroup.add(rbCustom);
        String[] widthdPanel = {"40%", "60%"};
        String[] heightdPanel = {"20", "25", "25"};
        periodPanel.setLayout(new FlexGridLayout(widthdPanel, heightdPanel, 5, 2));
//        periodPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("PERIOD")));
        periodPanel.add(rbPreDef);
        periodPanel.add(cmbPredef);
        periodPanel.add(rbCustom);
        periodPanel.add(fromPanel);
        periodPanel.add(new TWLabel());
        periodPanel.add(toPanel);
        GUISettings.applyOrientation(periodPanel);
        return periodPanel;
    }

    public void applyTheme() {

    }

    public void focusGained(FocusEvent e) {
        if (e.getSource() == fromDateCombo) {
            rbCustom.setSelected(true);
            rbPreDef.setSelected(false);
        } else if (e.getSource() == toDateCombo) {
            rbCustom.setSelected(true);
            rbPreDef.setSelected(false);
        } else if (e.getSource() == rbPreDef) {
        } else if (e.getSource() == rbCustom) {
        }
    }

    private void populateDuration() {
        predefArray.clear();
        predefArray.add(new TWComboItem(1, Language.getString("1_DAY")));
        predefArray.add(new TWComboItem(2, Language.getString("1_WEEK")));
        predefArray.add(new TWComboItem(3, Language.getString("1_MONTH")));
        predefArray.add(new TWComboItem(4, Language.getString("1_YEAR")));
        predefArray.add(new TWComboItem("*", Language.getString("LBL_ALL")));
        try {
            cmbPredef.setSelectedIndex(1);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void populateProviders() {
        providerSearchArray.clear();
        TWComboItem allItem = new TWComboItem("*", Language.getString("ALL"));
//        TWComboItem allItem2 = new TWComboItem("*", Language.getString("ALL"));
        providerSearchArray.add(allItem);
        try {
//            NewsProvidersStore.getSharedInstance().loadCommonNewsProvidersFile();
            Hashtable<String, String> providers = NewsProvidersStore.getSharedInstance().getProviderMap();
            Enumeration<String> providerObjects = providers.keys();
            while (providerObjects.hasMoreElements()) {
                String id = providerObjects.nextElement();
                NewsProvider dnp = NewsProvidersStore.getSharedInstance().getProvider(id);
                if (dnp != null) {
                    providerSearchArray.add(new TWComboItem(providers.get(id), dnp.getDescription()));
                }
            }
        } catch (Exception e) {
        }
//        Enumeration<String> providerObjects = NewsProvidersStore.getSharedInstance().getNewsProviders();
//        while (providerObjects.hasMoreElements()) {
//            String id = providerObjects.nextElement();
//            DetailedNewsProvider dnp = NewsProvidersStore.getSharedInstance().detailedProviders.get(id);
//            if (dnp != null) {
//                providerSearchArray.add(new TWComboItem(id, dnp.getDescription()));
//            }
//        }
//        providerObjects = null;
        Collections.sort(providerSearchArray);

        if (providerSearchArray.size() > 0) {
            cmbProvider.setSelectedIndex(0);
        }
        cmbProvider.updateUI();
    }

    private JPanel createSymbolPanel() {
        String[] widths = {"100%", "20"};
        String[] heights = {"20"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights, 0, 0);

        JPanel panel = new JPanel(layout);
        symbolField = new TWTextField();
        symbolField.addFocusListener(this);
        panel.add(symbolField);
        btnDownArraow = new TWButton("...");
        btnDownArraow.addActionListener(this);
        btnDownArraow.setActionCommand("ARROW");
        symbolField.setEditable(true);
        panel.add(btnDownArraow);
        GUISettings.applyOrientation(panel);
        return panel;
    }

    public void focusLost(FocusEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(searchBtn)) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_AnnouncementSearch, false)) return;
            if (isConnected()) {
                AnnouncementSearchWindow.getSharedInstance().setBusyMode(true);
                AnnouncementSearchWindow.getSharedInstance().setSearchedDataStore();
                search();
                AnnouncementSearchWindow.getSharedInstance().setCancelActions();
                AnnouncementSearchWindow.getSharedInstance().updateSearchedTable();
                this.setVisible(false);
            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }
        } else if (e.getSource().equals(btnDownArraow)) {
            searchSymbol();
        } else if (e.getActionCommand().equals("SEARCH")) {
            if (!Client.getInstance().isValidSystemWindow(Meta.IT_AnnouncementSearch, false)) return;
            if (isConnected()) {
                AnnouncementSearchWindow.getSharedInstance().setBusyMode(true);
                AnnouncementSearchWindow.getSharedInstance().setSearchedDataStore();
                search();
                AnnouncementSearchWindow.getSharedInstance().setCancelActions();
                AnnouncementSearchWindow.getSharedInstance().updateSearchedTable();
                this.setVisible(false);
            } else {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
            }
        } else if (e.getSource().equals(rbPreDef)) {
            if(!cmbPredef.isEnabled()){
                cmbPredef.setEnabled(true);
            }
            fromDateCombo.setEnable(false);
            toDateCombo.setEnable(false);
            fromDateCombo.clearDate();
            toDateCombo.clearDate();
        } else if (e.getSource().equals(rbCustom)) {
            if(cmbPredef.isEnabled()){
                cmbPredef.setEnabled(false);
            }
            fromDateCombo.setEnable(true);
            toDateCombo.setEnable(true);
        } else if (e.getSource().equals(cmboExchanges)) {
            if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("SE")) {
                removeItems();
                criteriaPanel.add(createSectorPanal());
                setItems();

            }
        }
    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {

    }

    public void keyReleased(KeyEvent e) {

    }

    public void exchangeAdded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeRemoved(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeUpgraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeDowngraded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangesAdded(boolean offlineMode) {
        //To change body of implemented methods use File | Settings | File Templates.
        populateExchanges();
    }

    public void exchangesLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeMasterFileLoaded(String exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeTradingInformationTypesChanged() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    //---------------------------------------------


    private class TWLabel extends JLabel {
        public TWLabel(String name) {
            super(name);
            this.setOpaque(true);
        }

        private TWLabel(String text, int horizontalAlignment) {
            super(text, horizontalAlignment);
            this.setOpaque(true);
        }

        private TWLabel() {
            super();
            this.setOpaque(true);
        }
    }

    public boolean isConnected() {
        return isconnected;
    }

    public void twConnected() {
        isconnected = true;
    }

    public void twDisconnected() {
        isconnected = false;
        SearchedNewsStore.getSharedInstance().setSearchInProgress(false);
        SearchedNewsStore.getSharedInstance().initSearch();
        Client.getInstance().setNewsSearchTitle(null);
        SearchedNewsStore.getSharedInstance().enableButtons();
        Client.getInstance().getDesktop().repaint();
    }

    private void searchSymbol() {
        try {
            Symbols symbols = new Symbols();
            SymbolSearch oCompanies = SymbolSearch.getSharedInstance();
            oCompanies.setTitle(getTitle());
            oCompanies.setSingleMode(true);
            oCompanies.setIndexSearchMode(false);
            oCompanies.init();
            oCompanies.setSymbols(symbols);
            oCompanies.setShowDefaultExchangesOnly(false);
            oCompanies.showDialog(true);
            oCompanies = null;

            String key = symbols.getSymbols()[0];
            exchange = SharedMethods.getExchangeFromKey(key);
            String symbol = SharedMethods.getSymbolFromKey(key);

            symbolField.setText(symbol);
            symbols = null;
        } catch (Exception e) {
        }
    }

    public void search(long sequenceNo) {
        if ((rbPreDef.isSelected()) || (rbCustom.isSelected())) {

            String symbol = symbolField.getText();
            if ((symbol == null) || (symbol.equals(""))) {
                symbol = "*";
            } else if (symbol.contains("`")) {
                try {
                    String symbol2 = symbol.split("`")[0];
                    symbol = symbol2;
                } catch (Exception e) {
                    e.printStackTrace();
                    symbol = "*";
                }
            }

            String keyword = UnicodeUtils.getUnicodeString(keywordField.getText());
            if ((keyword == null) || (keyword.equals(""))) {
                keyword = "*";
            }

            String providerList = getProviderListForExchange(exchange);

            String fromDate = null, toDate = null;
            SimpleDateFormat dateformat = new SimpleDateFormat("yyyyMMdd");
            if (rbCustom.isSelected()) {
//                fromDate = fromDateCombo.getDateString();
//                toDate = toDateCombo.getDateString();
                ////////////////////////////////////////////////////////////////////////
//                SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyyMMdd HH:MM:SS");
                SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyyMMddHHMMss");
                Date dt1 = new Date(Settings.getLocalTimeFor(fromDateCombo.getDateLong()));
                dt1.setHours(0);
                dt1.setMinutes(0);
                dt1.setSeconds(0);
                Date dt2 = new Date(Settings.getLocalTimeFor(toDateCombo.getDateLong()));
                dt2.setHours(23);
                dt2.setMinutes(59);
                dt2.setSeconds(999);

                String userTZ = System.getProperty("user.timezone");
                TimeZone tz = TimeZone.getTimeZone(userTZ);

//                System.out.println("Time zone Details : ");
//                System.out.println("Zone Name1 : "+tz.getDisplayName());
//                System.out.println("Zone Name2 : "+tz.getID());
//                System.out.println("Zone offset : "+dateformat2.format(tz.getOffset(dt.getTime())));
//                System.out.println("TIME NEWS : "+dt);
//                System.out.println("Test offset1 : "+dateformat2.format(dt.getTime()));
//                System.out.println("Test offset2 : "+dateformat2.format(dt.getTime()-tz.getOffset(dt.getTime())));

                fromDate = dateformat2.format(dt1.getTime() - tz.getOffset(dt1.getTime()));
                toDate = dateformat2.format(dt2.getTime() - tz.getOffset(dt2.getTime()));

//                Settings.getLocalTimeFor(toDateCombo.getDateLong());
//                Calendar cal = Calendar.getInstance();
                ////////////////////////////////////////////////////////////////////////

                if ((fromDate == null) || (toDate == null)) {
                    SharedMethods.showMessage(Language.getString("MSG_FILL_DATE_ENTRY"), JOptionPane.ERROR_MESSAGE);
                    return;
                }
            } else {
                //setting pre defined time constants in request message
                int index1 = cmbPredef.getSelectedIndex();
                Calendar cal = Calendar.getInstance();
                if (index1 == 0) {
                    cal.add(Calendar.DAY_OF_MONTH, -1);
                    ///////////////////////////////////////////////////////////////////////
                    SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyyMMddHHMMss");
                    Date dt1 = new Date(Settings.getLocalTimeFor(cal.getTimeInMillis()));
                    dt1.setHours(0);
                    dt1.setMinutes(0);
                    dt1.setSeconds(0);
                    String userTZ = System.getProperty("user.timezone");
                    TimeZone tz = TimeZone.getTimeZone(userTZ);
                    fromDate = dateformat2.format(dt1.getTime() - tz.getOffset(dt1.getTime()));
                    ////////////////////////////////////////////////////
//                    fromDate = dateformat.format(cal.getTime());
                } else if (index1 == 1) {
                    cal.add(Calendar.WEEK_OF_MONTH, -1);
                    ///////////////////////////////////////
                    SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyyMMddHHMMss");
                    Date dt1 = new Date(Settings.getLocalTimeFor(cal.getTimeInMillis()));
                    dt1.setHours(0);
                    dt1.setMinutes(0);
                    dt1.setSeconds(0);
                    String userTZ = System.getProperty("user.timezone");
                    TimeZone tz = TimeZone.getTimeZone(userTZ);
                    fromDate = dateformat2.format(dt1.getTime() - tz.getOffset(dt1.getTime()));
                    ///////////////////////////////////////
//                    fromDate = dateformat.format(cal.getTime());
                } else if (index1 == 2) {
                    cal.add(Calendar.MONTH, -1);
                    ////////////////////////////////////////////
                    SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyyMMddHHMMss");
                    Date dt1 = new Date(Settings.getLocalTimeFor(cal.getTimeInMillis()));
                    dt1.setHours(0);
                    dt1.setMinutes(0);
                    dt1.setSeconds(0);
                    String userTZ = System.getProperty("user.timezone");
                    TimeZone tz = TimeZone.getTimeZone(userTZ);
                    fromDate = dateformat2.format(dt1.getTime() - tz.getOffset(dt1.getTime()));
                    ///////////////////////////////////////////
//                    fromDate = dateformat.format(cal.getTime());
                } else if (index1 == 3) {
                    cal.add(Calendar.YEAR, -1);
                    ////////////////////////////////////////////////////////
                    SimpleDateFormat dateformat2 = new SimpleDateFormat("yyyyMMddHHMMss");
                    Date dt1 = new Date(Settings.getLocalTimeFor(cal.getTimeInMillis()));
                    dt1.setHours(0);
                    dt1.setMinutes(0);
                    dt1.setSeconds(0);
                    String userTZ = System.getProperty("user.timezone");
                    TimeZone tz = TimeZone.getTimeZone(userTZ);
                    fromDate = dateformat2.format(dt1.getTime() - tz.getOffset(dt1.getTime()));
                    ///////////////////////////////////////////////////////
//                    fromDate = dateformat.format(cal.getTime());
                } else {
                    fromDate = "19700101";
                }

//                fromDate = "19700101";
                toDate = dateformat.format(new Date(System.currentTimeMillis()));
            }
            String searchString = Meta.NEWS_SEARCH_ALL + Meta.DS + exchange + Meta.FD + symbol + Meta.FD + providerList + Meta.FD + keyword + Meta.FD + fromDate + Meta.FD + toDate;
            String errString = Meta.NEWS_SEARCH_ALL + Meta.DS + Meta.FD + -1;
            System.out.println(searchString);
            SearchedNewsStore.getSharedInstance().initSearch();
            String contentIP = null;
            try {
                contentIP = NewsProvidersStore.getSharedInstance().getContentIp(providerList);
            } catch (Exception e) {
                contentIP = Settings.getActiveIP();
            }
            if (contentIP.equals("")) {
                contentIP = Settings.getActiveIP();
            }

            SearchedNewsStore.getSharedInstance().doSearch(searchString, Language.getLanguageTag(), sequenceNo, exchange, contentIP, errString); //, path);
            SearchedNewsStore.getSharedInstance().setNewsSearchInProgress(true);
            SearchedNewsStore.getSharedInstance().disableButtons();
            NewsWindow.getSharedInstance().setDefaultValues();
            NewsWindow.getSharedInstance().getNewsTab().selectTab(1);
            NewsWindow.getSharedInstance().setLatest(false);
            Theme.unRegisterComponent(this);
        } else {
            SharedMethods.showMessage(Language.getString("MSG_FILL_DATE_ENTRY"), JOptionPane.ERROR_MESSAGE);
        }

    }

    private void search() {

        String symbol;
        String exchange;
        String startDate;
        String endDate;
        String symbolType;
        String subMkt;
        String language = ((TWComboItem) cmbLanguage.getSelectedItem()).getId();


        SimpleDateFormat dateFormatFull = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat dateFormatyyyy = new SimpleDateFormat("yyyy");

        Calendar calendar = Calendar.getInstance();

        if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("SY")) {
            symbol = symbolField.getText().toUpperCase();
            symbolType = "" + Meta.TYPE_SYMBOL;
            exchange = ((TWComboItem) cmboExchanges.getSelectedItem()).getId();
        } else if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("MK")) {
//                symbol = ((TWComboItem) mktCombo.getSelectedItem()).getId();
            symbol = "*";
            symbolType = "" + Meta.TYPE_MKT;
            exchange = ((TWComboItem) cmboExchanges.getSelectedItem()).getId();
            AnnouncementSearchWindow.getSharedInstance().clearSymbolField();
        } else if (((TWComboItem) typeCombo.getSelectedItem()).getId().equalsIgnoreCase("*")) {
            symbol = "*";
            symbolType = "" + Meta.TYPE_SYMBOL;
            exchange = ((TWComboItem) cmboExchanges.getSelectedItem()).getId();
            AnnouncementSearchWindow.getSharedInstance().clearSymbolField();
        } else {
            symbol = ((TWComboItem) sectorCombo.getSelectedItem()).getId();
            symbolType = "" + Meta.TYPE_SECTOR;
            exchange = ((TWComboItem) cmboExchanges.getSelectedItem()).getId();
            AnnouncementSearchWindow.getSharedInstance().clearSymbolField();
        }

        if (rbPreDef.isSelected()) {
            int index = cmbPredef.getSelectedIndex();
            int field = Integer.parseInt(periods[index][1]);
            if (field == -1) {// ytd type
                endDate = dateFormatFull.format(new Date(System.currentTimeMillis()));
                startDate = dateFormatyyyy.format(new Date(System.currentTimeMillis())) + "0101";
            } else {
                endDate = dateFormatFull.format(new Date(System.currentTimeMillis()));
                calendar.add(Integer.parseInt(periods[index][1]), -Integer.parseInt(periods[index][2]));
                startDate = dateFormatFull.format(calendar.getTime());
            }
        } else {
            endDate = fromDateCombo.getDateString();
            startDate = toDateCombo.getDateString();
        }

        long lStart = Long.parseLong(startDate);
        long lEnd = Long.parseLong(endDate);
        endDate = "" + Math.max(lStart, lEnd);
        startDate = "" + Math.min(lStart, lEnd);

        String searchString = Meta.ANNOUNCEMENT_SEARCH + Meta.DS + symbolType + Meta.FD +
                exchange + Meta.FD + symbol + Meta.FD + startDate + Meta.FD + endDate;

        SearchedAnnouncementStore.getSharedInstance().initSearch();
        SearchedAnnouncementStore.getSharedInstance().setSearchInProgress(true);
        SearchedAnnouncementStore.getSharedInstance().doSearch(searchString, language, Integer.MAX_VALUE, exchange);

        AnnouncementSearchWindow.getSharedInstance().disableButtons();
        AnnouncementSearchWindow.getSharedInstance().setDefaultValues();
        AnnouncementSearchWindow.getSharedInstance().getAnnouncementTabPanel().selectTab(1);
        AnnouncementSearchWindow.getSharedInstance().setLatest(false);
        Theme.unRegisterComponent(this);

        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Announcements, "Search");

        symbolType = null;
        symbol = null;
        dateFormatFull = null;
        dateFormatyyyy = null;
        calendar = null;
        startDate = null;
        endDate = null;

        AnnouncementSearchWindow.getSharedInstance().updateSearchedTable();

        setVisible(true);

    }

    public String getProviderListForExchange(String exchange) {
        String providers = ((TWComboItem) cmbProvider.getSelectedItem()).getId();
        try {
            if (exchange.equals("*")) {
                if (providers.equals("*")) {
                    Enumeration<String> keys = NewsProvidersStore.getSharedInstance().getProviderKeys();
                    String returnStr = "";
                    while (keys.hasMoreElements()) {
                        returnStr += keys.nextElement() + ",";
                    }
                    return returnStr;
                } else {
                    return providers;
                }
            } else {
                if (providers.equals("*")) {
                    ArrayList<String> keys = NewsProvidersStore.getSharedInstance().getAllProvidersForExchange(exchange);
                    String returnStr = "";
                    for (String key : keys) {
                        returnStr += key + ",";
                    }
                    return returnStr;
                } else {
                    return providers;
                }
            }
        } catch (Exception e) {
            return providers;
        }
    }

    public void clearFields() {
//        keywordField.setText("");
//        symbolField.setText("");
    }

    private JPanel createAllPanal() {
        String[] width = {"40%", "60%"};
        String[] height = {"20"};
        JPanel pan = new JPanel(new FlexGridLayout(width, height, 0, 0));
        pan.setLayout(new FlexGridLayout(width, height, 0, 0));
        pan.add(typeCombo);
        pan.add(getEmptyTextField());
        return pan;
    }

    private void removeItems() {
        criteriaPanel.remove(3);
        criteriaPanel.remove(4);
//        criteriaPanel.remove(5);
    }

    private void setItems() {
//        criteriaPanel.add(keywordLabel);
//        criteriaPanel.add(keywordField);
        criteriaPanel.add(languageLabel);
        criteriaPanel.add(cmbLanguage);
        GUISettings.applyOrientation(criteriaPanel);
        SwingUtilities.updateComponentTreeUI(criteriaPanel);

    }

    private JPanel createSymbolPanal() {

        String[] width = {"40%", "60%"};
        String[] height = {"20"};
        JPanel symbolPanel = new JPanel(new FlexGridLayout(width, height, 0, 0));
        symbolPanel.setLayout(new FlexGridLayout(width, height, 0, 0));
        symbolPanel.add(typeCombo);
        symbolPanel.add(createSymbolPanel());
        return symbolPanel;
    }

    private JPanel createSectorPanal() {

        String[] width = {"40%", "60%"};
        String[] height = {"20"};
        JPanel pan = new JPanel(new FlexGridLayout(width, height, 0, 0));
        populateSectors(((TWComboItem) cmboExchanges.getSelectedItem()).getId());
        pan.setLayout(new FlexGridLayout(width, height, 0, 0));
        pan.add(typeCombo);
        if (sectorCombo.getItemCount() < 2) {
            pan.add(getEmptyTextField());
        } else {
            sectorCombo.setSelectedIndex(0);
            pan.add(sectorCombo);
        }
        return pan;
    }

    private JPanel createMktPanal() {

        String[] width = {"40%", "60%"};
        String[] height = {"20"};
        JPanel pan = new JPanel(new FlexGridLayout(width, height, 0, 0));
        populateMkt(cmboExchanges.getSelectedItem().toString());
        pan.setLayout(new FlexGridLayout(width, height, 0, 0));
        pan.add(typeCombo);
        if (mktCombo.getItemCount() < 2) {
            pan.add(getEmptyTextField());
        } else {
            mktCombo.setSelectedIndex(0);
            pan.add(mktCombo);
        }
        return pan;
    }

    private JTextField getEmptyTextField() {
        JTextField temp = new JTextField();
        temp.setEnabled(false);
        temp.setEditable(false);
        return temp;
    }

    private ArrayList populateSectors(String exchange) {
        sectorArray.clear();
        int count = 1;
        if ((exchange == null) || (exchange.equalsIgnoreCase(ALL_EXCHANGE)) || (exchange.equalsIgnoreCase(NULL_EXCHANGE))) {
            sectorArray.add(new TWComboItem(NULL_EXCHANGE, ""));
        } else {
            try {
                Enumeration<Sector> sectorlist = SectorStore.getSharedInstance().getSectors(exchange);
                while (sectorlist.hasMoreElements()) {
                    Sector sector = sectorlist.nextElement();
                    sectorArray.add(new TWComboItem(sector.getId(), sector.getDescription()));
                }
                if (sectorArray.size() == 0) { // no sectors for the exchange
                    sectorArray.add(0, new TWComboItem("*", Language.getString("LBL_ALL")));
                    return sectorArray;
                }
                Collections.sort(sectorArray);
                sectorArray.add(0, new TWComboItem("*", Language.getString("LBL_ALL")));

            } catch (Exception err) {
                sectorArray.add(new TWComboItem(NULL_EXCHANGE, ""));
            }
        }
        return sectorArray;
    }

    private ArrayList populateMkt(String exchangeName) {
        mktArray.clear();
        try {
            Enumeration sectorlist = ExchangeStore.getSharedInstance().getExchanges();
            while (sectorlist.hasMoreElements()) {
                Exchange exchange = (Exchange) sectorlist.nextElement();
                if (exchange.getDescription().equalsIgnoreCase(exchangeName)) {
                    if (exchange.isUserSubMarketBreakdown() && exchange.hasSubMarkets()) {
                        Market[] submkt = exchange.getSubMarkets();
                        for (int i = 0; i < exchange.getSubMarketCount(); i++) {
                            mktArray.add(new TWComboItem(submkt[i].getMarketID(), submkt[i].getDescription()));
                        }
                    }
                }
                exchange = null;
            }

            Collections.sort(mktArray);
            mktArray.add(0, new TWComboItem("*", Language.getString("ALL")));

        }
        catch (Exception err) {
        }

        return mktArray;
    }

    public void populateExchanges() {
        exchangesArray.clear();
        exchangeArray.clear();
        SharedMethods.populateExchangesForAnnouncementsCombo(exchangesArray, true);
        SharedMethods.populateExchangesForAnnouncementsCombo(exchangeArray, true);
//        Enumeration exchangeObjects = ExchangeStore.getSharedInstance().getExchanges();
//        TWComboItem t1;
//        TWComboItem t2;
//        while (exchangeObjects.hasMoreElements()) {
//            Exchange exchanges = (Exchange) exchangeObjects.nextElement();
//            if (!exchanges.isExpired() && !exchanges.isInactive()) {
//                t1=new TWComboItem(exchanges.getSymbol(), exchanges.getDescription());
//                t2=new TWComboItem(exchanges.getSymbol(), exchanges.getDescription());
//                exchangesArray.add(t1);
//                exchangeArray.add(t2);
//            }
//            exchanges = null;
//        }
//        exchangeObjects = null;
//        Collections.sort(exchangesArray);
//        Collections.sort(exchangeArray);
        if (exchangeArray.size() > 1) {
            exchangeArray.add(0, new TWComboItem("*", Language.getString("ALL")));
        }
//        if (exchangesArray.size() > 1) {
//            exchangesArray.add(0, new TWComboItem("*", Language.getString("ALL")));
//        }
        try {
            cmbExchange.setSelectedIndex(0);
            cmboExchanges.setSelectedIndex(0);
        } catch (Exception e) {
        }
//        GUISettings.applyOrientation(cmbExchange);
//        GUISettings.applyOrientation(cmboExchanges);
    }

    private void populateLanguageCombo() {
        String[] saLanguages;
        String[] saLanguageIDs;

        saLanguages = Language.getLanguageCaptions();
        saLanguageIDs = Language.getLanguageIDs();

        for (int i = 0; i < saLanguages.length; i++) {
            TWComboItem item = new TWComboItem(saLanguageIDs[i], saLanguages[i]);
            cmbLanguage.addItem(item);
            if (item.getId().equals(Language.getSelectedLanguage())) {
                cmbLanguage.setSelectedItem(item);
            }
        }
    }

    public void setExchange(int exgIndex) {
        try {
            cmboExchanges.setSelectedIndex(exgIndex);
            typeCombo.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void setType() {

    }
}

