package com.isi.csvr.print;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum
 * Date: Sep 26, 2005
 * Time: 1:54:57 PM
 * To change this template use File | Settings | File Templates.
 */

import com.isi.csvr.shared.Constants;

import java.io.*;
import java.awt.print.*;
import java.awt.*;
import javax.swing.*;

public class PrintHTML extends JFrame {
    private PrintableEditorPane ep;

    public PrintHTML(String s, int type, PageFormat pf, PrinterJob job) {
        ep = new PrintableEditorPane();
        ep.setBorder(null);
        ep.setContentType("text/html");
        ep.setText(s);
        if ((type == Constants.DOC_TYPE_ANNOUNCEMENT) || (type == Constants.DOC_TYPE_NEWS) || (type == Constants.DOC_TYPE_BROADCAST)) {
            ep.setPreferredSize(new Dimension((int) pf.getImageableWidth(), (int) ep.getPreferredSize().getHeight()));
        }


        this.setDefaultCloseOperation(EXIT_ON_CLOSE);
        this.setVisible(false);
        this.getContentPane().add(ep);
        this.pack();

        //PrinterJob job = PrinterJob.getPrinterJob();
        // Set up a book
        Book bk = new Book();
        bk.append(ep, pf);
// Pass the book to the PrinterJob
        job.setPageable(bk);
// Put up the dialog box
        /*if (job.printDialog()) {
// Print the job if the user didn't cancel printing
            try {
                job.print();
            }
            catch (Exception exc) {  }
        }*/

    }

/*public static void main(String[] s){
       try {
           FileInputStream fio = new FileInputStream("D:\\TW\\Regional\\Run\\Ab.html");
           int size = fio.available();
           byte[] fl = new byte[size];
           fio.read(fl, 0, size);
           PrintHTML ph = new PrintHTML(new String(fl));
       } catch (IOException e) {
           e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
       }
   }*/
}

class PrintableEditorPane extends javax.swing.JEditorPane implements java.awt.print.Printable {

    public int print(java.awt.Graphics g, java.awt.print.PageFormat pageFormat, int pageIndex) {
        Dimension size = this.getPreferredSize();
        int x = (int) pageFormat.getImageableX();
        int y = (int) pageFormat.getImageableY();
        g.translate(x, y);
        Graphics2D grad = (Graphics2D) g;
        //System.out.println("HTML width"+size.getWidth());
        //System.out.println("HTML height"+size.getHeight());
        //System.out.println("View width"+pageFormat.getImageableWidth());
        //System.out.println("View hieght"+pageFormat.getImageableHeight());
        double Width = pageFormat.getImageableWidth() / size.getWidth();
        double Height = pageFormat.getImageableHeight() / size.getHeight();
        if (Width > 1) {
            Width = 1;
        }
        if (Height > 1) {
            Height = 1;
        }
        grad.scale(Width, Height);
        paint(g);
        return PAGE_EXISTS;
    }
}

