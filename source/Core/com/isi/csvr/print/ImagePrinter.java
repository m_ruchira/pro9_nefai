package com.isi.csvr.print;

import com.isi.csvr.Client;

import javax.swing.*;
import java.awt.print.Printable;
import java.awt.print.PageFormat;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 10, 2006
 * Time: 4:12:33 PM
 */
public class ImagePrinter implements Printable {

    ImageIcon imageIcon;
    private Component parent;

    public ImagePrinter(Component parent, String path) {
        this.parent = parent;
        imageIcon = new ImageIcon(path);


    }

    public void print(){
        PrinterJob job = PrinterJob.getPrinterJob();
        job.setPrintable(this);
        if (job.printDialog()){
            try {
                job.print();
            } catch (PrinterException e) {
                e.printStackTrace();
            }
        }
    }

    public int print(Graphics g, PageFormat pf, int pageIndex) throws PrinterException {
        if (pageIndex > 0){
            return Printable.NO_SUCH_PAGE;
        }

        Graphics2D g2 = (Graphics2D) g;
        g2.translate(pf.getImageableX(), pf.getImageableY());
        double xscale = pf.getImageableWidth() / imageIcon.getImage().getWidth(parent);
        double yxscale = pf.getImageableHeight() / imageIcon.getImage().getHeight(parent);
        double scale = Math.min(xscale, yxscale);
        if (scale > 1){ // width fit the page
            scale = 1;
        }
        g2.scale(scale, scale);
        g.drawImage(imageIcon.getImage(), 0, 0, imageIcon.getImage().getWidth(parent), imageIcon.getImage().getHeight(parent), parent);


        return Printable.PAGE_EXISTS;
    }
}
