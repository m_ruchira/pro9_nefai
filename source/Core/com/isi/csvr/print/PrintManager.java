package com.isi.csvr.print;

import javax.swing.*;
import java.awt.print.PrinterJob;
import java.awt.Cursor;
import java.awt.BorderLayout;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.FlowLayout;
import java.awt.Dimension;

import com.isi.csvr.shared.Settings;

import java.awt.Toolkit;
import java.awt.event.ComponentListener;
import java.awt.event.ComponentEvent;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.print.TableProcessor;


/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author not attributable
 * @version 1.0
 */

public class PrintManager extends JFrame implements ComponentListener {

    public static final int PRINT_JTABLE = 1;
    public static final int PRINT_JPANEL = 2;
    public static final int PRINT_JEDITORPANE = 3;

    public static final int TABLE_TYPE_DEFAULT = 10;
    public static final int TABLE_TYPE_DEFAULT_NO_HEAD = 12;
    public static final int TABLE_TYPE_WATCHLIST = 11;
    public static final int TABLE_TYPE_SNAP_QUOTE = 13;
    public static final int TABLE_TYPE_DEPTH_CALC = 14;
    public static final int TABLE_TYPE_MIST_VIEW = 15;
    public static final int TABLE_TYPE_DEPTH_VIEWS = 16;

    private static TableProcessor tableProcessor;
    private static EditorPaneProcessor editorPaneProc;
    private static PrintHTML printHtml;
    private static JEditorPane editorPane;
    private static JButton btnPrint;
    private static JButton btnClose;
    private static JButton btnRefresh;
    private static JScrollPane scrollPane;

    private int reportType;
    private boolean isPreviewAvailable;
    private Object[] objects;

    private static PrintManager self = null;
    public boolean printHorizontalMode=true; //print tables in Horizontaly and verticaly when multiples tables found in a particular window
    //private HashPrintRequestAttributeSet attr;

//    public PrintManager(Object[] objects, String sTitle, int type, boolean isPreviewAvailable) {
//        this(objects, sTitle, type);
//        this.isPreviewAvailable = isPreviewAvailable;
//    }

    private PrintManager() {

//        attr = new HashPrintRequestAttributeSet();
//        attr.add(new Copies(5));
//        attr.add(MediaSizeName.ISO_A4);
//        attr.add(new JobName("TWT Printing",Locale.ENGLISH));

        super(Language.getString("PRINT_PREVIEW"));
        PrintUtilities.loadImages(this);
        this.setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));
        setResizable(false);
        this.getContentPane().setLayout(new BorderLayout());


        btnRefresh = new JButton(Language.getString("REFRESH"));
        //btnPrint.setPreferredSize(new Dimension(70, 20));
        btnRefresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //pageSetup();
            }
        });

        btnPrint = new JButton(Language.getString("PRINT"));
        //btnPrint.setPreferredSize(new Dimension(70, 20));
        btnPrint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                print();
            }
        });

        btnClose = new JButton(Language.getString("CLOSE"));
        //btnClose.setPreferredSize(new Dimension(70, 20));
        btnClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //System.exit(0);
                closeWindow();
            }
        });

        GUISettings.setSameSize(btnClose, btnPrint);

        JPanel buttonPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 5, 1));
        //buttonPanel.add(btnRefresh);
        buttonPanel.add(btnPrint);
        buttonPanel.add(btnClose);

        scrollPane = new JScrollPane();
        this.getContentPane().add(scrollPane, BorderLayout.CENTER);
        this.getContentPane().add(buttonPanel, BorderLayout.SOUTH);

        GUISettings.applyOrientation(this);
        //setTitle(Language.getString("MNU_REPORTS"));
//        showWindow();
    }

    public void setParams(Object[] objects, String sTitle, int type,boolean mode) {
        this.objects = objects;
        if (objects[0] instanceof JTable) {
            reportType = PRINT_JTABLE;
        } else if (objects[0] instanceof JPanel) {
            reportType = PRINT_JPANEL;
        } else if (objects[0] instanceof JEditorPane) {
            reportType = PRINT_JEDITORPANE;
        }

        switch (reportType) {
            case PRINT_JTABLE :
                if (tableProcessor != null) {
                    scrollPane.remove(tableProcessor);
                    tableProcessor = null;
                }
                tableProcessor = new TableProcessor(objects, sTitle, type,mode);
                scrollPane.setViewportView(tableProcessor);
                break;
            case PRINT_JEDITORPANE :
                /*if (printHtml != null) {
                   //scrollPane.remove(tableProcessor);
                    printHtml = null;
                }*/

                /*if (editorPaneProc != null) {
                    scrollPane.remove(editorPaneProc);
                    editorPaneProc = null;
                }
                editorPaneProc = new EditorPaneProcessor(objects, sTitle, type);*/
                editorPane = (JEditorPane) objects[0];

                //boolean isAnnouncement = BrowserFrame.isAnnouncement;


                break;
//            case PRINT_JPANEL :
//                mktSummary = new DailyMarketSummary();
//                scrollPane = new JScrollPane(mktSummary);
//                break;
        }
    }

    public synchronized static PrintManager getSharedInstance() {
        if (self == null) {
            self = new PrintManager();
        }
        return self;
    }

    public static int getScrollBarWidth() {
        return scrollPane.getVerticalScrollBar().getPreferredSize().width;
    }

    public static void revalidateScrollPane() {
        System.out.println("PrintManager - revalidateScrollPane()");
//        tableProcessor.setFirstTimeUse();
//        tableProcessor.revalidatePreview();
        scrollPane.getViewport().revalidate();
//        tableProcessor.initReport();
    }

    public void initializeComponents(boolean isPreviewAvailable) {
//        if (isPreviewAvailable) {
        int width = Toolkit.getDefaultToolkit().getScreenSize().width;
        int height = Toolkit.getDefaultToolkit().getScreenSize().height;
        this.setSize(width, height - 30); //810, 555);

        switch (reportType) {
            case PRINT_JTABLE :
                tableProcessor.setPreferredSize(new Dimension(width - getScrollBarWidth() - 15, 2200)); //height-30));
                break;
                /*case PRINT_JEDITORPANE :
                    if((BrowserFrame.showType==Constants.DOC_TYPE_NEWS)||(BrowserFrame.showType==Constants.DOC_TYPE_ANNOUNCEMENT)){
                        editorPaneProc.setPreferredSize(new Dimension(width-getScrollBarWidth()-15, 2200)); //height-30));
                    }
                    //editorPaneProc.setPreferredSize(new Dimension(width-getScrollBarWidth()-15, 2200)); //height-30));
                    break;*/
        }
        //this.setResizable(false);
//        }
        this.setVisible(isPreviewAvailable);
        createPreview();
        //this.repaint();
    }

    private synchronized void createPreview() {
        switch (reportType) {
            case PRINT_JTABLE :
                tableProcessor.initReport();
                tableProcessor.revalidatePreview();
                break;
                /* case PRINT_JEDITORPANE :
                if((BrowserFrame.showType==Constants.DOC_TYPE_NEWS)||(BrowserFrame.showType==Constants.DOC_TYPE_ANNOUNCEMENT)){
                    editorPaneProc.initReport();
                    editorPaneProc.revalidatePreview();
                }
                //editorPaneProc.initReport();
                //editorPaneProc.revalidatePreview();
                break;*/
        }
    }

//    private void pageSetup(){
//        //byte[] data = "uditha".getBytes();
//
//
//                //DocFlavor flvr = new DocFlavor("image/gif","[B");
//                DocFlavor flvr = new DocFlavor("application/x-java-jvm-local-objectref","java.awt.print.Printable");
//                PrintService[] serv = PrintServiceLookup.lookupPrintServices(null, null);
//                PrintService selectedService = PrintDialog.printDialog(this,this.
//                    getGraphicsConfiguration(), 0, 0, serv, serv[0], flvr, attr);
//                DocPrintJob job = selectedService.createPrintJob();
//                //job.getPrintService().
//                //MyDoc doc = new MyDoc(flvr, new HashDocAttributeSet());
//                try {
//                    job.print(tableProcessor, attr);
//                }
//                catch (PrintException ex) {
//                    //ex.printStackTrace();
//                }
//            }


    public void print() {
        createPreview();
        PrinterJob job;
        //PrinterJob job=  PrinterJob.getPrinterJob();
        switch (reportType) {
            case PRINT_JTABLE :
                tableProcessor.setTopString(tableProcessor.getTopString());
                job = PrinterJob.getPrinterJob();
                tableProcessor.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                // Will enable this later
//                setTopString("this is new string, new one,new two ,new three",2);
                job.setPrintable(tableProcessor, Settings.getPageFormat());
                if (job.printDialog()) {
                    try {
                        job.print();
                    } catch (Exception ex) {
                        System.out.println(ex);
                    }
                    this.setVisible(false);
                    closeWindow();
                }
                break;
            case PRINT_JEDITORPANE :
                DocumentRenderer dRenderer = new DocumentRenderer();
                dRenderer.print(editorPane);
                /* if((BrowserFrame.showType==Constants.DOC_TYPE_NEWS)||(BrowserFrame.showType==Constants.DOC_TYPE_ANNOUNCEMENT)){
                    editorPaneProc.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                    job.setPrintable(editorPaneProc, Settings.getPageFormat());
                }else{
                    printHtml = new PrintHTML(editorPane.getText(),BrowserFrame.showType,Settings.getPageFormat(),job);
                }*/

                //editorPaneProc.setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
                // Will enable this later
                // job.setPrintable(editorPaneProc, Settings.getPageFormat());
                break;
        }
        /*if (job.printDialog()) {
            try {
                job.print();
            } catch (Exception ex) {
                System.out.println(ex);
            }
        }

        this.setVisible(false);
        closeWindow();
        */
    }

    private void closeWindow() {
        switch (reportType) {
            case PRINT_JTABLE :
                tableProcessor.setCursor(Cursor.getDefaultCursor());
                tableProcessor.disposeReport();
                break;
                /*case PRINT_JEDITORPANE :
                 if((BrowserFrame.showType==Constants.DOC_TYPE_NEWS)||(BrowserFrame.showType==Constants.DOC_TYPE_ANNOUNCEMENT)){
                     editorPaneProc.setCursor(Cursor.getDefaultCursor());
                     editorPaneProc.disposeReport();
                 }
                //editorPaneProc.setCursor(Cursor.getDefaultCursor());
                //editorPaneProc.disposeReport();
                break;*/
        }
        this.setVisible(false);
        this.dispose();
    }

    public static JScrollPane getScrollPane() {
        return scrollPane;
    }

    public void setTopString(String[][] topString){
        tableProcessor.setTopString(topString);
    }

    private void refreshReport() {
//        System.out.println("PrintManager - refreshReport()");
        switch (reportType) {
            case PRINT_JTABLE :
                tableProcessor.initReport();
                tableProcessor.revalidatePreview();
                break;
                /*case PRINT_JEDITORPANE :
                if((BrowserFrame.showType==Constants.DOC_TYPE_NEWS)||(BrowserFrame.showType==Constants.DOC_TYPE_ANNOUNCEMENT)){
                    editorPaneProc.initReport();
                    editorPaneProc.revalidatePreview();
                }
                //editorPaneProc.initReport();
                //editorPaneProc.revalidatePreview();
                break;*/
        }
    }

    /**
     * Invoked when the component's size changes.
     */
    public void componentResized(ComponentEvent e) {
//        System.out.println("PrintManager - componentResized()");
/*        switch (reportType) {
            case PRINT_JTABLE :
                tableProcessor.initReport();
                //tableProcessor.setPreferredSize(this.getSize());
                break;
//            case PRINT_JPANEL :
//                mktSummary.initReport();
//                break;
        } */
    }

    /**
     * Invoked when the component's position changes.
     */
    public void componentMoved(ComponentEvent e) {

    }

    /**
     * Invoked when the component has been made visible.
     */
    public void componentShown(ComponentEvent e) {

    }

    /**
     * Invoked when the component has been made invisible.
     */
    public void componentHidden(ComponentEvent e) {

    }

    public boolean isPrintHorizontalMode() {
        return printHorizontalMode;
    }

    public void setPrintHorizontalMode(boolean printHorizontalMode) {
        this.printHorizontalMode = printHorizontalMode;
    }
}