package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.ChartRecord;
import com.isi.csvr.chart.MovingAverage;
import com.isi.csvr.chart.GraphDataManager;

import java.util.LinkedList;

/**
 * User: Pramoda
 * Date: Aug 29, 2006
 * Time: 4:16:10 PM
 */
public class RSSimpleMovingAverage {

    private static int simpleMovAvgTimePeriod = 21;
    private static byte source = GraphDataManager.INNER_Close;

    private static long lastVisibleTime = 0L;

    private double value;

    public void calculateMA(RadarScreenData radarScreenData) {
        LinkedList<ChartRecord> listToBeUsed;
        int interval = RadarScreenInterface.getInstance().getInterval();
        if (interval > 1) {
            listToBeUsed = radarScreenData.getDataArray().getPreparedList();
            if (radarScreenData.getDataArray().getSize() < (simpleMovAvgTimePeriod*interval)) {
                radarScreenData.getDataArray().setMaxCapacity(simpleMovAvgTimePeriod * RadarScreenInterface.getInstance().getInterval());
                radarScreenData.loadDataFromFiles();
                radarScreenData.getDataArray().prepareForInterval(RadarScreenInterface.getInstance().getInterval());
            }
        } else { //Default
            listToBeUsed = radarScreenData.getDataArray().getList();
        }
        if (listToBeUsed.size() >= simpleMovAvgTimePeriod) {
            value = MovingAverage.getMovingAverage(listToBeUsed, simpleMovAvgTimePeriod, MovingAverage.METHOD_SIMPLE,
                    source, ChartRecord.STEP_1);
//            System.out.println(">>>>> RS Simple MA calculated for " + radarScreenData.getKey());
        } else {
            value = 0.0;
        }
        radarScreenData.setLastCalculatedTime(System.currentTimeMillis());
    }

    public double getValue() {
        return value;
    }

    public static int getSimpleMovAvgTimePeriod() {
        return simpleMovAvgTimePeriod;
    }

    public static void setSimpleMovAvgTimePeriod(int simpleMovAvgTimePeriod) {
        RSSimpleMovingAverage.simpleMovAvgTimePeriod = simpleMovAvgTimePeriod;
    }

    public static byte getSource() {
        return source;
    }

    public static void setSource(byte source) {
        RSSimpleMovingAverage.source = source;
    }

    public static long getLastVisibleTime() {
        return lastVisibleTime;
    }

    public static void setLastVisibleTime(long lastVisibleTime) {
        RSSimpleMovingAverage.lastVisibleTime = lastVisibleTime;
    }
}