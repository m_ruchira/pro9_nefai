package com.isi.csvr.radarscreen;

import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.Application;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.Stock;

import java.util.TimeZone;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * User: Pramoda
 * Date: Aug 18, 2006
 * Time: 11:55:55 AM
 */
public class RadarScreenDataLoader implements ApplicationListener {

    private static RadarScreenDataLoader thisInstance;

    private RadarScreenDataLoader() {
        Application.getInstance().addApplicationListener(this);
    }

    public static RadarScreenDataLoader getInstance() {
        if (thisInstance == null) {
            thisInstance = new RadarScreenDataLoader();
        }

        return thisInstance;
    }

    private void load() throws Exception {
        Enumeration<Exchange> enumeration = ExchangeStore.getSharedInstance().getExchanges();
        while(enumeration.hasMoreElements()) {
            Exchange exchange = enumeration.nextElement();
            if (!exchange.isDefault()) {
                continue;
            }
            /*if(exchange.getSymbol().equalsIgnoreCase("case")){
                String str = exchange.getSymbol();
                System.out.println("case");
            }*/
            Hashtable<String, Stock> symbols =
                    DataStore.getSharedInstance().getExchangeSymbolStore(exchange.getSymbol());
            Enumeration<Stock> stocks = symbols.elements();
            while (stocks.hasMoreElements()) {
                try {
                    Stock stock = stocks.nextElement();
                    String key = stock.getKey();

                    RadarScreenData radarScreenData = RadarScreenInterface.getInstance().getSymbols().get(key);
                    if (radarScreenData == null) {
                        radarScreenData = new RadarScreenData(key);
                        RadarScreenInterface.getInstance().getSymbols().put(key, radarScreenData);
                        //radarScreenData.loadDataFromFiles();  todo
                        Thread.sleep(3);
                    }
                } catch (Exception e) {
                    System.out.println("------------------------------------------- RadarScreen exception in exchange="+exchange);
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
    }


    /******* Implementation of ApplicationListener *********/

    public void applicationLoading(int percentage) {
    }

    public void applicationLoaded() {
    }

    public void applicationReadyForTransactions() {

        Runnable loader = new Runnable() {
            public void run() {
                try {
                    load();
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        };

        Thread loaderThread = new Thread(loader, "RadarScreen Loader");
        loaderThread.start();
    }

    public void applicationExiting() {
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
    }

    public void workspaceWillLoad() {
    }

    public void workspaceLoaded() {
    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void snapshotProcessingStarted(Exchange exchange) {
    }

    public void snapshotProcessingEnded(Exchange exchange) {
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }
}
