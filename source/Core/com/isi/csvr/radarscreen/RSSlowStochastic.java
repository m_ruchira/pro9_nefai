package com.isi.csvr.radarscreen;

import com.isi.csvr.chart.GraphDataManager;
import com.isi.csvr.chart.ChartRecord;
import com.isi.csvr.chart.StocasticOscillator;

import java.util.LinkedList;

/**
 * User: Pramoda
 * Date: Sep 12, 2006
 * Time: 11:31:41 AM
 */
public class RSSlowStochastic {

    private static int slowStochasticTimePeriod = 5;
    private static int slowingPeriod = 3;
    private static byte source = GraphDataManager.INNER_Close;

    private static long lastVisibleTime = 0L;

    private double value;

    public void calculateSlowStochastic(RadarScreenData radarScreenData) {
        LinkedList<ChartRecord> listToBeUsed;
        int interval = RadarScreenInterface.getInstance().getInterval();
        int loopingPeriod = slowStochasticTimePeriod + slowingPeriod;
        if (interval > 1) {
            listToBeUsed = radarScreenData.getDataArray().getPreparedList();
            if (radarScreenData.getDataArray().getSize() < (loopingPeriod*interval)) {
                radarScreenData.getDataArray().setMaxCapacity(loopingPeriod * interval);
                radarScreenData.loadDataFromFiles();
                radarScreenData.getDataArray().prepareForInterval(interval);
            }
        } else { //Default
            listToBeUsed = radarScreenData.getDataArray().getList();
        }
        if (listToBeUsed.size() >= loopingPeriod) {
            value = StocasticOscillator.getSlowStochastic(listToBeUsed, slowStochasticTimePeriod, slowingPeriod);
        } else {
            value = 0.0;
        }
        radarScreenData.setLastCalculatedTime(System.currentTimeMillis());
    }

    public double getValue() {
        return value;
    }

    public static int getSlowStochasticTimePeriod() {
        return slowStochasticTimePeriod;
    }

    public static void setSlowStochasticTimePeriod(int slowStochasticTimePeriod) {
        RSSlowStochastic.slowStochasticTimePeriod = slowStochasticTimePeriod;
    }

    public static int getSlowingPeriod() {
        return slowingPeriod;
    }

    public static void setSlowingPeriod(int slowingPeriod) {
        RSSlowStochastic.slowingPeriod = slowingPeriod;
    }

    public static byte getSource() {
        return source;
    }

    public static void setSource(byte source) {
        RSSlowStochastic.source = source;
    }

    public static long getLastVisibleTime() {
        return lastVisibleTime;
    }

    public static void setLastVisibleTime(long lastVisibleTime) {
        RSSlowStochastic.lastVisibleTime = lastVisibleTime;
    }
}
