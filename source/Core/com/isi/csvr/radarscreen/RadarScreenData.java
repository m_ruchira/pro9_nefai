package com.isi.csvr.radarscreen;

import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.chart.*;

/**
 * User: Pramoda
 * Date: Aug 14, 2006
 * Time: 11:26:43 AM
 */
public class RadarScreenData {

    private RadarScreenPointsArray dataArray;
    private String key;
    private boolean loaded = false;
    private boolean fullyLoaded = false;

    private static boolean simpleMovingAvgEnabled = false;
    private RSSimpleMovingAverage rsSimpleMovingAverage;

    private static boolean expMovingAvgEnabled = false;
    private RSExpMovingAverage rsExpMovingAverage;

    private static boolean wghtMovingAvgEnabled = false;
    private RSWghtMovingAverage rsWghtMovingAverage;

    private static boolean macdEnabled = false;
    private RSMACD rsMACD;

    private static boolean imiEnabled = false;
    private RSIMI rsIMI;

    private static boolean rsiEnabled = false;
    private RSRSI rsRSI;

    private static boolean rocEnabled = false;
    private RSROC rsROC;

    private static boolean chaikinEnabled = false;
    private RSChaikinOsc rsChaikinOsc;

    private static boolean fastStochasticEnabled = false;
    private RSFastStochastic rsFastStochastic;

    private static boolean bollingerMiddleEnabled = false;
    private RSBollingerMiddle rsBollingerMiddle;

    private static boolean bollingerUpperEnabled = false;
    private RSBollingerUpper rsBollingerUpper;

    private static boolean bollingerLowerEnabled = false;
    private RSBollingerLower rsBollingerLower;

    private static boolean slowStochasticEnabled = false;
    private RSSlowStochastic rsSlowStochastic;

    private long lastCalculatedTime = 0L;
    private long lastVisibleTime = 0L;

    public RadarScreenData(String key) {
        this.key = key;
        dataArray = new RadarScreenPointsArray(RadarScreenInterface.getInstance().getDataCapacity());
    }

    public String getKey() {
        return key;
    }

    public RadarScreenPointsArray getDataArray() {
        return dataArray;
    }

    public void setDataArray(RadarScreenPointsArray dataArray) {
        this.dataArray = dataArray;
    }

     public void forceRecalculateIndicators() {

            if (rsSimpleMovingAverage != null) {
                rsSimpleMovingAverage.calculateMA(this);
            }


            if (rsExpMovingAverage != null) {
                rsExpMovingAverage.calculateMA();
            }


            if (rsWghtMovingAverage != null) {
                rsWghtMovingAverage.calculateMA(this);
            }


            if (rsMACD != null) {
                rsMACD.calculateMACD(this);
            }


            if (rsIMI != null) {
                rsIMI.calculateIMI(this);
            }


            if (rsRSI != null) {
                rsRSI.calculateRSI();
            }


            if (rsROC != null) {

                rsROC.calculateROC(this);
            }


            if (rsChaikinOsc != null) {
                rsChaikinOsc.calculateChaikinAD();
            }


            if (rsFastStochastic != null) {
                rsFastStochastic.calculateFastStochastic(this);
            }


            if (rsBollingerMiddle != null) {
                rsBollingerMiddle.calculateBollingerMiddle(this);
            }


            if (rsBollingerUpper != null) {
                rsBollingerUpper.calculateBollingerUpper(this);
            }


            if (rsBollingerLower != null) {
                rsBollingerLower.calculateBollingerLower(this);
            }


            if (rsSlowStochastic != null) {
                rsSlowStochastic.calculateSlowStochastic(this);
            }

    }

    public void loadDataFromFiles() {
        DynamicArray dynamicArray = OHLCStore.getInstance().getFullIntradayHistory(key);
        for (int i=0; i<dynamicArray.size(); i++) {
            ChartPoint chartPoint = new ChartPoint();
            IntraDayOHLC intraDayOHLC = (IntraDayOHLC)dynamicArray.get(i);
            chartPoint.Open = intraDayOHLC.getOpen();
            chartPoint.High = intraDayOHLC.getHigh();
            chartPoint.Low = intraDayOHLC.getLow();
            chartPoint.Close = intraDayOHLC.getClose();
            chartPoint.Volume = intraDayOHLC.getVolume();
            ChartRecord chartRecord = new ChartRecord(chartPoint, intraDayOHLC.getTime());
            chartRecord.setStepSize(1);
            dataArray.insert(chartRecord);
            intraDayOHLC = null;
        }
        loaded = true;
    }

    public void recalculateIndicators() {
        if (simpleMovingAvgEnabled) {
            if (rsSimpleMovingAverage == null) {
                rsSimpleMovingAverage = new RSSimpleMovingAverage();
            }
            rsSimpleMovingAverage.calculateMA(this);
        }
        if (expMovingAvgEnabled) {
            if (rsExpMovingAverage == null) {
                rsExpMovingAverage = new RSExpMovingAverage(this);
            }
            rsExpMovingAverage.calculateMA();
        }
        if (wghtMovingAvgEnabled) {
            if (rsWghtMovingAverage == null) {
                rsWghtMovingAverage = new RSWghtMovingAverage();
            }
            rsWghtMovingAverage.calculateMA(this);
        }
        if (macdEnabled) {
            if (rsMACD == null) {
                rsMACD = new RSMACD();
            }
            rsMACD.calculateMACD(this);
        }
        if (imiEnabled) {
            if (rsIMI == null) {
                rsIMI = new RSIMI();
            }
            rsIMI.calculateIMI(this);
        }
        if (rsiEnabled) {
            if (rsRSI == null) {
                rsRSI = new RSRSI(this);
            }
            rsRSI.calculateRSI();
        }
        if (rocEnabled) {
            if (rsROC == null) {
                rsROC = new RSROC();
            }
            rsROC.calculateROC(this);
        }
        if (chaikinEnabled) {
            if (rsChaikinOsc == null) {
                rsChaikinOsc = new RSChaikinOsc(this);
            }
            rsChaikinOsc.calculateChaikinAD();
        }
        if (fastStochasticEnabled) {
            if (rsFastStochastic == null) {
                rsFastStochastic = new RSFastStochastic();
            }
            rsFastStochastic.calculateFastStochastic(this);
        }
        if (bollingerMiddleEnabled) {
            if (rsBollingerMiddle == null) {
                rsBollingerMiddle = new RSBollingerMiddle();
            }
            rsBollingerMiddle.calculateBollingerMiddle(this);
        }
        if (bollingerUpperEnabled) {
            if (rsBollingerUpper == null) {
                rsBollingerUpper = new RSBollingerUpper();
            }
            rsBollingerUpper.calculateBollingerUpper(this);
        }
        if (bollingerLowerEnabled) {
            if (rsBollingerLower == null) {
                rsBollingerLower = new RSBollingerLower();
            }
            rsBollingerLower.calculateBollingerLower(this);
        }
        if (slowStochasticEnabled) {
            if (rsSlowStochastic == null) {
                rsSlowStochastic = new RSSlowStochastic();
            }
            rsSlowStochastic.calculateSlowStochastic(this);
        }
    }

    public boolean isDataArrayUpdatedAfterLastCalculation() {
        return (dataArray.getDataPointInsertedTime() >= lastCalculatedTime);
    }

    public boolean isFullyLoaded() {
        return fullyLoaded;
    }

    public void setFullyLoaded(boolean fullyLoaded) {
        this.fullyLoaded = fullyLoaded;
    }

    public void setLoaded(boolean loaded) {
        this.loaded = loaded;
    }

    public boolean isLoaded() {
        return loaded;
    }

    public RSSimpleMovingAverage getRsSimpleMovingAverage() {
        return rsSimpleMovingAverage;
    }

    public static boolean isSimpleMovingAvgEnabled() {
        return simpleMovingAvgEnabled;
    }

    public static void setSimpleMovingAvgEnabled(boolean simpleMovingAvgEnabled) {
        RadarScreenData.simpleMovingAvgEnabled = simpleMovingAvgEnabled;
    }


    public RSExpMovingAverage getRsExpMovingAverage() {
        return rsExpMovingAverage;
    }

    public static boolean isExpMovingAvgEnabled() {
        return expMovingAvgEnabled;
    }

    public static void setExpMovingAvgEnabled(boolean expMovingAvgEnabled) {
        RadarScreenData.expMovingAvgEnabled = expMovingAvgEnabled;
    }


    public RSWghtMovingAverage getRsWghtMovingAverage() {
        return rsWghtMovingAverage;
    }

    public static boolean isWghtMovingAvgEnabled() {
        return wghtMovingAvgEnabled;
    }

    public  static void setWghtMovingAvgEnabled(boolean wghtMovingAvgEnabled) {
        RadarScreenData.wghtMovingAvgEnabled = wghtMovingAvgEnabled;
    }


    public static boolean isMacdEnabled() {
        return macdEnabled;
    }

    public static void setMacdEnabled(boolean macdEnabled) {
        RadarScreenData.macdEnabled = macdEnabled;
    }

    public RSMACD getRsMACD() {
        return rsMACD;
    }


    public static boolean isImiEnabled() {
        return imiEnabled;
    }

    public static void setImiEnabled(boolean imiEnabled) {
        RadarScreenData.imiEnabled = imiEnabled;
    }

    public RSIMI getRsIMI() {
        return rsIMI;
    }


    public static boolean isRsiEnabled() {
        return rsiEnabled;
    }

    public static void setRsiEnabled(boolean rsiEnabled) {
        RadarScreenData.rsiEnabled = rsiEnabled;
    }

    public RSRSI getRsRSI() {
        return rsRSI;
    }


    public static boolean isRocEnabled() {
        return rocEnabled;
    }

    public static void setRocEnabled(boolean rocEnabled) {
        RadarScreenData.rocEnabled = rocEnabled;
    }

    public RSROC getRsROC() {
        return rsROC;
    }


    public static boolean isChaikinEnabled() {
        return chaikinEnabled;
    }

    public static void setChaikinEnabled(boolean chaikinEnabled) {
        RadarScreenData.chaikinEnabled = chaikinEnabled;
    }

    public RSChaikinOsc getRsChaikinOsc() {
        return rsChaikinOsc;
    }


    public static boolean isFastStochasticEnabled() {
        return fastStochasticEnabled;
    }

    public static void setFastStochasticEnabled(boolean fastStochasticEnabled) {
        RadarScreenData.fastStochasticEnabled = fastStochasticEnabled;
    }

    public RSFastStochastic getRsFastStochastic() {
        return rsFastStochastic;
    }

    public static boolean isBollingerMiddleEnabled() {
        return bollingerMiddleEnabled;
    }

    public static void setBollingerMiddleEnabled(boolean bollingerMiddleEnabled) {
        RadarScreenData.bollingerMiddleEnabled = bollingerMiddleEnabled;
    }

    public static boolean isBollingerUpperEnabled() {
        return bollingerUpperEnabled;
    }

    public static void setBollingerUpperEnabled(boolean bollingerUpperEnabled) {
        RadarScreenData.bollingerUpperEnabled = bollingerUpperEnabled;
    }

    public static boolean isBollingerLowerEnabled() {
        return bollingerLowerEnabled;
    }

    public static void setBollingerLowerEnabled(boolean bollingerLowerEnabled) {
        RadarScreenData.bollingerLowerEnabled = bollingerLowerEnabled;
    }

    public RSBollingerMiddle getRsBollingerMiddle() {
        return rsBollingerMiddle;
    }

    public RSBollingerUpper getRsBollingerUpper() {
        return rsBollingerUpper;
    }

    public RSBollingerLower getRsBollingerLower() {
        return rsBollingerLower;
    }

    public void setLastCalculatedTime(long lastCalculatedTime) {
        this.lastCalculatedTime = lastCalculatedTime;
    }

    public static boolean isSlowStochasticEnabled() {
        return slowStochasticEnabled;
    }

    public static void setSlowStochasticEnabled(boolean slowStochasticEnabled) {
        RadarScreenData.slowStochasticEnabled = slowStochasticEnabled;
    }

    public RSSlowStochastic getRsSlowStochastic() {
        return rsSlowStochastic;
    }

    public long getLastVisibleTime() {
        return lastVisibleTime;
    }

    public void setLastVisibleTime(long lastVisibleTime) {
        this.lastVisibleTime = lastVisibleTime;
    }
}
