package com.isi.csvr.middlewarehandler;

/**
 * Created by IntelliJ IDEA.
 * User: janithk
 * Date: Mar 9, 2010
 * Time: 12:18:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class MWMenuItem {
    private String itemId;
    private String url;
    private String brokerId;
    private String itemName;
    private String iconName;

    public String getItemId() {
        return itemId;
    }

    public void setItemId(String itemId) {
        this.itemId = itemId;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

    public String getBrokerId() {
        return brokerId;
    }

    public void setBrokerId(String brokerId) {
        this.brokerId = brokerId;
    }

    public String getItemName() {
        return itemName;
    }

    public void setItemName(String itemName) {
        this.itemName = itemName;
    }

    public String getIconName() {
        return iconName;
    }

    public void setIconName(String iconName) {
        this.iconName = iconName;
    }
}
