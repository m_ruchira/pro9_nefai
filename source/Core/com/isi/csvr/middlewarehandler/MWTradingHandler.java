package com.isi.csvr.middlewarehandler;

import com.mubasher.tradeweb.TradingPanel;
import com.mubasher.tradeweb.SymbolEventListener;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.Client;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.iframe.InternalFrame;

/**
 * Created by IntelliJ IDEA.
 * User: janithk
 * Date: Mar 3, 2010
 * Time: 4:29:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class MWTradingHandler extends InternalFrame implements Themeable {
    private static MWTradingHandler self;
    private TradingPanel tradingPanel;
    private String tradingUrl = "";
    private String brokerId = "";

    public MWTradingHandler() {
        this.setSize(990, 400);
        this.setTitle(Language.getString("MUBASHER_TRADE"));
        this.add(createTradingPanel());
        this.setClosable(true);
        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.setLayer(GUISettings.TOP_LAYER);
        Theme.registerComponent(this);
        applyTheme();
        Client.getInstance().getDesktop().add(this);
        GUISettings.setLocationRelativeTo(this, Client.getInstance().getFrame());
    }

    public static MWTradingHandler getSharedInstance() {
        if (self == null) {
            self = new MWTradingHandler();
        }
        return self;
    }

    private TradingPanel createTradingPanel() {
//        tradingPanel = new TradingPanel("http://203.143.19.41:8080/pioneers/home.p?method=login&thm=sky", "default");
        tradingPanel = new TradingPanel(tradingUrl, "default");
        tradingPanel.setLang(Language.getSelectedLanguage().toLowerCase());
        tradingPanel.setSymbolChangeListener(new SymbolListener(tradingPanel) {
        });
        return tradingPanel;
    }

    public void showWindow(short side, String selectedSymbol, String mwUrl, String brokerId) {
        Settings.setMwTradingUrl(mwUrl);
        if(!tradingUrl.equalsIgnoreCase(mwUrl)){
            this.tradingUrl = mwUrl;
            if (!brokerId.equals("")) {
                this.brokerId = brokerId;
            }
            tradingPanel.setUrl(mwUrl);
            tradingPanel.applyTheme(getThemeString());
            beginTrading(side, selectedSymbol, false);
            this.setVisible(true);
            MWMenuPropagator downloader = new MWMenuPropagator(brokerId, mwUrl);
        }else{
            this.setVisible(true);
            beginTrading(side, selectedSymbol, true);
        }
    }

    @Override
    public void applyTheme() {
        super.applyTheme();
        try {
            tradingPanel.applyTheme(getThemeString());
        } catch (Exception e) {
        }
    }

    private String getThemeString() {
        int themeId = Integer.parseInt(Theme.getID());
        String theme = "";
        switch (themeId) {
            case 1:
                theme = "default";
                break;
            case 2:
                theme = "crystal";
                break;
            case 3:
                theme = "black";
                break;
            case 4:
                theme = "gray";
                break;
            default:
                theme = "default";
                break;
        }
        return theme;
    }

    public void beginTrading(short side, String selectedSymbol, boolean isLoggedIn) {
        String strParam = "";
        String theme = "";
        String symbol = "";
        Stock selectedStock = null;
        String symbolCode = "";
        double minPrice = 0;
        double maxPrice = 0;
        double lastPrice = 0;

        String pageId = "";
        String language = "";

        try {
            theme = getThemeString();
            symbol = selectedSymbol;
            selectedStock = DataStore.getSharedInstance().getStockObject(symbol);
            language = Language.getSelectedLanguage().toLowerCase();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (selectedStock != null) {
            symbolCode = selectedStock.getISINCode();
            minPrice = selectedStock.getMinPrice();
            maxPrice = selectedStock.getMaxPrice();
            lastPrice = selectedStock.getLastTradedPrice();
        }
        if (tradingPanel != null) {
            tradingPanel.loadPage(isLoggedIn,side,symbolCode,minPrice,maxPrice,lastPrice);
            tradingPanel.updateUI();
//            tradingPanel.loadPage(isLoggedIn, side, "EGS3G231C011", 20d, 30d, 25d);
//            tradingPanel.loadPage(true, side, "EGS02011C012", 100d, 120d, 115d);
        }

    }

    class SymbolListener extends SymbolEventListener {
        SymbolListener(TradingPanel tradingPanel) {
//            super(tradingPanel);
            super();
        }
        public void symbolChanged(String exchange, String isinCode) {
//            long start = System.currentTimeMillis();
            loadPageForSymbolChange(exchange, isinCode);
//            long end = System.currentTimeMillis();
        }
    }

    private void loadPageForSymbolChange(String exchange, String isinCode) {
        Stock stock;
        short side = 0;
        String symbolCode = "";
        double minPrice = 0d;
        double maxPrice = 0d;
        double lastPrice = 0d;
        try {
            stock = DataStore.getSharedInstance().getStockObjectFromISINCode(exchange, isinCode);
            if (stock != null) {
//                symbolCode = stock.getISINCode();
                symbolCode = isinCode;
                minPrice = stock.getMinPrice();
                maxPrice = stock.getMaxPrice();
                lastPrice = stock.getLastTradedPrice();
            }
        } catch (Exception e) {
        }
        if(tradingPanel != null){
            tradingPanel.loadPage(true,side,symbolCode,minPrice,maxPrice,lastPrice);
            tradingPanel.updateUI();
        }
    }
    public void clearBrokerDetails(){
        tradingUrl = "";
        brokerId = "";
    }

    public String getBrokerId() {
        return brokerId;
    }

}