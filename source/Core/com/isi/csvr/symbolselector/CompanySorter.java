package com.isi.csvr.symbolselector;

import java.util.Comparator;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class CompanySorter implements Comparator {

    public CompanySorter() {
    }

    public int compare(Object o1, Object o2) {
        Company company1 = (Company) o1;
        Company company2 = (Company) o2;

        return company1.getDescription().compareTo(company2.getDescription());
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }
}