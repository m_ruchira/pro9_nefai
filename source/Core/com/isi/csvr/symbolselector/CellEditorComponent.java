package com.isi.csvr.symbolselector;

import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.datastore.*;
import com.isi.csvr.*;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.CellEditorListener;
import javax.swing.event.ChangeEvent;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 11, 2005
 * Time: 2:22:58 PM
 */
public class CellEditorComponent extends JPanel
        implements CellEditorListener, ActionListener, PropertyChangeListener, KeyListener, FocusListener, Themeable {

    private TWButton selector;
    private TWTextField text;
    private DefaultCellEditor editor;

    public CellEditorComponent() {
        createUI();
        Theme.registerComponent(this);
    }

    private void createUI() {
        selector = new TWButton(new DownArrow());
        selector.setFocusable(false);
        selector.setFocusPainted(false);
        selector.setBorderPainted(false);
        selector.addActionListener(this);
        selector.addPropertyChangeListener(this);

        text = new TWTextField();
        text.setBorder(BorderFactory.createEmptyBorder());
        text.addKeyListener(this);
        text.addFocusListener(this);

        String[] widths = {"100%", "20"};
        String[] heights = {"0"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights);
        setLayout(layout);
        add(text);
        add(selector);
        GUISettings.applyOrientation(this);
    }


    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
    }

    public void clear() {
        text.setText("");
        doLayout();
    }

    private void closePopup() {
        //synchronized (this) {
        if(ExchangeStore.getSharedInstance().getDefaultExchange()!= null){
            CompanyTable.getSharedInstance().hide();
        }
        //}
    }

    public void setEditor(DefaultCellEditor editor) {
        this.editor = editor;
        if(ExchangeStore.getSharedInstance().getDefaultExchange()!= null){
            CompanyTable.getSharedInstance().setEditor(editor);
        }
    }

    public void editingCanceled(ChangeEvent e) {
        closePopup();
    }

    public void editingStopped(ChangeEvent e) {
        closePopup();
    }

    public void actionPerformed(ActionEvent e) {
        if(ExchangeStore.getSharedInstance().getDefaultExchange()!= null) {
            int x = 0;
            int y = 0;
            Rectangle editorRect = text.getBounds();
            Point origin;
            if (Language.isLTR()) {
                origin = editorRect.getLocation();
            } else {
                origin = editorRect.getLocation();
                origin.x = origin.x + text.getWidth();
            }

            Toolkit oToolkit = Toolkit.getDefaultToolkit();
            Dimension screenSize = oToolkit.getScreenSize();

            SwingUtilities.convertPointToScreen(origin, this);

            x = origin.x;
            if (Language.isLTR()) {
                if ((origin.x + CompanyTable.getSharedInstance().getPanelWidth(CompanyTable.WINDOW_FIXED)) > screenSize.getWidth())
                {
                    x = (int) (x - CompanyTable.getSharedInstance().getPanelWidth(CompanyTable.WINDOW_FIXED) + editorRect.getWidth());
                }
            } else {
                x = (int) (x - CompanyTable.getSharedInstance().getPanelWidth(CompanyTable.WINDOW_FIXED));// + editorRect.getWidth());
                if (x < 0) {
                    x = (int) (origin.x - this.getWidth());
                }
            }

            y = origin.y + selector.getHeight() + 1;
            if ((origin.y + CompanyTable.getSharedInstance().getPanelHeight(CompanyTable.WINDOW_FIXED)) > screenSize.getHeight())
            {
                y = (int) (y - CompanyTable.getSharedInstance().getPanelHeight(CompanyTable.WINDOW_FIXED) - editorRect.getHeight());
            }
            text.setText("");
            CompanyTable.getSharedInstance().show(x, y, false, CompanyTable.WINDOW_FIXED);
        } else {
            text.setText("");
            Client.getInstance().mnu_AddSymbol(false);
            editor.cancelCellEditing();
//            Symbols symbols = new Symbols();
//            SymbolSearch oCompanies = SymbolSearch.getSharedInstance();
//            oCompanies.setTitle(Language.getString("WHAT_IF_CALC"));
//            oCompanies.setSingleMode(true);
//            oCompanies.init();
//            oCompanies.setSymbols(symbols);
//            oCompanies.setShowDefaultExchangesOnly(false);
//            oCompanies.showDialog(true);
        }
    }

    public void propertyChange(PropertyChangeEvent evt) {
        closePopup();
    }

    public String getText() {
        if (!text.getText().trim().equals("")){
            return text.getText().trim();
        } else if(ExchangeStore.getSharedInstance().getDefaultExchange()!= null){
            return CompanyTable.getSharedInstance().getSelection();
        } else {
            return "";
        }
    }

    public void keyPressed(KeyEvent e) {

    }

    public void keyReleased(KeyEvent e) {

    }

    public void keyTyped(KeyEvent e) {
        if (e.getKeyChar() == KeyEvent.VK_ESCAPE)
            editor.cancelCellEditing();
    }

    public void focusGained(FocusEvent e) {
        text.repaint();
        selector.repaint();
    }

    public void focusLost(FocusEvent e) {
    }
}
