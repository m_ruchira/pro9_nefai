package com.isi.csvr.symbolselector;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 12, 2005
 * Time: 11:07:25 AM
 */
public interface SymbolListener {
    public void symbolSelected(String key);

    public void actionCanceled();
}
