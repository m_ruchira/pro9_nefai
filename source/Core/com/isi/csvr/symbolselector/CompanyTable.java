package com.isi.csvr.symbolselector;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.*;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.math.BigInteger;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 12, 2005
 * Time: 9:08:31 AM
 */
public class CompanyTable extends JPanel
        implements WindowListener, KeyListener, ConnectionListener, WindowWrapper,
        Runnable, MouseListener, ExchangeListener, ActionListener, Themeable {

    public static CompanyTable self = null;
    private DefaultCellEditor editor;
    private CompanyTableModel companyTableModel;
    private TWTextField txtSearch;
    private Table companyTable;
    private boolean filterInProgress;
    private long requestTime;
    private boolean active;
    private String selection;
    private TWComboBox cmbExchange;
    private ArrayList<TWComboItem> exchangeCodes;
    private SymbolListener symbolListener;
    private JPanel buttonPanel;
    private TWButton okButton;
    private TWButton cancelButton;
    private JCheckBox selectedCheck;
    private Symbols symbols;
    private JDialog dialog;
//    private int mode;
    private int style;

    public static final int SINGLE_SELECTION = 0;
    public static final int MULTI_SELECTION = 1;

    public static final int WINDOW_FLOATING = 0;
    public static final int WINDOW_FIXED = 1;

    public static final int WINDOW_FIXED_WIDTH = 405;//542;
    public static final int WINDOW_FIXED_HEIGHT = 200;
    public static final int WINDOW_FLOATING_WIDTH =410;// 547;
    public static final int WINDOW_FLOATING_HEIGHT = 400;

    public static synchronized CompanyTable getSharedInstance() {
        if (self == null) {
            self = new CompanyTable();
        }
        return self;
    }

    public void init() {
        companyTableModel.init();
    }

    private CompanyTable() throws HeadlessException {
        ConnectionNotifier.getInstance().addConnectionListener(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        createUI();
        loadExchanges();
        Theme.registerComponent(this);
        GUISettings.applyOrientation(this);
    }

    private void createUI() {
        setLayout(new BorderLayout());
        setBorder(GUISettings.getTWFrameBorder());

        String[] topWidths = {"40", "65", "55", "100%"};
        String[] topHeights = {"20"};
        FlexGridLayout toplLayout = new FlexGridLayout(topWidths, topHeights, 5, 5);
        JPanel topPanel = new JPanel(toplLayout);
        JLabel lblSearch = new JLabel(Language.getString("SEARCH"));
        topPanel.add(lblSearch);
        txtSearch = new TWTextField();
        txtSearch.addKeyListener(this);
        topPanel.add(txtSearch);
        JLabel lblExchanges = new JLabel(Language.getString("EXCHANGE"));
        topPanel.add(lblExchanges);
        exchangeCodes = new ArrayList<TWComboItem>();
        cmbExchange = new TWComboBox(new TWComboModel(exchangeCodes));
        cmbExchange.addActionListener(this);
        topPanel.add(cmbExchange);

        createTable();
        createButtonPanel();

        add(topPanel, BorderLayout.NORTH);
        add(companyTable, BorderLayout.CENTER);
        add(buttonPanel, BorderLayout.SOUTH);
        GUISettings.applyOrientation(topPanel);
        GUISettings.applyOrientation(buttonPanel);

        companyTable.getPopup().hideCustomizer();
        companyTable.getPopup().hidePrintMenu();
        companyTable.getPopup().hideFontMenu();
        companyTable.getPopup().enableUnsort(false);
        companyTable.getPopup().hideCustomizerMenu();
    }

    private Table createTable() {
        companyTable = new Table();
        //companyTable.setAutoResize(true);
        companyTable.setPopupActive(false); // no popup menu
        companyTable.setSortingEnabled();
        companyTable.setWindowType(ViewSettingsManager.COMPANY_TABLE_VIEW);
        ViewSetting setting = ViewSettingsManager.getSummaryView("COMPANY_LIST");
        companyTableModel = new CompanyTableModel(setting);
        companyTableModel.setViewSettings(setting);
        companyTable.setModel(companyTableModel);
        companyTableModel.setTable(companyTable);
        GUISettings.setColumnSettings(setting, TWColumnSettings.getItem("COMPANY_LIST"));
        companyTableModel.applyColumnSettings();
        companyTableModel.updateGUI();
        setting.setParent(this);
        companyTable.killThread();
        companyTable.getTable().addMouseListener(this);
        companyTable.getTable().addKeyListener(this);
        return companyTable;
    }

    private void createButtonPanel() {
        String[] widths = {"100%", "80", "80"};
        String[] heights = {"20"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights);
        buttonPanel = new JPanel(layout);

        okButton = new TWButton(Language.getString("OK"));
        okButton.setActionCommand("OK");
        okButton.addActionListener(this);
        cancelButton = new TWButton(Language.getString("CANCEL"));
        cancelButton.setActionCommand("CANCEL");
        cancelButton.addActionListener(this);
//        selectedCheck = new JCheckBox();

        buttonPanel.add(new JLabel());
        buttonPanel.add(okButton);
        buttonPanel.add(cancelButton);

    }

    public Symbols getSymbols() {
        return symbols;
    }

    public void setSymbols(Symbols symbols) {
        this.symbols = symbols;

        String[] sSymbols = symbols.getSymbols();
        companyTableModel.clearSelections();
        for (int i = 0; i < sSymbols.length; i++) {
            companyTableModel.toggleSelected(sSymbols[i]);
        }
    }

    public void addSymbolListener(SymbolListener symbolListener) {
        this.symbolListener = symbolListener;
    }

    public void removeSymbolListener() {
        this.symbolListener = null;
    }

    public void show(int mode, boolean modal, int style) {
        show(mode, -1, -1, modal, style);
    }

    public void show(int x, int y, boolean modal, int style) {
        show(SINGLE_SELECTION, x, y, modal, style);
    }

    public void show(int mode, int x, int y, boolean modal, int style) {
        active = true;
        selection = null;
        this.style = style;
        //this.mode = mode;
        txtSearch.requestFocus();
        if (dialog != null) {
            dialog.hide();
        }
        dialog = new JDialog(Client.getInstance().getFrame(), Language.getString("SELECT_COMPANIES"), modal);

//        if (mode == SINGLE_SELECTION) {
        //change start
        companyTableModel.adjustColumns(companyTable.getTable(), BigInteger.valueOf(30));
//        } else {
//            companyTableModel.adjustColumns(companyTable.getTable(), 15);
//        }

        if (style == WINDOW_FLOATING) {
            dialog.setSize(WINDOW_FLOATING_WIDTH, WINDOW_FLOATING_HEIGHT);
            buttonPanel.setVisible(true);
        } else {
            dialog.setUndecorated(true);
            dialog.setSize(WINDOW_FIXED_WIDTH, WINDOW_FIXED_HEIGHT);
            buttonPanel.setVisible(false);
        }

        Thread thread = new Thread(this, "CompanyTable");
        thread.start();
        dialog.addWindowListener(this);
        dialog.setDefaultCloseOperation(JDialog.HIDE_ON_CLOSE);
        dialog.setLayout(new BorderLayout());
        dialog.add(this, BorderLayout.CENTER);
        this.doLayout();
        if ((x == -1) && (y == -1)) {
            dialog.setLocationRelativeTo(Client.getInstance().getFrame());
        } else {
            dialog.setLocation(x, y);
        }

        dialog.show();
    }

    public void hide() {
        if (dialog != null)
            dialog.hide();
    }

    public int getPanelWidth() {
        if (style == WINDOW_FLOATING) {
            return WINDOW_FLOATING_WIDTH;
        } else {
            return WINDOW_FIXED_WIDTH;
        }
    }

    public int getPanelWidth(int style) {
        if (style == WINDOW_FLOATING) {
            return WINDOW_FLOATING_WIDTH;
        } else {
            return WINDOW_FIXED_WIDTH;
        }
    }

    public int getPanelHeight() {
        if (style == WINDOW_FLOATING) {
            return WINDOW_FLOATING_HEIGHT;
        } else {
            return WINDOW_FIXED_HEIGHT;
        }
    }

    public int getPanelHeight(int style) {
        if (style == WINDOW_FLOATING) {
            return WINDOW_FLOATING_HEIGHT;
        } else {
            return WINDOW_FIXED_HEIGHT;
        }
    }

    public synchronized long getRequestTime() {
        return requestTime;
    }

    public synchronized boolean isFilterInProgress() {
        return filterInProgress;
    }

    public synchronized void setFilterInProgress(boolean filterInProgress) {
        this.filterInProgress = filterInProgress;
        if (filterInProgress) {
            requestTime = System.currentTimeMillis();
        }
    }

    public void setEditor(DefaultCellEditor editor) {
        if (editor != null) {
            this.editor = editor;
        }
    }

    public String getSelection() {
        return selection;
    }

    private String getSelectedExchange() {
        try {
            if (cmbExchange.getSelectedIndex() == 0) {
                return null;
            } else {
                return exchangeCodes.get(cmbExchange.getSelectedIndex()).getId();
            }
        } catch (Exception e) {
            return null;
        }
    }

    public void run() {
        while (active) {
            try {
                if ((isFilterInProgress()) && ((System.currentTimeMillis() - getRequestTime()) > 300)) {
                    if (txtSearch.getText().trim().equals(""))
                        companyTableModel.filter(null, getSelectedExchange());
                    else
                        companyTableModel.filter("*"+txtSearch.getText().trim()+"*", getSelectedExchange());
                    setFilterInProgress(false);
                    companyTable.resort();
                    companyTable.getTable().updateUI();
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            try {
                Thread.sleep(500);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
    }

    private void selectRow() {
        int row = companyTable.getTable().getSelectedRow();
//        String exchange = (String) companyTable.getTable().getModel().getValueAt(row, 1);
//        String symbol = (String) companyTable.getTable().getModel().getValueAt(row, 2);
//        selection = exchange + Constants.KEY_SEPERATOR_CHARACTER + symbol;
        selection = (String) companyTable.getTable().getModel().getValueAt(row, -1);
        if (editor != null) {
            editor.stopCellEditing();
        }

        if (symbols != null) {
            symbols.appendSymbol(selection);
        }

        if (symbolListener != null) {
            symbolListener.symbolSelected(selection);
        }
    }

    private void loadExchanges() {
        if (ExchangeStore.getSharedInstance().count() > 0) {
            exchangeCodes.clear();
            Enumeration<Exchange> records = ExchangeStore.getSharedInstance().getExchanges();

            while (records.hasMoreElements()) {
                Exchange exchange = records.nextElement();
                if (exchange.isDefault()) {
                    TWComboItem comboItem = new TWComboItem(exchange.getSymbol(), exchange.getDescription());
                    exchangeCodes.add(comboItem);
                }
                exchange = null;
            }
            records = null;
            Collections.sort(exchangeCodes);
            //todo - 21/12/2006
            if(exchangeCodes.size() >1) {
                exchangeCodes.add(0,new TWComboItem("ALL", Language.getString("ALL")));
            }

            try {
                if (cmbExchange.getSelectedIndex() < 0) {
                    cmbExchange.setSelectedIndex(0);
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
            cmbExchange.updateUI();
        }
    }

    // Action listener

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == cmbExchange) {
            setFilterInProgress(true);
        } else if (e.getSource() == okButton) {
            symbols.clear();
//            if (mode == MULTI_SELECTION) {
//                Collection<String> collection = companyTableModel.getSelectedSymbols();
//                for (String key : collection) {
//                    symbols.appendSymbol(key);
//                }
//                collection = null;
//
//                if (symbolListener != null) {
//                    symbolListener.symbolSelected(null);
//                }
//            } else {
            if (symbols != null) {
                int row = companyTable.getTable().getSelectedRow();
//                String exchange = (String) companyTable.getTable().getModel().getValueAt(row, 1);
//                String symbol = (String) companyTable.getTable().getModel().getValueAt(row, 2);
//                symbols.appendSymbol(exchange + Constants.KEY_SEPERATOR_CHARACTER + symbol);
                symbols.appendSymbol((String) companyTable.getTable().getModel().getValueAt(row, -1));
            }
//            }
            dialog.hide();
            symbols = null;
            symbolListener = null;
        } else if (e.getSource() == cancelButton) {
            dialog.hide();
            symbols = null;
            symbolListener = null;
        }

    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        setBorder(GUISettings.getTWFrameBorder());
    }

    // Exchange listener

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        loadExchanges();
        companyTableModel.applySettings();
    }

    public void exchangesLoaded() {

    }

    public void exchangeMustInitialize(Exchange exchange,long newDate, long oldDate) {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    // Window listener

    public void windowActivated(WindowEvent e) {

    }

    public void windowClosed(WindowEvent e) {

    }

    public void windowClosing(WindowEvent e) {
        if (editor != null) {
            editor.cancelCellEditing();
        }
        if (symbolListener != null) {
            symbolListener.actionCanceled();
        }
        active = false;
        symbolListener = null;
    }

    public void windowDeactivated(WindowEvent e) {
        dialog.hide();
    }

    public void windowDeiconified(WindowEvent e) {

    }

    public void windowIconified(WindowEvent e) {

    }

    public void windowOpened(WindowEvent e) {
    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    // Connection listener

    public void twConnected() {
        companyTableModel.refilter();
    }

    public void twDisconnected() {
        companyTableModel.resetSymbols();
    }

    // Key listener

    public void keyPressed(KeyEvent e) {

    }

    public void keyReleased(KeyEvent e) {

    }

    public void keyTyped(KeyEvent e) {
        if (e.getSource() == txtSearch)
            setFilterInProgress(true);
        else if (e.getSource() == companyTable.getTable())
            if (e.getKeyChar() == KeyEvent.VK_ENTER)
                selectRow();
    }

    // Mouse listener

    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() > 1) {
            selectRow();
        } else {
            int col = companyTable.getTable().getColumnModel().getColumnIndexAtX(e.getPoint().x);
            if (col == 0) {
                int row = companyTable.getTable().getSelectedRow();
//                String exchange = (String) companyTable.getTable().getModel().getValueAt(row, 1);
//                String symbol = (String) companyTable.getTable().getModel().getValueAt(row, 2);
//                String status = exchange + Constants.KEY_SEPERATOR_CHARACTER + symbol;
                String status = (String) companyTable.getTable().getModel().getValueAt(row, -1);
                companyTableModel.toggleSelected(status);
                companyTable.getTable().repaint();
            }
        }
    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }


    public int getWindowStyle() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getZOrder() {
        return 0;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isWindowVisible() {
        return companyTable.getModel().getViewSettings().isVisible();  //To change body of implemented methods use File | Settings | File Templates.
    }

    public JTable getTable1() {
        return companyTable.getTable();  //To change body of implemented methods use File | Settings | File Templates.
    }

    public JTable getTable2() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public JTable getTabFoler(String id) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applySettings() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void printTable() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isTitleVisible() {
        return companyTable.getModel().isTitleVisible();  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setTitleVisible(boolean status) {

    }

    public void windowClosing() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isDetached() {
        return companyTable.getModel().getViewSettings().isDetached;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public Object getDetachedFrame() {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }
}
