package com.isi.csvr.datastore;

// Copyright (c) 2000 Home

import java.util.Comparator;
import java.util.Hashtable;

public class SymbolUnsortComparator implements Comparator {

    private Hashtable symbols;

    /**
     * Constructor
     */

    public SymbolUnsortComparator(Hashtable symbols) {
        this.symbols = symbols;
    }

    public int compare(Object o1, Object o2) {
        String symbol1 = (String) o1;
        String symbol2 = (String) o2;

        long time1 = ((Long) symbols.get(symbol1)).longValue();
        long time2 = ((Long) symbols.get(symbol2)).longValue();

        if (time1 > time2)
            return 1;
        else if (time1 < time2)
            return -1;
        else
            return 0;
    }


    public boolean equals(Object obj) {
        return super.equals(obj);
    }

}