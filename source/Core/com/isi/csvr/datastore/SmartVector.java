package com.isi.csvr.datastore;

// Copyright (c) 2000 Home

import java.util.Vector;

public class SmartVector extends Vector {

    public boolean locked = false;

    /**
     * Constructor
     */
    public SmartVector() {
        super();
    }

    public synchronized void lock() {
        locked = true;
    }

    public synchronized void unlock() {
        locked = false;
    }

    public synchronized boolean isLock() {
        return locked;
    }
}