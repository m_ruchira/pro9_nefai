package com.isi.csvr.datastore;

import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 5, 2005
 * Time: 4:45:14 PM
 */
class ZoneRecord {
    String exchange;
    String zoneID;
    String description;
    TimeZone timeZone;

    public ZoneRecord(String exchange, String zoneID, String description) {
        this.description = description;
        this.exchange = exchange;
        this.zoneID = zoneID;
        timeZone = TimeZone.getTimeZone(zoneID);
    }
}
