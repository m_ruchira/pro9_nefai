package com.isi.csvr.datastore;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 18, 2003
 * Time: 12:35:01 PM
 * To change this template use Options | File Templates.
 */
public interface SymbolFilter {
    public String[] getFilteredList(String[] symbols);
}
