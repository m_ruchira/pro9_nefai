package com.isi.csvr.datastore;

import java.io.FileOutputStream;
import java.io.DataInputStream;
import java.io.FileInputStream;

/**
 * Created by IntelliJ IDEA. User: admin Date: 09-Oct-2007 Time: 15:19:20 To change this template use File | Settings |
 * File Templates.
 */

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.*;
import com.isi.csvr.watchlist.WatchListStore;
import com.isi.csvr.watchlist.WatchListManager;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.Application;

import java.io.DataInputStream;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.TimeZone;
import java.util.ArrayList;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class SymbolPanelStore implements ApplicationListener {

	private static DynamicArray store;
    private static SymbolPanelStore self;
    private ArrayList<String> symbolList = new ArrayList<String>();

    public static SymbolPanelStore getSharedInstance(){
        if (self == null){
            self = new SymbolPanelStore();

        }
        return self;
    }


    private SymbolPanelStore() {
		store  = new DynamicArray();
        store.add("");
        store.add("");
        store.add("");
        store.add("");
        store.add("");
        Application.getInstance().addApplicationListener(this);
    }

	public DynamicArray getStore(){
		return store;
	}

	public  String  getSymbolAt(int index){
		try {
			return (String)store.get(index);
		}
		catch (Exception ex) {
			return null;
		}
	}

    /*public static String getInstrumentAt(int index){
		try {
			return (String)instrumentStore.get(index);
		}
		catch (Exception ex) {
			return null;
		}
	}*/

	public  void setSymbol(int index, String symbol){
		if (store.size()> index){
            store.set(index, symbol);
            if (!store.contains(symbol)) {
                WatchListStore symbolPanelStroe= WatchListManager.getInstance().getStore("9999999999999");
                symbolPanelStroe.appendSymbol(symbol);
            }
        }else{
			store.add(symbol);
		}
	}
    public  void setChangedSymbiol(int index, String symbol){
        try {
            WatchListStore symbolPanelStroe= WatchListManager.getInstance().getStore("9999999999999");
            String sSymbol=(String)store.get(index);
            symbolPanelStroe.removeSymbol(sSymbol);
            if (store.size()> index){
                store.set(index, symbol);
                symbolPanelStroe.appendSymbol(symbol);

            }else{
                store.add(index,symbol);
            }
        } catch (Exception e) {
            store.add(index,symbol);
            
        }
    }

    /*public static void setInstrument(int index, String symbol){
		if (instrumentStore.size() > index){
			instrumentStore.set(index, symbol);
		}else{
			instrumentStore.add(symbol);
		}
	}*/

	public  void setSymbols(String[] symbols){

        try {
//			String[] list = symbols.split(",");
            String symbol;
            String exchange;
            int instrument;
			for (int i = 0; ((i < symbols.length) && (i < 5)); i++) {
                try {
                    symbol = SharedMethods.getSymbolFromKey(symbols[i]);
                    exchange = SharedMethods.getExchangeFromKey(symbols[i]);
                    instrument = SharedMethods.getInstrumentTypeFromKey(symbols[i]);
                    if(store.contains(SharedMethods.getKey(exchange, symbol, instrument))){
                        //no need of sending requests
                    }else{
                        if(DataStore.getSharedInstance().getStockObject(exchange,symbol,instrument,false) == null){
                            symbolList.add(symbols[i]);
                        }else{
                              DataStore.getSharedInstance().addSymbolRequest(symbols[i]);
                        }

                    }
                    setSymbol(i, SharedMethods.getKey(exchange, symbol, instrument));
//                    setInstrument(i, symbolData[2]);
                } catch (Exception e) {
                    // do nothing
                }
			}
//            instrumentStore.trimToSize();
            store.trimToSize();
//			list = null;
		}
		catch (Exception ex) {
			ex.printStackTrace();
		}
	}

	public static String getSymbols(){
		StringBuffer buffer = new StringBuffer();
        String symbol = null;

		for (int i = 0; i < store.size(); i++) {
            symbol = (String)store.get(i);
            if (symbol != null){
                try {
                    buffer.append(symbol + Constants.KEY_SEPERATOR_CHARACTER +DataStore.getSharedInstance().getStockObject(symbol).getInstrumentType());
                } catch (Exception e) {
                    buffer.append(symbol + Constants.KEY_SEPERATOR_CHARACTER +" ");
                }
                if (i != store.size() - 1) {
                    buffer.append(",");
                }
            }
		}
		return buffer.toString();
	}

//	public static void clear(){
//		store.clear();
//	}

	public static int size(){
		return store.size();
	}

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
        for(int i=0; i< symbolList.size(); i++){
            DataStore.getSharedInstance().addSymbolRequest((String)symbolList.get(i));
        }
    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}