package com.isi.csvr.datastore;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 29, 2006
 * Time: 4:03:29 PM
 */
public interface ExchangeInterface {
    long getVolume();

    double getTurnover();

    int getMarketStatus();

    String getDescription();

    String getSymbol();

    int getNoOfTrades();

    int getSymbolsTraded();

    int getNoOfUps();

    int getNoOfDown();

    int getNoOfNoChange();

    int getExpansionStatus();
}
