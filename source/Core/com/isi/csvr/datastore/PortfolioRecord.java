package com.isi.csvr.datastore;

import com.isi.csvr.shared.Stock;

public class PortfolioRecord {
    private String symbol;
    private String currency;
    private long holding = 0;
//    private float avgCost = 0;
    private double avgCost = 0;
    private PortfolioStore store;
    private double coupon = 0;
    private double[] marketValue = new double[2];
    private double[] gainLoss = new double[2];
    private double[] pctGainLoss = new double[2];

    public PortfolioRecord() {
    }

    /*public float getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(float newAvgCost) {
        avgCost = newAvgCost;
    }*/

    public double getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(double newAvgCost) {
        avgCost = newAvgCost;
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String newCurrency) {
        currency = newCurrency;
    }

    public long getHolding() {
        return holding;
    }

    public void setHolding(long newHolding) {
        holding = newHolding;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String newSymbol) {
        symbol = newSymbol;
    }

    public void setStore(PortfolioStore store) {
        this.store = store;
    }

    public PortfolioStore getStore() {
        return store;
    }

    public String toString() {
        return getSymbol() + "," + getHolding() + "," + getAvgCost() + "," + getCoupon();
    }

    public double getTotalCost() {
        Stock stock = DataStore.getSharedInstance().getStockObject(getSymbol());
        return getAvgCost() * getHolding() *
                CurrencyStore.getRate(getStore().getBaseCurrency(), stock.getCurrencyCode());
    }

    public double[] getMarketValue() {
        double totalMarketValue = 0;
        if (DataStore.getSharedInstance().isValidSymbol(getSymbol())) {
            Stock stock = DataStore.getSharedInstance().getStockObject(getSymbol());
            if (stock.getLastTradeValue() > 0) {
                totalMarketValue = stock.getLastTradeValue() * getHolding() *
                        CurrencyStore.getRate(getStore().getBaseCurrency(), stock.getCurrencyCode());
                stock = null;
                marketValue[0] = totalMarketValue;
            } else {
                totalMarketValue = stock.getPreviousClosed() * getHolding() *
                        CurrencyStore.getRate(getStore().getBaseCurrency(), stock.getCurrencyCode());
                stock = null;
                marketValue[0] = totalMarketValue;
            }
            return marketValue;
        }
        marketValue[0] = 0;
        return marketValue;
    }

    public double[] getGainLoss() {
        gainLoss[0] = (getMarketValue()[0] + coupon - getTotalCost());
        return gainLoss;
    }

    public double[] getPctGainLoss() {
        try {
            if ((getGainLoss()[0] != 0) && (getTotalCost() > 0))
                pctGainLoss[0] = (getGainLoss()[0] * 100) / getTotalCost();
            else
                pctGainLoss[0] = 0;
        } catch (Exception e) {
            pctGainLoss[0] = 0;
        }
        return pctGainLoss;
    }

    public double getCoupon() {
        return coupon;
    }

    public void setCoupon(double newCoupon) {
        coupon = newCoupon;
    }
}