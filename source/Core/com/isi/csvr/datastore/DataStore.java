/**
 * This is the container class for the stock object hashtable
 * Contains methods save and load the hash object from the disk.
 */

// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.datastore;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.Client;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.ValidatedCompanySelector;
import com.isi.csvr.bandwidthoptimizer.MainUI;
import com.isi.csvr.bandwidthoptimizer.OptimizedTreeRenderer;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.downloader.ContentCMConnector;
import com.isi.csvr.event.*;
import com.isi.csvr.portfolio.PFStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.util.TWProgressBar;
import com.isi.csvr.watchlist.WatchListManager;

import javax.swing.*;
import java.awt.*;
import java.io.*;
import java.util.*;

public class DataStore implements ExchangeListener, ConnectionListener, FunctionDataStoreInterface, ApplicationListener {

    public static final int FILTER_TYPE_ABSOLUTE = 0;
    public static final int FILTER_TYPE_RELATIVE = 1;

    private static Hashtable<String, Hashtable<String, Stock>> dataStore;
    private static Hashtable<String, Hashtable<String, Stock>> baseSymbolDataStore;
    private Hashtable symbolStore;
    private Hashtable countStore;
    private static Hashtable<String, String> allSymbolsStore;
    private Hashtable<String, Integer> requestRegister;
    private Hashtable topStocksSymbols;
    private static DataStore self = null;
    private int allcount = 0;
    private static Hashtable<String, Stock> chainStore;
    private static Hashtable<String, ArrayList<String>> regQuoteStore;
    private static Hashtable<String, FDTable> fd2Store;
    private static Vector extraSymbolRegister;
    private static Hashtable userRegister;           // Contains the details about the Quote requests
    private static Hashtable chainRegister;           // Contains the details about the Quote requests
    //private static Symbols   g_oSymbols;
    private static TWProgressBar quoteMeter;
    //    private Hashtable<String,String> regHash;
    private Vector<String> offlineValidatedReqs;
    private int autoIncrementID = 0;
    public static synchronized DataStore getSharedInstance() {
        if (self == null) {
            throw new RuntimeException("Data Store not initialized");
        }
        return self;
    }

    public static void initialize() {
        if (self == null) {
            self = new DataStore();
        }
    }

    /**
     * Constructor
     */
    private DataStore() {
        requestRegister = new Hashtable<String, Integer>(20, 0.9F);
        symbolStore = new Hashtable(20, 0.9F);
        countStore = new Hashtable(20, 0.9F);
        topStocksSymbols = new Hashtable(20, 0.9F);
        allSymbolsStore = new Hashtable<String, String>(20, 0.9F);
        dataStore = new Hashtable<String, Hashtable<String, Stock>>();
        baseSymbolDataStore = new Hashtable<String, Hashtable<String, Stock>>();
        if (Settings.isOfflineMode()) {
            loadData();
        }
//        loadData();
        init(false);
        ConnectionNotifier.getInstance().addConnectionListener(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        chainStore = new Hashtable<String, Stock>(50, 0.9F);
        fd2Store = new Hashtable<String, FDTable>(50, 0.9F);
        userRegister = new Hashtable(50, 0.9F);
        chainRegister = new Hashtable(50, 0.9F);
        //symbolCounter        = new Hashtable(50, 0.9F);
        extraSymbolRegister = new Vector(50, 1);
        quoteMeter = new TWProgressBar();
        quoteMeter.setTotalCount(Settings.getMaximumSymbolCount());
        quoteMeter.setDisplayType(quoteMeter.DISPLAY_TYPE_RATIO);
        quoteMeter.setPreferredSize(new Dimension(10, 15));
        regQuoteStore = new Hashtable<String, ArrayList<String>>();
//        regHash=new Hashtable<String,String>();
        offlineValidatedReqs = new Vector<String>();
        Application.getInstance().addApplicationListener(this);
    }

    public void init(boolean clearDataStore) {
        if (clearDataStore) {
            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exchange = (Exchange) exchanges.nextElement();
                if (exchange.isDefault()) {
                    try {
                        if (!exchange.isExpired() && !exchange.isInactive()) {
                            dataStore.get(exchange.getSymbol()).clear();
                        }
                    } catch (Exception e) {
                        // just in case
                    }
                }
            }
        }
        allSymbolsStore.clear();
        countStore.clear();
        topStocksSymbols.clear();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault()) {
                Symbols topStocksSymbolsRef = (Symbols) topStocksSymbols.get(exchange.getSymbol());
                if (topStocksSymbolsRef != null) {
                    topStocksSymbolsRef.clear();
                } else {
                    topStocksSymbolsRef = new Symbols();
                    topStocksSymbols.put(exchange.getSymbol(), topStocksSymbolsRef);
                }
                Symbols symbols = (Symbols) symbolStore.get(exchange.getSymbol());
                if (symbols != null) {
                    symbols.clear();
                } else {
                    symbols = new Symbols();
                    symbolStore.put(exchange.getSymbol(), symbols);
                }

                Market[] markets = exchange.getSubMarkets();
                if (markets != null && markets.length > 0) {
                    for (int i = 0; i < markets.length; i++) {
                        Market market = markets[i];
                        Symbols marketsymbols = (Symbols) symbolStore.get(SharedMethods.getExchWithSubMktKey(exchange.getSymbol(), market.getMarketID()));
                        if (marketsymbols != null) {
                            marketsymbols.clear();
                        } else {
                            marketsymbols = new Symbols();
                            symbolStore.put(SharedMethods.getExchWithSubMktKey(exchange.getSymbol(), market.getMarketID()), marketsymbols);
                        }
                        marketsymbols = null;
                    }
                }
                //change end
                loadExchange(exchange.getSymbol());
                symbols = null;
                topStocksSymbolsRef = null;
            }
            exchange = null;
        }
        Hashtable<String, Stock> customIndexStore = new Hashtable<String, Stock>();
        dataStore.put("CUSTOM_INDEX", customIndexStore);
        resetAllSymbolsStore();
        UpdateNotifier.setShapshotUpdated();
    }
//    public void init() {
//        dataStore = new Hashtable<String, Hashtable<String, Stock>>(5, 0.9F);
//        symbolStore = new Hashtable(20, 0.9F);
//        countStore = new Hashtable(20, 0.9F);
//        topStocksSymbols = new Hashtable(20, 0.9F);
//        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
//        while (exchanges.hasMoreElements()) {
//            Exchange exchange = (Exchange) exchanges.nextElement();
//            if (exchange.isDefault()) {
//                Symbols topStocksSymbolsRef = new Symbols();
//                topStocksSymbols.put(exchange.getSymbol(), topStocksSymbolsRef);
//                //change start
//                Symbols symbols = new Symbols();
//                symbolStore.put(exchange.getSymbol(),symbols);
//                symbols = null;
//                Market[] markets =exchange.getSubMarkets();
//                if(markets!=null && markets.length>0){
//                    for (int i = 0; i < markets.length; i++) {
//                        Market market = markets[i];
//                        symbols = new Symbols();
//                        symbolStore.put((exchange+ Constants.MARKET_SEPERATOR_CHARACTER + market.getMarketID()), symbols);
//                        symbols = null;
//                    }
//                }
//                //change end
//                loadExchange(exchange.getSymbol());
//                topStocksSymbolsRef = null;
//            }
//            exchange = null;
//        }
//        Hashtable<String,Stock> customIndexStore = new Hashtable<String, Stock>();
//        dataStore.put("CUSTOM_INDEX",customIndexStore);
//    }


    public static void clear() {
        Enumeration oKeys = allSymbolsStore.keys();
        String sKey = null;

        while (oKeys.hasMoreElements()) {
            sKey = (String) oKeys.nextElement();
//            ((Stock)quoteStore.get(sKey)).clear();
            (fd2Store.get(sKey)).clear();
        }
        sKey = null;
    }

    /*public static void clear(String exchange) {
        Enumeration oKeys = allSymbolsStore.keys();
        String sKey = null;

        while (oKeys.hasMoreElements()) {
            try {
                sKey = (String)oKeys.nextElement();
                if (sKey.startsWith(ExchangeMap.getSymbolFor((exchange)) + Constants.KEY_SEPERATOR_CHARACTER)){
//                   ((Stock)quoteStore.get(sKey)).clear();
                   (fd2Store.get(sKey)).clear();
                }
            }
            catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        sKey = null;
    }*/

    public static void reset() {
        userRegister.clear();
        allSymbolsStore.clear();
        fd2Store.clear();
        extraSymbolRegister.removeAllElements();
    }

    public static boolean isContainedSymbol(String sSymbol) {
        if ((sSymbol == null) || (sSymbol.trim().equals("")))
            return false;
        if (allSymbolsStore.containsKey(sSymbol))
            return true;
        else
            return false;
    }

    public static boolean isContainedSymbolInChain(String sSymbol) {
        if ((sSymbol == null) || (sSymbol.trim().equals("")))
            return false;
        if (chainStore.containsKey(sSymbol))
            return true;
        else
            return false;
    }

    public static int getUnchanedSymbolCount() {
        return allSymbolsStore.size();
    }

    public Hashtable getCusmIndexStore() {
        return dataStore.get("CUSTOM_INDEX");
    }

    public static TWProgressBar getQuoteMeter() {
        return quoteMeter;
    }

    public Stock getStockofCustomIndex(String symbol, int instrumentType) {
        try {
            Stock stock = (dataStore.get("CUSTOM_INDEX")).get(SharedMethods.getExchangeKey(symbol, instrumentType));
            if (stock != null) {
                return stock;
            } else {
                throw new Exception("Custom index not found");
            }
        } catch (Exception e) {
            Stock stock = new Stock(symbol, "CUSTOM_INDEX", instrumentType);
            addStockofCustomIndex(SharedMethods.getExchangeKey(symbol, instrumentType), stock);
            return stock;
        }
    }

    public void removeStockofCustomIndex(String exchangeKey) {
        dataStore.get("CUSTOM_INDEX").remove(exchangeKey);
    }

    public void addStockofCustomIndex(String exchangeKey, Stock stock) {
//        stock.setInstrumentType(Constants.ITYPE_INDEX);
        dataStore.get("CUSTOM_INDEX").put(exchangeKey, stock);
    }

    public static void clearChainStore() {
        chainStore.clear();
        chainRegister.clear();
    }

    public ArrayList<String> getRegQuoteStore(String key) {
        if (regQuoteStore.containsKey(key)) {
            return regQuoteStore.get(key);
        } else {
            regQuoteStore.put(key, new ArrayList<String>());
            return regQuoteStore.get(key);
        }

    }

    public void addToRegQuoteStore(String symbolKey, String marketCenter) {
        String skey = SharedMethods.getRQuoteKey(symbolKey, marketCenter);
//        if (regHash.containsKey(skey)) {
        if (regQuoteStore.containsKey(symbolKey)) {
            if (!regQuoteStore.get(symbolKey).contains(skey)) {
                regQuoteStore.get(symbolKey).add(skey);
            }
        } else {
            regQuoteStore.put(symbolKey, new ArrayList<String>());
            regQuoteStore.get(symbolKey).add(skey);
        }
//        }
    }

    public int getStockIndex(ArrayList arr, String marketCenter) {
        for (int i = 0; i < arr.size(); i++) {
            Stock stock = (Stock) arr.get(i);
            if (stock.getMarketCenter().equalsIgnoreCase(marketCenter)) {
                return i;
            }
        }
        return -1;
    }

    public void removeFromRegQuateStore(String key, Stock stock) {
        if (regQuoteStore.containsKey(key)) {
            regQuoteStore.remove(key);
        }
    }

    public static void clearRegQuoteStore() {
        regQuoteStore.clear();
    }

    public void loadExchange(String exchange) {
        updateFromStockMaster(exchange);
        createSymbolsObject(exchange);
    }

    public void initMarket(Exchange exchange) {
        if (exchange.isDefault()) {
            try {
                Enumeration symbols = getExchangeSymbolStore(exchange.getSymbol()).keys();
                String data;
                String symbol;
                int instrument = -1;
                while (symbols.hasMoreElements()) {
                    try {
                        data = (String) symbols.nextElement();
                        symbol = SharedMethods.getSymbolFromExchangeKey(data);
                        instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                        loadStock(exchange.getSymbol(), symbol, instrument, true);
                        symbol = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                exchange.initBidAskRange();
                exchange.initMoneyFlowRange();
                exchange.initSpreadRange();
                exchange.initRangeRange();
                exchange.initChangeRange();
//                MainUI.stockUpdator();
                MainUI.updateStocksFromDataStore();
            } catch (Exception e) {
                System.out.println("Error loading stock master for " + exchange);
                e.printStackTrace();
            }
        }
    }

    public void initFields() {
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault()) {
                initFields(exchange);
            }
            exchange = null;
        }
        exchanges = null;
    }

    /*public static synchronized void setSymbol(boolean isExtraSymbol, String sKey, String caption, String sInstrumentType, boolean chainedSymbol) {
        int     noOfOccurences = 0;
        Stock   oStock  = null;
        FDTable fdTable = null;
		int 	instrumentType;


		try {
			instrumentType = Integer.parseInt(sInstrumentType);
		}
		catch (NumberFormatException ex) {
            // if the symbol is already in, take the inst type from there
            instrumentType = DataStore.getSharedInstance().getStockObject(sKey).getInstrumentType();
            if (instrumentType <0){ // not found, exit
			    ex.printStackTrace();
			    return;
            }
		}
        String symbol = SharedMethods.getSymbolFromKey(sKey);
        String exchange = SharedMethods.getExchangeFromKey(sKey);
        if ((isExtraSymbol) && (!extraSymbolRegister.contains(sKey))) {
            extraSymbolRegister.addElement(sKey);
        }

        if (!chainedSymbol){
            if (!userRegister.containsKey(sKey)) {
                userRegister.put(sKey, new Integer(1));

                oStock = DataStore.getSharedInstance().getStockObject(exchange, symbol,instrumentType,true);
                oStock.setInstrumentType(instrumentType);

                if (caption != null){
                   oStock.setShortDescription(caption);
                }


                // Add the FD 2 Object
                fdTable = new FDTable();
                fdTable.setSymbol(symbol);//sKey.substring(sKey.indexOf(ESPConstants.KEY_SEPERATOR_CHARACTER)+1, sKey.length()));
                fdTable.setExchangeCode(exchange);//sKey.substring(0, sKey.indexOf(ESPConstants.KEY_SEPERATOR_CHARACTER)));
                fd2Store.put(sKey, fdTable);

                if (Settings.isConnected()) {
                    SendQFactory.addAddRequest(exchange,symbol, instrumentType, instrumentType, null);
                }


                // Remove the entry from the NewsStore as well
//                NewsStore.setNewsForSymbol(sKey);
                // Check for news availability
//                NewsStore.isNewsAvailable(sKey);
            } else {
                noOfOccurences = ((Integer)userRegister.get(sKey)).intValue();
                noOfOccurences = noOfOccurences + 1;
                userRegister.put(sKey, new Integer(noOfOccurences));
            }
        }else{

            if (!chainRegister.containsKey(sKey)) {
                chainRegister.put(sKey, new Integer(1));

                oStock = DataStore.getSharedInstance().getStockObject(exchange, symbol,instrumentType,true);
                oStock.setInstrumentType(instrumentType);

                if (caption != null){
                   oStock.setShortDescription(caption);
                }

                chainStore.put(sKey, oStock);

                if (Settings.isConnected()) {
                    SendQFactory.addAddRequest(exchange,symbol, instrumentType, instrumentType, null);
                }
            } else {
                noOfOccurences = ((Integer)chainRegister.get(sKey)).intValue();
                noOfOccurences = noOfOccurences + 1;
                chainRegister.put(sKey, new Integer(noOfOccurences));

            }
        }

        quoteMeter.setCurrentCount(getSymbolCount());
        fdTable = null;
        oStock  = null;
    }*/

    public static int getSymbolCount() {
        return dataStore.size();
    }

    public void initFields(Exchange exchange) {

        try {
            double bidAskMax = 0;
            double bidAskMin = Double.POSITIVE_INFINITY;
            double moneyflowMax = 0;
            double moneyflowMin = Double.POSITIVE_INFINITY;
            double spreadMax = Double.NEGATIVE_INFINITY;
            double spreadMin = Double.POSITIVE_INFINITY;
            double rangeMax = Double.NEGATIVE_INFINITY;
            double changeMax = Double.NEGATIVE_INFINITY;
            double changeMin = Double.POSITIVE_INFINITY;
            String exchangeSymbol;

            exchangeSymbol = exchange.getSymbol();
            Enumeration objects = getExchangeSymbolStore(exchange.getSymbol()).elements();
            Stock stock;
            /*exchange.initBidAskRange();
            exchange.initSpreadRange();
            exchange.initRangeRange();
            exchange.initChangeRange();*/
            while (objects.hasMoreElements()) {
                stock = (Stock) objects.nextElement();

                // bid ask ratio
                if (stock.bidaskRatio >= 1) {
                    if (stock.bidaskRatio == Double.POSITIVE_INFINITY) {
                        // do nothing
                    } else if (stock.bidaskRatio > bidAskMax) {
                        bidAskMax = stock.bidaskRatio;
                    }
                } else {
                    if (stock.bidaskRatio < 0) {
                        // do nothing
                    } else if (stock.bidaskRatio < bidAskMin) {
                        bidAskMin = stock.bidaskRatio;
                    }
                }
                //stock.calculateBidAskRange();

                // money flow ratio
                if (stock.cashFlowRatio >= 1) {
                    if (stock.cashFlowRatio == Double.POSITIVE_INFINITY) {
                        // do nothing
                    } else if (stock.cashFlowRatio > moneyflowMax) {
                        moneyflowMax = stock.cashFlowRatio;
                    }
                } else {
                    if (stock.cashFlowRatio < 0) {
                        // do nothing
                    } else if (stock.cashFlowRatio < moneyflowMin) {
                        moneyflowMin = stock.cashFlowRatio;
                    }
                }

                if (!Double.isInfinite(stock.getPctSpread())) {
                    if (stock.getPctSpread() >= 0) {
                        if (stock.getPctSpread() > spreadMax) {
                            spreadMax = stock.getPctSpread();
                        }
                    } else {
                        if (stock.getPctSpread() < spreadMin) {
                            spreadMin = stock.getPctSpread();
                        }
                    }
                }
                //stock.calculateSpreadRange();

                if ((stock.getPctRange() >= 0) && (stock.getPctRange() != Double.POSITIVE_INFINITY)) {
                    if (stock.getPctRange() > rangeMax) {
                        rangeMax = stock.getPctRange();
                    }
                }
                //stock.calculateRangeRange();

                if (stock.getPercentChange() >= 0) {
                    if (stock.getPercentChange() > changeMax) {
                        changeMax = stock.getPercentChange();
                    }
                } else {
                    if (stock.getPercentChange() < changeMin) {
                        changeMin = stock.getPercentChange();
                    }
                }
                //stock.calculateChangeRange();
                stock = null;
            }
            exchange.setBidAskMax(bidAskMax);
            exchange.setBidAskMin(bidAskMin);
            exchange.setMoneyFlowMax(moneyflowMax);
            exchange.setMoneyFlowMin(moneyflowMin);
            exchange.setSpreadMax(spreadMax);
            exchange.setSpreadMin(spreadMin);
            exchange.setRangeMax(rangeMax);
            exchange.setChangeMax(changeMax);
            exchange.setChangeMin(changeMin);
            objects = null;
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public void initTickMaps(Exchange exchange) {
        try {
            Enumeration objects = getExchangeSymbolStore(exchange.getSymbol()).elements();
            Stock stock;
            while (objects.hasMoreElements()) {
                stock = (Stock) objects.nextElement();
                stock.initTickMap();
                stock = null;
            }
            objects = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void createExchanges() {
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
//            if (exchange.isDefault()) {
            createExchange(exchange.getSymbol());
//            }
            exchange = null;
        }
    }

    public Hashtable createExchange(String exchange) {
        Hashtable<String, Stock> exchangeTable = new Hashtable<String, Stock>();
        dataStore.put(exchange, exchangeTable);
        return exchangeTable;
    }

    public Hashtable<String, Stock> getExchangeSymbolStore(String exchange) {
        if (dataStore.containsKey(exchange)) {
            return dataStore.get(exchange);
    }
        else
            return null;
    }

    private Counter getCounter(String exchange) {
        Counter counter = (Counter) countStore.get(exchange);
        if (counter != null) {
            return counter;
        } else {
            counter = new Counter();
            countStore.put(exchange, counter);  // create a new exchange
        }
        return counter;
    }

    private void incCounter(String exchange, int group) {
        Counter counter = getCounter(exchange);

        if (group == Constants.GROUP_INDCATOR) {
            counter.indexCount++;
        } else if (group == Constants.GROUP_STOCK) {
            counter.stockCount++;
        }

    }

    private void resetCounter(String exchange) {
        Counter counter = getCounter(exchange);

        counter.indexCount = 0;
        counter.stockCount = 0;
    }

    public void reset(Exchange exchange) {
        dataStore.clear();
        resetCounter(exchange.getSymbol());
        createExchanges();
        updateFromStockMaster(exchange.getSymbol());
        createSymbolsObject(exchange.getSymbol());
        initFields(exchange);
        initTickMaps(exchange);
    }


    private void createSymbolsObject(String exchange) {
        try {
            Symbols symbols = getSymbolsObject(exchange, true);
            Symbols topStocksSymbolsRef = getTopStocksIndex(exchange);
            topStocksSymbolsRef.clear();
            Enumeration keys = getSymbols(exchange);
            Exchange exch = ExchangeStore.getSharedInstance().getExchange(exchange);
            String symbol = null;
            String data;
            int instrument = Meta.INSTRUMENT_EQUITY;
            while (keys.hasMoreElements()) {
                try {
                    data = (String) keys.nextElement();
                    symbol = SharedMethods.getSymbolFromExchangeKey(data);
                    instrument = SharedMethods.getInstrumentFromExchangeKey(data);
//                Stock stock;
//                stock = getStockObject(exchange, symbol, instrument);

                    if (instrument != Meta.INSTRUMENT_INDEX) {
                        if (exch.hasSubMarkets() && exch.isUserSubMarketBreakdown()) {
                            String marketID = null;
                            if (symbol.indexOf(Constants.MARKET_SEPERATOR_CHARACTER) > 0) {
                                marketID = symbol.split(Constants.MARKET_SEPERATOR_CHARACTER)[1];
                                for (Market market : exch.getSubMarkets()) {
                                    if (market.getSymbol().equals(marketID)) {
                                        marketID = market.getMarketID();
                                        break;
                                    }
                                }
                            } else {
                                try {
                                    Stock stock = getStockObject(exchange, symbol, instrument);
                                    marketID = stock.getMarketID();
                                    if (marketID.equals("")) {
                                        marketID = null;
                                    } else {
                                        for (Market market : exch.getSubMarkets()) {
                                            if (market.getSymbol().equals(marketID)) {
                                                marketID = market.getMarketID();
                                                break;
                                            }
                                        }
                                    }
                                } catch (Exception e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                            }
                            getSymbolsObject(exchange, marketID, false).appendSymbol(SharedMethods.getKey(exchange, symbol, instrument));
                            marketID = null;
                        }
                        symbols.appendSymbol(SharedMethods.getKey(exchange, symbol, instrument));
                        //high
                        allSymbolsStore.put(SharedMethods.getKey(exchange, symbol, instrument), "");
                        topStocksSymbolsRef.appendSymbol(SharedMethods.getKey(exchange, symbol, instrument));
                    }
//                stock = null;
                    symbol = null;
                } catch (Exception e) {
                    System.out.println("SharedMethods.getKey(exchange, symbol, instrument)==" + SharedMethods.getKey(exchange, symbol, instrument));
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            symbol = null;
        } catch (Exception e) {
            System.out.println("Error creating Symbols for " + exchange);
        }
    }

    public Symbols getSymbolsObject(String exchange) {
        return getSymbolsObject(exchange, null, false);
    }

    public Symbols getSymbolsObject(String exchange, String marketCode) {
        //change start - this variable is changed to true
        return getSymbolsObject(exchange, marketCode, true);
    }

    private Symbols getSymbolsObject(String exchange, boolean createNew) {
        return getSymbolsObject(exchange, null, createNew);
    }


    private Symbols getSymbolsObject(String exchange, String marketCode, boolean createNew) {
        Symbols symbols = null;
        if (marketCode != null) {
            symbols = (Symbols) symbolStore.get(SharedMethods.getExchWithSubMktKey(exchange, marketCode));
            if (createNew && (symbols == null)) {
                symbols = new Symbols();
                symbolStore.put(SharedMethods.getExchWithSubMktKey(exchange, marketCode), symbols);
            }
        } else {
            symbols = (Symbols) symbolStore.get((exchange));
            if (createNew && (symbols == null)) {
                symbols = new Symbols();
                symbolStore.put((exchange), symbols);
            }
        }
        //change start
//        if (marketCode == null){
        return symbols;
//        } else {
//            Symbols newSymbols = new Symbols();
//            for (String key: symbols.getSymbols()){
//                try {
//                    if (getStockObject(key).getMarketID().equals(marketCode)){
//                        newSymbols.appendSymbol(key);
//                    }
//                } catch (Exception e) { // market id may be null
//
//                }
//            }
//            return newSymbols;
//        }
        //change end
    }

    public Symbols getTopStocksIndex(String exchange) {
        Symbols symbols = (Symbols) topStocksSymbols.get(exchange);
        if (symbols == null) {
            symbols = new Symbols();
            topStocksSymbols.put(exchange, symbols);
        }
        return symbols;
    }

    /**
     * Read the list of symbols from the stock master and update
     * Company details for each stock in the hash table.
     * If the stock is not found in the hash table, it is added.
     */
    private void updateFromStockMaster(String exchange) {
        try {
            Enumeration oKeys = SymbolMaster.getSymbols(exchange);
            String symbol = null;
            String data = null;
            int instrument = -1;
            while (oKeys.hasMoreElements()) {
                try {
                    data = (String) oKeys.nextElement();
                    symbol = data; //SharedMethods.getSymbolFromExchangeKey(data);
                    instrument = SymbolMaster.getInstrumentType(symbol, exchange);
//                    instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                    if (!symbol.trim().equals(""))
                        loadStock(exchange, symbol, instrument, false);

                } catch (Exception e) {
                    System.out.println("updatestockMaster exception exchange=" + exchange);
                    if (symbol != null) {
                        System.out.println("symbol = " + symbol);
                    }
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            System.out.println("Error loading stock master for " + exchange);
        }
    }

    public void loadStock(String exchange, String symbol, int instrument, boolean removeExisting) {
        Stock stock;
//        int instrumentType = SymbolMaster.getInstrumentType(symbol, exchange);

        if (removeExisting) {
            removeStock(exchange, symbol, instrument);
        }
        stock = createStockObject(exchange, symbol, instrument);
        /*if (instrument == Meta.FUTURES){
            stock = createBaseSymbol(exchange, symbol, instrument);
        }else {
            stock = createStockObject(exchange, symbol, instrument);
        }*/

        stock.setLongDescription(SymbolMaster.getCompanyName(symbol, exchange));
        stock.setShortDescription(SymbolMaster.getShortDescription(symbol, exchange));
        stock.setCompanyCode(SymbolMaster.getCompanyCode(symbol, exchange));
        stock.setCurrencyCode(SymbolMaster.getCurrencyCode(symbol, exchange));
        stock.setAssetClass(SymbolMaster.getAssetClass(symbol, exchange));
        stock.setSectorCode(SymbolMaster.getSectorCode(symbol, exchange));
        stock.setParams(SymbolMaster.getSymbolParams(symbol, exchange));
        stock.setMarketID(SymbolMaster.getMarketID(symbol, exchange));
        stock.setInstrumentType(instrument);
        stock.setDecimalCount(SymbolMaster.getDecimalPlaces(symbol, exchange));
        stock.setAlias(SymbolMaster.getAlias(symbol, exchange, instrument));
        stock.setLotSize((long) SymbolMaster.getLotSize(symbol, exchange));
        stock.setUnit(SymbolMaster.getUnit(symbol, exchange));
        stock.setFirstSubscriptionStartDate(SymbolMaster.getFirstSubStartDate(symbol,exchange));
        stock.setFirstSubscriptionEndDate(SymbolMaster.getFirstSubEndDate(symbol,exchange));
        stock.setSecondSubscriptionStartDate(SymbolMaster.getSecondSubStartDate(symbol,exchange));
        stock.setSecondSubscriptionEndDate(SymbolMaster.getSecondSubEndDate(symbol,exchange));

        //---- Added by Shanika ---------------
        //todo: remove if not neccesary : issue in LKCSE
        if (OptimizedTreeRenderer.selectedSymbols.containsKey(stock.getKey())) {
            stock.setSymbolEnabled(true);
        }
        if ((stock.getSectorCode().equals("") || stock.getSectorCode().equals("N/A") || stock.getSectorCode().equals("0")) && (stock.getInstrumentType() != 7)) {
            stock.setSectorCode(Constants.DEFAULT_SECTOR);
        }
        stock = null;
    }

    public Stock createStockObject(String exchange, String symbol, int type) {
        return getStockObject(exchange, symbol, type, true); // must return a valid stock object allways
    }

    public Stock getStockObject(String key) {
        String symbol;
        String exchange;
        try {
            symbol = getSymbol(key);
            exchange = getExchangeSymbol(key);
            int instrument = getInstrumentType(key);
            return getStockObject(exchange, symbol, instrument, false);
        } finally {
            symbol = null;
            exchange = null;
        }
    }
    public Stock getStockByExchangeKey(String exchange, String symbol) {
        Stock stock = null;
        String symbolOfStock;
        Hashtable exchangetable = dataStore.get(exchange);
        Enumeration exchangeEnum = exchangetable.keys();
        while (exchangeEnum.hasMoreElements()) {
            String key = (String) exchangeEnum.nextElement();
            symbolOfStock = SharedMethods.getSymbolFromExchangeKey(key);
            if (symbol.equals(symbolOfStock)) {
                stock = (Stock) exchangetable.get(key);
            }

        }
        return stock;

    }
    public Stock getStockObject(String key, boolean createNewIfNotAvaiable) {
        String symbol;
        String exchange;
        try {
            symbol = getSymbol(key);
            exchange = getExchangeSymbol(key);
            int instrument = getInstrumentType(key);
            return getStockObject(exchange, symbol, instrument, createNewIfNotAvaiable);
        } finally {
            symbol = null;
            exchange = null;
        }
    }


    public Stock getStockObject(String exchange, String symbol, int instrument) {
        if (instrument > 1000) {
            return getStockObject(exchange, symbol, instrument, false);
        } else {
            return getStockObject(exchange, symbol, instrument, true);
        }
    }

    public Stock getBaseSymbol(String exchange, String symbol, int type) {
        return createBaseSymbol(exchange, symbol, type);
    }


    public Stock createBaseSymbol(String exchange, String symbol, int type) {
        if ((exchange == null) || (symbol == null) || (symbol.equals("") || (exchange.equals(""))))
            return null;

        Stock stock = null;
        Hashtable<String, Stock> exchangetable = baseSymbolDataStore.get(exchange);
        if (exchangetable != null) {
            stock = exchangetable.get(symbol);
        } else {
            exchangetable = new Hashtable<String, Stock>();
            baseSymbolDataStore.put(exchange, exchangetable);  // create a new exchange
        }

        if (stock == null) {
            stock = new Stock(symbol, exchange, type);
            exchangetable.put(symbol, stock);
            //updateStockFromMasterData(stock);
            stock.setInstrumentType(type);
            stock.setAssetClass(Constants.DEFAULT_ASSETCLASS);
            stock.setCurrencyCode(Constants.DEFAULT_CURRENCY_CODE);
            stock.setCompanyCode(Language.getString("NA"));
            stock.setLongDescription(Language.getString("NA"));
            stock.setSectorCode(Constants.DEFAULT_SECTOR);
            stock.setMarketID(Constants.DEFAULT_MARKET);
            stock.setLotSize((long) SymbolMaster.getLotSize(symbol, exchange));
            stock.setUnit(SymbolMaster.getUnit(symbol, exchange));
            try {
                stock.setDecimalCount(ExchangeStore.getSharedInstance().getExchange(exchange).getPriceDecimalPlaces());
            } catch (Exception e) {
            }
        }

        return stock;
    }

    /* public Stock getRegionalQuoteStock(String symbol,String exchange, int instrument, String marketCenter){
        String key=SharedMethods.getRQuoteKey(SharedMethods.getKey(exchange, symbol, instrument), marketCenter);
        if(regHash.containsKey(key)){
           return regHash.get(key);
        }else{
            regHash.put(key, String);
//            regHash.get(key).setMarketCenter(marketCenter);
            return regHash.get(key);
        }


    }*/
    /**
     * Returns the stock object for the given symbol
     */

    public Stock getStock(String exchange,String symbol, int type) {
        Stock stock = null;
        Hashtable exchangetable = dataStore.get(exchange);
        if (exchangetable != null) {
            if (exchangetable.containsKey(SharedMethods.getExchangeKey(symbol, type))) {
                stock = (Stock) exchangetable.get(SharedMethods.getExchangeKey(symbol, type));
            } else {
                if (exchangetable.containsKey(SharedMethods.getExchangeKey(symbol, Constants.TRUST))) {
                    stock = (Stock) exchangetable.get(SharedMethods.getExchangeKey(symbol, Constants.TRUST));
                } else if (exchangetable.containsKey(SharedMethods.getExchangeKey(symbol, Constants.WARRANT))) {
                    stock = (Stock) exchangetable.get(SharedMethods.getExchangeKey(symbol, Constants.WARRANT));
                } else if (exchangetable.containsKey(SharedMethods.getExchangeKey(symbol, Constants.RIGHT))) {
                    stock = (Stock) exchangetable.get(SharedMethods.getExchangeKey(symbol, Constants.RIGHT));
                } else if (exchangetable.containsKey(SharedMethods.getExchangeKey(symbol, Constants.INDEX))) {
                    stock = (Stock) exchangetable.get(SharedMethods.getExchangeKey(symbol, Constants.INDEX));
                } else if (exchangetable.containsKey(SharedMethods.getExchangeKey(symbol, Constants.OPTION))) {
                    stock = (Stock) exchangetable.get(SharedMethods.getExchangeKey(symbol, Constants.OPTION));
                }
            }
        }
        return stock;
    }
    public Stock getStockObject(String exchange, String symbol, int type, boolean createNewIfNotAvaiable) {

        if ((exchange == null) || (symbol == null) || (symbol.equals("") || (exchange.equals(""))))
            return null;

        Stock stock = null;
        Hashtable exchangetable = dataStore.get(exchange);
        if (exchangetable != null) {
            stock = (Stock) exchangetable.get(SharedMethods.getExchangeKey(symbol, type));
        } else {
            if (createNewIfNotAvaiable) {
                exchangetable = new Hashtable();
                dataStore.put(exchange, exchangetable);  // create a new exchange
            }
        }
        if (type < 0) {
            type = Meta.INSTRUMENT_EQUITY;
        }
        if (stock == null) {
            stock = getStock(exchange,symbol,type);
        }
        if ((stock == null) && (createNewIfNotAvaiable)) {
            stock = new Stock(symbol, exchange, type);

            stock.setInstrumentType(type);
            stock.setAssetClass(Constants.DEFAULT_ASSETCLASS);
            stock.setCurrencyCode(Constants.DEFAULT_CURRENCY_CODE);
            stock.setCompanyCode(Language.getString("NA"));
            stock.setLongDescription(Language.getString("NA"));
            stock.setSectorCode(Constants.DEFAULT_SECTOR);
            stock.setMarketID(Constants.DEFAULT_MARKET);
            try {
                stock.setDecimalCount(ExchangeStore.getSharedInstance().getExchange(exchange).getPriceDecimalPlaces());
            } catch (Exception e) {
            }
            incCounter(exchange, stock.getGroup());

            Market selectedMarket = null;
            String marketID = null;
//            loadStock(exchange,symbol, type, false);
            try {
                if (symbol.indexOf(Constants.MARKET_SEPERATOR_CHARACTER) > 0) {
                    marketID = (symbol).split(Constants.MARKET_SEPERATOR_CHARACTER)[1];
                } else {
                    marketID = SymbolMaster.getMarketID(symbol, exchange);
                }
            }
            catch (Exception e) {
                marketID = SymbolMaster.getMarketID(symbol, exchange);
            }
            if ((marketID != null) && !(marketID.equals("")) && !(marketID.equals(Constants.DEFAULT_MARKET)) && ExchangeStore.getSharedInstance().getExchange(exchange).hasSubMarkets()) {
                Market[] markets = ExchangeStore.getSharedInstance().getExchange(exchange).getSubMarkets();
                if (markets != null && markets.length > 0) {
                    for (int i = 0; i < markets.length; i++) {
                        Market market = markets[i];
                        if (market.getMarketID().equals(marketID)) {
                            selectedMarket = market;
                            stock.setMarketID(market.getMarketID());
                            break;
                        }
                    }
                }
            }

            exchangetable.put(SharedMethods.getExchangeKey(symbol, type), stock);
            if (ExchangeStore.getSharedInstance().isDefault(exchange)) {
                if (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX) {
                    Symbols symbols;
                    if (selectedMarket != null) {
                        symbols = getSymbolsObject(exchange, selectedMarket.getMarketID(), true);
                    } else {
                        symbols = getSymbolsObject(exchange, true);
                    }
                    symbols.appendSymbol(SharedMethods.getKey(exchange, symbol, type));
                    allSymbolsStore.put(SharedMethods.getKey(exchange, symbol, type), "");
                    try {
                        getTopStocksIndex(exchange).appendSymbol(SharedMethods.getKey(exchange, symbol, type));
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    symbols = null;
                }
            }
            marketID = null;
            selectedMarket = null;
        }
        exchangetable = null;

        return stock;
    }

    public void removeStock(String exchange, String symbol, int instrumentType) {
        try {
            Hashtable exchangetable;
            exchangetable = dataStore.get(exchange);
            /*if (instrumentType == Meta.FUTURES){
                exchangetable = baseSymbolDataStore.get(exchange);
            }else {
                exchangetable = dataStore.get(exchange);
            }*/
            if (exchangetable != null) {
                exchangetable.remove(SharedMethods.getExchangeKey(symbol, instrumentType));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     * Checks if the given sy mbol is in thr hashtable
     */
    public boolean isValidSymbol(String key) {

        if ((key == null) || (key.trim().equals("")))
            return false;

        String symbol = getSymbol(key);
        String exchange = getExchangeSymbol(key);
        int instrument = getInstrumentType(key);
        if ((instrument == -1) && (symbol != null) && (exchange != null)) { // only the instrument
            try {
                instrument = Integer.parseInt(symbol);
                symbol = exchange;
                if (searchStock(key) != null)
                    return true;
                else
                    return false;
            } catch (Exception e) {
                return false;
            }
        } else {
            if (getStockObject(exchange, symbol, instrument) != null) {
                return true;
            } else {
                return false;
            }
        }
    }

    public boolean isValidCompany(String exchange, String symbol) {
        try {
            if (ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()) {
                String key = findKeyFromDataStore(symbol, exchange);
                if (key != null) {
                    return true;
                }
            } else {
                return true;
            }
            return false;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return true;
        }
    }

    public Stock searchStock(String key) {
        try {
            String symbol = getSymbol(key);
            String exchange = getExchangeSymbol(key);
            int instrument = getInstrumentType(key);
            if ((instrument == -1) && (symbol != null) && (exchange != null)) { // only the instrument
                instrument = Integer.parseInt(symbol);
                symbol = exchange;
                Enumeration exchanges = dataStore.elements();
                while (exchanges.hasMoreElements()) {
                    Enumeration objects = ((Hashtable) exchanges.nextElement()).elements();
                    Stock stock;
                    while (objects.hasMoreElements()) {
                        stock = (Stock) objects.nextElement();
                        if ((stock.getSymbolCode().equals(symbol)) && (stock.getInstrumentType() == instrument)) {
                            return stock;
                        }
                        stock = null;
                    }
                    objects = null;
                }
                exchanges = null;
            } else {
                return getStockObject(exchange, symbol, instrument);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String searchSymbol(String key) {
        return searchSymbol(key, true);
    }

    public String searchSymbol(String key, boolean useGUI) {
        try {
            if (!useGUI) {
                Stock stock = searchStock(key);
                return stock.getKey();
            } else {
                StringBuilder buffer = new StringBuilder();
                String symbol = getSymbol(key);
                String exchange = getExchangeSymbol(key);
                int instrument = getInstrumentType(key);
                int count = 0;

                if ((instrument == -1) && (symbol != null) && (exchange != null)) { // only the instrument
                    instrument = Integer.parseInt(symbol);
                    symbol = exchange;

                    Enumeration exchanges = dataStore.elements();
                    while (exchanges.hasMoreElements()) {
                        Enumeration objects = ((Hashtable) exchanges.nextElement()).elements();
                        Stock stock;
                        while (objects.hasMoreElements()) {
                            stock = (Stock) objects.nextElement();
                            if ((stock.getSymbol().equals(symbol)) && (stock.getInstrumentType() == instrument)) {
                                count++;
                                exchange = stock.getExchange();
                                buffer.append(exchange);
                                buffer.append(Meta.ID);
                                buffer.append(symbol);
                                buffer.append(Meta.ID);
                                buffer.append(getCompanyName(SharedMethods.getKey(exchange, symbol, instrument)));
                                buffer.append(Meta.ID);
                                buffer.append(Meta.FD);
                            }
                            stock = null;
                        }
                    }
                    exchanges = null;

                    if (count > 1) {
                        if (buffer.toString().length() > 0) {
                            ValidatedCompanySelector companySelector = new ValidatedCompanySelector(buffer.toString(),
                                    ValidatedCompanySelector.MODE_INTERNAL_VALIDATION);
                            return companySelector.getSelectedRecord();
                        } else {
                            return null;
                        }
                    } else if (count == 1) {
                        return SharedMethods.getKey(exchange, symbol, instrument);
                    } else {
                        return null;
                    }

                } else {
                    return key;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /**
     * Returns the hashtable object
     */
    public Hashtable getHashtable(String exchange) {
        // todo reg references
        return getExchangeSymbolStore(exchange);
    }

    /**
     * Returns the company name for the given company symol
     * Returns a null string if the company is not found
     */
    public String getCompanyName(String key) {

        Stock stock = getStockObject(key);
        if (stock == null)
            return "";
        else if (stock.getLongDescription() == null)
            return key;
        else
            return stock.getLongDescription();
    }

    /*public int getInstrumentType(String exchange, String symbol) {

        Stock stock = getStockObject(exchange, symbol);
        if (stock == null)
            return -1;
        else
            return stock.getInstrumentType();
    }*/

    /**
     * Returns the Short description for the given company symol
     * Returns a null string if the company is not found
     */
    public String getShortDescription(String key) {

        Stock stock = getStockObject(key);
        if (stock == null)
            return "";
        else if (stock.getShortDescription() == null)
            return key;
        else
            return stock.getShortDescription();
    }

    public String getCompanyCode(String key) {

        Stock stock = getStockObject(key);
        if (stock == null)
            return "";
        else if (stock.getCompanyCode() == null)
            return key;
        else
            return stock.getCompanyCode();
    }

    public String getShortDescription(String exchange, String symbol, int instrument) {

        Stock stock = getStockObject(exchange, symbol, instrument);
        if (stock == null)
            return "";
        else if ((stock.getShortDescription() == null) || (stock.getShortDescription().equals("")))
            return symbol;
        else
            return stock.getShortDescription();
    }

    /**
     * Returns the asset class for the given company symol
     * Returns a null string if the company is not found
     */
    public String getAssetClass(String key) {

        Stock oStock = getStockObject(key);
        if (oStock == null)
            return "";
        else
            return oStock.getAssetClass();
    }

    /**
     * Returns the currency codes for the given company symol
     * Returns a null string if the company is not found
     */
    public String getCurrencyCode(String key) {

        Stock stock = getStockObject(key);
        if (stock == null)
            return "";
        else
            return stock.getCurrencyCode();
    }

    /**
     * Returns the sector code for the given company symol
     * Returns a null string if the company is not found
     */
    public String getSectorCode(String key) {

        Stock stock = getStockObject(key);
        if (stock == null)
            return "";
        else
            return stock.getSectorCode();
    }

    /**
     * Returns the list of keys as an enumeration
     */
    public Enumeration<String> getSymbols(String exchange) {
        return getExchangeSymbolStore(exchange).keys();
    }

    /**
     * Returns the symbol count
     */
    public int getSymbolCount(String exchange, int type) {
        //return getExchange(exchange).size();
        if (type == Constants.GROUP_STOCK)
            return getCounter(exchange).stockCount;
        else
            return getCounter(exchange).indexCount;
    }

    public synchronized void updateTickMap(String exchange, String symbol, int instrument, int location, byte tick, int size) {

        if ((size - 1 - location) < Stock.TICK_MAP_LENGTH) {
            Stock stock = getStockObject(exchange, symbol, instrument);
            if (stock != null) {
                stock.updateTickMap(size - 1 - location, tick);
                stock = null;
            }
        }
    }

    /*public String[] getAllSymbols(String exchange) {
        int symbolCount = getSymbolCount(exchange, ESPConstants.GROUP_STOCK);
        int indexCount = getSymbolCount(exchange, ESPConstants.GROUP_INDEX);

        String[] symbols = new String[symbolCount + indexCount];
        System.arraycopy(getSymbolsObject(exchange).getSymbols(), 0, symbols, 0, symbolCount);
        System.arraycopy(IndexStore.getAllIndices().getSymbols(), 0, symbols, symbolCount, indexCount);

        return symbols;
    }*/

    /*public String[][] getCompanyList() {

        int symbolCount = 0;
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.isDefault()) {
                symbolCount += getSymbolCount(exchange.getSymbol(), ESPConstants.GROUP_STOCK);
                symbolCount += getSymbolCount(exchange.getSymbol(), ESPConstants.GROUP_INDCATOR);
            }
            exchange = null;
        }

        String[][] symbolArray = new String[symbolCount][];
        exchanges = ExchangeStore.getSharedInstance().getExchanges();
        symbolCount = 0;
        String symbol;
        Stock stock;
        String[] exchange_symbol_company;
        try {
            while (exchanges.hasMoreElements()) {
                Exchange exchange = (Exchange) exchanges.nextElement();
                if (exchange.isDefault()) {
                    Enumeration symbols = DataStore.getSharedInstance().getFilteredList(exchange.getSymbol(),
                            ESPConstants.GROUP_ANY, ESPConstants.TYPE_ANY);
                    while (symbols.hasMoreElements()) {
                        symbol = (String) symbols.nextElement();
                        stock = getStockObject(exchange.getSymbol(), symbol);
                        exchange_symbol_company = new String[3];
                        exchange_symbol_company[0] = stock.getExchange();
                        exchange_symbol_company[1] = stock.getSymbol();
                        exchange_symbol_company[2] = stock.getLongDescription();
                        symbolArray[symbolCount] = exchange_symbol_company;
                        exchange_symbol_company = null;
                        stock = null;
                        symbol = null;
                        symbolCount++;
                    }
                    symbols = null;
                }
                exchange = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        exchanges = null;

        return symbolArray;
    }*/

    public Enumeration<String> getFilteredList(String exchange, int type) {
        return new FilteredList(exchange, type);
    }

    private String getSymbol(String key) {
        try {
            return SharedMethods.getSymbolFromKey(key);
        } catch (Exception e) {
            return null;
        }
    }

    private String getExchangeSymbol(String key) {
        try {
            return SharedMethods.getExchangeFromKey(key);
        } catch (Exception e) {
            return null;
        }
    }

    public int getInstrumentType(String key) {
        try {
            return SharedMethods.getInstrumentTypeFromKey(key);
        } catch (Exception e) {
            return -1;
        }
    }

    /*public synchronized int getRequestCount(String exchange, String symbol){
        try {
            if (ExchangeStore.getSharedInstance().isDefault(exchange)) return 0;

            Integer count = (Integer)requestRegister.get(exchange + ESPConstants.KEY_SEPERATOR_CHARACTER + symbol);
            return count.intValue();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }
*/

    public synchronized boolean addSymbolRequest(String key) {
        String symbol = getSymbol(key);
        String exchange = getExchangeSymbol(key);
        int instrumentType = getInstrumentType(key);
        return addSymbolRequest(exchange, symbol, instrumentType);
    }

    public synchronized boolean addSymbolRequest(String exchange, String symbol, int instrumentType) {
        int messageType = SharedMethods.getSymbolType(instrumentType);
        return addSymbolRequest(exchange, symbol, instrumentType, messageType);
    }

    public synchronized boolean addSymbolRequest(String exchange, String symbol, int instrumentType, int messageType) {
        try {
            if ((messageType != Meta.CUST_INDEX) && ExchangeStore.getSharedInstance().isDefault(exchange)) return false;
            if (instrumentType < 0) {
                instrumentType = Meta.INSTRUMENT_EQUITY;
            }
            String key = null;
            Integer count = null;
            try {
                key = SharedMethods.getKey(exchange, symbol, instrumentType) + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + messageType;
                count = requestRegister.get(key);
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            if (count == null) {
                boolean alreadyValidated = true;
                String marketCenter = null;
//                if(messageType == Meta.SYMBOL_TYPE_EQUITY){
                alreadyValidated = createNonDefaultStock(exchange, symbol, instrumentType);
//                }
                try {
                    marketCenter = getStockObject(exchange, symbol, instrumentType).getMarketCenter();
                } catch (Exception e) {
                    marketCenter = null;
                }
                if (alreadyValidated) {
                    requestRegister.put(key, 1);
                    SendQFactory.addAddRequest(exchange, symbol, messageType, instrumentType, marketCenter);
                } else if (messageType == Meta.SYMBOL_TYPE_OPTIONS) {   // to add Option symbols to the Option Chain need to send this request
                    requestRegister.put(key, 1);
                    SendQFactory.addAddRequest(exchange, symbol, messageType, instrumentType, marketCenter);
                } else { // remove symbol from everywhere
                    SymbolsRegistry.getSharedInstance().removeSymbol(SharedMethods.getKey(exchange, symbol, instrumentType));
                }
                return true;
            } else {
                requestRegister.put(key, count + 1);
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public synchronized boolean addSymbolRequest(String exchange, String symbol, int instrumentType, boolean sendValidateRequest, Object frame, int messageType, boolean validateBulk, String[] symbolkeys) {
        try {
            if (ExchangeStore.getSharedInstance().isDefault(exchange)) return false;

            boolean alreadyValidated = createNonDefaultStock(exchange, symbol, instrumentType);
            if (instrumentType < 0) {
                instrumentType = Meta.INSTRUMENT_EQUITY;
            }

            if (alreadyValidated || (messageType < 0)) {
                messageType = SharedMethods.getSymbolType(instrumentType);
            }
            String key = null;
            Integer count = null;
            try {
                key = SharedMethods.getKey(exchange, symbol, instrumentType) + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + messageType;
                count = requestRegister.get(key);
            } catch (Exception e) {
                key = SharedMethods.getKey(exchange, symbol, instrumentType);
                count = requestRegister.get(key);
            }

            if (count == null) {

                if (alreadyValidated) {
                    requestRegister.put(key, 1);

                    if (messageType < 0) {
                        SendQFactory.addAddRequest(exchange, symbol, Meta.SYMBOL_TYPE_EQUITY, instrumentType
                                , getStockObject(exchange, symbol, instrumentType).getMarketCenter());
                    } else {
                        SendQFactory.addAddRequest(exchange, symbol, messageType, instrumentType,
                                getStockObject(exchange, symbol, instrumentType).getMarketCenter());
                    }
                } else { // remove symbol from everywhere
                    if (sendValidateRequest) {
                        // if (!validateBulk) {
                        if (Settings.isConnected()) {
                            String requestID = getSequenceID();
                            DataStore.getSharedInstance().checkSymbolAvailability(symbol, requestID, exchange, instrumentType);
                            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, frame);
                        } else {
                            String request;
                            String requestID = getSequenceID();
                            if ((exchange == null) || (exchange.equals(""))) {
                                exchange = "*";
                                request = Meta.VALIDATE_SYMBOL + Meta.DS + symbol + Meta.FD + requestID + Meta.FD + exchange + Meta.FD + Language.getSelectedLanguage() + Meta.EOL;

                            } else {
                                request = Meta.VALIDATE_SYMBOL + Meta.DS + symbol + Meta.FD + requestID + Meta.FD + exchange + Meta.FD + Language.getSelectedLanguage() + Meta.FD + instrumentType + Meta.EOL;
                            }
                            offlineValidatedReqs.add(request);

                            //    }
                        }
                        //putting offline requets to new store to resend later


                    } else if (!ExchangeStore.getSharedInstance().isLoadingFailed()) { // loading of exchange store may fail during a auto update
                        SymbolsRegistry.getSharedInstance().removeSymbol(SharedMethods.getKey(exchange, symbol, instrumentType));
                    }
                }
                return true;
//                }
            } else {
                requestRegister.put(key, new Integer(count.intValue() + 1));
                return false;
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public synchronized String[] addBulkValidationrequests(String[] symbolKeys) {
        Vector<String> symbolvector = new Vector<String>(10);
        Vector<String> symbolsInFile = new Vector<String>(10);
        String[] removedAlreadyInFileStr;
        Object[] removedAlreadyInFileObj;
        for (String skey : symbolKeys) {
            boolean alreadyValidated = createNonDefaultStock(SharedMethods.getExchangeFromKey(skey), SharedMethods.getSymbolFromKey(skey), SharedMethods.getInstrumentTypeFromKey(skey));
            boolean isDefault = ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(skey));
            if (!isDefault) {
                if (!alreadyValidated) {
                    try {
                        if (!isSymbolExistOnFile(SharedMethods.getSymbolFromKey(skey), SharedMethods.getExchangeFromKey(skey), SharedMethods.getInstrumentTypeFromKey(skey))) {
                            symbolvector.add(skey);
                        } else {
                            symbolsInFile.add(skey);
                        }
                    } catch (Exception e) {
                        symbolvector.add(skey);  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        }
        //constructing symbols list
        removedAlreadyInFileStr = new String[symbolsInFile.size()];
        removedAlreadyInFileObj = new String[symbolsInFile.size()];
        if (symbolvector.size() > 0) {
            String symbolList = "";
            for (String skey : symbolvector) {
                symbolList = symbolList + SharedMethods.getExchangeFromKey(skey) + Meta.ID
                        + SharedMethods.getSymbolFromKey(skey) + Meta.ID
                        + SharedMethods.getInstrumentTypeFromKey(skey) + Meta.RS;

            }
            //////////////////////////////////////////////////////////////////////////////////////
            int k = 0;
            String testList = "";
            for (int j = 0; j < symbolvector.size(); j++) {
                testList = testList + SharedMethods.getExchangeFromKey(symbolvector.get(j)) + Meta.ID
                        + SharedMethods.getSymbolFromKey(symbolvector.get(j)) + Meta.ID
                        + SharedMethods.getInstrumentTypeFromKey(symbolvector.get(j)) + Meta.RS;
                k = k + 1;
                if ((k == 50) && (j <= symbolvector.size())) {
                    System.out.println("Test List : " + testList);
                    System.out.println("Count : " + j);
                    testList = "";
                    k = 0;
                }
                if ((k < 50) && (j == symbolvector.size() - 1)) {
                    System.out.println("Remaining List : " + testList);
                    System.out.println("rem Count : " + j);
                    testList = "";
                    k = 0;
                    break;
                }

            }


            //////////////////////////////////////////////////////////////////////////////////////
            if (Settings.isConnected()) {
                String requestID = getSequenceID();
                SendQFactory.addBulkValidateRequest(symbolList, requestID);
            } else {
                String requestID = getSequenceID();
                String request = Meta.VALIDATE_SYMBOL_BULK + Meta.DS + requestID + Meta.FD + Language.getSelectedLanguage() + Meta.FD + symbolList + Meta.EOL;
                offlineValidatedReqs.add(request);
            }
        }
        symbolsInFile.toArray(removedAlreadyInFileObj);
        removedAlreadyInFileStr = (String[]) removedAlreadyInFileObj;
        return removedAlreadyInFileStr;
    }
    public synchronized boolean removeSymbolRequest(String key) {
        String exchange = SharedMethods.getExchangeFromKey(key);
        String symbol = SharedMethods.getSymbolFromKey(key);
        int instrument = SharedMethods.getInstrumentTypeFromKey(key);
        return removeSymbolRequest(exchange, symbol, instrument);
    }

    public synchronized boolean removeSymbolRequest(String key, int messageType) {
        String exchange = SharedMethods.getExchangeFromKey(key);
        String symbol = SharedMethods.getSymbolFromKey(key);
        int instrument = SharedMethods.getInstrumentTypeFromKey(key);
        return removeSymbolRequest(exchange, symbol, instrument, messageType);
    }

    public synchronized boolean removeSymbolRequest(String exchange, String symbol, int instrumentType) {
        return removeSymbolRequest(exchange, symbol, instrumentType, SharedMethods.getSymbolType(instrumentType));
    }

    public synchronized boolean removeSymbolRequest(String exchange, String symbol, int instrumentType, int messageType) {
        try {
            if (ExchangeStore.getSharedInstance().isDefault(exchange)) return false;

//            SharedMethods.printLine("Data Store: Remove Request " + exchange + " : " + symbol, false);
            String key = null;
            Integer count = null;
            if (instrumentType < 0) {
                instrumentType = Meta.INSTRUMENT_EQUITY;
            }
            try {
                if (messageType < 0) {
                    messageType = Meta.SYMBOL_TYPE_EQUITY;
                    key = SharedMethods.getKey(exchange, symbol, instrumentType) + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + Meta.SYMBOL_TYPE_EQUITY;
                } else {
                    key = SharedMethods.getKey(exchange, symbol, instrumentType) + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + messageType;
                }
//                key = SharedMethods.getKey(exchange, symbol) + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + getStockObject(exchange, symbol).getInstrumentType();
                count = requestRegister.get(key);
            } catch (Exception e) {
                key = SharedMethods.getKey(exchange, symbol, instrumentType);
                count = requestRegister.get(key);
            }

            if (count != null) {
                if (count == 1) { // last request being removed
                    requestRegister.remove(key);

                    Stock stock;
                    if (messageType == Meta.SYMBOL_TYPE_EQUITY) {
                        if (!isSymbolInSystem(key)) { // donot remove definitions if symbol is
//                            SharedMethods.printLine("Tracing remove : ",true);
                            ValidatedSymbols.getSharedInstance().removeSymbol(key);//     still in the system
                            System.out.println("Data Store: Removed from validated store " + key);
                        }
                        if (!getRegQuoteStore(SharedMethods.getRQuoteSymbol(exchange, symbol, instrumentType)).contains(SharedMethods.getKey(exchange, symbol, instrumentType))) {
                            WatchListManager.getInstance().fireSymbolRemoved(key, null);
                            stock = getExchangeSymbolStore(exchange).remove(SharedMethods.getExchangeKey(symbol, instrumentType));
                            if (instrumentType < 0) {
                                instrumentType = stock.getInstrumentType();
                            }
                            if (Settings.isConnected()) {
                                SendQFactory.addRemoveRequest(exchange, symbol, messageType, instrumentType, stock.getMarketCenter());
                                System.out.println("Symbol removed " + symbol);
                            }
                        } else {
                            System.out.println("is in RQuote store");
                            if (Settings.isConnected()) {
                                SendQFactory.addRemoveRequest(exchange, symbol, messageType, instrumentType, SharedMethods.getMktCenterFromRQuoteKey(key));
                                System.out.println("Symbol removed " + symbol);
                            }
                        }
                    } else {
                        stock = getExchangeSymbolStore(exchange).get(SharedMethods.getExchangeKey(symbol, instrumentType));
                        if (instrumentType < 0) {
                            instrumentType = stock.getInstrumentType();
                        }
                        if (Settings.isConnected()) {
                            SendQFactory.addRemoveRequest(exchange, symbol, messageType, instrumentType, stock.getMarketCenter());
                            System.out.println("Symbol removed " + symbol);
                        }
                    }
                    return true;
                } else {
                    requestRegister.put(key, count - 1);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    public void setValidatedDataForStock(String key, String validatedData) {
        Stock stock = DataStore.getSharedInstance().getStockObject(key);
        if ((stock != null) && (validatedData != null)) {
            String symbol = stock.getSymbol();
            String[] fields = validatedData.split(Meta.ID);
            int instrumentType = Integer.parseInt(fields[2]);
            stock.setShortDescription(symbol);
//            stock.setLongDescription(symbol);
            if (!UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[8])).equals("")) {
                stock.setShortDescription(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[8])));
                stock.setLongDescription(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[8])));
            }
            if (!UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])).equals("")) {
                stock.setLongDescription(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])));
            }
            stock.setSectorCode(fields[5]);
            stock.setCurrencyCode(fields[6]);
            stock.setCompanyCode(fields[7]);
            stock.setInstrumentType(instrumentType);
            try {
                stock.setDecimalCount(Byte.parseByte(fields[9]));
            } catch (Exception e) {
                stock.setDecimalCount((byte) 2);
            }
            try {
                stock.setLotSize((long) Double.parseDouble(fields[10]));
            } catch (Exception e) {
                stock.setLotSize(1);
            }
            try {
                stock.setUnit(fields[11]);
            } catch (Exception e) {
                stock.setUnit("");
            }
            try {
                stock.setMarketCenter(fields[12]);
            } catch (Exception e) {
                stock.setMarketCenter(null);
            }
        }
    }

    private boolean createNonDefaultStock(String exchange, String symbol, int instrument) {

        String key = SharedMethods.getKey(exchange, symbol, instrument);
        String metaData = ValidatedSymbols.getSharedInstance().getFrame(key);
        if (metaData != null) {
            String[] fields = metaData.split(Meta.ID);
            int instrumentType = Integer.parseInt(fields[2]);
            Stock stock = DataStore.getSharedInstance().createStockObject(exchange, symbol, instrumentType);
            stock.setShortDescription(symbol);
//            stock.setLongDescription(symbol);
            stock.setSymbolEnabled(true);
            if (!UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[8])).equals("")) {

                stock.setShortDescription(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[8])));
                stock.setLongDescription(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[8])));
            }
            if (!UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])).equals("")) {

                stock.setLongDescription(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])));
            }
            stock.setSectorCode(fields[5]);
            stock.setCurrencyCode(fields[6]);
            stock.setCompanyCode(fields[7]);
            stock.setInstrumentType(instrumentType);
            try {
                stock.setDecimalCount(Byte.parseByte(fields[9]));
            } catch (Exception e) {
                stock.setDecimalCount((byte) 2);
            }
            try {
                stock.setLotSize((long) Double.parseDouble(fields[10]));
            } catch (Exception e) {
                stock.setLotSize(1);
            }
            try {
                stock.setUnit(fields[11]);
            } catch (Exception e) {
                stock.setUnit("");
            }
            try {
                stock.setMarketCenter(fields[12]);
            } catch (Exception e) {
                stock.setMarketCenter(null);
            }
            return true;
        } else if (getStockObject(exchange, symbol, instrument) != null) {
            return true;
        }
        return false;
    }

    public void createIndexes() {
        try {
//            ValidatedSymbols vSymbols=ValidatedSymbols.getSharedInstance();
            Enumeration indexEnum = SymbolMaster.indexTable.keys();
            String exchange = null;
            String symbol = null;
            int instrument = -1;
//            Stock oStock=null;
            while (indexEnum.hasMoreElements()) {
                try {
                    exchange = (String) indexEnum.nextElement();
                    DynamicArray indexes = SymbolMaster.indexTable.get(exchange);
                    for (int i = 0; i < indexes.size(); i++) {
                        String data = (String) indexes.get(i);
                        symbol = data; //SharedMethods.getSymbolFromExchangeKey(data);
                        //                        instrument = SymbolMaster.getInstrumentType(symbol, exchange);  --// Indexes are not added to the symbol master.so no point of getting instype from symbolmaster
                        //todo:load indexes details from indextable -shanika
                        instrument = Meta.INSTRUMENT_INDEX;
                        loadStock(exchange, symbol, instrument, false);
//                        createStockObject(exchange,symbol,Constants.ITYPE_INDEX);
//                        addSymbolRequest(exchange,symbol);
                    }
                    exchange = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


    }


    /*public synchronized boolean removeSymbolRequest(String exchange, String symbol,int messageType) {
        try {
            if (ExchangeStore.getSharedInstance().isDefault(exchange)) return false;

//            SharedMethods.printLine("Data Store: Remove Request " + exchange + " : " + symbol, false);
            String key = null;
            Integer count = null;
            try {
                key = SharedMethods.getKey(exchange, symbol) + Constants.REQUEST_TYPE_SEPERATOR_CHARACTER + messageType;
                count = requestRegister.get(key);
            } catch(Exception e) {
            }

            if (count != null) {
                if (count.intValue() == 1) { // last request being removed
                    WatchListManager.getInstance().fireSymbolRemoved(SharedMethods.getKey(exchange, symbol), null);
                    requestRegister.remove(key);
                    *//*if (!isSymbolInSystem(key)){ // donot remove definitions if symbol is
                        ValidatedSymbols.getSharedInstance().removeSymbol(key);//     still in the system
                        System.out.println("Data Store: Removed from validated store " + key);
                    }*//*
//                    Stock stock = getExchangeSymbolStore(exchange).remove(symbol);
//                    if (Settings.isConnected()) {
                        SendQFactory.addRemoveRequest(exchange, symbol, messageType, getStockObject(exchange, symbol).getInstrumentType(),
                                                      getStockObject(exchange, symbol).getMarketCenter());
                        System.out.println("Symbol removed with messageType = "+messageType+" , " + symbol + "-" + exchange);
//                    }
                    return true;
                } else {
                    requestRegister.put(key, new Integer(count.intValue() - 1));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
     }*/

    public boolean isSymbolInSystem(String key) {
        return SymbolsRegistry.getSharedInstance().isSymbolIn(key) ||
                PFStore.isContainedKey(key);
    }

    /*public void registerSymbols() {
        int type;

        try {
            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                Exchange exchange = (Exchange) exchanges.nextElement();
                if (exchange != null) {
                    if (!exchange.isDefault()) {
                        Enumeration stocks = getExchangeSymbolStore(exchange.getSymbol()).elements();
                        while (stocks.hasMoreElements()) {
                            Stock stock = (Stock) stocks.nextElement();
                            type = SymbolRepository.getSymbolType(exchange.getSymbol(), stock.getSymbol());
//                            if (indexType)
//                                SendQFactory.addAddRequest(exchange.getSymbol(), stock.getSymbol(), Meta.MESSAGE_TYPE_INDEX);
//                            else
                                SendQFactory.addAddRequest(exchange.getSymbol(), stock.getSymbol(), type);
                            stock = null;
                        }
                        stocks = null;
                    }
                }
                exchange = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public void unregisterSymbols(Symbols symbols) {
        try {
            if (!symbols.isRegisterFlagSet()) return; // already unregistered

            String[] keys = symbols.getSymbols();

            for (int i = 0; i < keys.length; i++) {
                removeSymbolRequest(keys[i]);
            }
            symbols.setRegisterFlag(false);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void registerSymbols(Symbols symbols) {
        try {
            if ((symbols == null) || (symbols.isRegisterFlagSet())) return; // already registered

            String[] keys = symbols.getSymbols();

            for (int i = 0; i < keys.length; i++) {
                addSymbolRequest(keys[i]);
            }
            symbols.setRegisterFlag(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void sendAddRequests(byte path) {
        Enumeration<String> keys = requestRegister.keys();
        String exchange;
        String symbol;
        int instrument = -1;
        String data;
        String[] records = null;
        while (keys.hasMoreElements()) {
            try {
                data = keys.nextElement();
                records = data.split(Constants.REQUEST_TYPE_SEPERATOR_CHARACTER);
                data = records[0];
                int messageType = Meta.SYMBOL_TYPE_EQUITY;
                try {
                    messageType = Integer.parseInt(records[1]);
                } catch (Exception e) {
                    messageType = Meta.SYMBOL_TYPE_EQUITY;
                }
                exchange = SharedMethods.getExchangeFromKey(data);
                symbol = SharedMethods.getSymbolFromKey(data);
                instrument = SharedMethods.getInstrumentTypeFromKey(data);
                Stock stock = getStockObject(exchange, symbol, instrument);
                if ((ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == path) && (!ExchangeStore.isExpired(exchange)) && !ExchangeStore.isInactive(exchange)) {
                    SendQFactory.addAddRequest(path, exchange, symbol, messageType, instrument, stock.getMarketCenter());
                }
                /*if ((path == Constants.PATH_PRIMARY) && (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == path)) {
                    SendQFactory.addAddRequest(Constants.PATH_PRIMARY, exchange, symbol, stock.getInstrumentType(), stock.getInstrumentType(), stock.getMarketCenter());
                } else if ((path == Constants.PATH_SECONDARY) && (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == path)) {
                    SendQFactory.addAddRequest(Constants.PATH_SECONDARY, exchange, symbol, stock.getInstrumentType(), stock.getInstrumentType(), stock.getMarketCenter());
//                    InternationalConnector.getSharedInstance().addAddRequest(exchange, symbol, stock.getInstrumentType(), stock.getInstrumentType());
                }*/
                stock = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void clearRequestCounts() {
        requestRegister.clear();
    }

    public synchronized void exchangeAdded(Exchange exchange) {
    }

    public void exchangeRemoved(Exchange exchange) {
        try {
            dataStore.remove(exchange.getSymbol());
            symbolStore.remove(exchange.getSymbol());
            WatchListManager.getInstance().removeSymbols(exchange.getSymbol());
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void exchangeUpgraded(Exchange exchange) {
        updateFromStockMaster(exchange.getSymbol());
    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {

    }

    public void exchangesLoaded() {

    }

    public void exchangeMustInitialize(Exchange exchange, long newDate, long oldDate) {
        System.out.println("------------- Initializing market " + exchange.getSymbol() + " -------------");
        initMarket(exchange);
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
        /*try {
            Hashtable exchangeTable = getExchangeSymbolStore(exchange.getSymbol());
            Enumeration<Stock> stocks = exchangeTable.elements();

            long timeOffset = exchange.getTodaysZoneAdjustment();
            System.out.println("Zone Offset " + timeOffset);
            while (stocks.hasMoreElements()) {
                Stock stock =  stocks.nextElement();
                stock.setTimeOffset(timeOffset);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void twConnected() {
        try {
//            WatchListStore[] wStore =WatchListManager.getInstance().getStores();
//            for(WatchListStore watchListStore: wStore){
//                String[] symbolList=((WatchListStore)watchListStore).getSymbols();
//                for(String key: symbolList){
//                    String frame= ValidatedSymbols.getSharedInstance().getFrame(key);
//                    if(frame!=null){
//                        String requestID = "MainBoard:" + System.currentTimeMillis();
//                        SendQFactory.addValidateRequest(SharedMethods.getSymbolFromKey(key),requestID, SharedMethods.getExchangeFromKey(key));
//
//                    }
//
//                }
//
//
//            }
//            DynamicArray symbolPanelStore=SymbolPanelStore.getSharedInstance().getStore();
//            for(int i=0;i<symbolPanelStore.size();i++){
//                if (((String)symbolPanelStore.get(i)!=null)&&(! SharedMethods.getExchangeFromKey((String)symbolPanelStore.get(i)).equals(""))) {
//                    String requestID = "MainBoard:" + System.currentTimeMillis();
//                    SendQFactory.addValidateRequest(SharedMethods.getSymbolFromKey((String)symbolPanelStore.get(i)),requestID, SharedMethods.getExchangeFromKey((String)symbolPanelStore.get(i)),SharedMethods.getInstrumentTypeFromKey((String)symbolPanelStore.get(i)));
//                }
//
//            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

    public void twDisconnected() {

    }

    private class FilteredList implements Enumeration<String> {
        private Enumeration allSymbols;
        private int type;
        private Hashtable table;
        private String nextElement;

        public FilteredList(String exchange, int type) {
            this.type = type;

            table = getExchangeSymbolStore(exchange);
            allSymbols = table.keys();
        }

        public boolean hasMoreElements() {
            try {
                nextElement = (String) allSymbols.nextElement();
                Stock stock = (Stock) table.get(nextElement);

                while ((type != stock.getInstrumentType())) {

                    nextElement = (String) allSymbols.nextElement();
                    stock = null;
                    stock = (Stock) table.get(nextElement);
                }
                stock = null;
                return true;
            } catch (Exception e) {
                allSymbols = null;
                table = null;
                nextElement = null;
                return false;
            }
        }

        public String nextElement() {
            return SharedMethods.getSymbolFromExchangeKey(nextElement);
        }
    }

    public void printPreferences() {
        Enumeration keys = requestRegister.keys();
        while (keys.hasMoreElements()) {
            String key = (String) keys.nextElement();
            System.out.println(key + " - " + (requestRegister.get(key)).intValue());
        }

        /*Enumeration stocks = getExchangeSymbolStore("DGCX").keys();
        while (stocks.hasMoreElements()){
            System.out.println(stocks.nextElement());
        }*/

    }

    /**
     * Returns the Market Id for the given company symol
     * Returns a null string if the company is not found
     */
    public String getMarketID(String key) {

        Stock oStock = getStockObject(key);
        if (oStock == null)
            return "";
        else
            return oStock.getMarketID();
    }

    /**
     * Save the hash table as an object to the disk
     */
    public static void saveData() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath() + "datastore/snapshot.mdf");
            ObjectOutputStream oObjOut = new ObjectOutputStream(out);
            oObjOut.writeObject(dataStore);
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
        Thread.yield();
        try {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath() + "datastore/optionsnapshot.mdf");
            ObjectOutputStream oObjOut = new ObjectOutputStream(out);
            oObjOut.writeObject(baseSymbolDataStore);
            out.close();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Loads the saved hash table from the disk as an object
     */
    public void loadData() {
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/snapshot.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            dataStore = (Hashtable<String, Hashtable<String, Stock>>) oObjIn.readObject();
            oIn.close();
//            resetAllSymbolsStore();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (dataStore == null) {
            dataStore = new Hashtable<String, Hashtable<String, Stock>>();
        }

        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/optionsnapshot.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            baseSymbolDataStore = (Hashtable<String, Hashtable<String, Stock>>) oObjIn.readObject();
            oIn.close();
//            resetAllSymbolsStore();
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (baseSymbolDataStore == null) {
            baseSymbolDataStore = new Hashtable<String, Hashtable<String, Stock>>();
        }
        resetAllSymbolsStore();
        try {
            Enumeration<String> exchanges = dataStore.keys();
            Enumeration<Stock> stocks;
            String exchange;
            Stock stock;
            Hashtable<String, Stock> exchangeStore;
            while (exchanges.hasMoreElements()) {
                exchange = exchanges.nextElement();
                if (ExchangeStore.getSharedInstance().isDefault(exchange)) {
                    exchangeStore = dataStore.get(exchange);
                    stocks = exchangeStore.elements();
                    while (stocks.hasMoreElements()) {
                        try {
                            stock = stocks.nextElement();
                            if (stock.getLastTradeValue() > 0) {
                                Client.getInstance().getTodaysTrades().insertSymbol(stock.getExchange(), stock.getSymbol(), stock.getMarketID(), stock.getInstrumentType());
                            }
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String findKey(String symbol) {
//        return SymbolMaster.getExchangeForSymbol(symbol,false);
        return findKeyFromDataStore(symbol);
    }

    public String findKeyFromDataStore(String symbol) {
        try {
            StringBuilder buffer = new StringBuilder();
//            Enumeration tables = dataStore.keys();
            Enumeration tables = getAllSymbols();

            String exchangeCode = null;
            String key = null;
            Exchange exchange = null;
            String lastSelectedSymbol = null;
            int lastSelectedInstruemnt = -1;
            String lastSelectedExchange = null;
            String lastSelectedMarket = null;
            int count = 0;
            while (tables.hasMoreElements()) {
                key = (String) tables.nextElement();
                exchangeCode = SharedMethods.getExchangeFromKey(key);
                exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
                if (exchange != null) {
                    try {
                        if ((exchange.getSubMarketCount() == 0) || symbol.contains(Constants.MARKET_SEPERATOR_CHARACTER)) {
                            if (SharedMethods.getSymbolFromKey(key).equalsIgnoreCase(symbol)) {
                                count++;
                                try {
                                    lastSelectedExchange = exchangeCode;
                                    lastSelectedSymbol = SharedMethods.getSymbolFromKey(key);
//                                    lastSelectedSymbol = symbol;
                                    lastSelectedInstruemnt = SharedMethods.getInstrumentTypeFromKey(key);
                                    buffer.append(exchange);
                                    buffer.append(Meta.ID);
                                    buffer.append(SharedMethods.getSymbolFromKey(key));
//                                    buffer.append(symbol);
                                    buffer.append(Meta.ID);
                                    buffer.append(getCompanyName(key));
                                    buffer.append(Meta.ID);
                                    buffer.append(lastSelectedInstruemnt);
                                    buffer.append(Meta.ID);
                                    buffer.append(Meta.FD);
                                } catch (Exception e) {
                                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                }
                            }
                        } else {
                            try {
                                Market[] markets = exchange.getSubMarkets();
                                for (Market market : markets) {
                                    if ((market.getSymbol() == null) || (market.getSymbol().trim().equals(""))) { // no duplicate symbols in sub markets
                                        if (SharedMethods.getSymbolFromKey(key).equalsIgnoreCase(symbol)) {
                                            count++;
                                            lastSelectedExchange = exchangeCode;
                                            lastSelectedSymbol = SharedMethods.getSymbolFromKey(key);
                                            //lastSelectedSymbol = symbol;
                                            lastSelectedInstruemnt = SharedMethods.getInstrumentTypeFromKey(key);
                                            buffer.append(exchange);
                                            buffer.append(Meta.ID);
                                            buffer.append(SharedMethods.getSymbolFromKey(key));
//                                            buffer.append(symbol);
                                            buffer.append(Meta.ID);
                                            buffer.append(getCompanyName(key));
                                            buffer.append(Meta.ID);
                                            buffer.append(lastSelectedInstruemnt);
                                            buffer.append(Meta.ID);
                                            buffer.append(Meta.FD);
                                            break;
                                        }
                                    } else {
                                        if (SharedMethods.getSymbolFromKey(key).equalsIgnoreCase(symbol + Constants.MARKET_SEPERATOR_CHARACTER + market.getSymbol())) {
                                            count++;
                                            lastSelectedExchange = exchangeCode;
                                            lastSelectedSymbol = SharedMethods.getSymbolFromKey(key);
//                                            lastSelectedSymbol = symbol;
                                            lastSelectedMarket = market.getSymbol();
                                            lastSelectedInstruemnt = SharedMethods.getInstrumentTypeFromKey(key);
                                            buffer.append(exchange);
                                            buffer.append(Meta.ID);
                                            buffer.append(SharedMethods.getSymbolFromKey(key));
                                            //buffer.append(symbol + Constants.MARKET_SEPERATOR_CHARACTER + market.getSymbol());
                                            buffer.append(Meta.ID);
                                            buffer.append(getCompanyName(key) + " - " + market.getDescription());
                                            buffer.append(Meta.ID);
                                            buffer.append(lastSelectedInstruemnt);
                                            buffer.append(Meta.ID);
                                            buffer.append(Meta.FD);
                                        } else if (SharedMethods.getSymbolFromKey(key).equalsIgnoreCase(symbol)) {
                                            if ((lastSelectedSymbol == null) || !lastSelectedSymbol.equals(symbol)) {
                                                count++;
                                                lastSelectedExchange = exchangeCode;
                                                lastSelectedSymbol = SharedMethods.getSymbolFromKey(key);
//                                                lastSelectedSymbol = symbol;
                                                lastSelectedInstruemnt = SharedMethods.getInstrumentTypeFromKey(key);
                                                //                                    lastSelectedMarket = null;
                                                buffer.append(exchange);
                                                buffer.append(Meta.ID);
                                                buffer.append(SharedMethods.getSymbolFromKey(key));
                                                buffer.append(Meta.ID);
                                                buffer.append(getCompanyName(key));
                                                buffer.append(Meta.ID);
                                                buffer.append(lastSelectedInstruemnt);
                                                buffer.append(Meta.ID);
                                                buffer.append(Meta.FD);
                                                //                                } else {
                                            }
                                        }
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }

            if (count > 1) {
                if (buffer.toString().length() > 0) {
                    ValidatedCompanySelector companySelector = new ValidatedCompanySelector(buffer.toString(), ValidatedCompanySelector.MODE_INTERNAL_VALIDATION);
                    return companySelector.getSelectedRecord();
                } else {
                    return null;
                }
            } else if (count == 1) {
                if (lastSelectedMarket == null) {
                    return SharedMethods.getKey(lastSelectedExchange, lastSelectedSymbol, lastSelectedInstruemnt);
                } else {
                    return SharedMethods.getKey(lastSelectedExchange, lastSelectedSymbol + Constants.MARKET_SEPERATOR_CHARACTER + lastSelectedMarket, lastSelectedInstruemnt);// lastSelectedExchange + Constants.KEY_SEPERATOR_CHARACTER + lastSelectedSymbol + Constants.MARKET_SEPERATOR_CHARACTER + lastSelectedMarket;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public String findKeyFromDataStore(String symbol, String exchangeCode) {
        try {
            StringBuilder buffer = new StringBuilder();
//            Enumeration tables = dataStore.keys();
            Enumeration tables = getSymbols(exchangeCode);
//            String exchangeCode = null;
            String key = null;
            Exchange exchange = null;
            String lastSelectedSymbol = null;
            int lastSelectedInstruemnt = -1;
            String lastSelectedExchange = null;
            String lastSelectedMarket = null;
            int count = 0;
//            exchangeCode = SharedMethods.getExchangeFromKey(key);
            exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
            while (tables.hasMoreElements()) {
                key = (String) tables.nextElement();
                if ((exchange.getSubMarketCount() == 0) || symbol.contains(Constants.MARKET_SEPERATOR_CHARACTER)) {
                    if (SharedMethods.getSymbolFromExchangeKey(key).equals(symbol)) {
                        count++;
                        lastSelectedExchange = exchangeCode;
                        lastSelectedSymbol = symbol;
                        lastSelectedInstruemnt = SharedMethods.getInstrumentFromExchangeKey(key);
                        buffer.append(exchange);
                        buffer.append(Meta.ID);
                        buffer.append(symbol);
                        buffer.append(Meta.ID);
                        buffer.append(getCompanyName(key));
                        buffer.append(Meta.ID);
                        buffer.append(lastSelectedInstruemnt);
                        buffer.append(Meta.ID);
                        buffer.append(Meta.FD);
                    }
                } else {
                    Market[] markets = exchange.getSubMarkets();
                    for (Market market : markets) {
                        if ((market.getSymbol() == null) || (market.getSymbol().trim().equals(""))) { // no duplicate symbols in sub markets
                            if (SharedMethods.getSymbolFromExchangeKey(key).equals(symbol)) {
                                count++;
                                lastSelectedExchange = exchangeCode;
                                lastSelectedSymbol = symbol;
                                lastSelectedInstruemnt = SharedMethods.getInstrumentFromExchangeKey(key);
                                buffer.append(exchange);
                                buffer.append(Meta.ID);
                                buffer.append(symbol);
                                buffer.append(Meta.ID);
                                buffer.append(getCompanyName(key));
                                buffer.append(Meta.ID);
                                buffer.append(lastSelectedInstruemnt);
                                buffer.append(Meta.ID);
                                buffer.append(Meta.FD);
                                break;
                            }
                        } else {
                            if (SharedMethods.getSymbolFromExchangeKey(key).equals(symbol + Constants.MARKET_SEPERATOR_CHARACTER + market.getSymbol())) {
                                count++;
                                lastSelectedExchange = exchangeCode;
                                lastSelectedSymbol = symbol;
                                lastSelectedMarket = market.getSymbol();
                                lastSelectedInstruemnt = SharedMethods.getInstrumentFromExchangeKey(key);
                                buffer.append(exchange);
                                buffer.append(Meta.ID);
                                buffer.append(symbol + Constants.MARKET_SEPERATOR_CHARACTER + market.getSymbol());
                                buffer.append(Meta.ID);
                                buffer.append(getCompanyName(key) + " - " + market.getDescription());
                                buffer.append(Meta.ID);
                                buffer.append(lastSelectedInstruemnt);
                                buffer.append(Meta.ID);
                                buffer.append(Meta.FD);
                            } else if (SharedMethods.getSymbolFromExchangeKey(key).equals(symbol)) {
                                if ((lastSelectedSymbol == null) || !lastSelectedSymbol.equals(symbol)) {
                                    count++;
                                    lastSelectedExchange = exchangeCode;
                                    lastSelectedSymbol = symbol;
                                    lastSelectedInstruemnt = SharedMethods.getInstrumentFromExchangeKey(key);
//                                    lastSelectedMarket = null;
                                    buffer.append(exchange);
                                    buffer.append(Meta.ID);
                                    buffer.append(symbol);
                                    buffer.append(Meta.ID);
                                    buffer.append(getCompanyName(key));
                                    buffer.append(Meta.ID);
                                    buffer.append(lastSelectedInstruemnt);
                                    buffer.append(Meta.ID);
                                    buffer.append(Meta.FD);
//                                } else {
                                }
                            }
                        }
                    }
                }
                if (count > 0) {
                    break;
                }
            }

            if (count > 0) {
                if (lastSelectedMarket == null) {
                    return SharedMethods.getKey(lastSelectedExchange, lastSelectedSymbol, lastSelectedInstruemnt);
                } else {
                    return SharedMethods.getKey(lastSelectedExchange, lastSelectedSymbol + Constants.MARKET_SEPERATOR_CHARACTER + lastSelectedMarket, lastSelectedInstruemnt);// lastSelectedExchange + Constants.KEY_SEPERATOR_CHARACTER + lastSelectedSymbol + Constants.MARKET_SEPERATOR_CHARACTER + lastSelectedMarket;
                }
            } else {
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public void resetAllSymbolsStore() {
//        allSymbolsStore.clear();
//        allcount = 0;
        ArrayList<String> symbolList = new ArrayList<String>();
        Enumeration<String> exchanges = dataStore.keys();
        String exchange;
        String symbol;
        String data;
        int instrument = -1;
        while (exchanges.hasMoreElements()) {
            exchange = exchanges.nextElement();
            Hashtable<String, Stock> symbolsTable = dataStore.get(exchange);
            Enumeration<String> symbols = symbolsTable.keys();
            while (symbols.hasMoreElements()) {
                data = symbols.nextElement();
                symbol = SharedMethods.getSymbolFromExchangeKey(data);
                instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                if (!symbol.trim().equals("")) {
                    symbolList.add(SharedMethods.getKey(exchange, symbol, instrument));
                    allSymbolsStore.put(SharedMethods.getKey(exchange, symbol, instrument), "");
//                    allcount ++;
                }
                symbol = null;
            }
            symbols = null;
            symbolsTable = null;
            exchange = null;
        }
        exchanges = null;
    }

    public Enumeration<String> getAllSymbols() {
        return allSymbolsStore.keys();
    }

    public int getAllSymbolCount() {
        return allSymbolsStore.size();
    }

    public void setExpiredStatus(String exchange) {
        Enumeration<Stock> stocks = getExchangeSymbolStore(exchange).elements();

        while (stocks.hasMoreElements()) {
            Stock stock = stocks.nextElement();
            stock.setSymbolStatus(Meta.SYMBOL_STATE_EXPIRED);
        }

    }

    public FDTable getFDTable(String key) {
        FDTable fd = fd2Store.get(key);
        if (fd == null) {
            fd = new FDTable();
            fd2Store.put(key, fd);
        }
        return fd;
    }

    public void resetNewsBulbs() {
        try {
            Enumeration allSymbols = getAllSymbols();
            while (allSymbols.hasMoreElements()) {
                Stock stock = getStockObject((String) allSymbols.nextElement());
                /*if (stock.getNewsTimeoutPeriod() <= 0) {
                    stock.setNewsAvailability(Constants.ITEM_NOT_AVAILABLE);
                } else {
                    stock.decrementTimeUnit();
                }*/
                if (Settings.getNewsIndicatorExpiryTime() != -1) {
                    if (stock.getNewsTimeoutPeriod() >= (Settings.getNewsIndicatorExpiryTime() * 60)) {
                        stock.setNewsAvailability(Constants.ITEM_NOT_AVAILABLE);
                        stock.setNewsTimeoutPeriod(0);
                    } else {
                        stock.incrementTimeUnit();
                    }
                } else {
                    stock.incrementTimeUnit();
                }

            }
        } catch (Exception e) {

            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.

        }
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationExiting() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        Hashtable<String, Hashtable<String, Stock>> tempTable;
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/snapshot.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            tempTable = (Hashtable<String, Hashtable<String, Stock>>) oObjIn.readObject();
            oIn.close();
            Enumeration<String> keys = tempTable.keys();
//            Enumeration<String> symbolKeys = null;
            String key;
//            String symbolKey;
//            Hashtable<String, Stock> data;
//            Hashtable<String, Stock> exchangeStore;
            while (keys.hasMoreElements()) {
                key = keys.nextElement();
                dataStore.put(key, tempTable.get(key));
                /*if(!dataStore.containsKey(key)){
                    dataStore.put(key,tempTable.get(key));
                } else {
                    data = tempTable.get(key);
                    symbolKeys = data.keys();
                    while(symbolKeys.hasMoreElements()){
                        symbolKey = symbolKeys.nextElement();
                        exchangeStore = dataStore.get(key);
                        exchangeStore.put(key,data.get(symbolKey));
                    }
                }*/
            }
//                resetAllSymbolsStore();
        } catch (Exception e) {
            e.printStackTrace();
        }
        tempTable = null;
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/optionsnapshot.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            tempTable = (Hashtable<String, Hashtable<String, Stock>>) oObjIn.readObject();
            oIn.close();
            Enumeration<String> keys = tempTable.keys();
//            Enumeration<String> symbolKeys = null;
            String key;
//            String symbolKey;
//            Hashtable<String, Stock> data;
//            Hashtable<String, Stock> exchangeStore;
            while (keys.hasMoreElements()) {
                key = keys.nextElement();
                baseSymbolDataStore.put(key, tempTable.get(key));
                /*if(!dataStore.containsKey(key)){
                    dataStore.put(key,tempTable.get(key));
                } else {
                    data = tempTable.get(key);
                    symbolKeys = data.keys();
                    while(symbolKeys.hasMoreElements()){
                        symbolKey = symbolKeys.nextElement();
                        exchangeStore = dataStore.get(key);
                        exchangeStore.put(key,data.get(symbolKey));
                    }
                }*/
            }
//                resetAllSymbolsStore();
        } catch (Exception e) {
            e.printStackTrace();
        }

        resetAllSymbolsStore();
        UpdateNotifier.setShapshotUpdated();
        Enumeration<String> exchanges = dataStore.keys();
        Enumeration<Stock> stocks;
        String exchange;
        Stock stock;
        Hashtable<String, Stock> exchangeStore;
        while (exchanges.hasMoreElements()) {
            exchange = exchanges.nextElement();
            if (ExchangeStore.getSharedInstance().isDefault(exchange)) {
                exchangeStore = dataStore.get(exchange);
                stocks = exchangeStore.elements();
                while (stocks.hasMoreElements()) {
                    try {
                        stock = stocks.nextElement();
                        if ((stock.getLastTradeValue() > 0) && (stock.getInstrumentType() != Meta.INSTRUMENT_INDEX)) {
                            Client.getInstance().getTodaysTrades().insertSymbol(stock.getExchange(), stock.getSymbol(), stock.getMarketID(), stock.getInstrumentType());
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        }
        UpdateNotifier.setShapshotUpdated();
    }


    public void selectedExchangeChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int requestCount() {
        return requestRegister.size();
    }

    public synchronized String getSequenceID() {
        autoIncrementID = autoIncrementID + 1;
        String requestID = "MainBoard:" + System.currentTimeMillis() + autoIncrementID;
        return requestID;

    }

    public void sendOfflineRequest() {
        if (offlineValidatedReqs.size() > 0) {
            for (int i = 0; i < offlineValidatedReqs.size(); i++) {
                new ContentCMConnector((String) offlineValidatedReqs.get(i), null, Constants.CONTENT_PATH_SECONDARY);
            }

        }

    }

    public Stock getStockObjectFromISINCode(String exchange, String isinCode) {
        Stock stock = null;
        Hashtable exchangetable = dataStore.get(exchange);
        Enumeration<Stock> enu = exchangetable.elements();
        while (enu.hasMoreElements()) {
            stock = enu.nextElement();
            if (stock.getISINCode().equalsIgnoreCase(isinCode)) {
                return stock;
            }
        }
        return null;
    }
    public void checkSymbolAvailability(String basesymbol, String referenceID, String exchange, int instrumentType) {
        if ((exchange == null) || (exchange.equals(""))) {
            SendQFactory.addValidateRequest(basesymbol, referenceID, exchange, instrumentType);
        } else {
            try {
                if (ExchangeStore.getSharedInstance().getExchange(exchange).isMarketFileAvailable()) {
                    if (ExchangeStore.getSharedInstance().getExchange(exchange).getMasterFileStaus() == Meta.FULL_EXCG_FILE_AVAILABLE && !findSymbolOnFile(basesymbol, exchange, instrumentType)) {
                        SharedMethods.showMessage("No symbol", JOptionPane.INFORMATION_MESSAGE);
                    } else if (ExchangeStore.getSharedInstance().getExchange(exchange).getMasterFileStaus() == Meta.PART_EXCG_FILE_AVAILABLE && !findSymbolOnFile(basesymbol, exchange, instrumentType)) {
                        SendQFactory.addValidateRequest(basesymbol, referenceID, exchange, instrumentType);
                    }
                } else {
                    SendQFactory.addValidateRequest(basesymbol, referenceID, exchange, instrumentType);
                }
            } catch (Exception e) {
                SendQFactory.addValidateRequest(basesymbol, referenceID, exchange, instrumentType);
            }

        }
    }
    public boolean checkSymbolAvailabilityForBulkValidation(String basesymbol, String exchange, int instrumentType) {
        boolean symbolNotFound = false;
        if (ExchangeStore.getSharedInstance().getExchange(exchange).isMarketFileAvailable()) {
            if (ExchangeStore.getSharedInstance().getExchange(exchange).getMasterFileStaus() == Meta.FULL_EXCG_FILE_AVAILABLE && !findSymbolOnFile(basesymbol, exchange, instrumentType)) {
                symbolNotFound = true;
            } else if (ExchangeStore.getSharedInstance().getExchange(exchange).getMasterFileStaus() == Meta.PART_EXCG_FILE_AVAILABLE && !findSymbolOnFile(basesymbol, exchange, instrumentType)) {
                symbolNotFound = true;
            }
        } else {
            symbolNotFound = true;
        }
        return symbolNotFound;
    }

    public boolean findSymbolOnFile(String basesymbol, String exchange, int instrumentType) {
        boolean symboleAvailable = false;

        /* if (ExchangeStore.getSharedInstance().getExchange(exchange).getMasterFileType() == Meta.FILE_TYPE_MSF) {

              SmartProperties stockMaster = new SmartProperties(Meta.DS);
              String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/market_" + Language.getSelectedLanguage() + ".msf";
          try {
                  stockMaster.loadCompressed(sLangSpecFileName);
              } catch (IOException e) {
              e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
          }

          try {
                  stockMaster.remove("VERSION");
              } catch (Exception e) {

              }

              //Read File Line By Line
              Enumeration exchageSymbols = stockMaster.keys();
              while (exchageSymbols.hasMoreElements()) {
                  String symbol = (String) exchageSymbols.nextElement();
                  String insType;
                      String shortDec = null;
                      String category = null;
                      String currency = null;

                      try {
                      insType = ((String) stockMaster.get(symbol)).split(Meta.FS)[0];
                      shortDec = ((String) stockMaster.get(symbol)).split(Meta.FS)[2];
                      category = ((String) stockMaster.get(symbol)).split(Meta.FS)[1];
                      currency = ((String) stockMaster.get(symbol)).split(Meta.FS)[4];
                      } catch (NumberFormatException e) {
                          shortDec = "";
                          category = "";
                      insType = "";
                      }
                      if (symbol.equals(basesymbol) && Integer.parseInt(insType) == instrumentType) {
                          String record = exchange + Meta.ID + symbol + Meta.ID +
                                          insType + Meta.ID + category + Meta.ID + shortDec +
                                          Meta.ID + "" + Meta.ID + currency + Meta.ID + symbol + Meta.ID + symbol;
                          createStockObject(exchange, basesymbol, instrumentType).setSymbolEnabled(true);
                          ValidatedSymbols.getSharedInstance().addSymbol(SharedMethods.getKey(exchange, basesymbol, instrumentType), record);
                          addSymbolRequest(exchange, basesymbol, instrumentType);
                          symboleAvailable = true;
                          break;
                      }
                  }
              return symboleAvailable;

  //               return false;


        }*/  // else if (ExchangeStore.getSharedInstance().getExchange(exchange).getMasterFileType() == Meta.FILE_TYPE_CSV) {
        String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/market_" + Language.getSelectedLanguage() + ".csv";

        FileInputStream fstream = null;
        try {
            fstream = new FileInputStream(sLangSpecFileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        //Read File Line By Line
        try {
            while ((strLine = br.readLine()) != null) {
                //                                    String ssymbol = (String) exchageSymbols.nextElement();
                String ssymbol = strLine;
                String symbol;
                String insType;

                String iType = ((String) ssymbol).split(Meta.FS)[0];
                symbol = iType.split(Meta.DS)[0];
                insType = iType.split(Meta.DS)[1];
                if (!symbol.equals("VERSION")) {

                    String shortDec = null;
                    String category = null;
                    Byte decimalCount = null;
                    String currency = null;

                    try {
                        category = (((String) ssymbol).split(Meta.FS)[1]);
                        shortDec = (((String) ssymbol).split(Meta.FS))[2];
                        currency = (((String) ssymbol).split(Meta.FS))[4];
                        decimalCount = Byte.parseByte(((String) ssymbol).split(Meta.FS)[9]);
                    } catch (NumberFormatException e) {
                        decimalCount = 2;
                        shortDec = "";
                        category = "";
                    }
                    if (symbol.equals(basesymbol) && Integer.parseInt(insType) == instrumentType) {
                        String record = exchange + Meta.ID + symbol + Meta.ID +
                                insType + Meta.ID + category + Meta.ID + shortDec +
                                Meta.ID + "" + Meta.ID + currency + Meta.ID + symbol + Meta.ID + symbol;
                        createStockObject(exchange, basesymbol, instrumentType).setSymbolEnabled(true);
                        ValidatedSymbols.getSharedInstance().addSymbol(SharedMethods.getKey(exchange, basesymbol, instrumentType), record);
                        addSymbolRequest(exchange, basesymbol, instrumentType);
                        symboleAvailable = true;
                        break;
                    }
                }
            }

//               return false;
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
//        }
        return symboleAvailable;
    }

    public boolean isSymbolExistOnFile(String basesymbol, String exchange, int instrumentType) {
        boolean symboleAvailable = false;
        String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/market_" + Language.getSelectedLanguage() + ".csv";

        FileInputStream fstream = null;
        try {
            fstream = new FileInputStream(sLangSpecFileName);
        } catch (FileNotFoundException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        //Read File Line By Line
        try {
            while ((strLine = br.readLine()) != null) {
                String ssymbol = strLine;
                String symbol;
                String insType;

                String iType = ((String) ssymbol).split(Meta.FS)[0];
                symbol = iType.split(Meta.DS)[0];
                insType = iType.split(Meta.DS)[1];
                if (!symbol.equals("VERSION")) {

                    String shortDec = null;
                    String category = null;
                    Byte decimalCount = null;
                    String currency = null;

                    try {
                        category = (((String) ssymbol).split(Meta.FS)[1]);
                        shortDec = (((String) ssymbol).split(Meta.FS))[2];
                        currency = (((String) ssymbol).split(Meta.FS))[4];
                        decimalCount = Byte.parseByte(((String) ssymbol).split(Meta.FS)[9]);
                    } catch (NumberFormatException e) {
                        decimalCount = 2;
                        shortDec = "";
                        category = "";
                    }
                    if (symbol.equals(basesymbol) && Integer.parseInt(insType) == instrumentType) {
                        symboleAvailable = true;
                        break;
                    }
                }
            }

//               return false;
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return symboleAvailable;
    }

}
    


