package com.isi.csvr.datastore;

// Copyright (c) 2000 Home


public class GDRRecord extends Object {

    private String currency = null;
    private String symbol = null;
    private double bid = 0;
    private double offer = 0;
    private double price = 0;
    private double change = 0;
    private double pChange = 0;
    private double high = 0;
    private double low = 0;
    private long volume = 0;
    private String time = null;
    private double close = 0;

    /**
     * Constructor
     */
    public GDRRecord() {
    }

    public double getBid() {
        return bid;
    }

    public void setBid(String newBid) throws Exception {
        bid = Double.parseDouble(newBid);
    }

    public double getChange() {
        return change;
    }

    public void setChange(String newChange) throws Exception {
        change = Double.parseDouble(newChange);
    }

    public double getClose() {
        return close;
    }

    public void setClose(String newClose) throws Exception {
        close = Double.parseDouble(newClose);
    }

    public String getCurrency() {
        return currency;
    }

    public void setCurrency(String newCurrency) {
        currency = newCurrency;
    }

    public double getHigh() {
        return high;
    }

    public void setHigh(String newHigh) throws Exception {
        high = Double.parseDouble(newHigh);
    }

    public double getLow() {
        return low;
    }

    public void setLow(String newLow) throws Exception {
        low = Double.parseDouble(newLow);
    }

    public double getOffer() {
        return offer;
    }

    public void setOffer(String newOffer) throws Exception {
        offer = Double.parseDouble(newOffer);
    }

    public double getPChange() {
        return pChange;
    }

    public void setPChange(String newPChange) throws Exception {
        pChange = Double.parseDouble(newPChange);
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(String newPrice) throws Exception {
        price = Double.parseDouble(newPrice);
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String newSymbol) {
        symbol = newSymbol;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String newTime) {
        time = newTime;
    }

    public long getVolume() throws Exception {
        return volume;
    }

    public void setVolume(String newVolume) {
        volume = Long.parseLong(newVolume);
    }

    public String toString() {
        return symbol + "," + bid + "," + offer + "," + price + "," + change + "," +
                pChange + "," + high + "," + low + "," + volume + "," + time + "," + close;
    }

}


