/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 17, 2005
 * Time: 4:52:05 PM
 */
package com.isi.csvr.datastore;

import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;

import java.util.Collections;
import java.util.*;
import java.io.*;

import static java.util.Arrays.sort;

public class TimeZoneMap {
//    private TimeZone activeTimeZone;
    private static TimeZoneMap self = new TimeZoneMap();
    private SmartProperties zones;

    public synchronized static TimeZoneMap getInstance() {
        return self;
    }

    private TimeZoneMap() {
        loadZoneDetails();
        self = this;
    }

    /*public synchronized void addTimeItem(String exchange, String zoneID, String description) {

        ZoneRecord zoneRecord = new ZoneRecord(exchange, zoneID, description);
        int index = Collections.binarySearch(table, zoneRecord, this);
        if (index >= 0){
            table.remove(index);
        }
        table.add(zoneRecord);
        Collections.sort(table, this);
        zoneRecord = null;
    }*/

    public void loadZoneDetails() {

        try {
            zones = new SmartProperties(Meta.DS);
            String sLangSpecFileName =  Settings.SYSTEM_PATH +  "/timezones_" + Language.getSelectedLanguage() +".msf";
            zones.loadCompressed(sLangSpecFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public String getZoneDescription(String id) {
        return Language.getLanguageSpecificString(zones.getProperty(id));
    }

    public Enumeration getZoneIDs() {
        return zones.keys();
    }

    /*
    added by madhawie to get the sorted Zone Ids
     */
    public ArrayList getSortedZoneIds() {
        Map<String, Double> tempIds = new HashMap<String, Double>();
        Enumeration zoneIDs = getZoneIDs();
        ArrayList<String> sortedZoneIDs = new ArrayList<String>();
        int hour, minute;
        double sorter;
        while (zoneIDs.hasMoreElements()) {
            String zoneID = (String) zoneIDs.nextElement();
            hour = (TimeZone.getTimeZone(zoneID).getRawOffset() / (60 * 60 * 1000));
            minute = Math.abs(TimeZone.getTimeZone(zoneID).getRawOffset() / (60 * 1000)) % 60;
            sorter = hour + minute / 60;
            tempIds.put(TimeZone.getTimeZone(zoneID).getID(), sorter);
        }

        List<String> mapKeys = new ArrayList<String>(tempIds.keySet());
        List<Double> mapValues = new ArrayList<Double>(tempIds.values());
        HashMap<String, Double> sortedMap = new LinkedHashMap<String, Double>();
        TreeSet<Double> sortedSet = new TreeSet<Double>(mapValues);
        Object[] sortedArray = sortedSet.toArray();
        int size = sortedArray.length;
        for (int i = 0; i < size; i++) {
            sortedMap.put(mapKeys.get(mapValues.indexOf(sortedArray[i])),
                    (Double) sortedArray[i]);
        }
        for (String s : sortedMap.keySet()) {
            sortedZoneIDs.add(s);
        }
        return sortedZoneIDs;
    }

    /*
    * added by madhawie - Return the GMT time format for a given zoneId
    * */
    public String getGMTTimeFormat(String zoneId) {
        TimeZone tz = TimeZone.getTimeZone(zoneId);
        int rawOffset = tz.getRawOffset();
        String hour = "" + (rawOffset / (60 * 60 * 1000));
        String minute = "" + Math.abs(rawOffset / (60 * 1000)) % 60;
        if (hour.length() < 2) {
            hour = "0" + hour;
        }
        if (minute.length() < 2) {
            minute += "0";
        }
        if (rawOffset / (60 * 60 * 1000) > 0) {
            hour = "+" + hour;
        }

        String timeZone = "\t"+"GMT" + hour + ":" + minute;
        return timeZone;
    }

    /*public void setActiveTimeZone(String activeTimeZone) {
        try {
            this.activeTimeZone = TimeZone.getTimeZone(activeTimeZone);
        } catch (Exception e) {
            this.activeTimeZone = null;
        }
    }*/

    /*public long getZoneAdjustedTime(long date) {
        try {
            return date + activeTimeZone.getOffset(date);
        } catch (Exception e) {
            return date;
        }

    }

    public long getZoneAdjustment(long date) {
        try {
            return activeTimeZone.getOffset(date);
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }

    }*/
}
