// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.datastore;

/**
 * A Class class.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class AssetClasses extends Object {

    private String[] g_asSymbols;
    //private String[] g_asFilteredSymbols;
    //private String   g_sFilter = null;

    /**
     * Constructor
     */
    public AssetClasses() {
        g_asSymbols = new String[0];
    }

    public void setSymbols(String[] asSymbols) {
        g_asSymbols = asSymbols;
        //applyFilter();
    }

    /**
     * Sets the filter to the symbol map
     *
     public void setFilter(String sFilter)
     {

     g_sFilter = sFilter;
     applyFilter();
     }*/

    /**
     * Apply the filter to the symbol map
     *
     private void applyFilter()
     {
     if (g_sFilter == null)
     {
     g_asFilteredSymbols =g_asSymbols;
     }
     else
     {
     g_asFilteredSymbols = new String[g_asSymbols.length];

     int j=0;
     for(int i = 0;i<g_asSymbols.length;i++)
     {
     //System.out.println (g_sFilter + " " + g_asSymbols[i] + " " + IndexStore.getIndex(g_asSymbols[i]).getType());
     if (g_sFilter.equals(IndexStore.getIndex(g_asSymbols[i]).getType()))
     {
     //System.out.println (g_sFilter + " " + g_asSymbols[i] + " " + IndexStore.getIndex(g_asSymbols[i]).getType());
     g_asFilteredSymbols[j] = g_asSymbols[i];
     //System.out.println (g_asSymbols[j]);
     j++;
     }
     }
     String[] asSelected = new String[j];
     System.arraycopy(g_asFilteredSymbols,0,asSelected,0,j);
     g_asFilteredSymbols = asSelected;

     /*for(int i = 0;i<g_asFilteredSymbols.length;i++)
     {
     System.out.println ("Fil " + g_asFilteredSymbols[i]);
     } *
     }
     }*/

    /**
     * Returns the symbols array
     */
    public String[] getSymbols() {
        //System.out.println(this);
        return g_asSymbols;
    }

    /**
     * Returns the filtered symbols array
     *
     public String[] getFilteredSymbols()
     {
     //System.out.println(this);
     return g_asFilteredSymbols;
     }*/

    /**
     * Appends a new symbol to the symbol list
     */
    public void appendSymbol(String sSymbol) {

        /* do no append if the symbol is already there*/
        if (isAlreadyIn(sSymbol)) return;
        int i = 0;
        String[] asData = new String[g_asSymbols.length + 1];
        while (i < g_asSymbols.length) {
            asData[i] = g_asSymbols[i];
            i++;
        }
        asData[i] = sSymbol;
        g_asSymbols = asData;
        //applyFilter();
    }

    /**
     * Checks if the symbol is already there.
     */
    private boolean isAlreadyIn(String sNewSymbol) {
        int i = 0;
        while (i < g_asSymbols.length) {
            if (g_asSymbols[i].equals(sNewSymbol))
                return true;
            i++;
        }
        return false;
    }

    /**
     * Remove a symbol from the list
     */
    public void removeSymbol(String sSymbol) {

        int i = 0;
        int iPos = 0;
        boolean bFound = false;

        /* Check if available */
        while (i < g_asSymbols.length) {
            if (g_asSymbols[i].equals(sSymbol)) {
                bFound = true;
                iPos = i;
                break;
            }
            i++;
        }

        /* if the symbol is available, remove it */
        if (bFound) {
            String[] asData = new String[g_asSymbols.length - 1];
            i = 0;
            while (i < iPos) {
                asData[i] = g_asSymbols[i];
                i++;
            }

            while (i < g_asSymbols.length - 1) {
                asData[i] = g_asSymbols[i + 1];
                i++;
            }
            g_asSymbols = asData;
        }
        //applyFilter();
    }

    /**
     * Returns the symbols string
     */
    public String toString() {
        String sSymbols = "";

        //for (int i=0;i<g_asFilteredSymbols.length;i++)
        //    sSymbols += ("," + g_asFilteredSymbols[i]);

        if (sSymbols.length() == 0)
            return sSymbols;
        else
            return sSymbols.substring(1);
    }
}

