package com.isi.csvr.datastore;

import com.isi.csvr.shared.*;
import java.util.StringTokenizer;


public class FDTable {


    public String symbol = null;
    public String exchange = null;
    public String exchangeCode = null;
    public int instrumentType = 0;

    public float beta = 0;//0
    public String businessDescription = null;
    public String leaps = null;
    public String yearForDividends = null;
    public float[] dividends = null;
    public String epsDateDiluted = null;
    public float percentageHeldByInst = 0;
    public float peRatio = 0;//0
    public String sp500Indicator = null;
    public String wraps = null;
    public float[] splitFactors = null;
    public String[] splitFactorDates = null;
    public float last12MonthsEpsDiluted = 0;//0
    public float bookValuePerShare = 0;
    public float adrRatio = 0;
    public float calYearHigh = 0;
    public float calYearLow = 0;
    public String calYearHighDate = null;
    public String calYearLowDate = null;
    public String companyName = null;
    public float divRate = 0;
    public float divAmount = 0;
    public String divPayDate = null;
    public String exDivDate = null;
    public float marketCap = 0;
    public String optionSymbols = null;
    public float fd52wkHigh = 0;
    public float fd52wkLow = 0;
    public String spRank = null;
    public float divYeild = 0;
    public String fd52wkHighDate = null;
    public String fd52wkLowDate = null;
    public float historicalVolit = 0;

    public String divRecordDate = null;       // Added on 20080507 by amilaj
    public int fd30DayAvgVolume = 0; //0
    public String last12MonthsEpsUndilutedDate = null;
    public float last12MonthsEpsUndiluted = 0;//0
    public int sharesOutstanding = 0;
    public String fiscalyearEnding1 = null;
    public String currencycodeFye1 = null;
    public int currentAssets = 0;
    public int currentLiabilities = 0;
    public int longTermDebt = 0;
    public int revenueFye1 = 0;
    public int netIncomeFye1 = 0;
    public float epsFye1 = 0;
    public String fiscalYearEnding2 = null;
    public String currencyCodeFye2 = null;
    public int revenueFye2 = 0;
    public float epsFye2 = 0;
    public float profitMarginFye1 = 0;
    public float operatingMarginFye1 = 0;
    public float fd5yearDivYeildFye1 = 0;
    public float pctHeldByInsiders = 0;


    public int decimalCorrFactor = 1;

    public FDTable() {
        dividends = new float[4];
        splitFactors = new float[3];
        splitFactorDates = new String[3];
        symbol = null;
        beta = 0;//0
        businessDescription = null;
        leaps = null;
        yearForDividends = null;
        dividends[0] = 0;
        dividends[1] = 0;
        dividends[2] = 0;
        dividends[3] = 0;
        epsDateDiluted = null;
        percentageHeldByInst = 0;
        peRatio = 0;//0
        sp500Indicator = null;
        wraps = null;
        splitFactors[0] = 0;
        splitFactors[1] = 0;
        splitFactors[2] = 0;
        splitFactorDates[0] = "";
        splitFactorDates[1] = "";
        splitFactorDates[2] = "";
        last12MonthsEpsDiluted = 0;//0
        bookValuePerShare = 0;
        adrRatio = 0;
        calYearHigh = 0;
        calYearLow = 0;
        calYearHighDate = null;
        calYearLowDate = null;
        companyName = null;
        divRate = 0;
        divAmount = 0;
        divPayDate = null;
        exDivDate = null;
        marketCap = 0;
        optionSymbols = null;
        fd52wkHigh = 0;
        fd52wkLow = 0;
        spRank = null;
        divYeild = 0;
        fd52wkHighDate = null;
        fd52wkLowDate = null;
        historicalVolit = 0;

        divRecordDate = null;       // Added on 20080507 by amilaj
        fd30DayAvgVolume = 0;  //0
        last12MonthsEpsUndilutedDate = null;
        last12MonthsEpsUndiluted = 0;//0
        sharesOutstanding = 0;
        fiscalyearEnding1 = null;
        currencycodeFye1 = null;
        currentAssets = 0;
        currentLiabilities = 0;
        longTermDebt = 0;
        revenueFye1 = 0;
        netIncomeFye1 = 0;
        epsFye1 = 0;
        fiscalYearEnding2 = null;
        currencyCodeFye2 = null;
        revenueFye2 = 0;
        epsFye2 = 0;
        profitMarginFye1 = 0;
        operatingMarginFye1 = 0;
        fd5yearDivYeildFye1 = 0;
        pctHeldByInsiders = 0;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getExchangeCode() {
        return exchangeCode;
    }

    public void setExchangeCode(String exchangeCode) {
        this.exchangeCode = exchangeCode;
    }

    public int getInstrumentType() {
        return instrumentType;
    }

    public void setInstrumentType(int instrumentType) {
        this.instrumentType = instrumentType;
    }

    public float getBeta() {
        return beta;
    }

    public void setBeta(float beta) {
        this.beta = beta;
    }

    public String getBusinessDescription() {
        return businessDescription;
    }

    public void setBusinessDescription(String businessDescription) {
        this.businessDescription = businessDescription;
    }

    public String getLeaps() {
        return leaps;
    }

    public void setLeaps(String leaps) {
        this.leaps = leaps;
    }

    public String getYearForDividends() {
        return yearForDividends;
    }

    public void setYearForDividends(String yearForDividends) {
        this.yearForDividends = yearForDividends;
    }

    public float[] getDividends() {
        return dividends;
    }

    public void setDividends(String frame,int decimalFactor) {
        String[] arr=frame.split("\\|");
        dividends[0] = Float.parseFloat(arr[0]);///decimalFactor;
        dividends[1] = Float.parseFloat(arr[1]);///decimalFactor;
        dividends[2] = Float.parseFloat(arr[2]);///decimalFactor;
        dividends[3] = Float.parseFloat(arr[3]);///decimalFactor;
    }

    public String getEpsDateDiluted() {
        return epsDateDiluted;
    }

    public void setEpsDateDiluted(String epsDateDiluted) {
        this.epsDateDiluted = epsDateDiluted;
    }

    public float getPercentageHeldByInst() {
        return percentageHeldByInst;
    }

    public void setPercentageHeldByInst(float percentageHeldByInst) {
        this.percentageHeldByInst = percentageHeldByInst;
    }

    public float getPeRatio() {
        return peRatio;
    }

    public void setPeRatio(float peRatio) {
        this.peRatio = peRatio;
    }

    public String getSp500Indicator() {
        return sp500Indicator;
    }

    public void setSp500Indicator(String sp500Indicator) {
        this.sp500Indicator = sp500Indicator;
    }

    public String getWraps() {
        return wraps;
    }

    public void setWraps(String wraps) {
        this.wraps = wraps;
    }

    public float[] getSplitFactors() {
        return splitFactors;
    }

    public void setSplitFactors(String frame,int decimalFactor) {
        String[] arr=frame.split("\\|");
        splitFactors[0] = Float.parseFloat(arr[0]);///decimalFactor;
        splitFactors[1] = Float.parseFloat(arr[1]);///decimalFactor;
        splitFactors[2] = Float.parseFloat(arr[2]);//decimalFactor;

    }

    public String[] getSplitFactorDates() {
        return splitFactorDates;
    }

    public void setSplitFactorDates(String frame) {
         String[] arr=frame.split("\\|");
        splitFactorDates[0] = arr[0];
        splitFactorDates[1] = arr[1];
        splitFactorDates[2] = arr[2];
    }

    public float getLast12MonthsEpsDiluted() {
        return last12MonthsEpsDiluted;
    }

    public void setLast12MonthsEpsDiluted(float last12MonthsEpsDiluted) {
        this.last12MonthsEpsDiluted = last12MonthsEpsDiluted;
    }

    public float getBookValuePerShare() {
        return bookValuePerShare;
    }

    public void setBookValuePerShare(float bookValuePerShare) {
        this.bookValuePerShare = bookValuePerShare;
    }

    public float getAdrRatio() {
        return adrRatio;
    }

    public void setAdrRatio(float adrRatio) {
        this.adrRatio = adrRatio;
    }

    public float getCalYearHigh() {
        return calYearHigh;
    }

    public void setCalYearHigh(float calYearHigh) {
        this.calYearHigh = calYearHigh;
    }

    public float getCalYearLow() {
        return calYearLow;
    }

    public void setCalYearLow(float calYearLow) {
        this.calYearLow = calYearLow;
    }

    public String getCalYearHighDate() {
         if((calYearHighDate== null) || (calYearHighDate.equals("null"))){
            return "";
        }
        return calYearHighDate;
    }

    public void setCalYearHighDate(String calYearHighDate) {
        this.calYearHighDate = calYearHighDate;
    }

    public String getCalYearLowDate() {
         if((calYearLowDate== null) || (calYearLowDate.equals("null"))){
            return "";
        }
        return calYearLowDate;
    }

    public void setCalYearLowDate(String calYearLowDate) {
        this.calYearLowDate = calYearLowDate;
    }

    public String getCompanyName() {
        return companyName;
    }

    public void setCompanyName(String companyName) {
        this.companyName = companyName;
    }

    public float getDivRate() {
        return divRate;
    }

    public void setDivRate(float divRate) {
        this.divRate = divRate;
    }

    public float getDivAmount() {
        return divAmount;
    }

    public void setDivAmount(float divAmount) {
        this.divAmount = divAmount;
    }

    public String getDivPayDate() {
        if((divPayDate== null) || (divPayDate.equals("null"))){
            return "";
        }
        return divPayDate;
    }

    public void setDivPayDate(String divPayDate) {
        this.divPayDate = divPayDate;
    }

    public String getExDivDate() {
        if((exDivDate == null) || (exDivDate.equals("null"))){
            return "";
        }
        return exDivDate;
    }

    public void setExDivDate(String exDivDate) {
        this.exDivDate = exDivDate;

    }

    public float getMarketCap() {
        return marketCap;
    }

    public void setMarketCap(float marketCap) {
        this.marketCap = marketCap;
    }

    public String getOptionSymbols() {
        return optionSymbols;
    }

    public void setOptionSymbols(String optionSymbols) {
        this.optionSymbols = optionSymbols;
    }

    public float getFd52wkHigh() {
        return fd52wkHigh;
    }

    public void setFd52wkHigh(float fd52wkHigh) {
        this.fd52wkHigh = fd52wkHigh;
    }

    public float getFd52wkLow() {
        return fd52wkLow;
    }

    public void setFd52wkLow(float fd52wkLow) {
        this.fd52wkLow = fd52wkLow;
    }

    public String getSpRank() {
        return spRank;
    }

    public void setSpRank(String spRank) {
        this.spRank = spRank;
    }

    public float getDivYeild() {
        return divYeild;
    }

    public void setDivYeild(float divYeild) {
        this.divYeild = divYeild;
    }

    public String getFd52wkHighDate() {
        if((fd52wkHighDate == null) || (fd52wkHighDate.equals("null"))){
            return "";
        }
        return fd52wkHighDate;
    }

    public void setFd52wkHighDate(String fd52wkHighDate) {
        this.fd52wkHighDate = fd52wkHighDate;
    }

    public String getFd52wkLowDate() {
        if((fd52wkLowDate == null) || (fd52wkLowDate.equals("null"))){
            return "";
        }
        return fd52wkLowDate;
    }

    public void setFd52wkLowDate(String fd52wkLowDate) {
        this.fd52wkLowDate = fd52wkLowDate;
    }

    public float getHistoricalVolit() {
        return historicalVolit;
    }

    public void setHistoricalVolit(float historicalVolit) {
        this.historicalVolit = historicalVolit;
    }

    public String getDivRecordDate() {
        if((divRecordDate == null) || (divRecordDate.equals("null"))){
            return "";
        }
        return divRecordDate;
    }

    public void setDivRecordDate(String divRecordDate) {
        this.divRecordDate = divRecordDate;
    }

    public int getFd30DayAvgVolume() {
        return fd30DayAvgVolume;
    }

    public void setFd30DayAvgVolume(int fd30DayAvgVolume) {
        this.fd30DayAvgVolume = fd30DayAvgVolume;
    }

    public String getLast12MonthsEpsUndilutedDate() {
        if((last12MonthsEpsUndilutedDate == null) || (last12MonthsEpsUndilutedDate.equals("null"))){
            return "";
        }
        return last12MonthsEpsUndilutedDate;
    }

    public void setLast12MonthsEpsUndilutedDate(String last12MonthsEpsUndilutedDate) {
        this.last12MonthsEpsUndilutedDate = last12MonthsEpsUndilutedDate;
    }

    public float getLast12MonthsEpsUndiluted() {
        return last12MonthsEpsUndiluted;
    }

    public void setLast12MonthsEpsUndiluted(float last12MonthsEpsUndiluted) {
        this.last12MonthsEpsUndiluted = last12MonthsEpsUndiluted;
    }

    public int getSharesOutstanding() {
        return sharesOutstanding;
    }

    public void setSharesOutstanding(int sharesOutstanding) {
        this.sharesOutstanding = sharesOutstanding;
    }

    public String getFiscalyearEnding1() {
        return fiscalyearEnding1;
    }

    public void setFiscalyearEnding1(String fiscalyearEnding1) {
        this.fiscalyearEnding1 = fiscalyearEnding1;
    }

    public String getCurrencycodeFye1() {
        return currencycodeFye1;
    }

    public void setCurrencycodeFye1(String currencycodeFye1) {
        this.currencycodeFye1 = currencycodeFye1;
    }

    public int getCurrentAssets() {
        return currentAssets;
    }

    public void setCurrentAssets(int currentAssets) {
        this.currentAssets = currentAssets;
    }

    public int getCurrentLiabilities() {
        return currentLiabilities;
    }

    public void setCurrentLiabilities(int currentLiabilities) {
        this.currentLiabilities = currentLiabilities;
    }

    public int getLongTermDebt() {
        return longTermDebt;
    }

    public void setLongTermDebt(int longTermDebt) {
        this.longTermDebt = longTermDebt;
    }

    public int getRevenueFye1() {
        return revenueFye1;
    }

    public void setRevenueFye1(int revenueFye1) {
        this.revenueFye1 = revenueFye1;
    }

    public int getNetIncomeFye1() {
        return netIncomeFye1;
    }

    public void setNetIncomeFye1(int netIncomeFye1) {
        this.netIncomeFye1 = netIncomeFye1;
    }

    public float getEpsFye1() {
        return epsFye1;
    }

    public void setEpsFye1(float epsFye1) {
        this.epsFye1 = epsFye1;
    }

    public String getFiscalYearEnding2() {
        return fiscalYearEnding2;
    }

    public void setFiscalYearEnding2(String fiscalYearEnding2) {
        this.fiscalYearEnding2 = fiscalYearEnding2;
    }

    public String getCurrencyCodeFye2() {
        return currencyCodeFye2;
    }

    public void setCurrencyCodeFye2(String currencyCodeFye2) {
        this.currencyCodeFye2 = currencyCodeFye2;
    }

    public int getRevenueFye2() {
        return revenueFye2;
    }

    public void setRevenueFye2(int revenueFye2) {
        this.revenueFye2 = revenueFye2;
    }

    public float getEpsFye2() {
        return epsFye2;
    }

    public void setEpsFye2(float epsFye2) {
        this.epsFye2 = epsFye2;
    }

    public float getProfitMarginFye1() {
        return profitMarginFye1;
    }

    public void setProfitMarginFye1(float profitMarginFye1) {
        this.profitMarginFye1 = profitMarginFye1;
    }

    public float getOperatingMarginFye1() {
        return operatingMarginFye1;
    }

    public void setOperatingMarginFye1(float operatingMarginFye1) {
        this.operatingMarginFye1 = operatingMarginFye1;
    }

    public float getFd5yearDivYeildFye1() {
        return fd5yearDivYeildFye1;
    }

    public void setFd5yearDivYeildFye1(float fd5yearDivYeildFye1) {
        this.fd5yearDivYeildFye1 = fd5yearDivYeildFye1;
    }

    public float getPctHeldByInsiders() {
        return pctHeldByInsiders;
    }

    public void setPctHeldByInsiders(float pctHeldByInsiders) {
        this.pctHeldByInsiders = pctHeldByInsiders;
    }

    public int getDecimalCorrFactor() {
        return decimalCorrFactor;
    }

    public void setDecimalCorrFactor(int decimalCorrFactor) {
        this.decimalCorrFactor = decimalCorrFactor;
    }

    public void clear() {

    }


    public void setData(String frame, int decimalFactor) {
        String[] oFields = frame.split(Meta.FD);
        String[] setData;
        for (int i = 0; i < oFields.length; i++) {
            try {
                switch (oFields[i].charAt(0)) {
                    case BATE_FD_BETA:
                        setBeta((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_BUSINESS_DESCRIPTION:
                        setBusinessDescription(oFields[i].substring(1));
                        break;
                    case BATE_FD_LEAPS:
                        setLeaps(oFields[i].substring(1));
                        break;
                    case BATE_FD_REF_YEAR_FOR_DIVIDENDS:
                        setYearForDividends(oFields[i].substring(1));
                        break;
                    case BATE_FD_SET_DIVIDEND:
                        setDividends(oFields[i].substring(1),decimalFactor);
                        break;
                    case BATE_EPS_DATE_DILUTED:
                        setEpsDateDiluted(oFields[i].substring(1));
                        break;
                    case BATE_FD_PERCENTAGE_HELD_BY_INST:
                        setPercentageHeldByInst( (float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_PE_RATIO:
                        setPeRatio((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_SP500_INDICATOR:
                        setSp500Indicator(oFields[i].substring(1));
                        break;
                    case BATE_FD_WRAPS:
                        setWraps( oFields[i].substring(1));
                        break;
                    case BATE_FD_SET_SPLIT_FACTOR:
                        setSplitFactors(oFields[i].substring(1),decimalFactor);
                        break;
                    case BATE_FD_SET_SPLIT_FACTOR_DATE:
                        setSplitFactorDates(oFields[i].substring(1));
                        break;
                    case BATE_LAST_12_MONTHS_EPS_DILUTED:
                        setLast12MonthsEpsDiluted((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_BOOK_VALUE_PER_SHARE_FYE1:
                        setBookValuePerShare((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_ADR_RATIO:
                        setAdrRatio((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_CAL_YEAR_HIGH:
                        setCalYearHigh((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_CAL_YEAR_LOW:
                        setCalYearLow((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_CAL_YEAR_HIGH_DATE:
                        setCalYearHighDate(oFields[i].substring(1));
                        break;
                    case BATE_FD_CAL_YEAR_LOW_DATE:
                        setCalYearLowDate(oFields[i].substring(1));
                        break;
                    case BATE_FD_COMPANY_NAME:
                        setCompanyName(oFields[i].substring(1));
                        break;
                    case BATE_FD_DIV_RATE:
                        setDivRate((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_DIV_AMOUNT:
                        setDivAmount((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_DIV_PAY_DATE:
                        setDivPayDate(oFields[i].substring(1));
                        break;
                    case BATE_FD_EX_DIV_DATE:
                        setExDivDate(oFields[i].substring(1));
                        break;
                    case BATE_FD_MARKET_CAP:
                        setMarketCap((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_OPTION_SYMBOL:
                        setOptionSymbols(oFields[i].substring(1));
                        break;
                    case BATE_FD_52WK_HIGH:
                        setFd52wkHigh((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_52WK_LOW:
                        setFd52wkLow((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_SP_RANK:
                        setSpRank(oFields[i].substring(1));
                        break;
                    case BATE_FD_DIV_YEILD:
                        setDivYeild((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_52WK_HIGH_DATE:
                        setFd52wkHighDate(oFields[i].substring(1));
                        break;
                    case BATE_FD_52WK_LOW_DATE:
                        setFd52wkLowDate(oFields[i].substring(1));
                        break;
                    case BATE_FD_HISTORICAL_VOLIT:
                        setHistoricalVolit((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_DIV_RECORD_DATE:
                        setDivRecordDate(oFields[i].substring(1));
                        break;
                    case BATE_30_DAY_AVG_VOLUME:
                        setFd30DayAvgVolume( (int) SharedMethods.getLong(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_LAST12_MONTHS_EPS_UNDILUTED_DATE:
                        setLast12MonthsEpsUndilutedDate( oFields[i].substring(1));
                        break;
                    case BATE_FD_LAST12_MONTHS_EPS_UNDILUTED:
                        setLast12MonthsEpsUndiluted((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_SHARES_OUTSTANDING:
                        setSharesOutstanding((int) SharedMethods.getLong(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_FISCALYEAR_ENDING_1:
                        setFiscalyearEnding1(oFields[i].substring(1));
                        break;
                    case BATE_FD_CURRENCYCODE_FYE1:
                        setCurrencycodeFye1(oFields[i].substring(1));
                        break;
                    case BATE_FD_CURRENT_ASSETS:
                        setCurrentAssets((int) SharedMethods.getLong(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_CURRENT_LIABILITIES:
                        setCurrentLiabilities((int) SharedMethods.getLong(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_LONG_TERM_DEBT:
                        setLongTermDebt((int) SharedMethods.getLong(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_REVENUE_FYE1:
                        setRevenueFye1((int) SharedMethods.getLong(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_NET_INCOME_FYE1:
                        setNetIncomeFye1((int) SharedMethods.getLong(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_EPS_FYE1:
                        setEpsFye1((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_FISCAL_YEAR_ENDING_2:
                        setFiscalYearEnding2(oFields[i].substring(1));
                        break;
                    case BATE_FD_CURRENCY_CODE_FYE2:
                        setCurrencyCodeFye2(oFields[i].substring(1));
                        break;
                    case BATE_FD_REVENUE_FYE2:
                        setRevenueFye2( (int) SharedMethods.getLong(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_EPS_FYE2:
                        setEpsFye2((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_PROFIT_MARGIN_FYE1:
                        setProfitMarginFye1((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_OPERATING_MARGIN_FYE1:
                        setOperatingMarginFye1((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_5_YEAR_DIV_YEILD_FYE1:
                        setFd5yearDivYeildFye1((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;
                    case BATE_FD_PCT_HELD_BY_INSIDERS:
                        setPctHeldByInsiders((float) SharedMethods.getDouble(oFields[i].substring(1))/decimalFactor);
                        break;


                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public static final char seperator = '|';
    public static final char BATE_FD_BETA = 'A';    //   float 6.5
    public static final char BATE_FD_BUSINESS_DESCRIPTION = 'B';    //	string 256
    public static final char BATE_FD_LEAPS = 'C';    //	string 60
    public static final char BATE_FD_REF_YEAR_FOR_DIVIDENDS = 'D';    //	only year ex: 2007
    public static final char BATE_FD_SET_DIVIDEND = 'E';    //	value1|value2|value3|value4 (float)
    public static final char BATE_EPS_DATE_DILUTED = 'F';    //	Date
    public static final char BATE_FD_PERCENTAGE_HELD_BY_INST = 'G';    //	float 3.2
    public static final char BATE_FD_PE_RATIO = 'H';    //	float 4.5
    public static final char BATE_FD_SP500_INDICATOR = 'I';    //	string 5
    public static final char BATE_FD_WRAPS = 'J';    //	string 60
    public static final char BATE_FD_SET_SPLIT_FACTOR = 'K';    //	value1|value2|value3
    public static final char BATE_FD_SET_SPLIT_FACTOR_DATE = 'L';    //	Date1|Date2|Date3
    public static final char BATE_LAST_12_MONTHS_EPS_DILUTED = 'M';    //	float 8.3
    public static final char BATE_FD_BOOK_VALUE_PER_SHARE_FYE1 = 'N';    //    float 11.6
    public static final char BATE_FD_ADR_RATIO = 'O';    //	float 5.5
    public static final char BATE_FD_CAL_YEAR_HIGH = 'P';    //	float 6.4
    public static final char BATE_FD_CAL_YEAR_LOW = 'Q';    //	float 6.4
    public static final char BATE_FD_CAL_YEAR_HIGH_DATE = 'R';    //	Date
    public static final char BATE_FD_CAL_YEAR_LOW_DATE = 'S';    //	Date
    public static final char BATE_FD_COMPANY_NAME = 'T';    //	string 60
    public static final char BATE_FD_DIV_RATE = 'U';    //	float 4.5
    public static final char BATE_FD_DIV_AMOUNT = 'V';    //	float 4.5
    public static final char BATE_FD_DIV_PAY_DATE = 'W';    //	Date
    public static final char BATE_FD_EX_DIV_DATE = 'X';    //	Date
    public static final char BATE_FD_MARKET_CAP = 'Y';    //	float 6.4
    public static final char BATE_FD_OPTION_SYMBOL = 'Z';    //    string 5
    public static final char BATE_FD_52WK_HIGH = 'a';    //	float 6.4
    public static final char BATE_FD_52WK_LOW = 'b';    //	float 6.4
    public static final char BATE_FD_SP_RANK = 'c';    //	string 5
    public static final char BATE_FD_DIV_YEILD = 'd';    //	float 3.5
    public static final char BATE_FD_52WK_HIGH_DATE = 'e';    //	Date
    public static final char BATE_FD_52WK_LOW_DATE = 'f';    //	Date

    public static final char BATE_FD_HISTORICAL_VOLIT = 'g';    //    float 3.4
    public static final char BATE_FD_DIV_RECORD_DATE = 'h';     //	Date
    public static final char BATE_30_DAY_AVG_VOLUME = 'i';    //    int  11
    public static final char BATE_FD_LAST12_MONTHS_EPS_UNDILUTED_DATE = 'j';    //	Date
    public static final char BATE_FD_LAST12_MONTHS_EPS_UNDILUTED = 'k';    //    float 8.3
    public static final char BATE_FD_SHARES_OUTSTANDING = 'l';    //    int 10
    public static final char BATE_FD_FISCALYEAR_ENDING_1 = 'm';    //    Date
    public static final char BATE_FD_CURRENCYCODE_FYE1 = 'n';    //    string 5
    public static final char BATE_FD_CURRENT_ASSETS = 'o';   //	int 16
    public static final char BATE_FD_CURRENT_LIABILITIES = 'p';    //    int 16
    public static final char BATE_FD_LONG_TERM_DEBT = 'q';    //	int 12
    public static final char BATE_FD_REVENUE_FYE1 = 'r';   //	int 18
    public static final char BATE_FD_NET_INCOME_FYE1 = 's';    //	int 15
    public static final char BATE_FD_EPS_FYE1 = 't';    //    float 8.4
    public static final char BATE_FD_FISCAL_YEAR_ENDING_2 = 'u';    //    Date
    public static final char BATE_FD_CURRENCY_CODE_FYE2 = 'v';    //    string 5
    public static final char BATE_FD_REVENUE_FYE2 = 'w';    //    int 18
    public static final char BATE_FD_EPS_FYE2 = 'x';    //    float 8.4
    public static final char BATE_FD_PROFIT_MARGIN_FYE1 = 'y';    //    float 8.4
    public static final char BATE_FD_OPERATING_MARGIN_FYE1 = 'z';    //    float 4.4
    public static final char BATE_FD_5_YEAR_DIV_YEILD_FYE1 = '0';    //    float 4.4

    public static final char BATE_FD_PCT_HELD_BY_INSIDERS = '1';    //    float 3.2

    //getters of new variables


}