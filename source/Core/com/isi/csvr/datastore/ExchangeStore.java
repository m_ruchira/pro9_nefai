package com.isi.csvr.datastore;

import com.isi.csvr.Client;
import com.isi.csvr.ExchangeMap;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.WorkInProgressIndicator;
import com.isi.csvr.plugin.*;
import com.isi.csvr.plugin.event.*;
import com.isi.csvr.topstocks.*;
import com.isi.csvr.brokers.Broker;
import com.isi.csvr.communication.tcp.TCPConnector;
import com.isi.csvr.communication.udp.FileListener;
import com.isi.csvr.communication.udp.Authenticator;
import com.isi.csvr.downloader.Downloader;
import com.isi.csvr.downloader.MarketDataDownloadManager;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.event.Application;
import com.isi.csvr.history.HistoryFilesManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolselector.CompanyTable;
import com.isi.csvr.util.Decompress;

import java.io.*;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 3, 2005
 * Time: 1:15:53 PM
 */
public class ExchangeStore {
    private List<Exchange> exchanges;
    private List<Exchange> defaultExchanges;
    private List<ExchangeListener> listeners;
    private static String selectedExchange;
    private static ExchangeStore self = null;
    private WorkInProgressIndicator workInProgressIndicator = null;
    private static Exchange applicationExchange;
    private Hashtable<String, ExchangeParams> exchangeParamsTable;
    private static boolean secondaryPathConnectionRequired;
    private static boolean noneDefaultAvailable;
    private static boolean defaultAvailable;
    private static boolean exchangeWatchListsExists = false;
    private static boolean canListenForStockMaster;
    private List<ExchangeInterface> allMarkets;
    private boolean loadingFailed = false;

    private static int fileDownloadCount = 0;
    private List<String> expiredExchanges;
    private List<String> inactiveExchanges;
    private static Hashtable<Byte, String> tradingWindows = new Hashtable<Byte, String>();
    private Hashtable<String,Hashtable> exchangeFeatures;


    public static ExchangeStore getSharedInstance() {
        if (self == null) {
            throw new RuntimeException("Exchange Store not initialized");
        }
        return self;
    }

    public static void initialize() {
        if (self == null) {
            self = new ExchangeStore();
        }
    }

    private ExchangeStore() {
        listeners = Collections.synchronizedList(new ArrayList<ExchangeListener>());

        allMarkets = Collections.synchronizedList(new ArrayList<ExchangeInterface>());
        expiredExchanges = new ArrayList<String>();
        inactiveExchanges = new ArrayList<String>();
        exchangeFeatures=new Hashtable<String,Hashtable>();
        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(Settings.getAbsolutepath()+"datastore/exchanges.mdf"));
            loadingFailed = false;
            exchanges = (List<Exchange>) in.readObject();
            try {
                applicationExchange = (Exchange) in.readObject();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            } catch (ClassNotFoundException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            in.close();
            in = null;

            defaultExchanges = Collections.synchronizedList(new ArrayList<Exchange>());
            for (Exchange exchange : exchanges) {
                if (exchange.isDefault()) {
                    defaultExchanges.add(exchange);
                }
            }
            loadExchangeDescriptions();
            rebuildAllMarkets();
            loadExchangeIndicesWatchLists();
            loadExchangeConstituent();
//            fireExchangesAdded();
        } catch (Exception e) {
            loadingFailed = true;
            exchanges = Collections.synchronizedList(new ArrayList<Exchange>());
            defaultExchanges = Collections.synchronizedList(new ArrayList<Exchange>());
            e.printStackTrace();
        }
    }

    public void notifyExchangesLoaded() {
        fireExchangesAdded(true);
    }

    public void save() {
        try {
//            Exchange newExchange = new Exchange("DUBI", "DUBI", true,"Dubai");
//            exchanges.add(newExchange);
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(Settings.getAbsolutepath()+"datastore/exchanges.mdf"));
            out.writeObject(exchanges);
//            out.writeInt(1111);
            out.writeObject(applicationExchange);
            out.close();
            out = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Exchange[] getExchangesFromCurrency(String currency) {
        ArrayList<Exchange> list = new ArrayList<Exchange>();
        Enumeration exchanges = getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.getActiveCurrency().equals(currency)) {
                list.add(exchange);
            }
            exchange = null;
        }
        return list.toArray(new Exchange[0]);
    }

    /**
     * This is the property file download method. This method will try to download the file from a particular CM
     * for 2 times. If unsuccessful, will reconnect and try again. i.e file download will be attempted from 2 servers.
     * After 2 CM attempts, file download will be aborted and normal process will resume.
     *
     * @param subscribedList
     * @param parameters
     */
    //Bug Id <#0008> introduced the 'fileDownloadCount' variable to handle endless file download scenario
    public void addExchangeList(String subscribedList, String parameters) {
        int count = 1;
        boolean sucess = false;
        defaultExchanges.clear();
        while (count > 0) {
            sucess = createExchanges(subscribedList, parameters , "");
            if (sucess) {
                fileDownloadCount = 0;
                break;
            } else {
                count--;
            }
            SharedMethods.sleep(1000);
        }

        if ((!sucess) && (count <= 0)) {
            fileDownloadCount++;
            if (fileDownloadCount < 2) { // login to the next server for 2 times and try the file download again
                Client.getInstance().disconnectFromServer();
                Authenticator.USER_VALID = false;
            } else {
                fileDownloadCount = 0; // reset for the future use
            }
        }
    }

      public void addExchangeList(String subscribedList, String parameters, String windowTypes) { // Added to hold the WSP loading, be4 adding window types
        int count = 1;
        boolean sucess = false;
        defaultExchanges.clear();
        while (count > 0) {
            sucess = createExchanges(subscribedList, parameters,windowTypes);
            if (sucess) {
                fileDownloadCount = 0;
                break;
            } else {
                count--;
            }
            SharedMethods.sleep(1000);
        }

        if ((!sucess) && (count <= 0)) {
            fileDownloadCount++;
            if (fileDownloadCount < 2) { // login to the next server for 2 times and try the file download again
                Client.getInstance().disconnectFromServer();
                Authenticator.USER_VALID = false;
            } else {
                fileDownloadCount = 0; // reset for the future use
            }
        }
    }
    /*private void loadDownloadedFiles() {
        System.out.println("Loading default data ....................");
        Enumeration exchangeEnum = ExchangeStore.getSharedInstance().getExchanges();
        while (exchangeEnum.hasMoreElements()) {
            Exchange exchange = (Exchange) exchangeEnum.nextElement();
            if (exchange.isDefault()) {
                if (!SymbolMaster.isMarketLoaded(exchange.getSymbol())) {
                    try {
                        SymbolMaster.loadMarketFile(exchange.getSymbol());
                        boolean marketLoaded = SymbolMaster.isMarketLoaded(exchange.getSymbol()); // check if sucess
                        if (marketLoaded) {
                            fireExchangeMasterFileLoaded(exchange.getSymbol());
                        }
                    } catch (Exception e) {
                        System.out.println("Error loading market file");
                        e.printStackTrace();
                    }
                }
            }
            exchange = null;
        }
        exchangeEnum = null;
    }*/

    private boolean createExchanges(String subscribedList, String parameters , String windowTypes) {

        float currentMasterDataVersion = 0;
        float currentPropertyVersion = 0;
        boolean newExchangesLoaded = false;
        noneDefaultAvailable = false;
        defaultAvailable = false;
        System.err.println("---------------------------------------------");

/*        append the application exchange to the list, so that it is easy to analyse the string
          subscribedList -> CASE,0,C,CA,Africa/Cairo,EGP,1|TDWL,1,T,SA,Asia/Riyadh,SAR,1
          parameters -> TDWL,T,2.0
        */
        subscribedList += "|";
        subscribedList += Constants.APPLICATION_EXCHANGE;
        subscribedList += ",1,S,1";

        loadExchangeParams(subscribedList, parameters);
        secondaryPathConnectionRequired = false;
        canListenForStockMaster = true;


        Collection<ExchangeParams> exchangeParams = exchangeParamsTable.values();
        for (ExchangeParams params : exchangeParams) {

            if ((!params.defaultMarket) && (!params.exchange.equals(Constants.APPLICATION_EXCHANGE))) {
                noneDefaultAvailable = true; // set flag to notify none non default exchange availability
            }

            if ((params.defaultMarket) && (!params.exchange.equals(Constants.APPLICATION_EXCHANGE))) {
                defaultAvailable = true; // set flag to notify none default exchange availability
            }

            boolean marketLoaded = SymbolMaster.isMarketLoaded(params.exchange);
            boolean exgPropLoaded = SymbolMaster.isExchangPropsLoaded(params.exchange);

            // check it secondary connections available
            if ((Settings.isTCPMode()) && (!secondaryPathConnectionRequired) && (params.path == Constants.PATH_SECONDARY)) {
                secondaryPathConnectionRequired = true;
            }
//            if ((params.defaultMarket)) {
            if (/*(params.masterFileAvailable()) &&*/ (params.defaultMarket)) {
                if ((!marketLoaded)) { // if market is notloaded, try to load it
                    if (params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
                        currentMasterDataVersion = Settings.getPropertyVersion();
                        if((currentMasterDataVersion > 0) && (currentMasterDataVersion == params.version)){
                            marketLoaded = true;
//                            NewsProviders.getSharedInstance().loadCommonNewsProvidersFile();
                        }
                    } else {
                        try {
                            SymbolMaster.loadMarketFile(params.exchange, true);
                            currentMasterDataVersion = SymbolMaster.getVersion(params.exchange);
                            if((currentMasterDataVersion > 0) && (currentMasterDataVersion == params.version)){
                                marketLoaded = true;
                                fireExchangeMasterFileLoaded(params.exchange);
                            } else {
                                SymbolMaster.unloadMarket(params.exchange);
                            }
                        } catch (Exception e) {
                            SymbolMaster.unloadMarket(params.exchange);
                            e.printStackTrace();
                            marketLoaded = false;
                            currentMasterDataVersion = 0;
                        }
                    }
                } else {
                    currentMasterDataVersion = SymbolMaster.getVersion(params.exchange);
                    if((currentMasterDataVersion > 0) && (currentMasterDataVersion == params.version)){
//                        fireExchangeMasterFileLoaded(params.exchange);
                    } else {
                        marketLoaded = false;
                        SymbolMaster.unloadMarket(params.exchange);
                    }
                }

                if ((!exgPropLoaded)) { // if exchange Properties is notloaded, try to load it
                    if (!params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
                        try {
                            SymbolMaster.loadExgPropertyFile(params.exchange);
                            currentPropertyVersion = SymbolMaster.getExgPropertyversion(params.exchange); //newly added
                            if((currentPropertyVersion > 0) && (currentPropertyVersion == params.propVersion)){
                                exgPropLoaded = true;
                                SymbolMaster.loadMarketIndexFile(params.exchange);
//                                NewsProviders.getSharedInstance().loadNewsProvidersFile(params.exchange, false);
                                loadExchangeFeatures(params.exchange);
                            }
                        } catch (Exception e) {
                            SymbolMaster.unloadExchangeProperties(params.exchange);
                            e.printStackTrace();
                            exgPropLoaded = false;
                            currentPropertyVersion = 0;
                        }
                    }
                } else {
                    if (!params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
                        currentPropertyVersion = SymbolMaster.getExgPropertyversion(params.exchange); //newly added
                        if((currentPropertyVersion > 0) && (currentPropertyVersion == params.propVersion)){
                            try {
                                SymbolMaster.loadMarketIndexFile(params.exchange);
//                                NewsProviders.getSharedInstance().loadNewsProvidersFile(params.exchange, false);
                                loadExchangeFeatures(params.exchange);
                            } catch(Exception e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                        } else {
                            SymbolMaster.unloadExchangeProperties(params.exchange);
                            exgPropLoaded = false;
                        }
                    }
                }

            } else { // symbol driven market  == Non default handle
                if ((!exgPropLoaded)) { // if market is notloaded, try to load it
                    try {
                        SymbolMaster.loadExgPropertyFile(params.exchange);
                        currentPropertyVersion = SymbolMaster.getExgPropertyversion(params.exchange); //newly added
                        if((currentPropertyVersion > 0) && (currentPropertyVersion == params.propVersion)){
                            exgPropLoaded = true;
                            SymbolMaster.loadMarketIndexFile(params.exchange);
//                            NewsProviders.getSharedInstance().loadNewsProvidersFile(params.exchange, false);
                            loadExchangeFeatures(params.exchange);
                        }
                    } catch (Exception e) {
                        SymbolMaster.unloadExchangeProperties(params.exchange);
                        e.printStackTrace();
                        exgPropLoaded = false;
                        currentPropertyVersion = 0;
                    }
                } else {
                    currentPropertyVersion = SymbolMaster.getExgPropertyversion(params.exchange); //newly added
                    if((currentPropertyVersion > 0) && (currentPropertyVersion == params.propVersion)){
                        try {
                            SymbolMaster.loadMarketIndexFile(params.exchange);
//                            NewsProviders.getSharedInstance().loadNewsProvidersFile(params.exchange, false);
                            loadExchangeFeatures(params.exchange);
                        } catch(Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    } else {
                        SymbolMaster.unloadExchangeProperties(params.exchange);
                        exgPropLoaded = false;
                    }
                }
            }

//            if ((params.defaultMarket)) {
//            if ((params.masterFileAvailable())) { // common for both ex
                if (params.defaultMarket) {
                if (!marketLoaded) {
                    if (params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
                        showDownloadMessage();
                        if (Settings.isTCPMode()) {
                            MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                    params.exchange, Downloader.APPLICATION_DATA, params.version, params.contentCMIP,params.defaultMarket);
                        }
                    } else {
                        showDownloadMessage();
                        if (Settings.isTCPMode()) {
                            MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                    params.exchange, Downloader.EXCHANGE_DATA, 0, params.contentCMIP,params.defaultMarket);

                        }
                    }
                } /*else if (currentMasterDataVersion < params.version) {
                    if (params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
                        showDownloadMessage();
                        if (Settings.isTCPMode()) {
                            MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                    params.exchange, Downloader.APPLICATION_DATA, params.version, params.contentCMIP);
                        }
                    } else {
                        showDownloadMessage();
                        SymbolMaster.unloadMarket(params.exchange);
                        if (Settings.isTCPMode()) {
                            MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                    params.exchange, Downloader.EXCHANGE_DATA, currentMasterDataVersion, params.contentCMIP);
                        }
                    }
                }*/

                if (!exgPropLoaded) {
                    showDownloadMessage();
                    if (Settings.isTCPMode()) {
                        MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                params.exchange, Downloader.EXCHANGE_PROPS, 0, params.contentCMIP,params.defaultMarket);
                    }

                } /*else if (!(params.exchange.equals(Constants.APPLICATION_EXCHANGE)) && (currentPropertyVersion < params.propVersion)) {
                    showDownloadMessage();
                    SymbolMaster.unloadExchangeProperties(params.exchange);
                    if (Settings.isTCPMode()) {
                        MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                params.exchange, Downloader.EXCHANGE_PROPS, currentPropertyVersion, params.contentCMIP);
                    }
                }*/
            } else {
                    int masterFileStatus = getFileStatus(params.exchange);
                    try {
//                        SymbolMaster.loadMarketFile(params.exchange, true);
//                        currentMasterDataVersion = SymbolMaster.getStockMasterVersionForNonDef(params.exchange);
                        if (isMarketFileAvailable(params.exchange) && masterFileStatus > 0 && masterFileStatus >= params.masterFileStaus) {
                            currentMasterDataVersion = SymbolMaster.getStockMasterVersionForNonDef(params.exchange);
                            
                            if ((currentMasterDataVersion > 0) && (currentMasterDataVersion == params.version)) {
                                marketLoaded = true;
                                fireExchangeMasterFileLoaded(params.exchange);
                            }
                        } else {
                            SymbolMaster.unloadMarket(params.exchange);
                        }
                    } catch (Exception e) {
                        SymbolMaster.unloadMarket(params.exchange);
                        e.printStackTrace();
                        marketLoaded = false;
                        currentMasterDataVersion = 0;
                    }
                        if (!marketLoaded) {
                            if (params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
                                showDownloadMessage();
                                if (Settings.isTCPMode()) {
                                    MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                            params.exchange, Downloader.APPLICATION_DATA, params.version, params.contentCMIP,params.defaultMarket);
                                }
                            } else {
                                showDownloadMessage();
                                if (Settings.isTCPMode()) {
                                    MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                            params.exchange, Downloader.EXCHANGE_DATA, 0, params.contentCMIP,params.defaultMarket);

                                }
                            }
                        } /*else if (currentMasterDataVersion < params.version) {
                        if (params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
                            showDownloadMessage();
                            if (Settings.isTCPMode()) {
                                MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                        params.exchange, Downloader.APPLICATION_DATA, params.version, params.contentCMIP);
                            }
                        } else {
                            showDownloadMessage();
                            SymbolMaster.unloadMarket(params.exchange);
                            if (Settings.isTCPMode()) {
                                MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                        params.exchange, Downloader.EXCHANGE_DATA, currentMasterDataVersion, params.contentCMIP);
                            }
                        }
                    }*/

                if (!exgPropLoaded) {
                    showDownloadMessage();
                    if (Settings.isTCPMode()) {
                        MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                params.exchange, Downloader.EXCHANGE_PROPS, 0, params.contentCMIP,params.defaultMarket);
                    }

                        } /*else if (!(params.exchange.equals(Constants.APPLICATION_EXCHANGE)) && (currentPropertyVersion < params.propVersion)) {
                    showDownloadMessage();
                    SymbolMaster.unloadExchangeProperties(params.exchange);
                    if (Settings.isTCPMode()) {
                        MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                params.exchange, Downloader.EXCHANGE_PROPS, currentPropertyVersion, params.contentCMIP);
                    }
                }*/


            }
//            }
           /* else { //todo : no need
                if (!exgPropLoaded) {
                    showDownloadMessage();
                    if (Settings.isTCPMode()) {
                        MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                params.exchange, Downloader.EXCHANGE_PROPS, 0, params.contentCMIP,params.defaultMarket);
                    }
                } *//*else if (currentPropertyVersion < params.propVersion) {
                    showDownloadMessage();
                    SymbolMaster.unloadExchangeProperties(params.exchange);
                    if (Settings.isTCPMode()) {
                        MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                params.exchange, Downloader.EXCHANGE_PROPS, currentPropertyVersion, params.contentCMIP);
                    }
                }*//*
            }*/
        }

        boolean waitedForDownload = false;
        if (Settings.isTCPMode()) { // Network Mode
            while (MarketDataDownloadManager.getSharedInstance().downloadCount() > 0) { // wait tille download complete
                try {
                    System.out.println("Download Count ===== " + MarketDataDownloadManager.getSharedInstance().downloadCount());
                    waitedForDownload = true;
                    Thread.sleep(5000);
                } catch (Exception e) {
                }
            }
            System.out.println("break the downloading loop,  waitedForDownload=="+waitedForDownload);
        } else { // Satellite Mode
            boolean completed = false;

            while ((!completed) && (canListenForStockMaster)) { // wait till download complete for all exchanges
                completed = true;
                System.err.println("====waiting for stock master");
                for (ExchangeParams params : exchangeParams) {
                    if (params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
                        if (params.version != FileListener.getInProgressStockMasterVersion(params.exchange) &&
                                (params.version != Settings.getPropertyVersion())) {
                            System.err.println(params.exchange + " " + params.version + " " + FileListener.getInProgressStockMasterVersion(params.exchange));
                            System.err.println("waiting for poperty =="+ params.exchange);
                            completed = false;
                            waitedForDownload = true;
                        }
                    } else {
                        if (params.version != FileListener.getInProgressStockMasterVersion(params.exchange) &&
                                (params.version != SymbolMaster.getVersion(params.exchange))) {
                            System.err.println(params.exchange + " " + params.version + " " + FileListener.getInProgressStockMasterVersion(params.exchange));
                            System.err.println("waiting for stockmaster =="+ params.exchange);
                            completed = false;
                            waitedForDownload = true;
                        }
                    }
                }
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                }
            }

            FileListener.clearInProgressStockMaster();
        }
        waitedForDownload = false;
        for (ExchangeParams params : exchangeParams) {

            if (!params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
                ExchangeMap.setMapData(params.exchangeCode, params.exchange);
                Exchange newExchange = new Exchange(params.exchangeCode,
                        params.exchange, params.country, params.defaultMarket, params.path, params.dataPort, params.filePort, params.contentCMIP, params.isDelayed);

                // todo: testing--
//                newExchange.setMarketFileAvailable(params.masterFileAvailable());
//                newExchange.setMasterFileStaus(params.masterFileStaus);
                Exchange currentExchange = getExchange(newExchange.getSymbol()); // check if the exchange is ..
                System.out.println("NEW exchange ------------"+newExchange.getSymbol());
                System.out.println("NEW exchange properties ----- "+ params.defaultMarket + " , "+ params.version + " , "+ params.propVersion);
                if (currentExchange == null) {                                   // already there
                    exchanges.add(newExchange);
//                    if(params.defaultMarket){
                    if((params.masterFileAvailable()) && (params.defaultMarket)){
                        try {
                            SymbolMaster.loadMarketFile(params.exchange, true);
                            fireExchangeMasterFileLoaded(params.exchange);
                        } catch(Exception e) {
                            waitedForDownload = true;
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    }
                    newExchange.setExchangeExpired(isExpiredExchange(params.exchange));
                    newExchange.setExchangeInactive(isInactiveExchange(params.exchange));
                    try {
                        if (!newExchange.isExpired() && !newExchange.isInactive()) {
                        SymbolMaster.loadMarketIndexFile(params.exchange);
//                        NewsProviders.getSharedInstance().loadNewsProvidersFile(params.exchange, false);
                        }
                        loadExchangeFeatures(params.exchange);
                    } catch(Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    loadSubMarkets(newExchange);
                    loadBrokers(newExchange);
                    System.out.println("fire exchange added exchange =="+newExchange.getSymbol());
                    fireExchangeAdded(newExchange);
                } else if (newExchange.isDefault() && !currentExchange.isDefault()) { // upgrade to default
                    currentExchange.setDefault(true);
                    currentExchange.setFlag();
                    currentExchange.setCountry(newExchange.getCountry());
                    currentExchange.setPath(newExchange.getPath());
                    currentExchange.setContentCMIP(newExchange.getContentCMIP());
                    currentExchange.setSatelliteDataPort(newExchange.getSatelliteDataPort());
                    currentExchange.setSatelliteFilePort(newExchange.getSatelliteFilePort());
                    currentExchange.setExchangeDelayed(newExchange.isDelayed());
                    currentExchange.setExchangeExpired(isExpiredExchange(newExchange.getSymbol()));
                    currentExchange.setExchangeInactive(isInactiveExchange(newExchange.getSymbol()));
                    //todo add new
                    currentExchange.setMarketFileAvailable(params.masterFileAvailable());
                    currentExchange.setMasterFileStaus(params.masterFileStaus);
                    //loadSubMarkets(currentExchange);
                    System.out.println("fire exchange upgraded exchange =="+currentExchange.getSymbol());
                    fireExchangeUpgraded(currentExchange);
//                    fireExchangeUpgraded(newExchange);
                } else if (!newExchange.isDefault() && currentExchange.isDefault()) { // downgrade to non-default
                    currentExchange.setDefault(false);
                    currentExchange.setFlag();
                    currentExchange.setCountry(newExchange.getCountry());
                    currentExchange.setPath(newExchange.getPath());
                    currentExchange.setContentCMIP(newExchange.getContentCMIP());
                    currentExchange.setSatelliteDataPort(newExchange.getSatelliteDataPort());
                    currentExchange.setSatelliteFilePort(newExchange.getSatelliteFilePort());
                    currentExchange.setExchangeDelayed(newExchange.isDelayed());
                    currentExchange.setExchangeExpired(isExpiredExchange(newExchange.getSymbol()));
                    currentExchange.setExchangeInactive(isInactiveExchange(newExchange.getSymbol()));

                    //todo add new
                    currentExchange.setMarketFileAvailable(params.masterFileAvailable());
                    currentExchange.setMasterFileStaus(params.masterFileStaus);
                    System.out.println("fire exchange downgraded exchange =="+currentExchange.getSymbol());
                    fireExchangeDowngraded(currentExchange);
//                    fireExchangeDowngraded(newExchange);
                    //loadSubMarkets(currentExchange);
                } else if ((newExchange.isDefault() && currentExchange.isDefault()) ||
                        (!newExchange.isDefault() && !currentExchange.isDefault())) { // no changes to the existing
                    currentExchange.setFlag();
                    currentExchange.setCountry(newExchange.getCountry());
                    currentExchange.setPath(newExchange.getPath());
                    currentExchange.setContentCMIP(newExchange.getContentCMIP());
                    currentExchange.setSatelliteDataPort(newExchange.getSatelliteDataPort());
                    currentExchange.setSatelliteFilePort(newExchange.getSatelliteFilePort());
                    currentExchange.setExchangeDelayed(newExchange.isDelayed());
                    currentExchange.setExchangeExpired(isExpiredExchange(newExchange.getSymbol()));
                    currentExchange.setExchangeInactive(isInactiveExchange(newExchange.getSymbol()));
                    //todo add new
                    currentExchange.setMarketFileAvailable(params.masterFileAvailable());
                    currentExchange.setMasterFileStaus(params.masterFileStaus);
                    loadSubMarkets(currentExchange); //included to load submarkets at normal loadup Bug ID <#0030>
                    System.out.println(" no change to the exchange =="+currentExchange.getSymbol());
                }
                if (newExchange.isDefault()) {
                    try {
                        SymbolMaster.loadMarketFile(params.exchange, true);
                        fireExchangeMasterFileLoaded(params.exchange);
                    } catch(Exception e) {
                        waitedForDownload = true;
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
//                    newExchangesLoaded = true;
                    System.out.println("newly loaded exchange =="+params.exchange);
                    DataStore.getSharedInstance().loadExchange(params.exchange); // load symbols for this exchange
                    SectorStore.getSharedInstance().loadExchange(params.exchange); // load sectors for this exchange
                }
                newExchange = null;
            } else {
                System.out.println("application exchange");
                applicationExchange = new Exchange(params.exchangeCode, params.exchange, params.country, false,
                        params.path, params.dataPort, params.filePort, params.contentCMIP, false);
                applicationExchange.setMarketFileAvailable(params.masterFileAvailable());
                applicationExchange.setMasterFileStaus(params.masterFileStaus);                
//                addWindowTypes(windowTypes);
            }
        }
        addWindowTypes(windowTypes);

        defaultExchanges = new ArrayList<Exchange>();
        for (Exchange exchange : exchanges) {
            if (exchange.isDefault()) {
                defaultExchanges.add(exchange);
            }
        }

        if (workInProgressIndicator != null) {
            workInProgressIndicator.setVisible(false);
        }

        boolean returnValue;
        if (waitedForDownload) { // if a download completed, must restart the connection to load the file
            //Client.getInstance().disconnectFromServer();
//            System.out.println("waited for download");
            returnValue = false;
        } else {
            System.out.println("download completed");
            /*updateExchangeParams();
            removeUnsubscribedExchanges();
            DataStore.getSharedInstance().createIndexes();
            loadSystemProperties();
            fireExchangesAdded(false);
//            System.out.println("fire exchanges added");
            if (newExchangesLoaded) { /// new exchanges loaded in this session. init the repository
//                SymbolRepository.initialize();
                CompanyTable.getSharedInstance().init();
            }
            rebuildAllMarkets();*/
//            loadCurrencies();
            returnValue = true;
        }
//        updateExpiryStatus();
        updateExchangeParams();
        removeUnsubscribedExchanges();
        DataStore.getSharedInstance().createIndexes();
        loadSystemProperties();
        fireExchangesAdded(false);
        if (newExchangesLoaded) { /// new exchanges loaded in this session. init the repository
            CompanyTable.getSharedInstance().init();
        }
        rebuildAllMarkets();

        exchangeParamsTable = null;
//        System.out.println("method ends here");
        return returnValue;
    }

   /* private boolean createExchangesOld(String subscribedList, String parameters) {

        float currentMasterDataVersion = 0;
        float currentPropertyVersion = 0;
        boolean newExchangesLoaded = false;
        noneDefaultAvailable = false;
        defaultAvailable = false;
        System.err.println("---------------------------------------------");

*//*        append the application exchange to the list, so that it is easy to analyse the string
          subscribedList -> CASE,0,C,CA,Africa/Cairo,EGP,1|TDWL,1,T,SA,Asia/Riyadh,SAR,1
          parameters -> TDWL,T,2.0
        *//*
        subscribedList += "|";
        subscribedList += Constants.APPLICATION_EXCHANGE;
        subscribedList += ",1,S,1";

        loadExchangeParams(subscribedList, parameters);
        secondaryPathConnectionRequired = false;
        canListenForStockMaster = true;


        Collection<ExchangeParams> exchangeParams = exchangeParamsTable.values();
        for (ExchangeParams params : exchangeParams) {

            if ((!params.defaultMarket) && (!params.exchange.equals(Constants.APPLICATION_EXCHANGE))) {
                noneDefaultAvailable = true; // set flag to notify none non default exchange availability
            }

            if ((params.defaultMarket) && (!params.exchange.equals(Constants.APPLICATION_EXCHANGE))) {
                defaultAvailable = true; // set flag to notify none default exchange availability
            }

            boolean marketLoaded = SymbolMaster.isMarketLoaded(params.exchange);
            boolean exgPropLoaded = SymbolMaster.isExchangPropsLoaded(params.exchange);
            boolean newlyLoadedExchange = false;

            // check it secondary connections available
            if ((Settings.isTCPMode()) && (!secondaryPathConnectionRequired) && (params.path == Constants.PATH_SECONDARY)) {
                secondaryPathConnectionRequired = true;
            }
            if ((params.defaultMarket)) {
                if ((!marketLoaded)) { // if market is notloaded, try to load it
                    try {
                        if (params.exchange.equals(Constants.APPLICATION_EXCHANGE)) { // for SYS exchange, thre is no file to load
                            currentMasterDataVersion = Settings.getPropertyVersion();
                            marketLoaded = true;
                            NewsProviders.getSharedInstance().loadCommonNewsProvidersFile();
                        } else {
                            SymbolMaster.loadMarketFile(params.exchange, true);
                            SymbolMaster.loadMarketIndexFile(params.exchange);
                            NewsProviders.getSharedInstance().loadNewsProvidersFile(params.exchange, false);
                            loadExchangeFeatures(params.exchange);
//                            NewsProviders.getSharedInstance().loadCommonNewsProvidersFile();
                            marketLoaded = SymbolMaster.isMarketLoaded(params.exchange); // check if sucess
                            currentMasterDataVersion = SymbolMaster.getVersion(params.exchange);
                            if (marketLoaded) {
                                fireExchangeMasterFileLoaded(params.exchange);
                            }
                        }
                        newlyLoadedExchange = marketLoaded;
                    } catch (Exception e) {
                        e.printStackTrace();
                        marketLoaded = false;
                        currentMasterDataVersion = 0;
                    }
                } else {
                    if (params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
                        exgPropLoaded = true;
                        currentMasterDataVersion = Settings.getPropertyVersion();
                        NewsProviders.getSharedInstance().loadCommonNewsProvidersFile();
                    } else {
                        currentMasterDataVersion = SymbolMaster.getVersion(params.exchange);
                    }
                }

                if ((!exgPropLoaded)) { // if market is notloaded, try to load it
                    try {
                        SymbolMaster.loadExgPropertyFile(params.exchange);
                        exgPropLoaded = SymbolMaster.isExchangPropsLoaded(params.exchange); // check if sucess
                        currentPropertyVersion = SymbolMaster.getExgPropertyversion(params.exchange); //newly added
                    } catch (Exception e) {
                        e.printStackTrace();
                        exgPropLoaded = false;
                        currentPropertyVersion = 0;
                    }
                } else {
//                    exgPropLoaded             = SymbolMaster.isExchangPropsLoaded(params.exchange); // check if sucess
                    currentPropertyVersion = SymbolMaster.getExgPropertyversion(params.exchange); //newly added
                }
            } else { // symbol driven market
                try {
                    if ((!exgPropLoaded)) { // if market is notloaded, try to load it
                        SymbolMaster.loadExgPropertyFile(params.exchange);
                        exgPropLoaded = SymbolMaster.isExchangPropsLoaded(params.exchange); // check if sucess
                        currentPropertyVersion = SymbolMaster.getExgPropertyversion(params.exchange); //newly added

                    } else {
                        exgPropLoaded = SymbolMaster.isExchangPropsLoaded(params.exchange); // check if sucess
                        currentPropertyVersion = SymbolMaster.getExgPropertyversion(params.exchange); //newly added
                    }
                    NewsProviders.getSharedInstance().loadNewsProvidersFile(params.exchange, false);
                    loadExchangeFeatures(params.exchange);
                } catch (Exception e) {
                    e.printStackTrace();
                    exgPropLoaded = false;
                    currentPropertyVersion = 0;
                }
            }


            if ((params.defaultMarket)) {
                if (!marketLoaded) {
                    showDownloadMessage();
                    if (Settings.isTCPMode()) {
                        MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                params.exchange, Downloader.EXCHANGE_DATA, 0, params.contentCMIP);

                    }
                } else if (currentMasterDataVersion < params.version) {
                    if (params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
                        showDownloadMessage();
                        if (Settings.isTCPMode()) {
                            MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                    params.exchange, Downloader.APPLICATION_DATA, params.version, params.contentCMIP);
                        }
                    } else {
                        showDownloadMessage();
                        SymbolMaster.unloadMarket(params.exchange);
                        if (Settings.isTCPMode()) {
                            MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                    params.exchange, Downloader.EXCHANGE_DATA, currentMasterDataVersion, params.contentCMIP);
                        }
                    }
                }

                if (!exgPropLoaded) {
                    showDownloadMessage();
                    if (Settings.isTCPMode()) {
                        MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                params.exchange, Downloader.EXCHANGE_PROPS, 0, params.contentCMIP);
                    }

                } else if (!(params.exchange.equals(Constants.APPLICATION_EXCHANGE)) && (currentPropertyVersion < params.propVersion)) {
                    showDownloadMessage();
                    SymbolMaster.unloadExchangeProperties(params.exchange);
                    if (Settings.isTCPMode()) {
                        MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                params.exchange, Downloader.EXCHANGE_PROPS, currentPropertyVersion, params.contentCMIP);
                    }
                }
            } else {
                if (!exgPropLoaded) {
                    showDownloadMessage();
                    if (Settings.isTCPMode()) {
                        MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                params.exchange, Downloader.EXCHANGE_PROPS, 0, params.contentCMIP);
                    }
                } else if (currentPropertyVersion < params.propVersion) {
                    showDownloadMessage();
                    SymbolMaster.unloadExchangeProperties(params.exchange);
                    if (Settings.isTCPMode()) {
                        MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
                                params.exchange, Downloader.EXCHANGE_PROPS, currentPropertyVersion, params.contentCMIP);
                    }
                }
            }
//==============================================================================================================
//            if ((params.defaultMarket) && (!marketLoaded))
//            { //looks like the exchange data file is not available. must dowload it
//                showDownloadMessage();
//                if (Settings.isTCPMode()) {
//                    MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
//                            params.exchange, Downloader.EXCHANGE_DATA, -1, params.contentCMIP);
//                }
//            }
// else if ((params.defaultMarket) && (currentMasterDataVersion < params.version)) {// old version. must download
//                showDownloadMessage();
//                if (params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
//                    if (Settings.isTCPMode()) {
//                        MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
//                                params.exchange, Downloader.APPLICATION_DATA, getMarketDataVersion(params.exchange), params.contentCMIP);
//                    }
//                } else {
//                    SymbolMaster.unloadMarket(params.exchange);
//                    if (Settings.isTCPMode()) {
//                        MarketDataDownloadManager.getSharedInstance().startNewDownloadSession(workInProgressIndicator,
//                                params.exchange, Downloader.EXCHANGE_DATA, currentMasterDataVersion, params.contentCMIP);
//                    }
//                }
//            }
//===================================================================================================================
            if (!params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
                ExchangeMap.setMapData(params.exchangeCode, params.exchange);
                Exchange newExchange = new Exchange(params.exchangeCode,
                        params.exchange, params.country, params.defaultMarket, params.path, params.dataPort, params.filePort, params.contentCMIP, params.isDelayed);

                Exchange currentExchange = getExchange(newExchange.getSymbol()); // check if the exchange is ..
                System.out.println("NEW exchange ------------"+newExchange.getSymbol());
                System.out.println("NEW exchange properties ----- "+ params.defaultMarket + " , "+ params.version + " , "+ params.propVersion);
                if (currentExchange == null) {                                   // already there
                    exchanges.add(newExchange);
                    loadSubMarkets(newExchange);
                    loadBrokers(newExchange);
                    System.out.println("fire exchange added exchange =="+newExchange.getSymbol());
                    fireExchangeAdded(newExchange);
                } else if (newExchange.isDefault() && !currentExchange.isDefault()) { // upgrade to default
                    currentExchange.setDefault(true);
                    currentExchange.setFlag();
                    currentExchange.setCountry(newExchange.getCountry());
                    currentExchange.setPath(newExchange.getPath());
                    currentExchange.setContentCMIP(newExchange.getContentCMIP());
                    currentExchange.setSatelliteDataPort(newExchange.getSatelliteDataPort());
                    currentExchange.setSatelliteFilePort(newExchange.getSatelliteFilePort());
                    currentExchange.setExchangeDelayed(newExchange.isDelayed());
                    //loadSubMarkets(currentExchange);
                    System.out.println("fire exchange upgraded exchange =="+newExchange.getSymbol());
                    fireExchangeUpgraded(newExchange);
                } else if (!newExchange.isDefault() && currentExchange.isDefault()) { // downgrade to non-default
                    currentExchange.setDefault(false);
                    currentExchange.setFlag();
                    currentExchange.setCountry(newExchange.getCountry());
                    currentExchange.setPath(newExchange.getPath());
                    currentExchange.setContentCMIP(newExchange.getContentCMIP());
                    currentExchange.setSatelliteDataPort(newExchange.getSatelliteDataPort());
                    currentExchange.setSatelliteFilePort(newExchange.getSatelliteFilePort());
                    currentExchange.setExchangeDelayed(newExchange.isDelayed());
                    System.out.println("fire exchange downgraded exchange =="+newExchange.getSymbol());
                    fireExchangeDowngraded(newExchange);
                    //loadSubMarkets(currentExchange);
                } else if ((newExchange.isDefault() && currentExchange.isDefault()) ||
                        (!newExchange.isDefault() && !currentExchange.isDefault())) { // no changes to the existing
                    currentExchange.setFlag();
                    currentExchange.setCountry(newExchange.getCountry());
                    currentExchange.setPath(newExchange.getPath());
                    currentExchange.setContentCMIP(newExchange.getContentCMIP());
                    currentExchange.setSatelliteDataPort(newExchange.getSatelliteDataPort());
                    currentExchange.setSatelliteFilePort(newExchange.getSatelliteFilePort());
                    currentExchange.setExchangeDelayed(newExchange.isDelayed());
                    loadSubMarkets(currentExchange); //included to load submarkets at normal loadup Bug ID <#0030>
                    System.out.println(" no change to the exchange =="+currentExchange.getSymbol());
                }
                if (newlyLoadedExchange && newExchange.isDefault()) {
                    newExchangesLoaded = true;
                    System.out.println("newly loaded exchange =="+params.exchange);
                    DataStore.getSharedInstance().loadExchange(params.exchange); // load symbols for this exchange
                    SectorStore.getSharedInstance().loadExchange(params.exchange); // load sectors for this exchange
                }
                newExchange = null;
            } else {
                System.out.println("application exchange");
                applicationExchange = new Exchange(params.exchangeCode, params.exchange, params.country, false,
                        params.path, params.dataPort, params.filePort, params.contentCMIP, false);
            }
        }

        defaultExchanges = new ArrayList<Exchange>();
        for (Exchange exchange : exchanges) {
            if (exchange.isDefault()) {
                defaultExchanges.add(exchange);
            }
        }

        boolean waitedForDownload = false;
        if (Settings.isTCPMode()) { // Network Mode
            while (MarketDataDownloadManager.getSharedInstance().downloadCount() > 0) { // wait tille download complete
                try {
                    System.out.println("Download Count ===== " + MarketDataDownloadManager.getSharedInstance().downloadCount());
                    waitedForDownload = true;
                    Thread.sleep(5000);
                } catch (Exception e) {
                }
            }
            System.out.println("break the downloading loop,  waitedForDownload=="+waitedForDownload);
        } else { // Satellite Mode
            boolean completed = false;

            while ((!completed) && (canListenForStockMaster)) { // wait till download complete for all exchanges
                completed = true;
                System.err.println("====waiting for stock master");
                for (ExchangeParams params : exchangeParams) {
                    if (params.exchange.equals(Constants.APPLICATION_EXCHANGE)) {
                        if (params.version != FileListener.getInProgressStockMasterVersion(params.exchange) &&
                                (params.version != Settings.getPropertyVersion())) {
                            System.err.println(params.exchange + " " + params.version + " " + FileListener.getInProgressStockMasterVersion(params.exchange));
                            System.err.println("waiting for poperty =="+ params.exchange);
                            completed = false;
                            waitedForDownload = true;
                        }
                    } else {
                        if (params.version != FileListener.getInProgressStockMasterVersion(params.exchange) &&
                                (params.version != SymbolMaster.getVersion(params.exchange))) {
                            System.err.println(params.exchange + " " + params.version + " " + FileListener.getInProgressStockMasterVersion(params.exchange));
                            System.err.println("waiting for stockmaster =="+ params.exchange);
                            completed = false;
                            waitedForDownload = true;
                        }
                        *//*if (params.version != FileListener.getInProgressStockMasterVersion(params.exchange) &&
                                (params.version != SymbolMaster.getVersion(params.exchange))) {
                            System.err.println(params.exchange + " " + params.version + " " + FileListener.getInProgressStockMasterVersion(params.exchange));
                            System.err.println("waiting for stockmaster =="+ params.exchange);
                            completed = false;
                            waitedForDownload = true;
                        }*//*
                    }
                }
                try {
                    Thread.sleep(5000);
                } catch (Exception e) {
                }
            }

            FileListener.clearInProgressStockMaster();
        }

        if (workInProgressIndicator != null) {
            workInProgressIndicator.setVisible(false);
        }

        boolean returnValue;
        if (waitedForDownload) { // if a download completed, must restart the connection to load the file
            //Client.getInstance().disconnectFromServer();
//            System.out.println("waited for download");
            returnValue = false;
        } else {
            System.out.println("download completed");
            *//*updateExchangeParams();
            removeUnsubscribedExchanges();
            DataStore.getSharedInstance().createIndexes();
            loadSystemProperties();
            fireExchangesAdded(false);
//            System.out.println("fire exchanges added");
            if (newExchangesLoaded) { /// new exchanges loaded in this session. init the repository
//                SymbolRepository.initialize();
                CompanyTable.getSharedInstance().init();
            }
            rebuildAllMarkets();*//*
//            loadCurrencies();
            returnValue = true;
        }
        updateExpiryStatus();
        updateExchangeParams();
        removeUnsubscribedExchanges();
        DataStore.getSharedInstance().createIndexes();
        loadSystemProperties();
        fireExchangesAdded(false);
        if (newExchangesLoaded) { /// new exchanges loaded in this session. init the repository
            CompanyTable.getSharedInstance().init();
        }
        rebuildAllMarkets();

        exchangeParamsTable = null;
//        System.out.println("method ends here");
        return returnValue;
    }*/

    public synchronized void loadSubMarkets(Exchange exchange) {
        try {
            Decompress decompress = new Decompress();
            String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH +"/" + exchange.getSymbol() + "/submarkets_" + Language.getSelectedLanguage() + ".msf";
            ByteArrayOutputStream out;
            out = decompress.setFiles(sLangSpecFileName);
            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));

            String record = null;
            ArrayList<Market> marketList = new ArrayList<Market>();

            do {
                record = null;
                record = in.readLine();
                if (record != null) {
                    String[] fields = record.split(Meta.DS);
                    if (exchange.getSymbol().equals(fields[0])) {
                        String caption = Language.getLanguageSpecificString(fields[2]);
                        caption = UnicodeUtils.getNativeString(caption.trim());
                        Market market = new Market(fields[0], fields[1]);
                        market.setDescription("   " + caption + "   ");
                        try {
                            if (!fields[3].equals("")) {
                            market.setSymbol(fields[3]);
                            } else {
                               market.setSymbol(fields[1]);
                            }
                        } catch (Exception e) {
                            market.setSymbol(fields[1]);
                        }
                        try {
                            market.setDefaultMarket(fields[4].equals("1"));
                            if (market.isDefaultMarket()) {
                                getExchange(exchange.getSymbol()).setDefaultMarket(market);
                                exchange.setDefaultMarket(market);
                            }
                        } catch (Exception e) {
                            market.setDefaultMarket(false);
                        }
                        try {
//                            System.out.println("market ID = "+market.getSymbol() + " ,show in ticker="+fields[5].equals("1"));
                            market.setShowInTicker(fields[5].equals("1"));
                        } catch (Exception e) {
//                            System.out.println("exception in market ID"+market.getSymbol());
//                            e.printStackTrace();
                            market.setShowInTicker(true);
                        }
                        marketList.add(market);
                        market = null;
                        caption = null;
                    }
                    fields = null;
                }
            } while (record != null);

            out = null;
            decompress = null;
            in.close();
            in = null;

            Collections.sort(marketList);
            Market[] subMarkets = exchange.getSubMarkets();
            int initialSize = subMarkets.length;
            subMarkets = marketList.toArray(new Market[0]);
            int newSize = subMarkets.length;
            if (subMarkets.length == 0) {
                exchange.setExpansionStatus(Constants.EXPANSION_STATUS_NO_CHILDREN);
            } else {
                if (initialSize < newSize) {
                    exchange.setExpansionStatus(Constants.EXPANSION_STATUS_COLLAPSED);
                } else {
                    if (exchange.getExpansionStatus() == Constants.EXPANSION_STATUS_NO_CHILDREN) {
                        exchange.setExpansionStatus(Constants.EXPANSION_STATUS_EXPANDED);
                    }
                }

            }
            exchange.setSubMarkets(subMarkets);
        } catch (Exception e) {
            exchange.setSubMarkets(new Market[0]);
            //e.printStackTrace();
        }
    }

    public synchronized void loadBrokers(Exchange exchange) {
        try {
            boolean bIsLangSpec = false;
            String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH +"/brokers/" + exchange.getSymbol() + "/brokers_" + Language.getSelectedLanguage() + ".msf";
            File f = new File(sLangSpecFileName);
            bIsLangSpec = f.exists();

            Decompress decompress = new Decompress();
            ByteArrayOutputStream out;
            //if (bIsLangSpec){
            out = decompress.setFiles(sLangSpecFileName);
            //} else {
            //    out =  decompress.setFiles("system/brokers/"+exchange.getSymbol()+"/brokers.msf");
            // }

            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));

            String record = null;
            ArrayList<Broker> marketList = new ArrayList<Broker>();

            do {
                record = null;
                record = in.readLine();
                if (record != null) {
                    String[] fields = record.split(Meta.DS);
                    record = record.trim();
                    Broker broker = new Broker(fields[0]);
                    if (broker != null) {
                        String description = Language.getLanguageSpecificString(fields[1]);
                        description = UnicodeUtils.getNativeString(description);
                        broker.setName(description);
                        description = null;
                    }
                    marketList.add(broker);
                    fields = null;
                    broker = null;
                }
            } while (record != null);

            out = null;
            decompress = null;
            in.close();
            in = null;

            Collections.sort(marketList);
            Broker[] brokers = marketList.toArray(new Broker[0]);
//            brokers = marketList.toArray(new Broker[0]);
            exchange.setBrokers(brokers);
        } catch (Exception e) {
            exchange.setBrokers(new Broker[0]);
            //e.printStackTrace();
        }
    }

    public Exchange getApplicationExchange() {
        return applicationExchange;
    }

    /*private void loadCurrencies() {
        Enumeration exchanges = getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            CurrencyStore.getSharedInstance().loadCurrencyDetails(exchange.getSymbol());
            exchange = null;
        }
        exchanges = null;
    }*/

    public void addWindowTypes(String frame) {
        try {
            String[] records = frame.split("\\|");

            for (int i = 0; i < records.length; i++) {
                String[] fields = records[i].split(",");
                int[] infoTypes = new int[fields.length - 1];
                for (int j = 1; j < fields.length; j++) {
                    infoTypes[j - 1] = Integer.parseInt(fields[j]);
                }
                Arrays.sort(infoTypes);
                if (fields[0].equals(Constants.APPLICATION_EXCHANGE)) {
                    applicationExchange.setInfoTypes(infoTypes);
                    applicationExchange.setInfoBandWidth();
                } else {
                    Exchange exchange = getExchange(fields[0]);
                    if (exchange != null) {
                        exchange.setInfoTypes(infoTypes);
                        exchange = null;
                    }
                }
            }
            initializeInformationTypes();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void clearTradingWindowTypes() {
        tradingWindows.clear();
//        tradingWindows = "";
    }

    public void addTradingWindowTypes(String frame, byte loginType) {
        try {
            if (frame == null) {
                frame = tradingWindows.get(new Byte(""+loginType));
            }
            String[] records = frame.split("\\|");
            tradingWindows.put(loginType, frame);
            for (int i = 0; i < records.length; i++) {
                String[] fields = records[i].split(",");
                int[] infoTypes = new int[fields.length - 1];
                for (int j = 1; j < fields.length; j++) {
                    infoTypes[j - 1] = Integer.parseInt(fields[j]);
                }
                Arrays.sort(infoTypes);
                if (fields[0].equals(Constants.APPLICATION_EXCHANGE)) {
                    applicationExchange.setTradingInfoTypes(infoTypes, loginType);
                } else {
                    Exchange exchange = getExchange(fields[0]);
                    if (exchange != null) {
                        exchange.setTradingInfoTypes(infoTypes, loginType);
                        exchange = null;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initializeInformationTypes() {
        if (isValidSystemFinformationType(Meta.IT_AllowDynamicIPs)) {
            TCPConnector.PRIMARY_IP_FORCED = false;
            IPSettings.resetIPIndex(); // make sure to go from the beginning next time
            Settings.setItem("DYNAMIC_IPS_ALLOWED", "true");
            IPSettings.includeDynamicIPs();
        } else {
            if (isValidSystemFinformationType(Meta.IT_ForcePrimaryIP)) {
                TCPConnector.PRIMARY_IP_FORCED = true;
            } else {
                TCPConnector.PRIMARY_IP_FORCED = false;
            }
            Settings.setItem("DYNAMIC_IPS_ALLOWED", "false"); //Bug ID <#0022>
        }
        fireExchangeInformationTypesChanged();
    }

    public static boolean isValidIinformationType(String exchangeSymbol, int type) {
        try {
//            String exchangeSymbol = SharedMethods.getExchangeFromKey(key);
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeSymbol);
            return (Arrays.binarySearch(exchange.getInfoTypes(), type) >= 0);
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isValidSystemFinformationType(int type) {
        try {
            Exchange exchange = applicationExchange;
            return (Arrays.binarySearch(exchange.getInfoTypes(), type) >= 0);
        } catch (Exception e) {
            return false;
        }
    }

    private float getMarketDataVersion(String exchange) {
        try {
            return exchangeParamsTable.get(exchange).version;
//            String[] records = list.split("\\|");
//
//            for (int i = 0; i < records.length; i++) {
//                String[] fields = records[i].split(",");
//                if (fields[0].equals(exchange)){
//                    return Float.parseFloat(fields[1]);
//                }
//            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0; // error, do not download
        }
    }

    /*private String addNewTimeZone(String exchange){
        try {
//            String zoneRecord = SymbolMaster.getZoneDescription(exchange);
            String[] fields = zoneRecord.split(Meta.FS);
            String zoneID = fields[0];
            String descriprion = Language.getLanguageSpecificString(fields[1]);
            TimeZoneMap.getInstance().addTimeItem(exchange, zoneID, descriprion);
            return zoneID;
        } catch (Exception e) {
            return null;
        }
    }*/

    private void showDownloadMessage() {
        if (workInProgressIndicator == null) {
            workInProgressIndicator = new WorkInProgressIndicator(WorkInProgressIndicator.DOWNLOADING);
//            }
            Thread thread = new Thread("ExchangeStore") {
                public void run() {
                    if (workInProgressIndicator != null) {
                        workInProgressIndicator.setVisible(true);
                    }
                }
            };
            thread.start();
        }
    }

    public Exchange getExchange(String symbol) {
        if((symbol == null) || symbol.equals("null")){
            return null;
        }
        if (symbol.toUpperCase().endsWith("_D")) {
            symbol = symbol.split("_D")[0];
        }
        try {
            for (int i = 0; i < exchanges.size(); i++) {
                Exchange exchange = exchanges.get(i);
                if (exchange.getSymbol().equals(symbol)) {
                    return exchange;
                }
                exchange = null;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

    //added by madhawie
    public Exchange getExchangeByDisplayExchange(String dispExchange) {
        if((dispExchange == null) || dispExchange.equals("null")){
            return null;
        }
        if (dispExchange.toUpperCase().endsWith("_D")) {
            dispExchange = dispExchange.split("_D")[0];
        }
        try {
            for (int i = 0; i < exchanges.size(); i++) {
                Exchange exchange = exchanges.get(i);
                if (exchange.getDisplayExchange().equals(dispExchange)) {
                    return exchange;
                }
                exchange = null;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }
    /**
     * Remove exchanges wich were there but not in the current lgin string.
     * this happens if the exchange is removed from the user
     */
    private void removeUnsubscribedExchanges() {
        for (int i = exchanges.size() - 1; i >= 0; i--) {
            Exchange exchange = exchanges.get(i);
//            exchange.setExchangeExpired(isExpiredExchange(exchange.getSymbol()));
            if (exchange.isFlaged()) {
                exchange.clearFlag();
                if (!exchange.isDefault()) { // it is possible that this got downgraded
//                    SymbolMaster.removeExchange(exchange.getSymbol()); todo: do not delete market file,if flagged according to new changes.. shanika
                    HistoryFilesManager.removeExchange(exchange.getSymbol());
                }
//                exchange.setExchangeExpired(false);
            } else {
//                if (!isExpiredExchange(exchange.getSymbol())) {
                    exchanges.remove(exchange);
                    SymbolMaster.removeExchange(exchange.getSymbol());
                    HistoryFilesManager.removeExchange(exchange.getSymbol());
                    fireExchangeRemoved(exchange);
//                } else {
//                }
                //
            }
            exchange = null;
        }
    }

    public Exchange getDefaultExchange() {
        for (Exchange exchange : exchanges) {
            if (exchange.isDefault()) {
                return exchange;
            }
        }
        return null;
    }

    public Exchange getExchange(int index) {
        return exchanges.get(index);
    }

    public Exchange getDefaultExchanges(int index) {
        return defaultExchanges.get(index);
    }

    public boolean isDefault(String symbol) {
        for (int i = 0; i < exchanges.size(); i++) {
            Exchange exchange = exchanges.get(i);
            if (exchange.getSymbol().equals(symbol)) {
                return exchange.isDefault();
            }
            exchange = null;
        }
        return false;
    }

    public boolean isValidExchange(String symbol) {
        for (Exchange exchange : exchanges) {
            if (exchange.getSymbol().equals(symbol)) {
                return true;
            }
        }
        return false;
    }

    public String getDescription(String symbol) {
        for (int i = 0; i < exchanges.size(); i++) {
            Exchange exchange = exchanges.get(i);
            if (exchange.getSymbol().equals(symbol)) {
                return exchange.getDescription();
            }
            exchange = null;
        }
        return null;
    }

    public void setCurrency(String exchangeID, String currencyID) {
        try {
            Exchange exchange = getExchange(exchangeID);
            exchange.setActiveCurrency(currencyID);
            fireExchangeCurrencyChanged(exchange);
            exchange = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setTimeZone(String exchangeID, String timeZoneID) {
        try {
            Exchange exchange = getExchange(exchangeID);
            TimeZone timeZone = TimeZone.getTimeZone(timeZoneID);
            exchange.setActiveTimeZone(timeZone);
            fireExchangeTimeZoneChanged(exchange);
            exchange = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public long getZoneAdjustedTimeFor(String exchangeSymbol, long time) {
        try {
            Exchange exchange = getExchange(exchangeSymbol);
            return exchange.getZoneAdjustedTime(time);
        } catch (Exception e) {
            return time;
        }
    }

    public long getPrevZoneAdjustedTimeFor(String exchangeSymbol, long time) {
        try {
            Exchange exchange = getExchange(exchangeSymbol);
            return exchange.getZoneAdjustedTime(time);
        } catch (Exception e) {
            return time;
        }
    }

    public static String getSelectedExchangeID() {
        return selectedExchange;
    }

    public Exchange getSelectedExchange() {
        return getExchange(selectedExchange);
    }

    public static void setSelectedExchangeID(String market) {
        selectedExchange = market;
        if(isExpired(selectedExchange) || isInactive(selectedExchange)){
            Application.getInstance().fireSectedExchangeChanged(null);
        }else {
        try {
            Application.getInstance().fireSectedExchangeChanged(ExchangeStore.getSharedInstance().getExchange(selectedExchange));
        } catch (Exception e) {
            Application.getInstance().fireSectedExchangeChanged(null);
        }
    }
    }

    public Enumeration<Exchange> getExchanges() {
        return new ExchangeList();
    }

//    public boolean isNoneDefaultExchangesAvailable() {
//        return noneDefaultAvailable;
//    }

    public static boolean isSecondaryPathConnectionRequired() {
        return secondaryPathConnectionRequired;
    }

    public void addExchangeListener(ExchangeListener listener) {
        listeners.add(listener);
    }

    public void removeExchangeListener(ExchangeListener listener) {
        listeners.remove(listener);
    }

    private void fireExchangeAdded(Exchange exchange) {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                ExchangeListener listener = listeners.get(i);
                listener.exchangeAdded(exchange);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fireExchangeUpgraded(Exchange exchange) {
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ExchangeListener listener = listeners.get(i);
                if (listener instanceof DataStore) {
                    listener.exchangeUpgraded(exchange);
                    listener = null;
                    break;
                }
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        try {
            for (int i = 0; i < listeners.size(); i++) {
                ExchangeListener listener = listeners.get(i);
                if (listener instanceof DataStore) { // ignore the event in the datastore since it is already fired
                    listener = null;
                    continue;
                }
                listener.exchangeUpgraded(exchange);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fireExchangeDowngraded(Exchange exchange) {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                ExchangeListener listener = listeners.get(i);
                listener.exchangeDowngraded(exchange);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fireExchangesAdded(boolean offlineMode) {
        // fire the event on DataStore first
        for (int i = 0; i < listeners.size(); i++) {
            try {
                ExchangeListener listener = listeners.get(i);
                if (listener instanceof DataStore) {
                    listener.exchangesAdded(offlineMode);
                    listener = null;
                    break;
                }
                listener = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


        try {
            for (int i = 0; i < listeners.size(); i++) {
                try {
                    ExchangeListener listener = listeners.get(i);
                    if (listener instanceof DataStore) { // ignore the event in the datastore since it is already fired
                        listener = null;
                        continue;
                    }
                    listener.exchangesAdded(offlineMode);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
		try {
            PluginStore.firePluginEvent(PluginEvent.EXCHANGES_ADDED);
        } catch (Exception e1) {
			e1.printStackTrace();
        }
	}

    private void loadSystemProperties(){
        try {
            loadExchangeDescriptions();
            loadExchangeIndicesWatchLists();
            loadExchangeConstituent();
            TimeZoneMap.getInstance().loadZoneDetails();
            AssetsStore.getSharedInstance().loadAssetsList();
            com.isi.csvr.trading.datastore.CurrencyStore.getSharedInstance().loadCurrencyDetails();
//            EntitlementsTree.getSharedInstance().loadExchanges();
        } catch(Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void fireExchangesLoaded() {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                ExchangeListener listener = listeners.get(i);
                listener.exchangesLoaded();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fireExchangeRemoved(Exchange exchange) {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                ExchangeListener listener = listeners.get(i);
                listener.exchangeRemoved(exchange);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fireExchangeMustInitialize(Exchange exchange,long newDate, long oldDate) {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                ExchangeListener listener = listeners.get(i);
                listener.exchangeMustInitialize(exchange, newDate, oldDate);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fireMarketStatusChanged(int oldStatus, Exchange exchange) {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                ExchangeListener listener = listeners.get(i);
                listener.marketStatsChanged(oldStatus, exchange);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fireExchangeTimeZoneChanged(Exchange exchange) {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                ExchangeListener listener = listeners.get(i);
                listener.exchangeTimeZoneChanged(exchange);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fireExchangeCurrencyChanged(Exchange exchange) {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                ExchangeListener listener = listeners.get(i);
                listener.exchangeCurrencyChanged(exchange);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fireExchangeMasterFileLoaded(String exchange) {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                ExchangeListener listener = listeners.get(i);
                listener.exchangeMasterFileLoaded(exchange);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void fireExchangeInformationTypesChanged() {
        try {
            for (int i = 0; i < listeners.size(); i++) {
                ExchangeListener listener = listeners.get(i);
                listener.exchangeInformationTypesChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int count() {
        return exchanges.size();
    }

    public int defaultCount() {
        return defaultExchanges.size();
    }

    public int nonDefaultCount() {
        return exchanges.size() - defaultExchanges.size();
    }

    public int expiredCount() {
        return expiredExchanges.size();
    }

    public void clear() {
        exchanges.clear();
    }

    private class ExchangeList implements Enumeration<Exchange> {
        private int index = -1;
        private Exchange exchange;
        public boolean hasMoreElements() {
            exchange = null;
            boolean found = false;
            while(!found){
            try {
                    exchange = exchanges.get(index + 1);
                    if(!exchange.isExpired() && !exchange.isInactive()){
                        found = true;
                    }
                    index ++;
                } catch (Exception e) {
                    return false;
                }
            }
            return true;
            /*try {
                if (exchanges.get(index + 1) != null) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                return false;
            }*/
        }

        public Exchange nextElement() {
            try {
                return exchange;
//                return exchanges.get(++index);
            } catch (Exception e) {
                return null;
            }
        }
    }

    public static boolean isNoneDefaultExchangesAvailable() {
        return noneDefaultAvailable;
    }

    public static boolean isDefaultExchangesAvailable() {
        return defaultAvailable;
    }

    public static boolean isExchangeWatchListsExists() {
        return exchangeWatchListsExists;
    }

    private void loadExchangeParams(String list, String params) {
        exchangeParamsTable = new Hashtable<String, ExchangeParams>();

        //  subscribedList -> CASE,0,C,CA,EGP,1|TDWL,1,T,SA,Asia/Riyadh,SAR,1
        //  parameters -> TDWL,2.0
        String[] records = list.split("\\|");
        for (int i = 0; i < records.length; i++) {
            ExchangeParams exchangeParams = new ExchangeParams(records[i]);
            exchangeParamsTable.put(exchangeParams.exchange, exchangeParams);
            exchangeParams = null;
        }

        records = params.split("\\|");
        for (int i = 0; i < records.length; i++) {
            String[] fields = records[i].split(",");
            ExchangeParams exchangeParams = exchangeParamsTable.get(fields[0]);
            if (exchangeParams != null) {
                exchangeParams.version = Float.parseFloat(fields[1]);
                try {
                    exchangeParams.propVersion = Float.parseFloat(fields[2]);
                } catch (Exception e) {
                }
                try {
                    exchangeParams.dataPort = Integer.parseInt(fields[3]);
                } catch (Exception e) {
                    //e.printStackTrace();
                }
                try {
                    exchangeParams.filePort = Integer.parseInt(fields[4]);
                } catch (Exception e) {
                    //e.printStackTrace();
                }

            }
            exchangeParams = null;
        }
    }

    /**
     * Loading of exchange descriptions is necessary because in case the user has changed the
     * active language of the application, it may display exchange descriptions in the wrong language.
     */
    private void loadExchangeDescriptions() {
        Enumeration exchanges = getExchanges();
        while (exchanges.hasMoreElements()) {
            try {
                Exchange exchangeTemp = (Exchange) exchanges.nextElement();
                String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH +"/" + exchangeTemp.getSymbol() + "/exchange_" + Language.getSelectedLanguage() + ".msf";
                File f = new File(sLangSpecFileName);
                Decompress decompress = new Decompress();
                ByteArrayOutputStream out = decompress.setFiles(sLangSpecFileName);
                decompress.decompress();
                DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
                String record = null;

                do {
                    record = SharedMethods.readLine(in);
                    if (record != null) {
                        record = record.trim();
                        String[] fields = record.split(Meta.DS);
                        if ((fields.length <= 1) || (!exchangeTemp.getSymbol().equals(fields[0])))
                            continue; // it is possible to have garbage in the file. Never trust Servers
                        Exchange exchange = getExchange(fields[0]);
                        if (exchange == null) {
                            exchange = getExchange(fields[0] + "_D"); // may be it is a delayed exchange
                        }
                        if (exchange != null) {
                            String description = Language.getLanguageSpecificString(fields[3]);
                            description = UnicodeUtils.getNativeString(description);
                            exchange.setDescription(description);
                            try {
                                exchange.setUserSubMarketBreakdown(Integer.parseInt(fields[4]) == 1);
                            } catch (Exception e) {
                            }
                            try {
                                exchange.setPriceDecimalPlaces(Byte.parseByte(fields[5]));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            try {
                                exchange.setPriceModificationFactor(Integer.parseInt(fields[6]));
                            } catch (Exception e) {
                                e.printStackTrace();
                                exchange.setPriceModificationFactor(1);
                            }
                            String shortdescription = null;
                            try {
                                shortdescription = Language.getLanguageSpecificString(fields[7]);
                                shortdescription = UnicodeUtils.getNativeString(shortdescription);
                                exchange.setShortDescription(shortdescription);
                            } catch (Exception e) {
                                exchange.setShortDescription(description);
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }  try {
                                short p = Short.parseShort(fields[9]) ;
                                exchange.setEnabledForIndexPanel(p);
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            String displayExchange = null;
                            try {
                                displayExchange = Language.getLanguageSpecificString(fields[10]);
                                displayExchange = UnicodeUtils.getNativeString(displayExchange);
                                exchange.setDisplayExchange(displayExchange);
                            } catch (Exception e) {
                                exchange.setDisplayExchange(description);
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }

                            loadBrokers(exchange);
                            loadSubMarkets(exchange);
                            description = null;
                        }
                        fields = null;
                        exchange = null;
                    }
                } while (record != null);

                out = null;
                decompress = null;
                in.close();
                in = null;
                exchangeTemp = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void loadExchangeIndicesWatchLists() {
        Enumeration exchanges = getExchanges();
        while (exchanges.hasMoreElements()) {
            try {
                Exchange exchangeTemp = (Exchange) exchanges.nextElement();
                String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH +"/" + exchangeTemp.getSymbol() + "/watchlist_" + Language.getSelectedLanguage() + ".msf";
                File f = new File(sLangSpecFileName);
                Decompress decompress = new Decompress();
                ByteArrayOutputStream out = decompress.setFiles(sLangSpecFileName);
                decompress.decompress();
                DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
                String record = null;

                do {
                    record = in.readLine();
                    if (record != null) {
                        record = record.trim();
                        String id = record.split("=")[0];
                        String discription = record.split("=")[1].split("\\|")[0];
                        String symbolList = record.split("=")[1].split("\\|")[1];
                        // according the new request by Jaminda
                        int visibility=1;
                        try{
                            visibility=Integer.parseInt(record.split("=")[1].split("\\|")[2]);
                        }catch(Exception e){
                             visibility=1;
                        }



                        Hashtable tempHash=new Hashtable<String,String>();
                        tempHash.put(discription,symbolList);
                        exchangeTemp.getExchangeIndices().put(id,tempHash);
                        exchangeTemp.setEXGWhatchListVisibility(id,""+visibility);
                        exchangeWatchListsExists = true;

                    }
                } while (record != null);

                out = null;
                decompress = null;
                in.close();
                in = null;
                exchangeTemp = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    // Saudi CR - can be subjected to further modifications - Malithi

    private void loadExchangeConstituent() {
        Enumeration exchanges = getExchanges();
        while (exchanges.hasMoreElements()) {
            try {
                Exchange exchangeTemp = (Exchange) exchanges.nextElement();
                if(exchangeTemp.cashFlowExchageIndicesExists())
                    exchangeTemp.getCashFlowExchangeIndices().clear();
                String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH +"/" + exchangeTemp.getSymbol() + "/constituent_" + Language.getSelectedLanguage() + ".msf";
                Decompress decompress = new Decompress();
                ByteArrayOutputStream out = decompress.setFiles(sLangSpecFileName);
                decompress.decompress();
                DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
                String record = null;
                Hashtable<String,ArrayList<String>> temp = exchangeTemp.getCashFlowExchangeIndices();

                do {
                    record = SharedMethods.readLine(in);
                    if (record != null) {
                        record = record.trim();
                        String[] fields = record.trim().split(Meta.DS);

                        if(fields.length < 1)
                            continue;

                        Stock stock = DataStore.getSharedInstance().getStockByExchangeKey(exchangeTemp.getSymbol(),fields[0]);

                        if(stock == null)
                            continue;

                        String[] indices = fields[1].split(Meta.ID);

                        if(indices.length > 0){
                            for(String index:indices){
                                if(temp.containsKey(index)){
                                    temp.get(index).add(stock.getKey());
                                }else{
                                    ArrayList<String> symbols = new ArrayList<String>();
                                    symbols.add(stock.getKey());
                                    temp.put(index,symbols);
                                }
                            }
                        }
                    }
                } while (record != null);

                Enumeration<String> keys = temp.keys();

                out = null;
                decompress = null;
                in.close();
                in = null;
                temp = null;
                exchangeTemp = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    private void updateExchangeParams() {

//        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        for(int i=0; i < count(); i++){
//        while (exchanges.hasMoreElements()) {
            try {
                Exchange exchangeTemp = getExchange(i);
//                Exchange exchangeTemp = (Exchange) exchanges.nextElement();
                Decompress decompress = new Decompress();
                String sLangSpecFileName =Settings.EXCHANGES_DATA_PATH + "/" + exchangeTemp.getSymbol() + "/exchange_" + Language.getSelectedLanguage() + ".msf";
                File f = new File(sLangSpecFileName);
                ByteArrayOutputStream out = decompress.setFiles(sLangSpecFileName);
                decompress.decompress();
                DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
                String record = null;

                do {
                    record = SharedMethods.readLine(in);
                    if (record != null) {
                        String[] fields = record.trim().split(Meta.DS);

                        if ((fields.length <= 1) || (!exchangeTemp.getSymbol().equals(fields[0]))) {
                            continue;
                        }
                        Exchange exchange = getExchange(fields[0]);

                        if (exchange == null) {
                            exchange = getExchange(fields[0] + "_D"); // may be it is a delayed exchange
                        }
                        if (exchange != null) {
                            if (exchange.getActiveTimeZone() == null) {
                                exchange.setActiveTimeZone(fields[1]);
                                exchange.setDescription(exchange.getSymbol());
                            }
                            exchange.setTimeZoneID(fields[1]);
                            exchange.setActiveCurrency(fields[2]);
                            String description = Language.getLanguageSpecificString(fields[3]);
                            description = UnicodeUtils.getNativeString(description);
                            exchange.setDescription(description);
                            try {
                                exchange.setUserSubMarketBreakdown(Integer.parseInt(fields[4]) == 1);
                            } catch (Exception e) {
                            }
                            try {
                                exchange.setPriceDecimalPlaces(Byte.parseByte(fields[5]));
                            } catch (Exception e) {
                                e.printStackTrace();
                            }
                            description = null;
                            try {
                                exchange.setPriceModificationFactor(Integer.parseInt(fields[6]));
                            } catch (Exception e) {
                                e.printStackTrace();
                                exchange.setPriceModificationFactor(1);
                            }
                            String shortdescription = null;
                            try {
                                shortdescription = Language.getLanguageSpecificString(fields[7]);
                                shortdescription = UnicodeUtils.getNativeString(shortdescription);
                                exchange.setShortDescription(shortdescription);
                            } catch (Exception e) {
                                exchange.setShortDescription(description);
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                             try {
                                short p = Short.parseShort(fields[9]) ;
                                exchange.setEnabledForIndexPanel(p);
                            } catch (Exception e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                             String displayExchange = null;
                            try {
                                displayExchange = Language.getLanguageSpecificString(fields[10]);
                                displayExchange = UnicodeUtils.getNativeString(displayExchange);
                                exchange.setDisplayExchange(displayExchange);
                            } catch (Exception e) {
                                exchange.setDisplayExchange(description);
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                        }
                        fields = null;
                        exchange = null;
                    }
                } while (record != null);

                out = null;
                decompress = null;
                in.close();
                in = null;
                exchangeTemp = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
//        exchanges = null;

    }


    public static void setStopListeningForStockMaster() {
        ExchangeStore.canListenForStockMaster = false;
    }

    public boolean isMarketFileAvailable(String exchangeId){
         for (int i = 0; i < exchanges.size(); i++) {
            Exchange exchange = exchanges.get(i);
            if (exchange.getSymbol().equals(exchangeId)) {
                return exchange.isMarketFileAvailable();
            }
            exchange = null;
        }
        return false;

        /*ExchangeParams exchangeParams = exchangeParamsTable.get(exchange);
        return exchangeParams.masterFileAvailable();*/
    }


    public int getFileStatus(String exchangeId) {
        for (int i = 0; i < exchanges.size(); i++) {
            Exchange exchange = exchanges.get(i);
            if (exchange.getSymbol().equals(exchangeId)) {
                return exchange.getMasterFileStaus();
            }
            exchange = null;
        }
        return 0;

        /*ExchangeParams exchangeParams = exchangeParamsTable.get(exchange);
        return exchangeParams.masterFileAvailable();*/
    }


    private class ExchangeParams {
        String exchange;
        String exchangeCode;
        boolean defaultMarket;
        float version;
        float propVersion;
        int dataPort;
        int filePort;
        String country;
        byte path;
        String contentCMIP;
        boolean isDelayed = false;
        short masterFileStaus;

        ExchangeParams(String frame) {
            try {
                //  subscribedList ->  ADSM,1,A,AE,1,192.168.0.1|DFM,1,D,AE,1,212.162.131.252|
                String[] fields = frame.split(",");
                if (fields[0].toUpperCase().endsWith("_D")) {
                    this.exchange = fields[0].split("_D")[0];
                    this.isDelayed = true;
                } else {
                    exchange = fields[0];
                }
                defaultMarket = fields[1].equals("1");
                exchangeCode = fields[2];
                country = fields[3];
                path = Byte.parseByte(fields[4]);
                contentCMIP = fields[5];
                masterFileStaus = Short.parseShort(fields[6]);
            } catch (Exception e) {
                //e.printStackTrace();
            }
            if (contentCMIP == null) {
                contentCMIP = Settings.getActiveIP();
            }
        }
        public boolean masterFileAvailable(){
            if(masterFileStaus==Meta.FULL_EXCG_FILE_AVAILABLE || masterFileStaus==Meta.PART_EXCG_FILE_AVAILABLE){
                return true;
            }else{
                return false;
            }
        }
    }

    public void rebuildAllMarkets() {
        allMarkets.clear();
        for (Exchange exchange : defaultExchanges) {
            allMarkets.add(exchange);
            if ((exchange.getExpansionStatus() == Constants.EXPANSION_STATUS_EXPANDED) && (exchange.getSubMarkets().length > 0)) {
                for (int i = 0; i < exchange.getSubMarkets().length; i++) {
                    allMarkets.add(exchange.getSubMarkets()[i]);
                }
                //change start
            } else if (exchange.getDefaultMarket() != null) {
                allMarkets.add(exchange.getDefaultMarket());
                //change start
            }
            /*if ((exchange.getExpansionStatus() == Constants.EXPANSION_STATUS_EXPANDED) && (exchange.getSubMarkets().length > 0)){
                for (int i = 0; i < exchange.getSubMarkets().length; i++) {
                    allMarkets.add(exchange.getSubMarkets()[i]);
                }
            }*/
        }
    }

    public List<ExchangeInterface> getAllMarkets() {
        return allMarkets;
    }

    public Market getMarket(String exchangeID, String marketID) {
        Exchange exchange = getExchange(exchangeID);
        for (Market market : exchange.getSubMarkets()) {
            if (market.getMarketID().equals(marketID)) {
                return market;
            }
        }
        return null;
    }

    public Broker getBroker(String exchangeID, String brokerID) {
        Exchange exchange = getExchange(exchangeID);
        for (Broker broker : exchange.getBrokers()) {
            if (broker.getId().equals(brokerID)) {
                return broker;
            }
        }
        return null;
    }

    public byte getDataPath(String exchange) {
        try {
            return getExchange(exchange).getPath();
        } catch (Exception e) {
            return Constants.PATH_PRIMARY;
        }
    }

    public boolean isLoadingFailed() {
        return loadingFailed;
    }

    public ArrayList<TopStockInstrumentType> getTopStockInstruments(String exchange) {
        ArrayList<TopStockInstrumentType> types = new ArrayList<TopStockInstrumentType>();
        try {
            Decompress decompress = new Decompress();
            String sLangSpecFileName =Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/topstocks_" + Language.getSelectedLanguage() + ".msf";
            File f = new File(sLangSpecFileName);
            ByteArrayOutputStream out;
            if (f.exists()) {
                out = decompress.setFiles(sLangSpecFileName);
            } else {
                return types;
            }
            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            String record = null;
            TopStockInstrumentType object;
            do {
                record = null;
                record = in.readLine();
                if (record != null) {
                    try {
                        String[] fields = record.split(Meta.DS);
                        object = new TopStockInstrumentType();
                        object.setExchange(exchange);
                        object.setMarket(fields[0]);
                        object.setInstrumentType(Byte.parseByte(fields[1]));
                        object.setDescryption(UnicodeUtils.getNativeString(fields[2]));
                        types.add(object);
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            } while (record != null);

            out = null;
            decompress = null;
            in.close();
            in = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return types;
    }

    public void loadExchangeFeatures(String exchange){
       Hashtable<String,String> featurestable=new Hashtable<String,String>();
        try {
            ByteArrayOutputStream out;
            Decompress decompress = new Decompress();
            String sLangSpecFileName =Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/features_" + Language.getSelectedLanguage() + ".msf";
            File f = new File(sLangSpecFileName);
            out = decompress.setFiles(sLangSpecFileName);
            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            String record = null;
            TopStockInstrumentType object;
            do {
                record = null;
                record = in.readLine();
                if (record != null) {
                    String[] fields = record.split(Meta.DS);
                    try {
                        featurestable.put(fields[0],fields[1]);

                    } catch (Exception e) {
                        featurestable.put(fields[0],"");
                    }
                }
            } while (record != null);

            out = null;
            decompress = null;
            in.close();
            in = null;
            exchangeFeatures.put(exchange,featurestable);

        } catch (Exception e) {
//            System.out.println("Loading Features Failed.....");
            e.printStackTrace();
        }
    }

    public Enumeration<String> getExchangeFeatures(String exchange){
        try {
            return exchangeFeatures.get(exchange).keys();
        } catch (Exception e) {
            loadExchangeFeatures(exchange);
            return exchangeFeatures.get(exchange).keys();
        }
    }

    public boolean isTopstockAvailable(String exchange) {
        try {
            if (exchange.equals("")) {
                return false;
            }
            if (exchangeFeatures.get(exchange).containsKey("" + Meta.IT_Topstock)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            if (exchange.equals("")) {
                return false;
            }
            loadExchangeFeatures(exchange);
            if (exchangeFeatures.get(exchange).containsKey("" + Meta.IT_Topstock)) {
                return true;
            } else {
                return false;
            }
        }
    }
      public boolean isAnnouncementsRealTimeandSearchAvailable(String exchange) {
        try {
            if (exchange.equals("")) {
                return false;
            }
            if (exchangeFeatures.get(exchange).containsKey("" + Meta.IT_Topstock)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            if (exchange.equals("")) {
                return false;
            }
            loadExchangeFeatures(exchange);
            if (exchangeFeatures.get(exchange).containsKey("" + Meta.IT_Topstock)) {
                return true;
            } else {
                return false;
            }
        }
    }
    public boolean isMarketDepthByPriceAvailable(String exg){
        try {
            if (exg.equals("")) {
                return false;
            }
            loadExchangeFeatures(exg);
            if (exchangeFeatures.get(exg).containsKey("" + Meta.IT_MarketDepthByPrice)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
           return false;
        }
    }

    public boolean isMarketDepthByOrderAvailable(String exg){
         try {
            if (exg.equals("")) {
                return false;
            }
            loadExchangeFeatures(exg);
            if (exchangeFeatures.get(exg).containsKey("" + Meta.IT_MarketDepthByOrder)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isTopStockCriteriaTypeAvailable(String exchange,String type) {
        try {
            loadExchangeFeatures(exchange);
            Hashtable<String,String> tempHash=exchangeFeatures.get(exchange);
            String fullList=tempHash.get(""+Meta.IT_Topstock);
            String criteriaList=fullList.split("\\|")[0];
            if(criteriaList.contains(type)){
                return true;
            }else{
                return false;
            }
            
        } catch (Exception e) {
            return false;
        }

    }

    public boolean ismarketSummaryAvailable(String exchange){
         try {
            loadExchangeFeatures(exchange);
            if (exchangeFeatures.get(exchange).containsKey("" + Meta.IT_Market_Summary)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isAnnouncementMenuAvailable(String key){
             try {
            String exchange = SharedMethods.getExchangeFromKey(key);
            if (exchange.equals("")) {
                return false;
            }
            loadExchangeFeatures(exchange);
            if (exchangeFeatures.get(SharedMethods.getExchangeFromKey(key)).containsKey("" + Meta.IT_AnnouncementSearch)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }

    public boolean isAnnouncementsAvailableforExchange(String exchange) {
        try {
            if (exchange.equals("")) {
                return false;
            }
            loadExchangeFeatures(exchange);
            if (exchangeFeatures.get(exchange).containsKey("" + Meta.IT_AnnouncementSearch)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;
        }

    }
    public boolean isMarketIndicesAvailable(String exchange){
        try {
            loadExchangeFeatures(exchange);
            if (exchangeFeatures.get(exchange).containsKey("" + Meta.IT_Market_indices)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;

        }
    }
    public boolean isRadarScreenAvailable(String exchange){
        try {
            loadExchangeFeatures(exchange);
            if (exchangeFeatures.get(exchange).containsKey("" + Meta.IT_Strategy_Watchlist)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
            return false;

        }
    }
    public boolean checkFeatureAvailability(String exchange,int type){
        try {
            if (exchange.equals("")) {
                return false;
            }
            loadExchangeFeatures(exchange);
            if (exchangeFeatures.get(exchange).containsKey("" + type)) {
                return true;
            } else {
                return false;
            }
        } catch (Exception e) {
               return false;

        }
    }

    public List<String> getExpiredExchanges() {
        return expiredExchanges;
    }

    public void setExpiredExchanges(String expiredExchanges) {
        this.expiredExchanges.clear();
        if((expiredExchanges == null) || (expiredExchanges.equals(""))){
            return;
        }
        try {
            String[] data = expiredExchanges.split("\\|");
            for (int i = 0; i < data.length; i++) {
                this.expiredExchanges.add(data[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
//        updateExpiryStatus();
    }

    public List<String> getInactiveExchanges() {
        return inactiveExchanges;
    }

    public void setInactiveExchanges(String expiredExchanges) {
        this.inactiveExchanges.clear();
        if((expiredExchanges == null) || (expiredExchanges.equals(""))){
            return;
        }
        try {
            String[] data = expiredExchanges.split("\\|");
            for (int i = 0; i < data.length; i++) {
                this.inactiveExchanges.add(data[i]);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void updateExpiryStatus(){
        for (int i = exchanges.size() - 1; i >= 0; i--) {
            Exchange exchange = exchanges.get(i);
            exchange.setExchangeExpired(isExpiredExchange(exchange.getSymbol()));
            exchange = null;
        }
    }

    private boolean isExpiredExchange(String exchange) {
        boolean isExpired=false;
        try {
            for (String sExchange : expiredExchanges) {
                String[] arr=sExchange.split(",");
                for (int i=0;i<arr.length;i++) {
                try {
                        if (arr[i].equals(exchange)) {
                            isExpired=true;
                    }
                } catch (Exception e) {
                        isExpired=false;
                    }
                }
            }
        } catch (Exception e) {
            isExpired=false;
        }
        return isExpired;
    }

    private boolean isInactiveExchange(String exchange) {
        try {
            for (String sExchange : inactiveExchanges) {
                try {
                    if (sExchange.equals(exchange)) {
                        return true;
                    }
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    public void setExpiaryDates(String list) {
        try {
            String[] expDate = list.split("\\|");
            for (int i = 0; i < expDate.length; i++) {
                try {
//                    System.out.println("Processing " + expDate[i]);
                    String[] temp = (expDate[i]).split(",");
                    Exchange exg = getExchange(temp[0]);
                    exg.setExpiryDate(formatDate(temp[1]));
                } catch (Exception e) {
                    // ignore any exceptions
                }
//                formatDate(temp[1]);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private String formatDate(String date) {
        String formatteddate = date.substring(6, 8) + "/" + date.substring(4, 6) + "/" + date.substring(0, 4);
        return formatteddate;
    }

    public static boolean isExpired(String exchange) {
        try {
            return getSharedInstance().getExchange(exchange).isExpired();
            //return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isInactive(String exchange) {
        try {
            return getSharedInstance().getExchange(exchange).isInactive();
            //return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isDelayed(String exchange) {
        try {
            return getSharedInstance().getExchange(exchange).isDelayed();
            //return true;
        } catch (Exception e) {
            return false;
        }
    }

    public boolean checkMarketTimeNSalesAvailability(){
        boolean availability=false;
        Enumeration exgEnum=getExchanges();
        while(exgEnum.hasMoreElements()){
            Exchange exg=(Exchange)exgEnum.nextElement();
            if((exg.isValidIinformationType(Meta.IT_MarketTimeAndSales))&&(exg.isDefault())){
                  availability=true;
            }

        }
        return availability;
    }
    /*public void saveExchanges(){
        System.out.println("Saving");
        XMLElement root = new XMLElement();
        root.setName("Exchanges");
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            XMLElement exchangeElement =  new XMLElement();
            root.addChild(exchangeElement);
            exchangeElement.setName(exchange.getSymbol());
            exchangeElement.setAttribute("symbol" , exchange.symbol);
            exchangeElement.setAttribute("symbol" , exchange.code);
            exchangeElement.setAttribute("symbol" , exchange.description);
            exchangeElement.setAttribute("symbol" , exchange.shortDescription);
            exchangeElement.setAttribute("symbol" , exchange.country);
            exchangeElement.setAttribute("symbol" , exchange.satelliteDataPort);
            exchangeElement.setAttribute("symbol" , exchange.satelliteFilePort);
            exchangeElement.setAttribute("symbol" , exchange.path);
            exchangeElement.setAttribute("symbol" , exchange.def);
            exchangeElement.setAttribute("symbol" , exchange.userSubMarketBreakdown);
            exchangeElement.setAttribute("symbol" , exchange.timensalesEnabled);
            exchangeElement.setAttribute("symbol" , exchange.currentEODDate);
            exchangeElement.setAttribute("symbol" , exchange.timeZoneID);
            exchangeElement.setAttribute("symbol" , exchange.activeTimeZone);
            exchangeElement.setAttribute("symbol" , exchange.activeCurrency);
            exchangeElement.setAttribute("symbol" , exchange.marketStatus);
            exchangeElement.setAttribute("symbol" , exchange.lastEODDate);
            exchangeElement.setAttribute("symbol" , exchange.marketDateInt);
            exchangeElement.setAttribute("symbol" , exchange.prevMarketDate);
            exchangeElement.setAttribute("symbol" , exchange.marketTime);
            exchangeElement.setAttribute("symbol" , exchange.marketDateTime);
            exchangeElement.setAttribute("symbol" , exchange.volume);
            exchangeElement.setAttribute("symbol" , exchange.turnover);
            exchangeElement.setAttribute("symbol" , exchange.noOfTrades);
            exchangeElement.setAttribute("symbol" , exchange.symbolsTraded);
            exchangeElement.setAttribute("symbol" , exchange.noOfUps);
            exchangeElement.setAttribute("symbol" , exchange.noOfDown);
            exchangeElement.setAttribute("symbol" , exchange.noOfNoChange);
            exchangeElement.setAttribute("symbol" , exchange.expansionStatus);
            exchangeElement.setAttribute("symbol" , exchange.priceDecimalPlaces);
            exchangeElement.setAttribute("symbol" , exchange.priceModificationFactor);
            exchangeElement.setAttribute("symbol" , exchange.lastIntradayHistorydate);
            exchangeElement.setAttribute("symbol" , exchange.bidAskMax);
            exchangeElement.setAttribute("symbol" , exchange.bidAskMin);
        }
        exchanges = null;

        try {
            Writer writer = new BufferedWriter(new FileWriter("exchanges.txt"));
            root.write(writer);
            writer.flush();
        } catch (IOException e) {
            e.printStackTrace();
        }
        System.out.println("Saved");
    }*/
}
