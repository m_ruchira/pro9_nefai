// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr;

import com.isi.csvr.shared.*;
import com.isi.csvr.trading.IPSettings;
import com.isi.csvr.trading.datastore.Broker;
import com.isi.csvr.trading.datastore.Brokers;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.virtualkeyboard.VirtualKeyboardTextField;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.util.FlexNullLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.util.Arrays;
import java.util.Enumeration;

/**
 * A Swing-based dialog class for the user authentcation.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class Login extends JDialog
        implements ActionListener, WindowListener, KeyListener, Themeable {

    public static final int MODE_PRICE_SERVERS = 0;
    public static final int MODE_TRADING_SERVERS = 1;

    private JPanel jPanel1;
    private JLabel lblUser = new JLabel();
    private JLabel lblPass = new JLabel();
    private JLabel lblBroker = new JLabel();
    private TWComboBox cmbBrokers;
    private JTextField txtUser = new JTextField();
    //    private JPasswordField txtPassN;
    private TWPasswordField txtPassK;// = new VirtualKeyboardTextField();
    //    private VirtualKeyboardTextField txtPassK;// = new VirtualKeyboardTextField();
    private TWButton btnOk = new TWButton();
    private TWButton btnCancel = new TWButton();
    //private VirtualKeyboard virtualKeyboard;
    private boolean g_bStatus;
    private int mode;
    private static boolean tradeLoginSucess = false;
    int buttonOffset = 0;
    private JCheckBox saveUsername = new JCheckBox();
    private JCheckBox virtualKeyBoard = new JCheckBox();
    private JCheckBox savePriceUNAndPWD = new JCheckBox();
    private ImageIcon banner;
    private JButton linkToConfig;

    /**
     * Constructs a new instance.
     *
     * @param parent
     * @param title
     * @param modal
     */
    public Login(Frame parent, String title, int mode, boolean modal) {
        super(parent, title, modal);
        try {
            if (mode == MODE_TRADING_SERVERS) {
                setTitle(Language.getString("TRADE_LOGIN"));
            } else {
                setTitle(Language.getString("USER_LOGIN"));
            }
            this.mode = mode;
            jbInit();
            pack();
            Theme.registerComponent(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Constructs a new non-modal unparented instance with a blank title.
     */
    public Login(Frame oParent, int mode) {
        this(oParent, "", mode, true);

    }


    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        if (cmbBrokers != null) {
            SwingUtilities.updateComponentTreeUI(cmbBrokers);
        }
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {
        banner = new ImageIcon("images/theme" + Theme.getID() + "/login.jpg");
        jPanel1 = new JPanel() {
            protected void paintComponent(Graphics g) {
                super.paintComponent(g);
                g.drawImage(banner.getImage(), 0, 0, null);
            }
        };

        linkToConfig=new JButton(Language.getString("LINK_TO_CONFIGURE"));
//        linkToConfig.setFont(new Font("Arial", Font.BOLD, 13));
        linkToConfig.setHorizontalTextPosition(SwingConstants.LEADING);
        linkToConfig.setHorizontalAlignment(SwingConstants.LEADING);
        linkToConfig.setCursor(new Cursor(Cursor.HAND_CURSOR));
//        linkToConfig.setForeground(Color.BLUE);
        linkToConfig.setBorder(BorderFactory.createEmptyBorder());
        linkToConfig.addActionListener(this);
//        banner = new ImageIcon("images/common/logo_en.jpg");
        linkToConfig.setBackground(jPanel1.getBackground());
        jPanel1.setOpaque(true);
//        txtPassN = new JPasswordField();
        /*if (mode == MODE_TRADING_SERVERS) {
            txtPassK = new VirtualKeyboardTextField(this, TWControl.isUseVirtualKeyBoard(),
                    TWControl.getBooleanItem("VIRTUAL_KEYBOARD_RANDOM"));
            if (!TWControl.isUseVirtualKeyBoard()) {
                txtPassK.addKeyListener(this);
            }
        } else {
            txtPassK = new VirtualKeyboardTextField(this, false, false);
            txtPassK.addKeyListener(this);
        }*/
        if (mode == MODE_TRADING_SERVERS) {
            txtPassK = new TWPasswordField(this, TWControl.isUseVirtualKeyBoard(),
                    TWControl.getBooleanItem("VIRTUAL_KEYBOARD_RANDOM"));
            if (!TWControl.isUseVirtualKeyBoard()) {
                txtPassK.addKeyListener(this);
            }
        } else {
            txtPassK = new TWPasswordField(this, false, false);
            txtPassK.addKeyListener(this);
        }
//        txtPass.setDocument(new LimitedLengthDocument(20));
        txtUser.setDocument(new LimitedLengthDocument(20));
        txtUser.addKeyListener(this);
        txtPassK.setUsernameFiled(txtUser);
//        txtPassK.addKeyListener(this);
//        txtPassN.addKeyListener(this);


        jPanel1.setLayout(new FlexNullLayout());
        lblUser.setText(Language.getString("USER"));
        lblPass.setText(Language.getString("PASSWORD"));
        lblUser.setBounds(new Rectangle(13, 21, 76, 17));
        lblPass.setBounds(new Rectangle(13, 54, 76, 17));
        txtUser.setBounds(new Rectangle(97, 21, 165, 21));
//        txtPassN.setBounds(new Rectangle(97, 51, 165, 21));
        txtPassK.setBounds(new Rectangle(97, 51, 165, 21));
        //txtUser.setNextFocusableComponent(txtPass);
        if (mode == MODE_TRADING_SERVERS) {
            lblBroker = new JLabel(Language.getString("BROKER"));
            cmbBrokers = getBrokerList();
            saveUsername.setText(Language.getString("REMEMBER_USERNAME"));
            cmbBrokers.setActionCommand("B");
            virtualKeyBoard.setText(Language.getString("VIRTUAL_KEYBOARD"));
            cmbBrokers.addActionListener(this);
            lblBroker.setBounds(new Rectangle(13, 86, 165, 21));
            cmbBrokers.setBounds(new Rectangle(97, 86, 165, 21));
            if (!TWControl.isSingleSingnOn()) {
                saveUsername.setBounds(13, 121, 280, 21);
                virtualKeyBoard.setBounds(13, 146, 280, 21);
                linkToConfig.setBounds(new Rectangle(13, 171, 280, 21));
            } else {
                saveUsername.setBounds(13, 75, 280, 21);
                virtualKeyBoard.setBounds(13, 100, 280, 21);
                linkToConfig.setBounds(new Rectangle(13, 125, 280, 21));
            }
            if(!TWControl.isSFCSpecificChangesAddToAccSum()){
                virtualKeyBoard.setSelected(TWControl.isUseVirtualKeyBoard());
            }else{
                if ((Settings.getItem("IS_VIRTUAL_KEYBOARD_SAVED") != null) && (Settings.getItem("IS_VIRTUAL_KEYBOARD_SAVED").equals("true"))) {
                    virtualKeyBoard.setSelected(true);
                }
            }
            if ((Settings.getItem("IS_TRADE_USER_SAVED") != null) && (Settings.getItem("IS_TRADE_USER_SAVED").equals("true"))) {
                saveUsername.setSelected(true);
            }
            saveUsername.addActionListener(this);
            virtualKeyBoard.addActionListener(this);
            saveUsername.setOpaque(false);
            virtualKeyBoard.setOpaque(false);
            broker_actionPerformed(); // load initial ips to the list
            buttonOffset = 85+25;

            if (!TWControl.isSingleSingnOn()) {
                btnCancel.setBounds(new Rectangle(184, 86 + buttonOffset, 78, 23));
                btnOk.setBounds(new Rectangle(97, 86 + buttonOffset, 78, 23));
        } else {
                btnCancel.setBounds(new Rectangle(184, 150, 78, 23));
                btnOk.setBounds(new Rectangle(97, 150, 78, 23));
            }
        } else {
            linkToConfig.setBounds(new Rectangle(8, 111, 280, 21));
            savePriceUNAndPWD.setBounds(8, 86, 280, 21);
            savePriceUNAndPWD.setText(Language.getString("SAVE_USERNAME_AND_PASSWORD"));
            savePriceUNAndPWD.addActionListener(this);
            savePriceUNAndPWD.addKeyListener(this);
            savePriceUNAndPWD.setOpaque(false);
            buttonOffset = 25;
            btnCancel.setBounds(new Rectangle(184, 135, 78, 23));
            btnOk.setBounds(new Rectangle(97, 135, 78, 23));
        }

        if (!TWControl.isSingleSingnOn()) {
            btnCancel.setBounds(new Rectangle(184, 86 + buttonOffset, 78, 23));
        } else {
            btnCancel.setBounds(new Rectangle(184, 120, 78, 23));
        }

        JPanel oPanel = (JPanel) this.getContentPane();
        oPanel.registerKeyboardAction(this, "O", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
        oPanel.registerKeyboardAction(this, "C", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);
        oPanel.setOpaque(true);

        btnCancel.setActionCommand("C");
        btnCancel.addActionListener(this);
        btnCancel.addKeyListener(new KeyAdapter() {
            public void keyPressed(KeyEvent e) {
                if (e.getKeyCode() == KeyEvent.VK_ENTER)
                    btnCancel_actionPerformed();
            }
        });

        btnCancel.setText(Language.getString("CANCEL"));
        btnOk.setText(Language.getString("OK"));
        btnOk.addActionListener(this);
        btnOk.setActionCommand("O");
        if (!TWControl.isSingleSingnOn()) {
            btnOk.setBounds(new Rectangle(97, 86 + buttonOffset, 78, 23));
        } else {
            btnOk.setBounds(new Rectangle(97, 120, 78, 23));
        }


        getContentPane().add(jPanel1);
        jPanel1.add(lblUser, null);
        jPanel1.add(lblPass, null);
        jPanel1.add(txtUser, null);


        if (mode == MODE_TRADING_SERVERS) {
            jPanel1.add(txtPassK, null);
            if (!TWControl.isSingleSingnOn()) {
                jPanel1.add(lblBroker, null);
                jPanel1.add(cmbBrokers, null);
            }
            jPanel1.add(saveUsername, null);
            if (TWControl.isVirtualKeyBoardControllable()) {
                jPanel1.add(virtualKeyBoard, null);
            }
            if (Settings.getTradeUser() != null) {
                if ((Settings.getItem("IS_TRADE_USER_SAVED") != null) && (Settings.getItem("IS_TRADE_USER_SAVED").equals("true"))) {
                    saveUsername.setSelected(true);
                    txtUser.setText(Settings.getTradeUser());
                }
                try {
                    SharedMethods.printLine("Trade Broker " + Settings.getItem("TRADE_BROKER"));
                    cmbBrokers.setSelectedIndex(Integer.parseInt(Settings.getItem("TRADE_BROKER")));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } else {
            jPanel1.add(txtPassK, null);
            if (Settings.getItemFromBulk("DEFAULT_USER") != null) {
                txtUser.setText(Settings.getItemFromBulk("DEFAULT_USER"));
//                txtPassK.setUsername(Settings.getItem("DEFAULT_USER"));
            }
            jPanel1.add(savePriceUNAndPWD, null);
//            jPanel1.add(linkToConfig, null);
        }
        jPanel1.add(linkToConfig, null);
        jPanel1.add(btnOk, null);
        jPanel1.add(btnCancel, null);

        /*if (isTradeLoginSucess()) { // if logged in once, no need to select again
            cmbBrokers.setEnabled(false);
        }*/

        GUISettings.applyOrientation(jPanel1);
        this.addWindowListener(this);
        applyTheme();
        setResizable(false);
    }


    private TWComboBox getBrokerList() {
        int brokerCount = Brokers.getSharedInstance().getBrokerCount();
        Enumeration brokers = Brokers.getSharedInstance().getBrokers();
        Object[] brokerList = new Object[brokerCount];
        int i = 0;

        while (brokers.hasMoreElements()) {
            Broker broker = (Broker) brokers.nextElement();
            TWComboItem comboItem = new TWComboItem(broker.getId(), broker.getDecription());
            brokerList[i] = comboItem;
            i++;
            broker = null;
            comboItem = null;
        }
        Arrays.sort(brokerList);
        TWComboBox cmbBrokers = new TWComboBox(brokerList);
        return cmbBrokers;
    }

    /**
     * Show the password dialog
     */
    public boolean showDialog() {
        g_bStatus = false;
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        if (mode == MODE_TRADING_SERVERS) {
            if (!TWControl.isSingleSingnOn()) {
                this.setSize(290, 255);
            } else {
                this.setSize(290, 210);
            }
        } else {
            this.setSize(290, 200);
        }
        this.setLocationRelativeTo(Client.getInstance().getFrame());
        /*this.setBounds((int) (oDm.getWidth() / 2 - 140),
            (int) (oDm.getHeight() / 2 - 90), 280, 230+buttonOffset);*/
        this.setVisible(true);
        return g_bStatus;
    }

    /**
     * Ok button is pressed
     */
    void btnOk_actionPerformed() {
        String password;

        password = new String(txtPassK.getPassword()).trim();

        if ((password.equals("")) || (txtUser.getText().trim().equals(""))) {
            Object[] options = {Language.getString("OK")};

            int n = JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                    Language.getString("MSG_PASSWORD_AND_USERNAME"),
                    Language.getString("ERROR"),
                    JOptionPane.OK_OPTION,
                    JOptionPane.ERROR_MESSAGE,
                    null,
                    options,
                    options[0]);
        } else {
            g_bStatus = true;
            if (mode == MODE_TRADING_SERVERS) {
                Settings.setItem("TRADE_BROKER", "" + cmbBrokers.getSelectedIndex());
                 Settings.setItemInBulk("TRADE_USER",txtUser.getText().trim());
            } else {
                if (savePriceUNAndPWD.isSelected()) {
                    Settings.setItemInBulk("DEFAULT_USER",txtUser.getText().trim());
                    Settings.setItemInBulk("PASSWORD",password);
                }
            }
            this.hide();
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("C")) {
            btnCancel_actionPerformed();
        } else if (e.getActionCommand().equals("O")) {
            btnOk_actionPerformed();
        } else if (e.getActionCommand().equals("B")) {
            broker_actionPerformed();
        } else if (e.getSource() == (saveUsername)) {
            if (saveUsername.isSelected()) {
                Settings.setItem("IS_TRADE_USER_SAVED", "true");
            } else {
                Settings.setItem("IS_TRADE_USER_SAVED", "false");
            }
        } else if (e.getSource() == (virtualKeyBoard)) {
            TWControl.setUseVirtualKeyBoard(virtualKeyBoard.isSelected());
            txtPassK.setUseVirtualKeyboard(virtualKeyBoard.isSelected());

            if (virtualKeyBoard.isSelected()) {
                Settings.setItem("IS_VIRTUAL_KEYBOARD_SAVED", "true");
            } else {
                Settings.setItem("IS_VIRTUAL_KEYBOARD_SAVED", "false");
            }

            if (virtualKeyBoard.isSelected()) {
                txtPassK.removeKeyListener(this);
            } else {
                txtPassK.addKeyListener(this);
            }
        } else if (e.getSource() == (savePriceUNAndPWD)) {
            if (savePriceUNAndPWD.isSelected()) {
//                Settings.setItem("DEFAULT_USER",);
            }
        }else if(e.getSource().equals(linkToConfig)){
             Client.getInstance().mnu_Config_ActionPerformed();
//             this.hide();
        }
    }

    private void broker_actionPerformed() {

        TWComboItem comboItem = (TWComboItem) cmbBrokers.getSelectedItem();
        Broker broker = Brokers.getSharedInstance().getBroker(comboItem.getId());
        if (!TWControl.isSingleSingnOn()) {
            Object[] ips = broker.getIPs();
            IPSettings.getSharedInstance().removeIPS();
            for (int i = 0; i < ips.length; i++) {

                IPSettings.getSharedInstance().addIP((String) ips[i]);
            }
        }
        Brokers.getSharedInstance().setSelectedBroker(broker.getId());
        broker = null;
        comboItem = null;

    }

    /**
     * Cancel button is pressed
     */
    void btnCancel_actionPerformed() {
        txtUser.setText("");
        txtPassK.setText("");
//        txtPassK.setUsername("");
//        txtPassN.setText("");
        g_bStatus = false;
        this.hide();
    }

    /*
    * Returns the user name
    */
    public String getPassword() {
        String password;

        password = new String(txtPassK.getPassword()).trim();

        return password;
    }

    /*
    * Returns the password
    */
    public String getUserName() {
        return txtUser.getText();
    }

    public static boolean isTradeLoginSucess() {
        return tradeLoginSucess;
    }

    public static void setTradeLoginSucess(boolean tradeLoginSucess) {
        Login.tradeLoginSucess = tradeLoginSucess;
    }

    public void windowOpened(WindowEvent e) {
        if (!txtUser.getText().trim().equals("")) {
            txtUser.transferFocus();
//            txtPassK.setUsername(txtUser.getText());
        }
    }

    public void windowClosing(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowActivated(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }


    public void keyPressed(KeyEvent e) {
//        if(e.getSource() == txtUser){
//        txtPassK.setUsername(txtUser.getText());
//        }
    }

    public void keyReleased(KeyEvent e) {

        if (e.getKeyChar() == '\n') {
            e.consume();
            btnOk_actionPerformed();
        }
    }

    public void keyTyped(KeyEvent e) {

    }
}
