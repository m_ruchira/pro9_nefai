package com.isi.csvr.options;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

public class OptionChainRenderer extends TWBasicTableRenderer {

    public static final int TYPE_CALL = 0;
    public static final int TYPE_PUT = 1;
    public static final int TYPE_STRIKE = 2;

    private String[] g_asColumns;
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private SimpleDateFormat g_oDateTimeFormat = new SimpleDateFormat(" dd/MM/yyyy HH:mm:ss ");
    private SimpleDateFormat g_oTimeFormat = new SimpleDateFormat(" HH:mm:ss ");

    private String[] g_asSessions = new String[3];
    private String g_sNA = Language.getString("LBL_NA");// "NA";

    private static Color g_oUpBGColor;
    private static Color g_oDownBGColor;
    private static Color g_oUpFGColor;
    private static Color priceFGColor;
    private static Color g_oDownFGColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oPutsFG1;
    private static Color g_oPutsBG1;
    private static Color g_oPutsFG2;
    private static Color g_oPutsBG2;
    private static Color g_oStrikeBG1;
    private static Color g_oStrikeFG1;
    private static Color g_oStrikeBG2;
    private static Color g_oStrikeFG2;
    private static Color g_oCallsBG1;
    private static Color g_oCallsFG1;
    private static Color g_oCallsBG2;
    private static Color g_oCallsFG2;
    private static Color g_oBGBid1;
    private static Color g_oBGBid2;
    private static Color g_oBGAsk1;
    private static Color g_oBGAsk2;

    private static int modelColumn;
    private DefaultTableCellRenderer lblRenderer;

    private static Color nearTheMoneyFG;
    private static Color nearTheMoneyBG;
    private static Color strikeColorBG;
    private static Color strikeColorFG;

    private TWDecimalFormat oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
    private TWDecimalFormat oStrikePriceFormat = new TWDecimalFormat(" ###,##0.00 ");
    private DecimalFormat oQuantityFormat = new DecimalFormat(" ###,##0 ");
    private TWDecimalFormat oChangeFormat = new TWDecimalFormat(" ###,##0.00 ");
    private DecimalFormat oPChangeFormat = new DecimalFormat(" ###,##0.00 ");

    private static double doubleValue;
    private static long updateDirection;
    private static long now;
    String key = "";

    public OptionChainRenderer(String key) {
        this.key = key;
        oStrikePriceFormat = SharedMethods.getDecimalFormat(key);
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        g_asColumns = asColumns;
        g_asRendIDs = asRendIDs;

        reload();
        try {
            g_asSessions[0] = Language.getString("LBL_SESSION0");
            g_asSessions[1] = Language.getString("LBL_SESSION1");
            g_asSessions[2] = Language.getString("LBL_SESSION2");
            g_sNA = Language.getString("LBL_NA");
        }
        catch (Exception e) {
            g_asSessions[0] = "1";
            g_asSessions[1] = "2";
            g_asSessions[2] = "3";
        }

        if (Language.isLTR()) {
            g_iStringAlign = JLabel.LEFT;
            g_iNumberAlign = JLabel.RIGHT;
        }
        else {
            g_iStringAlign = JLabel.RIGHT;
            g_iNumberAlign = JLabel.LEFT;
        }
    }

    public static void reload() {
        reloadRenderer();
        try {
            //            g_oUpFGColor = Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR");
            g_oUpBGColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            //            g_oDownFGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR");
            g_oDownBGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");

            g_oUpFGColor = Theme.getColor("OPTION_TABLE_UP_FGCOLOR");
            priceFGColor = Theme.getColor("OPTION_TABLE_PRICE_FGCOLOR");
//            priceFGColor = Color.white;
            g_oDownFGColor = Theme.getColor("OPTION_TABLE_DOWN_FGCOLOR");

            g_oStrikeBG1 = Theme.getColor("OPTION_TABLE_STRIKE_BGCOLOR1");
            g_oStrikeFG1 = Theme.getColor("OPTION_TABLE_STRIKE_FGCOLOR1");
            g_oStrikeBG2 = Theme.getColor("OPTION_TABLE_STRIKE_BGCOLOR2");
            g_oStrikeFG2 = Theme.getColor("OPTION_TABLE_STRIKE_FGCOLOR2");

            g_oPutsBG1 = Theme.getColor("OPTION_TABLE_PUTS_BGCOLOR1");
            g_oPutsFG1 = Theme.getColor("OPTION_TABLE_PUTS_FGCOLOR1");
            g_oPutsBG2 = Theme.getColor("OPTION_TABLE_PUTS_BGCOLOR2");
            g_oPutsFG2 = Theme.getColor("OPTION_TABLE_PUTS_FGCOLOR2");

            g_oCallsBG1 = Theme.getColor("OPTION_TABLE_CALLS_BGCOLOR1");
            g_oCallsFG1 = Theme.getColor("OPTION_TABLE_CALLS_FGCOLOR1");
            g_oCallsBG2 = Theme.getColor("OPTION_TABLE_CALLS_BGCOLOR2");
            g_oCallsFG2 = Theme.getColor("OPTION_TABLE_CALLS_FGCOLOR2");

            g_oSelectedFG = Theme.getColor("OPTION_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("OPTION_TABLE_SELECTED_BGCOLOR");

            nearTheMoneyBG = Theme.getColor("OPTION_IN_MONEY_BG_COLOR");
            nearTheMoneyFG = Theme.getColor("OPTION_IN_MONEY_FG_COLOR");
            strikeColorBG = Theme.getColor("STRIKE_BG_COLOR");
            strikeColorFG = Theme.getColor("STRIKE_FG_COLOR");

            g_oBGBid1 = Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1");
            g_oBGBid2 = Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR2");
            g_oBGAsk1 = Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1");
            g_oBGAsk2 = Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR2");
        }
        catch (Exception e) {
            g_oUpFGColor = Color.black;
            priceFGColor = Color.white;
            g_oDownFGColor = Color.black;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;

            g_oStrikeBG1 = Color.white;
            g_oStrikeFG1 = Color.black;
            g_oStrikeBG2 = Color.white;
            g_oStrikeFG2 = Color.black;

            g_oPutsBG1 = Color.white;
            g_oPutsFG1 = Color.black;
            g_oPutsBG2 = Color.white;
            g_oPutsFG2 = Color.black;

            g_oCallsBG1 = Color.white;
            g_oCallsFG1 = Color.black;
            g_oCallsBG2 = Color.white;
            g_oCallsFG2 = Color.black;

            g_oBGBid1 = Color.black;
            g_oBGBid2 = Color.black;
            g_oBGAsk1 = Color.black;
            g_oBGAsk2 = Color.black;

            nearTheMoneyFG = Color.white;
            nearTheMoneyBG = Color.black;
            strikeColorBG = Color.GRAY;
            strikeColorFG = Color.GRAY;
        }
    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        Color foreground = null;
        Color background = null;
        String sColName = null;
        Date dDateTime = null;

        lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);
        modelColumn = table.convertColumnIndexToModel(column);

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (!isCustomThemeEnabled) {
            if (isSelected) {
                if (modelColumn == OptionsModel.STRIKE_PRICE_PRICE_COLUMN) {
                    if (row % 2 == 0) {
                        foreground = g_oStrikeFG1;
                        background = g_oStrikeBG1;
                    }
                    else {
                        foreground = g_oStrikeFG2;
                        background = g_oStrikeBG2;
                    }
                }
                else {
                    foreground = g_oSelectedFG;
                    background = g_oSelectedBG;
                }
            }
            else {
                if (modelColumn == OptionsModel.STRIKE_PRICE_PRICE_COLUMN) {
                    foreground = strikeColorFG;
                    background = strikeColorBG;
                }
                else if ((modelColumn > 0) && modelColumn < OptionsModel.STRIKE_PRICE_PRICE_COLUMN) {
                    if (this.key != null) {
                        Stock stock = DataStore.getSharedInstance().getStockObject(key);
                        if (stock != null) {
                            if (((OptionsModel) table.getModel()).getStrikePrice(row) < stock.getPreviousClosed()) {
                                foreground = nearTheMoneyFG;
                                background = nearTheMoneyBG;
                            }
                            else {
                                if (row % 2 == 0) {
                                    foreground = g_oStrikeFG1;
                                    background = g_oStrikeBG1;
                                }
                                else {
                                    foreground = g_oStrikeFG2;
                                    background = g_oStrikeBG2;
                                }
                            }
                        }
                        stock = null;
                    }
                }
                else if (modelColumn > OptionsModel.STRIKE_PRICE_PRICE_COLUMN+1) {
                    if (this.key != null) {
                        Stock stock = DataStore.getSharedInstance().getStockObject(key);
                        if (stock != null) {
                            if (((OptionsModel) table.getModel()).getStrikePrice(row) > stock.getPreviousClosed()) {
                                foreground = nearTheMoneyFG;
                                background = nearTheMoneyBG;
                            }
                            else {
                                if (row % 2 == 0) {
                                    foreground = g_oStrikeFG1;
                                    background = g_oStrikeBG1;
                                }
                                else {
                                    foreground = g_oStrikeFG2;
                                    background = g_oStrikeBG2;
                                }
                            }
                        }
                        stock = null;
                    }
                }
                else {
                    if (row % 2 == 0) {
                        foreground = g_oStrikeFG1;
                        background = g_oStrikeBG1;
                    }
                    else {
                        foreground = g_oStrikeFG2;
                        background = g_oStrikeBG2;
                    }
                }
                if((modelColumn == OptionsModel.CALL_STOCK_BID_PRICE_COLUMN) || (modelColumn == OptionsModel.PUT_STOCK_BID_PRICE_COLUMN)){
                    if (row % 2 == 0) {
                        background = g_oBGBid2;
                    }
                    else {
                        background = g_oBGBid1;
                    }
                } else if ((modelColumn == OptionsModel.CALL_STOCK_ASK_PRICE_COLUMN) || (modelColumn == OptionsModel.PUT_STOCK_ASK_PRICE_COLUMN)){
                    if (row % 2 == 0) {
                        background = g_oBGAsk2;
                    }
                    else {
                        background = g_oBGAsk1;
                    }
                }
            }
        }
        else {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            g_oUpFGColor = sett.getPositiveChangeFG();
            g_oDownFGColor = sett.getNegativeChangeFG();
            if (isSelected) {
                if (modelColumn == OptionsModel.STRIKE_PRICE_PRICE_COLUMN) {
                    if (row % 2 == 0) {
                        foreground = sett.getOptionChainStrikeColor1FG();// g_oStrikeFG1;
                        background = sett.getOptionChainStrikeColor1BG();// g_oStrikeBG1;
                    }
                    else {
                        foreground = sett.getOptionChainStrikeColor2FG();// g_oStrikeFG2;
                        background = sett.getOptionChainStrikeColor2BG();// g_oStrikeBG2;
                    }
                }
                else {
                    foreground = sett.getSelectedColumnFG();// g_oSelectedFG;
                    background = sett.getSelectedColumnBG();// g_oSelectedBG;
                }
            }
            else {
                if (modelColumn == OptionsModel.STRIKE_PRICE_PRICE_COLUMN) {
                    if (row % 2 == 0) {
                        foreground = sett.getOptionChainStrikeColor1FG();// g_oStrikeFG1;
                        background = sett.getOptionChainStrikeColor1BG();// g_oStrikeBG1;
                    }
                    else {
                        foreground = sett.getOptionChainStrikeColor2FG();// g_oStrikeFG2;
                        background = sett.getOptionChainStrikeColor2BG();// g_oStrikeBG2;
                    }
                }
                else if (modelColumn < OptionsModel.STRIKE_PRICE_PRICE_COLUMN) {
                    if (row % 2 == 0) {
                        foreground = sett.getOptionChainCallColor1FG();// g_oCallsFG1;
                        background = sett.getOptionChainCallColor1BG();// g_oCallsBG1;
                    }
                    else {
                        foreground = sett.getOptionChainCallColor2FG();// g_oCallsFG2;
                        background = sett.getOptionChainCallColor2BG();// g_oCallsBG2;
                    }
                }
                else if (modelColumn > OptionsModel.STRIKE_PRICE_PRICE_COLUMN) {
                    if (row % 2 == 0) {
                        foreground = sett.getOptionChainPutColor1FG();// g_oPutsFG1;
                        background = sett.getOptionChainPutColor1BG();// g_oPutsBG1;
                    }
                    else {
                        foreground = sett.getOptionChainPutColor2FG();// g_oPutsFG2;
                        background = sett.getOptionChainPutColor2BG();// g_oPutsBG2;
                    }
                }
            }
        }
        String optionKey = null;
        if ((modelColumn > 0) && modelColumn < OptionsModel.STRIKE_PRICE_PRICE_COLUMN){
            optionKey = (String)((OptionsModel)table.getModel()).getValueAt(row, -2);
        } else {
            optionKey = (String)((OptionsModel)table.getModel()).getValueAt(row, -1);
        }
        try {
            oPriceFormat = SharedMethods.getDecimalFormat(optionKey);
            oChangeFormat = SharedMethods.getDecimalFormat(optionKey);
        } catch (Exception e) {
            oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
            oChangeFormat = new TWDecimalFormat(" ###,##0.00 ");
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        sColName = table.getColumnName(column);
        int iRendID = 0;
        for (int i = 0; i < g_asColumns.length; i++) {
            if (sColName.equals(g_asColumns[i])) {
                iRendID = g_asRendIDs[i];
                break;
            }
        }
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
            case 0:// SYMBOL
                if (value == null) {
                    lblRenderer.setText("");
                }
                else {
                    lblRenderer.setText(((StringTransferObject) value).getValue());
                    lblRenderer.setHorizontalAlignment(JLabel.LEADING);
                }
                break;
            case 1:// STRIKE PRICE
//                if (modelColumn == OptionsModel.STRIKE_PRICE_PRICE_COLUMN) {
                    lblRenderer.setText(oStrikePriceFormat.format(((DoubleTransferObject) (value)).getValue()));
//                } else {
//                    lblRenderer.setText(oPriceFormat.format(((DoubleTransferObject) (value)).getValue()));
//                }
                lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                break;
            case 2://
                lblRenderer.setText(((StringTransferObject) value).getValue());
                lblRenderer.setHorizontalAlignment(g_iStringAlign);
                break;
            case 'R'://
                lblRenderer.setText(((StringTransferObject) value).getValue());
                lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                break;
            case 3:// DECIMAL
                doubleValue = ((DoubleTransferObject) (value)).getValue();
                if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                    lblRenderer.setText(g_sNA);
                }
                else {
                    lblRenderer.setText((oPriceFormat.format(doubleValue)));
                }
                lblRenderer.setForeground(priceFGColor);
                lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                break;
            case 4:// QUANTITY
                lblRenderer.setText((oQuantityFormat.format(((LongTransferObject) (value)).getValue())));
                lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                break;
            case 5:// CHANGE
                doubleValue = ((DoubleTransferObject) (value)).getValue();
                lblRenderer.setText((oChangeFormat.format(doubleValue)));
                if (doubleValue > 0) {
                    lblRenderer.setForeground(g_oUpFGColor);
                }
                else if (doubleValue < 0) {
                    lblRenderer.setForeground(g_oDownFGColor);
                }
                else {
                    lblRenderer.setForeground(foreground);
                }
                lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                break;
            case 6:// % CHANGE
                doubleValue = ((DoubleTransferObject) (value)).getValue();
                lblRenderer.setText((oPChangeFormat.format(doubleValue)));
                if (doubleValue > 0) {
                    lblRenderer.setForeground(g_oUpFGColor);
                }
                else if (doubleValue < 0) {
                    lblRenderer.setForeground(g_oDownFGColor);
                }
                else {
                    lblRenderer.setForeground(foreground);
                }
                lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                break;
            case 7:// DATE TIME
                long lDateTime = toLongValue(value);
                if (lDateTime == 0) {
                    lblRenderer.setText(" " + g_sNA + " ");
                }
                else {
                    dDateTime = new Date(lDateTime + Settings.getTimeOffset(lDateTime));
                    lblRenderer.setText(g_oDateTimeFormat.format(dDateTime));
                }
                lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                break;
            case 8:// TIME
                long lTime = toLongValue(value);
                if (lTime == 0) {
                    lblRenderer.setText(" " + g_sNA + " ");
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                }
                else {
                    Date dTime = new Date(lTime + Settings.getTimeOffset(lTime));
                    lblRenderer.setText(g_oTimeFormat.format(dTime));
                    lblRenderer.setHorizontalAlignment(JLabel.CENTER);
                }
                break;
            case 'F':// Price with background highlight
                doubleValue = ((DoubleTransferObject) (value)).getValue();
                updateDirection = ((DoubleTransferObject) (value)).getFlag();
                now = System.currentTimeMillis();
                //floatArray = (float[]) value;
                lblRenderer.setText((oPriceFormat.format(doubleValue)));
                if (Math.abs(now - Math.abs(updateDirection)) < Constants.CELL_HIGHLIGHT_DEALY) {// is recently changed
                    if (updateDirection > 0) {
                        lblRenderer.setBackground(g_oUpBGColor);
                        //                            lblRenderer.setForeground(upFGColor);
                    }
                    else if (updateDirection < 0) {
                        //                            lblRenderer.setForeground(downFGColor);
                        lblRenderer.setBackground(g_oDownBGColor);
                    }
                }
                lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                break;
            case 9:
                doubleValue = ((DoubleTransferObject) (value)).getValue();
                if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) || (doubleValue == Double.NEGATIVE_INFINITY)) {
                    lblRenderer.setText(g_sNA);
                }
                else {
                    lblRenderer.setText("" + (int) doubleValue);//(oPriceFormat.format(doubleValue))
                }
                lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                break;
            default:
                lblRenderer.setText("");
            }
        }
        catch (Exception e) {
            //e.printStackTrace();
            lblRenderer.setText("");
        }
        foreground = null;
        background = null;
        sColName = null;
        dDateTime = null;

        return lblRenderer;
    }


    public void propertyChanged(int property) {
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }
}