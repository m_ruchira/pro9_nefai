package com.isi.csvr.options;

import java.util.Comparator;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class OptionSymbolComparator implements Comparator {

    public int compare(Object o1, Object o2) {
		   OpraObject op1 = (OpraObject)o1;
		   OpraObject op2 = (OpraObject)o2;

		   if (op1.getStrikePrice() > op2.getStrikePrice()){
			   return 1;
		   }else if (op1.getStrikePrice() < op2.getStrikePrice()){
			   return -1;
		   }else{
			   return 0;
		   }
    }

    public boolean equals(Object obj) {
		   return super.equals(obj);
    }
}