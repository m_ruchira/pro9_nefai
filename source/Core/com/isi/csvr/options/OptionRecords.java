package com.isi.csvr.options;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author Uditha Nagahawatta
 * @version 1.0
 */

import com.isi.csvr.datastore.OpraQuote;
import com.isi.csvr.shared.DynamicArray;

import java.util.Hashtable;

public class OptionRecords {

    private DynamicArray opraRecords = null;
	//private Hashtable    callIndex   = null;
	private Hashtable    symbolIndex    = null;
	private Hashtable    zeroIndex   = null;
	private OpraQuote    dummy       = null;

	public static final int CALL = 0;
	public static final int PUT  = 1;

    public OptionRecords(){
		opraRecords = new DynamicArray();
		//callIndex   = new Hashtable();
		symbolIndex    = new Hashtable();
		zeroIndex   = new Hashtable();
		OptionSymbolComparator comparator = new OptionSymbolComparator();
		opraRecords.setComparator(comparator);
		dummy       = new OpraQuote((byte)0);
	}

	public synchronized OpraQuote getRecord(int side,byte excode, String symbol, float strikePrice){
		String strike = null;
		int index;

		try{
			if (strikePrice == 0){ // real time record
				if (zeroIndex.containsKey(symbol)){  // snapshot not received yet and this is not the first record
					print(side, "stp 0 record found in z-index  " + symbol);
					return (OpraQuote)zeroIndex.remove(symbol);
				}else{ // snapshot may have received
					   print(side,"stp 0 checking in symbol index  " + symbol);
						strike = (String)symbolIndex.get(symbol);

						if (strike == null){
							print(side,"stp 0 not found in symbol index  " + symbol);
							return null;
						}
					print(side,"stp 0 record found in symbol index  " + symbol);
					dummy.setStrikePrice(Float.parseFloat(strike));
					index =opraRecords.indexOfContainingValue(dummy);

					if (index >= 0){ // already in the list
						print(side,"stp 0 foun in store  " + symbol);
						OpraQuote obj = (OpraQuote)opraRecords.get(index);
						symbolIndex.put(symbol,"" + obj.getStrikePrice());
						return obj;
					}else{ // not in the list. first rec is a snapshot
						print(side,"stp 0 not foun in store  " + symbol);
						return null;
					}
				}
			}else{ // snapshot record
				if (zeroIndex.containsKey(symbol)){ // real time had arrived before the snapshot
					print(side,"stp > 0 found in z index  " + symbol);
					return (OpraQuote)zeroIndex.remove(symbol);
				}else{ // no realtime recs before snapshot
					dummy.setStrikePrice(strikePrice);
					index =opraRecords.indexOfContainingValue(dummy);
					if (index >= 0){ // already in the list
						print(side,"stp > 0 found in store  " + symbol);
						OpraQuote obj = (OpraQuote)opraRecords.get(index);
						symbolIndex.put(symbol,"" + obj.getStrikePrice());
						return obj;
					}else{ // not in the list. first rec is a snapshot
						print(side,"stp > 0 not found in store  " + symbol);
						return null;
					}
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}
/*		try{
			if (strikePrice > 0){
				dummy.setStrikePrice(strikePrice);
		        index =opraRecords.indexOfContainingValue(dummy);
				if (index >= 0){
				   return (OpraQuote)opraRecords.get(index);
				}else{
				   return null;
				}
			}else if (side == CALL){
				strike = (String)callIndex.get(symbol);
				if (strike == null){
				   SharedMethods.printLine("Error - CALL not found " + symbol);
				   return null;
				}
				dummy.setStrikePrice(Float.parseFloat(strike));
				index  = opraRecords.indexOfContainingValue(dummy);
				if (index >= 0){
				   return (OpraQuote)opraRecords.get(index);
				}else{
				   return null;
				}
			}else{
				strike = (String)putIndex.get(symbol);
				if (strike == null){
					SharedMethods.printLine("Error - PUT not found" + symbol);
					return null;
				}
				dummy.setStrikePrice(Float.parseFloat(strike));
				index  = opraRecords.indexOfContainingValue(dummy);
				if (index >= 0){
				   return (OpraQuote)opraRecords.get(index);
				}else{
				   return null;
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			return null;
		}*/
	}


	public synchronized void addRecord(int side, String symbol, float strikePrice, OpraQuote record){

		if (record.getStrikePrice() == 0){
			zeroIndex.put(record.getSymbol(), record);
			print(side,"******************record added to z-index  " + symbol);
		}else{
			dummy.setStrikePrice(strikePrice);
		    if (!opraRecords.contains(dummy)){
			   opraRecords.insert(record);
			   print(side,"record insertrd to store  " + symbol);
		    }else{
				print(side,"Error :- Opra object already exist in the array");
			}
			symbolIndex.put(symbol,"" + strikePrice);
			print(side,"added to symbol index " + symbol);
		}

	}

	public synchronized DynamicArray getRecordset(){
		return opraRecords;
	}

	public void print(int tag, String msg){
		//if (tag == CALL)
		//   System.err.println(msg);
	}

}