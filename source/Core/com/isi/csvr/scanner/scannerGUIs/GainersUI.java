package com.isi.csvr.scanner.scannerGUIs;

import com.isi.csvr.scanner.Scans.Gainers;
import com.isi.csvr.scanner.Scans.ScanBase;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.calendar.CalCombo;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.util.ArrayList;
import java.util.Date;
import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 24, 2008
 * Time: 11:52:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class GainersUI extends JPanel {


    private JLabel lblDisplayTop;
    private JLabel lblSpecify;

    private TWTextField txtSpecify;

    public TWComboBox cmbDisplayTop;

    private CalCombo cmbDate;

    public ArrayList<TWComboItem> listDislayTop;


    public String getTxtSpecify() {
        return txtSpecify.getText();
    }

    public void setTxtSpecify(String value) {
        this.txtSpecify.setText(value);
    }

    public GainersUI() {


        lblDisplayTop = new JLabel(Language.getString("DISPLAY_TOP"));
        listDislayTop = new ArrayList<TWComboItem>(); //initialize each individually

        lblSpecify = new JLabel(Language.getString("SPECIFY"));
        lblSpecify.setEnabled(false);
        txtSpecify = new TWTextField();
        txtSpecify.setText("10");
        txtSpecify.setEnabled(false);

        cmbDate = new CalCombo();
        cmbDate.setBorder(UIManager.getBorder("TextField.border"));
        Calendar cal = Calendar.getInstance();
        Calendar calTemp = Calendar.getInstance();
        cal.set(calTemp.get(Calendar.YEAR), calTemp.get(Calendar.MONTH), calTemp.get(Calendar.DATE), 0, 0, 0);
        cmbDate.setDate(cal.getTimeInMillis());
        JLabel lblDate = new JLabel(Language.getString("SCANNER_DATE"));

        listDislayTop.add(new TWComboItem(0, "5", "5"));
        listDislayTop.add(new TWComboItem(1, "20", "20"));
        listDislayTop.add(new TWComboItem(2, "50", "50"));
        listDislayTop.add(new TWComboItem(3, Language.getString("CATEGORY_OTHER"), "OTHER"));

        cmbDisplayTop = new TWComboBox(new TWComboModel(listDislayTop));

        cmbDisplayTop.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent e) {
                TWComboItem item = (TWComboItem) e.getItem();
                if (item == null) return;

                if ((item.getType()).equals("OTHER")) {
                    txtSpecify.setEnabled(true);
                    lblSpecify.setEnabled(true);
                } else {
                    txtSpecify.setEnabled(false);
                    lblSpecify.setEnabled(false);

                }
            }
        });
        cmbDisplayTop.setSelectedIndex(1);
        JPanel specifyPnl = new JPanel();
        specifyPnl.setName("G_S_PANEL");
        specifyPnl.setLayout(new FlexGridLayout(new String[]{"70", "100%"}, new String[]{"100%"}, 0, 0));
        specifyPnl.add(lblSpecify);
        specifyPnl.add(txtSpecify);

        JPanel datePnl = new JPanel();
        datePnl.setName("G_S_PANEL");
        datePnl.setLayout(new FlexGridLayout(new String[]{"70", "100%"}, new String[]{"100%"}, 0, 0));
        if (!Language.isLTR()) {
            datePnl.setLayout(new FlexGridLayout(new String[]{"70", "100%"}, new String[]{"100%"}, 0, 0));
        }
        datePnl.add(lblDate);
//        datePnl.add(new JPanel());
        datePnl.add(cmbDate);

        //todo: added new
        JPanel pnlDisplayTop = new JPanel();
        pnlDisplayTop.setLayout(new FlexGridLayout(new String[]{"70", "100%"}, new String[]{"100%"}, 0, 0));
        pnlDisplayTop.add(lblDisplayTop);
        pnlDisplayTop.add(cmbDisplayTop);

        setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20","5","20","5","20","100%"}, 0, 0));
        this.add(pnlDisplayTop);
        this.add(new JLabel());
        this.add(specifyPnl);
        this.add(new JLabel());
        this.add(datePnl);
        /*setLayout(new FlexGridLayout(new String[]{"10", "70", "10", "100%", "10"}, new String[]{"20", "4", "20", "4", "20", "100%"}, 0, 2));

        add(new JLabel());
        add(lblDisplayTop);
        add(new JLabel());
        add(cmbDisplayTop);
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(specifyPnl);
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(new JLabel());

        add(new JLabel());
        add(new JLabel());
        add(new JLabel());
        add(datePnl);
        add(new JLabel());*/
    }

    protected ScanBase getScanObject() {
        Gainers scanObj = new Gainers();

        if (getSelectedCount() < 0) {
            throw new NumberFormatException();
        }
        scanObj.setCount(getSelectedCount());
        scanObj.setTime(geSelectedtDate());
        return scanObj;
    }

    private int getSelectedCount() {
        switch (cmbDisplayTop.getSelectedIndex()) {
            case 0:
                return 5;
            case 1:
                return 20;
            case 2:
                return 50;
            default:
                return Integer.parseInt(txtSpecify.getText());
        }
    }

    public String getselectedDisplayTop() {
        return (String) cmbDisplayTop.getSelectedItem();
    }

    public long geSelectedtDate() {

        //long time1 = cmbDate.getDate().getTimeInMillis();
        //Calendar cal = Calendar.getInstance();
        //cal.set(2009, 6, 26, 0, 0, 0);
        //long time2 = cal.getTimeInMillis();

        //TWDateFormat f = new TWDateFormat("yyyy MM dd");
        //System.out.println("time 2 " + f.format(time2));
        //System.out.println("time 1 " + f.format(time1));

        return cmbDate.getDate().getTimeInMillis();

    }


}
