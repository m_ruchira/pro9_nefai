package com.isi.csvr.scanner.Scans;

import com.isi.csvr.scanner.Meta;
import com.isi.csvr.scanner.ScanManager;
import com.isi.csvr.scanner.ScanPoint;
import com.isi.csvr.scanner.scannerGUIs.MainScannerWindow;
import com.isi.csvr.scanner.Results.RsIslandTops;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 18, 2008
 * Time: 8:12:46 PM
 * To change this template use File | Settings | File Templates.
 */


  public class IslandTops extends ScanBase {
        //GapUpPercent
     private float gapUpPercent;
     private float gapDownPercent;

    public float getGapUpPercent() {
        return gapUpPercent;
    }

    public void setGapUpPercent(float gapUpPercent) {
        this.gapUpPercent = gapUpPercent;
    }

    public float getGapDownPercent() {
        return gapDownPercent;
    }

    public void setGapDownPercent(float gapDownPercent) {
        this.gapDownPercent = gapDownPercent;
    }//GapDownPercent


        public IslandTops() {
            this.scanID = Meta.ISLAND_TOPS;
            this.gapUpPercent = 2.5f;
            this.gapDownPercent = 2.5f;
        }

        public void scan(ScanManager SM,ArrayList scResult) {
            long entryTime = System.currentTimeMillis();
//            ArrayList scResult = new ArrayList();
               ArrayList alCompanylist =  SM.getCompanyList();
                MainScannerWindow.setStatusMaxProgress(alCompanylist.size());
                 for (int itrCompany = 0; itrCompany  <alCompanylist.size() ; itrCompany ++) {
                   DynamicArray companyHistory  = SM.addScanRequest((String)alCompanylist.get(itrCompany));
                   SM.clearGraphStore();
                   SM.populateGraphStore(companyHistory);
 if (itrCompany % 100 == 0) {
                try {
                    Thread.sleep(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (itrCompany % 10 == 0) {

                MainScannerWindow.setStatus(Language.getString("SCANNING") + " : " + (itrCompany + 1) + "/" + alCompanylist.size() + "  " + SharedMethods.getSymbolFromKey((String) alCompanylist.get(itrCompany)));
                MainScannerWindow.setStatusProgress(itrCompany);
            }
                     if (!MainScannerWindow. isScanning){
                break;
            }
            if (SM.getGraphStore().size() > 2) {
                float gapUpFactor = 1f + gapUpPercent / 100f;
                float gapDownFactor = 1f - gapDownPercent / 100f;

                    //reading todays value
                    ScanPoint sP = SM.readScanPoint(SM.getGraphStore().size() - 1, Meta.GRAPH_INDEX);
                    if (sP == null) continue;
                    double currHigh = sP.High;
                    sP = SM.readScanPoint(SM.getGraphStore().size() - 2, Meta.GRAPH_INDEX);
                    if (sP == null) continue;
                    double prevLow = sP.Low;
                    sP = SM.readScanPoint(SM.getGraphStore().size() - 3, Meta.GRAPH_INDEX);
                    if (sP == null) continue;
                    double firstHigh = sP.High;
                    if ((currHigh <= gapDownFactor * prevLow) && (prevLow >= gapUpFactor * firstHigh)) {
                        String cmpny = SM.getCompanyList().get(itrCompany);
//                        String cmpny = SM.getCompanyList().get(j);
                        sP = SM.readScanPoint(SM.getGraphStore().size() - 1, Meta.GRAPH_INDEX);

                        double pcntGapUp = 100f * (prevLow - firstHigh) / firstHigh;
                        double pcntGapDown = 100f * (prevLow - currHigh) / prevLow;
                        RsIslandTops result = new RsIslandTops(cmpny, sP, pcntGapUp, pcntGapDown); 
                        scResult.add(result);
                         SM.fireUpDateUI();
                    }
                }
            }
            System.out.println("@@@@@@@@ End Scanning GapDowns after: " + (System.currentTimeMillis() - entryTime));
//            return scResult;
        }

        public  String getShortName() {
//            return "Island Tops > " + gapUpPercent + "% rise, " + gapDownPercent + "% drop";
            return Language.getString("ISLAND_TOPS")   + ">" +  gapUpPercent + Language.getString("PERCENTAGE_RISE") +  
                                                        ", " + gapDownPercent + Language.getString("PERCENTAGE_DROP");
        }
    }

