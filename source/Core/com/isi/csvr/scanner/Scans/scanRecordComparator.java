package com.isi.csvr.scanner.Scans;



import java.util.Comparator;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 26, 2008
 * Time: 11:08:11 AM
 * To change this template use File | Settings | File Templates.
 */

public class scanRecordComparator implements Comparator, Serializable {

    public int compare(Object o1, Object o2) {

        ScanRecord  sr1 = (ScanRecord) o1;
        ScanRecord sr2 = (ScanRecord) o2;

        if (sr2.getTime() > sr1.getTime())
            return 1;
      else
            return -1;

    }
}
