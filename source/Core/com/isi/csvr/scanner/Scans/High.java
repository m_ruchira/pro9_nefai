package com.isi.csvr.scanner.Scans;

import com.isi.csvr.scanner.ScanManager;
import com.isi.csvr.scanner.Meta;
import com.isi.csvr.scanner.ScanPoint;
import com.isi.csvr.scanner.ScanSlot;
import com.isi.csvr.scanner.Results.ResultBase;
import com.isi.csvr.scanner.Results.ResultUI.ResultComparator;
import com.isi.csvr.scanner.scannerGUIs.MainScannerWindow;
import com.isi.csvr.shared.*;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.DataStoreInterface;
import com.isi.csvr.ChartInterface;
//import com.isi.csvr.ScannerInterface;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Date;
import java.text.SimpleDateFormat;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Aug 20, 2008
 * Time: 1:33:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class High extends ScanBase{

     private double high;

    public double getHigh() {
        return high;
    }

    public void setHigh(double high) {
        this.high = high;
    }

    public High() {
        this.scanID = Meta.HIGH;
        this.high =  20;
     }

    //    public abstract ArrayList scan(ScanManager SM , ArrayList scResult );
        public void scan(ScanManager SM, ArrayList scResult) {
        System.out.println("inside high scan------------------------------------"+scResult.size());



        long entryTime = System.currentTimeMillis();
//        ArrayList scResult = new ArrayList();
        boolean isGainers = (this.scanID == Meta.HIGH);
        ArrayList alCompanylist = SM.getCompanyList();

        System.out.println("-----------------------------------alCompanylist"+alCompanylist.size());
        MainScannerWindow.setStatusMaxProgress(alCompanylist.size());
        for (int itrCompany = 0; itrCompany < alCompanylist.size(); itrCompany++) {
            String cmpKey = alCompanylist.get(itrCompany).toString();
            System.out.println("======================="+ cmpKey );
            DynamicArray companyHistory = SM.addScanRequest(cmpKey);
            SM.clearGraphStore();
            SM.populateGraphStore(companyHistory);
            //    int bIndex = Math.max(SM.getGraphStore().size() - period - 1, 0);// thi
            if (itrCompany % 100 == 0) {
                try {
                    Thread.sleep(1);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            if (itrCompany % 10 == 0) {

                MainScannerWindow.setStatus(Language.getString("SCANNING") + " : " + (itrCompany + 1) + "/" + alCompanylist.size() + "  " + SharedMethods.getSymbolFromKey((String) alCompanylist.get(itrCompany)));
                MainScannerWindow.setStatusProgress(itrCompany);
            }
            if (!MainScannerWindow.isScanning) {
                break;
            }
            if (SM.getGraphStore().size() > 1) {

                //  for (int j = 0; j < SM.getSources().size(); j++) {
                //reading todays value
                ScanPoint sPLast = SM.readScanPoint(SM.getGraphStore().size() - 1, Meta.GRAPH_INDEX);


            /*************************finding the 52 weeks back colosest index for the time *********************************************/
                ScanSlot  singlePoint  =  (ScanSlot) SM.getGraphStore().get(SM.getGraphStore().size() - 1);
                long time = singlePoint.Time;
                long time52Back = SM.getPeriodBeginMillisec(time);
                SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
                Date date = new Date(time );
                dateFormat .format(date);
                System.out.println("Scan date:  " + date );

                Date date52 = new Date(time52Back);
                dateFormat .format(date52);
                System.out.println("Scan 52 date:  " + date52 );
                int index52Back = SM.getClosestIndexFortheTime(time52Back);
                /**********************************************************************/



           /*******************  //finding the 52 weeks high value***************************************************/

               int highIndedx = index52Back;
               ScanPoint sPHigh52 = SM.readScanPoint(index52Back, Meta.GRAPH_INDEX);
                double high52 = sPHigh52.High;
                for (int giItr =index52Back ; giItr < SM.getGraphStore().size() - 1 ;giItr++  ){
                  ScanPoint sPDay = SM.readScanPoint(giItr, Meta.GRAPH_INDEX);
                  double specDaytHigh = sPDay.High;
                   if (specDaytHigh >= high52){
                        high52 =  specDaytHigh;
                       sPHigh52 = sPDay;
                       highIndedx = giItr;

                   }
                }
      /*******************  //finding the 52 weeks high value***************************************************/

                if (sPLast == null) continue;
                 //--------------- commented shanika
                System.out.println("----------High---"+sPLast.High);
//                 String symbol = SharedMethods.getSymbolFromKey(cmpKey);
//                     String sExchange = SharedMethods.getExchangeFromKey(cmpKey);
//                     int instrumentType = (int) SharedMethods.getInstrumentTypeFromKey(cmpKey);
//                     Stock stock = DataStore.getSharedInstance().getStockObject(SharedMethods.getKey(symbol,sExchange,instrumentType));//.getSharedInstance().getStockObject(symbol,sExchange,instrumentType);

//                Stock stock = ScannerInterface.getStockObject(cmpKey);
                if ( sPLast.High  > high52) {
                    ScanSlot highSlot52 =  (ScanSlot) SM.getGraphStore().get(highIndedx);
                    long Week52highTime  = highSlot52.Time;
                    String cmpny = SM.getCompanyList().get(itrCompany);



                    SimpleDateFormat date52Format = new SimpleDateFormat("MM/dd/yy");
                    Date date52High = new Date(Week52highTime );
                    String dateString52High =date52Format .format(date52High);


                    ResultBase result = new ResultBase(cmpny, sPHigh52 ,Week52highTime);

//                    System.out.println("================================"+dateString52High);
//                    ResultBase result = new ResultBase(cmpny, sPLast,dateString52High);
//                    ResultBase result = new ResultBase(cmpny, sPLast,date52High);

                    scResult.add(result);
                   SM.fireUpDateUI();

                    }

//                    if (scResult.size() > this.high) {
//                        //remove range
//                        int iCompletesize = scResult.size();
//                        for (int i = (int) this.high; i < iCompletesize; i++) {
//                            scResult.remove(this.high);
//
//                        }
//                        //scResult.removeRange(2,3);
//
//                    }
//                    SM.fireUpDateUI();
                }


                // ---------------------- end of commened
            }
//todo commentout  gainers use later

            System.out.println("@@@@@@@@ End Scanning after: " + (System.currentTimeMillis() - entryTime));
            //  return scResult;
        }


        //To change body of implemented methods use File | Settings | File Templates.


    public String getShortName() {
//        return Language.getString("GAP_DOWNS") +  " > " + getGapPercent() +Language.getString("PERCENTAGE_DROP");
        return Language.getString("HIGH_REACHED_52HIGH");
    }
}
