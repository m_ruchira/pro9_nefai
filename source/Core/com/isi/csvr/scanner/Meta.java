package com.isi.csvr.scanner;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 24, 2008
 * Time: 6:46:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class Meta {

    public static final int DEFAULT =0;
    public static final int BREAKOUTS =1;
    public static final int VOLUME =2;
    public static final int VOLUME_GAINERS=3;
    public static final int VOLUME_LOOSERS=4;
    public static final int GAP_UPS=5;
    public static final int GAP_DOWNS=6;
    public static final int ISLAND_TOPS=7;
    public static final int ISLAND_BOTTOMS=8;
    public static final int GAINERS=9;
    public static final int LOOSERS=10;
    public static final int HIGH=11;
    public static final int LOW=12;


   //STRINGS
     public static final String DEAFULT_CARD ="DEFAULT";
     public static final String BREAKOUTS_CARD ="BREAKOUTS";
    public static final String VOLUME_CARD ="VOLUME" ;
    public static final String VOLUME_GAINERS_CARD= "VOLUME_GAINERS";
    public static final String VOLUME_LOOSERS_CARD= "VOLUME_LOOSERS";
    public static final String GAP_UPS_CARD = "GAP_UPS";
    public static final String GAP_DOWNS_CARD= "GAP_DOWNS";
    public static final String ISLAND_TOPS_CARD= "ISLAND_TOPS";
    public static final String ISLAND_BOTTOMS_CARD = "ISLAND_BOTTOMS";
    public static final String GAINERS_CARD= "GAINERS";
    public static final String  LOOSERS_CARD = "LOOSERS";
    public static final String  HIGH_CARD = "HIGHS";
    public static final String  LOW_CARD = "LOWS";



// history interval

   public static final  int  YEARLY = 365 * 24 * 3600;
    public static final  int    QUARTERLY = 365 * 6 * 3600;
      public static final int   MONTHLY = 30 * 24 * 3600;
     public static final  int   WEEKLY = 7 * 24 * 3600;
      public static final int   DAILY = 1 * 24 * 3600;


    public static final  long TICKS_PER_DAY = 3600 * 24 * 10000000L;

  public static final  long     ADJ_MONDAY = 4 * 24 * 3600;
    public static final  long       ADJ_TUESDAY = 5 * 24 * 3600;
     public static final  long      ADJ_WEDNESDAY = 6 * 24 * 3600;
     public static final  long      ADJ_THURSDAY = 0 * 24 * 3600;
   public static final  long        ADJ_FRIDAY = 1 * 24 * 3600;
     public static final  long      ADJ_SATURDAY = 2 * 24 * 3600;
     public static final  long      ADJ_SUNDAY = 3 * 24 * 3600;

    public static final  double MAX_DOUBLE_VALUE= 1.79769e+308;
   public static final int GRAPH_INDEX = 0;

}
