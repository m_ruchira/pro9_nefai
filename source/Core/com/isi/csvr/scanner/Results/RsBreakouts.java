package com.isi.csvr.scanner.Results;

import com.isi.csvr.scanner.ScanPoint;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 18, 2008
 * Time: 8:06:51 PM
 * To change this template use File | Settings | File Templates.
 */

public class RsBreakouts extends ResultBase {
    //PercentAboveHigh
    private double pcntAboveHigh;

    public double getPercentAboveHigh() {
        return pcntAboveHigh;
    }

    public void setPercentAboveHigh(double value) {
        pcntAboveHigh = value;
    }

    //        public RsBreakouts(Company company, ScanPoint sP, double pcntAboveHigh)
    public RsBreakouts(String company, ScanPoint sP, double pcntAboveHigh) {
        super(company, sP);
        this.pcntAboveHigh = pcntAboveHigh;
    }

    protected int compareObj(Object obj) {
        // this is done opposite way to sort descending
        return Double.compare(Math.abs(((RsBreakouts) obj).pcntAboveHigh), Math.abs(this.pcntAboveHigh));

    }
}

