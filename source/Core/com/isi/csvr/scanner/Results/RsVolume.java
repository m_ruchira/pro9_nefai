package com.isi.csvr.scanner.Results;

import com.isi.csvr.scanner.ScanPoint;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 18, 2008
 * Time: 8:08:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class RsVolume extends ResultBase {
    //PercentAboveAvg
    private double pcntAboveAvg;

    public double getNoOfTimes() {
        return noOfTimes;
    }

    public void setNoOfTimes(double noOfTimes) {
        this.noOfTimes = noOfTimes;
    }

    private double noOfTimes = 0.00d ;


    public double getPercentAboveAvg() {
        return pcntAboveAvg;
    }

    public double PercentAboveAvg(double value) {
        return pcntAboveAvg = value;
    }

    //        public RsVolume(Company company, ScanPoint sP, double pcntAboveAvg)
    public RsVolume(String company, ScanPoint sP, double pcntAboveAvg,double noOfTimes ) {
        super(company, sP);
        this.pcntAboveAvg = pcntAboveAvg;
        this.noOfTimes = noOfTimes;

    }


    protected int compareObj(Object obj) {

        // this is done opposite way to sort descending
        return Double.compare(Math.abs(((RsVolume) obj).pcntAboveAvg), Math.abs(this.pcntAboveAvg));

    }
}

