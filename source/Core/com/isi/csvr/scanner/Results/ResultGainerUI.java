package com.isi.csvr.scanner.Results;

import com.isi.csvr.TWMenuItem;
import com.isi.csvr.Client;
import com.isi.csvr.print.PrintManager;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.scanner.ScanManager;
import com.isi.csvr.scanner.ScanResultListner;
import com.isi.csvr.scanner.scannerGUIs.MainScannerWindow;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWColumnSettings;
import com.isi.csvr.table.Table;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.util.ArrayList;
import java.math.BigInteger;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 27, 2008
 * Time: 7:00:23 PM
 * To change this template use File | Settings | File Templates.
 */


public class ResultGainerUI extends ResultBaseScanUI  implements ScanResultListner, MouseListener {

    //table
    
    private GainersModel vWModel;
    //view settings
    private ViewSetting resultSettings;
    //constructor
    //    VolumeWatcherWindow parent;
    private ArrayList<ResultBase> volumeDataStore;
    private ViewSetting volWatchSettings;
    private ScanManager scMgr;
    private TWMenuItem mnuCloseTab;

    public ResultGainerUI(ArrayList<ResultBase> volumeDataStore, ScanManager scMgr, String sResultType) {
        this.volumeDataStore = volumeDataStore;
        this.scMgr = scMgr;
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        //view settings
        //todo
        try {
            // Read the default view in the view settings file
            // get the default view
            volWatchSettings = ViewSettingsManager.getSummaryView("GAINERS_COLS");
            if (volWatchSettings == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        createResultTable();

        //check whether these are need
        GUISettings.setColumnSettings(volWatchSettings, TWColumnSettings.getItem("GAINERS_COLS"));
        volWatchSettings.setTableNumber(1);
        volWatchSettings.setParent(this);
        this.add(resultTable);
        scMgr.addScanResultListener(this);
        if (sResultType.equals("EXG")) {
            removeExgColumn();
        } else {
            resetExgColumn();
        }
        resultTable.getTable().getTableHeader().updateUI();
        resultTable.getModel().updateGUI();

    }

    private BigInteger pwr(int iPwr) {
        BigInteger i = BigInteger.valueOf(1);
        return (i.shiftLeft(iPwr));
    }


    public void removeExgColumn() {
        BigInteger lCols = BigInteger.valueOf(0);
        String[] asColumns = volWatchSettings.getColumnHeadings();
        for (int i = 0; i < asColumns.length; i++) {
            if (i != 2) {
                //change start
                lCols = lCols.add(pwr(i));
//                lCols += pwr(i);
            }
        }
        //change start
        volWatchSettings.setColumns((lCols));

    }

    public void resetExgColumn() {
        BigInteger lCols = BigInteger.valueOf(0);
        String[] asColumns = volWatchSettings.getColumnHeadings();
        for (int i = 0; i < asColumns.length; i++) {
            lCols = lCols.add(pwr(i));
        }
        volWatchSettings.setColumns((lCols));
    }


    public void SymbolAdded() {
        resultTable.getTable().updateUI();
    }

    public void createResultTable() {
        vWModel = new GainersModel(volumeDataStore);
        vWModel.setViewSettings(volWatchSettings);
        resultTable = new Table();
        resultTable.setSortingEnabled();
        resultTable.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
        resultTable.setModel(vWModel);
        vWModel.setBreakOutsTable(resultTable);
        resultTable.getPopup().setPrintTarget(resultTable.getTable(), PrintManager.PRINT_JTABLE);
        createCloseTab();
        resultTable.getPopup().enableUnsort(false);
        resultTable.getModel().updateGUI();
        GUISettings.applyOrientation(resultTable.getPopup());
        resultTable.getTable().addMouseListener(this);
        //set model

    }

    private void createCloseTab() {
        mnuCloseTab = new TWMenuItem(Language.getString("CLOSE_TAB"));
        mnuCloseTab.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                MainScannerWindow.closeSelectedTab();
            }
        });
        mnuCloseTab.setVisible(true);
        resultTable.getPopup().add(mnuCloseTab);
    }

    public void mouseClicked(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getSource() == resultTable.getTable()) {
            if ((e.getModifiers() & e.BUTTON1_MASK) == e.BUTTON1_MASK) {
            int col = resultTable.getTable().convertColumnIndexToModel(resultTable.getTable().getSelectedColumn());
            if (col == 0) {
                String key = (String) resultTable.getTable().getModel().getValueAt(resultTable.getTable().getSelectedRow(), -1);
                Client.getInstance().showChart(key, null, null, false, false);
            }
            }
        }

    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }//table

    public Table getResultTable() {
        return resultTable;
    }
}


