package com.isi.csvr.calendar;

import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.LineBorder;
import java.awt.*;
import java.awt.event.*;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class DatePicker extends JDialog implements ActionListener, FocusListener {
    private CalDialog datePicker = null;
	private JPanel closePanel;
	private boolean needClosebtn = false;
	private DatePicker self = null;
	private ImageIcon closeIcon = null;

	public DatePicker(Component parent, boolean modal) throws HeadlessException {

        // super((Window)SwingUtilities.getAncestorOfClass(Window.class,parent), Language.getString("WINDOW_TITLE_HISTORY_VIEW"), Dialog.ModalityType.APPLICATION_MODAL);
        //  super(Client.getInstance().getFrame());
//        super(JOptionPane.getFrameForComponent(parent), true);
        super((JFrame)SwingUtilities.getAncestorOfClass(JFrame .class,parent));
        datePicker = new CalDialog(Language.getList("MONTH_NAMES"), Language.getList("DAY_NAMES"));
//        datePicker.setBorder(UIManager.getBorder("TextField.border"));
        datePicker.setBorder(BorderFactory.createLineBorder(Theme.getColor("DATECOMBO_HEADER_BG_COLOR"), 2));
//        datePicker.setBorder(GUISettings.getTWFrameBorder());
        datePicker.setPreferredSize(new Dimension(175, 185));
        datePicker.setSelfDispose(false);
        this.getContentPane().add(datePicker);
        getClosePanel();
        this.setModal(true);
        //   this.add(datePicker);
        this.setUndecorated(true);
        ((JComponent) (this.getContentPane())).registerKeyboardAction(this, "ESCAPE", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);  //, KeyEvent.ALT_DOWN_MASK)
        datePicker.registerKeyboardAction(this, "ESCAPE", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);  //, KeyEvent.ALT_DOWN_MASK)
        this.pack();
        self = this;
	}

    public DatePicker(JDialog parent, boolean modal) throws HeadlessException {

        // super((Window)SwingUtilities.getAncestorOfClass(Window.class,parent), Language.getString("WINDOW_TITLE_HISTORY_VIEW"), Dialog.ModalityType.APPLICATION_MODAL);
        //  super(Client.getInstance().getFrame());
//        super(JOptionPane.getFrameForComponent(parent), true);
        super((JFrame) SwingUtilities.getAncestorOfClass(JDialog.class, parent));
        datePicker = new CalDialog(Language.getList("MONTH_NAMES"), Language.getList("DAY_NAMES"));
//        datePicker.setBorder(UIManager.getBorder("TextField.border"));
        datePicker.setBorder(BorderFactory.createLineBorder(Theme.getColor("DATECOMBO_HEADER_BG_COLOR"), 2));
//        datePicker.setBorder(GUISettings.getTWFrameBorder());
        datePicker.setPreferredSize(new Dimension(175, 185));
        datePicker.setSelfDispose(false);
        this.getContentPane().add(datePicker);
        getClosePanel();
        this.setModal(true);
        //   this.add(datePicker);
        this.setUndecorated(true);
        ((JComponent) (this.getContentPane())).registerKeyboardAction(this, "ESCAPE", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);  //, KeyEvent.ALT_DOWN_MASK)
        datePicker.registerKeyboardAction(this, "ESCAPE", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);  //, KeyEvent.ALT_DOWN_MASK)
        this.pack();
        self = this;
	}

    public DatePicker(InternalFrame parent, boolean modal, boolean needCloseBtn) throws HeadlessException {

        // super((Window)SwingUtilities.getAncestorOfClass(Window.class,parent), Language.getString("WINDOW_TITLE_HISTORY_VIEW"), Dialog.ModalityType.APPLICATION_MODAL);
        //  super(Client.getInstance().getFrame());
        super((JFrame)SwingUtilities.getAncestorOfClass(JFrame .class,parent));
		this.needClosebtn = needCloseBtn;
        datePicker = new CalDialog(Language.getList("MONTH_NAMES"), Language.getList("DAY_NAMES"));
//        datePicker.setBorder(UIManager.getBorder("TextField.border"));
        datePicker.setBorder(BorderFactory.createLineBorder(Theme.getColor("DATECOMBO_HEADER_BG_COLOR"), 2));
//        datePicker.setBorder(GUISettings.getTWFrameBorder());
        datePicker.setPreferredSize(new Dimension(175, 185));
        datePicker.setSelfDispose(false);
        getClosePanel();
        if (needCloseBtn) {
//			datePicker.setPreferredSize(new Dimension(175, 192));
            this.getContentPane().add(closePanel, BorderLayout.NORTH);
        } else {
        }
        this.getContentPane().add(datePicker, BorderLayout.CENTER);
        this.setModal(true);
        //   this.add(datePicker);
        this.setUndecorated(true);
        ((JComponent) (this.getContentPane())).registerKeyboardAction(this, "ESCAPE", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);  //, KeyEvent.ALT_DOWN_MASK)
        datePicker.registerKeyboardAction(this, "ESCAPE", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);  //, KeyEvent.ALT_DOWN_MASK)
        this.pack();
        self = this;
	}

     public DatePicker(JFrame parent, boolean modal, boolean needCloseBtn) throws HeadlessException {

        // super((Window)SwingUtilities.getAncestorOfClass(Window.class,parent), Language.getString("WINDOW_TITLE_HISTORY_VIEW"), Dialog.ModalityType.APPLICATION_MODAL);
        //  super(Client.getInstance().getFrame());
        super((JFrame)SwingUtilities.getAncestorOfClass(JFrame .class,parent));
		this.needClosebtn = needCloseBtn;
        datePicker = new CalDialog(Language.getList("MONTH_NAMES"), Language.getList("DAY_NAMES"));
//        datePicker.setBorder(UIManager.getBorder("TextField.border"));
        datePicker.setBorder(BorderFactory.createLineBorder(Theme.getColor("DATECOMBO_HEADER_BG_COLOR"), 2));
//        datePicker.setBorder(GUISettings.getTWFrameBorder());
        datePicker.setPreferredSize(new Dimension(175, 185));
        datePicker.setSelfDispose(false);
        getClosePanel();
        if (needCloseBtn) {
//			datePicker.setPreferredSize(new Dimension(175, 192));
            this.getContentPane().add(closePanel, BorderLayout.NORTH);
        } else {
        }
        this.getContentPane().add(datePicker, BorderLayout.CENTER);
        this.setModal(true);
        //   this.add(datePicker);
        this.setUndecorated(true);
//         this.focusLost();
        ((JComponent) (this.getContentPane())).registerKeyboardAction(this, "ESCAPE", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);  //, KeyEvent.ALT_DOWN_MASK)
        datePicker.registerKeyboardAction(this, "ESCAPE", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);  //, KeyEvent.ALT_DOWN_MASK)
        this.pack();
        self = this;
	}
    public DatePicker(JFrame parent, boolean modal, boolean needCloseBtn, int maxDays) throws HeadlessException {

        // super((Window)SwingUtilities.getAncestorOfClass(Window.class,parent), Language.getString("WINDOW_TITLE_HISTORY_VIEW"), Dialog.ModalityType.APPLICATION_MODAL);
        //  super(Client.getInstance().getFrame());
        super((JFrame)SwingUtilities.getAncestorOfClass(JFrame .class,parent));
        this.needClosebtn = needCloseBtn;
        datePicker = new CalDialog(Language.getList("MONTH_NAMES"), Language.getList("DAY_NAMES"), maxDays);
//        datePicker.setBorder(UIManager.getBorder("TextField.border"));
        datePicker.setBorder(BorderFactory.createLineBorder(Theme.getColor("DATECOMBO_HEADER_BG_COLOR"), 2));
//        datePicker.setBorder(GUISettings.getTWFrameBorder());
        datePicker.setPreferredSize(new Dimension(175, 185));
        datePicker.setSelfDispose(false);
        getClosePanel();
        if (needCloseBtn) {
//			datePicker.setPreferredSize(new Dimension(175, 192));
            this.getContentPane().add(closePanel, BorderLayout.NORTH);
        } else {
        }
        this.getContentPane().add(datePicker, BorderLayout.CENTER);
        this.setModal(true);
        //   this.add(datePicker);
        this.setUndecorated(true);
//         this.focusLost();
        ((JComponent) (this.getContentPane())).registerKeyboardAction(this, "ESCAPE", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);  //, KeyEvent.ALT_DOWN_MASK)
        datePicker.registerKeyboardAction(this, "ESCAPE", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);  //, KeyEvent.ALT_DOWN_MASK)
        this.pack();
        self = this;
    }
    public DatePicker(JDialog parent, boolean modal, boolean needCloseBtn) throws HeadlessException {

        // super((Window)SwingUtilities.getAncestorOfClass(Window.class,parent), Language.getString("WINDOW_TITLE_HISTORY_VIEW"), Dialog.ModalityType.APPLICATION_MODAL);
        //  super(Client.getInstance().getFrame());
        super((JFrame) SwingUtilities.getAncestorOfClass(JDialog.class, parent));
		this.needClosebtn = needCloseBtn;
        datePicker = new CalDialog(Language.getList("MONTH_NAMES"), Language.getList("DAY_NAMES"));
//        datePicker.setBorder(UIManager.getBorder("TextField.border"));
        datePicker.setBorder(BorderFactory.createLineBorder(Theme.getColor("DATECOMBO_HEADER_BG_COLOR"), 2));
//        datePicker.setBorder(GUISettings.getTWFrameBorder());
        datePicker.setPreferredSize(new Dimension(175, 185));
        datePicker.setSelfDispose(false);
        getClosePanel();
        if (needCloseBtn) {
//			datePicker.setPreferredSize(new Dimension(175, 192));
			this.getContentPane().add(closePanel, BorderLayout.NORTH);
        } else {
		}
        this.getContentPane().add(datePicker, BorderLayout.CENTER);
		this.setModal(true);
        //   this.add(datePicker);
        this.setUndecorated(true);
        ((JComponent) (this.getContentPane())).registerKeyboardAction(this, "ESCAPE", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);  //, KeyEvent.ALT_DOWN_MASK)
        datePicker.registerKeyboardAction(this, "ESCAPE", KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);  //, KeyEvent.ALT_DOWN_MASK)
        this.pack();
		self = this;
    }
	public void getClosePanel(){
		//dateclosebtn.gif
		closeIcon = new ImageIcon("images/theme" + Theme.getID() + "/dateclosebtn.gif");
		closePanel = new JPanel(){
			public void paint(Graphics g) {
				super.paint(g);	//To change body of overridden methods use File | Settings | File Templates.
				g.setColor(Theme.getColor("DATECOMBO_HEADER_BG_COLOR"));
				g.fillRect(1,1, this.getWidth() - 2, 14);
//				g.drawLine(2,2,this.getWidth() - 13,2);
//				g.drawLine(2,3,this.getWidth() - 13,3);
//				g.drawLine(2,6,this.getWidth() - 13,6);
//				g.drawLine(2,7,this.getWidth() - 13,7);
//				g.drawLine(2,10,this.getWidth() - 13,10);
//				g.drawLine(2,11,this.getWidth() - 13,11);
				g.drawImage(closeIcon.getImage(),this.getWidth() - 18, 0, this);
			}
		};
		closePanel.setBorder(BorderFactory.createLineBorder(Theme.getColor("DATECOMBO_HEADER_BG_COLOR"), 2));
//		closePanel.setBorder(UIManager.getBorder("TextField.border"));
		closePanel.setPreferredSize(new Dimension(175, 16));
		final int width = 175;// this.getWidth();
		final int height = 16;// this.getHeight();
		closePanel.addMouseListener(new MouseAdapter(){

			public void mousePressed(MouseEvent e) {
				Point point = e.getPoint();
				if((width - 18 <= point.x) && (point.x < width - 3)){
					if((height - 15 <= point.y) && (point.y < height -1)){
						self.setVisible(false);
					}
				}
			}
		});
	}

	public void showDialog() {
		if(!needClosebtn){
			datePicker.setPreferredSize(new Dimension(175, 185));
		} else {
			datePicker.setPreferredSize(new Dimension(175, 191));
		}
		try {
            this.setVisible(true);
        } catch (Exception e) {
            System.out.println("Failed while setting selected Date");
        }
    }

    public void setLocation(Point point){
        int xPos = point.x;
        int yPos = point.y;
        setLocation(xPos, yPos);
//        System.out.println("before point x and y"+ xPos +" , "+yPos);
//        if((xPos +  this.getWidth())> Client.getInstance().getFrame().getWidth()){
//            xPos = Client.getInstance().getFrame().getWidoth() - this.getWidth();
//        }
//        if((yPos +  this.getHeight())> Client.getInstance().getFrame().getHeight()){
//            yPos = Client.getInstance().getFrame().getHeight() - this.getHeight();
//        }
//        System.out.println("after point x and y"+ xPos +" , "+yPos);
//        super.setLocation(new Point(xPos, yPos));
    }

    public void setLocation(int x, int y){
        int xPos = x;
        int yPos = y;
//        System.out.println("before point x and y"+ xPos +" , "+yPos);
        if((xPos +  this.getWidth())> Client.getInstance().getFrame().getWidth()){
            xPos = Client.getInstance().getFrame().getWidth() - this.getWidth();
        }
        if((yPos +  this.getHeight())> Client.getInstance().getFrame().getHeight()){
            yPos = Client.getInstance().getFrame().getHeight() - this.getHeight();
        }
//        System.out.println("after point x and y"+ xPos +" , "+yPos);
        this.setBounds(xPos, yPos, this.getWidth(), this.getHeight());
//        super.setLocation(new Point(xPos, yPos));
    }

    public CalDialog getCalendar() {
        return datePicker;
    }

	public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("ESCAPE")) {
            this.setVisible(false);
        }
    }

    public void applyTheme() {
        datePicker.applyTheme();
		closeIcon = new ImageIcon("images/theme" + Theme.getID() + "/dateclosebtn.gif");
		datePicker.setBorder(BorderFactory.createLineBorder(Theme.getColor("DATECOMBO_HEADER_BG_COLOR"), 2));
//		datePicker.setBorder(GUISettings.getTWFrameBorder());
//		closePanel.setBorder(GUISettings.getTWFrameBorder());
		closePanel.setBorder(BorderFactory.createLineBorder(Theme.getColor("DATECOMBO_HEADER_BG_COLOR"), 2));
    }

    public void setDate(int year, int month, int date) {

           //setting the year
           int count = datePicker.getChYear().getItemCount();
           for (int i = 0; i < count; i++) {
               int y = Integer.parseInt(String.valueOf(datePicker.getChYear().getItemAt(i)));
               if (y == year) {
                   datePicker.getChYear().setSelectedIndex(i);
                   break;
               }
           }

           //setting the month
           datePicker.getChMonth().setSelectedIndex(month);

           //setting the date
           JLabel[] txtDays = datePicker.getTxtDays();
           for (int i = 0; i < txtDays.length; i++) {
               CalLabel cLbl = (CalLabel) txtDays[i];
               if (cLbl != null) {
                   if (cLbl.getText().equals(String.valueOf(date))) {
                       cLbl.setBorder(new LineBorder(Theme.getBlackColor(), 1));
                       cLbl.setBackground(Color.BLUE);
                   } else {
                       cLbl.setBorder(BorderFactory.createEmptyBorder());
                   }
               }
           }
       }

    public void focusGained(FocusEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void focusLost(FocusEvent e) {
        this.setVisible(false);
    }
}