package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.shared.*;
import com.isi.csvr.shared.*;
import com.isi.util.*;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 9, 2004
 * Time: 10:47:15 AM
 */
public class TradingPWDialog extends JDialog
        implements ActionListener, KeyListener, TradingConnectionListener, WindowListener, Themeable {
    private TWPasswordField pw;
    private String password;
    public static TradingPWDialog self = null;
    private JCheckBox saveUsername = new JCheckBox();

    public static synchronized TradingPWDialog getSharedInstance() {
        if (self == null) {
            self = new TradingPWDialog();
        } else {
            self.clearPW();
        }
        return self;
    }

    public TradingPWDialog() {
        super(Client.getInstance().getFrame(), Language.getString("TRADING_PASSWORD"), true);
        createUI();
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
    }

    private void clearPW() {
        pw.setText("");
    }

    public String getPW() {
        pw.requestFocusInWindow();
        setVisible(true);
        password = (new String(pw.getPassword())).trim();
        if (password.equals("")) {
            return null;
        } else {
            if(saveUsername.isSelected()){
                TradingShared.setLevel2Password(password);
            }
            return password;
        }
    }

    private void createUI() {
        String[] widths = {"130", "180"};
        String[] heights = {"20", "20","20"};
        FlexGridLayout mainLayout = new FlexGridLayout(widths, heights, 10, 10);
//        getContentPane().setLayout(mainLayout);
        getContentPane().setLayout(new ColumnLayout(ColumnLayout.MODE_VARIABLE_SIZE));

        JLabel lblPw = new JLabel(Language.getString("TRADING_PASSWORD"));
        pw = new TWPasswordField(this, false, false);
//        pw.setDocument(new LimitedLengthDocument(20));
        pw.addKeyListener(this);
        pw.setUsername(TradingShared.getTradeUserName());

        String[] widths2 = {"130","50%", "50%"};
        String[] heights2 = {"20"};
        FlexGridLayout buttonLayout = new FlexGridLayout(widths2, heights2, 5, 5, true, true);

        JPanel buttonPanel = new JPanel(buttonLayout);
        TWButton btnOK = new TWButton(Language.getString("PROCEED"));
        btnOK.setActionCommand("OK");
        btnOK.addActionListener(this);
        TWButton btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.setActionCommand("CANCEL");
        btnCancel.addActionListener(this);
        buttonPanel.add(new JLabel(""));
        buttonPanel.add(btnOK);
        buttonPanel.add(btnCancel);
        saveUsername.setOpaque(false);
        saveUsername.setText(Language.getString("REMEMBER_TRANSACTION_PASSWORD"));
        saveUsername.setSelected(false);
        saveUsername.addActionListener(this);
        saveUsername.setSize(new Dimension(150,20));
        JPanel centerPanel = new JPanel(new FlexGridLayout(new String[]{"100%"},new String[]{"20"},5,5,true,true));
        centerPanel.add(saveUsername);
        JPanel topPanel = new JPanel(new FlexGridLayout(widths,new String[]{"20"},5,5,true,true));
        topPanel.add(lblPw);
        topPanel.add(pw);

        getContentPane().add(topPanel);
        if((TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_PW_ALLWAYS) || (TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_RSA_ALLWAYS)) {
            getContentPane().add(centerPanel);
        }
        getContentPane().add(buttonPanel);

//        getContentPane().add(lblPw);
//        getContentPane().add(pw);
//        getContentPane().add(new JLabel());
//        getContentPane().add(buttonPanel);
//        setFocusTraversalPolicy(new FocusPolicy());
        pack();
        addWindowListener(this);
//        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(Client.getInstance().getFrame());

        Theme.registerComponent(this);
    }


    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("CANCEL")) {
            clearPW();
        } else if (e.getSource()== saveUsername) {
            return;
//            TradingShared.setLevel2Password(getPW());
        }
        hide();
    }

    public void keyPressed(KeyEvent e) {

    }

    public void keyReleased(KeyEvent e) {
        if (e.getKeyChar() == '\n') {
            e.consume();
            hide();
        }
    }

    public void keyTyped(KeyEvent e) {

    }

    public void tradeServerConnected() {

    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        pw.setText("");
        hide();
    }

    public void windowActivated(WindowEvent e) {
        /*SwingUtilities.invokeLater(new Runnable(){
            public void run() {
                pw.requestFocus();
            }
        });*/
    }

    public void windowClosed(WindowEvent e) {
        TradingConnectionNotifier.getInstance().removeConnectionListener(this);
    }

    public void windowClosing(WindowEvent e) {
        clearPW();
        password = null;
    }

    public void windowDeactivated(WindowEvent e) {

    }

    public void windowDeiconified(WindowEvent e) {

    }

    public void windowIconified(WindowEvent e) {

    }

    public void windowOpened(WindowEvent e) {

    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
    }
}
