package com.isi.csvr.trading.ui;

import com.isi.util.FlexGridLayout;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.Client;
import com.isi.csvr.trading.shared.*;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.datastore.CurrencyStore;
import com.isi.csvr.iframe.InternalFrame;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import java.util.ArrayList;
import java.awt.event.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: May 21, 2007
 * Time: 12:58:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashTransferUI extends InternalFrame implements TraderProfileDataListener, ItemListener, KeyListener {

    private TWComboBox fromPortfolio;
    private TWComboBox toPortfolio;
    private TWTextField txtFromAmount;
    private JLabel txtToAmount;
    private JLabel fromCurrency;
    private JLabel toCurrency;
    private ArrayList<TWComboItem> fromItems;
    private ArrayList<TWComboItem> toItems;


    public CashTransferUI() {
        super(false);
        createUI();
        TradingShared.getTrader().addAccountListener(this);
        populatePortfolios(fromPortfolio, fromItems,true);
        populatePortfolios(toPortfolio, toItems,false);
        adjustSelectedPortfolios();
    }

    private void createUI(){
        setTitle(Language.getString("CASH_TRANSFER"));
        
        String[] widths = {"40%", "60%"};
        String[] heights = {"22","22","22","22","25"};

        setLayout(new FlexGridLayout(widths, heights, 5,5));

        fromItems = new ArrayList<TWComboItem>();
        fromPortfolio = new TWComboBox(new TWComboModel(fromItems));
        fromPortfolio.addItemListener(this);
        toItems = new ArrayList<TWComboItem>();
        toPortfolio = new TWComboBox(new TWComboModel(toItems));
        toPortfolio.addItemListener(this);
        txtFromAmount = new TWTextField();
        txtFromAmount.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL, 3));
        txtFromAmount.addKeyListener(this);

        txtToAmount = new JLabel();

        TWButton submit = new TWButton(Language.getString("SUBMIT"));
        submit.setActionCommand("SUBMIT");
        submit.addActionListener(this);

        TWButton cancel = new TWButton(Language.getString("CANCEL"));
        cancel.setActionCommand("CANCEL");
        cancel.addActionListener(this);

        add(new JLabel(Language.getString("FROM_PORTFOLIO")));
        add(fromPortfolio);
        add(new JLabel(Language.getString("AMOUNT_TO")));
        add(createFromAmountPanel());
        add(new JLabel(Language.getString("TO_PORTFOLIO")));
        add(toPortfolio);
        add(new JLabel(Language.getString("AMOUNT_FROM")));
        add(createToAmountPanel());

        String[] buttonWidths = {"50%", "50%"};
        String[] buttonHeights = {"0"};
        JPanel buttonPanel = new JPanel(new FlexGridLayout(buttonWidths, buttonHeights,5,0,false, false));
        buttonPanel.add(submit, BorderLayout.WEST);
        buttonPanel.add(cancel, BorderLayout.EAST);
        add(new JLabel());
        add(buttonPanel);

        setClosable(true);
        setSize(350,170);
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        //Client.getInstance().getDesktop().add(this);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        GUISettings.applyOrientation(this);
        setLocationRelativeTo(Client.getInstance().getDesktop());
    }

    private JPanel createFromAmountPanel(){
        String[] widths = {"80%", "20%"};
        String[] heights = {"20"};

        JPanel panel = new JPanel(new FlexGridLayout(widths,  heights, 0,0));
        fromCurrency = new JLabel();
        fromCurrency.setHorizontalAlignment(JLabel.CENTER);
        fromCurrency.setOpaque(true);
        fromCurrency.setBackground(txtFromAmount.getBackground());
        panel.setBorder(txtFromAmount.getBorder());
        txtFromAmount.setBorder(null);
        txtFromAmount.setHorizontalAlignment(JTextField.TRAILING);
        panel.add(txtFromAmount);
        panel.add(fromCurrency);

        return panel;
    }

    private JPanel createToAmountPanel(){
        String[] widths = {"80%", "20%"};
        String[] heights = {"20"};

        JPanel panel = new JPanel(new FlexGridLayout(widths,  heights, 0,0));
        toCurrency = new JLabel();
        toCurrency.setHorizontalAlignment(JLabel.CENTER);
        toCurrency.setOpaque(true);
        toCurrency.setBackground(txtToAmount.getBackground());
        txtToAmount.setHorizontalAlignment(JLabel.TRAILING);
        panel.setBorder(UIManager.getBorder("TextField.border"));
        panel.add(txtToAmount);
        panel.add(toCurrency);

        return panel;
    }

    private void populatePortfolios(TWComboBox combo, ArrayList<TWComboItem> portfolioList, boolean isFirstPopup){
        try {
            if (TradingShared.getTrader() != null) {
                portfolioList.clear();
                TradingPortfolioRecord record;
                int currencyDecimals = 2;
                String selectedPortfolio = "";
//                byte path = Constants.PATH_PRIMARY;
                if(!isFirstPopup){
                    selectedPortfolio = ((TWComboItem) fromPortfolio.getSelectedItem()).getId();
//                    path = TradingShared.getTrader().getPath(selectedPortfolio);
                }
                ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    record = portfolios.get(i);
                    Account account =  TradingShared.getTrader().findAccountByPortfolio(record.getPortfolioID());
                    try {
                        currencyDecimals = SharedMethods.getCurrencyDecimalPlaces(account.getCurrency());
                    } catch (Exception e) {
                        currencyDecimals = 2;
                    }
//                    TWComboItem item = new TWComboItem(record.getPortfolioID(), record.getName() + " - " +SharedMethods.formatToDecimalPlaces(currencyDecimals, account.getCashAvailableForWithdrawal()) + " " + account.getCurrency());
                    TWComboItem item = new TWComboItem(record.getPortfolioID(), record.getName() + " - " +SharedMethods.formatToDecimalPlaces(currencyDecimals, account.getBalance()) + " " + account.getCurrency());
                    if(!isFirstPopup){
                        //todo - remove request came from Ruwan to enable the Third Party Cash Transfer
//                        if(TradingShared.getTrader().getPath(record.getPortfolioID()) == path){
                            portfolioList.add(item);
//                        }
                    } else{
                        portfolioList.add(item);
                    }
                    item = null;
                    record = null;
                }
            }
            try {
                combo.setSelectedIndex(0);
            } catch (Exception e) {
            }
            combo.updateUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);


        if (e.getActionCommand().equals("SUBMIT")) {
            double amount = 0;
            try {
                amount = Double.parseDouble(txtFromAmount.getText());
            } catch (NumberFormatException e1) {
            }
            if (amount <= 0) {
                SharedMethods.showMessage(Language.getString("MSG_INVALID_AMOUNT"), JOptionPane.ERROR_MESSAGE);
                return;
            }
            String fromPortf = ((TWComboItem) fromPortfolio.getSelectedItem()).getId();
            String toPortf = ((TWComboItem) toPortfolio.getSelectedItem()).getId();

            boolean sucess = TradeMethods.getSharedInstance().sendCashTransferRequest(fromPortf,toPortf, txtFromAmount.getText());
            if (sucess) {
                dispose();
            }
        } else if (e.getActionCommand().equals("CANCEL")) {
            dispose();
        }
    }


    public void accountDataChanged(String accountID) {

    }

    public void portfolioDataChanged(String portfolioID) {
        populatePortfolios(fromPortfolio, fromItems,true);
        populatePortfolios(toPortfolio, toItems,false);
        adjustSelectedPortfolios();
    }

    private void calculateToAmount(){
        try {
            double buyRate = CurrencyStore.getBuyRate(fromCurrency.getText(), toCurrency.getText(), TradingShared.getTrader().getPath(((TWComboItem) toPortfolio.getSelectedItem()).getId()));
            double sellRate = CurrencyStore.getBuyRate(fromCurrency.getText(), toCurrency.getText(), TradingShared.getTrader().getPath(((TWComboItem) toPortfolio.getSelectedItem()).getId()));
            txtToAmount.setText(SharedMethods.formatToCurrencyFormat(toCurrency.getText(), Double.parseDouble(txtFromAmount.getText()) * Math.min(buyRate, sellRate)));
        } catch (Exception e1) {
            txtToAmount.setText("");
        }
    }


    public void itemStateChanged(ItemEvent e) {
        if(e.getSource() == fromPortfolio){
            populatePortfolios(toPortfolio, toItems,false);
            try {
                Account fromAccount =  TradingShared.getTrader().findAccountByPortfolio(((TWComboItem)fromPortfolio.getSelectedItem()).getId());
                fromCurrency.setText(fromAccount.getCurrency());
                calculateToAmount();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        } else if(e.getSource() == toPortfolio) {
            Account toAccount =  TradingShared.getTrader().findAccountByPortfolio(((TWComboItem)toPortfolio.getSelectedItem()).getId());
            toCurrency.setText(toAccount.getCurrency());
            calculateToAmount();
        }
    }


    public void internalFrameClosed(InternalFrameEvent e) {
        super.internalFrameClosed(e);
        TradingShared.getTrader().removeAccountListener(this);
    }

    public void keyTyped(KeyEvent e) {

    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
         calculateToAmount();
    }

    private void adjustSelectedPortfolios(){
        if(!fromItems.isEmpty()) {
            fromPortfolio.setSelectedIndex(0);
        }
        if(!toItems.isEmpty() && toItems.size()>1){
            String frompfid = ((TWComboItem) fromPortfolio.getSelectedItem()).getId();
             Account account =  TradingShared.getTrader().findAccountByPortfolio(frompfid);
            int selected = 0;
            for(int i=0; i<toItems.size(); i++){
                TWComboItem item = toItems.get(i);
                Account account2 =  TradingShared.getTrader().findAccountByPortfolio(item.getId());
                if(!account.getAccountID().equals(account2.getAccountID()) ){
                    selected = i;
                    break;
                }
            }
           toPortfolio.setSelectedIndex(selected);
        }
    }
}
