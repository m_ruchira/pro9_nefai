package com.isi.csvr.trading.ui;

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.SendQueue;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.OrderSearchStore;
import com.isi.csvr.trading.datastore.IPOStore;
import com.isi.csvr.trading.datastore.IPORecord;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.trading.shared.TradeMessage;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.csvr.table.Table;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.Client;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.KeyEvent;
import java.awt.event.ActionListener;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 22-Jun-2008 Time: 15:55:14 To change this template use File | Settings
 * | File Templates.
 */
public class IPOSubscriptionUI extends InternalFrame implements TradingConnectionListener {
    public static IPOSubscriptionUI self;
    private JLabel ipoListLbl;
    private JLabel quantityLbl;
    private TWButton subscribeButton;
    private TWTextField quantityFdl;
    private Table ipoTable;
    private IPOTableModel tableModel;
    private JPanel mainPanel;
    private JPanel upperPanel;
    private JPanel tablePanel;
    private TWComboModel ipoComboModel;
    private TWComboModel pfComboModel;
    private ArrayList comboArray;
    private ViewSetting oSetting;
    private TWComboBox ipoCombo;
    private TWComboBox portfolioCombo;
    private ArrayList portfolioArray;

    public static IPOSubscriptionUI getSharedInstance() {
        if (self == null) {
            self = new IPOSubscriptionUI();
        }
        return self;
    }

    private IPOSubscriptionUI() {
        createUI();
        populatePortfolioCombo();
        sendInitialrequests();
    }

    public void createUI() {
        oSetting = ViewSettingsManager.getSummaryView("IPO_SUBSCRIBE");
        ipoListLbl = new JLabel(Language.getString("IPO_LIST"));
        quantityLbl = new JLabel(Language.getString("QUANTITY"));
        subscribeButton = new TWButton(Language.getString("SUBSCRIBE"));
        mainPanel = new JPanel();
        upperPanel = new JPanel();
        tablePanel = new JPanel();
        quantityFdl = new TWTextField();
        comboArray = IPOStore.getSharedInstance().getComboItemList();
        comboArray.add(new TWComboItem("One","Select IPO"));
        portfolioArray = new ArrayList();
        ipoComboModel = new TWComboModel(comboArray);
        pfComboModel = new TWComboModel(portfolioArray);
        ipoCombo = new TWComboBox(ipoComboModel);
        portfolioCombo = new TWComboBox(pfComboModel);
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"60", "100%"}, 0, 0));
        this.add(createUpperPanel());
        this.add(createTablePanel());
        setClosable(true);
        setSize(650, 350);
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        setTitle(oSetting.getCaption());
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        GUISettings.applyOrientation(this);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        setLocationRelativeTo(Client.getInstance().getDesktop());

    }

    private void populatePortfolioCombo() {
        ArrayList pfs = TradePortfolios.getInstance().getPortfolioList();
        portfolioCombo.removeAllItems();
        for (int i = 0; i < pfs.size(); i++) {
            final TradingPortfolioRecord record = (TradingPortfolioRecord) pfs.get(i);
            portfolioArray.add(new TWComboItem(record.getPortfolioID(), record.getName()));

        }
        portfolioCombo.setEnabled(true);
        portfolioCombo.setSelectedIndex(0);
    }

    public JPanel createUpperPanel() {
        upperPanel.setLayout(new FlexGridLayout(new String[]{"100", "20", "100", "20", "100", "20", "100", "100%"}, new String[]{"50%", "50%"}, 5, 5));
        upperPanel.add(ipoListLbl);
        upperPanel.add(new JLabel());
        upperPanel.add(ipoCombo);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel(Language.getString("PORTFOLIO")));
        upperPanel.add(new JLabel());
        upperPanel.add(portfolioCombo);
        upperPanel.add(new JLabel());
        upperPanel.add(quantityLbl);
        upperPanel.add(new JLabel());
        upperPanel.add(quantityFdl);
        upperPanel.add(new JLabel());
        upperPanel.add(subscribeButton);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        subscribeButton.addActionListener(this);
        portfolioCombo.addActionListener(this);
        ipoCombo.setSelectedIndex(0);
        return upperPanel;
    }

    public void sendInitialrequests() {


        String symbol;

        try {
            if (TradingShared.isConnected()) {
                //send IPO available
//                TradeMessage message1 = new TradeMessage(TradeMeta.MT_IPO_AVAILABLE);
//                String portfolio1 = ((TWComboItem) portfolioCombo.getSelectedItem()).getId();
//                String pfList = TradePortfolios.getInstance().getPortfolioListString();
//                message1.addData(pfList);
//                message1.addData(portfolio1);
//                SendQueue.getSharedInstance().addData(message1.toString(), TradingShared.getTrader().getPath(portfolio1));
//                message1 = null;
                //send IPO subscribed
                TradeMessage message2 = new TradeMessage(TradeMeta.MT_IPO_SUBSCRIBED);
                String portfolio2 = ((TWComboItem) portfolioCombo.getSelectedItem()).getId();
                String pfList2 = TradePortfolios.getInstance().getPortfolioListString();
                message2.addData("A"+pfList2);
                message2.addData("B"+portfolio2);
                SendQueue.getSharedInstance().addData(message2.toString(), TradingShared.getTrader().getPath(portfolio2));
                message2 = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public JPanel createTablePanel() {
        tablePanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        ipoTable = new Table();
        ipoTable.setPreferredSize(new Dimension(650, 290));
        tableModel = new IPOTableModel();
        try {
            ipoTable.getPopup().setMenuItem(getCopyMenu());
            ipoTable.getPopup().setMenuItem(getCopyWithHeadMenu());
            ipoTable.getPopup().setMenuItem(getIPOCancelMenu());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        ipoTable.setWindowType(ViewSettingsManager.IPO_SUBSCRIPTION);
        tableModel.setViewSettings(oSetting);
        ipoTable.setModel(tableModel);
        tableModel.setTable(ipoTable);
        ((SmartTable) ipoTable.getTable()).setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
        tableModel.applyColumnSettings();
        tableModel.updateGUI();
        oSetting.setParent(this);
        setLayer(GUISettings.TOP_LAYER);
        tablePanel.add(ipoTable);
        super.setTable(ipoTable);
        return tablePanel;
    }

    private void sendSubscriptionRequest() {
        try {
            if (TradingShared.isConnected()) {
                //send subscroption Request
                TradeMessage message1 = new TradeMessage(TradeMeta.MT_IPO_SUBSCRIPTION_RQ);
                String portfolio1 = ((TWComboItem) portfolioCombo.getSelectedItem()).getId();
                IPORecord  ipor= IPOStore.getSharedInstance().getIPORecord(((TWComboItem)ipoCombo.getSelectedItem()).getId());
                message1.addData("A"+portfolio1);
                message1.addData("B"+ipor.getValue());
                message1.addData("C"+quantityFdl.getText());
                message1.addData("D"+ipor.getId());
                message1.addData("E"+ipor.getSymbol());
                message1.addData("F"+ipor.getCurrency());
                SendQueue.getSharedInstance().addData(message1.toString(), TradingShared.getTrader().getPath(portfolio1));
                message1 = null;

            }
        } catch (Exception e) {
            System.out.println("Error Ocuured while Sending Subscription Request");
            e.printStackTrace();
        }
        //A-pf
        //B-amt
        //C-qty
        //D-ipoID
    }
    private void sendIPOCancelRequest(String id){
               try {
            if (TradingShared.isConnected()) {
                //send subscroption Request
                TradeMessage message1 = new TradeMessage(TradeMeta.MT_IPO_CANCEL_RQ);
                String portfolio1 = ((TWComboItem) portfolioCombo.getSelectedItem()).getId();
                IPORecord  ipor= IPOStore.getSharedInstance().getIPORecord(((TWComboItem)ipoCombo.getSelectedItem()).getId());
                message1.addData("A"+id);
                SendQueue.getSharedInstance().addData(message1.toString(), TradingShared.getTrader().getPath(portfolio1));
                message1 = null;

            }
        } catch (Exception e) {
            System.out.println("Error Ocuured while Sending Subscription Request");
            e.printStackTrace();
        }
    }

    public void actionPerformed(ActionEvent e) {
        super.actionPerformed(e);
        if (e.getSource().equals(subscribeButton)) {
            //send subscription request
            sendSubscriptionRequest();

        } else if (e.getSource().equals(portfolioCombo)) {
            String symbol;

            try {
                if (TradingShared.isConnected()) {
                    //send IPO available
//                    TradeMessage message1 = new TradeMessage(TradeMeta.MT_IPO_AVAILABLE);
//                    String portfolio1 = ((TWComboItem) portfolioCombo.getSelectedItem()).getId();
//                    String exchangeList1 = TradePortfolios.getInstance().getPortfolio(portfolio1).getExchangeString();
//                    message1.addData(exchangeList1);
//                    message1.addData(portfolio1);
//                    SendQueue.getSharedInstance().addData(message1.toString(), TradingShared.getTrader().getPath(portfolio1));
//                    message1 = null;
                    //send IPO subscribed
                    TradeMessage message2 = new TradeMessage(TradeMeta.MT_IPO_SUBSCRIBED);
                    String portfolio2 = ((TWComboItem) portfolioCombo.getSelectedItem()).getId();
                    String pfList2 = TradePortfolios.getInstance().getPortfolioListString();
                    message2.addData("A"+pfList2);
                    message2.addData("B"+portfolio2);
//                    SendQueue.getSharedInstance().addData(message2.toString(), TradingShared.getTrader().getPath(portfolio2));
                    message2 = null;
                }
            } catch (Exception e2) {
                e2.printStackTrace();
            }
        }

    }

    public TWMenuItem getCopyMenu() {
        TWMenuItem mnuCopy = new TWMenuItem(Language.getString("COPY"), "copy.gif");
        mnuCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK));
        mnuCopy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
//                    blotterModel.copyCells(blotterTable, false);
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });
        return mnuCopy;
    }

    public TWMenuItem getCopyWithHeadMenu() {
        TWMenuItem mnuCopyH = new TWMenuItem(Language.getString("COPY_WITH_HEAD"), "copywh.gif");
        mnuCopyH.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_DOWN_MASK));
        mnuCopyH.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
//                    blotterModel.copyCells(blotterTable, true);
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });
        return mnuCopyH;
    }
    private TWMenuItem getIPOCancelMenu(){
        TWMenuItem mnuCancelIPO = new TWMenuItem(Language.getString("CANCEL_IPO"), "ipocancel.gif");
        mnuCancelIPO.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_DOWN_MASK));
        mnuCancelIPO.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    int row = ipoTable.getTable().getSelectedRow();
                    String id=(String)ipoTable.getModel().getValueAt(row, -1);
                    System.out.println("IPO ID : "+id);
                    sendIPOCancelRequest(id);
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });
        return mnuCancelIPO;
    }
    public void tradeServerConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
        ArrayList pfs = TradePortfolios.getInstance().getPortfolioList();
        portfolioCombo.removeAllItems();
        for (int i = 0; i < pfs.size(); i++) {
            final TradingPortfolioRecord record = (TradingPortfolioRecord) pfs.get(i);
            portfolioArray.add(new TWComboItem(record.getPortfolioID(), record.getName()));

        }
        portfolioCombo.setEnabled(true);
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
        //To change body of implemented methods use File | Settings | File Templates.
        ArrayList pfs = TradePortfolios.getInstance().getPortfolioList();
        portfolioCombo.removeAllItems();
        for (int i = 0; i < pfs.size(); i++) {
            final TradingPortfolioRecord record = (TradingPortfolioRecord) pfs.get(i);
            portfolioArray.add(new TWComboItem(record.getPortfolioID(), record.getName()));

        }
        portfolioCombo.setEnabled(true);
    }

    public void tradeServerDisconnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

}
