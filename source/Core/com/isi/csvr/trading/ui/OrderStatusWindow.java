package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.print.PrintManager;
import com.isi.csvr.properties.DefaultSettingsManager;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.datastore.OrderDepthStore;
import com.isi.csvr.trading.datastore.OrderSearchStore;
import com.isi.csvr.trading.datastore.OrderStore;
import com.isi.csvr.trading.datastore.Transaction;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 31, 2004
 * Time: 11:01:13 AM
 */
public class OrderStatusWindow extends InternalFrame implements Runnable, MouseListener, ActionListener {

    private ViewSettingsManager viewSettingsManager;
    private String mubasherOrderNumber;
    private String clOrdId;
    private boolean active = true;

    private SnapshotLabel lblPortfolio;
    private SnapshotLabel lblPortfolioValue;
    private SnapshotLabel lblClOrdID;
    private SnapshotLabel lblClOrdIDValue;

    private SnapshotLabel lblMubasherOrderNo;
    private SnapshotLabel lblMubasherOrderNoValue;
    private SnapshotLabel lblAveragePrice;
    private SnapshotLabel lblAveragePriceValue;

    private SnapshotLabel lblOrderDate;
    private SnapshotLabel lblOrderDateValue;
    private SnapshotLabel lblOrderStatus;
    private SnapshotLabel lblOrderStatusValue;

    private SnapshotLabel lblSymbol;
    private SnapshotLabel lblSymbolValue;
    private SnapshotLabel lblSide;
    private SnapshotLabel lblSideValue;

    private SnapshotLabel lblPrice;
    private SnapshotLabel lblPriceValue;
    private SnapshotLabel lblType;
    private SnapshotLabel lblTypeValue;

    private SnapshotLabel lblQuantity;
    private SnapshotLabel lblQuantityValue;
    private SnapshotLabel lblFilledQuantity;
    private SnapshotLabel lblFilledQuantityValue;

    private SnapshotLabel lblMinFill;
    private SnapshotLabel lblMinFillValue;
    private SnapshotLabel lblPendingQuantity;
    private SnapshotLabel lblPendingQuantityValue;

    private SnapshotLabel lblDisclosed;
    private SnapshotLabel lblDisclosedValue;
    private SnapshotLabel lblOrderVal;
    private SnapshotLabel lblOrderValValue;

    private SnapshotLabel lblVat;
    private SnapshotLabel lblVatValue;

    private SnapshotLabel lblTiff;
    private SnapshotLabel lblTiffValue;
    private SnapshotLabel lblCommission;
    private SnapshotLabel lblCommissionValue;

    private SnapshotLabel lblTiffDate;
    private SnapshotLabel lblTiffDateValue;
    private SnapshotLabel lblNetValue;
    private SnapshotLabel lblNetValueValue;

    private SnapshotLabel lblExecTime;
    private SnapshotLabel lblExecTimeValue;
    private SnapshotLabel lblAvgCostOrGainLoss;
    private SnapshotLabel lblAvgCostOrGainLossValue;

    private SnapshotLabel lblTxnCurrency;
    private SnapshotLabel lblTxnCurrencyValue;
    private SnapshotLabel lblSettleCurrency;
    private SnapshotLabel lblSettleCurrencyValue;

    private SnapshotLabel lblExchangeRate;
    private SnapshotLabel lblExchangeRateValue;
    private SnapshotLabel lblNetSettleForCur;
    private SnapshotLabel lblNetSettleForCurValue;

    private int decimalPlaces;

    private Table detailTable;
    private OrderDepthModel detailModel;
    private JPanel summaryPanel;
    private JFrame printPanel;
//    private int defaultDecimalPlaces = -1;

    private boolean rejectedOrder;

    private JPopupMenu popup;
    private TWMenuItem print;
    private int decimalPlacesForStock;
    DateFormat formatters = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");

    public OrderStatusWindow(WindowDaemon oDaemon, ViewSettingsManager viewSettingsManager, String mubasherOrderNumber, String clOrdId) {

        super(oDaemon);

        ViewSetting oViewSetting = null;
        this.viewSettingsManager = viewSettingsManager;
        this.mubasherOrderNumber = mubasherOrderNumber;
        this.clOrdId = clOrdId;

        oViewSetting = viewSettingsManager.getOrderDepthView(ViewSettingsManager.DETAIL_QUOTE_ORDER + "|" + mubasherOrderNumber);

        if (oViewSetting == null) {
            oViewSetting = viewSettingsManager.getSymbolView("DETAIL_QUOTE_ORDER");
            oViewSetting = oViewSetting.getObject(); // clone the object
            GUISettings.setColumnSettings(oViewSetting, TWColumnSettings.getItem("DETAIL_QUOTE_COLS_ORDER"));
            DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oViewSetting.getType(), oViewSetting);
            oViewSetting.setID("" + mubasherOrderNumber);
            oViewSetting.setParent(this);
            viewSettingsManager.putOrderDepthView(ViewSettingsManager.DETAIL_QUOTE_ORDER + "|" + mubasherOrderNumber, oViewSetting);

        }
//        GUISettings.setColumnSettings(oViewSetting, TWColumnSettings.getItem("ORDER_DEPTH_COLS"));

//        String[] heights = {"0", "100%"};
//        String[] widths = {"100%"};
//        FlexGridLayout layout = new FlexGridLayout(widths, heights);
        JSplitPane split = new JSplitPane(JSplitPane.VERTICAL_SPLIT);
        split.setDividerSize(3);
        split.setDividerLocation(330);
        JScrollPane scroll = new JScrollPane();

        getContentPane().setLayout(new BorderLayout());
        createSumaryUI();
        createDetailUI(oViewSetting);
//        detailTable.setNorthPanel(summaryPanel);
//        getContentPane().add(detailTable, BorderLayout.CENTER);
         scroll.getViewport().add(summaryPanel);
        split.setLeftComponent(scroll);
        split.setRightComponent(detailTable);
        getContentPane().add(split, BorderLayout.CENTER);
        oViewSetting.setParent(this);
        setLocation(oViewSetting.getLocation());
//        setResizable(false);
        setResizable(true);
        setClosable(true);
//        setMaximizable(false);
        setMaximizable(true);
        setIconifiable(true);
        setLayer(GUISettings.TOP_LAYER);

        viewSettingsManager.setWindow(ViewSettingsManager.DETAIL_QUOTE_ORDER + "|" + mubasherOrderNumber, this);

        setTitle(Language.getString("ORDER_STATUS") + " [" + mubasherOrderNumber + "]");
        applySettings();
        setLocationRelativeTo(Client.getInstance().getDesktop());

        Thread thread = new Thread(this, "OrderStatusWindow");
        thread.start();
    }

//    private void createSumaryUI() {
//        ViewSetting oViewSetting = null;
//
//        Table summaryTable = new Table();
//        summaryTable.hideHeader();
//        summaryTable.getPopup().hideCopywHeadMenu();
//        summaryTable.setSortingDisabled();
//        summaryTable.setDQTableType();
//        summaryTable.hideCustomizer();
//        summaryTable.setWindowType(ViewSettingsManager.DETAIL_QUOTE_ORDER_SNAPSHOT);
//        summaryTable.setUseSameFontForHeader(true);
//
////            oViewSetting =  viewSettingsManager.getDTQView(ViewSettingsManager.DETAIL_QUOTE_ORDER_SNAPSHOT
////                +"|"+ orderNumber);
//
//        if (oViewSetting == null) {
//            oViewSetting = viewSettingsManager.getSymbolView("DETAIL_QUOTE_ORDER_SNAPSHOT");
//            oViewSetting = oViewSetting.getObject(); // clone the object
//            oViewSetting.setID("" + orderNumber);
//            //viewSettingsManager.putDTQView(ViewSettingsManager.DETAIL_QUOTE+"|"+ orderNumber,oViewSetting);
//        }
//
//        OrderSnapshotModel summaryModel = new OrderSnapshotModel(orderNumber);
//        summaryModel.setViewSettings(oViewSetting);
//        summaryTable.setModel(summaryModel);
//        summaryModel.setTable(summaryTable);
//        summaryTable.getTable().getTableHeader().setReorderingAllowed(false);
//        summaryModel.applyColumnSettings();
//
//        summaryTable.updateGUI();
//
//        getContentPane().add(summaryTable);
//    }

    private void createSumaryUI() {
      //  String[] widths = {"20%", "25%", "30%", "25%"};
        String[] widths = {"25%", "25%", "25%", "25%"};
        String[] heights = {"25", "25", "25", "25", "25", "25", "25", "25", "25", "25", "25",  "25", "25", "25"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights, 10, 0);
        summaryPanel = new JPanel(layout);
        summaryPanel.setOpaque(true);
        summaryPanel.setBackground(Color.white);


        lblPortfolio = new SnapshotLabel(Language.getString("PORTFOLIO"));
        lblPortfolioValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblClOrdID = new SnapshotLabel(Language.getString("CLORD_ID"));
        lblClOrdIDValue = new SnapshotLabel(InfoLabel.RIGHT);

        lblMubasherOrderNo = new SnapshotLabel(Language.getString("MUBASHER_ORDER_NUMBER"));
        lblMubasherOrderNoValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblAveragePrice = new SnapshotLabel(Language.getString("AVERAGE_PRICE"));
        lblAveragePriceValue = new SnapshotLabel(InfoLabel.RIGHT);

        lblOrderDate = new SnapshotLabel(Language.getString("ORDER_DATE"));
        lblOrderDateValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblOrderStatus = new SnapshotLabel(Language.getString("ORDER_STATUS"));
        lblOrderStatusValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblOrderStatusValue.addMouseListener(this);

        lblSymbol = new SnapshotLabel(Language.getString("SYMBOL"));
        lblSymbolValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblSide = new SnapshotLabel(Language.getString("SIDE"));
        lblSideValue = new SnapshotLabel(InfoLabel.RIGHT);

        lblPrice = new SnapshotLabel(Language.getString("PRICE"));
        lblPriceValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblType = new SnapshotLabel(Language.getString("ORDER_TYPE"));
        lblTypeValue = new SnapshotLabel(InfoLabel.RIGHT);

        lblQuantity = new SnapshotLabel(Language.getString("QUANTITY"));
        lblQuantityValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblFilledQuantity = new SnapshotLabel(Language.getString("FILLED_QTY"));
        lblFilledQuantityValue = new SnapshotLabel(InfoLabel.RIGHT);

        lblMinFill = new SnapshotLabel(Language.getString("MIN_FILL"));
        lblMinFillValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblPendingQuantity = new SnapshotLabel(Language.getString("PENDING_QTY"));
        lblPendingQuantityValue = new SnapshotLabel(InfoLabel.RIGHT);

        lblDisclosed = new SnapshotLabel(Language.getString("DISCLOSED"));
        lblDisclosedValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblOrderVal = new SnapshotLabel(Language.getString("ORDER_VALUE"));
        lblOrderValValue = new SnapshotLabel(InfoLabel.RIGHT);

        lblVat = new SnapshotLabel(Language.getString("VAT"));
        lblVatValue = new SnapshotLabel(InfoLabel.RIGHT);

        lblTiff = new SnapshotLabel(Language.getString("TIFF"));
        lblTiffValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblCommission = new SnapshotLabel(Language.getString("COMMISSION"));
        lblCommissionValue = new SnapshotLabel(InfoLabel.RIGHT);

        lblTiffDate = new SnapshotLabel(Language.getString("TIFF_DATE"));
        lblTiffDateValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblNetValue = new SnapshotLabel(Language.getString("NET_VALUE"));
        lblNetValueValue = new SnapshotLabel(InfoLabel.RIGHT);

        lblExecTime = new SnapshotLabel(Language.getString("EXECUTED_TIME"));
        lblExecTimeValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblAvgCostOrGainLoss = new SnapshotLabel("");
        lblAvgCostOrGainLossValue = new SnapshotLabel(InfoLabel.RIGHT);

        lblTxnCurrency = new SnapshotLabel(Language.getString("TXN_CURRENCY"));
        lblTxnCurrencyValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblSettleCurrency = new SnapshotLabel(Language.getString("SETTLEMENT_CURRENCY"));
        lblSettleCurrencyValue = new SnapshotLabel(InfoLabel.RIGHT);

        lblExchangeRate = new SnapshotLabel(Language.getString("EXCHANGE_RATE"));
        lblExchangeRateValue = new SnapshotLabel(InfoLabel.RIGHT);
        lblNetSettleForCur = new SnapshotLabel(Language.getString("NET_VALUE"));
        lblNetSettleForCurValue = new SnapshotLabel(InfoLabel.RIGHT);

        summaryPanel.add(lblPortfolio);
        summaryPanel.add(lblPortfolioValue);
        summaryPanel.add(lblClOrdID);
        summaryPanel.add(lblClOrdIDValue);

        summaryPanel.add(lblMubasherOrderNo);
        summaryPanel.add(lblMubasherOrderNoValue);
        summaryPanel.add(lblOrderStatus);
        summaryPanel.add(lblOrderStatusValue);

        summaryPanel.add(lblOrderDate);
        summaryPanel.add(lblOrderDateValue);
        summaryPanel.add(lblSide);
        summaryPanel.add(lblSideValue);

        summaryPanel.add(lblSymbol);
        summaryPanel.add(lblSymbolValue);
        summaryPanel.add(lblType);
        summaryPanel.add(lblTypeValue);

        summaryPanel.add(lblPrice);
        summaryPanel.add(lblPriceValue);
        summaryPanel.add(lblFilledQuantity);
        summaryPanel.add(lblFilledQuantityValue);

        summaryPanel.add(lblQuantity);
        summaryPanel.add(lblQuantityValue);
        summaryPanel.add(lblPendingQuantity);
        summaryPanel.add(lblPendingQuantityValue);

        summaryPanel.add(lblMinFill);
        summaryPanel.add(lblMinFillValue);
        summaryPanel.add(lblOrderVal);
        summaryPanel.add(lblOrderValValue);

        summaryPanel.add(lblDisclosed);
        summaryPanel.add(lblDisclosedValue);
        summaryPanel.add(lblCommission);
        summaryPanel.add(lblCommissionValue);

        summaryPanel.add(lblTiff);
        summaryPanel.add(lblTiffValue);
        summaryPanel.add(lblVat);
        summaryPanel.add(lblVatValue);


        summaryPanel.add(lblTiffDate);
        summaryPanel.add(lblTiffDateValue);
        summaryPanel.add(lblNetValue);
        summaryPanel.add(lblNetValueValue);


        summaryPanel.add(lblExecTime);
        summaryPanel.add(lblExecTimeValue);
        summaryPanel.add(lblAveragePrice);
        summaryPanel.add(lblAveragePriceValue);


        summaryPanel.add(lblTxnCurrency);
        summaryPanel.add(lblTxnCurrencyValue);
        summaryPanel.add(lblAvgCostOrGainLoss);
        summaryPanel.add(lblAvgCostOrGainLossValue);


        summaryPanel.add(lblSettleCurrency);
        summaryPanel.add(lblSettleCurrencyValue);
        summaryPanel.add(lblExchangeRate);
        summaryPanel.add(lblExchangeRateValue);

        summaryPanel.add(new JLabel());
        summaryPanel.add(new JLabel());
        summaryPanel.add(lblNetSettleForCur);
        summaryPanel.add(lblNetSettleForCurValue);

        summaryPanel.addMouseListener(this);

        popup = new JPopupMenu();
        print = new TWMenuItem(Language.getString("PRINT"), "print.gif");
        GUISettings.applyOrientation(print);
        print.addActionListener(this);
        print.setEnabled(true);
        print.setVisible(true);
        popup.add(print);
//        summaryPanel.add(popup);
//        this.

        //summaryPanel.setPreferredSize(new Dimension(100, 125));

//        getContentPane().add(summaryPanel);
    }

    public void actionPerformed(ActionEvent e){
        super.actionPerformed(e);
        if(e.getSource()==print){
            detailTable.getPopup().printTableWithPanel(PrintManager.TABLE_TYPE_DEFAULT);
       }
    }

    private void createDetailUI(ViewSetting oViewSetting) {

        detailTable = new Table();
        detailTable.setThreadID(Constants.ThreadTypes.TRADING);
//        detailTable.getPopup().hideCopywHeadMenu();
        //detailTable.hideCopywHeadMenu();
//        detailTable.hideCommonPopupPrint();
        detailTable.setSortingDisabled();
        detailTable.setDQTableType();
        detailTable.hideCustomizer();
        detailTable.setWindowType(ViewSettingsManager.DETAIL_QUOTE_ORDER);

        detailModel = new OrderDepthModel(mubasherOrderNumber);
        detailModel.setViewSettings(oViewSetting);
        detailTable.setModel(detailModel);
        detailModel.setTable(detailTable);
        detailTable.getTable().getTableHeader().setReorderingAllowed(false);
        detailModel.applyColumnSettings();

        setSize(oViewSetting.getSize());
               // setMaximumSize(new  Dimension((int)oViewSetting.getSize().getWidth(),2000));
             //   setMinimumSize(oViewSetting.getSize());
                setLocation(oViewSetting.getLocation());

        setLocation(oViewSetting.getLocation());

        detailTable.updateGUI();
        super.setTable(detailTable);

//        popup.add(detailTable.getPopup());
//        detailTable.getPopup().add(print);


        //getContentPane().add(detailTable);
    }

    public boolean finalizeWindow() {
        active = false;
        return super.finalizeWindow();

    }

    public void run() {
        while (active) {
            Transaction transaction;
            if (clOrdId != null){
                transaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(clOrdId);
                if (transaction == null) {
                    transaction = OrderSearchStore.getSharedInstance().searhTransactionByClOrderID(clOrdId);
                }
            }else {
                transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(mubasherOrderNumber);
                if (transaction == null) {
                    transaction = OrderSearchStore.getSharedInstance().searhTransactionByMubasherOrderID(mubasherOrderNumber);
                }
            }
            try {
                detailModel.setDecimalCount(SharedMethods.getDecimalPlaces(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()));
            } catch (Exception e) {
                e.printStackTrace();
                detailModel.setDecimalCount((byte)2);
            }

            /*Transaction transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(orderNumber);
            if (transaction == null) {
                transaction = OrderSearchStore.getSharedInstance().searhTransactionByMubasherOrderID(orderNumber);
            }*/
            //Exchange exchange = ExchangeStore.getSharedInstance().getExchange(transaction.getExchange());

            try {
                decimalPlaces = com.isi.csvr.datastore.CurrencyStore.getSharedInstance().getDecimalPlaces(transaction.getSettleCurrency());
            } catch (Exception e) {
              //  e.printStackTrace();
                decimalPlaces = 2;
            }
            try {
                decimalPlacesForStock = SharedMethods.getDecimalPlaces(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType());
                if (TWControl.isEnableDefaultValueDecimalCount())
                    decimalPlacesForStock = 3;
            } catch (Exception e) {
                e.printStackTrace();
                decimalPlacesForStock = 2;
            }

            lblPortfolioValue.setText("" + transaction.getPortfolioNo());
            lblClOrdIDValue.setText("" + transaction.getClOrderID());

            lblMubasherOrderNoValue.setText(transaction.getMubasherOrderNumber());
           // lblAveragePriceValue.setText(SharedMethods.toLongDeciamlFoamat(transaction.getAvgPrice()));
            lblAveragePriceValue.setText(SharedMethods.formatToDecimalPlaces(decimalPlacesForStock,transaction.getAvgPrice()));
//            String date = TradingShared.formatLongTimeToTW(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getOrderDate()));
            String date = formatters.format(new Date(transaction.getOrderDate()));
            if (Settings.getBooleanItem("HIDE_TIME_IN_EXECUTION_DATE"))
            date = date.substring(0,9);
            lblOrderDateValue.setText(date);
            //ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getOrderDate());
            lblOrderStatusValue.setText(TradingShared.getOMSStatusString(transaction.getStatus(), true));
            if (transaction.getStatus() == TradeMeta.T39_Rejected) {
                lblOrderStatusValue.setCursor(new Cursor(Cursor.HAND_CURSOR));
                rejectedOrder = true;
            } else {
                lblOrderStatusValue.setCursor(new Cursor(Cursor.DEFAULT_CURSOR));
                rejectedOrder = false;
            }

            lblSymbolValue.setText(transaction.getSymbol());
            lblSideValue.setText(TradingShared.getActionString(transaction.getSide()));

            lblTxnCurrencyValue.setText(transaction.getCurrency());
            lblSettleCurrencyValue.setText(transaction.getSettleCurrency());
            lblExchangeRateValue.setText(SharedMethods.toLongDeciamlFoamat(transaction.getSettleRate()));
            lblNetSettleForCur.setText(Language.getString("NET_VALUE") + "(" + transaction.getSettleCurrency() + ")");
            lblNetSettleForCurValue.setText(SharedMethods.toDeciamlFoamat(transaction.getCum_ordNetValue()));

            if  (transaction.getPrice() != 0){
                lblPriceValue.setText(SharedMethods.getDecimalFormat(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).format(transaction.getPrice()));
//                lblPriceValue.setText(SharedMethods.formatToDecimalPlaces(exchange.getPriceDecimalPlaces(), transaction.getPrice()));
//                lblPriceValue.setText(SharedMethods.formatToDecimalPlaces(defaultDecimalPlaces, adjustDecimalPoints(transaction.getExchange(), transaction.getPrice())));
            }else{
                lblPriceValue.setText(Language.getString("NA"));
            }
            lblTypeValue.setText(TradingShared.getTypeString(transaction.getType()));

            lblQuantityValue.setText(SharedMethods.toIntegerFoamat(transaction.getOrderQuantity()));
            lblFilledQuantityValue.setText(SharedMethods.toIntegerFoamat(transaction.getFilledQuantity()));

            lblMinFillValue.setText(SharedMethods.toIntegerFoamat(transaction.getMinQuantity()));
            lblPendingQuantityValue.setText(SharedMethods.toIntegerFoamat(transaction.getOrderQuantity() - transaction.getFilledQuantity()));

            lblDisclosedValue.setText(SharedMethods.toIntegerFoamat(transaction.getDiscloseQuantity()));
           // lblOrderValValue.setText(SharedMethods.formatToDecimalPlaces(decimalPlaces, transaction.getCum_ordValue()));
            //lblOrderValValue.setText(SharedMethods.formatToDecimalPlaces(decimalPlaces, transaction.getOrdValue()));
            lblOrderValValue.setText(SharedMethods.formatToDecimalPlaces(decimalPlacesForStock, transaction.getCum_ordValue()));

            lblTiffValue.setText(TradingShared.getTiffString(transaction.getTIFType(), null));
//            lblCommissionValue.setText(SharedMethods.formatToDecimalPlaces(decimalPlaces, transaction.getCum_commission()));
            lblCommissionValue.setText(SharedMethods.formatToDecimalPlaces(decimalPlacesForStock, transaction.getCum_commission()));
            lblVatValue.setText(SharedMethods.formatToDecimalPlaces(decimalPlacesForStock,transaction.getVat()));

            if ((transaction.getTIFType() == 4) || (transaction.getTIFType() == 1) || (transaction.getTIFType() == 3)) {
                lblTiffDateValue.setText(Language.getString("NA"));
            } else {
                lblTiffDateValue.setText(transaction.getExpireDate());
            }
//            lblNetValueValue.setText(SharedMethods.formatToDecimalPlaces(decimalPlaces, transaction.getOrdNetValue()));
            lblNetValueValue.setText(SharedMethods.formatToDecimalPlaces(decimalPlacesForStock, transaction.getOrdNetValue()));

            if (transaction.getTransactTime() <= Constants.ONE_DAY){
                lblExecTimeValue.setText(Language.getString("NA"));
            }else {
//                String orderDate = formatters.format(new Date(transaction.getTransactTime()));
//                lblExecTimeValue.setText(orderDate);
                lblExecTimeValue.setText(TradingShared.formatLongTimeToTW(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getTransactTime())));
            }
            if (transaction.getSide() == TradeMeta.BUY){
//                lblAvgCostOrGainLossValue.setText(SharedMethods.toDeciamlFoamat(transaction.getAverageCost()));
                lblAvgCostOrGainLossValue.setText(SharedMethods.formatToDecimalPlaces(decimalPlacesForStock, transaction.getAverageCost()));
                lblAvgCostOrGainLoss.setText(Language.getString("CUM_AVG_COST_PER_SHARE"));
            } else if (transaction.getSide() == TradeMeta.SELL){
//                lblAvgCostOrGainLossValue.setText(SharedMethods.toDeciamlFoamat((((transaction.getOrdNetValue())/transaction.getOrderQuantity()) - transaction.getAverageCost()) * transaction.getOrderQuantity()));
                lblAvgCostOrGainLossValue.setText(SharedMethods.formatToDecimalPlaces(decimalPlacesForStock,(((transaction.getOrdNetValue())/transaction.getOrderQuantity()) - transaction.getAverageCost()) * transaction.getOrderQuantity()));
                lblAvgCostOrGainLoss.setText(Language.getString("RIALIZED_GAIN_LOSS"));
            }

            transaction = null;
            try {
                Thread.sleep(1000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            }
        }
        OrderDepthStore.getSharedInstance().unRegister("" + mubasherOrderNumber);

    }

    class SnapshotLabel extends JLabel {

        public SnapshotLabel(String text) {
            super(text);
            setForeground(Color.black);
//            setOpaque(true);
//            setBackground(Color.white);
            setHorizontalAlignment(SwingUtilities.LEADING);
            setFont(getFont().deriveFont((float) getFont().getSize() + 1));
            setFont(getFont().deriveFont(Font.BOLD));
        }


        public SnapshotLabel(int alignment) {
            super();
            setHorizontalAlignment(alignment);
            setForeground(Color.black);
//            setOpaque(true);
//            setBackground(Color.white);
            setFont(getFont().deriveFont((float) getFont().getSize() + 1));
            setFont(getFont().deriveFont(Font.BOLD));
        }
    }

    public String[][] getPanelString() {
        int columns = 4;
        Component[] compArray =  summaryPanel.getComponents();
        String[][] strArray = new String[(int)(compArray.length/columns)][columns*2];
        int k=0;
        int m=0;
        for(int i=0; i < compArray.length; i= i+columns){
            m=0;
            for(int j=0; j< columns*2; j= j+2){
                strArray[k][j] = ((JLabel)compArray[i+m]).getHorizontalAlignment()+"";
                strArray[k][j+1] = ((JLabel)compArray[i+m]).getText();
                m++;
            }
            k++;
        }
        return strArray;
    }

    private float adjustDecimalPoints(String exchangeCode, float value) {
        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
        if (exchange != null)
            return value  / exchange.getPriceModificationFactor();
        else
            return value;
    }

    public void mouseClicked(MouseEvent e) {
        super.mouseClicked(e);
        if (e.getSource() == summaryPanel) {
            if (SwingUtilities.isRightMouseButton(e)) {
                SwingUtilities.updateComponentTreeUI(popup);
                GUISettings.showPopup(popup, summaryPanel, e.getX(), e.getY());
//                mousePoint = e.getPoint();
            }
        }
        if (e.getSource() == lblOrderStatusValue) {
            if (rejectedOrder) {
                TradeMethods.getSharedInstance().showRejectReason(mubasherOrderNumber);
            }
        }
    }
}
