package com.isi.csvr.trading.ui;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.BooleanTransferObject;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.datastore.OrderQueue;
import com.isi.csvr.trading.datastore.QueuedTransaction;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 27, 2004
 * Time: 2:10:12 PM
 */


public class OrderQModel extends CommonTable
        implements TableModel, CommonTableInterface {
//    IntTransferObject intTrasferObject;
    BooleanTransferObject booleanObject;
    private BasketOrderFrame frame;

    /**
     * Constructor
     */
    public OrderQModel(BasketOrderFrame frame) {
        this.frame = frame;
        init();
    }

    private void init() {
//        intTrasferObject = new IntTransferObject();
        booleanObject = new BooleanTransferObject();
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {
        return OrderQueue.getSharedInstance().size();
    }

    public void setSymbol(String symbol) {

    }

    public Object getValueAt(int iRow, int iCol) {
        QueuedTransaction transaction = OrderQueue.getSharedInstance().getTransaction(iRow);

        switch (iCol) {
            case -4:
                return DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getDecimalCount();
            case -1:
                return booleanObject.setValue(false);
            case 0:
//                return intTrasferObject.setValue(transaction.getSide());
                return ""+(transaction.getSide());
            case 1:
                return transaction.isSelected();
            case 2:
//                return "" + ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getOrderDate());
                return "" + Settings.getLocalTimeFor(transaction.getOrderDate());
//                return "" + transaction.getOrderDate();
            case 3:
                return "" + transaction.getSymbol();
            case 4:
                return "" + DataStore.getSharedInstance().getShortDescription(transaction.getSKeyWithMarketCode());
            case 5:
                return "" + TradingShared.getActionString(transaction.getSide());
            case 6:
                return "" + TradingShared.getTypeString(transaction.getType());
            case 7:
                if (transaction.getPrice() == 0)
                    return "" + Double.POSITIVE_INFINITY;
                else
                    return "" + transaction.getPrice();
            case 8:
                return "" + transaction.getOrderQuantity();
            case 9:
                return TradingShared.getTiffString(transaction.getTIFType(), transaction.getExpireDate());
            case 10:
                try {
                    return TradingShared.getTrader().getPortfolioName(transaction.getPortfolioNo());
                } catch(Exception e) {
                    return "";
                }
            default:
                return "0";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 0:
//            case 1:
            case 2:
            case 9:
                return String.class;
            case 1:
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
                return Number.class;
            case 'B':
                return Boolean.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        switch (super.getViewSettings().getRendererID(col)) {
            case 'B':
                return true;
            default:
                return false;
        }
    }

    public void setValueAt(Object value, int rowIndex, int columnIndex) {
        QueuedTransaction transaction = OrderQueue.getSharedInstance().getTransaction(rowIndex);
        switch (columnIndex) {
            case 1:
                transaction.setSelected(((Boolean) value).booleanValue());
                frame.fireTableDataChange();
                break;
        }
        transaction = null;
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }


}


