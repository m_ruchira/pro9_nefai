package com.isi.csvr.trading.ui;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 10, 2006
 * Time: 1:18:15 PM
 */
public class FundTransferTableRenderer extends TWBasicTableRenderer {
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private static TWDateFormat g_oDateTimeFormatHMS;
    private SimpleDateFormat g_oDateTimeFormatHM;
    private String[] g_asMarketStatus = new String[4];
    private String action;

    private final String g_sNA = "NA";

    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oDepFG1;
    private static Color g_oDepBG1;
    private static Color g_oDepFG2;
    private static Color g_oDepBG2;
    private static Color g_oWitFG1;
    private static Color g_oWitBG1;
    private static Color g_oWitFG2;
    private static Color g_oWitBG2;
    private Color upColor;
    private Color downColor;

    private int selectedDecimals = Constants.UNASSIGNED_DECIMAL_PLACES;
    private int stockDecimals = Constants.UNASSIGNED_DECIMAL_PLACES;


    private Color foreground, background;
    private static TWDecimalFormat oPriceFormat;
    private static TWDecimalFormat oQuantityFormat;
    private DefaultTableCellRenderer lblRenderer;
    private JCheckBox checkBoxRenderer;
    private double doubleValue;
    private long longValue;
    private Date date;


    public FundTransferTableRenderer(String[] asColumns, int[] asRendIDs) {
        g_asRendIDs = asRendIDs;

        reload();
        g_oDateTimeFormatHMS = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));//" dd:MM:yyyy '-' HH:mm:ss ");
        g_oDateTimeFormatHM = new SimpleDateFormat("dd/MM/yyyy");

        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");

        date = new Date();

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;

        checkBoxRenderer = new JCheckBox();
    }

    public static void reloadForPrinting() {
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oDepFG1 = Color.black;
        g_oDepBG1 = Color.white;
        g_oDepFG2 = Color.black;
        g_oDepBG2 = Color.white;
        g_oWitFG1 = Color.black;
        g_oWitBG1 = Color.white;
        g_oWitFG2 = Color.black;
        g_oWitBG2 = Color.white;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oDepFG1 = Theme.getColor("FUNDTRANSFER_TABLE_DEPOSITE_FGCOLOR1");
            g_oDepBG1 = Theme.getColor("FUNDTRANSFER_TABLE_DEPOSITE_BGCOLOR1");
            g_oDepFG2 = Theme.getColor("FUNDTRANSFER_TABLE_DEPOSITE_FGCOLOR2");
            g_oDepBG2 = Theme.getColor("FUNDTRANSFER_TABLE_DEPOSITE_BGCOLOR2");
            g_oWitFG1 = Theme.getColor("FUNDTRANSFER_TABLE_WITHDRAW_FGCOLOR1");
            g_oWitBG1 = Theme.getColor("FUNDTRANSFER_TABLE_WITHDRAW_BGCOLOR1");
            g_oWitFG2 = Theme.getColor("FUNDTRANSFER_TABLE_WITHDRAW_FGCOLOR2");
            g_oWitBG2 = Theme.getColor("FUNDTRANSFER_TABLE_WITHDRAW_BGCOLOR2");
        } catch (Exception e) {
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oDepFG1 = Color.black;
            g_oDepBG1 = Color.white;
            g_oDepFG2 = Color.black;
            g_oDepBG2 = Color.white;
            g_oWitFG1 = Color.black;
            g_oWitBG1 = Color.white;
            g_oWitFG2 = Color.black;
            g_oWitBG2 = Color.white;
        }
    }

    public void propertyChanged(int property) {
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
        g_asRendIDs = asRendIDs;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        action = ((String) table.getModel().getValueAt(row, 0));

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    if (action.equals(Language.getString("FUND_TRANSFER_NOTIFICATION"))) {
                        background = sett.getDepositeColor1BG();
                        foreground = sett.getDepositeColor1FG();
                    } else {
                        background = sett.getWithdrawColor1BG();
                        foreground = sett.getWithdrawColor1FG();
                    }
                } else {
                    if (action.equals(Language.getString("FUND_TRANSFER_NOTIFICATION"))) {
                        background = sett.getDepositeColor2BG();
                        foreground = sett.getDepositeColor2FG();
                    } else {
                        background = sett.getWithdrawColor2BG();
                        foreground = sett.getWithdrawColor2FG();
                    }
                }
            }
            upColor = sett.getPositiveChangeFG();
            downColor = sett.getNegativeChangeFG();
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else {
                if (row % 2 == 0) {
                    if (action.equals(Language.getString("FUND_TRANSFER_NOTIFICATION"))) {
                        background = g_oDepBG1;
                        foreground = g_oDepFG1;
                    } else {
                        background = g_oWitBG1;
                        foreground = g_oWitFG1;
                    }
                } else {
                    if (action.equals(Language.getString("FUND_TRANSFER_NOTIFICATION"))) {
                        background = g_oDepBG2;
                        foreground = g_oDepFG2;
                    } else {
                        background = g_oWitBG2;
                        foreground = g_oWitFG2;
                    }
                }
            }
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        try {
            stockDecimals = (Integer) table.getModel().getValueAt(row, -4);
        } catch (Exception e) {
            stockDecimals = 2;
        }

        if (selectedDecimals != stockDecimals) {
            applyDecimalPlaces(stockDecimals);
            selectedDecimals = stockDecimals;
        }

        int iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    if (Settings.isShowArabicNumbers())
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 2: // DESCRIPTION
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE

                    doubleValue = toDoubleValue(value);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                            (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oPriceFormat.format(doubleValue)));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // QUANTITY
                    lblRenderer.setText(oQuantityFormat.format(toLongValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);

                    break;
                case 5: // CHANGE
                    doubleValue = toDoubleValue(value);
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 6: // % CHANGE
                    //adPrice = (double[])value;
                    doubleValue = toDoubleValue(value);
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 8: // DATE TIME
                    longValue = toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateTimeFormatHM.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 'D': {// DATE TIME with secs
                    longValue = toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateTimeFormatHMS.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                }
                case 'B': // Bolean;
                    checkBoxRenderer.setSelected(((Boolean) value).booleanValue());
                    checkBoxRenderer.setForeground(foreground);
                    checkBoxRenderer.setBackground(background);
                    checkBoxRenderer.setHorizontalAlignment(g_iCenterAlign);
                    return checkBoxRenderer;
                case 'M': // Market Status
                    try {
                        lblRenderer.setText(g_asMarketStatus[toIntValue(value) - 1]);
                    } catch (Exception e) {
                        lblRenderer.setText(g_asMarketStatus[2]);
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            //e.printStackTrace();
            //lblRenderer.setText("err");
        }


        return lblRenderer;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private int toIntValue(Object oValue) throws Exception {
        return Integer.parseInt((String) oValue);
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return Double.parseDouble((String) oValue);
    }

    private void applyDecimalPlaces(int decimalPlaces) {
//        System.out.println("decimal places in tradeTableRenderer = "+decimalPlaces);
        switch (decimalPlaces) {
            case Constants.NO_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
//                    oChangeFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
//                    oPChangeFormat.applyPattern(Constants.PATTERN_NO_DECIMAL);
                break;
            case Constants.ONE_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
//                    oChangeFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
//                    oPChangeFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                break;
            case Constants.TWO_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
//                    oChangeFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
//                    oPChangeFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                break;
            case Constants.THREE_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
//                    oChangeFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
//                    oPChangeFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                break;
            case Constants.FOUR_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
//                    oChangeFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
//                    oPChangeFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                break;
        }
//            this.decimalPlaces = decimalPlaces;
    }
}