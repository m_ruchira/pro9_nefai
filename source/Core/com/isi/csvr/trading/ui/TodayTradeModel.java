package com.isi.csvr.trading.ui;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.dde.DDELinkInterface;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.datastore.OrderSearchStore;
import com.isi.csvr.trading.datastore.OrderStore;
import com.isi.csvr.trading.datastore.TPlusStore;
import com.isi.csvr.trading.datastore.Transaction;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: randulal
 * Date: Mar 27, 2012
 * Time: 11:54:28 AM
 * To change this template use File | Settings | File Templates.
 */


public class TodayTradeModel extends CommonTable
        implements TableModel, CommonTableInterface, ClipboardOwner {
    private int type;
    //    private IntTransferObject intTrasferObject;
    private BooleanTransferObject booleanTransferObject;
    private Clipboard clip;
    //    private DoubleTransferObject doubleTrasferObject;
    private static final TWDateFormat timeFormat1 = new TWDateFormat(Language.getString("BOARD_TIME_FORMAT"));
    private static final TWDateFormat timeFormat2 = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));
    private static long totalFilledBuyQty;
    private static long totalFilledSellQty;
    private static double totalBuyNetTrade;
    private static double totalSellNetTrade;
    //  private static double totalBuyComision;
    // private static double totalBuyAvgCostPrice;
    // private static double totalSellAvgCostPrice;
    //   private long oldQuantity;
    //   private double oldAvgCostPrice;
    //   private double newAvgCostPrice;

    /**
     * Constructor
     */
    public TodayTradeModel(int type) {
        this.type = type;
//        intTrasferObject = new IntTransferObject();
        clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        booleanTransferObject = new BooleanTransferObject();
//        doubleTrasferObject = new DoubleTransferObject();
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public void setSymbol(String symbol) {

    }

    public int getRowCount() {
        if (TradingShared.isReadyForTrading() && type == TradeMeta.BLOTTER_TODAY) {
            //   if (true) {                               /// change ! - sabbcbr
            return OrderSearchStore.getSharedInstance().todaySize() + 3;
        } else {
            return 0;
        }
    }

    public void clear() {
        OrderStore.getSharedInstance().clear();
    }

    public Object getValueAt(int iRow, int iCol) {
        Transaction transaction;

        if (iRow >= OrderSearchStore.getSharedInstance().todaySize() && type == TradeMeta.BLOTTER_TODAY) {
            if (iRow == OrderSearchStore.getSharedInstance().todaySize()) {
                try {
                    switch (iCol) {
                        case -13:
                            return true;
                        case -7:
                            return "1";
                        case -6:
                            return booleanTransferObject.setValue(false);
                        case -5:
                            return booleanTransferObject.setValue(false);
                        case -4:
                            return null;
                        case -1:
                            return booleanTransferObject.setValue(false);
                        case 0:
                            return 3 + "";
                        case 1:
                            return Language.getString("BUY_TOTAL");
                        case 3:
                            long qty = getTotalFilledQty(TradeMeta.BUY);
                            return qty != 0 ? qty + "" : "";
                        case 4:
                            double prce = getBuyAvgCostPrice();
                            return prce != 0 ? prce + "" : "";
                        case 6:
                            double tmt = getTotalTradeAmt(TradeMeta.BUY);
                            return tmt != 0 ? tmt + "" : "";
                        case 7:
                            double com = getTotalBuyCommision(TradeMeta.BUY);
                            return com != 0 ? com + "" : "";
                        case 8:
                            double net = getBuyNetTrade(TradeMeta.BUY);
                            return net != 0 ? net + "" : "";
                        default:
                            return "";
                    }
                } catch (Exception ex) {
                }
            } else if (iRow == OrderSearchStore.getSharedInstance().todaySize() + 1) {
                try {
                    switch (iCol) {
                        case -13:
                            return true;
                        case -7:
                            return "1";
                        case -6:
                            return booleanTransferObject.setValue(false);
                        case -5:
                            return booleanTransferObject.setValue(false);
                        case -4:
                            return null;
                        case -1:
                            return booleanTransferObject.setValue(false);
                        case 0:
                            return 3 + "";
                        case 1:
                            return Language.getString("SELL_TOTAL");
                        case 3:
                            //  return totalFilledSellQty + "";
                            long qty = getTotalFilledQty(TradeMeta.SELL);
                            return qty != 0 ? qty + "" : "";
                        case 4:
                            double prce = getSellAvgCostPrice();
                            return prce != 0 ? prce + "" : "";
                        case 6:
                            double tmt = getTotalTradeAmt(TradeMeta.SELL);
                            return tmt != 0 ? tmt + "" : "";
                        case 7:
                            double com = getTotalBuyCommision(TradeMeta.SELL);
                            return com != 0 ? com + "" : "";
                        case 8:
                            double net = getBuyNetTrade(TradeMeta.SELL);
                            return net != 0 ? net + "" : "";
                        default:
                            return "";
                    }
                } catch (Exception ex) {
                }
            } else if (iRow == OrderSearchStore.getSharedInstance().todaySize() + 2) {
                try {
                    switch (iCol) {
                        case -13:
                            return true;
                        case -7:
                            return "1";
                        case -6:
                            return booleanTransferObject.setValue(false);
                        case -5:
                            return booleanTransferObject.setValue(false);
                        case -4:
                            return null;
                        case -1:
                            return booleanTransferObject.setValue(false);
                        case 0:
                            return 3 + "";
                        case 1:
                            return Language.getString("GRAND_TOTAL");
                        case 3:
                            //  return totalFilledSellQty + "";
                            //  long qty = Math.abs(totalFilledBuyQty - totalFilledSellQty);
                            long qty = totalFilledBuyQty - totalFilledSellQty;
                            return "" + qty;
                        case 4:
                            return "" + getTotalAvgCostPrice();
                        case 6:
                            return "" + getTotalTradeAmt(-1);
                        case 7:
                            return "" + getTotalBuyCommision(-1);
                        case 8:
                            double net = totalBuyNetTrade - totalSellNetTrade;
                            return "" + net;
                        default:
                            return "";
                    }
                } catch (Exception ex) {
                }
            } else {
                return "";
            }
        }

        if (type == TradeMeta.BLOTTER_TODAY) {
            transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(iRow);
        } else {
            return "";
        }


        try {
            switch (iCol) {
                case -7:
                    return "" + transaction.getSecurityType();
                case -6:
                    return booleanTransferObject.setValue(transaction.isMarkForDelivery());
                case -5:
                    return booleanTransferObject.setValue(transaction.isDayOrder());
                case -4:
                    return DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getDecimalCount();
                case -1:
                    return booleanTransferObject.setValue(transaction.getRuleType() != TradeMeta.CONDITION_TYPE_NONE);
                case 0:
                    return "" + (transaction.getSide());
                case 1:
                    try {      // company code
                        return DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getCompanyCode();
                    } catch (Exception e) {
                        return transaction.getSymbol();
                    }
                case 2:
                    return TradingShared.getActionString(transaction.getSide());
                case 3:
                    return "" + transaction.getFilledQuantity();
                case 4:
                    return "" + transaction.getTodayAvgCostPrc();
                case 5:
                    /*if (transaction.getAvgPrice() == 0)
                        return "" + Double.POSITIVE_INFINITY;
                    else*/
                    return "" + transaction.getAvgPrice();
                case 6:
                    return "" + transaction.getNetAmount();     // = transaction.getFilledQuantity() * transaction.getAvgPrice()
                case 7:
                    return "" + transaction.getCommission_for_Filled();
                case 8:
                    return "" + transaction.getTodayNetTrade();

                default:
                    return "0";
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
//            case 0:
            case 1:
            case 2:
            case 20:
            case 22:
            case 23:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 0:
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 10:
            case 18:
            case 'M':
            case 'S':
            case 21:
            case 24:
            case 25:
            case 27:
            case 28:
                return Number.class;
//            case 0:
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
//    	switch (col){
//            case 3:
//            case 5:
//                return true;
//            default:
        return false;
//        }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, null, Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));     //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")
        customizerRecords[1] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_SELECTED_ROW, Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR"), Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("BUY_COLOR"), FIELD_BID_ROW1, Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"), Theme.getColor("BOARD_TABLE_CELL_BID_FGCOLOR1"));
        customizerRecords[3] = new CustomizerRecord(Language.getString("SELL_COLOR"), FIELD_ASK_ROW2, Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"), Theme.getColor("BOARD_TABLE_CELL_ASK_FGCOLOR1"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("CONDITIONAL_BUY_COLOR"), FIELD_CONDITIONAL_BUY, Theme.getColor("BOARD_TABLE_CONDITIONAL_BUY_BGCOLOR"), Theme.getColor("BOARD_TABLE_CONDITIONAL_BUY_FGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("CONDITIONAL_SELL_COLOR"), FIELD_CONDITIONAL_SELL, Theme.getColor("BOARD_TABLE_CONDITIONAL_SELL_BGCOLOR"), Theme.getColor("BOARD_TABLE_CONDITIONAL_SELL_FGCOLOR"));
        return customizerRecords;
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void copyCells(Table table, boolean copyHead) {
        int col;
        int cols;
        int row = table.getTable().getSelectedRow();
        int rows = table.getTable().getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
        if (copyHead)
            buffer.append(copyHeaders(table));

        int modelIndex;
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                modelIndex = table.getTable().convertColumnIndexToModel(col + c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    try {
                        Transaction transaction;
                        if (type == TradeMeta.BLOTTER_CURRENT) {
                            transaction = OrderStore.getSharedInstance().getFilteredTransaction(row + r);
                        } else if (type == TradeMeta.BLOTTER_TODAY) {
                            transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(row + r);
                        } else {
                            transaction = OrderSearchStore.getSharedInstance().getTransaction(row + r);
                        }
                        buffer.append(getValueAt(transaction, modelIndex));
                    } catch (Exception e) {
//                        e.printStackTrace();
                        buffer.append("");
                    }
                    if (c != (cols - 1))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
    }

    public String copyHeaders(Table table) {
        int col;
        int cols;
        //int row = this.getSelectedRow();
        //int rows= this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
        int modelIndex;
        for (int c = 0; c < cols; c++) {
            modelIndex = table.getTable().convertColumnIndexToModel(col + c);
            if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                buffer.append((String) table.getTable().getColumn("" + modelIndex).getHeaderValue());
                if (c != (cols - 1))  // do not append the tab char to the last item of the row
                    buffer.append("\t");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    private String getValueAt(Transaction transaction, int iCol) {


        switch (iCol) {
            case -7:
                return "" + transaction.getSecurityType();
            case -6:
                return "" + booleanTransferObject.setValue(transaction.isMarkForDelivery());
            case -5:
                return "" + booleanTransferObject.setValue(transaction.isDayOrder());
            case -4:
                return "" + DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getDecimalCount();
            case -1:
                return "" + booleanTransferObject.setValue(transaction.getRuleType() != TradeMeta.CONDITION_TYPE_NONE);
            case 0:
//                return intTrasferObject.setValue(transaction.getSide());
                return "" + (transaction.getSide());
            case 1:
//                return "" + ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getOrderDate());
                if (transaction.getOrderDate() > 86400000) {
                    return "" + timeFormat2.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getOrderDate()))) + " - " + timeFormat1.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getOrderDate())));
                } else {
                    return "";
                }
//                return transaction.getOrderDate();
            case 2:
                return "" + transaction.getSequenceNumber();
            case 3:
                return transaction.getClOrderID();
            case 4:
                return transaction.getExchange();
            case 5:
                return transaction.getSymbol();
            case 6:
                return TradingShared.getOMSStatusString(transaction.getStatus(), true);
            case 7:
                if (transaction.getAvgPrice() == 0)
                    return "" + Double.POSITIVE_INFINITY;
                else
                    return "" + transaction.getAvgPrice();
            case 8:
                return TradingShared.getActionString(transaction.getSide());
            case 9:
                return TradingShared.getTypeString(transaction.getType());
            case 10:
                if (transaction.getAvgPrice() == 0)
                    return "" + Double.POSITIVE_INFINITY;
                else
                    return "" + transaction.getAvgPrice();
            case 11:
                return "" + transaction.getOrderQuantity();
            case 12:
                return "" + transaction.getFilledQuantity();
            case 13:
                return TradingShared.getTiffString(transaction.getTIFType(), transaction.getExpireDate());
            case 14:
                return "" + transaction.getCommission_for_Filled();
            case 15:
                return transaction.getRejectReason();
            case 16:
                return DataStore.getSharedInstance().getCompanyName(SharedMethods.getKey(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()));
            case 17:
                return transaction.getMubasherOrderNumber();
            case 18:
                return "" + transaction.getStopLossPrice();
            case 19:
                return "" + transaction.getPendingQuantity();
            case 20:
                try {
                    if (transaction.getTransactTime() <= Constants.ONE_DAY) {
                        return "" + timeFormat2.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getTransactTime()))) + " - " + timeFormat1.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getTransactTime())));
                    } else {
                        return "";
                    }
                } catch (Exception e) {
                    return "0";
                }
            case 21:
                return "" + transaction.getTakeProfitPrice();
            case 22:
                try {
                    return DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getShortDescription();
                } catch (Exception e) {
                    return transaction.getSymbol();
                }
            case 23:
                try {
                    return DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getCompanyCode();
                } catch (Exception e) {
                    return transaction.getSymbol();
                }
            case 24:
                return "" + 1;
            case 25:
                return "" + 2;
            case 26:
                return "" + TPlusStore.getSharedInstance().getBookKeeper(transaction.getBookKeeper()).getName();
            case 27:
                return "" + transaction.getOrdValue();
            case 28:
                return "" + transaction.getOrdNetValue();
            default:
                return "0";
        }
    }

    private long getTotalFilledQty(int side) {
        Transaction transaction;
        long totalQty = 0;
        for (int i = 0; i < OrderSearchStore.getSharedInstance().todaySize(); i++) {
            transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(i);
            if (transaction.getSide() == side) {
                totalQty = totalQty + transaction.getFilledQuantity();
            }
        }
        if (side == TradeMeta.BUY) {
            totalFilledBuyQty = totalQty;
        } else {
            totalFilledSellQty = totalQty;
        }
        return totalQty;
    }


    private double getBuyNetTrade(int side) {
        Transaction transaction;
        double net = 0.0;
        for (int i = 0; i < OrderSearchStore.getSharedInstance().todaySize(); i++) {
            transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(i);
            if (transaction.getSide() == side) {
                net = net + transaction.getTodayNetTrade();
            }
        }
        if (side == TradeMeta.BUY) {
            totalBuyNetTrade = net;
        } else {
            totalSellNetTrade = net;
        }
        return net;
    }

    private double getTotalTradeAmt(int side) {
        Transaction transaction;
        double totalTradeAmt = 0;
        if (side == -1) {
            for (int i = 0; i < OrderSearchStore.getSharedInstance().todaySize(); i++) {
                transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(i);
                totalTradeAmt = totalTradeAmt + transaction.getNetAmount();
            }
            return totalTradeAmt;

        }
        for (int i = 0; i < OrderSearchStore.getSharedInstance().todaySize(); i++) {
            transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(i);
            if (transaction.getSide() == side) {
                totalTradeAmt = totalTradeAmt + transaction.getNetAmount();
            }
        }
        return totalTradeAmt;
    }

    private double getTotalBuyCommision(int side) {
        Transaction transaction;
        double totalcom = 0;
        if (side == -1) {
            for (int i = 0; i < OrderSearchStore.getSharedInstance().todaySize(); i++) {
                transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(i);
                totalcom = totalcom + transaction.getCommission_for_Filled();
            }
            return totalcom;

        }
        for (int i = 0; i < OrderSearchStore.getSharedInstance().todaySize(); i++) {
            transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(i);
            if (transaction.getSide() == side) {
                totalcom = totalcom + transaction.getCommission_for_Filled();
            }
        }
        return totalcom;
    }

    private double getTotalAvgCostPrice() {
        Transaction transaction;
        double avgCostPrice = 0;
        long oldQuantity = 0;

        for (int i = 0; i < OrderSearchStore.getSharedInstance().todaySize(); i++) {
            transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(i);
            if (transaction.getSide() == TradeMeta.BUY) {
                avgCostPrice = calculateBuyAvgCost(transaction.getAvgPrice() * transaction.getFilledQuantity(), transaction.getCommission_for_Filled(), avgCostPrice, oldQuantity, transaction.getFilledQuantity());
                oldQuantity = oldQuantity + transaction.getFilledQuantity();
            } else if (transaction.getSide() == TradeMeta.SELL) {
                avgCostPrice = calculateSellAvgCost(transaction.getAvgPrice() * transaction.getFilledQuantity(), transaction.getCommission_for_Filled(), avgCostPrice, oldQuantity, transaction.getFilledQuantity());
                oldQuantity = oldQuantity - transaction.getFilledQuantity();
            }
        }

        return avgCostPrice;
    }

    private double getBuyAvgCostPrice() {
        Transaction transaction;
        double oldAvgCostPrice = 0;
        long oldQuantity = 0;
        double newAvgCostPrice = 0;

        for (int i = 0; i < OrderSearchStore.getSharedInstance().todaySize(); i++) {
            transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(i);
            if (transaction.getSide() == TradeMeta.BUY) {
                newAvgCostPrice = calculateBuyAvgCost(transaction.getAvgPrice() * transaction.getFilledQuantity(), transaction.getCommission_for_Filled(), oldAvgCostPrice, oldQuantity, transaction.getFilledQuantity());
                oldQuantity = oldQuantity + transaction.getFilledQuantity();
                oldAvgCostPrice = newAvgCostPrice;
            }
        }

        return newAvgCostPrice;
    }

    private double getSellAvgCostPrice() {
        Transaction transaction;
        double oldAvgCostPrice = 0;
        long oldQuantity = 0;
        double newAvgCostPrice = 0;

        for (int i = 0; i < OrderSearchStore.getSharedInstance().todaySize(); i++) {
            transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(i);
            if (transaction.getSide() == TradeMeta.SELL) {
                newAvgCostPrice = calculateSellAvgCost(transaction.getAvgPrice() * transaction.getFilledQuantity(), transaction.getCommission_for_Filled(), oldAvgCostPrice, oldQuantity, transaction.getFilledQuantity());
                oldQuantity = oldQuantity - transaction.getFilledQuantity();
                oldAvgCostPrice = newAvgCostPrice;
            }
        }

        return newAvgCostPrice;
    }

    private double calculateBuyAvgCost(double totalOrderValue, double commision, double oldCostPrice, long oldQty, long qty) {
        if ((oldQty + qty) != 0) {
            return ((oldQty * oldCostPrice) + (totalOrderValue + commision)) / (oldQty + qty);
        }
        return 0.0;
    }

    private double calculateSellAvgCost(double totalOrderValue, double commision, double oldCostPrice, long oldQty, long qty) {
        if ((oldQty - qty) != 0) {
            return ((oldQty * oldCostPrice) - (totalOrderValue - commision)) / (oldQty - qty);
        }
        return 0.0;
    }

    /* public void getDDEString(Table table, boolean withHeadings) {
                int col;
        int cols;
        int row = table.getTable().getSelectedRow();
        int rows = table.getTable().getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        if (withHeadings)
            buffer.append(copyHeaders(table));
        for (int r = row; r < (rows + row); r++) {
            for (int c = 0; c < cols; c++) {
                int modelIndex = table.getTable().convertColumnIndexToModel(col + c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    if(TodayTradeModel.isDynamicData(modelIndex)){
                        buffer.append("=MRegionalDdeServer|'");
                        buffer.append("CF");   // CF(cash flow)  needs to be changed for "Today Trade".. etc
                        buffer.append(table.getTable().getModel().getValueAt(r,-2));
                        buffer.append("'!'");
                        buffer.append(modelIndex);
                        buffer.append("'");
                        buffer.append("*1");

                    }
                    else {
                        buffer.append(table.getTable().getModel().getValueAt(r,modelIndex));
                    }

                    if (c != (cols - 1))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
    }
     public static boolean isDynamicData(int column) {
        switch (column) {
            case 0:
            case 1:
                return false;
            default:
                return true;
        }
    }*/
}


