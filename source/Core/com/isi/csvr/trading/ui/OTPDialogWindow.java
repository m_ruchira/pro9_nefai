package com.isi.csvr.trading.ui;

import com.dfn.mtr.mix.beans.*;
import com.dfn.mtr.mix.common.MIXConstants;
import com.isi.csvr.Client;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.dfn.mtr.mix.beans.TransferObject;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.trading.AutoTradeLoginDaemon;
import com.isi.csvr.trading.SendQueue;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.shared.TradeMessage;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.util.MD5;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import java.io.OutputStream;
import java.util.ArrayList;

import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import static javax.swing.JOptionPane.PLAIN_MESSAGE;

/**
 * Created by IntelliJ IDEA.
 * User: shanakak
 * Date: Nov 15, 2013
 * Time: 1:27:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class OTPDialogWindow extends JDialog implements ActionListener, KeyListener {
    public static OTPDialogWindow self = null;
    private JPanel topPanel;
    private JPanel middlePanel;
    private JPanel bottomPanel;
    private JLabel lblOTPDescription;
    private JLabel lblOTP;
    private JPasswordField txtOTP;
    private JLabel lblOTPLink;
    private TWButton btnSend;
    private OutputStream out;

    public OTPDialogWindow() {
        createUI();
    }

    public static synchronized OTPDialogWindow getSharedInstance() {
        if (self == null) {
            self = new OTPDialogWindow();
        }
        return self;
    }

    private void createUI() {
        this.setTitle(Language.getString("OTP"));
        this.setSize(400, 150);
        this.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
//        this.setClosable(false);
//        this.setIconifiable(true);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
//        this.setLayer(GUISettings.TOP_LAYER);
//        this.hideTitleBarMenu();
        Client.getInstance().getDesktop().add(this);
        GUISettings.applyOrientation(this);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);

        topPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20"}, 5, 10));
        middlePanel = new JPanel(new FlexGridLayout(new String[]{"25%", "65%", "10%"}, new String[]{"20"}, 5, 10));
        bottomPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"20"}, 5, 10));
        lblOTPDescription = new JLabel(Language.getString("Your One Time Password (OTP) has been sent to ") + ("\n Please enter this number below to proceed"));
        lblOTP = new JLabel();
        txtOTP = new JPasswordField();
        lblOTPLink = new JLabel(Language.getString("Please click HERE if you still have not got the OTP to the above number"));
        btnSend = new TWButton(Language.getString("Send"));


        btnSend.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                sendOTPRequest();
            }
        });

        getContentPane().add(topPanel);
        getContentPane().add(middlePanel);
        getContentPane().add(bottomPanel);
        GUISettings.applyOrientation(getContentPane());
        this.add(getContentPane());
    }

    private void sendOTPRequest() {
        /*try {
            AuthenticationRequest mixaAthRequest = new AuthenticationRequest();
            MIXHeader header = new MIXHeader();
            header.setGroup(MIXConstants.GROUP_AUTHENTICATION);
            header.setRequestType(MIXConstants.REQUEST_TYPE_AUTH_NORMAL);
//        header.setChannelID(2);
            if (TWControl.enablePickNewChannelID()) {
                header.setChannelID(MIXConstants.CHANNEL_TYPE_TWS);
            } else {
                header.setChannelID(MIXConstants.CHANNEL_TYPE_NEWTW);
            }

            mixaAthRequest.setUsername(TradingShared.getTradeUserName());
            if (TWControl.enablePickNewChannelID()) {
                mixaAthRequest.setClientChannel(MIXConstants.CHANNEL_TYPE_TWS + "");
            } else {
                mixaAthRequest.setClientChannel(MIXConstants.CHANNEL_TYPE_NEWTW + "");
            }

            mixaAthRequest.setSessionID(TradingShared.getSessionID());
            mixaAthRequest.setIp(TradingShared.setIp(SharedMethods.getLocalIP()));
            mixaAthRequest.setClientVersion(Settings.TW_VERSION);
            mixaAthRequest.setOtpPinNumber(txtOTP.getText());

            TransferObject[] mixTransferObjects = new com.dfn.mtr.mix.beans.TransferObject[]{mixaAthRequest};
            MIXObject mixBasicObject = new MIXObject();
            mixBasicObject.setMIXHeader(header);
            mixBasicObject.setTransferObjects(mixTransferObjects);
//            out = socket.getOutputStream();
            out.write(mixBasicObject.getMIXString().getBytes());


        } catch (Exception e) {
            e.printStackTrace();
        }*/
    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void actionPerformed(ActionEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
