package com.isi.csvr.trading.ui;

import javax.swing.*;
import javax.swing.plaf.basic.BasicSpinnerUI;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * Created by hasakam on 11/1/2017.
 */
public class CustomPriceSpinnerUI extends BasicSpinnerUI {
    private OrderQuantityCalculator window;
    double increment = 0.01;
    double deincrement = -0.01;

    public CustomPriceSpinnerUI(OrderQuantityCalculator window) {
        this.window = window;
    }

    protected Component createNextButton() {
        JButton btnUp = (JButton) super.createNextButton();
        btnUp.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                try {
                    window.setPriceTickSize(increment);

                } catch (Exception e) {

                }

            }
        });
        return btnUp;
    }

    protected Component createPreviousButton() {
        JButton btnDown = (JButton) super.createPreviousButton();
        btnDown.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent ae) {
                try {
                    window.setPriceTickSize(deincrement);

                } catch (Exception e) {

                }
            }
        });
        return btnDown;
    }
}
