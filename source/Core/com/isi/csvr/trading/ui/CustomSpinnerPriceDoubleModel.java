package com.isi.csvr.trading.ui;

import javax.swing.*;

/**
 * Created by hasakam on 11/1/2017.
 */
public class CustomSpinnerPriceDoubleModel extends AbstractSpinnerModel {
    private double upStepSize;
    private double listIndex;
    private OrderQuantityCalculator cal;

    public CustomSpinnerPriceDoubleModel(double upStepSize, OrderQuantityCalculator cal) throws IllegalArgumentException {
        this.upStepSize = upStepSize;
        this.cal = cal;
    }

    public Object getValue() {
        return listIndex;
    }

    public void setValue(Object value) {
        if (value instanceof Double) {
            this.listIndex = (Double)value;
            fireStateChanged();
        } else if(value instanceof Integer){
            this.listIndex = (Integer)value;
            fireStateChanged();
        }
    }

    public Object getNextValue() {
        listIndex = listIndex + upStepSize;
        cal.incrementPrice(upStepSize);
        fireStateChanged();
        return listIndex;
    }

    public Object getPreviousValue() {
        listIndex = Math.max(listIndex  - upStepSize, 0);
        cal.incrementPrice(upStepSize);
        fireStateChanged();
        return listIndex;
    }

    public void setUPTickSize(double size){
        this.upStepSize = size;
    }
}
