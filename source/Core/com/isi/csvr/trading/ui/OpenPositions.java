package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.shared.TraderProfileDataListener;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.trading.datastore.OpenPositionsStore;
import com.isi.csvr.trading.TradeMethods;

import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.AbstractTableModel;
import javax.swing.*;
import java.awt.*;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Nov 17, 2007
 * Time: 12:50:24 PM
 */
public class OpenPositions  implements PopupMenuListener, ItemListener, ApplicationListener,
        TradingConnectionListener, TraderProfileDataListener, MouseListener {

    private ViewSetting oSetting = null;
    private Table table;

    public OpenPositions() {
    }

    public InternalFrame createUI(){
        oSetting = ViewSettingsManager.getSummaryView("OPENPOSITIONS");
        table = new Table();

        table.setPreferredSize(new Dimension(500, 100));
        OpenPositionsModel blotterModel = new OpenPositionsModel();
        table.setWindowType(ViewSettingsManager.OPEN_POSITIONS);
        blotterModel.setViewSettings(oSetting);
        table.setModel(blotterModel);
        blotterModel.setTable(table);
        blotterModel.applyColumnSettings();
        blotterModel.updateGUI();
        table.getTable().addMouseListener(this);

        InternalFrame frame = new InternalFrame(table);
        frame.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        frame.setColumnResizeable(true);
        frame.setDetachable(true);
        frame.setPrintable(true);
        oSetting.setParent(frame);
        frame.getContentPane().add(table);
        Client.getInstance().getDesktop().add(frame);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setMaximizable(true);
        frame.setIconifiable(true);
        frame.setTitle(Language.getString(oSetting.getCaptionID()));
        frame.updateUI();
        frame.applySettings();
        frame.setLayer(GUISettings.TOP_LAYER);
        frame.setVisible(false);

        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        GUISettings.applyOrientation(table);
        GUISettings.applyOrientation(frame);
        return frame;
    }

    private void sendSquareOrder(){
        int result = SharedMethods.showConfirmMessage("Close currnet position ?", JOptionPane.INFORMATION_MESSAGE);
        if ( result == JOptionPane.OK_OPTION){
            if (TradingShared.isConnected()) {
                int row = table.getTable().getSelectedRow();
                String portfolio = (String)table.getModel().getValueAt(row, 4);
                String exchange = (String)table.getModel().getValueAt(row, 2);
                String symbol = (String)table.getModel().getValueAt(row, 3);
                int side = (Integer)table.getModel().getValueAt(row, -103);
                long quantity = (Long)table.getModel().getValueAt(row, -104);

                if (side == TradeMeta.BUY){
                    side = TradeMeta.SELL;
                } else {
                    side = TradeMeta.BUY;
                }

                TradeMethods.getSharedInstance().sendNewOrder(true, false, 0, portfolio, exchange, symbol,
                       side, TradeMeta.ORDER_TYPE_MARKET, 0, 0, "*",(int)quantity, (short)TradeMeta.TIF_DAY,
                        0, -1, 0,false, 0, null, Meta.INSTRUMENT_QUOTE, -1, null, -1, null, null, null,-1,null,null,null,0d,null,-1,0);
            }
        }
    }

    public void accountDataChanged(String accountID) {

    }

    public void portfolioDataChanged(String portfolioID) {

    }

    public void applicationLoading(int percentage) {

    }

    public void applicationLoaded() {

    }

    public void applicationReadyForTransactions() {

    }

    public void applicationExiting() {

    }

    public void applicationTimeZoneChanged(TimeZone zone) {

    }

    public void workspaceWillLoad() {

    }

    public void workspaceLoaded() {

    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void snapshotProcessingStarted(Exchange exchange) {

    }

    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {

    }

    public void itemStateChanged(ItemEvent e) {

    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

    }

    public void popupMenuCanceled(PopupMenuEvent e) {

    }

    public void tradeServerConnected() {

    }

    public void tradeSecondaryPathConnected() {

    }

    public void tradeServerDisconnected() {
        OpenPositionsStore.getSharedInstance().clear();
    }

    public void mouseClicked(MouseEvent e) {
        int row = table.getTable().getSelectedRow();
        if (table.getTable().convertColumnIndexToModel(table.getTable().getSelectedColumn()) == 1){ // expander clicked
            boolean baseRecord =  (Boolean)table.getModel().getValueAt(row, 1); // base record property
            if (baseRecord){ // if this is a base record. then it is expandable
                boolean expanded =  (Boolean)table.getModel().getValueAt(row, -101); // what is the current status ( expabded ? )
                String key = (String)table.getModel().getValueAt(row, -102);
                if (expanded){ // invert the current status 
                    OpenPositionsStore.getSharedInstance().getExpandable(key).setCollapsed();
                } else {
                    OpenPositionsStore.getSharedInstance().getExpandable(key).setExpanded();
                }
                OpenPositionsStore.getSharedInstance().reconstructVisibleItems();
            }
            ((AbstractTableModel) table.getTable().getModel()).fireTableDataChanged(); // change data and repaint the table
        } else if (table.getTable().convertColumnIndexToModel(table.getTable().getSelectedColumn()) == 0){
            boolean closeable = (Boolean)table.getTable().getModel().getValueAt(row, 0);
            if (closeable){
                sendSquareOrder();
            }
        }
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }
}
