package com.isi.csvr.trading.ui;

import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.Table;
import com.isi.csvr.shared.*;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.trading.shared.CashLogRecord;
import com.isi.csvr.trading.datastore.*;
import com.isi.csvr.trading.marginTrading.MarginSymbolRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;

import javax.swing.table.TableModel;
import javax.swing.event.TableModelListener;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.Transferable;
import java.awt.datatransfer.StringSelection;
import java.awt.*;
import java.util.Date;

/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: Oct 30, 2008
 * Time: 6:25:48 PM
 * To change this template use File | Settings | File Templates.
 */
public class MarginableSymbolModel extends CommonTable
        implements TableModel, CommonTableInterface, ClipboardOwner {

    private Clipboard clip;
    private static final TWDateFormat timeFormat1 = new TWDateFormat(Language.getString("BOARD_TIME_FORMAT"));
    private static final TWDateFormat timeFormat2 = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));
    private String exchange="";

    /**
     * Constructor
     */
    public MarginableSymbolModel() {
        clip = Toolkit.getDefaultToolkit().getSystemClipboard();
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
     //   return 4;
//        return ViewSettingsManager.getSummaryView("MARGINABLE_SYMBOLS").getColumnHeadings().length;
    }

    public void setSymbol(String symbol) {

    }

    public int getRowCount() {
        if (TradingShared.isReadyForTrading()) {
           return MarginSymbolStore.getSharedInstance().getMarginbleStore(getExchange()).size();
        } else {
            return 0;
        }
    }

    public Object getValueAt(int iRow, int iCol) {

        MarginSymbolRecord mRecord= MarginSymbolStore.getSharedInstance().getTableRecord(getExchange(),iRow);

        try {
            switch (iCol) {

                case 0:
                    return TradingShared.getDisplaySmbol(mRecord.getSymbol(), null);
                case 1:
//                    return mRecord.getExchange();
                return ExchangeStore.getSharedInstance().getExchange(mRecord.getExchange().trim()).getDisplayExchange(); // To display exchange
                case 2:
                    return (mRecord.getMarginBuypct()*100)+"";
                case 3:
                    return mRecord.getMarginDayBuypct()+"";


                default:
                    return "0";
            }
        } catch (Exception e) {
            return "";
        }
       
    }

    public String getColumnName(int iCol) {
//        return super.getViewSettings().getColumnHeadings()[iCol];
        return ViewSettingsManager.getSummaryView("MARGINABLE_SYMBOLS").getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
//        switch (super.getViewSettings().getRendererID(iCol)) {
        switch (ViewSettingsManager.getSummaryView("MARGINABLE_SYMBOLS").getRendererID(iCol)) {
//            case 0:
            case 1:
            case 2:
            case 20:
            case 22:
            case 23:
            case 'B':
                return String.class;
            case 'P':
            case 'Q':
                return Number[].class;
            case 0:
            case 3:
            case 5:
            case 6:
            case 4:
            case 7:
            case 8:
            case 10:
            case 18:
            case 'M':
            case 'S':
            case 21:
            case 24:
            case 25:
                return Number.class;
//            case 0:
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void copyCells(Table table, boolean copyHead) {
        int col;
        int cols;
        int row = table.getTable().getSelectedRow();
        int rows = table.getTable().getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
        if (copyHead)
            buffer.append(copyHeaders(table));

        int modelIndex;
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                modelIndex = table.getTable().convertColumnIndexToModel(col + c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    try {
                        CashLogRecord transaction = CashLogSearchStore.getSharedInstance().getCashLogRecord(r);
                        buffer.append(getValueAt(transaction, modelIndex));
                    } catch (Exception e) {
//                        e.printStackTrace();
                        buffer.append("");
                    }
                    if (c != (cols - 1))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
    }

    public String copyHeaders(Table table) {
        int col;
        int cols;
        //int row = this.getSelectedRow();
        //int rows= this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
        int modelIndex;
        for (int c = 0; c < cols; c++) {
            modelIndex = table.getTable().convertColumnIndexToModel(col + c);
            if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                buffer.append((String) table.getTable().getColumn("" + modelIndex).getHeaderValue());
                if (c != (cols - 1))  // do not append the tab char to the last item of the row
                    buffer.append("\t");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public String getValueAt(CashLogRecord transaction, int iCol) {


        switch (iCol) {
            case -4:
                try {
                    return "" + DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getInstrumentType()).getDecimalCount();
                } catch (Exception e) {
                    return "" + Constants.DEFAULT_DECIMAL_COUNT;
                }
            case 0:
                return "" + timeFormat2.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getDate()))) + " - " + timeFormat1.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getDate())));
            case 1:
                return transaction.getExchange();
            case 2:
                return transaction.getSymbol();
            case 3:
                return transaction.getTransCurrency();
            case 4:
                return "" + transaction.getAmount();
            case 5:
                return "" + transaction.getAvgPrice();
            case 6:
                return "" + transaction.getAmountInTransCur();
            case 7:
                return "" + transaction.getAmountInSetlCur();
            case 8:
                return "" + transaction.getType();
            case 9:
                return "" + transaction.getOrderNo();
            case 10:
                return "" + transaction.getOrderQty();
            case 11:
                return "" + transaction.getCommision();
            case 12:
                return "" + timeFormat2.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getSettlementDate()))) + " - " + timeFormat1.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getSettlementDate())));
            case 13:
                return "" + transaction.getSettleRate();
            case 14:
                return "" + transaction.getReferenceNo();
            case 15:
                return "" + transaction.getSettleCurrency();
            default:
                return "0";
        }
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }
}
