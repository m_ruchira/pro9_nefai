package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.announcement.DateCombo;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;
import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Nov 5, 2005
 * Time: 12:04:53 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashTransactionSearch extends JDialog implements ActionListener, Themeable {
    private DateCombo fromDateCombo;
    private DateCombo toDateCombo;
    private TWButton okBtn;
    private TWButton cancelBtn;
    private ArrayList<TWComboItem> typeList;
    private TWComboModel typeModel;
    private JComboBox typeCombo;

    public CashTransactionSearch() {
        super(Client.getInstance().getFrame(), Language.getString("SEARCH"));
        createUI();
    }

    public void createUI() {
        this.setSize(300, 150);
        this.setLocationRelativeTo(Client.getInstance().getFrame());
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.setResizable(false);
        this.setUndecorated(false);
        Theme.registerComponent(this);
        Container container = this.getContentPane();
        String[] mainW = {"100%"};
        String[] mainH = {"100%", "40"};
        FlexGridLayout mainFlex = new FlexGridLayout(mainW, mainH);
        container.setLayout(mainFlex);

        JPanel searchPanel = new JPanel();
        String[] width = {"50%", "50%"};
        String[] height = {"20", "20", "20"};
        FlexGridLayout flex = new FlexGridLayout(width, height, 5, 5);
        searchPanel.setLayout(flex);
        JLabel fromDate = new JLabel(Language.getString("FROM"));
        fromDateCombo = new DateCombo(Client.getInstance().getFrame());
        JLabel toDate = new JLabel(Language.getString("TO"));
        toDateCombo = new DateCombo(Client.getInstance().getFrame());
        Calendar cal = Calendar.getInstance();
        fromDateCombo.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DATE));
        toDateCombo.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DATE));
        JLabel type = new JLabel(Language.getString("TRANSACTION_TYPE"));
        typeList = new ArrayList<TWComboItem>();
        populatetype();
        typeModel = new TWComboModel(typeList);
        typeCombo = new JComboBox(typeModel);
        if (typeList != null) {
            typeCombo.setSelectedIndex(0);
        }
        searchPanel.add(fromDate);
        searchPanel.add(fromDateCombo);
        searchPanel.add(toDate);
        searchPanel.add(toDateCombo);
        searchPanel.add(type);
        searchPanel.add(typeCombo);

        container.add(searchPanel);

        JPanel btnPanel = new JPanel();
        btnPanel.setLayout(new FlowLayout(FlowLayout.TRAILING));
        okBtn = new TWButton(Language.getString("SUBMIT"));
        cancelBtn = new TWButton(Language.getString("CLOSE"));
        okBtn.addActionListener(this);
        cancelBtn.addActionListener(this);
        btnPanel.add(okBtn);
        btnPanel.add(cancelBtn);
        container.add(btnPanel);
        this.applyTheme();
        GUISettings.applyOrientation(getContentPane());

    }

    public void applyTheme() {
        Color color1 = UIManager.getColor("controlLtHighlight");
        Color color2 = UIManager.getColor("controlDkShadow");
        SwingUtilities.updateComponentTreeUI(this);
        toDateCombo.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        toDateCombo.applyTheme();
        fromDateCombo.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        fromDateCombo.applyTheme();
    }

    private void populatetype() {
//        typeList.add(new TWComboItem(TradeMeta.CT_ALL, Language.getString("ALL")));
//        typeList.add(new TWComboItem(TradeMeta.CT_DEPOST, Language.getString("DEPOSITES")));
//        typeList.add(new TWComboItem(TradeMeta.CT_WITHDR, Language.getString("WITHDROWALS")));
//        typeList.add(new TWComboItem(TradeMeta.CT_STLBUY, Language.getString("BUY_SETTLE")));
//        typeList.add(new TWComboItem(TradeMeta.CT_STLSEL, Language.getString("SELL_SETTLE")));
//        typeList.add(new TWComboItem(TradeMeta.CT_CHARGS, Language.getString("OTHER_CHARGES")));
        //typeList.add(new TWComboItem("1",Language.getString("OPEN_BALANCE")));
        //typeList.add(new TWComboItem("7",Language.getString("CLOSING_BALANCE")));
    }

    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (obj == okBtn) {
            String from = fromDateCombo.getDateString();
            String to = toDateCombo.getDateString();
            //String type = (String)typeCombo.getSelectedItem();
            int curInt = typeCombo.getSelectedIndex();   //CurrencyStore.getSharedInstance().getC
            String type = typeList.get(curInt).getId();
            //Integer typeInt = new Integer(type);
            /*if(CashTransactionUI.self==null){
                TradeMethods.sendCashStatement();
            }*/
            CashTransactionUI.getSharedInstance().sendSummaryRequest(from, to);
            CashTransactionUI.getSharedInstance().sendDetailRequest(0, from, to, type);
            Theme.unRegisterComponent(this);
            this.dispose();
            // System.out.println("date   ---------- " + to);
        } else {
            Theme.unRegisterComponent(this);
            this.dispose();
        }

    }
}
