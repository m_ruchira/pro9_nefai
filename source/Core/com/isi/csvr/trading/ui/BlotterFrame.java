package com.isi.csvr.trading.ui;

import com.isi.csvr.*;
import com.isi.csvr.forex.*;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.Table;
import com.isi.csvr.trading.*;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.OrderSearchStore;
import com.isi.csvr.trading.datastore.OrderStore;
import com.isi.csvr.trading.datastore.Transaction;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TraderProfileDataListener;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 10, 2005
 * Time: 10:03:44 AM
 */
public class BlotterFrame extends InternalFrame implements PopupMenuListener, ItemListener, ApplicationListener, TradingConnectionListener, TraderProfileDataListener {

    private Table blotterTable;
    private ArrayList<TWComboItem> sideList;
    private ArrayList<TWComboItem> statusList;
//    private ArrayList<TWComboItem> exchangeList;
    private ArrayList<TWComboItem> portfoliolist;
    private TWComboBox cmbPortf;
    private TWComboBox cmbSide;
    private TWComboBox cmbStatus;
    private TWComboBox cmbExchange;
    private TWButton btnRefresh;
    private static TWButton btnSave = new TWButton(Language.getString("SAVE"));
    private Date date;

    //change start
    TWMenu mnuDecimalPlaces;
    TWRadioButtonMenuItem mnuDefaultDecimal;
    TWRadioButtonMenuItem mnuNoDecimal;
    TWRadioButtonMenuItem mnuOneDecimal;
    TWRadioButtonMenuItem mnuTwoDecimal;
    TWRadioButtonMenuItem mnuThrDecimal;
    TWRadioButtonMenuItem mnuFouDecimal;
    private byte decimalPlaces = Constants.ONE_DECIMAL_PLACES;
    ViewSetting oSetting = null;
    InternalFrame frame;
    private BlotterModel blotterModel;
    //change end

    public BlotterFrame() {
//        ExchangeStore.getSharedInstance().addExchangeListener(this);
//        TradingConnectionNotifier.getInstance().addConnectionListener(this);
    }

    public InternalFrame createBlotterFrame() {
        blotterTable = new Table(new int[]{24, 25});
        blotterTable.setThreadID(Constants.ThreadTypes.TRADING);

        TWMenuItem mnuMarkForDelivery = new TWMenuItem(Language.getString("MARK_FOR_DELIVERY"), "markfordelivery.gif");
        mnuMarkForDelivery.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                markForDeliveryActionPerformed();
            }
        });
        blotterTable.getPopup().setMenuItem(mnuMarkForDelivery);

        TWMenuItem mnuAmend = new TWMenuItem(Language.getString("AMEND_ORDER"), "amendorder.gif");
        mnuAmend.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                amendActionPerformed();
            }
        });
        blotterTable.getPopup().setMenuItem(mnuAmend);

        TWMenuItem mnuScaleOrder = new TWMenuItem(Language.getString("SCALE_ORDER"), "scaleorder.gif");
        mnuScaleOrder.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                } else {
                    SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
                }

            }
        });
        //blotterTable.getPopup().setMenuItem(mnuScaleOrder);

        TWMenuItem mnuDelete = new TWMenuItem(Language.getString("CANCEL_ORDER"), "cancelorder.gif");
        mnuDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                cancelActionPerformed();

            }
        });
        blotterTable.getPopup().setMenuItem(mnuDelete);

        TWMenuItem mnuCancelAll = new TWMenuItem(Language.getString("CANCEL_ALL"), "cancellall.gif");
        mnuCancelAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().cancelAll();
            }
        });
        mnuCancelAll.setVisible(true);
        mnuCancelAll.setEnabled(true);
        blotterTable.getPopup().setMenuItem(mnuCancelAll);

        TWMenuItem mnuCancelAllBuy = new TWMenuItem(Language.getString("CANCEL_ALL_BUY"), "cancelallbuy.gif");
        mnuCancelAllBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().cancelAllBuy();
            }
        });
        blotterTable.getPopup().setMenuItem(mnuCancelAllBuy);

        TWMenuItem mnuCancelAllSell = new TWMenuItem(Language.getString("CANCEL_ALL_SELL"), "cancelallsell.gif");
        mnuCancelAllSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                TradeMethods.getSharedInstance().cancelAllSell();
            }
        });
        blotterTable.getPopup().setMenuItem(mnuCancelAllSell);

        TWMenuItem mnuDuplicate = new TWMenuItem(Language.getString("DUPLICATE_ORDER"), "amendorder.gif");
        mnuDuplicate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int row = blotterTable.getTable().getSelectedRow();
                row = ((TableSorter) blotterTable.getTable().getModel()).getUnsortedRowFor(row);
                TradeMethods.getSharedInstance().duplicateOrder(OrderStore.getSharedInstance().getFilteredTransaction(row), null);
            }
        });
        blotterTable.getPopup().setMenuItem(mnuDuplicate);

        TWMenuItem mnuShowHistory = new TWMenuItem(Language.getString("SHOW_ORDER_HISTORY"), "orderdetails.gif");
        mnuShowHistory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                    int row = blotterTable.getTable().getSelectedRow();
                    row = ((TableSorter) blotterTable.getTable().getModel()).getUnsortedRowFor(row);
                    String orderID = (String) blotterTable.getTable().getModel().getValueAt(row, 3);
                    String mubasherOrderID = (String) blotterTable.getTable().getModel().getValueAt(row, 17);
                    Transaction transaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(orderID);
                    if (transaction == null) {
                        transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(mubasherOrderID);
                    }
                    TradeMethods.getSharedInstance().showDetailQuoteOrder(transaction);
                    transaction = null;
                } else {
                    SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
                }
            }
        });
        blotterTable.getPopup().setMenuItem(mnuShowHistory);
        blotterTable.getPopup().addPopupMenuListener(this);
        blotterTable.setSortingEnabled();
        createDecimalMenu(blotterTable.getPopup());
//        setDecimalPlaces(decimalPlaces);

        blotterTable.setPreferredSize(new Dimension(500, 100));
        blotterModel = new BlotterModel(TradeMeta.BLOTTER_CURRENT);
        try {
            blotterTable.getPopup().setMenuItem(getCopyMenu());
            blotterTable.getPopup().setMenuItem(getCopyWithHeadMenu());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        oSetting = ViewSettingsManager.getSummaryView("BLOTTER");
//        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("BLOTTER_COLS"));
        blotterTable.setWindowType(ViewSettingsManager.TRADE_PENDING_ORDERS);
        blotterModel.setViewSettings(oSetting);
        blotterTable.setModel(blotterModel);
        blotterModel.setTable(blotterTable);
        blotterModel.applyColumnSettings();
//        blotterTable.getTable().setDefaultEditor(String.class, new DefaultCellEditor(new TWTextField()));
//        blotterTable.getTable().setDefaultEditor(Object.class, new DefaultCellEditor(new TWTextField()));
//        blotterTable.getTable().setDefaultEditor(Number.class, new DefaultCellEditor(new TWTextField()));
        blotterModel.updateGUI();

        frame = new InternalFrame(blotterTable);
        frame.setDefaultCloseOperation(InternalFrame.HIDE_ON_CLOSE);
        frame.setColumnResizeable(true);
        frame.setDetachable(true);
        frame.setPrintable(true);
        oSetting.setParent(frame);
        frame.getContentPane().add(blotterTable);
        Client.getInstance().getDesktop().add(frame);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setMaximizable(true);
        frame.setIconifiable(true);

        if (TradingShared.isConnected() && BrokerConfig.getSharedInstance().isLoggedInUserShown() &&
                ((TradingShared.getTrader().getTraderName(Constants.PATH_PRIMARY) != null) && (!TradingShared.getTrader().getTraderName(Constants.PATH_PRIMARY).equals("")))) {

            //     String sPortfolio = cmbPortf.getSelectedItem().toString();
            frame.setTitle(Language.getString(oSetting.getCaptionID()) + " - " + TradingShared.TRADE_USER_STATUS); // + Language.getString("GDR_MESSAGE"));
        } else {
            frame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        }


        frame.updateUI();
        frame.applySettings();
        frame.setLayer(GUISettings.TOP_LAYER);
        frame.setVisible(false);

        try {
            decimalPlaces = Byte.parseByte(oSetting.getProperty(ViewConstants.VC_DECIMAL_PLACES));
        } catch (Exception e) {
            decimalPlaces = Constants.TWO_DECIMAL_PLACES;
        }
        setDecimalPlaces(decimalPlaces);

        blotterTable.setNorthPanel(getBlotterFilter());

        blotterTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int row = blotterTable.getTable().getSelectedRow();
                int col = blotterTable.getTable().convertColumnIndexToModel(blotterTable.getTable().getSelectedColumn());
                String orderID = (String) blotterTable.getTable().getModel().getValueAt(row, 3);
                String mubasherOrderID = (String) blotterTable.getTable().getModel().getValueAt(row, 17);
                Transaction transaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(orderID);
                char status = (char) transaction.getStatus();
                String exchange = transaction.getExchange();
                if (transaction == null) {
                    transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(mubasherOrderID);
                }
                if ((col == 24) && (SwingUtilities.isLeftMouseButton(e))) {
                    boolean amendStatus = false;
                    amendStatus = BrokerConfig.getSharedInstance().isOrderAmendAllowedByBroker(TradingShared.getTrader().getPath(transaction.getPortfolioNo()), transaction.getOrderCategory() + "");
                    if (amendStatus) {
                        amendStatus = (TradeMethods.isValidForAmend(transaction.getType()));
                    }
                    if (amendStatus) {
                        amendStatus = TradeMethods.getSharedInstance().getItemValidity("AMEND", "ORDER_STATUS_WINDOW", exchange, status);
                    }
                    if (amendStatus) {
                        amendActionPerformed();
                    }
                } else if ((col == 25) && (SwingUtilities.isLeftMouseButton(e))) {
                    boolean cancelstatus = TradeMethods.isValidForCancel(transaction.getType());
                    if (cancelstatus) {
                        cancelstatus = TradeMethods.getSharedInstance().getItemValidity("CANCEL", "ORDER_STATUS_WINDOW", exchange, status);
                    }
                    if (cancelstatus) {
                        cancelActionPerformed();
                    }
                } else {
                    if (e.getClickCount() > 1) {
                        if (BrokerConfig.getSharedInstance().showOrderDetails(TradingShared.getTrader().getPath(transaction.getPortfolioNo()))) {
                            TradeMethods.getSharedInstance().showDetailQuoteOrder(transaction);
                        }
//                        TradeMethods.getSharedInstance().showDetailQuoteOrder(transaction);
                    } else {
                        if (SwingUtilities.isLeftMouseButton(e)) {
                            if (col == 6) { //Bug ID <#0024> earlier it was if (col == 5)
                                if ((transaction != null) && (transaction.getStatus() == TradeMeta.T39_Rejected)) {
                                    TradeMethods.getSharedInstance().showRejectReasonFull(transaction);
                                    transaction = null;
                                }
                            }
                        }
                    }
                }
                transaction = null;
            }
        });

        GUISettings.applyOrientation(frame);
        Application.getInstance().addApplicationListener(this);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        populateExchanges();
        populatePortfolios();
        blotterTable.getPopup().showLinkMenus();
        return frame;
    }

    private void amendActionPerformed() {
        if (TradingShared.isReadyForTrading()) {
            int row = blotterTable.getTable().getSelectedRow();
            row = ((TableSorter) blotterTable.getTable().getModel()).getUnsortedRowFor(row);
            Transaction transaction = OrderStore.getSharedInstance().getFilteredTransaction(row);
//            Stock st= DataStore.getSharedInstance().getStockObject(transaction.getExchange(),transaction.getSymbol(), transaction.getSecurityType());
            if (transaction.getSecurityType() == Meta.INSTRUMENT_FOREX) {
                ForexAmmendOrder fao = ForexAmmendOrder.getSharedInstance(transaction);
                fao.setVisible(true);
                GUISettings.setLocationRelativeTo(fao, Client.getInstance().getDesktop());
            } else {
                Client.getInstance().doTransaction(TradeMeta.AMEND, transaction, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, transaction.getPortfolioNo(), transaction.getBookKeeper());
            }
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }
    }

    private void markForDeliveryActionPerformed() {
        if (TradingShared.isReadyForTrading()) {
            int row = blotterTable.getTable().getSelectedRow();
            row = ((TableSorter) blotterTable.getTable().getModel()).getUnsortedRowFor(row);
            Transaction transaction = OrderStore.getSharedInstance().getFilteredTransaction(row);
            TradeMethods.sendMarkForDeliveryOrder(transaction);
//            TradeMethods.amendOrder(transaction, false, transaction.getSide(), transaction.getType(), transaction.getPrice(),
//                                    transaction.getCurrency(), (int)transaction.getOrderQuantity(), (short)transaction.getTIFType(),
//                                    TradingShared.getGoodTillLong(transaction.getExpireDate()), ""+transaction.getDiscloseQuantity(), ""+transaction.getMinQuantity(),
//                                    transaction.getStopLossPrice(), transaction.getRule(), transaction.getRuleExpireTime(),
//                                    transaction.getTakeProfitPrice(), transaction.getSliceTimeInterval(), transaction.getSliceBlockSIze(),
//                                    transaction.getRuleType(), transaction.getStopLossType(), false,
//                                    transaction.getTrailStopLossPrice(), true);
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }
    }

    private void cancelActionPerformed() {
        if (TradingShared.isReadyForTrading()) {
            int row = blotterTable.getTable().getSelectedRow();
            row = ((TableSorter) blotterTable.getTable().getModel()).getUnsortedRowFor(row);
            Transaction transaction = OrderStore.getSharedInstance().getFilteredTransaction(row);
//            Stock st= DataStore.getSharedInstance().getStockObject(transaction.getExchange(),transaction.getSymbol(), transaction.getSecurityType());
            if (transaction.getSecurityType() == Meta.INSTRUMENT_FOREX) {
                ForexCancelOrder fco = ForexCancelOrder.getSharedInstance(transaction);
                fco.setVisible(true);
                GUISettings.setLocationRelativeTo(fco, Client.getInstance().getDesktop());
            } else if (TradeMethods.isValidForCancel(transaction.getType())) {
                Client.getInstance().doTransaction(TradeMeta.CANCEL,
                        transaction, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, transaction.getPortfolioNo(), transaction.getBookKeeper());
            }
//            Client.getInstance().doTransaction(TradeMeta.CANCEL,
//                    transaction, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, transaction.getPortfolioNo());
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }
    }

    public void popupMenuCanceled(PopupMenuEvent e) {

    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        JPopupMenu menu = (JPopupMenu) e.getSource();
        Component[] items = menu.getComponents();

        try {
            int row = blotterTable.getTable().getSelectedRow();
//            row = ((TableSorter) blotterTable.getTable().getModel()).getUnsortedRowFor(row);
//            Transaction transaction = OrderStore.getSharedInstance().getFilteredTransaction(row);
            String orderID = (String) blotterTable.getTable().getModel().getValueAt(row, 3);
            String mubasherOrderID = (String) blotterTable.getTable().getModel().getValueAt(row, 17);
            Transaction transaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(orderID);
            if (transaction == null) {
                transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(mubasherOrderID);
            }
            char status = (char) transaction.getStatus();
            String exchange = transaction.getExchange();

            for (int i = 0; i < items.length; i++) {
                if (items[i] instanceof TWMenuItem) {
                    TWMenuItem item = (TWMenuItem) items[i];
                    if (item.getText().equals(Language.getString("AMEND_ORDER"))) {
                        boolean amendStatus = false;
                        amendStatus = BrokerConfig.getSharedInstance().isOrderAmendAllowedByBroker(TradingShared.getTrader().getPath(transaction.getPortfolioNo()), transaction.getOrderCategory() + "");
                        if (amendStatus) {
                            amendStatus = (TradeMethods.isValidForAmend(transaction.getType()));
                        }
                        if (amendStatus) {
                            amendStatus = TradeMethods.getSharedInstance().getItemValidity("AMEND", "ORDER_STATUS_WINDOW", exchange, status);
                        }
                        item.setEnabled(amendStatus);
                    } else if (item.getText().equals(Language.getString("CANCEL_ORDER"))) {
                        boolean cancelstatus = TradeMethods.isValidForCancel(transaction.getType());
                        if (cancelstatus) {
                            cancelstatus = TradeMethods.getSharedInstance().getItemValidity("CANCEL", "ORDER_STATUS_WINDOW", exchange, status);
                        }
                        item.setEnabled(cancelstatus);
                    } else if (item.getText().equals(Language.getString("DUPLICATE_ORDER"))) {
                        item.setEnabled(TradeMethods.getSharedInstance().getItemValidity("DUPLICATE", "ORDER_STATUS_WINDOW", exchange, status));
                    } else if (item.getText().equals(Language.getString("SHOW_ORDER_HISTORY"))) {
                        if (BrokerConfig.getSharedInstance().showOrderHistoryInBlotter(TradingShared.getTrader().getPath(transaction.getPortfolioNo()))) {
                            if (transaction.getClOrderID() == null) {
                                item.setEnabled(false);
                            } else {
                                item.setEnabled(true);
                            }
                        } else {
                            item.setVisible(false);
                        }
                    } /*else if (item.getText().equals( Language.getString("CANCEL_ALL"))) {
                        if (transaction.getClOrderID() == null) {
                            item.setEnabled(false);
                        } else {
                            item.setEnabled(true);
                        }
                    } */
                    else if (item.getText().equals(Language.getString("MARK_FOR_DELIVERY"))) {
                        if (transaction.isDayOrder()) {
                            item.setVisible(true);
//                            item.setEnabled(TradeMethods.getSharedInstance().isValidForMarkForDelivery(status));
                            item.setEnabled(TradeMethods.getSharedInstance().getItemValidity("MARK_FOR_DELIVERY", "ORDER_STATUS_WINDOW", exchange, status));
                        } else {
                            item.setVisible(false);
                        }
                    }
                    item = null;
                }
            }

        } catch (Exception e1) {
            // error occured, hide both items
            for (int i = 0; i < items.length; i++) {
                if (items[i] instanceof TWMenuItem) {
                    TWMenuItem item = (TWMenuItem) items[i];
                    System.out.println(item.getText());
                    if ((item.getText().equals(Language.getString("AMEND_ORDER"))) ||
                            (item.getText().equals(Language.getString("CANCEL_ORDER"))) ||
                            (item.getText().equals(Language.getString("DUPLICATE_ORDER"))) ||
                            (item.getText().equals(Language.getString("SHOW_ORDER_HISTORY")))) {
                        item.setEnabled(false);
                    } else {
                        item.setEnabled(true);
                    }
                    item = null;
                }
            }
        }

        items = null;
        menu = null;
    }

    private JPanel getBlotterFilter() {
        String[] widths = {"8%", "12%", "8%", "11%", "11%", "12%", "9%", "12%", "8%", "9%"};
        //String[] widths = {"80", "140", "80", "80", "80", "140", "80", "135", "100"};
        String[] heights = {"20"};
        JPanel panel = new JPanel(new FlexGridLayout(widths, heights, 1, 1));

        JLabel lblPortf = new JLabel(Language.getString("PORTFOLIO"));
        panel.add(lblPortf);
        portfoliolist = new ArrayList<TWComboItem>();
        TWComboModel portfModel = new TWComboModel(portfoliolist);
        cmbPortf = new TWComboBox(portfModel);
        cmbPortf.setExtendedMode(true);
        panel.add(cmbPortf);
        cmbPortf.addItemListener(this);

        JLabel lblType = new JLabel(Language.getString("SIDE"), JLabel.CENTER);
        panel.add(lblType);
        sideList = new ArrayList<TWComboItem>();
        TWComboModel typeComboModel = new TWComboModel(sideList);
        sideList.add(new TWComboItem("*", Language.getString("ALL")));
        sideList.add(new TWComboItem("" + TradeMeta.BUY, Language.getString("BUY")));
        sideList.add(new TWComboItem("" + TradeMeta.SELL, Language.getString("SELL")));
        cmbSide = new TWComboBox(typeComboModel);
        panel.add(cmbSide);
        try {
            cmbSide.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        cmbSide.addItemListener(this);

        JLabel lblStatus = new JLabel(Language.getString("ORDER_STATUS"), JLabel.CENTER);
        panel.add(lblStatus);
        statusList = new ArrayList<TWComboItem>();
        TWComboModel statusomboModel = new TWComboModel(statusList);
        statusList.add(new TWComboItem("*", Language.getString("ALL")));
        statusList.add(new TWComboItem("OPEN", Language.getString("ORDER_STAUS_OPEN")));
        statusList.add(new TWComboItem(TradeMeta.T39_Partially_Filled + "," + TradeMeta.T39_PF_EXPIRED + "," + TradeMeta.T39_PF_CANCELLED, TradingShared.T39_PARTIALLY_FILLED));
        statusList.add(new TWComboItem("" + TradeMeta.T39_Filled, TradingShared.T39_FILLED));
        statusList.add(new TWComboItem("" + TradeMeta.T39_PENDING_TRIGGER, TradingShared.T39_PENDING_TRIGGER));
        statusList.add(new TWComboItem("" + TradeMeta.T39_Canceled, TradingShared.T39_CANCELED));
        statusList.add(new TWComboItem("" + TradeMeta.T39_Rejected, TradingShared.T39_REJECTED));
        if(TWControl.getClientName().equals(Constants.CO_BRANDING_CLIENT_NEFAIE)){
            statusList.add(new TWComboItem("" + TradeMeta.T39_MURABAHA_PENDING, TradingShared.T39_MURABAHA_PENDING));
            statusList.add(new TWComboItem("" + TradeMeta.T39_MURABAHA_APPROVED, TradingShared.T39_MURABAHA_APPROVED));
        }
        statusList.add(new TWComboItem(TradeMeta.T39_COMPLETED + "," + TradeMeta.T39_Completed, TradingShared.T39_COMPLETED));
        cmbStatus = new TWComboBox(statusomboModel);
        panel.add(cmbStatus);
        try {
            cmbStatus.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();
        }
        cmbStatus.addItemListener(this);

        JLabel lblExchange = new JLabel(Language.getString("EXCHANGE"), JLabel.CENTER);
        panel.add(lblExchange);
//        exchangeList = new ArrayList<TWComboItem>();
//        TWComboModel exchangeComboModel = new TWComboModel(exchangeList);
        cmbExchange = new TWComboBox();
        panel.add(cmbExchange);
        cmbExchange.addItemListener(this);

        panel.add(btnSave);

        btnRefresh = new TWButton(Language.getString("REFRESH"));
        btnRefresh.setVisible(false);
        panel.add(btnRefresh);
        btnRefresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new Thread("Blotter Refresh Button") {
                    public void run() {
                        try {
                            btnRefresh.setEnabled(false);
                            if (TWControl.clearDataBeforeRefresh()) {
                                blotterModel.clear();
                            }
                            TradeMethods.requestPortfolioData(Constants.PATH_PRIMARY);
                            TradeMethods.requestAccountData(Constants.PATH_PRIMARY);
                            if (TradingShared.SECONDARY_LOGIN_SUCCESS) {
                                TradeMethods.requestPortfolioData(Constants.PATH_SECONDARY);
                                TradeMethods.requestAccountData(Constants.PATH_SECONDARY);
                            }
                            Thread.sleep(5000);
                            btnRefresh.setEnabled(true);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                }.start();
            }
        });
        checkRefreshButton();

        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                exportDataToExcelSheet(blotterTable);
            }
        });

        /*final JCheckBox check = new JCheckBox(Language.getString("SHOW_ONLY_PENDING_ORDERS"));
        check.setSelected(Settings.getBooleanItem("SHOW_ONLY_PENDING_ORDERS"));
        OrderStore.getSharedInstance().setShowOnlyOpenOrders(check.isSelected());

        check.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                OrderStore.getSharedInstance().setShowOnlyOpenOrders(check.isSelected());
                Settings.setItem("SHOW_ONLY_PENDING_ORDERS", "" + check.isSelected());
            }                                                                        
        });
        panel.add(check);*/


        return panel;
    }

    private void populatePortfolios() {
        cmbPortf.removeItemListener(this);
        try {
            portfoliolist.clear();
            if (TradingShared.getTrader() != null) {

                TradingPortfolioRecord record;
                ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    record = portfolios.get(i);
                    portfoliolist.add(new TWComboItem(record.getPortfolioID(), record.getName()));
                    record = null;
                }

            }
            Collections.sort(portfoliolist);
            portfoliolist.add(0, new TWComboItem("*", Language.getString("ALL")));
            try {
                cmbPortf.setSelectedIndex(0);
            } catch (Exception e) {
                e.printStackTrace();
            }
            cmbPortf.updateUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
        cmbPortf.addItemListener(this);
    }

    private String copyHeaders(Table table) {
        int cols = 0;
        StringBuffer buffer = new StringBuffer("");

        cols = table.getTable().getColumnCount();


        for (int i = 0; i < cols; i++) {
            int modalIndex = table.getTable().convertColumnIndexToModel(i);
            if (table.getTable().getColumn("" + modalIndex).getWidth() != 0) {
                String header = table.getTable().getColumn("" + modalIndex).getHeaderValue().toString();

                //buffer.append("<b>");
                buffer.append(header);
                //buffer.append("</b>");
                buffer.append(",");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public String saveExcelFileAs() {
        int status = 0;
        String sFile;
        String filePathCharts = "./";
        JFileChooser oFC = new JFileChooser(filePathCharts);
        oFC.setFileSelectionMode(JFileChooser.FILES_ONLY);
        oFC.setDialogTitle(Language.getString("SCANNER_SAVE_RESULTS"));
        ExtensionFileFilter oFilter1 = new ExtensionFileFilter("CSV format", "csv");
        ExtensionFileFilter oFilter2 = new ExtensionFileFilter("Microsoft excel 2007 format", "xlsx");

        oFC.setFileFilter(oFilter1);
//        oFC.setFileFilter(oFilter2); //2007 excel (xlss) format

        oFC.setAcceptAllFileFilterUsed(false);
        status = oFC.showSaveDialog(this);
        if (status == JFileChooser.APPROVE_OPTION) {
            sFile = oFC.getSelectedFile().getAbsolutePath();
            filePathCharts = oFC.getCurrentDirectory().getAbsolutePath();
            if (oFC.getFileFilter() == oFilter1) {
                if (!sFile.toLowerCase().endsWith(".csv"))
                    sFile += ".csv";
            } else if (oFC.getFileFilter() == oFilter2) {
                if (!sFile.toLowerCase().endsWith(".xlsx"))
                    sFile += ".xlsx";
            }
            return sFile;
        } else {
            return null;
        }
    }

    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c <= '/' || c >= ':') {
                return false;
            }
        }
        return true;
    }

    private void exportDataToExcelSheet(Table table) {

        StringBuffer buffer = new StringBuffer("");
        buffer.append(copyHeaders(table));
        int rows = table.getTable().getRowCount();
        int cols = table.getTable().getColumnCount();
        TWDecimalFormat f;
        TWDateFormat g_oDateTimeFormatHM;

        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                int modelIndex = table.getTable().convertColumnIndexToModel(c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    Class cls = table.getTable().getColumnClass(c);
//                    Object val = table.getTable().getValueAt(r, c);
                    Object val = null;
                    String temp = "";
                    Transaction transaction;
                    transaction = OrderStore.getSharedInstance().getFilteredTransaction(r);
                    if (modelIndex != 6) {
                        val = blotterModel.getValueAt(transaction, modelIndex);
                    } else {
                        val = TradingShared.getOMSStatusString(transaction.getStatus());
                    }

                    if (val != null) {
                        temp = val.toString();
                        if (cls == Double.class || cls == Float.class) {
                            if (!temp.equals("")) {
                                f = new TWDecimalFormat("#####0.00");
                                temp = f.format(Double.parseDouble(temp));
                            }
                        } else if (cls == String.class) {
                            /*if (isInteger(temp)) {
                                f = new TWDecimalFormat("#####0.00");
                                temp = f.format(Double.parseDouble(temp));
                            }*/
                            if (!temp.equals("")) {
                                temp = temp.replaceAll("(?i)<script.*?</script>", "");
                            }
                        } else if (cls == Object.class) {
                            long longValue;
                            date = new Date();
                            Object value = table.getTable().getValueAt(r, c);
                            g_oDateTimeFormatHM = new TWDateFormat("dd/MM/yyyy - HH:mm:ss");
                            try {
                                longValue = toLongValue(value);
                                date.setTime(longValue);
                                temp = g_oDateTimeFormatHM.format(date);
                            } catch (Exception e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }

                        }
                    }
                    buffer.append(temp);
                    buffer.append(" ");
                    buffer.append(",");

                }
            }
            buffer.append("\n");
        }

        //System.out.println(" =============== BUFFER ============");
        //System.out.println(buffer.toString());

        try {
            String path = saveExcelFileAs();
            FileWriter writer = new FileWriter(path);
            writer.write(buffer.toString());
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private void populateExchanges() {
        cmbExchange.removeItemListener(this);
        cmbExchange.removeAllItems();
        cmbExchange.addItem(new TWComboItem("*", Language.getString("ALL")));
        Enumeration<String> exchanges = TradingShared.getTradingExchanges();
        if (exchanges != null) {
            String exchangeCode = null;
            while (exchanges.hasMoreElements()) {
                try {
                    exchangeCode = exchanges.nextElement();
                    String exchangeName = (ExchangeStore.getSharedInstance().getExchange(exchangeCode)).getDescription();
                    cmbExchange.addItem(new TWComboItem(exchangeCode, exchangeName));
                } catch (Exception e) {
                    System.out.println("Error: Tradable exchange " + exchangeCode + " not found in price exchanges list");
                }
            }
        }
        cmbExchange.updateUI();
//        try {
//            cmbExchange.removeAllItems();
//            Enumeration<String> exchanges = TradingShared.getTradingExchanges();
////            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
////            while (exchanges.hasMoreElements()) {
////                Exchange exchange = (Exchange) exchanges.nextElement();
////                exchangeList.add(new TWComboItem(exchange.getSymbol(), exchange.getDescription()));
////                exchange = null;
////            }
//
//            if(exchanges!= null){
//                while(exchanges.hasMoreElements()){
//                    String exchangeCode = exchanges.nextElement();
//                    String exchangeName = (ExchangeStore.getSharedInstance().getExchange(exchangeCode)).getDescription();
//                    cmbExchange.addItem(new TWComboItem(exchangeCode,exchangeName));
//                }
//            }
//            exchanges = null;
//            Collections.sort(exchangeList);
//            exchangeList.add(0, new TWComboItem("*", Language.getString("ALL")));
//            try {
//                cmbExchange.setSelectedIndex(0);
//            } catch (Exception e) {
//                e.printStackTrace();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        cmbExchange.addItemListener(this);
    }

    public Table getTable() {
        return blotterTable;
    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getStateChange() == ItemEvent.SELECTED) {
            String side = ((TWComboItem) cmbSide.getSelectedItem()).getId();
            String status = ((TWComboItem) cmbStatus.getSelectedItem()).getId();
            String exchanges = ((TWComboItem) cmbExchange.getSelectedItem()).getId();
            String portfolioID = ((TWComboItem) cmbPortf.getSelectedItem()).getId();

            OrderStore.getSharedInstance().setFilter(side, status, exchanges, portfolioID);
        }
    }

    private void createDecimalMenu(JPopupMenu parent) {
        TWMenu mnuDecimalPlaces = new TWMenu(Language.getString("POPUP_DECIMAL_PLACES"), "decimalplaces.gif");
        parent.add(mnuDecimalPlaces);

        //transHistoryTable.getPopup().add(mnuDecimalPlaces);
        ButtonGroup oGroup = new ButtonGroup();
        mnuDefaultDecimal = new TWRadioButtonMenuItem(Language.getString("DEFAULT_DECIMAL"));
        mnuDecimalPlaces.add(mnuDefaultDecimal);
        mnuDefaultDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.UNASSIGNED_DECIMAL_PLACES){
//            mnuDefaultDecimal.setSelected(true);
//        }
        oGroup.add(mnuDefaultDecimal);
        mnuNoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_NO_DECIMAL"));
        mnuDecimalPlaces.add(mnuNoDecimal);
        mnuNoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.NO_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.NO_DECIMAL_PLACES){
//            mnuNoDecimal.setSelected(true);
//        }
        oGroup.add(mnuNoDecimal);
        mnuOneDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_ONE_DECIMAL"));
        mnuDecimalPlaces.add(mnuOneDecimal);
        mnuOneDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.ONE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.ONE_DECIMAL_PLACES){
//            mnuOneDecimal.setSelected(true);
//        }
        oGroup.add(mnuOneDecimal);
        mnuTwoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_TWO_DECIMAL"));
        mnuDecimalPlaces.add(mnuTwoDecimal);
        mnuTwoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.TWO_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.TWO_DECIMAL_PLACES){
//            mnuTwoDecimal.setSelected(true);
//        }
        oGroup.add(mnuTwoDecimal);
        mnuThrDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_THREE_DECIMAL"));
        mnuDecimalPlaces.add(mnuThrDecimal);
        mnuThrDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.THREE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.THREE_DECIMAL_PLACES){
//            mnuThrDecimal.setSelected(true);
//        }
        oGroup.add(mnuThrDecimal);
        mnuFouDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_FOUR_DECIMAL"));
        mnuDecimalPlaces.add(mnuFouDecimal);
        mnuFouDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.FOUR_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.FOUR_DECIMAL_PLACES){
//            mnuFouDecimal.setSelected(true);
//        }
        oGroup.add(mnuFouDecimal);
        GUISettings.applyOrientation(mnuDecimalPlaces);
    }

    private void setDecimalPlaces(byte places) {
        decimalPlaces = places;
        ((SmartTable) blotterTable.getTable()).setDecimalPlaces(places);
        blotterTable.updateGUI();
        blotterTable.getTable().updateUI();
        oSetting.putProperty(ViewConstants.VC_DECIMAL_PLACES, places);
        if (decimalPlaces == Constants.NO_DECIMAL_PLACES) {
            mnuNoDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.ONE_DECIMAL_PLACES) {
            mnuOneDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.TWO_DECIMAL_PLACES) {
            mnuTwoDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.THREE_DECIMAL_PLACES) {
            mnuThrDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.FOUR_DECIMAL_PLACES) {
            mnuFouDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.UNASSIGNED_DECIMAL_PLACES) {
            mnuDefaultDecimal.setSelected(true);
        }
    }

    private void checkRefreshButton() {
        if (TradingShared.isConnected() && TradingShared.isManaulBroker() || TWControl.enableRefreshButton()) {
            btnRefresh.setVisible(true);
        } else {
            btnRefresh.setVisible(false);
        }
    }

    public void applicationLoading(int percentage) {
    }

    public void applicationLoaded() {
    }

    public void applicationReadyForTransactions() {
    }

    public void applicationExiting() {
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
    }

    public void workspaceWillLoad() {
    }

    public void workspaceLoaded() {
        try {
            decimalPlaces = Byte.parseByte(oSetting.getProperty(ViewConstants.VC_DECIMAL_PLACES));
        } catch (Exception e) {
            decimalPlaces = Constants.TWO_DECIMAL_PLACES;
        }
        setDecimalPlaces(decimalPlaces);
    }

    public void workspaceWillSave() {
    }

    public void workspaceSaved() {
    }

    public void snapshotProcessingStarted(Exchange exchange) {
    }

    public void snapshotProcessingEnded(Exchange exchange) {
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void tradeServerConnected() {
        populateExchanges();
        populatePortfolios();
        checkRefreshButton();
        if (frame != null) {
            //String sPortfolio = cmbPortf.getSelectedItem().toString();
            setTitle();
        }
        btnRefresh.updateUI();
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        OrderStore.getSharedInstance().clear();

        if (frame != null) {
            setTitle();
        }

        if (TradingShared.isManaulBroker()) {
            btnRefresh.setEnabled(false);
        }
    }

    public void accountDataChanged(String accountID) {
    }

    public void portfolioDataChanged(String portfolioID) {
        populatePortfolios();
    }


    public void setTitle() {
        if (BrokerConfig.getSharedInstance().isLoggedInUserShown()) {
            frame.setTitle(Language.getString(oSetting.getCaptionID()) + " - " + TradingShared.TRADE_USER_STATUS); // + Language.getString("GDR_MESSAGE"))
        } else {
            frame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"))
        }
    }

    public TWMenuItem getCopyMenu() {
        TWMenuItem mnuCopy = new TWMenuItem(Language.getString("COPY"), "copy.gif");
        mnuCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK));
        mnuCopy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    blotterModel.getDDEString(blotterTable, false);
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });
        return mnuCopy;
    }

    public TWMenuItem getCopyWithHeadMenu() {
        TWMenuItem mnuCopyH = new TWMenuItem(Language.getString("COPY_WITH_HEAD"), "copywh.gif");
        mnuCopyH.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_DOWN_MASK));
        mnuCopyH.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    blotterModel.getDDEString(blotterTable, true);
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });
        return mnuCopyH;
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void fireUpdateData() {
        new Thread("Update Data") {
            public void run() {
                while (Client.getInstance().isBlotterVisible()) {
                    try {
                        TradeMethods.requestPortfolioData(Constants.PATH_PRIMARY);
                        TradeMethods.requestAccountData(Constants.PATH_PRIMARY);
                        if (TradingShared.SECONDARY_LOGIN_SUCCESS) {
                            TradeMethods.requestPortfolioData(Constants.PATH_SECONDARY);
                            TradeMethods.requestAccountData(Constants.PATH_SECONDARY);
                        }
                        Thread.sleep(10000);
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
            }
        }.start();
    }


}
