package com.isi.csvr.trading.ui;

import com.isi.csvr.shared.*;
import com.isi.csvr.symbolfilter.FunctionListModel;
import com.isi.csvr.symbolfilter.FunctionBuilder;
import com.isi.csvr.symbolfilter.FunctionBuilderComboItem;
import com.isi.csvr.symbolfilter.FunctionStore;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.Client;
import com.isi.csvr.CWTabbedPaneUI;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;

/**
 * Created by IntelliJ IDEA. User: admin Date: 28-Sep-2007 Time: 13:30:19 To change this template use File | Settings |
 * File Templates.
 */
public class ConditionBuilderDialog extends JPanel implements ActionListener, Themeable {
    private TWComboBox symbolList;
    private TWComboBox fieldList;
    private TWComboBox conditionList;
    private JTextField value;
    private int editingRow = -1;
    private JDialog dialog;
    private JLabel symbol;
    private JLabel field;
    private JLabel condition;
    private JLabel valueLabel;
    public ConditionPanelComponent parent;
    //constants
    private int symbolType=4;
    private int indexType=2;
    private int fullMarketType=3;
    private int searchType=1;
    int type=0;
    //parameters
    private String currentSymbol="";
    private String currentExchange="";
    private String symbolField="";

    public DynamicArray symbolArray=new DynamicArray();
    public DynamicArray fieldArray=new DynamicArray();
    public DynamicArray conArray=new DynamicArray();

    public ConditionBuilderDialog(ConditionPanelComponent sParent,String cSymbol,String cExchange){
        parent=sParent;
        this.currentSymbol=cSymbol;
        this.currentExchange=cExchange;
        buildGUI();
        Theme.registerComponent(this);
    }
    public void buildGUI(){
        String[] heights = {"20", "20", "20", "20", "25"};
        String[] widths = {"100", "170"};
        FlexGridLayout flexGridLayout = new FlexGridLayout(widths, heights, 5, 5, true, true);
        this.setLayout(flexGridLayout);

        symbol = new JLabel(Language.getString("WHAT_IF_CAL_PORTFOLIO_SYMBOL"));
        symbolList=new TWComboBox();
        TWComboItem currentSymbol=new TWComboItem("1",Language.getString("CURRENT_SYMBOL"),"4");
        TWComboItem fullMarket=new TWComboItem("3",Language.getString("FULL_MARKET"),"3");
        TWComboItem search=new TWComboItem("4",Language.getString("SYMBOL_SEARCH"),"1");
        symbolList.addItem(currentSymbol);
        symbolList.addItem(fullMarket);
        symbolList.addItem(search);
        symbolArray.add(currentSymbol);
        symbolArray.add(fullMarket);
        symbolArray.add(search);
        symbolList.addActionListener(this);
        symbol.setHorizontalAlignment(SwingConstants.LEADING);
        this.add(symbol);
        this.add(symbolList);

        field = new JLabel(Language.getString("FIELD"));
        fieldList=new TWComboBox();
        populateFieldList(1);
        field.setHorizontalAlignment(SwingConstants.LEADING);
        this.add(field);
        this.add(fieldList);

        condition = new JLabel(Language.getString("CONDITION"));
        conditionList=new TWComboBox();
        populateConditionList();
        condition.setHorizontalAlignment(SwingConstants.LEADING);
        this.add(condition);
        this.add(conditionList);

        valueLabel = new JLabel(Language.getString("VALUE"));
        value=new JTextField();
        valueLabel.setHorizontalAlignment(SwingConstants.LEADING);
        this.add(valueLabel);
        value.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
        this.add(value);

        JPanel buttonPanel = new JPanel();
        buttonPanel.setLayout(new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS));
        TWButton btnOK = new TWButton(Language.getString("OK"));
        btnOK.setActionCommand("OK");
        btnOK.setPreferredSize(new Dimension(80, 20));
        btnOK.addActionListener(this);
        buttonPanel.add(btnOK);

        buttonPanel.add(Box.createHorizontalGlue());

        TWButton btnCancel = new TWButton(Language.getString("CLOSE"));
        btnCancel.setActionCommand("CANCEL");
        btnCancel.setPreferredSize(new Dimension(80, 20));
        btnCancel.addActionListener(this);
        buttonPanel.add(btnCancel);

        this.add(new JLabel()); // dummy
        this.add(buttonPanel);

        GUISettings.applyOrientation(buttonPanel);
        GUISettings.applyOrientation(this);
    }
      public void addNewFunction() {
        value.setText("0");
        editingRow = -1;
        symbolList.setSelectedItem(0);
        symbolList.updateUI();
        this.showDialog(false);
    }
      public void showDialog(final boolean isEdit){

        dialog = new JDialog(Client.getInstance().getFrame(),true){

            public void setDefaultCloseOperation(int operation) {
                super.setDefaultCloseOperation(operation);    //To change body of overridden methods use File | Settings | File Templates.
                if (!isEdit) {
                    symbolList.setSelectedIndex(0);
                    symbolList.updateUI();
                    conditionList.setSelectedIndex(0);
                    conditionList.updateUI();
                }
            }
        };
        dialog.setTitle(Language.getString("ADD_FUNCTION"));
        dialog.setResizable(false);
        dialog.setSize(290,170);
        dialog.getContentPane().add(this);
        dialog.setDefaultCloseOperation(JDialog.DISPOSE_ON_CLOSE);
        dialog.setLocationRelativeTo(FunctionBuilder.getSharedInstance());
        dialog.setVisible(true);
        dialog.repaint();
        GUISettings.applyOrientation(dialog);
        super.show();
    }

     public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
            addFunction(symbolField,((TWComboItem)fieldList.getSelectedItem()).getValue(), ((TWComboItem)conditionList.getSelectedItem()).getValue(), value.getText());
            if(dialog!=null){
                dialog.dispose();
            }
        }
        else if(e.getSource()==symbolList){
            TWComboItem twcItem=(TWComboItem)symbolList.getSelectedItem();
            populateFieldList(Integer.parseInt(twcItem.getType()));
            if(Integer.parseInt(twcItem.getType())==1){
               searchSymbol();
               type=searchType;
            }
            else if(Integer.parseInt(twcItem.getType())==3){
                symbolField=parent.getCurrentExchange();
                type=fullMarketType;
            }
            else if(Integer.parseInt(twcItem.getType())==4){
                symbolField=parent.getCurrenSymbol();
                type=symbolType;
            }else if(Integer.parseInt(twcItem.getType())==2){
                symbolField=twcItem.getValue();
                type=indexType;
            }

            twcItem=null;
        }
        else {
            if(dialog!=null){
                symbolList.setSelectedIndex(0);
                symbolList.updateUI();
                dialog.dispose();
            }
        }
    }
    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
     private void addFunction(String symbol, String field, String condition, String value) {
        int newIndex;
        if (editingRow < 0) {
            newIndex = parent.conditionArray.size();
        } else {
            newIndex = editingRow;
            parent.conditionArray.remove(newIndex);
        }
        ConditionObject conditionObject = new ConditionObject();
        conditionObject.setType(type);
        conditionObject.setSymbol(symbol);
        conditionObject.setField((field));
        conditionObject.setCondition((condition));
        conditionObject.setValue(Double.parseDouble(value));
        parent.conditionArray.add(newIndex,conditionObject);
        parent.updateUI();

    }
    private void populateConditionList(){
        TWComboItem eqItem=new TWComboItem("1",Language.getString("EQUAL"));
        TWComboItem neqItem=new TWComboItem("2",Language.getString("NOT_EQUAL"));
        TWComboItem lseqItem=new TWComboItem("3",Language.getString("LESS_THAN_OR_EQUAL"));
        TWComboItem moeqItem=new TWComboItem("4",Language.getString("GREATER_THAN_OR_EQUAL"));
        conditionList.addItem(eqItem);
        conditionList.addItem(neqItem);
        conditionList.addItem(lseqItem);
        conditionList.addItem(moeqItem);
        conArray.add(eqItem);
        conArray.add(neqItem);
        conArray.add(lseqItem);
        conArray.add(moeqItem);
    }
    private void populateFieldList(int fieldType){
        fieldList.removeAllItems();
        fieldArray.clear();
        if((fieldType==symbolType)||(fieldType==searchType)){
            TWComboItem lTrade=new TWComboItem("1",Language.getString("LAST_TRADE"));
            TWComboItem bid=new TWComboItem("2",Language.getString("CASH_IN"));
            TWComboItem offer=new TWComboItem("3",Language.getString("CASH_OUT"));
            TWComboItem minPrice=new TWComboItem("4",Language.getString("CONDITION_MIN"));
            TWComboItem maxPrice=new TWComboItem("5",Language.getString("CONDITION_MAX"));
            TWComboItem volume=new TWComboItem("6",Language.getString("CASH_DQ_VOLUME"));
            TWComboItem turnover=new TWComboItem("7",Language.getString("CASH_DQ_TURNOVER"));
            TWComboItem trades=new TWComboItem("8",Language.getString("HEAT_MAP_CRITERIA_NUM_TRADES"));
            fieldList.addItem(lTrade);
            fieldList.addItem(bid);
            fieldList.addItem(offer);
            fieldList.addItem(minPrice);
            fieldList.addItem(maxPrice);
            fieldList.addItem(volume);
            fieldList.addItem(turnover);
            fieldList.addItem(trades);
            fieldArray.add(lTrade);
            fieldArray.add(bid);
            fieldArray.add(offer);
            fieldArray.add(minPrice);
            fieldArray.add(maxPrice);
            fieldArray.add(volume);
            fieldArray.add(turnover);
            fieldArray.add(trades);

        }else if(fieldType==indexType){
            TWComboItem lTrade=new TWComboItem("1",Language.getString("LAST_TRADE"));
            TWComboItem volume=new TWComboItem("6",Language.getString("CASH_DQ_VOLUME"));
            TWComboItem turnover=new TWComboItem("7",Language.getString("CASH_DQ_TURNOVER"));
            TWComboItem trades=new TWComboItem("8",Language.getString("HEAT_MAP_CRITERIA_NUM_TRADES"));
            fieldList.addItem(lTrade);
            fieldList.addItem(volume);
            fieldList.addItem(turnover);
            fieldList.addItem(trades);
            fieldArray.add(lTrade);
            fieldArray.add(volume);
            fieldArray.add(turnover);
            fieldArray.add(trades);

        }else if(fieldType==fullMarketType){
            TWComboItem volume=new TWComboItem("6",Language.getString("CASH_DQ_VOLUME"));
            TWComboItem turnover=new TWComboItem("7",Language.getString("CASH_DQ_TURNOVER"));
            TWComboItem trades=new TWComboItem("8",Language.getString("HEAT_MAP_CRITERIA_NUM_TRADES"));
            fieldList.addItem(volume);
            fieldList.addItem(turnover);
            fieldList.addItem(trades);
            fieldArray.add(volume);
            fieldArray.add(turnover);
            fieldArray.add(trades);
        }


    }
    private void searchSymbol(){
            Symbols symbols = new Symbols();
            SymbolSearch oCompanies = SymbolSearch.getSharedInstance();// new SymbolSearch(g_oTempTable.getTitle(), false,false);//, Meta.QUOTE);
            oCompanies.setTitle(Language.getString("SYMBOL_SEARCH"));
            oCompanies.setSingleMode(true);
            oCompanies.setIndexSearchMode(false);
            oCompanies.init();
            oCompanies.setSymbols(symbols);
            oCompanies.setShowDefaultExchangesOnly(false);
            oCompanies.showDialog(true);
            oCompanies = null;

        try {
            if (symbols.getSymbols()[0] != null) {
                    String key = symbols.getSymbols()[0];
                    String symbol = SharedMethods.getSymbolFromKey(key);
                    TWComboItem symbolItem;//=new TWComboItem(key,key,"4");
                    if(isIndex(key)){
                       populateFieldList(indexType);
                       symbolItem=new TWComboItem(key,key,"2");
                    }else{
                       symbolItem=new TWComboItem(key,key,"4"); 
                    }
                    symbolList.addItem(symbolItem);
                    symbolArray.add(symbolItem);
                    symbolList.setSelectedItem(symbolItem);
                    symbolField=key;
                    symbolList.updateUI();

                }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        symbols = null;
    }
    public boolean isIndex(String key){
        try {
            Stock st=(Stock)DataStore.getSharedInstance().getStockObject(key);
            if(st.getInstrumentType()==7){
             return true;
            }else{
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return false;
        }

    }

    public void editFunction(int row) {
        if (row < parent.conditionArray.size()) {

            editingRow = row;
            ConditionObject conditionObject = (ConditionObject) parent.getCondition(row);
            if (conditionObject.getType() == symbolType) {
                 for (int i = 0; i < symbolArray.size(); i++) {
                    if (((TWComboItem) symbolArray.get(i)).getValue().equals(Language.getString("CURRENT_SYMBOL"))) {
                        symbolList.setSelectedIndex(i);
                        symbolList.updateUI();
                    }
                }
            } else if (conditionObject.getType() == fullMarketType) {
                 for (int i = 0; i < symbolArray.size(); i++) {
                    if (((TWComboItem) symbolArray.get(i)).getValue().equals(Language.getString("FULL_MARKET"))) {
                        symbolList.setSelectedIndex(i);
                        symbolList.updateUI();
                    }
                }
            } else if (conditionObject.getType() == searchType) {
                for (int i = 0; i < symbolArray.size(); i++) {
                    if (((TWComboItem) symbolArray.get(i)).getValue().equals(conditionObject.getSymbol())) {
                        symbolList.setSelectedIndex(i);
                        symbolList.updateUI();
                    }
                }

            }else if (conditionObject.getType() ==indexType) {
                for (int j = 0; j < symbolArray.size(); j++) {
                    if (((TWComboItem) symbolArray.get(j)).getValue().equals(conditionObject.getSymbol())) {
                        symbolList.setSelectedIndex(j);
                        symbolList.updateUI();
                    }
                }
            }
                conditionObject = (ConditionObject) parent.getCondition(row);
                for (int i = 0; i < fieldArray.size(); i++) {
                    if (((TWComboItem) fieldArray.get(i)).getValue().equals(conditionObject.getField()))
                        fieldList.setSelectedIndex(i);
                }
                conditionObject = (ConditionObject) parent.getCondition(row);
                for (int i = 0; i < conArray.size(); i++) {
                    if (((TWComboItem) conArray.get(i)).getValue().equals(conditionObject.getCondition()))
                        conditionList.setSelectedIndex(i);
                }
                value.setText("" + conditionObject.getValue());
                this.showDialog(true);
            }
        }
    }

