package com.isi.csvr.trading.ui;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.table.Table;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.*;
import com.isi.csvr.trading.datastore.QueuedTransaction;
import com.isi.csvr.trading.datastore.OrderQueue;
import com.isi.csvr.trading.datastore.QComponent;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.util.FlexGridLayout;
import com.isi.util.FlexFlowLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: dilum
 * Date: Jul 16, 2009
 * Time: 4:06:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class BasketOrderFrame implements ActionListener, TradingConnectionListener{

    private InternalFrame orderQFrame;
    private TWButton btnDelete;
    private TWButton selectAll;

    public BasketOrderFrame(){
        createUI();
    }

    public InternalFrame getInternalFrame(){
        return orderQFrame;
    }

    private void createUI(){
        final Table orderQTable = new Table(new int[]{2});

        orderQTable.setPreferredSize(new Dimension(500, 100));
        OrderQModel orderQrModel = new OrderQModel(this);
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("ORDER_Q");
        orderQrModel.setViewSettings(oSetting);
        orderQTable.setModel(orderQrModel);
        orderQTable.setWindowType(ViewSettingsManager.TRADE_QUEUED_TRADES);
        orderQrModel.setTable(orderQTable);
        orderQrModel.applyColumnSettings();
        orderQrModel.updateGUI();

        orderQFrame = new InternalFrame(orderQTable);
//        orderQFrame.getContentPane().setLayout(new BorderLayout());
        orderQFrame.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        oSetting.setParent(orderQFrame);
        orderQFrame.getContentPane().add(orderQTable);
//        orderQFrame.getContentPane().add(createQComponentsPanel(orderQTable) , BorderLayout.CENTER);
        Client.getInstance().getDesktop().add(orderQFrame);
        orderQFrame.setResizable(true);
        orderQFrame.setClosable(true);
        orderQFrame.setMaximizable(true);
        orderQFrame.setIconifiable(true);
        orderQFrame.setColumnResizeable(true);
        orderQFrame.setDetachable(true);
        orderQFrame.setPrintable(true);
        orderQFrame.setTitle(Language.getString(oSetting.getCaptionID())); // + Language.getString("GDR_MESSAGE"));
        orderQFrame.updateUI();
        orderQFrame.applySettings();
        orderQFrame.setLayer(GUISettings.TOP_LAYER);
        orderQFrame.setVisible(false);

        orderQTable.getPopup().add(new TWSeparator());

        TWMenuItem mnuDelete = new TWMenuItem(Language.getString("DELETE"), "deleteorder.gif");
        mnuDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //OrderQueue.getSharedInstance().removeTransaction(orderQTable.getTable().getSelectedRow());
                //orderQTable.getTable().repaint();

                int[] transactions = orderQTable.getTable().getSelectedRows();
                if ((orderQTable.getTable().getSelectedRowCount() > 0) &&
                        (SharedMethods.showConfirmMessage(Language.getString("MSG_DELETE_SELECTED_ORDERS"), JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION)) {
                    QueuedTransaction transaction;
                    for (int i = transactions.length - 1; i >= 0; i--) {
                        transaction = OrderQueue.getSharedInstance().getTransaction(i);
                        OrderQueue.getSharedInstance().removeTransaction(transaction);
                        transaction = null;
                    }
                }
                transactions = null;
                orderQTable.getTable().repaint();
            }
        });
        //orderQTable.getPopup().add(mnuDelete);

        TWMenuItem mnuSelectBuy = new TWMenuItem(Language.getString("SELECT_BUY_ORDERS"), "selectbuyorders.gif");
        mnuSelectBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                    markQueuedOrders(TradeMeta.BUY);
                }
            }
        });
        orderQTable.getPopup().add(mnuSelectBuy);

        TWMenuItem mnuSelectSell = new TWMenuItem(Language.getString("SELECT_SELL_ORDERS"), "selectsellorders.gif");
        mnuSelectSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                    markQueuedOrders(TradeMeta.SELL);
                }
            }
        });
        orderQTable.getPopup().add(mnuSelectSell);

        TWMenuItem mnuSelectAll = new TWMenuItem(Language.getString("SELECT_ALL"), "selectall.gif");
        mnuSelectAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                    markQueuedOrders(TradeMeta.ALL);
                }
            }
        });
        orderQTable.getPopup().add(mnuSelectAll);

        TWMenuItem mnuUnSelectAll = new TWMenuItem(Language.getString("UNSELECT_ALL"), "unselectall.gif");
        mnuUnSelectAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                    markQueuedOrders(TradeMeta.NONE);
                }
            }
        });
        orderQTable.getPopup().add(mnuUnSelectAll);

        GUISettings.applyOrientation(orderQTable.getPopup());

        orderQTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                /*if (e.getClickCount() > 1) {
                    if (TradingShared.isReadyForTrading()) {
                        QueuedTransaction transaction = OrderQueue.getSharedInstance().getTransaction(orderQTable.getTable().getSelectedRow());
                        doTransaction(TradeMeta.AMEND, transaction, true, transaction.getQID() , TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, transaction.getPortfolioNo());
                    }
                }*/
            }
        });
        ((SmartTable) orderQTable.getTable()).setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
        orderQTable.setSouthPanel(createOrderQButtonPanel());
        orderQTable.setNorthPanel(createQComponentsPanel(orderQTable));
        GUISettings.applyOrientation(orderQFrame);
    }

    public void fireTableDataChange(){
        try {
            int size = OrderQueue.getSharedInstance().size();
            QueuedTransaction transaction;
            boolean isAllSelected = true;
            for (int i = size - 1; i >= 0; i--) {
                try {
                    transaction = OrderQueue.getSharedInstance().getTransaction(i);
                    if (!transaction.isSelected()) {
                        isAllSelected = false;
                        break;
                    }
                    transaction = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            if (isAllSelected) {
                selectAll.setText(Language.getString("BASKET_UNSELECT_ALL"));
            } else {
                selectAll.setText(Language.getString("BASKET_SELECT_ALL"));
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private JPanel createQComponentsPanel(final Table orderQTable) {
        String[] widths = {"100%", "0"};
        FlexFlowLayout layout = new FlexFlowLayout(widths);
        JPanel panel = new JPanel(layout);
        final Dimension size = new Dimension(300, 0);

        //JPanel panel2 = new JPanel();
        //panel2.setLayout(new com.isi.util.ColumnLayout());
        final JPanel basketPanel = new JPanel();
        basketPanel.setPreferredSize(new Dimension(380, 30));
        String[] width = {"80", "160", "100", "100", "100"};
        String[] height = {"20"};
        basketPanel.setLayout(new FlexGridLayout(width, height, 5, 5));

        final ArrayList<TWComboItem> basketList = new ArrayList<TWComboItem>();
        TWComboModel basketModel = new TWComboModel(basketList);
        final JComboBox basketCombo = new JComboBox(basketModel);

        JLabel basketLabel = new JLabel(Language.getString("SELECTED_BASKET"));
        TWButton addNewBtn = new TWButton(Language.getString("ADD"));
        TWButton deleteBtn = new TWButton(Language.getString("DELETE"));
        TWButton renameBtn = new TWButton(Language.getString("RENAME"));

        basketPanel.add(basketLabel);
        basketPanel.add(basketCombo);
        basketPanel.add(addNewBtn);
        basketPanel.add(renameBtn);
        basketPanel.add(deleteBtn);
        GUISettings.applyOrientation(basketPanel);

        basketCombo.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int index = basketCombo.getSelectedIndex();
                String captionID = basketList.get(index).getId();
                OrderQueue.getSharedInstance().setSelectedQID(new Long(captionID));
                orderQTable.getTable().updateUI();
            }
        });

        int QCount = OrderQueue.getSharedInstance().getQCount();
        if (QCount == 0) {
            QComponent component = OrderQueue.getSharedInstance().createComponent(Language.getString("DEFAULT_BASKET"));
            basketList.add(new TWComboItem("" + component.getId(), component.getValue()));
        } else {
            for (int i = 0; i < QCount; i++) {
                QComponent component = OrderQueue.getSharedInstance().getQConmonent(i);
                basketList.add(component);
            }
        }
        OrderQueue.getSharedInstance().setSelectedQID(new Long(OrderQueue.getSharedInstance().getQConmonent(0).getId()));
        if (basketList != null) {
            basketCombo.setSelectedIndex(0);
        }

        addNewBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                CreateView oCreateView = new CreateView(Client.getInstance().getFrame(), Language.getString("CREATE_VIEW"), true, false);
                String caption = oCreateView.getNewName();

                //Bug ID <#0001>
                if (caption != null) {
                    if (!isValidBasket(basketList, caption)) {
                        new ShowMessage(false, Language.getString("MSG_DUPLICATE_VIEW_NAME"), "E");
                    } else {
                        QComponent component = OrderQueue.getSharedInstance().createComponent(caption);
                        basketList.add(component);
                        basketCombo.setSelectedItem(component);
                        basketCombo.updateUI();
                    }
                }
                oCreateView = null;
            }
        });


        deleteBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int index = basketCombo.getSelectedIndex();
                String captionID = basketList.get(index).getId();
                if (basketList.size() == 1) {
                    SharedMethods.showMessage(Language.getString("MSG_CANNOT_DELETE_BASKET"), JOptionPane.ERROR_MESSAGE);
                } else {
                    int i = SharedMethods.showConfirmMessage(Language.getString("MSG_DELETE_BASKET"), JOptionPane.WARNING_MESSAGE);
                    if (i == JOptionPane.OK_OPTION) {
                        OrderQueue.getSharedInstance().removeQueue(new Long(captionID));
                        basketList.remove(index);
                        orderQTable.getTable().repaint();
                        basketCombo.setSelectedIndex(0);
                        basketCombo.updateUI();
                    }
                }
            }
        });

        renameBtn.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int index = basketCombo.getSelectedIndex();
                String captionID = basketList.get(index).getId();
                String captionValue = basketList.get(index).getValue();
                CreateView oCreateView = new CreateView(Client.getInstance().getFrame(), Language.getString("RENAME") + " " + basketList.get(index).getValue(), true, false);
                QComponent component = OrderQueue.getSharedInstance().getQComponent(new Long(captionID));
                oCreateView.setCaptions(component.getFullCaption());
                String caption = oCreateView.getNewName();
                if (caption != null) { //Bug id <#0005>
                    if (!isValidBasket(basketList, caption)) {
                        new ShowMessage(false, Language.getString("MSG_DUPLICATE_VIEW_NAME"), "E");
                    } else {
                        component.setCaption(caption);
                        basketCombo.setSelectedIndex(index);
                        basketCombo.updateUI();
                    }
                }
            }
        });

        return basketPanel;
    }

    private JPanel createOrderQButtonPanel() {

        JPanel panel = new JPanel(new FlexGridLayout(new String[]{"122", "102", "100%", "100"}, new String[]{"24"}, 2, 3));
        selectAll = new TWButton(Language.getString("BASKET_SELECT_ALL"));
        int size = OrderQueue.getSharedInstance().size();
        QueuedTransaction transaction;
        boolean isAllSelected = true;
        for (int i = size - 1; i >= 0; i--) {
            try {
                transaction = OrderQueue.getSharedInstance().getTransaction(i);
                if (!transaction.isSelected()) {
                    isAllSelected = false;
                    break;
                }
                transaction = null;
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        if (isAllSelected) {
            selectAll.setText(Language.getString("BASKET_UNSELECT_ALL"));
        } else {
            selectAll.setText(Language.getString("BASKET_SELECT_ALL"));
        }
        selectAll.setSize(new Dimension(120, 18));
        selectAll.addActionListener(new ActionListener() {

            public void actionPerformed(ActionEvent e) {
                if (selectAll.getText().equals(Language.getString("BASKET_SELECT_ALL"))) {
                    markQueuedOrders(TradeMeta.ALL);
                    selectAll.setText(Language.getString("BASKET_UNSELECT_ALL"));
                } else {
                    markQueuedOrders(TradeMeta.NONE);
                    selectAll.setText(Language.getString("BASKET_SELECT_ALL"));
                }
            }
        });
        panel.add(selectAll);

        btnDelete = new TWButton(Language.getString("DELETE"));
        btnDelete.setSize(new Dimension(100, 18));
        panel.add(btnDelete);
        btnDelete.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (TradingShared.isReadyForTrading()) {
                    deleteQueuedOrders();
                } else {
                    SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
                }
            }
        });

        panel.add(new JLabel(""));

        TWButton btnExec = new TWButton(Language.getString("EXECUTE"));
        btnExec.setSize(new Dimension(100, 18));
        panel.add(btnExec);
        btnExec.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                executeQueuedOrders();
            }
        });

        return panel;
    }

    private boolean isValidBasket(ArrayList<TWComboItem> basketList, String newItem) {
        try {
            for (TWComboItem item : basketList) {
                if (Language.getLanguageSpecificString(item.getValue(), ",").equals(Language.getLanguageSpecificString(newItem, ","))) {
                    return false;
                }
            }

            return true;
        } catch (Exception e) {
            return true;
        }
    }

    private void executeQueuedOrders() {
        int size = OrderQueue.getSharedInstance().size();
        QueuedTransaction transaction;
        boolean itemsSelected = false;
        boolean first = true;

        if (TradingShared.isReadyForTrading()) {

            for (int i = size - 1; i >= 0; i--) {
                try {
                    transaction = OrderQueue.getSharedInstance().getTransaction(i);
                    if (transaction.isSelected()) {
                        itemsSelected = true;
                    }
                    transaction = null;
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }

            if ((itemsSelected) &&
                    (SharedMethods.showConfirmMessage(Language.getString("MSG_EXECUTE_BASKET_ORDERS"), JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION)) {

                    ArrayList list50 = new ArrayList();
                    ArrayList list75 = new ArrayList();
                    ArrayList list100 = new ArrayList();

                    StringBuilder sb1 = new StringBuilder();
                    StringBuilder sb2 = new StringBuilder();
                    StringBuilder sb3 = new StringBuilder();

                
                for (int i = size - 1; i >= 0; i--) {
                    transaction = OrderQueue.getSharedInstance().getTransaction(i);
                    if (transaction.isSelected()) {
                        if (TradingShared.isExchangeAllowedToTrade(transaction.getExchange(), false)) {
/////////////Basketorder Notification at Execution Time
                             if(transaction.getSide() == TradeMeta.BUY){
                    String warningMessage=null;
                    int dialogResponse;


                    Stock selectedStock = DataStore.getSharedInstance().getStockByExchangeKey(transaction.getExchange(), transaction.getSymbol());

                    if(selectedStock.getLoosingCategory() == Meta.ACCUMULATED_LOSSES_50){
                        list50.add(transaction);
                        continue;

                    } else if(selectedStock.getLoosingCategory() == Meta.ACCUMULATED_LOSSES_75){
                        list75.add(transaction);
                        continue;

                    } else if(selectedStock.getLoosingCategory() == Meta.ACCUMULATED_LOSSES_100){
                        list100.add(transaction);
                        continue;

                    }
                    }

                            if (first) { // authenticate only the first order of the list
                                boolean sucess = TradeMethods.getSharedInstance().sendNewOrder(transaction, true);
                                if (!sucess) break; //Bug ID <#0014>
                                first = false;
                            } else {
                                TradeMethods.getSharedInstance().sendNewOrder(transaction, false);
                            }
                            OrderQueue.getSharedInstance().removeTransaction(transaction);
                        }
                    }
                    transaction = null;
//                        System.out.println("Time1>>>>>>>>>>>"+System.currentTimeMillis());
                    if(TWControl.isMultipleOrderTimeOutEnabled()){
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                }
                if(list50.size() != 0){
                    int dialogResponse;
                    String s = "";
                    String x = sb1.append(Language.getString("LOOSING_STOCK_MSG_50_2")).append("<BR>").toString();
                    for(int i = list50.size() - 1; i >= 0; i--){
                        if(!s.contains(((QueuedTransaction)list50.get(i)).getSymbol())){
                               s = s +((QueuedTransaction)list50.get(i)).getSymbol() +" ,";
                        }
                    }
                    StringBuilder stb = new StringBuilder();
                    stb.append(x).append("<center>").append(s).append("</center></div>");
//                    String k = x+"<center>"+ s + "</center></div>";
                    Object[] options = {Language.getString("OK")};
                            dialogResponse = JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                            stb.toString(),
                            Language.getString("MSG_TITLE"),
                            JOptionPane.OK_OPTION,
                            JOptionPane.WARNING_MESSAGE,
                            null,
                            options,
                            options[0]);
                        if(dialogResponse == 0){

                        for (int i = list50.size() - 1; i >= 0; i--) {
                        TradeMethods.getSharedInstance().sendNewOrder((QueuedTransaction)list50.get(i), true);
//                        list50.remove(i);
                        OrderQueue.getSharedInstance().removeTransaction((QueuedTransaction)list50.get(i));
                        list50.remove(i);
                        if(TWControl.isMultipleOrderTimeOutEnabled()){
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    }
                    }
                }
                if(list75.size() != 0){
                    int dialogResponse;
                    String s = "";
                    String x = sb2.append(Language.getString("LOOSING_STOCK_MSG_75_2")).append("<BR>").toString();
                    for(int i = list75.size() - 1; i >= 0; i--){
                        if(!s.contains(((QueuedTransaction)list75.get(i)).getSymbol())){
                               s = s + ((QueuedTransaction)list75.get(i)).getSymbol()+" ,";
                        }
                    }
//                    String k = x + "<center>"+ s + "</center></div>";
                    StringBuilder stb = new StringBuilder();
                    stb.append(x).append("<center>").append(s).append("</center></div>");
                    Object[] options = {Language.getString("OK")};
                            dialogResponse = JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                            stb.toString(),
                            Language.getString("MSG_TITLE"),
                            JOptionPane.OK_OPTION,
                            JOptionPane.WARNING_MESSAGE,
                            null,
                            options,
                            options[0]);
                        if(dialogResponse == 0){

                        for (int i = list75.size() - 1; i >= 0; i--) {
                        TradeMethods.getSharedInstance().sendNewOrder((QueuedTransaction)list75.get(i), true);
//                        list75.remove(i);
                        OrderQueue.getSharedInstance().removeTransaction((QueuedTransaction)list75.get(i));
                            list75.remove(i);
                        if(TWControl.isMultipleOrderTimeOutEnabled()){
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    }
                    }
                }if(list100.size() != 0){
                    int dialogResponse;
                    String s = "";
                    String x = sb3.append(Language.getString("LOOSING_STOCK_MSG_100_2")).append("<BR>").toString();
                    for(int i = list100.size() - 1; i >= 0; i--){
                        if(!s.contains(((QueuedTransaction)list100.get(i)).getSymbol())){
                               s = s + ((QueuedTransaction)list100.get(i)).getSymbol()+" ,";
                        }
                    }
//                    String k = x + "<center>"+ s + "</center></div>";
                    StringBuilder stb = new StringBuilder();
                    stb.append(x).append("<center>").append(s).append("</center></div>");
                    Object[] options = {Language.getString("OK")};
                        dialogResponse = JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                            stb.toString(),
                            Language.getString("MSG_TITLE"),
                            JOptionPane.OK_OPTION,
                            JOptionPane.WARNING_MESSAGE,
                            null,
                            options,
                            options[0]);

                         if(dialogResponse == 0){

                        for (int i = list100.size() - 1; i >= 0; i--) {
                        TradeMethods.getSharedInstance().sendNewOrder((QueuedTransaction)list100.get(i), true);
//                        list75.remove(i);
                        OrderQueue.getSharedInstance().removeTransaction((QueuedTransaction)list100.get(i));
                            list100.remove(i);
                        if(TWControl.isMultipleOrderTimeOutEnabled()){
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException e) {
                            e.printStackTrace();
                        }
                    }
                    }
                    }

                }
            }
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }
    }

    private void markQueuedOrders(int type) {
        int size = OrderQueue.getSharedInstance().size();
        QueuedTransaction transaction;

//        if (TradingShared.isReadyForTrading()) {
        for (int i = size - 1; i >= 0; i--) {
            try {
                transaction = OrderQueue.getSharedInstance().getTransaction(i);
                transaction.setSelected(false);
                if ((type == TradeMeta.ALL) || (type == transaction.getSide())) {
                    transaction.setSelected(true);
                }
                transaction = null;
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
//        }
        }
    }

    private void deleteQueuedOrders() {
        int size = OrderQueue.getSharedInstance().size();
        boolean itemsSelected = false;
        QueuedTransaction transaction;

        for (int i = size - 1; i >= 0; i--) {
            try {
                transaction = OrderQueue.getSharedInstance().getTransaction(i);
                if (transaction.isSelected()) {
                    itemsSelected = true;
                }
                transaction = null;
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        if ((itemsSelected) &&
                (SharedMethods.showConfirmMessage(Language.getString("MSG_DELETE_SELECTED_ORDERS"), JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION)) {
            for (int i = size - 1; i >= 0; i--) {
                transaction = OrderQueue.getSharedInstance().getTransaction(i);
                if (transaction.isSelected()) {
                    OrderQueue.getSharedInstance().removeTransaction(transaction);
                }
                transaction = null;
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
