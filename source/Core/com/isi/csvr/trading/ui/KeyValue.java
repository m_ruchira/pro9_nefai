package com.isi.csvr.trading.ui;

public class KeyValue<T> implements Comparable {
    private T key;
    private String value;

    public KeyValue() {
    }

    public KeyValue(T key, String value) {
        this.key = key;
        this.value = value;
    }

    public T getKey() {
        return key;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }

    @Override
    public String toString() {
        return value;
    }

    @Override
    public int hashCode() {
        return value.hashCode();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == this) {
            return true;
        }
        if (obj == null || obj.getClass() != this.getClass()) {
            return false;
        }
        KeyValue value = (KeyValue) obj;
        return (getKey().equals(value.getKey())) && (getValue().equals(value.getValue()));

    }

    public int compareTo(Object o) {
        return ((KeyValue) this).getValue().compareTo(((KeyValue) o).getValue());  //To change body of implemented methods use File | Settings | File Templates.
    }
}
