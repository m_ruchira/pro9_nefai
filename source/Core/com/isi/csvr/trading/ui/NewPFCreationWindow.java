package com.isi.csvr.trading.ui;

import com.dfn.mtr.mix.beans.*;
import com.dfn.mtr.mix.beans.TransferObject;
import com.dfn.mtr.mix.sabb.beans.CreatePortfolioRequest;
import com.dfn.mtr.mix.sabb.beans.CreatePortfolioResponse;
import com.dfn.mtr.mix.sabb.beans.MIXExtendedObject;
import com.dfn.mtr.mix.sabb.constants.MIXCustomConstants;
import com.isi.csvr.Client;
import com.isi.csvr.announcement.DateCombo;
import com.isi.csvr.event.Application;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.SendQueue;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.shared.*;
import com.isi.csvr.util.MD5;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyListener;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

import static javax.swing.JOptionPane.OK_CANCEL_OPTION;
import static javax.swing.JOptionPane.PLAIN_MESSAGE;

/**
 * Created by IntelliJ IDEA.
 * User: shanakak
 * Date: Apr 16, 2013
 * Time: 5:21:14 PM
 * To change this template use File | Settings | File Templates.
 */
public class NewPFCreationWindow extends InternalFrame
        implements ActionListener, TraderProfileDataListener, Themeable {

    public static NewPFCreationWindow self = null;
    private JPanel mainPanel;
    private JLabel lblUserID;
    private TWTextField txtUserID;
    private JLabel lblPassword;
    private JPasswordField txtPwd;
    private JLabel lblAccount;
    private TWComboBox cmbAccounts;
    private JLabel lblEmpty;
    private TWButton btnCreate;
    private String selectedAccount = null;

    public NewPFCreationWindow() {
        createUI();
        populateAccounts();
    }

    public static synchronized NewPFCreationWindow getSharedInstance() {
        if (self == null) {
            self = new NewPFCreationWindow();
        }
        return self;
    }

    private void createUI() {
        this.setTitle(Language.getString("ADDITIONAL_PORTFOLIO_TITLE"));
        this.setSize(300, 150);
        this.setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setClosable(true);
        this.setIconifiable(true);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        this.setLayer(GUISettings.TOP_LAYER);
        this.hideTitleBarMenu();
        Client.getInstance().getDesktop().add(this);
        GUISettings.applyOrientation(this);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);

        mainPanel = new JPanel(new FlexGridLayout(new String[]{"50%", "50%"}, new String[]{"20", "20", "20", "20"}, 5, 10));
        lblUserID = new JLabel(Language.getString("USER_NAME"));
        txtUserID = new TWTextField();
        lblPassword = new JLabel(Language.getString("LEVEL_2_PASSWORD"));
        txtPwd = new JPasswordField();
        lblAccount = new JLabel(Language.getString("ADDITIONAL_PORTFOLIO_ACCOUNT_NUMBER"));
        cmbAccounts = new TWComboBox();
        lblEmpty = new JLabel("");
        btnCreate = new TWButton(Language.getString("CREATE"));

        txtUserID.setText(TradingShared.getTrader().getUserID(Constants.PATH_PRIMARY));
        txtUserID.setEditable(false);


        btnCreate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Object[] option = {Language.getString("OK")};
                Object[] options = new Object[]{Language.getString("YES"), Language.getString("NO")};
                if (new String(txtPwd.getPassword()).trim().equals("")) {
                    JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                            Language.getString("ADDITIONAL_PORTFOLIO_PASSWORD_EMPTY"),
                            Language.getString("ERROR"),
                            JOptionPane.OK_OPTION,
                            JOptionPane.ERROR_MESSAGE,
                            null,
                            option,
                            option[0]);
                } else {
                    int selectedOption = JOptionPane.showOptionDialog(Client.getInstance().getFrame(), Language.getString("ADDITIONAL_PORTFOLIO_CONFIRMATION_MESSAGE"), Language.getString("ADDITIONAL_PORTFOLIO_CONFIRMATION"), OK_CANCEL_OPTION, PLAIN_MESSAGE, null, options, null);
                    if (selectedOption == JOptionPane.OK_OPTION) {
                        sendRequest(0);
                        txtPwd.setText("");
                    } else if (selectedOption == JOptionPane.CANCEL_OPTION) {
                        txtPwd.setText("");
                    }
                }
            }
        });

        mainPanel.add(lblUserID);
        mainPanel.add(txtUserID);
        mainPanel.add(lblPassword);
        mainPanel.add(txtPwd);
        mainPanel.add(lblAccount);
        mainPanel.add(cmbAccounts);
        mainPanel.add(lblEmpty);
        mainPanel.add(btnCreate);
        GUISettings.applyOrientation(mainPanel);
        this.add(mainPanel);
    }

    private synchronized void populateAccounts() {
        try {
            if (TradingShared.getTrader() != null) {
                try {
                    cmbAccounts.removeAllItems();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                Account account;
                ArrayList<Account> accounts = TradingShared.getTrader().getAccounts();
                for (int i = 0; i < accounts.size(); i++) {
                    account = accounts.get(i);
                    TWComboItem item;
                    item = new TWComboItem(account.getAccountID(), account.getAccountNumber());
                    cmbAccounts.addItem(item);
                    item = null;
                    account = null;
                }
                cmbAccounts.updateUI();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void sendRequest(long startID) {
        String pfID = null;
        try {
            selectedAccount = ((TWComboItem) cmbAccounts.getSelectedItem()).getId();

            ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
            for (TradingPortfolioRecord record : portfolios) {
                if (record.getAccountNumber().equalsIgnoreCase(selectedAccount))
                    pfID = record.getPortfolioID();
            }

            MIXObject newPFCreation = new MIXObject();
            MIXHeader header = new MIXHeader();
            header.setGroup(MIXCustomConstants.GROUP_CREATE_ACCOUNTS);
            header.setRequestType(MIXCustomConstants.REQUEST_TYPE_CREATE_ADDITIONAL_PORTFOLIO);
            header.setSessionID(TradingShared.getTrader().getSessionID(pfID));
            header.setChannelID(2);

            CreatePortfolioRequest inquiry = new CreatePortfolioRequest();

            inquiry.setUserId(txtUserID.getText());
            String encryptPwd = MD5.getHashString(new String(txtPwd.getPassword()).trim());
            System.out.println("MD5 Password: " + encryptPwd);
            inquiry.setPassword(encryptPwd);
            inquiry.setAccountNo(selectedAccount);

            Pagination page = new Pagination();
            page.setStartingSequenceNumber(startID + "");
            page.setPageWidth(25);
            newPFCreation.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inquiry});
            newPFCreation.setMIXHeader(header);
            System.out.println("NEW PF CREATION MIX HEADER: " + newPFCreation.getMIXHeader());
            System.out.println("NEW PF CREATION MIX STRING: " + newPFCreation.getMIXString());
            SendQueue.getSharedInstance().addData(newPFCreation.getMIXString(), TradingShared.getTrader().getPath(pfID));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void analyseResponse(MIXObject record) {
        try {
            MIXExtendedObject extendedMix = new MIXExtendedObject();
            extendedMix.setMIXString(record.getMIXString());

            TransferObject[] response = extendedMix.getTransferObjects();
            if (response == null || response.length == 0) {
                System.out.println("Additional Portfolio Creation - Response is null or empty");
                return;
            }

            CreatePortfolioResponse newPFCreationResponse = (CreatePortfolioResponse) response[0];
            if (newPFCreationResponse.getAuthStatus() == 1) {
                String str = Language.getString("ADDITIONAL_PORTFOLIO_ACCEPTED");
                SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
                str = null;
            } else if (newPFCreationResponse.getAuthStatus() == 0 && newPFCreationResponse.getFailureDescription().equalsIgnoreCase("1001")) {
                String failMessage = Language.getString("ADDITIONAL_PORTFOLIO_EXCEED_PF");
                SharedMethods.showMessage(failMessage, JOptionPane.ERROR_MESSAGE);
                failMessage = null;
            } else if (newPFCreationResponse.getAuthStatus() == 0 && newPFCreationResponse.getFailureDescription().equalsIgnoreCase("1002")) {
                String failMessage = Language.getString("ADDITIONAL_PORTFOLIO_REJECT_FROM_TDWL");
                SharedMethods.showMessage(failMessage, JOptionPane.ERROR_MESSAGE);
                failMessage = null;
            } else if (newPFCreationResponse.getAuthStatus() == 0 && newPFCreationResponse.getFailureDescription().equalsIgnoreCase("1003")) {
                String failMessage = Language.getString("ADDITIONAL_PORTFOLIO_INVALID_USER_DETAILS");
                SharedMethods.showMessage(failMessage, JOptionPane.ERROR_MESSAGE);
                failMessage = null;
            } else if (newPFCreationResponse.getAuthStatus() == 0 && newPFCreationResponse.getFailureDescription().equalsIgnoreCase("1004")) {
                String failMessage = Language.getString("ADDITIONAL_PORTFOLIO_NO_CONNECTIVITY_TDWL");
                SharedMethods.showMessage(failMessage, JOptionPane.ERROR_MESSAGE);
                failMessage = null;
            }
        } catch (Exception e) {
            System.out.println("Additional Portfolio Creation - Exception");
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void accountDataChanged(String accountID) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void portfolioDataChanged(String portfolioID) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
