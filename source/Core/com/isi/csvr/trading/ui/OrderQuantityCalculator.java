package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;
import sun.awt.CausedFocusEvent;

import javax.swing.*;
import java.awt.event.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.text.DecimalFormat;
import java.text.NumberFormat;
import java.text.ParseException;

/**
 * Created by hasakam on 10/18/2017.
 */
public class OrderQuantityCalculator extends JDialog implements ActionListener, Themeable, FocusListener, PropertyChangeListener {
    private JSpinner txtNetVal;
    private JSpinner txtPrice;
    private TWButton calculateBtn, closeBtn;
    private double buyingPower, minPrice;
    private TransactionDialog model;
    private int side;
    private JPanel mainPanel,upperPanel,lowerPanel;
    private JFormattedTextField priceSpinnerTextField, netValSpinnerTextField;
    private CustomSpinnerDoubleModel modelSpin;
    private CustomSpinnerPriceDoubleModel priceSpin;

    protected OrderQuantityCalculator(TransactionDialog tDialog, double buyingPwr, double mp, int side) {
        super(Client.getInstance().getFrame(), true);
        buyingPower = buyingPwr;
        minPrice = mp;
        model = tDialog;
        this.side = side;
        setTitle(Language.getString("CAL_ODR_QTY"));
        setSize(350, 125);
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 4, 4));
        modelSpin = new CustomSpinnerDoubleModel(1d, this);
        priceSpin = new CustomSpinnerPriceDoubleModel(1d, this);
        this.add(createMainPanel());
        applyColours();
        GUISettings.setLocationRelativeTo(this, Client.getInstance().getFrame());
        GUISettings.applyOrientation(this);
        setResizable(false);
        this.setVisible(true);
    }

    private JPanel createMainPanel() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"50", "30"}, 4, 4));
        mainPanel.add(createUpperPanel());
        mainPanel.add(createLowerpanel());
        return mainPanel;
    }

    private JPanel createUpperPanel() {
        upperPanel = new JPanel();
        txtNetVal = new JSpinner(modelSpin);
        txtNetVal.setUI(new CustomSpinnerUI(this));
        txtPrice = new JSpinner(priceSpin);
        txtPrice.setUI(new CustomPriceSpinnerUI(this));
        netValSpinnerTextField = ((JSpinner.DefaultEditor)txtNetVal.getEditor()).getTextField();
        netValSpinnerTextField.setEditable(true);
        priceSpinnerTextField = ((JSpinner.DefaultEditor)txtNetVal.getEditor()).getTextField();
        priceSpinnerTextField.setEditable(true);
        initPriceSpinnerTextField(2);
        initNetValSpinnerTextField(2);

        if(side == 1) {
            DecimalFormat format = new DecimalFormat("#,###.##");
            netValSpinnerTextField.setText(format.format(buyingPower));
            priceSpinnerTextField.setText(format.format(minPrice));
        }

        netValSpinnerTextField.addKeyListener(new KeyListener() {
                                                  public void keyTyped(KeyEvent e) {
                                                      switch (e.getKeyChar()) {
                                                          case '1':
                                                          case '2':
                                                          case '3':
                                                          case '4':
                                                          case '5':
                                                          case '6':
                                                          case '7':
                                                          case '8':
                                                          case '9':
                                                          case '0':
                                                          case '.':
                                                              if (netValSpinnerTextField.getText().indexOf(".") != -1) {
                                                                  if (e.getKeyChar() == '.') {
                                                                      e.consume();
                                                                  }
                                                              }
                                                              break;
                                                          default:
                                                              e.consume();//delete the typed char
                                                              break;
                                                      }
                                                  }

                                                  public void keyPressed(KeyEvent e) {

                                                  }

                                                  public void keyReleased(KeyEvent e) {

                                                  }
                                              }
        );
        priceSpinnerTextField.addKeyListener(new KeyListener() {
                                                 public void keyTyped(KeyEvent e) {
                                                     switch (e.getKeyChar()) {
                                                         case '1':
                                                         case '2':
                                                         case '3':
                                                         case '4':
                                                         case '5':
                                                         case '6':
                                                         case '7':
                                                         case '8':
                                                         case '9':
                                                         case '0':
                                                         case '.':
                                                             if (priceSpinnerTextField.getText().indexOf(".") != -1) {
                                                                 if (e.getKeyChar() == '.') {
                                                                     e.consume();
                                                                 }
                                                             }
                                                             break;
                                                         default:
                                                             e.consume();//delete the typed char
                                                             break;
                                                     }
                                                 }

                                                 public void keyPressed(KeyEvent e) {

                                                 }

                                                 public void keyReleased(KeyEvent e) {
                                                     if ((e.getKeyCode() == KeyEvent.VK_ENTER) && (!(txtNetVal.equals(null)) || !(txtNetVal.equals("0"))) && ((!txtPrice.equals(null)) || (!txtPrice.equals("0")))) {
                                                         Thread thread = new Thread(new Runnable() {
                                                             @Override
                                                             public void run() {
                                                                 calculate();
                                                                 dispose();
                                                             }

                                                         }, "Order Quantity calculator");
                                                         thread.start();
                                                     }
                                                 }
                                             }
        );
        upperPanel.setLayout(new FlexGridLayout(new String[]{"100", "200"}, new String[]{"20", "20"}, 5, 5));
        upperPanel.add(new JLabel(Language.getString("NET_VAL")));
        upperPanel.add(txtNetVal);
        upperPanel.add(new JLabel(Language.getString("PRICE")));
        upperPanel.add(txtPrice);
        return upperPanel;
    }

    private JPanel createLowerpanel() {
        lowerPanel = new JPanel();
        calculateBtn = new TWButton(Language.getString("CALCULATE"));
        closeBtn = new TWButton(Language.getString("CLOSE"));
        calculateBtn.addActionListener(this);
        closeBtn.addActionListener(this);
        calculateBtn.addFocusListener(this);
        lowerPanel.setLayout(new FlexGridLayout(new String[]{"120", "95", "80"}, new String[]{"100%"}, 5, 5));
        lowerPanel.add(new JLabel());
        lowerPanel.add(calculateBtn);
        lowerPanel.add(closeBtn);
        return lowerPanel;
    }

    private void calculate() {
        String netVal = netValSpinnerTextField.getText();
        String price = priceSpinnerTextField.getText();
        double prce = Double.parseDouble(price.replaceAll(",", ""));
        if (prce == 0) {
            SharedMethods.showMessage(Language.getString("PRICE_GREATER_ZERO"), JOptionPane.ERROR_MESSAGE);
            return;
        }
        if(side == 1) {
            double netValue = Double.parseDouble(netVal.replaceAll(",", ""));
            double qty = netValue / prce;
            int quantity = (int) qty;
            if (netValue <= buyingPower) {
                model.setQuantityForBuy(quantity, prce, netValue);
            } else {
                SharedMethods.showMessage(Language.getString("NET_VAL_EXCEED_BP"),
                                            JOptionPane.ERROR_MESSAGE);
            }
        } else {
            double netValue = Double.parseDouble(netVal.replaceAll(",", ""));
            double qty = netValue / prce;
            int quantity = (int) qty;
            if (netValue <= buyingPower) {
                model.setQuantityForSell(quantity, prce, netValue);
            } else {
                SharedMethods.showMessage(Language.getString("NET_VAL_EXCEED_BP"),
                        JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    private void applyColours() {
        if (side == 1) {
            this.setBackground(Theme.getColor("ORDER_ROW_BUY_BGCOLOR"));
            mainPanel.setBackground(Theme.getColor("ORDER_ROW_BUY_BGCOLOR"));
            upperPanel.setBackground(Theme.getColor("ORDER_ROW_BUY_BGCOLOR"));
            lowerPanel.setBackground(Theme.getColor("ORDER_ROW_BUY_BGCOLOR"));
        } else {
            this.setBackground(Theme.getColor("ORDER_ROW_SELL_BGCOLOR"));
            mainPanel.setBackground(Theme.getColor("ORDER_ROW_SELL_BGCOLOR"));
            upperPanel.setBackground(Theme.getColor("ORDER_ROW_SELL_BGCOLOR"));
            lowerPanel.setBackground(Theme.getColor("ORDER_ROW_SELL_BGCOLOR"));
        }
    }

    private void initPriceSpinnerTextField(int digits) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMinimumFractionDigits(digits);
        numberFormat.setMaximumFractionDigits(digits);
        priceSpinnerTextField = new JFormattedTextField(numberFormat);
        priceSpinnerTextField.setHorizontalAlignment(JFormattedTextField.RIGHT);
        priceSpinnerTextField.setEditable(true);
        priceSpinnerTextField.setFocusLostBehavior(JFormattedTextField.COMMIT);
        txtPrice.setEditor(priceSpinnerTextField);
        priceSpinnerTextField.addPropertyChangeListener("price", this);
    }

    private void initNetValSpinnerTextField(int digits) {
        NumberFormat numberFormat = NumberFormat.getNumberInstance();
        numberFormat.setMinimumFractionDigits(digits);
        numberFormat.setMaximumFractionDigits(digits);
        netValSpinnerTextField = new JFormattedTextField(numberFormat);
        netValSpinnerTextField.setHorizontalAlignment(JFormattedTextField.RIGHT);
        netValSpinnerTextField.setEditable(true);
        netValSpinnerTextField.setFocusLostBehavior(JFormattedTextField.COMMIT);
        txtNetVal.setEditor(netValSpinnerTextField);
        netValSpinnerTextField.addPropertyChangeListener("value", this);
    }

    protected void setTickSize(double increment) {
        modelSpin.setUPTickSize(increment);
    }

    protected void setPriceTickSize(double increment) {
        priceSpin.setUPTickSize(increment);
    }

    protected void increment(double val) {
        netValSpinnerTextField.setText(String.valueOf(Double.parseDouble(netValSpinnerTextField.getText().replaceAll(",", "")) + val));
        try {
            netValSpinnerTextField.commitEdit();
        } catch (ParseException e) {

        }
    }

    protected void incrementPrice(double val) {
        priceSpinnerTextField.setText(String.valueOf(Double.parseDouble(priceSpinnerTextField.getText().replaceAll(",", "")) + val));
        try {
            priceSpinnerTextField.commitEdit();
        } catch (ParseException e) {

        }
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource() == calculateBtn) {
            Thread thread = new Thread(new Runnable() {
                @Override
                public void run() {
                    calculate();
                    dispose();
                }

            }, "Order Quantity calculator");
            thread.start();
        } else {
            dispose();
        }
    }

    public void applyTheme() {
        applyColours();
    }

    public void focusGained(FocusEvent e) {
        if(e.getSource().equals(calculateBtn)) {
            if ( (!(txtNetVal.equals(null)) || !(txtNetVal.equals("0"))) && ((!txtPrice.equals(null)) || (!txtPrice.equals("0"))) && ((CausedFocusEvent) e).getCause().name().equals("TRAVERSAL_FORWARD") ) {
                Thread thread = new Thread(new Runnable() {
                    @Override
                    public void run() {
                        calculate();
                        dispose();
                    }

                }, "Order Quantity calculator");
                thread.start();
            }
        }
    }

    public void focusLost(FocusEvent e) {
    }

    public void propertyChange(PropertyChangeEvent evt) {
        try {
            txtNetVal.setValue(Double.parseDouble(netValSpinnerTextField.getValue().toString()));
            txtPrice.setValue(Double.parseDouble(priceSpinnerTextField.getValue().toString()));
            double value = Double.parseDouble(String.valueOf(netValSpinnerTextField.getValue()));
            double price = Double.parseDouble(String.valueOf(priceSpinnerTextField.getValue()));

            if (value == (long) value) {
                if (value != 0) {
                    netValSpinnerTextField.setText(String.format("%d", (long) value));
                } else {
                    netValSpinnerTextField.setText("");
                }
            } else {
                netValSpinnerTextField.setText(String.format("%s", value));
            }

            if (price == (long) price) {
                if (price != 0) {
                    priceSpinnerTextField.setText(String.format("%d", (long) price));
                } else {
                    priceSpinnerTextField.setText("");
                }
            } else {
                priceSpinnerTextField.setText(String.format("%s", price));
            }
        } catch (Exception e) {

        }
    }
}


