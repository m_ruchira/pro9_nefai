// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.trading.ui;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.shared.TradeMeta;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;
import java.util.Date;


public class TradeTableRenderer extends TWBasicTableRenderer {
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;
    private static TWDateFormat g_oDateTimeFormatHMS;
    private static TWDateFormat g_oDateTimeFormat;
    private TWDateFormat g_oDateTimeFormatHM;
    private String[] g_asMarketStatus = new String[4];
    private int action;
    private boolean isConditional;
    private boolean isDayOrder;
    private boolean isMarkForDelivery;
    private boolean isLastRow;
    private boolean secondTopLastRow;

    private final String g_sNA = "NA";
    private static Color bidColor;
    private static Color askColor;
    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oConditionalBuyBG;
    private static Color g_oConditionalBuyFG;
    private static Color g_oConditionalSellBG;
    private static Color g_oConditionalSellFG;
    private static Color g_oDayOrderBuyBG1;
    private static Color g_oDayOrderSellBG2;
    private static Color g_oMarkForDeliveryOrderBuyBG1;
    private static Color g_oMarkForDeliveryOrderSellBG2;
    private Color upColor;
    private Color downColor;
    private static Color originalBG1;
    private static Border selectedBorder;
    private static Border upperBorder;

    private Color foreground, background;
    private static TWDecimalFormat oPriceFormat;
    private static TWDecimalFormat oQuantityFormat;
    private DefaultTableCellRenderer lblRenderer;
    private JCheckBox checkBoxRenderer;
    private double doubleValue;
    private long longValue;
    private Date date;
    private int selectedDecimals = Constants.UNASSIGNED_DECIMAL_PLACES;
    private int tableDecimals = Constants.UNASSIGNED_DECIMAL_PLACES;
    private int stockDecimals = Constants.UNASSIGNED_DECIMAL_PLACES;

    private static ImageIcon amendImage;
    private static ImageIcon cancelImage;
    private String amendToolTip;
    private String cancelToolTip;

    public TradeTableRenderer(String[] asColumns, int[] asRendIDs) {
        g_asRendIDs = asRendIDs;

        reload();
        g_oDateTimeFormatHMS = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));
        g_oDateTimeFormatHM = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HM_FORMAT"));
        g_oDateTimeFormat = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_FORMAT"));

        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");

        date = new Date();

        if (Language.isLTR())
            g_iStringAlign = JLabel.LEFT;
        else
            g_iStringAlign = JLabel.RIGHT;
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;

        checkBoxRenderer = new JCheckBox();
        checkBoxRenderer.setOpaque(true);

        try {
            amendImage = new ImageIcon("images/Theme" + Theme.getID() + "/menu/amendorder.gif");
        } catch (Exception e) {
            amendImage = null;
        }
        try {
            cancelImage = new ImageIcon("images/Theme" + Theme.getID() + "/menu/cancelorder.gif");
        } catch (Exception e) {
            cancelImage = null;
        }
        amendToolTip = Language.getString("AMEND_TOOLTIP");
        cancelToolTip = Language.getString("CANCEL_TOOLTIP");
    }

    public static void reloadForPrinting() {
        bidColor = Color.black;
        askColor = Color.black;
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oConditionalBuyFG = Color.black;
        g_oConditionalBuyBG = Color.white;
        g_oConditionalSellFG = Color.black;
        g_oConditionalSellBG = Color.white;
        originalBG1 = Color.white;
        g_oMarkForDeliveryOrderBuyBG1 = Color.white;
        g_oMarkForDeliveryOrderSellBG2 = Color.white;
        g_oDayOrderBuyBG1 = Color.white;
        g_oDayOrderSellBG2 = Color.white;
    }

    public static void reload() {
        reloadRenderer();
        try {
            bidColor = Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1");
            askColor = Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            originalBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oConditionalBuyFG = Theme.getOptionalColor("BOARD_TABLE_CONDITIONAL_BUY_FGCOLOR");
            g_oConditionalBuyBG = Theme.getOptionalColor("BOARD_TABLE_CONDITIONAL_BUY_BGCOLOR");
            g_oConditionalSellFG = Theme.getOptionalColor("BOARD_TABLE_CONDITIONAL_SELL_FGCOLOR");
            g_oConditionalSellBG = Theme.getOptionalColor("BOARD_TABLE_CONDITIONAL_SELL_BGCOLOR");
            g_oMarkForDeliveryOrderBuyBG1 = Theme.getOptionalColor("MARK_FOR_DELIVERY_ROW_BUY_BGCOLOR");
            g_oMarkForDeliveryOrderSellBG2 = Theme.getOptionalColor("MARK_FOR_DELIVERY_ROW_SELL_BGCOLOR");
            g_oDayOrderBuyBG1 = Theme.getOptionalColor("DAY_ORDER_ROW_BUY_BGCOLOR");
            g_oDayOrderSellBG2 = Theme.getOptionalColor("DAY_ORDER_ROW_SELL_BGCOLOR");

            selectedBorder = BorderFactory.createMatteBorder(1, 0, 2, 0, g_oSelectedFG);
            upperBorder = BorderFactory.createMatteBorder(1, 0, 0, 0, g_oSelectedFG);
        } catch (Exception e) {
            bidColor = Color.green;
            askColor = Color.red;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            originalBG1 = Color.white;
            g_oConditionalBuyFG = Color.black;
            g_oConditionalBuyBG = Color.white;
            g_oConditionalSellFG = Color.black;
            g_oConditionalSellBG = Color.white;
            g_oMarkForDeliveryOrderBuyBG1 = Color.white;
            g_oMarkForDeliveryOrderSellBG2 = Color.white;
            g_oDayOrderBuyBG1 = Color.white;
            g_oDayOrderSellBG2 = Color.white;
        }
        try {
            amendImage = new ImageIcon("images/Theme" + Theme.getID() + "/menu/amendorder.gif");
        } catch (Exception e) {
            amendImage = null;
        }
        try {
            cancelImage = new ImageIcon("images/Theme" + Theme.getID() + "/menu/cancelorder.gif");
        } catch (Exception e) {
            cancelImage = null;
        }
    }

    public void propertyChanged(int property) {
    }

    public static final DefaultTableCellRenderer DEFAULT_RENDERER =
            new DefaultTableCellRenderer();

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        reload();
        g_asRendIDs = asRendIDs;
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        isLastRow = (table.getModel().getRowCount() == (row + 1));
        secondTopLastRow = (table.getModel().getRowCount() - 2 == (row + 1));
        lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        try {
            action = Integer.parseInt((String) table.getModel().getValueAt(row, 0));
        } catch (NumberFormatException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        isConditional = ((BooleanTransferObject) table.getModel().getValueAt(row, -1)).getValue();
        try {
            isDayOrder = ((BooleanTransferObject) table.getModel().getValueAt(row, -5)).getValue();
        } catch (Exception e) {
            isDayOrder = false;
        }
        try {
            isMarkForDelivery = ((BooleanTransferObject) table.getModel().getValueAt(row, -6)).getValue();
        } catch (Exception e) {
            isMarkForDelivery = false;
        }
        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
                if (action == TradeMeta.BUY) {
                    if (isConditional) {
                        originalBG1 = sett.getConditionBuyColorBG();
                    } else if (isDayOrder) {
                        originalBG1 = g_oDayOrderBuyBG1;
                    } else if (isMarkForDelivery) {
                        originalBG1 = g_oMarkForDeliveryOrderBuyBG1;
                    } else {
                        originalBG1 = sett.getBidColor1BG();
                    }
                } else {
                    if (isConditional) {
                        originalBG1 = sett.getConditionSellColorBG();
                    } else if (isDayOrder) {
                        originalBG1 = g_oDayOrderSellBG2;
                    } else if (isMarkForDelivery) {
                        originalBG1 = g_oMarkForDeliveryOrderSellBG2;
                    } else {
                        originalBG1 = sett.getAskColor1BG();
                    }
                }
            } else {
                if (action == TradeMeta.BUY) {
                    if (isConditional) {
                        background = sett.getConditionBuyColorBG();
                        foreground = sett.getConditionBuyColorFG();
                        originalBG1 = sett.getConditionBuyColorBG();
                    } else if (isDayOrder) {
                        background = g_oDayOrderBuyBG1;
                        foreground = sett.getBidColor1FG();
                        originalBG1 = g_oDayOrderBuyBG1;
                    } else if (isMarkForDelivery) {
                        originalBG1 = g_oMarkForDeliveryOrderBuyBG1;
                        background = g_oMarkForDeliveryOrderBuyBG1;
                        foreground = sett.getBidColor1FG();
                    } else {
                        background = sett.getBidColor1BG();
                        foreground = sett.getBidColor1FG();
                        originalBG1 = sett.getBidColor1BG();
                    }
                } else {
                    if (isConditional) {
                        background = sett.getConditionSellColorBG();
                        foreground = sett.getConditionSellColorFG();
                        originalBG1 = sett.getConditionSellColorBG();
                    } else if (isDayOrder) {
                        background = g_oDayOrderSellBG2;
                        foreground = sett.getAskColor1FG();
                        originalBG1 = g_oDayOrderSellBG2;
                    } else if (isMarkForDelivery) {
                        originalBG1 = g_oMarkForDeliveryOrderSellBG2;
                        background = g_oMarkForDeliveryOrderSellBG2;
                        foreground = sett.getAskColor1FG();
                    } else {
                        background = sett.getAskColor1BG();
                        foreground = sett.getAskColor1FG();
                        originalBG1 = sett.getAskColor1BG();
                    }
                }
            }
            upColor = sett.getPositiveChangeFG();
            downColor = sett.getNegativeChangeFG();
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
                if (action == TradeMeta.BUY) {
                    if ((isConditional) && (g_oConditionalBuyBG != null)) {
                        originalBG1 = g_oConditionalBuyBG;
                    } else if (isDayOrder) {
                        originalBG1 = g_oDayOrderBuyBG1;
                    } else if (isMarkForDelivery) {
                        originalBG1 = g_oMarkForDeliveryOrderBuyBG1;
                    } else {
                        originalBG1 = bidColor;
                    }
                } else {
                    if ((isConditional) && (g_oConditionalSellBG != null)) {
                        originalBG1 = g_oConditionalSellBG;
                    } else if (isDayOrder) {
                        originalBG1 = g_oDayOrderSellBG2;
                    } else if (isMarkForDelivery) {
                        originalBG1 = g_oMarkForDeliveryOrderSellBG2;
                    } else {
                        originalBG1 = askColor;
                    }
                }
            } else {
                foreground = g_oFG1;
                if (action == TradeMeta.BUY) {
                    if ((isConditional) && (g_oConditionalBuyBG != null) && (g_oConditionalBuyFG != null)) {
                        background = g_oConditionalBuyBG;
                        foreground = g_oConditionalBuyFG;
                        originalBG1 = g_oConditionalBuyBG;
                    } else if (isDayOrder) {
                        background = g_oDayOrderBuyBG1;
                        originalBG1 = g_oDayOrderBuyBG1;
                    } else if (isMarkForDelivery) {
                        originalBG1 = g_oMarkForDeliveryOrderBuyBG1;
                        background = g_oMarkForDeliveryOrderBuyBG1;
                    } else {
                        background = bidColor;
                        originalBG1 = bidColor;
                    }
                } else {
                    if ((isConditional) && (g_oConditionalSellBG != null) && (g_oConditionalSellFG != null)) {
                        background = g_oConditionalSellBG;
                        foreground = g_oConditionalSellFG;
                        originalBG1 = g_oConditionalSellBG;
                    } else if (isDayOrder) {
                        background = g_oDayOrderSellBG2;
                        originalBG1 = g_oDayOrderSellBG2;
                    } else if (isMarkForDelivery) {
                        originalBG1 = g_oMarkForDeliveryOrderSellBG2;
                        background = g_oMarkForDeliveryOrderSellBG2;
                    } else {
                        background = askColor;
                        originalBG1 = askColor;
                    }
                }

            }
        }

        if (action == 3) {           // only for today trade window
            foreground = Color.BLACK;
            background = Color.WHITE;
            originalBG1 = Color.WHITE;
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

        tableDecimals = ((SmartTable) table).getDecimalPlaces();
        try {
            stockDecimals = (Byte) table.getModel().getValueAt(row, -4);
        } catch (Exception e) {
            stockDecimals = 2;
        }
        if (tableDecimals == Constants.UNASSIGNED_DECIMAL_PLACES) { // do not use table's decimal places
            if (selectedDecimals != stockDecimals) {
                oPriceFormat = SharedMethods.getDecimalFormat(stockDecimals);
            }
        } else { // use decimals from table
            if (selectedDecimals != tableDecimals) {
                oPriceFormat = SharedMethods.getDecimalFormat(tableDecimals);
            }
        }

        lblRenderer.setToolTipText(null);

        int iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            if ((secondTopLastRow || isLastRow) && action == 3) {
                lblRenderer.setBorder(getBorder(table, isLastRow, column, cellBorder, secondTopLastRow));
            }
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 1: // SYMBOL
                    if (Settings.isShowArabicNumbers())
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 2: // DESCRIPTION
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 3: // PRICE

                    doubleValue = toDoubleValue(value);
                    if ((Double.isNaN(doubleValue)) || (doubleValue == Double.POSITIVE_INFINITY) ||
                            (doubleValue == Double.NEGATIVE_INFINITY) || (doubleValue == Constants.DEFAULT_DOUBLE_VALUE)) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText((oPriceFormat.format(doubleValue)));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4: // QUANTITY
                    lblRenderer.setText(oQuantityFormat.format(toLongValue(value)));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 5: // CHANGE
                    doubleValue = toDoubleValue(value);
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 6: // % CHANGE
                    doubleValue = toDoubleValue(value);
                    lblRenderer.setText(oPriceFormat.format(doubleValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (doubleValue > 0)
                        lblRenderer.setForeground(upColor);
                    else if (doubleValue < 0)
                        lblRenderer.setForeground(downColor);
                    break;
                case 8: // DATE TIME
                    longValue = toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateTimeFormatHM.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    break;
                case 'D': {// DATE TIME with secs
                    longValue = toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateTimeFormatHMS.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                }
                case 'z': {// DATE without TIME
                    longValue = toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue);
                        lblRenderer.setText(g_oDateTimeFormat.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                }
                case 'B': // Bolean;
                    checkBoxRenderer.setSelected(((Boolean) value));
                    checkBoxRenderer.setForeground(foreground);
                    checkBoxRenderer.setBackground(originalBG1);
                    checkBoxRenderer.setHorizontalAlignment(g_iCenterAlign);
                    checkBoxRenderer.setBorder(cellBorder);
                    checkBoxRenderer.repaint();
                    return checkBoxRenderer;
                case 'M': // Market Status
                    try {
                        lblRenderer.setText(g_asMarketStatus[toIntValue(value) - 1]);
                    } catch (Exception e) {
                        lblRenderer.setText(g_asMarketStatus[2]);
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 'A': // Amend image
                    lblRenderer.setText("");
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    if (value.equals(""))
                        lblRenderer.setIcon(null);
                    else
                        lblRenderer.setIcon(amendImage);
                    lblRenderer.setToolTipText(amendToolTip);
                    break;
                case 'C': // Cancel Image
                    lblRenderer.setText("");
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    if (value.equals(""))
                        lblRenderer.setIcon(null);
                    else
                        lblRenderer.setIcon(cancelImage);
                    lblRenderer.setToolTipText(cancelToolTip);
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
        }
        return lblRenderer;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private int toIntValue(Object oValue) throws Exception {
        return Integer.parseInt((String) oValue);
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return Double.parseDouble((String) oValue);
    }

    public Border getBorder(JTable table, boolean isLastRow, int column, Border cellBorder, boolean secondTopLastRow) {
        if ((isLastRow) && (table.convertColumnIndexToModel(column) > 2) && (table.convertColumnIndexToModel(column) < 10)) {
            return selectedBorder;
        } else if ((secondTopLastRow) && (table.convertColumnIndexToModel(column) > 2) && (table.convertColumnIndexToModel(column) < 10)) {
            return upperBorder;
        } else {
            return cellBorder;
        }
    }
}