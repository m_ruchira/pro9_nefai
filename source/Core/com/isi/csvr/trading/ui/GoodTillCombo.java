package com.isi.csvr.trading.ui;

import com.isi.csvr.shared.TWComboBox;

import javax.swing.*;

/**
 * Created by ruchiram on 02/12/19.
 */

public class GoodTillCombo extends TWComboBox {

    public GoodTillCombo(DefaultComboBoxModel defaultComboBoxModel) {
        super(defaultComboBoxModel);
    }


    protected void fireActionEvent() {
        if(this.hasFocus())
            super.fireActionEvent();
    }
}
