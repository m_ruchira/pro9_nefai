package com.isi.csvr.trading.ui;

import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.shared.IntTransferObject;
import com.isi.csvr.shared.DoubleTransferObject;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.trading.datastore.FundTransferStore;
import com.isi.csvr.trading.shared.FundTransfer;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.theme.Theme;

import javax.swing.table.TableModel;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 9, 2006
 * Time: 5:13:25 PM
 */
public class FundTransferModel extends CommonTable
        implements TableModel, CommonTableInterface {
    private IntTransferObject intTrasferObject;
    private DoubleTransferObject doubleTransferObject;

    public FundTransferModel(){
        intTrasferObject = new IntTransferObject();
        doubleTransferObject = new DoubleTransferObject();
    }

    public int getRowCount() {
        if (TradingShared.isReadyForTrading()) {
            return FundTransferStore.getSharedInstance().filteredSize();
        }else {
            return 0;
        }
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public void setSymbol(String symbol) {

    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        FundTransfer transaction = FundTransferStore.getSharedInstance().getFilteredTransaction(rowIndex);
        try {
        switch (columnIndex) {
            case -4:
                return SharedMethods.getCurrencyDecimalPlaces(transaction.getCurrency());
            case 0:
                return TradingShared.getFundTransferType( transaction.getType());
            case 1:
                return  TradingShared.getFundTransferMethod(transaction.getMethod());
            case 2:
                return transaction.getCurrency();
            case 3:
                return (transaction.getAmount()) + "";
            case 4:
                try {
                    return TradingShared.getTrader().getPortfolio(transaction.getPortfolio()).getName() ;
                } catch (Exception e) {
                    return Language.getString("NA");
                }
            case 5:
                try {
                return TradingShared.getFundTransferStatus(transaction.getStatus());
                } catch (Exception e) {
                    return Language.getString("NA");
                }
            case 6:
                   // return transaction.getReference() + "";
            try {
                    return (transaction.getCashTransactionID() + "").equals("-1") ? Language.getString("NA") : transaction.getCashTransactionID() + "";
                } catch (Exception e) {
                    return Language.getString("NA");
                }
            case 7:
                return transaction.getReason();
            case 8:
                return ""+transaction.getDepositeDateLong();
            default:
                return "0";
        }
        } catch (Exception e) {
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        switch (super.getViewSettings().getRendererID(iCol)) {
            case 1:
            case 2:
            case 0:
            case 5:
            case 4:
            case 6:
//            case 8:
//            case 3:
            case 'B':
                return String.class;
//            case  0:
//            case  3:
            case 'P':
            case 'Q':
                return Number[].class;
//            case  0:
            case  3:
            case 8:
            case 'M':
            case 'S':
                return Number.class;
            default:
                return Object.class;
        }
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, null, Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));    //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")
        customizerRecords[1] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_SELECTED_ROW, Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR"), Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("DEPOSITE_ROW_COLOR_1"), FIELD_DEPOSITE_ROW1, Theme.getColor("FUNDTRANSFER_TABLE_DEPOSITE_BGCOLOR1"), Theme.getColor("FUNDTRANSFER_TABLE_DEPOSITE_FGCOLOR1"));
        customizerRecords[3] = new CustomizerRecord(Language.getString("DEPOSITE_ROW_COLOR_2"), FIELD_DEPOSITE_ROW2, Theme.getColor("FUNDTRANSFER_TABLE_DEPOSITE_BGCOLOR2"), Theme.getColor("FUNDTRANSFER_TABLE_DEPOSITE_FGCOLOR2"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("WITHDRAW_ROW_COLOR_1"), FIELD_WITHDRAW_ROW1, Theme.getColor("FUNDTRANSFER_TABLE_WITHDRAW_BGCOLOR1"), Theme.getColor("FUNDTRANSFER_TABLE_WITHDRAW_FGCOLOR1"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("WITHDRAW_ROW_COLOR_2"), FIELD_WITHDRAW_ROW2, Theme.getColor("FUNDTRANSFER_TABLE_WITHDRAW_BGCOLOR2"), Theme.getColor("FUNDTRANSFER_TABLE_WITHDRAW_FGCOLOR2"));

        return customizerRecords;
    }
}
