package com.isi.csvr.trading.ui;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.announcement.DateCombo;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.trading.shared.*;
import com.isi.csvr.trading.*;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.OrderSearchStore;
import com.isi.csvr.trading.datastore.Transaction;
import com.isi.csvr.trading.datastore.OrderStore;
import com.isi.util.FlexGridLayout;
import com.dfn.mtr.mix.beans.*;
import com.dfn.mtr.mix.beans.TransferObject;
import com.dfn.mtr.mix.common.MIXConstants;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import java.awt.*;
import java.awt.event.*;
import java.util.*;
import java.text.*;

/**
 * Created by IntelliJ IDEA.
 * User: randulal
 * Date: Mar 23, 2012
 * Time: 10:05:51 AM
 * To change this template use File | Settings | File Templates.
 */


public class TodayTradeWindow
        extends InternalFrame
        implements ActionListener, TraderProfileDataListener, TradingConnectionListener, Themeable {

    //    private JPanel symbolPanel;
    private TWTextField cmbSymbols;
    private DateCombo fromDateCombo;
    private static DateCombo toDateCombo;
    private TWComboBox cmbPortfolios;
    private TWComboBox cmbSides;
    private TWComboBox cmbStatuses;
    private ArrayList<TWComboItem> statuses;
    private TWTextField txtOrderNumber;
    private TWButton btnClose;
    private TWButton btnOK;
    private ViewSetting oSetting;
    private JButton btnNext;
    private JButton btnPrev;
    private Table blotterTable;
    private static boolean searchInProgress;
    private TWComboBox cmbExchanges;

    public static TodayTradeWindow self = null;
    private TodayTradeModel todayTradeModel;
    private String selectedPortfolio = null;

//    public static synchronized OrderSearchWindow getSharedInstance() {
//        return self;
//    }

    public TodayTradeWindow(ViewSetting oSetting) {
        super(oSetting.getCaption(), oSetting);
        this.oSetting = oSetting;
        oSetting.setParent(this);
        createUI();
        self = this;
    }

    private void createUI() {
        JPanel configPanel = new JPanel();
        String[] configWidths = {"100", "150", "20", "100", "150"};
        String[] configHeights = {"20", "20", "1", "1", "20"};
        FlexGridLayout configLayout = new FlexGridLayout(configWidths, configHeights, 5, 5);
        configPanel.setLayout(configLayout);

        JLabel lblPortfolio = new JLabel(Language.getString("PORTFOLIO"));
        configPanel.add(lblPortfolio);
        cmbPortfolios = new TWComboBox();
        configPanel.add(cmbPortfolios);
        populatePortfolios();   // added additional - cbr
        configPanel.add(new JLabel(""));

        JLabel lblSymbol = new JLabel(Language.getString("SYMBOL"));
        configPanel.add(lblSymbol);
        cmbSymbols = new TWTextField();
        configPanel.add(cmbSymbols);
        /*symbolPanel = new JPanel(new BorderLayout(0, 0));
        configPanel.add(symbolPanel);*/
        JLabel lblStatus = new JLabel(Language.getString("STATUS"));


        fromDateCombo = new DateCombo(Client.getInstance().getFrame());

        configPanel.add(new JLabel(Language.getString("DATE")));
        toDateCombo = new DateCombo(Client.getInstance().getFrame());
        configPanel.add(toDateCombo);

        populateStatusList();

        configPanel.add(new JLabel(""));

        configPanel.add(new JLabel(Language.getString("EXCHANGE")));
        cmbExchanges = new TWComboBox();
        configPanel.add(cmbExchanges);
        populateExchanges();

        /* configPanel.add(lblStatus);
        statuses = new ArrayList<TWComboItem>();
        cmbStatuses = new TWComboBox(new TWComboModel(statuses));
        configPanel.add(cmbStatuses);*/

        configPanel.add(new JLabel(""));
        configPanel.add(new JLabel(""));
        configPanel.add(new JLabel(""));

        configPanel.add(new JLabel(""));//configPanel.add(new JLabel(Language.getString("FROM")));
        configPanel.add(new JLabel(""));  //configPanel.add(fromDateCombo);


        JLabel lblOrderNimber = new JLabel(Language.getString("ORDER_NUMBER"));
        configPanel.add(new JLabel(""));//configPanel.add(lblOrderNimber);
        txtOrderNumber = new TWTextField();
        configPanel.add(new JLabel(""));//configPanel.add(txtOrderNumber);

        configPanel.add(new JLabel(""));

        JLabel lblSide = new JLabel(Language.getString("SIDE"));
        configPanel.add(new JLabel(""));//configPanel.add(lblSide);
        cmbSides = new TWComboBox();
        configPanel.add(new JLabel(""));//configPanel.add(cmbSides);
        populateSides();

        configPanel.add(new JLabel(""));
        configPanel.add(new JLabel(""));
        configPanel.add(new JLabel(""));

        configPanel.add(new JLabel(""));

        JPanel buttonPanel = new JPanel();
        BoxLayout buttonLayout = new BoxLayout(buttonPanel, BoxLayout.LINE_AXIS);
        buttonPanel.setLayout(buttonLayout);
        btnOK = new TWButton(Language.getString("REFRESH"));
        btnOK.addActionListener(this);
        btnOK.setActionCommand("OK");
        btnClose = new TWButton(Language.getString("CLOSE"));
        btnClose.addActionListener(this);
        btnClose.setActionCommand("CANCEL");
        buttonPanel.add(btnOK);
        buttonPanel.add(Box.createHorizontalGlue());
        buttonPanel.add(btnClose);
        configPanel.add(buttonPanel);

        //getContentPane().setLayout(new BorderLayout());
        //getContentPane().add(configPanel, BorderLayout.NORTH);
        createTable();
        blotterTable.setNorthPanel(configPanel);
        createButtonPanel();
        setSize(600, 400);
        Client.getInstance().getDesktop().add(this);
        setDefaultCloseOperation(HIDE_ON_CLOSE);

        Calendar cal = Calendar.getInstance();
        fromDateCombo.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DATE));
        toDateCombo.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONDAY), cal.get(Calendar.DATE));
        //setLocationRelativeTo(Client.getInstance().getDesktop());
        if (cmbPortfolios.getSelectedItem() != null) {
            setTitle(((TWComboItem) cmbPortfolios.getSelectedItem()).getId() + "  -  " + Language.getString("TODAY_TRADE_TITLE"));
        } else {
            setTitle(Language.getString("TODAY_TRADE_TITLE"));
        }
        blotterTable.getPopup().hideCustomizer();
        //   blotterTable.getPopup().showLinkMenus();
        setResizable(true);
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setDetachable(true);
        setPrintable(true);
        setColumnResizeable(true);
        setVisible(oSetting.isVisible());
        hideTitleBarMenu();
        super.applySettings();
        GUISettings.applyOrientation(this);
        Theme.registerComponent(this);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        //   sendRequest(0);
    }

    private void createTable() {
        final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
        final String searching = Language.getString("SEARCHING");

        blotterTable = new Table() {
            public void paint(Graphics g) {
                super.paint(g);
                if (isSearchInProgress()) {
                    FontMetrics fontMetrics = blotterTable.getFontMetrics(getFont());
                    g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
                            (getHeight()) / 2);
                    g.drawImage(icon.getImage(),
                            (getWidth() - fontMetrics.stringWidth(searching)) / 2 + fontMetrics.stringWidth(searching) + 10,
                            (getHeight() - icon.getImage().getHeight(this)) / 2, this);
                    fontMetrics = null;
                }
            }
        };
        blotterTable.setThreadID(Constants.ThreadTypes.TRADING);
        //final FontMetrics fontMetrics = blotterTable.getFontMetrics(getFont());
        blotterTable.setPreferredSize(new Dimension(500, 100));
        blotterTable.setSortingDisabled();
        //     blotterTable.sortData();
        todayTradeModel = new TodayTradeModel(TradeMeta.BLOTTER_TODAY);          //sabbcbr ??
        try {
            blotterTable.getPopup().setMenuItem(getCopyMenu());
            blotterTable.getPopup().setMenuItem(getCopyWithHeadMenu());
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        blotterTable.setWindowType(ViewSettingsManager.ORDER_SEARCH);  // sabbcbr ??
        oSetting.setColumnHeadings(Language.getList("TODAY_TRADE_COLS"));
        todayTradeModel.setViewSettings(oSetting);
        blotterTable.setModel(todayTradeModel);

        blotterTable.setSortingDisabled();

        todayTradeModel.setTable(blotterTable);
        ((SmartTable) blotterTable.getTable()).setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
        todayTradeModel.applyColumnSettings();
        todayTradeModel.updateGUI();

        blotterTable.getPopup().hideTitleBarMenu();
        this.setResizable(false);   // cbr
        oSetting.setParent(this);
        //super.applySettings();
        setLayer(GUISettings.TOP_LAYER);
        //getContentPane().add(blotterTable, BorderLayout.CENTER);
        getContentPane().add(blotterTable);
        super.setTable(blotterTable);

        blotterTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
/*                if (e.getClickCount() > 1) {
                    int row = blotterTable.getTable().getSelectedRow();
                    String clOrdId = (String) blotterTable.getTable().getModel().getValueAt(row, 3); // get the clOrdId
                    String mubasherOrderID = (String) blotterTable.getTable().getModel().getValueAt(row, 17);
                    Transaction order = OrderSearchStore.getSharedInstance().searhTransactionByClOrderID(clOrdId);
                    if (order == null) {
                        order = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(mubasherOrderID);
                    }
                    if (BrokerConfig.getSharedInstance().showOrderHistoryInBlotter(TradingShared.getTrader().getPath(order.getPortfolioNo()))) {
                        TradeMethods.getSharedInstance().showDetailQuoteOrder(order);
                    }
//                    TradeMethods.getSharedInstance().showDetailQuoteOrder(order);
                    order = null;
                } else {
                    if (SwingUtilities.isLeftMouseButton(e)) {
                        int row = blotterTable.getTable().getSelectedRow();
                        int col = blotterTable.getTable().convertColumnIndexToModel(blotterTable.getTable().getSelectedColumn());
                        if (col == 6) {
                            String orderID = (String) blotterTable.getTable().getModel().getValueAt(row, 3);
                            String mubasherOrderID = (String) blotterTable.getTable().getModel().getValueAt(row, 17);
                            Transaction transaction = OrderSearchStore.getSharedInstance().searhTransactionByClOrderID(orderID);
                            if (transaction == null) {
                                transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(mubasherOrderID);
                            }
                            //Transaction transaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(orderID);
                            if ((transaction != null) && (transaction.getStatus() == TradeMeta.T39_Rejected)) {
                                TradeMethods.getSharedInstance().showRejectReasonFull(transaction);
                                transaction = null;
                            }
                        }
                    }
                }*/
            }
        });
    }

    public void createButtonPanel() {
        JPanel buttonPanel = new JPanel(new BorderLayout(5, 5));
        btnPrev = new JButton("  " + Language.getString("PREVIOUS") + "  ");
        btnPrev.setBorder(BorderFactory.createEmptyBorder());
        btnNext = new JButton("  " + Language.getString("NEXT") + "  ");
        btnNext.setBorder(BorderFactory.createEmptyBorder());
//        if (BrokerConfig.getSharedInstance().isPrevNextInOrderSearchHidden()) {
        buttonPanel.add(new JLabel(""), BorderLayout.WEST);
        buttonPanel.add(new JLabel(""), BorderLayout.EAST);
        //      } else {
        //        buttonPanel.add(btnPrev, BorderLayout.WEST);
        //         buttonPanel.add(btnNext, BorderLayout.EAST);
        //      }
        btnPrev.addActionListener(this);
        btnNext.addActionListener(this);
        btnPrev.setEnabled(false);
        btnNext.setEnabled(false);
        //getContentPane().add(buttonPanel, BorderLayout.SOUTH);
        blotterTable.setSouthPanel(buttonPanel);
    }

    private void populateSides() {
        cmbSides.addItem(new TWComboItem("0", Language.getString("ALL")));
        cmbSides.addItem(new TWComboItem("" + TradeMeta.BUY, Language.getString("BUY")));
        cmbSides.addItem(new TWComboItem("" + TradeMeta.SELL, Language.getString("SELL")));
    }

    private void populateExchanges() {
        cmbExchanges.addItem(new TWComboItem("*", Language.getString("ALL")));
        Enumeration<String> exchanges = TradingShared.getTradingExchanges();
        if (exchanges != null) {
            String exchangeCode = null;
            while (exchanges.hasMoreElements()) {
                try {
                    exchangeCode = exchanges.nextElement();
                    String exchangeName = (ExchangeStore.getSharedInstance().getExchange(exchangeCode)).getDescription();
                    cmbExchanges.addItem(new TWComboItem(exchangeCode, exchangeName));
                } catch (Exception e) {
                    System.out.println("Error: Tradable exchange " + exchangeCode + " not found in price exchanges list");
                }
            }
        }
    }

    private void populateStatusList() {
        try { // sabb cbr
            statuses.clear();
            String str = BrokerConfig.getSharedInstance().getOrderTypesForOrderSearch(TradingShared.getTrader().getPath(selectedPortfolio));
            if (str == null || str.equals("")) {
                statuses.add(getStatusItem(TradeMeta.T39_New));
                statuses.add(getStatusItem(TradeMeta.T39_Partially_Filled));
                statuses.add(getStatusItem(TradeMeta.T39_Filled));
                //cmbStatuses.addItem(getStatusItem(TradeMeta.T39_Executed));
                statuses.add(getStatusItem(TradeMeta.T39_Done_For_Day));
                statuses.add(getStatusItem(TradeMeta.T39_Canceled));
                statuses.add(getStatusItem(TradeMeta.T39_Replaced));
                //cmbStatuses.addItem(getStatusItem(TradeMeta.T39_Pending_Cancel));
                //cmbStatuses.addItem(getStatusItem(TradeMeta.T39_Stopped));
                statuses.add(getStatusItem(TradeMeta.T39_Rejected));
                statuses.add(getStatusItem(TradeMeta.T39_Suspended));
                //cmbStatuses.addItem(getStatusItem(TradeMeta.T39_Pending_New));
                //cmbStatuses.addItem(getStatusItem(TradeMeta.T39_Calculated));
                statuses.add(getStatusItem(TradeMeta.T39_Expired));
                statuses.add(getStatusItem(TradeMeta.T39_COMPLETED));
                //cmbStatuses.addItem(getStatusItem(TradeMeta.T39_Accepted_For_Bidding));
                //cmbStatuses.addItem(getStatusItem(TradeMeta.T39_Pending_Replace));
                //cmbStatuses.addItem(getStatusItem(TradeMeta.T39_DEFAULT));
                //cmbStatuses.addItem(getStatusItem(TradeMeta.T39_RECEIVED));
                //cmbStatuses.addItem(getStatusItem(TradeMeta.T39_OMSACCEPTED));
                //cmbStatuses.addItem(getStatusItem(TradeMeta.T39_SEND_TO_EXCHANGE_NEW));
                //cmbStatuses.addItem(getStatusItem(TradeMeta.T39_SEND_TO_EXCHANGE_CANCEL));
                //cmbStatuses.addItem(getStatusItem(TradeMeta.T39_SEND_TO_EXCHANGE_AMEND));
                if(TWControl.getClientName().equals(Constants.CO_BRANDING_CLIENT_NEFAIE)){
                    statuses.add(getStatusItem(TradeMeta.T39_MURABAHA_PENDING));
                    statuses.add(getStatusItem(TradeMeta.T39_MURABAHA_APPROVED));
                }
            } else {
                String[] data = str.split(",");
                for (int i = 0; i < data.length; i++) {
                    char s = data[i].toCharArray()[0];
                    statuses.add(getStatusItem(s));
                }
            }

            Collections.sort(statuses);
            statuses.add(0, new TWComboItem("" + TradeMeta.T39_DEFAULT, Language.getString("ALL")));
            cmbStatuses.setSelectedIndex(0);
            cmbStatuses.updateUI();
        } catch (Exception ex) {

        }
    }

    public void doSearch(String portfolio, String symbol, Date from, Date to) {

        initSearch();

        // select the portfolio
        for (int i = 0; i < cmbPortfolios.getItemCount(); i++) {
            //  if (((TWComboItem) cmbPortfolios.getItemAt(i)).getValue().equals(portfolio)) {
            if (((TWComboItem) cmbPortfolios.getItemAt(i)).getId().equals(portfolio)) {
                cmbPortfolios.setSelectedIndex(i);
                break;
            }
        }

        cmbSymbols.setText(symbol);

        // set from date
        Calendar cal = Calendar.getInstance();
        cal.setTime(from);
        fromDateCombo.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

        // set to date
        cal = Calendar.getInstance();
        cal.setTime(to);
        toDateCombo.setDate(cal.get(Calendar.YEAR), cal.get(Calendar.MONTH), cal.get(Calendar.DAY_OF_MONTH));

        // reset the Status
        cmbStatuses.setSelectedIndex(0);

        // clear the order id
        txtOrderNumber.setText("");

        sendRequest(0);
    }

    private TWComboItem getStatusItem(char status) {
        return new TWComboItem("" + status, TradingShared.getOMSStatusString(status, false));
    }

    /*public void setVisible(boolean status) {
        if (status == true) {
            *//*cmbSymbols = new TWTextField();
            symbolPanel.add(cmbSymbols);*//*
            super.show();
        }
        super.setVisible(status);
    }*/

    private synchronized void populatePortfolios() {
        try {
            if (TradingShared.getTrader() != null) {
                try {
                    cmbPortfolios.removeAllItems();
                } catch (Exception e) {
                    e.printStackTrace();
                }
                TradingPortfolioRecord record;
                ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    record = portfolios.get(i);
                    TWComboItem item;
                    item = new TWComboItem(record.getPortfolioID(), record.getName());
                    cmbPortfolios.addItem(item);
                    item = null;
                    record = null;
                }
                cmbPortfolios.updateUI();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initSearch() {
        OrderSearchStore.getSharedInstance().init();
    }

    private boolean isSymbolSelected() {
        String symbol = cmbSymbols.getText().trim().toUpperCase();
        if ((symbol == null) || (symbol.trim().equals(""))) {
            return false;

        }
        return true;
    }

    public static String getSelectedDate() {
        try {
            return toDateCombo.getDateString();
        } catch (Exception e) {
            return "";
        }
    }


    private void sendRequest(long startID) {
        String symbol;

        try {
            if (TradingShared.isConnected()) {
                String orderID;
                //    String date = new SimpleDateFormat("yyyyMMdd").format(Calendar.getInstance().getTime());
                String date = toDateCombo.getDateString();
                //    String date = "20120409";
                orderID = "*";

                setSearchInProgress(true);    //cbrsabb
                symbol = cmbSymbols.getText().trim().toUpperCase();
                if ((symbol == null) || (symbol.trim().equals(""))) {
                    symbol = "*";
                }

                TradeMessage message = new TradeMessage(TradeMeta.MT_SEARCH);
                message.addData(((TWComboItem) cmbPortfolios.getSelectedItem()).getId());
                message.addData('X');
                //        if (toDateCombo.getDateString().compareToIgnoreCase(fromDateCombo.getDateString()) > 0) {
                //           message.addData(fromDateCombo.getDateString());
                //           message.addData(toDateCombo.getDateString());
                //        } else {
                message.addData(date);
                message.addData(date);
                //       }
                String portfolio = ((TWComboItem) cmbPortfolios.getSelectedItem()).getId();
                message.addData(startID);
                message.addData(TradingShared.getTradingSymbol(symbol, portfolio));
                message.addData(orderID);
                message.addData("0");
                message.addData(TradingShared.getTrader().getUserID(portfolio));
                message.addData(OrderSearchStore.getSharedInstance().getTableName());
                message.addData(((TWComboItem) cmbExchanges.getSelectedItem()).getId());

                String pfId = ((TWComboItem) cmbPortfolios.getSelectedItem()).getId();

                MIXObject orderSerch = new MIXObject();
                MIXHeader header = new MIXHeader();
                header.setGroup(MIXConstants.GROUP_TRADING_INQUIRY);
                header.setRequestType(MIXConstants.REQUEST_TYPE_ORDER_SEARCH);
                header.setSessionID(TradingShared.getTrader().getSessionID(pfId));

                OrderSearchInquiryObject inquiry = new OrderSearchInquiryObject();
                inquiry.setChannelID("6");
                inquiry.setOrdPortfolioID(portfolio);
                inquiry.setOrdUserID(TradingShared.getTrader().getUserID(portfolio));
                inquiry.setOrderStatus('X');
                inquiry.setOrdStartDate(date);
                inquiry.setOrdEndDate(date);
                inquiry.setOrdSequenceNo("0");
                inquiry.setSymbol(TradingShared.getTradingSymbol(symbol, portfolio));
                inquiry.setClOrderID("*");
                inquiry.setOrderSide(0);
                inquiry.setExchange(((TWComboItem) cmbExchanges.getSelectedItem()).getId());

//                inquiry.setOrdPortfolioID("S000008460");
//                inquiry.setOrdUserID("33539");
//                inquiry.setOrderStatus('2');
//                inquiry.setOrdStartDate("20070906");
//                inquiry.setOrdEndDate("20070908");
//                inquiry.setOrdSequenceNo(802383);
//                inquiry.setSymbol("DIC ");
//                inquiry.setClOrderID(802383);
//                inquiry.setOrderSide(1);
                Pagination page = new Pagination();
                page.setStartingSequenceNumber(startID + "");
                page.setPageWidth(25);
                inquiry.setPages(page);
                orderSerch.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inquiry});
//                orderSerch.setTransferObjects(inquiry);
                orderSerch.setMIXHeader(header);

                //   SendQueue.getSharedInstance().addData(message.toString(), TradingShared.getTrader().getPath(portfolio));
                SendQueue.getSharedInstance().addData(orderSerch.getMIXString(), TradingShared.getTrader().getPath(portfolio));   // SABBcbr
                message = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isTodayTradeVisible() {
        return false;
    }

    public void enableButtons() {
        btnOK.setEnabled(true);
    }

    private boolean isValidDateRange() {
        Calendar cal = Calendar.getInstance();
        SimpleDateFormat format = new SimpleDateFormat("yyyyMMdd");
        Long toDate;
        Long fromDate;
        try {
            toDate = format.parse(toDateCombo.getDateString()).getTime();
            fromDate = format.parse(fromDateCombo.getDateString()).getTime();
            if (fromDate > toDate) {
                SharedMethods.showMessage(Language.getString("INVALID_DATE_RANGE"), JOptionPane.WARNING_MESSAGE);
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        int months = BrokerConfig.getSharedInstance().getOrderSearchValidMonthRange(TradingShared.getTrader().getPath(selectedPortfolio));
        if (months == 0) {
            return true;
        }
        cal.add(Calendar.MONTH, months);

        try {
            Long lastValidDate = format.parse(format.format(cal.getTime())).getTime();
            toDate = format.parse(toDateCombo.getDateString()).getTime();
            fromDate = format.parse(fromDateCombo.getDateString()).getTime();
            if (lastValidDate > toDate) {
                SharedMethods.showMessage(Language.getString("MSG_INVALID_DATE_RANGE_SELECTED"), JOptionPane.WARNING_MESSAGE);
                return false;
            }
            if (lastValidDate > fromDate) {
                SharedMethods.showMessage(Language.getString("MSG_INVALID_DATE_RANGE_SELECTED"), JOptionPane.WARNING_MESSAGE);
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return true;
    }

    // action listener

    public void actionPerformed(ActionEvent e) {
        if (e.getSource() == btnOK) {
            if (TradingShared.isReadyForTrading()) {
                if (isSymbolSelected()) {
//                    new Thread("Order Search Disable Thread") {

//                        public void run() {
                            initSearch();
                            sendRequest(0);
                            btnOK.setEnabled(false);
                            try {
                                Thread.sleep(3000);
                            } catch (InterruptedException e1) {
                                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                            btnOK.setEnabled(true);
//                        }
//                    }.start();
                } else {
                    SharedMethods.showMessage(Language.getString("MSG_SYMBOL_CANNOT_BE_BLANK"), JOptionPane.WARNING_MESSAGE);
                }
                /*else {
                    SharedMethods.showMessage(Language.getString("MSG_INVALID_DATE_RANGE_SELECTED"), JOptionPane.WARNING_MESSAGE);
                }*/

            } else {
                SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            }
        } else if (e.getSource() == btnClose) {
            setVisible(false);
        } else if (e.getSource() == cmbPortfolios) {
            selectedPortfolio = ((TWComboItem) cmbPortfolios.getSelectedItem()).getId();
            populateStatusList();
        } else if (e.getSource() == cmbSymbols) {

        } else if (e.getSource() == btnPrev) {
            if (TradingShared.isReadyForTrading()) {
                OrderSearchStore.getSharedInstance().clearTodayTrades();
                OrderSearchStore.getSharedInstance().goPageBack();
                sendRequest(OrderSearchStore.getSharedInstance().getPageBottom());
            } else {
                SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            }
        } else if (e.getSource() == btnNext) {
            if (TradingShared.isReadyForTrading()) {
                OrderSearchStore.getSharedInstance().clearTodayTrades();
                sendRequest(OrderSearchStore.getSharedInstance().getPageBottom());
            } else {
                SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            }
        }
    }

    public static boolean isSearchInProgress() {
        return searchInProgress;
    }

    public static void setSearchInProgress(boolean status) {
        searchInProgress = status;
        /* if (!searchInProgress) {
            self.btnPrev.setEnabled(OrderSearchStore.getSharedInstance().canGoPrev());
            self.btnNext.setEnabled(OrderSearchStore.getSharedInstance().canGoNext());
        }*/
    }

    // Trader profile listener

    public void accountDataChanged(String accountID) {

    }

    public void portfolioDataChanged(String portfolioID) {
        populatePortfolios();
    }

    public void internalFrameActivated(InternalFrameEvent e) {
        super.internalFrameActivated(e);
        //trade
        //cmbSymbols = Client.getInstance().getCompanySelector();
        //reg
        //cmbSymbols = new JComboBox();
        //symbolPanel.add(cmbSymbols);
        super.show();
    }

    public void tradeServerConnected() {
        cmbExchanges.removeAllItems();
        populateExchanges();
        populateStatusList();
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        setSearchInProgress(false);
        OrderSearchStore.getSharedInstance().clearTodayTrades();
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        toDateCombo.applyTheme();
        fromDateCombo.applyTheme();
    }

    public TWMenuItem getCopyMenu() {
        TWMenuItem mnuCopy = new TWMenuItem(Language.getString("COPY"), "copy.gif");
        mnuCopy.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK));
        mnuCopy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    todayTradeModel.copyCells(blotterTable, false);
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });
        return mnuCopy;
    }

    public TWMenuItem getCopyWithHeadMenu() {
        TWMenuItem mnuCopyH = new TWMenuItem(Language.getString("COPY_WITH_HEAD"), "copywh.gif");
        mnuCopyH.setAccelerator(KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_DOWN_MASK));
        mnuCopyH.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    todayTradeModel.copyCells(blotterTable, true);
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });
        return mnuCopyH;
    }
}

