/*
 * Created by JFormDesigner on Wed Dec 02 13:40:49 IST 2009
 */

package com.isi.csvr.trading.ui.orderentry;

import java.awt.event.*;
import javax.swing.*;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.*;
import info.clearthought.layout.*;

/**
 * @author Shanika Liyanage
 */
public class MiniTradingPanel extends JPanel {
    public MiniTradingPanel() {
        initComponents();
    }

    public JComboBox getComboPorfolio() {
        return comboPorfolio;
    }

    public JComboBox getComboCustodian() {
        return comboCustodian;
    }

    public JTextField getComboExchange() {
        return comboExchange;
    }

    public JTextField getTextSymbol() {
        return textSymbol;
    }

    public JComboBox getComboSide() {
        return comboSide;
    }

    public JComboBox getComboType() {
        return comboType;
    }

    public JTextField getTextPrice() {
        return textPrice;
    }

    public JTextField getTextQty() {
        return textQty;
    }

    public JButton getButtonSend() {
        return buttonSend;
    }

    public JLabel getLabelPortfolio() {
        return labelPortfolio;
    }

    public JLabel getLabelPriQty() {
        return labelPriQty;
    }

    public JLabel getLabelSideType() {
        return labelSideType;
    }

    public JLabel getLabelExsymbol() {
        return labelExsymbol;
    }

    public JLabel getLabelCustodian() {
        return labelCustodian;
    }

    public void buttonSendMouseClicked() {
        // TODO add your code here
    }

    public void comboSideMouseClicked() {
        // TODO add your code here
    }

    public void comboSideActionPerformed() {
        // TODO add your code here
    }

    public void textSymbolFocusGained() {
        // TODO add your code here
    }

    public void textSymbolFocusLost() {
        // TODO add your code here
    }

    public void textSymbolKeyReleased() {
        // TODO add your code here
    }

    public void textSymbolKeyTyped() {
        // TODO add your code here
    }

    private void textSymbolKeyPressed() {
        // TODO add your code here
    }

    public void comboTypeActionPerformed() {
        // TODO add your code here
    }

    public void removeCustodians(){
        remove(labelCustodian);
        remove(comboCustodian);
        comboCustodianComponentRemoved();
    }

    public void addCustodians() {
        add(labelCustodian, new TableLayoutConstraints(0, 1, 0, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
        add(comboCustodian, 3);
//        comboCustodianComponentRemoved();
    }

   /* public void requestFocusSymbol() {
         textSymbol.addFocusListener((FocusListener) this);
        textSymbol.requestFocus(true);

    }*/

    private void comboCustodianComponentRemoved() {
        // TODO add your code here
        updateUI();
    }

    private void labelCustodianComponentRemoved() {
        // TODO add your code here
    }

    private void textPriceKeyPressed() {
        // TODO add your code here
    }

    private void textPriceKeyReleased() {
        // TODO add your code here
    }

    public void textPriceKeyTyped() {
        // TODO add your code here
    }

    private void textQtyKeyPressed() {
        // TODO add your code here
    }

    private void textQtyKeyReleased() {
        // TODO add your code here
    }

    public void textQtyKeyTyped() {
        // TODO add your code here
    }

    public TableLayout getTabeLayout() {
        return ((TableLayout) getLayout());
    }

    private void initComponents() {
        // JFormDesigner - Component initialization - DO NOT MODIFY  //GEN-BEGIN:initComponents
        // Generated using JFormDesigner Evaluation license - Shanika Liyanage
        labelPortfolio = new JLabel();
        comboPorfolio = new TWComboBox();
        labelCustodian = new JLabel();
        comboCustodian = new TWComboBox();
        labelExsymbol = new JLabel();
        comboExchange = new TWTextField();
        textSymbol = new TWTextField();
        labelSideType = new JLabel();
        comboSide = new TWComboBox();
        comboType = new TWComboBox();
        labelPriQty = new JLabel();
        textPrice = new TWTextField();
        textQty = new TWTextField();
        buttonSend = new TWButton();

        //======== this ========

        // JFormDesigner evaluation mark
        setBorder(new javax.swing.border.CompoundBorder(
            new javax.swing.border.TitledBorder(new javax.swing.border.EmptyBorder(0, 0, 0, 0),
                "", javax.swing.border.TitledBorder.CENTER,
                javax.swing.border.TitledBorder.BOTTOM, new java.awt.Font("Dialog", java.awt.Font.BOLD, 12),
                java.awt.Color.red), getBorder())); addPropertyChangeListener(new java.beans.PropertyChangeListener(){public void propertyChange(java.beans.PropertyChangeEvent e){if("border".equals(e.getPropertyName()))throw new RuntimeException();}});

        setLayout(new TableLayout(new double[][] {
            {103, 72, 75},
            {24, 24, 24, 24, 24, TableLayout.PREFERRED, TableLayout.PREFERRED}}));
        ((TableLayout)getLayout()).setHGap(5);
        ((TableLayout)getLayout()).setVGap(5);

        //---- labelPortfolio ----
        labelPortfolio.setText("Portfolio");
        add(labelPortfolio, new TableLayoutConstraints(0, 0, 0, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
        add(comboPorfolio, new TableLayoutConstraints(1, 0, 2, 0, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

        //---- labelCustodian ----
        labelCustodian.setText("Custodian");
        labelCustodian.addContainerListener(new ContainerAdapter() {
            @Override
            public void componentRemoved(ContainerEvent e) {
                labelCustodianComponentRemoved();
            }
        });
        add(labelCustodian, new TableLayoutConstraints(0, 1, 0, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

        //---- comboCustodian ----
        comboCustodian.addContainerListener(new ContainerAdapter() {
            @Override
            public void componentRemoved(ContainerEvent e) {
                comboCustodianComponentRemoved();
            }
        });
        add(comboCustodian, new TableLayoutConstraints(1, 1, 2, 1, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

        //---- labelExsymbol ----
        labelExsymbol.setText("Exch./Symbol");
        add(labelExsymbol, new TableLayoutConstraints(0, 2, 0, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
        add(comboExchange, new TableLayoutConstraints(1, 2, 1, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

        //---- textSymbol ----
        textSymbol.addFocusListener(new FocusAdapter() {
            @Override
            public void focusGained(FocusEvent e) {
                textSymbolFocusGained();
            }
            @Override
            public void focusLost(FocusEvent e) {
                textSymbolFocusLost();
            }
        });
        textSymbol.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                textSymbolKeyPressed();
            }
            @Override
            public void keyReleased(KeyEvent e) {
                textSymbolKeyReleased();
            }
            @Override
            public void keyTyped(KeyEvent e) {
                textSymbolKeyTyped();
            }
        });
        add(textSymbol, new TableLayoutConstraints(2, 2, 2, 2, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

        //---- labelSideType ----
        labelSideType.setText("Side/Type");
        add(labelSideType, new TableLayoutConstraints(0, 3, 0, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

        //---- comboSide ----
        comboSide.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                comboSideMouseClicked();
            }
        });
        comboSide.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                comboSideActionPerformed();
            }
        });
        add(comboSide, new TableLayoutConstraints(1, 3, 1, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

        //---- comboType ----
        comboType.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                comboTypeActionPerformed();
            }
        });
        add(comboType, new TableLayoutConstraints(2, 3, 2, 3, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

        //---- labelPriQty ----
        labelPriQty.setText("Price/Qty");
        add(labelPriQty, new TableLayoutConstraints(0, 4, 0, 4, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

        //---- textQty ----
        textQty.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                textQtyKeyPressed();
                textQtyKeyPressed();
                textQtyKeyPressed();
            }

            @Override
            public void keyReleased(KeyEvent e) {
                textQtyKeyReleased();
            }

            @Override
            public void keyTyped(KeyEvent e) {
                textQtyKeyTyped();
            }
        });
        //add(textQty, new TableLayoutConstraints(2, 4, 2, 4, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
        add(textQty, new TableLayoutConstraints(1, 4, 1, 4, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

        //---- textPrice ----
        textPrice.addKeyListener(new KeyAdapter() {
            @Override
            public void keyPressed(KeyEvent e) {
                textPriceKeyPressed();
            }

            @Override
            public void keyReleased(KeyEvent e) {
                textPriceKeyReleased();
            }

            @Override
            public void keyTyped(KeyEvent e) {
                textPriceKeyTyped();
                }
        });
        //add(textPrice, new TableLayoutConstraints(1, 4, 1, 4, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
        add(textPrice, new TableLayoutConstraints(2, 4, 2, 4, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));

        //---- buttonSend ----
        buttonSend.setText(Language.getString("MINI_TRADE_SEND"));
        buttonSend.addMouseListener(new MouseAdapter() {
            @Override
            public void mouseClicked(MouseEvent e) {
                buttonSendMouseClicked();
            }
        });
        add(buttonSend, new TableLayoutConstraints(1, 5, 2, 5, TableLayoutConstraints.FULL, TableLayoutConstraints.FULL));
        GUISettings.applyOrientation(this);
        // JFormDesigner - End of component initialization  //GEN-END:initComponents
    }

    // JFormDesigner - Variables declaration - DO NOT MODIFY  //GEN-BEGIN:variables
    // Generated using JFormDesigner Evaluation license - Shanika Liyanage
    private JLabel labelPortfolio;
    private TWComboBox comboPorfolio;
    private JLabel labelCustodian;
    private TWComboBox comboCustodian;
    private JLabel labelExsymbol;
    private TWTextField comboExchange;
    private TWTextField textSymbol;
    private JLabel labelSideType;
    private TWComboBox comboSide;
    private TWComboBox comboType;
    private JLabel labelPriQty;
    private TWTextField textPrice;
    private TWTextField textQty;
    private TWButton buttonSend;
    // JFormDesigner - End of variables declaration  //GEN-END:variables
}
