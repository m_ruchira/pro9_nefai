package com.isi.csvr.trading.ui.orderentry;

import bsh.Interpreter;
import com.isi.csvr.Client;
import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.chrriis.dj.swingsuite.JNumberEntryField;
import com.isi.csvr.chrriis.dj.swingsuite.JTextEntryField;
import com.isi.csvr.chrriis.dj.swingsuite.TextEntryFormatter;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.SymbolsRegistry;
import com.isi.csvr.portfolio.PFStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.*;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.portfolio.TransactRecord;
import com.isi.csvr.trading.shared.Account;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.util.Decompress;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.text.NumberFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Dec 2, 2009
 * Time: 1:51:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class MiniTradingCorePanel extends MiniTradingPanel implements TradingConnectionListener, Themeable, ActionListener {

    private JComboBox cmbPortfolio;
    private JComboBox bookKeepersCombo;
    private TWComboModel bookKeepersModel;
    private ArrayList<TWComboItem> bookKeepers;
    //    private JComboBox cmbExchanges;
    private JTextField cmbExchanges;
    private JTextField txtSymbol;
    private JComboBox cmbSide;
    private JComboBox cmbType;
    private ArrayList<TWComboItem> orderTypesList;
    private JTextField txtPrice;
    private JTextField txtQty;
    private JButton btnSend;

    private String selectedKey;
    private String symbol;
    private String exchange;
    private int insType;
    private String symbolCode;

    private int side = 0; // order variables
    private int selectedside = 0; // order variables

    private String orderValue = "";
    private double commission = 0;
    private double vat = 0;
    private double netValue = 0;
    private String initMargin = "";
    private String maintMargin = "";

    private String miniTradeParams;

    private String selectedPortfolio = "";
    private String selectedCustodian = "";

    private char type = '2';
    private String price = "";
    private String quantity = "";

    private boolean isSymbolTyped;
    private boolean isSymbolFocused;

    private boolean isTradeConnected;
    private MiniTradingFrame parent;

    private int conditionalMode = TradeMeta.CONDITION_TYPE_NONE;
    private int decimalCount = 2;
    private double stopPrice = -1;
    private double trailStopPrice = -1;
    private double takeProfit = -1;
    private int stopLossType = -1;
    private String field;
    private String operator;
    private float conditionValue;
    private String stopLossTypeStr = "";
    private boolean isDayOrder = false;
    private String tiffStr = "";
    private boolean isMarginUsed = false;
    private int rightsAwarenessStatus = 0;

    public MiniTradingCorePanel(String key, String miniTradeParams, MiniTradingFrame miniTradingFrame) {
        this.selectedKey = key;
        this.exchange = SharedMethods.getExchangeFromKey(key);
        this.symbol = SharedMethods.getSymbolFromKey(key);
        this.insType = SharedMethods.getInstrumentTypeFromKey(key);
        Stock st = DataStore.getSharedInstance().getStockObject(key);
        if (st != null) {
            this.symbolCode = st.getSymbolCode();
        }
        this.miniTradeParams = miniTradeParams;
        this.parent = miniTradingFrame;

        if (miniTradeParams != null) {
            frameParamsBreaker(miniTradeParams);
        }
        initPanelParams();
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        this.registerKeyboardAction(this, "ENTER", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);

        Theme.registerComponent(this);
    }

    private void frameParamsBreaker(String miniTradeParams) {
        try {
            this.selectedPortfolio = miniTradeParams.split(":")[0];
            this.selectedCustodian = miniTradeParams.split(":")[1];
            this.exchange = miniTradeParams.split(":")[2];
            this.symbol = miniTradeParams.split(":")[3];
            this.selectedside = Integer.parseInt(miniTradeParams.split(":")[4]);
            this.type = miniTradeParams.split(":")[5].charAt(0);
            this.price = miniTradeParams.split(":")[6];
            this.quantity = miniTradeParams.split(":")[7];
        } catch (NumberFormatException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private void initPanelParams() {
        populatePortfolios();
        populateCustodians();
        populateExchanges();
        addSymbol();
        addPrice();
        populateType();
//        addPrice();
        addQuantity();
        setSendButton();
        populateSide();
    }

    private synchronized void populatePortfolios() {
        try {
            getLabelPortfolio().setText(Language.getString("MINI_TRADE_PORTFOLIO"));
            cmbPortfolio = getComboPorfolio();
            if (TradingShared.getTrader() != null) {
                cmbPortfolio.removeAllItems();
                TradingPortfolioRecord record;
                ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    record = portfolios.get(i);
                    Account account = TradingShared.getTrader().findAccountByPortfolio(record.getPortfolioID());
//                    TWComboItem item = new TWComboItem(record.getPortfolioID(), record.getName() + " - " + account.getCurrency()); todo:check for accounts
                    TWComboItem item = new TWComboItem(record.getPortfolioID(), record.getName());

                    cmbPortfolio.addItem(item);
                    if ((selectedPortfolio != null) && (selectedPortfolio.equals(record.getPortfolioID()))) {
                        cmbPortfolio.setSelectedIndex(i);
                    }
//                    account = null;
                    item = null;
                    record = null;
                }
                if (selectedPortfolio == null) {
                    cmbPortfolio.setSelectedIndex(0);
                }
                selectedPortfolio = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
                cmbPortfolio.updateUI();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void populateCustodians() {
        getLabelCustodian().setText(Language.getString("MINI_TRADE_CUSTODIAN"));
        bookKeepersCombo = getComboCustodian();
        bookKeepers = new ArrayList<TWComboItem>();
        bookKeepersModel = new TWComboModel(bookKeepers);
        bookKeepersCombo.setModel(bookKeepersModel);
        TradeMethods.getSharedInstance().populateBookKeepers(selectedPortfolio, bookKeepers);
        if (bookKeepers.size() == 0) {
            bookKeepersCombo.setEnabled(false);
        } else {
            for (int i = 0; i < bookKeepers.size(); i++) {
                if ((selectedCustodian != null) && (selectedCustodian.equals(bookKeepers.get(i)))) {
                    bookKeepersCombo.setSelectedIndex(i);
                }
            }
            if (selectedCustodian == null) {
                bookKeepersCombo.setSelectedIndex(0);
            }
        }
        bookKeepersCombo.updateUI();
        if (!TradingShared.isBookKeepersAvailable()) {
            removeCustodians();
            getTabeLayout().deleteRow(0);
        }
    }

    /*  private void populateExchanges() {
        cmbExchanges = getComboExchange();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exg = (Exchange) exchanges.nextElement();
            if (exg.isDefault()) {
                TWComboItem item = new TWComboItem(exg.getSymbol(), exg.getDescription());
                cmbExchanges.addItem(item);
                if ((exchange != null) && (exchange.equals(exg.getSymbol()))) {
                    cmbExchanges.setSelectedItem(item);
                }
                item = null;
            }
        }
        if (exchange == null) {
            cmbExchanges.setSelectedIndex(0);
        }
        cmbExchanges.updateUI();
    }*/

    private void populateExchanges() {
        getLabelExsymbol().setText(Language.getString("MINI_TRADE_EXCH_SYMBOL"));
        cmbExchanges = getComboExchange();
        cmbExchanges.setEditable(false);
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exg = (Exchange) exchanges.nextElement();
            if (exg.isDefault()) {
//                TWComboItem item = new TWComboItem(exg.getSymbol(), exg.getDescription());
//                cmbExchanges.addItem(item);
                if ((exchange != null) && (exchange.equals(exg.getSymbol()))) {
//                    cmbExchanges.setSelectedItem(item);
                    cmbExchanges.setText(exg.getSymbol());
                }
//                item = null;
            }
        }
        if (exchange == null) {
//            cmbExchanges.setSelectedIndex(0);
        }
        cmbExchanges.updateUI();
    }

    private void addSymbol() {
        txtSymbol = getTextSymbol();
        txtSymbol.setText(SharedMethods.getSymbolFromKey(selectedKey));
    }

    private void populateSide() {
        getLabelSideType().setText(Language.getString("MINI_TRADE_SIDE_TYPE"));

        cmbSide = getComboSide();
        TWComboItem itembuy = new TWComboItem(TradeMeta.BUY, Language.getString("BUY"));
        cmbSide.addItem(itembuy);
        TWComboItem itemsell = new TWComboItem(TradeMeta.SELL, Language.getString("SELL"));
        cmbSide.addItem(itemsell);

        if ((selectedside != 0) && (selectedside == TradeMeta.BUY)) {
            cmbSide.setSelectedItem(itembuy);
        } else if ((selectedside != 0) && (selectedside == TradeMeta.SELL)) {
            cmbSide.setSelectedItem(itemsell);
        } else {
            cmbSide.setSelectedItem(itembuy);
        }
    }

    private void populateType() {
        cmbType = getComboType();
        orderTypesList = new ArrayList<TWComboItem>();
        TradeMethods.getSharedInstance().populateMiniTradeOrderTypes(exchange, orderTypesList, TradingShared.getTrader().getPath(selectedPortfolio));
        TWComboModel typesModel = new TWComboModel(orderTypesList);
        cmbType.setModel(typesModel);

        int size = cmbType.getModel().getSize();
        for (int i = 0; i < size; i++) {
            TWComboItem item = (TWComboItem) cmbType.getModel().getElementAt(i);
            if (item.getId().charAt(0) == type) {
                cmbType.setSelectedIndex(i);
                break;
            } else {
                cmbType.setSelectedIndex(1);
            }
        }
    }

    private void addPrice() {
        getLabelPriQty().setText(Language.getString("MINI_TRADE_QTY_PRICE"));
        txtPrice = getTextPrice();
        txtPrice.setHorizontalAlignment(SwingConstants.RIGHT);
        ValueFormatter priceFormatter = new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL);
        txtPrice.setDocument(priceFormatter);
        if (price != null) {
            txtPrice.setText(price);
        }
    }

    private void addQuantity() {
        txtQty = getTextQty();
//        txtQty =  createDoubleFieldWithDecimalsAndCustomFormatter();
        txtQty.setHorizontalAlignment(SwingConstants.RIGHT);
        ValueFormatter priceFormatter = new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL);
//        priceFormatter.addDocumentListener(this);
        txtQty.setDocument(priceFormatter);
        if (quantity != null) {
            txtQty.setText(quantity);
        }
    }

    private void setSendButton() {
        btnSend = getButtonSend();
        if (TradingShared.isReadyForTrading() && TradingShared.isTradableExchange(exchange)) {
            btnSend.setEnabled(true);
        } else {
            btnSend.setEnabled(false);
        }
    }


    public void buttonSendMouseClicked() {
        TWComboItem item = (TWComboItem) cmbSide.getSelectedItem();
        side = Integer.parseInt(item.getId());
        if (btnSend.isEnabled()) {
            sendNewOrder(side);
            if (((TWComboItem) cmbType.getSelectedItem()).getId().charAt(0) == '2') {
                txtPrice.requestFocus();
            } else {
                txtQty.requestFocus();
            }
        }
    }


    public void comboSideActionPerformed() {
        TWComboItem item = (TWComboItem) cmbSide.getSelectedItem();
        side = Integer.parseInt(item.getId());
        setColors();
    }

    private void clearData() {
        txtPrice.setText("");
        txtQty.setText("");
    }

    private void sendNewOrder(int side) {
        String portfolio = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
        if (TWControl.requestCommissionFromServer()) {
            setValuesForCommission();
        }
        if (TradingShared.isReadyForTrading()) {
            if (TradeMethods.isSymbolAllowedForProtfolio(portfolio, exchange, true)) {
                if (TradingShared.isShariaTestPassed(exchange, symbolCode, insType)) {
                    char type = orderTypesList.get(cmbType.getSelectedIndex()).getId().charAt(0);
                    if (isValidInputs(type) && validateRules(side)) {
                        boolean allOrNone = false;
                        int quantity = Integer.parseInt(txtQty.getText());

                        double price = 0;
                        try {
                            price = Double.parseDouble(txtPrice.getText());
                        } catch (Exception e) {
                            price = 0; // for market orders
                        }

                        try {
                            Rule rule = RuleManager.getSharedInstance().getRule("AON_SELECT", exchange, "NEW_ORDER");
                            if (rule != null) {
                                Interpreter interpreter = new Interpreter();
                                interpreter.set("instrumentType", insType);
                                interpreter.set("quantity", quantity);
                                interpreter.set("price", Math.round(price * 1000) / (double) 1000);
                                int result = (Integer) interpreter.eval(rule.getRule());

                                allOrNone = (result == 1); // bit 3

                            }
                        } catch (Exception evalError) {
                            evalError.printStackTrace();
                        }

                        int currencyDecimals = 2;
                        int tPlus = 2;
                        String bookKeeper = "";
                        try {
                            bookKeeper = ((TWComboItem) bookKeepersCombo.getSelectedItem()).getId();

                        } catch (Exception e) {
                            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            tPlus = 2;
                        }
                        try {
                            String currency = TradingShared.getTrader().findAccountByPortfolio(portfolio).getCurrency();
                            currencyDecimals = SharedMethods.getCurrencyDecimalPlaces(currency);
                            bookKeeper = ((TradingPortfolioRecord) TradingShared.getTrader().getPortfolio(portfolio)).getDefaultBookKeeper();
                        } catch (Exception e) {
                        }
                        calculateOrderValue();
                        calculateMarginValues();

                        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbolCode, insType);
                        boolean validationSucess = TradeMethods.ruleBasedValidationSucess(false, exchange, type, //TradeMeta.ORDER_TYPE_LIMIT,
                                price, quantity, 0, 0, stock.getMinPrice(), stock.getMaxPrice(), (short) 0, side, symbolCode, 0, stock, true);
                        if (!validationSucess) {
                            return;
                        }

//                        int i = TradeMethods.getNewOrderConfirmMsg(symbolCode, exchange, insType, price, currencyDecimals, 0, 0,
//                                Long.parseLong(txtQty.getText()), null, null, 0f, type, side, side, TradeMeta.CONDITION_TYPE_NONE, "", 0, false, tiffStr, portfolio,
//                                orderValue, commission, netValue, initMargin, maintMargin, null);
                        int tifType = TradingShared.getDefaultTifType(type);
                        String tiffStr = TradingShared.getTiffString(tifType, null);

                        if(side == TradeMeta.BUY){

                            Stock selectedStock = DataStore.getSharedInstance().getStockObject(exchange, symbol, insType);

                    String warningMessage=null;
                    if(selectedStock.getLoosingCategory() == Meta.ACCUMULATED_LOSSES_50){
                        warningMessage=Language.getString("LOOSING_STOCK_MSG_50");
                    } else if(selectedStock.getLoosingCategory() == Meta.ACCUMULATED_LOSSES_75){
                        warningMessage=Language.getString("LOOSING_STOCK_MSG_75");
                    } else if(selectedStock.getLoosingCategory() == Meta.ACCUMULATED_LOSSES_100){
                        warningMessage=Language.getString("LOOSING_STOCK_MSG_100");
                    }
                        int i;
                        if(warningMessage != null){
                            Object[] options = {
                            Language.getString("OK")};
                            i=JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                                warningMessage,
                                Language.getString("MSG_TITLE"),
                                JOptionPane.OK_OPTION,
                                JOptionPane.WARNING_MESSAGE,
                                null,
                                options,
                                options[0]);
                        if(i!=JOptionPane.OK_OPTION){
                            return;
                        }
                        }
                    }

                        Stock selectedStock = DataStore.getSharedInstance().getStockObject(exchange, symbol, insType);

                    if(TWControl.isRightsAwarenessMessageEnabled() && selectedStock.getInstrumentType() == Meta.INSTRUMENT_RIGHT && side == TradeMeta.BUY && selectedStock.getExchange().equals("TDWL") && selectedStock.getFirstSubscriptionStartDate() != null && selectedStock.getFirstSubscriptionEndDate() != null){

                        Date sysDate = new Date();
                        SimpleDateFormat ft = new SimpleDateFormat("yyyyMMdd");
                        Date fSubStartDate = null;
                        Date fSubEndDate = null;
                        Date parsedSysDate = null;

                        try {
                            fSubStartDate = ft.parse(selectedStock.getFirstSubscriptionStartDate());
                            fSubEndDate = ft.parse(selectedStock.getFirstSubscriptionEndDate());
                            String formattedSysDate = ft.format(sysDate);
                            parsedSysDate = ft.parse(formattedSysDate);
                        } catch (ParseException e) {
                            e.printStackTrace();
                        }

                        if((fSubStartDate.before(parsedSysDate) || fSubStartDate.equals(parsedSysDate)) && (fSubEndDate.after(parsedSysDate) || fSubEndDate.equals(parsedSysDate))){

                        String str= Language.getString("RIGHTS_AWARENESS_MESSAGE");
//                            if(selectedStock.getFirstSubscriptionStartDate() != null && selectedStock.getFirstSubscriptionEndDate() != null ){
                            str = str.replaceFirst("\\[TYPE\\]", Language.getString("RIGHTS_FIRST"));
                            str = str.replaceFirst("\\[COMPANY\\]", selectedStock.getShortDescription());
                            str = str.replaceFirst("\\[DATE1\\]", TradeMethods.getFormattedDate(selectedStock.getFirstSubscriptionStartDate()));
                            str = str.replaceFirst("\\[DATE2\\]", TradeMethods.getFormattedDate(selectedStock.getFirstSubscriptionEndDate()));
                            /*}

                            else if(selectedStock.getSecondSubscriptionStartDate() != null && selectedStock.getSecondSubscriptionEndDate() != null){
                            str = str.replaceFirst("\\[TYPE\\]", Language.getString("RIGHTS_SECOND"));
                            str = str.replaceFirst("\\[COMPANY\\]", selectedStock.getShortDescription());
                            str = str.replaceFirst("\\[DATE1\\]", TradeMethods.getFormattedDate(selectedStock.getSecondSubscriptionStartDate()));
                            str = str.replaceFirst("\\[DATE2\\]", TradeMethods.getFormattedDate(selectedStock.getSecondSubscriptionEndDate()));
                            }
                            else{
                            str = str.replaceFirst("\\[TYPE\\]", Language.getString("RIGHTS_FIRST")+"/"+Language.getString("RIGHTS_SECOND"));
                            str = str.replaceFirst("\\[COMPANY\\]", selectedStock.getShortDescription());
                            str = str.replaceFirst("\\[DATE1\\]", Language.getString("NA"));
                            str = str.replaceFirst("\\[DATE2\\]", Language.getString("NA"));
                            }*/
                            Object[] options = {Language.getString("OK"), Language.getString("CANCEL")};
                            int i = JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                                    str,
                                    Language.getString("MSG_TITLE"),
                                    JOptionPane.OK_OPTION,
                                    JOptionPane.WARNING_MESSAGE,
                                    null,
                                    options,
                                    options[0]);
                    if(i == JOptionPane.OK_OPTION){
                            rightsAwarenessStatus = 1;
                    }else{
                        return;
                    }
                    }
                }

                        int i = TradeMethods.getNewOrderConfirmMsg(symbolCode, exchange, insType, price, currencyDecimals, 0, 0,
                                Long.parseLong(txtQty.getText()), "", "", 0f, type, side, side, TradeMeta.CONDITION_TYPE_NONE, "", 0, false, tiffStr, portfolio,
                                orderValue, ""+commission, ""+netValue, initMargin, maintMargin, "", 0, null, false, ""+vat);

                        /*ShowMessage oMessage = new ShowMessage(TradeMethods.getConfirmMessage(symbolCode ,exchange, instrument, price, currencyDecimals, 0, 0,
                             Long.parseLong(txtQuantity.getText()),null,null,0f, type, side, side, TradeMeta.CONDITION_TYPE_NONE,"",0,false), "I", true);
                     int i = oMessage.getReturnValue();*/
                        if (i == JOptionPane.OK_OPTION) {
//                            Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbolCode, insType);
//
//                            boolean validationSucess = TradeMethods.ruleBasedValidationSucess(false, exchange, type, //TradeMeta.ORDER_TYPE_LIMIT,
//                                    price, quantity, -1, 0, stock.getMinPrice(), stock.getMaxPrice(), (short) 0, side, symbolCode, 0, stock, true);
                            if (validationSucess) {

                                boolean sucess = TradeMethods.getSharedInstance().sendNewOrder(true, false, 0, portfolio, exchange, symbolCode, side, type, price,
                                        0, "*", Long.parseLong(txtQty.getText()), tifType, 0, -1, 0, allOrNone, stock.getStrikePrice(),
                                        TradingShared.formatOpraExpireDate(stock.getExpirationDate()), stock.getInstrumentType(), -1,
                                        stock.getOptionBaseSymbol(), -1, null, null, null, -1, null, null, null, 0d, bookKeeper, 0, rightsAwarenessStatus);
                                if (sucess) { //Bug ID <#0017> inroduced the if condition to avaoid clearing if authentication failed
//                                    clear(); todo : add clearing part if need or dispose window
                                    clearData();
                                    SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "RapidOrder");
                                }
                            }
                            stock = null;
                        }
                    }
                }
            }
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }

    }

    public void setValuesForCommission() {
        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbolCode, insType);
        String comPortfolio = selectedPortfolio;
        String comSymbol = stock.getSymbol();
        String comExchange = stock.getExchange();
        char type = orderTypesList.get(cmbType.getSelectedIndex()).getId().charAt(0);
        String comOrderType = cmbType.getSelectedItem().toString();
        int comInstrumentType = insType;
        Long comQty = Long.parseLong(txtQty.getText());
        int comOrderSide = side;
        double comPrice = 0D;

        if (type == TradeMeta.ORDER_TYPE_LIMIT) {
            comPrice = getPrice();
        } else {
            if (exchange != null) {
                if (stock != null) {
                    if (side == TradeMeta.SELL)
                        comPrice = stock.getBestBidPrice();
                    else
                        comPrice = stock.getBestAskPrice();
                }
            }
        }

        if (comSymbol != null && comQty > 0 && comPrice > 0) {
            System.out.println("Commission Request Sending");
            TradeMethods.requestCommissionServer(comPortfolio, comOrderType, comPrice, comQty, comSymbol, comExchange, comInstrumentType, comOrderSide);
        }
    }

    private boolean isValidInputs(char type) {
        if (txtSymbol.getText().trim().equals("")) {
            SharedMethods.showMessage(Language.getString("INVALID_SYMBOL"), JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if ((txtPrice.getText().trim().equals("")) && ((type != TradeMeta.ORDER_TYPE_MARKET) && (type != TradeMeta.ORDER_TYPE_STOPLOSS_MARKET)
                /*&& (type != TradeMeta.ORDER_TYPE_DAY_MARKET)*/ && (type != TradeMeta.ORDER_TYPE_MARKET_ON_CLOSE)  /*&& (type != TradeMeta.ORDER_TYPE_TRAILING_STOP_MARKET)*/
                && (type != TradeMeta.ORDER_TYPE_SQUARE_OFF))) {
            SharedMethods.showMessage(Language.getString("INVALID_PRICE"), JOptionPane.ERROR_MESSAGE);
            return false;
        }
        if ((txtQty.getText().trim().equals("")) || (txtQty.getText().trim().equals("0"))) {
            SharedMethods.showMessage(Language.getString("INVALID_QUANTITY"), JOptionPane.ERROR_MESSAGE);
            return false;
        }


        return true;
    }

    private boolean validateRules(int side) {
        Hashtable<String, String> ruleTable = new Hashtable<String, String>();

        try {
            ByteArrayOutputStream out = null;
            try {
                Decompress decompress = new Decompress();
                out = decompress.setFiles("rules/" + exchange + "/qv.msf");
                decompress.decompress();
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            ruleTable.put("QUANTITY_VALIDATION", new String(out.toByteArray()));

            Interpreter interpreter = loadInterpriter(side);
            long value = (Long) interpreter.eval(ruleTable.get("QUANTITY_VALIDATION"));
            if (value < 0) {
                int result = SharedMethods.showConfirmMessage(Language.getString("MSG_POSSIBLE_INVALID_QUANTITY"),
                        JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                if (result != JOptionPane.OK_OPTION) {
                    return false;
                }
                /*new ShowMessage(Language.getString("INVALID_QUANTITY"), "E");
                return false;*/
            }
            interpreter = null;
        } catch (Exception e) {
            e.printStackTrace();
            //return false;
        }

        try {
            char type = orderTypesList.get(cmbType.getSelectedIndex()).getId().charAt(0);
            if ((type != TradeMeta.ORDER_TYPE_MARKET)) {
                Decompress decompress = new Decompress();
                ByteArrayOutputStream out = decompress.setFiles("rules/" + exchange + "/pv.msf");
                decompress.decompress();
                ruleTable.put("PRICE_VALIDATION", new String(out.toByteArray()));

                Interpreter interpreter = loadInterpriter(side);
                double value = (Double) interpreter.eval(ruleTable.get("PRICE_VALIDATION"));
//                if (value < 0) {
//                    int result = SharedMethods.showConfirmMessage(Language.getString("MSG_POSSIBLE_INVALID_PRICE"),
//                            JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
//                    if (result != JOptionPane.OK_OPTION) {
//                        return false;
//                    }
                    /*new ShowMessage(Language.getString("INVALID_PRICE"), "E");
                    return false;*/
//                }
                interpreter = null;
            }
        } catch (Exception e) { // could not eveluate the rule or rule not found
            //e.printStackTrace();
//            return false;
        }
        return true;
    }

    private Interpreter loadInterpriter(int side) throws Exception {
        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbolCode, insType);

        Interpreter interpreter = new Interpreter();
        interpreter.set("symbol", stock.getSymbolCode());
        interpreter.set("instrumentType", insType);
        interpreter.set("sector", stock.getSectorCode());
        interpreter.set("price", Double.parseDouble(txtPrice.getText()));
        interpreter.set("quantity", Integer.parseInt(txtQty.getText()));
        interpreter.set("min", stock.getMinPrice());
        interpreter.set("max", stock.getMaxPrice());
        interpreter.set("open", stock.getTodaysOpen());
        interpreter.set("high", stock.getHigh());
        interpreter.set("low", stock.getLow());
        interpreter.set("side", side);
        interpreter.set("tiff", 0);
        interpreter.set("type", orderTypesList.get(cmbType.getSelectedIndex()).getId().charAt(0));
        interpreter.set("disclosed", 0);
        interpreter.set("minfill", 0);
        interpreter.set("marketCode", stock.getMarketID());
        interpreter.set("refPrice", stock.getRefPrice());
        interpreter.set("bid", stock.getBestBidPrice());
        interpreter.set("offer", stock.getBestAskPrice());
        interpreter.set("direction", 0);

        return interpreter;
    }

    private void calculateMarginValues() {
        try {
            String portfolio = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
//            TWDecimalFormat currencyFormat = null;
            double initMargin = 0;
            boolean isDayOrder = false;
            /*try {
                Account account = TradingShared.getTrader().findAccountByPortfolio(portfolio);
                String currency = account.getCurrency();
                currencyFormat = SharedMethods.getCurrencyDecimalFormat(currency);
                initMargin = TradeMethods.getMarginForSymbol(exchange,  symbolCode, instrument,  portfolio, isDayOrder, account.getMarginLimit(), account.getDayMarginLimit());
            } catch (Exception e) {

            }*/
//            this.initMargin = currencyFormat.format(initMargin);
//            this.maintMargin = currencyFormat.format(initMargin);

            long tradeQty = Integer.parseInt(txtQty.getText());
            Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbolCode, insType);
            /*if (stock.getLotSize() >0) {
                tradeQty  = tradeQty * stock.getLotSize();
            }*/
            Account account = TradingShared.getTrader().findAccountByPortfolio(portfolio);
            String currency = account.getCurrency();
            TWDecimalFormat currencyFormat = SharedMethods.getCurrencyDecimalFormat(currency);
            FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, stock.getOptionBaseSymbol(), insType));
            double tradePrice = 0;
            long openBuyCount = 0;
            long openSellCount = 0;
            long prndingBuy = 0;
            long prndingSell = 0;
            double currencyFactor = TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(stock.getCurrencyCode());
            if (futureBaseAttributes != null) {
                tradePrice = futureBaseAttributes.getMargin();
                TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecordForFutures(SharedMethods.getKey(exchange, symbolCode, insType), portfolio);
                if (transactRecord != null) {
                    openBuyCount = transactRecord.getOpenBuyCount();
                    openSellCount = transactRecord.getOpenSellCount();
                    prndingBuy = transactRecord.getPendingBuy();
                    prndingSell = transactRecord.getPendingSell();
                }
                if (side == TradeMeta.BUY) {
                    if ((openSellCount - prndingBuy) >= tradeQty) {
                        this.initMargin = "0";
                        this.maintMargin = "0";
                    } else {
                        long marginContracts = 0;
                        if (prndingBuy >= openSellCount) {
                            marginContracts = tradeQty;
                        } else {
                            marginContracts = tradeQty - (openSellCount - prndingBuy);
                        }
                        this.initMargin = currencyFormat.format(marginContracts * tradePrice * currencyFactor);
                        this.maintMargin = currencyFormat.format(marginContracts * tradePrice * currencyFactor);
                    }
                } else {
                    if ((openBuyCount - prndingSell) >= tradeQty) {
                        this.initMargin = "0";
                        this.maintMargin = "0";
                    } else {
                        long marginContracts = 0;
                        if (prndingSell >= openBuyCount) {
                            marginContracts = tradeQty;
                        } else {
                            marginContracts = tradeQty - (openBuyCount - prndingSell);
                        }
                        this.initMargin = currencyFormat.format(marginContracts * tradePrice * currencyFactor);
                        this.maintMargin = currencyFormat.format(marginContracts * tradePrice * currencyFactor);
                    }
                }
            }

        } catch (Exception e) {
        }
    }

    private void calculateOrderValue() {
        try {


            String portfolio = ((TWComboItem) cmbPortfolio.getSelectedItem()).getId();
            long tradeQty = Integer.parseInt(txtQty.getText());
            long tradeActualQty = Integer.parseInt(txtQty.getText());
            char type = orderTypesList.get(cmbType.getSelectedIndex()).getId().charAt(0);
            double tradePrice = 0;
            TWDecimalFormat currencyFormat = null;
            Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbolCode, insType);
            if (stock.getLotSize() > 0) {
                tradeQty = tradeQty * stock.getLotSize();
            }
            try {
                String currency = TradingShared.getTrader().findAccountByPortfolio(portfolio).getCurrency();
                currencyFormat = SharedMethods.getCurrencyDecimalFormat(currency);
            } catch (Exception e) {
                currencyFormat = SharedMethods.getDecimalFormat(2);
            }

            if (type == TradeMeta.ORDER_TYPE_LIMIT) {
                tradePrice = Double.parseDouble(txtPrice.getText());
            } else {
                if (insType == Meta.INSTRUMENT_MUTUALFUND) {
                    tradePrice = Double.parseDouble(txtPrice.getText());
                } else {
                    if (exchange != null) {
                        //                        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol);
                        if (stock != null) {
                            if (side == TradeMeta.SELL)
                                tradePrice = stock.getBestBidPrice();
                            else
                                tradePrice = stock.getBestAskPrice();
                        }
                        stock = null;
                    }
                }
            }
            double value = TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(stock.getCurrencyCode());
            if (!TWControl.requestCommissionFromServer()) {
                commission = 0;
                vat = 0;
                if (side == TradeMeta.BUY) {
                    if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(SharedMethods.getKey(exchange, symbolCode, insType), "0")) {
                        commission = SharedMethods.getCommission(exchange, tradePrice, tradeActualQty, null, portfolio, SharedMethods.getKey(exchange, symbolCode, insType), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                    } else {
                        commission = SharedMethods.getCommission(exchange, tradePrice, tradeActualQty, null, portfolio, SharedMethods.getKey(exchange, symbolCode, insType), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                    }
                } else {
                    commission = SharedMethods.getCommission(exchange, tradePrice, tradeActualQty, null, portfolio, SharedMethods.getKey(exchange, symbolCode, insType), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                }

                if (stock.getInstrumentType() == Meta.INSTRUMENT_FUTURE) {
                    commission = -1;
                }
                if (commission == -1) { // if there is not commission rule, make the commission NA
                    commission = 0;
                } /*else {
                    this.commission = currencyFormat.format(commission * value / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor());//TradingShared.getCommission(tradeQty * tradePrice)));
                }*/
                vat = SharedMethods.vatAmount(commission);
            } else {
                String comPortfolio = selectedPortfolio;
                String comSymbol = stock.getSymbol();
                String comExchange = stock.getExchange();
                String comOrderType = cmbType.getSelectedItem().toString();
                int comInstrumentType = stock.getInstrumentType();
                Long comQty = Long.parseLong(txtQty.getText());
                int comOrderSide = side;
                double comPrice = 0D;

                if (type == TradeMeta.ORDER_TYPE_LIMIT) {
                    comPrice = Double.parseDouble(txtPrice.getText());
                } else {
                    if (stock.getExchange() != null) {
                        if (stock != null) {
                            if (side == TradeMeta.SELL)
                                comPrice = stock.getBestBidPrice();
                            else
                                comPrice = stock.getBestAskPrice();
                        }
                    }
                }

                System.out.println("before timer");
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException ie) {
                    ie.printStackTrace();
                }

                commission = TradeMethods.getCommission(comPortfolio, comExchange, comSymbol, comInstrumentType, comPrice, comQty);
                vat = TradeMethods.getVat(comPortfolio, comExchange, comSymbol, comInstrumentType, comPrice, comQty);
            }

            this.orderValue = currencyFormat.format((tradeQty * tradePrice * value) / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor());
            if (side == TradeMeta.SELL) {
                netValue = ((tradeQty * tradePrice) - commission - vat) * value;// TradingShared.getCommission(tradeQty * tradePrice);
            } else {
                netValue = ((tradeQty * tradePrice) + commission + vat) * value;
            }//TradingShared.getCommission(tradeQty * tradePrice);
            netValue = netValue / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor(); // adjust to base currency
//            this.netValue = currencyFormat.format(netValue);

            TradeMethods.setDataForConfirmMsg(stock.getSymbol(), exchange, insType, tradePrice, decimalCount, stopPrice, takeProfit, tradeQty,
                    field, operator, conditionValue, type, side, conditionalMode, stopLossTypeStr, trailStopPrice, isDayOrder, tiffStr, selectedPortfolio,
                    this.orderValue, "" + commission, ""+netValue, "",
                    "", null, null, isMarginUsed, "" + vat);

        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    private void setColors() {
        if (side == 1) {
            this.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
            cmbPortfolio.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
            bookKeepersCombo.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
            cmbExchanges.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
            cmbType.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
            cmbSide.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        } else if (side == 2) {
            this.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
            cmbPortfolio.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
            bookKeepersCombo.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
            cmbExchanges.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
            cmbType.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
            cmbSide.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
        }
        updateUI();
    }

    private JNumberEntryField<Double> createDoubleFieldWithDecimalsAndCustomFormatter() {
//    final JNumberEntryField<Double> formattedField = new JNumberEntryField<Double>(1000000000.0d, 14, 2);
//         txtQty= new JNumberEntryField<Double>(1000000000.0d, 14, 2);
        JNumberEntryField<Double> formattedField = new JNumberEntryField<Double>(0.0d, 14, 2);
        formattedField.setFormatter(new TextEntryFormatter() {
            public String getTextForDisplay(JTextEntryField textEntryField, String validText) {
                return NumberFormat.getNumberInstance().format(((JNumberEntryField<?>) textEntryField).getNumber());
            }
        });
//           txtQty.addFocusListener(this);
//           txtQty.addMouseListener(this);
        /* formattedField.addKeyListener(new KeyListener() {
public void keyTyped(KeyEvent e) {
    if (e.getKeyChar() == KeyEvent.VK_ENTER) {
        *//* if (e.getSource().equals(txtQty) || e.getSource().equals(txtTPrice) || e.getSource().equals(txtDisclosed) || e.getSource().equals(txtMinFill)) {
                        if (windowMode == AMEND) {
                            executeTransaction(TradeMeta.AMEND, queued, selectedQID);
                        } else if (windowMode == CANCEL) {
                            executeTransaction(TradeMeta.CANCEL, queued, selectedQID);
                        } else {
                            changeType();
                            executeTransaction(side, queued, selectedQID);
                        }
                    }*//*
                }
            }

            public void keyPressed(KeyEvent e) {
            }

            public void keyReleased(KeyEvent e) {
                try {
//                  System.out.println("222VALUE in Number =="+txtQty.getNumber());
//                   System.out.println("222VALUE in text =="+txtQty.getText());
                    if (e.getSource().equals(txtQty)) {
//                        setSpecialConditions();
                    }
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                try {
//                  System.out.println("222VALUE in formatter =="+ NumberFormat.getNumberInstance().format(txtQty.getNumber()));
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        });*/
        return formattedField;
    }

    public void textSymbolFocusGained() {
        isSymbolFocused = true;
    }

    public void textSymbolFocusLost() {
        if (isSymbolFocused && isSymbolTyped && !txtSymbol.getText().trim().equals("")) {
            validateSymbol(txtSymbol.getText());
            isSymbolTyped = false;
            isSymbolFocused = false;
        }
    }

    public void textSymbolKeyTyped() {
        isSymbolTyped = true;
    }

    private double getPrice() {
        try {
            return Double.parseDouble(txtPrice.getText());
        } catch (Exception e) {
            return 0;
        }
    }

    private void validateSymbol(String symbol) {
        if (ExchangeStore.isNoneDefaultExchangesAvailable()) {

            String requestID = "MiniTradeFrame" + "-" + parent.getViewSetting().getID() + "-" + selectedKey + ":" + System.currentTimeMillis();
            SendQFactory.addValidateRequest(symbol.toUpperCase(), requestID, null, 0);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, this);
        } else {
            Enumeration enu = ExchangeStore.getSharedInstance().getExchanges();
            Stock stk = null;
            if (enu != null) {
                while (enu.hasMoreElements()) {
                    Exchange exchange = (Exchange) enu.nextElement();
                    stk = DataStore.getSharedInstance().getStockByExchangeKey(exchange.getSymbol(), symbol);
                    if (stk != null) {
                        break;
                    }
                }
                if (stk != null) {
                    parent.resetSymbolDetails(stk.getKey());
                }

            }
        }
    }

    public void textSymbolKeyReleased() {
//        super.textSymbolKeyReleased();    //To change body of overridden methods use File | Settings | File Templates.
        btnSend.setEnabled(false);
    }

    public void comboTypeActionPerformed() {
        super.comboTypeActionPerformed();    //To change body of overridden methods use File | Settings | File Templates.
        if (cmbType.getItemCount() > 0) {
            if (((TWComboItem) cmbType.getSelectedItem()).getId().charAt(0) == TradeMeta.ORDER_TYPE_MARKET) {
                txtPrice.setText("");
                txtPrice.setEnabled(false);
            } else {
                txtPrice.setEnabled(true);
            }
        }
    }

    public void tradeServerConnected() {
        isTradeConnected = true;
        btnSend.setEnabled(true);
    }

    public void tradeSecondaryPathConnected() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void tradeServerDisconnected() {
        isTradeConnected = false;
        btnSend.setEnabled(false);
    }

    public void applyTheme() {
        setColors();
    }

    public void resetSymbolDetails(String key) {
        this.selectedKey = key;
        this.exchange = SharedMethods.getExchangeFromKey(key);
        this.symbol = SharedMethods.getSymbolFromKey(key);
        this.insType = SharedMethods.getInstrumentTypeFromKey(key);
        Stock st = DataStore.getSharedInstance().getStockObject(key);
        this.symbolCode = st.getSymbolCode();
        txtSymbol.setText(symbol);
        cmbExchanges.setText(exchange);
        btnSend.setEnabled(true);
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equalsIgnoreCase("ENTER")) {
            if (btnSend.isEnabled()) {
                sendNewOrder(side);
                if (((TWComboItem) cmbType.getSelectedItem()).getId().charAt(0) == '2') {
                    txtPrice.requestFocus();
                } else {
                    txtQty.requestFocus();
                }
            }
        }
    }

    /*  public void focusGained(FocusEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void focusLost(FocusEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }*/

    public void setFocus() {
        if (parent.isVisible()) {
            if (type == '2') {
                txtPrice.requestFocus();
            } else {
                txtQty.requestFocus();
            }
        }
    }
}
