package com.isi.csvr.trading.ui;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.dde.DDELinkInterface;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.BrokerConfig;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.datastore.OrderSearchStore;
import com.isi.csvr.trading.datastore.OrderStore;
import com.isi.csvr.trading.datastore.TPlusStore;
import com.isi.csvr.trading.datastore.Transaction;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.text.DateFormat;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.Date;

/**
 * Created by IntelliJ IDEA. User: Uditha Nagahawatta Date: May 27, 2004 Time: 2:10:12 PM
 */


public class BlotterModel extends CommonTable
        implements TableModel, CommonTableInterface, ClipboardOwner, DDELinkInterface {
    private int type;
    //    private IntTransferObject intTrasferObject;
    private BooleanTransferObject booleanTransferObject;
    private Clipboard clip;
    //    private DoubleTransferObject doubleTrasferObject;
    private static final TWDateFormat timeFormat1 = new TWDateFormat(Language.getString("BOARD_TIME_FORMAT"));
    private static final TWDateFormat timeFormat2 = new TWDateFormat(Language.getString("BOARD_DATE_FORMAT"));
    DateFormat formatters = new SimpleDateFormat("dd/MM/yyyy - HH:mm:ss");

    /**
     * Constructor
     */
    public BlotterModel(int type) {
        this.type = type;
//        intTrasferObject = new IntTransferObject();
        clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        booleanTransferObject = new BooleanTransferObject();
//        doubleTrasferObject = new DoubleTransferObject();
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public void setSymbol(String symbol) {

    }

    public int getRowCount() {
        //    if (TradingShared.isReadyForTrading()) {
        if (true) {                               /// change ! - sabbcbr
            if (type == TradeMeta.BLOTTER_CURRENT)
                return OrderStore.getSharedInstance().filteredSize();
            else if (type == TradeMeta.BLOTTER_TODAY)
                return OrderSearchStore.getSharedInstance().todaySize() + 2;
            else
                return OrderSearchStore.getSharedInstance().size();
        } else {
            return 0;
        }
    }

    public void clear() {
        OrderStore.getSharedInstance().clear();
    }

    public Object getValueAt(int iRow, int iCol) {
        Transaction transaction;
        int extraRows;

        if (iRow >= OrderSearchStore.getSharedInstance().todaySize() && type == TradeMeta.BLOTTER_TODAY) {
            if (iRow == OrderSearchStore.getSharedInstance().todaySize() + 1) {
                try {
                    switch (iCol) {
                        case -13:
                            return true;
                        case 3:
                            return " ( total )";
                        default:
                            return "";
                    }
                } catch (Exception ex) {
                }
            } else {
                return "";
            }
        }

        if (type == TradeMeta.BLOTTER_CURRENT) {
            transaction = OrderStore.getSharedInstance().getFilteredTransaction(iRow);
        } else if (type == TradeMeta.BLOTTER_TODAY) {
            transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(iRow);

        } else {//TradeMeta.BLOTTER_HISTORY
            transaction = OrderSearchStore.getSharedInstance().getTransaction(iRow);
        }


        try {
            switch (iCol) {
                case -7:
                    return "" + transaction.getSecurityType();
                case -6:
                    return booleanTransferObject.setValue(transaction.isMarkForDelivery());
                case -5:
                    return booleanTransferObject.setValue(transaction.isDayOrder());
                case -4:
                    return DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getDecimalCount();
                case -1:
                    return booleanTransferObject.setValue(transaction.getRuleType() != TradeMeta.CONDITION_TYPE_NONE);
                case 0:
                    //                return intTrasferObject.setValue(transaction.getSide());
                    return "" + (transaction.getSide());
                case 1:
                    String orderDate = formatters.format(new Date(transaction.getOrderDate()));
                    return orderDate;
//                    return "" + ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getOrderDate());
//                    return transaction.getOrderDate();
                case 2:
                    return "" + transaction.getSequenceNumber();
                case 3:
                    return transaction.getClOrderID();
                case 4:
//                    return transaction.getExchange();
                    return ExchangeStore.getSharedInstance().getExchange(transaction.getExchange().trim()).getDisplayExchange(); //Display Exchange
                case 5:
                    return TradingShared.getDisplaySmbol(transaction.getSymbol(), transaction.getPortfolioNo());
                case 6:
                    return TradingShared.getOMSStatusString(transaction.getStatus(), true);
                case 7:
                    if (transaction.getAvgPrice() == 0)
                        return "" + Double.POSITIVE_INFINITY;
                    else
                        return "" + transaction.getAvgPrice();
                case 8:
                    return TradingShared.getActionString(transaction.getSide());
                case 9:
                    return TradingShared.getTypeString(transaction.getType());
                case 10:
                    if (transaction.getPrice() == 0)
                        return "" + Double.POSITIVE_INFINITY;
                    else
                        return "" + transaction.getPrice();
                case 11:
                    if (Language.getString("ORDER_STAUS_PARTIALLY_FILLED").equals(TradingShared.getOMSStatusString(transaction.getStatus(), true))) {
                        return "" + new DecimalFormat("#.##").format(transaction.getCumVat());
                    } else {
                        return "" + new DecimalFormat("#.##").format(transaction.getVat());
                    }
                case 12:
                    return "" + transaction.getOrderQuantity();
                case 13:
                    return "" + transaction.getFilledQuantity();
                case 14:
                    return TradingShared.getTiffString(transaction.getTIFType(), transaction.getExpireDate());
                case 15:
                    if (type == TradeMeta.BLOTTER_CURRENT)
                        return "" + transaction.getCum_commission();
                    else
                        return "" + transaction.getCommission();
                case 16:
                    return transaction.getRejectReason();
                case 17:
                    return DataStore.getSharedInstance().getCompanyName(SharedMethods.getKey(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()));
                case 18:
                    return transaction.getMubasherOrderNumber();
                case 19:
                    return "" + transaction.getStopLossPrice();
                case 20:
                    return "" + transaction.getPendingQuantity();
                case 21:
                    try {
                        if (transaction.getTransactTime() <= Constants.ONE_DAY) {
                            return "0";
                        } else {
                            return "" + (ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getTransactTime()));   //TradingShared.formatLongTimeToTW
                        }
                    } catch (Exception e) {
                        return "0";
                    }
                case 22:
                    return "" + transaction.getTakeProfitPrice();
                case 23:
                    try {
                        return DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getShortDescription();
                    } catch (Exception e) {
                        return transaction.getSymbol();
                    }
                case 24:
                    try {
                        return DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getCompanyCode();
                    } catch (Exception e) {
                        return transaction.getSymbol();
                    }
                case 25:
                    return "" + 1;
                case 26:
                    return "" + 2;
                case 27:
                    return "" + TPlusStore.getSharedInstance().getBookKeeper(transaction.getBookKeeper()).getName();
                case 28:
                    return "" + transaction.getOrdValue();
                case 29:
                    return "" + transaction.getOrdNetValue();
                default:
                    return "0";
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        if (type == TradeMeta.BLOTTER_TODAY) {

            return String.class;

        } else {
            switch (super.getViewSettings().getRendererID(iCol)) {
//            case 0:
                case 1:
                case 2:
                case 21:
                case 23:
                case 24:
                case 65:
                case 68:
                case 'B':
                    return String.class;
                case 'P':
                case 'Q':
                    return Number[].class;
                case 0:
                case 3:
                case 5:
                case 6:
                case 4:
                case 7:
                case 8:
                case 10:
                case 11:
                case 19:
                case 'M':
                case 'S':
                case 22:
                case 25:
                case 26:
                case 28:
                case 29:
                    return Number.class;
//            case 0:
                default:
                    return Object.class;
            }
        }
    }

    public boolean isCellEditable(int row, int col) {
//    	switch (col){
//            case 3:
//            case 5:
//                return true;
//            default:
        return false;
//        }
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {

    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        customizerRecords[0] = new CustomizerRecord(Language.getString("TABLE_HEADER"), FIELD_HEADER_COLOR_ROW, null, Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));     //Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR")
        customizerRecords[1] = new CustomizerRecord(Language.getString("SELECTED_CELLS"), FIELD_SELECTED_ROW, Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR"), Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR"));
        customizerRecords[2] = new CustomizerRecord(Language.getString("BUY_COLOR"), FIELD_BID_ROW1, Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"), Theme.getColor("BOARD_TABLE_CELL_BID_FGCOLOR1"));
        customizerRecords[3] = new CustomizerRecord(Language.getString("SELL_COLOR"), FIELD_ASK_ROW2, Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"), Theme.getColor("BOARD_TABLE_CELL_ASK_FGCOLOR1"));
        customizerRecords[4] = new CustomizerRecord(Language.getString("CONDITIONAL_BUY_COLOR"), FIELD_CONDITIONAL_BUY, Theme.getColor("BOARD_TABLE_CONDITIONAL_BUY_BGCOLOR"), Theme.getColor("BOARD_TABLE_CONDITIONAL_BUY_FGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("CONDITIONAL_SELL_COLOR"), FIELD_CONDITIONAL_SELL, Theme.getColor("BOARD_TABLE_CONDITIONAL_SELL_BGCOLOR"), Theme.getColor("BOARD_TABLE_CONDITIONAL_SELL_FGCOLOR"));
        return customizerRecords;
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    /*public void copyCells(Table table, boolean copyHead) {
        int col;
        int cols;
        int row = table.getTable().getSelectedRow();
        int rows = table.getTable().getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
        if (copyHead)
            buffer.append(copyHeaders(table));

        int modelIndex;
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                modelIndex = table.getTable().convertColumnIndexToModel(col + c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    try {
                        Transaction transaction;
                        if (type == TradeMeta.BLOTTER_CURRENT) {
                            transaction = OrderStore.getSharedInstance().getFilteredTransaction(row + r);
                        } else if (type == TradeMeta.BLOTTER_TODAY) {
                            transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(row + r);
                        } else {
                            transaction = OrderSearchStore.getSharedInstance().getTransaction(row + r);
                        }
                        buffer.append(getValueAt(transaction, modelIndex));
                    } catch (Exception e) {
//                        e.printStackTrace();
                        buffer.append("");
                    }
                    if (c != (cols - 1))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
    }*/

    public void getDDEString(Table table, boolean withHeadings) {
        //To change body of implemented methods use File | Settings | File Templates.
        int col;
        int cols;
        int row = table.getTable().getSelectedRow();
        int rows = table.getTable().getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
        if (withHeadings)
            buffer.append(copyHeaders(table));

        int modelIndex;
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                modelIndex = table.getTable().convertColumnIndexToModel(col + c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    try {
                        Transaction transaction;
                        if (type == TradeMeta.BLOTTER_CURRENT) {
                            transaction = OrderStore.getSharedInstance().getFilteredTransaction(row + r);
                        } else if (type == TradeMeta.BLOTTER_TODAY) {
                            transaction = OrderSearchStore.getSharedInstance().getTodayTransaction(row + r);
                        } else {
                            transaction = OrderSearchStore.getSharedInstance().getTransaction(row + r);
                        }
                        buffer.append(getValueAt(transaction, modelIndex));
                    } catch (Exception e) {
//                        e.printStackTrace();
                        buffer.append("");
                    }
                    if (c != (cols - 1))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
    }

    public String copyHeaders(Table table) {
        int col;
        int cols;
        //int row = this.getSelectedRow();
        //int rows= this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (table.getTable().getCellSelectionEnabled()) {
            col = table.getTable().getSelectedColumn();
            cols = table.getTable().getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
        int modelIndex;
        for (int c = 0; c < cols; c++) {
            modelIndex = table.getTable().convertColumnIndexToModel(col + c);
            if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                buffer.append((String) table.getTable().getColumn("" + modelIndex).getHeaderValue());
                if (c != (cols - 1))  // do not append the tab char to the last item of the row
                    buffer.append("\t");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public String getValueAt(Transaction transaction, int iCol) {


        switch (iCol) {
            case -7:
                return "" + transaction.getSecurityType();
            case -6:
                return "" + booleanTransferObject.setValue(transaction.isMarkForDelivery());
            case -5:
                return "" + booleanTransferObject.setValue(transaction.isDayOrder());
            case -4:
                return "" + DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getDecimalCount();
            case -1:
                return "" + booleanTransferObject.setValue(transaction.getRuleType() != TradeMeta.CONDITION_TYPE_NONE);
            case 0:
//                return intTrasferObject.setValue(transaction.getSide());
                return "" + (transaction.getSide());
            case 1:
//                return "" + ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getOrderDate());
                /*if (transaction.getOrderDate() > 86400000) {
                    return "" + timeFormat2.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getOrderDate()))) + " - " + timeFormat1.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getOrderDate())));
                } else {
                    return "";
                }*/
//                return transaction.getOrderDate();
                String orderDate = formatters.format(new Date(transaction.getOrderDate()));
                return orderDate;
            case 2:
                return "" + transaction.getSequenceNumber();
            case 3:
                return transaction.getClOrderID();
            case 4:
                return transaction.getExchange();
            case 5:
                return transaction.getSymbol();
            case 6:
                return TradingShared.getOMSStatusString(transaction.getStatus(), true);
            case 7:
                if (transaction.getAvgPrice() == 0)
                    return Language.getString("NA");
                else
                    return "" + transaction.getAvgPrice();
            case 8:
                return TradingShared.getActionString(transaction.getSide());
            case 9:
                return TradingShared.getTypeString(transaction.getType());
            case 10:
                if (transaction.getPrice() == 0)
                    return Language.getString("NA");
                else
                    return "" + transaction.getPrice();
            case 11:
                if (Language.getString("ORDER_STAUS_PARTIALLY_FILLED").equals(TradingShared.getOMSStatusString(transaction.getStatus(), true))) {
                    return "" + new DecimalFormat("#.##").format(transaction.getCumVat());
                } else {
                    return "" + new DecimalFormat("#.##").format(transaction.getVat());
                }
            case 12:
                return "" + transaction.getOrderQuantity();
            case 13:
                return "" + transaction.getFilledQuantity();
            case 14:
                return TradingShared.getTiffString(transaction.getTIFType(), transaction.getExpireDate());
            case 15:
                if (type == TradeMeta.BLOTTER_CURRENT)
                    return "" + transaction.getCum_commission();
                else
                    return "" + transaction.getCommission();
            case 16:
                return transaction.getRejectReason();
            case 17:
                return DataStore.getSharedInstance().getCompanyName(SharedMethods.getKey(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()));
            case 18:
                return transaction.getMubasherOrderNumber();
            case 19:
                return "" + transaction.getStopLossPrice();
            case 20:
                return "" + transaction.getPendingQuantity();
            case 21:
                try {
                    if (transaction.getTransactTime() <= Constants.ONE_DAY) {
                        return "" + timeFormat2.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getTransactTime()))) + " - " + timeFormat1.format(new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getTransactTime())));
                    } else {
                        return "";
                    }
                } catch (Exception e) {
                    return "0";
                }
            case 22:
                return "" + transaction.getTakeProfitPrice();
            case 23:
                try {
                    return DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getShortDescription();
                } catch (Exception e) {
                    return transaction.getSymbol();
                }
            case 24:
                try {
                    return DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getCompanyCode();
                } catch (Exception e) {
                    return transaction.getSymbol();
                }
            case 25:
                boolean amendStatus = BrokerConfig.getSharedInstance().isOrderAmendAllowedByBroker(TradingShared.getTrader().getPath(transaction.getPortfolioNo()), transaction.getOrderCategory() + "");
                if (amendStatus) {
                    amendStatus = (TradeMethods.isValidForAmend(transaction.getType()));
                }
                if (amendStatus) {
                    amendStatus = TradeMethods.getSharedInstance().getItemValidity("AMEND", "ORDER_STATUS_WINDOW", transaction.getExchange(), (char) transaction.getStatus());
                }
                if (amendStatus) {
                    return "Y";
                } else {
                    return "N";
                }
            case 26:
                boolean cancelstatus = TradeMethods.isValidForCancel(transaction.getType());
                if (cancelstatus) {
                    cancelstatus = TradeMethods.getSharedInstance().getItemValidity("CANCEL", "ORDER_STATUS_WINDOW", transaction.getExchange(), (char) transaction.getStatus());
                }
                if (cancelstatus) {
                    return "Y";
                } else {
                    return "N";
                }
            case 27:
                return "" + TPlusStore.getSharedInstance().getBookKeeper(transaction.getBookKeeper()).getName();
            case 28:
                return "" + transaction.getOrdValue();
            case 29:
                return "" + transaction.getOrdNetValue();
            default:
                return "0";
        }
    }


}


