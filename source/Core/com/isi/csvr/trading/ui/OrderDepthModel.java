package com.isi.csvr.trading.ui;

import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.IntTransferObject;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.datastore.OrderDepthStore;
import com.isi.csvr.trading.datastore.Transaction;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.datastore.ExchangeStore;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.text.DecimalFormat;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 31, 2004
 * Time: 3:14:16 PM
 */

public class OrderDepthModel extends CommonTable
        implements TableModel, CommonTableInterface {

    IntTransferObject intTrasferObject;
    private String orderID;

    /**
     * Constructor
     */
    public OrderDepthModel(String orderID) {
        this.orderID = orderID;
        init();
    }

    private void init() {
        intTrasferObject = new IntTransferObject();
    }

    /* --- Table Modal's metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public void setSymbol(String symbol) {

    }

    public int getRowCount() {
        try {
            return OrderDepthStore.getSharedInstance().getTransactions("" + orderID).size();
        } catch (Exception e) {
            return 0;
        }
    }

    public Object getValueAt(int iRow, int iCol) {
        DynamicArray list = OrderDepthStore.getSharedInstance().getTransactions("" + orderID);
        Transaction transaction = (Transaction) list.get(iRow);

        switch (iCol) {
            case 0:
//                return transaction.getTransactTime(); // Bug ID <#0028>
                return "" + ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(transaction.getExchange(), transaction.getTransactTime());
            case 1:
                return "" + transaction.getTransactionID();
            case 2:
                return TradingShared.getOMSStatusString(transaction.getStatus(), true);
            case 3:
                if (transaction.getLastPrice() == 0)
                    return "" + Double.POSITIVE_INFINITY;
                else
                    return "" + transaction.getLastPrice();
            case 4:
                return "" + transaction.getLastQuantity();
            case 5:
                return "" + transaction.getCommission();
            case 6:
                return "" + new DecimalFormat("#.##").format(transaction.getVat());
            default:
                return "0";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return Object.class;
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }


}



