package com.isi.csvr.trading;

import com.dfn.mtr.mix.beans.*;
import com.dfn.mtr.mix.beans.TransferObject;
import com.dfn.mtr.mix.common.MIXConstants;
import com.isi.csvr.Client;
import com.isi.csvr.Login;
import com.isi.csvr.LoginWindow;
import com.isi.csvr.WorkInProgressIndicator;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.shared.*;
import com.isi.csvr.trading.api.TradeServer;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.*;
import com.isi.csvr.trading.datastore.Rule;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.positionMoniter.PositionDataStore;
import com.isi.csvr.trading.shared.*;

import javax.net.ssl.*;
import javax.swing.*;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.security.KeyStore;
import java.util.ArrayList;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class
        AutoTradeLoginDaemon extends Thread {


    public static final int CONNECTING = 0;
    public static final int STOPPED = 1;
    public static final int CONNECTED = 2;
    public static final int IDLE = 3;
    public static final int DISCONNECTED = 4;
    public static final int OTP_REQUIRED = 5;

    private Socket socket = null;
    private String user;
    private String pass;
    private static boolean manualConnectMode = true;
    private OutputStream out;
    private static int connectionStatus = IDLE;
    private IP currentIP;
    private IP previousIP;
    private ReceiveQueue receiveQueue;
    private TradePulseGenerator pulseGenerator = null;
    private boolean paswordChangeRequest = false;
    private FrameAnalyser frameAnalyser;
    private SecondaryLoginDeamon secondaryLogin = null;
    private WorkInProgressIndicator applyOfflineIndicator;

    public AutoTradeLoginDaemon() {
        super("AutoTradeLoginDaemon");
        start();
    }

    public void run() {
        while (true) {
            try {
                if (isReadyToConnect()) {
                    if ((TradingShared.isOkForTradeConnection()) && (!TradingShared.isConnected())) {
                        setMode(CONNECTING);
                        System.err.println("reconnecting trading channel................");
                        sleepThread(500); // just in case, another requesr comes in
                        connect();
                    }
                }
                sleepThread(1000);
            } catch (Exception e) {
                e.printStackTrace();
                sleepThread(1000);
            }
        }
    }

    private boolean isReadyToConnect() {
        if (getMode() == IDLE) {
            return true;
        } else if ((getMode() == DISCONNECTED) && (Settings.isConnected())) {
            Client.removeLoyaltyPoints();
            int result = SharedMethods.showConfirmMessage(Language.getString("MSG_TRADING_DISCONNECTED"), JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
            if (result == JOptionPane.YES_OPTION) {
                TradingShared.setLevel1Password(null);
                setMode(IDLE);
                IPSettings.getSharedInstance().resetIPIndex();
                return true;
            } else {
                TradingShared.setLevel1Password(null);
                setMode(STOPPED);
                Client.getInstance().getMenus().unlockTradeServerConnection();
                IPSettings.getSharedInstance().resetIPIndex();
                return false;
            }
        } else {
            return false;
        }
    }

    public void setAuthenticationStatus(MIXObject resp) {
        AuthenticationResponse mixAuthResponse = (AuthenticationResponse) resp.getTransferObjects()[0];
        int status = mixAuthResponse.getAuthenticationStatus();
        try {
            switch (status) {
                case TradeMeta.AUTH_ERROR:
                    setDisconnected(mixAuthResponse.getRejectReason());
                    IPSettings.getSharedInstance().resetIPIndex();
                    setMode(IDLE);
                    break;
                case TradeMeta.AUTH_LOCKED:
                    setMode(DISCONNECTED);
                    setDisconnected(mixAuthResponse.getRejectReason());
                    IPSettings.getSharedInstance().resetIPIndex();
                    break;
                case TradeMeta.AUTH_OTP_EXPIRED:
                    setMode(DISCONNECTED);
                    setDisconnected(mixAuthResponse.getRejectReason());
                    setMode(IDLE);
                    break;
                case TradeMeta.AUTH_OTP_INVALID:
                    showInvalidAuthMessage(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(mixAuthResponse.getRejectReason())));
                    TradeMethods.getSharedInstance().otpPane();
                    break;
                case TradeMeta.AUTH_OK:
                case TradeMeta.AUTH_CHANGE_PW:
                    setMode(CONNECTED);
                    Client.getInstance().setTitleText();

                    Settings.setBurstReuestMode(false);
                    processMixAuthenticationResponse(mixAuthResponse, Constants.PATH_PRIMARY, resp.getMIXHeader());
                    System.out.println("result " + status);
                    MixStore.getSharedInstance().setPrimaryAuthenticationObj(resp);

                    if (secondaryLogin != null) {
                        secondaryLogin.setFrameAnalyser(frameAnalyser);
                        secondaryLogin.start();
                    }
                    System.out.println("============ Trade reply " + resp);
                    initializeApp(); //todo  removed for testing
                    break;
                case TradeMeta.AUTH_OTP_REQUIRED:
                    if (mixAuthResponse.getMobileNumber() == null || mixAuthResponse.getMobileNumber() == "") {
                        setDisconnected(Language.getString("NO_MOBILE_NUMBER"));
                    } else {
                        setMode(OTP_REQUIRED);
                        processMixAuthenticationResponse(mixAuthResponse, Constants.PATH_PRIMARY, resp.getMIXHeader());
                        MixStore.getSharedInstance().setPrimaryAuthenticationObj(resp);
                        TradeMethods.getSharedInstance().otpPane();
                    }
                    break;
                default:
                    setMode(DISCONNECTED);
                    setDisconnected(mixAuthResponse.getRejectReason());
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            setMode(DISCONNECTED);
            Client.getInstance().setTitleText();

            closeSocket();
            closeSecondarySocket();
        }
    }

    public void setDisconnected(String message) {
        Client.getInstance().setTitleText();
        if (message != null) {
            showInvalidAuthMessage(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(message)));
        }
        closeSocket();
        closeSecondarySocket();
        try {
            frameAnalyser.interrupt();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            pulseGenerator.interrupt();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        frameAnalyser = null;
        pulseGenerator = null;
        try {
            receiveQueue.interrupt();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            SendQueue.getSharedInstance().interrupt();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    private synchronized void connect() {
        try {
            String ip;
            while (true) {
                try {
                    if (getUserInfo(IPSettings.getSharedInstance().getNextIpIndex(), IPSettings.getSharedInstance().getIPCount())) {
                        ip = getNextIP();
                        if (previousIP != null && previousIP.getIP().equalsIgnoreCase(ip)) {
                            continue;                              //added by udaya to stop connecting to the same ip twice
                        }
                        openSocket(ip);
                        authenticate();
                        break;
                    } else {
                        Client.getInstance().setTradeSessionDisconnected(true);
                        if (TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                            Client.getInstance().getMenus().unlockTradeServerConnection();
                        }
                        break;
                    }

                } catch (SSLHandshakeException ssle) {
                    ssle.printStackTrace();
                    if (ssle.getCause() instanceof java.security.cert.CertificateExpiredException) {
                        showSSLExpiration(ssle);
                    }
                } catch (Exception ex1) {
                    ex1.printStackTrace();
                }
                sleepThread(5000);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            sleepThread(5000);
            // no ips found exception
            setMode(DISCONNECTED);
        }
    }

    private void showSSLExpiration(SSLHandshakeException ssle) {
        String message = Language.getString("MSG_SSL_EXPIRED");
        message = message.replaceFirst("\\[DATE\\]", ssle.getCause().getLocalizedMessage());
        SharedMethods.showMessage(message, JOptionPane.ERROR_MESSAGE);
    }

    private String getNextIP() throws Exception {

        currentIP = IPSettings.getSharedInstance().getNextIP();
        String newip = currentIP.getIP();
        return newip;
    }

    private void openSocket(String ip) throws Exception {

        try {
//            ip = "192.168.13.67";
//            ip = "192.168.13.14";
//            ip = "10.1.200.129";
            System.out.println("Connecting to Trading ip " + ip);
//            Socket socket;
            SSLSocketFactory factory = null;
            SSLContext ctx = SSLContext.getInstance("TLS");
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            KeyStore ks = KeyStore.getInstance("JKS");
                char[] passphrase;
                char[] passphrase2;
            if(TWControl.isDefaultKeyStorePassword()){
                passphrase = "12345678".toCharArray();
                passphrase2 = "12345678".toCharArray();

            }else{
                passphrase = "mystorepass".toCharArray();
                passphrase2 = "mykeypass".toCharArray();
            }
            ks.load(new FileInputStream(Settings.CONFIG_DATA_PATH + "/trs.dll"), passphrase);
            kmf.init(ks, passphrase2);
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(ks);
            ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            factory = ctx.getSocketFactory();
//            socket = factory.createSocket(ip, 9997);

            socket = factory.createSocket(ip, Settings.TRADE_ROUTER_PORT);
//            return socket;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private boolean getUserInfo(int loopIndex, int ipCount) {
        if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web)) {
            if (!Settings.getTradeToken().equals("")) {
                user = Settings.getTradeToken();
            } else {
                user = Settings.getSingleSignOnID();
            }
            pass = Settings.getInstitutionID();
            TradingShared.setLevel1Password(pass);
            TradingShared.setTradeUserName(user);
            Settings.setItem("TRADE_USER", "");
            setMode(CONNECTING);
            Client.getInstance().setTitleText();
            return true;
        } else {
            if (loopIndex == 0) {
                LoginWindow loginDialog = new LoginWindow(Client.getInstance().getFrame(), Login.MODE_TRADING_SERVERS);
                previousIP = null;
                currentIP = null;
                while (true) {
                    if (loginDialog.showDialog()) {
                        user = loginDialog.getUserName().trim();
                        pass = loginDialog.getPassword().trim();
                        TradingShared.setLevel1Password(pass);
                        TradingShared.setTradeUserName(user);
                        if ((Settings.getItem("IS_TRADE_USER_SAVED") != null) && (Settings.getItem("IS_TRADE_USER_SAVED").equals("true"))) { //Bug Id <#0034> included null check 8.20.012
                            Settings.setItem("TRADE_USER", user);
                        } else {
                            Settings.setItem("TRADE_USER", "");
                        }
                        loginDialog.dispose();
                        TradingShared.setChangePassowrdNeeded(false);
                        TradingShared.setChangePWDType(TradingConstants.CHANGE_PASSWORD_TYPE_NONE);
                        setMode(CONNECTING);
                        Client.getInstance().setTitleText();
                        return true;
                    } else {
                        pass = null;
                        TradingShared.setLevel1Password(null);
                        setMode(IDLE);
                        Client.getInstance().setTitleText();

                        if (TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
                            if (!Settings.isConnectedFirstTime && !Settings.isOfflineDataLoaded) {
                                applyOfflineIndicator = new WorkInProgressIndicator(WorkInProgressIndicator.APPLYING_OFFLINE_DATA);
                                applyOfflineIndicator.setVisible(true);
                                Settings.setOfflineMode(true);
                                System.out.println("start of work in indicator");
                                new Thread("Offline Data Thread") {
                                    public void run() {
                                        Application.getInstance().fireLoadOfflineData();
                                        if (applyOfflineIndicator != null) {
                                            applyOfflineIndicator.dispose();
                                        }
                                        Settings.isOfflineDataLoaded = true;
                                    }
                                }.start();
                            }
                        }

                        return false; // login cancelled by user
                    }
                }
            } else {
                return true;
            }
        }
        //return true;
    }

    private void authenticate() throws Exception {
        MIXObject mixBasicObject = TradeMethods.getSharedInstance().authenticateRequestGenerator(user, pass, "", true);
        out = socket.getOutputStream();
        receiveQueue = new ReceiveQueue(socket, Constants.PATH_PRIMARY);
        SendQueue.getSharedInstance().init(socket, out, Constants.PATH_PRIMARY);

        if (frameAnalyser == null) {
            frameAnalyser = new FrameAnalyser();
        }
        receiveQueue.setReceiveBuffer(frameAnalyser.getList());
        receiveQueue.start();
        System.out.println("Trading  request in Primary->" + mixBasicObject.getMIXString());
        out.write(mixBasicObject.getMIXString().getBytes());
        if (pulseGenerator == null) {
            pulseGenerator = new TradePulseGenerator(Constants.PATH_PRIMARY);
            pulseGenerator.start();
        }
    }

    public String readLine(InputStream in) throws Exception {
        int iValue;
        StringBuffer buffer = new StringBuffer();
        try {
            while (true) {
                iValue = in.read();
                if (iValue == -1)
                    throw new Exception("End of Stream");
                if (iValue != '\n') {
                    buffer.append((char) iValue);
                    continue;
                } else {
                    return buffer.toString();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }
    }


    private void initializeApp() throws Exception {
        BrokerConfig.getSharedInstance().loadData(TradingShared.getTrader().getBrokerID(Constants.PATH_PRIMARY), Constants.PATH_PRIMARY);
        TradingShared.setDisconnectionMode(TradingConstants.DISCONNECTION_UNEXPECTED); // clear any maual disconnection  requests
        TradingShared.getTrader().createAccounts();
        TradePortfolios.getInstance().clear();
        TradingShared.resetMessageID();
        OrderStore.getSharedInstance().clear();
        OpenPositionsStore.getSharedInstance().clear();
        CurrencyStore.getSharedInstance().clearStore();
        sendInitialRequests();

        PositionDataStore.getSharedInstance().clear();
        TradingShared.getTrader().addAccountListener(Client.getInstance().getTradingPortfolio());
        TradingShared.getTrader().addAccountListener(Client.getInstance().getRapidOrderPanel());
        TradingShared.getTrader().addAccountListener(TradeMethods.getSharedInstance().getOrderSearchWindow());
        TradingShared.getTrader().addAccountListener(TradeMethods.getSharedInstance().getCashLogSearchWindow());
        TradingShared.getTrader().addAccountListener(TradeMethods.getSharedInstance().getAccountWindow());
//        TradingShared.getTrader().addAccountListener(TradeMethods.getSharedInstance().getNewPFCreationWindow());

        if (TradeMethods.getSharedInstance().getBlotterFrame() != null)
            TradingShared.getTrader().addAccountListener(TradeMethods.getSharedInstance().getBlotterFrame());
        if (TradeMethods.getSharedInstance().getStockTransferFrame() != null)
            TradingShared.getTrader().addAccountListener(TradeMethods.getSharedInstance().getStockTransferFrame());
        if (TradeMethods.getSharedInstance().getFundTransferWindow() != null) {
            TradingShared.getTrader().addAccountListener(TradeMethods.getSharedInstance().getFundTransferWindow());
        }
        // TradingShared.getTrader().addAccountListener(IntraDayPositionMoniterUI.getSharedInstance());
        TradingShared.getTrader().notifyAccountListeners(null);
        TradingShared.getTrader().notifyPortfolioListeners(null);
        OrderDepthStore.getSharedInstance().clear();

        try {
            Rule amendMode = RuleManager.getSharedInstance().getRule("AMEND_MODE_DELTA", "*", "NEW_ORDER");
            if ((amendMode.getRule().equals("1") || amendMode.getRule().equals("true"))) {
                TradingShared.setAmendMode(TradingShared.AMEND_MODE_DELTA);
            } else {
                TradingShared.setAmendMode(TradingShared.AMEND_MODE_FULL);
            }
        } catch (Exception e) {
            TradingShared.setAmendMode(TradingShared.AMEND_MODE_FULL);
        }

        if (TradingShared.getChangePWDType() == TradingConstants.CHANGE_PASSWORD_TYPE_VALIDATION) {
//        if(TradingShared.isUserDetailsNeeded()){
            TradeMessage message = new TradeMessage(TradeMeta.MT_AUTH_VALIDITY);
            message.addData("");
            //  frameAnalyser.getList().add(message.toString());
            message = null;
        } else if (paswordChangeRequest) {
            TradeMessage message = new TradeMessage(TradeMeta.MT_CHANGE_PW);
            message.addData("");
            //    frameAnalyser.getList().add(message.toString());
            message = null;

            MIXObject changePasswordReq = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_AUTHENTICATION, MIXConstants.RESPONSE_TYPE_CHANG_PWD, Constants.PATH_PRIMARY);
            ChangePasswordResponse resp = new ChangePasswordResponse();
            resp.setUserID("N/A");
            changePasswordReq.setTransferObjects(new TransferObject[]{resp});
            frameAnalyser.getList().add(changePasswordReq);
        }
        FundTransferStore.getSharedInstance().clear();
//        TradeMethods.getSharedInstance().pendingFundTransfer(Constants.PATH_PRIMARY);
        StockTransferObjectStore.getSharedInstance().clear();
        //TradeMethods.getSharedInstance().pendingStockTransfer();
        TradingConnectionNotifier.getInstance().fireTWConnected();
//        OMSMessageList.getSharedInstance().requestOMSMessages();
        Client.getInstance().tradingSessionEnabled();
        if (TradingShared.isTradingAPIEnabled()) {
            TradeServer.start();
        }
    }

    private void initMessageQueues() throws Exception {
        // receiveQueue = new ReceiveQueue(socket, Constants.PATH_PRIMARY);
        SendQueue.getSharedInstance().init(socket, out, Constants.PATH_PRIMARY);
        // frameAnalyser = new FrameAnalyser();
        // receiveQueue.setReceiveBuffer(frameAnalyser.getList());
        //  receiveQueue.start();
        pulseGenerator = new TradePulseGenerator(Constants.PATH_PRIMARY);
        pulseGenerator.start();
    }

    private void sendInitialRequests() {
        try {
            if (Settings.isBurstRequestMode()) {
                TradeMethods.requestUserDataPrimary(Constants.PATH_PRIMARY, true);
            } else {
                //  TradeMethods.requestUserDataPrimary(Constants.PATH_PRIMARY, false);  //todo removed for testing
                TradeMethods.requestPortfolioData(Constants.PATH_PRIMARY);        //todo removed for testing
                TradeMethods.requestAccountData(Constants.PATH_PRIMARY);
                TradeMethods.requestCurrencyData(Constants.PATH_PRIMARY);
//                TradeMethods.requestTPlusData(Constants.PATH_PRIMARY);
//                TradeMethods.requestTPlusCommisionData(Constants.PATH_PRIMARY);


            }
            TradeMethods.requestBankAccountDetails(Constants.PATH_PRIMARY);
            TradeMethods.getTiFFTypesFromServer(Constants.PATH_PRIMARY);
            TradeMethods.reqExchangeLvlConfig(Constants.PATH_PRIMARY);
            TradingShared.setReadyForTrading(true);  // todo  added for testing remove

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean processMixAuthenticationResponse(AuthenticationResponse mixAuthResponse, byte loginType, MIXHeader header) {
        try {
            int authStatus = mixAuthResponse.getAuthenticationStatus();
            //   int authStatus = 1;
            Trader trader = null;
            MixStore.getSharedInstance().clear(loginType);

            /*======= Authentication General ============*/
            if (loginType == Constants.PATH_PRIMARY) {
                RuleManager.getSharedInstance().clear(); // clear all rules and get ready for new data
                TradingShared.setTrsID(header.getTrsID());    //todo check TRSID
            }

            /*if (loginType == Constants.PATH_PRIMARY && (authStatus == TradeMeta.AUTH_ERROR || authStatus == TradeMeta.AUTH_LOCKED || authStatus == TradeMeta.AUTH_OTP_EXPIRED)) {
                showInvalidAuthMessage(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(mixAuthResponse.getRejectReason())));
                IPSettings.getSharedInstance().resetIPIndex();
                return false;
            }

            if (loginType == Constants.PATH_PRIMARY && (authStatus == TradeMeta.AUTH_OTP_INVALID)) {
                showInvalidAuthMessage(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(mixAuthResponse.getRejectReason())));
                TradeMethods.getSharedInstance().otpPane(mixAuthResponse.getMobileNumber(), mixAuthResponse.getOneTimePasswordFlag(), mixAuthResponse.getUserID(), mixAuthResponse.getLoginName());
            }*/

            if (loginType == Constants.PATH_SECONDARY) {
                trader = TradingShared.getTrader();
                trader.setUserID(mixAuthResponse.getUserID());
                trader.setMubasherID(secondaryLogin.getMubasherID());
                trader.setPath(Constants.PATH_SECONDARY);
                //Settings.setBurstReuestMode(); todo check for burst request mode
            } else {
                trader = TradingShared.createTrader();
                trader.setUserID(mixAuthResponse.getUserID());
                trader.setMubasherID(user);
                trader.setPath(Constants.PATH_PRIMARY);

                //Settings.setBurstReuestMode(); todo check for burst request mode
            }

            if (loginType == Constants.PATH_PRIMARY) {
                TradingShared.setLevel2AuthType(mixAuthResponse.getLevel2AuthType());
                TradingShared.setChangePassowrdNeeded((authStatus == TradeMeta.AUTH_CHANGE_PW));
                TradingShared.setLevel2AuthType(mixAuthResponse.getLevel2AuthType());
                TradingShared.setSessionID(mixAuthResponse.getSSessionID());
                TradingShared.setBrokerID(mixAuthResponse.getBrokerID());
                assignContactDetails(mixAuthResponse.getContactDetails());
                if (mixAuthResponse.getMaxConditionCount() != null) {
                    TradingShared.setMaxCondionCount(Integer.parseInt(mixAuthResponse.getMaxConditionCount()));
                }
                TradingShared.setDuInvestURL(mixAuthResponse.getDUTradeUrl());
            }

            TradingShared.setCurrentDay(mixAuthResponse.getCurrentDay());
            TradingShared.setReportURL(mixAuthResponse.getReportUrl());
            //TradingShared.setReportDepth(mixAuthResponse.getr);  // todo check for report depth
            TradingShared.setXstreamEnable(mixAuthResponse.isXstreamEnabled() == 1);

            trader.setLevel2AuthType(mixAuthResponse.getLevel2AuthType());
            trader.setCurrentDay(mixAuthResponse.getCurrentDay());
            trader.setSessionID(mixAuthResponse.getSSessionID());
            try {
                trader.addExchanges(mixAuthResponse.getExchanges());
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }

            //  trader.setTraderName(mixAuthResponse.getCustomerName()); //todo check Trader name
            trader.setTraderName(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(mixAuthResponse.getCustomerName()))); //todo check Trader name
            trader.setBrokerID(mixAuthResponse.getBrokerID());
            trader.setReportURL(mixAuthResponse.getReportUrl());
            //trader.setReportDepth();     todo check for report depth
            trader.setBanks(mixAuthResponse.getUserBankIDs()); //todo check for banks
            //   trader.setBanks(mixAuthResponse.getUserID(),new String[]{mixAuthResponse.getUserBankIDs()}); //todo check for banks

            ExchangeStore.getSharedInstance().addTradingWindowTypes(mixAuthResponse.getTradingWindowTypes(), loginType); //todo check for trading window typs


            if ((loginType == Constants.PATH_PRIMARY) && TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct) && mixAuthResponse.getAuthenticationStatus() != TradeMeta.AUTH_OTP_REQUIRED) {
                Settings.setSingleSignOnMode(true);
                //            Settings.setSingleSignOnID(mixAuthResponse.get);// todo check for SSO
                if (mixAuthResponse.getEncryptedSecurityToken() == null || mixAuthResponse.getEncryptedSecurityToken().equalsIgnoreCase("null")
                        || mixAuthResponse.getEncryptedSecurityToken().trim().length() == 0) {
                    String errMsg = Language.getString("SSO_PRICE_USER_NULL");
                    SharedMethods.showBlockingMessage(errMsg, JOptionPane.ERROR_MESSAGE);
                    return false;
                }
                Settings.setSingleSignOnID(mixAuthResponse.getEncryptedSecurityToken());// todo check for SSO
                if (mixAuthResponse.getPriceEncryptionType() > 0) {
                    Settings.setInstitutionID("" + mixAuthResponse.getPriceEncryptionType());
                }
                Settings.setBrokerInstitutionID(mixAuthResponse.getBrokerInstitutionID());

            }
            //  Settings.setInstitutionID(mixAuthResponse.getInstitutionID());

            /*======= Authentication General ============*/

            /*============= Portfolio Creation =================   */
            if (trader != null) {
                trader.createPortfolios(mixAuthResponse.getPortfolios());
            }
            //  createPortfolio(mixAuthResponse,trader);
            /*============= Portfolio Creation =================   */
            setRules(mixAuthResponse.getRules());

            //   mixAuthResponse.get
//            if (mixAuthResponse.getSecondaryLoginAvailable() != null && mixAuthResponse.getSecondaryLoginAvailable().equalsIgnoreCase("1")) {
            if (mixAuthResponse.getSecondaryOMSIP() != null && (mixAuthResponse.getSecondaryOMSIP().length() == 0) &&
                    mixAuthResponse.getSecondaryOMSLoginName() != null && (mixAuthResponse.getSecondaryOMSLoginName().length() == 0)) {
                String secondaryIP = null;
                String mubasherNumber = null;
                secondaryIP = mixAuthResponse.getSecondaryOMSIP();
                mubasherNumber = mixAuthResponse.getSecondaryOMSLoginName();
                TradingShared.INVALID_SECONDARY_LOGIN = false;
                TradingShared.setSecondaryLoginEnabled(true);
                secondaryLogin = new SecondaryLoginDeamon(mubasherNumber, secondaryIP, this);
            }

            if (loginType == Constants.PATH_PRIMARY) {
                TradingShared.setChangePassowrdNeeded((authStatus == TradeMeta.AUTH_CHANGE_PW));
            }
            if (authStatus == TradeMeta.AUTH_OK) {
                if (loginType == Constants.PATH_PRIMARY) {
                    paswordChangeRequest = false;
                    Login.setTradeLoginSucess(true);
                }
                return true;
            } else if (authStatus == TradeMeta.AUTH_CHANGE_PW) {
                if (loginType == Constants.PATH_PRIMARY) {
                    paswordChangeRequest = true;
                    TradingShared.setLevel1Password(null);
                } else {
                    TradingShared.INVALID_SECONDARY_LOGIN = true;
                }
                return true;
            } else {
                //showInvalidAuthMessage(Language.getString("INVALID_USER_OR_PASS"));
                if (loginType == Constants.PATH_PRIMARY) {
                    Login.setTradeLoginSucess(false);
                    paswordChangeRequest = false;
                    TradingShared.setLevel1Password(null);
                } else {
                    TradingShared.INVALID_SECONDARY_LOGIN = true;
                }
                return false;
            }
        } catch (Exception e) {

            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return false;
        }


    }


    public void setRules(ArrayList<com.dfn.mtr.mix.beans.Rule> rules) {
        if (rules != null && !rules.isEmpty()) {

            for (int i = 0; i < rules.size(); i++) {
                String id = null;
                String exchange = null;
                String group = null;
                String rule = null;
                String errormessage = "";
                int requiredCOunt = 0;
                String portfolioID = "*";
                com.dfn.mtr.mix.beans.Rule r = rules.get(i);

                id = r.getRuleID();
                exchange = r.getExchange();
                group = r.getGroupID();
                rule = r.getCondition();
                errormessage = r.getErrorMsg();
                if (errormessage == null) {
                    errormessage = "";
                }

                //      errormessage = r.getMessage();
                //    portfolioID = r.getPortfolio();

                if ((id != null) && (exchange != null) && (group != null) && (rule != null) && (errormessage != null)) {
                    if (!group.equals("COMMISSION")) {
                        RuleManager.getSharedInstance().setRule(id, exchange, group, rule, errormessage, portfolioID);
                    } else {
//                            String commisionRule="double commision=0;double totalCommision=0;String[] arr=list.split(\"~\");for(int i=0;i<arr.length;i++){String[] arr2=arr[i].split(\",\");double max=Double.parseDouble(arr2[0]);double min=Double.parseDouble(arr2[1]);double flat=Double.parseDouble(arr2[2]);double pct=Double.parseDouble(arr2[3]); commision=(price*quantity*pct/100)+(flat*currencyRate);if((commision!=0)&&(commision<min)){commision=min;}else if((commision!=0)&&(commision>max)){commision=max;}totalCommision=totalCommision+commision;}return totalCommision;";
                        if ((!rule.equals("")) && (rule != null)) {
                            RuleManager.getSharedInstance().setRule(id, exchange, group, rule, errormessage, portfolioID);
                        }

                    }

                }
            }
        }
    }

    private void createPortfolio(AuthenticationResponse mixAuthResponse, Trader trader) {
        if (trader != null) {
            ArrayList<BookKeeper> bookKeepers = new ArrayList<BookKeeper>();
            String portfolioID = null;
            String brokerID = null;
            int status = 0;
            String accountNumber = null;
            String name = null;
            boolean isDefault = true;
            int requiredCOunt = 0;
            String allowedExchanges = null;
            if (mixAuthResponse.getPortfolios() != null && !mixAuthResponse.getPortfolios().isEmpty()) {
                ArrayList<PortfolioHeader> portfolios = mixAuthResponse.getPortfolios();
                for (int i = 0; i < portfolios.size(); i++) {
                    PortfolioHeader pfRecord = portfolios.get(i);
                    portfolioID = pfRecord.getPortflioID();


                }


            }

            // requiredCOunt = mixAuthResponse.getp

        }
    }

    private void assignContactDetails(String data) {
        try {
            String[] fields = data.split("\\|");
            Settings.setBrokerPhone(fields[0]);
            Settings.setBrokerFax(fields[1]);
            Settings.setBrokerURL(fields[2]);
        } catch (Exception e) {
        }
    }

    public void showInvalidAuthMessage(String message) {
        Object[] options = {
                Language.getString("OK")};
        JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                message,
                Language.getString("ERROR"),
                JOptionPane.OK_OPTION,
                JOptionPane.ERROR_MESSAGE,
                null,
                options,
                options[0]);
    }

    /**
     * Display the invalid authentication dialog
     */


    public static void sleepThread(long time) {
        try {
            sleep(time);
        }
        catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void closeSocket() {
        try {
            ExchangeStore.getSharedInstance().clearTradingWindowTypes();
            socket.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void closeSecondarySocket() {
        if (secondaryLogin != null) {
            secondaryLogin.closeSocket();
            secondaryLogin = null;
        }
    }

    public synchronized static int getMode() {
        return connectionStatus;
    }

    public synchronized static void setMode(int status) {
        connectionStatus = status;
    }

    public synchronized static void setIdle() {
        if (TWControl.isSingleSingnOn() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)) {
            connectionStatus = IDLE;
        } else if (connectionStatus == STOPPED) {
            IPSettings.getSharedInstance().resetIPIndex();
            connectionStatus = IDLE;
        }
    }
}
 