// Copyright (c) 2001 Integrated Systems International (ISI)
package com.isi.csvr.trading;

/**
 * This is the class where from which data is sent
 * to the server. Given data items are queued and
 * sent to the server in a FIFo manner.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.Client;
import com.isi.csvr.shared.*;
import com.isi.csvr.trading.shared.TradingShared;

import java.io.*;
import java.util.*;
import java.net.*;

public class SendQueue extends Thread {

    private List primarylist;
    private List secondarylist;
    private Socket primarySocket;
    private Socket secondarySocket;
    private OutputStream primaryOut;
    private OutputStream secondaryOut;
    private static boolean initialized = false;
    public boolean isAuthenticated = false;

    public static SendQueue self = null;

    public static synchronized SendQueue getSharedInstance() {
        if (self == null) {
            self = new SendQueue();
        } else if (!initialized) {
            throw new RuntimeException("UnInitialized Send Queue");
        }
        return self;
    }

    /**
     * Constructor
     */
    private SendQueue() {
        super("TradeSendQueue");
        primarylist = Collections.synchronizedList(new LinkedList());
        secondarylist = Collections.synchronizedList(new LinkedList());
        start();
    }

    public void init(Socket socket, OutputStream out, byte path) throws Exception {
        if (path == Constants.PATH_SECONDARY && TradingShared.SECONDARY_LOGIN_SUCCESS) {
            secondarylist.clear();
            closeSecondaryConnection();
            this.secondarySocket = socket;
            this.secondaryOut = out;
        } else if (path == Constants.PATH_PRIMARY) {
            primarylist.clear();
            closeConnection();
            this.primarySocket = socket;
            this.primaryOut = out;
            initialized = true;
        }
    }

    public void setSecondarySocket(Socket socket) {
        try {
            secondarylist.clear();
            closeSecondaryConnection();
            this.secondarySocket = socket;
            this.secondaryOut = socket.getOutputStream();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }


    public void run() {
        while (true) {
            try {
                /* Loop while the client is connected to the server and there
                    are frames in the queue*/
                String sOutData = null;
                boolean status = TradingShared.isAuthenticated();
//                while (TradingShared.isConnected() && ((primarylist.size() > 0) || (secondarylist.size() > 0))) {
                while (status) {
                    try {
                        if ((primarylist.size() > 0)) {
                            sOutData = (String) primarylist.remove(0);
//                            byte path = Byte.parseByte(sOutData.substring(0,1));
                            writeData(sOutData, Constants.PATH_PRIMARY);
                            sOutData = null;
                        }
                        if ((secondarylist.size() > 0)) {
                            sOutData = (String) secondarylist.remove(0);
//                            byte path = Byte.parseByte(sOutData.substring(0,1));
                            writeData(sOutData, Constants.PATH_SECONDARY);
                            sOutData = null;
                        }
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }

                Sleep(1000);
            } catch (Exception e) {
                e.printStackTrace();
                closeConnection();
                if ((TradingShared.isManualDisconnection())) {// || (TradingShared.isPriceDisconnection())){
                    System.out.println("Send Queue and manual disconnection true");
                    Client.getInstance().setTradeSessionDisconnected(true);
                } else {
                    System.out.println("Send Queue and manual disconnection false");
                    Client.getInstance().setTradeSessionDisconnected(false);
                }
            }
        }
    }

    private void closePrimaryConnection() {
        try {
            primarySocket.close();
            primarySocket = null;
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    private void closeSecondaryConnection() {
        try {
            secondarySocket.close();
            secondarySocket = null;
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    private void closeConnection() {
        closePrimaryConnection();
        closeSecondaryConnection();
    }

    public synchronized void writeData(String data, byte path) throws Exception {

//        try {
        if (path == Constants.PATH_SECONDARY && TradingShared.SECONDARY_LOGIN_SUCCESS) {
            System.out.println("Trading request in Secondary->" + data);
            secondaryOut.write(data.getBytes());
        } else if (path == Constants.PATH_PRIMARY) {
            System.out.println("Trading  request in Primary->" + data);
            primaryOut.write(data.getBytes());
        }
//        } catch(IOException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
    }

    /**
     * Adds the given frame to the out queue.
     */
    public synchronized void addData(String sData, byte path) {
        if ((secondarylist != null) && (path == Constants.PATH_SECONDARY)) {
            secondarylist.add(sData);
            SharedMethods.printLine(sData, true);
        } else if ((primarylist != null) && (path == Constants.PATH_PRIMARY)) {
            SharedMethods.printLine(sData, true);
            primarylist.add(sData);
        }
    }


    /**
     * Sleeps the theread for the given interval
     */
    private void Sleep(long lDelay) {
        try {
            sleep(lDelay);
        } catch (Exception e) {
        }
    }
}

