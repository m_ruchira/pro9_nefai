// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.trading;


import com.isi.csvr.Client;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.trading.shared.TradingShared;
import com.dfn.mtr.mix.beans.MIXObject;

import java.io.FileOutputStream;
import java.io.InputStream;
import java.io.InterruptedIOException;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

/**
 * Receive buffer for the in-comming data
 * Adds all incomming frames to a Queue.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class ReceiveQueue extends Thread {
    private List<MixObjectWrapper> receiveBuffer;
    private InputStream g_oIn;
    private Socket g_oSocket;
    private byte path;
    public boolean isAuthenticated = false;
//    private List frameAnalyser;

    /**
     * Constructor
     */
    public ReceiveQueue(Socket oSocket, byte path) throws Exception {
        super("TradingReceiveQueue");
//        receiveBuffer = Collections.synchronizedList(new LinkedList());
        g_oSocket = oSocket;
        this.path = path;
        g_oIn = oSocket.getInputStream();
//        start();
    }

    public void setReceiveBuffer(List receiveBuffer){
        this.receiveBuffer = receiveBuffer;
    }


    public void interrupt() {
        try {
            if(g_oIn != null) {
                g_oIn.close();
                g_oIn = null;
            }
            super.interrupt();    //To change body of overridden methods use File | Settings | File Templates.
        } catch(Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    /**
     * run method of the thread.
     */
    public void run() {

        StringBuffer sFrame = new StringBuffer("");
        int iValue = -1;

        try {
            start:
            g_oSocket.setSoTimeout(60000);
            boolean status = TradingShared.isAuthenticated();
            /* Read the normal data stream from the server */
            if(path == Constants.PATH_SECONDARY){
                status = (TradingShared.isConnected() && TradingShared.isSecondaryLoginEnabled() && TradingShared.SECONDARY_LOGIN_SUCCESS);
            }
            while (status) {
                iValue = g_oIn.read();
                try {

                    if (iValue == -1)
                        throw new Exception("End of Stream");
                    if (iValue != '\n') {
                        sFrame.append((char) iValue);
                        continue;
                    }
                } catch (InterruptedIOException e) {
                    continue;
                }
                if ((sFrame != null) && (sFrame.length() > 0)) {
                    MixObjectWrapper mixReply = new MixObjectWrapper();
                    mixReply.setMIXString(sFrame.toString());
                    mixReply.setPath(path);
                    receiveBuffer.add(mixReply);
                    if(path != Constants.PATH_SECONDARY){
//                        System.out.println("Trading received in Primary <--" + sFrame.toString());
                    } else {
//                        System.out.println("Trading received in Secondary <--" + sFrame.toString());
                    }
                    sFrame = null; // added 7/1/2002 uditha
                    sFrame = new StringBuffer("");
                }
            }
        } catch (Exception e) {
            if ((TradingShared.isManualDisconnection())){// || (TradingShared.isPriceDisconnection())){
                if(path == Constants.PATH_PRIMARY){
                    Client.getInstance().setTradeSessionDisconnected(true);
                    System.out.println("Primary Receive Queue and manual disconnection true");
                } else {
                    TradingShared.SECONDARY_LOGIN_SUCCESS = false;
                    TradingShared.INVALID_SECONDARY_LOGIN_ATEMPTS =0;
                    Client.getInstance().setTradeConnectionBulbStatus();
                    System.out.println("Secondary Receive Queue and manual disconnection true");
                }
            } else {
                if(path == Constants.PATH_PRIMARY){
//                    TradingShared.SECONDARY_LOGIN_SUCCESS = false;
                    Client.getInstance().setTradeSessionDisconnected(false);
                    System.out.println("Primary Receive Queue and manual disconnection false");
                } else {
                    TradingShared.SECONDARY_LOGIN_SUCCESS = false;
                    TradingShared.INVALID_SECONDARY_LOGIN_ATEMPTS =0;
                    Client.getInstance().setTradeConnectionBulbStatus();
                    System.out.println("Secondary Receive Queue and manual disconnection false");
                }
            }
            SharedMethods.printLine("Exception in Trade ReceiveQ and SFrame =="+sFrame,true);
            e.printStackTrace();
            closeConnection();
        }
        try {
            g_oIn.close();
            g_oIn = null;
        } catch (Exception e) {
        }
    }


    private void closeConnection() {
        try {
            g_oSocket.close();
            g_oSocket = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Returns the receive buffer
     */
    public List getReceiveBuffer() {
        return receiveBuffer;
    }



}

