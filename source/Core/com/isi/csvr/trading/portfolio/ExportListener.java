package com.isi.csvr.trading.portfolio;

// Copyright (c) 2000 Home

public interface ExportListener {
    public void exportCompleted(Exception e);

    public void exportProgressChanged(int iProgress, int iMax);
}