package com.isi.csvr.trading.portfolio;

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.trading.shared.TradeKey;
import com.isi.csvr.trading.shared.TradingPortfolioRecord;

import java.io.*;
import java.util.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public final class TradePortfolios implements Serializable {

    public static String transFileName = Settings.getAbsolutepath()+"datastore/TPortfolio.mdf";
    private static ArrayList<TradingPortfolioRecord> portfolioList;         // Contains the portfolio list
    private static ArrayList<TransactRecord> transactionRecords;    // Contains the whole list of transactions
    private static ArrayList filteredTransactions;  // Contains the whole list of transactions
    private static ArrayList<ValuationRecord> valuationRecords;      // Contains calculated record details for the
    private static  ArrayList<ValuationRecord> aModifidValRecords ;
    private  static  ArrayList<PfValutionRecord> aPfValRecords ;
    Hashtable <String,Integer> hashPfRecoredssize;
    private static ArrayList<Integer> aPfsizes = new ArrayList<Integer>();
    private static ArrayList<String> aSelPfNames = new ArrayList<String>();


    //public static

    // selected Portfolio(s). These records may get
    // change due to the post transactions associated
    // with the selected portfolio(s).
    //    private static  String  baseCurrency    = ""; //"USD"; // "USD"; //
    public static Hashtable<String, Float> changedAVGCost;//= new Hashtable();



    private static Hashtable<String, ArrayList<ValuationRecord>> hashPFWiseValReocrds;       //pfId : corrosponding valutionreocrds

    private PortfolioSettings portfolioSettings;
    private String[] selectedPortfolios;
    private static TradePortfolios selfRef = null;
    public Properties props = null;

    private TradePortfolios() {
        portfolioList = new ArrayList<TradingPortfolioRecord>();
        transactionRecords = new ArrayList<TransactRecord>();
        filteredTransactions = new ArrayList();
        valuationRecords = new ArrayList<ValuationRecord>(); //(20, 0.9f);
        aModifidValRecords = new ArrayList<ValuationRecord>();
        aPfValRecords = new ArrayList<PfValutionRecord>();

        changedAVGCost = new Hashtable();
        hashPfRecoredssize = new Hashtable<String, Integer>();

        hashPFWiseValReocrds = new Hashtable();

        try {
            loadSettings();
        } catch (Exception ex) {
        }
    }

    public static synchronized TradePortfolios getInstance() {
        if (selfRef == null)
            selfRef = new TradePortfolios();
        return selfRef;
    }

    public ArrayList<TradingPortfolioRecord> getPortfolioList() {
        return portfolioList;
    }

    public ArrayList getTransactionList() {
        return transactionRecords;
    }

    public ArrayList getFilteredTransactionList() {
        return filteredTransactions;
    }

    public void addPortfolio(TradingPortfolioRecord record) {
        portfolioList.add(record);
    }

    public boolean isSufficientQuantityAvailable(String pfID, String sKey, int qty) {
        int availQty = 0;
        TransactRecord pfRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if ((pfRec.getPfID().equals(pfID)) && (pfRec.getKey().equals(sKey))) {
                switch (pfRec.getTxnType()) {
                    case PFUtilities.BUY:
                    case PFUtilities.OP_BAL:
                        availQty += pfRec.getQuantity();
                        break;
                    case PFUtilities.SELL:
                        availQty -= pfRec.getQuantity();
                        break;
                }
            }
            pfRec = null;
        }
        if (availQty >= qty)
            return true;
        else
            return false;
    }

    public int getAvailableQuantity(String pfID, String sKey) {
        int availQty = 0;
        TransactRecord pfRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if ((pfRec.getPfID().equals(pfID)) && (pfRec.getKey().equals(sKey))) {
                availQty += pfRec.getQuantity();
            }
            pfRec = null;
        }
        return availQty;
    }

    public int getAvilHoldingForSell(String pfID, String sKey){
        int holdings = 0;
        TransactRecord pfRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if ((pfRec.getPfID().equals(pfID)) && (pfRec.getKey().equals(sKey))) {
                holdings += ((pfRec.getQuantity()-pfRec.getPledged()-pfRec.getUnsettledSellQty()) + pfRec.getUnsettledBuyQty());
//                        holdings += (pfRec.getQuantity()-pfRec.getPledged()-pfRec.getPendingSell());
            }
            pfRec = null;
        }
        return holdings;
    }

    public int getAvailableQuantityForRapidOrder(String pfID, String sKey){
        int holdings = 0;
                TransactRecord pfRec = null;
                for (int i = 0; i < transactionRecords.size(); i++) {
                    pfRec = transactionRecords.get(i);
                    if ((pfRec.getPfID().equals(pfID)) && (pfRec.getKey().equals(sKey))) {
                        holdings += pfRec.getAvailableForSell();
                    }
                    pfRec = null;
                }
                return holdings;

    }
    
    public int getTPlusHoldings(String pfID, String sKey){
        int holdings = 0;
        TransactRecord pfRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if ((pfRec.getPfID().equals(pfID)) && (pfRec.getKey().equals(sKey))) {
                holdings += (pfRec.getQuantity()-pfRec.getPledged());
            }
            pfRec = null;
        }
        return holdings;
    }
     public int getTPlusPendingStocks(String pfID, String sKey){
        int pStocks = 0;
        TransactRecord pfRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if ((pfRec.getPfID().equals(pfID)) && (pfRec.getKey().equals(sKey))) {
                pStocks += (pfRec.getTPlusPendingStock());
            }
            pfRec = null;
        }
        return pStocks;
    }
    public String getBookKeeper(String pfID, String sKey){
        String bKeeper="";
        TransactRecord pfRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if ((pfRec.getPfID().equals(pfID)) && (pfRec.getKey().equals(sKey))) {
                bKeeper=pfRec.getBookKeeper();
            }
            pfRec = null;
        }
        return bKeeper;
    }
    public int getDayHolding(String pfID, String sKey) {
        int availQty = 0;
        TransactRecord pfRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if ((pfRec.getPfID().equals(pfID)) && (pfRec.getKey().equals(sKey))) {
                availQty += pfRec.getDayHolding();
            }
            pfRec = null;
        }
        return availQty;
    }

    public long getPendingBuyQuantity(String pfID, String sKey) {
        TransactRecord pfRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if ((pfRec.getPfID().equals(pfID)) && (pfRec.getKey().equals(sKey))) {
                return pfRec.getPendingBuy();
            }
            pfRec = null;
        }
        return 0;
    }

    public long getPendingSellQuantity(String pfID, String sKey) {
        TransactRecord pfRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if ((pfRec.getPfID().equals(pfID)) && (pfRec.getKey().equals(sKey))) {
                return pfRec.getPendingSell();
            }
            pfRec = null;
        }
        return 0;
    }

    public long getOpenBuyCount(String pfID, String sKey) {
        TransactRecord pfRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if ((pfRec.getPfID().equals(pfID)) && (pfRec.getKey().equals(sKey))) {
                return pfRec.getOpenBuyCount();
            }
            pfRec = null;
        }
        return 0;
    }

    public long getOpenSellCount(String pfID, String sKey) {
        TransactRecord pfRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if ((pfRec.getPfID().equals(pfID)) && (pfRec.getKey().equals(sKey))) {
                return pfRec.getOpenSellCount();
            }
            pfRec = null;
        }
        return 0;
    }

    public boolean isPortfolioNameExists(String name) {
        TradingPortfolioRecord pfRec = null;
        for (int i = 0; i < portfolioList.size(); i++) {
            pfRec = portfolioList.get(i);
            if (pfRec.getName().equalsIgnoreCase(name)) {
                return true;
            }
            pfRec = null;
        }
        return false;
    }

    public TradingPortfolioRecord getPortfolio(int index) {
//        PortfolioRecord pfRec  = null;
//        for (int i = 0; i < portfolioList.size(); i++) {
//            pfRec = (PortfolioRecord)portfolioList.get(i);
//            if (pfRec.getPfID() == pfID) {
//                break;
//            }
//            pfRec = null;
//        }
        try {
            return portfolioList.get(index);
        } catch (Exception ex) {
            return null;
        }
    }

    public TradingPortfolioRecord getPortfolio(String pfID) {
        TradingPortfolioRecord pfRec = null;
        for (int i = 0; i < portfolioList.size(); i++) {
            pfRec = portfolioList.get(i);
            if (pfRec.getPortfolioID().equals(pfID)) {
                break;
            }
            pfRec = null;
        }
        return pfRec;
    }

    public String getPortfolioListString(){
     TradingPortfolioRecord pfRec = null;
     String pfList="";
        for (int i = 0; i < portfolioList.size(); i++) {
            pfRec = portfolioList.get(i);
            pfList=pfList+","+pfRec.getPortfolioID();
            pfRec = null;
        }
        return pfList.substring(1);
    }

    public String getPortfolioName(String pfID) {
        TradingPortfolioRecord pfRec = null;
        for (int i = 0; i < portfolioList.size(); i++) {
            pfRec = portfolioList.get(i);
            if (pfRec.getPortfolioID().equals(pfID)) {
//                System.out.println("PF NAME " + pfID + " " + pfRec.getName());
                return pfRec.getName();
            }
            pfRec = null;
        }
        return "";
    }

    public String getPortfolioCurrency(String pfID){

        TradingPortfolioRecord pfRec = null;
        for (int i = 0; i < portfolioList.size(); i++) {
            pfRec = portfolioList.get(i);
            if (pfRec.getPortfolioID().equals(pfID)) {
//                System.out.println("PF NAME " + pfID + " " + pfRec.getName());
                return pfRec.getCurrencyID();
            }
            pfRec = null;
        }
        return "";

    }

    public void deletePortfolio(String pfID) {
        TradeKey sKey = null;
        TransactRecord pfRec = null;
        TradingPortfolioRecord portflio = null;
        for (int i = 0; i < portfolioList.size(); i++) {
            portflio = portfolioList.get(i);
            if (portflio.getPortfolioID().equals(pfID)) {
                portfolioList.remove(i);
                break;
            }
            portflio = null;
        }
        portflio = null;

        // First remove from the Transaction log
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            if (pfRec.getPfID().equals(pfID)) {
                sKey = pfRec.getKey();
                transactionRecords.remove(i);
            }
            pfRec = null;
        }

        // Second remove from the filtered Transaction log
        for (int i = 0; i < filteredTransactions.size(); i++) {
            pfRec = (TransactRecord) filteredTransactions.get(i);
            if (pfRec.getPfID().equals(pfID)) {
                filteredTransactions.remove(i);
            }
            pfRec = null;
        }
        pfRec = null;

        ValuationRecord valRec = null;
        for (int i = 0; i < valuationRecords.size(); i++) {
            valRec = valuationRecords.get(i);
            if (sKey.equals(valRec.getSKey())) {
                valuationRecords.remove(i);
                PortfolioInterface.removeSymbol(sKey);  // Added on 13-05-03
            }
            valRec = null;
        }
        sKey = null;

    }

    public ValuationRecord getValuationRecord(String sKey) {
        ValuationRecord valRec = null;
        for (int i = 0; i < valuationRecords.size(); i++) {
            valRec = valuationRecords.get(i);
            if (valRec.getSKey().equals(sKey)) {
                break;
            }
            valRec = null;
        }
        return valRec; //(ValuationRecord)valuationRecords.get(sKey);
    }

    public TransactRecord getTransactRecord(String sKey) {
        TransactRecord TrRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            TrRec = transactionRecords.get(i);
            if (TrRec.getKey().equals(sKey)) {
                if (TrRec.getQuantity() != -1) { // this is a valid order
                    return TrRec;
                }
            }

        }
        return null;
    }

    /*public TransactRecord getTransactRecord(String sKey,Double val) {
        TransactRecord TrRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            TrRec = (TransactRecord) transactionRecords.get(i);
            if (TrRec.getKey().equals(sKey)) {
                if (TrRec.getQuantity() > 0) { // this is a valid order

                    if(TrRec.getPrice()==val){
        return TrRec;
    }
                }


        }

        }
        return null;
    }*/
    public TransactRecord getTransactRecord(String sKey, String pid) {
        TransactRecord TrRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            TrRec = transactionRecords.get(i);
            if (TrRec.getKey().equals(sKey)) {
                if ((TrRec.getBalance()) != 0 ) { // this is a valid order //HASA
                    if (TrRec.getPfID().equals(pid)) {
                        return TrRec;
                    }
                    //} catch (Exception e) {
                }
            }

        }
        return null;
    }

    public TransactRecord getTransactRecordForFutures(String sKey, String pid) {
        TransactRecord TrRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            TrRec = transactionRecords.get(i);
            if (TrRec.getKey().equals(sKey)) {
                // if (TrRec.getQuantity() > 0) { // this is a valid order
                if (TrRec.getPfID().equals(pid)) {
                    return TrRec;
                }
                //} catch (Exception e) {
                //}
            }

        }
        return null;
    }

    public ArrayList getTransactRecordList(String sKey) {
        TransactRecord TrRec = null;
        ArrayList trecArray = new ArrayList();
        for (int i = 0; i < transactionRecords.size(); i++) {
            TrRec =  transactionRecords.get(i);
            if (TrRec.getKey().equals(sKey)) {
                if (TrRec.getQuantity() != 0) { // this is a valid order
                    trecArray.add(TrRec);
                }
            }
            TrRec = null;
        }
        return trecArray;
    }

    public ArrayList getValuationList() {
        return valuationRecords;
    }

     public ArrayList getPfValuationList() {
       // return valuationRecords;
        return aPfValRecords;
         //return aModifidValRecords;

    }
    public ArrayList<String> getSelectedPortFolioList(){

        return aSelPfNames;

    }
    public Hashtable<String ,Integer> getPfcount(){
        return hashPfRecoredssize;
    }

    public ArrayList<Integer> getPfsizes (){
        
        return aPfsizes;
    }

    public Hashtable<String ,ArrayList<ValuationRecord>> getPFWiseValList(){
        return hashPFWiseValReocrds;

    }

    public synchronized void addTransaction(TransactRecord record) {
        int noOfRecords = transactionRecords.size();
        boolean processed = false;
        TransactRecord oldRec = null;
        for (int i = 0; i < noOfRecords; i++) {
            oldRec = transactionRecords.get(i);
            if ((oldRec.getKey().equals(record.getKey())) && (oldRec.getPfID().equals(record.getPfID()))) {
                if (record.getQuantity() != 0) { // this is a valid order
                    transactionRecords.set(i, record);
                } else { // this record only has pending orders
                    if ((record.getPendingBuy() > 0) || (record.getPendingSell() > 0) ||
                            (record.getOpenBuyCount() > 0) || (record.getOpenSellCount() > 0) ||
                            (record.getMarginHoldings() != 0) || (record.getDayMarginHoldings() != 0) ||
                            (record.getDayHolding() != 0 || record.getAvailableForSell() != 0) ) {
                        transactionRecords.set(i, record);
                    } else { // this is a remove request (i.e. qty = 0)
                        transactionRecords.remove(oldRec);
                    }
                }
                processed = true;
                break;
            }
            oldRec = null;
        }
                
        if (!processed) {
            //if (record.getQuantity() > 0){ // add only valid records
            transactionRecords.add(record);
            System.out.println("Added " + record.getPfID() + " " + record.getKey() + " " + record.getQuantity());
            //}                                                                    
        }
    }

    public TransactRecord getTransaction(long txnID) {
        TransactRecord pfRec = null;
        for (int i = 0; i < filteredTransactions.size(); i++) {
            pfRec = (TransactRecord) filteredTransactions.get(i);
            if (pfRec.getId() == txnID) {
                break;
            }
            pfRec = null;
        }
        return pfRec;
    }

    public double getWeightedAvgCostForTheTransaction(long txnID) {
        double wAvgCost = 0f;
        double value = 0f;
        double totCost = 0f;
        long holding = 0;

        TransactRecord record = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            record = transactionRecords.get(i);
            if (record.getId() < txnID) {
                switch (record.getTxnType()) {
                    case PFUtilities.OP_BAL:
                    case PFUtilities.BUY:
                       // totCost = Math.abs(record.getQuantity()) * record.getPrice() + record.getBrokerage();
                        totCost = Math.abs(record.getQuantity()) * record.getPrice() ; // Brokerage was removed since this var is not assigned a value 
                        holding += record.getQuantity();
                        value += totCost;
                        wAvgCost = value / holding;
                        break;
                    case PFUtilities.SELL:
                        holding -= record.getQuantity();
                        value -= wAvgCost * Math.abs(record.getQuantity());
                        break;
                }
            } else if (record.getId() == txnID) {
                break;
            }
            record = null;
        }

        return wAvgCost;
    }

//    public void deleteTransaction(long txnID) {
//        String   sKey   = null;
//        TransactRecord pfRec  = null;
//        // First remove from the Transaction log
//        for (int i = 0; i < transactionRecords.size(); i++) {
//            pfRec = (TransactRecord)transactionRecords.get(i);
//            if (pfRec.getId() == txnID) {
//                sKey = pfRec.getSKey();
//                undoTransaction((TransactRecord)transactionRecords.remove(i));
//                break;
//            }
//            pfRec = null;
//        }
//
//        // Second remove from the filtered Transaction log
//        for (int i = 0; i < filteredTransactions.size(); i++) {
//            pfRec = (TransactRecord)filteredTransactions.get(i);
//            if (pfRec.getId() == txnID) {
//                filteredTransactions.remove(i);
//                break;
//            }
//            pfRec = null;
//        }
//        pfRec  = null;
//
//        ValuationRecord valRec = null;
//        for (int i = 0; i < valuationRecords.size(); i++) {
//            valRec = (ValuationRecord)valuationRecords.get(i);
//            if (valRec.getSKey().equals(sKey)) {
//                valuationRecords.remove(i);
//                PortfolioInterface.removeSymbol(sKey);  // Added on 13-05-03
//            }
//            valRec = null;
//        }
//        sKey = null;
//
////        createFilteredTransactionList(null, PFRecord.NONE, null, null);
//    }

    public synchronized void createFilteredTransactionList(String[] pfIDs, String sKey, byte type, Date startDate, Date endDate) {
        boolean isPFSelected = false;
        boolean isNotRelevant = false;
        TransactRecord pfRec = null;

        if (pfIDs == null)
            return;
        filteredTransactions.clear();
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);

            for (int j = 0; j < pfIDs.length; j++) {
                if (pfRec.getPfID().equals(pfIDs[j])) {
                    isPFSelected = true;
                    break;
                }
            }
            if (isPFSelected) {
                // Apply the conditions to filter the history records
                if ((sKey != null) && (pfRec.getKey().equals(sKey)))
                    isNotRelevant = true;
                if ((type != PFUtilities.NONE) && (type != pfRec.getTxnType()))
                    isNotRelevant = true;
                if (startDate != null) {
                    // To apply the date filter startDate should not be NULL. Other wise date filter is not applied.
                    // If teh endDate is NULL, current time is taken as the end date.
                    if (endDate == null)
                        endDate = new Date();
                    Date recordedDate = new Date(pfRec.getTxnDate());
//                    if (recordedDate.before(startDate) || recordedDate.after(endDate))
                    if (PFUtilities.isPriorDate(recordedDate, startDate) || PFUtilities.isPriorDate(endDate, recordedDate))
                        isNotRelevant = true;
                    recordedDate = null;
                } else {
                    if (endDate == null)
                        endDate = new Date();
                    Date recordedDate = new Date(pfRec.getTxnDate());
//                    if (recordedDate.after(endDate))
                    if (PFUtilities.isPriorDate(endDate, recordedDate))
                        isNotRelevant = true;
                    recordedDate = null;
                }
                // Ignore the current record
                if (isNotRelevant) {
                    isNotRelevant = false;
                    pfRec = null;
                    continue;
                } else {
                    filteredTransactions.add(pfRec);
                    pfRec = null;
                }
                isPFSelected = false;
            }
        }
    }

    public synchronized void createValuationList(String[] portfolioIDs, Date endDate) {
        boolean isPFSelected = false;
        boolean isNotRelevant = false;
        TransactRecord pfRec = null;
        ValuationRecord valRec = null;

        if (portfolioIDs != null)
            selectedPortfolios = portfolioIDs;

        if (selectedPortfolios == null)
            return;

        valuationRecords.clear();

        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = (TransactRecord) transactionRecords.get(i);
            // To apply the date filter startDate should not be NULL. Other wise date filter is not applied.
            // If teh endDate is NULL, current time is taken as the end date.
            if (endDate == null)
                endDate = new Date();
            Date recordedDate = new Date(pfRec.getTxnDate());
            if (recordedDate.after(endDate)) {
                isNotRelevant = true;
            }
            recordedDate = null;

            /*if ((pfRec.getQuantity() <= 0) && (pfRec.getOpenBuyCount() <= 0) && (pfRec.getOpenSellCount() <= 0)) {
                isNotRelevant = true;
            }*/

            if ((pfRec.getQuantity() < 0) && (pfRec.getOpenBuyCount() <= 0) && (pfRec.getOpenSellCount() <= 0) &&
                    (pfRec.getDayMarginHoldings() ==0) && (pfRec.getMarginHoldings() ==0) && (pfRec.getDayHolding() ==0)) {
                isNotRelevant = true;
            }

//            }
            // Ignore the current record
            if (isNotRelevant) {
                isNotRelevant = false;
                continue;
            }

            for (int j = 0; j < selectedPortfolios.length; j++) {
                if (pfRec.getPfID().equals(selectedPortfolios[j])) {
                    isPFSelected = true;
                    break;
                }
            }

            if (isPFSelected) {
                /*for (int i2 = 0; i2 < valuationRecords.size(); i2++) {
                    valRec = (ValuationRecord) valuationRecords.get(i2);
                    if (valRec.getSKey().equals(pfRec.getKey())) {
                        break;
                    }
                    valRec = null;
                }

                if (valRec == null) {
                    valRec = new ValuationRecord();
                    valRec.setSKey(pfRec.getKey());
                    valRec.setCurrency(pfRec.getCurrency());
                }*/
                valRec = setValutionRecord(pfRec);
                valuationRecords.add(valRec);
                valRec = null;
                isPFSelected = false;
            }
            pfRec = null;
        }
    }

    private ValuationRecord setValutionRecord(TransactRecord pfRec) {
        ValuationRecord valRec;
                valRec = new ValuationRecord();
                valRec.setSKey(pfRec.getKey());
                valRec.setCurrency(pfRec.getCurrency());
                valRec.setOwned(pfRec.getQuantity());                      // Holding = Qty
                valRec.setPledged(pfRec.getPledged());                      // Holding = Qty
                valRec.setAvgCost(pfRec.getPrice());
                valRec.setCumCost(Math.abs(pfRec.getBalance() + (pfRec.getOpenBuyCount() + pfRec.getOpenSellCount() + pfRec.getPendingSell()) * pfRec.getContractSize()) * pfRec.getPrice()); // only one of Quantity, gOpenBuyCount, OpenSellCount will be preset at time
                valRec.setBuyPending(pfRec.getPendingBuy());
                valRec.setSellPending(pfRec.getPendingSell());
                valRec.setPortfolioID(pfRec.getPfID());
                valRec.setDayHoldings(pfRec.getDayHolding());
                valRec.setMarginHoldings(pfRec.getMarginHoldings());
                valRec.setDayMarginHoldings(pfRec.getDayMarginHoldings());
                valRec.setMarginDue(pfRec.getMarginDue());
                valRec.setDayMarginDue(pfRec.getDayMarginDue());
                valRec.setMarginBuyPct(pfRec.getMarginBuyPct());
                valRec.setOpenBuyCount(pfRec.getOpenBuyCount());
                valRec.setOpenSellCount(pfRec.getOpenSellCount());
                valRec.setContractSize(pfRec.getContractSize());
                valRec.setPriceCorrectionFactor(pfRec.getPriceCorrectionFactor());
                valRec.setBookKeeper(pfRec.getBookKeeper());
                valRec.setTPlusDayNetHolding(pfRec.getTPlusDayNetHolding());
                valRec.setTPlusPendingStock(pfRec.getTPlusPendingStock());
                valRec.setTPlusDaySellPending(pfRec.getTPlusDaySellPending());
                valRec.setBlockedAmount(pfRec.getBlockedQuantity());
                valRec.setBalanceFromServer(pfRec.getBalance());
                valRec.setPayableHoldings(pfRec.getPayableHolding());
//                valRec.setReceivableHoldings(pfRec.get);
                valRec.setAveragePrice(pfRec.getAveragePrice());
                valRec.setReceivableHoldings(pfRec.getReceivableHolding());
                valRec.setWANC(pfRec.getWeightedNetCost());


        return valRec;
    }

    private ArrayList<ValuationRecord> getAllValuationRecords() {
        ArrayList<ValuationRecord> records;
        ValuationRecord valRec = null;
        TransactRecord pfRec = null;
        boolean isNotRelevant = true;

        records = new ArrayList<ValuationRecord>();
        for (int i = 0; i < transactionRecords.size(); i++) {
            pfRec = transactionRecords.get(i);
            // To apply the date filter startDate should not be NULL. Other wise date filter is not applied.
            // If teh endDate is NULL, current time is taken as the end date.


            if ((pfRec.getQuantity() < 0) && (pfRec.getOpenBuyCount() <= 0) && (pfRec.getOpenSellCount() <= 0) &&
                    (pfRec.getDayMarginHoldings() ==0) && (pfRec.getMarginHoldings() ==0) && (pfRec.getDayHolding() ==0)) {
                isNotRelevant = true;
            }

            // Ignore the current record
            if (isNotRelevant) {
                isNotRelevant = false;
                continue;
            }
            valRec = new ValuationRecord();
            valRec.setSKey(pfRec.getKey());
            valRec.setCurrency(pfRec.getCurrency());
            valRec.setOwned(pfRec.getQuantity());                      // Holding = Qty
            valRec.setPledged(pfRec.getPledged());                      // Holding = Qty
            valRec.setAvgCost(pfRec.getPrice());
            valRec.setCumCost(Math.abs(pfRec.getBalance() + (pfRec.getOpenBuyCount() + pfRec.getOpenSellCount() + pfRec.getPendingSell()) * pfRec.getContractSize()) * pfRec.getPrice()); // only one of Quantity, gOpenBuyCount, OpenSellCount will be preset at time
            valRec.setBuyPending(pfRec.getPendingBuy());
            valRec.setSellPending(pfRec.getPendingSell());
            valRec.setPortfolioID(pfRec.getPfID());

            valRec.setMarginDue(pfRec.getMarginDue());
            valRec.setDayMarginDue(pfRec.getDayMarginDue());
            valRec.setMarginBuyPct(pfRec.getMarginBuyPct());
            valRec.setOpenBuyCount(pfRec.getOpenBuyCount());
            valRec.setOpenSellCount(pfRec.getOpenSellCount());
            valRec.setContractSize(pfRec.getContractSize());
            valRec.setPriceCorrectionFactor(pfRec.getPriceCorrectionFactor());
            valRec.setBookKeeper(pfRec.getBookKeeper());
            valRec.setPayableHoldings(pfRec.getPayableHolding());
            valRec.setAveragePrice(pfRec.getAveragePrice());
            valRec.setReceivableHoldings(pfRec.getReceivableHolding());
            valRec.setWANC(pfRec.getWeightedNetCost());
            valRec.setBalanceFromServer(pfRec.getBalance());



            records.add(valRec);

            valRec = null;
            pfRec = null;
        }

        return records;
    }

    public void clear() {
        transactionRecords.clear();
        valuationRecords.clear();
        portfolioList.clear();
        filteredTransactions.clear();  
    }

    public void clearTransactions() {
        transactionRecords.clear();
        valuationRecords.clear();
        filteredTransactions.clear();
    }

    public String getBaseCurrency() {
        return portfolioSettings.baseCurrency;
    }

    public void setBaseCurrency(String newBaseCurrency) {
        portfolioSettings.baseCurrency = newBaseCurrency;
    }

    public TradingPortfolioRecord getDefaultPortfolio() {
        TradingPortfolioRecord record = null;
        for (int i = 0; i < portfolioList.size(); i++) {
            record = portfolioList.get(i);
            if (record.isDefaultPortfolio()) {
                return record;
            }
            record = null;
        }
        return null;
    }


    public synchronized void saveSettings() {
        try {
            ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(transFileName));
            out.writeObject(portfolioSettings);
            out.flush();
            out.close();
            //  CustomerAVGCostWriter.write("testing...");
            saveCustomerAVGCostSettings();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public void loadSettings() {

        try {
            ObjectInputStream in = new ObjectInputStream(new FileInputStream(transFileName));
            portfolioSettings = (PortfolioSettings) in.readObject();
            in.close();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        if (portfolioSettings == null) {
            portfolioSettings = new PortfolioSettings();
        }
    }

    public void saveCustomerAVGCostSettings() {
        try {
            Properties props = new Properties();
            OutputStream out = new FileOutputStream(Settings.SYSTEM_PATH +"/customerAVG.properties");
            synchronized (changedAVGCost) {
                Enumeration e = changedAVGCost.keys();
                while (e.hasMoreElements()) {
                    String sskey = (String) e.nextElement();
                    Float changedVG = changedAVGCost.get(sskey);
                    //System.out.println("Checking values :" + sskey + " = " + changedVG);
                    props.setProperty(sskey, "" + changedVG);

                }
                props.store(out, null);
            }
        } catch (Exception e) {
            System.out.println("Customer AVGCost prices not saved......");

        }

    }

    public void loadCustomerAVGCostSettings() {

        try {


            if (props == null) {
                props = new Properties();
                InputStream in = new FileInputStream(Settings.SYSTEM_PATH +"/customerAVG.properties");
                props.load(in);
                synchronized (props) {
                    Enumeration e = props.keys();//elements();// changedAVGCost.keys();
                    while (e.hasMoreElements()) {
                        String sskey = (String) e.nextElement();
                        setCustomerAverageCost(sskey, props.getProperty(sskey));
                        //System.out.println("Loading values :"+sskey+" = "+props.getProperty(sskey));

                    }
                }
            } else {
                synchronized (props) {
                    Enumeration e = props.keys();//elements();// changedAVGCost.keys();
                    while (e.hasMoreElements()) {
                        String sskey = (String) e.nextElement();
                        setCustomerAverageCost(sskey, props.getProperty(sskey));
                        //System.out.println("Loading values :"+sskey+" = "+props.getProperty(sskey));

                    }
                }

            }


        } catch (Exception e) {
            System.out.println("Loading CustomerAVG properties failed....");
        }

    }

//    public static boolean isContainedSymbol(String symbol) {
//        TransactRecord valRec = null;
//        for (int i = 0; i < transactionRecords.size(); i++) {
//            valRec = transactionRecords.get(i);
//            if (PortfolioInterface.getSymbolFromKey(valRec.getKey()).equals(symbol)) {
//                valRec = null;
//                return true;
//            }
//            valRec = null;
//        }
//        return false;
//    }

    public static boolean isContainedKey(String sKey) {
        TransactRecord valRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            valRec = transactionRecords.get(i);
            if (valRec.getKey().equals(sKey)) {
                valRec = null;
                return true;
            }
            valRec = null;
        }
        return false;
    }

    public static TradeKey getKeyForValidSymbol(String symbol) {
        TransactRecord valRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            valRec = transactionRecords.get(i);
            if (valRec.getKey().getSymbol().equals(symbol)) {
                return valRec.getKey();
            }
            valRec = null;
        }
        return null;
    }

    public static TradeKey[] getCurrentSymbolList() {
        TradeKey[] list = new TradeKey[valuationRecords.size()];
        ValuationRecord valRec = null;
        for (int i = 0; i < valuationRecords.size(); i++) {
            valRec = valuationRecords.get(i);
            list[i] = valRec.getSKey();
            valRec = null;
        }
        return list;
    }

    public static TradeKey[] getAllSymbolList() {
        TradeKey[] newArray = null;
        ArrayList newList = new ArrayList();
        TransactRecord valRec = null;
        for (int i = 0; i < transactionRecords.size(); i++) {
            valRec = transactionRecords.get(i);
            if (!newList.contains(valRec.getKey())) {
                newList.add(valRec.getKey());
            }
            valRec = null;
        }

        newArray = new TradeKey[newList.size()];
        for (int i = 0; i < newArray.length; i++) {
            newArray[i] = (TradeKey) newList.get(i);
        }
        newList = null;
        return newArray;
    }

    private void setCustomerAverageCost(String sskey, String value) {
        String splitPattern = "@";
        String[] tokens = sskey.split(splitPattern);
        try {
            TransactRecord trecord =  getTransactRecord(tokens[0], tokens[1]);
            if (trecord.getQuantity() != 0) {
                if (!trecord.addCostomerAverageCost) {
                    //trecord.setRealAverageCost(trecord.getPrice());

                    trecord.setIschanged(true);
                    //props.setProperty(tokens[0] + "@" + trecord.getPfID(),value);
                } else {
                }
                //trecord.setPrice(Float.parseFloat(value));
                trecord.setCustomerAVGPPrice(Float.parseFloat(value));
                changedAVGCost.put(tokens[0] + "@" + trecord.getPfID(), Float.parseFloat(value));
                props.setProperty(tokens[0] + "@" + trecord.getPfID(), value);
            }
        } catch (Exception e) {
            System.out.println("Loading Customer average cost fail....");
            e.printStackTrace();

        }
    }

    public ArrayList<TradingPortfolioRecord> getPortfoliosForAccount(String accountID) {
        ArrayList<TradingPortfolioRecord> portfolios = new ArrayList<TradingPortfolioRecord>();
        for (TradingPortfolioRecord portfolioRecord : portfolioList) {
            if (portfolioRecord.getAccountNumber().equals(accountID)) {
                portfolios.add(portfolioRecord);
            }
        }
        return portfolios;
    }
    // for testing by Shanaka
    private class PortfolioSettings implements Serializable {
        public String baseCurrency = "";
    }

    public TransactRecord findTransaction(String portfolioID, String key) {
        try {
            for (TransactRecord transactionRecord : transactionRecords) {
                try {
                    if ((transactionRecord.getKey().equals(key)) && (transactionRecord.getPfID().equals(portfolioID))) {
                        return transactionRecord;
                    }
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public String getPortfolioXML() {
        StringBuilder buffer = new StringBuilder();
        for (TradingPortfolioRecord portflio : portfolioList) {
            buffer.append(portflio.getXML());
        }

        return buffer.toString();
    }

    public String getPortfolioDataXML() {
        StringBuilder buffer = new StringBuilder();
        for (ValuationRecord portflio : getAllValuationRecords()) {
            buffer.append(portflio.getXML());
        }

        return buffer.toString();
    }





    public synchronized void createPfWiseValutionLists(String[] portfolioIDs, Date endDate){

       // ValuationRecord valRec = null;

        //aPFWisevalRecords.clear();
        ArrayList<ValuationRecord> aPFWisevalRecords ;

        if (hashPFWiseValReocrds!=null){
            hashPFWiseValReocrds.clear();
        }


        if (portfolioIDs != null)
            selectedPortfolios = portfolioIDs;

        if (selectedPortfolios == null)
            return;



        int iNoOfSelPfs =  selectedPortfolios.length;
        for (int itrSelPfs = 0 ; itrSelPfs <  iNoOfSelPfs;itrSelPfs ++  ){
           String selPf =  selectedPortfolios[itrSelPfs];
             aPFWisevalRecords = null;
             aPFWisevalRecords   = new ArrayList<ValuationRecord>();

           for (int i = 0; i < transactionRecords.size(); i++) {
                ValuationRecord valRec = null;
                TransactRecord pfRec = null;

               pfRec = (TransactRecord) transactionRecords.get(i);

               //relevency*********************
                boolean isNotRelevant = false;
                 if (endDate == null)
                    endDate = new Date();
                Date recordedDate = new Date(pfRec.getTxnDate());
                if (recordedDate.after(endDate)) {
                    isNotRelevant = true;
                }
                   recordedDate = null;



               if (isNotRelevant) {
                   isNotRelevant = false;
                   continue;
               }

               //**********************relevency



              if ((pfRec.getPfID()).trim().equals(selPf.trim())){
                  valRec = setValutionRecord(pfRec);
                  aPFWisevalRecords.add(valRec);
                  pfRec = null;
              }

           }
          //  System.out.println("sathinitapfarraysize:  " + aPFWisevalRecords.size());
            hashPFWiseValReocrds.put(selPf, aPFWisevalRecords);
            //System.out.println("sathAftertapfarraysize:  " + aPFWisevalRecords.size());


        }

        //testing

    }

     //default currency valution list
    public synchronized void createDCValList(String[] portfolioIDs, Date endDate){

          // ValuationRecord valRec = null;
           //aPFWisevalRecords.clear();
       aModifidValRecords.clear();
       aPfValRecords.clear();
       aPfsizes.clear();
        boolean isNotRelevant = false;
       aSelPfNames.clear();
       if (portfolioIDs != null)
           selectedPortfolios = portfolioIDs;

       if (selectedPortfolios == null)
           return;
       int iNoOfSelPfs =  selectedPortfolios.length;
        
       for (int itrSelPfs = 0 ; itrSelPfs <  iNoOfSelPfs; itrSelPfs ++  ){
       String selPf =  selectedPortfolios[itrSelPfs];
       aSelPfNames.add(getPortfolioName(selPf));

       PfValutionRecord  pfDetail = new PfValutionRecord(null,PfValutionRecord.PORTFOLIODETAIL);
        //selPf =   + selPf;
        pfDetail.setPfName(getPortfolioName(selPf));
        pfDetail.setPfID(selPf);

        pfDetail.setCurrency(getPortfolioCurrency(selPf));
        pfDetail.setPrePfNo(itrSelPfs );

            aPfValRecords.add(pfDetail);
            int iPrePfNo = aPfValRecords.size() ;  //pf records starts in this index
             int iNoOfpfs = 0;

              for (int i = 0; i < transactionRecords.size(); i++) {
                   ValuationRecord valRec = null;
                   TransactRecord pfRec = null;
                   PfValutionRecord pfTransRecord = null;
                  pfRec = (TransactRecord) transactionRecords.get(i);

                  
                  //relevency*********************
                  //**********************relevency
                  /**releven*/


               if (endDate == null)
                endDate = new Date();
              Date recordedDate = new Date(pfRec.getTxnDate());
            if (recordedDate.after(endDate)) {
                isNotRelevant = true;
            }
            recordedDate = null;

            /*if ((pfRec.getQuantity() <= 0) && (pfRec.getOpenBuyCount() <= 0) && (pfRec.getOpenSellCount() <= 0)) {
                isNotRelevant = true;
            }*/

            if ((pfRec.getQuantity() < 0) && (pfRec.getOpenBuyCount() <= 0) && (pfRec.getOpenSellCount() <= 0) &&
                    (pfRec.getDayMarginHoldings() ==0) && (pfRec.getMarginHoldings() ==0) && (pfRec.getDayHolding() ==0)) {
                isNotRelevant = true;
            }

//            }
            // Ignore the current record
            if (isNotRelevant) {
                isNotRelevant = false;
                continue;
            }

              /**********************************/


                 if ((pfRec.getPfID()).trim().equals(selPf.trim())){
                     valRec = setValutionRecord(pfRec);

                     pfTransRecord = new  PfValutionRecord (valRec,PfValutionRecord.VALRECORD);

                     aModifidValRecords.add(valRec);
                     aPfValRecords.add(pfTransRecord);
                     iNoOfpfs++;
                     pfRec = null;
                 }

                                                        
              }
               Integer oINoofPfs = new Integer(iNoOfpfs);
               //hashPfRecoredssize.put(selPf,oINoofPfs);
               aPfsizes.add(oINoofPfs);
               //todo ashould add the total record here tothe val record object  for corersopdding fields

               PfValutionRecord pfTotal = new PfValutionRecord(null,PfValutionRecord.TOTOAL);
               pfTotal.setPrePfNo(iPrePfNo);
               aPfValRecords.add(pfTotal);
               PfValutionRecord pfEmpty = new PfValutionRecord(null,PfValutionRecord.EMPTYROW);
               aPfValRecords.add(pfEmpty);
             //  System.out.println("sathinitapfarraysize:  " + aPFWisevalRecords.size());
               //System.out.println("sathAftertapfarraysize:  " + aPFWisevalRecords.size());
           }

           //testing
    //       printPFWiseHash();
      //     printPfsizes();

       }

    public static int getRowType(int row ){
        try{
           return   aPfValRecords.get(row ).getRecType();
        }catch(Exception e){
           return -1;
        }

    }

    public ArrayList<TransactRecord> getTransactionRecordsForPortfolio(String pfId){
        ArrayList<TransactRecord> list = new ArrayList<TransactRecord>();
        if(pfId!= null && (!pfId.trim().isEmpty())){
            for(TransactRecord record: transactionRecords){
                if(record.getPfID().equalsIgnoreCase(pfId)){
                    list.add(record);
                }
            }

        }
       return list;
    }

}


//backup
/* public void createPfWiseValutionLists(String[] portfolioIDs, Date endDate){

       // ValuationRecord valRec = null;

        //aPFWisevalRecords.clear();
        ArrayList<ValuationRecord> aPFWisevalRecords ;

        if (hashPFWiseValReocrds!=null){
            hashPFWiseValReocrds.clear();
        }


        if (portfolioIDs != null)
            selectedPortfolios = portfolioIDs;

        if (selectedPortfolios == null)
            return;



        int iNoOfSelPfs =  selectedPortfolios.length;
        for (int itrSelPfs = 0 ; itrSelPfs <  iNoOfSelPfs;itrSelPfs ++  ){
           String selPf =  selectedPortfolios[itrSelPfs];
             aPFWisevalRecords = null;
             aPFWisevalRecords   = new ArrayList<ValuationRecord>();

           for (int i = 0; i < transactionRecords.size(); i++) {
                ValuationRecord valRec = null;
                TransactRecord pfRec = null;

               pfRec = (TransactRecord) transactionRecords.get(i);

               //relevency*********************
                boolean isNotRelevant = false;
                 if (endDate == null)
                    endDate = new Date();
                Date recordedDate = new Date(pfRec.getTxnDate());
                if (recordedDate.after(endDate)) {
                    isNotRelevant = true;
                }
                   recordedDate = null;



               if (isNotRelevant) {
                   isNotRelevant = false;
                   continue;
               }

               //**********************relevency


              if (pfRec.getPfID().equals(selPf)){
                  valRec = setValutionRecord(pfRec);
                  aPFWisevalRecords.add(valRec);
                  pfRec = null;
              }

           }
            System.out.println("sathinitapfarraysize:  " + aPFWisevalRecords.size());
            hashPFWiseValReocrds.put(selPf, aPFWisevalRecords);
            System.out.println("sathAftertapfarraysize:  " + aPFWisevalRecords.size());


        }

        //testing
        printPFWiseHash();





    }*/


//backup 2

   /* public  synchronized void createPfWiseValutionLists(String[] portfolioIDs, Date endDate){


         if (portfolioIDs != null)
            selectedPortfolios = portfolioIDs;

        if (selectedPortfolios == null)
            return;

        // ValuationRecord valRec = null;

        //aPFWisevalRecords.clear();
      //  ArrayList<ValuationRecord> aPFWisevalRecords  = new ArrayList<ValuationRecord>();

        System.out.println("sathincreatepf");
        int iNoOfPfs = selectedPortfolios.length;

        for (int i = 0 ; i< iNoOfPfs;i++){

            String selPfId = selectedPortfolios[i];

            for (int j = 0; j < transactionRecords.size(); j++) {

               TransactRecord pfRec = null;
               pfRec = (TransactRecord) transactionRecords.get(j);
               String trasnKey  = pfRec.getPfID();

              //trim
               trasnKey = trasnKey.trim();
               selPfId = selPfId.trim();


                if (trasnKey.equals(selPfId)){

                    System.out.println( selPfId  + "::::sathsymbol2" + pfRec.getSymbol());


                }


            }




        }


} */