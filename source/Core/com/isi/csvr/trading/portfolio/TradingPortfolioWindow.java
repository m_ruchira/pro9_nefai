package com.isi.csvr.trading.portfolio;

import bsh.EvalError;
import bsh.Interpreter;
import com.isi.csvr.*;
import com.isi.csvr.commission.CommissionConfig;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.portfolio.PFStore;
import com.isi.csvr.print.PrintManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabEvent;
import com.isi.csvr.tabbedpane.TWTabbedPaleListener;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.BrokerConfig;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.*;
import com.isi.csvr.trading.shared.*;
import com.isi.csvr.wicalculator.WhatiFCalculatorPortfolioFrame;
import com.isi.util.FlexFlowLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import java.awt.*;
import java.awt.event.*;
import java.io.FileWriter;
import java.util.*;


/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class TradingPortfolioWindow
        extends InternalFrame
        implements ItemListener, PopupMenuListener,
        ActionListener, MouseListener, Themeable, TWDesktopInterface, CurrencyListener,
        InternalFrameListener, WindowWrapper, TraderProfileDataListener,
        TradingConnectionListener, TWTitlePaneInterface, ApplicationListener, TWTabbedPaleListener {


    public static final int VALUATION = 1;
    public static final int TRANS_HISTORY = 2;

    public static final String VALUATION_PANEL = "VALUATION";
    public static final String HISTORY_PANEL = "HISTORY";

    private int windowType;
    private int windowIndex = TWDesktopInterface.UNASSIGNED;
    private String[] currencyIDs;
    private JPanel toolBarPanel;
    private JPanel tablePanel;
    //  default currency mode
    private JPanel dCTablePnl;   //default currency table panel
    private JPanel valuationPanel;
    private JPanel dCValuationPanel;


    private TWMenuBar g_mnuMain = new TWMenuBar();
    private JMenu g_mnuFile = new JMenu();
    private JMenu g_mnuView = new JMenu();
    private JMenu g_mnuAnalysis = new JMenu();
    private JMenu g_mnuTools = new JMenu();
    private TWMenu mnuTrade = new TWMenu();


    private TWMenuItem mnuTradeItem;
    private TWMenuItem mnuSqureOffItem;
    private TWMenuItem mnuTradeAll;
    private TWMenuItem mnuOrderHistory;


    //default currency mode
    private TWMenuItem mnuDcTradeItem;
    private TWMenuItem mnuDCSqureOffItem;
    private TWMenuItem mnuDCTradeAll;
    private TWMenuItem mnuDCOrderHistory;

    //default currency decimal
    private TWMenu mnuDCDecimalPlaces;
    private TWRadioButtonMenuItem mnuDCNoDecimal;
    private TWRadioButtonMenuItem mnuDCDefaultDecimal;
    private TWRadioButtonMenuItem mnuDCOneDecimal;
    private TWRadioButtonMenuItem mnuDCTwoDecimal;
    private TWRadioButtonMenuItem mnuDCThrDecimal;
    private TWRadioButtonMenuItem mnuDCFouDecimal;
    private TWRadioButtonMenuItem mnuDCFiveDecimal;
    private TWRadioButtonMenuItem mnuDCSixDecimal;
//

    //unified currency mode
    private TWMenu mnuDecimalPlaces;
    private TWRadioButtonMenuItem mnuNoDecimal;
    private TWRadioButtonMenuItem mnuDefaultDecimal;
    private TWRadioButtonMenuItem mnuOneDecimal;
    private TWRadioButtonMenuItem mnuTwoDecimal;
    private TWRadioButtonMenuItem mnuThrDecimal;
    private TWRadioButtonMenuItem mnuFouDecimal;
    private TWRadioButtonMenuItem mnuFiveDecimal;
    private TWRadioButtonMenuItem mnuSixDecimal;


    private TWMenuItem g_mnuPrintPF;
    private TWMenuItem g_mnuReports;
    private TWMenuItem mnuCommission;
    private ButtonGroup btnGroupCurrency;
    private JPopupMenu titleBarMenu;
    private JPopupMenu currencyPopup;
    private JButton btnPortfolioReview;
    private JButton btnPrint;
    private JToggleButton toggleIncludeCash; // = new JToggleButton();
    private JLabel lblAvailablePFs = new JLabel();
    private JLabel btnAvailablePFs = new JLabel();
    private JPanel pnlAvailablePFs = new JPanel(new FlowLayout(FlowLayout.CENTER, 1, 1));


    private JLabel lblCashBalDesc;
//    private JLabel lblCashBalCurrency;
//    private static JLabel lblCashBal = new JLabel();

    private JLabel lblSelectedCurrencyID;// = new JLabel();
    private JLabel lblCurrencyIDs = new JLabel();
    private JLabel btnCurrencyIDs = new JLabel();
    private JPanel pnlCurrencyIDs = new JPanel(new FlowLayout(FlowLayout.TRAILING, 1, 1));
    private JLabel lblFilterDesc = new JLabel();
    private JLabel lblApplyFilter = new JLabel();


    private TradingPortfolioWindow selfRef;
    private ValuationModel valuationModel;
    private DCValuationModel dCValuationModel;


    private TWTabbedPane tabbedPane;
    private Table valuationTable;
    private Table dCValTable;


    //private Table dCValTable;

    private TradePortfolios dataStore;
    private ViewSetting oSettings1;
    private ViewSetting oSettings2;

    private String[] selectedPFIDs;
    private int selectedPanel;
    private boolean isRegisteredSymbols;
    private TradeKey[] oldSymbolList;
    //    private boolean isDefault = false;
    private byte decimalPlaces = Constants.ONE_DECIMAL_PLACES;
    private byte dcDecimalPlaces = Constants.ONE_DECIMAL_PLACES;

    private WhatiFCalculatorPortfolioFrame wiframe;

    private boolean initialSecondaryPathConnection = true;

    private String orderValue = null;
    private String commission = null;
    private String netValue = null;
    private String initMargin = null;
    private String maintMargin = null;
    private String vat = null;

    //sath
    public static boolean isDefCurMode = false;
    private static boolean isTradeConnected = false;
    private static boolean isTradeConnectedSecondary = false;

    //shanaka
    private static TWButton btnSave = new TWButton(Language.getString("SAVE"));
    private Date date;

    private int layer = -1;

    public TradingPortfolioWindow(ViewSetting oSettings1, ViewSetting oSettings2) {
        lblSelectedCurrencyID = new JLabel() {
            public void setText(String text) {
                super.setText(text);
            }
        };
        isRegisteredSymbols = true;
        selectedPanel = VALUATION;
        this.oSettings1 = oSettings1;
        this.oSettings2 = oSettings2;
        dataStore = TradePortfolios.getInstance();

        lblCashBalDesc = new JLabel();
        lblCashBalDesc.setToolTipText("");
        lblCashBalDesc.addMouseListener(this);
//        lblCashBalCurrency = new JLabel("");
        currencyIDs = new String[0];
        createLayout();
        setTextLabels();
        lblSelectedCurrencyID.setText(dataStore.getBaseCurrency());
        GUISettings.applyOrientation(this);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        Theme.registerComponent(this);
        CurrencyStore.getSharedInstance().addCurrencyListener(this);
    }

    private void createLayout() {
//        this.setFrameIcon(new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif")));
        this.setTitle(Language.getString("PORTFOLIO")); // "Portfolio Manager");
        this.getContentPane().setLayout(new BorderLayout());
        selfRef = this;
        createTitlebarManu();
        createManinMenu(); // menu created but not added to the window
        createToolbar();
        createValuationTable();
        createDCValuationTable();

        //tabbed pane default curency mode + unified currency mode                                     QT3HMM
//         tabbedPane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "tp");
        tabbedPane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "nw");

        // ImageIcon icon = createImageIcon("images/middle.gif");
        JPanel defaultCurModePanel = new JPanel();
        defaultCurModePanel.setLayout(new BorderLayout());
        JPanel unifiedCurModePnl = new JPanel();
        unifiedCurModePnl.setLayout(new BorderLayout());


        tabbedPane.addTab(Language.getString("PORTFOLIO_COMBINED_MODE"), unifiedCurModePnl);
        tabbedPane.addTab(Language.getString("PORTFOLIO_DEFAULT_MODE"), defaultCurModePanel);
        tabbedPane.showTab(0);


        tabbedPane.addTabPanelListener(this);


        int selectedIndex = tabbedPane.getSelectedIndex();
        if (selectedIndex == 1) {
            isDefCurMode = true;
            pnlCurrencyIDs.setEnabled(false);
            pnlCurrencyIDs.setVisible(false);
            lblSelectedCurrencyID.setVisible(false);
//            lblCurrencyIDs.setEnabled(false);
//            btnCurrencyIDs.setEnabled(false);
        } else {
            isDefCurMode = false;
            pnlCurrencyIDs.setEnabled(true);
            pnlCurrencyIDs.setVisible(true);
            lblSelectedCurrencyID.setVisible(true);
//            lblCurrencyIDs.setEnabled(true);
//            btnCurrencyIDs.setEnabled(true);
        }

        // getSelected

        JPanel northBasePanel = new JPanel();
        String[] widths = {"0", "0", "0", "0", "100%", "150", "0", "0"};//, "100", "40"};
        northBasePanel.setLayout(new FlexFlowLayout(widths)); //new GridLayout(2, 1));

        // Create available Portfolio List
        pnlAvailablePFs.setPreferredSize(new Dimension(90, 24));
        btnAvailablePFs.setPreferredSize(new Dimension(10, 22));
        btnAvailablePFs.setIcon(new ImageIcon("images/Common/graph_down_pop.gif"));

        btnAvailablePFs.addMouseListener(this);
        lblAvailablePFs.addMouseListener(this);

        pnlAvailablePFs.add(lblAvailablePFs);
        pnlAvailablePFs.add(btnAvailablePFs);

        pnlAvailablePFs.setPreferredSize(new Dimension(120, 24));

        // Create Currency List
        pnlCurrencyIDs.setPreferredSize(new Dimension(90, 24));
        btnCurrencyIDs.setPreferredSize(new Dimension(10, 22));
        btnCurrencyIDs.setIcon(new ImageIcon("images/Common/graph_down_pop.gif"));
        //      btnCurrencyIDs.setEnabled(true);

        btnCurrencyIDs.addMouseListener(this);
        lblCurrencyIDs.addMouseListener(this);
//        lblCurrencyIDs.setEnabled(true);

        pnlCurrencyIDs.add(lblCurrencyIDs);
        pnlCurrencyIDs.add(btnCurrencyIDs);

        lblSelectedCurrencyID.setPreferredSize(new Dimension(80, 20));


//        lblCashBal.setFont(Theme.getDefaultFont(Font.BOLD, 12));
//        lblCashBal.setHorizontalAlignment(JLabel.LEADING);
//        lblCashBal.setPreferredSize(new Dimension(100, 20));
        final TWButton btnRefresh = TradeMethods.getPortfolioRefresh();
        btnRefresh.setVisible(false);
        btnRefresh.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new Thread("Blotter Refresh Button") {
                    public void run() {
                        try {
                            TradeMethods.setPfRefreshClicked(true);
                            if (TWControl.clearDataBeforeRefresh()) {
                                valuationModel.clear();
                                dCValuationModel.clear();
                                // if want can add
//                                valuationTable.getTable().updateUI();
//                                dCValTable.getTable().updateUI();
                            }
                            TradeMethods.requestPortfolioData(Constants.PATH_PRIMARY);
                            TradeMethods.requestAccountData(Constants.PATH_PRIMARY);
                            Thread.sleep(5000);
                            TradeMethods.enableRefreshButtons(true);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                }.start();
            }
        });

        btnSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int selectedIndexSave = tabbedPane.getSelectedIndex();
                if (selectedIndexSave == 0) {
                    exportDataToExcelSheet(valuationTable);
                } else {
                    exportDataToExcelSheet(dCValTable);
                }
            }
        });

        northBasePanel.add(toolBarPanel);
        northBasePanel.add(pnlAvailablePFs);
        northBasePanel.add(pnlCurrencyIDs);
        northBasePanel.add(lblSelectedCurrencyID);
        northBasePanel.add(new JLabel());
        northBasePanel.add(lblCashBalDesc);
        northBasePanel.add(btnSave);
        northBasePanel.add(btnRefresh);
        tablePanel = new JPanel(new CardLayout(1, 1));
        dCTablePnl = new JPanel(new CardLayout(1, 1));

        valuationPanel = new JPanel(new BorderLayout());
        dCValuationPanel = new JPanel(new BorderLayout());
        valuationPanel.add(valuationTable, BorderLayout.CENTER);
        //todo  put a defvalution table
        dCValuationPanel.add(dCValTable, BorderLayout.CENTER);
//        JButton  defValbutton = new JButton("defvalButon");
        //   dCValuationPanel.add(defValbutton, BorderLayout.CENTER);
        //dCValuationPanel.add(defValbutton, BorderLayout.CENTER);
        //dCValuationPanel.add(defValbutton, BorderLayout.CENTER);
        //  dCValTable = new Table();
        //dCValTable.setModel();


        //dCValuationPanel.add(dCValTable, BorderLayout.CENTER);


        createDecimalMenu(valuationTable.getPopup());
        createDCDecimalMenu(dCValTable.getPopup());

        //add decimal menus to default currency table

        try {
            decimalPlaces = Byte.parseByte(oSettings1.getProperty(ViewConstants.VC_DECIMAL_PLACES));
            dcDecimalPlaces = Byte.parseByte(oSettings2.getProperty(ViewConstants.VC_DECIMAL_PLACES));
        } catch (Exception e) {
            decimalPlaces = TWControl.getPortfolioDecimalCount();
            dcDecimalPlaces = TWControl.getPortfolioDecimalCount();
        }

        setDecimalPlaces(decimalPlaces);
        setDCDecimalPlaces(dcDecimalPlaces);

        createCustomAverageCostMenu(valuationTable.getPopup());
        createDCCustomAverageCostMenu(dCValTable.getPopup());

        //what if calculator
        createWhatIfCalculatormenu(valuationTable.getPopup());
        createDCWhatIfCalculatormenu(dCValTable.getPopup());


        lblApplyFilter.addMouseListener(this);
        tablePanel.add(VALUATION_PANEL, valuationPanel);
        //todo string corrosponding to VALUATION_PANEL
        dCTablePnl.add("defValPanel", dCValuationPanel);

        //common to both curency modes
//        unifiedCurModePnl.add(northBasePanel, BorderLayout.NORTH);
//        defaultCurModePanel.add(northBasePanel, BorderLayout.NORTH);


        //should change the table according to the currency mode
        defaultCurModePanel.add(dCTablePnl, BorderLayout.CENTER);
        unifiedCurModePnl.add(tablePanel, BorderLayout.CENTER);
        //?
        this.getContentPane().add(northBasePanel, BorderLayout.NORTH);
        this.getContentPane().add(tabbedPane, BorderLayout.CENTER);

        this.setViewSetting(this.oSettings1);

        g_mnuFile.setOpaque(false);
        g_mnuView.setOpaque(false);
        g_mnuAnalysis.setOpaque(false);
        g_mnuTools.setOpaque(false);
        TradeMethods.checkRefreshButton();
        this.addInternalFrameListener(this);
        Application.getInstance().addApplicationListener(this);

    }

    private String copyHeaders(Table table) {
        int cols = 0;
        StringBuffer buffer = new StringBuffer("");

        cols = table.getTable().getColumnCount();


        for (int i = 0; i < cols; i++) {
            int modalIndex = table.getTable().convertColumnIndexToModel(i);
            if (table.getTable().getColumn("" + modalIndex).getWidth() != 0) {
                String header = table.getTable().getColumn("" + modalIndex).getHeaderValue().toString();

                //buffer.append("<b>");
                buffer.append(header);
                //buffer.append("</b>");
                buffer.append(",");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public String saveExcelFileAs() {
        int status = 0;
        String sFile;
        String filePathCharts = "./";
        JFileChooser oFC = new JFileChooser(filePathCharts);
        oFC.setFileSelectionMode(JFileChooser.FILES_ONLY);
        oFC.setDialogTitle(Language.getString("SCANNER_SAVE_RESULTS"));
        ExtensionFileFilter oFilter1 = new ExtensionFileFilter("CSV format", "csv");
        ExtensionFileFilter oFilter2 = new ExtensionFileFilter("Microsoft excel 2007 format", "xlsx");

        oFC.setFileFilter(oFilter1);
//        oFC.setFileFilter(oFilter2); //2007 excel (xlss) format

        oFC.setAcceptAllFileFilterUsed(false);
        status = oFC.showSaveDialog(this);
        if (status == JFileChooser.APPROVE_OPTION) {
            sFile = oFC.getSelectedFile().getAbsolutePath();
            filePathCharts = oFC.getCurrentDirectory().getAbsolutePath();
            if (oFC.getFileFilter() == oFilter1) {
                if (!sFile.toLowerCase().endsWith(".csv"))
                    sFile += ".csv";
            } else if (oFC.getFileFilter() == oFilter2) {
                if (!sFile.toLowerCase().endsWith(".xlsx"))
                    sFile += ".xlsx";
            }
            return sFile;
        } else {
            return null;
        }
    }

    public static boolean isInteger(String str) {
        if (str == null) {
            return false;
        }
        int length = str.length();
        if (length == 0) {
            return false;
        }
        int i = 0;
        if (str.charAt(0) == '-') {
            if (length == 1) {
                return false;
            }
            i = 1;
        }
        for (; i < length; i++) {
            char c = str.charAt(i);
            if (c <= '/' || c >= ':') {
                return false;
            }
        }
        return true;
    }

    private void exportDataToExcelSheet(Table table) {

        StringBuffer buffer = new StringBuffer("");
        buffer.append(copyHeaders(table));
        int rows = table.getTable().getRowCount();
        int cols = table.getTable().getColumnCount();
        TWDecimalFormat f;
        TWDateFormat fdate;
        TWDateFormat g_oDateTimeFormatHM;
        for (int r = 0; r < rows; r++) {
            for (int c = 0; c < cols; c++) {
                int modelIndex = table.getTable().convertColumnIndexToModel(c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    Class cls = table.getTable().getColumnClass(c);
                    Object val = table.getTable().getValueAt(r, c);
                    Object val2 = table.getTable().getValueAt(r, -12);
                    System.out.println("Column: " + c);
                    System.out.println("Row: " + r);
                    System.out.println("Model Index: " + modelIndex);
                    System.out.println("Column class: " + cls.toString());
                    System.out.println("Value: " + val.toString());
                    String temp = "";
                    if (val2.equals(DCValuationModel.COMBINE) && c != 0) {
                        val = "";
                        cls = table.getTable().getColumnClass(0);
                    }
                    if (val != null) {
                        temp = val.toString();
                        if (cls == Double.class || cls == Float.class) {
                            if (!temp.equals("")) {
                                f = new TWDecimalFormat("#####0.00");
                                temp = f.format(Double.parseDouble(temp));
                            }
                        } else if (cls == String.class) {
                            /*if (isInteger(temp)) {
                                f = new TWDecimalFormat("#####0.00");
                                temp = f.format(Double.parseDouble(temp));
                            }*/
                            if (!temp.equals("")) {
                                temp = temp.replaceAll("(?i)<script.*?</script>", "");
                            }
                        } else if (cls == Object.class) {
                            long longValue;
                            date = new Date();
                            Object value = table.getTable().getValueAt(r, c);
                            g_oDateTimeFormatHM = new TWDateFormat("dd/MM/yyyy - HH:mm:ss");
                            try {
                                longValue = toLongValue(value);
                                date.setTime(longValue);
                                temp = g_oDateTimeFormatHM.format(date);
                            } catch (Exception e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }
                        }

                        buffer.append(temp);
                        buffer.append(" ");
                        buffer.append(",");

                    }
                }
            }
            buffer.append("\n");
        }

        //System.out.println(" =============== BUFFER ============");
        //System.out.println(buffer.toString());

        try {
            String path = saveExcelFileAs();
            FileWriter writer = new FileWriter(path);
            writer.write(buffer.toString());
            writer.close();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    public void tabStareChanged(TWTabEvent event) {
        // This method is called whenever the selected tab changes
        //JTabbedPane pane = (JTabbedPane)evt.getSource();
        // Get current tab
        int sel = tabbedPane.getSelectedIndex();

        if (sel == 1) {
            isDefCurMode = true;
            pnlCurrencyIDs.setEnabled(false);
            pnlCurrencyIDs.setVisible(false);
            lblSelectedCurrencyID.setVisible(false);
//            lblCurrencyIDs.setEnabled(false);
//            btnCurrencyIDs.setEnabled(false);
        } else {
            isDefCurMode = false;
            pnlCurrencyIDs.setEnabled(true);
            pnlCurrencyIDs.setVisible(true);
            lblSelectedCurrencyID.setVisible(true);
//            lblCurrencyIDs.setEnabled(true);
//            btnCurrencyIDs.setEnabled(true);
        }
    }

    private void setTextLabels() {
        lblAvailablePFs.setText(Language.getString("PORTFOLIO_LIST"));
        lblCurrencyIDs.setText(Language.getString("CURRENCY"));
        lblCashBalDesc.setText(Language.getString("ACCOUNT_SUMMARY_HTML"));
        lblCashBalDesc.setCursor(new Cursor(Cursor.HAND_CURSOR));
//        lblCashBal.setText("0.00");
        lblApplyFilter.setText(Language.getString("APPLY_FILTER"));
        lblFilterDesc.setText("");
        g_mnuPrintPF.setText(Language.getString("PRINT"));
        g_mnuView.setText(Language.getString("VIEW"));
        g_mnuReports.setText(Language.getString("POERFOLIO_REVIEW"));
        g_mnuAnalysis.setText(Language.getString("ANALYSIS"));
        g_mnuFile.setText(Language.getString("FILE"));
        g_mnuTools.setText(Language.getString("TOOLS"));

        btnPortfolioReview.setToolTipText(Language.getString("PORTFOLIO_REPORT_TITLE"));
        btnPrint.setToolTipText(Language.getString("PRINT"));
        toggleIncludeCash.setToolTipText(Language.getString("INCLUDE_CASH_BALANCE"));
    }

    public void displayCashBalance() {
        try {
//            lblCashBal.setText(PFUtilities.getFormattedPrice(
//                    convertToSelectedCurrency(TradingShared.getTrader().getAccount(0).getCurrency(),
//                            TradingShared.getTrader().getAccount(0).getBuyingPower())));
        } catch (Exception e) {
        }

        try {
//            lblCashBalCurrency.setText(dataStore.getBaseCurrency());
        } catch (Exception e) {

        }
    }

    public double getCashBalanceForSelectedPFs() {
        try {
            double cashBalance = 0;
            Enumeration<Account> accounts = TradingShared.getTrader().getAccoutsOfPortfolios(selectedPFIDs);
            while (accounts.hasMoreElements()) {
                Account account = accounts.nextElement();
                cashBalance += convertToSelectedCurrency(account.getCurrency(), account.getBuyingPower(), account.getPath());
            }
            return cashBalance;
//            return TradingShared.getTrader().getAccount(0).getBuyingPower();
        } catch (Exception e) {
            return 0;
        }
    }

    public static float convertToSelectedCurrency(String sCurrency, float value, byte path) {
        return value * (float) CurrencyStore.getBuyRate(sCurrency, TradePortfolios.getInstance().getBaseCurrency(), path);
    }

    public static double convertToSelectedCurrency(String sCurrency, double value, byte path) {
        return value * CurrencyStore.getBuyRate(sCurrency, TradePortfolios.getInstance().getBaseCurrency(), path);

    }

    /**
     * Create the main menu
     */
    public JMenuBar createManinMenu() {
        g_mnuMain.setLayout(new BoxLayout(g_mnuMain, BoxLayout.LINE_AXIS));

        g_mnuPrintPF = new TWMenuItem(Language.getString("PRINT"));
        g_mnuPrintPF.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                printPortfolio();
            }
        });
        g_mnuFile.add(g_mnuPrintPF);

        g_mnuReports = new TWMenuItem(Language.getString("REPORTS"));
        final TradingPortfolioWindow pfWindow = this;
        g_mnuReports.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                PFReportFrame.showReport(pfWindow);
            }
        });
        g_mnuView.add(g_mnuReports);

        mnuCommission = new TWMenuItem(Language.getString("EDIT_COMMISSION"));
        mnuCommission.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                new CommissionConfig();
            }
        });
        //g_mnuTools.add(mnuCommission);

        g_mnuMain.add(g_mnuFile);
        g_mnuMain.add(g_mnuView);
//        g_mnuMain.add(g_mnuManage);
        g_mnuMain.add(g_mnuTools);

        return g_mnuMain;
    }

    private void loadDefaultPortfolio() {
        try {
            if (selectedPFIDs == null || initialSecondaryPathConnection) {
                ArrayList<TradingPortfolioRecord> records = TradePortfolios.getInstance().getPortfolioList();
                TradingPortfolioRecord record = null;
                String[] pfIDs = new String[records.size()];
                for (int i = 0; i < records.size(); i++) {
                    record = records.get(i);
                    if (record.isDefaultPortfolio()) {
                        pfIDs[i] = record.getPortfolioID();
                    }
                    /*if(i == 0){
                        lblSelectedCurrencyID.setText(record.getCurrencyID());
                        dataStore.setBaseCurrency(record.getCurrencyID());
                    }*/
                    record = null;
                }
                setSelectedPFIDs(pfIDs);

                //sath
                // if (isDefCurMode){
                dCValTable.getTable().updateUI();

                //}else {
                valuationTable.getTable().updateUI();
                //}


            } else {
                setSelectedPFIDs(selectedPFIDs);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
//        try {
//            if (selectedPFIDs == null) {
//                isDefault = true;
//                TradingPortfolioRecord record = dataStore.getDefaultPortfolio();
//                String[] pfIDs = {record.getPortfolioID()};
//                lblSelectedCurrencyID.setText(record.getCurrencyID());
//                dataStore.setBaseCurrency(record.getCurrencyID());
//                setSelectedPFIDs(pfIDs);
//                valuationTable.getTable().updateUI();
//                record = null;
//            }else{
//                setSelectedPFIDs(selectedPFIDs);
//            }
////            displayCashBalance();
//        } catch (Exception ex) {
//        }
    }


    public void setSelectedPFIDs(String[] pfIDs) {
        String sTitle = Language.getString("PORTFOLIO") + " - ";
        TradingPortfolioRecord pfRecord = null;
        selectedPFIDs = pfIDs;
        for (int i = 0; i < selectedPFIDs.length; i++) {
            pfRecord = TradePortfolios.getInstance().getPortfolio(selectedPFIDs[i]);
            if (pfRecord != null)
                sTitle += pfRecord.getName();
            if (i != (selectedPFIDs.length - 1))
                sTitle += pfRecord != null ? ", " : "";
        }
        if (sTitle.trim().endsWith(",")) {
            sTitle = sTitle.substring(0, sTitle.lastIndexOf(","));
        }
        if (sTitle.trim().endsWith("-")) {
            sTitle = sTitle.substring(0, sTitle.lastIndexOf("-"));
        }
        this.setTitle(sTitle);
        createFilteredTransactionList(null, PFUtilities.NONE, null, null);
        createValuationList();


    }

    public void createFilteredTransactionList(String sKey, byte type, Date startDate, Date endDate) {
//        selectedSKey      = sKey;
//        selectedType      = type;
//        selectedStartDate = startDate;
//        selectedEndDate   = endDate;
        dataStore.createFilteredTransactionList(selectedPFIDs, sKey, type, startDate, endDate);
    }

    public void createValuationList() {
        dataStore.createValuationList(selectedPFIDs, null);
        dataStore.createDCValList(selectedPFIDs, null);
        // dataStore.createPfWiseValutionLists(selectedPFIDs,null);
        valuationTable.getTable().updateUI();
        dCValTable.getTable().updateUI();
        activateCurrentSymbols();
    }

    public void createToolbar() {
        toolBarPanel = new JPanel();
        toolBarPanel.setLayout(new FlowLayout(FlowLayout.LEADING, 2, 2));

        toggleIncludeCash = new JToggleButton();
        toggleIncludeCash.setPreferredSize(new Dimension(25, 25));
        toggleIncludeCash.setBorder(new EmptyBorder(0, 0, 0, 0));
        toggleIncludeCash.addMouseListener(this);
//        toolBarPanel.add(toggleIncludeCash);

        btnPortfolioReview = new JButton();
        btnPortfolioReview.setPreferredSize(new Dimension(25, 25));
        btnPortfolioReview.setBorder(new EmptyBorder(0, 0, 0, 0));
        final TradingPortfolioWindow pfWindow = this;
        btnPortfolioReview.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                PFReportFrame.showReport(pfWindow);
            }
        });
        toolBarPanel.add(btnPortfolioReview);

        btnPrint = new JButton();
        btnPrint.setPreferredSize(new Dimension(25, 25));
        btnPrint.setBorder(new EmptyBorder(0, 0, 0, 0));
        btnPrint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                printPortfolio();
            }
        });
        toolBarPanel.add(btnPrint);

        loadButtonImages();
    }

    private void createTitlebarManu() {
        titleBarMenu = new JPopupMenu();
        TWMenuItem hideTitlemenu = new TWMenuItem(Language.getString("HIDE_TITLEBAR"));
        hideTitlemenu.setActionCommand("TITLE_HIDE");
        hideTitlemenu.addActionListener(this);
        titleBarMenu.add(hideTitlemenu);
        addTitlebarManu();
    }

    /*private void createHistoryTable() {
        ViewSetting oSettings;
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings = (ViewSetting)Client.getInstance().getViewSettingsManager().getPortfolioView(1280, "2").clone();
            if (oSettings == null)
                throw (new Exception("View not found"));
        } catch(Exception e) {
            e.printStackTrace();
            return;
        }
        GUISettings.setColumnSettings(oSettings, PortfolioInterface.getColumnSetting("PORTFOLIO_HISTORY_COLS"));
        oSettings.setTableNumber(2);
        oSettings.setParent(selfRef);
        Client.getInstance().getViewSettingsManager().addPortfolioSetting(oSettings);
    }*/

    private void createValuationTable() {
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings1 = (ViewSetting) Client.getInstance().getViewSettingsManager().getPortfolioView(1280, "4").clone();
            if (oSettings1 == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        GUISettings.setColumnSettings(oSettings1, PortfolioInterface.getColumnSetting("TRADING_PORTFOLIO_VALUATION_COLS"));

/*
        mnuTrade.setText(Language.getString("MUBASHER_TRADE"));
        mnuTrade.setBackground(Theme.getColor("BOARD_TABLE_CELL_TRADE_BGCOLOR1"));
        mnuTrade.setEnabled(false);
        TWMenuItem mnuBuy = new TWMenuItem(Language.getString("BUY"));
        mnuBuy.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        mnuBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().doTransaction(TradeMeta.BUY, getSelectedSymbol(), getSelectedHolding(), false, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_PORTFOLIO);
            }
        });
        mnuTrade.add(mnuBuy);
        TWMenuItem mnuSell = new TWMenuItem(Language.getString("SELL"));
        mnuSell.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
        mnuSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().doTransaction(TradeMeta.SELL, getSelectedSymbol(), getSelectedHolding(), false, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_PORTFOLIO);
            }
        });
        */

        mnuTradeItem = new TWMenuItem(Language.getString("LIQUIDATE"), "sell.gif");
        mnuTradeItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //todo get selected symbol from    def tale
                Client.getInstance().doTransaction(TradeMeta.SELL, getSelectedSymbol(), getSelectedHolding(),
                        false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_PORTFOLIO, getSelectedPortfolio(), getSelectedBookKeeper());
            }
        });

        mnuTradeAll = new TWMenuItem(Language.getString("LIQUIDATE_ALL"), "liquidateall.gif");
        mnuTradeAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                liquidateOrders();
            }
        });


        mnuSqureOffItem = new TWMenuItem(Language.getString("SQUARE_OFF"), "squareoff.gif");
        mnuSqureOffItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                long dayHoldings = getSelectedDayHoldings();
//                dayHoldings = -100;
                Stock stock = DataStore.getSharedInstance().getStockObject(getSelectedSymbol());
                if (dayHoldings > 0) {
                    calculateOrderValue(TradeMeta.SELL, dayHoldings);
                    calculateMarginValues(TradeMeta.SELL, dayHoldings);
                    String tiffStr = TradingShared.getTiffString(TradeMeta.TIF_DAY, null);
                    int i = TradeMethods.getNewOrderConfirmMsg(SharedMethods.getSymbolFromKey(getSelectedSymbol()), SharedMethods.getExchangeFromKey(getSelectedSymbol()),
                            SharedMethods.getInstrumentTypeFromKey(getSelectedSymbol()), 0d, stock.getDecimalCount(), 0, 0,
                            dayHoldings, null, null, 0f, TradeMeta.ORDER_TYPE_SQUARE_OFF, TradeMeta.SELL, TradeMeta.SELL, TradeMeta.CONDITION_TYPE_NONE, "", 0, true, tiffStr, getSelectedPortfolio(),
                            orderValue, commission, netValue, initMargin, maintMargin, null, 0, null, false, vat);


                    /*ShowMessage oMessage = new ShowMessage(TradeMethods.getConfirmMessage(SharedMethods.getSymbolFromKey(getSelectedSymbol()),
                            SharedMethods.getExchangeFromKey(getSelectedSymbol()), SharedMethods.getInstrumentTypeFromKey(getSelectedSymbol()), 0d, stock.getDecimalCount(), 0d, 0d, dayHoldings, null,null,0f,
                            TradeMeta.ORDER_TYPE_SQUARE_OFF, TradeMeta.SELL, TradeMeta.SELL, TradeMeta.CONDITION_TYPE_NONE,"",0,false), "I", true);
                    int i = oMessage.getReturnValue();*/
                    if (i == JOptionPane.OK_OPTION) {
                        TradeMethods.getSharedInstance().sendNewOrderForExec(false, false, 0, getSelectedPortfolio(), SharedMethods.getExchangeFromKey(getSelectedSymbol()),
                                SharedMethods.getSymbolFromKey(getSelectedSymbol()), TradeMeta.SELL, TradeMeta.ORDER_TYPE_SQUARE_OFF, 0d, stock.getMaxPrice(),
                                stock.getCurrencyCode(), (int) dayHoldings, TradeMeta.TIF_DAY, 0, 0, 0, true, stock.getStrikePrice(),
                                TradingShared.formatOpraExpireDate(stock.getExpirationDate()), stock.getInstrumentType(), 0, stock.getOptionBaseSymbol(),
                                -1, null, null, null, -1, false, -1, 0, -1, -1, false, 0, true, null, null, null, 0, null, 0);
                    }
//                    Client.getInstance().doTransaction(TradeMeta.SELL, getSelectedSymbol(), dayHoldings,
//                            false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_PORTFOLIO, getSelectedPortfolio(),true);
                } else if (dayHoldings < 0) {
                    calculateOrderValue(TradeMeta.BUY, -dayHoldings);
                    calculateMarginValues(TradeMeta.BUY, -dayHoldings);
                    String tiffStr = TradingShared.getTiffString(TradeMeta.TIF_DAY, null);
                    int i = TradeMethods.getNewOrderConfirmMsg(SharedMethods.getSymbolFromKey(getSelectedSymbol()), SharedMethods.getExchangeFromKey(getSelectedSymbol()),
                            SharedMethods.getInstrumentTypeFromKey(getSelectedSymbol()), 0d, stock.getDecimalCount(), 0, 0,
                            -dayHoldings, null, null, 0f, TradeMeta.ORDER_TYPE_SQUARE_OFF, TradeMeta.BUY, TradeMeta.BUY, TradeMeta.CONDITION_TYPE_NONE, "", 0, true, tiffStr, getSelectedPortfolio(),
                            orderValue, commission, netValue, initMargin, maintMargin, null, 0, null, false, vat);

                    /*ShowMessage oMessage = new ShowMessage(TradeMethods.getConfirmMessage(SharedMethods.getSymbolFromKey(getSelectedSymbol()),
                            SharedMethods.getExchangeFromKey(getSelectedSymbol()), SharedMethods.getInstrumentTypeFromKey(getSelectedSymbol()), 0d, stock.getDecimalCount(), 0d, 0d, -dayHoldings, null,null,0f,
                            TradeMeta.ORDER_TYPE_SQUARE_OFF, TradeMeta.BUY, TradeMeta.BUY, TradeMeta.CONDITION_TYPE_NONE,"",0,false), "I", true);
                    int i = oMessage.getReturnValue();*/
                    if (i == JOptionPane.OK_OPTION) {
                        TradeMethods.getSharedInstance().sendNewOrderForExec(false, false, 0, getSelectedPortfolio(), SharedMethods.getExchangeFromKey(getSelectedSymbol()),
                                SharedMethods.getSymbolFromKey(getSelectedSymbol()), TradeMeta.BUY, TradeMeta.ORDER_TYPE_SQUARE_OFF, 0d, stock.getMaxPrice(),
                                stock.getCurrencyCode(), -(int) dayHoldings, TradeMeta.TIF_DAY, 0, 0, 0, true, stock.getStrikePrice(),
                                TradingShared.formatOpraExpireDate(stock.getExpirationDate()), stock.getInstrumentType(), 0, stock.getOptionBaseSymbol(),
                                -1, null, null, null, -1, false, -1, 0, -1, -1, false, 0, true, null, null, null, 0, null,0);
                    }
//                    Client.getInstance().doTransaction(TradeMeta.BUY, getSelectedSymbol(), dayHoldings,
//                            false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_PORTFOLIO, getSelectedPortfolio(),true);
                }

            }
        });


        mnuOrderHistory = new TWMenuItem(Language.getString("ORDER_HISTORY"), "ordersearch.gif");
        mnuOrderHistory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                searchHistory();
            }
        });
        mnuTradeItem.setVisible(true);
//        mnuSqureOffItem.setVisible(true); // Mix Dubai
        mnuTradeAll.setVisible(false);
        /*if (BrokerConfig.getSharedInstance().isLiquidateAllAvailable()) {
        } else {
            mnuTradeAll.setVisible(false);
        }*/
        mnuOrderHistory.setVisible(true);
        oSettings1.setTableNumber(1);
        //valuationModel = new ValuationModel(dataStore.getValuationList());
        //valuationModel = new ValuationModel(dataStore.getPfValuationList());
        valuationModel = new ValuationModel(dataStore.getValuationList());

        valuationModel.setViewSettings(oSettings1);
        valuationTable = new Table();
        valuationTable.setThreadID(Constants.ThreadTypes.TRADING);
        /* if((this.getSelectedSymbol()==null) ){
             mnuTradeItem.setVisible(false);
             mnuOrderHistory.setVisible(false);
         }*/
        valuationTable.setSortingEnabled();
        //     dCValTable.setSortingEnabled();
        //    valuationTable.setSortingDisabled();
        valuationTable.setWindowType(ViewSettingsManager.PORTFOLIO_VALUATION);
        //valuationTable.getPopup().add(mnuTrade);
        valuationTable.getPopup().add(mnuTradeItem);
//        valuationTable.getPopup().add(mnuSqureOffItem);
        valuationTable.getPopup().add(mnuTradeAll);
        valuationTable.getPopup().add(mnuOrderHistory);
        valuationTable.getPopup().addPopupMenuListener(this);
        valuationModel.setPFWindow(this);

        oSettings1.setParent(selfRef);
        valuationTable.setModel(valuationModel);
        valuationModel.setTradingPortfolioTable(valuationTable, true);

        Theme.registerComponent(valuationTable);
        valuationTable.getTable().addMouseListener(this);
        valuationTable.getPopup().setPrintTarget(valuationTable.getTable(), PrintManager.PRINT_JTABLE);
        //      valuationTable.getPopup().enableUnsort(false);
        //      dCValTable.getPopup().enableUnsort(false);
        valuationTable.getPopup().showLinkMenus();
        valuationTable.getModel().updateGUI();
        GUISettings.applyOrientation(valuationTable.getPopup());
        Client.getInstance().getViewSettingsManager().addPortfolioSetting(oSettings1);
    }

    private void calculateMarginValues(int side, long qty) {
        try {
            String portfolio = getSelectedPortfolio();
            /*TWDecimalFormat currencyFormat = null;
            double initMargin = 0;
            boolean isDayOrder = false;
            try {
                Account account = TradingShared.getTrader().findAccountByPortfolio(portfolio);
                String currency = account.getCurrency();
                currencyFormat = SharedMethods.getCurrencyDecimalFormat(currency);
                initMargin = TradeMethods.getMarginForSymbol(SharedMethods.getSymbolFromKey(getSelectedSymbol()), SharedMethods.getExchangeFromKey(getSelectedSymbol()),
                             SharedMethods.getInstrumentTypeFromKey(getSelectedSymbol()),  portfolio, isDayOrder, account.getMarginLimit(), account.getDayMarginLimit());
            } catch (Exception e) {

            }
            this.initMargin = currencyFormat.format(initMargin);
            this.maintMargin = currencyFormat.format(initMargin);*/

            long tradeQty = qty;
            Stock stock = DataStore.getSharedInstance().getStockObject(getSelectedSymbol());
            /*if (stock.getLotSize() >0) {
                tradeQty  = tradeQty * stock.getLotSize();
            }*/
            Account account = TradingShared.getTrader().findAccountByPortfolio(portfolio);
            String currency = account.getCurrency();
            TWDecimalFormat currencyFormat = SharedMethods.getCurrencyDecimalFormat(currency);
            FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(
                    SharedMethods.getExchangeFromKey(getSelectedSymbol()), stock.getOptionBaseSymbol(), SharedMethods.getInstrumentTypeFromKey(getSelectedSymbol())));
            double tradePrice = 0;
            long openBuyCount = 0;
            long openSellCount = 0;
            long prndingBuy = 0;
            long prndingSell = 0;
            double currencyFactor = TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(stock.getCurrencyCode());
            if (futureBaseAttributes != null) {
                tradePrice = futureBaseAttributes.getMargin();
                TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecordForFutures(getSelectedSymbol(), portfolio);
                if (transactRecord != null) {
                    openBuyCount = transactRecord.getOpenBuyCount();
                    openSellCount = transactRecord.getOpenSellCount();
                    prndingBuy = transactRecord.getPendingBuy();
                    prndingSell = transactRecord.getPendingSell();
                }
                if (side == TradeMeta.BUY) {
                    if ((openSellCount - prndingBuy) >= tradeQty) {
                        this.initMargin = "0";
                        this.maintMargin = "0";
                    } else {
                        long marginContracts = 0;
                        if (prndingBuy >= openSellCount) {
                            marginContracts = tradeQty;
                        } else {
                            marginContracts = tradeQty - (openSellCount - prndingBuy);
                        }
                        this.initMargin = currencyFormat.format(marginContracts * tradePrice * currencyFactor);
                        this.maintMargin = currencyFormat.format(marginContracts * tradePrice * currencyFactor);
                    }
                } else {
                    if ((openBuyCount - prndingSell) >= tradeQty) {
                        this.initMargin = "0";
                        this.maintMargin = "0";
                    } else {
                        long marginContracts = 0;
                        if (prndingSell >= openBuyCount) {
                            marginContracts = tradeQty;
                        } else {
                            marginContracts = tradeQty - (openBuyCount - prndingSell);
                        }
                        this.initMargin = currencyFormat.format(marginContracts * tradePrice * currencyFactor);
                        this.maintMargin = currencyFormat.format(marginContracts * tradePrice * currencyFactor);
                    }
                }
            }
        } catch (Exception e) {
        }
    }

    private void calculateOrderValue(int side, long qty) {
        try {
            /*String portfolio = getSelectedPortfolio();
            long dayHoldings = getSelectedDayHoldings();
            long tradeQty = dayHoldings;
            if(dayHoldings < 0 ){
                tradeQty = -dayHoldings;
            }
            double tradePrice = 0;
            double netValue = 0;
            TWDecimalFormat currencyFormat = null;
            char type = TradeMeta.ORDER_TYPE_SQUARE_OFF;
            try {
                String currency = TradingShared.getTrader().findAccountByPortfolio(portfolio).getCurrency();
                currencyFormat = SharedMethods.getCurrencyDecimalFormat(currency);
            } catch (Exception e) {
            }
            Stock stock = DataStore.getSharedInstance().getStockObject(getSelectedSymbol());
            String exchange = stock.getExchange();
            int instrument = stock.getInstrumentType();
            if (stock.getInstrumentType() == Meta.INSTRUMENT_FUTURE) {
                long openBuyCount = 0;
                long openSellCount = 0;
                long prndingBuy = 0;
                long prndingSell = 0;
                if (stock != null) {
                    FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, stock.getOptionBaseSymbol(), instrument));
                    long effectiveQuantity = 0;
                    tradePrice = futureBaseAttributes.getMarginPct();
                    TransactRecord transactRecord = TradePortfolios.getInstance().getTransactRecordForFutures(getSelectedSymbol(), portfolio);
                    if (transactRecord != null) {
                        openBuyCount = transactRecord.getOpenBuyCount();
                        openSellCount = transactRecord.getOpenSellCount();
                        prndingBuy = transactRecord.getPendingBuy();
                        prndingSell = transactRecord.getPendingSell();
                    }
                    if (side == TradeMeta.BUY) {
                        if ((openSellCount - prndingBuy) >= tradeQty) {
                            netValue = 0;
                            this.netValue = "0";
                            this.commission = "0";
                            this.orderValue = "0";
                        } else {
                            long marginContracts = 0;
                            if (prndingBuy >= openSellCount) {
                                marginContracts = tradeQty;
                            } else {
                                marginContracts = tradeQty - (openSellCount - prndingBuy);
                            }
                            double commission = 0;
                            if (marginContracts > 0) {
                                commission = SharedMethods.getCommission(exchange, 1, marginContracts, null, portfolio, getSelectedSymbol(), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                            }
                            netValue = (marginContracts * tradePrice) + commission;
                            this.commission = currencyFormat.format(commission);
                            this.orderValue = currencyFormat.format(marginContracts * tradePrice);
                            this.netValue = currencyFormat.format(netValue);
                        }
                    } else {
                        if ((openBuyCount - prndingSell) >= tradeQty) {
                            netValue = 0;
                            this.netValue = "0";
                            this.commission = "0";
                            this.orderValue = "0";
                        } else {
                            long marginContracts = 0;
                            if (prndingSell >= openBuyCount) {
                                marginContracts = tradeQty;
                            } else {
                                marginContracts = tradeQty - (openBuyCount - prndingSell);
                            }
                            double commission = 0;
                            if (marginContracts > 0) {
                                commission = SharedMethods.getCommission(exchange, 1, marginContracts, null, portfolio, getSelectedSymbol(),
                                        TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                            }
                            netValue = (marginContracts * tradePrice) + commission;
                            this.commission = currencyFormat.format(commission);
                            this.orderValue = currencyFormat.format(marginContracts * tradePrice);
                            this.netValue = currencyFormat.format(netValue);
                        }
                    }
                }
            } else {
                if (type == TradeMeta.ORDER_TYPE_LIMIT) {
                    tradePrice = 0d;
                } else {
                    if (instrument == Meta.INSTRUMENT_MUTUALFUND) {
                        tradePrice = 0d;
                    } else {
                        if (exchange != null) {
                            //                        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol);
                            if (stock != null) {
                                if (side == TradeMeta.SELL)
                                    tradePrice = stock.getBestBidPrice();
                                else
                                    tradePrice = stock.getBestAskPrice();
                            }
                            stock = null;
                        }
                    }
                }
                double commission =0;
                if (side == TradeMeta.BUY) {
                    if(TPlusStore.getSharedInstance().isTPlusSupportedSymbol(getSelectedSymbol(),"0")){
                        commission = SharedMethods.getCommission(exchange, tradePrice, tradeQty, null, portfolio, getSelectedSymbol(), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                    }else{
                        commission = SharedMethods.getCommission(exchange, tradePrice, tradeQty, null, portfolio, getSelectedSymbol(), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                    }
                } else {
                    commission = SharedMethods.getCommission(exchange, tradePrice, tradeQty, null, portfolio, getSelectedSymbol(), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                }

                this.orderValue = currencyFormat.format((tradeQty * tradePrice) / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor());
                if (commission == -1) { // if there is not commission rule, make the commission NA
                    this.commission = Language.getString("NA");
                    commission = 0;
                } else {
                    this.commission = currencyFormat.format(commission / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor());
                }
                if (side == TradeMeta.SELL)
                    netValue = (tradeQty * tradePrice) - commission;// TradingShared.getCommission(tradeQty * tradePrice);
                else
                    netValue = (tradeQty * tradePrice) + commission;//TradingShared.getCommission(tradeQty * tradePrice);
                netValue = netValue / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor(); // adjust to base currency
                this.netValue = currencyFormat.format(netValue);
            }*/


            String portfolio = getSelectedPortfolio();
            long tradeQty = qty;
            long tradeActualQty = qty;
            char type = (char) side;
            double tradePrice = 0;
            double netValue = 0;
            TWDecimalFormat currencyFormat = null;
            Stock stock = DataStore.getSharedInstance().getStockObject(getSelectedSymbol());
            String exchange = stock.getExchange();
            int instrument = stock.getInstrumentType();
            if (stock.getLotSize() > 0) {
                tradeQty = tradeQty * stock.getLotSize();
            }
            try {
                String currency = TradingShared.getTrader().findAccountByPortfolio(portfolio).getCurrency();
                currencyFormat = SharedMethods.getCurrencyDecimalFormat(currency);
            } catch (Exception e) {
                currencyFormat = SharedMethods.getDecimalFormat(2);
            }

            if (type == TradeMeta.ORDER_TYPE_LIMIT) {
                tradePrice = 0D;
            } else {
                if (instrument == Meta.INSTRUMENT_MUTUALFUND) {
                    tradePrice = 0D;
                } else {
                    if (exchange != null) {
                        //                        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol);
                        if (stock != null) {
                            if (side == TradeMeta.SELL)
                                tradePrice = stock.getBestBidPrice();
                            else
                                tradePrice = stock.getBestAskPrice();
                        }
                        stock = null;
                    }
                }
            }
            double commission = 0;
            double vat = 0;
            if (side == TradeMeta.BUY) {
                if (TPlusStore.getSharedInstance().isTPlusSupportedSymbol(getSelectedSymbol(), "0")) {
                    commission = SharedMethods.getCommission(exchange, tradePrice, tradeActualQty, null, portfolio, getSelectedSymbol(), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "0"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                } else {
                    commission = SharedMethods.getCommission(exchange, tradePrice, tradeActualQty, null, portfolio, getSelectedSymbol(), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
                }
            } else {
                commission = SharedMethods.getCommission(exchange, tradePrice, tradeActualQty, null, portfolio, getSelectedSymbol(), TPlusStore.getSharedInstance().getTPlusCommisionObject(stock.getCurrencyCode(), "2"), com.isi.csvr.datastore.CurrencyStore.getRate(PortfolioInterface.getCurrency(stock), PFStore.getBaseCurrency()));
            }

            double value = TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(stock.getCurrencyCode());
            if (stock.getInstrumentType() == Meta.INSTRUMENT_FUTURE) {
                commission = -1;
            }
            this.orderValue = currencyFormat.format((tradeQty * tradePrice * value) / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor());
            if (commission == -1) { // if there is not commission rule, make the commission NA
                this.commission = Language.getString("NA");
                commission = 0;
            } else {
                this.commission = currencyFormat.format(commission * value / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor());//TradingShared.getCommission(tradeQty * tradePrice)));
            }
            vat = TradeMethods.getVat(portfolio, exchange, stock.getSymbol(), stock.getInstrumentType(), tradePrice, tradeActualQty);
            this.vat = currencyFormat.format(vat * value / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor());
            if (side == TradeMeta.SELL)
                netValue = ((tradeQty * tradePrice) - commission - vat) * value;// TradingShared.getCommission(tradeQty * tradePrice);
            else
                netValue = ((tradeQty * tradePrice) + commission + vat) * value;//TradingShared.getCommission(tradeQty * tradePrice);
            netValue = netValue / ExchangeStore.getSharedInstance().getExchange(exchange).getPriceModificationFactor(); // adjust to base currency
            this.netValue = currencyFormat.format(netValue);

        } catch (Exception e) {
            //e.printStackTrace();
        }
    }


    private void createDCValuationTable() {
        try {
            // Read the default view in the view settings file
            // get the default view
            oSettings2 = (ViewSetting) Client.getInstance().getViewSettingsManager().getPortfolioView
                    (1280, "5").clone();
            if (oSettings2 == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }
        GUISettings.setColumnSettings(oSettings2, PortfolioInterface.getColumnSetting("TRADING_PORTFOLIO_VALUATION_COLS"));
/*
        mnuTrade.setText(Language.getString("MUBASHER_TRADE"));
        mnuTrade.setBackground(Theme.getColor("BOARD_TABLE_CELL_TRADE_BGCOLOR1"));
        mnuTrade.setEnabled(false);
        TWMenuItem mnuBuy = new TWMenuItem(Language.getString("BUY"));
        mnuBuy.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        mnuBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().doTransaction(TradeMeta.BUY, getSelectedSymbol(), getSelectedHolding(), false, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_PORTFOLIO);
            }
        });
        mnuTrade.add(mnuBuy);
        TWMenuItem mnuSell = new TWMenuItem(Language.getString("SELL"));
        mnuSell.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));
        mnuSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().doTransaction(TradeMeta.SELL, getSelectedSymbol(), getSelectedHolding(), false, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_PORTFOLIO);
            }
        });
        */


        //todo  add menu item to defValTable
//        /*******************************************************************************************************************************************************************************/
        mnuDcTradeItem = new TWMenuItem(Language.getString("LIQUIDATE"), "sell.gif");
        mnuDcTradeItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().doTransaction(TradeMeta.SELL, getSelectedSymbol(), getSelectedHolding(),
                        false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_PORTFOLIO, getSelectedPortfolio(), getSelectedBookKeeper());
            }
        });

        mnuDCTradeAll = new TWMenuItem(Language.getString("LIQUIDATE_ALL"), "liquidateall.gif");
        mnuDCTradeAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                liquidateOrders();
            }
        });

        mnuDCSqureOffItem = new TWMenuItem(Language.getString("SQUARE_OFF"), "sell.gif");
        mnuDCSqureOffItem.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                long dayHoldings = getSelectedDayHoldings();
                if (dayHoldings > 0) {
                    Client.getInstance().doTransaction(TradeMeta.SELL, getSelectedSymbol(), dayHoldings,
                            false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_PORTFOLIO, getSelectedPortfolio(), true);
                } else if (dayHoldings < 0) {
                    Client.getInstance().doTransaction(TradeMeta.BUY, getSelectedSymbol(), dayHoldings,
                            false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_PORTFOLIO, getSelectedPortfolio(), true);
                }
            }
        });

        //order history
        mnuDCOrderHistory = new TWMenuItem(Language.getString("ORDER_HISTORY"), "ordersearch.gif");
        mnuDCOrderHistory.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                searchHistory();
            }
        });
        mnuDcTradeItem.setVisible(true);
//        mnuDCSqureOffItem.setVisible(true); // Mix Dubai
        mnuDCOrderHistory.setVisible(true);
        mnuDCTradeAll.setVisible(true);


/*******************************************************************************************************************************************************************************/


        oSettings2.setTableNumber(2);
        //dCValTable = new

        /*/****************************** sath/******************************/
        // valuationModel = new ValuationModel(dataStore.getValuationList());
        //dCValuationModel = new DCValuationModel(dataStore.getValuationList());
        //data store is not using
        dCValuationModel = new DCValuationModel(dataStore.getPfValuationList());//,dataStore.getValuationList());
        //dCValuationModel = new DCValuationModel(dataStore.getValuationList());
        //valuationModel.setViewSettings(oSettings);
        dCValuationModel.setViewSettings(oSettings2);
        /*******************************/

        dCValTable = new Table();
        dCValTable.setThreadID(Constants.ThreadTypes.TRADING);
        //  zzdefCurValTable.setGroupablePortfolioModel(dCValuationModel);
        /* if((this.getSelectedSymbol()==null) ){
             mnuTradeItem.setVisible(false);
             mnuOrderHistory.setVisible(false);
         }*/
        dCValTable.setSortingEnabled();
        valuationTable.setSortingEnabled();
        //   dCValTable.setSortingDisabled();
        dCValTable.setWindowType(ViewSettingsManager.PORTFOLIO_VALUATION);
        //valuationTable.getPopup().add(mnuTrade);
        /**sath*************************************/
        dCValTable.getPopup().add(mnuDcTradeItem);
//        dCValTable.getPopup().add(mnuDCSqureOffItem);
        dCValTable.getPopup().add(mnuDCTradeAll);
        dCValTable.getPopup().add(mnuDCOrderHistory);
        dCValTable.getPopup().addPopupMenuListener(this);
//                valuationModel.setPFWindow(this);
        dCValTable.getPopup().showLinkMenus();

        dCValuationModel.setPFWindow(this);
        oSettings2.setParent(selfRef);

        /**sath*************************************/
        //dCValTable.setModel(valuationModel);
        //todo set the new uI
        // dCValTable.setModel(dCValuationModel);
        dCValTable.setGroupablePortfolioModel(dCValuationModel);


        //todo ?? need another model
        //valuationModel.setTradingPortfolioTable(dCValTable, true);
        //dCValuationModel.setTradingPortfolioTable(dCValTable, true);
        dCValuationModel.setDefCurTradingPortfolioTable(dCValTable, true);

        Theme.registerComponent(dCValTable);
        dCValTable.getTable().addMouseListener(this);
        //todo
        // valuationTable.getPopup().setPrintTarget(valuationTable.getTable(), PrintManager.PRINT_JTABLE);
        //        dCValTable.getPopup().enableUnsort(false);
        //        valuationTable.getPopup().enableUnsort(false);

        //make the currency column invisible


        dCValTable.getModel().updateGUI();
        dCValTable.getTable().getColumnModel().getColumn(3).setMaxWidth(0);
        dCValTable.getTable().getColumnModel().getColumn(3).setMinWidth(0);
        dCValTable.getTable().getColumnModel().getColumn(3).setPreferredWidth(0);
        dCValTable.getTable().getColumnModel().getColumn(3).setWidth(0);
        dCValTable.getTable().getColumnModel().getColumn(3).setResizable(false);

        dCValTable.getTable().getTableHeader().getColumnModel().getColumn(3).setMaxWidth(0);
        dCValTable.getTable().getTableHeader().getColumnModel().getColumn(3).setMinWidth(0);


        //todo pop up
        GUISettings.applyOrientation(dCValTable.getPopup());
        Client.getInstance().getViewSettingsManager().addPortfolioSetting(oSettings2);
    }


    private void liquidateOrders() {

        int size = dataStore.getValuationList().size();
        ValuationRecord record = null;
        boolean first = true;

        if (TradingShared.isReadyForTrading()) {

            if ((SharedMethods.showConfirmMessage(Language.getString("MSG_LIQUIDATE_ALL_PORTFOLIOS"), JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION)) {
                for (int i = size - 1; i >= 0; i--) {
                    boolean allOrNone = false;
                    record = (ValuationRecord) dataStore.getValuationList().get(i);
                    Stock stock = DataStore.getSharedInstance().getStockByExchangeKey(record.getSKey().getExchange(), record.getSKey().getSymbol());
                    try {
                        Rule rule = RuleManager.getSharedInstance().getRule("AON_SELECT", record.getSKey().getExchange(), "NEW_ORDER");
                        if (rule != null) {
                            Interpreter interpreter = new Interpreter();
                            interpreter.set("instrumentType", stock.getInstrumentType());
                            interpreter.set("quantity", record.getOwned(stock.getInstrumentType()));
                            interpreter.set("price", Math.round(stock.getLastTradeValue() * 1000) / (double) 1000);
                            int result = (Integer) interpreter.eval(rule.getRule());

                            allOrNone = (result == 1);
                        }
                    } catch (EvalError evalError) {
                        evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    if (first) { // authenticate only the first order of the list
                        boolean sucess = TradeMethods.getSharedInstance().sendNewOrder(true, false, 0, record.getPortfolioID(), record.getSKey().getExchange(),
                                record.getSKey().getSymbol(), TradeMeta.SELL, TradeMeta.ORDER_TYPE_MARKET, 0,
                                0, "*", record.getBalance() /*record.getOwned(stock.getInstrumentType())*/, TradeMeta.TIF_FOK, 0, -1, 0, allOrNone, stock.getStrikePrice(),
                                TradingShared.formatOpraExpireDate(stock.getExpirationDate()), stock.getInstrumentType(), -1,
                                stock.getOptionBaseSymbol(), -1, null, null, null, -1, null, null, null, 0d, record.getBookKeeper(), 0, 0);
                        if (!sucess) break;
                        first = false;
                    } else {
                        TradeMethods.getSharedInstance().sendNewOrder(false, false, 0, record.getPortfolioID(), record.getSKey().getExchange(),
                                record.getSKey().getSymbol(), TradeMeta.SELL, TradeMeta.ORDER_TYPE_MARKET, 0,
                                0, "*", record.getBalance()/*record.getOwned(stock.getInstrumentType())*/, TradeMeta.TIF_FOK, 0, -1, 0, allOrNone, stock.getStrikePrice(),
                                TradingShared.formatOpraExpireDate(stock.getExpirationDate()), stock.getInstrumentType(), -1,
                                stock.getOptionBaseSymbol(), -1, null, null, null, -1, null, null, null, 0d, record.getBookKeeper(), 0 ,0);
                    }
                }
            }
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }
    }

    private void searchHistory() {
        String portfolio = ",";

        if (TradingShared.isReadyForTrading()) {
            for (int i = 0; i < selectedPFIDs.length; i++) {
                portfolio += selectedPFIDs[i];
            }
            portfolio = portfolio.substring(1);
            String symbol = "";
            if (isDefCurMode) {
                symbol = (String) dCValTable.getTable().getModel().getValueAt(dCValTable.getTable().getSelectedRow(), 0);//getSelectedSymbol();
                portfolio = (String) dCValTable.getTable().getModel().getValueAt(dCValTable.getTable().getSelectedRow(), -11);//getSelectedSymbol();
            } else {
                symbol = (String) valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), 0);//getSelectedSymbol();
                portfolio = (String) valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), -11);//getSelectedSymbol();
            }
            Date toDate = new Date(System.currentTimeMillis());
            Calendar fromCal = Calendar.getInstance();
            fromCal.add(Calendar.YEAR, -1);

            TradeMethods.getSharedInstance().doSearch(portfolio, symbol, fromCal.getTime(), toDate);
            TradeMethods.getSharedInstance().showOrderSearchWindow();
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }

    }

    private String getSelectedPortfolio() {
        try {
            if (isDefCurMode) {
                //todo    should map with the model

                PfValutionRecord pfTemp = (PfValutionRecord) dataStore.getPfValuationList().get(dCValTable.getTable().getSelectedRow());//dCValTable.getTable().getSelectedRow()
                if (pfTemp.getRecType() == PfValutionRecord.VALRECORD) {
                    return pfTemp.getValRecord().getPortfolioID();
                } else {   //other type of rows
                    return null;
                }

            } else {            //unified mode
                return ((ValuationRecord) dataStore.getValuationList().get(valuationTable.getTable().getSelectedRow())).getPortfolioID();
            }

        } catch (Exception e) {
            return null;
        }
    }

    private String getSelectedBookKeeper() {
        try {
            if (isDefCurMode) {
                //todo    should map with the model

                PfValutionRecord pfTemp = (PfValutionRecord) dataStore.getPfValuationList().get(dCValTable.getTable().getSelectedRow());//dCValTable.getTable().getSelectedRow()
                if (pfTemp.getRecType() == PfValutionRecord.VALRECORD) {
                    return pfTemp.getValRecord().getBookKeeper();
                } else {   //other type of rows
                    return null;
                }

            } else {            //unified mode
                return ((ValuationRecord) dataStore.getValuationList().get(valuationTable.getTable().getSelectedRow())).getBookKeeper();
            }

        } catch (Exception e) {
            return null;
        }
    }

    private String getSelectedSymbol() {
        try {
//            return valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), 1) +
//                    Constants.KEY_SEPERATOR_CHARACTER +
//                    valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), 0);
            if (isDefCurMode) {
                return (String) dCValTable.getTable().getModel().getValueAt(dCValTable.getTable().getSelectedRow(), -1);
            } else {
                return (String) valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), -1);
            }


        } catch (Exception e) {
            return null;
        }
    }

    private long getSelectedHolding() {
        /*try {
            return ((Long) valuationModel.getValueAt(valuationTable.getTable().getSelectedRow(), 5)).longValue();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }*/ //Bug ID <#0038> 
        /*try {
            return ((Long) valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), 26)).longValue();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }*/
        try {
            if (isDefCurMode) { //do someting to remove exeptionss
                return ((Long) dCValTable.getTable().getModel().getValueAt(dCValTable.getTable().getSelectedRow(), 7)).longValue(); //previously send owned amt as quantity

            } else {
                return ((Long) valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), 7)).longValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }


    private long getSelectedDayHoldings() {
        try {
            //todo add for defvaltable
            if (isDefCurMode) { //do someting to remove exeptionss
                return ((Long) dCValTable.getTable().getModel().getValueAt(dCValTable.getTable().getSelectedRow(), 25)).longValue();
            } else {
                return ((Long) valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), 25)).longValue();
            }
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    @Override
    public void setLayer(int layer) {
        if (this.layer == -1) {
            this.layer = layer;
        }
        try {
            if (getViewSetting() == null) {
                setViewSetting(valuationModel.getViewSettings());
            }
            if ((getViewSetting().getLayer() == -1))
                getViewSetting().setLayer(layer);
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        super.setLayer(layer);    //To change body of overridden methods use File | Settings | File Templates.
    }

    @Override
    public void setViewSetting(ViewSetting oSettings) {
        this.oSettings1 = oSettings;
        try {
            this.oSettings1.setLayer(layer);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void applySettings() {
        if (oSettings1 == null) {
            this.getDesktopPane().setLayer(this, layer, 0);
            return;
        } else if (oSettings1.isVisible()) {
            if (Settings.isPutAllToSameLayer() && (getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, oSettings1.getIndex());
            } else {
                this.getDesktopPane().setLayer(this, oSettings1.getLayer(), oSettings1.getIndex());
            }
        } else {
            if (Settings.isPutAllToSameLayer() && (getViewSetting().getLayer() != GUISettings.INTERNAL_DIALOG_LAYER)) {
                this.getDesktopPane().setLayer(this, GUISettings.DEFAULT_LAYER, 0);
            } else {
                this.getDesktopPane().setLayer(this, oSettings1.getLayer(), 0);
            }
        }
        valuationModel.applySettings();
        dCValuationModel.applySettings();
        this.setLocation(oSettings1.getLocation());
        if (oSettings1.isVisible()) {
            this.setVisible(true);
            try {
                this.setSelected(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            //==this.getDesktopPane().setLayer(this, this.getLayer(), 0);
        }

        try {
            switch (oSettings1.getStyle()) {
                case ViewSettingsManager.STYLE_ICONIFIED:
                    this.setIcon(true);
                    break;
                case ViewSettingsManager.STYLE_MAXIMIZED:
                    this.setMaximum(true);
                    break;
                default:
                    this.setMaximum(false);
                    this.setIcon(false);
                    break;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        if (oSettings1.getLocation() != null) {
            this.setLocation(oSettings1.getLocation());
        }

        if (oSettings1.getSize() != null) {
            this.setSize(oSettings1.getSize());
        }
        this.setVisible(oSettings1.isVisible());
        revalidateHeader();
    }

    public void addTitlebarManu() {
        Component np = (Component) ((BasicInternalFrameUI) getUI()).getNorthPane();
        try {
            np.removeMouseListener(this); // if existing
        } catch (Exception e) {
        }
        np.addMouseListener(this);
    }

    public void revalidateHeader() {
        try {
            if (!oSettings1.isHeaderVisible()) {
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                this.revalidate();
            }
        } catch (Exception e) {
        }
    }

    public void show() {
        super.show();
        setBaseCurrency();
        revalidateHeader();
    }

    public void updateUI() {
        super.updateUI();
        addTitlebarManu();
        revalidateHeader();
        this.setBorder(GUISettings.getTWFrameBorder());
    }

    public boolean isTitleVisible() {
        if (oSettings1 != null) {
            return oSettings1.isHeaderVisible(); // table.getModel().getViewSettings().isHeaderVisible();
        } else {
            return true;
        }
    }

    public void setTitleVisible(boolean setVisible) {
        ViewSetting settings = oSettings1; // null; //table.getModel().getViewSettings();
        if (setVisible) {
            if (settings != null) {
                settings.setHeaderVisible(true);
            }
            updateUI();
        } else {
            Component titleBar = ((BasicInternalFrameUI) getUI()).getNorthPane();
            if (titleBar != null) {
                //System.out.println("hiding");
                ((BasicInternalFrameUI) getUI()).setNorthPane(null);
                revalidate();
                if (settings != null) {
                    settings.setHeaderVisible(false);
                }
            }
        }
        settings = null;
    }

    public void windowClosing() {

    }

    public void updateGUI() {

    }

    public String toString() {
        return getTitle();
    }

    public void setBaseCurrency() {
        try {
        } catch (Exception ex) {
        }
    }

    public void activateCurrentSymbols() {
        if (PortfolioInterface.getPortfolioVersion() != PFUtilities.INTERNATIONAL_VERSION)
            return;

        boolean isSymbolFound = false;
        TradeKey sKey = null;
        TradeKey[] list = null;
        TradeKey[] curList = null;

        if (oldSymbolList == null) {
            list = dataStore.getAllSymbolList();
        } else {
            list = oldSymbolList;
        }
        curList = dataStore.getCurrentSymbolList();
        oldSymbolList = curList;
        for (int i = 0; i < list.length; i++) {
            sKey = list[i];
            for (int j = 0; j < curList.length; j++) {
                if (sKey.equals(curList[j])) {
                    isSymbolFound = true;
                    break;
                }
            }
            if (!isSymbolFound) {
                PortfolioInterface.removeSymbol(sKey);
            } else
                isSymbolFound = false;
            sKey = null;
        }

        for (int i = 0; i < curList.length; i++) {
            sKey = curList[i];
            for (int j = 0; j < list.length; j++) {
                if (sKey.equals(list[j])) {
                    isSymbolFound = true;
                    break;
                }
            }
            if (!isSymbolFound) {
                PortfolioInterface.setSymbol(true, sKey);
            } else
                isSymbolFound = false;

            sKey = null;
        }

        sKey = null;
        list = null;
        curList = null;
    }

    private void unregisterSymbols() {
        /*if (!isRegisteredSymbols)
            return;
        isRegisteredSymbols = false;
        String[] list = dataStore.getCurrentSymbolList();
        for (int i = 0; i < list.length; i++) {
            PortfolioInterface.removeSymbol(list[i]);
        }*/
    }

//    private void reregisterAllSymbols() {
//        if (PortfolioInterface.getPortfolioVersion() != PFUtilities.INTERNATIONAL_VERSION)
//            return;
//        String[] list = dataStore.getAllSymbolList();
//        for (int i = 0; i < list.length; i++) {
//            PortfolioInterface.setSymbol(false, list[i]);
//        }
//    }

    private void reregisterSymbols() {
        if (PortfolioInterface.getPortfolioVersion() != PFUtilities.INTERNATIONAL_VERSION)
            return;
        if (isRegisteredSymbols)
            return;
        isRegisteredSymbols = true;
        TradeKey[] list = dataStore.getCurrentSymbolList();
        for (int i = 0; i < list.length; i++) {
            PortfolioInterface.setSymbol(false, list[i]);
        }
    }

    /*private void loadTransactionToEdit(TransactRecord record) {
        TransactionDialog txnDialog = new TransactionDialog(Client.getInstance().getFrame(), "", true);
        txnDialog.setParent(selfRef);
        if (record != null)
            txnDialog.setRecord(record);
        Thread txnThread = new Thread(txnDialog, "TradingPortfolioWindow-loadTransactionToEdit");
        txnDialog.setCurrentThread(txnThread);
        txnThread.start();
        txnDialog.showDialog();
        txnDialog = null;
    }*/

    /*private void disableTxHistoryFilter() {
        selectedSKey    = null;
        selectedType    = PFUtilities.NONE;
        selectedStartDate   = null;
        selectedEndDate     = null;

        createFilteredTransactionList(null, PFUtilities.NONE, null, null);
        String sDesc = "";
        if (Language.isLTR())
            sDesc = Language.getString("CURRENT_VIEW") + " : " + Language.getString("PF_MSG_CURRENT_VIEW");
        else
            sDesc = Language.getString("PF_MSG_CURRENT_VIEW") + " : " + Language.getString("CURRENT_VIEW");
        lblFilterDesc.setText(sDesc);
        sDesc = null;
    }*/

    public void itemStateChanged(ItemEvent e) {
        CardLayout cl = (CardLayout) (tablePanel.getLayout());
        cl.show(tablePanel, (String) e.getItem());
        cl = null;
    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        String portfolio = getSelectedPortfolio();
        if (TradingShared.getTrader().isTradableExchangesAvailable()) {
            if (portfolio != null) {
                mnuTradeAll.setVisible(BrokerConfig.getSharedInstance().isLiquidateAllAvailable(TradingShared.getTrader().getPath(portfolio)));
                mnuDCTradeAll.setVisible(BrokerConfig.getSharedInstance().isLiquidateAllAvailable(TradingShared.getTrader().getPath(portfolio)));
            } else {
                mnuTradeAll.setVisible(BrokerConfig.getSharedInstance().isLiquidateAllAvailable(Constants.PATH_PRIMARY));
                mnuDCTradeAll.setVisible(BrokerConfig.getSharedInstance().isLiquidateAllAvailable(Constants.PATH_PRIMARY));
            }
            mnuTradeItem.setVisible(true);
            mnuDcTradeItem.setVisible(true);
//            mnuSqureOffItem.setVisible(true); //Mix dubai
//            mnuDCSqureOffItem.setVisible(true); //Mix dubai
        } else {
            mnuTradeAll.setVisible(false);
            mnuDCTradeAll.setVisible(false);
            mnuTradeItem.setVisible(false);
            mnuDcTradeItem.setVisible(false);
            mnuSqureOffItem.setVisible(false);
            mnuDCSqureOffItem.setVisible(false);
        }

    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
    }

    public void popupMenuCanceled(PopupMenuEvent e) {
    }

    public void mouseClicked(MouseEvent e) {
        /*if (SwingUtilities.isRightMouseButton(e)){
               if((this.getSelectedSymbol()==null) || (this.getSelectedSymbol().equals(""))){
                   int index = valuationTable.getPopup().getComponentIndex(mnuTradeItem);
                   int index2 = valuationTable.getPopup().getComponentIndex(mnuOrderHistory);
                   valuationTable.getPopup().getComponent(index).setVisible(false);
                   valuationTable.getPopup().getComponent(index2).setVisible(false);
                   //mnuOrderHistory.setVisible(false);
                   //mnuTradeItem.setVisible(false);
               }
        }*/
        System.out.println("test");
        if (e.getSource() instanceof javax.swing.plaf.metal.MetalInternalFrameTitlePane) {
            if (SwingUtilities.isRightMouseButton(e)) {
                /*if((this.getSelectedSymbol()==null) || (this.getSelectedSymbol().equals(""))){
                    mnuOrderHistory.setVisible(false);
                    mnuTradeItem.setVisible(false);
                }*/
                this.setLocationRelativeTo(Client.getInstance().getDesktop());
                GUISettings.showPopup(titleBarMenu, this, e.getX(), e.getY());
            }
        } else if (e.getSource() == toggleIncludeCash) {
            if (toggleIncludeCash.isSelected())
                setCashBalanceStatus(true);
            else
                setCashBalanceStatus(false);
        } else if (e.getSource() == btnAvailablePFs) {
            portfolioButtonClicked();
        } else if (e.getSource() == lblAvailablePFs) {
            portfolioButtonClicked();
        } else if (e.getSource() == lblCurrencyIDs) {
            currencyButtonClicked();
        } else if (e.getSource() == btnCurrencyIDs) {
            currencyButtonClicked();
        } else if (e.getSource() == valuationTable.getTable()) {
            isDefCurMode = false;
            if (e.getClickCount() > 1) {
                /*Table blotterTable = new Table(4);
                ViewSetting vSetting = ViewSettingsManager.getSummaryView("DETAIL_QUOTE");
                PortfolioDetailQuoteModel model = new PortfolioDetailQuoteModel(valuationModel,valuationTable.getTable().getSelectedRow());
                if(vSetting!=null){
                model.setViewSettings(vSetting);
                }
                blotterTable.setModel(model);
                model.setTable(blotterTable);
                model.updateGUI();

                InternalFrame frame = new InternalFrame(blotterTable);
            vSetting.setParent(frame);
            frame.setColumnResizeable(true);
            frame.setPrintable(true);
            frame.setSize(vSetting.getSize());
            frame.setLocation(vSetting.getLocation());
            frame.getContentPane().add(blotterTable);
            frame.setResizable(true);
            frame.setClosable(true);
            frame.setMaximizable(false);
            frame.setIconifiable(true);
            //oTopDesktop.add(frame);
            frame.setTitle(Language.getString("DETAIL_QUOTE") + " : " );
            frame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            frame.updateUI();
            frame.applySettings();*/
//                Client.getInstance().mnuPortfolioDQ(valuationTable.getUnsortedRow(valuationTable.getSelectedRow()), dataStore
                Client.getInstance().mnuPortfolioDQ(valuationTable.getUnsortedRow(valuationTable.getTable().getSelectedRow()), dataStore
                        , getSelectedSymbol());
                /* String key = (String)valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), 1)+
                        Constants.KEY_SEPERATOR_CHARACTER +
                        (String)valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), 0);
                Client.getInstance().mnuDetailQuoteSymbol(key);*/
            }
        } else if (e.getSource() == dCValTable.getTable()) {
            isDefCurMode = true;
            if (e.getClickCount() > 1) {
                //todo popupmenu
            }
        } else if (e.getSource() == lblCashBalDesc) {
            TradeMethods.getSharedInstance().showAccountFrame(selectedPFIDs, dataStore.getBaseCurrency());

        }

    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }

    public void mouseDragged(MouseEvent e) {
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("TITLE_HIDE")) {
            toggleTitleBar();
        } else { //if (e.getActionCommand().equals("USD")) {
            setCurrency(e.getActionCommand());
        }
    }

    private void setCurrency(String currency) {
        String sItem = null;
        for (int i = 0; i < currencyIDs.length; i++) {
            sItem = currencyIDs[i];
            if (currency.equals(sItem)) {
                lblSelectedCurrencyID.setText(sItem);
                dataStore.setBaseCurrency(sItem);
//                    displayCashBalance();
                valuationTable.getTable().updateUI();
                //todo shouldnot change the currncy
                dCValTable.getTable().updateUI();
            }
            sItem = null;
        }
        displayCashBalance();
    }

    private void currencyButtonClicked() {
        TWMenuItem menuItem = null;
        String sItem = null;
        btnGroupCurrency = new ButtonGroup();
        currencyPopup = new JPopupMenu();
        currencyPopup.removeAll();
        ArrayList<String> idlist = TradeMethods.getAccountSummaryCurrensyList();
        String[] currencies = new String[0];
        currencies = idlist.toArray(currencies);


        for (int i = 0; i < currencies.length; i++) {
            sItem = currencies[i];
            menuItem = new TWMenuItem(sItem);
            menuItem.setActionCommand(sItem);
            menuItem.addActionListener(this);
            currencyPopup.add(menuItem);
            btnGroupCurrency.add(menuItem);
            menuItem = null;
        }
        GUISettings.applyOrientation(currencyPopup);

        int popWidth = 0;
        if (Language.isLTR()) {
            popWidth = (int) currencyPopup.getPreferredSize().getWidth();
            popWidth = 20;
        }
        //if (btnCurrencyIDs.isEnabled()){
        currencyPopup.show(btnCurrencyIDs, btnCurrencyIDs.getWidth() - popWidth, btnCurrencyIDs.getHeight() - 4);
        //}
        SwingUtilities.updateComponentTreeUI(currencyPopup);
//        currencyPopup = null;
    }

    private void portfolioButtonClicked() {
        if (isTradeConnected) {
            int popWidth = 0;
            PortfolioList pf = new PortfolioList(this, selectedPFIDs);
            if (Language.isLTR()) {
                popWidth = (int) pf.getPreferredSize().getWidth();
            }
            pf.show(btnAvailablePFs, btnAvailablePFs.getWidth() - popWidth, btnAvailablePFs.getHeight() - 4);
            pf = null;
        }
    }

    private void loadButtonImages() {
        int themeID = PortfolioInterface.getThemeID();
        toggleIncludeCash.setIcon(new ImageIcon(PortfolioInterface.DEFAULT_THEME_PATH + themeID + "/pf_include_cashBal.gif"));
        toggleIncludeCash.updateUI();

        btnPortfolioReview.setIcon(new ImageIcon(PortfolioInterface.DEFAULT_THEME_PATH + themeID + "/pf_report.gif"));
        btnPortfolioReview.updateUI();
        btnPrint.setIcon(new ImageIcon(PortfolioInterface.DEFAULT_THEME_PATH + themeID + "/pf_print.gif"));
        btnPrint.updateUI();
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        SwingUtilities.updateComponentTreeUI(titleBarMenu);
        Theme.applyFGColor(g_mnuFile, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(g_mnuView, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(g_mnuAnalysis, "MAIN_MENU_FGCOLOR");
        Theme.applyFGColor(g_mnuTools, "MAIN_MENU_FGCOLOR");
        tabbedPane.repaint();
        SharedMethods.updateComponent(tabbedPane);
        loadButtonImages();
    }

    public int getDesktopItemType() {
        return BOARD_TYPE;
    }

    public void setWindowType(int type) {
        windowType = type;
    }

    public int getWindowType() {
        return windowType;
    }

    public void closeWindow() {
        doDefaultCloseAction();
    }

    public int getDesktopIndex() {
        return windowIndex;
    }

    public void doDefaultCloseAction() {
        super.doDefaultCloseAction();
        focusNextWindow();
    }

    public void currencyAdded() {
        ArrayList<String> idlist = CurrencyStore.getSharedInstance().getCurrencyIDs();
        currencyIDs = new String[0];
        currencyIDs = idlist.toArray(currencyIDs);
        currencyPopup = null;

        /*if ((dataStore.getBaseCurrency() != null) && (!dataStore.getBaseCurrency().equals(""))){
                setCurrency(dataStore.getBaseCurrency());
        }if (lblSelectedCurrencyID.getText().equals("")){
            if (TradingShared.getTrader().getAccount(0).getCurrency() != null){
                setCurrency(TradingShared.getTrader().getAccount(0).getCurrency());
            }else{
                setCurrency(currencyIDs[0]);
            }
        }*/
    }

    private void focusNextWindow() {
        TWDesktop desktop = (TWDesktop) Client.getInstance().getDesktop();
        desktop.focusNextWindow(desktop.getListPosition(this, true), true);
        desktop = null;
    }

    public void setDesktopIndex(int index) {
        windowIndex = index;
    }

    /**
     * Invoked when a internal frame has been opened.
     *
     * @see javax.swing.JInternalFrame#show
     */
    public void internalFrameOpened(InternalFrameEvent e) {

    }

    /**
     * Invoked when an internal frame is in the process of being closed.
     * The close operation can be overridden at this point.
     *
     * @see javax.swing.JInternalFrame#setDefaultCloseOperation
     */
    public void internalFrameClosing(InternalFrameEvent e) {
        unregisterSymbols();
    }

    /**
     * Invoked when an internal frame has been closed.
     *
     * @see javax.swing.JInternalFrame#setClosed
     */
    public void internalFrameClosed(InternalFrameEvent e) {
        unregisterSymbols();
    }

    /**
     * Invoked when an internal frame is iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameIconified(InternalFrameEvent e) {
//         System.out.println("PortfolioTable.internalFrameIconified");
        unregisterSymbols();
    }

    /**
     * Invoked when an internal frame is de-iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameDeiconified(InternalFrameEvent e) {
        reregisterSymbols();
    }

    /**
     * Invoked when an internal frame is activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameActivated(InternalFrameEvent e) {
        reregisterSymbols();
    }

    /**
     * Invoked when an internal frame is de-activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameDeactivated(InternalFrameEvent e) {

    }

//     public void stateChanged(ChangeEvent e) {
    //System.err.println("state changed " + tabbedPane.getSelectedIndex());
//         if (tabbedPane.getSelectedIndex() == 0) {
//         } else if (tabbedPane.getSelectedIndex() == 1) {
//         }
//     }

    public JTable getTable2() {
        return dCValTable.getTable();
        //return null;
    }


    public JTable getTable1() {
//        if (isDefCurMode) {
//            return dCValTable.getTable();
//        }else{
        return valuationTable.getTable();
        //}


    }


    public boolean isWindowVisible() {
        return isVisible();
    }

    public int getZOrder() {
        return getDesktopPane().getPosition(this);
    }

    public void printTable() {

    }

    public int getWindowStyle() {
        if (isMaximum())
            return ViewSettingsManager.STYLE_MAXIMIZED;
        else if (isIcon())
            return ViewSettingsManager.STYLE_ICONIFIED;
        else
            return ViewSettingsManager.STYLE_NORMAL;
    }

    public void setFilterDescription(boolean isApplied, Date fromDate, Date toDate, String sKey, byte type) {
//        String sDesc = "<html>" + Language.getString("CURRENT_VIEW") + " : " + Language.getString("PF_TRANSACTIONS") + "&nbsp;";
        String sDesc = "<html>" + Language.getString("CURRENT_VIEW") + " : " + "&nbsp;";
        if (isApplied) {
            if (fromDate != null) {
                sDesc += Language.getString("FROM") + "&nbsp;<b>" + PFUtilities.getFormattedDate(fromDate) + "</b>&nbsp;,&nbsp;";
            }
            if (toDate != null) {
                sDesc += Language.getString("TO") + "&nbsp;<b>" + PFUtilities.getFormattedDate(toDate) + "</b>&nbsp;,&nbsp;";
            }
            if (sKey != null) {
                sDesc += Language.getString("SYMBOL") + "&nbsp;<b>" + PortfolioInterface.getSymbolFromKey(sKey) + "</b>&nbsp;,&nbsp;";
            }
            switch (type) {
                case PFUtilities.NONE:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("ALL") + "</b>";
                    break;
                case PFUtilities.BUY:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("BUY") + "</b>";
                    break;
                case PFUtilities.SELL:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("SELL") + "</b>";
                    break;
                case PFUtilities.OP_BAL:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("OPENING_BAL") + "</b>";
                    break;
                case PFUtilities.DIVIDEND:
                    sDesc += Language.getString("TRANSACTION_TYPE") + "&nbsp;<b>" + Language.getString("DIVIDEND") + "</b>";
                    break;
            }
            sDesc += "</html>";
        } else {
            if (Language.isLTR())
                sDesc = Language.getString("CURRENT_VIEW") + " : " + Language.getString("PF_MSG_CURRENT_VIEW");
            else
                sDesc = Language.getString("PF_MSG_CURRENT_VIEW") + " : " + Language.getString("CURRENT_VIEW");
        }
        lblFilterDesc.setText(sDesc);
    }

    private void setCashBalanceStatus(boolean status) {
        if (!status)
            toggleIncludeCash.setToolTipText(Language.getString("INCLUDE_CASH_BALANCE"));
        else
            toggleIncludeCash.setToolTipText(Language.getString("EXCLUDE_CASH_BALANCE"));
        valuationModel.includeCashBalance(status);//todo after this usages are for report
        dCValuationModel.includeCashBalance(status);
        valuationTable.getTable().updateUI();
        dCValTable.getTable().updateUI();


    }

    public String[] getSelectedPFIDs() {
        return selectedPFIDs;
    }

    public ViewSetting getValuationSettings() {
        return valuationTable.getModel().getViewSettings();
    }

    public ViewSetting getDefcurValuationSettings() {
        return dCValTable.getModel().getViewSettings();
    }

//    public ViewSetting getDefValuationSettings() {
//        return valuationTable.getModel().getViewSettings();
//    }


    //should save tab situation etc.....

    public ViewSetting getDefValuationSettings() {
        return this.oSettings2;
    }
//     public ViewSetting getValuationSettings() {
//        return valuationTable.getModel().getViewSettings();
//    }

    public ViewSetting getPortfolioViewSettings() {
        return this.oSettings1;
    }

    //need for report panel

    public ValuationModel getValuationModel() {
        return valuationModel;
    }

    @Override
    public ViewSetting getViewSetting() {
        return this.oSettings1;
    }

    public Table getValuationTable() {
        return valuationTable;
    }

    private void printPortfolio() {
        switch (selectedPanel) {
            case VALUATION:
                valuationTable.getPopup().printPFTable(PrintManager.TABLE_TYPE_DEFAULT, tabbedPane.getSelectedIndex());
                break;
//            case TRANS_HISTORY :
//                transHistoryTable.getPopup().printTable(PrintManager.TABLE_TYPE_DEFAULT);
//                break;

            /*if (tabbedPane.getSelectedIndex() == 0) {
                valuationTable.getPopup().printPFTable(PrintManager.TABLE_TYPE_DEFAULT);
            } else {
                valuationTable.getPopup().printPFTable(PrintManager.TABLE_TYPE_DEFAULT);
            }
            break;*/
        }
    }

    /**
     * Notified by the Account Object when the Account has been changed.
     * Add all the portfolios in the account obj to the Portfolio list
     *
     * @param accountID
     */
    public void accountDataChanged(String accountID) {
        try {
//            lblCashBal.setText(PFUtilities.getFormattedPrice(
//                    convertToSelectedCurrency(dataStore.getBaseCurrency(),  TradingShared.getTrader().getAccount(0).getBuyingPower())));
//            lblCashBalCurrency.setText(TradingShared.getTrader().findAccount(accountID).getCurrency());

            //if(isDefault){
            if (TradingShared.getTrader().getPortfolio(selectedPFIDs[0]).getAccountNumber().equals(accountID)) {
                String currency = TradingShared.getTrader().findAccount(accountID).getCurrency();
                lblSelectedCurrencyID.setText(currency);
                dataStore.setBaseCurrency(currency);
                valuationTable.getTable().updateUI();
                currency = null;
//                    isDefault = false;
            }
            //}

            if ((dataStore.getBaseCurrency() != null) && (!dataStore.getBaseCurrency().equals(""))) {
                setCurrency(dataStore.getBaseCurrency());
            }
            if ((lblSelectedCurrencyID.getText() == null) || (lblSelectedCurrencyID.getText().equals(""))) {
                if (TradingShared.getTrader().getAccount(0).getCurrency() != null) {
                    setCurrency(TradingShared.getTrader().getAccount(0).getCurrency());
                } else {
                    setCurrency(currencyIDs[0]);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void portfolioDataChanged(String portfolioID) {
        ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
        for (int i = 0; i < portfolios.size(); i++) {
            TradingPortfolioRecord record = portfolios.get(i);
            if (TradePortfolios.getInstance().getPortfolio(record.getPortfolioID()) == null) { // if not already added
                TradePortfolios.getInstance().addPortfolio(record); // add to the list
            }
            record = null;
        }
        loadDefaultPortfolio();

        if (portfolios.size() == 0) {
            tabbedPane.setTabLineVisible(false);
        } else {
            tabbedPane.setTabLineVisible(true);
        }
        tabbedPane.selectTab(0);
        SwingUtilities.updateComponentTreeUI(tabbedPane);
        tabbedPane.updateUI();
    }

    public void tradeServerConnected() {
        mnuTrade.setEnabled(true);
        isTradeConnected = true;
        TradeMethods.checkRefreshButton();
    }


    public void tradeSecondaryPathConnected() {
        if (initialSecondaryPathConnection) {
            loadDefaultPortfolio();
            initialSecondaryPathConnection = false;
            isTradeConnectedSecondary = true;
        }
    }

    public void tradeServerDisconnected() {
        mnuTrade.setEnabled(false);
        isTradeConnected = false;
        isTradeConnectedSecondary = false;
        TradeMethods.checkRefreshButton();
    }

    public boolean isColumnResizeable() {
        return true;
    }

    public boolean isPrintable() {
        return true;
    }

    public boolean isDetachable() {
        return true;
    }

    public void printContents() {
        printPortfolio();
    }

    public void showServices() {
    }

    public void showLinkGroup() {

    }

    public String getLinkGroupIDForTitleBar() {
        return null;
    }

    public boolean isServicesEnabled() {
        return false;
    }

    public boolean isLinkGroupEnabled() {
        return false;
    }

    public String getSelectedCurrency() {
        return CurrencyStore.getSharedInstance().getCurrencyDescription(lblSelectedCurrencyID.getText());
    }

    private void createDecimalMenu(JPopupMenu parent) {
        TWMenu mnuDecimalPlaces = new TWMenu(Language.getString("POPUP_DECIMAL_PLACES"), "decimalplaces.gif");
        parent.add(mnuDecimalPlaces);

        //transHistoryTable.getPopup().add(mnuDecimalPlaces);
        ButtonGroup oGroup = new ButtonGroup();
        mnuNoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_NO_DECIMAL"));
        mnuDecimalPlaces.add(mnuNoDecimal);
        mnuNoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.NO_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.NO_DECIMAL_PLACES){
//            mnuNoDecimal.setSelected(true);
//        }
        oGroup.add(mnuNoDecimal);
        mnuDefaultDecimal = new TWRadioButtonMenuItem(Language.getString("DEFAULT_DECIMAL"));
        mnuDecimalPlaces.add(mnuDefaultDecimal);
        mnuDefaultDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.NO_DECIMAL_PLACES){
//            mnuNoDecimal.setSelected(true);
//        }
        oGroup.add(mnuDefaultDecimal);
        mnuOneDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_ONE_DECIMAL"));
        mnuDecimalPlaces.add(mnuOneDecimal);
        mnuOneDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.ONE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.ONE_DECIMAL_PLACES){
//            mnuOneDecimal.setSelected(true);
//        }
        oGroup.add(mnuOneDecimal);
        mnuTwoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_TWO_DECIMAL"));
        mnuDecimalPlaces.add(mnuTwoDecimal);
        mnuTwoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.TWO_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.TWO_DECIMAL_PLACES){
//            mnuTwoDecimal.setSelected(true);
//        }
        oGroup.add(mnuTwoDecimal);
        mnuThrDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_THREE_DECIMAL"));
        mnuDecimalPlaces.add(mnuThrDecimal);
        mnuThrDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.THREE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.THREE_DECIMAL_PLACES){
//            mnuThrDecimal.setSelected(true);
//        }
        oGroup.add(mnuThrDecimal);
        mnuFouDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_FOUR_DECIMAL"));
        mnuDecimalPlaces.add(mnuFouDecimal);
        mnuFouDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.FOUR_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.FOUR_DECIMAL_PLACES){
//            mnuFouDecimal.setSelected(true);
//        }
        oGroup.add(mnuFouDecimal);
        mnuFiveDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_FIVE_DECIMAL"));
        mnuDecimalPlaces.add(mnuFiveDecimal);
        mnuFiveDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.FIVE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.FIVE_DECIMAL_PLACES){
//            mnuFiveDecimal.setSelected(true);
//        }
        oGroup.add(mnuFiveDecimal);
        mnuSixDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_SIX_DECIMAL"));
        mnuDecimalPlaces.add(mnuSixDecimal);
        mnuSixDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.SIX_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.SIX_DECIMAL_PLACES){
//            mnuSixDecimal.setSelected(true);
//        }
        oGroup.add(mnuSixDecimal);
        GUISettings.applyOrientation(mnuDecimalPlaces);
    }

    //default curency mode deci

    private void createDCDecimalMenu(JPopupMenu parent) {
        TWMenu mnuDCDecimalPlaces = new TWMenu(Language.getString("POPUP_DECIMAL_PLACES"), "decimalplaces.gif");
        parent.add(mnuDCDecimalPlaces);

        //transHistoryTable.getPopup().add(mnuDecimalPlaces);
        ButtonGroup oGroup = new ButtonGroup();
        mnuDCNoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_NO_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCNoDecimal);
        mnuDCNoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //todo  for DC tble
                setDCDecimalPlaces(Constants.NO_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.NO_DECIMAL_PLACES){
//            mnuNoDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCNoDecimal);
        mnuDCDefaultDecimal = new TWRadioButtonMenuItem(Language.getString("DEFAULT_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCDefaultDecimal);
        mnuDCDefaultDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //todo  for dc
                setDCDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.NO_DECIMAL_PLACES){
//            mnuNoDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCDefaultDecimal);
        mnuDCOneDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_ONE_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCOneDecimal);
        mnuDCOneDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDCDecimalPlaces(Constants.ONE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.ONE_DECIMAL_PLACES){
//            mnuOneDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCOneDecimal);
        mnuDCTwoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_TWO_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCTwoDecimal);
        mnuDCTwoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDCDecimalPlaces(Constants.TWO_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.TWO_DECIMAL_PLACES){
//            mnuTwoDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCTwoDecimal);

        mnuDCThrDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_THREE_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCThrDecimal);
        mnuDCThrDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDCDecimalPlaces(Constants.THREE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.THREE_DECIMAL_PLACES){
//            mnuThrDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCThrDecimal);
        mnuDCFouDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_FOUR_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCFouDecimal);
        mnuDCFouDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDCDecimalPlaces(Constants.FOUR_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.FOUR_DECIMAL_PLACES){
//            mnuFouDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCFouDecimal);
        mnuDCFiveDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_FIVE_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCFiveDecimal);
        mnuDCFiveDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDCDecimalPlaces(Constants.FIVE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.FIVE_DECIMAL_PLACES){
//            mnuFiveDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCFiveDecimal);
        mnuDCSixDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_SIX_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCSixDecimal);
        mnuDCSixDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDCDecimalPlaces(Constants.SIX_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.SIX_DECIMAL_PLACES){
//            mnuSixDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCSixDecimal);
        GUISettings.applyOrientation(mnuDCDecimalPlaces);
    }

    private void createCustomAverageCostMenu(JPopupMenu parent) {
        TWMenuItem mnueditAverageCost = new TWMenuItem(Language.getString("EDIT_AVG_COST"), "editavgcost.gif");
        mnueditAverageCost.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String symbol = (String) valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), -1);
                String pfID = (String) valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), -11);
                new EditAverageCostDialog(symbol, selfRef, pfID);
            }
        });
        GUISettings.applyOrientation(mnueditAverageCost);
        parent.add(mnueditAverageCost);
    }

    private void createDCCustomAverageCostMenu(JPopupMenu parent) {
        TWMenuItem mnuDCeditAverageCost = new TWMenuItem(Language.getString("EDIT_AVG_COST"), "editavgcost.gif");
        mnuDCeditAverageCost.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                String symbol = (String) dCValTable.getTable().getModel().getValueAt(dCValTable.getTable().getSelectedRow(), -1);
                String pfID = (String) dCValTable.getTable().getModel().getValueAt(dCValTable.getTable().getSelectedRow(), -11);
                new EditAverageCostDialog(symbol, selfRef, pfID);
            }
        });
        GUISettings.applyOrientation(mnuDCeditAverageCost);
        parent.add(mnuDCeditAverageCost);
    }

    private void createWhatIfCalculatormenu(JPopupMenu parent) {
        TWMenuItem whatIfCal = new TWMenuItem(Language.getString("FIX_AVERAGE_COST"), "editavgcost.gif");
        whatIfCal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String symbol = (String) valuationTable.getTable().getModel().getValueAt(valuationTable.getTable().getSelectedRow(), -1);
                wiframe = new WhatiFCalculatorPortfolioFrame(symbol, WhatiFCalculatorPortfolioFrame.MODE_TRADING);
                wiframe.setSize(new Dimension(290, 320));
                wiframe.setClosable(true);
                wiframe.setIconifiable(true);
                wiframe.setVisible(true);
                wiframe.setLayer(GUISettings.TICKER_LAYER);
                wiframe.setLocationRelativeTo(Client.getInstance().getDesktop());
            }
        });
        GUISettings.applyOrientation(whatIfCal);
        parent.add(whatIfCal);
    }

    private void createDCWhatIfCalculatormenu(JPopupMenu parent) {
        TWMenuItem dcwhatIfCal = new TWMenuItem(Language.getString("FIX_AVERAGE_COST"), "editavgcost.gif");
        dcwhatIfCal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                String symbol = (String) dCValTable.getTable().getModel().getValueAt(dCValTable.getTable().getSelectedRow(), -1);
                wiframe = new WhatiFCalculatorPortfolioFrame(symbol, WhatiFCalculatorPortfolioFrame.MODE_TRADING);
                wiframe.setSize(new Dimension(290, 320));
                wiframe.setClosable(true);
                wiframe.setIconifiable(true);
                wiframe.setVisible(true);
                wiframe.setLayer(GUISettings.TICKER_LAYER);
                wiframe.setLocationRelativeTo(Client.getInstance().getDesktop());
            }
        });
        GUISettings.applyOrientation(dcwhatIfCal);
        parent.add(dcwhatIfCal);
    }

    private void setDecimalPlaces(byte places) {
        decimalPlaces = places;
        ((SmartTable) valuationTable.getTable()).setDecimalPlaces(places);
        valuationTable.updateGUI();
        valuationTable.getTable().updateUI();
        oSettings1.putProperty(ViewConstants.VC_DECIMAL_PLACES, places);
        if (decimalPlaces == Constants.NO_DECIMAL_PLACES) {
            mnuNoDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.ONE_DECIMAL_PLACES) {
            mnuOneDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.UNASSIGNED_DECIMAL_PLACES) {
            mnuDefaultDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.TWO_DECIMAL_PLACES) {
            mnuTwoDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.THREE_DECIMAL_PLACES) {
            mnuThrDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.FOUR_DECIMAL_PLACES) {
            mnuFouDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.FIVE_DECIMAL_PLACES) {
            mnuFiveDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.SIX_DECIMAL_PLACES) {
            mnuSixDecimal.setSelected(true);
        }
    }


    //for default currency table

    private void setDCDecimalPlaces(byte places) {
        dcDecimalPlaces = places;
        ((SmartTable) dCValTable.getTable()).setDecimalPlaces(places);
        dCValTable.updateGUI();
        dCValTable.getTable().updateUI();
        //todo another viw setting
        oSettings2.putProperty(ViewConstants.VC_DECIMAL_PLACES, places);
        if (dcDecimalPlaces == Constants.NO_DECIMAL_PLACES) {
            mnuDCDecimalPlaces.setSelected(true);

        } else if (dcDecimalPlaces == Constants.ONE_DECIMAL_PLACES) {
            mnuDCOneDecimal.setSelected(true);
        } else if (dcDecimalPlaces == Constants.UNASSIGNED_DECIMAL_PLACES) {
            //mnuDefaultDecimal.setSelected(true);
            mnuDCDefaultDecimal.setSelected(true);
        } else if (dcDecimalPlaces == Constants.TWO_DECIMAL_PLACES) {
            mnuDCTwoDecimal.setSelected(true);
        } else if (dcDecimalPlaces == Constants.THREE_DECIMAL_PLACES) {
            mnuDCThrDecimal.setSelected(true);
        } else if (dcDecimalPlaces == Constants.FOUR_DECIMAL_PLACES) {
            mnuDCFouDecimal.setSelected(true);
        } else if (dcDecimalPlaces == Constants.FIVE_DECIMAL_PLACES) {
            mnuDCFiveDecimal.setSelected(true);
        } else if (dcDecimalPlaces == Constants.SIX_DECIMAL_PLACES) {
            mnuDCSixDecimal.setSelected(true);
        }
    }

    public void applicationExiting() {
    }

    public void applicationLoaded() {
    }

    public void applicationLoading(int percentage) {
    }

    public void applicationReadyForTransactions() {
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void snapshotProcessingEnded(Exchange exchange) {
    }

    public void snapshotProcessingStarted(Exchange exchange) {
    }

    public void workspaceLoaded() {
        try {
            decimalPlaces = Byte.parseByte(oSettings1.getProperty(ViewConstants.VC_DECIMAL_PLACES));
            dcDecimalPlaces = Byte.parseByte(oSettings2.getProperty(ViewConstants.VC_DECIMAL_PLACES));
        } catch (Exception e) {
            decimalPlaces = Constants.TWO_DECIMAL_PLACES;
            dcDecimalPlaces = Constants.TWO_DECIMAL_PLACES;
        }
        setDecimalPlaces(decimalPlaces);
        setDCDecimalPlaces(dcDecimalPlaces);
    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void workspaceWillLoad() {

    }
}
