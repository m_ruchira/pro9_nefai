package com.isi.csvr.trading.portfolio;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 09-Apr-2007
 * Time: 17:54:41
 * To change this template use File | Settings | File Templates.
 */

import com.isi.csvr.Client;
import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.portfolio.PFStore;
import com.isi.csvr.trading.portfolio.*;
import com.isi.csvr.trading.portfolio.ValuationRecord;
import com.isi.csvr.trading.datastore.CurrencyStore;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.shared.*;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.KeyListener;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.ActionEvent;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 09-Apr-2007
 * Time: 10:52:57
 * To change this template use File | Settings | File Templates.
 */
public class EditAverageCostDialog extends JDialog implements KeyListener, ActionListener {

    private JPanel mainPanel;
    private JPanel upperPanel;
    private JPanel descriptionPanel;

    private JPanel lowerPanel;
    private JLabel averageCost;
    private JLabel averageCostNew;
    private JLabel descriptionLabel;

    private TWTextField averageCostValue;
    private TWTextField averageCostValueNew;

    private TWButton resetButton;
    private TWButton okButton;
    private TWButton cancelButton;

    TradePortfolios tradePortfolios;
    private String sskey = null;
    public static TradingPortfolioWindow tradingPortfolioWindow;
    private String pid;
    private String currency = null;

    public EditAverageCostDialog(String skey, TradingPortfolioWindow tradingPFWindow, String pid) {
        super(Client.getInstance().getFrame(), true);
        this.tradingPortfolioWindow = tradingPFWindow;
        this.sskey = skey;
        this.pid = pid;
        setTitle(Language.getString("EDIT_AVG_COST"));
        setSize(350, 160);
        this.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 4, 4));
        this.add(createMainPanel());
        getAverageCost(skey);
        setLongdescription(skey);
        GUISettings.setLocationRelativeTo(this, Client.getInstance().getFrame());
        GUISettings.applyOrientation(this);
        setResizable(false);
        this.setVisible(true);
    }

    private JPanel createMainPanel() {
        mainPanel = new JPanel();
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"20", "60", "25"}, 4, 4));
        mainPanel.add(createDescriptionpanel());
        mainPanel.add(createUpperPanel());
        mainPanel.add(createLowerpanel());
        return mainPanel;
    }

    private JPanel createUpperPanel() {
        averageCost = new JLabel(Language.getString("AVG_COST"));
        averageCostNew = new JLabel(Language.getString("AVG_COST_CUSTOMER"));
        averageCostValue = new TWTextField();
        averageCostValueNew = new TWTextField();
        averageCostValueNew.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_DECIMAL, 4));
        averageCostValue.setEditable(false);
        upperPanel = new JPanel();
        upperPanel.setLayout(new FlexGridLayout(new String[]{"5", "110", "50", "140", "5"}, new String[]{"25", "25"}, 4, 4));
        upperPanel.add(new JLabel());
        upperPanel.add(averageCost);
        upperPanel.add(new JLabel("(" + setActualCurrency(this.sskey) + ")"));
        upperPanel.add(averageCostValue);
        upperPanel.add(new JLabel());
        upperPanel.add(new JLabel());
        upperPanel.add(averageCostNew);
        upperPanel.add(new JLabel("(" + setActualCurrency(this.sskey) + ")"));
        upperPanel.add(averageCostValueNew);
        upperPanel.add(new JLabel());
        return upperPanel;
    }

    private JPanel createLowerpanel() {
        resetButton = new TWButton(Language.getString("RESET"));
        okButton = new TWButton(Language.getString("OK"));
        cancelButton = new TWButton(Language.getString("CANCEL"));
        lowerPanel = new JPanel();
        lowerPanel.setLayout(new FlexGridLayout(new String[]{"5", "88", "15", "88", "15", "88", "5"}, new String[]{"22"}, 4, 4));
        lowerPanel.add(new JLabel());
        lowerPanel.add(resetButton);
        lowerPanel.add(new JLabel());
        lowerPanel.add(okButton);
        lowerPanel.add(new JLabel());
        lowerPanel.add(cancelButton);
        lowerPanel.add(new JLabel());
        okButton.addActionListener(this);
        resetButton.addActionListener(this);
        cancelButton.addActionListener(this);
        return lowerPanel;
    }

    private JPanel createDescriptionpanel() {
        descriptionPanel = new JPanel();
        descriptionPanel.setLayout(new FlexGridLayout(new String[]{"5", "100%"}, new String[]{"100%"}, 4, 4));
        descriptionLabel = new JLabel();
        descriptionLabel.setForeground(Color.BLACK);
        descriptionLabel.setFont(new TWFont("Arial", 1, 14));
        descriptionLabel.setHorizontalAlignment(SwingConstants.LEADING);
        descriptionPanel.add(new JLabel());
        descriptionPanel.add(descriptionLabel);

        return descriptionPanel;
    }

    private void setLongdescription(String skey) {
        try {
            descriptionLabel.setText(DataStore.getSharedInstance().getStockObject(skey).getLongDescription());
        } catch (Exception e) {
            System.out.println("Long description not set....");
            e.printStackTrace();
        }

    }

    private String setActualCurrency(String skey) {

        tradePortfolios = TradePortfolios.getInstance();
        TransactRecord trecord = (TransactRecord) tradePortfolios.getTransactRecord(skey);
        currency = DataStore.getSharedInstance().getStockObject(skey).getCurrencyCode();

        return currency;
    }

    private void getAverageCost(String skey) {
        try {
            tradePortfolios = TradePortfolios.getInstance();
            TransactRecord trecord = (TransactRecord) tradePortfolios.getTransactRecord(skey, this.pid);
            averageCostValue.setText(SharedMethods.fomatAVGCurrency(trecord.getrealaverageCost()));
            averageCostValueNew.setText(SharedMethods.fomatAVGCurrency(trecord.getPrice()));

        } catch (Exception e) {
            System.out.println("Getting values failed......");
        }
    }

    private void setCustomerAverageCost() {
        try {
            tradePortfolios = TradePortfolios.getInstance();
            TransactRecord trecord = (TransactRecord) tradePortfolios.getTransactRecord(sskey, this.pid);
            if (!trecord.addCostomerAverageCost) {
                trecord.setIschanged(true);
            } else {
                System.out.println("average cost previously changed....");
            }
            trecord.setCustomerAVGPPrice(Float.parseFloat(averageCostValueNew.getText()));
            TradePortfolios.changedAVGCost.put(sskey + "@" + trecord.getPfID(), Float.parseFloat(averageCostValueNew.getText()));
            TradePortfolios.getInstance().props.setProperty(sskey + "@" + trecord.getPfID(), averageCostValueNew.getText());
        } catch (Exception e) {
            System.out.println("Setting Customer average cost fail....");

        }

    }

    private void resetAverageCost() {
        try {
            tradePortfolios = TradePortfolios.getInstance();
            TransactRecord trecord = tradePortfolios.getTransactRecord(sskey, this.pid);
            trecord.setRealAverageCost(trecord.getrealaverageCost());
            trecord.setIschanged(false);
            TradePortfolios.changedAVGCost.remove(sskey + "@" + trecord.getPfID());
        } catch (Exception e) {
            System.out.println("Average cost resetting failed...");

        }


    }

    public static TradingPortfolioWindow getTPFWindow() {
        return tradingPortfolioWindow;
    }

    public void keyTyped(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyPressed(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void actionPerformed(ActionEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
        if (e.getSource() == okButton) {
            setCustomerAverageCost();
            this.tradingPortfolioWindow.createValuationList();
            this.dispose();

        }
        if (e.getSource() == resetButton) {
            resetAverageCost();
            this.tradingPortfolioWindow.createValuationList();
            this.dispose();

        }
        if (e.getSource() == cancelButton) {
            this.dispose();

        }
    }


}

