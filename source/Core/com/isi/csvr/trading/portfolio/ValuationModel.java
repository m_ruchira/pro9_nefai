// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.trading.portfolio;

/**
 * A Class class.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.PortfolioInterface;
import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.dde.DDELinkInterface;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.datastore.CurrencyStore;
import com.isi.csvr.trading.datastore.TPlusStore;
import com.isi.csvr.trading.datastore.TradingCurrencyConverter;
import com.isi.csvr.trading.shared.TradeKey;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;
import java.awt.*;
import java.awt.datatransfer.Clipboard;
import java.awt.datatransfer.ClipboardOwner;
import java.awt.datatransfer.StringSelection;
import java.awt.datatransfer.Transferable;
import java.util.ArrayList;

public class ValuationModel
        extends CommonTable
        implements TableModel, CommonTableInterface, DDELinkInterface, ClipboardOwner {

    private TradePortfolios tradePortfolios;
    private ArrayList dataStore;
    //    private ValuationModel  selfReference; // Holds the self reference
    private boolean isCashIncluded = false;
    private TradingPortfolioWindow pfWindow;
    private String sCash;
    private Clipboard clip;
    //private String  baseCurrency = "SR";

    /**
     * Constructor
     */
    public ValuationModel(ArrayList dataStore) {
        this.dataStore = dataStore;
//        selfReference = this;
        clip = Toolkit.getDefaultToolkit().getSystemClipboard();
        tradePortfolios = TradePortfolios.getInstance();
        sCash = Language.getString("CASH");
    }

    public void includeCashBalance(boolean status) {
        isCashIncluded = status;
    }

    public void setPFWindow(TradingPortfolioWindow pfWindow) {
        this.pfWindow = pfWindow;
    }

    public void setSymbol(String symbol) {

    }

    /* --- Table Model metods start from here --- */

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public void clear() {
        this.dataStore.clear();
    }

    public int getRowCount() {
        if (TradingShared.isReadyForTrading()) {
            if (isCashIncluded)
                return dataStore.size() + 2;
            else
                return dataStore.size() + 1;
        } else {
            return 0;
        }
    }

    public Object getValueAt(int row, int col) {
        //float conversionRate = CurrencyStore.getCurrency("SR|" + getBaseCurrency());
        int extraRows = 1;
        int gainLossModifier = 1;
        //int lotSize = 1; // for Futures only
        if (isCashIncluded)
            extraRows = 2;
        try {
            if (row < (getRowCount() - extraRows)) { // only transaction rows
                ValuationRecord record = (ValuationRecord) dataStore.get(row);
                if (record == null)
                    return ""; //null;
                Stock stock = PortfolioInterface.getStockObject(record.getSKey().getExchange(), record.getSKey().getSymbol());
//                if (stock == null)
//                    return ""; //null;
                String exchange = null;
                try {
                    exchange = stock.getExchange();
                } catch (Exception e) {
                    exchange = SharedMethods.getExchangeFromKey(record.getSKey().getExchange());
                }

                if (record.getOpenSellCount() > 0) { // this is a Sell Contract
                    gainLossModifier = -1; // for Sell contracts,  negative gain is a profit
                }

                /*try {
                    FutureBaseAttributes futureBaseAttributes = FutureBaseStore.getSharedInstance().getAttribute(SharedMethods.getKey(exchange, stock.getOptionBaseSymbol()));
                    lotSize = futureBaseAttributes.getContractSize();
                } catch (Exception e) {
                }*/

                switch (col) {
                    case -11:
                        return record.getPortfolioID();
                    case -10:
                        try {
                            if ((tradePortfolios.getTransactRecord(record.getSKey().getTradeKey())).addCostomerAverageCost) {
                                return 1; // custom mode
                            } else {
                                return 2; // normal mode
                            }
                        } catch (Exception e) {
                            return 2;
                        }
                    case -4:
                        if (stock != null) {
                            return stock.getDecimalCount();
                        } else {
                            return 2;
                        }
                    case -1:
                        if (stock != null) {
                            return stock.getKey();
                        } else {
                            return record.getSKey();
                        }
                    case 0:     // SYMBOL
                        if (stock != null) {
                            return TradingShared.getDisplaySmbol(stock.getSymbolCode(), record.getPortfolioID());
                        } else {
                            return TradingShared.getDisplaySmbol(record.getSKey().getSymbol(), record.getPortfolioID());
                        }
                    case 1:     // EXCHANGE CODE
                        if (stock != null) {
//                            return PortfolioInterface.getExchangeCode(stock);
                            return ExchangeStore.getSharedInstance().getExchange(PortfolioInterface.getExchangeCode(stock).trim()).getDisplayExchange(); //Display Exchange

                        } else {
//                            return SharedMethods.getExchangeFromKey(record.getSKey());
                            return ExchangeStore.getSharedInstance().getExchange(record.getSKey().getExchange().trim()).getDisplayExchange(); //Display Exchange

                        }
                    case 2:     // COMPANY NAME
                        if (stock != null) {
                            if (PortfolioInterface.getCompanyName(stock) == null)
                                return "";
                            else
                                return PortfolioInterface.getCompanyName(stock);
                        } else {
                            return TradingShared.getDisplaySmbol(record.getSKey().getSymbol(), record.getPortfolioID());
                        }
                    case 3:     // CURRENCY
                        if (record.getCurrency() == null) {
                            if (PortfolioInterface.getCurrency(stock) == null) {
                                return "";
                            } else {
                                return TradingCurrencyConverter.getSharedInstance().getDisplayCurrency(PortfolioInterface.getCurrency(stock));
                            }
                        } else {
                            return record.getCurrency();
                        }
                    case 4:     // EXCHANGE(Conversion) RATE
//                        return CurrencyStore.getCurrency(PortfolioInterface.getCurrency(stock)
//                            + "|" + PFStore.getBaseCurrency()) + "";
                        // trad
//                        return new Float(CurrencyStore.getCurrency(PortfolioInterface.getCurrency(stock)
                        // Reg
                        if (record.getCurrency() == null) {
                            return CurrencyStore.getBuyRate(PortfolioInterface.getCurrency(stock), TradePortfolios.getInstance().getBaseCurrency(), TradingShared.getTrader().getPath(record.getPortfolioID()));
                        } else {
                            return CurrencyStore.getBuyRate(record.getCurrency(), TradePortfolios.getInstance().getBaseCurrency(), TradingShared.getTrader().getPath(record.getPortfolioID()));
                        }
                    case 5:     // Owned
                        if (stock != null) {
                            return (long) record.getOwned(stock.getInstrumentType());
                        } else {
                            return (long) record.getOwned(record.getSKey().getInstrumentType());
                        }
                    case 6:   //  Pledge
                        return record.getPledged();
                    case 7: // Balance
                        return record.getBalance();
                    case 8:     // AVERAGE COST
                        if (record.getCurrency() == null) {
                            return (convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), false, record.getAvgCost(), record.getConversionRate(), record.getPortfolioID()));
                        } else {
                            return (convertToSelectedCurrency(exchange, record.getCurrency(), false, record.getAvgCost(), record.getConversionRate(), record.getPortfolioID()));
                        }
                    case 9:     // LATEST(MARKET) PRICE
                        if (stock != null) {
                            if (record.getCurrency() == null) {
                                if (stock.getTodaysClose() > 0) {
                                    return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), true, PortfolioInterface.getTodaysClose(stock), record.getConversionRate(), record.getPortfolioID());
                                }
                                else if (stock.getLastTradeValue() > 0) {
                                    return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), true, PortfolioInterface.getLastTrade(stock), record.getConversionRate(), record.getPortfolioID());
                                }
                                else {
                                    return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), true, PortfolioInterface.getPreviousClosed(stock), record.getConversionRate(), record.getPortfolioID());
                                }
                            } else {
                                if (stock.getTodaysClose() > 0) {
                                    return convertToSelectedCurrency(exchange, record.getCurrency(), true, PortfolioInterface.getTodaysClose(stock), record.getConversionRate(), record.getPortfolioID());
                                }
                                else if (stock.getLastTradeValue() > 0) {
                                    return convertToSelectedCurrency(exchange, record.getCurrency(), true, PortfolioInterface.getLastTrade(stock), record.getConversionRate(), record.getPortfolioID());
                                }
                                else {
                                    return convertToSelectedCurrency(exchange, record.getCurrency(), true, PortfolioInterface.getPreviousClosed(stock), record.getConversionRate(), record.getPortfolioID());
                                }
                            }
                        } else {
                            return 0D;
                        }

                    case 10:     // TOTAL COST (COST BASICS)
                        if (record.getCurrency() == null) {
                            return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), false, record.getCumCost(), record.getConversionRate(), record.getPortfolioID());
                        } else {
                            return convertToSelectedCurrency(exchange, record.getCurrency(), false, record.getCumCost(), record.getConversionRate(), record.getPortfolioID());
                        }
//                        return new Long((long)convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), record.getTotCost()));
                    case 11:     // MARKET VALUE (VALUATION)
                        if (stock != null) {
                            if (record.getCurrency() == null) {
                                if (stock.getTodaysClose() > 0) {
                                    return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
                                            (record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getTodaysClose(stock) +
                                                    record.getCashDividends(), record.getConversionRate(), record.getPortfolioID());
                                }
                                else if (stock.getLastTradeValue() > 0) {
                                    return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
                                            (record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getLastTrade(stock) +
                                                    record.getCashDividends(), record.getConversionRate(), record.getPortfolioID());
                                }
                                else {
                                    return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock),
                                            (record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getPreviousClosed(stock) +
                                                    record.getCashDividends(), record.getConversionRate(), record.getPortfolioID());
                                }
                            } else {
                                if (stock.getTodaysClose() > 0) {
                                    return convertToSelectedCurrency(exchange, record.getCurrency(),
                                            (record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getTodaysClose(stock) +
                                                    record.getCashDividends(), record.getConversionRate(), record.getPortfolioID());
                                }
                                else if (stock.getLastTradeValue() > 0) {
                                    return convertToSelectedCurrency(exchange, record.getCurrency(),
                                            (record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getLastTrade(stock) +
                                                    record.getCashDividends(), record.getConversionRate(), record.getPortfolioID());
                                }
                                else {
                                    return convertToSelectedCurrency(exchange, record.getCurrency(),
                                            (record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getPreviousClosed(stock) +
                                                    record.getCashDividends(), record.getConversionRate(), record.getPortfolioID());
                                }
                            }
                        } else {
                            return 0D;
                        }

                    case 12:    //  GAIN LOSS TODAY
                        if (stock != null) {
                            if (record.getCurrency() == null) {
                                return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), ((record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getChange(stock)[0]) * record.getPriceCorrectionFactor() * gainLossModifier, record.getConversionRate(), record.getPortfolioID());
                            } else {
                                return convertToSelectedCurrency(exchange, record.getCurrency(), ((record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getChange(stock)[0]) * record.getPriceCorrectionFactor() * gainLossModifier, record.getConversionRate(), record.getPortfolioID());
                            }
                        } else {
                            return 0;
                        }

                    case 13:    //  % GAIN LOSS TODAY
                        if (stock != null) {
                            return adjustToPriceModofication(exchange, (((record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getChange(stock)[0] * gainLossModifier) / record.getCumCost()) * 100, record.getConversionRate());
                        } else {
                            return 0;
                        }
                    case 14: {  //  GAIN LOSS OVERALL
                        double marketValue = 0f;
                        if (stock != null) {
                            if (stock.getTodaysClose() > 0) {
                                marketValue = adjustToPriceModofication(exchange, (((record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getTodaysClose(stock))), 1D);
                            }
                            else if (stock.getLastTradeValue() > 0) {
                                marketValue = adjustToPriceModofication(exchange, (((record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getLastTrade(stock))), 1D);
                            }
                            else {
                                marketValue = adjustToPriceModofication(exchange, ((record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getPreviousClosed(stock)), 1D);
                            }
                            marketValue += record.getCashDividends();
                            if (record.getCurrency() == null) {
                                return convertToSelectedCurrency(exchange, PortfolioInterface.getCurrency(stock), false, (marketValue - record.getCumCost()) * record.getPriceCorrectionFactor() * gainLossModifier, record.getConversionRate(), record.getPortfolioID());
                            } else {
                                return convertToSelectedCurrency(exchange, record.getCurrency(), false, (marketValue - record.getCumCost()) * record.getPriceCorrectionFactor() * gainLossModifier, record.getConversionRate(), record.getPortfolioID());
                            }
                        } else {
                            return 0;
                        }
                    }
                    case 15: {  //  % GAIN LOSS OVERALL
                        double marketValue = 0f;
                        if (stock != null) {
                            if (stock.getTodaysClose() > 0) {
                                marketValue = adjustToPriceModofication(exchange, ((record.getBalance() + record.getSellPending()) * record.getContractSize() *
                                        PortfolioInterface.getTodaysClose(stock)), record.getConversionRate());
                            }
                            else if (stock.getLastTradeValue() > 0) {
                                marketValue = adjustToPriceModofication(exchange, ((record.getBalance() + record.getSellPending()) * record.getContractSize() *
                                        PortfolioInterface.getLastTrade(stock)), record.getConversionRate());
                            }
                            else {
                                marketValue = adjustToPriceModofication(exchange, ((record.getBalance() + record.getSellPending()) * record.getContractSize() *
                                        PortfolioInterface.getPreviousClosed(stock)), record.getConversionRate());
                            }
                            marketValue += record.getCashDividends();
                            return ((marketValue - record.getCumCost()) / record.getCumCost()) * 100 * gainLossModifier;
                        } else {
                            return 0;
                        }
//                        return new Float(convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((marketValue - record.getTotCost()) / record.getTotCost()))*100);
                    }
                    case 16:   //  % OF PORTFOLIO(HOLDINGS)
                        try {
                            return getPercOfPortfolio(row);
                        } catch (Exception e) {
                            return 0;
                        }
                    case 17:   //  CASH DIVIDENDS
                        return record.getCashDividends();
                    case 18:   //  BUY PENDING
                        return record.getBuyPending();
                    case 19:   //  SELL PENDING
                        return record.getSellPending();
                    case 20:   //  Company code
                        if (stock != null) {
                            return PortfolioInterface.getCompanyCode(stock);
                        } else {
                            return TradingShared.getDisplaySmbol(record.getSKey().getSymbol(), record.getPortfolioID());
                        }
                    case 21:   //  margin due ammount
                        return record.getMarginDue();
                    case 22:   //  daily margin due ammount
                        return record.getDayMarginDue();
                    case 23:   //  open buy contracts
                        return (long) record.getOpenBuyCount();
                    case 24:   //  open sell contracts
                        return (long) record.getOpenSellCount();
                    case 25:   //  open sell contracts
                        return record.getBuyPending();

                    case 26:   //  portfolio name
                        return " " + TradePortfolios.getInstance().getPortfolioName(record.getPortfolioID()) + " ";
                    case 27:   //  bookkeeper
                        return "" + TPlusStore.getSharedInstance().getBookKeeper(record.getBookKeeper()).getName();


                    case 28:
                        return record.getTPlusPendingStock();
                    case 29:
                        return (long) record.getMarginHoldings();
                    case 30:
                        return record.getSellPending();
                    case 31:
                        return record.getPayableHoldings();
                    case 32:
                        return record.getReceivableHoldings();
                    case 33:
                        int ins = stock.getInstrumentType();
                        if(ins==0){
                            return  "Common";
                        }else if(ins==66){
                            return  "Rights";
                        }else if(ins==75){
                            return  "Bonds";
                        }else if(ins==10){
                            return  "Options";
                        }else if(ins==66){
                            return  "Warrant";
                        }else if(ins==86){
                            return  "ETF";
                        }else if(ins==3){
                            return  "Mutual Funds";
                        }
                    case 34:
                        return record.getPayableHoldings();
                    case 35:
                        return record.getReceivableHoldings();
                    case 36:
                        return (record.getSellPending()+record.getPledged()+record.getPayableHoldings());
                    case 37:
                        return record.getWANC();
                    case 38:
                        return record.getAveragePrice();
                    default:
                        return "";
                }
            } else if (isCashIncluded && (row == (getRowCount() - 2))) {  //cash balance row 
                switch (col) {
                    case 2:     // COMPANY NAME
                        return sCash + " (" + TradePortfolios.getInstance().getBaseCurrency() + ")";
                    case 10:             // CUMULATIVE TOTAL COST
                        return pfWindow.getCashBalanceForSelectedPFs();
                    case 11:             // CUMULATIVE MARKET VALUE
                        return pfWindow.getCashBalanceForSelectedPFs();
                    case 16:            //  % OF PORTFOLIO
                        try {
                            return getPercOfPortfolio(row);
                        } catch (Exception e) {
                            return 0;
                        }
//                    case 15:   //  CASH DIVIDENDS
//                        return new Float(getTotalCashDividends());
                    default:
                        return "";
                }
            } else if (row == (getRowCount() - 1)) {
                switch (col) {
                    case 10:         // CUMULATIVE TOTAL COST
                        try {
                            return getCumulativeTotalCost();
                        } catch (Exception e) {
                            return 0;
                        }
//                        return new Long((long)getCumulativeTotalCost());
                    case 11:         // CUMULATIVE MARKET VALUE
                        return getMarketValue();
                    case 12:        // CUMULATIVE GAIN LOSS TODAY
                        return getCumulativeGainLoss();
                    case 13: {       // CUMULATIVE % GAIN LOSS TODAY
                        double cumCost = getCumulativeTotalCost();
                        if (cumCost == 0)
                            return 0D;
                        else
                            return (getCumulativeGainLoss() / cumCost) * 100;
//                        return new Float(getCumulativePercentGainLoss());
                    }
                    case 14:        // CUMULATIVE GAIN LOSS OVERALL
                        return getCumGainLossOverall();
                    case 15: {       // CUMULATIVE % GAIN LOSS OVERALL
//                        return new Float(getCumPercentGainLossOverall());
                        double cumCost = getCumulativeTotalCost();
                        if (cumCost == 0)
                            return 0f;
                        else
                            return (getCumGainLossOverall() / cumCost) * 100;
                    }
                    case 16:        //  % OF PORTFOLIO
                        if (getRowCount() == 1)
                            return new Double("0");
                        else
                            return new Double("100");
                    case 17:   //  CASH DIVIDENDS
                        return getTotalCashDividends();
                    case 18:
                        return getCumulativeBuyPending();
                    case 19:
                        return getCumulativeSellPending();
                    default:
                        return "";
                }
            } else {
                return "";
            }
        } catch (Exception e) {
//            e.printStackTrace();
            return "";
        }
    }

    public double getTotalCashDividends() {
        double totMValue = 0D;
        ValuationRecord record = null;
        for (Object aDataStore : dataStore) {
            try {
                record = (ValuationRecord) aDataStore;
                totMValue += record.getCashDividends();
                record = null;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return totMValue;
    }

    public double getPercOfPortfolio(int row) {
        double mValue = 0D;
        double selMValue = 0D;
        double totMValue = 0D;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            try {
                record = (ValuationRecord) dataStore.get(i);
                stock = PortfolioInterface.getStockObject(record.getSKey().getExchange(), record.getSKey().getSymbol());
                if (record.getCurrency() == null) {
                    if (stock.getTodaysClose() > 0) {
                        mValue = (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),
                                PortfolioInterface.getTodaysClose(stock) +
                                        record.getCashDividends(), record.getConversionRate(), record.getPortfolioID());
                    }
                    else if (stock.getLastTradeValue() > 0) {
                        mValue = (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),
                                PortfolioInterface.getLastTrade(stock) +
                                        record.getCashDividends(), record.getConversionRate(), record.getPortfolioID());
                    }
                    else {
                        mValue = (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock),
                                PortfolioInterface.getPreviousClosed(stock) +
                                        record.getCashDividends(), record.getConversionRate(), record.getPortfolioID());
                    }
                } else {
                    if (stock.getTodaysClose() > 0) {
                        mValue = (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), record.getCurrency(),
                                PortfolioInterface.getTodaysClose(stock) +
                                        record.getCashDividends(), record.getConversionRate(), record.getPortfolioID());
                    }
                    else if (stock.getLastTradeValue() > 0)
                        mValue = (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), record.getCurrency(),
                                PortfolioInterface.getLastTrade(stock) +
                                        record.getCashDividends(), record.getConversionRate(), record.getPortfolioID());
                    else {
                        mValue = (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), record.getCurrency(),
                                PortfolioInterface.getPreviousClosed(stock) +
                                        record.getCashDividends(), record.getConversionRate(), record.getPortfolioID());
                    }
                }
                if (row == i)
                    selMValue = mValue;
                totMValue += mValue;
                record = null;
                stock = null;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        if (isCashIncluded) {
            if (row == (getRowCount() - 2))
                selMValue = pfWindow.getCashBalanceForSelectedPFs();
            totMValue += pfWindow.getCashBalanceForSelectedPFs();
        }
        if (totMValue == 0)
            return 0D;
        else
            return (selMValue / totMValue) * 100;
    }

/*
        public float getPercOfPortfolio(int row) {
            float mValue    = 0f;
            float selMValue = 0f;
            float totMValue = 0f;
            Stock stock  = null;
            ValuationRecord record = null;
            for (int i = 0; i < dataStore.size(); i++) {
                record = (ValuationRecord)dataStore.get(i);
                stock  = PortfolioInterface.getStockObject(record.getSKey());
                if (stock.getLastTrade()[0] > 0)
                    mValue += record.getHolding() * convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock)[0]);
                else
                    mValue += record.getHolding() * convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock)[0]);
                if (row == i)
                    selMValue = mValue;
                totMValue += mValue;
                record = null;
                stock  = null;
            }
            if (isCashIncluded) {
                if (row == (getRowCount() - 2))
                    selMValue = pfWindow.getCashBalanceForSelectedPFs();
                totMValue += pfWindow.getCashBalanceForSelectedPFs();
            }
            return (selMValue / totMValue) * 100;
        }
*/

    public double getCumulativeTotalCost() {
        double totCost = 0D;
        Stock stock = null;
        ValuationRecord record = null;
        int gainLossModifier = 1;
        for (int i = 0; i < dataStore.size(); i++) {
            try {
                record = (ValuationRecord) dataStore.get(i);
                stock = PortfolioInterface.getStockObject(record.getSKey().getExchange(), record.getSKey().getSymbol());
                if (record.getOpenSellCount() > 0) { // this is a Sell Contract
                    gainLossModifier = -1; // for Sell contracts,  negative gain is a profit
                } else {
                    gainLossModifier = 1;
                }
                if (record.getCurrency() == null) {
                    totCost += convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), false, record.getCumCost() * record.getPriceCorrectionFactor() * gainLossModifier, record.getConversionRate(), record.getPortfolioID());
                } else {
                    totCost += convertToSelectedCurrency(stock.getExchange(), record.getCurrency(), false, record.getCumCost() * record.getPriceCorrectionFactor() * gainLossModifier, record.getConversionRate(), record.getPortfolioID());
                }
                record = null;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        if (isCashIncluded)
            totCost += pfWindow.getCashBalanceForSelectedPFs();
        return totCost;
    }

    public long getCumulativeBuyPending() {
        long total = 0;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            try {
                record = (ValuationRecord) dataStore.get(i);
                total += record.getBuyPending();
                record = null;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return total;
    }

    public long getCumulativeSellPending() {
        long total = 0;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            try {
                record = (ValuationRecord) dataStore.get(i);
                total += record.getSellPending();
                record = null;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return total;
    }

    public double getMarketValue() {
        double mValue = 0D;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            try {
                record = (ValuationRecord) dataStore.get(i);
                stock = PortfolioInterface.getStockObject(record.getSKey().getExchange(), record.getSKey().getSymbol());
                if (record.getCurrency() == null) {
                    if (stock.getTodaysClose() > 0) {
                        mValue += (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getTodaysClose(stock), record.getConversionRate(), record.getPortfolioID());
                    }
                    else if (stock.getLastTradeValue() > 0) {
                        mValue += (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock), record.getConversionRate(), record.getPortfolioID());
                    }
                    else {
                        mValue += (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock), record.getConversionRate(), record.getPortfolioID());
                    }
                } else {
                    if (stock.getTodaysClose() > 0) {
                        mValue += (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), record.getCurrency(), PortfolioInterface.getTodaysClose(stock), record.getConversionRate(), record.getPortfolioID());
                    }
                    else if (stock.getLastTradeValue() > 0) {
                        mValue += (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), record.getCurrency(), PortfolioInterface.getLastTrade(stock), record.getConversionRate(), record.getPortfolioID());
                    }
                    else {
                        mValue += (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), record.getCurrency(), PortfolioInterface.getPreviousClosed(stock), record.getConversionRate(), record.getPortfolioID());
                    }
                }
                record = null;
                stock = null;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        if (isCashIncluded)
            mValue += pfWindow.getCashBalanceForSelectedPFs();
        return mValue;
    }

    public double getCumGainLossOverall() {
        double mValue = 0D;
        double cumValue = 0D;
        Stock stock = null;
        ValuationRecord record = null;
        int gainLossModifier = 1;
        for (int i = 0; i < dataStore.size(); i++) {
            try {
                record = (ValuationRecord) dataStore.get(i);
                stock = PortfolioInterface.getStockObject(record.getSKey().getExchange(), record.getSKey().getSymbol());
                if (record.getOpenSellCount() > 0) { // this is a Sell Contract
                    gainLossModifier = -1; // for Sell contracts,  negative gain is a profit
                } else {
                    gainLossModifier = 1;
                }
                if (stock.getTodaysClose() > 0) {
                    mValue = adjustToPriceModofication(stock.getExchange(), (record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getTodaysClose(stock), 1D);
                }
                else if (stock.getLastTradeValue() > 0) {
                    mValue = adjustToPriceModofication(stock.getExchange(), (record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getLastTrade(stock), 1D);
                }
                else {
                    mValue = adjustToPriceModofication(stock.getExchange(), (record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getPreviousClosed(stock), 1D);
                }
//            mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), (mValue - record.getTotCost()));
                if (record.getCurrency() == null) {
                    cumValue += convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), false, (mValue - record.getCumCost()) * record.getPriceCorrectionFactor() * gainLossModifier, record.getConversionRate(), record.getPortfolioID());
                } else {
                    cumValue += convertToSelectedCurrency(stock.getExchange(), record.getCurrency(), false, (mValue - record.getCumCost()) * record.getPriceCorrectionFactor() * gainLossModifier, record.getConversionRate(), record.getPortfolioID());
                }
                record = null;
                stock = null;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return cumValue;
    }
/*
        public float getCumGainLossOverall() {
            float mValue = 0f;
            Stock stock  = null;
            ValuationRecord record = null;
            for (int i = 0; i < dataStore.size(); i++) {
                record = (ValuationRecord)dataStore.get(i);
                stock  = PortfolioInterface.getStockObject(record.getSKey());
                if (stock.getLastTrade()[0] > 0)
                    mValue += record.getHolding() * PortfolioInterface.getLastTrade(stock)[0];
                else
                    mValue += record.getHolding() * PortfolioInterface.getPreviousClosed(stock)[0];
                mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), (mValue - record.getTotCost()));
                record = null;
                stock  = null;
            }
            return mValue;
        }
*/

    public double getCumPercentGainLossOverall() {
        double mValue = 0D;
        double cumValue = 0D;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            try {
                record = (ValuationRecord) dataStore.get(i);
                stock = PortfolioInterface.getStockObject(record.getSKey().getExchange(), record.getSKey().getSymbol());
                if (PortfolioInterface.getLastTrade(stock) > 0)
                    mValue = record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getLastTrade(stock);
                else
                    mValue = record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getPreviousClosed(stock);
//            mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((mValue - record.getTotCost()) / record.getTotCost()));
                if (record.getCumCost() != 0)
                    cumValue += ((mValue - record.getCumCost()) / record.getCumCost()) * 100;
                record = null;
                stock = null;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return cumValue;
    }

/*
        public float getCumPercentGainLossOverall() {
            float mValue = 0f;
            float cumValue = 0f;
            Stock stock  = null;
            ValuationRecord record = null;
            for (int i = 0; i < dataStore.size(); i++) {
                record = (ValuationRecord)dataStore.get(i);
                stock  = PortfolioInterface.getStockObject(record.getSKey());
                if (PortfolioInterface.getLastTrade(stock)[0] > 0)
                    mValue += record.getHolding() * PortfolioInterface.getLastTrade(stock)[0];
                else
                    mValue += record.getHolding() * PortfolioInterface.getPreviousClosed(stock)[0];
//            mValue = convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), ((mValue - record.getTotCost()) / record.getTotCost()));
                cumValue += ((mValue - record.getTotCost()) / record.getTotCost()) * 100;
                record = null;
                stock  = null;
            }
            return cumValue;
        }
     */

    public double getCumulativeGainLoss() {
        double totCost = 0D;
        Stock stock = null;
        int gainLossModifier = 1;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            try {
                record = (ValuationRecord) dataStore.get(i);
                stock = PortfolioInterface.getStockObject(record.getSKey().getExchange(), record.getSKey().getSymbol());
                if (record.getOpenSellCount() > 0) { // this is a Sell Contract
                    gainLossModifier = -1; // for Sell contracts,  negative gain is a profit
                } else {
                    gainLossModifier = 1;
                }
                if (record.getCurrency() == null) {
                    totCost += convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), ((record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getChange(stock)[0]) * record.getPriceCorrectionFactor() * gainLossModifier, record.getConversionRate(), record.getPortfolioID());
                } else {
                    totCost += convertToSelectedCurrency(stock.getExchange(), record.getCurrency(), ((record.getBalance() + record.getSellPending()) * record.getContractSize() * PortfolioInterface.getChange(stock)[0]) * record.getPriceCorrectionFactor() * gainLossModifier, record.getConversionRate(), record.getPortfolioID());
                }
                record = null;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return totCost;
    }

    public double getCumulativePercentGainLoss() {
        double totCost = 0D;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            try {
                record = (ValuationRecord) dataStore.get(i);
                stock = PortfolioInterface.getStockObject(record.getSKey().getExchange(), record.getSKey().getSymbol());
                if (record.getCumCost() != 0)
                    totCost += (record.getOwned(stock.getInstrumentType()) * record.getContractSize() * PortfolioInterface.getChange(stock)[0] / record.getCumCost()) * 100;
//            totCost += convertToSelectedCurrency(PortfolioInterface.getCurrency(stock), (record.getHolding() * PortfolioInterface.getChange(stock)[0] / record.getTotCost()));
                record = null;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return totCost;
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        /*try {
            return getValueAt(0, iCol).getClass();
                 } catch (Exception e) {
            return Object.class;
                 }*/
        switch (iCol) {
            case 0:
            case 1:
            case 2:
            case 3:
            case 20:
            case 26:
            case 27:
                return String.class;
            case 5:
            case 6:
            case 7:
            case 18:
            case 19:
            case 23:
            case 24:
            case 25:
            case 28:
//            case 29:
                return Long.class;
            case 4:
            case 8:
            case 9:
            case 10:
            case 11:
            case 12:
            case 13:
            case 14:
            case 15:
            case 16:
            case 17:
            case 29:
                return Long.class;
            case 30:
                return Long.class;
            case 31:
                return Long.class;
            case 32:
                return Long.class;
            case 21:
            case 22:
                return Double.class;


            default:
                return Object.class;
        }

//        if ( (iCol == 8) || (iCol >= 10)) {
//        if (iCol == 7) {
//            return Number[].class;
//        } else {
//            return getValueAt(0, iCol).getClass();
//        }
//        return Object.class;
    }

    public boolean isCellEditable(int row, int col) {
        return false;
    }

    public void setValueAt(Object value, int rowIndex, int column) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    /* --- Table Modal's metods end here --- */

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }

    private double convertToSelectedCurrency(String exchangeCode, String sCurrency, boolean applyPriceModificationFactor, double value, double multiplyfactor, String portfolio) {
        if (applyPriceModificationFactor) {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
            if (exchange != null)
                return value * CurrencyStore.getBuyRate(sCurrency, tradePortfolios.getBaseCurrency(), TradingShared.getTrader().getPath(portfolio)) * multiplyfactor / exchange.getPriceModificationFactor();
            else
                return value * CurrencyStore.getBuyRate(sCurrency, tradePortfolios.getBaseCurrency(), TradingShared.getTrader().getPath(portfolio)) * multiplyfactor;
        } else {
            return value * CurrencyStore.getBuyRate(sCurrency, tradePortfolios.getBaseCurrency(), TradingShared.getTrader().getPath(portfolio)) * multiplyfactor;
        }
    }

    private double convertToSelectedCurrency(String exchangeCode, String sCurrency, double value, double multiplyfactor, String portfolio) {
        return convertToSelectedCurrency(exchangeCode, sCurrency, true, value, multiplyfactor, portfolio);
    }

    private double adjustToPriceModofication(String exchangeCode, double value, double multiplyfactor) {
        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
        if (exchange != null)
            return value / exchange.getPriceModificationFactor();
        else
            return value;
    }

    private long toLongValue(Object oValue) throws Exception {
        return Long.parseLong((String) oValue);
    }

    private double toDoubleValue(Object oValue) throws Exception {
        return Double.parseDouble((String) oValue);
    }

    private int toIntValue(Object oValue) throws Exception {
        return Integer.parseInt((String) oValue);
    }

    private float toFloatValue(Object oValue) throws Exception {
        return Float.parseFloat((String) oValue);
    }

    public double getMarketValueForSymbol(String sKey, String pfid) {
        double marketValue = 0d;
        ValuationRecord valRecord = null;
        ArrayList list = TradePortfolios.getInstance().getValuationList();
        Stock stock = PortfolioInterface.getStockObject(sKey);
        if (stock == null)
            return 0f;

        for (int i = 0; i < list.size(); i++) {
            try {
                valRecord = (ValuationRecord) list.get(i);
                if (valRecord.getSKey().equals(sKey) && valRecord.getPortfolioID().equals(pfid)) {
                    if (stock.getTodaysClose() > 0) {
                        marketValue = convertToSelectedCurrency(stock.getExchange(), TradingCurrencyConverter.getSharedInstance().getDisplayCurrency(PortfolioInterface.getCurrency(stock)), valRecord.getOwned(stock.getInstrumentType()) * valRecord.getContractSize() * PortfolioInterface.getTodaysClose(stock), TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(PortfolioInterface.getCurrency(stock)), valRecord.getPortfolioID());
                    }
                    else if (stock.getLastTradeValue() > 0) {
                        marketValue = convertToSelectedCurrency(stock.getExchange(), TradingCurrencyConverter.getSharedInstance().getDisplayCurrency(PortfolioInterface.getCurrency(stock)), valRecord.getOwned(stock.getInstrumentType()) * valRecord.getContractSize() * PortfolioInterface.getLastTrade(stock), TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(PortfolioInterface.getCurrency(stock)), valRecord.getPortfolioID());
                    }
                    else {
                        marketValue = convertToSelectedCurrency(stock.getExchange(), TradingCurrencyConverter.getSharedInstance().getDisplayCurrency(PortfolioInterface.getCurrency(stock)), valRecord.getOwned(stock.getInstrumentType()) * valRecord.getContractSize() * PortfolioInterface.getPreviousClosed(stock), TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(PortfolioInterface.getCurrency(stock)), valRecord.getPortfolioID());
                    }
                    break;
                }
                valRecord = null;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        valRecord = null;
        list = null;
        stock = null;

        return marketValue;
    }

    public double getMktValuePerShareForSymbol(TradeKey tradeKey, String portfolio) {
        double marketValue = 0d;
        Stock stock = PortfolioInterface.getStockObject(tradeKey.getExchange(), tradeKey.getSymbol());
        if (stock == null)
            return 0f;

        if (stock.getTodaysClose() > 0) {
            marketValue = convertToSelectedCurrency(stock.getExchange(), TradingCurrencyConverter.getSharedInstance().getDisplayCurrency(PortfolioInterface.getCurrency(stock)), PortfolioInterface.getTodaysClose(stock), TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(PortfolioInterface.getCurrency(stock)), portfolio);
        }
        else if (stock.getLastTradeValue() > 0) {
            marketValue = convertToSelectedCurrency(stock.getExchange(), TradingCurrencyConverter.getSharedInstance().getDisplayCurrency(PortfolioInterface.getCurrency(stock)), PortfolioInterface.getLastTrade(stock), TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(PortfolioInterface.getCurrency(stock)), portfolio);
        }
        else {
            marketValue = convertToSelectedCurrency(stock.getExchange(), TradingCurrencyConverter.getSharedInstance().getDisplayCurrency(PortfolioInterface.getCurrency(stock)), PortfolioInterface.getPreviousClosed(stock), TradingCurrencyConverter.getSharedInstance().getCurrencyMultiflyFactor(PortfolioInterface.getCurrency(stock)), portfolio);
        }

        stock = null;
        return marketValue;
    }

    public double getMarketValue(boolean isCashIncludedLocal) {
        double mValue = 0D;
        Stock stock = null;
        ValuationRecord record = null;
        for (int i = 0; i < dataStore.size(); i++) {
            try {
                record = (ValuationRecord) dataStore.get(i);
                stock = PortfolioInterface.getStockObject(record.getSKey().getExchange(), record.getSKey().getSymbol());
                if (record.getCurrency() == null) {
                    if (stock.getTodaysClose() > 0) {
                        mValue += (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getTodaysClose(stock), record.getConversionRate(), record.getPortfolioID());
                    }
                    else if (stock.getLastTradeValue() > 0) {
                        mValue += (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getLastTrade(stock), record.getConversionRate(), record.getPortfolioID());
                    }
                    else {
                        mValue += (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), PortfolioInterface.getCurrency(stock), PortfolioInterface.getPreviousClosed(stock), record.getConversionRate(), record.getPortfolioID());
                    }
                } else {
                    if (stock.getTodaysClose() > 0) {
                        mValue += (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), record.getCurrency(), PortfolioInterface.getTodaysClose(stock), record.getConversionRate(), record.getPortfolioID());
                    }
                    else if (stock.getLastTradeValue() > 0) {
                        mValue += (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), record.getCurrency(), PortfolioInterface.getLastTrade(stock), record.getConversionRate(), record.getPortfolioID());
                    }
                    else {
                        mValue += (record.getBalance() + record.getSellPending()) * record.getContractSize() * convertToSelectedCurrency(stock.getExchange(), record.getCurrency(), PortfolioInterface.getPreviousClosed(stock), record.getConversionRate(), record.getPortfolioID());
                    }
                }
                record = null;
                stock = null;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        if (isCashIncludedLocal)
            mValue += pfWindow.getCashBalanceForSelectedPFs();
        return mValue;
    }

    public void getDDEString(Table table, boolean withHeadings) {
        //To change body of implemented methods use File | Settings | File Templates.
        int col;
        int cols;
        int row = table.getTable().getSelectedRow();
        int rows = table.getTable().getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        col = 0;
        cols = this.getColumnCount();
        if (withHeadings)
            buffer.append(copyHeaders(table));

        int modelIndex;
        for (int r = row; r < (rows + row); r++) {
            for (int c = 0; c < cols; c++) {
                modelIndex = table.getTable().convertColumnIndexToModel(col + c);
                if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                    try {
                        buffer.append(table.getTable().getModel().getValueAt(r, modelIndex));
                    } catch (Exception e) {
//                        e.printStackTrace();
                        buffer.append("");
                    }
                    if (c != (cols - 1))  // do not append the tab char to the last item of the row
                        buffer.append("\t");
                }
            }
            buffer.append("\n");
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
    }

    public String copyHeaders(Table table) {
        int col;
        int cols;
        //int row = this.getSelectedRow();
        //int rows= this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        col = 0;
        cols = this.getColumnCount();
        int modelIndex;
        for (int c = 0; c < cols; c++) {
            modelIndex = table.getTable().convertColumnIndexToModel(col + c);
            if (table.getTable().getColumn("" + modelIndex).getWidth() != 0) {
                buffer.append((String) table.getTable().getColumn("" + modelIndex).getHeaderValue());
                if (c != (cols - 1))  // do not append the tab char to the last item of the row
                    buffer.append("\t");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}