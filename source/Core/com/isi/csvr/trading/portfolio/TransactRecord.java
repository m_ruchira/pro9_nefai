package com.isi.csvr.trading.portfolio;

import com.dfn.mtr.mix.beans.PortfolioRecord;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.trading.shared.TradeKey;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author not attributable
 * @version 1.0
 */

public class TransactRecord {

    public boolean addCostomerAverageCost = false;
    private byte txnType;                // Buy / Sell / Opening Balance
    private int contractSize = 1;
    private long transID;
    private long txnDate = 0;
    private double customerAVGPrice;
    private double priceCorrectionFactor = 1;
    private String pfID;                   // PF ID that belongs this transaction
    private PortfolioRecord mixPortfolioRecord = null;
    private long availableForSell;

    public TransactRecord() {
        transID = System.currentTimeMillis(); //txn_ID;
        txnDate = System.currentTimeMillis();
    }

    public TransactRecord(String pfID) {
        this.pfID = pfID;
        transID = System.currentTimeMillis(); //txn_ID;
    }

    public String getExchange() {
        return mixPortfolioRecord.getExchange();
    }

    public void setExchange(String exchange) {
        mixPortfolioRecord.setExchange(exchange);
    }

    public String getSymbol() {
        return mixPortfolioRecord.getSymbol();
    }

    public void setSymbol(String symbol) {
        mixPortfolioRecord.setSymbol(symbol);
    }

    public int getInstrument() {
        return mixPortfolioRecord.getInstrumentType();
    }

    public void setInstrument(int instrument) {
        if (instrument < 0) {
            instrument = Meta.INSTRUMENT_EQUITY;
        }
        mixPortfolioRecord.setInstrumentType(instrument);
    }

    public String getBaseSymbol() {
        return mixPortfolioRecord.getBaseSymbol();
    }

    public void setBaseSymbol(String baseSymbol) {
        mixPortfolioRecord.setBaseSymbol(baseSymbol);
    }

//    public float getBrokerage() {
//        return brokerage;
//    }
//
//    public void setBrokerage(float brokerage) {
//
//        this.brokerage = brokerage;
//
//
//    }

    public String getCurrency() {
        return mixPortfolioRecord.getCurrency();
    }

    public double getrealaverageCost() {
        return mixPortfolioRecord.getAverageCost();
    }

    public double getCustomerAVGCost() {
        return customerAVGPrice;
    }

    public void setCurrency(String currency) {
        mixPortfolioRecord.setCurrency(currency);
    }

    public int getQuantity() {
        return (int)mixPortfolioRecord.getQuantity();
    }

    public void setQuantity(int holding) {
        mixPortfolioRecord.setQuantity(holding);
    }

    public long getId() {
        return transID;
    }

    public String getPfID() {
        return pfID;
    }

    public void setPfID(String pfID) {
        this.pfID = pfID;
    }

    public double getPrice() {
        if (this.addCostomerAverageCost) {
            return customerAVGPrice;
        } else {
            return getrealaverageCost();
        }
    }

    public long getPendingBuy() {
        return mixPortfolioRecord.getPendingBuy();
    }

    public void setPendingBuy(long pendingBuy) {
        mixPortfolioRecord.setPendingBuy((int) pendingBuy);
    }

    public long getPendingSell() {
        return mixPortfolioRecord.getPendingSell();
    }

    public long getBlockedQuantity() {
        return mixPortfolioRecord.getBlockedQuantity();
    }

    public long getBalance() {
        return mixPortfolioRecord.getAvailableForSell();
    }

    public void setPendingSell(long pendingSell) {
        mixPortfolioRecord.setPendingSell((int) pendingSell);
    }

    public void setRealAverageCost(double realaverageCost) {
        mixPortfolioRecord.setAverageCost(realaverageCost);
    }

    public void setCustomerAVGPPrice(double cusAVG) {
        this.customerAVGPrice = cusAVG;
    }

    public void setIschanged(boolean state) {
        this.addCostomerAverageCost = state;
    }

    public double getMarginDue() {
        return mixPortfolioRecord.getMarginDue();
    }

    public void setMarginDue(double marginDue) {
        mixPortfolioRecord.setMarginDue(marginDue);
    }

    public double getDayMarginDue() {
        return mixPortfolioRecord.getDayMarginDue();
    }

    public void setDayMarginDue(double dayMarginDue) {
        mixPortfolioRecord.setDayMarginDue(dayMarginDue);
    }

    public double getMarginBuyPct() {
        return mixPortfolioRecord.getMarginBuyPercentage();
    }

    public void setMarginBuyPct(double marginBuyPct) {
        mixPortfolioRecord.setMarginBuyPercentage(marginBuyPct / 100);
    }

    public int getOpenBuyCount() {
        return mixPortfolioRecord.getOpenBuyCount();
    }

    public void setOpenBuyCount(int openBuyCount) {
        mixPortfolioRecord.setOpenBuyCount(openBuyCount);
    }

    public int getOpenSellCount() {
        return mixPortfolioRecord.getOpenSellCount();
    }

    public void setOpenSellCount(int openSellCount) {
        mixPortfolioRecord.setOpenSellCount(openSellCount);
    }

    public TradeKey getKey() {
        return new TradeKey(getExchange(), getSymbol(), getInstrument(), getBookKeeper());
//        return SharedMethods.getTradeKey(getExchange(), getSymbol(), getInstrument(), getBookKeeper());
    }

    public long getTxnDate() {
        return txnDate;
    }

    public void setTxnDate(long txnDate) {
        this.txnDate = txnDate;
    }

    public byte getTxnType() {
        return txnType;
    }

    public void setTxnType(byte txnType) {
        this.txnType = txnType;
    }

    public long getPledged() {
        return mixPortfolioRecord.getPledged();
    }

    public void setPledged(long pledged) {
        mixPortfolioRecord.setPledged((int) pledged);
    }


    public double getDayMarginBuyPct() {
        return mixPortfolioRecord.getDayMarginBuyPercentage();
    }

    public void setDayMarginBuyPct(double dayMarginBuyPct) {
        mixPortfolioRecord.setDayMarginBuyPercentage(dayMarginBuyPct / 100);
    }

    public double getMarginCallPct() {
        return mixPortfolioRecord.getMarginCallPercentage();
    }

    public void setMarginCallPct(double marginCallPct) {
        mixPortfolioRecord.setMarginCallPercentage(marginCallPct);
    }

    public double getDayMarginCallPct() {
        return mixPortfolioRecord.getDayMarginCallPercentage();

    }

    public void setDayMarginCallPct(double dayMarginCallPct) {
        mixPortfolioRecord.setDayMarginCallPercentage(dayMarginCallPct);
    }

    public int getContractSize() {
        return contractSize;
    }

    public void setContractSize(int contractSize) {
        this.contractSize = contractSize;
    }

    public double getPriceCorrectionFactor() {
        return priceCorrectionFactor;
    }

    public void setPriceCorrectionFactor(double priceCorrectionFactor) {
        this.priceCorrectionFactor = priceCorrectionFactor;
    }





    /*public String getXML(){
        StringBuilder buffer = new StringBuilder();
        buffer.append("<PortfolioData>");
        buffer.append("<ID value=\"").append(getPfID()).append("\"/>");
        buffer.append("<Symbol value=\"").append(getSymbol()).append("\"/>");
        buffer.append("<Exchange value=\"").append(getExchange()).append("\"/>");
        buffer.append("<AverageCost value=\"").append(getrealaverageCost()).append("\"/>");
        buffer.append("<Quantity value=\"").append(getQuantity()).append("\"/>");
        buffer.append("</PortfolioData>");

        return buffer.toString();
    }*/

    public int getDayHolding() {
        return (int) mixPortfolioRecord.getNetDayHoldings();
    }

    public void setDayHolding(int dayHolding) {
        mixPortfolioRecord.setNetDayHoldings(dayHolding);
    }

    public int getDayMarginHoldings() {
        return (int) mixPortfolioRecord.getDayMarginHoldings();
    }

    public void setDayMarginHoldings(int dayMarginHoldings) {
        mixPortfolioRecord.setDayMarginHoldings(dayMarginHoldings);
    }

    public int getMarginHoldings() {
        return (int) mixPortfolioRecord.getMarginHoldings();
    }

    public void setMarginHoldings(int marginHoldings) {
        mixPortfolioRecord.setDayMarginHoldings(marginHoldings);
    }

    public String getBookKeeper() {
        return mixPortfolioRecord.getBookKeeper();
    }

    public void setBookKeeper(String bookKeeper) {
        mixPortfolioRecord.setBookKeeper(bookKeeper);
    }

    public long getTPlusDayNetHolding() {
        return mixPortfolioRecord.getTPlusDayNetHolding();
    }

    public void setTPlusDayNetHolding(long tPlusDayNetHolding) {
        mixPortfolioRecord.setTPlusDayNetHolding(tPlusDayNetHolding);
    }

    public long getTPlusPendingStock() {
        return mixPortfolioRecord.getTPlusPendingStock();
    }

    public void setTPlusPendingStock(long tPlusPendingStock) {
        mixPortfolioRecord.setTPlusPendingStock(tPlusPendingStock);
    }

    public long getTPlusDaySellPending() {
        return mixPortfolioRecord.getTPlusDaySellPending();
    }

    public void setTPlusDaySellPending(long tPlusDaySellPending) {
        mixPortfolioRecord.setTPlusDaySellPending(tPlusDaySellPending);
    }

    public PortfolioRecord getMixPortfolioRecord() {
        return mixPortfolioRecord;
    }

    public void setMixPortfolioRecord(PortfolioRecord mixPortfolioRecord) {
        this.mixPortfolioRecord = mixPortfolioRecord;
    }

    public long getPayableHolding() {
        return mixPortfolioRecord.getPayableHolding();    
    }

    public void setPayableHolding(long payableHolding){
        mixPortfolioRecord.setPayableHolding(payableHolding);
    }

    public double getAveragePrice(){
        return mixPortfolioRecord.getAveragePrice();
    }

    public long getReceivableHolding(){
        return mixPortfolioRecord.getReceivableStock();
    }

    public double getWeightedNetCost(){
        return mixPortfolioRecord.getWeightedAvgNetPrice();
    }

    public long getUnsettledSellQty(){
        return mixPortfolioRecord.getSecuritiesReceivingT2();
    }

    public long getUnsettledBuyQty(){
        return mixPortfolioRecord.getSecuritiesReceivingT1();
    }



    public String toString(byte fileType) {
        String delim = "";
       /* switch (fileType) {
            case PFUtilities.DATA:
                delim = "|";
                break;
            case PFUtilities.TXT:
                delim = "\t";
                break;
            case PFUtilities.CSV:
                delim = ",";
                break;
            case PFUtilities.QIF:
                delim = "|";
                break;
        }
        if (fileType == PFUtilities.DATA) {
            return PFUtilities.TRANSACTION_RECORD + delim + sKey + delim + txnDate + delim + getPrice() + delim + brokerage + delim + txnType + delim +
                    holding + delim + pfID + delim + transID + delim + memo + delim + "\n";
        } else if (fileType == PFUtilities.CSV) {
            String dataStr = "";
            dataStr += sKey + delim + PFUtilities.getFormattedDateWithTime(txnDate) + delim +
//            dataStr +=  sKey + delim + PFUtilities.getFormattedDateWithTime(System.currentTimeMillis()) + delim +
                    getPrice() + delim +
                    brokerage + delim +
                    PFUtilities.getTxnTypeString(txnType) + delim +
                    holding + delim;
            if (memo == null)
                dataStr += delim + "\r\n";
            else
                dataStr += memo + delim + "\r\n";
            return dataStr;
        } else if (fileType == PFUtilities.TXT) {
            String dataStr = "";
            dataStr += sKey + delim + PFUtilities.getFormattedDateWithTime(txnDate) + delim +
                    PFUtilities.getFormattedPrice(getPrice()) + delim +
                    PFUtilities.getFormattedPrice(brokerage) + delim +
                    PFUtilities.getTxnTypeString(txnType) + delim +
                    PFUtilities.getFormattedQty(holding) + delim;
            if (memo == null)
                dataStr += delim + "\r\n";
            else
                dataStr += memo + delim + "\r\n";
            return dataStr;
//               return sKey + delim + PFUtilities.getFormattedDateWithTime(txnDate) + delim +
//                      PFUtilities.getFormattedPrice(price) + delim +
//                      PFUtilities.getFormattedPrice(brokerage) + delim +
//                      PFUtilities.getTxnTypeString(txnType) + delim +
//                      PFUtilities.getFormattedQty(holding) + delim + memo + delim + "\r\n";
        } else if (fileType == PFUtilities.QIF) {
            String dataStr = "";
            dataStr += "D" + PFUtilities.getFormattedQIFDate(txnDate) + "\r\n";
            dataStr += "N" + PFUtilities.getQIFTxnTypeString(txnType) + "\r\n";
            dataStr += "I" + PFUtilities.getFormattedQIFPrice(getPrice()) + "\r\n";
            dataStr += "Q" + PFUtilities.getFormattedQIFQty(get) + "\r\n";
            dataStr += "Y" + PortfolioInterface.getSymbolFromKey(sKey) + "\r\n";
            dataStr += "O" + PFUtilities.getFormattedQIFPrice(brokerage) + "\r\n";
            if (memo != null)
                dataStr += "M" + memo + "\r\n";
            double totCost = 0f;
            if ((txnType == PFUtilities.BUY) || (txnType == PFUtilities.OP_BAL))
                totCost = getPrice() * holding + brokerage;
            else if (txnType == PFUtilities.SELL)
                totCost = getPrice() * holding - brokerage;
            dataStr += "T" + PFUtilities.getFormattedQIFPrice(totCost) + "\r\n";
            dataStr += "^" + "\r\n";
            return dataStr;
        }*/
        return "";
    }

    public long getAvailableForSell() {
        return mixPortfolioRecord.getAvailableForSell();
    }

    public void setAvailableForSell(long availableForSell) {
        this.availableForSell = availableForSell;
    }

}

