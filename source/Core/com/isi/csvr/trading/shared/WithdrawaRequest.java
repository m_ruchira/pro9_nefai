package com.isi.csvr.trading.shared;

import com.dfn.mtr.mix.beans.CashTransfer;

import java.text.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 9, 2006
 * Time: 2:55:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class WithdrawaRequest implements FundTransfer {
    private String currency;
    private int method;
    private double amount;
    private int status;
    private String reference;
    private String account;
    private String bank;
    private double availableBalance;
    private int sellPending;
    private double holdings;
    private double ledgerBalance;
    private String portfolio;
    private String date;
    private String depositeDate;
    private String reason = "";
    private SimpleDateFormat formatted = new SimpleDateFormat("dd/MM/yyyy");
    private SimpleDateFormat unformatted = new SimpleDateFormat("yyyyMMdd");
    private CashTransfer withdrawal;

    public WithdrawaRequest() {
        withdrawal = new CashTransfer();
    }

    public CashTransfer getWithdrawal() {
        return withdrawal;
    }

    public void setWithdrawal(CashTransfer withdrawal) {
        this.withdrawal = withdrawal;
    //    currency = withdrawal.getCurrency();
      //  method = Integer.parseInt(withdrawal.getPayMethod());
        //amount = withdrawal.getAmount();
    }

    public int getType() {
        return TradingConstants.TRANSFER_WITHDRAWAL;
    }

    public double getAmount() {
        return withdrawal.getAmount();
        //return amount;
    }

    public void setAmount(double amount) {
        withdrawal.setAmount(amount);
        //this.amount = amount;
    }

    public String getCurrency() {
        return withdrawal.getCurrency();
//        return currency;
    }

    public void setCurrency(String currency) {
        withdrawal.setCurrency(currency);
//        this.currency = currency;
    }

    public int getMethod() {
       return Integer.parseInt( withdrawal.getPayMethod());
//        return method;
    }

    public void setMethod(int method) {
        withdrawal.setPayMethod(method+"");
//        this.method = method;
    }

    public String getReference() {
//        return withdrawal.getReferenceID(); //the id genetated form srever
        return withdrawal.getCashTransactionID(); //the id genetated form srever
//        return reference;
    }

    public void setReference(int reference) {
        withdrawal.setReferenceID(reference+"");
//        this.reference = reference;
    }

    public int getStatus() {
        return withdrawal.getStatus();
//        return status;
    }

    public void setStatus(int status) {
        withdrawal.setStatus(status);
//        this.status = status;
    }

    public String getAccount() {
        return withdrawal.getAccountID();
//        return account;
    }

    public void setAccount(String account) {
        withdrawal.setAccountID(account);
//        this.account = account;
    }

//    public double getAvailableBalance() {
//      //  return withdrawal.get
//        return availableBalance;
//    }
//
//    public void setAvailableBalance(double availableBalance) {
//        this.availableBalance = availableBalance;
//    }
//
//    public int getSellPending() {
//        return sellPending;
//    }
//
//    public void setSellPending(int sellPending) {
//        this.sellPending = sellPending;
//    }
//
//    public double getHoldings() {
//        return holdings;
//    }
//
//    public void setHoldings(double holdings) {
//        this.holdings = holdings;
//    }
//
//    public double getLedgerBalance() {
//        return ledgerBalance;
//    }
//
//    public void setLedgerBalance(double ledgerBalance) {
//        this.ledgerBalance = ledgerBalance;
//    }

    public String getPortfolio() {
        return withdrawal.getPortfolioID();
//        return portfolio;
    }

    public void setPortfolio(String portfolio) {
        withdrawal.setPortfolioID(portfolio);
//        this.portfolio = portfolio;
    }

    public String getDate() {
        return withdrawal.getDate();
//        return date;
    }

    public String getDepositeDate(){
       try {
            return formatted.format(unformatted.parse(withdrawal.getDepositDate()));
        } catch (Exception e) {
           if(depositeDate != null)
                return depositeDate;
           else
                return "";
        }
    }

    public void setDepositeDate(String date) {
        withdrawal.setDepositDate(date); //todo check for deposit date
//        depositeDate = date;
    }

    public void setDate(String date) {
        withdrawal.setDate(date);
//        this.date = date;
    }

    public String getBank() {
        return withdrawal.getBank();
//        return bank;
    }

    public void setBank(String bank) {
        withdrawal.setBank(bank);
//        this.bank = bank;
    }

    public void setReason(String reason) {
        withdrawal.setReason(reason);
//        this.reason = reason;
    }

    public String getReason() {
        return withdrawal.getReason();

//        return reason;
    }


    public long getDepositeDateLong() {
        try {
            return (unformatted.parse(depositeDate)).getTime();  //To change body of implemented methods use File | Settings | File Templates.
        } catch(Exception e) {
            return System.currentTimeMillis();
        }
    }

    public String getCashTransactionID() {
        return withdrawal.getCashTransactionID();
    }
}
