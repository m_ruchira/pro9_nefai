package com.isi.csvr.trading.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 26, 2004
 * Time: 5:12:43 PM
 */
public interface TradingConstants {
    public static final int DISCONNECTION_UNEXPECTED = 0;
    public static final int DISCONNECTION_MANUAL = 1;
    public static final int DISCONNECTION_PRICE_CONNECTION = 2;

    public static final int TRANSFER_FUND_TRANSFER = 0;
    public static final int TRANSFER_WITHDRAWAL = 1;
    public static final int TRANSFER_STOCK_TRANSFER = 3;

    public static final int TRANSFER_STATUS_PENDING = 1;
    public static final int TRANSFER_STATUS_VALIDATED = 2;
    public static final int TRANSFER_STATUS_L1_APPROVED = 3;
    public static final int TRANSFER_STATUS_L2_APPROVED = 4;
    public static final int TRANSFER_STATUS_CANCELLED = 5;
    public static final int TRANSFER_STATUS_REJECTED = 6;
    public static final int TRANSFER_STATUS_APPROVED = 7;

    public static final int TRANSFER_REJECT_REASON_NONE = 0;
    public static final int TRANSFER_REJECT_REASON_NO_FUND = 1;

    public static final byte LEVEL1_TRADING_PASSWORD = 0;
    public static final byte LEVEL2_TRADING_PASSWORD = 1;

    public static final byte CHANGE_PASSWORD_TYPE_NORMAL = 0;
    public static final byte CHANGE_PASSWORD_TYPE_VALIDATION = 1;
    public static final byte CHANGE_PASSWORD_TYPE_EXPIRED = 2;
    public static final byte CHANGE_PASSWORD_TYPE_NONE = 3;

}
