package com.isi.csvr.trading.shared;

import com.dfn.mtr.mix.beans.CashAccount;
import com.isi.csvr.shared.SharedMethods;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 11, 2004
 * Time: 3:27:16 PM
 */
public class Account {
    private String accountNumber;
    private CashAccount cashAccount;
    private byte path;
    private double marginBuyingPower = 0;
    private double buyingPower_Zero = 0;
    private double buyingPower_50 = 0;
    private double buyingPower_75 = 0;
    private double buyingPower_100 = 0;
    private double coverageRatio = 0;
    private double concentrateRatio = 0;
    private int coverageLevel = 0;
    private int isMurbahEnabled = 0;
    private double marginFee = 0;
    private double recoveryAmount = 0;

    public Account(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public void setUnrializedSales(double unrealizedSales) {
        cashAccount.setUnrealizedSales(unrealizedSales);
    }

    public void setCashAccount(CashAccount cashAccount) {
        this.cashAccount = cashAccount;
    }

    public double getUnrializedSales() {
        return cashAccount.getOpenOrdVal();
    }

    public byte getPath() {
        return path;
    }

    public void setPath(byte path) {
        this.path = path;
    }

    public void setBlockedAmount(double blockedAmount) {
        cashAccount.setBlockedAmount(blockedAmount);
    }

    public long getDate() {
        return Long.parseLong(cashAccount.getLastAccountUpdatedTime());
    }

    public void setDate(long date) {
        cashAccount.setLastAccountUpdatedTime(date + "");
    }

    public void setBuyingPower(double buyingPower) {
        cashAccount.setBuyingPower(buyingPower);
    }

    public String getAccountID() {
        return accountNumber;
    }

    public double getBlockedAmount() {
        return cashAccount.getBlockedAmount();
    }

    public double getCashAvailableForWithdrawal() {
        return cashAccount.getCashForWithdrawal();
    }

    public void setCashAvailableForWithdrawal(double availableCash) {
        cashAccount.setCashForWithdrawal(availableCash);
    }

    public double getBuyingPower() {
        return cashAccount.getBuyingPower();
    }

    public String getAccountNumber() {
        return accountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        this.accountNumber = accountNumber;
    }

    public int getType() {
        return cashAccount.getAccountType();
    }

    public void setType(int type) {
        cashAccount.setAccountType(type);
    }

    public double getBalance() {
        return cashAccount.getBalance();
    }

    public void setBalance(double balance) {
        cashAccount.setBalance(balance);
    }

    public double getODLimit() {
        return cashAccount.getODLimit();
    }

    public void setODLimit(double ODLimit) {
        cashAccount.setODLimit(ODLimit);
    }

    public double getMarginLimit() {
        return cashAccount.getMarginLimit();
    }

    public void setMarginLimit(double marginLimit) {
        cashAccount.setMarginLimit(marginLimit);
    }

    public String getCurrency() {
        return cashAccount.getCurrency();
    }

    public void setCurrency(String currency) {
        cashAccount.setCurrency(currency);
    }

    public double getDayMarginLimit() {
        return cashAccount.getDayCashMargin();
    }

    public void setDayMarginLimit(double dayMarginLimit) {
        cashAccount.setDayCashMargin(dayMarginLimit);
    }

    public void clear() {
        cashAccount = new CashAccount();
    }

    public String toString() {
        return "Account -> Buying Power " + getBuyingPower() + " Balance " + getBalance();
    }

//    private boolean isUpdated() {
//        synchronized (this) {
//            return updated;
//        }
//    }
//
//    private void setUpdated(boolean updated) {
//        synchronized (this) {
//            this.updated = updated;
//        }
//    }

    public double getPendingDeposites() {
        return cashAccount.getPendingDeposits();
    }

    public void setPendingDeposites(double pendingDeposites) {
        cashAccount.setPendingDeposits(pendingDeposites);
    }

    public double getPendingTransfers() {
        return cashAccount.getPendingTrasfers();
    }

    public void setPendingTransfers(double pendingTransfers) {
        this.cashAccount.setPendingTrasfers(pendingTransfers);
    }


    public String getXML() {
        StringBuilder buffer = new StringBuilder();
        buffer.append("<Account>");
        buffer.append("<ID value=\"").append(getAccountID()).append("\"/>");
        buffer.append("<Currency value=\"").append(getCurrency()).append("\"/>");
        //buffer.append("<Balance value=").append(SharedMethods.formatToCurrencyFormatNoComma(getCurrency(), getBalance())).append("/>");
        buffer.append("<Blocked value=\"").append(SharedMethods.formatToCurrencyFormatNoCommaNoSpace(getCurrency(), getBlockedAmount())).append("\"/>");
        buffer.append("<ODLimit value=\"").append(SharedMethods.formatToCurrencyFormatNoCommaNoSpace(getCurrency(), getODLimit())).append("\"/>");
        buffer.append("<BuyingPower value=\"").append(SharedMethods.formatToCurrencyFormatNoCommaNoSpace(getCurrency(), getBuyingPower())).append("\"/>");
        buffer.append("<Withdrawable value=\"").append(SharedMethods.formatToCurrencyFormatNoCommaNoSpace(getCurrency(), getCashAvailableForWithdrawal())).append("\"/>");
        buffer.append("</Account>\n");

        return buffer.toString();

    }

    public double getTotMarginBlock(){
      return  cashAccount.getMarginBlock();
    }

    public double getTotMarginDue(){
        return cashAccount.getMarginDue();
    }

    public double getNonMarginableCashBlock(){
        return cashAccount.getNonMarginableCashBlock();
    }

     public double getNetSecurityValue() {
        return cashAccount.getNetSecurityValue();
    }

     public double getNetPosition() {
        return cashAccount.getNetPosition();
    }

    public double getMarginBuyingPower() {
        return marginBuyingPower;
    }

    public void setMarginBuyingPower(double marginBuyingPower) {
        this.marginBuyingPower = marginBuyingPower;
    }

    public double getBuyingPower_Zero() {
        return cashAccount.getMarginZeroPct();
    }

    public double getBuyingPower_50() {
        return cashAccount.getMargin50Pct();
    }

//    Margin buying power 75% only in SFC
    public double getBuyingPower_75() {
        return buyingPower_75;
    }

    public double getBuyingPower_100() {
        return cashAccount.getMargin100Pct();
    }

    public void setBuyingPower_Zero(double buyingPower_Zero) {
        this.buyingPower_Zero = buyingPower_Zero;
    }

    public void setBuyingPower_50(double buyingPower_50) {
        this.buyingPower_50 = buyingPower_50;
    }

    public void setBuyingPower_75(double buyingPower_75) {
        this.buyingPower_75 = buyingPower_75;
    }

    public void setBuyingPower_100(double buyingPower_100) {
        this.buyingPower_100 = buyingPower_100;
    }

    public double getPortfolioValuation(){
        return cashAccount.getPortfolioValuation();
    }

    public double getPortfolioValuationWithPledge(){
        return cashAccount.getPortfolioValWithPledge();
    }

    public double getPayableAmount(){
        return cashAccount.getPayableAmount();
    }

    public double getCoverage(){
        return cashAccount.getCoverage();
    }

    public double getMaxMargin(){
        return cashAccount.getMarginMaxAmount();
    }

    public double getReceivableCash() {
        return cashAccount.getUnrealizedSales();
    }

    public double getPayableCash() {
        return cashAccount.getPayableAmount();
    }

    public double getCoverageRatio() {
        return coverageRatio;
    }

    public void setCoverageRatio(double coverageRatio) {
        this.coverageRatio = coverageRatio;
    }

    public double getConcentrateRatio() {
        return concentrateRatio;
    }

    public int getCoverageLevel() {
        return coverageLevel;
    }

    public void setCoverageLevel(int coverageLevel) {
        this.coverageLevel = coverageLevel;
    }

    public void setConcentrateRatio(double concentrateRatio) {
        this.concentrateRatio = concentrateRatio;
    }

    public int getIsMurbahEnabled() {
        return cashAccount.getMurabahaEnabled();
    }

    public double getMarginFee() {
        return cashAccount.getMarginFee();
    }

    public double getRecoveryAmount() {
        return cashAccount.getRecoveryAmount();
    }
}
