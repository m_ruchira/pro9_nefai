package com.isi.csvr.trading.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: May 9, 2006
 * Time: 2:59:54 PM
 * To change this template use File | Settings | File Templates.
 */
public interface FundTransfer {

    int getType();

    double getAmount();

    void setAmount(double amount);

    String getCurrency();

    void setCurrency(String currency);

    int getMethod();

    void setMethod(int method);

    String getReference();

    void setReference(int reference);

    int getStatus();

    void setStatus(int status);

    String getPortfolio();

    void setPortfolio(String portfolio);

    void setReason(String reason);

    String getReason();

    void setDepositeDate(String date);

//    String getDepositeDate();

    long getDepositeDateLong();

     public String getCashTransactionID();
}
