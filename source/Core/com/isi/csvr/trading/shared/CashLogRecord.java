package com.isi.csvr.trading.shared;

import com.isi.csvr.shared.*;
import com.dfn.mtr.mix.beans.CashLogSearchRecord;

import java.text.*;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Jan 10, 2008
 * Time: 11:54:16 AM
 * To change this template use File | Settings | File Templates.
 */
public class CashLogRecord {
    private String id;
    private String symbol;
    private int instrumentType;
    private String exchange;
    private long settlementDate;
    private long date;
    private String type;
    private double amount;
    private String orderNo;
    private long orderQty;
    private double commision;
    private String transCurrency;
    private String settleCurrency;
    private double amountInTransCur;
    private double avgPrice;
    private String referenceNo;
    private double settleRate;
    private double amountInSetlCur;

    private CashLogSearchRecord record;

    public static SimpleDateFormat format = new SimpleDateFormat(" dd/MM/yyyy HH:mm ");

    public CashLogRecord(String id) {
        this.id = id;
        record = new CashLogSearchRecord();
    }

    public CashLogRecord() {
         record = new CashLogSearchRecord();
    }

    public CashLogSearchRecord getRecord() {
        return record;
    }

    public void setRecord(CashLogSearchRecord record) {
        this.record = record;
    }

    public void setData(String data){
        try {
            String[] datas = data.split(TradeMeta.FD);
            id = datas[0];
            exchange = datas[1].equalsIgnoreCase("null")?"":datas[1];
            date = Long.parseLong(datas[2]);
            settlementDate= Long.parseLong(datas[3]);
            /*try {
                date = format.format(new Date(Long.parseLong(datas[2])));
            } catch(Exception e) {
                date = datas[2];
            }
            try {
                settlementDate = format.format(new Date(Long.parseLong(datas[3])));
            } catch(Exception e) {
                settlementDate = datas[3];
            }*/
            type = datas[4];
            amount = Double.parseDouble(datas[5]);
            orderNo = datas[6];
            symbol = datas[7].equalsIgnoreCase("null")?"":datas[7];
            orderQty = Long.parseLong(datas[8]);
            transCurrency = datas[9];
            amountInTransCur = Double.parseDouble(datas[10]);
            commision = Double.parseDouble(datas[11]);
            avgPrice = Double.parseDouble(datas[12]);
            referenceNo = datas[13].equalsIgnoreCase("null")? Language.getString("NA") :datas[13];
            settleRate = Double.parseDouble(datas[14]);
            amountInSetlCur = Double.parseDouble(datas[15]);
            settleCurrency = datas[16].equalsIgnoreCase("null")? Language.getString("NA") :datas[16];
            instrumentType = Integer.parseInt(datas[17]);
        } catch(Exception e) {
            e.printStackTrace();
        }
    }

    public String getId() {
        return record.getCashTransactionID();
       // return id;
    }

    public void setId(String id) {
        record.setCashTransactionID(id);
//        this.id = id;
    }

    public String getSymbol() {
        return record.getSymbol();
//        return symbol;
    }

    public void setSymbol(String symbol) {
        record.setSymbol(symbol);
//        this.symbol = symbol;
    }

    public String getExchange() {
        return record.getExchange();
//        return exchange;
    }

    public void setExchange(String exchange) {
        record.setExchange(exchange);
//        this.exchange = exchange;
    }

    public long getSettlementDate() {

        return (record.getSettlementDate());
//        return settlementDate;
    }

    public long getDate() {
        
        return (record.getDate());
//        return date;
    }

    public int getInstrumentType() {
//        return record.ge
        return instrumentType;
    }

    public String getType() {
        return record.getCashLogType();
//        return type;
    }

    public void setType(String type) {
        record.setCashLogType(type);
//        this.type = type;
    }

    public double getAmount() {
       return record.getAmount();
//        return amount;
    }

    public void setAmount(double amount) {
        record.setAmount(amount );
//        this.amount = amount;
    }

    public String getOrderNo() {
        return record.getOrderNumber()+"";
//        return orderNo;
    }

    public void setOrderNo(String orderNo) {
        record.setOrderNumber(Long.parseLong(orderNo));
//        this.orderNo = orderNo;
    }

    public long getOrderQty() {
        return record.getOrderQuantity();
//        return orderQty;
    }

    public void setOrderQty(long orderQty) {
        record.setOrderQuantity(orderQty);
//        this.orderQty = orderQty;
    }

    public double getCommision() {
        return record.getCommission();
//        return commision;
    }

    public void setCommision(double commision) {
        record.setCommission(commision);
//        this.commision = commision;
    }

    public String getTransCurrency() {
        return record.getTransactionCurrency();
//        return transCurrency;
    }

    public void setTransCurrency(String transCurrency) {
        record.setTransactionCurrency(transCurrency);
//        this.transCurrency = transCurrency;
    }

    public double getAmountInTransCur() {
        return record.getAmountInTxnCurrency();
//        return amountInTransCur;
    }

    public void setAmountInTransCur(double amountInTransCur) {
        record.setAmountInTxnCurrency(amountInTransCur);
//        this.amountInTransCur = amountInTransCur;
    }

    public double getAvgPrice() {
        return record.getAveragePrice();
//        return avgPrice;
    }

    public void setAvgPrice(double avgPrice) {
        record.setAveragePrice(avgPrice);
//        this.avgPrice = avgPrice;
    }

    public String getReferenceNo() {
        return record.getReferenceDocumentNumber()+"";
//        return referenceNo;
    }

    public void setReferenceNo(String referenceNo) {
        record.setReferenceDocumentNumber((referenceNo));
//        this.referenceNo = referenceNo;
    }

    public double getSettleRate() {

        return (record.getIssueSettlingRate());

//        return settleRate;
    }

    public void setSettleRate(double settleRate) {
        record.setIssueSettlingRate(settleRate);
//        this.settleRate = settleRate;
    }

    public double getAmountInSetlCur() {
        return record.getAmountInSettleCurrency();
//        return amountInSetlCur;
    }

    public void setAmountInSetlCur(double amountInSetlCur) {
        record.setAmountInSettleCurrency(amountInSetlCur);
//        this.amountInSetlCur = amountInSetlCur;
    }

    public String getSettleCurrency() {
        return record.getSettleCurrency();
//        return settleCurrency;
    }
}
