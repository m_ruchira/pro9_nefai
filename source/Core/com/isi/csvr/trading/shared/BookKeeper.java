package com.isi.csvr.trading.shared;

/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: Jun 27, 2007
 * Time: 9:55:45 PM
 */
/**
 * Created by IntelliJ IDEA.
 * User: chandika
 * Date: Nov 18, 2008
 * Time: 3:34:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class BookKeeper {
    private String id="";
    private String code="";
    private boolean isDefault = false;
    private String name="";

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }

    public boolean isDefault() {
        return isDefault;
    }

    public void setDefault(boolean aDefault) {
        isDefault = aDefault;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}
