package com.isi.csvr.trading.shared;

import com.dfn.mtr.mix.beans.*;
import com.dfn.mtr.mix.common.MIXConstants;
import com.isi.csvr.Client;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.trading.TradeMethods;

import java.util.*;


/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 4, 2004
 * Time: 1:12:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class Trader {
    private String userID;
    private ArrayList<Account> accounts;
    private String mubasherID;
    private Vector accountListeners;
    private ArrayList<TradingPortfolioRecord> portfolios;
    private Hashtable<String, PathInfo> pathObjects;
    private Hashtable<String, String> pathMap;
    private Hashtable<String, String> exchanges;
    private ArrayList<String[]> banks;
    private Hashtable<String, String> accountMap;
    private ArrayList<BookKeeper> bookKeepers;
    private PathInfo pathInfo;
    private String traderName;
    private boolean isMarginEnabled = false;


    public Trader(String userID) {
        this.userID = userID;
        accounts = new ArrayList<Account>();
        pathMap = new Hashtable<String, String>();
        pathObjects = new Hashtable<String, PathInfo>();
        accountListeners = new Vector();
        portfolios = new ArrayList<TradingPortfolioRecord>();
        exchanges = new Hashtable<String, String>();
        banks = new ArrayList<String[]>();
    }

    public Trader() {
        accounts = new ArrayList<Account>();
        pathMap = new Hashtable<String, String>();
        pathObjects = new Hashtable<String, PathInfo>();
        accountListeners = new Vector();
        portfolios = new ArrayList<TradingPortfolioRecord>();
        exchanges = new Hashtable<String, String>();
        banks = new ArrayList<String[]>();
    }

    public void setUserID(String userID) {
        pathInfo = new PathInfo(userID);
        pathObjects.put(userID, pathInfo);
    }

    public void setTraderName(String traderName) {
        if (pathInfo != null) {
            pathInfo.setTraderName(traderName);
        }
    }

    public void setReportDepth(int depth) {
        if (pathInfo != null) {
            pathInfo.setReportDepth(depth);
        }
    }

    public int getReportDepth(String portfolio) {
        try {
            return pathObjects.get(pathMap.get(portfolio)).getReportDepth();
        } catch (Exception e) {
            return 6;
        }
    }

    public String getReportURL(String portfolio) {
        try {
            return pathObjects.get(pathMap.get(portfolio)).getReportURL();
        } catch (Exception e) {
            return null;
        }
    }

    public String getCurrentDay(String portfolio) {
        try {
            return pathObjects.get(pathMap.get(portfolio)).getCurrentDay();
        } catch (Exception e) {
            return "";
        }
    }

    public String getSessionID(String portfolio) {
        try {
            return pathObjects.get(pathMap.get(portfolio)).getSessionId();
        } catch (Exception e) {
            return null;
        }
    }

    public String getUserId(String portfolio) {
        try {
            return pathObjects.get(pathMap.get(portfolio)).getUserID();
        } catch (Exception e) {
            return null;
        }
    }

    public String getBrokerID(String portfolio) {
        try {
            return pathObjects.get(pathMap.get(portfolio)).getBrokerID();
        } catch (Exception e) {
            return null;
        }
    }

    public String getBrokerID(byte path) {
        try {
            Enumeration paths = pathObjects.elements();
            PathInfo objects;
            while (paths.hasMoreElements()) {
                objects = (PathInfo) paths.nextElement();
                if (objects.getPath() == path) {
                    return objects.getBrokerID();
                }
            }
        } catch (Exception e) {
            return null;
        }
        return mubasherID;
    }

    public void setLevel2AuthType(int type) {
        pathInfo.setLevel2AuthType(type);
    }

    public int getLevel2AuthType(String portfolio) {
        try {
            return pathObjects.get(pathMap.get(portfolio)).getLevel2AuthType();
        } catch (Exception e) {
            return -1;
        }
    }

    public byte getPath(String portfolio) {
        try {
            return pathObjects.get(pathMap.get(portfolio)).getPath();
        } catch (Exception e) {
            return -1;
        }
    }

    public byte getPathForSysID(String sysID) {
        try {
            Enumeration paths = pathObjects.elements();
            PathInfo objects;
            while (paths.hasMoreElements()) {
                objects = (PathInfo) paths.nextElement();
                if (objects.getBrokerID().equals(sysID)) {
                    return objects.getPath();
                }
            }
        } catch (Exception e) {
        }
        return Constants.PATH_PRIMARY;
    }

    public void setReportURL(String reportURL) {
        pathInfo.setReportURL(reportURL);
    }

    public void setCurrentDay(String currentDay) {
        pathInfo.setCurrentDay(currentDay);
    }

    public void setSessionID(String sessionID) {
        pathInfo.setSessionId(sessionID);
    }

    public void setBrokerID(String brokerID) {
        pathInfo.setBrokerID(brokerID);
    }

    public void setPath(byte path) {
        if (pathInfo != null) {
            pathInfo.setPath(path);
        }
    }

    public String getUserID() {
        return userID;
    }

    public String getUserID(String portfolio) {
        try {
            return pathObjects.get(pathMap.get(portfolio)).getUserID();
        } catch (Exception e) {
            return null;
        }
//        return userID;
    }

    public String getMubasherID(String portfolio) {
        try {
            return pathObjects.get(pathMap.get(portfolio)).getMubasherID();
        } catch (Exception e) {
            return null;
        }
//        return userID;
    }

    public String getMubasherID(byte path) {
        try {
            Enumeration paths = pathObjects.elements();
            PathInfo objects;
            while (paths.hasMoreElements()) {
                objects = (PathInfo) paths.nextElement();
                if (objects.getPath() == path) {
                    return objects.getMubasherID();
                }
            }
        } catch (Exception e) {
            return null;
        }
        return mubasherID;
    }

    public String getTraderName(String portfolio) {
        try {
            return pathObjects.get(pathMap.get(portfolio)).getTraderName();
        } catch (Exception e) {
            return null;
        }
//        return userID;
    }

    public String getTraderName(byte path) {
        try {
            Enumeration paths = pathObjects.elements();
            PathInfo objects;
            while (paths.hasMoreElements()) {
                objects = (PathInfo) paths.nextElement();
                if (objects.getPath() == path) {
                    return objects.getTraderName();
                }
            }
        } catch (Exception e) {
            return null;
        }
        return mubasherID;
    }

    public String getUserID(byte path) {
        try {
            Enumeration paths = pathObjects.elements();
            PathInfo objects;
            while (paths.hasMoreElements()) {
                objects = (PathInfo) paths.nextElement();
                if (objects.getPath() == path) {
                    return objects.getUserID();
                }
            }
        } catch (Exception e) {
            return null;
        }
        return userID;
    }

    public void addExchanges(String exchangedata) {
        String[] exchangesArr = exchangedata.split(",");
        for (String aExchangesArr : exchangesArr) {
            exchanges.put(aExchangesArr, aExchangesArr);
        }
        exchangesArr = null;
    }

    public boolean isTradableExchange(String exchange) {
        try {
            return exchanges.containsKey(exchange);
        } catch (Exception e) {
            return false;
        }
    }

    public boolean isTradableExchangesAvailable() {
        return (exchanges.size() > 0);
    }

    public void createBookKeepers(String data) {
        String[] records = data.split(":");
        for (String record : records) {
//            String[] fields = record.split("~");
//            for (String field : fields) {
//            }
        }
    }

    public void inInitialize() {
        for (Account account : accounts) {
            account.clear();
            fireAccountDataChanged(account.getAccountID());
            account = null;
        }
        accounts.clear();
        accounts.trimToSize();

        accountListeners.removeAllElements();
        accountListeners.trimToSize();

        portfolios.clear();
        portfolios.trimToSize();

    }

    // account related

    public synchronized Account findAccount(String accountID) {
        try {
            for (Account account : accounts) {
                if (account.getAccountID().equals(accountID)) {
                    return account;
                }
                account = null;
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    /*public double getEffectiveBuyingPowerForPortfolio(String portfolioID){
        try {
            TradingPortfolioRecord portfolio = getPortfolio(portfolioID);
            Account account = findAccountByPortfolio(portfolioID);
            return account.getBuyingPower() - portfolio.getMarginBlocked() - portfolio.getMarginDue();
        } catch (Exception e) {
            return 0;
        }
    }*/

    public Account findAccountByPortfolio(String portolioId) {
        try {
            TradingPortfolioRecord portfolio = getPortfolio(portolioId);
            if (portfolio != null) {
                for (Account account : accounts) {
                    if (account.getAccountID().equals(portfolio.getAccountNumber())) {
                        return account;
                    }
                    account = null;
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public int numberOfAccounts() {
        return accounts.size();
    }

    public String getCurrencyCode(String portolioId) {
        try {
            Account account = findAccountByPortfolio(portolioId);
            return account.getCurrency();
        } catch (Exception e) {
            return null;
        }
    }

    public void notifyAccountListeners(String accountID) {
        fireAccountDataChanged(accountID);
    }

    public void createAccounts() {
        accountMap = new Hashtable<String, String>();
        String accountNumber = null;
        String portfolio = null;
        for (TradingPortfolioRecord portfolio1 : portfolios) {
            portfolio = portfolio1.getPortfolioID();
            accountNumber = portfolio1.getAccountNumber();
            try {
                accountMap.put(accountNumber, portfolio);
            } catch (Exception e) {
            }
        }
        Enumeration accounts = accountMap.keys();
        while (accounts.hasMoreElements()) {
            accountNumber = (String) accounts.nextElement();
            try {
                createAccount(accountNumber, getPath(accountMap.get(accountNumber)));
            } catch (Exception e) {
            }
        }
        accounts = null;
    }

    public Hashtable<String, String> getAccountMap() {
        return accountMap;
    }

    /*    public Account createAccount(String accountID, byte path) {
        System.out.println("Creating Account ======== " + accountID);
        Account account = new Account(accountID);
        account.setPath(path);
        accounts.add(account);
        return account;
    }*/

    public Account createAccount(String accountID, byte path) {
        SharedMethods.printLine("Creating Account ======== " + accountID, true);
        Account account = getAccount(accountID);
        if (account == null) {
            account = new Account(accountID);
            account.setPath(path);
            accounts.add(account);
        }
        return account;
    }

    public Account getAccount(int index) {
        return accounts.get(index);
    }

    public Account getAccount(String id) {
        try {
            for (Account account : accounts) {
                if (account.getAccountID().equals(id)) {
                    return account;
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public void setBanks(String bankIDs) {
        banks = TradeMethods.getBanks(bankIDs);
    }

    public void setBanks(String userID, String[] bankIDs) {
        try {
            pathObjects.get(userID).setBanks(TradeMethods.getBanks(bankIDs));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
//        banks = TradeMethods.getBanks(bankIDs);
    }

    public void setBanks(String userID, ArrayList<BankRecord> banklist) {
        try {
            pathObjects.get(userID).setBanks(TradeMethods.getBanks(banklist));
        }
        catch (Exception e) {
            e.printStackTrace();
        }
//        banks = TradeMethods.getBanks(bankIDs);
    }

    public ArrayList<String[]> getBanks() {
        return banks;
    }

    public ArrayList<String[]> getBanks(byte path) {
        try {
            Enumeration paths = pathObjects.elements();
            PathInfo objects;
            while (paths.hasMoreElements()) {
                objects = (PathInfo) paths.nextElement();
                if (objects.getPath() == path) {
                    return objects.getBanks();
                }
            }
        } catch (Exception e) {
            return null;
        }
        return banks;
    }

    public ArrayList<String[]> getBanks(String portfolio) {
//        if (!banks.isEmpty()) {
//            return banks;
//        } else {
//            try {
//                return pathObjects.get(pathMap.get(portfolio)).getBanks();
//            } catch (Exception e) {
//                return banks;
//            }
//        }
        if(pathObjects.get(pathMap.get(portfolio)).getBanks() != null){
            return pathObjects.get(pathMap.get(portfolio)).getBanks();
        } else{
           return banks; 
        }
//        return userID;
    }

    public void setBlockedAmount(String accountID, double blockedAmount) {
        Account account = findAccount(accountID);
        if (account != null) {
            account.setBlockedAmount(blockedAmount);
            account = null;
        }
    }

    public void setBuyingPower(String accountID, double buyingPower) {
        Account account = findAccount(accountID);
        if (account != null) {
            account.setBuyingPower(buyingPower);
            account = null;
        }
    }

    public void addAccountListener(TraderProfileDataListener listener) {
        accountListeners.add(listener);
    }

    public void removeAccountListener(TraderProfileDataListener listener) {
        accountListeners.remove(listener);
    }

    public void notifyPortfolioListeners(String portfolioID) {
        firePortfolioDataChanged(portfolioID);
    }

    public void firePortfolioDataChanged(String portfolioID) {
        try {
            for (int i = 0; i < accountListeners.size(); i++) {
                try {
                    ((TraderProfileDataListener) accountListeners.get(i)).portfolioDataChanged(portfolioID);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }

            /*for (int i = 0; i < accounts.size(); i++) {
                 try {
                     ((TraderProfileDataListener) accounts.elementAt(i)).portfolioDataChanged(portfolioID);
                 } catch (Exception e) {
                     e.printStackTrace();
                 }
             }*/

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void fireAccountDataChanged(String accountID) {
        try {
            Account account = findAccount(accountID);
            if (account != null) {
                for (int i = 0; i < accountListeners.size(); i++) {
                    try {
                        ((TraderProfileDataListener) accountListeners.get(i)).accountDataChanged(account.getAccountID());
                    } catch (Exception e) {
//                        e.printStackTrace();
                    }
                }
//                account.portfolioDataChanged(null); // fire the portfolio data changed event to initialize values
                account = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    // Portfolio related

    public void createPortfolio(String id, String brokerID, int status, String account, String name, boolean defaultPortfolio, String allowedExchanges, ArrayList bookKeepers, boolean isMarginEnabled) {
        TradingPortfolioRecord tradingPortfolioRecord = new TradingPortfolioRecord(id, brokerID, status, account, defaultPortfolio, name, allowedExchanges, bookKeepers, isMarginEnabled);
        addPortfolio(tradingPortfolioRecord);
//            portfolios.add(tradingPortfolioRecord);
        if (pathInfo != null) {
            pathMap.put(id, pathInfo.getUserID());
        }
        tradingPortfolioRecord = null;
    }

    public void createPortfolios(ArrayList<PortfolioHeader> pfList) {
        boolean isAnyPortfolioMarginable = false;
        if (pfList != null && !pfList.isEmpty()) {
            for (int i = 0; i < pfList.size(); i++) {
                PortfolioHeader record = pfList.get(i);
                ArrayList<BookKeeper> bookKeepers = new ArrayList<BookKeeper>();
                String portfolioID = null;
                String brokerID = null;
                int status = 0;
                String accountNumber = null;
                String name = null;
                boolean isDefault = true;
                int requiredCOunt = 0;
                String allowedExchanges = null;
                boolean isMarginEnabledNew = false;

                portfolioID = record.getPortflioID();
                brokerID = record.getBrokerID();
                status = record.getStatus();
                accountNumber = record.getAccountNo();
                name = Language.getLanguageSpecificString(record.getName(), "|");
                isDefault = (record.getDefault()) == 1;
                allowedExchanges = record.getExchanges();
                isMarginEnabledNew = (record.getMarginEnabled())== 1;
                if (portfolioID != null) {
                    createPortfolio(portfolioID, brokerID, status, accountNumber, name, isDefault, allowedExchanges, bookKeepers, isMarginEnabledNew);
                }
                /*if (!isAnyPortfolioMarginable && isMarginEnabledNew) {
                        isAnyPortfolioMarginable = true;
                }*/

            }
            /*if (isAnyPortfolioMarginable) {
                TradeMethods.requestMarginSymbols(Constants.PATH_PRIMARY);
                TradingShared.getTrader().setMarginEnabled(true);
                TradeMethods.getSharedInstance().getAccountWindow().checkMarginAvailability();
                Client.getInstance().getMenus().updateMarginableSymbolsMenu(true);
            }*/
        }
    }

    public void updatePortfolios(MIXObject frame) {
        if (frame == null || frame.getTransferObjects() == null || frame.getTransferObjects().length < 1) {
            return;
        }
        boolean isAnyPortfolioMarginable = false;
        MarginConfigResponse resp = (MarginConfigResponse) frame.getTransferObjects()[0];
        if (resp.getMarginConfigList() != null && resp.getMarginConfigList().size() > 0) {
            List<PortfolioMarginConfig> list = resp.getMarginConfigList();
            for (int i = 0; i < list.size(); i++) {
                PortfolioMarginConfig config = list.get(i);
                TradingPortfolioRecord addedRecord = getPortfolio(config.getPortfolioId());
                if (addedRecord != null) {
                    addedRecord.setDayMarginEnabled(config.getDayMarginEnable() == MIXConstants.DAY_MARGIN_ENABLED);
                    addedRecord.setMarginEnabled(config.getMarginEnable() == MIXConstants.MARGIN_ENABLED);
                    addedRecord.setMarginPct(config.getInitialMargin() / 100);
                    addedRecord.setDayMarginPct(config.getDayInitialMargin() / 100);
                    addedRecord.setDayLiquidationTime(config.getDayLiquidationTime());
                    addedRecord.setMaxMarginAmt(config.getMaxMarginLimit());
                    addedRecord.setMaxDayMarginAmt(config.getMaxDayMarginLimit());
                    if (!isAnyPortfolioMarginable && addedRecord.isMarginable()) {
                        isAnyPortfolioMarginable = true;
                    }
                }

            }
            
            if (isAnyPortfolioMarginable) {
                TradeMethods.requestMarginSymbols(Constants.PATH_PRIMARY);
                TradingShared.getTrader().setMarginEnabled(true);
                TradeMethods.getSharedInstance().getAccountWindow().checkMarginAvailability();
                Client.getInstance().getMenus().updateMarginableSymbolsMenu(true);
            }

        }
    }

    public void analyseBuyingPowerResponse(MIXObject record) {
        try {
            MIXObject mixObj = new MIXObject();
            mixObj.setMIXString(record.getMIXString());
            com.dfn.mtr.mix.beans.TransferObject[] response = record.getTransferObjects();
            if (response == null || response.length == 0) {
                System.out.println("Buying Power From The Server - Response is null or empty");
                return;
            }

            BuyingPowerStatus buyingPowerResponse = (BuyingPowerStatus) response[0];
            getAccoutOfPortfolio(buyingPowerResponse.getPortfolioNo()).setMarginBuyingPower(buyingPowerResponse.getBuyingPower());
            getAccoutOfPortfolio(buyingPowerResponse.getPortfolioNo()).setCoverageRatio(buyingPowerResponse.getCoverageRatio());
            getAccoutOfPortfolio(buyingPowerResponse.getPortfolioNo()).setConcentrateRatio(buyingPowerResponse.getConcentrationRatio());
            getAccoutOfPortfolio(buyingPowerResponse.getPortfolioNo()).setCoverageLevel(buyingPowerResponse.getCoverageLevel());
        } catch (Exception e) {
            System.out.println("Buying Power From The Server - Exception");
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String getPortfolioName(String id) {
        for (int i = 0; i < portfolios.size(); i++) {
            if ((portfolios.get(i)).getPortfolioID().equals(id)) {
                return (portfolios.get(i)).getName();
            }
        }
        return null;
    }

    public TradingPortfolioRecord getPortfolio(String id) {
        for (int i = 0; i < portfolios.size(); i++) {
            if (portfolios.get(i).getPortfolioID().equals(id)) {
                return portfolios.get(i);
            }
        }
        return null;
    }

    public Account getAccoutOfPortfolio(String portfolioID) {
        try {
            for (TradingPortfolioRecord portfolio : portfolios) {
                if (portfolio.getPortfolioID().equals(portfolioID)) {
                    // found the portfolio
                    for (Account account : accounts) {
                        if (account.getAccountID().equals(portfolio.getAccountNumber())) {
                            return account;
                        }
                    }
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public Enumeration<Account> getAccoutsOfPortfolios(String[] portfolioIDs) {
        Hashtable<String, Account> accountTable = new Hashtable<String, Account>();
        try {
            for (String portfolioID : portfolioIDs) {
                for (TradingPortfolioRecord portfolio : portfolios) {
                    if (portfolio.getPortfolioID().equals(portfolioID)) {
                        if (!accountTable.containsKey(portfolioID)) {
                            accountTable.put(portfolio.getAccountNumber(), findAccountByPortfolio(portfolioID));
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return accountTable.elements();
    }

    public String getPortfolioID(int index) {
        return portfolios.get(index).getPortfolioID();
    }

    public int getPortfolioCount() {
        return portfolios.size();
    }

    public ArrayList<TradingPortfolioRecord> getPortfolios() {
        return portfolios;
    }

    public Enumeration<String> getExchanges() {
        return exchanges.keys();
    }

    public void setMubasherID(String mubasherID) {
        this.mubasherID = mubasherID;
        if (pathInfo != null) {
            pathInfo.setMubasherID(mubasherID);
        }
    }

    public String getAccountsXML() {
        StringBuilder buffer = new StringBuilder();
        for (Account account : accounts) {
            buffer.append(account.getXML());
        }
        return buffer.toString();
    }

    public String getTraderName() {
        return traderName;
    }

//    public void setTraderName(String traderName) {
//        this.traderName = traderName;
//    }

    private void addPortfolio(TradingPortfolioRecord record) {
        if (portfolios != null && record != null) {
            int location = -1;
            for (int i = 0; i < portfolios.size(); i++) {
                TradingPortfolioRecord rec = portfolios.get(i);
                if (rec.getPortfolioID().equalsIgnoreCase(record.getPortfolioID())) {
                    location = i;
                    break;
                }
            }
            if (location >= 0) {
                portfolios.remove(location);
                portfolios.add(location, record);
            } else {
                portfolios.add(record);
            }
        }

    }

    private void addAccount(Account record) {
        if (accounts != null && record != null) {
            int location = -1;
            for (int i = 0; i < accounts.size(); i++) {
                Account rec = accounts.get(i);
                if (rec.getAccountID().equalsIgnoreCase(record.getAccountID())) {
                    location = i;
                    break;
                }
            }
            if (location >= 0) {
                accounts.remove(location);
                accounts.add(location, record);
            } else {
                accounts.add(record);
            }
        }

    }

    private class PortfolioComparator implements Comparator {
        public int compare(Object o1, Object o2) {
            TradingPortfolioRecord rec1 = (TradingPortfolioRecord) o1;
            TradingPortfolioRecord rec2 = (TradingPortfolioRecord) o2;
            if (rec1.getPortfolioID().trim().equalsIgnoreCase(rec2.getPortfolioID().trim())) {
                return 0;
            } else {
                return -1;  //To change body of implemented methods use File | Settings | File Templates.
            }
        }
    }

    public boolean isMarginEnabled() {
        return isMarginEnabled;
    }

    public void setMarginEnabled(boolean marginEnabled) {
        isMarginEnabled = marginEnabled;
    }

    public ArrayList<Account> getAccounts() {
        return accounts;
    }

}