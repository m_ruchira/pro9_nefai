package com.isi.csvr.trading.shared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 18, 2004
 * Time: 2:57:58 PM
 */
public interface TradeMeta {
    public static final String EOL = "\n";
    public static final String FS = "\u001C";
    public static final String DS = "\u0002";
    public static final String RS = "\u001E";
    public static final String FD = "\u0001";
    public static final String ID = "\u0018";
    public static final String DD = "" + (char) 30;         // 126 - Delimeter for data within data
    public static final String INTERNAL_SEPARATOR = "\u0003";         // 126 - Delimeter for data within data

    public static final String BLOCK_TYPE_AUTH          = "0";
    public static final String BLOCK_TYPE_PORTFOLIO     = "2";
    public static final String BLOCK_TYPE_RULES         = "3";
    public static final String BLOCK_TYPE_SECOND_LOGIN  = "4";

    public static final byte MT_PULSE                   = 0;
    public static final byte MT_AUTH                    = 1;
    public static final byte MT_ACK                     = 2;
    public static final byte MT_ORDER                   = 3;
    public static final byte MT_PENDING                 = 4;
    public static final byte MT_SEARCH                  = 5;
    public static final byte MT_QUEUED                  = 6;
    public static final byte MT_CANCEL                  = 7;
    public static final byte MT_AMEND                   = 8;
    public static final byte MT_PORTFOLIO_MARGIN        = 9;
    public static final byte MT_ACCOUNT                 = 10;
    public static final byte MT_PORTFOLIO_DETAILS       = 11;
    public static final byte MT_ORDER_DEPTH             = 12;
    public static final byte MT_CHANGE_PW               = 15;
    public static final byte MT_AUTH_2                  = 16;
    public static final byte MT_CHANGE_PW_LEVEL2        = 17;
    public static final byte MT_CASH_SUMMARY            = 27;
    public static final byte MT_CASH_DETAIL             = 28;
    public static final byte MT_AUTH_VALIDITY           = 29;
    public static final byte MT_WITHDRAWAL              = 30;
    public static final byte MT_FUND_TRANSFER_PENDING   = 31;
    public static final byte MT_FUND_TRANSFER_CANCEL    = 32;
    public static final byte MT_DEPOSITE                = 33;
    public static final byte MT_STOCK_TRANSFER          = 34;
    public static final byte MT_STOCK_TRANSFER_PENDING  = 35;
    public static final byte MT_STOCK_TRANSFER_CANCEL   = 36;
    public static final byte MT_OMS_SYSTEM_MESSAGE      = 37;
    public static final byte MT_CURRENCY_DATA           = 38;
    public static final byte MT_AUTH_SINGLE_SIGNON      = 40;                    
    public static final byte MT_CONDITIONAL_ORDER       = 42;
    public static final byte MT_CONDITIONAL_PENDING     = 43;
    public static final byte MT_EDIT_PORTFOLIO          = 44;
    public static final byte MT_CASH_TRANSFER           = 45;
    public static final byte MT_FUTURE_BASE_ATTRIBUTES  = 46;
    public static final byte MT_REQUEST_CUSTOMER_DATA   = 47;
    public static final byte MT_SLICED_ORDER		    = 48;
    public static final byte MT_SLICED_ORDER_LIST       = 49;
    public static final byte MT_MBS_AUTH			    = 70;
    public static final byte MT_FORX_DISCOUNTS		    = 73;
    public static final byte MT_OPEN_POSITIONS_LIST     = 74;
    public static final byte MT_FOREX_QUOTE			    = 75;
    public static final byte MT_CASH_LOG_SEARCH         = 76;
    public static final byte MT_3rd_Party_CASH_TRANSFER = 77;
    public static final byte MARK_FOR_DELIVERY          = 78;
    public static final byte MT_IPO_SUBSCRIPTION_RQ     = 79;
    public static final byte MT_IPO_CANCEL_RQ           = 82;
    public static final byte MT_IPO_AVAILABLE           = 80;
    public static final byte MT_IPO_SUBSCRIBED          = 81;
    public static final byte MT_MARGIN_SYMBOLS          = 83;
    public static final byte MT_BANK_ACC_DETAILS        = 84;
    public static final byte MT_TPLUS_SYMBOLS           = 90;
    public static final byte MT_TPLUS_COMMISIONS        = 91;
    public static final byte MT_TPLUS_SYMBOLS_NEW       = 92;

    public static final byte MT_COMMISSION              = 73; // should be change into 84

    public static final char T39_New                            = '0';
    public static final char T39_Partially_Filled               = '1';
    public static final char T39_Filled                         = '2';
    public static final char T39_Done_For_Day                   = '3';
    public static final char T39_Canceled                       = '4';
    public static final char T39_Replaced                       = '5';
    public static final char T39_Pending_Cancel                 = '6';
    public static final char T39_Stopped                        = '7';
    public static final char T39_Rejected                       = '8';
    public static final char T39_Suspended                      = '9';
    public static final char T39_Pending_New                    = 'A';
    public static final char T39_Calculated                     = 'B';
    public static final char T39_Expired                        = 'C';
    public static final char T39_Accepted_For_Bidding           = 'D';
    public static final char T39_Pending_Replace                = 'E';
    public static final char T39_SEND_TO_EXCHANGE_CANCEL        = 'F';
    public static final char T39_SEND_TO_EXCHANGE_AMEND         = 'G';
    public static final char T39_REQUEST_FAILED                 = 'H';
    public static final char T39_AMEND_REJECTED                 = 'I';
    public static final char T39_CANCEL_REJECTED                = 'J';
    public static final char T39_OMS_RECEIVED                   = 'K';
    public static final char T39_VALIDATED 	                    = 'L';
    public static final char T39_SEND_TO_OMS_NEW                = 'M';
    public static final char T39_SEND_TO_EXCHANGE_NEW           = 'N';
    public static final char T39_OMSACCEPTED                    = 'O';
    public static final char T39_SEND_TO_OMS_CANCEL             = 'P';
    public static final char T39_SEND_TO_OMS_AMEND              = 'Q';
    public static final char T39_RECEIVED                       = 'R';
    public static final char T39_NEW_WAITING                    = 'T';
    public static final char T39_CANCEL_WAITING                 = 'U';
    public static final char T39_NEW_DELETED                    = 'V';
    public static final char T39_AMEND_WAITING                  = 'W';
    public static final char T39_DEFAULT                        = 'X';
    public static final char T39_Executed                       = 'Y';
    public static final char T39_INITIATED                      = 'Z';
    public static final char T39_APPROVED                       = 'a';
    public static final char T39_APPROVAL_REJECTED              = 'b';
    public static final char T39_WAITING_FOR_APPROVAL           = 'c';
    public static final char T39_APPROVAL_VALIDATION_REJECTED   = 'd';
    public static final char T39_INVALIDATED_BY_REPLACE         = 'e';
    public static final char T39_INVALIDATED_BY_CHANAGE         = 'f';
    public static final char T39_CANELED_BY_EXCHANGE            = 'h';
    public static final char T39_SEND_TO_DEALER_NEW             = 'i';
    public static final char T39_SEND_TO_DEALER_AMEND           = 'j';
    public static final char T39_SEND_TO_DEALER_CANCEL          = 'k';
    public static final char T39_INVALIDATED_BY_CANCEL          = 'm';
    public static final char T39_PROCESSED                      = 'n';
    public static final char T39_PROCESSING                     = 'p';
    public static final char T39_PF_EXPIRED                     = 'q';
    public static final char T39_PF_CANCELLED                   = 'r';
    public static final char T39_COMPLETED                      = 's';
    public static final char T39_PENDING_TRIGGER                = 't';
    public static final char T39_SENDING 	                    = 'y';
    public static final char T39_Completed                      = '!';
    public static final char T39_MURABAHA_PENDING               = '[';
    public static final char T39_MURABAHA_APPROVED              = ']';

    //Available Order statuses are 'S','g','l','o','u','v','w','x','z','#','(',')','&','%','?', '[',']'

    public static final int LEVEL2_AUTH_NONE        = 1;
    public static final int LEVEL2_AUTH_PW          = 2;
    public static final int LEVEL2_AUTH_PW_ALLWAYS  = 3;
    public static final int LEVEL2_AUTH_USB         = 4;
    public static final int LEVEL2_AUTH_RSA_ONCE    = 5;
    public static final int LEVEL2_AUTH_RSA_ALLWAYS = 6;

    public static final int AUTH_ERROR              = 0;
    public static final int AUTH_OK                 = 1;
    public static final int AUTH_CHANGE_PW          = 2;
    public static final int AUTH_LOCKED             = 3;
    public static final int AUTH_INACTIVE           = 4;
    public static final int AUTH_SUSPENDED          = 5;
    public static final int AUTH_ALREADY_LOGGED_IN  = 6;
    public static final int AUTH_OTP_REQUIRED       = 9;
    public static final int AUTH_OTP_INVALID        = 14;
    public static final int AUTH_OTP_EXPIRED        = 15;
    // action

    public static final int NEW     = 0;
    public static final int BUY     = 1;
    public static final int SELL    = 2;
    public static final int AMEND   = 3;
    public static final int CANCEL  = 4;
    public static final int ALL     = 90;
    public static final int NONE    = 91;
    public static final int DUPLICATE = 92;
    // FOREX types
    public static final int LONG    = 101;
    public static final int SHORT   = 102;
    public static final int SQUARE  = 103;
    // FOREX statuses
    public static final int OPEN    = 0;
    public static final int CLOSED  = 1;
    public static final int BLOCKED = 2;
    public static final int SQUARED = 3;

    // type
    public static final char ORDER_TYPE_MARKET                  = '1';
    public static final char ORDER_TYPE_LIMIT                   = '2';
//    public static final char ORDER_TYPE_STOP_MARKET             = '3';
//    public static final char ORDER_TYPE_STOP_LIMIT              = '4';
    public static final char ORDER_TYPE_STOPLOSS_MARKET         = '3';
    public static final char ORDER_TYPE_STOPLOSS_LIMIT          = '4';
    public static final char ORDER_TYPE_TRAILING_STOP_MARKET    = 'y';
    public static final char ORDER_TYPE_TRAILING_STOP_LIMIT     = 'z';
    public static final char ORDER_TYPE_MARKET_ON_CLOSE         = '5';
    public static final char ORDER_TYPE_LIMIT_ON_CLOSE          = 'B';
    public static final char ORDER_TYPE_DAY_MARKET              = 'a';
    public static final char ORDER_TYPE_DAY_LIMIT               = 'b';
    public static final char ORDER_TYPE_SQUARE_OFF              = 'c';
    public static final char ORDER_TYPE_QUOTED                  ='D';

    public static final int MODE_NORMAL     = 0;
    public static final int MODE_PROGRAMMED = 1;

    public static final int BLOTTER_CURRENT = 1;
    public static final int BLOTTER_HISTORY = 2;
    public static final int BLOTTER_TODAY = 3;

    public static final int CHANNEL = 6;

    public static final String DEFAULT_CURRENCY = "SAR";

    public static final byte SOURCE_BOARD       = 0;
    public static final byte SOURCE_PORTFOLIO   = 1;


    public static final byte PIN_CHANGE_FAILED  = 0;
    public static final byte PIN_CHANGE_SUCESS  = 1;
    public static final byte PIN_CHANGE_ERROR   = 2;

    public static final int TIF_na      = -1;
    public static final int TIF_DAY     = 0;
    public static final int TIF_GTC     = 1;
    public static final int TIF_AOP     = 2;
    public static final int TIF_IOC     = 3;
    public static final int TIF_FOK     = 4;
    public static final int TIF_GTX     = 5;
    public static final int TIF_GTD     = 6;
    public static final int TIF_WEEK    = 7;
    public static final int TIF_MONTH   = 8;

    public static int CONDITION_FIELD_EXCHANGE = 0;
    public static int CONDITION_FIELD_SYMBOL   = 1;
    public static int CONDITION_FIELD_METHOD   = 2;
    public static int CONDITION_FIELD_OPERATOR = 3;
    public static int CONDITION_FIELD_VALUE1   = 4;
    public static int CONDITION_FIELD_VALUE2   = 5;

    public static final int TRANSFER_CHEQUE = 0;
    public static final int TRANSFER_BANK   = 1;
    public static final int TRANSFER_CASH   = 2;

    public static final int SHARIA_TRSTRICTED   = 1;
    public static final int SHARIA_WARNING      = 0;

     /*
        WIPFEE	Withdrawal Processing Fee
        MDAFEE	Market Data Access Fee
        CRPFEE	Cheque Return Processing Fee
        MISFEE	Miscellaneous
        REFUND	Refund
        COUFEE	Courier Charges
        REVBUY	Buy Reverse
        REVSEL	Sell Reverse
        TRFFEE	Bank Charges
        DEPOST	Deposit
        WITHDR	Withdraw
        STLBUY	Buy
        STLSEL	Sell
        TRNSFR	Cash Transfer
        CONOPN	Contract Open
        CONCLS	Contract Close
        MKTMKT	Mark to Market
    */
    public static final String CT_ALL           = "*";
    public static final String CT_WIPFEE        = "WIPFEE";
    public static final String CT_MDAFEE        = "MDAFEE";
    public static final String CT_CRPFEE        = "CRPFEE";
    public static final String CT_MISFEE        = "MISFEE";
    public static final String CT_REFUND        = "REFUND";
    public static final String CT_COUFEE        = "COUFEE";
    public static final String CT_REVBUY        = "REVBUY";
    public static final String CT_REVSEL        = "REVSEL";
    public static final String CT_TRFFEE        = "TRFFEE";
    public static final String CT_DEPOST        = "DEPOST";
    public static final String CT_WITHDR        = "WITHDR";
    public static final String CT_STLBUY        = "STLBUY";
    public static final String CT_STLSEL        = "STLSEL";
    public static final String CT_TRNSFR        = "TRNSFR";
    public static final String CT_CONOPN        = "CONOPN";
    public static final String CT_CONCLS        = "CONCLS";
    public static final String CT_MKTMKT        = "MKTMKT";

    public static final int CONDITION_STATUS_PENDING    = 1;
    public static final int CONDITION_STATUS_REJECTED   = 2;
    public static final int CONDITION_STATUS_TRIGGERED  = 3;
    public static final int CONDITION_STATUS_EXPIREDG   = 4;
    public static final int CONDITION_STATUS_CENCELLED  = 5;
    public static final int CONDITION_STATUS_AMENDED    = 6;

    
    public static final int SLICE_STATUS_PENDING     = 1;
    public static final int SLICE_STATUS_REJECTED    = 2;
    public static final int SLICE_STATUS_TRIGGERED   = 3;
    public static final int SLICE_STATUS_EXPIREDG    = 4;
    public static final int SLICE_STATUS_CENCELLED   = 5;
    public static final int SLICE_STATUS_AMENDED     = 6;
    public static final int SLICE_STATUS_DONE        = 7;//done for sliced orders , need to decide 39 tag value for this status

    public static final int CONDITION_TYPE_NONE             = 0;
    public static final int CONDITION_TYPE_STOP_LOSS        = 1;
    public static final int CONDITION_TYPE_NORMAL           = 2;
    public static final int CONDITION_TYPE_BRACKET          = 3;
    public static final int CONDITION_TYPE_STRADEL          = 4;
//    public static final int CONDITION_TYPE_CONDITIONAL_BID  = 3;
//    public static final int CONDITION_TYPE_CONDITIONAL_OFFER= 4;

    public static int CONDITION_TRIGGER_LAST = 1;
    public static int CONDITION_TRIGGER_BID  = 2;
    public static int CONDITION_TRIGGER_ASK  = 3;
    public static int CONDITION_TRIGGER_MIN  = 4;
    public static int CONDITION_TRIGGER_MAX  = 5;

    public static int CONDITION_OPERATOR_GREATER_THAN_OR_EQ = 1;
    public static int CONDITION_OPERATOR_LESS_THAN_OR_EQ    = 2;
    public static int CONDITION_OPERATOR_EQUAL              = 0;

    public static final int TIT_TRADE_SERVICES            = 0;
    public static final int TIT_CONDITIONAL_ORDERS        = 1;
    public static final int TIT_MANUAL_BROKER             = 2;
    public static final int TIT_ENABLE_BRACKET_ORDERS     = 3;
    public static final int TIT_ENABLE_SLICED_ORDERS      = 4;
//    public static final int TIT_ENABLE_ADVANCED_ORDERS    = 4;
    public static final int TIT_ENABLE_DAY_ORDERS         = 5;
    public static final int TIT_ENABLE_TRADE_API          = 6;
    public static final int TIT_BLOTTER_PULL_ONLY = 7;

//    public static final int SLICE_ORD_EXEC_NORMAL	= 0;
//    public static final int SLICE_ORD_EXEC_SLICED	= 1;
//    public static final int SLICE_ORD_EXEC_COND 	= 2;

    public static final int SLICE_ORD_TYPE_ALL 	        = 0;
    public static final int SLICE_ORD_TYPE_ICEBURG 	    = 1;
    public static final int SLICE_ORD_TYPE_TIME_INTERVAL= 2;

    //Trade services

}
