package com.isi.csvr.trading;

import com.isi.csvr.trading.shared.*;
import com.isi.csvr.trading.datastore.*;
import com.isi.csvr.trading.datastore.Rule;
import com.isi.csvr.trading.connection.*;
import com.isi.csvr.shared.*;
import com.isi.csvr.*;
import com.dfn.mtr.mix.beans.*;
import com.dfn.mtr.mix.common.MIXConstants;

import javax.swing.*;
import javax.net.ssl.*;
import java.net.*;
import java.security.*;
import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Aug 24, 2007
 * Time: 9:19:17 AM
 * To change this template use File | Settings | File Templates.
 */

public class SecondaryLoginDeamon extends Thread{
    private Socket socketSecondary = null;
    private OutputStream out;
    private static int connectionStatus = AutoTradeLoginDaemon.IDLE;
    private IP currentIP;
    private ReceiveQueue receiveQueue;
    private TradePulseGenerator pulseGenerator = null;
    private FrameAnalyser frameAnalyser;
    private String user;
    private AutoTradeLoginDaemon parent;
    private String secondaryIps;

    public SecondaryLoginDeamon(String user,String secondaryIps,AutoTradeLoginDaemon parent) {
        super("SecondaryLoginDeamon");
        this.parent = parent;
        this.user = user;
        this.secondaryIps = secondaryIps;
    }

    public void setFrameAnalyser(FrameAnalyser frameAnalyser){
        this.frameAnalyser = frameAnalyser;
    }

    public String getMubasherID(){
        return user;
    }

     public void run() {
        while (TradingShared.isConnected() && TradingShared.isSecondaryLoginEnabled()) {
            try {
                if (!TradingShared.SECONDARY_LOGIN_SUCCESS && !TradingShared.INVALID_SECONDARY_LOGIN) {
                    setMode(AutoTradeLoginDaemon.CONNECTING);
                    System.err.println("reconnecting secondary trading channel................");
                    sleepThread(500); // just in case, another request comes in
                    connect();
                }
                sleepThread(1000);
            } catch (Exception e) {
                e.printStackTrace();
                sleepThread(1000);
            }
        }
        TradingShared.SECONDARY_LOGIN_SUCCESS = false;
         TradingShared.INVALID_SECONDARY_LOGIN_ATEMPTS =0;
    }

    private synchronized void connect() {
        try {
            String ip;
            while (TradingShared.isConnected() && TradingShared.isSecondaryLoginEnabled() && !TradingShared.INVALID_SECONDARY_LOGIN && TradingShared.INVALID_SECONDARY_LOGIN_ATEMPTS==0) {
                try {
                    ip = getNextIP();
                    openSocket(ip);
                    if (authenticate()) {
                        setMode(AutoTradeLoginDaemon.CONNECTED);
                        TradingShared.SECONDARY_LOGIN_SUCCESS = true;
                        Client.getInstance().setTradeConnectionBulbStatus();
                        initializeApp();
                        break;
                    } else {
                        TradingShared.SECONDARY_LOGIN_SUCCESS = false;
                        Client.getInstance().setTradeConnectionBulbStatus();
                        closeSocket();
                        TradingShared.INVALID_SECONDARY_LOGIN_ATEMPTS  +=1;
                        SharedMethods.showMessage(Language.getString("SEONDARY_OMS_LOGIN_FAIL"), JOptionPane.ERROR_MESSAGE);
                    }
                }catch(SSLHandshakeException ssle){
                    ssle.printStackTrace();
                    if (ssle.getCause() instanceof java.security.cert.CertificateExpiredException){
//                        showSSLExpiration(ssle);
                    }
                    TradingShared.INVALID_SECONDARY_LOGIN = true;
                    break;
                }catch (Exception ex1) {
                    ex1.printStackTrace();
                }
                sleepThread(5000);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
            sleepThread(5000);
            TradingShared.SECONDARY_LOGIN_SUCCESS = false;
            TradingShared.INVALID_SECONDARY_LOGIN_ATEMPTS =0;
            Client.getInstance().setTradeConnectionBulbStatus();
            setMode(AutoTradeLoginDaemon.DISCONNECTED);
        }
    }

    private void showSSLExpiration(SSLHandshakeException ssle){
        String message = Language.getString("MSG_SSL_EXPIRED");
        message = message.replaceFirst("\\[DATE\\]",ssle.getCause().getLocalizedMessage());
        SharedMethods.showMessage(message, JOptionPane.ERROR_MESSAGE);
    }

    private String getNextIP() throws Exception {
        currentIP = IPSettings.getSharedInstance().getNextIP();
        String newip = currentIP.getIP();
        return newip;
    }

    private Socket openSocket(String ip) throws Exception {

        try {
            //todo - added temparary to test with jayalal
            ip  = secondaryIps.trim().split("\\|")[0];
            System.out.println("Connecting to secondary Trading ip " + secondaryIps );
            SSLSocketFactory factory = null;
            SSLContext ctx = SSLContext.getInstance("TLS");
            KeyManagerFactory kmf = KeyManagerFactory.getInstance("SunX509");
            KeyStore ks = KeyStore.getInstance("JKS");
            char[] passphrase = "mystorepass".toCharArray();
            char[] passphrase2 = "mykeypass".toCharArray();
            ks.load(new FileInputStream(Settings.CONFIG_DATA_PATH+"/trs.dll"), passphrase);
            kmf.init(ks, passphrase2);
            TrustManagerFactory tmf = TrustManagerFactory.getInstance("SunX509");
            tmf.init(ks);
            ctx.init(kmf.getKeyManagers(), tmf.getTrustManagers(), null);
            factory = ctx.getSocketFactory();
            socketSecondary = factory.createSocket(ip, Settings.TRADE_ROUTER_PORT);
            return socketSecondary;
        } catch (Exception e) {
            e.printStackTrace();
            throw e;
        }
    }

    private boolean authenticate() throws Exception {

        TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_MBS_AUTH);
        tradeMessage.addData(user);
        tradeMessage.addData(Settings.TW_VERSION);
        tradeMessage.addData(TradingShared.getIp());

         AuthenticationRequest mixaAthRequest = new AuthenticationRequest();
        MIXHeader mixHeader = new MIXHeader();
        mixHeader.setGroup(MIXConstants.GROUP_AUTHENTICATION);
        mixHeader.setRequestType(MIXConstants.REQUEST_TYPE_SECONDARY_LOGIN);

        mixaAthRequest.setUsername(user);
        mixaAthRequest.setIp(TradingShared.getIp());
        mixaAthRequest.setClientVersion(Settings.TW_VERSION);
//        mixaAthRequest.setClientChannel(MIXConstants.CHANNEL_TYPE_TW + "");
        if(TWControl.enablePickNewChannelID()){
            mixaAthRequest.setClientChannel(MIXConstants.CHANNEL_TYPE_TWS + "");
        } else {
            mixaAthRequest.setClientChannel(MIXConstants.CHANNEL_TYPE_NEWTW + "");
        }
        com.dfn.mtr.mix.beans.TransferObject[] mixTransferObjects = new com.dfn.mtr.mix.beans.TransferObject[]{mixaAthRequest};
        MIXObject mixBasicObject = new MIXObject();
        mixBasicObject.setMIXHeader(mixHeader);
        mixBasicObject.setTransferObjects(mixTransferObjects);


        out = socketSecondary.getOutputStream();
      //  out.write(tradeMessage.toString().getBytes());
        out.write(mixBasicObject.getMIXString().getBytes());
//        System.out.println("Trade Authenication Request sent in secondary " + tradeMessage.toString());
        InputStream in = socketSecondary.getInputStream();
        readLine(in); // ignore the ACK message
        String g_sAuthResult = readLine(in); // 1 auth reply
        System.out.println("============ Trade auth reply " + g_sAuthResult);
        Settings.setBurstReuestModeSecondary(false);
        MIXObject mixReply = new MIXObject();
        mixReply.setMIXString(g_sAuthResult);
        AuthenticationResponse mixAuthReply = (AuthenticationResponse) mixReply.getTransferObjects()[0];
      //  boolean authResult = parent.isAuthenticationValid(g_sAuthResult,Constants.PATH_SECONDARY);
        boolean authResult = parent.processMixAuthenticationResponse(mixAuthReply,Constants.PATH_SECONDARY, mixReply.getMIXHeader());
        System.out.println("result " + authResult);
        MixStore.getSharedInstance().setSecondaryAuthenticationObj(mixReply);

        return authResult;
    }

    public String readLine(InputStream in) throws Exception {
        int iValue;
        StringBuffer buffer = new StringBuffer();
        while (true) {
            iValue = in.read();
            if (iValue == -1) {
                throw new Exception("End of Stream");
            }
            if (iValue != '\n') {
                buffer.append((char) iValue);
                continue;
            } else {
                return buffer.toString();
            }
        }
    }


    private void initializeApp() throws Exception {
        BrokerConfig.getSharedInstance().loadData(TradingShared.getTrader().getBrokerID(Constants.PATH_SECONDARY), Constants.PATH_SECONDARY);
        TradingShared.setDisconnectionMode(TradingConstants.DISCONNECTION_UNEXPECTED); // clear any maual disconnection  requests
        TradingShared.getTrader().createAccounts();
        receiveQueue = new ReceiveQueue(socketSecondary,Constants.PATH_SECONDARY);
        SendQueue.getSharedInstance().init(socketSecondary, out, Constants.PATH_SECONDARY);
        receiveQueue.setReceiveBuffer(frameAnalyser.getList());
        receiveQueue.start();
        sendInitialRequests();
//            pulseGenerator = new TradePulseGenerator(Constants.PATH_SECONDARY);
//            pulseGenerator.start();
        try {
            Rule amendMode = RuleManager.getSharedInstance().getRule("AMEND_MODE_DELTA", "*", "NEW_ORDER");
            if ((amendMode.getRule().equals("1") || amendMode.getRule().equals("true"))){
                TradingShared.setAmendMode(TradingShared.AMEND_MODE_DELTA);
            } else {
                TradingShared.setAmendMode(TradingShared.AMEND_MODE_FULL);
            }
        } catch (Exception e) {
            TradingShared.setAmendMode(TradingShared.AMEND_MODE_FULL);
        }
        TradingShared.getTrader().notifyAccountListeners(null);
        TradingShared.getTrader().notifyPortfolioListeners(null);
        TradingConnectionNotifier.getInstance().fireSecondaryPathConnected();
//        TradeMethods.getSharedInstance().pendingFundTransfer(Constants.PATH_SECONDARY);
//        OMSMessageList.getSharedInstance().requestSecondaryOMSMessages();
    }

    private void sendInitialRequests() {
        try {
            if (Settings.isBurstRequestModeSecondary()) {
                TradeMethods.requestUserDataSecondary(Constants.PATH_SECONDARY, true);

            } else {
              //  TradeMethods.requestUserDataSecondary(Constants.PATH_SECONDARY, false);
            TradeMethods.requestPortfolioData(Constants.PATH_SECONDARY);
            TradeMethods.requestAccountData(Constants.PATH_SECONDARY);
                TradeMethods.requestCurrencyData(Constants.PATH_SECONDARY);
            }
            TradeMethods.requestBankAccountDetails(Constants.PATH_SECONDARY);
            TradingShared.setReadyForTrading(true);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isPasswordSaved() {
        return ((TradingShared.getLevel1Password() != null) && ((user != null) && (!user.trim().equals(""))));
    }

    private void assignContactDetails(String data){
        try {
            String[] fields = data.split("\\|");
            Settings.setBrokerPhone(fields[0]);
            Settings.setBrokerFax(fields[1]);
            Settings.setBrokerURL(fields[2]);
        } catch (Exception e) {
        }
    }

    public void showInvalidAuthMessage(String message) {
        Object[] options = {
                Language.getString("OK")};
        JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                message,
                Language.getString("ERROR"),
                JOptionPane.OK_OPTION,
                JOptionPane.ERROR_MESSAGE,
                null,
                options,
                options[0]);
    }

    /**
     * Display the invalid authentication dialog
     */


    public static void sleepThread(long time) {
        try {
            sleep(time);
        }
        catch (InterruptedException ex) {
            ex.printStackTrace();
        }
    }

    public void closeSocket() {
        try {
            socketSecondary.close();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    public synchronized static int getMode() {
        return connectionStatus;
    }

    public synchronized static void setMode(int status) {
        connectionStatus = status;
    }

}
