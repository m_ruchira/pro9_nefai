package com.isi.csvr.trading;

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.IP;
import com.isi.csvr.shared.Settings;

import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 24, 2003
 * Time: 12:07:04 PM
 * To change this template use Options | File Templates.
 */
public class IPSettings {

    private DynamicArray ips;
//    private DynamicArray SSOips;
    private int ipIndex = 0;
    private int activeIndex = 0;

    public static IPSettings self = null;

    public static synchronized IPSettings getSharedInstance() {
        if (self == null) {
            self = new IPSettings();
        }
        return self;
    }

    private IPSettings() {
        DataInputStream fileIn = null;
        ips = new DynamicArray(3);
//        SSOips = new DynamicArray(3);

        //load tip.dll filre for sso mode
        File ipFile = new File(Settings.SYSTEM_PATH + "/tip.dll"); // get static IPs
        if (ipFile.exists()) {
            try {
                fileIn = new DataInputStream(new FileInputStream(ipFile));
                String line;
                boolean first = true;
                while (true) {
                    line = fileIn.readLine();
                    if (line == null) break;
                    if (!line.trim().equals("")) {
                        if (first)
                            addSSOIP(line.trim(), IP.STATIC, IP.PRIMARY);
                        else
                            addSSOIP(line.trim(), IP.STATIC, IP.SECONDARAY);
                        first = false;
                    }
                }
                fileIn.close();
                fileIn = null;
            } catch (IOException e) {
                e.printStackTrace();
            }
        } else {
            clearOldIPs(); // if no ip file check for the st.dll for IPs
        }
        ipFile = null;
    }

    public void addIP(String ip) {

        addIP(ip, IP.STATIC, IP.PRIMARY);
    }

    public synchronized int getIPCount() {
        return ips.size();
    }

    public synchronized IP getIP(int index) {
        return ((IP) ips.get(index));
    }

    public synchronized IP getSSOIP(int index) {
        return ((IP) ips.get(index));
    }

    public synchronized String getStaticIP(int index) {
        int counter = 0;
        for (int i = 0; i < ips.size(); i++) {
            IP ip = getIP(i);
            if (ip.getType() == IP.STATIC) {
                if (counter == index) {
                    return ip.getIP();
                }
                counter++;
            }
            ip = null;
        }
        return "";
    }

    /*public static void setIP(int index, String ip) {
        try {
            ips.set(index, ip);
        } catch (Exception e) {
            ips.add(ip);
        }
    }*/

    public synchronized void addIP(String ip, int type, int ipClass) {
        ips.add(new IP(ip, type, ipClass));
    }

    public synchronized void addSSOIP(String ip, int type, int ipClass) {
        ips.add(new IP(ip, type, ipClass));
    }

/*    public synchronized static void save() {
        FileOutputStream fileOut = null;

        try {
            fileOut = new FileOutputStream("./system/ip.dll");
            for (int i = 0; i < ips.size(); i++) {
                IP ip = getIP(i);
                if ((ip != null) && (ip.getType() == IP.STATIC)){
                    fileOut.write(ip.getIP().getBytes());
                    fileOut.write("\r\n".getBytes());
                }
                ip = null;
            }
            fileOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fileOut = null;
    }*/

    public synchronized int getIpIndex() {
        return activeIndex;
    }

    public synchronized int getNextIpIndex() {
        return ipIndex;
    }

    public synchronized void resetIPIndex() {
        ipIndex = 0;
    }

    public synchronized IP getNextIP() throws Exception {

        IP ip = getIP(ipIndex);
        activeIndex = ipIndex;
        if (ip == null) {
            throw new Exception("No IPs found");
        } else {
            ipIndex++;
        }
        if (ipIndex >= ips.size()) {
            ipIndex = 0;
        }
        return ip;
//        return new IP("192.168.0.24",IP.STATIC, IP.PRIMARY);
//        return new IP("10.0.0.113",IP.STATIC, IP.PRIMARY);
    }

    private synchronized void clearOldIPs() {

    }

    public void removeIPS() {
       ips.clear(); 
    }

    public void save() {
        FileOutputStream fileOut = null;

        try {
            fileOut = new FileOutputStream(Settings.SYSTEM_PATH + "/tip.dll");
            for (int i = 0; i < ips.size(); i++) {
                IP ip = getSSOIP(i);
                if ((ip != null) && (ip.getType() == IP.STATIC)) {
                    fileOut.write(ip.getIP().getBytes());
                    fileOut.write("\r\n".getBytes());
                }
                ip = null;
            }
            fileOut.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        fileOut = null;
    }
    public void clearSSOIPs(){
        ips.clear();
        ips.trimToSize();
        ipIndex=0;
        activeIndex=0;
    }

}
