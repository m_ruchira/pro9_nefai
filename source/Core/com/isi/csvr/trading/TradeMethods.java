package com.isi.csvr.trading;

import bsh.EvalError;
import bsh.Interpreter;
import com.dfn.mtr.mix.beans.*;
import com.dfn.mtr.mix.beans.TransferObject;
import com.dfn.mtr.mix.common.MIXConstants;
import com.isi.csvr.ChangePass;
import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.trading.datastore.*;
import com.isi.csvr.trading.datastore.Rule;
import com.isi.csvr.trading.marginTrading.MarginCalculator;
import com.isi.csvr.trading.marginTrading.MarginSymbolRecord;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.portfolio.TransactRecord;
import com.isi.csvr.trading.security.TWSecurityManager;
import com.isi.csvr.trading.shared.*;
import com.isi.csvr.trading.ui.*;
import com.isi.util.FlexGridLayout;
import com.jniwrapper.win32.ie.Browser;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.*;
import java.beans.PropertyVetoException;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.text.SimpleDateFormat;
import java.util.*;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 16, 2004
 * Time: 3:28:41 PM
 * To change this template use File | Settings | File Templates.
 */
public class TradeMethods implements PopupMenuListener, TradeMeta {

    private AccountWindow accountFrame;
    private OrderSearchWindow orderSearchWindow;
    private NewPFCreationWindow newPFCreationWindow;
    private TodayTradeWindow todayTradeWindow;
    private CashLogSearchWindow cashLogSearchWindow;
    //    private Table blotterTable;
    private BlotterFrame blotterFrame;
    private InternalFrame blotterFrameInternalFrame = null;
    private OpenPositions openPositionsFrame;
    private ViewSetting accountSettings;
    private ViewSetting fundTransferSettings;
    private FundTransferWindow fundTransferFrame;
    private StockTransferListWindow stockTransferFrame;
    private static TradeMethods self = null;
    MarginableSymbolWindow marginableUI;
    private static boolean pfRefreshClicked = false;
    private static HashMap<String, Double> commissionStore = new HashMap<String, Double>();
    private static HashMap<String, Double> vatStore = new HashMap<String, Double>();
    private static HashMap<String, OrderCommissionRequest> commissionRequests = new HashMap<String, OrderCommissionRequest>();
    private static Browser browser;
    private ReceiveQueue receiveQueue;

    public static synchronized TradeMethods getSharedInstance() {
        if (self == null) {
            self = new TradeMethods();
        }
        return self;
    }

    public void init() {
        createAccountWindow();
        createFundTranferFrame();
        orderSearchWindow = createOrderSearchWindow();  // Create the Programed orders frame
        cashLogSearchWindow = createCashLogSearchWindow();  // Create the Programed orders frame
    }

    public TradeMethods() {

    }

    public static void setBrowserDetails(int mode, Stock stock, int type, String pricestr, long quantity, String commission, String total, String stopLossString, String takeprofitString, String condtionMsg, String warningMsg) {
        String scriptString = "showOrderConfirmation(" + "'" + TradingShared.getActionString(mode) + "'" + "," +
                "'" + stock.getShortDescription() + "'" + "," +
                "'" + stock.getSymbol() + "'" + "," +
                "'" + TradingShared.getTypeString(type) + "'" + "," +
                "'" + pricestr + "'" + "," +
                "'" + SharedMethods.toIntegerFoamat(quantity) + "'" + "," +
                "'" + commission + "'" + "," +
                "'" + total + "'" + "," +
                "'" + stopLossString + "'" + "," +
                "'" + takeprofitString + "'" + "," +
                "'" + condtionMsg + "'" + "," +
                "'" + warningMsg + "'" + "," +
                true + "," +
                false + "," +
                false + "," +
                false + "," +
                false + ")";

        browser.setSilent(true);
        browser.executeScript(scriptString);
    }

    public static boolean isSymbolAllowedForProtfolio(String portfolioID, String exchange, boolean showerrmsg) {
        ArrayList<String> exchanges = TradingShared.getTrader().getPortfolio(portfolioID).getExchangeList();
        if (exchanges.contains(exchange)) {
            return true;
        } else {
            if (showerrmsg) {
//                if (exchange == null || exchange.equals("")) {
//
//                } else {
                SharedMethods.showMessage(Language.getString("INVALID_TRADING_SYMBOL_FOR_PORTFOLIO"), JOptionPane.ERROR_MESSAGE);
//                }
            }
            return false;
        }
    }

    public static TradingPortfolioRecord getPreferredProtfolio(String exchange) {
        try {
            ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
            for (TradingPortfolioRecord portflio : portfolios) {
                ArrayList<String> exchanges = portflio.getExchangeList();
                if (exchanges.contains(exchange)) {
                    return portflio;
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public static boolean isSignatureRequired() {
        return (TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_USB);
    }

    public boolean isTradingAuthenticationInitiated() {
        String pw = null;

        switch (TradingShared.getLevel2AuthType()) {
            case TradeMeta.LEVEL2_AUTH_PW_ALLWAYS:
                if (TradingShared.getLevel2Password() == null) {
                    pw = TradingPWDialog.getSharedInstance().getPW();
                    //                    TradingShared.setLevel2Password(pw);
                    TradingShared.setLevel2AuthenticationPending(true); // request the blocking window
                    return sendLevel2AuthRequest(pw);
                } else {
                    TradingShared.setLevel2AuthenticationPending(false);
                    TradingShared.level2AuthenticationSucess = true;
                    return true;
                }
            case TradeMeta.LEVEL2_AUTH_PW:                          // ask for the password once and keep as authenticated for the rest
                if (TradingShared.getLevel2Password() == null) {    // first time
                    pw = TradingPWDialog.getSharedInstance().getPW();
                    TradingShared.setLevel2Password(pw);
                    TradingShared.setLevel2AuthenticationPending(true); // request the blocking window
                    TradingShared.setLevel2Password(pw);
                    return sendLevel2AuthRequest(pw);
                } else {
                    TradingShared.setLevel2AuthenticationPending(false);
                    TradingShared.level2AuthenticationSucess = true;
                    return true;
                }
            case TradeMeta.LEVEL2_AUTH_NONE:
                TradingShared.setLevel2AuthenticationPending(false);
                TradingShared.level2AuthenticationSucess = true;
                return true; // no level 2 authentication
            case TradeMeta.LEVEL2_AUTH_RSA_ONCE:
                if (TradingShared.getLevel2Password() == null) {    // first time
                    pw = TradingPWDialog.getSharedInstance().getPW();
                    TradingShared.setLevel2Password(pw);
                    TradingShared.setLevel2AuthenticationPending(true); // request the blocking window
                    TradingShared.setLevel2Password(pw);
                    return sendLevel2AuthRequest(pw);
                } else {
                    TradingShared.setLevel2AuthenticationPending(false);
                    TradingShared.level2AuthenticationSucess = true;
                    return true;
                }
            case TradeMeta.LEVEL2_AUTH_RSA_ALLWAYS:
                pw = TradingPWDialog.getSharedInstance().getPW();
                TradingShared.setLevel2Password(pw);
                TradingShared.setLevel2AuthenticationPending(true); // request the blocking window
                return sendLevel2AuthRequest(pw);
            case TradeMeta.LEVEL2_AUTH_USB:
                if (TWSecurityManager.isUSBConected()) { // check if the usb device is connected
                    if (TradingShared.getUSBPin() == null) {    // first time
                        pw = TradingPWDialog.getSharedInstance().getPW();
                        if (pw != null) {
                            boolean changed = changeInitialUSBPin(pw); // check if the pin is Mubasher Default and force change if so
                            if (changed) {
                                pw = SharedMethods.decrypt(TradingShared.getUSBPin());
                            }
                            if (TWSecurityManager.matchPWWithUSB(pw)) {
                                TradingShared.setLevel2AuthenticationPending(false); // request the blocking window
                                TradingShared.setUSBPin(SharedMethods.encrypt(pw));
                                TradingShared.level2AuthenticationSucess = true;
                                return true;
                                /*TradingShared.setLevel2Password(pw);
                                TradingShared.setLevel2AuthenticationPending(true); // request the blocking window
                                TradingShared.setLevel2Password(pw);
                                return sendLevel2AuthRequest(pw);*/
                            } else {
                                TradingShared.setLevel2AuthenticationPending(false);
                                TradingShared.level2AuthenticationSucess = false;
                                TradingShared.setUSBPin(null);
                                showInvalidHWDeviceMessage();
                                return false;
                            }
                        } else { // user cancelled the pw dialog
                            return false;
                        }
                    } else {
                        if (TWSecurityManager.matchPWWithUSB(SharedMethods.decrypt(TradingShared.getUSBPin()))) {
                            TradingShared.setLevel2AuthenticationPending(false); // request the blocking window
                            TradingShared.level2AuthenticationSucess = true;
                            return true;
                        } else {
                            TradingShared.setLevel2AuthenticationPending(false);
                            TradingShared.level2AuthenticationSucess = false;
                            showInvalidHWDeviceMessage();
                            return false;
                        }
                    }
                } else {
                    showNoUSBDeviceMessage();
                    return false;
                }
            default:
                return false;
        }
    }

    private boolean changeInitialUSBPin(String pin) {
        if (TWSecurityManager.isInitialPin(pin)) {
            int returnValue = -1;
            while (returnValue < 0) {
                returnValue = TradeMethods.getSharedInstance().changeUSBPW(true);
            }
            return returnValue == 1;
            //SharedMethods.showBlockingMessage("Initial PW", JOptionPane.WARNING_MESSAGE);
        }
        return false;
    }

    public boolean isUSBRequired() {
        return (TradingShared.getLevel2AuthType() == TradeMeta.LEVEL2_AUTH_USB);
    }

    public void showInvalidHWDeviceMessage() {
//        try {
//            Thread.sleep(300);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        Object[] options = {Language.getString("OK")};
        JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                Language.getString("INVALID_USB_PW_MESSAGE"),
                Language.getString("ERROR"),
                JOptionPane.OK_OPTION,
                JOptionPane.ERROR_MESSAGE,
                null,
                options,
                options[0]);
    }

    public void shoowInquiryNotAvailableMessage() {
//        try {
//            Thread.sleep(300);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        Object[] options = {Language.getString("OK")};
        JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                Language.getString("REQUESTE_INFORMATION_NOTAVAILABLE"),
                Language.getString("INFORMATION"),
                JOptionPane.OK_OPTION,
                JOptionPane.INFORMATION_MESSAGE,
                null,
                options,
                options[0]);
    }

    public void showNoUSBDeviceMessage() {
//        try {
//            Thread.sleep(300);
//        } catch (InterruptedException e) {
//            e.printStackTrace();
//        }
        Object[] options = {Language.getString("OK")};
        JOptionPane.showOptionDialog(Client.getInstance().getFrame(),
                Language.getString("NO_USB_DEVICE"),
                Language.getString("ERROR"),
                JOptionPane.OK_OPTION,
                JOptionPane.ERROR_MESSAGE,
                null,
                options,
                options[0]);
    }

    public void blockTillValidated() {
        if (TradingShared.level2AuthenticationPending) {
            Level2AuthWait.getSharedInstance().show();      // show the blocking window
        }
    }

    public void showAuthenticationWait(boolean blocking) {
        try {
            if (blocking) {
                Level2AuthWait.getSharedInstance().showWithBlocking();
            } else {
                Level2AuthWait.getSharedInstance().showWithNoneBlocking();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void hideAuthenticationWait() {
        try {
            Level2AuthWait.getSharedInstance().dispose();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean sendLevel2AuthRequest(String pw, String portfolioID) {
        if (pw != null) {
            TradeMessage message = new TradeMessage(TradeMeta.MT_AUTH_2);
            message.addData(TradingShared.getTrader().getUserID(portfolioID));
            message.addData(pw);
            message.addData(TradingShared.getIp());
            com.isi.csvr.trading.SendQueue.getSharedInstance().addData(message.toString(), TradingShared.getTrader().getPath(portfolioID));
            message = null;
            return true;
        } else {
            return false;
        }
    }

    private boolean sendLevel2AuthRequest(String pw) {
        if (pw != null) {
            TradeMessage message = new TradeMessage(TradeMeta.MT_AUTH_2);
            message.addData(TradingShared.getTrader().getUserID(Constants.PATH_PRIMARY));
            message.addData(pw);
            message.addData(TradingShared.getIp());

            MIXObject level2Auth = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_AUTHENTICATION, MIXConstants.REQUEST_TYPE_AUTH_LEVEL2, Constants.PATH_PRIMARY);
            AuthenticationRequest mixaAthRequest = new AuthenticationRequest();
            mixaAthRequest.setUserId(TradingShared.getTrader().getUserID(Constants.PATH_PRIMARY));
            mixaAthRequest.setTxnPassword(pw);
            mixaAthRequest.setIp(TradingShared.getIp());
            level2Auth.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{mixaAthRequest});

            //SendQueue.getSharedInstance().addData(message.toString(), Constants.PATH_PRIMARY);
            SendQueue.getSharedInstance().addData(level2Auth.getMIXString(), Constants.PATH_PRIMARY);

            message = null;
            return true;
        } else {
            return false;
        }
    }

    public void createAccountWindow() {
        if (accountFrame == null) {
            //Table accountTable = new Table();
            //AccountWindow accWin =new AccountWindow(accountTable);
            ViewSetting oSetting = ViewSettingsManager.getSummaryView("ACCOUNT");
//            GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("ACCOUNT_COLS"));
            Table accountTable = new Table();
            accountFrame = new AccountWindow(accountTable, oSetting);
            accountSettings = accountFrame.getViewSettings();
        }
    }

    public boolean getIsAccountFrameCreated() {

        if (accountSettings != null) {
            return true;
        } else {
            return false;
        }

    }


    public void showAccountFrame() {
        showAccountFrame(null, null);
    }

    public void showAccountFrame(String[] portfolios, String currency) {
        try {
            if (TradingShared.isReadyForTrading()) {
//                if (accountFrame == null) {
                if (!accountFrame.isVisible()) {
                    createAccountWindow();
                    // Added by Shanaka
                    TradeMethods.requestPortfolioData(Constants.PATH_PRIMARY);
                    TradeMethods.requestAccountData(Constants.PATH_PRIMARY);
                }
                if (accountFrame.isIcon()) {
                    accountFrame.setIcon(false);
                }
                accountFrame.show();
                accountFrame.setSelected(true);
                accountFrame.selectAccounts(portfolios, currency);
                SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "AccountSummary");
            } else {
                SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


    public void showDetailQuoteOrder(Transaction order) {
        try {
            String mubasherOrderNumber = null;
            String clOdrID = null;

            try {
                mubasherOrderNumber = order.getMubasherOrderNumber();
                clOdrID = order.getClOrderID();
                if (mubasherOrderNumber == null) {
                    shoowInquiryNotAvailableMessage();
                    return;
                }
            } catch (Exception e) {
                shoowInquiryNotAvailableMessage();
                return;
            }

            /* check if the window is already created
                If so, do not create it again, just focus it
            */
            InternalFrame oFrame = Client.getInstance().getViewSettingsManager().getWindow(ViewSettingsManager.DETAIL_QUOTE_ORDER + "|" + mubasherOrderNumber);
            if (oFrame != null) {
                oFrame.show();
                try {
                    oFrame.setSelected(true);
                } catch (Exception x) {
                }

                if (oFrame.isIcon())
                    oFrame.setIcon(false);
                return;
            }

            OrderStatusWindow oDQuoteFrame = new OrderStatusWindow(Client.getInstance().getWindowDaemon(), Client.getInstance().getViewSettingsManager(), mubasherOrderNumber, clOdrID);
            Client.getInstance().getDesktop().add(oDQuoteFrame);
            oDQuoteFrame.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);
            oDQuoteFrame.updateUI();

            oDQuoteFrame.show();
            oDQuoteFrame.setLayer(GUISettings.TOP_LAYER);
            oDQuoteFrame.setOrientation();
            oDQuoteFrame = null;

            OrderDepthStore.getSharedInstance().register(mubasherOrderNumber);
            if ((order.getStatus() == TradeMeta.T39_Filled) || (order.getStatus() == TradeMeta.T39_Partially_Filled) || (order.getStatus() == 114 /*Partially filled Cancel*/) ||
                    ((order.getStatus() == TradeMeta.T39_Canceled) && order.filledQuantity > 0)) {
                MIXObject execHistory = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING_INQUIRY, MIXConstants.REQUEST_TYPE_EXECUTION_HISTORY, TradingShared.getTrader().getPath(order.getPortfolioNo()));
                OrderSearchInquiryObject historyInq = new OrderSearchInquiryObject();
                historyInq.setOrdPortfolioID(order.getPortfolioNo());
                historyInq.setOrdUserID(TradingShared.getTrader().getUserID(order.getPortfolioNo()));
                historyInq.setClOrderID((order.getClOrderID()));
                historyInq.setMubasherOrderNumber(Long.parseLong(order.getMubasherOrderNumber()));
                execHistory.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{historyInq});
                // TradeMessage message = new TradeMessage(TradeMeta.MT_ORDER_DEPTH);
                // message.addData(order.getClOrderID());
                // message.addData(TradingShared.getTrader().getUserID(order.getPortfolioNo()));
                //  message.addData(order.getPortfolioNo());
                //  message.addData(order.getMubasherOrderNumber());
                // SendQueue.getSharedInstance().addData(message.toString(), TradingShared.getTrader().getPath(order.getPortfolioNo()));
                SendQueue.getSharedInstance().addData(execHistory.getMIXString(), TradingShared.getTrader().getPath(order.getPortfolioNo()));
                //  message = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void showFundTransferFrame() {
        try {
            if (TradingShared.isReadyForTrading()) {
                if (fundTransferFrame == null) {
                    createFundTranferFrame();
                    pendingFundTransfer(Constants.PATH_PRIMARY);
                    pendingFundTransfer(Constants.PATH_SECONDARY);
                }
                if (!fundTransferFrame.isVisible()) {
                    if (fundTransferFrame.isIcon()) {
                        fundTransferFrame.setIcon(false);
                    }
                    fundTransferFrame.show();
                    fundTransferFrame.setSelected(true);
                    SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "DepositWithdrawalStatus");
                } else {
                    if (fundTransferFrame.isIcon()) {
                        fundTransferFrame.setIcon(false);
                    }
                    fundTransferFrame.setSelected(true);
                }
            } else {
                SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public InternalFrame createBlotterFrame() {
        if (blotterFrameInternalFrame == null) {
            blotterFrame = new BlotterFrame();
            blotterFrameInternalFrame = blotterFrame.createBlotterFrame();
            return blotterFrameInternalFrame;
        } else {
            return blotterFrameInternalFrame;
        }
    }

    public InternalFrame createOpenPositionsFrame() {
        openPositionsFrame = new OpenPositions();
        return openPositionsFrame.createUI();
    }

    public BlotterFrame getBlotterFrame() {
        if (blotterFrame == null) {
            createBlotterFrame();
        }
        return blotterFrame;
    }

    public OpenPositions getOpenPositionsFrame() {
        if (openPositionsFrame == null) {
            createOpenPositionsFrame();
        }
        return openPositionsFrame;
    }

    public void createFundTranferFrame() {
        if (fundTransferFrame == null) {
            Table fundTransferTable = new Table();
            fundTransferFrame = new FundTransferWindow(fundTransferTable);
            fundTransferSettings = fundTransferFrame.getViewSetting();
        }
    }

    public void cancelFundTransfer(FundTransfer fundtransfer, int type, String referneceID) {
        String str = Language.getString("FUNDTRANS_CANCEL_CONFIRM_MESSAGE");
        int result = SharedMethods.showConfirmMessage(str, JOptionPane.INFORMATION_MESSAGE, JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
                TradeMethods.getSharedInstance().blockTillValidated();
                if (TradingShared.level2AuthenticationSucess) {
                    try {
                        MIXObject fundTranferCancel = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_FINANCE_AND_HOLDING, MIXConstants.REQUEST_TYPE_DEPOSITE_WITHDRAWAL_CANCEL
                                , TradingShared.getTrader().getPath(fundtransfer.getPortfolio()));
                        CashTransfer cancel = new CashTransfer();
                        cancel.setReferenceID(referneceID);
                        cancel.setPortfolioID(fundtransfer.getPortfolio());
                        cancel.setUserID(TradingShared.getTrader().getUserID(fundtransfer.getPortfolio()));
                        fundTranferCancel.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{cancel});
//                        TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_FUND_TRANSFER_CANCEL);
//                        tradeMessage.addData("A" + referneceID);
//                        tradeMessage.addData("C" + fundtransfer.getPortfolio());
//                        tradeMessage.addData("B" + TradingShared.getTrader().getUserID(fundtransfer.getPortfolio()));
                        // SendQueue.getSharedInstance().writeData(tradeMessage.toString(), TradingShared.getTrader().getPath(fundtransfer.getPortfolio()));
                        SendQueue.getSharedInstance().writeData(fundTranferCancel.getMIXString(), TradingShared.getTrader().getPath(fundtransfer.getPortfolio()));
                        //tradeMessage = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
        }
    }

    public InternalFrame createStockTranferFrame() {
        stockTransferFrame = new StockTransferListWindow();
        return stockTransferFrame.createStockTransferFrame();
    }

    public StockTransferListWindow getStockTransferFrame() {
        if (stockTransferFrame != null) {
            return stockTransferFrame;
        } else {
            return null;
        }
    }

    public FundTransferWindow getFundTransferWindow() {
        if (fundTransferFrame == null) {
            createFundTranferFrame();
        }
        return fundTransferFrame;
    }

    public void cancelStockTransfer(StockTransferObject stocktransfer, String referneceID) {
        String str = Language.getString("STOCKTRANS_CANCEL_CONFIRM_MESSAGE");
        int result = SharedMethods.showConfirmMessage(str,
                JOptionPane.INFORMATION_MESSAGE, JOptionPane.YES_NO_OPTION);
        if (result == JOptionPane.YES_OPTION) {
            try {
                TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_STOCK_TRANSFER_CANCEL);
                tradeMessage.addData("A" + referneceID);
                tradeMessage.addData("B" + TradingShared.getTrader().getUserID(stocktransfer.getPortfolioID()));
                tradeMessage.addData("C" + TradingShared.getTrader().getMubasherID(stocktransfer.getPortfolioID()));
                SendQueue.getSharedInstance().writeData(tradeMessage.toString(), TradingShared.getTrader().getPath(stocktransfer.getPortfolioID()));
                tradeMessage = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void cancelAll() {
        if (TradingShared.isReadyForTrading()) {
            if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
                TradeMethods.getSharedInstance().blockTillValidated();
                if (TradingShared.level2AuthenticationSucess) {
                    ListIterator<Transaction> transactions = OrderStore.getSharedInstance().getTransactions();
                    try {
                        if (OrderStore.getSharedInstance().size() > 0) {
                            int status = SharedMethods.showConfirmMessage(Language.getString("MSG_CONFIRM_CANCEL_OPEN_ORDERS"),
                                    JOptionPane.WARNING_MESSAGE);
                            if (status == JOptionPane.OK_OPTION) {
                                while (transactions.hasNext()) {
                                    Transaction transaction = transactions.next();
                                    if (getItemValidity("CANCEL", "ORDER_STATUS_WINDOW", transaction.getExchange(),
                                            (char) transaction.getStatus())) {
                                        cancelOrder(transaction);
                                    }
                                    transaction = null;
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    transactions = null;
                }
            }
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }
    }

    public void cancelAllBuy() {
        if (TradingShared.isReadyForTrading()) {
            if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
                TradeMethods.getSharedInstance().blockTillValidated();
                if (TradingShared.level2AuthenticationSucess) {
                    ListIterator<Transaction> transactions = OrderStore.getSharedInstance().getTransactions();
                    try {
                        if (OrderStore.getSharedInstance().size() > 0) {
                            int status = SharedMethods.showConfirmMessage(Language.getString("MSG_CONFIRM_CANCEL_BUY_ORDERS"),
                                    JOptionPane.WARNING_MESSAGE);
                            if (status == JOptionPane.OK_OPTION) {
                                while (transactions.hasNext()) {
                                    Transaction transaction = transactions.next();
                                    if (transaction.getSide() == TradeMeta.BUY) {
                                        if (getItemValidity("CANCEL", "ORDER_STATUS_WINDOW", transaction.getExchange(),
                                                (char) transaction.getStatus())) {
                                            cancelOrder(transaction);
                                        }
                                    }
                                    transaction = null;
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    transactions = null;
                }
            }
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }
    }

    public void cancelAllSell() {
        if (TradingShared.isReadyForTrading()) {
            if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
                TradeMethods.getSharedInstance().blockTillValidated();
                if (TradingShared.level2AuthenticationSucess) {
                    ListIterator<Transaction> transactions = OrderStore.getSharedInstance().getTransactions();
                    try {
                        if (OrderStore.getSharedInstance().size() > 0) {
                            int status = SharedMethods.showConfirmMessage(Language.getString("MSG_CONFIRM_CANCEL_SELL_ORDERS"),
                                    JOptionPane.WARNING_MESSAGE);
                            if (status == JOptionPane.OK_OPTION) {
                                while (transactions.hasNext()) {
                                    Transaction transaction = transactions.next();
                                    if (transaction.getSide() == TradeMeta.SELL) {
                                        if (getItemValidity("CANCEL", "ORDER_STATUS_WINDOW", transaction.getExchange(),
                                                (char) transaction.getStatus())) {
                                            cancelOrder(transaction);
                                        }
                                    }
                                    transaction = null;
                                }
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                    transactions = null;
                }
            }
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }
    }

    public void sortBlotter() {
        try {
            blotterFrame.getTable().resort();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void unsortBlotter() {
        try {
            blotterFrame.getTable().unsort();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void removeBlotterSorting() {
        try {
            blotterFrame.getTable().getModel().getViewSettings().setSortColumn(-1);
        } catch (Exception e) {
//            e.printStackTrace();
        }
    }

    public int getBlotterSortColumn() {
        try {
            return blotterFrame.getTable().getModel().getViewSettings().getSortColumn();
        } catch (Exception e) {
            return 0;
        }
    }

    public void setBlotterSortColumn(int col) {
        try {
            try {
                blotterFrame.getTable().getModel().getViewSettings().setSortColumn(col);
            } catch (Exception e) {
                blotterFrame.getTable().getModel().getViewSettings().setSortColumn(1);
            }
        } catch (Exception e) {
            // blotter not loaded
        }
    }


    public void showRejectReason(String orderNumber) {
        Transaction transaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(orderNumber);
        if (transaction == null) {
            transaction = OrderSearchStore.getSharedInstance().searhTransactionByClOrderID(orderNumber);
        }
        String message = Language.getLanguageSpecificString(transaction.getRejectReason(), "|");
        String template = Language.getString("REJECT_REASON_EXPLAIN");
        template = template.replaceFirst("\\[REASON\\]", message);
        new ShowMessage(template, "I");
        message = null;
        template = null;
        transaction = null;
    }


    public void showRejectReasonFull(Transaction transaction) {
        int currencyDecimals = 2;
        int stockDecimals = 2;
        try {
            String currency = TradingShared.getTrader().findAccountByPortfolio(transaction.getPortfolioNo()).getCurrency();
            currencyDecimals = SharedMethods.getCurrencyDecimalPlaces(currency);
            stockDecimals = DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getDecimalCount();
        } catch (Exception e) {
            stockDecimals = 3;
        }
        String message = transaction.getRejectReason();
        String template = Language.getString("REJECT_REASON_POPUP");
        template = template.replaceFirst("\\[ID\\]", "" + transaction.getMubasherOrderNumber());
        if (transaction.getPrice() > 0)
//            template = template.replaceFirst("\\[PRICE\\]", SharedMethods.formatToDecimalPlaces(currencyDecimals, transaction.getPrice()));
            template = template.replaceFirst("\\[PRICE\\]", SharedMethods.formatToDecimalPlaces(stockDecimals, transaction.getPrice()));
        else
            template = template.replaceFirst("\\[PRICE\\]", Language.getString("NA"));
        template = template.replaceFirst("\\[QTY\\]", SharedMethods.toIntegerFoamat(transaction.getOrderQuantity()));
        template = template.replaceFirst("\\[REASON\\]", message);
        new ShowMessage(template, "I");
//        SharedMethods.showInternalFrameMessage(template, JOptionPane.INFORMATION_MESSAGE);
        message = null;
        template = null;
    }

    public void showOrderAcceptedPopup(TRSOrder trsOrder) {
        int currencyDecimals = 2;
        int stockDecimals = 2;
        Transaction transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(trsOrder.getMubasherOrderNumber());
        try {
            String currency = TradingShared.getTrader().findAccountByPortfolio(transaction.getPortfolioNo()).getCurrency();
            currencyDecimals = SharedMethods.getCurrencyDecimalPlaces(currency);
            stockDecimals = DataStore.getSharedInstance().getStockObject(transaction.getExchange(), transaction.getSymbol(), transaction.getSecurityType()).getDecimalCount();
        } catch (Exception e) {
            stockDecimals = 3;
        }

        String template = Language.getString("ORDER_ACCEPTED_POPUP");
        template = template.replaceFirst("\\[ID\\]", "" + transaction.getMubasherOrderNumber());
        if (transaction.getPrice() > 0)
//            template = template.replaceFirst("\\[PRICE\\]", SharedMethods.formatToDecimalPlaces(currencyDecimals, transaction.getPrice()));
            template = template.replaceFirst("\\[PRICE\\]", SharedMethods.formatToDecimalPlaces(stockDecimals, transaction.getPrice()));
        else
            template = template.replaceFirst("\\[PRICE\\]", Language.getString("NA"));
        template = template.replaceFirst("\\[QTY\\]", SharedMethods.toIntegerFoamat(transaction.getOrderQuantity()));
        try {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(transaction.getExchange());
            if (exchange.getMarketStatus() == Meta.MARKET_CLOSE) {
                template += "<BR>";
                template += Language.getString("ORDER_ACCEPTED_POPUP_2");
            }
        } catch (Exception e) {
        }
        new ShowMessage(template, "I");
//        SharedMethods.showInternalFrameMessage(template, JOptionPane.INFORMATION_MESSAGE);
        template = null;
    }

    public void showOrderAcceptedPopup(Order trsOrder) {
        int currencyDecimals = 2;
        Transaction transaction = OrderStore.getSharedInstance().searhTransactionByMubasherOrderID(trsOrder.getMubasherOrderNumber() + "");
        try {
            String currency = TradingShared.getTrader().findAccountByPortfolio(transaction.getPortfolioNo()).getCurrency();
            currencyDecimals = SharedMethods.getCurrencyDecimalPlaces(currency);
        } catch (Exception e) {
        }

        String template = Language.getString("ORDER_ACCEPTED_POPUP");
        template = template.replaceFirst("\\[ID\\]", "" + transaction.getMubasherOrderNumber());
        if (transaction.getPrice() > 0)
            template = template.replaceFirst("\\[PRICE\\]", SharedMethods.formatToDecimalPlaces(currencyDecimals, transaction.getPrice()));
        else
            template = template.replaceFirst("\\[PRICE\\]", Language.getString("NA"));
        template = template.replaceFirst("\\[QTY\\]", SharedMethods.toIntegerFoamat(transaction.getOrderQuantity()));
        try {
            Exchange exchange = ExchangeStore.getSharedInstance().getExchange(transaction.getExchange());
            if (exchange.getMarketStatus() == Meta.MARKET_CLOSE) {
                template += "<BR>";
                template += Language.getString("ORDER_ACCEPTED_POPUP_2");
            }
        } catch (Exception e) {
        }
        new ShowMessage(template, "I");
//        SharedMethods.showInternalFrameMessage(template, JOptionPane.INFORMATION_MESSAGE);
        template = null;
    }


    public void popupMenuCanceled(PopupMenuEvent e) {

    }

    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

    }

    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {

    }

    public boolean isValidForMarkForDelivery(char status) {
        if ((status == TradeMeta.T39_Filled) || (status == TradeMeta.T39_Partially_Filled)) {
            return true;
        } else {
            return false;
        }
    }

    public boolean getItemValidity(String id, String group, String exchange, char status) {
        try {
            Rule rule = RuleManager.getSharedInstance().getRule(id, exchange, group);
            if (rule == null) { // rule not found
                rule = RuleManager.getSharedInstance().getRule(id, "*", group); // check for a rulefor all exchanges
            }

            if (rule == null) { // still no rule
                return false; // make the item invalid
            } else {
                String[] statues = rule.getRule().split(",");
                for (int i = 0; i < statues.length; i++) {
                    if (status == statues[i].charAt(0)) {
                        return true;
                    }
                }
                return false;
            }
        } catch (Exception e) {
            return true; // if something goes wrong make it anable anyway
        }

    }


    public OrderSearchWindow createOrderSearchWindow() {
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("ORDER_SEARCH");
//        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("ORDER_SEARCH_COLS"));
        orderSearchWindow = new OrderSearchWindow(oSetting);
//        orderSearchWindow.setVisible(false);
        return orderSearchWindow;
    }

    public OrderSearchWindow getOrderSearchWindow() {
        if (orderSearchWindow == null) {
            createOrderSearchWindow();
        }
        return orderSearchWindow;
    }

    public void showOrderSearchWindow() {
        try {
            if (orderSearchWindow == null) {
                createOrderSearchWindow();
            }
            if (!orderSearchWindow.isVisible()) {
                if (orderSearchWindow.isIcon()) {
                    orderSearchWindow.setIcon(false);
                }
                orderSearchWindow.show();
                orderSearchWindow.setSelected(true);
            } else {
                if (orderSearchWindow.isIcon()) {
                    orderSearchWindow.setIcon(false);
                }
                orderSearchWindow.setSelected(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

//********************** Added by Shanaka - SABB CR ***************************

    public void showNewPFCreationWindow() {
        if (newPFCreationWindow == null) {
            newPFCreationWindow = new NewPFCreationWindow();
            newPFCreationWindow.setVisible(true);
        } else {
            newPFCreationWindow.setVisible(true);

            try {
                if (newPFCreationWindow.isIcon())
                    newPFCreationWindow.setIcon(false);
                newPFCreationWindow.setSelected(true);

            } catch (PropertyVetoException e) {
                e.printStackTrace();
            }
        }
    }

//********************** End - SABB CR ***************************


    public TodayTradeWindow createTodayTradeWindow() {
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("TODAY_TRADE");
//        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("ORDER_SEARCH_COLS"));
        todayTradeWindow = new TodayTradeWindow(oSetting);
//        orderSearchWindow.setVisible(false);
        return todayTradeWindow;
    }

    public TodayTradeWindow getTodayTradeWindow() {
        if (todayTradeWindow == null) {
            createTodayTradeWindow();
        }
        return todayTradeWindow;
    }

    public void showTodayTradeWindow() {
        try {
            if (todayTradeWindow == null) {
                createTodayTradeWindow();
            }
            if (!todayTradeWindow.isVisible()) {
                if (todayTradeWindow.isIcon()) {
                    todayTradeWindow.setIcon(false);
                }
                todayTradeWindow.show();
                todayTradeWindow.setSelected(true);
            } else {
                if (todayTradeWindow.isIcon()) {
                    todayTradeWindow.setIcon(false);
                }
                todayTradeWindow.setSelected(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public CashLogSearchWindow createCashLogSearchWindow() {
        ViewSetting oSetting = ViewSettingsManager.getSummaryView("CASHLOG_SEARCH");
//        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("ORDER_SEARCH_COLS"));
        cashLogSearchWindow = new CashLogSearchWindow(oSetting);
//        orderSearchWindow.setVisible(false);
        return cashLogSearchWindow;
    }

    public CashLogSearchWindow getCashLogSearchWindow() {
        if (cashLogSearchWindow == null) {
            createCashLogSearchWindow();
        }
        return cashLogSearchWindow;
    }

    public void showCashLogSearchWindow() {
        try {
            if (cashLogSearchWindow == null) {
                createCashLogSearchWindow();
            }
            if (!cashLogSearchWindow.isVisible()) {
                if (cashLogSearchWindow.isIcon()) {
                    cashLogSearchWindow.setIcon(false);
                }
                cashLogSearchWindow.show();
                cashLogSearchWindow.setSelected(true);
            } else {
                if (cashLogSearchWindow.isIcon()) {
                    cashLogSearchWindow.setIcon(false);
                }
                cashLogSearchWindow.setSelected(true);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public static String getConfirmMessage(String symbol,String exchange, double price, int decimalCount, double stopLoss, double takeProfit, long quantity,
                                           String field, String operator, double value,
                                           int type, int mode, int side, int conditionType) {
        String msg = "";
        String stopLossMsg = null;
        String conditionMsg = null;

        switch (type) {
            case TradeMeta.ORDER_TYPE_LIMIT:
                if ((side == TradeMeta.SELL) && ((stopLoss > 0) || (takeProfit > 0))){
                    msg = Language.getString("MSG_COMFIRM_LIMIT_ORDER_NOPRICE");
                } else {
                    msg = Language.getString("MSG_COMFIRM_LIMIT_ORDER");
                    msg = msg.replaceFirst("\\[PRICE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, price));
                }
                break;
            case TradeMeta.ORDER_TYPE_MARKET:
                msg = Language.getString("MSG_COMFIRM_MARKET_ORDER");
                break;
             *//*case TradeMeta.ORDER_TYPE_CONDITIONAL:
                msg = Language.getString("MSG_COMFIRM_CONDITIONAL_LIMIT_ORDER");
                break;*//*
            case TradeMeta.ORDER_TYPE_STOP_MARKET:
                msg = Language.getString("MSG_COMFIRM_STOP_ORDER");
                break;
            case TradeMeta.ORDER_TYPE_STOP_LIMIT:
                msg = Language.getString("MSG_COMFIRM_STOP_LIMIT_ORDER");
                msg = msg.replaceFirst("\\[PRICE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, price));
                break;
            case TradeMeta.ORDER_TYPE_DAY_MARKET:
                msg = Language.getString("MSG_COMFIRM_STOP_ORDER");
                break;
            case TradeMeta.ORDER_TYPE_DAY_LIMIT:
                msg = Language.getString("MSG_COMFIRM_STOP_LIMIT_ORDER");
                msg = msg.replaceFirst("\\[PRICE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, price));
                break;
            case TradeMeta.ORDER_TYPE_MARKET_ON_CLOSE:
                msg = Language.getString("MSG_COMFIRM_STOP_ORDER");
                break;
            case TradeMeta.ORDER_TYPE_LIMIT_ON_CLOSE:
                msg = Language.getString("MSG_COMFIRM_STOP_LIMIT_ORDER");
                msg = msg.replaceFirst("\\[PRICE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, price));
                break;
            case TradeMeta.ORDER_TYPE_TRAILING_STOP_MARKET:
                msg = Language.getString("MSG_COMFIRM_STOP_ORDER");
                break;
            case TradeMeta.ORDER_TYPE_TRAILING_STOP_LIMIT:
                msg = Language.getString("MSG_COMFIRM_STOP_LIMIT_ORDER");
                msg = msg.replaceFirst("\\[PRICE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, price));
                break;
            case TradeMeta.ORDER_TYPE_SQUARE_OFF:
                msg = Language.getString("MSG_COMFIRM_STOP_ORDER");
                break;


        }

        *//*if (conditionType == TradeMeta.CONDITION_TYPE_NONE){
        } else {
            conditionMsg = Language.getString("MSG_COMFIRM_ORDER_CONDITION");
        }*//*

        if (conditionType == TradeMeta.CONDITION_TYPE_NORMAL){
            conditionMsg = Language.getString("MSG_COMFIRM_ORDER_CONDITION");
        }

        if ((stopLoss > 0) && (takeProfit > 0)){
            stopLossMsg = Language.getString("MSG_COMFIRM_ORDER_WITH_TP_SL");
        } else if (stopLoss > 0) {
            stopLossMsg = Language.getString("MSG_COMFIRM_ORDER_WITH_SL");
        } else if (takeProfit > 0) {
            stopLossMsg = Language.getString("MSG_COMFIRM_ORDER_WITH_TP");
        } else {
            stopLossMsg = null;
        }

        msg = msg.replaceFirst("\\[TYPE\\]", TradingShared.getActionString(mode));
        String str = DataStore.getSharedInstance().getStockObject(exchange,symbol).getShortDescription() +" - "+ symbol;
        msg = msg.replaceFirst("\\[SYMBOL\\]", str);
        str = null;
//        msg = msg.replaceFirst("\\[SYMBOL\\]", symbol);
//        if (type == TradeMeta.ORDER_TYPE_LIMIT) {
//            msg = msg.replaceFirst("\\[PRICE\\]", SharedMethods.toDeciamlFoamat(price));
//        }
        msg = msg.replaceFirst("\\[QTY\\]", SharedMethods.toIntegerFoamat(quantity));
        if ((mode == TradeMeta.AMEND) || (mode == TradeMeta.CANCEL)) {
            msg += Language.getString("MSG_PRIOR_ORDER_WARNING");
        }

        if (stopLossMsg != null){
            stopLossMsg = stopLossMsg.replaceFirst("\\[SPRICE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, stopLoss));
            stopLossMsg = stopLossMsg.replaceFirst("\\[TPRICE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, takeProfit));
        }


        if ((mode != TradeMeta.CANCEL) && (conditionMsg != null)){
            conditionMsg = conditionMsg.replaceFirst("\\[FIELD\\]", field);
            conditionMsg = conditionMsg.replaceFirst("\\[OPERATOR\\]", operator);
            conditionMsg = conditionMsg.replaceFirst("\\[CPRICE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, value));
        }

        if (stopLossMsg != null){
            msg = msg.replaceFirst("\\[STOPLOSS_MSG\\]", "<BR>" + stopLossMsg);
        }else {
            msg = msg.replaceFirst("\\[STOPLOSS_MSG\\]", "");
        }

        if ((mode != TradeMeta.CANCEL) && (conditionMsg != null)){
            msg = msg.replaceFirst("\\[CONDITION_MSG\\]",  "<BR>" + conditionMsg);
        }else {
            msg = msg.replaceFirst("\\[CONDITION_MSG\\]", "");
        }

        if (mode == TradeMeta.CANCEL){
            msg = msg.replaceFirst("\\[AGREEMENT_MSG\\]", "");
        }else {
            if ((conditionType == TradeMeta.CONDITION_TYPE_NONE) && (stopLossMsg == null)){
                msg = msg.replaceFirst("\\[AGREEMENT_MSG\\]", "");
            } else {
                msg = msg.replaceFirst("\\[AGREEMENT_MSG\\]",  "<BR>" + Language.getString("MSG_CONFIRM_ORDER_AGREEMENT"));
            }
        }
        return msg;
    }*/

    private static String loadTemplate(String language, short symbolType, int condtionalmode) {
        StringBuffer buffer = new StringBuffer();
        byte[] temp = new byte[1000];
        int count = 0;
        String data = "";

        try {
            InputStream in;

            if (symbolType == Meta.SYMBOL_TYPE_EQUITY && condtionalmode == TradeMeta.CONDITION_TYPE_NORMAL) {

                in = new FileInputStream("./Templates/Order_Confirmation_Cond_Equity_" + language + ".html");
            } else if (symbolType == Meta.SYMBOL_TYPE_EQUITY) {
                in = new FileInputStream("./Templates/Order_Confirmation_Equity_" + language + ".html");
            } else {
                in = new FileInputStream("./Templates/Order_Confirmation_Normal_" + language + ".html");
            }
            while (true) {
                count = in.read(temp);
                if (count == -1) break;
                if (count > 0) {
                    String str = new String(temp, 0, count);
                    buffer.append(str);
                    str = null;
                }
            }
            in.close();
            data = buffer.toString();
            data = data.replaceFirst("\\[PAGE_BG_COLOR\\]", getHexColor("ORDER_CONFIRMATION_PAGE_BG_COLOR"));
            data = data.replaceFirst("\\[MAIN_FONT_COLOR\\]", getHexColor("ORDER_CONFIRMATION_MAIN_FONT_COLOR"));
            data = data.replaceFirst("\\[NORMAL_FONT_COLOR\\]", getHexColor("ORDER_CONFIRMATION_NORMAL_FONT_COLOR"));
            data = data.replaceFirst("\\[NORMAL_LEFT_FONT_COLOR\\]", getHexColor("ORDER_CONFIRMATION_NORMAL_LEFT_FONT_COLOR"));
            data = data.replaceFirst("\\[BOLD_FONT_COLOR\\]", getHexColor("ORDER_CONFIRMATION_BOLD_FONT_COLOR"));
            data = data.replaceFirst("\\[MIDLINE_COLOR\\]", getHexColor("ORDER_CONFIRMATION_MIDLINE_COLOR"));
            data = data.replaceFirst("\\[HORIZON_LINE_COLOR\\]", getHexColor("ORDER_CONFIRMATION_HORIZON_LINE_COLOR"));
            data = data.replaceFirst("\\[SEPARATION_LINE_COLOR\\]", getHexColor("ORDER_CONFIRMATION_SEPARATION_LINE_COLOR"));
            data = data.replaceFirst("\\[TABLE_BORDER_COLOR\\]", getHexColor("ORDER_CONFIRMATION_TABLE_BORDER_COLOR"));
            data = data.replaceAll("\\[PATH\\]", (System.getProperties().get("user.dir") + "/Templates/").replaceAll("\\\\", "/"));
            return data;
        }
        catch (Exception ex) {
            return "";
        }
    }

    private static void loadDynamicTemplate(String language, Browser browser) {
        if (browser != null) {
            byte[] temp = new byte[1000];
            int count = 0;
            String data = "";
            StringBuffer buffer = new StringBuffer();
            try {
                /*InputStream in;



                    in = new FileInputStream("./Templates/OrderConfirmation_" + language + ".html");
                while (true) {
                    count = in.read(temp);
                    if (count == -1) break;
                    if (count > 0) {
                        String str = new String(temp, 0, count);
                        buffer.append(str);
                        str = null;
                    }
                }
                in.close();
                data = buffer.toString();
                browser.setContent(data);*/
                File file = new File("./Templates/OrderConfirmation_" + language + ".html");
                browser.navigate(file.getAbsolutePath());
            } catch (Exception ex) {
                ex.printStackTrace();
                ;
            }
        }
    }

    public static String getHexColor(String themeTag) {
        try {
            Color color = Theme.getColor(themeTag);
            String value = Integer.toHexString(color.getRed());
            if (value.length() == 1) {
                value = "0" + value;
            }
            String data = value;
            value = Integer.toHexString(color.getGreen());
            if (value.length() == 1) {
                value = "0" + value;
            }
            data += value;
            value = Integer.toHexString(color.getBlue());
            if (value.length() == 1) {
                value = "0" + value;
            }
            data += value;
            return data;
        } catch (Exception e) {
            return "000000";
        }
    }

    public static void setDataForConfirmMsg(String symbol, String exchange, int instrument, double price, int decimalCount, double stopLoss,
                                            double takeProfit, long quantity, String conditionField, String conditionOperator, double conditionPrice,
                                            int type, int mode, int conditionType, String stopLossType, double trailsStopPrice, boolean isDayOrder,
                                            String tifStr, String portfolio, String amount, String commission, String total, String initMargin,
                                            String maintMargin, String equityLoan, ConditionalBehaviour behavourRule, boolean isMargin, String vat) {
        String msg = "";
        String stopLossMsg = "";
        String conditionMsg = null;
        Stock stock;
        boolean isPriceVisible = false;         //    : corresponds to the visibility of Price
        boolean isStopLossVisible = false;       //: corresponds to the visibility of Stop loss
        boolean isTakeProfitVisible = false;    //: corresponds to the visibility of Take Profit
        boolean isConditionVisible = false;    // : corresponds to the visibility of condition
        boolean isWarningVisible = false;     //  : corresponds to the visibility of warning message

        boolean needToAmendMsg = true;
        switch (SharedMethods.getSymbolType(instrument)) {
            case Meta.SYMBOL_TYPE_OPTIONS:
                msg = loadTemplate(Language.getSelectedLanguage(), Meta.SYMBOL_TYPE_OPTIONS, conditionType);
                needToAmendMsg = false;
                break;
            case Meta.SYMBOL_TYPE_EQUITY:
                needToAmendMsg = false;
                break;
            case Meta.SYMBOL_TYPE_BONDS:
                needToAmendMsg = false;
                break;
            default:
                switch (type) {
                    case TradeMeta.ORDER_TYPE_LIMIT:
                        msg = Language.getString("MSG_CONFIRM_LIMIT_ORDER");
                        msg = msg.replaceFirst("\\[PRICE\\]", "" + price);
                        msg = msg.replaceFirst("\\[COMMISSION\\]", "" + commission);
                        msg = msg.replaceFirst("\\[VAT\\]", "" + vat);
                        msg = msg.replaceFirst("\\[NETVALUE\\]", "" + total);
                        break;
                    case TradeMeta.ORDER_TYPE_MARKET:
                        msg = Language.getString("MSG_CONFIRM_MARKET_ORDER");
                        msg = msg.replaceFirst("\\[COMMISSION\\]", "" + commission);
                        msg = msg.replaceFirst("\\[VAT\\]", "" + vat);
                        msg = msg.replaceFirst("\\[NETVALUE\\]", "" + total);
                        break;
                    case TradeMeta.ORDER_TYPE_STOPLOSS_MARKET:
                        msg = Language.getString("MSG_CONFIRM_MARKET_ORDER");
                        break;
                    case TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT:
                        msg = Language.getString("MSG_CONFIRM_LIMIT_ORDER");
                        msg = msg.replaceFirst("\\[PRICE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, price));
                        break;
                    case TradeMeta.ORDER_TYPE_MARKET_ON_CLOSE:
                        msg = Language.getString("MSG_CONFIRM_MARKET_ORDER");
                        break;
                    case TradeMeta.ORDER_TYPE_LIMIT_ON_CLOSE:
                        msg = Language.getString("MSG_CONFIRM_LIMIT_ORDER");
                        msg = msg.replaceFirst("\\[PRICE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, price));
                        break;
                    case TradeMeta.ORDER_TYPE_TRAILING_STOP_MARKET:
                        msg = Language.getString("MSG_CONFIRM_MARKET_ORDER");
                        break;
                    case TradeMeta.ORDER_TYPE_TRAILING_STOP_LIMIT:
                        msg = Language.getString("MSG_CONFIRM_LIMIT_ORDER");
                        msg = msg.replaceFirst("\\[PRICE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, price));
                        break;
                    case TradeMeta.ORDER_TYPE_SQUARE_OFF:
                        msg = Language.getString("MSG_CONFIRM_MARKET_ORDER");
                        break;
                }
                break;
        }
        stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);

        if (SharedMethods.getSymbolType(instrument) == Meta.SYMBOL_TYPE_EQUITY || SharedMethods.getSymbolType(instrument) == Meta.SYMBOL_TYPE_BONDS) { //todo:veryfy needed fields
            String pricestr;
            if (price > 0) {
                pricestr = price + "";
                isPriceVisible = true;
            } else {
                pricestr = TradingShared.NA;
                isPriceVisible = false;
            }
            String stopLossString = "";
            if (stopLoss > 0) {
                stopLossString = stopLossType + " " + stopLoss;
                isStopLossVisible = true;
            }
            String takeprofitString = "";
            if (takeProfit > 0) {
                takeprofitString = takeProfit + "";
                isTakeProfitVisible = true;
            }

            String condtionMsg = "";
            String warningMsg = "";
            if (conditionType == TradeMeta.CONDITION_TYPE_NORMAL && behavourRule != null && behavourRule.getConditions() != null && behavourRule.getConditions().size() > 0) {
                condtionMsg = Language.getString("MSG_COMFIRM_ORDER_CONDITION");
                Condition condtion = behavourRule.getConditions().get(0);
                String operator = getCondtionalOperatorString(condtion.getOperator());
                String parameter = getCondtionlaParamString(condtion.getConditionParameter());
                String value = condtion.getConditionValue();
                condtionMsg = condtionMsg.replaceFirst("\\[FIELD\\]", parameter);
                condtionMsg = condtionMsg.replaceFirst("\\[OPERATOR\\]", operator);
                condtionMsg = condtionMsg.replaceFirst("\\[CPRICE\\]", value);
                warningMsg = Language.getString("MSG_CONFIRM_ORDER_AGREEMENT");
                isConditionVisible = true;
                isWarningVisible = true;
            } else if (isMargin) {
                warningMsg = Language.getString("INSUFFICIENT_BUYING_POWER_POPUP");
                isWarningVisible = true;
            }

            String scriptString = "showOrderConfirmation(" + "'" + TradingShared.getActionString(mode) + "'" + "," +
                    "'" + stock.getShortDescription() + "'" + "," +
                    "'" + symbol + "'" + "," +
                    "'" + TradingShared.getTypeString(type) + "'" + "," +
                    "'" + pricestr + "'" + "," +
                    "'" + SharedMethods.toIntegerFoamat(quantity) + "'" + "," +
                    "'" + commission + "'" + "," +
                    "'" + vat + "'" + "," +
                    "'" + total + "'" + "," +
                    "'" + stopLossString + "'" + "," +
                    "'" + takeprofitString + "'" + "," +
                    "'" + condtionMsg + "'" + "," +
                    "'" + warningMsg + "'" + "," +
                    isPriceVisible + "," +
                    isStopLossVisible + "," +
                    isTakeProfitVisible + "," +
                    isConditionVisible + "," +
                    isWarningVisible + ")";

            browser.setSilent(true);
            browser.executeScript(scriptString);


        } else {
            msg = msg.replaceFirst("\\[SIDE\\]", TradingShared.getActionString(mode));
            msg = msg.replaceFirst("\\[SYMBOL\\]", symbol);
            if (stock.getBaseSymbol() != null) {
                msg = msg.replaceFirst("\\[BASE_SYMBOL\\]", stock.getBaseSymbol());
            } else {
                msg = msg.replaceFirst("\\[BASE_SYMBOL\\]", TradingShared.NA);
            }
            msg = msg.replaceFirst("\\[DESCRIPTION\\]", stock.getLongDescription());
            msg = msg.replaceFirst("\\[SHORT_DESCRIPTION\\]", stock.getShortDescription());
            msg = msg.replaceFirst("\\[EXCHANGE\\]", exchange);
            msg = msg.replaceFirst("\\[PORTFOLIO\\]", portfolio);
            msg = msg.replaceFirst("\\[TIF\\]", tifStr);
            if (price > 0) {
                msg = msg.replaceFirst("\\[PRICE\\]", "" + SharedMethods.formatToDecimalPlaces(decimalCount, price));
            } else {
                msg = msg.replaceFirst("\\[PRICE\\]", "" + TradingShared.NA);
            }
            double bidPrice = stock.getBestBidPrice();
            if (bidPrice > 0) {
                msg = msg.replaceFirst("\\[BID_PRICE\\]", "" + SharedMethods.formatToDecimalPlaces(decimalCount, bidPrice));
            } else {
                msg = msg.replaceFirst("\\[BID_PRICE\\]", TradingShared.NA);
            }
            double mktPrice = stock.getLastTradeValue();
            if (mktPrice > 0) {
                msg = msg.replaceFirst("\\[LAST_PRICE\\]", "" + SharedMethods.formatToDecimalPlaces(decimalCount, mktPrice));
            } else {
                msg = msg.replaceFirst("\\[LAST_PRICE\\]", TradingShared.NA);
            }
            double askPrice = stock.getBestAskPrice();
            if (askPrice > 0) {
                msg = msg.replaceFirst("\\[ASK_PRICE\\]", "" + SharedMethods.formatToDecimalPlaces(decimalCount, askPrice));
            } else {
                msg = msg.replaceFirst("\\[ASK_PRICE\\]", TradingShared.NA);
            }
            if (amount != null) {
                msg = msg.replaceFirst("\\[ORDER_VALUE\\]", amount);
            } else {
                msg = msg.replaceFirst("\\[ORDER_VALUE\\]", TradingShared.NA);
            }
            if (initMargin != null) {
                msg = msg.replaceFirst("\\[INITIAL_MARGIN\\]", initMargin);
            } else {
                msg = msg.replaceFirst("\\[INITIAL_MARGIN\\]", TradingShared.NA);
            }
            if (commission != null) {
                msg = msg.replaceFirst("\\[COMMISSION\\]", commission);
            } else {
                msg = msg.replaceFirst("\\[COMMISSION\\]", TradingShared.NA);
            }
            if (vat != null) {
                msg = msg.replaceFirst("\\[VAT\\]", vat);
            } else {
                msg = msg.replaceFirst("\\[VAT\\]", TradingShared.NA);
            }
            if (maintMargin != null) {
                msg = msg.replaceFirst("\\[MT_MARGIN\\]", maintMargin);
            } else {
                msg = msg.replaceFirst("\\[MT_MARGIN\\]", TradingShared.NA);
            }
            msg = msg.replaceFirst("\\[NET_VALUE\\]", total);
            if (equityLoan != null) {
                msg = msg.replaceFirst("\\[EQUITY_WITH_LOAN\\]", equityLoan);
            } else {
                msg = msg.replaceFirst("\\[EQUITY_WITH_LOAN\\]", TradingShared.NA);
            }
            msg = msg.replaceFirst("\\[ORDER_TYPE\\]", TradingShared.getTypeString(type));
            msg = msg.replaceFirst("\\[QUANTITY\\]", SharedMethods.toIntegerFoamat(quantity));
        }

        if (needToAmendMsg) {
            if (conditionType == TradeMeta.CONDITION_TYPE_NORMAL) {
                conditionMsg = Language.getString("MSG_COMFIRM_ORDER_CONDITION");
            }

            if ((stopLoss > 0)) {
                stopLossMsg = Language.getString("MSG_CONFIRM_ORDER_STOPLOSS");
                stopLossMsg = stopLossMsg.replaceFirst("\\[STOP_TYPE\\]", stopLossType);
                stopLossMsg = stopLossMsg.replaceFirst("\\[STOP_VALUE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, stopLoss));
                stopLossMsg = stopLossMsg.replaceFirst("\\[TRAIL_VALUE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, trailsStopPrice));
            }
            if (takeProfit > 0) {
                stopLossMsg = stopLossMsg + Language.getString("MSG_CONFIRM_ORDER_TAKEPROFIT");
                stopLossMsg = stopLossMsg.replaceFirst("\\[TAKE_PROFIT_VALUE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, takeProfit));
            }
            if (!stopLossMsg.equals("")) {
                msg = msg + stopLossMsg;
            }
            msg = msg + Language.getString("MSG_CONFIRM_ORDER_END");

            if ((mode == TradeMeta.AMEND) || (mode == TradeMeta.CANCEL)) {
                msg = msg + Language.getString("MSG_PRIOR_ORDER_WARNING");
            }
            if (isDayOrder) {
                msg = msg + Language.getString("MSG_DAY_ORDER_WARNING");
            }

            if ((mode != TradeMeta.CANCEL) && (conditionMsg != null)) {
                conditionMsg = conditionMsg.replaceFirst("\\[FIELD\\]", conditionField);
                conditionMsg = conditionMsg.replaceFirst("\\[OPERATOR\\]", conditionOperator);
                conditionMsg = conditionMsg.replaceFirst("\\[CPRICE\\]", SharedMethods.formatToDecimalPlaces(decimalCount, conditionPrice));
            }
            if ((mode != TradeMeta.CANCEL) && (conditionMsg != null)) {
                msg = msg + conditionMsg;
            }
            if (mode != TradeMeta.CANCEL) {
                if ((conditionType != TradeMeta.CONDITION_TYPE_NONE)) {
                    msg = msg + "<BR>" + Language.getString("MSG_CONFIRM_ORDER_AGREEMENT");
                }
            }
        }
        if (SharedMethods.getSymbolType(instrument) != Meta.SYMBOL_TYPE_EQUITY && SharedMethods.getSymbolType(instrument) != Meta.SYMBOL_TYPE_BONDS) {
            browser.setContent(msg);
        }
    }


    public static int getNewOrderConfirmMsg(String symbol, String exchange, int instrument, double price, int decimalCount, double stopLoss,
                                            double takeProfit, long quantity, String conditionField, String conditionOperator, double conditionPrice,
                                            int type, int mode, int side, int conditionType, String stopLossType, double trailsStopPrice, boolean isDayOrder,
                                            String tifStr, String portfolio, String amount, String commission, String total, String initMargin,
                                            String maintMargin, String equityLoan, int disclose, ConditionalBehaviour behavourRule, boolean isMargin, String vat) {
        browser = new Browser();
        loadDynamicTemplate(Language.getSelectedLanguage(), browser);
        setDataForConfirmMsg(symbol, exchange, instrument, price, decimalCount, stopLoss, takeProfit, quantity, conditionField, conditionOperator, conditionPrice,
                type, mode, conditionType, stopLossType, trailsStopPrice, isDayOrder, tifStr, portfolio, amount, commission,
                total, initMargin, maintMargin, equityLoan, behavourRule, isMargin, vat);
        Dimension dim = null;
        int width;
        if (SharedMethods.getSymbolType(instrument) == Meta.SYMBOL_TYPE_EQUITY || SharedMethods.getSymbolType(instrument) == Meta.SYMBOL_TYPE_BONDS) {
            dim = browser.getDocument().getBody().getSize();
            try {
                width = Integer.parseInt(browser.getDocument().getElementById("maintbl").getAttribute("width"));
            } catch (Exception e) {
                width = 475;
            }
            browser.setPreferredSize(new Dimension(width, dim.height)); //todo : check for non equity -shanika
        } else {
            dim = browser.getDocument().getBody().getSize();
            browser.setPreferredSize(new Dimension(dim.width + 25, dim.height)); //todo : check for non equity -shanika
        }

        return SharedMethods.showOrderConfirmationMessage(new Object[]{browser}, 1);
    }

    private static String getCondtionalOperatorString(String operator) {
        int opr = Integer.parseInt(operator);
        switch (opr) {
            case TradeMeta.CONDITION_OPERATOR_GREATER_THAN_OR_EQ:
                return Language.getString("GREATER_THAN_OR_EQUAL");
            case TradeMeta.CONDITION_OPERATOR_LESS_THAN_OR_EQ:
                return Language.getString("LESS_THAN_OR_EQUAL");
            case TradeMeta.CONDITION_OPERATOR_EQUAL:
                return Language.getString("EQUAL");
            default:
                return "";
        }

    }

    private static String getCondtionlaParamString(String param) {
        int parm = Integer.parseInt(param);
        switch (parm) {
            case TradeMeta.CONDITION_TRIGGER_LAST:
                return Language.getString("CONDITION_LAST");
            case TradeMeta.CONDITION_TRIGGER_BID:
                return Language.getString("CONDITION_BID");
            case TradeMeta.CONDITION_TRIGGER_ASK:
                return Language.getString("CONDITION_OFFER");
            case TradeMeta.CONDITION_TRIGGER_MIN:
                return Language.getString("CONDITION_MIN");
            case TradeMeta.CONDITION_TRIGGER_MAX:
                return Language.getString("CONDITION_MAX");
            default:
                return Language.getString("SLICE_TYPE_NONE");
        }
    }

    public void doSearch(String portfolio, String symbol, Date from, Date to) {
        orderSearchWindow.doSearch(portfolio, symbol, from, to);
    }

    public TransactionDialog showBuyScreen(String symbol) {
        if (TradingShared.isReadyForTrading()) {
            return Client.getInstance().doTransaction(TradeMeta.BUY, symbol, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            return null;
        }
    }

    public TransactionDialog showBuyScreen(String symbol, long qty) {
        if (TradingShared.isReadyForTrading()) {
            return Client.getInstance().doTransaction(TradeMeta.BUY, symbol, qty, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            return null;
        }
    }

    public TransactionDialog showSellScreen(String symbol) {
        if (TradingShared.isReadyForTrading()) {
            return Client.getInstance().doTransaction(TradeMeta.SELL, symbol, 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            return null;
        }
    }

    public TransactionDialog showSellScreen(String symbol, long qty) {
        if (TradingShared.isReadyForTrading()) {
            return Client.getInstance().doTransaction(TradeMeta.SELL, symbol, qty, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            return null;
        }
    }
    /*public byte changeInitialUSBPW() {
        try {
            if (TWSecurityManager.getSharedInstance().isUSBConected()) { // check if the usb device is connected
                boolean sucess = TWSecurityManager.getSharedInstance().matchPWWithUSB("00000000");
                if (sucess) { // still initial password. Must change it
                    ChangePass changePass = new ChangePass(Client.getInstance().getFrame(),
                            Language.getString("CHANGE_USB_PIN"), null, ChangePass.Modes.MODE_NO_OLD_PW);
                    changePass.showDialog();
                    String newPassword = changePass.getNewPassword();
                    changePass.dispose();
                    changePass = null;
                    if (newPassword != null) {
                        TWSecurityManager.getSharedInstance().changePIN("1234", newPassword);
                        return PIN_CHANGE_SUCESS;
                    } else {
                        return PIN_CHANGE_FAILED;
                    }
                } else {
                    return PIN_CHANGE_SUCESS; // no need to change
                }
            } else {
                showNoUSBDeviceMessage();
                return PIN_CHANGE_ERROR;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return PIN_CHANGE_FAILED;
        }
    }*/

    public int changeUSBPW(boolean initialChange) {
        try {
            if (TWSecurityManager.isUSBConected()) { // check if the usb device is connected
                String messag = null;
                String oldPassword;
                ChangePass.Modes mode;
//                if (initialChange) {
//                    mode = ChangePass.Modes.MODE_NO_OLD_PW;
//                    messag = Language.getString("MSG_FIRST_TIME_USB");
//                } else {
                mode = ChangePass.Modes.MODE_INCLUDE_OLD_PW;
//                }
                ChangePass changePass = new ChangePass(Client.getInstance().getFrame(),
                        Language.getString("CHANGE_USB_PIN"), messag, mode);
                changePass.showDialog();
                String newPassword = changePass.getNewPassword();
                //if (initialChange) {
                //    oldPassword = TradingShared.INITAL_USB_PW;
                //} else {
                oldPassword = changePass.getOldPassword();
                //}
                changePass.dispose();
                changePass = null;
                TradingShared.setUSBPin(null);
                TradingShared.setLevel2Password(null);
                if (newPassword != null) {
                    TWSecurityManager.changePIN(oldPassword, newPassword);
                    TradingShared.setUSBPin(SharedMethods.encrypt(newPassword));
                    return 1;
                } else {
                    return 0;
                }
            } else {
                showNoUSBDeviceMessage();
                return -1;
            }
        } catch (Exception e) {
            e.printStackTrace();
            SharedMethods.showBlockingMessage(Language.getString("USB_PIN_CHANGE_FAILED"), JOptionPane.ERROR_MESSAGE);
            return -1;
        }
    }

//    public boolean sendNewOrder(Transaction transaction, boolean mustAuthenticte) {
//        long expireDate;
//        try {
//            expireDate = TradingShared.getGoodTillLong(transaction.getExpireDate());
//        } catch (Exception e) {
//            expireDate = -1;
//        }
//        return sendNewOrder(mustAuthenticte, false, 0, transaction.getPortfolioNo(), transaction.getExchange(), transaction.getSymbol(),
//                transaction.getSide(), transaction.getType(), transaction.getPrice(), transaction.getMinQuantity(),
//                transaction.getCurrency(), (int) transaction.getOrderQuantity(), (short) transaction.getTIFType(),
//                expireDate, transaction.getDiscloseQuantity(), -1, false, transaction.getStrikePrice(),
//                transaction.getMaturityMonthYear(), transaction.getSecurityType(), transaction.getStopPrice(),
//                transaction.getBaseSymbol(), transaction.getRuleStatus(), transaction.getRule(), transaction.getRuleStartTime() , transaction.getRuleExpireTime());
//    }
//
//    public boolean sendNewOrder(boolean mustAuthenitate, boolean queue, long qID, String portfolio, String exchange, String symbol, int side, char type,
//                                float price, double maxPrice, String currency, int quantity, short tiff, long goodTillLong,
//                                int disclosed, int minfill, boolean allOrNone, double strikePrice, String maturityMMYY,
//                                int securityType, float stopprice, String baseSymbol, int ruleStatus, String rule, long ruleStartDate, long ruleExpiryDate) {
//        try {
//            boolean authenticated = false;
//
//            if (!Settings.isConnected()){
//                SharedMethods.showMessage(Language.getString("MSG_PRICE_DISCONNECTION_WARNING"), JOptionPane.WARNING_MESSAGE);
//            }
//
//            if (mustAuthenitate) {
//                if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
//                    TradeMethods.getSharedInstance().blockTillValidated();
//                    if (TradingShared.level2AuthenticationSucess) {
//                        authenticated = true;
//                    }
//                }
//            } else {
//                authenticated = true;
//            }
//            if (authenticated) {
//                TradeMessage tradeMessage;
//                if (rule == null){
//                    tradeMessage = new TradeMessage(MT_ORDER);
//                }else {
//                    tradeMessage = new TradeMessage(MT_CONDITIONAL_ORDER);
//                }
//
//                TRSOrder order = new TRSOrder();
//                order.setTwOrderID(TradingShared.getCurrentDay() + TradingShared.getNextMessageID());
//                order.setPortfolioNo(portfolio);
//                order.setSecurityExchange(exchange);
//                order.setSymbol(symbol);
//                order.setSecurityType(securityType);
//                order.setBrokerID(TradingShared.getTrader().getPortfolio(order.getPortfolioNo()).getBrokerID());
//
//                order.setSide(side);
//                order.setOrdType(type);
//                order.setPrice(price);
//                order.setStopPrice(stopprice);
//                order.setMaxPrice(maxPrice);
//                order.setCurrency(currency);
//                order.setOrderQty(quantity);
//                if ((baseSymbol != null) && !(baseSymbol.equals(""))) {
//                    order.setBaseSymbol(baseSymbol);
//                }
//                if (TradingShared.isTiffDateValid(tiff)) {
//                    order.setExpireTime(TradingShared.formatExpireDate(goodTillLong));
//                }
//                order.setTimeInForce(tiff);
//                order.setUserID(TradingShared.getTrader().getUserID());
//                if (disclosed >= 0) {
//                    order.setMaxFloor(disclosed);
//                }
//                order.setMinQty(minfill);
//                if (allOrNone) {
//                    order.setMinQty(order.getOrderQty());
//                }
//
//                order.setStrikePrice(strikePrice);
//                order.setMaturityMonthYear(maturityMMYY);
//
//                order.setPassword(TradingShared.getLevel2Password());
//                order.setCertificate(TradingShared.getLevel2Password());
//
//                if (rule != null){
//                    order.setConditionStatus(ruleStatus);
//                    order.setCondition(rule);
//                    order.setConditionExpireTime(TradingShared.formatExpireDate(ruleExpiryDate));
//                    order.setConditionStartTime(TradingShared.formatExpireDate(ruleStartDate));
//                }
//
//                if (TradeMethods.isSignatureRequired()) {
//                    tradeMessage.setSignature(TWSecurityManager.sign(order.getMubasherMessage(), SharedMethods.decrypt(TradingShared.getUSBPin())));
//                }
//                tradeMessage.addData(order.getMubasherMessage());
//                if (queue) {
//                    QueuedTransaction queuedTransaction = new QueuedTransaction(qID);
//                    queuedTransaction.setOrderData(order);
//                    queuedTransaction.setOrderDate(System.currentTimeMillis());
//                    OrderQueue.getSharedInstance().addTransaction(queuedTransaction);
//                } else {
//                    SendQueue.getSharedInstance().addData(tradeMessage.toString());
//                }
//                return true;
//            }
//
//            return false;
//        } catch (Exception e) {
//            e.printStackTrace();
//            return false;
//        }
//
//    }

    public boolean sendNewOrder(Transaction transaction, boolean mustAuthenticte) {
        long expireDate;
        try {
            expireDate = TradingShared.getGoodTillLong(transaction.getExpireDate());
        } catch (Exception e) {
            expireDate = -1;
        }
        boolean isSlieOrder = false;
        if ((transaction.getSliceExecType() == TradeMeta.SLICE_ORD_TYPE_TIME_INTERVAL) ||
                (transaction.getSliceExecType() == TradeMeta.SLICE_ORD_TYPE_ICEBURG) ||
                (transaction.getSliceExecType() == TradeMeta.SLICE_ORD_TYPE_ALL)) {
            isSlieOrder = true;
        }

        return sendNewOrderForExec(mustAuthenticte, false, 0, transaction.getPortfolioNo(), transaction.getExchange(), transaction.getSymbolWithMarketCode(),
                transaction.getSide(), transaction.getType(), transaction.getPrice(), transaction.getMinQuantity(),
                transaction.getCurrency(), (int) transaction.getOrderQuantity(), (short) transaction.getTIFType(),
                expireDate, transaction.getDiscloseQuantity(), 0, false, transaction.getStrikePrice(),
                transaction.getMaturityMonthYear(), transaction.getSecurityType(), transaction.getStopLossPrice(),
                transaction.getBaseSymbol(), transaction.getRuleStatus(), transaction.getRule(), transaction.getRuleStartTime(),
                transaction.getRuleExpireTime(), transaction.getTakeProfitPrice(), isSlieOrder, transaction.getSliceExecType(), transaction.getSliceTimeInterval(),
                transaction.getSliceBlockSIze(), transaction.getStopLossType(), false, transaction.getTrailStopLossPrice(), false, transaction.getTwOrderID(), null, transaction.getBookKeeper(), 0, transaction.getBracketCondition(),0);
    }

    public boolean sendNewOrder(boolean mustAuthenitate, boolean queue, long qID, String portfolio, String exchange,
                                String symbol, int side, char type, double price, double maxPrice, String currency,
                                long quantity, int tiff, long goodTillLong, int disclosed, int minfill,
                                boolean allOrNone, double strikePrice, String maturityMMYY, int securityType,
                                double stopprice, String baseSymbol, int ruleStatus, String rule, String ruleStartDate,
                                String ruleExpiryDate, double takeProfit, String quoteID, String bestQuoteID, String brokerID, double bestBuySell, String bookKeepr, int tPlus , int rightsAwarenessStatus) {
        try {

            boolean authenticated = false;

            if (!Settings.isConnected()) {
                SharedMethods.showMessage(Language.getString("MSG_PRICE_DISCONNECTION_WARNING"), JOptionPane.WARNING_MESSAGE);
            }

            if (mustAuthenitate) {
                if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
                    TradeMethods.getSharedInstance().blockTillValidated();
                    if (TradingShared.level2AuthenticationSucess) {
                        authenticated = true;
                    }
                }
            } else {
                authenticated = true;
            }
            if (authenticated) {
                TradeMessage tradeMessage;
                MIXObject mixtadeMeaasge;
                if (rule != null) {
                    tradeMessage = new TradeMessage(MT_CONDITIONAL_ORDER);
                    mixtadeMeaasge = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_NEW_ORDER, TradingShared.getTrader().getPath(portfolio));
                } else {
                    tradeMessage = new TradeMessage(MT_ORDER);
                    mixtadeMeaasge = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_NEW_ORDER, TradingShared.getTrader().getPath(portfolio));
                }

                //   TRSOrder order = new TRSOrder();
                Order order = new Order();
                order.setTwOrderID(TradingShared.getTrader().getCurrentDay(order.getPortfolioNo()) + TradingShared.getNextMessageID());
                order.setPortfolioNo(portfolio);
                order.setSecurityExchange(exchange);
                order.setSymbol(TradingShared.getTradingSymbol(symbol, portfolio));
                if (TradingShared.getTradingSymbolMarketCode(symbol) != null) {
                    order.setMarketCode(TradingShared.getTradingSymbolMarketCode(symbol));
                }
                order.setSecurityType(securityType + "");
                order.setMubasherSecurityType(securityType);
                order.setBrokerID(TradingShared.getTrader().getPortfolio(order.getPortfolioNo()).getBrokerID());
                if (bookKeepr != null && !bookKeepr.isEmpty()) {
                    order.setBookKeeperID((bookKeepr));
                }
                //order.setTPlus(tPlus); //todo check for tplus

                /*if ((type == TradeMeta.ORDER_TYPE_CONDITIONAL) && (side == TradeMeta.BUY)){
                    order.setConditionType(TradeMeta.CONDITION_TYPE_CONDITIONAL_BID);
                } else if ((type == TradeMeta.ORDER_TYPE_CONDITIONAL) && (side == TradeMeta.SELL)){
                    order.setConditionType(TradeMeta.CONDITION_TYPE_CONDITIONAL_OFFER);
                } else*/


                if (rule != null) {
//                    order.setConditionType(TradeMeta.CONDITION_TYPE_NORMAL);
//                } else if ((takeProfit > 0) || (stopprice > 0)){
//                    order.setConditionType(TradeMeta.CONDITION_TYPE_STOP_LOSS);
                } else if (quoteID != null) {
//                    order.setQuoteID(quoteID); //todo check for quote data
//                    order.setForexBestQuoteID(bestQuoteID);   //todo check for quote data
//                    order.setForexBrockerID(brokerID); //todo check for quote data
//                    order.setForexBestPrice(bestBuySell);  //todo check for quote data
                } else {
//                    order.setConditionType(TradeMeta.CONDITION_TYPE_NONE);
                }

//                order.setChannel(MIXConstants.CHANNEL_TYPE_TW);
                if (TWControl.enablePickNewChannelID()) {
                    order.setChannel(MIXConstants.CHANNEL_TYPE_TWS);
                } else {
                    order.setChannel(MIXConstants.CHANNEL_TYPE_NEWTW);
                }

                order.setSide(side);
                order.setOrdType(type);
                order.setPrice(price);
                order.setStopPrice(stopprice);
                order.setRightsAwarenessStatus(rightsAwarenessStatus);
                //  order.setTakeProfit(takeProfit);   //todo check for takeprofit data

                order.setCurrency(currency);
                order.setOrderQty(quantity);
                if ((baseSymbol != null) && !(baseSymbol.equals(""))) {
                    //  order.setBaseSymbol(TradingShared.getTradingSymbol(baseSymbol, portfolio));
                    //todo check for base symbol data
                }
                if (TradingShared.isTiffDateValid(tiff)) {
                    if (goodTillLong == 0) {
                        Date date = TradeMethods.getDateFromDays(0);
                        goodTillLong = date.getTime();
                        date = null;
                    }
                    order.setExpireTime(TradingShared.formatExpireDate(goodTillLong));
                }
                order.setTimeInForce(tiff);
                order.setUserID(TradingShared.getTrader().getUserID(order.getPortfolioNo()));
                if (disclosed >= 0) {
                    order.setMaxFloor(disclosed);
                }
                order.setMinimumQty(minfill);
                if (allOrNone) {
                    order.setMinimumQty(order.getOrderQty());
                }

                order.setStrikePrice(strikePrice);
                order.setMaturityMonthYear(maturityMMYY);

                //      order.setPassword(TradingShared.getLevel2Password());   //todo check for passwoed
                //     order.setCertificate(TradingShared.getLevel2Password()); //todo check for certificate

                if (rule != null) {
//                    order.setConditionStatus(ruleStatus);
//                    order.setConditionParameters(rule);
//                    //order.setConditionExpire(ruleExpiryDate);   //todo check for condtion expire
//                    order.setConditionStartTime(ruleStartDate);
                }
//                order.setChannel(MIXConstants.CHANNEL_TYPE_TW);
                if (TWControl.enablePickNewChannelID()) {
                    order.setChannel(MIXConstants.CHANNEL_TYPE_TWS);
                } else {
                    order.setChannel(MIXConstants.CHANNEL_TYPE_NEWTW);
                }

                if (TradeMethods.isSignatureRequired()) {
                    // tradeMessage.setSignature(TWSecurityManager.sign(order.getMubasherMessage(), SharedMethods.decrypt(TradingShared.getUSBPin())));
                    //todo check for signature
                }
                //tradeMessage.addData(order.getMubasherMessage());
                mixtadeMeaasge.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{order});
                if (queue) {
                    QueuedTransaction queuedTransaction = new QueuedTransaction(qID);
                    queuedTransaction.setMixOrderObject(order);
                    queuedTransaction.setOrderDate(System.currentTimeMillis());
                    OrderQueue.getSharedInstance().addTransaction(queuedTransaction);
                } else {
                    //SendQueue.getSharedInstance().addData(tradeMessage.toString(), TradingShared.getTrader().getPath(order.getPortfolioNo()));
                    SendQueue.getSharedInstance().addData(mixtadeMeaasge.getMIXString(), TradingShared.getTrader().getPath(order.getPortfolioNo()));
                }
                return true;
            }

            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }

    public boolean sendNewOrderForExec(boolean mustAuthenitate, boolean queue, long qID, String portfolio, String exchange,
                                       String symbol, int side, char type, double price, double maxPrice, String currency,
                                       long quantity, int tiff, long goodTillLong, int disclosed, int minfill,
                                       boolean allOrNone, double strikePrice, String maturityMMYY, int securityType,
                                       double stopprice, String baseSymbol, int ruleStatus, ConditionalBehaviour rule, String ruleStartDate,
                                       String ruleExpiryDate, double takeProfit, boolean isSliceEnabled, int execType, long timeInterval,
                                       int blockSize, int stopPriceType, boolean isBracketEnabled, double trailStopPrice, boolean isDayOrder, String tWOrderID, String refID, String bookKeeper, int tPlus, ConditionalBehaviour bracketRule, int rightsAwarenessStatus) {
        try {

            boolean authenticated = false;

            if (!Settings.isConnected()) {
                SharedMethods.showMessage(Language.getString("MSG_PRICE_DISCONNECTION_WARNING"), JOptionPane.WARNING_MESSAGE);
            }

            if (mustAuthenitate) {
                if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
                    TradeMethods.getSharedInstance().blockTillValidated();
                    if (TradingShared.level2AuthenticationSucess) {
                        authenticated = true;
                    }
                }
            } else {
                authenticated = true;
            }
            if (authenticated) {
                TradeMessage tradeMessage;
                MIXObject mixtadeMeaasge;
                if (isSliceEnabled) {
                    tradeMessage = new TradeMessage(MT_SLICED_ORDER);    // todo add othe order types
                    mixtadeMeaasge = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_NEW_ORDER, TradingShared.getTrader().getPath(portfolio));

                } else {
                    if (rule != null) {
                        tradeMessage = new TradeMessage(MT_CONDITIONAL_ORDER);    // todo add othe order types
                        mixtadeMeaasge = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_NEW_ORDER, TradingShared.getTrader().getPath(portfolio));

                    } else {
                        tradeMessage = new TradeMessage(MT_ORDER);
                        mixtadeMeaasge = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_NEW_ORDER, TradingShared.getTrader().getPath(portfolio));
                    }
                }

                //  TRSOrder order = new TRSOrder();
                Order order = new Order();
                if (tWOrderID == null) {
                    order.setTwOrderID(TradingShared.getTrader().getCurrentDay(order.getPortfolioNo()) + TradingShared.getNextMessageID());
                } else {
                    order.setTwOrderID(tWOrderID);
                }
                order.setPortfolioNo(portfolio);
                order.setSecurityExchange(exchange);
                order.setSymbol(TradingShared.getTradingSymbol(symbol, portfolio));
                if (TradingShared.getTradingSymbolMarketCode(symbol) != null) {
                    order.setMarketCode(TradingShared.getTradingSymbolMarketCode(symbol));
                }
                order.setSecurityType(securityType + "");
                order.setMubasherSecurityType(securityType);
                order.setBrokerID(TradingShared.getTrader().getPortfolio(order.getPortfolioNo()).getBrokerID());
                //order.setRefID(refID); //todo add ref id
                if (bookKeeper != null && !bookKeeper.isEmpty()) {
                    order.setBookKeeperID((bookKeeper));  // todo check for type
                }
                // order.setTPlus(tPlus);    //todo check for tplus

                /*if ((type == TradeMeta.ORDER_TYPE_CONDITIONAL) && (side == TradeMeta.BUY)){
                   order.setConditionType(TradeMeta.CONDITION_TYPE_CONDITIONAL_BID);
               } else if ((type == TradeMeta.ORDER_TYPE_CONDITIONAL) && (side == TradeMeta.SELL)){
                   order.setConditionType(TradeMeta.CONDITION_TYPE_CONDITIONAL_OFFER);
               } else*/


                if (isSliceEnabled) {
                    if (rule != null) {
                        order.setOrderCategory(MIXConstants.ORDER_CATEGORY_ADVANCE);
                    } else {
                        order.setOrderCategory(MIXConstants.ORDER_CATEGORY_ALGO);
                    }
                } else {
                    if (rule != null) {
                        order.setOrderCategory(MIXConstants.ORDER_CATEGORY_CONDITIONAL);
                    } else if (bracketRule != null) {
                        order.setOrderCategory(MIXConstants.ORDER_CATEGORY_CONDITIONAL);
                    } else {
                        order.setOrderCategory(MIXConstants.ORDER_CATEGORY_NORMAL);
                    }
                }

                if (rule != null) {        //todo check for condtion type
//                    order.setOrderCategory(MIXConstants.ORDER_CATEGORY_CONDITIONAL);
                    ArrayList<ConditionalBehaviour> list = new ArrayList<ConditionalBehaviour>();
                    list.add(rule);

                    order.setBehaviourList(list);
//                    if(isBracketEnabled){
//                        rule.setConditionType(MIXConstants.CONDITION_TYPE_BRACKET);
//                    }
                    //      order.setConditionType(TradeMeta.CONDITION_TYPE_NORMAL);
//                } else if (isBracketEnabled){
//                    order.setConditionType(TradeMeta.CONDITION_TYPE_BRACKET);
                }

                if (bracketRule != null) {
                    //if (order.getOrderCategory() != MIXConstants.ORDER_CATEGORY_CONDITIONAL) {
//                        order.setOrderCategory(MIXConstants.ORDER_CATEGORY_CONDITIONAL);
                    //}
                    if (order.getBehaviourList() != null) {
                        order.getBehaviourList().add(bracketRule);
                    } else {
                        ArrayList<ConditionalBehaviour> list = new ArrayList<ConditionalBehaviour>();
                        list.add(bracketRule);
                        order.setBehaviourList(list);
                    }

                }
                /*if (bracketRule == null && rule == null) {
                    order.setOrderCategory(MIXConstants.ORDER_CATEGORY_NORMAL);
                }*/
//                else {
//                    order.setOrderCategory(MIXConstants.ORDER_CATEGORY_NORMAL);
//                }

                /*if (type == TradeMeta.ORDER_TYPE_CONDITIONAL){
                    type = TradeMeta.ORDER_TYPE_LIMIT;

                    StringBuilder ruleBuffer = new StringBuilder();
                    ruleBuffer.append(exchange);
                    ruleBuffer.append(Meta.DD);
                    ruleBuffer.append(symbol);
                    ruleBuffer.append(Meta.DD);
                    ruleBuffer.append(0);
                    ruleBuffer.append(Meta.DD);
                    ruleBuffer.append(0);
                    ruleBuffer.append(Meta.DD);
                    ruleBuffer.append(price);
                    ruleBuffer.append(Meta.DD);
                    ruleBuffer.append("0");
                    rule = ruleBuffer.toString();

                    order.setConditionStatus(ruleStatus);
                    order.setCondition(rule);
                    order.setConditionExpireTime(TradingShared.formatExpireDate(goodTillLong));
                    order.setConditionStartTime(TradingShared.formatExpireDate(System.currentTimeMillis())); // this field is currently not being used
                }*/

                order.setSide(side);
                order.setOrdType(type);
                order.setPrice(price);
                order.setStopPrice(stopprice);
                order.setTakeProfit(takeProfit); //todo check for take profit
                order.setStopPxType(stopPriceType); //todo check for stop loss price
                order.setStopLossTrailPrice(trailStopPrice);
                int dayorder = 0;
                if (isDayOrder) {
                    dayorder = 1;
                }
                order.setDayOrder(dayorder);

                order.setCurrency(currency);
                order.setOrderQty(quantity);
                if ((baseSymbol != null) && !(baseSymbol.equals(""))) {
                    // order.setBaseSymbol(TradingShared.getTradingSymbol(baseSymbol, portfolio));  todo add base symbol
                }
                if (TradingShared.isTiffDateValid(tiff)) {
                    order.setExpireTime(TradingShared.formatExpireDate(goodTillLong));
                }
                order.setTimeInForce(tiff);
                order.setUserID(TradingShared.getTrader().getUserID(order.getPortfolioNo()));
                if (disclosed >= 0) {
                    order.setMaxFloor(disclosed);
                }
                order.setMinimumQty(minfill);
                if (allOrNone) {
                    order.setMinimumQty(order.getOrderQty());
                }

                order.setStrikePrice(strikePrice);
                order.setMaturityMonthYear(maturityMMYY);
                order.setRightsAwarenessStatus(rightsAwarenessStatus);

                //order.setPassword(TradingShared.getLevel2Password());  //todo check for setpassword
                //order.setCertificate(TradingShared.getLevel2Password()); //  todo check for certificate

                if (rule != null) {
//                    order.setConditionStatus(ruleStatus);
//                    order.setConditionParameters(rule);
//                    //  order.setConditionExpireTime(ruleExpiryDate);  //todo check for expiary time
//                    order.setConditionStartTime(ruleStartDate);
                }
                if (isSliceEnabled) {

//                    order.setOrderCategory(MIXConstants.ORDER_CATEGORY_ALGO);
                    AlgoCondition algo = new AlgoCondition();
                    algo.setAlgoType(execType);
                    algo.setSliceOrderBlock(blockSize + "");
                    algo.setSliceOrderInterval(timeInterval + "");
                    //   algo.setSliceOrderIntervalType(MIXConstants.SLICE_ORDER_INTERVAL_TYPE_AGGRESSIVE);
                    algo.setSliceOrderStatus(TradeMeta.SLICE_STATUS_PENDING + "");
                    order.setAlgoCondition(algo);
//                    order.setSliceBlockSIze(blockSize);
//                    order.setSliceExecType(execType);               // todo check for slice order params
//                    order.setSliceTimeInterval(timeInterval);
//                    order.setSliceOrderStatus(TradeMeta.SLICE_STATUS_PENDING);
                }

                if (TradeMethods.isSignatureRequired()) {
                    // tradeMessage.setSignature(TWSecurityManager.sign(order.getMubasherMessage(), SharedMethods.decrypt(TradingShared.getUSBPin())));
                    // todo check for signature
                }
//                order.setChannel(MIXConstants.CHANNEL_TYPE_TW);
                if (TWControl.enablePickNewChannelID()) {
                    order.setChannel(MIXConstants.CHANNEL_TYPE_TWS);
                } else {
                    order.setChannel(MIXConstants.CHANNEL_TYPE_NEWTW);
                }
                mixtadeMeaasge.getMIXHeader().setChannelID(order.getChannel());
                mixtadeMeaasge.getMIXHeader().setUserID(order.getUserID());
                //    tradeMessage.addData(order.getMubasherMessage());
                mixtadeMeaasge.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{order});
                if (queue) {
                    QueuedTransaction queuedTransaction = new QueuedTransaction(qID);
                    queuedTransaction.setSelected(true);
                    queuedTransaction.setMixOrderObject(order);
                    queuedTransaction.setOrderDate(System.currentTimeMillis());
                    OrderQueue.getSharedInstance().addTransaction(queuedTransaction);
                } else {
                    // SendQueue.getSharedInstance().addData(tradeMessage.toString(), TradingShared.getTrader().getPath(order.getPortfolioNo()));
                    SendQueue.getSharedInstance().addData(mixtadeMeaasge.getMIXString(), TradingShared.getTrader().getPath(order.getPortfolioNo()));
                }
                return true;
            }

            return false;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }

    }


    public static void cancelOrder(Transaction oldTransaction) throws Exception {

        TradeMessage tradeMessage;
        MIXObject mixTradeMeassage;
        // TRSOrder order = new TRSOrder();
        Order order = new Order();

        if (oldTransaction.getSliceExecType() != -1) {
            tradeMessage = new TradeMessage(MT_SLICED_ORDER);
//              order.setSliceOrderStatus(TradeMeta.SLICE_STATUS_CENCELLED);
            mixTradeMeassage = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_CANCEL_ORDER, TradingShared.getTrader().getPath(oldTransaction.getPortfolioNo()));
        } else {
            if (oldTransaction.getRuleType() == TradeMeta.CONDITION_TYPE_STOP_LOSS) { // check if this is a conditional order
                mixTradeMeassage = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_CANCEL_ORDER, TradingShared.getTrader().getPath(oldTransaction.getPortfolioNo()));
                tradeMessage = new TradeMessage(MT_CONDITIONAL_ORDER);
//                order.setConditionType(TradeMeta.CONDITION_TYPE_STOP_LOSS);
//                order.setConditionStatus(oldTransaction.getRuleType());        //todo
//                order.setConditionStatus(TradeMeta.CONDITION_STATUS_CENCELLED); // tell the rule engine to cancel it
            } else if (oldTransaction.getRuleType() == TradeMeta.CONDITION_TYPE_BRACKET) { // check if this is a conditional order
                mixTradeMeassage = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_CANCEL_ORDER, TradingShared.getTrader().getPath(oldTransaction.getPortfolioNo()));
                tradeMessage = new TradeMessage(MT_CANCEL);
//                order.setConditionType(TradeMeta.CONDITION_TYPE_BRACKET);
//                order.setConditionStatus(oldTransaction.getRuleType());         //todo
//                order.setConditionStatus(TradeMeta.CONDITION_STATUS_CENCELLED); // tell the rule engine to cancel it
            } else if (oldTransaction.getRuleType() == TradeMeta.CONDITION_TYPE_STRADEL) { // check if this is a conditional order
                mixTradeMeassage = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_CANCEL_ORDER, TradingShared.getTrader().getPath(oldTransaction.getPortfolioNo()));
                tradeMessage = new TradeMessage(MT_CANCEL);
//                order.setConditionType(TradeMeta.CONDITION_TYPE_BRACKET);
//                order.setConditionStatus(oldTransaction.getRuleType());        //todo
//                order.setConditionStatus(TradeMeta.CONDITION_STATUS_CENCELLED); // tell the rule engine to cancel it
            } else if (oldTransaction.getRuleType() == TradeMeta.CONDITION_TYPE_NORMAL) {
                mixTradeMeassage = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_CANCEL_ORDER, TradingShared.getTrader().getPath(oldTransaction.getPortfolioNo()));
                tradeMessage = new TradeMessage(MT_CONDITIONAL_ORDER);
                order.setOrderCategory(MIXConstants.CONDITION_TYPE_NORMAL);
                order.setBehaviourList(oldTransaction.getMixOrderObject().getBehaviourList());    //todo
                cancelBehavourList(order.getBehaviourList());
                //order.getBehaviourList().get(0).setConditionStatus(TradeMeta.CONDITION_STATUS_CENCELLED);
//                order.setConditionStatus(TradeMeta.CONDITION_STATUS_CENCELLED); // tell the rule engine to cancel it
            } else {
                mixTradeMeassage = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_CANCEL_ORDER, TradingShared.getTrader().getPath(oldTransaction.getPortfolioNo()));
                tradeMessage = new TradeMessage(MT_CANCEL);
            }
        }

        if (oldTransaction.getTwOrderID() == null) {
            order.setTwOrderID(TradingShared.getTrader().getCurrentDay(order.getPortfolioNo()) + TradingShared.getNextMessageID());
        } else {
            order.setTwOrderID(oldTransaction.getTwOrderID());
        }
        order.setPortfolioNo(oldTransaction.getPortfolioNo());
        order.setOrderCategory(oldTransaction.getMixOrderObject().getOrderCategory());
        order.setBrokerID(TradingShared.getTrader().getPortfolio(order.getPortfolioNo()).getBrokerID());
        order.setSecurityExchange(oldTransaction.getExchange());
        order.setSymbol(TradingShared.getTradingSymbol(oldTransaction.getSymbol(), oldTransaction.getPortfolioNo()));
        if (TradingShared.getTradingSymbolMarketCode(oldTransaction.getSymbol()) != null) {
            order.setMarketCode(TradingShared.getTradingSymbolMarketCode(oldTransaction.getSymbol()));
        }
        order.setSecurityType(oldTransaction.getSecurityType() + "");
        order.setMubasherSecurityType(oldTransaction.getSecurityType());
        order.setCurrency(oldTransaction.getCurrency());
        order.setOrderQty((int) oldTransaction.getOrderQuantity());
        order.setSide(oldTransaction.getSide());
        order.setOrdType(oldTransaction.getType());
        order.setTimeInForce(oldTransaction.getTIFType());
        order.setOrderID(oldTransaction.getOrderID());
        order.setOrigClOrdID((oldTransaction.getClOrderID()));
        order.setClOrdID((oldTransaction.getClOrderID()));
        order.setMubasherOrderNumber(Long.parseLong(oldTransaction.getMubasherOrderNumber())); // order Number
        order.setStrikePrice(oldTransaction.getStrikePrice());
        order.setMaturityMonthYear(oldTransaction.getMaturityMonthYear());
        order.setSide(oldTransaction.getSide());
        order.setUserID(TradingShared.getTrader().getUserID(oldTransaction.getPortfolioNo()));

        order.setBehaviourList(oldTransaction.getMixOrderObject().getBehaviourList());    //todo
        cancelBehavourList(order.getBehaviourList());
        //  order.setPassword(TradingShared.getLevel2Password());   //todo pwd
        //    order.setCertificate(TradingShared.getLevel2Password()); //todo certi
        if (oldTransaction.getMixOrderObject().getAlgoCondition() != null) {
            order.setAlgoCondition(oldTransaction.getMixOrderObject().getAlgoCondition());
            order.getAlgoCondition().setSliceOrderStatus(TradeMeta.SLICE_STATUS_CENCELLED + "");

        }
//        order.setSliceOrderID(oldTransaction.getSliceOrderID());      //todo slice
//        order.setSliceExecType(oldTransaction.getSliceExecType());   //todo slice
//        order.setSliceBlockSIze(oldTransaction.getSliceBlockSIze()); //todo slice
//        order.setSliceTimeInterval(oldTransaction.getSliceTimeInterval());//todo slice
        int dayorder = 0;
        if (oldTransaction.isDayOrder()) {
            dayorder = 1;
        }
        order.setDayOrder(dayorder);
        // order.setMarkedForDelivery(oldTransaction.isMarkForDelivery());  //todo
        order.setBookKeeperID((oldTransaction.getBookKeeper()));
//        order.setChannel(MIXConstants.CHANNEL_TYPE_TW);
        if (TWControl.enablePickNewChannelID()) {
            order.setChannel(MIXConstants.CHANNEL_TYPE_TWS);
        } else {
            order.setChannel(MIXConstants.CHANNEL_TYPE_NEWTW);
        }
        if (TradeMethods.isSignatureRequired()) {
            //  tradeMessage.setSignature(TWSecurityManager.sign(order.getMubasherMessage(), SharedMethods.decrypt(TradingShared.getUSBPin())));
            //todo
        }
        mixTradeMeassage.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{order});
        //  SendQueue.getSharedInstance().addData(tradeMessage.toString(), TradingShared.getTrader().getPath(order.getPortfolioNo()));
        SendQueue.getSharedInstance().addData(mixTradeMeassage.getMIXString(), TradingShared.getTrader().getPath(order.getPortfolioNo()));
    }

    public static void sendMarkForDeliveryOrder(Transaction oldTransaction) {
        TradeMessage tradeMessage = new TradeMessage(MARK_FOR_DELIVERY);
        tradeMessage.addData("A" + oldTransaction.getClOrderID());
        SendQueue.getSharedInstance().addData(tradeMessage.toString(), TradingShared.getTrader().getPath(oldTransaction.getPortfolioNo()));
        tradeMessage = null;
    }

    public static Order amendOrder(Transaction oldTransaction, boolean queued, int side, char type, double price,
                                   String currency, long quantity, short tiff, long goodTillLong, String disclosed,
                                   String minfill, double stopprice, ConditionalBehaviour rule, String ruleExpiryDate,
                                   double takeProfit, long timeInterval, int blockSize, int conditionalMode, int stopLossType,
                                   boolean isDayOrder, double trailStopLoss, boolean isMarkForDelivery, String tWOrderID, String bookKeeper, ConditionalBehaviour bracketCondtion, int rightsAwarenessStatus) {

        try {
            // TRSOrder tradeMessage = new TRSOrder();
            Order tradeMessage = new Order();
            TradeMessage tradeMessage2;
            MIXObject mixtadeMeaasge;
            if (oldTransaction.getSliceExecType() != -1) {
                tradeMessage2 = new TradeMessage(MT_SLICED_ORDER);
                //   tradeMessage.setSliceOrderStatus(SLICE_STATUS_AMENDED);    //todo check for slice
                mixtadeMeaasge = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_AMEND_ORDER, TradingShared.getTrader().getPath(oldTransaction.getPortfolioNo()));
            } else {
                if ((conditionalMode == TradeMeta.CONDITION_TYPE_NORMAL) ||
                        ((conditionalMode == TradeMeta.CONDITION_TYPE_STOP_LOSS) && (side == TradeMeta.SELL))) {
                    tradeMessage2 = new TradeMessage(MT_CONDITIONAL_ORDER);
                    mixtadeMeaasge = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_AMEND_ORDER, TradingShared.getTrader().getPath(oldTransaction.getPortfolioNo()));
                } else {
                    tradeMessage2 = new TradeMessage(MT_AMEND);
                    mixtadeMeaasge = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING, MIXConstants.REQUEST_TYPE_AMEND_ORDER, TradingShared.getTrader().getPath(oldTransaction.getPortfolioNo()));
                }
            }

            if (!queued) {
                if (tWOrderID == null) {
                    tradeMessage.setTwOrderID(TradingShared.getTrader().getCurrentDay(oldTransaction.getPortfolioNo()) + TradingShared.getNextMessageID());
                } else {
                    tradeMessage.setTwOrderID(tWOrderID);
                }
                tradeMessage.setOrderID(oldTransaction.getOrderID()); // order Number
                tradeMessage.setClOrdID((oldTransaction.getClOrderID())); // order Number
                // tradeMessage.setOrigClOrdID((oldTransaction.getOrigClOrderID())); // order Number
                tradeMessage.setOrigClOrdID((oldTransaction.getClOrderID())); // order Number
                tradeMessage.setMubasherOrderNumber(Long.parseLong(oldTransaction.getMubasherOrderNumber())); // order Number
                tradeMessage.setESISOrdNumber(oldTransaction.getESISOrderNo()); // ESIS Number
                tradeMessage.setPortfolioNo(oldTransaction.getPortfolioNo());// TradingShared.getAccount().getPortfolioID(0));
                tradeMessage.setBrokerID(TradingShared.getTrader().getPortfolio(tradeMessage.getPortfolioNo()).getBrokerID());
                tradeMessage.setSymbol(TradingShared.getTradingSymbol(oldTransaction.getSymbol(), oldTransaction.getPortfolioNo()));
                if (TradingShared.getTradingSymbolMarketCode(oldTransaction.getSymbol()) != null) {
                    tradeMessage.setMarketCode(TradingShared.getTradingSymbolMarketCode(oldTransaction.getSymbol()));
                }
                tradeMessage.setSecurityType(oldTransaction.getSecurityType() + "");
                tradeMessage.setMubasherSecurityType(oldTransaction.getSecurityType());

                tradeMessage.setStrikePrice(oldTransaction.getStrikePrice());
                tradeMessage.setMaturityMonthYear(oldTransaction.getMaturityMonthYear());
                tradeMessage.setOrderCategory(oldTransaction.getOrderCategory());
                tradeMessage.setRightsAwarenessStatus(rightsAwarenessStatus);
            }

            if (oldTransaction.getSliceExecType() != -1) {
                if (rule != null) {
                    tradeMessage.setOrderCategory(MIXConstants.ORDER_CATEGORY_ADVANCE);
                } else {
                    tradeMessage.setOrderCategory(MIXConstants.ORDER_CATEGORY_ALGO);
                }
            } else {
                if (rule != null) {
                    tradeMessage.setOrderCategory(MIXConstants.ORDER_CATEGORY_CONDITIONAL);
                } else if (bracketCondtion != null) {
                    tradeMessage.setOrderCategory(MIXConstants.ORDER_CATEGORY_CONDITIONAL);
                } else {
                    tradeMessage.setOrderCategory(MIXConstants.ORDER_CATEGORY_NORMAL);
                }
            }

            if (rule != null) {        //todo check for condtion type
                ArrayList<ConditionalBehaviour> list = new ArrayList<ConditionalBehaviour>();
                list.add(rule);

                tradeMessage.setBehaviourList(list);
            }

            if (bracketCondtion != null) {
                if (tradeMessage.getBehaviourList() != null) {
                    tradeMessage.getBehaviourList().add(bracketCondtion);
                } else {
                    ArrayList<ConditionalBehaviour> list = new ArrayList<ConditionalBehaviour>();
                    list.add(bracketCondtion);
                    tradeMessage.setBehaviourList(list);
                }

            }


            if (conditionalMode == TradeMeta.CONDITION_TYPE_NORMAL) {
//                if (oldTransaction.getOrderCategory() == MIXConstants.ORDER_CATEGORY_ALGO) {
//                    tradeMessage.setOrderCategory(MIXConstants.ORDER_CATEGORY_ADVANCE);
//                } else {
//                    tradeMessage.setOrderCategory(MIXConstants.ORDER_CATEGORY_CONDITIONAL);
//                }
//                ArrayList<ConditionalBehaviour> list = new ArrayList<ConditionalBehaviour>();
//                list.add(rule);
//                tradeMessage.setBehaviourList(list);
//                tradeMessage.setConditionType(TradeMeta.CONDITION_TYPE_NORMAL);
//                tradeMessage.setConditionStatus(TradeMeta.CONDITION_STATUS_AMENDED);
//                tradeMessage.setConditionParameters(rule);
//                //  tradeMessage.setConditionExpireTime(ruleExpiryDate);   //todo expiary
//                tradeMessage.setConditionStartTime(TradingShared.formatExpireDate(System.currentTimeMillis())); // this field is currently not being used

            } else if (conditionalMode == TradeMeta.CONDITION_TYPE_STOP_LOSS) {
//                tradeMessage.setConditionType(TradeMeta.CONDITION_TYPE_STOP_LOSS);
//                tradeMessage.setConditionStatus(TradeMeta.CONDITION_STATUS_AMENDED);
//                tradeMessage.setConditionParameters(rule);
//                // tradeMessage.setConditionExpireTime(ruleExpiryDate); //todo expiary
//                tradeMessage.setConditionStartTime(TradingShared.formatExpireDate(System.currentTimeMillis())); // this field is currently not being used

            } else if (conditionalMode == TradeMeta.CONDITION_TYPE_BRACKET) {
//                tradeMessage.setConditionType(TradeMeta.CONDITION_TYPE_BRACKET);
//                tradeMessage.setConditionStatus(TradeMeta.CONDITION_STATUS_AMENDED);
//                tradeMessage.setCondition(null);
//                tradeMessage.setConditionExpireTime(ruleExpiryDate);
//                tradeMessage.setConditionStartTime(TradingShared.formatExpireDate(System.currentTimeMillis())); // this field is currently not being used

            } else {
//                tradeMessage.setConditionType(TradeMeta.CONDITION_TYPE_NONE);
            }
//            if (bracketCondtion != null) {
//                if (tradeMessage.getBehaviourList() != null) {
//                    tradeMessage.getBehaviourList().add(bracketCondtion);
//                } else {
//                    ArrayList<ConditionalBehaviour> list = new ArrayList<ConditionalBehaviour>();
//                    list.add(bracketCondtion);
//                    tradeMessage.setBehaviourList(list);
//                }
//
//            }

            //   tradeMessage.setConditionType(conditionalMode);   todo
            tradeMessage.setSecurityExchange(oldTransaction.getExchange());
            tradeMessage.setSide(side);
            tradeMessage.setOrdType(type);
            tradeMessage.setTimeInForce(tiff);
            if (TradingShared.isTiffDateValid(tiff)) {
                tradeMessage.setExpireTime(TradingShared.formatExpireDate(goodTillLong));
            }
            tradeMessage.setPrice(price);
            tradeMessage.setStopPrice(stopprice);
            tradeMessage.setStopPxType(stopLossType); //todo stop loss
            tradeMessage.setTakeProfit(takeProfit);  //todo take profit
            tradeMessage.setCurrency(currency);
            tradeMessage.setStopLossTrailPrice(trailStopLoss);
            int dayord = 0;
            if (isDayOrder) {
                dayord = 1;
            }
            tradeMessage.setDayOrder(dayord);
            //  tradeMessage.setMarkedForDelivery(isMarkForDelivery);  // todo
            try {
                tradeMessage.setMinimumQty(Integer.parseInt(minfill));
            } catch (Exception e) {
                tradeMessage.setMinimumQty(0);
            }
                tradeMessage.setMaxFloor(TradingShared.getIntValue(disclosed));
            /*if ((!disclosed.equals(""))) {
                tradeMessage.setMaxFloor(TradingShared.getIntValue(disclosed));
            }*/
            tradeMessage.setOrderQty(quantity); // filled quantity
            tradeMessage.setUserID(TradingShared.getTrader().getUserID(oldTransaction.getPortfolioNo()));
            //   tradeMessage.setPassword(TradingShared.getLevel2Password());    //todo pwd
            //   tradeMessage.setCertificate(TradingShared.getLevel2Password()); //todo certificate
            //    tradeMessage.setChannel(1); // channel
            tradeMessage.setChannel(oldTransaction.getChannel()); // channel


            if (oldTransaction.getSliceExecType() != -1) {
                //    tradeMessage.setOrderCategory(oldTransaction.getMixOrderObject().getOrderCategory());
                tradeMessage.setAlgoCondition(oldTransaction.getMixOrderObject().getAlgoCondition());
                tradeMessage.getAlgoCondition().setSliceOrderStatus(TradeMeta.SLICE_STATUS_AMENDED + "");
                // tradeMessage.getAlgoCondition().setSliceOrderBlock(blockSize+"");
                //   tradeMessage.getAlgoCondition().setSliceOrderInterval(timeInterval +"");
//                tradeMessage.getAlgoCondition().setSliceOrderID();
                //    tradeMessage.getAlgoCondition().setAlgoType(oldTransaction.getSliceExecType());

//                tradeMessage.setSliceBlockSIze(blockSize);     //todo slice
//                tradeMessage.setSliceTimeInterval(timeInterval);      //todo slice
//                tradeMessage.setSliceOrderID(oldTransaction.getSliceOrderID()); //todo slice
//                tradeMessage.setSliceExecType(oldTransaction.getSliceExecType());   //todo slice
            }
//            tradeMessage.setChannel(MIXConstants.CHANNEL_TYPE_TW);

            if (TWControl.enablePickNewChannelID()) {
                tradeMessage.setChannel(MIXConstants.CHANNEL_TYPE_TWS);
            } else {
                tradeMessage.setChannel(MIXConstants.CHANNEL_TYPE_NEWTW);
            }

            if (TradeMethods.isSignatureRequired()) {
                //    tradeMessage2.setSignature(TWSecurityManager.sign(tradeMessage.getMubasherMessage(), SharedMethods.decrypt(TradingShared.getUSBPin())));
                //todo
            }
            if (queued) {
                return tradeMessage;
            } else {
                mixtadeMeaasge.getMIXHeader().setChannelID(tradeMessage.getChannel());
                mixtadeMeaasge.getMIXHeader().setUserID(tradeMessage.getUserID());
                mixtadeMeaasge.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{tradeMessage});
                SendQueue.getSharedInstance().addData(mixtadeMeaasge.getMIXString(), TradingShared.getTrader().getPath(oldTransaction.getPortfolioNo()));
                return null;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return null;
    }

    public void duplicateOrder(Transaction oldTransaction, String tWOrderID) {
        TradeMessage tradeMessage2;
        try {
            if (TradingShared.isReadyForTrading()) {
                String symbol = oldTransaction.getSymbol();
                String exchange = oldTransaction.getExchange();
                Stock stock = DataStore.getSharedInstance().getStockByExchangeKey(exchange,symbol);
                int instrumentType = stock.getInstrumentType();
                String symbolKey = SharedMethods.getKey(exchange, symbol, instrumentType);
                int type = oldTransaction.getSide();

                Client.getInstance().doTransactionForDuplicate(type, symbolKey, oldTransaction.getOrderQuantity(), false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, oldTransaction.getPortfolioNo(), oldTransaction.getBookKeeper(), oldTransaction.getPrice(),oldTransaction);


                //Commented for Duplicate Oreder implementation by Oshada
                /*String message = Language.getString("DUPLICATE_ORDER_CONFIRM").replace("\\[NUMBER\\]", oldTransaction.getMubasherOrderNumber());
                int result = SharedMethods.showConfirmMessage(message, JOptionPane.WARNING_MESSAGE);
                if (result == JOptionPane.OK_OPTION) {
                    if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
                        TradeMethods.getSharedInstance().blockTillValidated();
                        if (TradingShared.level2AuthenticationSucess) {
                            tradeMessage2 = new TradeMessage(MT_ORDER);

                            TRSOrder tradeMessage = new TRSOrder();
                            if (tWOrderID == null) {
                                tradeMessage.setTwOrderID(TradingShared.getTrader().getCurrentDay(tradeMessage.getPortfolioNo()) + TradingShared.getNextMessageID());
                            } else {
                                tradeMessage.setTwOrderID(tWOrderID);
                            }
//                    tradeMessage.setOrderID(oldTransaction.getOrderID()); // order Number
//                    tradeMessage.setClOrdID(oldTransaction.getClOrderID()); // order Number
//                    tradeMessage.setOrigClOrdID(oldTransaction.getClOrderID()); // order Number
//                    tradeMessage.setMubasherOrderNumber(oldTransaction.getMubasherOrderNumber()); // order Number
//                    tradeMessage.setESISNumber(oldTransaction.getESISOrderNo()); // ESIS Number
                            tradeMessage.setPortfolioNo(oldTransaction.getPortfolioNo());// TradingShared.getAccount().getPortfolioID(0));
                            tradeMessage.setBrokerID(TradingShared.getTrader().getPortfolio(tradeMessage.getPortfolioNo()).getBrokerID());
                            tradeMessage.setSymbol(TradingShared.getTradingSymbol(oldTransaction.getSymbol(), oldTransaction.getPortfolioNo()));
                            tradeMessage.setSecurityExchange(oldTransaction.getExchange());
                            tradeMessage.setSide(oldTransaction.getSide());
                            tradeMessage.setOrdType(oldTransaction.getType());
                            tradeMessage.setTimeInForce(oldTransaction.getTIFType());
                            tradeMessage.setPrice(oldTransaction.getPrice());
                            //tradeMessage.setMaxPrice(maxPrice);
                            tradeMessage.setCurrency(oldTransaction.getCurrency());
                            tradeMessage.setMinQty((int) oldTransaction.getMinQuantity());
                            //if ((!txtDisclosed.getText().equals("")) || (!txtDisclosed.getText().trim().equals("0"))) {
                            tradeMessage.setMaxFloor(oldTransaction.getDiscloseQuantity());
                            //}
                            tradeMessage.setOrderQty((int) oldTransaction.getOrderQuantity()); // filled quantity
                            tradeMessage.setUserID(TradingShared.getTrader().getUserID(oldTransaction.getPortfolioNo()));
                            tradeMessage.setPassword(TradingShared.getLevel2Password());
                            tradeMessage.setCertificate(TradingShared.getLevel2Password());
                            tradeMessage.setChannel(1); // channel

                            if (TradeMethods.isSignatureRequired()) {
                                tradeMessage2.setSignature(TWSecurityManager.sign(tradeMessage.getMubasherMessage(),
                                        SharedMethods.decrypt(TradingShared.getUSBPin())));
                            }
                            tradeMessage2.addData(tradeMessage.getMubasherMessage());
                            SendQueue.getSharedInstance().addData(tradeMessage2.toString(), TradingShared.getTrader().getPath(oldTransaction.getPortfolioNo()));
                        }
                    }
                }*/

            }
            else {
                SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean ruleBasedValidationSucess(boolean conditionalMode, String exchange, int type, double price, long quantity, long disclosed,
                                                    long minfill, double min, double max, short tiff, int side, String symbol, int direction, Stock stock, boolean needToShowMessage, StringBuilder... errorMessage) {

        Interpreter interpreter = new Interpreter();

        if (ExchangeConfigListStore.getSharedInstance().getExgTypeConfig(exchange) != 0) {
            try {
                Iterator<Rule> rules = RuleManager.getSharedInstance().getRules();

                while (rules.hasNext()) {
                    Rule rule = rules.next();

                    if (((exchange.equals("*")) || SharedMethods.contains(rule.getExchange().split(","), exchange)) &&             //(rule.getExchange().indexOf(exchange) >= 0))
                            (rule.getId().startsWith("MIN_MAX_ONE_SIDE_PRICE_LI")) &&
                            (rule.getCategory().equals("NEW_ORDER"))) { // execute general orders only

                        if (conditionalMode && RuleManager.getSharedInstance().isExcempted(exchange, rule.getId())) { // check if the rule is excepted for conditional mode
                            continue;
                        }
                        int mktStatus = ExchangeStore.getSharedInstance().getExchange(exchange).getMarketStatus();
                        try {
                            interpreter.set("symbol", symbol);
                            interpreter.set("instrumentType", stock.getInstrumentType());
                            interpreter.set("sector", stock.getSectorCode());
                            interpreter.set("price", Math.round(price * 1000) / (double) 1000);
                            interpreter.set("quantity", quantity);
                            interpreter.set("min", Math.round(min * 1000) / (double) 1000);
                            interpreter.set("max", Math.round(max * 1000) / (double) 1000);
                            interpreter.set("open", stock.getTodaysOpen());
                            interpreter.set("high", stock.getHigh());
                            interpreter.set("low", stock.getLow());
                            interpreter.set("side", side);
                            interpreter.set("tiff", tiff);
                            interpreter.set("type", type);
                            interpreter.set("disclosed", disclosed);
                            interpreter.set("minfill", minfill);
                            interpreter.set("marketCode", stock.getMarketID());
                            interpreter.set("refPrice", stock.getRefPrice());
                            interpreter.set("bid", stock.getBestBidPrice());
                            interpreter.set("offer", stock.getBestAskPrice());
                            interpreter.set("direction", direction);
                            interpreter.set("mktstatus", mktStatus);
                            System.out.println("Rule ID and Rule ==" + rule.getId() + " ,, " + rule.getRule());
                            System.out.println("Rule message: " + rule.getMessage());
                            boolean result = ((Boolean) interpreter.eval(rule.getRule()));
                            if (!conditionalMode) {
                                if (result) {
                                    if (needToShowMessage) {
//                                    SharedMethods.showMessage(Language.getLanguageSpecificString(rule.getMessage()),
//                                            JOptionPane.ERROR_MESSAGE);
//                                        SharedMethods.showMessage(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(rule.getMessage())),
//                                                JOptionPane.ERROR_MESSAGE);
//                                        return false;
                                        if (side == 1) {
                                            int resultMsge = SharedMethods.showConfirmMessage(Language.getString("BUY_ORDER_PRICE_NOT_EXCEED_MAX"),
                                                    JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                                            if (resultMsge != JOptionPane.OK_OPTION) {
                                                return false;
                                            }
                                        } else {
                                            int resultMsge = SharedMethods.showConfirmMessage(Language.getString("SELL_ORDER_PRICE_NOT_LESS_MIN"),
                                                    JOptionPane.WARNING_MESSAGE, JOptionPane.OK_CANCEL_OPTION);
                                            if (resultMsge != JOptionPane.OK_OPTION) {
                                                return false;
                                            }
                                        }
                                    } else {

                                        errorMessage[0].append(rule.getMessage());
                                        return false;
                                    }
                                }
                            }
                        } catch (Exception evalError) {
                            //evalError.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                interpreter = null;
            }


        } else {

            try {
                Iterator<Rule> rules = RuleManager.getSharedInstance().getRules();

                while (rules.hasNext()) {
                    Rule rule = rules.next();

                    if (((exchange.equals("*")) || SharedMethods.contains(rule.getExchange().split(","), exchange)) &&             //(rule.getExchange().indexOf(exchange) >= 0))
                            (rule.getId().startsWith("GEN_")) &&
                            (rule.getCategory().equals("NEW_ORDER"))) { // execute general orders only

                        if (conditionalMode && RuleManager.getSharedInstance().isExcempted(exchange, rule.getId())) { // check if the rule is excepted for conditional mode
                            continue;
                        }
                        int mktStatus = ExchangeStore.getSharedInstance().getExchange(exchange).getMarketStatus();
                        try {
                            interpreter.set("symbol", symbol);
                            interpreter.set("instrumentType", stock.getInstrumentType());
                            interpreter.set("sector", stock.getSectorCode());
                            interpreter.set("price", Math.round(price * 1000) / (double) 1000);
                            interpreter.set("quantity", quantity);
                            interpreter.set("min", Math.round(min * 1000) / (double) 1000);
                            interpreter.set("max", Math.round(max * 1000) / (double) 1000);
                            interpreter.set("open", stock.getTodaysOpen());
                            interpreter.set("high", stock.getHigh());
                            interpreter.set("low", stock.getLow());
                            interpreter.set("side", side);
                            interpreter.set("tiff", tiff);
                            interpreter.set("type", type);
                            interpreter.set("disclosed", disclosed);
                            interpreter.set("minfill", minfill);
                            interpreter.set("marketCode", stock.getMarketID());
                            interpreter.set("refPrice", stock.getRefPrice());
                            interpreter.set("bid", stock.getBestBidPrice());
                            interpreter.set("offer", stock.getBestAskPrice());
                            interpreter.set("direction", direction);
                            interpreter.set("mktstatus", mktStatus);
                            System.out.println("Rule ID and Rule ==" + rule.getId() + " ,, " + rule.getRule());
                            System.out.println("Rule message: " + rule.getMessage());
                            boolean result = ((Boolean) interpreter.eval(rule.getRule()));
                            if (!conditionalMode) {
                                if (result) {
                                    if (needToShowMessage) {
//                                    SharedMethods.showMessage(Language.getLanguageSpecificString(rule.getMessage()),
//                                            JOptionPane.ERROR_MESSAGE);
                                        SharedMethods.showMessage(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(rule.getMessage())),
                                                JOptionPane.ERROR_MESSAGE);
                                        return false;
                                    } else {

                                        errorMessage[0].append(rule.getMessage());
                                        return false;
                                    }
                                }
                            }
                        } catch (Exception evalError) {
                            //evalError.printStackTrace();
                        }
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                interpreter = null;
            }
        }
        return true;
    }

    /*
    public static boolean ruleBasedValidationSucess(boolean conditionalMode, String exchange, int type, double price, long quantity, long disclosed,
                                                   long minfill, double min, double max, short tiff, int side, boolean needToShowMessage, StringBuilder... errorMessage) {

       Interpreter interpreter = new Interpreter();

       try {
           Iterator<Rule> rules = RuleManager.getSharedInstance().getRules();

           while (rules.hasNext()) {
               Rule rule = rules.next();

               if (((exchange.equals("*")) || SharedMethods.contains(rule.getExchange().split(","), exchange))  &&             //(rule.getExchange().indexOf(exchange) >= 0))
                       (rule.getId().startsWith("GEN_")) &&
                       (rule.getCategory().equals("NEW_ORDER"))) { // execute general orders only

                   if (conditionalMode && RuleManager.getSharedInstance().isExcempted(exchange,rule.getId())){ // check if the rule is excepted for conditional mode
                       continue;
                   }
                   int mktStatus = ExchangeStore.getSharedInstance().getExchange(exchange).getMarketStatus();
                   try {
                       interpreter.set("disclosed", disclosed);
                       interpreter.set("quantity", quantity);
                       interpreter.set("price", Math.round(price * 1000) / (double) 1000); // this trick is to remove any
                       interpreter.set("minfill", minfill);                               //   trailing gabage digits in double s
                       interpreter.set("min", Math.round(min * 1000) / (double) 1000);
                       interpreter.set("max", Math.round(max * 1000) / (double) 1000);
                       interpreter.set("type", type);
                       interpreter.set("tiff", tiff);
                       interpreter.set("side", side);
                       interpreter.set("mktstatus", mktStatus);
                       boolean result = ((Boolean) interpreter.eval(rule.getRule()));
                       if (result == true) {
                           if(needToShowMessage) {
                               SharedMethods.showMessage(Language.getLanguageSpecificString(rule.getMessage()),
                                   JOptionPane.ERROR_MESSAGE);
                               return false;
                           } else {
                               errorMessage[0].append(rule.getMessage());
                               return false;
                           }
                       }
                   } catch (Exception evalError) {
                       //evalError.printStackTrace();
                   }
               }
           }
       } catch (Exception e) {
           e.printStackTrace();
       } finally {
           interpreter = null;
       }
       return true;
   }
    */

    public boolean isStopLossEnabledForExchanges() {
        Exchange exchange;
        String[] types;
        Rule rule;
        try {
            rule = RuleManager.getSharedInstance().getRule("ORDER_TYPES", "*", "NEW_ORDER"); //BUG ID <#????>
            types = rule.getRule().split(",");
            for (int i = 0; i < types.length; i++) {
                if ((types[i].equals("" + TradeMeta.ORDER_TYPE_STOPLOSS_MARKET)) || (types[i].equals("" + TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT))) {
                    return true;
                }
            }
        } catch (Exception ex) {
        }
        Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            try {
                exchange = exchanges.nextElement();
                rule = RuleManager.getSharedInstance().getRule("ORDER_TYPES", exchange.getSymbol(), "NEW_ORDER");
                types = rule.getRule().split(",");
                for (int i = 0; i < types.length; i++) {
                    if ((types[i].equals("" + TradeMeta.ORDER_TYPE_STOPLOSS_MARKET)) || (types[i].equals("" + TradeMeta.ORDER_TYPE_STOPLOSS_LIMIT))) {
                        return true;
                    }
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return false;
    }

    public boolean isPasswordValid(String password, byte type) {
        if (type == TradingConstants.LEVEL1_TRADING_PASSWORD) {
            Rule rule = RuleManager.getSharedInstance().getRule("L1_VALIDATION", "*", "BROKER");
            if (rule != null) {
                try {
                    Interpreter interpreter = new Interpreter();
                    interpreter.set("username", TradingShared.getTradeUserName());
                    interpreter.set("password", password);
                    boolean result = ((Boolean) interpreter.eval(rule.getRule()));
                    if (result == true) {
                        SharedMethods.showMessage(Language.getLanguageSpecificString(rule.getMessage()),
                                JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                } catch (EvalError evalError) {
                    evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                return true;
            } else {
                return true;
            }
        } else if (type == TradingConstants.LEVEL2_TRADING_PASSWORD) {
            Rule rule = RuleManager.getSharedInstance().getRule("L2_VALIDATION", "*", "BROKER");
            if (rule != null) {
                try {
                    Interpreter interpreter = new Interpreter();
                    interpreter.set("username", TradingShared.getTradeUserName());
                    interpreter.set("password", password);
                    boolean result = ((Boolean) interpreter.eval(rule.getRule()));
                    if (result == true) {
                        SharedMethods.showMessage(Language.getLanguageSpecificString(rule.getMessage()),
                                JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                } catch (EvalError evalError) {
                    evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                return true;
            } else {
                return true;
            }
        }
        return false;
    }

    public boolean isPasswordComplexityValid(String password, byte type) {
        if (type == TradingConstants.LEVEL1_TRADING_PASSWORD) {
            Rule rule = RuleManager.getSharedInstance().getRule("L1_VALIDATION", "*", "COMPLEXITY");
            if (rule != null) {
                try {
                    Object[] resultarr = evaluateRule(rule, password);

                    boolean result = ((Boolean) resultarr[0]);
                    if (result == false) {
                        SharedMethods.showMessage(Language.getLanguageSpecificString((String) resultarr[1]),
                                JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                } catch (Exception evalError) {
                    evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                return true;
            } else {
                return true;
            }
        } else if (type == TradingConstants.LEVEL2_TRADING_PASSWORD) {
            Rule rule = RuleManager.getSharedInstance().getRule("L2_VALIDATION", "*", "COMPLEXITY");
            if (rule != null) {
                try {
                    Object[] resultarr = evaluateRule(rule, password);

                    boolean result = ((Boolean) resultarr[0]);
                    if (result == false) {
                        SharedMethods.showMessage(Language.getLanguageSpecificString((String) resultarr[1]),
                                JOptionPane.ERROR_MESSAGE);
                        return false;
                    }
                } catch (Exception evalError) {
                    evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                return true;
            } else {
                return true;
            }
        }
        return false;
    }

    private Object[] evaluateRule(Rule rule, String password) {
        Object[] reply = new Object[2];
        Boolean pass = true;
        String Errormsg = "";
        reply[0] = pass;
        reply[1] = Errormsg;
        String ruleStr = rule.getRule();
        String tradeusername = TradingShared.getTradeUserName();
        String[] regexlist = ruleStr.split(";");
        String[] regexerrlist = rule.getMessage().split(";");
        if (regexlist != null) {
            for (int i = 0; i < regexlist.length; i++) {
                String regx = regexlist[i];
                boolean tradeuser = false;
                try {
                    if (regx.contains("tradeuser")) {
                        regx = regx.replaceFirst("\\[tradeuser\\]", tradeusername);
                        tradeuser = true;
                    }
                    Pattern pattern = Pattern.compile(regx);
                    Matcher matcher = pattern.matcher(password);
                    boolean match = matcher.find();
                    if (tradeuser) {
                        match = !match;
                    }
                    if (!match) {
                        pass = false;
                        try {
                            Errormsg = regexerrlist[i];
                        } catch (Exception e) {
                            Errormsg = Language.getString("TRADING_PASSWORD_COMPLEXITY");
                        }
                        reply[0] = pass;
                        reply[1] = Errormsg;
                        return reply;
                    }
                } catch (Exception e) {
                    continue;
                }
            }
        }
        return reply;
    }


    public void populateBookKeepers(String portfolio, ArrayList list) {
        list.clear();
        try {
            ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
            for (TradingPortfolioRecord portflio : portfolios) {
                if (portflio.getPortfolioID().equals(portfolio)) {
                    ArrayList<BookKeeper> bk = portflio.getBookKeepers();
                    for (BookKeeper bks : bk) {
                        TWComboItem item = new TWComboItem(bks.getCode(), bks.getName());
                        list.add(item);

                    }
                }

            }
        } catch (Exception e) {
            e.printStackTrace();

        }
    }

    public int getBookKeeper(String portfolio, ArrayList list, String bookKeeper) {
        int index = 0;
        try {
            ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
            for (TradingPortfolioRecord portflio : portfolios) {
                if (portflio.getPortfolioID().equals(portfolio)) {
                    ArrayList<BookKeeper> bk = portflio.getBookKeepers();
                    for (int i = 0; i < bk.size(); i++) {
                        if ((bk.get(i)).getCode().equals(bookKeeper)) {
                            index = i;
                        }

                    }
                }

            }
        } catch (Exception e) {
            index = 0;

        }
        return index;
    }

    public int getDefaultBookKeeper(String portfolio, ArrayList list) {
        int index = 0;
        try {
            ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
            for (TradingPortfolioRecord portflio : portfolios) {
                if (portflio.getPortfolioID().equals(portfolio)) {
                    ArrayList<BookKeeper> bk = portflio.getBookKeepers();
                    for (int i = 0; i < bk.size(); i++) {
                        if ((bk.get(i)).isDefault()) {
                            index = i;
                        }

                    }
                }

            }
        } catch (Exception e) {
            index = 0;

        }
        return index;
    }

    public void populateOrderTypes(String exchange, ArrayList list, byte path) {
        list.clear();
        try {
            Rule rule = RuleManager.getSharedInstance().getRule("ORDER_TYPES", exchange, "NEW_ORDER");
//            Rule rule = RuleManager.getSharedInstance().getRule(exchange, "ORDER_TYPES"); //BUG ID <#????>
            if (rule == null) {
                rule = RuleManager.getSharedInstance().getRule("ORDER_TYPES", "*", "NEW_ORDER"); //BUG ID <#????>
//                rule = RuleManager.getSharedInstance().getRule("*", "ORDER_TYPES");
            }
            /* if (path == Constants.PATH_PRIMARY) {
                String[] types = {"1", "2", "3", "4",};     // commented by udaya default order types do not include "3" and "4"
                TradingShared.populateOrderTypes(types, list, path);
                return;
            }*/
            if (rule != null) {
//                Interpreter interpreter = new Interpreter();

                try {
//                    interpreter.set("exchange", exchange);
//                    String result = (String) (interpreter.eval(rule.getRule()));
//                    String[] types = result.split(","); //BUG ID <#????>
                    String[] types = rule.getRule().split(",");
                    TradingShared.populateOrderTypes(types, list, path);
//                    result = null;
                } catch (Exception evalError) {
                    TradingShared.populateOrderTypes(getDefaultOrderTypes(), list, path);
                    evalError.printStackTrace();
                }
//                interpreter = null;
            } else {

                TradingShared.populateOrderTypes(getDefaultOrderTypes(), list, path);
            }
            rule = null;
        } catch (Exception e) {
            TradingShared.populateOrderTypes(getDefaultOrderTypes(), list, path);
            e.printStackTrace();
        }
//        Collections.sort(list);
    }

    public void populateMiniTradeOrderTypes(String exchange, ArrayList list, byte path) {
        list.clear();
        try {
            Rule rule = RuleManager.getSharedInstance().getRule("MINI_TRADE_ORDER_TYPES", exchange, "NEW_ORDER");
//            Rule rule = RuleManager.getSharedInstance().getRule(exchange, "ORDER_TYPES"); //BUG ID <#????>
            if (rule == null) {
                rule = RuleManager.getSharedInstance().getRule("MINI_TRADE_ORDER_TYPES", "*", "NEW_ORDER"); //BUG ID <#????>
//                rule = RuleManager.getSharedInstance().getRule("*", "ORDER_TYPES");
            }
            if (path == Constants.PATH_PRIMARY) {
                String[] types = {"1", "2"};
                TradingShared.populateOrderTypes(types, list, path);
                return;
            }
            if (rule != null) {
//                Interpreter interpreter = new Interpreter();

                try {
//                    interpreter.set("exchange", exchange);
//                    String result = (String) (interpreter.eval(rule.getRule()));
//                    String[] types = result.split(","); //BUG ID <#????>
                    String[] types = rule.getRule().split(",");
                    TradingShared.populateOrderTypes(types, list, path);
//                    result = null;
                } catch (Exception evalError) {
                    TradingShared.populateOrderTypes(getDefaultOrderTypes(), list, path);
                    evalError.printStackTrace();
                }
//                interpreter = null;
            } else {

                TradingShared.populateOrderTypes(getDefaultOrderTypes(), list, path);
            }
            rule = null;
        } catch (Exception e) {
            TradingShared.populateOrderTypes(getDefaultOrderTypes(), list, path);
            e.printStackTrace();
        }
//        Collections.sort(list);
    }

    public void populateRapidOrderTypes(String exchange, ArrayList list) {
        try {
            Rule rule = RuleManager.getSharedInstance().getRule("ORDER_TYPES", exchange, "NEW_ORDER");
//            Rule rule = RuleManager.getSharedInstance().getRule(exchange, "ORDER_TYPES"); //BUG ID <#????>
            if (rule == null) {
                rule = RuleManager.getSharedInstance().getRule("ORDER_TYPES", "*", "NEW_ORDER"); //BUG ID <#????>
//                rule = RuleManager.getSharedInstance().getRule("*", "ORDER_TYPES");
            }
            if (rule != null) {
//                Interpreter interpreter = new Interpreter();

                try {
//                    interpreter.set("exchange", exchange);
//                    String result = (String) (interpreter.eval(rule.getRule()));
//                    String[] types = result.split(","); //BUG ID <#????>
                    String[] types = rule.getRule().split(",");
                    TradingShared.populateRapidOrderTypes(types, list);
//                    result = null;
                } catch (Exception evalError) {
                    TradingShared.populateRapidOrderTypes(getDefaultOrderTypes(), list);
                    evalError.printStackTrace();
                }
//                interpreter = null;
            } else {

                TradingShared.populateRapidOrderTypes(getDefaultOrderTypes(), list);
            }
            rule = null;
        } catch (Exception e) {
            TradingShared.populateRapidOrderTypes(getDefaultOrderTypes(), list);
            e.printStackTrace();
        }
//        Collections.sort(list);
    }


    private String[] getDefaultOrderTypes() {
        String[] types = {"1", "2"};
        return types;
    }

    public WithdrawalRequestFrame showWithdrawalRequest(boolean isCheck) {
        WithdrawalRequestFrame withdrawalRequest = new WithdrawalRequestFrame(isCheck);
        Client.getInstance().getDesktop().add(withdrawalRequest);
        withdrawalRequest.setLayer(GUISettings.TOP_LAYER);
        withdrawalRequest.show();
        return withdrawalRequest;
    }

    public FundTransferUI showDepositeFrame() {
        FundTransferUI fundTranferFrame = new FundTransferUI();
        fundTranferFrame.setLayer(GUISettings.TOP_LAYER);
        Client.getInstance().getDesktop().add(fundTranferFrame);
        fundTranferFrame.show();
        return fundTranferFrame;
    }

    public void showCashTransferFrame() {
        CashTransferUI cashTransferUI = new CashTransferUI();
        cashTransferUI.setLayer(GUISettings.TOP_LAYER);
        Client.getInstance().getDesktop().add(cashTransferUI);
        cashTransferUI.show();
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "CashTransfer");
    }

    public void showIPOSubscribeFrame() {
        IPOSubscriptionUI ipoUI = IPOSubscriptionUI.getSharedInstance();
        ipoUI.setLayer(GUISettings.TOP_LAYER);
        Client.getInstance().getDesktop().add(ipoUI);
        ipoUI.show();
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "IpoSubscribe");
    }

    public void showMarginableSymbolFrame() {
        if (marginableUI == null) {
            marginableUI = MarginableSymbolWindow.getSharedInstance();
            marginableUI.setLayer(GUISettings.TOP_LAYER);
            Client.getInstance().getDesktop().add(marginableUI);
            marginableUI.show();
        } else {
            marginableUI.show();
        }
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Trading, "marginableSymbolWindow");
    }

    public void pendingFundTransfer(byte path) {
        try {
            MIXObject pendingFundTransfer = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_FINANCE_AND_HOLDING, MIXConstants.REQUEST_TYPE_DEPOSIT_WITHDRAWAL_LIST, path);
//            TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_FUND_TRANSFER_PENDING);
//            tradeMessage.addData("B" + TradingShared.getTrader().getMubasherID(path));
//            tradeMessage.addData("A" + TradingShared.getTrader().getUserID(path));
            CashTransfer pendingDW = new CashTransfer();
            pendingDW.setSourceMubasherNo(TradingShared.getTrader().getMubasherID(path));
            pendingDW.setUserID(TradingShared.getTrader().getUserID(path));
            pendingFundTransfer.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{pendingDW});
            // SendQueue.getSharedInstance().writeData(tradeMessage.toString(), path);
//            SendQueue.getSharedInstance().writeData(pendingFundTransfer.getMIXString(), path);
            //  tradeMessage = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void pendingStockTransfer(byte path) {
        try {
            TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_STOCK_TRANSFER_PENDING);
            tradeMessage.addData("B" + TradingShared.getTrader().getMubasherID(path));
            tradeMessage.addData("A" + TradingShared.getTrader().getUserID(path));
            SendQueue.getSharedInstance().writeData(tradeMessage.toString(), path);
            tradeMessage = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public StockTransferWindow showStockTransferWindow() {
        StockTransferWindow stockTranferFrame = new StockTransferWindow();
        stockTranferFrame.setLayer(GUISettings.TOP_LAYER);
        Client.getInstance().getDesktop().add(stockTranferFrame);
        stockTranferFrame.show();
        return stockTranferFrame;
    }

    public boolean sendCashTransferRequest(String fromPortfolio, String toPortfolio, String amount) {
        boolean authenticated = false;
        Account fromAccount = TradingShared.getTrader().findAccountByPortfolio(fromPortfolio);
        Account toAccount = TradingShared.getTrader().findAccountByPortfolio(toPortfolio);

        if (toAccount.getAccountNumber().equals(fromAccount.getAccountNumber())) {
            SharedMethods.showMessage(Language.getString("MSG_TRANSFER_SAME_ACCOUNT"), JOptionPane.ERROR_MESSAGE);
            return authenticated;
        }
        try {
            if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
                TradeMethods.getSharedInstance().blockTillValidated();
                if (TradingShared.level2AuthenticationSucess) {
                    authenticated = true;
                }
            }
            byte fromPath = TradingShared.getTrader().getPath(fromPortfolio);
            byte toPath = TradingShared.getTrader().getPath(toPortfolio);
            if (authenticated) {
                TradeMessage tradeMessage;
                CashTransfer transfer = new CashTransfer();
                MIXObject cashTransfer;
                if (fromPath == toPath) {
                    cashTransfer = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_FINANCE_AND_HOLDING, MIXConstants.REQUEST_TYPE_CASH_TRANSFER, fromPath);
                    tradeMessage = new TradeMessage(TradeMeta.MT_CASH_TRANSFER);
                } else {                                                                       //todo 3rd party cash transfer
//                    cashTransfer  = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_FINANCE_AND_HOLDING, MIXConstants.REQUEST_TYPE_CASH_TRANSFER, fromPath);
                    cashTransfer = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_FINANCE_AND_HOLDING, MIXConstants.REQUEST_TYPE_THIRD_PARTY_CASH_TRANSFER, fromPath);
                    tradeMessage = new TradeMessage(TradeMeta.MT_3rd_Party_CASH_TRANSFER);
                }
                tradeMessage.addData("A" + TradingShared.getTrader().getUserID(fromPortfolio));
                tradeMessage.addData("B" + TradingShared.getTrader().getMubasherID(fromPortfolio));
                tradeMessage.addData("C" + fromAccount.getAccountNumber());
                tradeMessage.addData("D" + toAccount.getAccountNumber());
                tradeMessage.addData("E" + amount);
                tradeMessage.addData("F" + TradingShared.getTrader().getBrokerID(toPortfolio));
                tradeMessage.addData("G" + TradingShared.getTrader().getMubasherID(toPortfolio));

                transfer.setUserID(TradingShared.getTrader().getUserID(fromPortfolio));
                transfer.setSourceMubasherNo(TradingShared.getTrader().getMubasherID(fromPortfolio));
                transfer.setTargetMubasherNo(TradingShared.getTrader().getMubasherID(toPortfolio));
                transfer.setFromAccount(fromAccount.getAccountNumber());
                transfer.setToAccount(toAccount.getAccountNumber());
                transfer.setAmount(Double.parseDouble(amount));
                transfer.setBroker(TradingShared.getTrader().getBrokerID(toPortfolio));
                cashTransfer.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{transfer});
                //SendQueue.getSharedInstance().addData(tradeMessage.toString(), fromPath);
                SendQueue.getSharedInstance().addData(cashTransfer.getMIXString(), fromPath);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return authenticated;
    }

    public boolean sendStockTransferRequest(String portfolioID, String symbol, String exchange, String broker, long quantity, double avgCost) {
        boolean authenticated = false;
        try {
            if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
                TradeMethods.getSharedInstance().blockTillValidated();
                if (TradingShared.level2AuthenticationSucess) {
                    authenticated = true;
                }
            }
            if (authenticated) {
                TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_STOCK_TRANSFER);
                tradeMessage.addData("A" + portfolioID);
                tradeMessage.addData("B" + symbol);
                tradeMessage.addData("C" + UnicodeUtils.getUnicodeString(broker)); // this fied is depricated
                tradeMessage.addData("D" + quantity);
                tradeMessage.addData("E" + avgCost);
                tradeMessage.addData("F" + TradingShared.getTrader().getUserID(portfolioID));
                tradeMessage.addData("G" + TradingShared.getTrader().getMubasherID(portfolioID));
                tradeMessage.addData("H" + exchange);
                SendQueue.getSharedInstance().addData(tradeMessage.toString(), TradingShared.getTrader().getPath(portfolioID));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return authenticated;
    }

    public boolean sendWinthdrawalRequest(int mode, double amount, String bank, String accoutNumber, String portfolio, String location, String currency) {
        boolean authenticated = false;
        try {
            if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
                TradeMethods.getSharedInstance().blockTillValidated();
                if (TradingShared.level2AuthenticationSucess) {
                    authenticated = true;
                }
            }
            if (authenticated) {
                TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_WITHDRAWAL);
                tradeMessage.addData("A" + mode);
                tradeMessage.addData("B" + amount);
                tradeMessage.addData("C" + "0"); // this fied is depricated
                tradeMessage.addData("D" + "0");
                tradeMessage.addData("E" + TradingShared.getTrader().getMubasherID(portfolio));
                tradeMessage.addData("F" + bank);
                tradeMessage.addData("G" + accoutNumber);
                tradeMessage.addData("H" + portfolio);
                tradeMessage.addData("I" + location);
                MIXObject withdrawalReq = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_FINANCE_AND_HOLDING, MIXConstants.REQUEST_TYPE_WITHDRAWAL, TradingShared.getTrader().getPath(portfolio));
                CashTransfer withdraw = new CashTransfer();
                withdraw.setPayMethod(mode + "");
                withdraw.setAmount(amount);
                withdraw.setSourceMubasherNo(TradingShared.getTrader().getMubasherID(portfolio));
                withdraw.setBank(bank);
                withdraw.setBankID(bank);
                withdraw.setAccountID(accoutNumber);
                withdraw.setPortfolioID(portfolio);
                withdraw.setLocation(location);
                withdraw.setReferenceID(System.currentTimeMillis() + "");
                withdraw.setCurrency(currency);
                withdrawalReq.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{withdraw});
                //SendQueue.getSharedInstance().addData(tradeMessage.toString(), TradingShared.getTrader().getPath(portfolio));
                SendQueue.getSharedInstance().addData(withdrawalReq.getMIXString(), TradingShared.getTrader().getPath(portfolio));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return authenticated;
    }

    /* public static CashTransactionUI sendCashStatement(){
        CashTransactionUI cashStatementUI =CashTransactionUI.getSharedInstance();
        cashStatementUI.setVisible(true);
        return cashStatementUI;
      /*  try {
          TradeMessage tradeMessage = null;
          tradeMessage = new TradeMessage(TradeMeta.MT_CASH_SUMMARY);
          tradeMessage.addData(str1);
          tradeMessage.addData(str2);
          tradeMessage.addData(TradingShared.getTradeUserName());
          com.isi.csvr.trading.SendQueue.getSharedInstance().writeData(tradeMessage.toString());
          tradeMessage = null;
      } catch (Exception e) {
          e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
      }
    }*/

    private static SimpleDateFormat fundTransferDate = new SimpleDateFormat("yyyyMMdd");

    public boolean fundTransferRequest(int type, String currency, String bank, String bankAddress, String chequeNo,
                                       Calendar chequeDate, String amount, Calendar depositeDate, String reference,
                                       String notes, String bankID, String branch, long referenceID, String portfolio, String bankAccountNo) {

        boolean authenticated = false;

        if (TradeMethods.getSharedInstance().isTradingAuthenticationInitiated()) {
            TradeMethods.getSharedInstance().blockTillValidated();
            if (TradingShared.level2AuthenticationSucess) {
                authenticated = true;
            }
        }
        TradeMessage tradeMessage = null;
        if (authenticated) {
            try {
                tradeMessage = new TradeMessage(TradeMeta.MT_DEPOSITE);
                MIXObject depositRequest = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_FINANCE_AND_HOLDING, MIXConstants.REQUEST_TYPE_DEPOSITE, TradingShared.getTrader().getPath(portfolio));
                CashTransfer deposit = new CashTransfer();
                tradeMessage.addData("A" + type);
                tradeMessage.addData("B" + currency);
                deposit.setPayMethod(type + "");
                deposit.setCurrency(currency);

                if (type == TradeMeta.TRANSFER_CHEQUE) {
//                    tradeMessage.addData("C" + UnicodeUtils.getUnicodeString(bank));
//                    tradeMessage.addData("E" + UnicodeUtils.getUnicodeString(chequeNo));
//                    tradeMessage.addData("F" + fundTransferDate.format(chequeDate.getTime()));
//                    tradeMessage.addData("H" + fundTransferDate.format(depositeDate.getTime()));
                    deposit.setBank(UnicodeUtils.getUnicodeString(bank));
                    deposit.setChequeNumber(UnicodeUtils.getUnicodeString(chequeNo));
                    deposit.setChequeDate(fundTransferDate.format(chequeDate.getTime()));
                    deposit.setDepositDate(fundTransferDate.format(depositeDate.getTime()));        // todo add Deposit date


                } else if (type == TradeMeta.TRANSFER_BANK) {
//                    tradeMessage.addData("C" + UnicodeUtils.getUnicodeString(bank));
//                    tradeMessage.addData("D" + UnicodeUtils.getUnicodeString(bankAddress));
                    deposit.setBank(UnicodeUtils.getUnicodeString(bank));
                    deposit.setBankAddress(UnicodeUtils.getUnicodeString(bankAddress));

                    //tradeMessage.addData("I" + UnicodeUtils.getUnicodeString(reference));
                } else {
//                    tradeMessage.addData("H" + fundTransferDate.format(depositeDate.getTime()));
//                    tradeMessage.addData("K" + bankID);
//                    tradeMessage.addData("L" + UnicodeUtils.getUnicodeString(branch));
                    deposit.setDepositDate(fundTransferDate.format(depositeDate.getTime()));
                    deposit.setBankID(bankID);
                    deposit.setAccountID(bankAccountNo);
                    deposit.setBranch(UnicodeUtils.getUnicodeString(branch));

                }
                tradeMessage.addData("I" + UnicodeUtils.getUnicodeString(reference));
                tradeMessage.addData("M" + referenceID);
                tradeMessage.addData("G" + amount);
                tradeMessage.addData("N" + TradingShared.getTrader().getMubasherID(portfolio));

                deposit.setBankReferenceNo(UnicodeUtils.getUnicodeString(reference));   //todo check for reference
                deposit.setReferenceID(referenceID + "");
                deposit.setAmount(Double.parseDouble(amount));
                deposit.setSourceMubasherNo(TradingShared.getTrader().getMubasherID(portfolio));


                if (notes.equals("")) {
                } else {
//                    tradeMessage.addData("J" + UnicodeUtils.getUnicodeString(notes));
                    deposit.setNotes(notes);
                }
//                tradeMessage.addData("O" + portfolio);
                deposit.setPortfolioID(portfolio);
                depositRequest.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{deposit});
                //  SendQueue.getSharedInstance().writeData(tradeMessage.toString(), TradingShared.getTrader().getPath(portfolio));
                SendQueue.getSharedInstance().writeData(depositRequest.getMIXString(), TradingShared.getTrader().getPath(portfolio));
                tradeMessage = null;
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return authenticated;
    }

    public static ArrayList<String[]> getBanks(String bankIDs) {

        /* bankIDs format
                id1|account number1,id2|account number2, ...
                a person may have many accounts in a bank. In this case
                id repeats.
        */
        String sLangSpecFileName = Settings.SYSTEM_PATH + "/banks_" + Language.getSelectedLanguage() + ".msf";
        ArrayList<String[]> records = new ArrayList<String[]>();
        SmartProperties bankData = null;
        try {
            bankData = new SmartProperties(Meta.DS);
            bankData.loadCompressed(sLangSpecFileName);
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            String[] accountData = bankIDs.split(",");
            for (String id : accountData) {
                String[] account = id.split("\\|");
                if (account.length == 2) { // if no account specified, ignore it
                    String[] accountDetails = new String[3];
                    accountDetails[0] = account[0];
                    accountDetails[1] = account[1];
                    try {
                        String[] fields = bankData.getProperty(account[0]).split(Meta.DS);
                        accountDetails[2] = Language.getLanguageSpecificString(fields[1]);
                        if (accountDetails[2].equals("")) {
                            accountDetails[2] = Language.getString("NA");
                        }
                        fields = null;
                    } catch (Exception e) {
                        accountDetails[2] = Language.getString("NA");
                        // bankData may be null
                    }
                    records.add(accountDetails);
                }
                account = null;
            }

            /*Decompress decompress = new Decompress();
       ByteArrayOutputStream out = decompress.setFiles("system/banks.msf");
       decompress.decompress();
       DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));*/

            /*String line;
            while (true) {
                line = in.readLine();
                if (line == null) break;
                try {
                    String[] fields = line.split(Meta.DS);
                    if (preferedIDs.containsKey(fields[0])) {
                        String[] record = new String[2];
                        record[0] = fields[0]; // id
                        record[1] = Language.getLanguageSpecificString(fields[1]); // description
                        records.add(record);
                        record = null;
                    }
                    fields = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            decompress = null;
            out = null;*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        return records;
    }

    public static ArrayList<String[]> getBanks(String[] bankIDs) {

        /* bankIDs format
                id1|account number1,id2|account number2, ...
                a person may have many accounts in a bank. In this case
                id repeats.
        */
        String sLangSpecFileName = Settings.getAbsolutepath() + "system/banks_" + Language.getSelectedLanguage() + ".msf";
        File f = new File(sLangSpecFileName);
        boolean bIsLangSpec = f.exists();
        ArrayList<String[]> records = new ArrayList<String[]>();
        SmartProperties bankData = null;
        try {
            bankData = new SmartProperties(Meta.DS);
            if (bIsLangSpec) {
                bankData.loadCompressed(sLangSpecFileName);
            } else {
                bankData.loadCompressed(Settings.getAbsolutepath() + "system/banks.msf");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        try {
            String[] accountData = bankIDs;
            for (String id : accountData) {
                try {
                    String[] account = id.split(TradeMeta.FD);
                    if (account.length == 2) { // if no account specified, ignore it
                        String[] accountDetails = new String[3];
                        accountDetails[0] = account[0];
                        accountDetails[1] = account[1];
                        try {
                            String[] fields = bankData.getProperty(account[0]).split(Meta.DS);
                            accountDetails[2] = Language.getLanguageSpecificString(fields[1]);
                            if (accountDetails[2].equals("")) {
                                accountDetails[2] = Language.getString("NA");
                            }
                            fields = null;
                        } catch (Exception e) {
                            accountDetails[2] = Language.getString("NA");
                            // bankData may be null
                        }
                        records.add(accountDetails);
                    }
                    account = null;
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }

            /*Decompress decompress = new Decompress();
       ByteArrayOutputStream out = decompress.setFiles("system/banks.msf");
       decompress.decompress();
       DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));*/

            /*String line;
            while (true) {
                line = in.readLine();
                if (line == null) break;
                try {
                    String[] fields = line.split(Meta.DS);
                    if (preferedIDs.containsKey(fields[0])) {
                        String[] record = new String[2];
                        record[0] = fields[0]; // id
                        record[1] = Language.getLanguageSpecificString(fields[1]); // description
                        records.add(record);
                        record = null;
                    }
                    fields = null;
                } catch (Exception e) {
                    e.printStackTrace();
                }
            decompress = null;
            out = null;*/
        } catch (Exception e) {
            e.printStackTrace();
        }
        return records;
    }

    public static ArrayList<String[]> getBanks(ArrayList<BankRecord> banklist) {

        /* bankIDs format
                id1|account number1,id2|account number2, ...
                a person may have many accounts in a bank. In this case
                id repeats.
        */
        String sLangSpecFileName = Settings.getAbsolutepath() + "system/banks_" + Language.getSelectedLanguage() + ".msf";
        File f = new File(sLangSpecFileName);
        boolean bIsLangSpec = f.exists();
        ArrayList<String[]> records = new ArrayList<String[]>();
        SmartProperties bankData = null;
        try {
            bankData = new SmartProperties(Meta.DS);
            if (bIsLangSpec) {
                bankData.loadCompressed(sLangSpecFileName);
            } else {
                bankData.loadCompressed(Settings.getAbsolutepath() + "system/banks.msf");
            }
        } catch (Exception e) {
            e.printStackTrace();
        }


        for (int i = 0; i < banklist.size(); i++) {
            BankRecord bank = banklist.get(i);
            String[] accountDetails = new String[3];
            accountDetails[0] = bank.getBankCode();
            accountDetails[1] = bank.getAccountNumber();
            try {
                String[] fields = bankData.getProperty(bank.getBankCode()).split(Meta.DS);
                accountDetails[2] = Language.getLanguageSpecificString(fields[1]);
                if (accountDetails[2].equals("")) {
                    accountDetails[2] = Language.getString("NA");
                }
                fields = null;
            } catch (Exception e) {
                accountDetails[2] = Language.getString("NA");
                // bankData may be null
            }
            records.add(accountDetails);
            bank = null;
        }

//        try {
//            String[] accountData = bankIDs;
//            for (String id : accountData) {
//                try {
//                    String[] account = id.split(TradeMeta.FD);
//                    if (account.length == 2) { // if no account specified, ignore it
//                        String[] accountDetails = new String[3];
//                        accountDetails[0] = account[0];
//                        accountDetails[1] = account[1];
//                        try {
//                            String[] fields = bankData.getProperty(account[0]).split(Meta.DS);
//                            accountDetails[2] = Language.getLanguageSpecificString(fields[1]);
//                            if (accountDetails[2].equals("")) {
//                                accountDetails[2] = Language.getString("NA");
//                            }
//                            fields = null;
//                        } catch (Exception e) {
//                            accountDetails[2] = Language.getString("NA");
//                            // bankData may be null
//                        }
//                        records.add(accountDetails);
//                    }
//                    account = null;
//                }
//                catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }

//        } catch (Exception e) {
//            e.printStackTrace();
//        }
        return records;
    }

    public AccountWindow getAccountWindow() {
        if (accountFrame == null) {
            createAccountWindow();
        }
        return accountFrame;
    }

    public static Date getDateFromDays(int days) {
        //todo - in here need to pass the portfolio to get exact current day  - dilum  -31/08/2007
        Calendar cal = Calendar.getInstance();
        try {
            cal.set(Integer.parseInt(TradingShared.getCurrentDay().substring(0, 4)),
                    Integer.parseInt(TradingShared.getCurrentDay().substring(4, 6)) - 1,
                    Integer.parseInt(TradingShared.getCurrentDay().substring(6, 8)),
                    23, 59, 59);
        } catch (Exception e) {
            e.printStackTrace();
        }
        cal.add(Calendar.DATE, days);
        return cal.getTime();
    }

    public static void sendEdittedPortfolioData(String portfolioID, String name) {
        try {
            MIXObject request = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_SYSTEM, MIXConstants.REQUEST_TYPE_EDIT_PORTFOLIO_ALIAS, TradingShared.getTrader().getPath(portfolioID));
            EditPortfolioAliasRequest editReq = new EditPortfolioAliasRequest();
            editReq.setPortfolioAlias(UnicodeUtils.getUnicodeString(name));
            editReq.setPortfolioNumber(portfolioID);
            // TradeMessage message = new TradeMessage(TradeMeta.MT_EDIT_PORTFOLIO);
            //   message.addData("A" + portfolioID);
            //  message.addData("B" + UnicodeUtils.getUnicodeString(name));
//            message.addData("C" + TradingShared.getTradeUserName());
            request.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{editReq});
            SendQueue.getSharedInstance().writeData(request.getMIXString(), TradingShared.getTrader().getPath(portfolioID));
            //message = null;
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static void requestPortfolioData(byte path) {
        try {
            TradeMessage message;
            MIXObject mixReq;
            MIXHeader mixHeader;
            //com.dfn.mtr.mix.beans.TransferObject transferObjects;
            InquiryObject inquary = null;
            boolean isAnyPortfolioMarginable = false;

            int portfolioCount = TradingShared.getTrader().getPortfolioCount();

            for (int i = 0; i < portfolioCount; i++) {
                if (TradingShared.getTrader().getPath(TradingShared.getTrader().getPortfolioID(i)) == path) {
                    mixReq = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING_INQUIRY, MIXConstants.REQUEST_TYPE_ORDER_LIST, path);
                    OrderSearchInquiryObject inquaryOrderList4 = new OrderSearchInquiryObject();
                    inquaryOrderList4.setOrdPortfolioID(TradingShared.getTrader().getPortfolioID(i));
                    inquaryOrderList4.setOrdUserID(TradingShared.getTrader().getUserID(path));
                    // inquaryOrderList4.setOrdGroup("1");
                    inquaryOrderList4.setOrdGroup(MIXConstants.ORDER_GROUP_ALL + "");
                    mixReq.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inquaryOrderList4});
                    SendQueue.getSharedInstance().addData(mixReq.getMIXString(), path);
                    mixReq = null;
                    mixHeader = null;
                    inquary = null;

                    mixReq = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING_INQUIRY, MIXConstants.REQUEST_TYPE_ORDER_LIST, path);
                    OrderSearchInquiryObject inquaryOrderList = new OrderSearchInquiryObject();
                    inquaryOrderList.setOrdPortfolioID(TradingShared.getTrader().getPortfolioID(i));
                    inquaryOrderList.setOrdUserID(TradingShared.getTrader().getUserID(path));
                    // inquaryOrderList.setOrdGroup("2");
                    inquaryOrderList.setOrdGroup(MIXConstants.ORDER_GROUP_OPEN_ORDERS + "");
                    mixReq.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inquaryOrderList});
                    // SendQueue.getSharedInstance().addData(mixReq.getMIXString(), path);
                    mixReq = null;
                    mixHeader = null;
                    inquaryOrderList = null;

                    mixReq = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING_INQUIRY, MIXConstants.REQUEST_TYPE_ORDER_LIST, path);
                    OrderSearchInquiryObject inquaryOrderList2 = new OrderSearchInquiryObject();
                    inquaryOrderList2.setOrdPortfolioID(TradingShared.getTrader().getPortfolioID(i));
                    inquaryOrderList2.setOrdUserID(TradingShared.getTrader().getUserID(path));
                    //inquaryOrderList2.setOrdGroup("3");
                    inquaryOrderList2.setOrdGroup(MIXConstants.ORDER_GROUP_CONDITIONAL_ORDERS + "");
                    mixReq.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inquaryOrderList2});
//                    SendQueue.getSharedInstance().addData(mixReq.getMIXString(), path);
                    mixReq = null;
                    mixHeader = null;
                    inquary = null;

                    mixReq = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING_INQUIRY, MIXConstants.REQUEST_TYPE_ORDER_LIST, path);
                    OrderSearchInquiryObject inquaryOrderList3 = new OrderSearchInquiryObject();
                    inquaryOrderList3.setOrdPortfolioID(TradingShared.getTrader().getPortfolioID(i));
                    inquaryOrderList3.setOrdUserID(TradingShared.getTrader().getUserID(path));
                    // inquaryOrderList3.setOrdGroup("4");
                    inquaryOrderList3.setOrdGroup(MIXConstants.ORDER_GROUP_ALGO_ORDERS + "");
                    mixReq.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inquaryOrderList3});
//                    SendQueue.getSharedInstance().addData(mixReq.getMIXString(), path);
                    mixReq = null;
                    mixHeader = null;
                    inquaryOrderList = null;


                    mixReq = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_INQUIRY, MIXConstants.REQUEST_TYPE_PORTFOLIO, path);
                    inquary = new InquiryObject();
                    inquary.setPortfolioID(TradingShared.getTrader().getPortfolioID(i));
                    inquary.setUserID(TradingShared.getTrader().getUserID(path));
                    mixReq.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inquary});
                    SendQueue.getSharedInstance().addData(mixReq.getMIXString(), path);
                    mixReq = null;
                    mixHeader = null;
                    inquary = null;

                    if (!isAnyPortfolioMarginable && TradingShared.getTrader().getPortfolio(TradingShared.getTrader().getPortfolioID(i)).isMarginEnabled()) {
                        isAnyPortfolioMarginable = true;
                }

                }
            }

            if (BrokerConfig.getSharedInstance().isIntradayPositionMonitorAvailable(path)) {
                mixReq = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_TRADING_INQUIRY, MIXConstants.REQUEST_TYPE_INTRADAY_POSITION_MONITOR, path);
                OrderSearchInquiryObject intraday = new OrderSearchInquiryObject();
                intraday.setChannelID(mixReq.getMIXHeader().getChannelID() + "");
                intraday.setOrdUserID(TradingShared.getTrader().getMubasherID(path));
                mixReq.setTransferObjects(new TransferObject[]{intraday});
                SendQueue.getSharedInstance().addData(mixReq.getMIXString(), path);
            }

            if (isAnyPortfolioMarginable) {
                TradeMethods.requestMarginSymbols(Constants.PATH_PRIMARY);
                TradingShared.getTrader().setMarginEnabled(true);
                TradeMethods.getSharedInstance().getAccountWindow().checkMarginAvailability();
                Client.getInstance().getMenus().updateMarginableSymbolsMenu(true);
            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void requestAccountData(byte path) {
        try {
            TradeMessage message;
            Enumeration portfolios = TradingShared.getTrader().getAccountMap().elements();
            String portfolio;
            while (portfolios.hasMoreElements()) {
                try {
                    message = new TradeMessage(TradeMeta.MT_ACCOUNT);
                    portfolio = (String) portfolios.nextElement();
                    MIXHeader mixHeader = new MIXHeader();
                    mixHeader.setGroup(MIXConstants.GROUP_INQUIRY);
                    mixHeader.setRequestType(MIXConstants.REQUEST_TYPE_BUYING_POWER);
                    InquiryObject inq = new InquiryObject();
                    if (TradingShared.getTrader().getPath(portfolio) == path) {
                        message.addData(TradingShared.getTrader().getUserID(path));
                        message.addData(portfolio);
                        inq.setUserID(TradingShared.getTrader().getUserID(path));
                        inq.setPortfolioID(portfolio);
                        com.dfn.mtr.mix.beans.TransferObject[] mixTransferObjects = new com.dfn.mtr.mix.beans.TransferObject[]{inq};
                        MIXObject mixBasicObject = new MIXObject();
                        mixBasicObject.setMIXHeader(mixHeader);
                        mixBasicObject.setTransferObjects(mixTransferObjects);
                        //  SendQueue.getSharedInstance().addData(message.toString(),path);
                        SendQueue.getSharedInstance().addData(mixBasicObject.getMIXString(), path);
                    }
                    message = null;
                } catch (Exception e) {
                }
            }
            portfolios = null;
        } catch (Exception e) {

        }
    }

    public static void requestCurrencyData(byte path) {
        try {
//            TradeMessage message;
//            message = new TradeMessage(TradeMeta.MT_CURRENCY_DATA);
//            message.addData("A" + TradingShared.getTrader().getUserID(path));
//            SendQueue.getSharedInstance().addData(message.toString(), path);
            MIXObject mixReq = new MIXObject();
            MIXHeader mixHeader = new MIXHeader();
            mixHeader.setSessionID(MixStore.getSharedInstance().getAuthenticationResponse(path).getSSessionID());
            mixHeader.setGroup(MIXConstants.GROUP_FINANCE_AND_HOLDING);
            mixHeader.setRequestType(MIXConstants.REQUEST_TYPE_EXCHANGE_RATE);  //todo check for type
            InquiryObject inquary = new InquiryObject();
            inquary.setUserID(TradingShared.getTrader().getUserID(path));
            mixReq.setMIXHeader(mixHeader);
            mixReq.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inquary});
            SendQueue.getSharedInstance().addData(mixReq.getMIXString(), path);
            mixReq = null;
            mixHeader = null;
            inquary = null;

        } catch (Exception e) {

        }
    }

    public static void requestTPlusData(byte path) {
        try {
            //adding exchange for the T+0 request

            Enumeration<String> exchanges = TradingShared.getTrader().getExchanges();
            while (exchanges.hasMoreElements()) {
                String exg = exchanges.nextElement();
                TradeMessage message;
                message = new TradeMessage(TradeMeta.MT_TPLUS_SYMBOLS_NEW);
                message.addData("A" + 0);
                message.addData("B" + exg);
                SendQueue.getSharedInstance().addData(message.toString(), path);

                MIXObject mixReq = new MIXObject();
                MIXHeader mixHeader = new MIXHeader();
                mixHeader.setSessionID(MixStore.getSharedInstance().getAuthenticationResponse(path).getSSessionID());
                mixHeader.setGroup(MIXConstants.GROUP_INQUIRY);
                // mixHeader.setRequestType(MIXConstants.REQUEST_TYPE_);  //todo check for type
                InquiryObject inquary = new InquiryObject();
                // inquary.set                                     // todo cannot set parameters
                mixReq.setMIXHeader(mixHeader);
                mixReq.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inquary});
                SendQueue.getSharedInstance().addData(mixReq.getMIXString(), path);
                mixReq = null;
                mixHeader = null;
                inquary = null;
            }

            TradingShared.setReadyForTrading(true);
        } catch (Exception e) {

        }

    }

    public static void requestTPlusCommisionData(byte path) {
        try {
            TradeMessage message;
            message = new TradeMessage(TradeMeta.MT_TPLUS_COMMISIONS);
            message.addData("A" + TradingShared.getTrader().getUserID(path));
            SendQueue.getSharedInstance().addData(message.toString(), path);
            TradingShared.setReadyForTrading(true);
        } catch (Exception e) {

        }

    }
    /*public static boolean isDayMarketType(char orderType){
        switch (orderType){
            case TradeMeta.ORDER_TYPE_DAY_MARKET :
            case TradeMeta.ORDER_TYPE_DAY_LIMIT :
            case TradeMeta.ORDER_TYPE_SQUARE_OFF :
                return true;
            default:
                return false;
        }
    }*/

    public static double getEffectiveMarginForPortfolio(String portfolioID, boolean isDayOrder, double cashMargin, double dayCashMargin) {
        try {
            TradingPortfolioRecord portfolio = TradingShared.getTrader().getPortfolio(portfolioID);
            double margin = portfolio.getMarginPct() + cashMargin;
            if (isDayOrder) {
                margin = portfolio.getDayMarginPct() + dayCashMargin;
            }
            // return margin - portfolio.getMarginBlocked() - portfolio.getMarginDue();
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public static double getEffectiveDayMarginForPortfolio(String portfolioID) {
        try {
            TradingPortfolioRecord portfolio = TradingShared.getTrader().getPortfolio(portfolioID);
            // return portfolio.getDayMarginPct() - portfolio.getMarginBlocked() - portfolio.getMarginDue();
            return 0;
        } catch (Exception e) {
            return 0;
        }
    }

    public static double getEffectiveBuyingPowerForSymbol(String exchange, String symbol, int instrument, String portfolioID, boolean isDayOrder, double comission) {
        try {
            Account account = TradingShared.getTrader().findAccountByPortfolio(portfolioID);
            if (isMarginApplicableForPortfolio(portfolioID, exchange) && isMarginMapsValidForSymbol(SharedMethods.getKey(exchange, symbol, instrument))) {
                return MarginCalculator.getSharedInstance().getBuyingPowerForNewOrder(portfolioID, SharedMethods.getKey(exchange, symbol, instrument), comission);
            } else {
                return account.getBuyingPower();
            }
        } catch (Exception e) {
            return 0;
        }
    }

    private static boolean isMarginApplicableForPortfolio(String pfid, String exchage) {
        TradingPortfolioRecord record = TradingShared.getTrader().getPortfolio(pfid);
        if (record != null) {
            if ((record.isMarginEnabled() && record.isDayMarginEnabled()) || record.isMarginEnabled()) {
                return true;
            } else if (record.isDayMarginEnabled()) {
                return record.isDayMarginAvailable(exchage);
//                if (isMarketClose(exchage)) {
//                    return true;
//                } else {
//                    return !record.isMarginTransisitionPassed();
//                }
            } else {
                return false;
            }

        } else {
            return false;
        }
    }

    private static boolean isMarketClose(String exchange) {
        try {
            return ((ExchangeStore.getSharedInstance().getExchange(exchange)).getMarketStatus() == Meta.MARKET_CLOSE);
        } catch (Exception e) {
            return false;
        }
    }

    private static boolean isMarginMapsValidForSymbol(String sKey) {
        if (sKey != null || sKey.trim().isEmpty()) {
            return MarginSymbolStore.getSharedInstance().isMarginSymbolAvailable(new TradeKey(SharedMethods.getExchangeFromKey(sKey), SharedMethods.getSymbolFromKey(sKey), SharedMethods.getInstrumentTypeFromKey(sKey)));
        } else {
            return false;
        }
    }

    public static double getCurrentMarginCallPercentage(String exchange, String symbol, int instrument,
                                                        String portfolioID, long quantity, double price) {
        try {
            TransactRecord transactRecord = TradePortfolios.getInstance().findTransaction(portfolioID, SharedMethods.getKey(exchange, symbol, instrument));
//            System.out.println("Transaction Record Margin Due =="+transactRecord.getMarginDue());
//            System.out.println("Transaction Record Quantity =="+transactRecord.getQuantity());
            return ((price * Math.abs(transactRecord.getQuantity() + quantity)) - transactRecord.getMarginDue()) / (price * Math.abs(transactRecord.getQuantity() + quantity));
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static double getCurrentDayMarginCallPercentage(String exchange, String symbol, int instrument,
                                                           String portfolioID, long quantity, double price) {
        try {
            TransactRecord transactRecord = TradePortfolios.getInstance().findTransaction(portfolioID, SharedMethods.getKey(exchange, symbol, instrument));

            return ((price * Math.abs(transactRecord.getQuantity() + quantity)) - transactRecord.getDayMarginDue()) / (price * Math.abs(transactRecord.getQuantity() + quantity));

        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static int getHoldingForSqureOff(String exchange, String symbol, int instrument, String portfolioID) {
        try {
            TransactRecord transactRecord = TradePortfolios.getInstance().findTransaction(portfolioID, SharedMethods.getKey(exchange, symbol, instrument));

            return transactRecord.getDayHolding();
        } catch (Exception e) {
            e.printStackTrace();
            return 0;
        }
    }

    public static double getMarginForSymbol(String exchange, String symbol, int instrument, String portfolioID, boolean isDayOrder, double cashMargin, double dayCashMargin) {
        //todo - need to add the order type here
        if (TradeMethods.isMarginTradingEnabled(portfolioID)) {
            try {
                double symbolBuyingPower = 0;
                double margin = getEffectiveMarginForPortfolio(portfolioID, isDayOrder, cashMargin, dayCashMargin);
                try {
                    TransactRecord transactRecord = TradePortfolios.getInstance().findTransaction(portfolioID, SharedMethods.getKey(exchange, symbol, instrument));
                    double marginPct = 0;
                    MarginSymbolRecord mr = (MarginSymbolRecord) MarginSymbolStore.getSharedInstance().getMarginStore().get(SharedMethods.getKey(exchange, symbol, instrument));
                    if ((TradeMethods.isDayMarginTradingEnabled(portfolioID) && isDayOrder)) {
                        marginPct = mr.getMarginDayBuypct();
//                        marginPct = transactRecord.getDayMarginBuyPct();
                    } else if (isDayOrder) {
                        return 0;
                    } else {
                        marginPct = transactRecord.getMarginBuyPct();
                    }
                    symbolBuyingPower = margin * marginPct;
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                return symbolBuyingPower;
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                return 0;
            }
        } else {
            return 0;
        }
//        try {
//            double symbolBuyingPower = 0;
//            double margin = getEffectiveMarginForPortfolio(portfolioID, orderType);
//
//            try {
//                TransactRecord transactRecord = TradePortfolios.getInstance().findTransaction(portfolioID, SharedMethods.getKey(exchange,  symbol));
//                double marginPct =  transactRecord.getMarginBuyPct();
//                if(isDayMarketType(orderType)){
//                    marginPct = transactRecord.getDayMarginBuyPct();
//                }
//                symbolBuyingPower = margin * marginPct;
//            } catch (Exception e) {
//            }
//            return symbolBuyingPower;
//        } catch (Exception e) {
//            return 0;
//        }
    }

    public static boolean hasMarginThresholdExceeded(String exchange, String symbol, int instrument,
                                                     String portfolioID, long quantity, double price) {
        try {
            double currentMarginCallPct = getCurrentMarginCallPercentage(exchange, symbol, instrument, portfolioID, quantity, price);
//            System.out.println("currentMarginCallPct=="+currentMarginCallPct);
            double marginPct = TradePortfolios.getInstance().findTransaction(portfolioID, SharedMethods.getKey(exchange, symbol, instrument)).getMarginCallPct();
//            System.out.println("Transaction Record getMarginCallPct=="+marginPct);
            double marginThreshold = marginPct / 100;
//            System.out.println("marginThreshold=="+marginThreshold);
            return marginThreshold > currentMarginCallPct;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean hasDayMarginThresholdExceeded(String exchange, String symbol, int instrument,
                                                        String portfolioID, long quantity, double price) {
        try {
            double currentMarginCallPct = getCurrentDayMarginCallPercentage(exchange, symbol, instrument, portfolioID, quantity, price);
            double marginThreshold = TradePortfolios.getInstance().findTransaction(portfolioID, SharedMethods.getKey(exchange, symbol, instrument)).getDayMarginCallPct() / 100;

            return marginThreshold > currentMarginCallPct;
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public static boolean isMarginTradingEnabled(String portfolioID) {
        try {
            return TradePortfolios.getInstance().getPortfolio(portfolioID).isMarginEnabled();
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean isValidForCancel(char orderType) {
        if (!TradingShared.getTrader().isTradableExchangesAvailable()) {
            return false;
        }
        switch (orderType) {
            case ORDER_TYPE_SQUARE_OFF:
                return false;
            default:
                return true;
        }
    }

    public static boolean isValidForAmend(char orderType) {
        if (!TradingShared.getTrader().isTradableExchangesAvailable()) {
            return false;
        }
        switch (orderType) {
            case ORDER_TYPE_MARKET:
            case ORDER_TYPE_LIMIT:
            case ORDER_TYPE_STOPLOSS_MARKET:
            case ORDER_TYPE_STOPLOSS_LIMIT:

                return true;
            default:
                return false;
        }
    }

    public static boolean isDayMarginTradingEnabled(String portfolioID) {
        try {
            return TradePortfolios.getInstance().getPortfolio(portfolioID).isDayMarginEnabled();
        } catch (Exception e) {
            return false;
        }
    }

    /*public static  void requestFutureAttributes(){
        try {
            Enumeration<String> exchanges = TradingShared.getTrader().getExchanges();
            while (exchanges.hasMoreElements()){
                TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_FUTURE_BASE_ATTRIBUTES);
                tradeMessage.addData("A" + exchanges.nextElement());
                SendQueue.getSharedInstance().addData(tradeMessage.toString());
            }
        } catch (Exception e) {
            e.printStackTrace();  
        }
    }*/

    public static void requestUserDataPrimary(byte path, boolean mode) {
        try {
            StringBuilder sExchanges = new StringBuilder();
            StringBuilder sPortfolios = new StringBuilder();

            MIXHeader mixHeader = new MIXHeader();
            mixHeader.setGroup(MIXConstants.GROUP_INQUIRY);
            mixHeader.setRequestType(MIXConstants.REQUEST_TYPE_PORTFOLIO);
            InquiryObject inq = new InquiryObject();


            Enumeration<String> exchanges = TradingShared.getTrader().getExchanges();
            while (exchanges.hasMoreElements()) {
                sExchanges.append(",");
                sExchanges.append(exchanges.nextElement());
            }

            ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
            for (TradingPortfolioRecord record : portfolios) {
                if (TradingShared.getTrader().getPath(record.getPortfolioID()) == path) {
                    sPortfolios.append(",");
                    sPortfolios.append(record.getPortfolioID());
                    inq.setPortfolioID(record.getPortfolioID());
                    // inq.setPortfolioID("S000008899");
                    mixHeader.setSessionID(TradingShared.getTrader().getSessionID(record.getPortfolioID()));
                    inq.setUserID(TradingShared.getTrader().getUserID(record.getPortfolioID()));
                }
            }
            com.dfn.mtr.mix.beans.TransferObject[] mixTransferObjects = new com.dfn.mtr.mix.beans.TransferObject[]{inq};
            MIXObject mixBasicObject = new MIXObject();
            mixBasicObject.setMIXHeader(mixHeader);
            mixBasicObject.setTransferObjects(mixTransferObjects);

            // MIXObject customerDataReq = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_CUSTOMER_INQUIRIES, MIXConstants.REQUEST_TYPE_CUSTOMER_DETAIL, path);
            CustomerAccountRequest req = new CustomerAccountRequest();
            req.setCustomerId(TradingShared.getTrader().getUserID(path));

            //req.setDealerID();
            TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_REQUEST_CUSTOMER_DATA);
            if (sPortfolios.toString().length() > 1) {
                tradeMessage.addData("A" + TradingShared.getTrader().getUserID(path));
                tradeMessage.addData("B" + sExchanges.toString().substring(1));
                tradeMessage.addData("C" + sPortfolios.toString().substring(1));
                if (mode) {
                    tradeMessage.addData("D" + "1");
                }
                tradeMessage.addData("E" + OMSMessageList.getSharedInstance().getLastPrimaryMessageID());
                // SendQueue.getSharedInstance().addData(tradeMessage.toString(),path);
                SendQueue.getSharedInstance().addData(mixBasicObject.getMIXString(), path);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void requestMarginSymbols(byte path) {
        ArrayList<String> sExchanges = new ArrayList<String>();
        ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
        for (int i = 0; i < portfolios.size(); i++) {
            TradingPortfolioRecord record = portfolios.get(i);
            if (TradingShared.getTrader().getPath(record.getPortfolioID()) == path) {
                ArrayList<String> exchanges = record.getExchangeList();
                for (int j = 0; j < exchanges.size(); j++) {
                    if (!sExchanges.contains(exchanges.get(j))) {
                        sExchanges.add(exchanges.get(j));
                    }
                }

            }
        }
        for (int p = 0; p < sExchanges.size(); p++) {
            String exchange = sExchanges.get(p);
            MIXObject mixBasicObject = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_INQUIRY, MIXConstants.REQUEST_TYPE_MARGINABLE_SYMBOL, path);
            MarginableSymbolRequest inq = new MarginableSymbolRequest();
            inq.setInstitutionID(Settings.getBrokerInstitutionID());
            inq.setCustomerID(TradingShared.getTrader().getUserID(path));
            inq.setExchange(exchange);
            mixBasicObject.setTransferObjects(new TransferObject[]{inq});
            SendQueue.getSharedInstance().addData(mixBasicObject.getMIXString(), path);
        }

    }

    public static void requestUserDataSecondary(byte path, boolean mode) {
        try {
            StringBuilder sExchanges = new StringBuilder();
            StringBuilder sPortfolios = new StringBuilder();

            MIXHeader mixHeader = new MIXHeader();
            mixHeader.setGroup(MIXConstants.GROUP_INQUIRY);
            mixHeader.setRequestType(MIXConstants.REQUEST_TYPE_PORTFOLIO);
            InquiryObject inq = new InquiryObject();


            Enumeration<String> exchanges = TradingShared.getTrader().getExchanges();
            while (exchanges.hasMoreElements()) {
                sExchanges.append(",");
                sExchanges.append(exchanges.nextElement());
            }

            ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
            for (TradingPortfolioRecord record : portfolios) {
                if (TradingShared.getTrader().getPath(record.getPortfolioID()) == path) {
                    sPortfolios.append(",");
                    sPortfolios.append(record.getPortfolioID());
                    inq.setPortfolioID(record.getPortfolioID());
                    // inq.setPortfolioID("S000008899");
                    mixHeader.setSessionID(TradingShared.getTrader().getSessionID(record.getPortfolioID()));
                    inq.setUserID(TradingShared.getTrader().getUserID(record.getPortfolioID()));
                }
            }
            com.dfn.mtr.mix.beans.TransferObject[] mixTransferObjects = new com.dfn.mtr.mix.beans.TransferObject[]{inq};
            MIXObject mixBasicObject = new MIXObject();
            mixBasicObject.setMIXHeader(mixHeader);
            mixBasicObject.setTransferObjects(mixTransferObjects);

            // MIXObject customerDataReq = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_CUSTOMER_INQUIRIES, MIXConstants.REQUEST_TYPE_CUSTOMER_DETAIL, path);
            CustomerAccountRequest req = new CustomerAccountRequest();
            req.setCustomerId(TradingShared.getTrader().getUserID(path));

            //req.setDealerID();
            TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_REQUEST_CUSTOMER_DATA);
            if (sPortfolios.toString().length() > 1) {
                tradeMessage.addData("A" + TradingShared.getTrader().getUserID(path));
                tradeMessage.addData("B" + sExchanges.toString().substring(1));
                tradeMessage.addData("C" + sPortfolios.toString().substring(1));
                if (mode) {
                    tradeMessage.addData("D" + "1");
                }
                tradeMessage.addData("E" + OMSMessageList.getSharedInstance().getLastPrimaryMessageID());
                // SendQueue.getSharedInstance().addData(tradeMessage.toString(),path);
                SendQueue.getSharedInstance().addData(mixBasicObject.getMIXString(), path);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }

//        try {
//            StringBuilder sExchanges = new StringBuilder();
//            StringBuilder sPortfolios = new StringBuilder();
//
//            Enumeration<String> exchanges = TradingShared.getTrader().getExchanges();
//            while (exchanges.hasMoreElements()) {
//                sExchanges.append(",");
//                sExchanges.append(exchanges.nextElement());
//            }
//
//            ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
//            for (TradingPortfolioRecord record : portfolios) {
//                if (TradingShared.getTrader().getPath(record.getPortfolioID()) == path) {
//                    sPortfolios.append(",");
//                    sPortfolios.append(record.getPortfolioID());
//                }
//            }
//
//            TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_REQUEST_CUSTOMER_DATA);
//            if (sPortfolios.toString().length() > 1) {
//                tradeMessage.addData("A" + TradingShared.getTrader().getUserID(path));
//                tradeMessage.addData("B" + sExchanges.toString().substring(1));
//                tradeMessage.addData("C" + sPortfolios.toString().substring(1));
//                if (mode) {
//                    tradeMessage.addData("D" + "1");
//                }
//                tradeMessage.addData("E" + OMSMessageList.getSharedInstance().getLastSecondaryMessageID());
//                SendQueue.getSharedInstance().addData(tradeMessage.toString(), path);
//            }
//
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
    }

    public static boolean isDuplicateSendingOrder(String exchange, String symbol, int instrumentType, String portfolio, long quantity, int side) {
        ArrayList<Transaction> trans = OrderStore.getSharedInstance().getSendingTransactions();
        Transaction transaction;
        if (trans.size() > 0) {
            for (int i = 0; i < trans.size(); i++) {
                try {
                    transaction = trans.get(i);
                    if ((transaction.getPortfolioNo().equals(portfolio)) && (transaction.getSymbol().equals(symbol)) &&
                            (transaction.getExchange().equals(exchange)) && (transaction.getSecurityType() == instrumentType) &&
                            (transaction.getOrderQuantity() == quantity) && (transaction.getSide() == side)) {
                        return true;
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return false;
    }

    public static void GetCommission(String pf, String exg, String symbol, double price, double qty, String bookKeeper, int side, boolean isTPlusZero, byte path) {
        try {
            TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_COMMISSION);
            tradeMessage.addData("A" + pf);
            tradeMessage.addData("B" + price);
            tradeMessage.addData("C" + qty);
            tradeMessage.addData("D" + exg);
            tradeMessage.addData("E" + bookKeeper);
            tradeMessage.addData("F" + side);
            tradeMessage.addData("G" + symbol);
            if (isTPlusZero)
                tradeMessage.addData("H" + "0");

            SendQueue.getSharedInstance().addData(tradeMessage.toString(), path);

        }
        catch (Exception e) {
            e.printStackTrace();

        }
    }

    public static void requestBankAccountDetails(byte path) {
        try {
            MIXObject bankrequest = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_INQUIRY, MIXConstants.REQUEST_TYPE_BANK_ACC_DETAILS, path);
            InquiryObject bankInq = new InquiryObject();
            bankInq.setUserID(TradingShared.getTrader().getUserID(path));
            bankInq.setMubasherID(TradingShared.getTrader().getMubasherID(path));
            //  TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_BANK_ACC_DETAILS);
            //  tradeMessage.addData("A" + TradingShared.getTrader().getUserID(path));
            //  tradeMessage.addData("B" + TradingShared.getTrader().getMubasherID(path));

            bankrequest.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{bankInq});

            //SendQueue.getSharedInstance().addData(tradeMessage.toString(), path);
//            SendQueue.getSharedInstance().addData(bankrequest.getMIXString(), path);

        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static String doInvestClicked() {
        String URL = TradingShared.getDuInvestURL();
        if (URL != null && !URL.trim().isEmpty()) {
            URL = URL.replaceFirst("\\[MUBASHER_NO\\]", TradingShared.getTrader().getMubasherID(Constants.PATH_PRIMARY));
            URL = URL.replaceFirst("\\[SESSION_ID\\]", MixStore.getSharedInstance().getAuthenticationResponse(Constants.PATH_PRIMARY).getSSessionID());
            return URL;
        }
        return null;
    }

    public static boolean isDuInvestURLAvailable() {
        return (!(TradingShared.getDuInvestURL() == null || TradingShared.getDuInvestURL().trim().isEmpty()));
    }

//
//    public static  void requestSecondaryFutureAttributes(){
//        try {
//            StringBuilder sExchanges = new StringBuilder();
//            StringBuilder sPortfolios = new StringBuilder();
//
//            Enumeration<String> exchanges = TradingShared.getTrader().getExchanges();
//            while (exchanges.hasMoreElements()){
//                sExchanges.append(",");
//                sExchanges.append(exchanges.nextElement());
//            }
//
//            ArrayList<TradingPortfolioRecord> portfolios = TradingShared.getTrader().getPortfolios();
//            for (TradingPortfolioRecord record: portfolios){
//                if(TradingShared.getTrader().getPath(record.getPortfolioID())==Constants.PATH_SECONDARY && TradingShared.SECONDARY_LOGIN_SUCCESS){
//                    sPortfolios.append(",");
//                    sPortfolios.append(record.getPortfolioID());
//                }
//            }
//            TradeMessage tradeMessage = new TradeMessage(TradeMeta.MT_REQUEST_CUSTOMER_DATA);
//            if(sPortfolios.toString().length()>1) {
//                tradeMessage.addData("A" + TradingShared.getTrader().getUserID(Constants.PATH_SECONDARY));
//                tradeMessage.addData("B" + sExchanges.toString().substring(1));
//                tradeMessage.addData("C" + sPortfolios.toString().substring(1));
//                SendQueue.getSharedInstance().addData(tradeMessage.toString(),Constants.PATH_SECONDARY);
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

    //    }

    private static void cancelBehavourList(List<ConditionalBehaviour> list) {
        if (list != null && list.size() > 0) {
            for (int i = 0; i < list.size(); i++) {
                list.get(i).setConditionStatus(TradeMeta.CONDITION_STATUS_CENCELLED);
            }
        }

    }

    public static ArrayList<String> getAccountSummaryCurrensyList() {
        ArrayList<String> currencylist = new ArrayList<String>();
        ArrayList<Account> records = TradingShared.getTrader().getAccounts();
        Account record = null;
        for (int i = 0; i < records.size(); i++) {
            record = records.get(i);
            if (!isIncluded(currencylist, record.getCurrency())) {
                currencylist.add(record.getCurrency());
            }

            record = null;
        }
        return currencylist;
    }

    private static boolean isIncluded(ArrayList<String> currencylist, String currency) {
        try {
            boolean found = false;
            for (int i = 0; i < currencylist.size(); i++) {
                if (currencylist.get(i).equals(currency)) {
                    found = true;
                }
            }
            return found;
        } catch (Exception e) {
            return false;
        }
    }

    public static String getReferenceMarginExchangeForAccSummary() {
        String exchang = "*";
        Rule r = RuleManager.getSharedInstance().getRule("MARGIN_REF_EXCHANGE", "*", "ACCOUNT_SUMMARY");
        try {
            exchang = r.getRule();
        } catch (Exception e) {
            exchang = "*";
        }

        return exchang;
    }
    /*
        Added by chandika for SABB version release perposes
     */
    private static TWButton btnRefreshA = new TWButton(Language.getString("REFRESH"));
    private static TWButton btnRefreshP = new TWButton(Language.getString("REFRESH"));
    private static TWButton btnRefreshO = new TWButton(Language.getString("REFRESH"));

    public static synchronized void checkRefreshButton() {

        if (TradingShared.isConnected() && TradingShared.isManaulBroker() || TWControl.enableRefreshButton()) {
            btnRefreshA.setVisible(true);
            btnRefreshA.setPreferredSize(null);
            btnRefreshP.setVisible(true);
            btnRefreshP.setPreferredSize(null);
            btnRefreshO.setVisible(true);
            btnRefreshO.setPreferredSize(null);
        } else {
            btnRefreshA.setVisible(false);
            btnRefreshA.setPreferredSize(new Dimension(0, 0));
            btnRefreshP.setVisible(false);
            btnRefreshP.setPreferredSize(new Dimension(0, 0));
            btnRefreshO.setVisible(false);
            btnRefreshO.setPreferredSize(new Dimension(0, 0));
        }
    }

    public static synchronized void enableRefreshButtons(boolean enable) {
        btnRefreshA.setEnabled(enable);
        btnRefreshO.setEnabled(enable);
        btnRefreshP.setEnabled(enable);
    }

    public static boolean isPortfolioRefreshButtonEnable() {
        return btnRefreshP.isVisible();
    }

    public static TWButton getAccountRefresh() {
        return btnRefreshA;
    }

    public static TWButton getPortfolioRefresh() {
        return btnRefreshO;
    }

    public static TWButton getOrderListRefresh() {
        return btnRefreshP;
    }

    public static boolean isPfRefreshClicked() {
        return pfRefreshClicked;
    }

    public static void setPfRefreshClicked(boolean pfRefreshClicked) {
        TradeMethods.pfRefreshClicked = pfRefreshClicked;
    }

    public static void addCommissionResponse(double commission, String portfolio, String exchange, String symbol, int securityType, double price, long qty) {
        String key = portfolio + "_" + exchange + "_" + price + "_" + qty;
        commissionStore.put(key, commission);
    }

    public static double getCommission(String portfolio, String exchange, String symbol, int securityType, double price, long qty) {
        try {
            String key = portfolio + "_" + exchange + "_" + price + "_" + qty;
            Double commission = commissionStore.get(key);
            if (commission == null) {
                return 0;
            } else {
                return commission.doubleValue();
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public static void addVatResponse(double vat, String portfolio, String exchange, String symbol, int securityType, double price, long qty) {
        String key = portfolio + "_" + exchange + "_" + price + "_" + qty;
        vatStore.put(key, vat);
    }

    public static double getVat(String portfolio, String exchange, String symbol, int securityType, double price, long qty) {
        try {
            String key = portfolio + "_" + exchange + "_" + price + "_" + qty;
            Double vat = vatStore.get(key);
            if (vat == null) {
                return 0;
            } else {
                return vat.doubleValue();
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public static void addCommissionRequest(String sequence, OrderCommissionRequest request) {
        commissionRequests.put(sequence, request);
    }

    public static OrderCommissionRequest getCommissionRequest(String seguence) {
        return commissionRequests.get(seguence);
    }

    public static void requestCommissionServer(String portfolio, String type, double price, long qty, String symbol, String exchange, int mubSecType, int side) {
        try {
            MIXObject orderCommission = new MIXObject();
            MIXHeader header = new MIXHeader();
            header.setGroup(MIXConstants.GROUP_TRADING_INQUIRY);
            header.setRequestType(MIXConstants.REQUEST_TYPE_ORDER_COMMISSION);

            OrderCommissionRequest inquiry = new OrderCommissionRequest();
            inquiry.setOrderType(type);
            inquiry.setPrice(price);
            inquiry.setQuantity(qty);
            inquiry.setSecurityAccountNumber(portfolio);
            inquiry.setSymbol(symbol);
            inquiry.setExchange(exchange);
            inquiry.setMubasherSecurityType(mubSecType);
            inquiry.setSequenceNumber(System.currentTimeMillis() + "");
            inquiry.setSide(side);
            inquiry.setSettleType(0);

            orderCommission.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inquiry});
            orderCommission.setMIXHeader(header);
            TradeMethods.addCommissionRequest(inquiry.getSequenceNumber(), inquiry);
            SendQueue.getSharedInstance().addData(orderCommission.getMIXString(), TradingShared.getTrader().getPath(portfolio));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void requestBuyingPowerFromServer(String[] pfs, String portfolio, String symbol, String exchange) {
        try {
            MIXObject buyingPowerStatus = new MIXObject();
            MIXHeader header = new MIXHeader();
            header.setGroup(MIXConstants.GROUP_INQUIRY);
            header.setRequestType(MIXConstants.REQUEST_TYPE_BUYING_POWER_STATUS);

            CommonInquiry inquiry = new CommonInquiry();
            inquiry.setSymbol(symbol);
            inquiry.setExchange(exchange);
            inquiry.setRequestId("1234-dfdsf-3234-frfd");

            if (pfs == null && portfolio != "") {
                inquiry.setPortfolioId(portfolio);
                buyingPowerStatus.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inquiry});
                buyingPowerStatus.setMIXHeader(header);
                SendQueue.getSharedInstance().addData(buyingPowerStatus.getMIXString(), TradingShared.getTrader().getPath(portfolio));
            } else {
                for (int i = 0; i < pfs.length; i++) {
                    String pf = pfs[i];
                    inquiry.setPortfolioId(pf);
                    buyingPowerStatus.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inquiry});
                    buyingPowerStatus.setMIXHeader(header);
                    SendQueue.getSharedInstance().addData(buyingPowerStatus.getMIXString(), TradingShared.getTrader().getPath(pf));
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public MIXObject  authenticateRequestGenerator(String user, String pass, String otpNumber, boolean isEncrypted) throws Exception {

        AuthenticationRequest mixaAthRequest = new AuthenticationRequest();
        MIXHeader mixHeader = new MIXHeader();
        mixHeader.setGroup(MIXConstants.GROUP_AUTHENTICATION);

        String md5Password=null;
        String password=null;
        int replaceDigit=0;

        if(TWControl.isPasswordEncryptionEnabled()){
        md5Password = getEncryptedPassword(pass);
        replaceDigit = getReplaceDigitForSalt(md5Password);
        password = getSaltedPassword(md5Password, replaceDigit);
        }

        if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web)) {
            mixHeader.setRequestType(MIXConstants.REQUEST_TYPE_AUTH_SSO);

        } else {
            mixHeader.setRequestType(MIXConstants.REQUEST_TYPE_AUTH_NORMAL);
        }

        if (TWControl.enablePickNewChannelID()) {
            mixHeader.setChannelID(MIXConstants.CHANNEL_TYPE_TWS);
        } else {
            mixHeader.setChannelID(MIXConstants.CHANNEL_TYPE_NEWTW);
        }
        mixHeader.setClientIP(TradingShared.getIp());

        if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web)) {
            if (!Settings.getTradeToken().equals("")) {
                mixaAthRequest.setSSOId(Settings.getTradeToken());
            } else {
                mixaAthRequest.setSSOId(Settings.getSsoId());
            }
            mixaAthRequest.setInstitutionType(pass);
        } else if(TWControl.isPasswordEncryptionEnabled()){
            mixaAthRequest.setUsername(user);
            mixaAthRequest.setPassword(password);
            mixaAthRequest.setPwRandomCharPosition(replaceDigit);
        } else {
            mixaAthRequest.setUsername(user);
            mixaAthRequest.setPassword(pass);
        }
        mixaAthRequest.setLanguage(Language.getLanguageTag());
        mixaAthRequest.setSessionID(TradingShared.getSessionID());
//        mixaAthRequest.setIp(TradingShared.setIp(SharedMethods.getLocalIP()));
        String publicIP = SharedMethods.getPublicIP();
        if(publicIP != null) {
            mixaAthRequest.setIp(TradingShared.setIp(publicIP));
        } else {
        mixaAthRequest.setIp(TradingShared.setIp(SharedMethods.getLocalIP()));
        }
        mixaAthRequest.setClientVersion(Settings.TW_VERSION);
        mixaAthRequest.setOtpPinNumber(otpNumber);
        if (TWControl.enablePickNewChannelID()) {
            mixaAthRequest.setClientChannel(MIXConstants.CHANNEL_TYPE_TWS + "");
        } else {
            mixaAthRequest.setClientChannel(MIXConstants.CHANNEL_TYPE_NEWTW + "");
        }

        TransferObject[] mixTransferObjects = new TransferObject[]{mixaAthRequest};
        MIXObject mixBasicObject = new MIXObject();
        mixBasicObject.setMIXHeader(mixHeader);
        mixBasicObject.setTransferObjects(mixTransferObjects);
        return mixBasicObject;
    }

    public MIXObject otpResend(String userID, String username) throws Exception {
        AuthenticationRequest mixaAthRequest = new AuthenticationRequest();
        MIXHeader mixHeader = new MIXHeader();
        mixHeader.setGroup(MIXConstants.GROUP_AUTHENTICATION);
        mixHeader.setRequestType(MIXConstants.REQUEST_TYPE_SECONDARY_LOGIN);
        mixHeader.setUserID(userID);

        if (TWControl.enablePickNewChannelID()) {
            mixHeader.setChannelID(MIXConstants.CHANNEL_TYPE_TWS);
        } else {
            mixHeader.setChannelID(MIXConstants.CHANNEL_TYPE_NEWTW);
        }
        mixaAthRequest.setUsername(username);
        mixaAthRequest.setUserId(userID);
        mixaAthRequest.setSessionID(TradingShared.getSessionID());
        mixaAthRequest.setIp(TradingShared.getIp());
        mixaAthRequest.setClientVersion(Settings.TW_VERSION);
        if (TWControl.enablePickNewChannelID()) {
            mixaAthRequest.setClientChannel(MIXConstants.CHANNEL_TYPE_TWS + ""); 
        } else {
            mixaAthRequest.setClientChannel(MIXConstants.CHANNEL_TYPE_NEWTW + "");
        }

        TransferObject[] mixTransferObjects = new TransferObject[]{mixaAthRequest};
        MIXObject mixBasicObject = new MIXObject();
        mixBasicObject.setMIXHeader(mixHeader);
        mixBasicObject.setTransferObjects(mixTransferObjects);
        return mixBasicObject;
    }

    public void otpPane() {
        AuthenticationResponse mixAuthOTPResponse = MixStore.getSharedInstance().getAuthenticationResponse(Constants.PATH_PRIMARY);
        String mob = mixAuthOTPResponse.getMobileNumber();
        String duration = mixAuthOTPResponse.getOneTimePasswordFlag();
        final String userID = mixAuthOTPResponse.getUserID();
        final String username = mixAuthOTPResponse.getLoginName();

        final JDialog di = new JDialog();
        di.setTitle(Language.getString("OTP"));
        di.setSize(450, 160);
        di.setResizable(false);
        di.setAlwaysOnTop(true);
        di.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        di.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                Client.getInstance().setTradeSessionDisconnected(true);
            }
        });
        JPanel msgPanel = new JPanel();
        JPanel inputPanel = new JPanel();
        JPanel linkPanel = new JPanel();

        String mob2 = "";
        int length = mob.length();
        String lastDigits = mob.substring(length - 3, length);
        String xDigits = Language.getString("MOB_NUM_FORMAT");
        mob2 = xDigits.concat(lastDigits);

        JLabel msg1 = new JLabel(Language.getString("OTP_LINE1")+" " + mob2);
        JLabel msg2 = new JLabel(Language.getString("OTP_LINE2"));
        JLabel msg3 = new JLabel(Language.getString("OTP_LINE3")+" " + duration + " "+Language.getString("MINUTE"));
        JLabel link = new JLabel(Language.getString("OTP_LINE4"));
        JLabel lblOTP = new JLabel(Language.getString("OTP_NUMBER"));
        JLabel lblClick = new JLabel(Language.getString("CLICK_HERE"));
        lblClick.setForeground(Color.BLUE);
        JButton btnSend = new JButton(Language.getString("SEND"));
        JButton btnCancel = new JButton(Language.getString("CANCEL"));
        final JTextField otpNumber = new JTextField(6);
        otpNumber.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));
        msgPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"15","15","15"}, 2, 3));
        inputPanel = new JPanel(new FlexGridLayout(new String[]{"25%", "30%", "20%", "20%"}, new String[]{"20"}, 2, 8));
        linkPanel = new JPanel(new FlexGridLayout(new String[]{"15%", "80%"}, new String[]{"15"}, 2, 3));
        JPanel linkPanel2 = new JPanel();

        lblClick.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lblClick.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() > 0) {
                    try {
                        MIXObject mixBasicObject = otpResend(userID, username);
                        SendQueue.getSharedInstance().writeData(mixBasicObject.getMIXString(), Constants.PATH_PRIMARY);
//                        di.dispose();
                    } catch (Exception e1) {
                        e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        });

        msgPanel.add(msg1);
        msgPanel.add(msg2);
        if(duration != null){
        msgPanel.add(msg3);
        }
        inputPanel.add(lblOTP);
        inputPanel.add(otpNumber);
        inputPanel.add(btnSend);
        inputPanel.add(btnCancel);
        linkPanel.add(lblClick);
        linkPanel.add(link);

        btnSend.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Object[] option = {Language.getString("OK")};
                if (new String(otpNumber.getText()).trim().equals("")) {
                    JOptionPane.showOptionDialog(di,
                            Language.getString("OTP_NULL_VALUE"),
                            Language.getString("ERROR"),
                            JOptionPane.OK_OPTION,
                            JOptionPane.ERROR_MESSAGE,
                            null,
                            option,
                            option[0]);
                } else {
                    try {
                        MIXObject mixBasicObject = TradeMethods.getSharedInstance().authenticateRequestGenerator(username, null, otpNumber.getText(), false);
                        SendQueue.getSharedInstance().writeData(mixBasicObject.getMIXString(), Constants.PATH_PRIMARY);
                        di.dispose();
                    } catch (Exception e1) {
                        e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        });
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().setTradeSessionDisconnected(true);
                di.dispose();
//                LoginWindow loginDialog = new LoginWindow(Client.getInstance().getFrame(), Login.MODE_TRADING_SERVERS);
            }
        });
            Object[] array = new Object[2];
        if(!TWControl.isOTPResendEnabled()){
            array[0] = msgPanel;
            array[1] = linkPanel;
        }else{
            array[0] = msgPanel;
        }    
        Object[] options = {inputPanel};
        JOptionPane otpPane = new JOptionPane(array,
                -1,
                JOptionPane.OK_OPTION,
                null,
                options,
                options[0]);

        otpPane.setName("OTP");
        otpPane.add(msgPanel);
        otpPane.add(inputPanel);
        if(!TWControl.isOTPResendEnabled()){
        otpPane.add(linkPanel);
        }
        di.setLocationRelativeTo(Client.getInstance().getFrame());
        GUISettings.applyOrientation(di);
        GUISettings.applyOrientation(otpPane);
        di.setContentPane(otpPane);
        di.setVisible(true);
    }

    public static String getFormattedDate(String date){

        StringBuilder sb = new StringBuilder();
        sb.append(date.substring(6,8)).append("/").append(date.substring(4,6)).append("/").append(date.substring(0,4));
        return sb.toString();
    }

    public static String getEncryptedPassword(String password){

        MessageDigest digest = null;

        try {
            digest = MessageDigest.getInstance("SHA-1");
        } catch (NoSuchAlgorithmException e) {
            e.printStackTrace();
        }
        digest.reset();
        String newDigestMsg = new sun.misc.BASE64Encoder().encode(digest.digest(password.getBytes()));

        return newDigestMsg;
    }

    public static int getReplaceDigitForSalt(String password){
        int digit=1;
        int length = password.length();

        if(length > 1){
            digit = (int)(Math.random()*(length/2));

            if(digit==0){
                return 1;
            }
        }
        return digit;
    }

    public static String getSaltedPassword(String data, int replaceDigit){

        char[] dataBuffer = data.toCharArray();
            if(replaceDigit < dataBuffer.length){
                StringBuilder sb = new StringBuilder();
                sb.append(dataBuffer[0]);
                for (int i = 1; i < dataBuffer.length; i++)
                {
                    int res = i % replaceDigit;
                    if (res == 0){
                        sb.append((int)(Math.random()*9));
                    }
                    sb.append(dataBuffer[i]);
                }
                return sb.toString();
            }
        return null;

    }

    public static void getTiFFTypesFromServer(byte path){

        MIXObject mixObj = null;
        InquiryObject inqObj = new InquiryObject();

        mixObj = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_INQUIRY, MIXConstants.REQUEST_TYPE_GET_TIF_RULE, path);

                    mixObj.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inqObj});
                    SendQueue.getSharedInstance().addData(mixObj.getMIXString(), Constants.PATH_PRIMARY);
                    mixObj = null;
                    inqObj = null;
    }

    public static void reqExchangeLvlConfig(byte path) {
        MIXObject mixObj = null;
        InquiryObject inqObj = new InquiryObject();

        mixObj = MixStore.getSharedInstance().getMixObject(MIXConstants.GROUP_INQUIRY, MIXConstants.REQUEST_TYPE_EXCHANGE_LEVEL_CONFIG_INFO, path);

        mixObj.setTransferObjects(new com.dfn.mtr.mix.beans.TransferObject[]{inqObj});
        SendQueue.getSharedInstance().addData(mixObj.getMIXString(), path);
        mixObj = null;
        inqObj = null;

    }
}


