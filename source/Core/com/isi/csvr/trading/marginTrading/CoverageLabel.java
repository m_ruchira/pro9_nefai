package com.isi.csvr.trading.marginTrading;

import com.isi.csvr.variationmap.VariationImage;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.*;
import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Feb 19, 2010
 * Time: 4:10:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class CoverageLabel  extends JLabel{
    private  VariationImage image = new SimplevariationImage();
    private TWDecimalFormat formatter;
    private int decimals;
    private int roundMTD;

    public CoverageLabel(int decimals, int roundingMtd ){
        super("",JLabel.CENTER );
        image = new SimplevariationImage();
       // image.setType(VariationImage.TYPE_COVERAGE_MAP);
        if(decimals<1){
            decimals =1;
        }
        this.decimals = decimals;
        roundMTD= roundingMtd;
        formatter= SharedMethods.getDecimalFormat(decimals);

        
//        setHorizontalTextPosition(JLabel.CENTER);
       // this.setIcon(image);
    }

    public void setDecimals(int decimals){
     if(decimals<1){
            decimals =1;
        }
        formatter= SharedMethods.getDecimalFormat(decimals);
        this.decimals = decimals;
    }
    public void setPreferredSize(Dimension preferredSize) {
        image.setWidth(preferredSize.width);
        image.setHeight(preferredSize.height);
        super.setPreferredSize(preferredSize);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void setSize(int width, int height) {
        image.setWidth(width);
        image.setHeight(height);
        super.setSize(width, height);    //To change body of overridden methods use File | Settings | File Templates.
    }
    public void initIcon(){
        setIcon(image);
    }
    public void initIcon(VariationImage image){
        setIcon(image);
        this.image = image;
    }

    public void setCoverageValues(double benchmark, double calculated){
        try {
//            double bench = Double.valueOf(formatter.format(benchmark));
//            double calc = Double.valueOf(formatter.format(calculated));

            BigDecimal benchMark = new BigDecimal(benchmark);
               double bench = (benchMark.setScale(decimals, roundMTD)).doubleValue();
                BigDecimal coverage = new BigDecimal(calculated);
               double calc = (coverage.setScale(decimals, roundMTD)).doubleValue();

            if(calc > bench){
                image.setValue(0.00);
//                this.setBackground(Theme.BIDASK_UP_COLOR);
                this.setText(formatter.format(bench) +   "<"+ formatter.format(calc));
            }else if(Double.compare(calc, bench)==0) {
                image.setValue(1.00);
//                 this.setBackground(Theme.BIDASK_UP_COLOR);
                this.setText(formatter.format(bench) + "="+ formatter.format(calc));
            }else{
                image.setValue(1.00);
//                 this.setBackground(Theme.BIDASK_DOWN_COLOR);
                this.setText(formatter.format(benchmark)  + ">"+ formatter.format(calc));
            }
        } catch (NumberFormatException e) {
            this.setText(Language.getString("N/A"));
            image.setValue(Double.NaN);
        }
        

    }

    public void setEmptyCoverageValues(){
            this.setText(Language.getString("NA"));
            image.setValue(Double.NaN);

    }

    private class SimplevariationImage extends VariationImage {
        public static final int TYPE_COVERAGE = 0;
        public static final int TYPE_SPREAD = 1;
        public static final int TYPE_RANGE = 2;
        public static final int TYPE_CHANGE = 3;
        public static final int TYPE_TICK = 4;
        public static final int TICK_FILTER = 3;
        public static final int TYPE_OHLC_MAP = 5;
        public static final int TYPE_CASH_MAP = 6;
        public static final int TYPE_CASH_FLOW = 7;
//    public static final int TYPE_FULL_MAP=8;

        private int type;
        private int height = 10;
        private int width = 50;
        private int CLEARENCE = 4;
        private int LOW_CLEARENCE = 2;
        private Double value = Double.NaN;
        private int intValue = 0;
        private int imageWidth = 0;
        private String exchange;
        private double[] values;//=new double[13];
        private double[] fullMapValues = new double[7];


        public SimplevariationImage() {
            fullMapValues[0] = 20;
            fullMapValues[6] = 70;
            imageWidth = width - (CLEARENCE * 2);
           
        }

        private SimplevariationImage(int type) {
            this.type = type;
        }

        public int getIconHeight() {
            return height;
        }

        public int getIconWidth() {
            return imageWidth;
        }

        public void setValue(Double value) {
            if(value>1){
                value=1.00;
            }
            this.value = value;
        }


        public void setWidth(int width) {
            this.width = width;
            imageWidth = width - (CLEARENCE * 2);
        }

        public void setHeight(int height) {
            this.height = height;
            CLEARENCE = height / 4;
        }

        public void setType(int type) {
            this.type = type;
        }

        public void paintIcon(Component c, Graphics g, int x, int y) {
            if (!Double.isNaN(value)) {
                try {
                    if (type == TYPE_COVERAGE) {
                        g.setColor(Theme.BIDASK_UP_COLOR);
                        g.fillRect(2, 2, c.getWidth() - 4, c.getHeight() - 4);
                        g.setColor(Theme.BIDASK_DOWN_COLOR);
                        g.fillRect(2, 2, (int) (value * (c.getWidth() - 4)), c.getHeight() - 4);
                        g.setColor(Theme.INDEXPANEL_CASHMAP_BORDER_COLOR);
                        g.drawRect(2, 2, c.getWidth() - 4, c.getHeight() - 4);
//                        g.drawRect(1, 1, c.getWidth() - 2, c.getHeight() - 3);
                    } else {
                        g.setColor(Theme.BIDASK_UP_COLOR);
                        g.fillRect(2, 2, c.getWidth() - 4, c.getHeight() - 4);
                        g.setColor(Theme.BIDASK_DOWN_COLOR);
                        g.fillRect(2, 2, (int) (value * (c.getWidth() - 4)), c.getHeight() - 4);
                        g.setColor(Theme.INDEXPANEL_CASHMAP_BORDER_COLOR);
                      //  g.drawRect(2, 2, c.getWidth() - 4, c.getHeight() - 4);
                        g.drawRect(0, 0, c.getWidth() - 2, c.getHeight() - 2);
                    }
                } catch (Exception e) {
                }
            }
        }


    }
}
