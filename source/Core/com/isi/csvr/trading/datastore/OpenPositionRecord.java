package com.isi.csvr.trading.datastore;

import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.SharedMethods;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Nov 17, 2007
 * Time: 5:23:05 PM
 */
public class OpenPositionRecord {
    private String symbol;
    private String exchange;
    private String key;
    private String orderID;
    private String portfolio;
    private long date;
    private long openQuantity;
    private long closedQuantity;
    private double openPrice;
    private int side;
    private int instrument = -1;
    private int status = -1;

    boolean baseRecord;

    public OpenPositionRecord(boolean baseRecord, String exchange, String symbol, String orderID, String portfolio, int instrument) {
        this.baseRecord = baseRecord;
        this.symbol = symbol;
        this.exchange = exchange;
        this.instrument = instrument;
        this.key =SharedMethods.getKey(exchange,symbol,instrument);
        this.orderID = orderID;
        this.portfolio = portfolio;
    }

    public String getOrderID() {
        return orderID;
    }

    public int getInstrument() {
        return instrument;
    }

    public long getDate() {
        return date;
    }

    public void setDate(long date) {
        this.date = date;
    }

    public double getOpenPrice() {
        return openPrice;
    }

    public void setOpenPrice(double openPrice) {
        this.openPrice = openPrice;
    }

    public String getSymbol() {
        return symbol;
    }

    public String getKey() {
        return key;
    }

    public long getClosedQuantity() {
        return closedQuantity;
    }

    public void setClosedQuantity(long closedQuantity) {
        this.closedQuantity = closedQuantity;
    }

    public String getExchange() {
        return exchange;
    }

    public long getOpenQuantity() {
        return openQuantity;
    }

    public void setOpenQuantity(long openQuantity) {
        this.openQuantity = openQuantity;
    }

    public String getPortfolio() {
        return portfolio;
    }

    public int getSide() {
        return side;
    }

    public void setSide(int side) {
        this.side = side;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public boolean isBaseRecord() {
        return baseRecord;
    }

    public String toString() {
        return "symbol " + symbol + " side " + side + " open " + openQuantity + " closed " + closedQuantity;
    }
}
