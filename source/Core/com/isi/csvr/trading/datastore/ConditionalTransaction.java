package com.isi.csvr.trading.datastore;

import com.isi.csvr.trading.shared.TRSOrder;
import com.isi.csvr.trading.shared.TradingShared;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Sep 13, 2006
 * Time: 11:26:22 AM
 */
public class ConditionalTransaction extends Transaction{
    private String condition;
    private long conditionExpireTime;
    private int status;

    public void setOrderData(TRSOrder trsOrdera) {
        super.setOrderData(trsOrdera);
        if (trsOrdera.getCondition() != null)
            condition = trsOrdera.getCondition();
        if (trsOrdera.getConditionExpireTime() != null)
            conditionExpireTime = TradingShared.formatFIXTimeToLong(trsOrdera.getOrderID());
        if (trsOrdera.getConditionStatus() != -1)
            status = trsOrdera.getConditionStatus();
    }

    public String getCondition() {
        return condition;
    }

    public void setCondition(String condition) {
        this.condition = condition;
    }

    public long getConditionExpireTime() {
        return conditionExpireTime;
    }

    public void setConditionExpireTime(long conditionExpireTime) {
        this.conditionExpireTime = conditionExpireTime;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

}
