package com.isi.csvr.trading.datastore;

import com.dfn.mtr.mix.beans.TIFRule;
import com.isi.csvr.datastore.ExchangeStore;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: Oshadau
 * Date: Jun 16, 2014
 * Time: 12:26:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class TIFTypeStore {

    private List<TIFRule> store = null;

    public static TIFTypeStore self = null;

    public TIFTypeStore() {
        store = new ArrayList<TIFRule>();
    }

    public static TIFTypeStore getSharedInstance(){
        if(self == null){
            self = new TIFTypeStore();
        }
        return self;
    }

    public void addToStore(List<TIFRule> list){

        for(int i=0; i<list.size(); i++){
            store.add(list.get(i));
        }

    }

    public String getTIFTypes(String exchange, char type){

        int mktStatus;

        if(exchange != null){
        mktStatus = ExchangeStore.getSharedInstance().getExchange(exchange).getMarketStatus();
        }else{
            return null;
        }
//        Iterator<TIFRule> itr = store.iterator();
        String tifTypes=null;
        for (int i = 0; i < store.size(); i++) {

                TIFRule rule = store.get(i);

                if(rule.getExchange().equals(exchange) && rule.getMarketStatus() == mktStatus && rule.getOrdType() == type){
                     tifTypes = rule.getTIFTypes();
                }
	    }
        return tifTypes;
    }
}