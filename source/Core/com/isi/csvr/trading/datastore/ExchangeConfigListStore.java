package com.isi.csvr.trading.datastore;

import com.dfn.mtr.mix.beans.ExchangeLevelConfData;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hasakam on 10/5/2017.
 */
public class ExchangeConfigListStore {

    private List<ExchangeLevelConfData> store = null;
    private static ExchangeConfigListStore instance = null;

    private ExchangeConfigListStore() {
        store = new ArrayList<ExchangeLevelConfData>();
    }

    public static ExchangeConfigListStore getSharedInstance() {
        if (instance == null) {
             instance = new ExchangeConfigListStore();
        }
        return instance;
    }

    public void addToStore(List<ExchangeLevelConfData> list) {
        for (ExchangeLevelConfData obj : list) {
            store.add(obj);
        }
    }

    public int getExgTypeConfig(String exchange) {
        int oneSideValidation = 0;
        if (!store.isEmpty()) {
            for (ExchangeLevelConfData obj : store) {
                if (obj.getOneSideMinMaxValidation() != 0 && obj.getExchangeCode().equals(exchange)) {
                    oneSideValidation = obj.getOneSideMinMaxValidation();
                }
            }
        }
        return oneSideValidation;
    }

    public int getMaximunGtdDays(String exchange) {
        int maxDays =0;
        if (!store.isEmpty()) {
            for (ExchangeLevelConfData obj : store) {
                if (obj.getExchangeCode().equals(exchange)) {
                    maxDays = obj.getGtdMaxNoOfDays();
                }
            }
        }
        return maxDays;
    }
}
