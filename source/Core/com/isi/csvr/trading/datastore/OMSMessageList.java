package com.isi.csvr.trading.datastore;

import com.isi.csvr.trading.shared.TradeMessage;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.trading.SendQueue;
import com.isi.csvr.shared.*;

import javax.swing.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Jun 1, 2006
 * Time: 10:12:43 PM
 * To change this template use File | Settings | File Templates.
 */
public class OMSMessageList {

    private long lastPrimaryMessageID;
    private long lastSecondaryMessageID;
    private String messageBody;
    private long messageID;
    private static OMSMessageList self;

    private OMSMessageList(){
        try {
            String messID = (Settings.getItem("OMS_PRIMARY_MESS_ID"));
            if ((messID!=null) &&(!messID.equals(""))){
                lastPrimaryMessageID = Long.parseLong(messID);
            }else{
                lastPrimaryMessageID = 0;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            lastPrimaryMessageID = 0;
        }
        try {
            String messID = (Settings.getItem("OMS_SECONDARY_MESS_ID"));
            if ((messID!=null) &&(!messID.equals(""))){
                lastSecondaryMessageID = Long.parseLong(messID);
            }else{
                lastSecondaryMessageID = 0;
            }
        } catch (NumberFormatException e) {
            e.printStackTrace();
            lastSecondaryMessageID = 0;
        }
    }

    public static OMSMessageList getSharedInstance(){
        if(self==null){
            self = new OMSMessageList();
        }
        return self;
    }

    public void addData(String record){
        try {
            TradeMessage message = new TradeMessage(record);
            String[] messageData = (message.getMessageData()).split(TradeMeta.DS);
            byte path = Constants.PATH_PRIMARY ;
            for(int i=0; i< messageData.length; i++){
                String[] data = messageData[i].split(TradeMeta.FD);
                for(int j= 0; j < data.length; j++){
                    try {
                        if(data[j].startsWith("A")){
                            messageID = Long.parseLong(data[j].substring(1));
//                        }else if(data[j].startsWith("B")){
//                            addedDate = (data[j].substring(1));
//                        } else if(data[j].startsWith("D")){
//                            messageEn = (data[j].substring(1));
                        } else if(data[j].startsWith("C")){
                            messageBody = Language.getLanguageSpecificString(data[j].substring(1));
                            messageBody = UnicodeUtils.getNativeStringFromCompressed(messageBody);
//                        } else if(data[j].startsWith("E")){
//                            expiryDate = (data[j].substring(1));
                        } else if(data[j].startsWith("F")){
                            path = TradingShared.getTrader().getPathForSysID(data[j].substring(1));
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
                        // ignore the message
                    }
                }
                if(path == Constants.PATH_SECONDARY){
                    if (messageID > lastSecondaryMessageID) {
                        showMessage(messageBody);
                        lastSecondaryMessageID = messageID;
                        Settings.saveItem("OMS_SECONDARY_MESS_ID", ""+lastSecondaryMessageID);
                    }
                } else{
                    if (messageID > lastPrimaryMessageID) {
                        showMessage(messageBody);
                        lastPrimaryMessageID = messageID;
                        Settings.saveItem("OMS_PRIMARY_MESS_ID", ""+lastPrimaryMessageID);
                    }
                }
                path = Constants.PATH_PRIMARY;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    private void showMessage(String message){
        if ((message != null) && (!message.trim().equals(""))){
            SharedMethods.showMessage(message, JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public void requestOMSMessages(){
        TradeMessage message = new TradeMessage(TradeMeta.MT_OMS_SYSTEM_MESSAGE);
        message.addData("A" + lastPrimaryMessageID);
        message.addData("B" + TradingShared.getTrader().getUserID(Constants.PATH_PRIMARY));
        SendQueue.getSharedInstance().addData(message.toString(), Constants.PATH_PRIMARY);
        message = null;
    }

    public void requestSecondaryOMSMessages(){
        TradeMessage message = new TradeMessage(TradeMeta.MT_OMS_SYSTEM_MESSAGE);
        message.addData("A" + lastSecondaryMessageID);
        message.addData("B" + TradingShared.getTrader().getUserID(Constants.PATH_SECONDARY));
        SendQueue.getSharedInstance().addData(message.toString(), Constants.PATH_SECONDARY);
        message = null;
    }
    public long getLastSecondaryMessageID(){
        return lastSecondaryMessageID;
    }
    public long getLastPrimaryMessageID(){
        return lastPrimaryMessageID;
    }
}
