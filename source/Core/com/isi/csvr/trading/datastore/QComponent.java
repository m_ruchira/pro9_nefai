package com.isi.csvr.trading.datastore;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.shared.TWComboItem;

import java.awt.*;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Oct 27, 2005
 * Time: 10:17:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class QComponent extends TWComboItem implements Serializable {

    public QComponent(long ID, String caption) {
        super("" + ID,caption);
    }

    public String getValue() {
        return Language.getLanguageSpecificString(UnicodeUtils.getNativeString(value), ",");
    }

    public String getFullCaption() {
        return value;
    }

    public void setCaption(String caption) {
        this.value = caption;
    }
     public String toString(){
         return getValue();
     }
}
