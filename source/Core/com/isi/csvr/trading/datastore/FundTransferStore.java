package com.isi.csvr.trading.datastore;

import com.isi.csvr.trading.shared.FundTransfer;
import com.isi.csvr.trading.*;

import java.util.ArrayList;
import java.util.ListIterator;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 9, 2006
 * Time: 5:19:28 PM
 * To change this template use File | Settings | File Templates.
 */
public class FundTransferStore {

    private ArrayList<FundTransfer> store;
    private ArrayList<FundTransfer> filteredStore;

    private String filterType = "*";
    private String filterMethod = "*";
    private String filterPortfolio = "*";

    public static FundTransferStore self = null;

    private FundTransferStore() {
        try {
            store = new ArrayList<FundTransfer>();
            filteredStore = new ArrayList<FundTransfer>();
//            for (int i=0; i<5; i++) {
//                WithdrawaRequest wr = new WithdrawaRequest();
//
//                wr.setAccount("111");
//                wr.setAmount(2222);
//                wr.setAvailableBalance(3333);
//                wr.setCurrency("AED");
//                wr.setDate("22/02/2222");
//                wr.setHoldings(44444);
//                wr.setLedgerBalance(55555);
//                wr.setMethod(2);
//                wr.setPortfolio("S000000000");
//                wr.setReference(6666666);
//                wr.setSellPending(7777);
//                wr.setStatus(1);
//                addTransaction(wr);
//            }


        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static synchronized FundTransferStore getSharedInstance() {
        if (self == null) {
            self = new FundTransferStore();
        }
        return self;
    }

    public void addTransaction(FundTransfer transaction) {
        store.add(transaction);
//        applyFilter();
    }

    public FundTransfer getFilteredTransaction(int index) {
        return filteredStore.get(index);
    }

    public FundTransfer getFilteredTransaction(String referenceID){
        for(int i=0; i<filteredStore.size(); i++){
           // String reference = filteredStore.get(i).getReference();
            String reference = filteredStore.get(i).getCashTransactionID();
            if(reference.equals(referenceID)){
                return filteredStore.get(i);
            }
        }
        return null;
    }

    public FundTransfer getTransaction(int index) {
        return store.get(index);
    }

    public ListIterator<FundTransfer> getTransactions() {
        return store.listIterator();
    }

    public void setFilter(String type, String method, String portfolio) {
        filterType = type;
        filterMethod = method;
        filterPortfolio = portfolio;
        applyFilter();
    }

    public void applyFilter() {
        boolean typeOK = false;
        boolean methodOK = false;
        boolean portfolioOK = false;

        FundTransfer transaction;
        filteredStore.clear();
        for (int i = 0; i < store.size(); i++) {
            transaction = store.get(i);
            typeOK = false;
            methodOK = false;
            portfolioOK = false;

            if ((filterType.equals("*")) || (filterType.equals("" + transaction.getType()))) {
                typeOK = true;
            }

            if ((filterMethod.equals("*")) || (filterMethod.equals("" + transaction.getStatus()))) {
                methodOK = true;
            }

            if ((filterPortfolio.equals("*")) || (filterPortfolio.equals(transaction.getPortfolio()))) {
                portfolioOK = true;
            }
            
            if (typeOK && methodOK && portfolioOK) {
                filteredStore.add(0, transaction);
            }
            transaction = null;
        }
        TradeMethods.getSharedInstance().getFundTransferWindow().sort();
    }

    public int filteredSize() {
        return filteredStore.size();
    }

    public int size() {
        return store.size();
    }

    public void clear() {
        store.clear();
        store.trimToSize();
        filteredStore.clear();
        filteredStore.trimToSize();
    }

}
