package com.isi.csvr.trading.datastore;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 17, 2006
 * Time: 6:12:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class StockTransferObject {

    private String ID;
    private String transferDate;
    private int qty;
    private String symbol;
    private double avgCost;
    private String portfolioID;
    private String broker;
    private int status;
    private int rejectReason;
    private int instrument;
    private String exchange;

    public StockTransferObject(String ID){
        this.ID = ID;
    }

    public StockTransferObject(String ID,String symbol, int quantity, String transferDate, double avgCost){
        this.ID = ID;
        this.symbol = symbol;
        this.qty = quantity;
        this.avgCost = avgCost;
        this.transferDate = transferDate;
    }

    public String getID(){
        return ID;
    }

    public String getTransferDate() {
        return transferDate;
    }

    public void setTransferDate(String transferDate) {
        this.transferDate = transferDate;
    }

    public int getQuantity() {
        return qty;
    }

    public void setQuantity(int quantity) {
        this.qty = quantity;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public double getAvgCost() {
        return avgCost;
    }

    public void setAvgCost(double avgCost) {
        this.avgCost = avgCost;
    }

    public String getPortfolioID() {
        return portfolioID;
    }

    public void setPortfolioID(String portfolioID) {
        this.portfolioID = portfolioID;
    }

    public String getBroker() {
        return broker;
    }

    public void setBroker(String broker) {
        this.broker = broker;
    }

    public int getStatus() {
        return status;
    }

    public void setStatus(int status) {
        this.status = status;
    }

    public int getRejectReason() {
        return rejectReason;
    }

    public void setRejectReason(int rejectReason) {
        this.rejectReason = rejectReason;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public int getInstrument() {
        return instrument;
    }

    public void setInstrument(int instrument) {
        this.instrument = instrument;
    }
}
