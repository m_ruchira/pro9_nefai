package com.isi.csvr.trading.datastore;


import com.isi.csvr.shared.*;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 10, 2005
 * Time: 3:14:26 PM
 */
public class RuleManager {
    private LinkedList<Rule> ruleStore;
    private Hashtable<String, Rule> commissionRules;
    private Hashtable<String, String> conditionalExceptions;

    private static RuleManager self = null;

    public static synchronized RuleManager getSharedInstance() {
        if (self == null) {
            self = new RuleManager();
        }
        return self;
    }

    private RuleManager() {
        ruleStore = new LinkedList<Rule>();
        commissionRules = new Hashtable<String, Rule>();
        conditionalExceptions = new Hashtable<String, String>();
    }

    public void setRule(String id, String exchange, String category, String rule, String message, String portfolio) {
 
        Rule record = new Rule(id, exchange, category, portfolio, rule, message);

        ruleStore.add(record);
        if (category.equals("COMMISSION")){
            commissionRules.put(exchange + "_" + portfolio, record);
        } else if (id.equalsIgnoreCase("CONDITIONAL_EXCEPTIONS")){
            String exchanges[] = exchange.split(",");
            for (String excahangeid: exchanges){
                conditionalExceptions.put(excahangeid, rule);
            }
            exchanges = null;
        }
        record = null;

        System.out.println("Rule " + id + " " + exchange + " " + category + " " + rule + " " + message);
    }

    public Rule getRule(String id, String exchange, String group) {
        if (id == null) {
            return getRule(exchange, group);
        } else {
            for (Rule rule : ruleStore) {
                if ((rule.getId().equalsIgnoreCase(id)) &&
                        SharedMethods.contains(rule.getExchange().split(","), exchange) &&
//                        (rule.getExchange().indexOf(exchange) >= 0) &&
                        (rule.getCategory().equalsIgnoreCase(group))) {
                    return rule;
                }
            }
        }
        return null;
    }

    public List<Rule> getRuleList(String id, String exchange, String group) {
        List<Rule> ruleList = new ArrayList<Rule>();
        if (id != null) {
            for (Rule rule : ruleStore) {
                if ((rule.getId().equalsIgnoreCase(id)) &&
                        SharedMethods.contains(rule.getExchange().split(","), exchange) &&
//                        (rule.getExchange().indexOf(exchange) >= 0) &&
                        (rule.getCategory().equalsIgnoreCase(group))) {
                    ruleList.add(rule);
                }
            }
        }
        return ruleList;
    }

    public Rule getCommissionRule(String portfolio, String exchange) {
        return commissionRules.get(exchange + "_" + portfolio);
    }

    public Rule getRule(String exchange, String group) {
        try {
            for (Rule rule : ruleStore) {
                if (SharedMethods.contains(rule.getExchange().split(","), exchange) && (rule.getCategory().equals(group))) {   //(rule.getExchange().indexOf(exchange) >= 0)
                    return rule;
                }
            }
            return null;
        } catch (Exception e) {
            return null;
        }
    }

    public Iterator<Rule> getRules() {
        return ruleStore.iterator();
//        return ruleStore.elements();
    }

    public int ruleCount() {
        return ruleStore.size();
    }

    public boolean isExcempted(String exchange, String id){
        try {
            String[] tags = conditionalExceptions.get(exchange).split(",");
            for (String tag: tags) {
                if (tag.equals(id)){
                    System.out.println("Excepted " + exchange + " " + id);
                    return true;
                }
            }
            return false;
        } catch (Exception e) {
            return false;
        }

    }

    public void clear(){
        ruleStore.clear();
        commissionRules.clear();
        conditionalExceptions.clear();
    }

    public void showRules() {
        StringBuilder buffer = new StringBuilder();

        buffer.append("Rule Store -------\n");
        for (Rule rule: ruleStore){
            buffer.append('\t');
            buffer.append(rule);
            buffer.append('\n');
        }
        buffer.append("Commission Store -------\n");
        for (Rule rule: commissionRules.values()){
            buffer.append('\t');
            buffer.append(rule);
            buffer.append('\n');
        }
        System.out.println(buffer.toString());
    }
}
