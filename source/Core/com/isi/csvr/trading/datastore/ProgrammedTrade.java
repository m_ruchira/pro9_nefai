package com.isi.csvr.trading.datastore;

import com.isi.csvr.symbolfilter.FunctionBuilderComboItem;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jun 4, 2004
 * Time: 3:59:30 PM
 */
public class ProgrammedTrade extends QueuedTransaction {
    private FunctionBuilderComboItem field;
    private FunctionBuilderComboItem condition;
    private String value;

    public ProgrammedTrade(long messageID, String symbol, int action, int type, double price,
                           long quanity, long minFill, boolean allOrNone, boolean fillOrKill, long goodTill,//String goodTill,
                           FunctionBuilderComboItem field, FunctionBuilderComboItem condition, String value) {
        //super(messageID, symbol, action, type, price, quanity, minFill, allOrNone, fillOrKill,goodTill);
        super(0);
        this.field = field;
        this.condition = condition;
        this.value = value;

    }

    public FunctionBuilderComboItem getCondition() {
        return condition;
    }

    public void setCondition(FunctionBuilderComboItem condition) {
        this.condition = condition;
    }

    public FunctionBuilderComboItem getField() {
        return field;
    }

    public void setField(FunctionBuilderComboItem field) {
        this.field = field;
    }

    public String getValue() {
        return value;
    }

    public void setValue(String value) {
        this.value = value;
    }
}
