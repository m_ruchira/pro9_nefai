package com.isi.csvr.trading.datastore;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Nov 8, 2004
 * Time: 3:45:40 PM
 * To change this template use File | Settings | File Templates.
 */
public class Broker {
    private String id;
    private String decription;
    private ArrayList ips;
    private short type;

    public Broker(String id, String decription, short type) {
        this.id = id;
        this.decription = decription;
        ips = new ArrayList();
        this.type = type;
    }

    public String getId() {
        return id;
    }

    public String getDecription() {
        return decription;
    }

    public void addIP(String ip) {
        ips.add(ip);
    }

    public String getIP(int index) {
        return (String) ips.get(index);
    }

    public Object[] getIPs() {
        return ips.toArray();
    }

    public int getIPCount() {
        return ips.size();
    }

    public short getType() {
        return type;
    }

    public void setType(short type) {
        this.type = type;
    }
}
