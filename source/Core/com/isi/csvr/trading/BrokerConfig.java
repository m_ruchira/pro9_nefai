package com.isi.csvr.trading;

import com.isi.csvr.properties.*;
import com.isi.csvr.trading.shared.*;
import com.isi.csvr.trading.connection.*;
import com.isi.csvr.trading.datastore.*;
import com.isi.csvr.shared.*;

import java.io.*;
import java.util.Hashtable;
import java.math.BigDecimal;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Feb 18, 2008
 * Time: 11:42:28 AM
 * To change this template use File | Settings | File Templates.
 */
public class BrokerConfig implements TradingConnectionListener {

    private Hashtable<Byte, SmartProperties> store;
//    private SmartProperties prop;
    private static BrokerConfig self = null;

    private BrokerConfig(){
//        prop = new SmartProperties("=");
        store = new Hashtable<Byte, SmartProperties>();
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
    }

    public static BrokerConfig getSharedInstance(){
        if(self == null){
            self = new BrokerConfig();
        }
        return self;
    }

    public boolean getBooleanItem(String id, byte path){
        return getBooleanItem(id, true, path);
    }

    public boolean getBooleanItem(String id, boolean defaultValue, byte path){
        try {
            return store.get(path).getProperty(id).equalsIgnoreCase("TRUE");
        } catch(Exception e) {
            return defaultValue;
        }
    }

    public boolean showOrderHistoryInBlotter(byte path){
        try {
            return getBooleanItem("SHOW_ORDER_HISTORY_IN_BLOTTER", path);
        } catch(Exception e) {
            return true;
        }
    }

    public boolean showOrderDetails(byte path){
           try {
            return getBooleanItem("SHOW_ORDER_DETAILS_IN_BLOTTER", path);
        } catch(Exception e) {
            return true;
        }
    }

    public String getOrderTypesForOrderSearch(byte path){
        try {
            return (store.get(path).getProperty("SHOW_ORDER_TYPES_FOR_ORDER_SEARCH"));
        } catch(Exception e) {
            return null;
        }
    }

    public boolean canChangeTradingPWinSSO() {
        try {
            String value = store.get(Constants.PATH_PRIMARY).getProperty("CAN_CHANGE_TRADING_PASSWORD_IN_SSO");
            return ((value.trim().equals("1")) || (value.trim().toLowerCase().equals("true")));
        } catch (Exception e) {
            return true;
        }
    }

    public int getOrderSearchValidMonthRange(byte path){
        try {
            return Integer.parseInt(store.get(path).getProperty("VALID_MONTH_RANGE_FOR_ORDER_SEARCH"));
        } catch(Exception e) {
            return 0;
        }
    }

    /*public void loadData() {
        prop.clear();
        try {
            prop.loadCompressed(Settings.getAbsolutepath() + "/System/Broker/" + TradingShared.getBrokerID() + "_config.msf");
        } catch (Exception e) {
            try {
                prop.loadCompressed(Settings.getAbsolutepath() + "/System/Broker/_config.msf");
            } catch (Exception e1) {

            }
        }
    }*/

     public void loadData(String broker, byte path) {
         SmartProperties prop = new SmartProperties("=");
         try {
            prop.loadCompressed(Settings.CONFIG_DATA_PATH + "/Broker/" + broker + "_config.msf");
        } catch (Exception e) {
            try {
                prop.loadCompressed(Settings.CONFIG_DATA_PATH + "/Broker/_config.msf");
            } catch (Exception e1) {
            }
        }
         store.put(path, prop);
     }

     public String[] getWithdrawalTypes(byte path){
        try {
            return store.get(path).getProperty("WITHDRAWAL_TYPES").split(",");
        } catch (Exception e) {
            return null;
        }
    }

    public String[] getDepositTypes(byte path){
        try {
            return store.get(path).getProperty("DEPOSIT_TYPES").split(",");
        } catch (Exception e) {
            return null;
        }
    }

    public void tradeServerConnected() {
    }

    public void tradeServerDisconnected() {
    }

    public void tradeSecondaryPathConnected() {
    }

    public boolean isMarginableSymbolsAvailable(byte path){
       return  getBooleanItem("MARGINABLE_SYMBOLS_AVAILABLE", false, path);
    }

    public boolean isSliceOrderTimeInrevalAvailable(byte path){
      return  getBooleanItem("SLICE_ORDER_ATINTERVAL_AVAILABLE", true, path);
    }


    public boolean isOPenPositionsAvailable(byte path){
       return getBooleanItem("OPEN_POSITIONS_AVAILABLE", false, path);
    }

    public boolean isDepositWindowAvailable(byte path){
       return getBooleanItem("FUND_TRANSFER_DIALOG", false, path);
    }

    public boolean isWithdrawalWindowAvailable(byte path){
       return getBooleanItem("WITHDRAWAL_DIALOG", false, path);
    }

    public boolean isCashTransferAvailable(byte path){
       return getBooleanItem("CASH_TRANSFER_DIALOG", true, path);
    }
    public boolean isCashLogSearchAvailable(byte path){
       return getBooleanItem("CASH_LOG_SEARCH", true, path);
    }
    public boolean isCustomerStatementAvailable(byte path){
       return getBooleanItem("CUSTOMER_STATEMENT", true, path);
    }
    public boolean isLiquidateAllAvailable(byte path){
        return getBooleanItem("SHOW_LIQUIDATE_ALL", false, path);
    }

    public boolean isRemoveMarketCodeFromSymbol(byte path){
        return getBooleanItem("REMOVE_MARKETCODE_FROM_SYMBOL", false, path);
    }

    public boolean isIntradayPositionMonitorAvailable(byte path){
        return getBooleanItem("IS_INTRADAY_POSITION_MONITER_AVAILABLE", false, path); 
    }

    public boolean isOrderAmendAllowedByBroker(byte path, String orderCatagory){
        if(orderCatagory==null || orderCatagory.trim().isEmpty()){
            return false;
        }
       try {
            String[] orderTypes= store.get(path).getProperty("ORDER_TYPES_NOT_ALLOWED_FOR_AMMEND").split(",");
           if(orderTypes== null || orderTypes.length==0){
               return true;
           }
           for(int i=0; i<orderTypes.length; i++){
               if(orderTypes[i].trim().equalsIgnoreCase(orderCatagory)){
                   return false;
               }

           }
           return true;
        } catch (Exception e) {
            return true;
        }
    }

     public boolean isRemoveMarketCodeFromSymbolForAllBrokers(){
        boolean value = getBooleanItem("REMOVE_MARKETCODE_FROM_SYMBOL", false, Constants.PATH_PRIMARY);
        if(!value){
            value = getBooleanItem("REMOVE_MARKETCODE_FROM_SYMBOL", false, Constants.PATH_SECONDARY);
        }
         return value;
    }
     public String getDUInvestURL(){
      try {
            String value = store.get(Constants.PATH_PRIMARY).getProperty("DU_INVEST_URL");
            return value;
        } catch (Exception e) {
            return null;
        }
    }

    public int getCoverageDecimalCount(){
         try {
            String value = store.get(Constants.PATH_PRIMARY).getProperty("COVERAGE_DECIMALS");
            if(value!= null && (!value.trim().isEmpty())){
                return Integer.parseInt(value);
            }else{
                return 2;
            }
        } catch (Exception e) {
            return 2;
        }
    }

    public int getCoverageDecimalRoundingMode(){
         try {
            String value = store.get(Constants.PATH_PRIMARY).getProperty("COVERAGE_ROUND_MODE");
            if(value!= null && (!value.trim().isEmpty())){
                int val = Integer.parseInt(value);
                if(val < BigDecimal.ROUND_UP || val > BigDecimal.ROUND_UNNECESSARY ){
                    val = BigDecimal.ROUND_DOWN;
                }
                return Integer.parseInt(value);
            }else{
                return BigDecimal.ROUND_DOWN;
            }
        } catch (Exception e) {
            return BigDecimal.ROUND_DOWN;
        }
    }

    public boolean isCashBalAddedToTotPf(){
        boolean value = getBooleanItem("IS_CASH_BALANCE_ADDED_TO_TOT_PF", false, Constants.PATH_PRIMARY);
        return value;
    }

     public boolean isLoggedInUserShown(){
        boolean value = getBooleanItem("IS_LOGGED_IN_USER_SHOWN", true, Constants.PATH_PRIMARY);
        return value;
    }

     public boolean isPrevNextInOrderSearchHidden(){
        boolean value = getBooleanItem("HIDE_PREV_NEXT_IN_ORDER_SEARCH", false, Constants.PATH_PRIMARY);
        return value;
    }
}
