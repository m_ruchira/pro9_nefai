package com.isi.csvr.trading.positionMoniter;

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.table.Table;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.shared.*;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.tabbedpane.TWTabbedPaleListener;
import com.isi.csvr.tabbedpane.TWTabEvent;
import com.isi.csvr.*;
import com.isi.csvr.event.Application;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.datastore.Rule;
import com.isi.csvr.trading.datastore.RuleManager;
import com.isi.csvr.trading.portfolio.TradePortfolios;
import com.isi.csvr.trading.portfolio.ValuationRecord;
import com.isi.csvr.trading.connection.TradingConnectionListener;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.shared.*;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.PopupMenuListener;
import javax.swing.event.PopupMenuEvent;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;

import bsh.Interpreter;
import bsh.EvalError;

/**
 * Created by IntelliJ IDEA.
 * User: udayaa
 * Date: Jul 9, 2010
 * Time: 12:13:08 PM
 * To change this template use File | Settings | File Templates.
 */
public class IntraDayPositionMoniterUI extends InternalFrame implements TWTabbedPaleListener, TradingConnectionListener, ItemListener, TraderProfileDataListener {
    static IntraDayPositionMoniterUI self;
    static String PANEL_ALL = "ALL";
    static String PANEL_PF = "PF";

    ViewSetting settingSinglePf;
    ViewSetting settingAllPf;
    Table tableSinglePf;
    Table tableAllPf;
    PositionModel modelSinglePf;
    PositionModelAll modelAllPf;
    TWTabbedPane tabpane;
    JPanel middleContainerPanel;
    JPanel tablePanel;
    JPanel tablePanelAll;
    JPanel tablePanelPF;
    TWComboBox currency;
    JPanel currencyPanel;
    ArrayList<TradingPortfolioRecord> portfolios;
    String selectedCurrency = "";
    TWMenuItem mnuClosePosition;
    TWMenuItem mnuCloseAllPosition;
    TWMenu mnuClosePositionAllPf;

    private String selectedPfId;
    ArrayList<String> pfNames;

     private TWRadioButtonMenuItem mnuDCNoDecimal;
    private TWRadioButtonMenuItem mnuDCDefaultDecimal;
    private TWRadioButtonMenuItem mnuDCOneDecimal;
    private TWRadioButtonMenuItem mnuDCTwoDecimal;
    private TWRadioButtonMenuItem mnuDCThrDecimal;
    private TWRadioButtonMenuItem mnuDCFouDecimal;
    private TWRadioButtonMenuItem mnuDCFiveDecimal;
    private TWRadioButtonMenuItem mnuDCSixDecimal;
//

    //unified currency mode
    private TWMenu mnuDecimalPlaces;
    private TWRadioButtonMenuItem mnuNoDecimal;
    private TWRadioButtonMenuItem mnuDefaultDecimal;
    private TWRadioButtonMenuItem mnuOneDecimal;
    private TWRadioButtonMenuItem mnuTwoDecimal;
    private TWRadioButtonMenuItem mnuThrDecimal;
    private TWRadioButtonMenuItem mnuFouDecimal;
    private TWRadioButtonMenuItem mnuFiveDecimal;
    private TWRadioButtonMenuItem mnuSixDecimal;

    byte decimalPlaces =2;
    byte dcDecimalPlaces =2;
     ArrayList<String> idlist;


    private IntraDayPositionMoniterUI() {
        initUI();


    }

    public static IntraDayPositionMoniterUI getSharedInstance() {
        if (self == null) {
            self = new IntraDayPositionMoniterUI();
        }
        return self;
    }


    public void initUI() {

//      settingSinglePf = ViewSettingsManager.getSummaryView("TRADE_INTRADY_POSITION_MONITER");

        try {
            settingSinglePf = (ViewSetting) Client.getInstance().getViewSettingsManager().getPortfolioView(1280, "11").clone();
            settingAllPf = (ViewSetting) Client.getInstance().getViewSettingsManager().getPortfolioView(1280, "12").clone();
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        GUISettings.setColumnSettings(settingSinglePf, null);
        GUISettings.setColumnSettings(settingAllPf, null);
        setTitle(Language.getString("TRADE_INTRADAY_POSITION_MONITER"));
        setSize(settingSinglePf.getSize());
        tableSinglePf = new Table();
        modelSinglePf = new PositionModel();
        modelSinglePf.setViewSettings(settingSinglePf);
        tableSinglePf.setModel(modelSinglePf);
        modelSinglePf.setTable(tableSinglePf);
        tableSinglePf.getTable().setDefaultRenderer(Object.class, new IntradayPositionRenderer(modelSinglePf.getRendIDs()));
        tableSinglePf.getTable().setDefaultRenderer(String.class, new IntradayPositionRenderer(modelSinglePf.getRendIDs()));
        tableSinglePf.getTable().setDefaultRenderer(Double.class, new IntradayPositionRenderer(modelSinglePf.getRendIDs()));
        tableSinglePf.getTable().setDefaultRenderer(Long.class, new IntradayPositionRenderer(modelSinglePf.getRendIDs()));
        tableSinglePf.getTable().setDefaultRenderer(Number.class, new IntradayPositionRenderer(modelSinglePf.getRendIDs()));

        modelAllPf = new PositionModelAll(PositionDataStore.getSharedInstance().getAllPositionStore());
        tableAllPf = new Table();
        modelAllPf.setViewSettings(settingAllPf);
        tableAllPf.setModel(modelAllPf);
        modelAllPf.setTable(tableAllPf);
        tableAllPf.getTable().setDefaultRenderer(Object.class, new IntradayPositionRenderer(modelSinglePf.getRendIDs()));
        tableAllPf.getTable().setDefaultRenderer(String.class, new IntradayPositionRenderer(modelSinglePf.getRendIDs()));
        tableAllPf.getTable().setDefaultRenderer(Double.class, new IntradayPositionRenderer(modelSinglePf.getRendIDs()));
        tableAllPf.getTable().setDefaultRenderer(Long.class, new IntradayPositionRenderer(modelSinglePf.getRendIDs()));
        tableAllPf.getTable().setDefaultRenderer(Number.class, new IntradayPositionRenderer(modelSinglePf.getRendIDs()));


        this.setViewSetting(settingAllPf);
        portfolios = new ArrayList<TradingPortfolioRecord>();
        pfNames = new ArrayList<String>();
        settingAllPf.setParent(this);
        settingSinglePf.setParent(this);
        applySettings();
        createTabpane();
        currency = new TWComboBox();
        idlist = new ArrayList<String>();
        createCurrencyPane();
        this.add(currencyPanel, BorderLayout.NORTH);
        this.add(middleContainerPanel, BorderLayout.CENTER);
        setLayer(GUISettings.TOP_LAYER);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        Client.getInstance().getDesktop().add(this);
        TradingConnectionNotifier.getInstance().addConnectionListener(this);
        setClosable(true);
        setIconifiable(true);
        setMaximizable(true);
        setResizable(true);
        setColumnResizeable(true);
        setDetachable(true);
        //setPrintable(true);
        this.setTable(tableAllPf);
        this.setTable2(new Table[]{tableSinglePf});
        setSelectedPanel(PANEL_ALL);
        populateTradeData();
        populateCurrencyData();
        createMenus();
        setDecimalPlaces(decimalPlaces);
        setDCDecimalPlaces(dcDecimalPlaces);
        currency.addItemListener(this);
        Application.getInstance().addApplicationListener(this);
        tabpane.addTabPanelListener(this);
       

    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource().equals(currency)) {
            String currency = (String) e.getItem();
            modelAllPf.setCurrency(currency);                                    
            selectedCurrency = currency;
        }
    }

    private void createMenus() {
        mnuClosePosition = new TWMenuItem(Language.getString("INTRADAY_POSITION_CLOSE_POSITION"), "intradayPositionclose.gif");
        mnuClosePosition.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                closePosition((IntraDayRecord) tableSinglePf.getModel().getValueAt(tableSinglePf.getTable().getSelectedRow(), -1));
            }
        });

        mnuClosePositionAllPf = new TWMenu(Language.getString("INTRADAY_POSITION_CLOSE_POSITION"), "intradayPositionclose.gif");
        mnuClosePositionAllPf.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            }
        });

        mnuCloseAllPosition = new TWMenuItem(Language.getString("INTRADAY_POSITION_CLOSE_ALL_POSITIONS"), "intradayPositioncloseall.gif");
        mnuCloseAllPosition.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                liquidateOrders(modelSinglePf.getDataStore(), selectedPfId);
            }
        });

        tableSinglePf.getPopup().add(mnuClosePosition);
        tableSinglePf.getPopup().add(mnuCloseAllPosition);

        tableAllPf.getPopup().add(mnuClosePositionAllPf);
         createDCDecimalMenu( tableAllPf.getPopup());
        createDecimalMenu( tableSinglePf.getPopup());
        tableSinglePf.getPopup().addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                IntraDayRecord record = (IntraDayRecord) tableSinglePf.getModel().getValueAt(tableSinglePf.getTable().getSelectedRow(), -1);
                if (record == null) {
                    mnuClosePosition.setEnabled(false);
                    mnuCloseAllPosition.setEnabled(false);

                } else if (record.getHoldings() <= 0) {
                    mnuClosePosition.setEnabled(false);
                    mnuCloseAllPosition.setEnabled(false);
                } else {
                    mnuClosePosition.setEnabled(true);
                    mnuCloseAllPosition.setEnabled(true);
                }
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                mnuClosePosition.setEnabled(true);
                mnuCloseAllPosition.setEnabled(true);
            }

            public void popupMenuCanceled(PopupMenuEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

        tableAllPf.getPopup().addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                IntraDayRecord record = (IntraDayRecord) tableAllPf.getModel().getValueAt(tableAllPf.getTable().getSelectedRow(), -1);
                if (record == null) {
                    mnuClosePositionAllPf.setEnabled(false);
                } else if (record.getHoldings() <= 0) {
                    mnuClosePositionAllPf.setEnabled(false);
                } else {
                    createSubMenu(mnuClosePositionAllPf, ((IntraDayRecord) tableAllPf.getModel().getValueAt(tableAllPf.getTable().getSelectedRow(), -1)).getSKey());
                    mnuClosePositionAllPf.setEnabled(true);
                }
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
                mnuClosePositionAllPf.removeAll();
                mnuClosePositionAllPf.setEnabled(true);
            }

            public void popupMenuCanceled(PopupMenuEvent e) {
                //To change body of implemented methods use File | Settings | File Templates.
            }
        });

    }

    private void createTabpane() {
        tabpane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Simulated, "dt");
        tablePanel = new JPanel(new CardLayout(1, 1));
        middleContainerPanel = new JPanel();
        middleContainerPanel.setLayout(new BorderLayout());
        middleContainerPanel.add(tabpane, BorderLayout.NORTH);
        middleContainerPanel.add(tablePanel, BorderLayout.CENTER);
        tablePanelAll = new JPanel();
        tablePanelAll.setLayout(new BorderLayout());
        tablePanelAll.add(tableAllPf, BorderLayout.CENTER);
        tablePanelPF = new JPanel();
        tablePanelPF.setLayout(new BorderLayout());
        tablePanelPF.add(tableSinglePf, BorderLayout.CENTER);
        tabpane.addTab(0, Language.getString("ALL"), tablePanelAll);
        //tabpane.setSelectedIndex(0);
        tablePanel.add(PANEL_ALL, tablePanelAll);
        tablePanel.add(PANEL_PF, tablePanelPF);
//        tablePanel.add("A",tablePanelAll);

    }

    private void createCurrencyPane() {
        currencyPanel = new JPanel();
        currency = new TWComboBox();
        currency.setPreferredSize(new Dimension(60, 20));
        currencyPanel.setLayout(new FlexGridLayout(new String[]{"100%", "60", "10", "60", "10"}, new String[]{"20"}, 5, 5));
        JLabel curr = new JLabel(Language.getString("CURRENCY"));
        currencyPanel.add(new JLabel());
        currencyPanel.add(curr);
        currencyPanel.add(new JLabel());
        currencyPanel.add(currency);
        currencyPanel.add(new JLabel());
    }

    public void setVisible(boolean value) {
        super.setVisible(value);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void tabStareChanged(TWTabEvent event) {
        if (event.getState() == TWTabEvent.STATE_REMOVED || event.getState() == TWTabEvent.STATE_ADDED) {
            return;
        }
        int selected = tabpane.getSelectedIndex();
        if (selected > 0) {
            setSelectedPanel(PANEL_PF);
        } else {
            setSelectedPanel(PANEL_ALL);
            selectedPfId = PANEL_ALL;
        }
        if (selected > 0) {
            String pfid = portfolios.get(selected - 1).getPortfolioID();
            modelSinglePf.setDataStore(PositionDataStore.getSharedInstance().getFilteredStore(pfid));
            modelSinglePf.setCurrency(getCurrencIDForPortfolio(pfid));
            selectedPfId = pfid;
        }

    }

    private void populateTradeData() {
//        portfolios = TradePortfolios.getInstance().getPortfolioList();
        clearstores();
        portfolios = TradingShared.getTrader().getPortfolios();
        if (portfolios != null && portfolios.size() > 0) {
            for (int i = 0; i < portfolios.size(); i++) {
                TradingPortfolioRecord record = portfolios.get(i);
 //               Account account = TradingShared.getTrader().findAccountByPortfolio(record.getPortfolioID());
                String name =new String(record.getPortfolioID()); //+ "-" + account.getCurrency();
                pfNames.add(name);
//                if (!Language.isLTR()) {
//                    name = account.getCurrency() + "-" + record.getPortfolioID();
//                }
                tabpane.addTab(i + 1, name, tablePanelAll);
            }

        }
        selectTab(0);

    }

    private void populateCurrencyData() {
        ArrayList<String> idlist = TradeMethods.getAccountSummaryCurrensyList();
        try {
        if (idlist != null && !idlist.isEmpty()) {
              //  if(currency. )
            currency.setModel(new TWComboModel(idlist));
                if ((selectedCurrency==null||selectedCurrency.isEmpty()) ){
                //currency.getSelectedItem() == null || currency.getSelectedIndex() <= 0) {
            currency.setSelectedIndex(0);
            selectedCurrency = idlist.get(0);
                }else{
                    int j=   idlist.indexOf(selectedCurrency );
                    if(j<=0){
                        j=0;
                    }
                    currency.setSelectedIndex(j);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

//    private void compareIDLists(ArrayList<String> list) {
//       if(idlist.isEmpty()){
//           for(String curr)
//       }
//
//    }

     private static boolean isIncluded(ArrayList<String> currencylist, String currency) {
        try {
            boolean found = false;
            for (int i = 0; i < currencylist.size(); i++) {
                if (currencylist.get(i).equals(currency)) {
                    found = true;
                }
            }
            return found;
        } catch (Exception e) {
            return false;
        }
    }

    public void setSelectedPanel(String panelID) {
        CardLayout cl = (CardLayout) (tablePanel.getLayout());
        cl.show(tablePanel, panelID);
        cl = null;
    }

    public void tradeServerConnected() {
        try {
        populateTradeData();
        populateCurrencyData();
        tabpane.setSelectedIndex(0);
        if (selectedCurrency.isEmpty()) {
            currency.setSelectedIndex(0);
            modelAllPf.setCurrency(selectedCurrency);

        }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void tradeSecondaryPathConnected() {
        populateTradeData();
        populateCurrencyData();
        tabpane.setSelectedIndex(0);
        if (selectedCurrency.isEmpty()) {
            currency.setSelectedIndex(0);
            modelAllPf.setCurrency(selectedCurrency);
        }
    }

    public void tradeServerDisconnected() {
        portfolios.clear();
        int size = tabpane.getTabCount();
        try {
            if (size > 1) {
                for (int i = size - 1; i > 0; i--) {
                    tabpane.removeTab(i);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        try {
            tabpane.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                 tableAllPf.updateUI();
            }
        });


    }

    private void clearstores(){
       //  portfolios.clear();
        int size = tabpane.getTabCount();
        try {
            if (size > 1) {
                for (int i = size - 1; i > 0; i--) {
                    tabpane.removeTab(i);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public ViewSetting getSettingSinglePf() {
        return settingSinglePf;
    }

    public ViewSetting getSettingAllPf() {
        return settingAllPf;
    }

    public void applySettings() {
        
        super.applySettings();    //To change body of overridden methods use File | Settings | File Templates.
        modelAllPf.applySettings();
        modelSinglePf.applySettings();
    }

    private void selectTab(int i) {
        if (i < tabpane.getTabCount()) {
            tabpane.selectTab(i);
        }
    }

    private void createSubMenu(TWMenu main, String key) {
        main.removeAll();
        ArrayList<IntraDayRecord> records = PositionDataStore.getSharedInstance().getRecordsForSelecteSymbol(key);
        for (IntraDayRecord rec : records) {
            if (rec.getHoldings() <= 0) {
                continue;
            }
            String name = rec.getPfId() + " " + "- " + "[" + rec.getHoldings() + "]";
            if (!Language.isLTR()) {
                name = "[" + rec.getHoldings() + "] " + "- " + rec.getPfId();
            }
            TWMenuItem menu = new TWMenuItem(name);
            menu.setActionCommand(rec.PfId);
            menu.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    String pfid = e.getActionCommand();
                    IntraDayRecord record = (IntraDayRecord) tableAllPf.getModel().getValueAt(tableAllPf.getTable().getSelectedRow(), -1);
                    IntraDayRecord sellrecord = PositionDataStore.getSharedInstance().getRecordByPfId(pfid, record.getSKey());
                    if (sellrecord != null) {
                        closePosition(sellrecord);
                    }
                }
            });
            main.add(menu);
        }

    }

    private void closePosition(IntraDayRecord record) {
        long holding = TradePortfolios.getInstance().getAvailableQuantity(record.getPfId(), record.getSKey());
        long pendingSellQty = TradePortfolios.getInstance().getPendingSellQuantity(record.getPfId(), record.getSKey());
        long available = holding - pendingSellQty;
        long tobesold = record.getHoldings();
//        if (tobesold > 0 && available>0) {
        if (tobesold > 0 ) {
            if (tobesold > available) {
                tobesold = available;
            }
            Client.getInstance().doTransaction(TradeMeta.SELL, record.getSKey(), tobesold,
                    false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_PORTFOLIO, record.getPfId(), null);

        }                
    }

    private void closePositionInMultiplePortfoloios() {


    }

    private void liquidateOrders(ArrayList<IntraDayRecord> dataStore, String PfId) {

        int size = dataStore.size();
        IntraDayRecord record = null;
        boolean first = true;


        if (TradingShared.isReadyForTrading() ) {
            if(!validateDatastore(dataStore)){
                return;
            }

            if ((SharedMethods.showConfirmMessage(Language.getString("MSG_LIQUIDATE_ALL_PORTFOLIOS"), JOptionPane.WARNING_MESSAGE) == JOptionPane.OK_OPTION)) {
                for (int i = size - 1; i >= 0; i--) {
                    boolean allOrNone = false;
                    record = (IntraDayRecord) dataStore.get(i);
                    Stock stock = DataStore.getSharedInstance().getStockByExchangeKey(record.getExchange(), record.getSymbol());
                    long holding = TradePortfolios.getInstance().getAvailableQuantity(selectedPfId, record.getSKey());
                    long pendingSellQty = TradePortfolios.getInstance().getPendingSellQuantity(selectedPfId, record.getSKey());
                    long available = holding - pendingSellQty;
                    long tobesold = record.getHoldings();
                    if (tobesold > 0 && available>0 ) {
                        if (tobesold > available) {
                            tobesold = available;
                        }
                    } else {
                        continue;
                    }
                    try {
                        Rule rule = RuleManager.getSharedInstance().getRule("AON_SELECT", record.getExchange(), "NEW_ORDER");
                        if (rule != null) {
                            Interpreter interpreter = new Interpreter();
                            interpreter.set("instrumentType", stock.getInstrumentType());
                            interpreter.set("quantity", tobesold);
                            interpreter.set("price", Math.round(stock.getLastTradeValue() * 1000) / (double) 1000);
                            int result = (Integer) interpreter.eval(rule.getRule());

                            allOrNone = (result == 1);
                        }
                    } catch (EvalError evalError) {
                        evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    if (first) { // authenticate only the first order of the list
                        boolean sucess = TradeMethods.getSharedInstance().sendNewOrder(true, false, 0, PfId, record.getExchange(),
                                record.getSymbol(), TradeMeta.SELL, TradeMeta.ORDER_TYPE_MARKET, 0,
                                0, "*", tobesold /*record.getOwned(stock.getInstrumentType())*/, (short) 0, 0, -1, 0, allOrNone, stock.getStrikePrice(),
                                TradingShared.formatOpraExpireDate(stock.getExpirationDate()), stock.getInstrumentType(), -1,
                                stock.getOptionBaseSymbol(), -1, null, null, null, -1, null, null, null, 0d, null, 0, 0);
                        if (!sucess) break;
                        first = false;
                    } else {
                        TradeMethods.getSharedInstance().sendNewOrder(false, false, 0, PfId, record.getExchange(),
                                record.getSymbol(), TradeMeta.SELL, TradeMeta.ORDER_TYPE_MARKET, 0,
                                0, "*", tobesold/*record.getOwned(stock.getInstrumentType())*/, (short) 0, 0, -1, 0, allOrNone, stock.getStrikePrice(),
                                TradingShared.formatOpraExpireDate(stock.getExpirationDate()), stock.getInstrumentType(), -1,
                                stock.getOptionBaseSymbol(), -1, null, null, null, -1, null, null, null, 0d, null, 0, 0);
                    }
                }
            }
        } else {
            SharedMethods.showMessage(Language.getString("MSG_NOT_CONNECTED_TO_TRADING"), JOptionPane.WARNING_MESSAGE);
        }
    }

    private boolean validateDatastore(ArrayList<IntraDayRecord> dataStore){
        for(IntraDayRecord rec : dataStore){
            if(rec.getHoldings()>0){
                return true;
            }
        }
        return false;
    }

    private String getCurrencIDForPortfolio(String pfId) {
        Account account = TradingShared.getTrader().findAccountByPortfolio(pfId);
        if (account != null) {
            return account.getCurrency();
        } else {
            return null;
        }
    }

    public void accountDataChanged(String accountID) {
         if (tabpane.getTabCount() > 1) {
            for (int i = 1; i < tabpane.getTabCount(); i++) {
                if(portfolios.isEmpty()){
                    return;
                }
                try {
               String name= portfolios.get(i-1).getPortfolioID();
               Account account = TradingShared.getTrader().findAccountByPortfolio(name);
                 name = name + "-" + account.getCurrency();
                
                if (!Language.isLTR()) {
                    name = account.getCurrency() + "-" + name;
                }
               tabpane.setTitleAt(i,name);
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }

            }
        }
      //  if (selectedCurrency == null || (selectedCurrency.isEmpty())) {
        populateCurrencyData();
      //  }
    }

    public void portfolioDataChanged(String portfolioID) {

    }

     private void createDecimalMenu(JPopupMenu parent) {
        TWMenu mnuDecimalPlaces = new TWMenu(Language.getString("POPUP_DECIMAL_PLACES"), "decimalplaces.gif");
        parent.add(mnuDecimalPlaces);

        //transHistoryTable.getPopup().add(mnuDecimalPlaces);
        ButtonGroup oGroup = new ButtonGroup();
        mnuNoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_NO_DECIMAL"));
        mnuDecimalPlaces.add(mnuNoDecimal);
        mnuNoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.NO_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.NO_DECIMAL_PLACES){
//            mnuNoDecimal.setSelected(true);
//        }
        oGroup.add(mnuNoDecimal);
        mnuDefaultDecimal = new TWRadioButtonMenuItem(Language.getString("DEFAULT_DECIMAL"));
        mnuDecimalPlaces.add(mnuDefaultDecimal);
        mnuDefaultDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.NO_DECIMAL_PLACES){
//            mnuNoDecimal.setSelected(true);
//        }
        oGroup.add(mnuDefaultDecimal);
        mnuOneDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_ONE_DECIMAL"));
        mnuDecimalPlaces.add(mnuOneDecimal);
        mnuOneDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.ONE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.ONE_DECIMAL_PLACES){
//            mnuOneDecimal.setSelected(true);
//        }
        oGroup.add(mnuOneDecimal);
        mnuTwoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_TWO_DECIMAL"));
        mnuDecimalPlaces.add(mnuTwoDecimal);
        mnuTwoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.TWO_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.TWO_DECIMAL_PLACES){
//            mnuTwoDecimal.setSelected(true);
//        }
        oGroup.add(mnuTwoDecimal);
        mnuThrDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_THREE_DECIMAL"));
        mnuDecimalPlaces.add(mnuThrDecimal);
        mnuThrDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.THREE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.THREE_DECIMAL_PLACES){
//            mnuThrDecimal.setSelected(true);
//        }
        oGroup.add(mnuThrDecimal);
        mnuFouDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_FOUR_DECIMAL"));
        mnuDecimalPlaces.add(mnuFouDecimal);
        mnuFouDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.FOUR_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.FOUR_DECIMAL_PLACES){
//            mnuFouDecimal.setSelected(true);
//        }
        oGroup.add(mnuFouDecimal);
        mnuFiveDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_FIVE_DECIMAL"));
        mnuDecimalPlaces.add(mnuFiveDecimal);
        mnuFiveDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.FIVE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.FIVE_DECIMAL_PLACES){
//            mnuFiveDecimal.setSelected(true);
//        }
        oGroup.add(mnuFiveDecimal);
        mnuSixDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_SIX_DECIMAL"));
        mnuDecimalPlaces.add(mnuSixDecimal);
        mnuSixDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDecimalPlaces(Constants.SIX_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.SIX_DECIMAL_PLACES){
//            mnuSixDecimal.setSelected(true);
//        }
        oGroup.add(mnuSixDecimal);
        GUISettings.applyOrientation(mnuDecimalPlaces);
    }

    //default curency mode deci
    private void createDCDecimalMenu(JPopupMenu parent) {
        TWMenu mnuDCDecimalPlaces = new TWMenu(Language.getString("POPUP_DECIMAL_PLACES"), "decimalplaces.gif");
        parent.add(mnuDCDecimalPlaces);

        //transHistoryTable.getPopup().add(mnuDecimalPlaces);
        ButtonGroup oGroup = new ButtonGroup();
        mnuDCNoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_NO_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCNoDecimal);
        mnuDCNoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //todo  for DC tble
                setDCDecimalPlaces(Constants.NO_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.NO_DECIMAL_PLACES){
//            mnuNoDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCNoDecimal);
        mnuDCDefaultDecimal = new TWRadioButtonMenuItem(Language.getString("DEFAULT_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCDefaultDecimal);
        mnuDCDefaultDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //todo  for dc
                setDCDecimalPlaces(Constants.UNASSIGNED_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.NO_DECIMAL_PLACES){
//            mnuNoDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCDefaultDecimal);
        mnuDCOneDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_ONE_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCOneDecimal);
        mnuDCOneDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDCDecimalPlaces(Constants.ONE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.ONE_DECIMAL_PLACES){
//            mnuOneDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCOneDecimal);
        mnuDCTwoDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_TWO_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCTwoDecimal);
        mnuDCTwoDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDCDecimalPlaces(Constants.TWO_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.TWO_DECIMAL_PLACES){
//            mnuTwoDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCTwoDecimal);

        mnuDCThrDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_THREE_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCThrDecimal);
        mnuDCThrDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDCDecimalPlaces(Constants.THREE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.THREE_DECIMAL_PLACES){
//            mnuThrDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCThrDecimal);
        mnuDCFouDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_FOUR_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCFouDecimal);
        mnuDCFouDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDCDecimalPlaces(Constants.FOUR_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.FOUR_DECIMAL_PLACES){
//            mnuFouDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCFouDecimal);
        mnuDCFiveDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_FIVE_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCFiveDecimal);
        mnuDCFiveDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDCDecimalPlaces(Constants.FIVE_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.FIVE_DECIMAL_PLACES){
//            mnuFiveDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCFiveDecimal);
        mnuDCSixDecimal = new TWRadioButtonMenuItem(Language.getString("POPUP_SIX_DECIMAL"));
        mnuDCDecimalPlaces.add(mnuDCSixDecimal);
        mnuDCSixDecimal.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setDCDecimalPlaces(Constants.SIX_DECIMAL_PLACES);
            }
        });
//        if (decimalPlaces == Constants.SIX_DECIMAL_PLACES){
//            mnuSixDecimal.setSelected(true);
//        }
        oGroup.add(mnuDCSixDecimal);
        GUISettings.applyOrientation(mnuDCDecimalPlaces);
    }

     private void setDecimalPlaces(byte places) {
        decimalPlaces = places;
        ((SmartTable) tableSinglePf.getTable()).setDecimalPlaces(places);
        tableSinglePf.updateGUI();
        tableSinglePf.getTable().updateUI();
        settingSinglePf.putProperty(ViewConstants.VC_DECIMAL_PLACES, places);
        if (decimalPlaces == Constants.NO_DECIMAL_PLACES) {
            mnuNoDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.ONE_DECIMAL_PLACES) {
            mnuOneDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.UNASSIGNED_DECIMAL_PLACES) {
            mnuDefaultDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.TWO_DECIMAL_PLACES) {
            mnuTwoDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.THREE_DECIMAL_PLACES) {
            mnuThrDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.FOUR_DECIMAL_PLACES) {
            mnuFouDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.FIVE_DECIMAL_PLACES) {
            mnuFiveDecimal.setSelected(true);
        } else if (decimalPlaces == Constants.SIX_DECIMAL_PLACES) {
            mnuSixDecimal.setSelected(true);
        }
    }


    //for default currency table
    private void setDCDecimalPlaces(byte places) {
        dcDecimalPlaces = places;
        ((SmartTable) tableAllPf.getTable()).setDecimalPlaces(places);
        tableAllPf.updateGUI();
        tableAllPf.getTable().updateUI();
        //todo another viw setting
        settingAllPf.putProperty(ViewConstants.VC_DECIMAL_PLACES, places);
        if (dcDecimalPlaces == Constants.NO_DECIMAL_PLACES) {
            mnuDCNoDecimal.setSelected(true);

        } else if (dcDecimalPlaces == Constants.ONE_DECIMAL_PLACES) {
            mnuDCOneDecimal.setSelected(true);
        } else if (dcDecimalPlaces == Constants.UNASSIGNED_DECIMAL_PLACES) {
            //mnuDefaultDecimal.setSelected(true);
            mnuDCDefaultDecimal.setSelected(true);
        } else if (dcDecimalPlaces == Constants.TWO_DECIMAL_PLACES) {
            mnuDCTwoDecimal.setSelected(true);
        } else if (dcDecimalPlaces == Constants.THREE_DECIMAL_PLACES) {
            mnuDCThrDecimal.setSelected(true);
        } else if (dcDecimalPlaces == Constants.FOUR_DECIMAL_PLACES) {
            mnuDCFouDecimal.setSelected(true);
        } else if (dcDecimalPlaces == Constants.FIVE_DECIMAL_PLACES) {
            mnuDCFiveDecimal.setSelected(true);
        } else if (dcDecimalPlaces == Constants.SIX_DECIMAL_PLACES) {
            mnuDCSixDecimal.setSelected(true);
        }
    }

     public void workspaceLoaded() {
        try {
            decimalPlaces = Byte.parseByte(settingSinglePf.getProperty(ViewConstants.VC_DECIMAL_PLACES));
            dcDecimalPlaces = Byte.parseByte(settingAllPf.getProperty(ViewConstants.VC_DECIMAL_PLACES));
        } catch (Exception e) {
            decimalPlaces = Constants.TWO_DECIMAL_PLACES;
            dcDecimalPlaces = Constants.TWO_DECIMAL_PLACES;
        }
        setDecimalPlaces(decimalPlaces);
        setDCDecimalPlaces(dcDecimalPlaces);
    }
}
