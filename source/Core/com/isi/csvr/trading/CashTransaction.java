package com.isi.csvr.trading;

import com.isi.csvr.shared.Language;
import com.isi.csvr.trading.shared.TradeMeta;

import java.text.SimpleDateFormat;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Nov 4, 2005
 * Time: 2:42:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class CashTransaction {
    private String type;
    private long reference;
    private String narration;
    private float amount;
    private long date;
    private String all, open, close, buy, sell, deposite, withdraw, other;

    public CashTransaction(String type, float amount, long reference, long dateAndTime, String narration) {
        this.type = type;
        this.amount = amount;
        this.reference = reference;
        this.date = dateAndTime;
        this.narration = narration;
        setTypeNames();
    }

    public String getTypeID() {
        return type;
    }

    /*public void setType(String type){
            this.type = type;
        }
    */
    public float getAmount() {
        return amount;
    }

    /*public void setAmount(int amount) {
        this.amount = amount;
    }*/

    public long getReference() {
        return reference;
    }

    public long getDate() {
        return date;
    }

    public String getNarration() {
        return narration;
    }

    private void setTypeNames() {
        all = Language.getString("ALL");
        open = Language.getString("OPEN_BALANCE");
        deposite = Language.getString("DEPOSITES");
        sell = Language.getString("SELL_SETTLE");
        buy = Language.getString("BUY_SETTLE");
        withdraw = Language.getString("WITHDROWALS");
        other = Language.getString("CHARGS");
        close = Language.getString("CLOSING_BALANCE");
    }

    public String getTypeName(String type) {
        if (type.equals(TradeMeta.CT_ALL)) {
            return all;
//        } else if (type.equals(TradeMeta.CT_OPEN)) {
//            return open;
        } else if (type.equals(TradeMeta.CT_DEPOST)) {
            return deposite;
        } else if (type.equals(TradeMeta.CT_STLSEL)) {
            return sell;
        } else if (type.equals(TradeMeta.CT_STLBUY)) {
            return buy;
        } else if (type.equals(TradeMeta.CT_WITHDR)) {
            return withdraw;
//        } else if (type.equals(TradeMeta.CT_CHARGS)) {
//            return other;
//        } else if (type.equals(TradeMeta.CT_CLOSE)) {
//            return close;
        }
        /*switch(type){
            case ALL:
                return Language.getString("ALL");
            case 1:
                return Language.getString("OPEN_BALANCE");
            case DEPOST:
                return Language.getString("DEPOSITES");
            case 3:
                return Language.getString("WITHDROWALS");
            case 4:
                return Language.getString("BUY_SETTLE");
            case 5:
                return Language.getString("SELL_SETTLE");
            case 6:
                return Language.getString("CHARGES");
            case 7:
                return Language.getString("CLOSING_BALANCE");
        }*/
        else {
            return "";
        }
    }
}
