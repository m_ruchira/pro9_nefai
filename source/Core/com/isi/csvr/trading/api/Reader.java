package com.isi.csvr.trading.api;

import com.isi.csvr.shared.nanoxml.*;
import com.isi.csvr.shared.*;
import com.isi.csvr.trading.shared.*;
import com.isi.csvr.trading.datastore.*;
import com.isi.csvr.trading.*;
import com.isi.csvr.datastore.*;

import java.io.*;
import java.util.*;
import java.text.SimpleDateFormat;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Sep 4, 2007
 * Time: 9:16:36 AM
 */
public class Reader extends Thread {

    private InputStream in;
    boolean active = true;
    private SimpleDateFormat expiryDateFormat;

    public Reader(InputStream in) {
        this.in = in;
        start();
    }

    public void run() {
        System.out.println("========== Reader Active ===========");
        byte[] data = new byte[100];
        try {
            StringBuffer buff = new StringBuffer();
            while (active) {
                int len = in.read(data);
                if (len > 0) {
                    String str = new String(data, 0, len);
                    buff.append(str);
                    if (str.endsWith("\n")) {
                        System.out.println("complete string ==" + buff.toString());
                        analyseFrame(buff.toString());
                        buff = null;
                        buff = new StringBuffer();

                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
            closeClient();
        }
    }

    private void closeClient() {
        TradeServer.getInstance().closeClient();
    }

    private void errorMessage(int errorType, String... msg) {
        StringBuilder buffer = new StringBuilder();
        buffer.append("<Error>");
        //buffer.append("<Type value=\"").append("Error").append("\"/>");
        if (errorType == 1) {
            buffer.append("<ID value=\"").append("100").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Invalid Symbol").append("\"/>");
        } else if (errorType == 2) {
            buffer.append("<ID value=\"").append("100").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Invalid Portfolio Number").append("\"/>");
        } else if (errorType == 3) {
            buffer.append("<ID value=\"").append("100").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Invalid Symbol for Trading").append("\"/>");
        } else if (errorType == 4) {
            buffer.append("<ID value=\"").append("100").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Rule validation failed: ").append(msg[0]).append("\"/>");
        } else if (errorType == 5) {
            buffer.append("<ID value=\"").append("100").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Not a tradeable exchange").append("\"/>");
        } else if (errorType == 6) {
            buffer.append("<ID value=\"").append("100").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Daily Margin Limit exceeded. Cannot proceed with order.").append("\"/>");
        } else if (errorType == 7) {
            buffer.append("<ID value=\"").append("100").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Portfolio Margin Limit exceeded. Cannot proceed with order.").append("\"/>");
        } else if (errorType == 8) {
            buffer.append("<ID value=\"").append("100").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Invalid Price").append("\"/>");
        } else if (errorType == 9) {
            buffer.append("<ID value=\"").append("100").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Invalid Quantity").append("\"/>");
        } else if (errorType == 10) {
            buffer.append("<ID value=\"").append("10").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Invalid Order Type").append("\"/>");
        } else if (errorType == 11) {
            buffer.append("<ID value=\"").append("10").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Invalid Order Side").append("\"/>");
        } else if (errorType == 12) {
            buffer.append("<ID value=\"").append("10").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Invalid Order Expiry Date").append("\"/>");
        } else if (errorType == 13) {
            buffer.append("<ID value=\"").append("10").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Invalid Order Number").append("\"/>");
        } else if (errorType == 14) {
            buffer.append("<ID value=\"").append("10").append(errorType).append("\"/>");
            buffer.append("<Body value=\"").append("Original Order does not exist").append("\"/>");
        }
        buffer.append("</Error>\n");
        String error = buffer.toString();
        TradeServer.write(error);
    }

    private void analyseFrame(String data) {
        try {
            XMLElement root = new XMLElement();
            root.parseString(data);
            ArrayList<XMLElement> rootElements = root.getChildren();
            byte requestType = -1;
            byte orderSide = -1;
            double orderPrice = -1;
            int orderQty = -1;
            long orderExpiry = -1;
            byte orderTIF = TradeMeta.TIF_DAY;
            char orderType = ' ';
            String clOrderID = null;
            String tWOrderID = null;
            String symbol = null;
            String exchange = null;
            String portfolio = null;
            int instrument = Meta.INSTRUMENT_EQUITY;

            if (expiryDateFormat == null) {
                expiryDateFormat = new SimpleDateFormat("ddMMyyyy");
            }

            if (root.getName().equalsIgnoreCase("NEWORDER")) {
                requestType = TradeMeta.NEW;
            } else if (root.getName().equalsIgnoreCase("AMENDORDER")) {
                requestType = TradeMeta.AMEND;
            } else if (root.getName().equalsIgnoreCase("CANCELORDER")) {
                requestType = TradeMeta.CANCEL;
            }

            for (int i = 0; i < rootElements.size(); i++) {
                XMLElement rootItem = rootElements.get(i);
                //ArrayList<XMLElement> elements = rootItem.getChildren();
                String name = rootItem.getName();
                String value = rootItem.getAttribute("VALUE");
                if (name.equals("SYMBOL")) {
                    symbol = value;
                } else if (name.equals("EXCHANGE")) {
                    exchange = value;
                } else if (name.equalsIgnoreCase("PRICE")) {
                    try {
                        orderPrice = Double.parseDouble(value);
                    } catch (NumberFormatException e) {
                        orderPrice = 0;
                    }
                } else if (name.equalsIgnoreCase("QUANTITY")) {
                    try {
                        orderQty = Integer.parseInt(value);
                    } catch (NumberFormatException e) {
                        orderQty = 0;
                    }
                } else if (name.equalsIgnoreCase("INSTRUMENTTYPE")) {
                    try {
                        instrument = Integer.parseInt(value);
                    } catch (NumberFormatException e) {
                        instrument = Meta.INSTRUMENT_EQUITY;
                    }
                } else if (name.equalsIgnoreCase("EXPIRYDATE")) {
                    try {
                        orderExpiry = expiryDateFormat.parse(value).getTime();
                        orderTIF = TradeMeta.TIF_GTD;
                    } catch (Exception e) {
                        orderTIF = TradeMeta.TIF_DAY;
//                        orderExpiry = -1;
                    }
                } else if (name.equalsIgnoreCase("SIDE")) {
                    try {
                        if (value.equalsIgnoreCase("B")) {
                            orderSide = TradeMeta.BUY;
                        } else if (value.equalsIgnoreCase("S")) {
                            orderSide = TradeMeta.SELL;
                        }
                    } catch (NumberFormatException e) {
                        orderSide = -1;
                    }
                } else if (name.equalsIgnoreCase("TYPE")) {
                    try {
                        if (value.equalsIgnoreCase("M")) {
                            orderType = TradeMeta.ORDER_TYPE_MARKET;
                        } else if (value.equalsIgnoreCase("L")) {
                            orderType = TradeMeta.ORDER_TYPE_LIMIT;
                        }
                    } catch (Exception e) {
                        orderType = ' ';
                    }
                } else if (name.equalsIgnoreCase("PORTFOLIO")) {
                    portfolio = value;
                    /*} else if(name.equalsIgnoreCase("TIF")){
                    try {
                        orderTIF = Byte.parseByte(value);
                    } catch(NumberFormatException e) {
                        orderTIF = -99;
                    }*/
                } else if (name.equalsIgnoreCase("ORDERID")) {
                    clOrderID = value;
                } else if (name.equalsIgnoreCase("USERORDERID")) {
                    tWOrderID = value;
                }
                System.out.println("name and value ==" + name + " , " + value);
            }
            Stock stock = null;
            if (requestType == TradeMeta.NEW) {
                try {
                    stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
                    if (stock == null) {
                        errorMessage(1);
                        return;
                    }
                } catch (Exception e) {
                    errorMessage(1);
                    return;
                }

                if (portfolio == null || TradingShared.getTrader().getPortfolio(portfolio) == null) {
                    errorMessage(2);
                    return;
                }

                if (orderSide < 0) {
                    errorMessage(11);
                    return;
                }

                if (!TradingShared.getTrader().isTradableExchange(exchange)) {
                    errorMessage(5);
                    return;
                }
                if (!TradingShared.isShariaTestPassed(exchange, symbol, instrument)) {
                    errorMessage(3);
                    return;
                }
            }

            if ((requestType == TradeMeta.CANCEL) || (requestType == TradeMeta.AMEND)) { // check if the original order exist
                Transaction oldTransaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(clOrderID);
                if (oldTransaction == null){
                    errorMessage(14);
                    return;
                } else {
                    exchange = oldTransaction.getExchange();
                    symbol = oldTransaction.getSymbol();
                    stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
                    oldTransaction = null;
                }
            }

            if ((requestType == TradeMeta.NEW) || (requestType == TradeMeta.AMEND)) {
                if (orderType == ' ') {
                    errorMessage(10);
                    return;
                }
                if ((orderType == TradeMeta.ORDER_TYPE_LIMIT) && (orderPrice <= 0)) {
                    errorMessage(8);
                    return;
                }
                if (orderQty <= 0) {
                    errorMessage(9);
                    return;
                }


                /*if (orderExpiry < 0) {
                    errorMessage(12);
                    return;
                }*/


                /*if (orderTIF == -99) {
                    errorMessage(12);
                    return;
                }*/

                StringBuilder errorMessage = new StringBuilder();
                if (!TradeMethods.ruleBasedValidationSucess(false, exchange, orderType, orderPrice, orderQty, 0, 0,
                        stock.getMinPrice(), stock.getMaxPrice(), orderTIF, orderSide, symbol, 0, stock,false,  errorMessage)) {
                    errorMessage(4, errorMessage.toString());
                    return;
                }
                if (requestType == TradeMeta.NEW) {
                    if (TradeMethods.isMarginTradingEnabled(portfolio)) {
//                        if (orderTIF == TradeMeta.TIF_DAY) {
//                        if((TradeMethods.isDayMarginTradingEnabled(portfolio) && )) {
//                            if (TradeMethods.hasDayMarginThresholdExceeded(exchange, symbol, instrument, portfolio, orderQty, orderPrice)) {//high priority
//                                errorMessage(6);
//                                return;
//                            }
////                        } else if(TradeMethods.isDayMarketType(orderType)) {
////                            return;
//                        } else {
                        if (TradeMethods.hasMarginThresholdExceeded(exchange, symbol, instrument, portfolio, orderQty, orderPrice)) {//high priority
                            errorMessage(7);
                            return;
                        }
//                        }
                    }
                }
            }
            if (requestType == TradeMeta.AMEND || requestType == TradeMeta.CANCEL) {
                if (clOrderID == null) {
                    errorMessage(13);
                    return;
                }
            }

            if (requestType == TradeMeta.AMEND) {
                amendOrder(orderPrice, orderQty, orderType, clOrderID, orderTIF, orderExpiry, tWOrderID);
            } else if (requestType == TradeMeta.CANCEL) {
                cancelOrder(clOrderID, tWOrderID);
            } else if (requestType == TradeMeta.NEW) {
                newOrder(symbol, exchange, instrument, orderPrice, orderQty, orderSide, orderType, orderTIF, portfolio, orderExpiry, tWOrderID);
            }

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean amendOrder(double price, int qty, char type,
                               String clOrderID, short orderTIF, long orderExpiry, String tWOrderID) {
//        TradeMessage tradeMessage2;
        try {
            Transaction oldTransaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(clOrderID);
            TradeMethods.amendOrder(oldTransaction, false, oldTransaction.getSide(), type, price, oldTransaction.getCurrency(), qty, orderTIF, orderExpiry, "", "", -1, null, null, -1, 0, 0, TradeMeta.CONDITION_TYPE_NONE, 0, false, 0, false, tWOrderID,null, null, 0);

            return true;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return false;
    }

    private boolean cancelOrder(String clOrderID, String tWOrderID) {
        try {
            Transaction oldTransaction = OrderStore.getSharedInstance().searhTransactionByClOrderID(clOrderID);
            oldTransaction.setTwOrderID(tWOrderID);
            TradeMethods.cancelOrder(oldTransaction);
            return true;
        } catch (Exception e) {
            e.printStackTrace();  
        }
        return false;
    }

    private boolean newOrder(String symbol, String exchange, int instrument, double price, int qty, byte side, char type, byte tif,
                             String portfolio, long orderExpiry, String tWOrderID) {

        String currency = null;
        Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
        TradeMethods.getSharedInstance().sendNewOrderForExec(false, false, 0, portfolio, exchange, symbol, side, type, price, stock.getMaxPrice(), currency, qty, tif, orderExpiry,
                0, 0, true, stock.getStrikePrice(), TradingShared.formatOpraExpireDate(stock.getExpirationDate()), stock.getInstrumentType(),
                0, stock.getOptionBaseSymbol(), -1, null, null, null, -1, false, -1, 0, -1, -1, false, 0, false, tWOrderID,null,null,0, null, 0);
        return false;
    }

}
