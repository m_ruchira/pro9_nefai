package com.isi.csvr.iframe;

// Copyright (c) 2000 Home

import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;

import java.util.Enumeration;
import java.util.Hashtable;

public class WindowDaemon extends Thread {
    Hashtable g_oTimeNSalesTable;
    Hashtable g_oMarketDepthTable;
    Hashtable g_oDQTable;
    Hashtable g_oSnapTable;
    Hashtable g_oOrderDepthTable;
    Hashtable g_oDepthByPrice;
    Hashtable g_oDepthByQuantity;
    Hashtable g_oDepthCalculators;
    Hashtable g_oRegionalQuotes;
    Hashtable g_oFundamentalData;
    Hashtable g_oOptionChain;
    Hashtable g_oFullQuotes;
    ViewSettingsManager g_oViewSettingsManager;

    /**
     * Constructor
     */
    public WindowDaemon(ViewSettingsManager oViewSettingsManager)
    //Hashtable oTimeNSalesTable, Hashtable oMarketDepthTable)
    {
        super("WindowDaemon");
        g_oViewSettingsManager = oViewSettingsManager;
        g_oTimeNSalesTable = oViewSettingsManager.getTimenSalesViews();
        g_oMarketDepthTable = oViewSettingsManager.getDepthViews();
        g_oDQTable = oViewSettingsManager.getDTQViews();
        g_oSnapTable = oViewSettingsManager.getSnapViews();
        g_oOrderDepthTable = oViewSettingsManager.getOrderDepthViews();
        g_oDepthCalculators = oViewSettingsManager.getDepthCalViews();
        g_oRegionalQuotes = oViewSettingsManager.getRQuotesViews();
        g_oFundamentalData = oViewSettingsManager.getFD2Views();
        g_oOptionChain = oViewSettingsManager.getOptionChainViews();
        g_oFullQuotes = oViewSettingsManager.getFullQuoteViews();
        g_oDepthByPrice = new Hashtable();
        g_oDepthByQuantity = new Hashtable();
        this.start();
    }

    public void run() {
        while (true) {
            try {
                Enumeration oTNS = g_oTimeNSalesTable.keys();
                while (oTNS.hasMoreElements()) {
                    String key = (String) oTNS.nextElement();
                    ViewSetting oFrame = (ViewSetting) g_oTimeNSalesTable.get(key);

//                    ViewSetting oFrame = (ViewSetting) g_oTimeNSalesTable.get((String) oTNS.nextElement());
                    if (oFrame.getParent() == null) continue;
                    if (!((InternalFrame) oFrame.getParent()).isActive()) {
//                        System.out.println("Deleting....");
                        oFrame.setParent(null);
                        g_oTimeNSalesTable.remove(key);
                    }
                }

                Enumeration oDQT = g_oDQTable.keys();
                while (oDQT.hasMoreElements()) {
                    //System.out.println("++");
                    String key = (String) oDQT.nextElement();
                    ViewSetting oFrame = (ViewSetting) g_oDQTable.get(key);
                    if (oFrame.getParent() == null) continue;
                    if (!((InternalFrame) oFrame.getParent()).isActive()) {
                        oFrame.setParent(null);
                        g_oDQTable.remove(key);
                    }
                    key = null;
                }

                Enumeration oSnap = g_oSnapTable.keys();
                while (oSnap.hasMoreElements()) {
                    //System.out.println("++");
                    String key = (String) oSnap.nextElement();
                    ViewSetting oFrame = (ViewSetting) g_oSnapTable.get(key);
                    if (oFrame.getParent() == null) continue;
                    if (!((InternalFrame) oFrame.getParent()).isActive()) {
                        oFrame.setParent(null);
                        g_oSnapTable.remove(key);
                    }
                    key = null;
                }
                oSnap = null;

                Enumeration oOrdDepth = g_oOrderDepthTable.keys();
                while (oOrdDepth.hasMoreElements()) {
                    String key = (String) oOrdDepth.nextElement();
                    ViewSetting oFrame = (ViewSetting) g_oOrderDepthTable.get(key);
                    if (oFrame.getParent() == null) continue;
                    if (!((InternalFrame) oFrame.getParent()).isActive()) {
                        oFrame.setParent(null);
                        g_oOrderDepthTable.remove(key);
                    }
                    key = null;
                }
                oOrdDepth = null;

                Enumeration oDepth = g_oMarketDepthTable.keys();
                while (oDepth.hasMoreElements()) {
                    //System.out.println("Deleting....");
                    String key = (String) oDepth.nextElement();
                    ViewSetting oSetting = (ViewSetting) g_oMarketDepthTable.get(key);
                    if (oSetting.getParent() == null) continue;
                    InternalFrame frame = (InternalFrame) oSetting.getParent();
                    if (!(frame).isActive()) {
//                        System.out.println("Deleting....");
                        oSetting.setParent(null);
                        frame = null;
                        g_oMarketDepthTable.remove(key);
                    }
                }

                Enumeration oCalc = g_oDepthCalculators.keys();
                while (oCalc.hasMoreElements()) {
                    String key = (String) oCalc.nextElement();
                    ViewSetting setting = (ViewSetting) g_oDepthCalculators.get(key);
                    InternalFrame frame = (InternalFrame) setting.getParent();
                    if (setting.getParent() == null) continue;
                    if (!(frame).isActive()) {
                        setting.setParent(null);
                        frame = null;
                        g_oDepthCalculators.remove(key);
                    }
                }

                Enumeration oRQuotes = g_oRegionalQuotes.keys();
                while (oRQuotes.hasMoreElements()) {
                    String key = (String) oRQuotes.nextElement();
                    ViewSetting setting = (ViewSetting) g_oRegionalQuotes.get(key);
                    InternalFrame frame = (InternalFrame) setting.getParent();
                    if (setting.getParent() == null) continue;
                    if (!(frame).isActive()) {
                        setting.setParent(null);
                        frame = null;
                        g_oRegionalQuotes.remove(key);
                    }
                }

                Enumeration oFDs = g_oFundamentalData.keys();
                while (oFDs.hasMoreElements()) {
                    String key = (String) oFDs.nextElement();
                    ViewSetting setting = (ViewSetting) g_oFundamentalData.get(key);
                    InternalFrame frame = (InternalFrame) setting.getParent();
                    if (setting.getParent() == null) continue;
                    if (!(frame).isActive()) {
                        setting.setParent(null);
                        frame = null;
                        g_oFundamentalData.remove(key);
                    }
                }

                Enumeration oOChains = g_oOptionChain.keys();
                while (oOChains.hasMoreElements()) {
                    String key = (String) oOChains.nextElement();
                    ViewSetting setting = (ViewSetting) g_oOptionChain.get(key);
                    InternalFrame frame = (InternalFrame) setting.getParent();
                    if (setting.getParent() == null) continue;
                    if (!(frame).isActive()) {
                        setting.setParent(null);
                        frame = null;
                        g_oOptionChain.remove(key);
                    }

                }

                Enumeration oFullQ = g_oFullQuotes.keys();
                while (oFullQ.hasMoreElements()) {
                    String key = (String) oFullQ.nextElement();
                    ViewSetting setting = (ViewSetting) g_oFullQuotes.get(key);
                    InternalFrame frame = (InternalFrame) setting.getParent();
                    if (setting.getSubType() == ViewSettingsManager.FULL_QUOTE_VIEW) {
                        if (setting.getParent() == null) continue;
                        if (!(frame).isActive()) {
                            setting.setParent(null);
                            frame = null;
                            g_oFullQuotes.remove(key);
                        }
                    }
                }

                Hashtable oWindowList = g_oViewSettingsManager.getWindowList();
                Enumeration enuWindowList = oWindowList.keys();
                while (enuWindowList.hasMoreElements()) {
                    String sKey = (String) enuWindowList.nextElement();
                    InternalFrame oFrame = (InternalFrame) oWindowList.get(sKey);

                    if (oFrame != null) {
                        //System.out.println("window found");
                        if (!oFrame.isActive()) {
                            //System.out.println("not active");
                            oFrame = null;
                            Object frame = oWindowList.remove(sKey);
                            frame = null;
                        }
                    }
                    sKey = null;
                }
            }
            catch (Exception e) {
                e.printStackTrace();
            }
            synchronized (this) {
                try {
                    this.wait();
                }
                catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }
    }

    public synchronized void notifyThread() {
        try {
            this.notify();
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    /*public void addDepthByPrice(StaticDepthStore store,String symbol){
        g_oDepthByPrice.put(symbol,store);
    }

    public void addDepthByQuantity(StaticDepthStore store,String symbol){
        g_oDepthByQuantity.put(symbol,store);
    }*/
}