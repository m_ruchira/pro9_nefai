package com.isi.csvr.globalMarket.forex;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 6, 2008
 * Time: 3:49:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class ForexRecord {


    private String exchange;
    private String symbol;
    private String description;
    private double usdRate=0;
    private double gbpRate=0;
    private double eurRate=0;
    private double yenRate=0;
    private double chfRate=0;
    private double audRate=0;
    private double baseRate=1;


    public ForexRecord() {
    }

    public ForexRecord(String exchange, String symbol, String description) {
        this.exchange = exchange;
        this.symbol = symbol;
        this.description = description;
    }

    public String getExchange() {
        return exchange;
    }

    public void setExchange(String exchange) {
        this.exchange = exchange;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public double getUsdRate() {
        return usdRate;
    }

    public void setUsdRate(double usdRate) {
        this.usdRate = usdRate;
    }

    public double getGbpRate() {
        return gbpRate;
    }

    public void setGbpRate(double gbpRate) {
        this.gbpRate = gbpRate;
    }

    public double getEurRate() {
        return eurRate;
    }

    public void setEurRate(double eurRate) {
        this.eurRate = eurRate;
    }

    public double getYenRate() {
        return yenRate;
    }

    public void setYenRate(double yenRate) {
        this.yenRate = yenRate;
    }

    public double getAudRate() {
        return audRate;
    }

    public void setAudRate(double audRate) {
        this.audRate = audRate;
    }

    public double getChfRate() {
        return chfRate;
    }

    public void setChfRate(double chfRate) {
        this.chfRate = chfRate;
    }

    public double getBaseRate() {
        return baseRate;
    }

    public void setBaseRate(double baseRate) {
        this.baseRate = baseRate;
    }
}
