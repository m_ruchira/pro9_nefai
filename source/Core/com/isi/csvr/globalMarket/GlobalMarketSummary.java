package com.isi.csvr.globalMarket;

import com.isi.csvr.globalMarket.equity.EquityModel;
import com.isi.csvr.globalMarket.commodities.CommoditiesModel;
import com.isi.csvr.globalMarket.forex.ForexModel;
import com.isi.csvr.globalMarket.forex.ForexRecord;
import com.isi.csvr.globalMarket.forex.CombinedForexRecord;
import com.isi.csvr.globalMarket.money.MoneyMarketModel;
import com.isi.csvr.properties.ViewSetting;

import javax.swing.*;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableColumnModel;
import java.util.Hashtable;
import java.util.Enumeration;
import java.util.ArrayList;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;

/**
 * Created by IntelliJ IDEA. User: Udaya Athukorala Date: Jun 6, 2008 Time: 6:20:24 PM To change this template use File
 * | Settings | File Templates.
 */
public class GlobalMarketSummary implements MouseListener {

    private GlobalMarketSummaryWindow summaryWindow;
    private Hashtable equityMarketstore;
    private Hashtable comoditiesstore;
    private Hashtable forexstore;
    private Hashtable moneyMaketstore;


    public GlobalMarketSummary() {
        GlobalMarketSummaryWindow.getSharedInstance();
        MarketSummaryLoader.getSharedInstance();
        initStores();
        initModels();
        processEqiutyStore();
        processComoditiesStore();
        processForexStores();
        GlobalMarketSummaryWindow.getSharedInstance().setSummaryRef(this);
        setShowableInwindow();



    }


    public void initStores() {

        try {
            equityMarketstore = MarketSummaryLoader.getSharedInstance().getEquityMarketStore();
            comoditiesstore = MarketSummaryLoader.getSharedInstance().getComoditiesStore();
            forexstore = MarketSummaryLoader.getSharedInstance().getForexStore();
            moneyMaketstore = MarketSummaryLoader.getSharedInstance().getMoneyMarketStore();
     //       GlobalMarketSummaryWindow.getSharedInstance().setModels(new EquityModel(equityMarketstore), new CommoditiesModel(comoditiesstore), new ForexModel(forexstore), new MoneyMarketModel(moneyMaketstore));
      //      GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getTable().getTableHeader().addMouseListener(this);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    public void initModels(){
        EquityModel eqmodel = new EquityModel(equityMarketstore);
        ViewSetting eqsetting =MarketSummaryLoader.getSharedInstance().getEquityViewSetting();
        CommoditiesModel  comoditiesmodel = new CommoditiesModel(comoditiesstore);
        ViewSetting commoditiessetting = MarketSummaryLoader.getSharedInstance().getCommoditiesViewSetting();
        ForexModel forexmodel = new ForexModel(forexstore);
        MoneyMarketModel moneymodel = new MoneyMarketModel(moneyMaketstore);

        GlobalMarketSummaryWindow.getSharedInstance().setViewsettings(eqsetting,commoditiessetting);
        GlobalMarketSummaryWindow.getSharedInstance().setModels(eqmodel,comoditiesmodel,forexmodel,moneymodel);
        GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getTable().getTableHeader().addMouseListener(this);
        GlobalMarketSummaryWindow.getSharedInstance().applyWidths();
        

    }

    private void setShowableInwindow(){
        boolean eq = !(equityMarketstore==null|| equityMarketstore.isEmpty());
        boolean como = !(comoditiesstore==null|| comoditiesstore.isEmpty());
        boolean forex = !(forexstore==null|| forexstore.isEmpty());
        GlobalMarketSummaryWindow.getSharedInstance().setShowableItems(eq,como,forex);

    }




    private void processEqiutyStore() {

        int size = equityMarketstore.size();


        if (size > 0) {

            Enumeration en = equityMarketstore.keys();
            while (en.hasMoreElements()) {

                final String name = (String) en.nextElement();
                TabToolbarButton btn = new TabToolbarButton(name);
                //   btn.setPreferredSize(new Dimension(20,20));
                btn.setActionCommand(name);
                 btn.setBorder(BorderFactory.createEmptyBorder(5,5,2,5));
               //  btn.setBorder(BorderFactory.createLineBorder(Color.BLACK));
                btn.setName(name);
                btn.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (e.getActionCommand().equals(name)) {
                            (GlobalMarketSummaryWindow.getSharedInstance().getEquityMarketTableItem().getOModel()).changeTab(name); //etEquityTable((ArrayList)equityMarketstore.get(name));
                            GlobalMarketSummaryWindow.getSharedInstance().getEquityMarketTableItem().getDataTable().updateUI();
                            //   GlobalMarketSummaryWindow.getSharedInstance().getEquityMarketTableItem().updateUI();
                            // equityMarketstore.get(name);
                        }
                    }
                });
                GlobalMarketSummaryWindow.getSharedInstance().getEquityMarketTableItem().AddTab(btn);
            }
        } else {


        }

    }


    private void processComoditiesStore() {

        int size = comoditiesstore.size();


        if (size > 0) {

            Enumeration en = comoditiesstore.keys();
            while (en.hasMoreElements()) {

                final String name = (String) en.nextElement();
                TabToolbarButton btn = new TabToolbarButton(name);
                //   btn.setPreferredSize(new Dimension(20,20));
                btn.setActionCommand(name);
                btn.setBorder(BorderFactory.createEmptyBorder(5, 5, 2, 5));
                btn.setName(name);
                btn.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (e.getActionCommand().equals(name)) {
                            GlobalMarketSummaryWindow.getSharedInstance().getComoditiesTableItem().getOModel().changeTab(name);
                            GlobalMarketSummaryWindow.getSharedInstance().getComoditiesTableItem().getDataTable().updateGUI();
                            //  GlobalMarketSummaryWindow.getSharedInstance().getComoditiesTableItem().updateUI();
                        }
                    }
                });
                GlobalMarketSummaryWindow.getSharedInstance().getComoditiesTableItem().AddTab(btn);
            }
        } else {


        }

    }

    private void processMoneyMarketStore() {


    }

    public void processForexStores() {
        Hashtable forexsymbols = MarketSummaryLoader.getSharedInstance().getForexStore();
        if(!forexsymbols.isEmpty()) {
        try {
            Enumeration en = forexsymbols.elements();
            ArrayList symbollist = (ArrayList) en.nextElement();
            String cols[] = new String[symbollist.size()];
            int[][] colSetings = new int[cols.length][3];
            ForexRecord rec;
            for (int i = 0; i < symbollist.size(); i++) {

                rec = (ForexRecord) symbollist.get(i);
                String colname = rec.getSymbol();
                cols[i] = colname;
                colSetings[i][0] = i;
                colSetings[i][1] = 0;
                colSetings[i][2] = i;
//                CombinedForexRecord.getSharedInstance().getCombinedStore().put(i,0);

            }
            ViewSetting oSeting = GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getModel().getViewSettings();

            GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getModel().getViewSettings().setColumnHeadings(cols);
            ;
            GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getModel().getViewSettings().setColumnSettings(colSetings);
            //    GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getModel().fireTableStructureChanged();    //seting.setColumnHeadings(cols);
            GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getTable().createDefaultColumnsFromModel();
            PlainHeaderRenderer renderer = new PlainHeaderRenderer(true,true);
            //renderer.setShowToolTip();
            TableColumnModel colmodel = GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getTable().getColumnModel();
            int n = oSeting.getColumnHeadings().length;
            for (int i = 0; i < n; i++) {
                colmodel.getColumn(i).setHeaderRenderer(renderer);
            }


            GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().updateGUI();
            GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().updateUI();
            GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getTable().getTableHeader().repaint();
            changeColHeadings(0);
            GlobalMarketSummaryWindow.getSharedInstance().setSelectedTabInViewSetting(Constants.FOREX_TYPE,0);
            CombinedForexRecord.getSharedInstance().setForexExchange(MarketSummaryLoader.getSharedInstance().getForexExchange());
            CombinedForexRecord.getSharedInstance().setForexDecimals(MarketSummaryLoader.getSharedInstance().getForexDecimals());
            CombinedForexRecord.getSharedInstance().sendForexAddRequests(0);

        } catch (Exception ex) {


            ex.printStackTrace();

        }
        }

    }


    public void mouseClicked(MouseEvent e) {
        int index;
        if (e.getSource() instanceof JTableHeader) {
            JTable tbl = GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getTable();
            TableColumnModel columnModel = GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getTable().getColumnModel();
            java.awt.Point p = e.getPoint();
            index = columnModel.getColumnIndexAtX(p.x);
            System.out.println("column ++++++=++++++====+++++  " + index);
            int column = tbl.convertColumnIndexToModel(index);
            System.out.println("column ++++++=++++++====+++++2  " + column);
            changeColHeadings(column);
            GlobalMarketSummaryWindow.getSharedInstance().setSelectedTabInViewSetting(Constants.FOREX_TYPE,column);
            CombinedForexRecord.getSharedInstance().sendForexAddRequests(column);
        }


    }

    public void loadSelectedColFromViewSeting(int column){

        try{
            changeColHeadings(column);
            CombinedForexRecord.getSharedInstance().sendForexAddRequests(column);
        } catch (Exception e){
            e.printStackTrace();
        }



    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }


    public void changeColHeadings(int realcolindex) {

        String[] cols = new String[0];
        String[] newCols = new String[0];
        ViewSetting oSeting = GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getModel().getViewSettings();
        cols = oSeting.getColumnHeadings();
        newCols = new String[cols.length];
        cols = removeHTMLTags(cols);
        for (int i = 0; i < cols.length; i++) {

            if (i != realcolindex) {

                newCols[i] = "<html><u>" + cols[i] + "</u></html>";

            } else {
                newCols[i] = cols[i];
            }


        }

        oSeting.setColumnHeadings(newCols);
        GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getTable().createDefaultColumnsFromModel();
        PlainHeaderRenderer renderer = new PlainHeaderRenderer(true,true);
        //renderer.setShowToolTip();
        TableColumnModel colmodel = GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getTable().getColumnModel();
        int n = oSeting.getColumnHeadings().length;
        for (int i = 0; i < n; i++) {
            colmodel.getColumn(i).setHeaderRenderer(renderer);
        }


        GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().updateGUI();
        GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().updateUI();
        GlobalMarketSummaryWindow.getSharedInstance().getForextTableItem().getDataTable().getTable().getTableHeader().repaint();


    }


    public String[] removeHTMLTags(String cols[]) {

        for (int i = 0; i < cols.length; i++) {

            String header = cols[i];
            String realH = "";

            if (header.startsWith("<")) {

                realH = header.substring(9);
                realH = realH.substring(0, realH.indexOf("<"));
                cols[i] = realH;
            }


        }


        return cols;

    }
}
