package com.isi.csvr.globalMarket;

import com.isi.csvr.theme.Themeable;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.SortButtonRenderer;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Jun 12, 2008
 * Time: 4:56:26 PM
 * To change this template use File | Settings | File Templates.
 */
public class PlainHeaderRenderer extends JLabel implements TableCellRenderer, Themeable{
    private int selectedColumn;
    private Color darkColor = Color.red.darker();
    private Color lightColor = Color.red.brighter();
    private Color g_oBG1;
    private boolean isForex;
    private boolean isLTR;


    public PlainHeaderRenderer(boolean isForex, boolean isLTR) {

        setOpaque(false);
        this.isForex = isForex;
        this.isLTR = isLTR;
        Theme.registerComponent(this);
        applyTheme();



    }

    public Component getTableCellRendererComponent(JTable table, Object value, boolean isSelected, boolean hasFocus, int row, int column) {
        SortButtonRenderer headerbtn = new SortButtonRenderer(false);
        if (table != null) {
            this.selectedColumn = table.convertColumnIndexToModel(column);
        }
        if(isForex){
            this.setHorizontalAlignment(SwingConstants.CENTER);
            headerbtn.setHorizontalAlignment(SwingConstants.CENTER);
        }else if(selectedColumn ==0 && value!= null && ((String)value).equalsIgnoreCase("description") ){

            if(isLTR){
            this.setHorizontalAlignment(SwingConstants.LEFT);
            headerbtn.setHorizontalAlignment(SwingConstants.LEFT);
            }else{
             this.setHorizontalAlignment(SwingConstants.RIGHT);
             headerbtn.setHorizontalAlignment(SwingConstants.RIGHT);
            }
        }else{
            if(isLTR){
                this.setHorizontalAlignment(SwingConstants.CENTER);
                headerbtn.setHorizontalAlignment(SwingConstants.CENTER);
            }else{
                this.setHorizontalAlignment(SwingConstants.CENTER);
                headerbtn.setHorizontalAlignment(SwingConstants.CENTER);
            }
        }
        headerbtn.setText((value == null) ? "" : value.toString());
        this.setText((value == null) ? "" : value.toString());
        return this;
      //  return headerbtn;

    }

    public void applyTheme() {

//        this.setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));
//        Color color =  Theme.getOptionalColor("BOARD_TABLE_HEAD_BGCOLOR1");
//        if (color != null){
//            lightColor = color;
//        }
//        color =  Theme.getOptionalColor("BOARD_TABLE_HEAD_BGCOLOR2");
//        if (color != null){
//            darkColor = color;
//        }

        //To change body of implemented methods use File | Settings | File Templates.
        g_oBG1 = Theme.getColor("GM_TABLE_CELL_BGCOLOR2");
    }


     public void paint(Graphics g) {
       if(isForex){
       //paintGradient(g, getWidth(), getHeight());
       }else{
           paintPlain(g, getWidth(), getHeight());
       }
       super.paint(g);
    }

    private void paintGradient(Graphics g, int width, int height) {

        int halfheight = height / 2;
        for (int i = 0; i <= halfheight; i++) {
            g.setColor(new Color((((lightColor.getRed() * (halfheight - i)) + darkColor.getRed() * i) / halfheight),
                    (((lightColor.getGreen() * (halfheight - i)) + darkColor.getGreen() * i) / halfheight),
                    (((lightColor.getBlue() * (halfheight - i)) + darkColor.getBlue() * i) / halfheight)));
            g.drawLine(0, i, width, i);
        }
        for (int i = 0; i <= halfheight; i++) {
            g.setColor(new Color((((lightColor.getRed() * i) + darkColor.getRed() * (halfheight - i)) / halfheight),
                    (((lightColor.getGreen() * i) + darkColor.getGreen() * (halfheight - i)) / halfheight),
                    (((lightColor.getBlue() * i) + darkColor.getBlue() * (halfheight - i)) / halfheight)));
            g.drawLine(0, i + halfheight, width, i + halfheight);
        }
    }

    public void paintPlain(Graphics g, int width, int height){

         g.setColor(g_oBG1);
        g.fillRect(0,0,width,height);

    }
}
