package com.isi.csvr.downloader;

import com.isi.csvr.downloader.ui.DownloadProgressManager;
import com.isi.csvr.downloader.ui.DownloadProgressMeter;
import com.isi.csvr.event.ProgressListener;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWSocket;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.datastore.*;
import com.isi.util.TypeConvert;

import java.io.BufferedInputStream;
import java.io.File;
import java.io.FileOutputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.ArrayList;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Dec 12, 2003
 * Time: 9:02:25 AM
 * To change this template use Options | File Templates.
 */
public class ZipFileDownloader {
    private String url;
    private String exchange;
    private String id;
    private BufferedInputStream in;
    private OutputStream out;
    private ProgressListener progressListener;
    private ArrayList<ProgressListener> progressListeners = null;
    private int fileIndex = 1;
    private int path;
    private String ip = null;

    private static final String DOWNLOAD_LOCK = "DOWNLOAD_LOCK";

    public ZipFileDownloader(String url, String exchange, String id) {
        this(url, exchange, id, Constants.CONTENT_PATH_PRIMARY,null);
    }

    public ZipFileDownloader(String url, String exchange, String id, int path, String contentIP) {
        this.url = url;
        this.id = id;
        this.ip = contentIP;
        this.exchange = exchange;
        this.path = path;
        this.progressListeners = new ArrayList<ProgressListener>();
    }

    public ZipFileDownloader(String url, String exchange, String id, int path) {
        this(url, exchange, id, path,null);
    }

    public void addProgressListener(ProgressListener progressListener) {
        progressListeners.add(progressListener);
    }

    public File[] downloadFiles() throws Exception {
        Vector files;
        Socket socket ;

        String contentCMIP;
        if(ip == null) {
            if(path == Constants.CONTENT_PATH_SECONDARY) {
                contentCMIP = Settings.getGlobalContentProviderIP();
            } else if(path == Constants.CONTENT_PATH_TERTIARY) {
                contentCMIP = Settings.getRegionalContentProviderIP();
            } else  {
                contentCMIP = Settings.getActiveIP();
            }
            String exchangeContentIP = null;
            try {
                if(exchange != null && !exchange.equals(Constants.APPLICATION_EXCHANGE) ){
                    exchangeContentIP = ExchangeStore.getSharedInstance().getExchange(exchange).getContentCMIP();
                }
            } catch(Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            if(exchangeContentIP != null){
                contentCMIP = exchangeContentIP;
            }
        } else {
            contentCMIP = ip;
        }
        if(contentCMIP != null){
            socket = TWSocket.createSocket(contentCMIP, Settings.CONTENT_SERVER_PORT);
        }else {
            socket = TWSocket.createSocket(Settings.getActiveIP(), Settings.DOWNLOAD_PORT);
        }
        socket.setSoTimeout(60000);
        out = socket.getOutputStream();
        in = new BufferedInputStream(socket.getInputStream());


        out.write((Meta.USER_SESSION + Meta.DS + Settings.getSessionID() +
                Meta.FS + Meta.USER_TYPE + Meta.DS + Meta.USER_TYPE_NEW_TW +
                Meta.FS + Meta.USERID + Meta.DS + Settings.getUserID() +Meta.EOL).getBytes());

//        SharedMethods.printLine(url);

        // ignore the authentication reply
        int value = -1;
        value = in.read();
        while (value != '\n') {
            value = in.read();
            if (value == -1) {
                throw new Exception("Error while downloading file " + url);
            }
        }

        out.write((url).getBytes());

        files = new Vector();
        File file;
        boolean noMoreData = false;

        // read the first in to get the size of the file to be read
        while (!noMoreData) {

            synchronized (DOWNLOAD_LOCK) {
                file = new File(Settings.getAbsolutepath()+"temp/downloads/" + exchange + "-" + id + "-" + System.currentTimeMillis() + ".tmp");
//                System.out.println("File Name : "+file.getName());
            }
            files.addElement(file);
            FileOutputStream fout = new FileOutputStream(file);

            noMoreData = readZip(fout);
            Thread.sleep(100); // to avoid  file downloading issue
            fileIndex++;

            fout.close();
            fout = null;
            file = null;
        }

        in = null;
        socket.close();
        socket = null;

        // prepare the files areray to return
        File[] fileArray = new File[files.size()];
        for (int i = 0; i < fileArray.length; i++) {
            fileArray[i] = (File) files.elementAt(i);
        }
        files = null;

        return fileArray;
    }

    public boolean readZip(FileOutputStream fout) throws Exception {
        DownloadProgressMeter progressMeter = null;
        try {

            byte[] length = new byte[4];

            fireProgressStarted();

            byte type = (byte) in.read(); // file type ;

            for (int i = 0; i < length.length; i++) {
                length[i] = (byte) in.read();
            }

            int size = TypeConvert.byteArrayToInt(length, 0, 4);
            if (size <= 0) {
                fireProgressCompleted();
                return true; // no more data
            }

            fireSetMax(size);

            byte[] btempData = new byte[1000];
            int readcount = 0;
            int iValue;

            progressMeter = DownloadProgressManager.createMeter(exchange, id + " " + fileIndex);
            progressMeter.setMax(id, size);
          
            while (true) {

                if ((size - readcount) >= 1000) {
                    iValue = in.read(btempData);
                    if (iValue == -1) {
                        progressMeter.setProcessCompleted(id, false);
                        System.out.println("Zip File Downloader Disconnected");
                        throw new Exception("IO Stream disconnected.");
                    }
                    fout.write(btempData, 0, iValue);
                    readcount = readcount + iValue;
                    fireProgress((readcount * 100) / size);
                    progressMeter.setProgress(id, (readcount * 100) / size);
                    if (readcount == size)
                        break;
                } else {
                    if (readcount == size)
                        break;
                    btempData = new byte[size - readcount];
                    iValue = in.read(btempData);
                    if (iValue == -1) {
                        progressMeter.setProcessCompleted(id, false);
                        System.out.println("Zip File Downloader Disconnected");
                        throw new Exception("IO Stream disconnected.");
                    }
                    fout.write(btempData, 0, iValue);
                    readcount = readcount + iValue;
                    fireProgress((readcount * 100) / size);
                    progressMeter.setProgress(id, (readcount * 100) / size);
                    if (readcount == size)
                        break;
                }
                Thread.sleep(10);
            }
            fireProgressCompleted();
            progressMeter.setProcessCompleted(id, true);
            btempData = null;
            return false;
        } catch (Exception e) {
            if (progressMeter != null)
                progressMeter.setProcessCompleted(id, false);
            throw e;
        }
    }

    private void fireSetMax(int max) {
        for (int i = 0; i < progressListeners.size(); i++) {
            try {
            progressListeners.get(i).setMax(id, max);
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    private void fireProgress(int progress) {
        for (int i = 0; i < progressListeners.size(); i++) {
            try {
            progressListeners.get(i).setProgress(id, progress);
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    private void fireProgressStarted() {
        for (int i = 0; i < progressListeners.size(); i++) {
            try {
            progressListeners.get(i).setProcessStarted(id);
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    private void fireProgressCompleted() {
        for (int i = 0; i < progressListeners.size(); i++) {
            try {
            progressListeners.get(i).setProcessCompleted(id, true);
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void interrupt() {
        try {
            in.close();
        } catch (Exception e) {
        }
        try {
            out.close();
        } catch (Exception e) {
        }
    }
}
