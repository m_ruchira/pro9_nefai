package com.isi.csvr.downloader;

import com.isi.csvr.shared.*;
import com.isi.csvr.datastore.*;
import com.isi.csvr.communication.tcp.*;
import com.isi.csvr.trading.*;

import java.io.*;
import java.net.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Oct 19, 2007
 * Time: 5:25:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class ContentCMConnector extends Thread {
    private String request = null;
    private String errRequest=null;
    private BufferedInputStream in;
    private OutputStream out;
    private String exchange;
    private String appendStr;
    private byte path = 1;
    private int type = -1;
    String ip;

    public ContentCMConnector(String request, String exchange, byte path) {
        this(request, exchange, null, -1, path);
    }
    public ContentCMConnector(String request, String exchange, byte path,String errMsg) {
        this(request, exchange, null, -1, path,null,errMsg);
    }
    public ContentCMConnector(String request, String exchange, String ip, int type, byte path) {
        this(request, exchange, ip, type, path, null,null);
    }
    //added 19/11/2009
    public ContentCMConnector(String request, String exchange, String ip, int type, byte path,String errData) {
        this(request, exchange, ip, type, path, null,errData);
    }

    public ContentCMConnector(String request, String exchange, String ip, int type, byte path, String strToAppend,String errmsg) {
        this.request = request;
        this.exchange = exchange;
        this.path = path;
        this.type = type;
        this.appendStr = strToAppend;
        this.ip = ip;
        this.errRequest=errmsg;
        this.start();
    }

    public ContentCMConnector(String request, String exchange, String ip, byte path) {
        this(request, exchange, ip, -1, path);
    }
    //added 19/11/2009
    public ContentCMConnector(String request, String exchange, String ip, byte path,String errData) {
        this(request, exchange, ip, -1, path,errData);
    }

    public ContentCMConnector(String request, String exchange, byte path, int type) {
        this(request, exchange, null, type, path);
    }

    public void run() {
        try {
            downloadData();
        } catch (Exception e) {
//            e.printStackTrace();
            if (errRequest != null){
            TCPConnector.getFrameAnalyser().addFrame(this.errRequest+Meta.FD+e.toString()+Meta.EOL);
        }

        }
    }

    public void interrupt() {
        try {
            in.close();
        } catch (Exception e) {
        }
        try {
            out.close();
        } catch (Exception e) {
        }
    }

    public void downloadData() throws Exception {
        boolean isFromContentCM = false;
        boolean isFromGlobalCM = false;
        boolean isFromRegionalCM = false;
        boolean isFromPrimaryCM = false;
        Socket socket;

        String contentCMIP;
        if (path == Constants.CONTENT_PATH_SECONDARY) {
            contentCMIP = Settings.getGlobalContentProviderIP();
            isFromGlobalCM = true;
            isFromRegionalCM = false;
            isFromPrimaryCM = false;
            isFromContentCM = false;
        } else if (path == Constants.CONTENT_PATH_TERTIARY) {
            contentCMIP = Settings.getRegionalContentProviderIP();
            isFromRegionalCM = true;
            isFromGlobalCM = false;
            isFromPrimaryCM = false;
            isFromContentCM = false;
        } else {
            contentCMIP = Settings.getActiveIP();
            if (exchange != null) {
                try {
                    if (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == Constants.PATH_SECONDARY) {
                        contentCMIP = InternationalConnector.getActiveIP();
                    }
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            isFromPrimaryCM = true;
            isFromGlobalCM = false;
            isFromRegionalCM = false;
            isFromContentCM = false;
        }
        String exchangeContentIP = null;
        if (ip != null) {
            exchangeContentIP = ip;
        } else if (exchange != null) {
            try {
                exchangeContentIP = ExchangeStore.getSharedInstance().getExchange(exchange).getContentCMIP();
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        if (exchangeContentIP != null) {
            contentCMIP = exchangeContentIP;
            isFromContentCM = true;
            isFromGlobalCM = false;
            isFromRegionalCM = false;
            isFromPrimaryCM = false;
        }
        if (contentCMIP != null) {
            System.out.println("CCM " + contentCMIP+" Request : "+request);
            socket = TWSocket.createSocket(contentCMIP, Settings.CONTENT_SERVER_PORT);
//            socket = TWSocket.createSocket("", Settings.CONTENT_SERVER_PORT);
        } else {
            contentCMIP = Settings.getActiveIP();
            if (exchange != null) {
                if (ExchangeStore.getSharedInstance().getExchange(exchange).getPath() == Constants.PATH_SECONDARY) {
                    contentCMIP = InternationalConnector.getActiveIP();
                }
            }
            System.out.println("CCM " + contentCMIP+" Request : "+request);
            socket = TWSocket.createSocket(contentCMIP, Settings.DOWNLOAD_PORT);
//            socket = TWSocket.createSocket("", Settings.DOWNLOAD_PORT);
        }
        socket.setSoTimeout(60000);
        out = socket.getOutputStream();
        in = new BufferedInputStream(socket.getInputStream());


        out.write((Meta.USER_SESSION + Meta.DS + Settings.getSessionID() +
                Meta.FS + Meta.USER_TYPE + Meta.DS + Meta.USER_TYPE_NEW_TW +
                Meta.FS + Meta.USERID + Meta.DS + Settings.getUserID() + Meta.EOL).getBytes());
//
        int value = 0;
        // ignore the authentication reply
        value = in.read();
        while (value != '\n') {
            value = in.read();
            if (value == -1) {
                ContentListenerStore.getSharedInstance().fireContentDownloaded(false, type);
                throw new Exception("Error while getting data from content server ");
            }
        }
//        System.out.println("request =="+request);
        if (isFromGlobalCM) {
            System.out.println("Request to GlobalCM==" + request + " , port : " + Settings.CONTENT_SERVER_PORT + "ip : " + contentCMIP);

        } else if (isFromRegionalCM) {
            System.out.println("Request to RegionalCM==" + request + " , port : " + Settings.CONTENT_SERVER_PORT + "ip : " + contentCMIP);

        } else if (isFromContentCM) {
            System.out.println("Request to ContentCM==" + request + " , port : " + Settings.CONTENT_SERVER_PORT + "ip : " + contentCMIP);

        } else if (isFromPrimaryCM) {
            System.out.println("Request to PrimaryCM==" + request + " , port : " + Settings.DOWNLOAD_PORT + "ip : " + contentCMIP);

        } else {
            System.out.println("invalid request==" + request);
        }
        out.write((request).getBytes());

        StringBuilder buffer = new StringBuilder();
        if(appendStr != null){
            buffer.append(appendStr);
        }
        try {
            boolean active = true;
            while (active) {
                value = in.read();
                if (value == -1) {
                    //ContentListenerStore.getSharedInstance().fireContentDownloaded(false, type);
                    throw new Exception("End of Stream");
                } else if (value == Meta.ETX) {
//                    buffer.append((char) value);
                    active = false;
                } else if (value != '\n') {
                    buffer.append((char) value);
                    continue;
                }
//                if(value=='\n'){
//                   break;
//                }
                if ((buffer != null) && (buffer.length() > 0)) {
                    if (isFromGlobalCM) {
                        System.out.println("received from GlobalCM==" + buffer.toString());

                    } else if (isFromRegionalCM) {
                        System.out.println("received from RegionalCM==" + buffer.toString());

                    } else if (isFromContentCM) {
                        System.out.println("received from ContentCM==" + buffer.toString());

                    } else if (isFromPrimaryCM) {
                        System.out.println("received from PrimaryCM==" + buffer.toString());
                    }
                    TCPConnector.getFrameAnalyser().addFrame(buffer.toString());
                    buffer = null;
                    buffer = new StringBuilder("");
                    if(appendStr != null){
                        buffer.append(appendStr);
                    }
                }
            }

        } catch (Exception e) {
//            e.printStackTrace();
//            TCPConnector.getFrameAnalyser().addFrame(this.errRequest);
//            TCPConnector.getFrameAnalyser().addFrame(buffer.toString());
        }
        try {
            in.close();
            in = null;
            out.close();
            out = null;
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        socket.close();
        socket = null;
        isFromContentCM = false;
        isFromGlobalCM = false;
        isFromRegionalCM = false;
        isFromPrimaryCM = false;
    }
}
