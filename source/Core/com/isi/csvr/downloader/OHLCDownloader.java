package com.isi.csvr.downloader;

import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.chart.SplitPointTable;
import com.isi.csvr.chart.ChartSplitPoints;

import java.io.DataInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.zip.ZipInputStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.ArrayList;
import java.util.Calendar;
import java.text.SimpleDateFormat;
import java.text.ParseException;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 1, 2005
 * Time: 2:18:23 PM
 */
public class OHLCDownloader implements Runnable {

    private String exchange;
    private String symbol;
    private String key;
    private int type;
    private byte path;
    private boolean active = true;
    private SimpleDateFormat format;
    private ZipFileDownloader downloader;
    private static final String FILE_EXTRACT_LOCK   = "FILE_EXTRACT_LOCK";

    public OHLCDownloader(String exchange, String symbol, int instrument, int type, byte path) {
        this.exchange = exchange;
        this.symbol = symbol;
        this.key = SharedMethods.getKey(exchange, symbol, instrument);
        this.type = type;
        this.path = path;
        format = new SimpleDateFormat("yyyyMMdd");
        Thread thread = new Thread(this, "OHLCDownloader " + exchange);
        thread.start();
    }

    public int getType() {
        return type;
    }

    public void run() {
        String url = Meta.ADD_SYMBOL_REQUEST + Meta.DS + type +
                Meta.FD + exchange + Meta.FD + symbol + Meta.EOL;
        LinkedList<IntraDayOHLC> tempArray = new LinkedList<IntraDayOHLC>();
        while (active) {
            try {
                System.out.println("[OHLC Download] Started " + exchange + " " + symbol + SharedMethods.getMemoryDetails());
                downloader = new ZipFileDownloader(url, exchange, "OHLC-" + type + "-" + symbol, path);
                File[] files = downloader.downloadFiles();
                downloader = null;
                for (int i = 0; i < files.length; i++) {
                    if (files[i].length() > 0) {
                        extractFile(files[i], tempArray);
                    }
                    try {
                        files[i].delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                while (tempArray.size() > 0) {
                    if (type == Meta.HISTORY) {
                        OHLCStore.getInstance().addHistoryRecord(tempArray.removeLast());
                    } else {
                        OHLCStore.getInstance().addIntradayRecord(tempArray.removeLast());
                    }
                }
                break;
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Thread.sleep(5000);
                } catch (InterruptedException e1) {
                }
            } finally {
                //TodaysTradeBacklogDownloadManager.getSharedInstance().removeDownloader(key);
            }
        }
        tempArray = null;
        OHLCDownloadManager.getSharedInstance().removeHistoryRequest(key);
        System.out.println("[OHLC Download] Completed " + exchange + " " + symbol + SharedMethods.getMemoryDetails());
    }

    private void extractFile(File file, LinkedList<IntraDayOHLC> tempArray) {
        String record;
        synchronized (FILE_EXTRACT_LOCK) {
            int count = 0;
            try {
                SplitPointTable splitPointTable = OHLCStore.getInstance().getSplitPointTable(key);
                splitPointTable.clear();

                Date date;
                String[] ohlcData = null;
                ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));
                DataInputStream in = new DataInputStream(zIn);
                zIn.getNextEntry();

                while (true) {
                    record = in.readLine();
                    if (record == null) break; // end of streeam
                    count++;
                    ohlcData = record.split(Meta.FS);
                    IntraDayOHLC ohlcRecord;

                    if (type == Meta.HISTORY) {
                        date = format.parse(ohlcData[0]);
                        ohlcRecord = new IntraDayOHLC(key, date.getTime() / 60000,
                                Float.parseFloat(ohlcData[1]), Float.parseFloat(ohlcData[2]),
                                Float.parseFloat(ohlcData[3]), Float.parseFloat(ohlcData[4]),
                                Long.parseLong(ohlcData[8]), 0, 0); // todo intraday vwap
                        tempArray.add(ohlcRecord);
//                        OHLCStore.getInstance().addHistoryRecord(ohlcRecord);
                        date = null;
                    } else {
                        ohlcRecord = new IntraDayOHLC(key, Long.parseLong(ohlcData[0]),
                                Float.parseFloat(ohlcData[1]), Float.parseFloat(ohlcData[2]),
                                Float.parseFloat(ohlcData[3]), Float.parseFloat(ohlcData[4]),
                                Long.parseLong(ohlcData[5]), 0, 0); // todo intraday vwap
                        tempArray.add(ohlcRecord);
                        //OHLCStore.getInstance().addOHLCRecord(ohlcRecord);
                    }

                    record = null;
                    ohlcRecord = null;
                    if(count == 100){
                        Thread.sleep(1);
                        count = 0;
                    }
                }
                zIn.closeEntry();
                zIn.close();
                zIn = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Runtime.getRuntime().gc();
    }

    public void deactivate() {
        try {
            active = false;
            downloader.interrupt();
        } catch (Exception e) {

        }
    }
}
