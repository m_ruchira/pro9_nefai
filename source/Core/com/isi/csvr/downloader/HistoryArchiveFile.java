package com.isi.csvr.downloader;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 31, 2005
 * Time: 3:37:20 PM
 */
public class HistoryArchiveFile implements Serializable {
    private static final long serialVersionUID = 1212121212121212L;

    public String exchange;
    public String fileName;
    public long size;
    public transient boolean inProgress;

    public static final boolean START = true;
    public static final boolean END = false;

    public HistoryArchiveFile(String exchange, String fileName, long size, boolean start) {
        this.exchange = exchange;
        this.fileName = fileName;
        this.size = size;
        this.inProgress = start;
    }

    public String toString() {
        return "[Exchange: " + exchange + " File: " + fileName + " Size: " + size + " In Progress: " + inProgress + "]";
    }
}
