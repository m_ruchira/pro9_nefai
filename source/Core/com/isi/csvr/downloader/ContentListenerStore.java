package com.isi.csvr.downloader;

import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Dec 11, 2007
 * Time: 5:32:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class ContentListenerStore {

    private static ContentListenerStore self = null;
    private ArrayList<ContentListener> store = null;

    private ContentListenerStore() {
        store = new ArrayList<ContentListener>();
    }

    public static ContentListenerStore getSharedInstance(){
        if(self == null){
            self = new ContentListenerStore();
        }
        return self;
    }

    public void addContentListener(ContentListener listener){
        store.add(listener);
    }

    public void removeContentListener(ContentListener listener){
        store.remove(listener);
//        store.trimToSize();
    }

    public void fireContentDownloaded(boolean status, int type){
        for(int i = 0; i < store.size(); i++) {
            try {
                store.get(i).contentDownloaded(status, type);
            } catch(Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }
}
