package com.isi.csvr.downloader;

import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.event.ProgressListener;
import com.isi.csvr.shared.Constants;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 21, 2005
 * Time: 11:27:15 AM
 */
public class MarketDataDownloadManager implements ConnectionListener {
    private Hashtable<String, Downloader> table;

    private static MarketDataDownloadManager self = null;

    public static synchronized MarketDataDownloadManager getSharedInstance() {
        if (self == null) {
            self = new MarketDataDownloadManager();
        }
        return self;
    }

    private MarketDataDownloadManager() {
        table = new Hashtable<String, Downloader>();
        ConnectionNotifier.getInstance().addConnectionListener(this);
    }

    public void startNewDownloadSession(ProgressListener progressListener, String id, int type, float version, String contentIP,boolean isdefault) {
        Downloader downloader = table.get(id + type);

        
        // destroy the current download processtive if still active
        if (downloader != null) {
            downloader = table.get(id + type);
            downloader.deactivate();
            removeDownloader(id);
            downloader = null;
        }

        // start a new download process
        if (type == Downloader.EXCHANGE_DATA) {
//            contentIP = "75.124.69.67"; // todo: delete this == for testing only-shanika
            downloader = new MarketDataDownloader(progressListener, id, version, contentIP, type,isdefault);
        } else if (type == Downloader.EXCHANGE_PROPS) {
            downloader = new MarketDataDownloader(progressListener, id, version, contentIP, type,isdefault);
        } else {
            downloader = new PropertyFileDownloader(progressListener, id, version, type);
        }
        table.put(id + type, downloader);
    }

    public void removeDownloader(String id) {
        table.remove(id);
    }

    public int downloadCount() {
        return table.size();
    }

    /**
     * Deactivate all downloads if TW disconnected
     */
    public void twDisconnected() {
        Enumeration ids = table.keys();
        String id;
        MarketDataDownloader downloader;

        while (ids.hasMoreElements()) {
            id = (String) ids.nextElement();
            downloader = (MarketDataDownloader) table.get(id);
            if (downloader != null) {
                downloader.deactivate();
                removeDownloader(id);
                downloader = null;
            }
        }
    }

    public void twConnected() {

    }

}
