package com.isi.csvr.downloader;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import com.isi.csvr.Client;
import com.isi.csvr.util.MD5InputStream;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.downloader.ui.DownloadProgressManager;
import com.isi.csvr.downloader.ui.DownloadProgressMeter;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.shared.*;
import com.isi.util.TypeConvert;

import javax.swing.*;
import java.io.*;
import java.net.Socket;
import java.util.TimeZone;
import java.util.Vector;

public class TWUpdateDownloader implements Runnable, ApplicationListener {
    StringBuffer message;
    JTextArea txtMessage = new JTextArea();
    JScrollPane scroll;
    private boolean downloadInprogress = false;
    private static String serverTWVersion = null;
    private static String serverTWMD5 = null;

    public TWUpdateDownloader() {
        Application.getInstance().addApplicationListener(this);
    }

    public synchronized boolean isDownloadInprogress() {
        return downloadInprogress;
    }

    public synchronized void setDownloadInprogress(boolean downloadInprogress) {
        this.downloadInprogress = downloadInprogress;
    }

    public synchronized void activate() {
        if ((!isDownloadInprogress() ) && (!isAlreadyDownloaded())){  
            if (hasWriteAccess()){
                setDownloadInprogress(true);
                Thread thread = new Thread(this, "TWUpdateDownloader");
                thread.start();
            }else {
                String msg  = Language.getString("UPDATE_DOWNLOAD_FAILED_NO_WRITE_ACCESS");
                String path = System.getProperty("user.dir");
                path = path.replace("\\","\\\\");
                msg = msg.replaceFirst("\\[PATH\\]", path);
                SharedMethods.showNonBlockingMessage(msg, JOptionPane.ERROR_MESSAGE);
            }
        }
    }

    public void run() {
        downloadUpdate();
    }

    public void downloadUpdate() {
        try {
            // check if existing md5 code is equal to the active version's. if not save the active as he currebt
            SharedMethods.printLine("In TW update ================");

            
            Settings.setTwUpdateAvailable(false);
//            if ((serverTWMD5 != null) && (getServerTWVersion() != null) && (!Settings.TW_VERSION.equals(getServerTWVersion())))
//            { // must download new version
            SharedMethods.printLine("In TW update version mismatch ================");
            String currentMD5 = getCurrentMD5();
            if ((currentMD5 == null) || (!currentMD5.equalsIgnoreCase(serverTWMD5))) { // must start from beginning
                SharedMethods.printLine("In TW update NEW MD5 ================");
                resetUpdate();
                saveMD5();
            }
            System.out.println("[TW UPdate Download] Started ");
            downloadFile();
            System.out.println("[TW UPdate Download] Completed ");
            String newMD5 = getMD5OfUpdate();

            if (newMD5.equalsIgnoreCase(serverTWMD5)) {
                SharedMethods.printLine("Update validated ok");
                Settings.setTwUpdateAvailable(true);
                showDownloadCompletedMessage();
            } else {
                SharedMethods.printLine("[Corrupted or invalid update file detected. Update aborted]");
                resetUpdate();
//            }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        setDownloadInprogress(false);

    }

    private boolean isAlreadyDownloaded(){
        return Settings.isTwUpdateAvailable() || TWUpdateDownloader.isUpdateAuthentic();
    }

    public void downloadFile() throws Exception {
        Vector files;
        String serverIP = Settings.getRegionalContentProviderIP();
        if(serverIP == null){
            serverIP = Settings.getActiveIP();
        }
        Socket socket = TWSocket.createSocket(serverIP, Settings.DOWNLOAD_PORT);
        socket.setSoTimeout(60000);
        OutputStream out = socket.getOutputStream();
        BufferedInputStream in = new BufferedInputStream(socket.getInputStream());


        out.write((Meta.USER_SESSION + Meta.DS + Settings.getSessionID() +
                Meta.FS + Meta.USER_TYPE + Meta.DS + Meta.USER_TYPE_NEW_TW + Meta.EOL).getBytes());

        SharedMethods.printLine(Meta.USER_SESSION + Meta.DS + Settings.getSessionID());

        String url = Meta.TWV9_AUTOUPDATE_FILE + Meta.DS + getServerTWVersion() + Meta.FD + getSavedUpdateSize() + Meta.EOL;
        SharedMethods.printLine("Update URL ===========> " + url);

        // ignore the authentication reply
        int value;
        value = in.read();
        while (value != '\n') {
            value = in.read();
            if (value == -1) {
                throw new Exception("Error while downloading TW Update");
            }
        }

        out.write((url).getBytes());

        files = new Vector();
        File file;
        boolean noMoreData = false;

        // read the first 4 bytes to get the size of the updateTrigger to be read

        file = new File(Settings.UPDATE_PATH + "/update.mdf");
        files.addElement(file);
        FileOutputStream fout = new FileOutputStream(file, true);

        byte[] length = new byte[4];

        //fireProgressStarted();

        byte type = (byte) in.read(); // updateTrigger type ;

        for (int i = 0; i < length.length; i++) {
            length[i] = (byte) in.read();
        }

        int size = TypeConvert.byteArrayToInt(length, 0, 4);

        SharedMethods.printLine("Update size " + size);

        if (size <= 0) {
            //fireProgressCompleted();
            return; // no more data
        }

        //fireSetMax(size);

        DownloadProgressMeter progressMeter = null;
        byte[] btempData = new byte[1000];
        int readcount = 0;
        int iValue;

        progressMeter = DownloadProgressManager.createMeter("", "TWUpdate");
        progressMeter.setMax(null, size);

        while (true) {
            if ((size - readcount) >= 1000) {
                iValue = in.read(btempData);
                if (iValue == -1) {
                    progressMeter.setProcessCompleted(null, false);
                    System.out.println("TW Update Downloader Disconnected");
                    throw new Exception("IO Stream disconnected.");
                }
                fout.write(btempData, 0, iValue);
                fout.flush();

                readcount = readcount + iValue;
//                fireProgress((readcount * 100) / size);
                progressMeter.setProgress(null, (int)((readcount * 100L) / size));
                //System.out.println("1Size " + size + " readcount " + readcount + " iValue " + iValue);
                //System.out.println("Progress " + ((readcount * 100) / size));
                if (readcount == size)
                    break;
            } else {
                if (readcount == size)
                    break;
                btempData = new byte[size - readcount];
                iValue = in.read(btempData);
                if (iValue == -1) {
                    progressMeter.setProcessCompleted(null, false);
                    System.out.println("TW Update Downloader Disconnected");
                    throw new Exception("IO Stream disconnected.");
                }
                fout.write(btempData, 0, iValue);
                fout.flush();

                readcount = readcount + iValue;
//                fireProgress((readcount * 100) / size);
                progressMeter.setProgress(null, (int)((readcount * 100L) / size));
                //System.out.println("2Size " + size + " readcount " + readcount + " iValue " + iValue);
                //System.out.println("Progress " + ((readcount * 100) / size));
                if (readcount == size)
                    break;
            }
            Thread.sleep(10);
        }
//        fireProgressCompleted();
        progressMeter.setProcessCompleted(null, true);
        btempData = null;

        fout.close();
        fout = null;
        file = null;

        in = null;
        socket.close();
        socket = null;

        return;
    }

    /*public boolean readZip(FileOutputStream fout) throws Exception {
        DownloadProgressMeter progressMeter = null;
        try {


            return false;
        } catch (Exception e) {
            if (progressMeter != null)
                progressMeter.setProcessCompleted(id, false);
            throw e;
        }
    }*/

    /*public static void showDownloadCompletedMessage() {
        JOptionPane pane = new JOptionPane();
        int result = pane.showConfirmDialog(Client.getInstance().getFrame(),
                Language.getString("MSG_RESTART_AFTER_UPDATE"),
                Language.getString("INFORMATION"),
                JOptionPane.YES_NO_OPTION,
                JOptionPane.INFORMATION_MESSAGE);
        if (result == JOptionPane.YES_OPTION) {
            Client.getInstance().saveDataOnExit();
            System.exit(0);
        }
    }*/

    public static void showDownloadCompletedMessage() {

        // set the update trigger
        try {
            FileOutputStream out = new FileOutputStream("autoupdate/updatable");
            out.write("ok".getBytes());
            out.flush();
            out.close();
            out =null;

        } catch (Exception e) {
            e.printStackTrace();
        }

        JOptionPane pane = new JOptionPane();
        pane.showConfirmDialog(Client.getInstance().getFrame(),
                Language.getString("MSG_RESTART_AFTER_UPDATE"),
                Language.getString("INFORMATION"),
                JOptionPane.DEFAULT_OPTION,
                JOptionPane.INFORMATION_MESSAGE);


    }

    public void applicationExiting() {

    }

    public void applicationLoaded() {

    }

    public void applicationLoading(int percentage) {

    }

    public void applicationReadyForTransactions() {
        if ((serverTWMD5 != null) && (getServerTWVersion() != null) && (!Settings.TW_VERSION_NUMBER.equals(getServerTWVersion())))
        {
            activate();
        }
    }

    public void applicationTimeZoneChanged(TimeZone zone) {

    }

    public void snapshotProcessingEnded(Exchange exchange) {

    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
    }

    public void snapshotProcessingStarted(Exchange exchange) {

    }

    public void workspaceLoaded() {

    }

    public void workspaceWillSave() {

    }

    public void workspaceSaved() {

    }

    public void workspaceWillLoad() {

    }

    public static String getServerTWVersion() {
        return serverTWVersion;
    }

    public static void setServerTWVersionData(String versionData) {
        try {
            String[] fields = versionData.split(Meta.FD);
            serverTWVersion = fields[0];
            serverTWMD5 = fields[1];
        } catch (Exception e) {
            serverTWVersion = null;
            serverTWMD5 = null;
        }
    }

    public static String getCurrentMD5() {
        String currentMD5 = null;
        // check the existing md5 code
        try {
            DataInputStream in = new DataInputStream(new FileInputStream(Settings.UPDATE_PATH + "/update.msf"));
            currentMD5 = in.readLine();
            in.close();
            in = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return currentMD5;
    }

    public static synchronized String getMD5OfUpdate() {
        String md5 = null;
        try {
            SharedMethods.printLine("checking MD5 ...");
            int value = 0;
            MD5InputStream md5InputStream = new MD5InputStream(new FileInputStream(Settings.UPDATE_PATH + "/update.mdf"));
            while (true) {
                value = md5InputStream.read();
                if (value == -1) {
                    break;
                }
            }
            md5 = md5InputStream.getHashString();
            md5InputStream.close();
            md5InputStream = null;
            SharedMethods.printLine("MD5 Done");
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
        return md5;
    }

    private void saveMD5() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.UPDATE_PATH + "/update.msf");
            out.write(serverTWMD5.getBytes());
            out.flush();
            out.close();
            out = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private long getSavedUpdateSize() {
        try {
            File file = new File(Settings.UPDATE_PATH + "/update.mdf");
            long size = file.length();
            file = null;
            return size;
        } catch (Exception e) {
            return 0;
        }
    }

    private void resetUpdate() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.UPDATE_PATH + "/update.mdf");
            out.close();
            out = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }


public static boolean hasWriteAccess(){
        try {
            File check = new File("./writeCheck");
            OutputStream out = new FileOutputStream(check);
            out.write(0);
            out.close();
            check.deleteOnExit();
            return true;
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return false;
        }
    }
    private static File updateTrigger;
    public static boolean isUpdateDownloaded(){
        updateTrigger = new File( "autoupdate/updatable"); // if this updateTrigger is available, there is an update available
        if (updateTrigger.length() > 0){ // must update
            updateTrigger = null;
            return true;
        }
        updateTrigger = null;
        return false;
    }

    public static void finishUpdate(){
        try {
            updateTrigger.deleteOnExit();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static boolean isUpdateAuthentic(){
        try {
            return TWUpdateDownloader.getCurrentMD5().equalsIgnoreCase(TWUpdateDownloader.getMD5OfUpdate());
        } catch (Exception e) {
            return false;
        }
    }
}