package com.isi.csvr.downloader.ui;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 20, 2005
 * Time: 11:02:35 AM
 */
public class DownloadProgressManager {
    private static ArrayList<DownloadProgressMeter> store;

    private static DownloadProgressManager self;

    public DownloadProgressManager() {
        store = new ArrayList<DownloadProgressMeter>();

    }

    private static void init() {
        if (self == null) {
            self = new DownloadProgressManager();
        }
    }

    public static DownloadProgressMeter createMeter(String exchange, String caption) {
        init();
        DownloadProgressMeter progressMeter = new DownloadProgressMeter(exchange, caption);
        store.add(progressMeter);

        return progressMeter;
    }

    public static void removeMeter(DownloadProgressMeter toBeRemoved) {
        init();
        for (DownloadProgressMeter meter : store) {
            if (meter == toBeRemoved) {
                store.remove(meter);
                break;
            }
        }
    }

    public ArrayList<DownloadProgressMeter> getMeters() {
        return store;
    }

    public static String getStatus() {
        StringBuilder stringBuilder = new StringBuilder();
        if (store != null) {
            for (DownloadProgressMeter meter : store) {
                stringBuilder.append(meter.toString());
            }

            return stringBuilder.toString();
        } else {
            return "";
        }
    }

    public static void removeCompletedItems() {
        for (int i = store.size() - 1; i >= 0; i--) {
            if (store.get(i).getProgress() >= 100) {
                store.remove(i);
            }
        }
    }

    public static int count() {
        if (store != null)
            return store.size();
        else
            return 0;
    }

}
