package com.isi.csvr.downloader;

import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.trade.TradeStore;

import java.util.LinkedList;
import java.util.List;
import java.util.zip.ZipInputStream;
import java.io.File;
import java.io.FileInputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 11, 2005
 * Time: 2:05:44 PM
 */
public class TodaysOHLCBacklogDownloader implements Runnable {
    private List<String> queue;
    private boolean active = true;
    private String id;
    private ZipFileDownloader downloader;
    private static final String FILE_EXTRACT_LOCK   = "FILE_EXTRACT_LOCK";

    public TodaysOHLCBacklogDownloader(List<String> queue, String id) {
        this.queue = queue;
        this.id = id;
        Thread thread = new Thread(this, "TodaysOHLCBacklogDownloader " + id);
        thread.start();
    }

    public void run() {
        int loopCount = 0;
        while (active) {
            System.out.println("[OHLC Backlog Download] Started " + id + SharedMethods.getMemoryDetails());
            try {
                loopCount++;
                //HistoryArchiveRegister.getSharedInstance().addEntry(id, "TodaysOHLCBacklog", 0, HistoryArchiveFile.START);
                String url = Meta.ADD_EXCHANGE_REQUEST + Meta.DS + Meta.ALL_OHLC +
                        Meta.FD + id + Meta.FD + TradeStore.getSharedInstance().getLastValidTradeSequence(id) + Meta.EOL;
                downloader = new ZipFileDownloader(url, id, "OHLC-Backlog");
                File[] files = downloader.downloadFiles();
                downloader = null;
                for (int i = 0; i < files.length; i++) {
                    if (files[i].length() > 0) {
                        extractFile(files[i]);
                    }
                    try {
                        files[i].delete();
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                //HistoryArchiveRegister.getSharedInstance().addEntry(id, "TodaysOHLCBacklog", size, HistoryArchiveFile.END);
                break;
            } catch (Exception e) {
                e.printStackTrace();
                try {
                    Thread.sleep(2000);
                } catch (InterruptedException e1) {
                }
                if (loopCount >10){
                    break;
                }
            }
        }
        TodaysOHLCBacklogDownloadManager.getSharedInstance().removeDownloader(id);
        TodaysOHLCBacklogDownloadManager.getSharedInstance().fireOHLCBacklogDownloaded(id);
        System.out.println("[OHLC Backlog Download] Completed " + id + SharedMethods.getMemoryDetails());
    }

    private void extractFile(File file) {

        synchronized (FILE_EXTRACT_LOCK) {
            try {
                int rs = Meta.RS.charAt(0);
                StringBuilder sFrame = new StringBuilder();
                ZipInputStream zIn = new ZipInputStream(new FileInputStream(file));
                zIn.getNextEntry();
                int iValue;

                sFrame.append(Meta.BACKLOG_FRAME);
                sFrame.append(Meta.DS);
                sFrame.append(Meta.YES);
                sFrame.append(Meta.FS);

                while (true) {
                    iValue = zIn.read();
                    if (iValue == -1) break; // end of streeam
                    if (iValue != rs) {
                        sFrame.append((char) iValue);
                        continue;
                    }
                    if ((sFrame != null) && (sFrame.length() > 0)) {
                        queue.add(sFrame.toString());
                        sFrame = null;
                        sFrame = new StringBuilder("");
                        sFrame.append(Meta.BACKLOG_FRAME);
                        sFrame.append(Meta.DS);
                        sFrame.append(Meta.YES);
                        sFrame.append(Meta.FS);
                        Thread.sleep(1);
                    }
                }
                zIn.closeEntry();
                zIn.close();
                zIn = null;
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        Runtime.getRuntime().gc();
    }

    public void deactivate() {
        if (downloader != null) {
            downloader.interrupt();
        }
        active = false;
    }

    public boolean isActive() {
        return active;
    }
}
