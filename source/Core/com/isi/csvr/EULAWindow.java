package com.isi.csvr;

import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.io.InputStream;
import java.util.concurrent.CountDownLatch;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 7, 2003
 * Time: 3:11:01 PM
 * To change this template use Options | File Templates.
 */
public class EULAWindow extends JDialog implements Themeable, ActionListener, Runnable {
    public static EULAWindow self = null;
    public JLabel messageBodyEN;
    public JLabel messageBodyAR;
    public CardLayout cardLayout;
    public JPanel messagePanel;
    public TWButton nextMessage;
    private boolean accepted = false;
    private boolean startupMode;
    private CountDownLatch lock;
    private TWButton donotAgreeButton;

    private JPanel messageContentPanel;

    public EULAWindow(JFrame parent, boolean startupMode) {
        super(parent, true);
        this.startupMode = startupMode;
        jbinit(startupMode);
    }

    private void jbinit(boolean startupMode) {
        Theme.registerComponent(this);
        this.getContentPane().setLayout(new BorderLayout());

        messageBodyEN = new JLabel();
        JScrollPane scrollpaneEN = new JScrollPane(messageBodyEN);
        messageBodyEN.setFont(Theme.getDefaultFont());

        messageBodyAR = new JLabel();
        JScrollPane scrollpaneAR = new JScrollPane(messageBodyAR);
        messageBodyAR.setFont(Theme.getDefaultFont());

        cardLayout = new CardLayout();
        messagePanel = new JPanel(cardLayout);

        messagePanel.add(scrollpaneAR, "AR");
        messagePanel.add(scrollpaneEN, "EN");

        messageContentPanel = new JPanel(new BorderLayout());
        messageContentPanel.add(messagePanel, BorderLayout.CENTER);
        nextMessage = new TWButton();
        nextMessage.setBorderPainted(false);
        nextMessage.setContentAreaFilled(false);
        nextMessage.addActionListener(this);
        nextMessage.setActionCommand("EN");
        nextMessage.setCursor(new Cursor(Cursor.HAND_CURSOR));
        nextMessage.setText(Language.getString("SHOW_ENGLISH_MESSAGE"));
//        messageContentPanel.add(nextMessage, BorderLayout.SOUTH);

        this.getContentPane().add(messageContentPanel, BorderLayout.CENTER);

        setTitle(Language.getString("EULA"));
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        GUISettings.applyOrientation(scrollpaneEN, ComponentOrientation.LEFT_TO_RIGHT);
        GUISettings.applyOrientation(scrollpaneAR, ComponentOrientation.RIGHT_TO_LEFT);
        SwingUtilities.updateComponentTreeUI(this.getContentPane());

        lock = new CountDownLatch(1);

    }

    public void show() {
        Thread thread = new Thread(this, "Eula");
        thread.start();
        try {
            lock.await();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        super.show();

    }

    public JPanel createBottomPanel() {
        JPanel panel = new JPanel();

        panel.setLayout(new FlowLayout(FlowLayout.CENTER));
        TWButton agreeButton = new TWButton(Language.getString("I_AGREE"));
         donotAgreeButton = new TWButton(Language.getString("I_DONT_AGREE"));

        donotAgreeButton.addActionListener(this);
        donotAgreeButton.setActionCommand("DONOT");
        agreeButton.addActionListener(this);
        agreeButton.setActionCommand("AGREE");

        panel.add(agreeButton);
        panel.add(donotAgreeButton);
        GUISettings.applyOrientation(panel);
     //   donotAgreeButton.requestFocusInWindow();
        return panel;
    }

    public void requestFocusTobutton(){
        try {
            donotAgreeButton.requestFocusInWindow();
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void run() {
        loadMessages();
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        if (startupMode)
            this.getContentPane().add(createBottomPanel(), BorderLayout.SOUTH);
        int width;
        int height;
        int scrollBarWidth = UIManager.getDefaults().getInt("ScrollBar.width");
        if ((messageBodyEN.getPreferredSize().getWidth()) > (oDm.getWidth() - 100)) {
            width = (int) oDm.getWidth() - 100;
        } else {
            width = (int) messageBodyEN.getPreferredSize().getWidth() + scrollBarWidth + 1;
        }

        if ((messageBodyEN.getPreferredSize().getHeight()) > (oDm.getHeight() - 100)) {
            height = (int) oDm.getHeight() - 200;
        } else {
            height = (int) messageBodyEN.getPreferredSize().getHeight();
        }

        messageContentPanel.setPreferredSize(new Dimension(width, height));
        pack();
        requestFocusTobutton();

        SharedMethods.centerWindow(this);
        if (Language.isLTR()) {
            nextMessage.doClick();
        }
        lock.countDown();
    }

    private void loadMessages() {
        StringBuffer buffer = new StringBuffer();
        String template;

        byte[] temp = new byte[10000];
        int count = 0;
        InputStream in;

        try {
            in = new FileInputStream(".\\Templates\\eula_EN.htm");
            while (true) {
                count = in.read(temp);
                Thread.sleep(10);
                if (count == -1) {
                    break;
                }
                if (count > 0) {
                    String str = new String(temp, 0, count);
                    buffer.append(str);
                    str = null;
                }
            }
            template = buffer.toString();
            template = template.replaceAll("\\[PATH\\]",
                    (System.getProperties().get("user.dir") + "/Templates/").replaceAll("\\\\", "/"));
            messageBodyEN.setText(template);
            //messageBodyEN.updateUI();
            template = null;
            in = null;
            buffer = null;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            template = "";
        }

        buffer = new StringBuffer();
        try {
            in = new FileInputStream(".\\Templates\\eula_AR.htm");
            while (true) {
                count = in.read(temp);
                Thread.sleep(10);
                if (count == -1) {
                    break;
                }
                if (count > 0) {
                    String str = new String(temp, 0, count);
                    buffer.append(str);
                    str = null;
                }
            }
            template = buffer.toString();
            template = template.replaceAll("\\[PATH\\]",
                    (System.getProperties().get("user.dir") + "/Templates/").replaceAll("\\\\", "/"));
            messageBodyAR.setText(template);
            //messageBodyAR.updateUI();

        }
        catch (Exception ex) {
            ex.printStackTrace();
            template = "";
        }
        buffer = null;
        in = null;
        temp = null;
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this.getContentPane());
    }

    public boolean isAccepted() {
        return accepted;
    }

    public void setAccepted(boolean accepted) {
        this.accepted = accepted;
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("DONOT")) {
            System.exit(0);
        } else if (e.getActionCommand().equals("AGREE")) {
            Settings.setItem("EULA_SHOWN", "true");
            setAccepted(true);
            dispose();
        } else if (e.getActionCommand().equals("EN")) {
            nextMessage.setText(Language.getString("SHOW_ARABIC_MESSAGE"));
            cardLayout.show(messagePanel, "EN");
            nextMessage.setActionCommand("AR");
        } else if (e.getActionCommand().equals("AR")) {
            nextMessage.setText(Language.getString("SHOW_ENGLISH_MESSAGE"));
            cardLayout.show(messagePanel, "AR");
            nextMessage.setActionCommand("EN");
        }
        messagePanel.repaint();
    }
}
