package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.Application;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.WindowDaemon;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.linkedwindows.LinkedWindowListener;
import com.isi.csvr.marketdepth.DepthObject;
import com.isi.csvr.marketdepth.DepthPriceModel;
import com.isi.csvr.marketdepth.DepthStore;
import com.isi.csvr.marketdepth.TotalQuantityIndicator;
import com.isi.csvr.properties.DefaultSettingsManager;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.MarketAskDepthRenderer;
import com.isi.csvr.table.MarketBidDepthRenderer;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.csvr.trading.shared.TradingShared;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Oct 22, 2008
 * Time: 2:54:42 PM
 */
public class DepthByPriceNormalFrame extends InternalFrame implements LinkedWindowListener, ActionListener, Themeable {

    private Table bidTable;
    private Table askTable;
    private ViewSetting oBidViewSetting = null;
    private ViewSetting oAskViewSetting = null;
    private DepthPriceModel askModel;
    private DepthPriceModel bidModel;
    private String selectedKey;
    boolean loadedFromWorkspace = false;
    byte stockDecimalPlaces = 2;
    byte parentWindowType;
    private ViewSettingsManager g_oViewSettings;
    private long timeStamp;
    private boolean isLinked;
    private MultiDialogWindow fullquote;
    private static Timer timer;
    private static boolean wasDoubleClick;
    private int tableHeight;
    private int tableWidth;
    private boolean sizeChanged = false;
//    private Dimension size;
//    private String exchange = "TDWL";
    private static final Dimension DIM_FOR_FIVE_ROWS = new Dimension(531, 210);
    private static final Dimension DIM_FOR_TEN_ROWS = new Dimension(531, 335);

    public DepthByPriceNormalFrame(String sThreadID, WindowDaemon oDaemon, Table oTable, Table[] oTable2, String key,
                                   boolean fromWSP, byte parentWindowType, boolean isLinked, String linkgroup) {
        super(sThreadID, oDaemon, oTable, oTable2);
        bidTable = oTable;
        askTable = oTable2[0];
        loadedFromWorkspace = fromWSP;
        selectedKey = key;
        this.parentWindowType = parentWindowType;
        g_oViewSettings = Client.getInstance().getViewSettingsManager();
        this.isLinked = isLinked;
        setLinkedGroupID(linkgroup);
        initUI();
        Theme.registerComponent(this);
        this.setResizable(true);
        Exchange ex = ExchangeStore.getSharedInstance().getSelectedExchange();
        String exchange = ex.toString();
        
        if (!loadedFromWorkspace) {
            if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketDepthByPriceAD) && ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketDepthByPrice) && !isChartVisible && parentWindowType == 1) {
                this.setSize(DIM_FOR_TEN_ROWS);
            } else if (!ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketDepthByPriceAD) && ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketDepthByPrice) && !isChartVisible && parentWindowType == 1) {
                this.setSize(DIM_FOR_FIVE_ROWS);
            }
        }
    }

    private boolean isChartVisible = false;
    private int VERTICAL_GAP = 10;
    private JPanel bidPanel;
    private BarChartPanel bidChart;
    private JPanel askPanel;
    private BarChartPanel askChart;
    private TWButton btnUpDown;
    private JLabel lblSymbolName;

    public void initUI() {
        bidTable.setAutoResize(true);
        bidTable.getPopup().showLinkMenus();
        if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
            bidTable.getPopup().showSetDefaultMenu();
        }
        bidTable.setWindowType(ViewSettingsManager.DEPTH_PRICE_BID_VIEW);
        TotalQuantityIndicator bidTotalIndicator = new TotalQuantityIndicator(Language.getString("TOTAL_BID_QUANTITY_SPLITS"));
        bidModel = new DepthPriceModel(bidTotalIndicator, selectedKey, DepthObject.BID);

        if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
            if (oBidViewSetting == null && loadedFromWorkspace && isLinked) {
                ViewSetting oSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
                if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                    oBidViewSetting = oSetting;
                }
            }
            if (oBidViewSetting == null) {
                oBidViewSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_PRICE_BID_VIEW +
                        "|" + selectedKey);

            }
        } else {
            if (oBidViewSetting == null && loadedFromWorkspace && isLinked) {
                ViewSetting oSetting = g_oViewSettings.getFullQuoteSubView(ViewSettingsManager.DEPTH_PRICE_BID_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
                if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                    oBidViewSetting = oSetting;
                }
            }
            if (oBidViewSetting == null) {
                oBidViewSetting = g_oViewSettings.getFullQuoteSubView(ViewSettingsManager.DEPTH_PRICE_BID_VIEW +
                        "|" + selectedKey);
            }
        }
        if (oBidViewSetting == null) {
            oBidViewSetting = g_oViewSettings.getSymbolView("DEPTH_PRICE_BID_VIEW");
            oBidViewSetting = oBidViewSetting.getObject();
            oBidViewSetting.setDesktopType(parentWindowType);
            if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
                GUISettings.setColumnSettings(oBidViewSetting, TWColumnSettings.getItem("DEPTH_PRICE_COLS"));
                SharedMethods.applyCustomViewSetting(oBidViewSetting);
                DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oBidViewSetting.getType(), oBidViewSetting);
                timeStamp = System.currentTimeMillis();
                oBidViewSetting.setID(timeStamp + "");
                oBidViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
                oBidViewSetting.putProperty(ViewConstants.VC_LINKED, false);
                oBidViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);

                g_oViewSettings.putDepthView(ViewSettingsManager.DEPTH_PRICE_BID_VIEW +
                        "|" + selectedKey, oBidViewSetting);
            } else {
                oBidViewSetting.setRecordSeperator(MultiDialogWindow.RECORD_SEPERATOR);
                SharedMethods.applyCustomViewSetting(oBidViewSetting);
                timeStamp = System.currentTimeMillis();
                oBidViewSetting.setID(timeStamp + "");
                oBidViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
                oBidViewSetting.putProperty(ViewConstants.VC_LINKED, false);
                oBidViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
                g_oViewSettings.putFullQuoteSubView(ViewSettingsManager.DEPTH_PRICE_BID_VIEW +
                        "|" + selectedKey, oBidViewSetting);
            }
        } else {
            loadedFromWorkspace = true;
        }

        bidModel.setViewSettings(oBidViewSetting);
        bidTable.setModel(bidModel);
        bidModel.setTable(bidTable, new MarketBidDepthRenderer(), true);  // Add the renderer...
        bidModel.applyColumnSettings();
        bidTable.setSouthPanel(bidTotalIndicator);
        TWMenuItem calcMenu1 = new TWMenuItem(Language.getString("PRICE_CALC"), "ordercalc.gif");
        calcMenu1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (parentWindowType == Constants.FULL_QUOTE_TYPE) {
                    Client.getInstance().mnu_MDepth_Calculator(selectedKey, TWTypes.TradeSides.SELL, false, false, LinkStore.LINK_NONE);
                } else {
                    Client.getInstance().mnu_MDepth_Calculator(selectedKey, TWTypes.TradeSides.SELL, false, false, LinkStore.LINK_NONE);
                }
            }
        });
        bidTable.getPopup().setMenuItem(calcMenu1);
        final TWMenuItem sellmenu = new TWMenuItem(Language.getString("SELL"), "sell.gif");
        sellmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (parentWindowType == Constants.FULL_QUOTE_TYPE) {
                    TradeMethods.getSharedInstance().showSellScreen(selectedKey);
                } else {
                    long qty = bidModel.getTradeQty(bidTable.getTable().getSelectedRow());
                    TradeMethods.getSharedInstance().showSellScreen(selectedKey, qty);
                }
            }
        });
        bidTable.getPopup().setMenuItem(sellmenu);
        bidTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    if (parentWindowType == Constants.FULL_QUOTE_TYPE) {
                        TradeMethods.getSharedInstance().showSellScreen(selectedKey);
                    } else {
                        long qty = bidModel.getTradeQty(bidTable.getTable().getSelectedRow());
                        TradeMethods.getSharedInstance().showSellScreen(selectedKey, qty);
                    }
                    wasDoubleClick = true;
                } else {
                    Integer timerinterval = (Integer)
                            Toolkit.getDefaultToolkit().getDesktopProperty(
                                    "awt.multiClickInterval");
                    timer = new Timer(timerinterval.intValue(), new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            if (wasDoubleClick) {
                                wasDoubleClick = false; // reset flag
                            } else {//single click
                                //     long q = bidModel.getTradeQty(bidTable.getTable().getSelectedRow());
                                double price = bidModel.getTradePrice(0);
                                long qty = bidModel.getSelectedTradeQty(0);
                                Client.getInstance().loadRapidOrderFromDepth(selectedKey, price, qty, TradeMeta.SELL);
                            }
                        }
                    });
                    timer.setRepeats(false);
                    timer.start();
                }
            }
        });
        bidTable.getPopup().addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                sellmenu.setVisible(TradingShared.isReadyForTrading());
                sellmenu.setEnabled(TradingShared.isReadyForTrading());
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

            }

            public void popupMenuCanceled(PopupMenuEvent e) {

            }
        });

        /* Create the Ask Market Depth Table */
        askTable.setAutoResize(true);
        askTable.getPopup().showLinkMenus();
        if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
            askTable.getPopup().showSetDefaultMenu();
        }
        askTable.setWindowType(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW);
        TotalQuantityIndicator askTotalIndicator = new TotalQuantityIndicator(Language.getString("TOTAL_ASK_QUANTITY_SPLITS"));
        askModel = new DepthPriceModel(askTotalIndicator, selectedKey, DepthObject.ASK);

        if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
            if (oAskViewSetting == null && loadedFromWorkspace && isLinked) {
                ViewSetting oSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
                if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                    oAskViewSetting = oSetting;
                }

            }
            if (oAskViewSetting == null) {
                oAskViewSetting = g_oViewSettings.getDepthView(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW +
                        "|" + selectedKey);
            }
        } else {

            if (oAskViewSetting == null && loadedFromWorkspace && isLinked) {
                ViewSetting oSetting = g_oViewSettings.getFullQuoteSubView(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" + LinkStore.linked + "_" + getLinkedGroupID());
                if (oSetting != null && oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).equals(selectedKey)) {
                    oAskViewSetting = oSetting;
                }
            }
            if (oAskViewSetting == null) {
                oAskViewSetting = g_oViewSettings.getFullQuoteSubView(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW +
                        "|" + selectedKey);
            }
        }

        if (oAskViewSetting == null) {
            oAskViewSetting = g_oViewSettings.getSymbolView("DEPTH_PRICE_ASK_VIEW");
            oAskViewSetting = oAskViewSetting.getObject();
            oAskViewSetting.setDesktopType(parentWindowType);
            if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
                GUISettings.setColumnSettings(oAskViewSetting, TWColumnSettings.getItem("DEPTH_PRICE_COLS"));
                SharedMethods.applyCustomViewSetting(oAskViewSetting);
                DefaultSettingsManager.getSharedInstance().applyDefaultSetting(oAskViewSetting.getType(), oAskViewSetting);
                oAskViewSetting.setID(timeStamp + "");
                oAskViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
                oAskViewSetting.putProperty(ViewConstants.VC_LINKED, false);
                oAskViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
                g_oViewSettings.putDepthView(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW +
                        "|" + selectedKey, oAskViewSetting);
            } else {
                oAskViewSetting.setRecordSeperator(MultiDialogWindow.RECORD_SEPERATOR);
                SharedMethods.applyCustomViewSetting(oAskViewSetting);
                oAskViewSetting.setID(timeStamp + "");
                oAskViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
                oAskViewSetting.putProperty(ViewConstants.VC_LINKED, false);
                oAskViewSetting.putProperty(ViewConstants.VC_LINKED_GROUP, LinkStore.LINK_NONE);
                g_oViewSettings.putFullQuoteSubView(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW +
                        "|" + selectedKey, oAskViewSetting);
            }
        }

        askModel.setViewSettings(oAskViewSetting);
        askTable.setModel(askModel);
        askModel.setTable(askTable, new MarketAskDepthRenderer(), false);  // Add the renderer...
        askModel.applyColumnSettings();
        askTable.setSouthPanel(askTotalIndicator);
        TWMenuItem calcMenu2 = new TWMenuItem(Language.getString("PRICE_CALC"), "ordercalc.gif");
        calcMenu2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (parentWindowType == Constants.FULL_QUOTE_TYPE) {
                    Client.getInstance().mnu_MDepth_Calculator(selectedKey, TWTypes.TradeSides.BUY, false, false, LinkStore.LINK_NONE);
                } else {
                    Client.getInstance().mnu_MDepth_Calculator(selectedKey, TWTypes.TradeSides.BUY, false, false, LinkStore.LINK_NONE);
                }
            }
        });
        askTable.getPopup().setMenuItem(calcMenu2);
        final TWMenuItem buymenu = new TWMenuItem(Language.getString("BUY"), "buy.gif");
        buymenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (parentWindowType == Constants.FULL_QUOTE_TYPE) {
                    TradeMethods.getSharedInstance().showBuyScreen(selectedKey);
                } else {
                    long qty = askModel.getTradeQty(askTable.getTable().getSelectedRow());
                    TradeMethods.getSharedInstance().showBuyScreen(selectedKey, qty);
                }
            }
        });
        askTable.getPopup().setMenuItem(buymenu);
        askTable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() == 2) {
                    if (parentWindowType == Constants.FULL_QUOTE_TYPE) {
                        TradeMethods.getSharedInstance().showBuyScreen(selectedKey);
                    } else {
                        long qty = askModel.getTradeQty(askTable.getTable().getSelectedRow());
                        TradeMethods.getSharedInstance().showBuyScreen(selectedKey, qty);
                    }
                    wasDoubleClick = true;
                } else {
                    Integer timerinterval = (Integer)
                            Toolkit.getDefaultToolkit().getDesktopProperty(
                                    "awt.multiClickInterval");
                    timer = new Timer(timerinterval.intValue(), new ActionListener() {
                        public void actionPerformed(ActionEvent evt) {
                            if (wasDoubleClick) {
                                wasDoubleClick = false; // reset flag
                            } else {//single click
                                //     long q = bidModel.getTradeQty(bidTable.getTable().getSelectedRow());
                                double price = askModel.getTradePrice(0);
                                long qty = askModel.getSelectedTradeQty(0);
                                Client.getInstance().loadRapidOrderFromDepth(selectedKey, price, qty, TradeMeta.BUY);
                            }
                        }
                    });
                    timer.setRepeats(false);
                    timer.start();
                }
            }
        });

        askTable.getPopup().addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                buymenu.setVisible(TradingShared.isReadyForTrading());
                buymenu.setEnabled(TradingShared.isReadyForTrading());
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

            }

            public void popupMenuCanceled(PopupMenuEvent e) {

            }
        });

        Application.getInstance().addApplicationListener(this);
        oAskViewSetting.setParent(this);
        oAskViewSetting.setTableNumber(2);
        oBidViewSetting.setParent(this);
        oBidViewSetting.setTableNumber(1);

        this.setShowServicesMenu(true);
        this.getContentPane().setLayout(new BorderLayout());
        this.setSize(oBidViewSetting.getSize());
//        this.setSize(size);
        this.setColumnResizeable(true);
        this.setDetachable(true);
        this.setPrintable(true);
        this.setResizable(true);
        this.setClosable(true);
        this.setMaximizable(false);
        this.setIconifiable(true);
        this.setLinkGroupsEnabled(true);
        try {
            if (oBidViewSetting.getProperty(ViewConstants.VC_BID_ASK_CHART_VISIBLE) != null) {
                isChartVisible = Boolean.parseBoolean(oBidViewSetting.getProperty(ViewConstants.VC_BID_ASK_CHART_VISIBLE));
            } else {
                isChartVisible = false;
            }
        } catch (Exception e) {
            isChartVisible = false;
            e.printStackTrace();
        }
        if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
            Client.getInstance().oTopDesktop.add(this);
            this.setTitle(Language.getString("MARKET_DEPTH_BY_PRICE") + " : (" + SharedMethods.getSymbolFromKey(selectedKey) + ") ");
        }

        this.setDefaultCloseOperation(JInternalFrame.DISPOSE_ON_CLOSE);

        bidPanel = new JPanel(new BorderLayout(VERTICAL_GAP, VERTICAL_GAP));
        bidChart = new BarChartPanel(bidTable, askTable, BarChartPanel.TYPE_BID);
        bidChart.setPreferredSize(new Dimension(this.getWidth() * (int) (oBidViewSetting.getSize().getWidth() / 2), 120));
        if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
            bidPanel.add(bidChart, BorderLayout.NORTH);
        }
        bidPanel.add(bidTable, BorderLayout.CENTER);

        askPanel = new JPanel(new BorderLayout(VERTICAL_GAP, VERTICAL_GAP));
        askChart = new BarChartPanel(askTable, bidTable, BarChartPanel.TYPE_ASK);
        askChart.setPreferredSize(new Dimension(this.getWidth() * (int) (oBidViewSetting.getSize().getWidth() / 2), 120));
        if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
            askPanel.add(askChart, BorderLayout.NORTH);
        }
        askPanel.add(askTable, BorderLayout.CENTER);

        JSplitPane splitPane;
        if (Language.isLTR()) {
            this.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
            splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, bidPanel, askPanel) {
                public int getDividerSize() {
                    return 4;
                }
            };
            splitPane.setOneTouchExpandable(false);
            if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
                splitPane.setDividerLocation((int) (oBidViewSetting.getSize().getWidth() / 2));
            } else {
                if (oBidViewSetting.getSplitLocation() == -1) {
                    splitPane.setDividerLocation((int) (g_oViewSettings.getSymbolView("FULL_QUOTE_VIEW").getSize().getWidth() / 2));
                    oBidViewSetting.setSplitLocation((int) (g_oViewSettings.getSymbolView("FULL_QUOTE_VIEW").getSize().getWidth() / 2));
                }
            }
            JPanel paneTop = new JPanel();
            paneTop.setLayout(new BoxLayout(paneTop, BoxLayout.X_AXIS));

            lblSymbolName = new JLabel();
            lblSymbolName.setFont(new Font("Arial", Font.BOLD, 12));
            Stock st = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(selectedKey), SharedMethods.getSymbolFromKey(selectedKey), SharedMethods.getInstrumentTypeFromKey(selectedKey));
            if (st != null) {
                lblSymbolName.setText(st.getLongDescription());
            }
            btnUpDown = new TWButton();
            btnUpDown.setPreferredSize(new Dimension(20, 20));
            btnUpDown.addActionListener(this);

            paneTop.add(Box.createHorizontalStrut(2));
            paneTop.add(lblSymbolName);
            paneTop.add(Box.createHorizontalGlue());
            paneTop.add(btnUpDown);
            paneTop.add(Box.createHorizontalStrut(1));

            if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
                this.getContentPane().add(BorderLayout.NORTH, paneTop);
            }
            this.getContentPane().add(BorderLayout.CENTER, splitPane);

            if (!isChartVisible) {
                btnUpDown.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/expand_chart.gif"));
                bidChart.setVisible(false);
                askChart.setVisible(false);
                askPanel.doLayout();
                bidPanel.doLayout();
            } else {
                btnUpDown.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/collapse_chart.gif"));
                bidChart.setVisible(true);
                askChart.setVisible(true);
                askPanel.doLayout();
                bidPanel.doLayout();
            }

            splitPane.updateUI();
        } else {
            this.setComponentOrientation(ComponentOrientation.RIGHT_TO_LEFT);
            splitPane = new JSplitPane(JSplitPane.HORIZONTAL_SPLIT, askPanel, bidPanel) {
                public int getDividerSize() {
                    return 4;
                }
            };
            splitPane.setOneTouchExpandable(false);
            if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
                splitPane.setDividerLocation((int) (oBidViewSetting.getSize().getWidth() / 2));
            } else {
                if (oBidViewSetting.getSplitLocation() == -1) {
                    splitPane.setDividerLocation((int) (g_oViewSettings.getSymbolView("FULL_QUOTE_VIEW").getSize().getWidth() / 2));
                    oBidViewSetting.setSplitLocation((int) (g_oViewSettings.getSymbolView("FULL_QUOTE_VIEW").getSize().getWidth() / 2));

                }

            }
            JPanel paneTop = new JPanel();
            paneTop.setLayout(new BoxLayout(paneTop, BoxLayout.X_AXIS));

            lblSymbolName = new JLabel();
            lblSymbolName.setFont(new Font("Arial", Font.BOLD, 12));
            Stock st = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(selectedKey), SharedMethods.getSymbolFromKey(selectedKey), SharedMethods.getInstrumentTypeFromKey(selectedKey));
            lblSymbolName.setText(st.getLongDescription());
            btnUpDown = new TWButton();
            btnUpDown.setPreferredSize(new Dimension(20, 20));
            btnUpDown.addActionListener(this);

            paneTop.add(Box.createHorizontalStrut(2));
            paneTop.add(lblSymbolName);
            paneTop.add(Box.createHorizontalGlue());
            paneTop.add(btnUpDown);
            paneTop.add(Box.createHorizontalStrut(1));

            if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
                this.getContentPane().add(BorderLayout.NORTH, paneTop);
            }
            this.getContentPane().add(BorderLayout.CENTER, splitPane);

            if (!isChartVisible) {
                btnUpDown.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/expand_chart.gif"));
                bidChart.setVisible(false);
                askChart.setVisible(false);
//                this.setSize(this.getWidth(), this.getHeight() - bidChart.getHeight() - 10);
                askPanel.doLayout();
                bidPanel.doLayout();
            } else {
                btnUpDown.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/collapse_chart.gif"));
                bidChart.setVisible(true);
                askChart.setVisible(true);
//                this.setSize(this.getWidth(), this.getHeight() + bidChart.getHeight() + 10);
                askPanel.doLayout();
                bidPanel.doLayout();
            }
            splitPane.updateUI();
        }
        GUISettings.applyOrientation(this);
        bidTable.updateGUI();
        askTable.getTable().getColumnModel().setSelectionModel(bidTable.getTable().getColumnModel().getSelectionModel());
        askTable.updateGUI();
        this.applySettings();
//        this.setSize(new Dimension(this.getWidth(), 210));
        this.setSplitPane(splitPane);
        this.show();
        splitPane.updateUI();
        bidModel.adjustRows();
        askModel.adjustRows();
        this.setLayer(GUISettings.TOP_LAYER);
        setDecimalPlaces();
        bidChart.setFormatter(stockDecimalPlaces);
        askChart.setFormatter(stockDecimalPlaces);

        if (parentWindowType != Constants.FULL_QUOTE_TYPE) {
            if (loadedFromWorkspace && isLinked) {
                g_oViewSettings.setWindow(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" +
                        LinkStore.linked + "_" + getLinkedGroupID(), this);
                LinkStore.getSharedInstance().addLinkedWindowFromWSP(this);
            } else {
                g_oViewSettings.setWindow(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" +
                        selectedKey, this);
            }
            String requestID = Meta.PRICE_DEPTH + "|" + selectedKey;
            Client.getInstance().addDepthRequest(requestID, selectedKey, Meta.PRICE_DEPTH);
            this.setDataRequestID(DepthStore.getInstance(), selectedKey, requestID);
            if (!loadedFromWorkspace) {
                if (!oBidViewSetting.isLocationValid()) {
                    this.setLocationRelativeTo(Client.getInstance().oTopDesktop);
                    oBidViewSetting.setLocation(this.getLocation());
                } else {
                    this.setLocation(oBidViewSetting.getLocation());
                }
            }
        } else {
//                 g_oViewSettings.setWindow(ViewSettingsManager.DEPTH_PRICE_ASK_VIEW + "|" +
//                        MultiDialogWindow.TABLE_ID, frame);
        }

        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        }

    }

    public void symbolChanged
            (String
                    sKey) {
        selectedKey = sKey;
        String oldKey = oBidViewSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY);
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oldKey))) {
            DataStore.getSharedInstance().removeSymbolRequest(oldKey);

        }

        String oldReq = getDataRequestID();
        RequestManager.getSharedInstance().removeRequest(oldReq);
        if (parentWindowType != Constants.FULL_QUOTE_TYPE) {


            String requestID = Meta.PRICE_DEPTH + "|" + selectedKey;
            Client.getInstance().addDepthRequest(requestID, selectedKey, Meta.PRICE_DEPTH);
            this.setDataRequestID(DepthStore.getInstance(), selectedKey, requestID);

        }
        if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(selectedKey))) {
            DataStore.getSharedInstance().addSymbolRequest(selectedKey);
        }

        if (parentWindowType != Constants.FULL_QUOTE_TYPE) {

            this.setTitle(Language.getString("MARKET_DEPTH_BY_PRICE") + " : (" + SharedMethods.getSymbolFromKey(selectedKey)
                    + ") " + DataStore.getSharedInstance().getCompanyName(selectedKey));
        }
        askModel.setSymbol(selectedKey);
        bidModel.setSymbol(selectedKey);
        askTable.getTable().updateUI();
        bidTable.getTable().updateUI();
        this.updateUI();
        oBidViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);
        oAskViewSetting.putProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY, selectedKey);

        setDecimalPlaces();
        bidChart.setFormatter(stockDecimalPlaces);
        askChart.setFormatter(stockDecimalPlaces);
        Stock st = DataStore.getSharedInstance().getStockObject(SharedMethods.getExchangeFromKey(selectedKey), SharedMethods.getSymbolFromKey(selectedKey), SharedMethods.getInstrumentTypeFromKey(selectedKey));
        if (st != null) {
            lblSymbolName.setText(st.getLongDescription());
        }
    }

    public void setDecimalPlaces
            () {
        try {
            stockDecimalPlaces = SharedMethods.getDecimalPlaces(selectedKey);
        } catch (Exception e) {
            stockDecimalPlaces = 2;
        }
        bidTable.getModel().setDecimalCount(stockDecimalPlaces);
        bidTable.getModel().getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, stockDecimalPlaces);
        askTable.getModel().setDecimalCount(stockDecimalPlaces);
        askTable.getModel().getViewSettings().putProperty(ViewConstants.VC_DECIMAL_PLACES, stockDecimalPlaces);
    }

    public void setSelectedKey
            (String
                    selectedKey) {
        this.selectedKey = selectedKey;
    }

    public void actionPerformed
            (ActionEvent
                    e) {

        super.actionPerformed(e);

        if (e.getSource().equals(btnUpDown)) {
            isChartVisible ^= true;
//            isChartVisible = !isChartVisible;
            oBidViewSetting.putProperty(ViewConstants.VC_BID_ASK_CHART_VISIBLE, isChartVisible);
            if (!isChartVisible) {
                btnUpDown.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/expand_chart.gif"));
                bidChart.setVisible(false);
                askChart.setVisible(false);
                askPanel.doLayout();
                bidPanel.doLayout();
                if (!isDetached()) {
                    this.setSize(this.getWidth(), this.getHeight() - bidChart.getHeight() - VERTICAL_GAP);
                } else {
                    try {
                        JFrame frame = (JFrame) getDetachedFrame();
                        frame.setSize(frame.getWidth(), frame.getHeight() - bidChart.getHeight() - VERTICAL_GAP);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
            } else {
                btnUpDown.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/collapse_chart.gif"));
                bidChart.setVisible(true);
                askChart.setVisible(true);
                askPanel.doLayout();
                bidPanel.doLayout();
                if (!isDetached()) {
                    this.setSize(this.getWidth(), this.getHeight() + bidChart.getHeight() + VERTICAL_GAP);
                } else {
                    try {
                        JFrame frame2 = (JFrame) getDetachedFrame();

                        frame2 = (JFrame) getDetachedFrame();
                        frame2.setSize(frame2.getWidth(), frame2.getHeight() + bidChart.getHeight() + VERTICAL_GAP);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }

            }
            if (parentWindowType == Constants.FULL_QUOTE_TYPE && fullquote != null) {
                fullquote.doLayout();
                fullquote.adjustDetachedFrameSize();
            }
        }
    }

    @Override
    public void internalFrameClosing
            (InternalFrameEvent
                    e) {
        super.internalFrameClosing(e);
        bidChart.removeThread();
        askChart.removeThread();
    }

    @Override
    public void workspaceWillSave
            () {
        oBidViewSetting.putProperty(ViewConstants.VC_BID_ASK_CHART_VISIBLE, isChartVisible);
    }

    @Override
    public void applyTheme
            () {
        //super.applyTheme();

        if (!isChartVisible) {
            btnUpDown.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/expand_chart.gif"));
        } else {
            btnUpDown.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/collapse_chart.gif"));
        }
    }

    public void setFullquote
            (MultiDialogWindow
                    fullquote) {
        this.fullquote = fullquote;
    }
}
