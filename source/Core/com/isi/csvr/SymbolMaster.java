// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr;

/**
 * This class loads the stockmaster, sectors asnd assetclasses
 *  properties files.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Market;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.*;

import javax.swing.*;
import java.io.*;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.StringTokenizer;
import java.util.Properties;

public class SymbolMaster {
    private static Hashtable stockMasterTable;
    private static Hashtable<String, Float> stockMasterVersions;
    private static Hashtable<String, Float> exchagePropertyVersions;
    private static SmartProperties g_oAssetClasses;
    private static SmartProperties g_oCurrencies;
    private static int g_iLanguageID = 0;
    private static Hashtable<String, String> aliasTable;
    public static Hashtable<String, DynamicArray<String>> indexTable;
    public static byte ALIAS_REPLACE = 0;
    public static byte ALIAS_MERGE = 1;

//DEUR68DUBAI EURO-DOLLAR|\u062F\u0628\u064A\u0020\u064A\u0648\u0631\u0648\u002D\u062F\u0648\u0644\u0627\u0631 N/AUSDDEURDEUR|\u0044\u0045\u0055\u0052 1450000.000Euro
    //StockMaster Format
    //  Symbol
    //  Data
    //      0 -> Instrument type
    //      1 -> Symbol Params
    //      2 -> Long Name
    //      3 -> Sector Code
    //      4 -> Currency Code
    //      5 -> Company Code
    //      6 -> Short Name
    //      7 -> market ID
    //      8 -> Price Correction Factor
    //      9 -> Decimal Count
    //     10 -> Lot Size
    //     11 -> Unit 
    //     12 -> English Company description 

    /*
    * Loads the Stock master file
    */

    public static void loadStocks() {
        try {
            g_iLanguageID = Language.getLanguageID();

            //updateStockMaster();
            stockMasterTable = new Hashtable();
            stockMasterVersions = new Hashtable<String, Float>();
            exchagePropertyVersions = new Hashtable<String, Float>();
            indexTable = new Hashtable<String, DynamicArray<String>>();

            Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
            while (exchanges.hasMoreElements()) {
                try {
                    Exchange exchange = (Exchange) exchanges.nextElement();
                    if (exchange.isDefault()) {
                        loadMarketFile(exchange.getSymbol(), false);
                    }
                    loadMarketIndexFile(exchange.getSymbol());
                    loadExgPropertyFile(exchange.getSymbol());
//                    loadExgPropertyFile(exchange.getSymbol());
                    ExchangeStore.getSharedInstance().fireExchangeMasterFileLoaded(exchange.getSymbol());
                    exchange = null;
                } catch (Exception e) {
                    // do nothing. will be downloaded later
                }
            }
            exchanges = null;

            FileInputStream oIn = new FileInputStream(Settings.CONFIG_DATA_PATH + "/ac.dll");
            g_oAssetClasses = new SmartProperties(Meta.DS);
            g_oAssetClasses.load(oIn);
            oIn.close();

            g_oCurrencies = new SmartProperties(Meta.DS);

        } catch (Exception e) {
            Object[] options = {Language.getString("OK")};

            e.printStackTrace();
            JOptionPane.showOptionDialog(null,
                    "Your Client has been disabled. Please contact Customer Support",
                    "Warning",
                    JOptionPane.OK_OPTION,
                    JOptionPane.INFORMATION_MESSAGE,
                    null,
                    options,
                    options[0]);
            System.exit(0);
        }
//        try {
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/alias.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            aliasTable = (Hashtable<String, String>) oObjIn.readObject();
            oIn.close();
        } catch (Exception e) {
            aliasTable = new Hashtable<String, String>();
            e.printStackTrace();
        }
//
//            aliasTable = new Hashtable<String, String>();
//            SmartProperties stockMaster = new SmartProperties(Meta.DS);
//            stockMaster.loadCompressed(".\\datastore\\alias.mdf");
//            Enumeration keys = stockMaster.keys();
//            String symbol;
//            while(keys.hasMoreElements()){
//                symbol = (String)keys.nextElement();
//                aliasTable.put(symbol,stockMaster.getProperty(symbol));
//                symbol = null;
//            }
//            keys = null;
//            stockMaster = null;
//        } catch (Exception e) {
//            e.printStackTrace();
//            aliasTable = new Hashtable<String, String>();
//        }
    }

    public static void mergeAlias() {
        System.out.println("MERGE............................");
        Hashtable<String, String> temp = new Hashtable<String, String>();
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/alias.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            temp = (Hashtable<String, String>) oObjIn.readObject();
            oIn.close();


        } catch (Exception e) {
            aliasTable = new Hashtable<String, String>();
            e.printStackTrace();
            System.out.println("Merging alias .....Failed");
        }

        Enumeration<String> symbols = DataStore.getSharedInstance().getAllSymbols();
//        String symbol;
        String key;
//        int instrument;
        String alias = null;
        try {

            String symbolMasterAlias = null;
            String symbolAlias = null;
            alias = null;
            while (symbols.hasMoreElements()) {
                try {
                    key = symbols.nextElement();
                    if (temp.containsKey(key)) {
                        if (aliasTable.containsKey(key)) {
                            aliasTable.put(key, temp.get(key));
                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            symbols = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
        updateAlias();
    }


    public static boolean isMarketLoaded(String exchange) {
        return ((stockMasterVersions.get(exchange)) != null);
    }

    public static boolean isExchangPropsLoaded(String exchange) {
        if (exchange.equals(Constants.APPLICATION_EXCHANGE)) { // for the application exchange, it should be true ..
            return true;                                         //   allways to avoid download requests
        } else {
            return ((exchagePropertyVersions.get(exchange)) != null);
        }
    }

    public static void unloadMarket(String exchange) {
        try {
            stockMasterTable.remove(exchange);
            stockMasterVersions.remove(exchange);
//            marketNames.remove(exchange);
//            marketTimeZones.remove(exchange);
//            marketCurruncies.remove(exchange);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void unloadExchangeProperties(String exchange) {
        try {
            exchagePropertyVersions.remove(exchange);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void loadMarketFile(String exchange, boolean getDefault) throws Exception {
        SmartProperties stockMaster = new SmartProperties(Meta.DS);
        String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/market_" + Language.getSelectedLanguage() + ".msf";
        File f = new File(sLangSpecFileName);
//        boolean bIsLangSpec = f.exists();
//        if (getDefault) {
        stockMaster.loadCompressed(sLangSpecFileName);
//        } else if(getDefault){
//            try {
//                stockMaster.loadCompressed("./system/" + exchange + "/market.msf");
//            } catch (IOException e) {
////                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            }
//        }
        stockMasterTable.put(exchange, stockMaster);
        stockMaster = null;
        float version = -1;
        if ((version = readVersion(exchange)) != 0) {
            stockMasterVersions.put(exchange, version);
        }
    }

    public static void loadExgPropertyFile(String exchange) throws Exception {
        try {
            //Properties stockMaster       = new Properties();
            DataInputStream oIn = new DataInputStream(new FileInputStream(Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/version_" + Language.getSelectedLanguage() + ".dll"));
            exchagePropertyVersions.put(exchange, Float.parseFloat(oIn.readLine()));
            oIn.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        //================================================================================
//        try {
//            SmartProperties version           = new SmartProperties(Meta.DS);
//            String          sLangSpecFileName = "system/"+exchange + "/version_" + Language.getSelectedLanguage() + ".msf";
//            File            f                 = new File(sLangSpecFileName);
//            boolean         bIsLangSpec       = f.exists();
//            if (bIsLangSpec) {
//                version.loadCompressed(sLangSpecFileName);
//            }
//            exgVersions.put(exchange, Float.parseFloat(version.getProperty("version")));
//        } catch (IOException e) {
//        } catch (NumberFormatException e) {
//        }
        //================================================================================
    }

    public static void loadMarketIndexFile(String exchange) throws Exception {
        try {
            if (ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()) {
                String key = null;
                String value = null;
                SmartProperties indexMaster = new SmartProperties(Meta.DS);
                String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/index_" + Language.getSelectedLanguage() + ".msf";
                File f = new File(sLangSpecFileName);
                boolean bIsLangSpec = f.exists();
                if (bIsLangSpec) {
                    indexMaster.loadCompressed(sLangSpecFileName);
                }
                DynamicArray<String> indexes;
                if (!stockMasterTable.containsKey(exchange)) {
                    stockMasterTable.put(exchange, indexMaster);
                }
                indexes = new DynamicArray<String>();
                Enumeration indexEnum = indexMaster.keys();
                while (indexEnum.hasMoreElements()) {
                    key = (String) (indexEnum.nextElement());
                    value = (String) indexMaster.get(key);
                    if ((key != null) && (value != null)) {
                        ((SmartProperties) stockMasterTable.get(exchange)).put(key, value);
                        indexes.add(key);
                    }
                }
                indexTable.put(exchange, indexes);
            }
        } catch (IOException e) {
            System.out.println("exception exchange : " + exchange);
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public static String getIndexValue(String exchange, String key) {
        return (String) ((SmartProperties) stockMasterTable.get(exchange)).get(key);
    }

    public static void removeExchange(String exchange) {
        // Remove the stock master file
        try {
            String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/market_" + Language.getSelectedLanguage() + ".msf";
            File file = new File(sLangSpecFileName);
            boolean bIsLangSpec = file.exists();
            if (!bIsLangSpec) {
                file = new File(Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/market.msf");
            }
//            File file = new File( "./system/" + exchange + "/market.msf"); // delete the file
            boolean sucess = file.delete();
            if (!sucess) {
                file.deleteOnExit();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
//        try {
//            File file = new File( "./system/" + exchange); // delete the folder
//            boolean sucess = file.delete();
//            if (!sucess) {
//                file.deleteOnExit();
//            }
//        } catch (Exception e) {
//            e.printStackTrace();
//        }

        unloadMarket(exchange); // remove symbol references from the memory
        try {
            loadMarketIndexFile(exchange);
        } catch (Exception e) {
        }
    }

    public static String getExchangeForSymbol(String symbol) {
        return getExchangeForSymbol(symbol, true);
    }

    public static String getExchangeForSymbol(String symbol, boolean returnExchangeOnly) {
        if (!returnExchangeOnly) {
            return DataStore.getSharedInstance().findKeyFromDataStore(symbol);
        } else {
            try {
                StringBuilder buffer = new StringBuilder();
                Enumeration tables = stockMasterTable.keys();
                String exchangeCode = null;
                Exchange exchange = null;
                String lastSelectedSymbol = null;
                String lastSelectedExchange = null;
                String lastSelectedMarket = null;
                int lastSelectedInstrument = -1;
                int count = 0;
                while (tables.hasMoreElements()) {
                    exchangeCode = (String) tables.nextElement();
                    exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
                    if ((exchange.getSubMarketCount() == 0) || symbol.contains(Constants.MARKET_SEPERATOR_CHARACTER)) {
                        String record = ((SmartProperties) stockMasterTable.get(exchangeCode)).getProperty(symbol);
                        if (record != null) {
                            count++;
                            lastSelectedExchange = exchangeCode;
                            lastSelectedSymbol = symbol;
                            lastSelectedInstrument = getInstrumentType(symbol, exchange.getSymbol());
                            buffer.append(exchange);
                            buffer.append(Meta.ID);
                            buffer.append(symbol);
                            buffer.append(Meta.ID);
                            buffer.append(getCompanyName(symbol, exchangeCode));
                            buffer.append(Meta.ID);
                            buffer.append(lastSelectedInstrument);
                            buffer.append(Meta.ID);
                            buffer.append(Meta.FD);
                        }
                        record = null;
                    } else {
                        Market[] markets = exchange.getSubMarkets();
                        for (Market market : markets) {
                            String record = null;
                            if ((market.getSymbol() == null) || (market.getSymbol().trim().equals(""))) { // no duplicate symbols in sub markets
                                record = ((SmartProperties) stockMasterTable.get(exchangeCode)).getProperty(symbol);
                                if (record != null) {
                                    count++;
                                    lastSelectedExchange = exchangeCode;
                                    lastSelectedSymbol = symbol;
                                    lastSelectedInstrument = getInstrumentType(symbol, exchange.getSymbol());
                                    buffer.append(exchange);
                                    buffer.append(Meta.ID);
                                    buffer.append(symbol);
                                    buffer.append(Meta.ID);
                                    buffer.append(getCompanyName(symbol, exchangeCode));
                                    buffer.append(Meta.ID);
                                    buffer.append(lastSelectedInstrument);
                                    buffer.append(Meta.ID);
                                    buffer.append(Meta.FD);
                                    break;
                                }
                                record = null;
                            } else {
                                record = ((SmartProperties) stockMasterTable.get(exchangeCode)).getProperty(symbol + Constants.MARKET_SEPERATOR_CHARACTER + market.getSymbol());
                                if (record != null) {
                                    count++;
                                    lastSelectedExchange = exchangeCode;
                                    lastSelectedSymbol = symbol;
                                    lastSelectedMarket = market.getSymbol();
                                    lastSelectedInstrument = getInstrumentType(symbol, exchange.getSymbol());
                                    buffer.append(exchange);
                                    buffer.append(Meta.ID);
                                    buffer.append(symbol + Constants.MARKET_SEPERATOR_CHARACTER + market.getSymbol());
                                    buffer.append(Meta.ID);
                                    buffer.append(getCompanyName(symbol + Constants.MARKET_SEPERATOR_CHARACTER + market.getSymbol(), exchangeCode) + " - " + market.getDescription());
                                    buffer.append(Meta.ID);
                                    buffer.append(lastSelectedInstrument);
                                    buffer.append(Meta.ID);
                                    buffer.append(Meta.FD);
                                }
                                record = null;
                            }
                        }
                    }
                }

                if (count > 1) {
                    if (buffer.toString().length() > 0) {
                        ValidatedCompanySelector companySelector = new ValidatedCompanySelector(buffer.toString(), ValidatedCompanySelector.MODE_INTERNAL_VALIDATION);
                        if (returnExchangeOnly) {
                            String record = companySelector.getSelectedRecord();

                                return SharedMethods.getExchangeFromKey(companySelector.getSelectedRecord());

                        } else {
                            
                            return companySelector.getSelectedRecord();
                            
                        }
                    } else {
                        return null;
                    }
                } else if (count == 1) {
                    if (returnExchangeOnly) {
                        return lastSelectedExchange;
                    } else {
                        if (lastSelectedMarket == null) {
                            return SharedMethods.getKey(lastSelectedExchange, lastSelectedSymbol, lastSelectedInstrument);
                        } else {
                            return SharedMethods.getKey(lastSelectedExchange, lastSelectedSymbol + Constants.MARKET_SEPERATOR_CHARACTER + lastSelectedMarket, lastSelectedInstrument);
                        }
                    }
                } else {
                    return null;
                }
            } catch (Exception e) {
                return null;
            }
        }
    }
    public static String getFirstSubStartDate(String symbol, String exchange){
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            if (sValue == null) return null;
            String[] fields = sValue.split(Meta.FS);
            return fields[18];
        } catch (Exception e) {
            return null;
        }
    }

    public static String getFirstSubEndDate(String symbol, String exchange){
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            if (sValue == null) return null;
            String[] fields = sValue.split(Meta.FS);
            return fields[19];
        } catch (Exception e) {
            return null;
        }
    }

    public static String getSecondSubStartDate(String symbol, String exchange){
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            if (sValue == null) return null;
            String[] fields = sValue.split(Meta.FS);
            return fields[20];
        } catch (Exception e) {
            return null;
        }
    }

    public static String getSecondSubEndDate(String symbol, String exchange){
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            if (sValue == null) return null;
            String[] fields = sValue.split(Meta.FS);
            return fields[21];
        } catch (Exception e) {
            return null;
        }
    }

    public static String getMarketID(String symbol, String exchange) {
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            if (sValue == null) return null;
            String[] fields = sValue.split(Meta.FS);
            return fields[7];
        } catch (Exception e) {
            return Constants.DEFAULT_MARKET;
        }
    }

    public static int getPriceCorrectionFactor(String symbol, String exchange) {
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            if (sValue == null) return Constants.DEFAULT_PRICE_CORRECTION_FACTOR;
            String[] fields = sValue.split(Meta.FS);
            return Integer.parseInt(fields[8]);
        } catch (Exception e) {
            return Constants.DEFAULT_PRICE_CORRECTION_FACTOR;
        }
    }

    public static byte getDecimalPlaces(String symbol, String exchange) {
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            if (sValue == null) return Constants.DEFAULT_DECIMAL_COUNT;
//            if (sValue == null) return Constants.DEFAULT_PRICE_CORRECTION_FACTOR;  //commented by chandika
            String[] fields = sValue.split(Meta.FS);
            return Byte.parseByte(fields[9]);
        } catch (Exception e) {
            return Constants.TWO_DECIMAL_PLACES;
        }
    }

    public static double getLotSize(String symbol, String exchange) {
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            String[] fields = sValue.split(Meta.FS);
            return Double.parseDouble(fields[10]);
        } catch (Exception e) {
            return 1;
        }
    }

    public static String getUnit(String symbol, String exchange) {
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            String[] fields = sValue.split(Meta.FS);
            return fields[11];
        } catch (Exception e) {
            return "";
        }
    }

    /**
     * Unloads the stockmaster from the memory
     */
    public static void unloadStocks() {
        // this must be commentd for the use of MetaStock
//    	g_oStockMaster.clear();
//		g_oStockMaster= null;
    }

    /*
    * Copies the new stock master version from the dump directory
    */
    /*public static void updateStockMaster() //throws Exception
    {
        try {
            /* Check if a new stock master version is avalable*
            if (!Settings.isNewStockMasterAvailable())
                return;

            int i = 0;
            byte bytData[] = new byte[500];

            FileInputStream oIn = new FileInputStream(Settings.TEMP_PATH + "\\sm.dll");

            ZipInputStream oZIn = new ZipInputStream(oIn);
            ZipEntry oEntry;

            while (true) {
                oEntry = oZIn.getNextEntry();
                if (oEntry == null) break;

                File f = new File(oEntry.getName());
                FileOutputStream oOut = new FileOutputStream(Settings.SYSTEM_PATH + f.getName());

                while (true) {
                    i = oZIn.read(bytData);
                    if (i == -1)
                        break;
                    oOut.write(bytData, 0, i);
                }
                oZIn.closeEntry();
                oOut.close();
            }

            oZIn.close();
            Settings.setInvlidateNewStockMaster();

        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    /**
     * Returns the list of keys as an enumeration
     */
    public static Enumeration getSymbols(String exchange) {
        return ((SmartProperties) stockMasterTable.get(exchange)).keys();
    }

    /**
     * Returns size of the symbol map
     */
    public static int getSymbolCount(String exchange) {
        try {
            return ((SmartProperties) stockMasterTable.get(exchange)).size();
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Returns the company name for the given key
     */
    public static String getCompanyName(String symbol, String exchange) {
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            String sName = getLongName(sValue);
            if (sName != null)
                return sName.trim();
            else
                return Language.getString("NA");
        } catch (Exception e) {
//            System.out.println("exception in getCompanycode in  SymbolMaster symbol=" + symbol + "~" + exchange);
            return Language.getString("NA");

        }
    }

    /**
     * Returns the Alias for the given key
     */
    /*public static String getAlias(String symbol, String exchange) {
        try {
            String sName = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(aliasTable.get(SharedMethods.getKey(symbol,exchange))));
            if (sName != null)
                return sName.trim();
            else
                return null;
        } catch (Exception e) {
//            System.out.println("exception in getCompanycode in  SymbolMaster symbol="+symbol+"~"+exchange);
            return null;

        }
    }*/
    public static boolean isValidSymbol(String exchange, String symbol) {
        return ((SmartProperties) stockMasterTable.get(exchange)).containsKey(symbol);
    }

    /**
     * Returns the company name for the given key
     */
    public static String getCompanyCode(String symbol, String exchange) {
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);

            if (sValue == null) return Language.getString("NA");

            String[] fields = sValue.split(Meta.FS);
            String sCode = Language.getString("NA");


            sCode = fields[5]; // COMPANY CODE
            fields = null;
            sValue = null;
            return sCode;
        } catch (Exception e) {
            //e.printStackTrace();
            return Language.getString("NA");
        }
    }

    /**
     * Returns the company name for the given key
     */
    public static String getCurrencyCode(String symbol, String exchange) {
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);

            if (sValue == null) return Language.getString("NA");

            String[] fields = sValue.split(Meta.FS);
            String sCode = Language.getString("NA");

            sCode = fields[4]; // CURRENCY CODE
            fields = null;
            sValue = null;

            // this is a W/A for for the "N/A" prob in the DB must be fixed
            if (sCode.equals("N/A"))
                return Constants.DEFAULT_CURRENCY_CODE;
            else
                return sCode;
        } catch (Exception e) {
            //e.printStackTrace();
            return Constants.DEFAULT_CURRENCY_CODE;
        }
    }

    public static int getInstrumentType(String symbol, String exchange) {
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);

            if (sValue == null) return Meta.INSTRUMENT_EQUITY; // defult

            String[] fields = sValue.split(Meta.FS);
            int type = Integer.parseInt(fields[0]);
            //String category = st.nextToken();
            //int returnType;

            /*if (type.equals("I")) { //Indicator
                returnType = ESPConstants.GROUP_INDCATOR;
                if (category.indexOf('S') >= 0) { // sector
                    returnType |= ESPConstants.TYPE_SECTOR;
                }
                if (category.indexOf('I') >= 0) { // Index
                    returnType |= ESPConstants.TYPE_INDEX;
                }
                if (category.indexOf('M') >= 0) { // Market Index
                    returnType |= ESPConstants.TYPE_MAIN_INDEX;
                }
            } else { // Stock
                returnType = ESPConstants.GROUP_STOCK;
                if (category.indexOf('E') >= 0) { // Equiry
                    returnType |= ESPConstants.TYPE_EQUITY;
                }
                if (category.indexOf('B') >= 0) { // Bond
                    returnType |= ESPConstants.TYPE_BOND;
                }
                if (category.indexOf('M') >= 0) { // Mutual Fund
                    returnType |= ESPConstants.TYPE_MUTUAL_FUND;
                }
            }*/

//            type = null;
//            category = null;
            fields = null;

            return type;
        } catch (Exception e) {
            return 0;
            //return ESPConstants.GROUP_STOCK | ESPConstants.TYPE_EQUITY; // defult
        }

    }

    public static String getSymbolParams(String symbol, String exchange) {
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);

            if (sValue == null) return null; // defult

            String[] fields = sValue.split(Meta.FS);
            //String category = st.nextToken();
            //int returnType;

            /*if (type.equals("I")) { //Indicator
                returnType = ESPConstants.GROUP_INDCATOR;
                if (category.indexOf('S') >= 0) { // sector
                    returnType |= ESPConstants.TYPE_SECTOR;
                }
                if (category.indexOf('I') >= 0) { // Index
                    returnType |= ESPConstants.TYPE_INDEX;
                }
                if (category.indexOf('M') >= 0) { // Market Index
                    returnType |= ESPConstants.TYPE_MAIN_INDEX;
                }
            } else { // Stock
                returnType = ESPConstants.GROUP_STOCK;
                if (category.indexOf('E') >= 0) { // Equiry
                    returnType |= ESPConstants.TYPE_EQUITY;
                }
                if (category.indexOf('B') >= 0) { // Bond
                    returnType |= ESPConstants.TYPE_BOND;
                }
                if (category.indexOf('M') >= 0) { // Mutual Fund
                    returnType |= ESPConstants.TYPE_MUTUAL_FUND;
                }
            }*/

//            type = null;
//            category = null;
//            fields = null;

            return fields[1];
        } catch (Exception e) {
            return null;
            //return ESPConstants.GROUP_STOCK | ESPConstants.TYPE_EQUITY; // defult
        }

    }


    /**
     * Returns the asset classs for the symbol
     */
    public static String getAssetClass(String symbol, String exchange) {
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            String[] fields = sValue.split(Meta.FS);
            sValue = fields[1];
            return sValue;
        } catch (Exception e) {
            return Constants.DEFAULT_ASSETCLASS;
        }
    }

    /**
     * Returns the Sector Code for the symbol
     */
    public static String getSectorCode(String symbol, String exchange) {
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            String[] fields = sValue.split(Meta.FS);

            return fields[3];
        } catch (Exception e) {
            return Constants.DEFAULT_SECTOR;
        }
    }

    /**
     * Returns the short description for the given key
     */
    public static String getShortDescription(String symbol, String exchange) {
        try {
            String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            return getShortName(sValue);
        } catch (Exception e) {
//            return Language.getString("NA"); changed 5 dec 2005 Uditha
            return symbol;
        }
    }

    /**
     * Returns the Long Name of the company
     */
    private static String getLongName(String sItem) {
        if (sItem == null) return Language.getString("NA");
        //StringTokenizer oST = new StringTokenizer(sItem, Meta.FS);
        String[] fields = sItem.split(Meta.FS);
        String sNames = "";

        try {
            g_iLanguageID = Language.getLanguageID();
            /* select the language specific component */
//            SharedMethods.printLine(sItem);
            sNames = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(fields[2]));
//            sNames = Language.getLanguageSpecificString(fields[2]);
            fields = null;
        } catch (Exception e) {
            e.printStackTrace();
            sNames = sItem;
        }

        return sNames;
    }

    /**
     * Returns the stockmaster version
     */
    public static float readVersion(String exchange) {

        try {
            String sVersion = (String) ((SmartProperties) stockMasterTable.get(exchange)).get("VERSION");
            ((SmartProperties) stockMasterTable.get(exchange)).remove("VERSION");
            if (sVersion == null)
                return 0;
            else
                return Float.parseFloat(sVersion);
        } catch (NumberFormatException e) {
            return 0;
        }
    }

    /**
     * Returns the exchange names
     */
    public static String readName(String exchange) {

        String name = (String) ((SmartProperties) stockMasterTable.get(exchange)).get("NAME");
        ((SmartProperties) stockMasterTable.get(exchange)).remove("NAME");
        if (name == null)
            return exchange;
        else
            return Language.getLanguageSpecificString(name, "|");
    }

    /**
     * Returns the exchange names
     */
    public static String readTimeZone(String exchange) {

        String name = (String) ((SmartProperties) stockMasterTable.get(exchange)).get("TIME_ZONE");
        ((SmartProperties) stockMasterTable.get(exchange)).remove("TIME_ZONE");
        if (name == null)
            return Language.getString("NA");
        else
            return name;
    }

    /**
     * Returns the currencies names
     */
    public static String readCurrency(String exchange) {

        String name = (String) ((SmartProperties) stockMasterTable.get(exchange)).get("CURRENCY");
        ((SmartProperties) stockMasterTable.get(exchange)).remove("CURRENCY");
        if (name == null)
            return Language.getString("NA");
        else
            return name;
    }

    /**
     * Returns the stockmaster version
     */
    public static float getVersion(String exchange) {
        try {
            return stockMasterVersions.get(exchange);
        } catch (Exception e) {
            //e.printStackTrace();
            return 0;
        }
    }

    public static float getExgPropertyversion(String exchange) {
        try {
            return exchagePropertyVersions.get(exchange);
        } catch (Exception e) {
            //e.printStackTrace();
            return 0;
        }
    }
    /**
     * Returns the exchange name
     */
//    public static String getMarketName(String exchange) {
//        if (exchange.equals(Constants.APPLICATION_EXCHANGE)) {
//            return Language.getString("NA");
//        } else {
//            return (String) marketNames.get(exchange);
//        }
//    }

    /**
     * Returns the Short Name of the company
     */
    private static String getShortName(String sItem) {
        if (sItem == null) return Language.getString("NA");
        String[] fields = sItem.split(Meta.FS);
        String sNames = "";

        try {
            g_iLanguageID = Language.getLanguageID();
            sNames = Language.getLanguageSpecificString(UnicodeUtils.getNativeString(fields[6]));
            fields = null;
        } catch (Exception e) {
            sNames = Constants.NULL_STRING;
        }

        return sNames;
    }

    /* --------- Asset Classes ---------- */

    /**
     * Returns the Asset classes list
     */
    public static String[] getAssetClasses() {
        String[] saClasses = new String[g_oAssetClasses.size()];
        int i = 0;
        String sKey = null;
        String sValue = null;

        Enumeration oKeys = g_oAssetClasses.keys();

        while (oKeys.hasMoreElements()) {
            sKey = (String) oKeys.nextElement();
            if (sKey.equals("999")) continue; // primary market
            sValue = getAssetClassCaption(sKey);
            if (sValue != null) {
                saClasses[i] = sValue + Meta.FS + sKey;
                i++;
            }
        }

        String[] saSelected = new String[i];
        System.arraycopy(saClasses, 0, saSelected, 0, i);

        return saSelected;
    }

    /**
     * Returns the asset class id list
     */
    public static String[] getAssetClassIDs() {
        String[] saClasses = new String[g_oAssetClasses.size()];
        int i = 0;

        Enumeration oKeys = g_oAssetClasses.keys();

        while (oKeys.hasMoreElements()) {
            saClasses[i] = (String) oKeys.nextElement();
            i++;
        }

        return saClasses;
    }

    /**
     * Returns the Asset classes Caption
     */
    public static String getAssetClassCaption(String sKey) {
        String sValue = null;

        sValue = getAssetClassName((String) g_oAssetClasses.get(sKey));
        return sValue;
    }

//    public static String getZoneDescription(String id) {
//        try {
//            return (String) marketTimeZones.get(id);
//        } catch (Exception e) {
//            return id;
//        }
//    }
//
//    public static String getCurrencyData(String exchange){
//        return marketCurruncies.get(exchange);
//    }

    /**
     * Extracts the asset class name for the selected language
     */
    private static String getAssetClassName(String sItem) {
        int i = 0;
        String sName = null;

        StringTokenizer oST = new StringTokenizer(sItem, Meta.FS);

        try {
            g_iLanguageID = Language.getLanguageID();
            while (i < g_iLanguageID) {
                oST.nextToken();
                i++;
            }
            sName = oST.nextToken();
        } catch (Exception e) {
            sName = null;
        }

        return sName;
    }

    /* --------- Currencies ---------- */

    /**
     * Returns the Currency classes list
     */
    public static String[] getCurrencies() {
        String[] saCurrencies = new String[g_oCurrencies.size()];
        int i = 0;
        String sKey = null;
        String sValue = null;

        Enumeration oKeys = g_oCurrencies.keys();

        while (oKeys.hasMoreElements()) {
            sKey = (String) oKeys.nextElement();
            sValue = getCurrencyCaption(sKey);
            //sValue =  (String)g_oCurrencies.get(sKey);
            if (sValue != null) {

                saCurrencies[i] = sValue + Meta.FS + sKey;
                i++;
            }
        }

        String[] saSelected = new String[i];
        System.arraycopy(saCurrencies, 0, saSelected, 0, i);

        return saCurrencies;
    }

    /**
     * Returns the Long Name of the company
     */
    public static String getEnglishCompanyName(String key) {

        try {
            String symbol = SharedMethods.getSymbolFromKey(key);
            String exchange = SharedMethods.getExchangeFromKey(key);
            if (Language.getLanguageTag().equals("EN")) {
                return getCompanyName(symbol, exchange);
            } else {
                String sValue = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
                String[] fields = sValue.split(Meta.FS);
                return fields[12];
            }
        } catch (Exception e) {
            return null;
        }
        /*String symbol = null;
        String[] fields = new String[0];
        String sNames = null;
        try {
            if (key == null) return Language.getString("NA");
            symbol = SharedMethods.getSymbolFromKey(key);
            String exchange = SharedMethods.getExchangeFromKey(key);
            String sItem = (String) ((SmartProperties) stockMasterTable.get(exchange)).get(symbol);
            fields = sItem.split(Meta.FS);
            sNames = "";
        } catch (Exception e) {
            return SharedMethods.getSymbolFromKey(key);
        }

        try {
            sNames = Language.getEnglishString(fields[2]);
            fields = null;
            return sNames;
        } catch (Exception e) {
            e.printStackTrace();
            return symbol;
        } finally {
            sNames = null;
        }*/

    }

    /**
     * Returns the asset class id list
     */
    public static String[] getCurrencyIDs() {
        String[] saCurrencies = new String[g_oCurrencies.size()];
        int i = 0;
        Enumeration oKeys = g_oCurrencies.keys();

        while (oKeys.hasMoreElements()) {
            saCurrencies[i] = (String) oKeys.nextElement();
            i++;
        }

        return saCurrencies;
    }

    /**
     * Returns the Currency Caption
     */
    public static String getCurrencyCaption(String key) {
        String sValue = null;

        Enumeration oKeys = g_oCurrencies.keys();

        sValue = Language.getLanguageSpecificString(g_oCurrencies.getProperty(key), Meta.FS);

        return sValue;
    }

    public static void saveSymbolAlias() {
        try {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath() + "datastore/alias.mdf");
            ObjectOutputStream oObjOut = new ObjectOutputStream(out);
            oObjOut.writeObject(aliasTable);
            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void removeAlias(String key) {
        aliasTable.remove(key);
    }

    public static void addAlias(String key, String value) {
        aliasTable.put(key, value);
    }

    public static String getAlias(String symbol, String exchange, int instrument) {
        try {
            String sName = (aliasTable.get(SharedMethods.getKey(exchange, symbol, instrument)));
            if (sName != null) {
                return sName.trim();
            } else {
                return null;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static void updateAlias() {
        //updating the Stock object alias
        Enumeration<String> keys = aliasTable.keys();
        String symbol;
        while (keys.hasMoreElements()) {
            try {
                symbol = keys.nextElement();
                DataStore.getSharedInstance().getStockObject(symbol).setAlias(aliasTable.get(symbol));
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public static void mergeAlias(byte mergeType) {

        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/alias.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            Hashtable<String, String> temp = (Hashtable<String, String>) oObjIn.readObject();
            oIn.close();
            Enumeration<String> keys = temp.keys();
            String symbol;
            String aliasSymbol;
            while (keys.hasMoreElements()) {
                try {
                    symbol = keys.nextElement();
                    aliasSymbol = aliasTable.get(symbol);
                    if (aliasSymbol == null) {
                        aliasTable.put(symbol, temp.get(symbol));
                        DataStore.getSharedInstance().getStockObject(symbol).setAlias(aliasTable.get(symbol));
                    } else if (mergeType == ALIAS_REPLACE) {
                        aliasTable.put(symbol, temp.get(symbol));
                        DataStore.getSharedInstance().getStockObject(symbol).setAlias(aliasTable.get(symbol));
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            System.out.println("MERGE............................");
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println("Merging alias .....Failed");
        }
//		saveSymbolAlias();
    }

    public static void loadAlias() {
        System.out.println("RESTORE..........");
        try {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath() + "datastore/alias.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            aliasTable = (Hashtable<String, String>) oObjIn.readObject();
            oIn.close();
        } catch (Exception e) {
            aliasTable = new Hashtable<String, String>();
            e.printStackTrace();
            System.out.println("Loading alias .....Failed");
        }
//		updateAlias();
    }


    public static SmartProperties loadNonDefaultMarketFile(String exchange, boolean getDefault) throws Exception {

        SmartProperties stockMaster = new SmartProperties(Meta.DS);
//        String sLangSpecFileName =  Settings.CONFIG_DATA_PATH + "/market_" + Language.getSelectedLanguage() + ".msf";

//        String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/market_" + Language.getSelectedLanguage() + ".msf";
        String sLangSpecFileName;
        sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/market_" + Language.getSelectedLanguage() + ".msf";
        File f = new File(sLangSpecFileName);
        stockMaster.loadCompressed(sLangSpecFileName);

        return stockMaster;
    }

    public static int getStockMasterVersionForNonDef(String exchange) {
        int fileVersion = 0;
        String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/market_" + Language.getSelectedLanguage() + ".csv";

        FileInputStream fstream = null;
        try {
            fstream = new FileInputStream(sLangSpecFileName);
        } catch (FileNotFoundException e) {
            return 0;

        }

        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
//Read File Line By Line

        try {
            while ((strLine = br.readLine()) != null) {
//                                    String ssymbol = (String) exchageSymbols.nextElement();
                String ssymbol = strLine;
                String symbol;
                String insType;

                String iType = ((String) ssymbol).split(Meta.FS)[0];
                symbol = iType.split(Meta.DS)[0];

                if (symbol.equals("VERSION")) {
                    fileVersion = Integer.parseInt(iType.split(Meta.DS)[1]);
                    break;
                }
            }
        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return fileVersion;
    }
}


