package com.isi.csvr.vas;

import com.isi.csvr.Client;
import com.isi.csvr.TWMenu;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.TWSeparator;
import com.isi.csvr.ie.BrowserManager;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.VASMenuListener;
import com.isi.csvr.shared.*;
import com.isi.csvr.shared.nanoxml.XMLElement;
import com.isi.csvr.util.Encripter;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.IOException;
import java.io.InputStream;
import java.io.ByteArrayOutputStream;
import java.io.FileInputStream;
import java.net.*;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.zip.DeflaterInputStream;
import java.util.zip.GZIPInputStream;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 15, 2006
 * Time: 4:28:41 PM
 */
public class MenuLoader implements Runnable {

    private static final int MAX_DOWNLOAD_COUNT = 5;

    private StringBuilder buffer;
    private ArrayList<JComponent> links;
    private ArrayList<JComponent> menus;
    private ArrayList<VASMenuListener> listeners;
    private static boolean inProgress;
    private static boolean downloaded;
    //hashtable to store data in VasMenu.xml
    private Hashtable<String, ArrayList> vasData;
    private String rsaKey = "";

    private static MenuLoader self;
    String instrumnetTypes[] = null;
    private MenuLoader() {
        links = new ArrayList<JComponent>();
        menus = new ArrayList<JComponent>();
        listeners = new ArrayList<VASMenuListener>();
        vasData = new Hashtable<String, ArrayList>();
    }

    public static MenuLoader getSharedInstance(){
        if(self == null){
            self = new MenuLoader();
        }
        return self;
    }

    public synchronized void start() {
        inProgress = true;
        Thread thread = new Thread(this, "VAS Menu Loader");
        thread.start();
    }

    public static synchronized boolean isInProgress() {
        return inProgress;
    }

    public static boolean isDownloaded() {
        return downloaded;
    }

    public void run() {
        try {
            boolean downloaded = downloadXML();
//            boolean downloaded = readXML();   //only for testing
            if (downloaded) {
                analyseMenu();
                filreVASDataDownloadSucess();
            } else {
                filreVASDataDownloadFailed();
            }
        } catch (Exception e) {
            filreVASDataDownloadFailed();
            e.printStackTrace();
        }
    }

    public void addListener(VASMenuListener listener) {
        listeners.add(listener);
    }

//    private boolean downloadXML() throws Exception {
//        int downloadCount = 0;
//
//        while (downloadCount < MAX_DOWNLOAD_COUNT) {
//            try {
//                buffer = new StringBuilder();
//                String urlString = VASSettings.getXMLUrl();
//                urlString = formatURL(urlString);
//                urlString = urlString.replaceFirst("\\[SSO\\]", Settings.RSA_SSO_CODE);
//                urlString = urlString.replaceFirst("\\[LANGUAGE\\]", Language.getLanguageTag());
//
//                /*String authLink = VASSettings.getAuthenticationPage();
//                authLink = authLink.replaceFirst("\\[SID\\]", Settings.getSessionID());
//                authLink = authLink.replaceFirst("\\[URL\\]", url);
//                authLink = authLink.replaceFirst("\\[LID\\]", Settings.getUserID());   // Settings.getUserID());
//                authLink = authLink.replaceFirst("\\[LANGUAGE\\]", Language.getLanguageTag());*/
////**************************************************************************************************************************
//                URLConnection connection;
////                if (BrowserManager.getInstance().isProxyAvailable()) { // browser proxy available
////                    URL url = new URL(urlString);
////                    connection = url.openConnection(new Proxy(Proxy.Type.HTTP, new InetSocketAddress(BrowserManager.getInstance().getProxyIP(),
////                            BrowserManager.getInstance().getProxyPort())));
////                    System.out.println("VAS Connecting through Proxy " + BrowserManager.getInstance().getProxyIP() + " " +
////                            BrowserManager.getInstance().getProxyPort());
////                } else {
//                    URL url = new URL(urlString);
//                    connection = url.openConnection();
//                    System.out.println("VAS no proxy");
//               // }
//                InputStream in = connection.getInputStream();
////**************************************************************************************************************************
////                InputStream in = SharedMethods.getURLConnection(urlString).getInputStream(); //todo:newly added
//
////                InputStream in = new FileInputStream("vasmenu.xml");
//                byte[] data = new byte[100];
//
//                while (true) {
//                    int i = in.read(data);
//                    if (i == -1) break;
//
//                    buffer.append(new String(data, 0, i));
//                }
//                downloaded = true;
//                /*FileOutputStream out = new FileOutputStream("Vas.xml");
//                out.write(buffer.toString().getBytes());
//                out.flush();
//                out.close();*/
//                return true;
//            } catch (Exception e) {
//                downloadCount++;
//                e.printStackTrace();
//            }
//            try {
//                Thread.sleep(5000);
//            } catch (InterruptedException e) {
//            }
//        }
//
//        return false;

//    }
    private boolean downloadXML() throws Exception {
        int downloadCount = 0;

        while (downloadCount < MAX_DOWNLOAD_COUNT) {
            try {
                buffer = new StringBuilder();
                //String urlString ="http://localhost/vas.xml"; //VASSettings.getXMLUrl();
                String urlString = VASSettings.getXMLUrl();
                urlString = formatURL(urlString);
                urlString = urlString.replaceFirst("\\[SSO\\]", Settings.RSA_SSO_CODE);
                urlString = urlString.replaceFirst("\\[LANGUAGE\\]", Language.getLanguageTag());

                /*String authLink = VASSettings.getAuthenticationPage();
                authLink = authLink.replaceFirst("\\[SID\\]", Settings.getSessionID());
                authLink = authLink.replaceFirst("\\[URL\\]", url);
                authLink = authLink.replaceFirst("\\[LID\\]", Settings.getUserID());   // Settings.getUserID());
                authLink = authLink.replaceFirst("\\[LANGUAGE\\]", Language.getLanguageTag());*/

                URLConnection connection;
//                    URL url = new URL(urlString);
//                    connection = url.openConnection();
                connection = SharedMethods.getURLConnection(urlString);
                byte[] data = SharedMethods.readFullResponse(connection);
                //todo: try out direct path
                if (data == null) {
                    URLConnection alternateConnection;
                    URL url = new URL(urlString);
                    alternateConnection = url.openConnection();
                    data = SharedMethods.readFullResponse(alternateConnection);
                }
                //todo: try out direct path
                buffer.append(new String(data));
                System.out.println(new String(data));
                downloaded = true;
                return true;
            } catch (Exception e) {
                downloadCount++;
                e.printStackTrace();
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }
        }
        return false;
    }


       private boolean readXML() throws Exception {       // for test only
        int downloadCount = 0;

        while (downloadCount < MAX_DOWNLOAD_COUNT) {
            try {
                buffer = new StringBuilder();


                InputStream in = new FileInputStream("D:\\vasmenu.xml");
                byte[] data = new byte[100];

                while (true) {
                    int i = in.read(data);
                    if (i == -1) break;

                    buffer.append(new String(data, 0, i));
                }
                downloaded = true;

                return true;
            } catch (Exception e) {
                downloadCount++;
                e.printStackTrace();
            }
            try {
                Thread.sleep(5000);
            } catch (InterruptedException e) {
            }
        }

        return false;
    }

    private String formatURL(String url) {
        if (url.indexOf("?") < 0) {
            return url + "?";
        } else {
            return url;
        }
    }

    private String requestPublicKey() {
        if (rsaKey.equals("")) {
        StringBuilder sBuffer = new StringBuilder();
        String requestPublicKeyLink = VASSettings.getPublicKeyUrl();
        try {
            //////////////////////////////////////////////////////////
            URLConnection connection;
//                URL url = new URL(requestPublicKeyLink);
    //            connection = url.openConnection();
    //            System.out.println("VAS no proxy");
                connection = SharedMethods.getURLConnection(requestPublicKeyLink);
            //////////////////////////////////////////////////////////
            InputStream in = connection.getInputStream();
            byte[] data = new byte[100];
            while (true) {
                int i = in.read(data);
                if (i == -1) break;
                sBuffer.append(new String(data, 0, i));
            }
            downloaded = true;
        } catch (IOException e) {
                //try out direct path
                try {
                    URLConnection altConnection;
                    URL url = new URL(requestPublicKeyLink);
                    altConnection = url.openConnection();
                    InputStream in = altConnection.getInputStream();
                    byte[] data = new byte[100];
                    while (true) {
                        int i = in.read(data);
                        if (i == -1) break;
                        sBuffer.append(new String(data, 0, i));
                    }
                    downloaded = true;
                } catch (Exception e1) {
                    e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                //try out direct path
        }
            rsaKey = sBuffer.toString();
            return rsaKey;
        } else {
            return rsaKey;
        }
    }

    private String getPublicKey(String sFrame) {
        String sPublicKey = null;
        try {
            if (sFrame != null) {
                String[] tokens = sFrame.split(Meta.PUBLIC_KEY_SEPERATOR);
                sPublicKey = tokens[1];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return sPublicKey;
    }

    private String getKeyID(String sFrame) {
        String sPublicKey = null;
        String id = null;
        try {
            if (sFrame != null) {
                String[] tokens = sFrame.split(Meta.PUBLIC_KEY_SEPERATOR);
                id = tokens[2];
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return id;
    }


    private void analyseMenu() {
        XMLElement root = new XMLElement();
        root.parseString(buffer.toString());       //.substring(3)
        ArrayList<XMLElement> rootElements = root.getChildren();

        for (int i = 0; i < rootElements.size(); i++) {
            XMLElement rootItem = rootElements.get(i);
            if (rootItem.getName().equals("TABLE")) {
                ArrayList<XMLElement> elements = rootItem.getChildren();
                for (int j = 0; j < elements.size(); j++) {
                    XMLElement element = elements.get(j);
                    createMenuItems(element, null, links);
                    element = null;
                }
            } else if (rootItem.getName().equals("MENUBAR")) {
                ArrayList<XMLElement> elements = rootItem.getChildren();
                for (int j = 0; j < elements.size(); j++) {
                    XMLElement element = elements.get(j);
                    createMenuItems(element, null, menus);
                    element = null;
                }
            } else if (rootItem.getName().equals("MEDIAPLAYER")) {      //load default channels of mediaplayer
                ArrayList<String> channelList = new ArrayList<String>();
                ArrayList<XMLElement> elements = rootItem.getChildren();
                for (int j = 0; j < elements.size(); j++) {
                    XMLElement element = elements.get(j);
                    createMediaPlayerChannelList(element, channelList);
                    element = null;
                }
                vasData.put("MEDIAPLAYER", channelList);

            }
        }
    }

    /**
     * Convert the XML elements (representing MP channels) to list of strings
     *
     * @param element     <LINK><ITEM CAPTION="ABC" URL="http://www.abc.com" /></LINK>
     * @param channelList list holding formatted strings CAPTION,URL
     */
    private void createMediaPlayerChannelList(XMLElement element, ArrayList<String> channelList) {
        if (element.getName().equals("LINKS")) {
            ArrayList<XMLElement> linksList = element.getChildren();
            for (int j = 0; j < linksList.size(); j++) {
                String channelString = "";
                if ((linksList.get(j)).getName().equals("ITEM")) {
                    try {
                        channelString += (linksList.get(j)).getAttribute("CAPTION");
                        channelString += ",";
                        channelString += (linksList.get(j)).getAttribute("URL");
                    } catch (Exception e) {
                        channelString = "";
                    }
                    channelList.add(channelString);
                    channelString = "";
                }
            }
        }
    }

    private void createMenuItems(final XMLElement element, TWMenu menu, ArrayList<JComponent> store) {
        if (element.getName().equals("SEPARATOR")) {
            if (menu != null) {
                menu.add(new TWSeparator());
            } else {
                store.add(new TWSeparator());
            }
        } else if (element.getName().equals("MENU")) {
            boolean iconAvailable = false;
            ImageIcon icon = null;

            /* Valiate the exchange availability of the user first */
            boolean exchangeOK = false;
            String exchanges[] = null;
            try {
                exchanges = new String[0];
                try {
                    exchanges = element.getAttribute("EXCHANGES").split(",");
                } catch (Exception e) {
                    exchangeOK = true; // exchanges not defined
                }

                for (String exchange : exchanges) {
                    if ((exchange != null) && (ExchangeStore.getSharedInstance().isValidExchange(exchange.toUpperCase()))) {
                        exchangeOK = true; // if any of the exchnages in the list are vslid. proceed
                    }
                }

            } catch (Exception e) {
            }

            if (!exchangeOK) return;

            String instruments[] = new String[0];
            try {
                instruments = element.getAttribute("INSTRUMENT_TYPES").split(",");
            } catch (Exception e) {
            }

            try {
                String iconURL = element.getAttribute("ICON");
                if (iconURL != null) {
                    icon = new ImageIcon(new URL(iconURL));
                    iconAvailable = true;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }

            if (element.countChildren() > 0) {
                TWMenu submenu;
                if (iconAvailable) {
                    submenu = new TWMenu(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(element.getAttribute("CAPTION"))));
                    submenu.setIcon(icon);
                    submenu.putClientProperty("exchanges", exchanges);
                    submenu.putClientProperty("instruments", instruments);
                } else {
                    submenu = new TWMenu(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(element.getAttribute("CAPTION"))), "vasmenu.gif");
                    submenu.putClientProperty("exchanges", exchanges);
                    submenu.putClientProperty("instruments", instruments);
                }
                GUISettings.applyOrientation(submenu);
                if (menu != null) {
                    menu.add(submenu);
                } else {
                    store.add(submenu);
                }
                String location = element.getAttribute("LOCATION");
                if (location != null) {
                    submenu.putClientProperty("LOCATION", location);
                }
                ArrayList<XMLElement> children = element.getChildren();
                for (int i = 0; i < children.size(); i++) {
                    createMenuItems(children.get(i), submenu, store);
                }
            } else {
                if (element.getName().equals("MENU")) {
                    final TWMenuItem item;
                    if (iconAvailable) {
                        item = new TWMenuItem(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(element.getAttribute("CAPTION"))));
                        item.setIcon(icon);
                    } else {
                        item = new TWMenuItem(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(element.getAttribute("CAPTION"))), "vasmenu.gif");
                    }
                    if (exchanges != null) {
                        item.putClientProperty("exchanges", exchanges);
                    }
                    item.putClientProperty("instruments", instruments);
                    //item.updateUI();
                    GUISettings.applyOrientation(item);
                    try {
                        String winType = element.getAttribute("WIN_TYPE");
                        if ((winType != null) && !(ExchangeStore.isValidSystemFinformationType(Integer.parseInt(winType))))
                            return;
                    } catch (Exception e) {
                        e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    String location = element.getAttribute("LOCATION");
                    if (location != null) {
                        item.putClientProperty("LOCATION", location);
                    }
                    item.setActionCommand(element.getAttribute("URL"));

                    String windowID = element.getAttribute("WINID");
                    if (windowID != null) {
                        item.putClientProperty("WINID", windowID);
                    } else {
                        item.putClientProperty("WINID", "NULL");
                    }

                    String size = element.getAttribute("SIZE");
                    if (size != null) {
                        item.putClientProperty("SIZE", size);
                    }

                    item.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {

                            BrowserManager.getInstance().showLoading((String) item.getClientProperty("WINID"),
                                    item.getText(), (String) item.getClientProperty("SIZE"));

                            SwingUtilities.invokeLater(new Runnable() {
                                public void run() {
                                    String publicKeyRequestString = null;
                                    String publicKey = null;
                                    String id = null;
                                    String cyperText = null;
                                    boolean popError = false;
                                    boolean secured = false;

                                    try {
                                        secured = element.getIntAttribute("isEncripted", 1) == 1;
                                    } catch (Exception ex) {
                                        // do nothing
                                    }

                                    try {
                                        if (secured) {
                                            //request public key details from VAS Server
                                            String authLinkEncrypt = null;
                                            try {
                                                publicKeyRequestString = requestPublicKey();
                                                //read public key and unique id
                                                publicKey = getPublicKey(publicKeyRequestString);
                                                id = getKeyID(publicKeyRequestString);

                                                //encrypt userid using the key
                                                Encripter encrypter = new Encripter(publicKey);
                                                //plain text = Pro|hash(userid+loginname)
                                                cyperText = encrypter.encrypt(Meta.RSA_PRODUCT_TYPE + "|" + Settings.getUserID() + "#" + Settings.getUserName());

                                                String url = item.getActionCommand();
                                                url = formatURL(url);
                                                String key = Client.getInstance().getSelectedSymbol();
                                                if ((key != null) && (!key.equals(Constants.NULL_STRING))) {
                                                    if (SharedMethods.checkInstrumentType(key, Meta.INSTRUMENT_FUTURE)) {
                                                        key = Client.getInstance().getSelectedBaseSymbol();
                                                        if ((key != null) && (!key.equals(Constants.NULL_STRING))) {
                                                            url = url.replaceFirst("\\[SYMBOL\\]", SharedMethods.getSymbolFromKey(key));
                                                            url = url.replaceFirst("\\[EXCHANGE\\]", SharedMethods.getExchangeFromKey(key));
                                                            url = url.replaceFirst("\\[INSTRUMENT\\]", "" + SharedMethods.getInstrumentTypeFromKey(key));
                                                            url = url.replaceFirst("\\[LANG_CODE\\]", Language.getLanguageTag());
                                                        }
                                                    } else {
                                                        url = url.replaceFirst("\\[SYMBOL\\]", SharedMethods.getSymbolFromKey(key));
                                                        url = url.replaceFirst("\\[EXCHANGE\\]", SharedMethods.getExchangeFromKey(key));
                                                        url = url.replaceFirst("\\[INSTRUMENT\\]", "" + SharedMethods.getInstrumentTypeFromKey(key));
                                                        url = url.replaceFirst("\\[LANG_CODE\\]", Language.getLanguageTag());
                                                    }
                                                    url = url.replaceFirst("\\[EXCHANGE_LIST\\]", getExchanges());
                                                }
                                                url = url.replaceAll("\\&", "|");
                                                popError = isEmptySymbol(url);
                                                if (!popError) {
                                                    authLinkEncrypt = VASSettings.getRSAAuthenticationPage();
                                                    try {
                                                        cyperText = URLEncoder.encode(cyperText);
                                                    } catch (Exception e1) {
                                                        e1.printStackTrace();
                                                    }
                                                    authLinkEncrypt = authLinkEncrypt.replaceFirst("\\[CYPER\\]", cyperText);
                                                    authLinkEncrypt = authLinkEncrypt.replaceFirst("\\[URL\\]", url);
                                                    authLinkEncrypt = authLinkEncrypt.replaceFirst("\\[KID\\]", id);   // Settings.getUserID());
                                                    authLinkEncrypt = authLinkEncrypt.replaceFirst("\\[LANGUAGE\\]", Language.getLanguageTag());
                                                    System.out.println("<DEBUG> RSA LINK : " + authLinkEncrypt);
                                                }
                                            } catch (Exception e1) {
                                                popError = true;
                                            }
                                            if (!popError) {
//                                        Client.getInstance().getWebBrowserFrame().setURL(authLinkEncrypt, item.getText());
                                                BrowserManager.getInstance().navigate((String) item.getClientProperty("WINID"),
                                                        authLinkEncrypt, item.getText(), (String) item.getClientProperty("SIZE"), true);
                                            }
                                        } else {
                                            String url = item.getActionCommand();
                                            url = formatURL(url);
                                            String key = Client.getInstance().getSelectedSymbol();
                                            if ((key != null) && (!key.equals(Constants.NULL_STRING))) {
                                                if (SharedMethods.checkInstrumentType(key, Meta.INSTRUMENT_FUTURE)) {
                                                    key = Client.getInstance().getSelectedBaseSymbol();
                                                    if ((key != null) && (!key.equals(Constants.NULL_STRING))) {
                                                        url = url.replaceFirst("\\[SYMBOL\\]", SharedMethods.getSymbolFromKey(key));
                                                        url = url.replaceFirst("\\[EXCHANGE\\]", SharedMethods.getExchangeFromKey(key));
                                                        url = url.replaceFirst("\\[INSTRUMENT\\]", "" + SharedMethods.getInstrumentTypeFromKey(key));
                                                        url = url.replaceFirst("\\[LANG_CODE\\]", Language.getLanguageTag());
                                                    }
                                                } else {
                                                    url = url.replaceFirst("\\[SYMBOL\\]", SharedMethods.getSymbolFromKey(key));
                                                    url = url.replaceFirst("\\[EXCHANGE\\]", SharedMethods.getExchangeFromKey(key));
                                                    url = url.replaceFirst("\\[INSTRUMENT\\]", "" + SharedMethods.getInstrumentTypeFromKey(key));
                                                    url = url.replaceFirst("\\[LANGUAGE\\]", Language.getLanguageTag());
                                                }
                                                url = url.replaceFirst("\\[EXCHANGE_LIST\\]", getExchanges());
                                            }
                                            System.out.println("<DEBUG> URL : " + url);
                                            popError = isEmptySymbol(url);
                                            if (!popError) {
//                                        Client.getInstance().getWebBrowserFrame().setURL(url, item.getText());
                                                BrowserManager.getInstance().navigate((String) item.getClientProperty("WINID"),
                                                        url, item.getText(), (String) item.getClientProperty("SIZE"), true);
                                            }
                                        }

                                    } catch (Exception e1) {
                                        e1.printStackTrace();
                                        popError = true;
                                    }
                                    /*if(popError){
                                        SharedMethods.showMessage(Language.getString("VAS_SERVICE_NOT_AVAILABLE"),JOptionPane.ERROR_MESSAGE);
                                    }*/
                                }
                            });
                        }
                    });
                    if (menu != null) {
                        menu.add(item);
                    } else {
                        store.add(item);
                    }
                }
            }
        }
    }

    public String[] getInstrumnetTypes() {
        return instrumnetTypes;
    }

    private boolean isEmptySymbol(String URL) {

        if (URL.contains("[SYMBOL]")) {
            return true;
        } else {
            return false;

        }
    }

    private String getExchanges() {
        StringBuilder list = new StringBuilder("");
        Enumeration<Exchange> exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            list.append(",");
            list.append(exchanges.nextElement().getSymbol());
        }
        return list.substring(1);
    }

    /*private void createMenuItems(XMLElement element, TWMenu menu, ArrayList<Object> store) {
        if (element.getName().equals("SEPARATOR")) {
            if (menu != null) {
                menu.add(new JSeparator());
            } else {
                store.add(new JSeparator());
            }
        } else if (element.getName().equals("MENU")) {
            if (element.countChildren() > 0) {
                // Bug Id <#?????> // need to complete this 
                TWMenu submenu = new TWMenu(UnicodeUtils.getNativeString(Language.getLanguageSpecificString((String) element.getAttribute("CAPTION"))), "vasmenu.gif");
                //submenu.updateUI();
                GUISettings.applyOrientation(submenu);
                if (menu != null) {
                    menu.add(submenu);
                } else {
                    store.add(submenu);
                }
                ArrayList<XMLElement> children = element.getChildren();
                for (int i = 0; i < children.size(); i++) {
                    createMenuItems(children.get(i), submenu, store);
                }
            } else {
                if (element.getName().equals("MENU")) {
                    final TWMenuItem item = new TWMenuItem(UnicodeUtils.getNativeString(Language.getLanguageSpecificString((String) element.getAttribute("CAPTION"))), "vasmenu.gif");
                    //item.updateUI();
                    GUISettings.applyOrientation(item);
                    item.setActionCommand((String) element.getAttribute("URL"));
                    item.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            try {
                                String url = item.getActionCommand();
                                url = formatURL(url);
                                String key = Client.getInstance().getSelectedSymbol();
                                if ((key != null) && (!key.equals(Constants.NULL_STRING))) {
                                    url = url.replaceFirst("\\[SYMBOL\\]", SharedMethods.getSymbolFromKey(key));
                                    url = url.replaceFirst("\\[EXCHANGE\\]", SharedMethods.getExchangeFromKey(key));
                                }
                                url = url.replaceAll("\\&", "|");
                                String authLink = VASSettings.getAuthenticationPage();
                                authLink = authLink.replaceFirst("\\[SID\\]", Settings.getSessionID());
                                authLink = authLink.replaceFirst("\\[URL\\]", url);
                                authLink = authLink.replaceFirst("\\[LID\\]", Settings.getUserID());   // Settings.getUserID());
                                authLink = authLink.replaceFirst("\\[LANGUAGE\\]", Language.getLanguageTag());
                                //System.out.println(url);
                                //BrowserConnector.getSharedInstance().sendURL("URL " + url);
                                Client.getInstance().getWebBrowserFrame().setURL(authLink);
//                                System.out.println("LINK " + authLink);
//                                Runtime.getRuntime().exec("START " + authLink);
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    });
                    if (menu != null) {
                        menu.add(item);
                    } else {
                        store.add(item);
                    }
                }
            }
        }
    }*/

    public ArrayList<JComponent> getLinks() {
        return links;
    }

    public ArrayList<JComponent> getMenus() {
        return menus;
    }

    public void filreVASDataDownloadStarted() {
        for (VASMenuListener listener : listeners) {
            try {
                listener.VASDataDownloadStarted();
            } catch (Exception e) {
            }
        }
    }

    public void filreVASDataDownloadSucess() {
        for (VASMenuListener listener : listeners) {
            try {
                listener.VASDataDownloadSucess(menus, links);
                Enumeration e = vasData.keys();
                while(e.hasMoreElements()){
                    String key = (String)e.nextElement();
                    listener.VASDataDownloadSucess(key,vasData.get(key));
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }

    public void filreVASDataDownloadFailed() {
        for (VASMenuListener listener : listeners) {
            try {
                listener.VASDataDownloadFailed();
            } catch (Exception e) {
            }
        }
    }
}
