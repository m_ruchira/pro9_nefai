package com.isi.csvr.vas;

import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.Settings;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 18, 2006
 * Time: 10:35:29 AM
 */
public class VASSettings {
    private static SmartProperties prop;

    private static void load() {
        try {
            prop = new SmartProperties("=");
            prop.loadCompressed(Settings.CONFIG_DATA_PATH+"/vas.msf");
        }
        catch (Exception s_e) {
            prop = new SmartProperties("=");
        }
    }

    public static String getAuthenticationPage() {
        if (prop == null) {
            load();
        }
        return prop.getProperty("AUTH_PAGE");
    }

    public static String getXMLUrl() {
        if (prop == null) {
            load();
        }
        return prop.getProperty("XML_URL");
    }

    public static String getPublicKeyUrl() {
        if (prop == null) {
            load();
        }
        return prop.getProperty("PUBLIC_KEY_REQUEST_PAGE");
    }

    public static String getRSAAuthenticationPage() {
        if (prop == null) {
            load();
        }
        return prop.getProperty("AUTH_PAGE_RSA");
    }
}
