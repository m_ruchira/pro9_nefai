package com.isi.csvr.mist;

import com.isi.csvr.Client;
import com.isi.csvr.shared.Language;
import com.isi.csvr.table.Table;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class MISTTableCustomizerDialog extends JDialog implements ActionListener {

    private JPanel panel1 = new JPanel();
    private BorderLayout borderLayout1 = new BorderLayout();
    private MISTColorConfigPanel colorConfigPanel;
    private static MISTTableCustomizerDialog self = null;

    private MISTTableCustomizerDialog(Frame frame, String title, boolean modal) {
        super(frame, title, modal);
        try {
            jbInit();
            pack();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private MISTTableCustomizerDialog() {
        this(Client.getInstance().getFrame(), Language.getString("CUSTOMIZER"), true);
        this.setResizable(false);
        //colorConfigPanel.setTarget(target);
        setLocationRelativeTo(Client.getInstance().getFrame());
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        colorConfigPanel.registerKeyboardAction(this, "ESC", KeyStroke.getKeyStroke("released ESCAPE"), JComponent.WHEN_IN_FOCUSED_WINDOW);//KeyEvent.VK_ESCAPE,0
    }

    public static MISTTableCustomizerDialog getSharedInstance() {
        if (self == null) {
            self = new MISTTableCustomizerDialog();
        }
        return self;
    }

    public void show(Table target) {
        System.out.println("Showing Colour Config Panel");
        colorConfigPanel.setTarget(target);
        colorConfigPanel.applyTheme();
        show();
    }

    public Font getBodyFont() {
        return colorConfigPanel.getBodyFont();
    }

    public Font getHeadingFont() {
        return colorConfigPanel.getHeadingFont();
    }

    public int getBodyGap(){
        return colorConfigPanel.getBodyGap();
    }

    private void jbInit() throws Exception {
        panel1.setLayout(new FlowLayout());
        colorConfigPanel = new MISTColorConfigPanel(this);
        getContentPane().add(colorConfigPanel);
//		setResizable(false);
    }

    public void actionPerformed(ActionEvent e) {
        setVisible(false);
    }

}