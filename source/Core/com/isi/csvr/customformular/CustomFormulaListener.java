package com.isi.csvr.customformular;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Aug 22, 2006
 * Time: 1:48:41 PM
 * To change this template use File | Settings | File Templates.
 */
public interface CustomFormulaListener {

    void customFormulaAdded(String columnName);

    void customFormulaRemoved(String columnName);

    void customFormulaEditted(String columnName, String newName);
}
