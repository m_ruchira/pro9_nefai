package com.isi.csvr.customformular;

import bsh.EvalError;
import bsh.Interpreter;
import com.isi.csvr.*;
import com.isi.csvr.help.HelpManager;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.profiles.export.DataSelection;
import com.isi.csvr.profiles.imprt.ImportUI;
import com.isi.csvr.profiles.imprt.OpenZipFile;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartFileChooser;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.util.Decompress;
import com.isi.csvr.win32.NativeMethods;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.event.MenuEvent;
import javax.swing.event.MenuListener;
import javax.swing.text.BadLocationException;
import javax.swing.text.Style;
import javax.swing.text.StyleConstants;
import javax.swing.text.StyledDocument;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: Aug 21, 2006
 * Time: 1:53:47 PM
 * To change this template use File | Settings | File Templates.
 */
public class FormularEditor extends InternalFrame implements KeyListener, ActionListener, MouseListener, Themeable, MenuListener {

//    private String[] words = {"Last","LastQty","Changed","PerChanged","VWAP","TotVolumn","Turnover","Trades","AvgVolumn","MinPrice","MaxPrice","Open","High","Low","PrevClosed","Bid","BidQty","Offer","OfferQty","PERatio","PBRatio","Yield","52WkHigh","52WkLow","LTPrice","TotBidQty","NoOfBids","TotOfferQty","NoOfOffers","Spread","Range","%Spread","%Range","RefPrice","HighA","LowB","SAvgA","WAvgA","SAvgB","WAvgB","LastAskPrice","LastAskQty","LastBidPrice","LastBidQty"};
//    private String[] wordsnameArray = {"Last","Last Qty.","Changed","Per. Changed","VWAP","Tot. Volumn","Turnover","Trades","Avg. Volumn","Min. Price","Max. Price","Open","High","Low","Prev. Closed","Bid","Bid Qty.","Offer","Offer Qty.","P-E Ratio","P-B Ratio","Yield","52Wk. High","52Wk. Low","L.T.Price","Tot. Bid. Qty.","No Of Bids","Tot. Offer. Qty.","No Of Offers","Spread","Range","%Spread","%Range","Ref. Price","High A","Low B","S.Avg.A","W.Avg.A","S.Avg.B","W.Avg.B","Last Ask Price","Last Ask Qty.","Last Bid Price","Last Bid Qty."};
//    private String[] wordsmethodArray = {"stock.getLastTradeValue()","stock.getTradeQuantity()","stock.getChange()","stock.getPercentChange()","stock.getAvgTradePrice()","stock.getVolume()","stock.getTurnover()","stock.getNoOfTrades()","stock.getAvgTradeVolume()","stock.getMinPrice()","stock.getMaxPrice()","stock.getTodaysOpen()","stock.getHigh()","stock.getLow()","stock.getPreviousClosed()","stock.getBestBidPrice()","stock.getBestBidQuantity()","stock.getBestAskPrice()","stock.getBestAskQuantity()","stock.getPER()","stock.getPBR()",
//            "stock.getYield()","stock.getHighPriceOf52Weeks()","stock.getLowPriceOf52Weeks()","stock.getLastTradedPrice()","stock.getTotalBidQty()","stock.getNoOfBids()","stock.getTotalAskQty()","stock.getNoOfAsks()","stock.getSpread()","stock.getRange()","stock.getPctSpread()","stock.getPctRange()","stock.getRefPrice()","stock.getHighAsk()","stock.getLowBid()","stock.getSimpleAverageAsk()","stock.getWAverageAsk()","stock.getSimpleAverageBid()","stock.getWAverageBid()","stock.getLastAskPrice()","stock.getLastAskQuantity()","stock.getLastBidPrice()","stock.getLastBidQuantity()"};
    private String[] operators = {"+","*","-","/","(",")"};
    private String[] operatorsnaemArray = {"Add","Multiply","Subtract","Divide","(",")"};

    private static FormularEditor self = null;
    private Hashtable table = null;
    private Interpreter interpreter = null;
//    private FormularEditorInterface object = null;
    private Vector nameArray = null;
    private StyledDocument doc;
//    private ViewSetting viewSetting;
    private JTextPane textArea;
    private JList wordList;

    private String editFormulaName ="";
    private String operatorsString = " ";
    private String additonalSeperators = " \n";
    private TWButton btnGen;
    private TWButton btnClose;
    private TWButton btnDelete;
    private JPanel listPanel;
    private JPanel areaPanel;
    private JLabel lblMessage;
    private JLabel nameLabel;
    private TWTextField nameField;

//    private String columnName = "";
    private JPanel oToolBar;
    private ToolBarButton btnPlus;
    private ToolBarButton btnMinus;
    private ToolBarButton btnMulti;
    private ToolBarButton btnDiv;
    private ToolBarButton btnfbrac;
    private ToolBarButton btnbbrac;
//    private ToolBarButton btnmod;

    public static final byte NEW_FORMULA = 1;
    public static final byte EDIT_FORMULA = 2;
    private byte type = 0;
    private int width = 575;
    private int height = 450;
    private int listPanelwidth = 170;
    private int listPanelheight = 300;

    private TWMenuBar menuBar;
    private JMenu formulaMenu;
    private JMenu toolsMenu;
    private JMenu helpMenu;
    private TWMenuItem newMenuItem;
    private TWMenu modifyMenu;
    private TWMenu deleteMenu;
    private TWMenuItem importMenuItem;
    private TWMenuItem exportMenuItem;
//    private ClientTable selectedTable;
    private boolean isModified = false;
    private JPopupMenu popupMenu;
    private TWMenuItem saveOnly;
    private TWMenu addToWindows;
    private TWMenuItem addToAll;
    private TWMenuItem addToshowings;
    private TWMenuItem helpContent;
    private boolean helpshowing = false;


    private FormularEditor(){   //ViewSetting vSetting){
        super();
        this.setTitle(Language.getString("FORMULAR_EDITOR"));
//        super(Language.getString("FORMULAR_EDITOR"),vSetting);
//        this.viewSetting = vSetting;
//        viewSetting.setParent(this);
        textArea = new JTextPane();
        doc = textArea.getStyledDocument();
        textArea.setPreferredSize(new Dimension(300,275));
        Style style = textArea.addStyle("RED", null);
        StyleConstants.setForeground(style, Color.red);
        StyleConstants.setBold(style,false);
        style = textArea.addStyle("BLUE",null);
        StyleConstants.setBold(style,true);
        StyleConstants.setForeground(style, Color.blue);
        style = textArea.addStyle("BLACK",null);
        StyleConstants.setBold(style,true);
        StyleConstants.setForeground(style, Color.black);
        style = textArea.addStyle("GREEN",null);
        StyleConstants.setBold(style,true);
        StyleConstants.setForeground(style, Color.green);
        style = null;

        interpreter = new Interpreter();
        table = new Hashtable();
        nameArray = new Vector();

        FormularObject object = null;
        for(int i=0; i < operators.length;i++){
            operatorsString = operatorsString + operators[i];
            object = new FormularObject(operators[i]);
            object.setStyle(textArea.getStyle("BLACK"));
            object.setMethodName(operators[i]);
            object.setName(operatorsnaemArray[i]);
            object.setOperator(true);
            table.put(operators[i], object);
            object = null;
        }
//                for(int i=0; i < words.length;i++){
////            wordsString = wordsString + words[i];
//            object = new FormularObject(words[i]);
//            object.setStyle(textArea.getStyle("BLUE"));
//            object.setMethodName(wordsmethodArray[i]);
//            object.setName(wordsnameArray[i]);
//            object.setOperator(false);
//            table.put(words[i].toUpperCase(), object);
//            try {
//                interpreter.set(words[i].toUpperCase(),1);
////                System.out.println("set="+i);
//            } catch (EvalError evalError) {
//                evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//            }
//            object = null;
//        }
        loadPropertyFile();
        createMenu();
        createUI();
        createPopup();
        this.setJMenuBar(menuBar);
        Theme.registerComponent(this);
        applyTheme();
        setVisible(false);
        GUISettings.applyOrientation(this);
        GUISettings.applyOrientation(popupMenu);
        GUISettings.applyOrientation(addToWindows);
//        textArea.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
    }

    private void createMenu(){
        menuBar = new TWMenuBar();
        menuBar.setLayout(new BoxLayout(menuBar, BoxLayout.LINE_AXIS));

        formulaMenu = new JMenu(Language.getString("FORMULA_MENU"));
        formulaMenu.addMenuListener(this);

        newMenuItem = new TWMenuItem(Language.getString("NEW"),"newformula.gif");
        newMenuItem.addActionListener(this);
        formulaMenu.add(newMenuItem);

        modifyMenu = new TWMenu(Language.getString("MODIFY"),"editformula.gif");
        modifyMenu.addMenuListener(this);
        formulaMenu.add(modifyMenu);

        deleteMenu = new TWMenu(Language.getString("DELETE"),"deleteformula.gif");
        deleteMenu.addMenuListener(this);
        formulaMenu.add(deleteMenu);
        formulaMenu.setOpaque(false);
        menuBar.add(formulaMenu);

        toolsMenu = new JMenu(Language.getString("TOOLS"));
        importMenuItem = new TWMenuItem(Language.getString("IMPORT"),"importformula.gif");
        importMenuItem.addActionListener(this);
        toolsMenu.add(importMenuItem);

        exportMenuItem = new  TWMenuItem(Language.getString("EXPORT"),"exportformula.gif");
        exportMenuItem.addActionListener(this);
        toolsMenu.add(exportMenuItem);
        toolsMenu.setOpaque(false);
        menuBar.add(toolsMenu);

        helpMenu = new JMenu(Language.getString("HELP"));
//        helpMenu.addActionListener(this);
//        helpMenu.addMenuListener(this);

        helpContent = new TWMenuItem(Language.getString("HELP_CONTENTS"));

        helpContent.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_CUSTOM_COLUMNS"));
            }
        });
        helpMenu.add(helpContent);
        helpMenu.setOpaque(false);
        menuBar.add(helpMenu);

    }

    private void createFormulaList(final boolean isModifyMenu){
        TWMenu menu = null;
        if(isModifyMenu){
            menu = modifyMenu;
        }else{
            menu = deleteMenu;
        }
        menu.removeAll();
        int size = CustomFormulaStore.getSharedInstance().getColumnCount();
        for(int i=0; i<size; i++){
            final TWMenuItem item = new TWMenuItem(CustomFormulaStore.getSharedInstance().getColumnName(i));
            item.setText(CustomFormulaStore.getSharedInstance().getColumnName(i));
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    if(isModifyMenu){
                        if(saveOpenedFormula()){
                            isModified = false;
                            setType(FormularEditor.EDIT_FORMULA);
                            setEdittingFormula(item.getText());
                        }
                    }else{
                        String str = Language.getString("FORMULA_DELETE_MESSAGE");
                        str= str.replace("[NAME]",item.getText());
                        int option = SharedMethods.showConfirmMessage(str,JOptionPane.WARNING_MESSAGE,JOptionPane.YES_NO_OPTION);
                        if(option == JOptionPane.YES_OPTION){
                            CustomFormulaStore.getSharedInstance().removeCustomeFormula(item.getText());
                            setType(NEW_FORMULA);
                            clearField();
                        }
                        str = null;
                    }
                }
            });
            menu.add(item);
        }
    }

    private boolean saveOpenedFormula(){
        int status = 0;
        if(isModified && !textArea.getText().equals("")){
            status = SharedMethods.showConfirmMessage(Language.getString("FORMULA_SAVE_MODIFIED_MESSAGE"),JOptionPane.WARNING_MESSAGE,JOptionPane.YES_NO_OPTION);
            if(status == JOptionPane.YES_OPTION){
                return createFormula(false);
            }else{
                return true;
            }
        }
        return true;
    }

    public void setType(byte type){
        this.type = type;
        if(type == FormularEditor.EDIT_FORMULA){
            btnDelete.setVisible(true);
        }else{
            btnDelete.setVisible(false);
        }
    }

    public void showWindow(){
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        textArea.setComponentOrientation(ComponentOrientation.LEFT_TO_RIGHT);
        this.setVisible(true);
    }

    public void setEdittingFormula(String columnName){
        this.editFormulaName = columnName;
//        object = CustomFormulaStore.getSharedInstance().getFormulaObject(columnName);
        nameField.setText(columnName);
        textArea.setText(CustomFormulaStore.getSharedInstance().getFormulaObject(columnName).getFunctionString());
        getWords();
    }

    public static FormularEditor getSharedInstance(){
        if(self == null){
            self = new FormularEditor();
            self.setType(NEW_FORMULA);
        }
        return self;
    }

    private void createUI(){
        this.setSize(width,height);
        this.setLayout(new FlexGridLayout(new String[]{"100%"},new String[]{"35","100%","0"},4,4,true,true ));
        JPanel mainPanel = new JPanel(new BorderLayout(2,2));
        this.setFont(new TWFont("Arial",Font.PLAIN, 12));

        JPanel topPanel = new JPanel(new FlexGridLayout(new String[]{"100%"},new String[]{"5","25"}));
        JPanel namepanel = new JPanel(new FlexGridLayout(new String[]{""+listPanelwidth,"150"},new String[]{"100%"}));
        nameField = new TWTextField("",15);
        nameField.setPreferredSize(new Dimension(100,20));
        nameField.addKeyListener(this);
        nameLabel = new JLabel(Language.getString("COLUMN_NAME"));
        nameLabel.setPreferredSize(new Dimension(listPanelwidth,20));
        nameLabel.setFont(this.getFont().deriveFont(Font.BOLD));
        namepanel.add(nameLabel);
        namepanel.add(nameField);
        topPanel.add(new JLabel(""));
        topPanel.add(namepanel);
//        topPanel.add(new JSeparator());

        wordList = new JList(nameArray);
        JScrollPane wordScroll = new JScrollPane(wordList);
        wordScroll.setPreferredSize(new Dimension(100,listPanelheight));

        textArea.addKeyListener(this);
        wordList.addMouseListener(this);
        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        Border border = BorderFactory.createEtchedBorder();
        textArea.setBorder(border);

        btnGen = new TWButton(Language.getString("SAVE"));
        btnClose = new TWButton(Language.getString("BTN_CLOSE"));
        btnDelete = new TWButton(Language.getString("DELETE"));
        btnDelete.setVisible(false);
        btnGen.addMouseListener(this);
        btnGen.addActionListener(this);
        btnClose.addActionListener(this);
        btnDelete.addActionListener(this);
        JPanel btnPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING,0,0));
//        btnPanel.setSize(width,50);
//        btnPanel.setPreferredSize(new Dimension(width,50));
        JLabel nullLabel = new JLabel("");
        nullLabel.setPreferredSize(new Dimension(5,10));
        JLabel nullLabel2 = new JLabel("");
        nullLabel2.setPreferredSize(new Dimension(5,10));
        btnPanel.add(btnDelete);
        btnPanel.add(nullLabel2);
        btnPanel.add(btnGen);
        btnPanel.add(nullLabel);
        btnPanel.add(btnClose);


        listPanel = new JPanel(new FlexGridLayout(new String[]{"100%"},new String[]{"22","30","22","100%"} ));
        listPanel.setPreferredSize(new Dimension(listPanelwidth,listPanelheight));

        JLabel lblOpera = new JLabel(Language.getString("OPERATIONS"));
        lblOpera.setFont(this.getFont().deriveFont(Font.BOLD));
        JLabel lblFields = new JLabel(Language.getString("FIELDS"));
        lblFields.setFont(this.getFont().deriveFont(Font.BOLD));
        JLabel lblFormular = new JLabel(Language.getString("FORMULA"));
        lblFormular.setFont(this.getFont().deriveFont(Font.BOLD));
        listPanel.add(lblOpera);
        JPanel toolbar = new JPanel();
        toolbar.setBackground(Theme.getColor("TOOLBAR_BGCOLOR"));
        toolbar.setForeground(Theme.getColor("TOOLBAR_FGCOLOR"));
        listPanel.add(createToolbar());
        listPanel.add(lblFields);
        listPanel.add(wordScroll);

        areaPanel = new JPanel(new FlexGridLayout(new String[]{"100%"},new String[]{"25","100%"} ));
        areaPanel.setPreferredSize(new Dimension(300,listPanelheight));
        areaPanel.add(lblFormular);
        areaPanel.add(textArea);

//        JPanel southPanel = new JPanel(new BorderLayout());
        JPanel southPanel = new JPanel(new FlexGridLayout(new String[]{"100%"},new String[]{"0","25"}));
        lblMessage = new JLabel("");
//        lblMessage.setPreferredSize(new Dimension(width,40));
//        lblMessage.setSize(new Dimension(width,40));
        lblMessage.setText(Language.getString("FORMULA_EDITOR_MESSAGE"));
//        lblMessage.setFont(this.getFont().deriveFont(Font.BOLD));
        southPanel .add(lblMessage);
        southPanel.add(btnPanel);
        if (Language.isLTR()){
            mainPanel.add(listPanel, BorderLayout.WEST);
        }else{
            mainPanel.add(listPanel, BorderLayout.EAST);
        }
        mainPanel.add(areaPanel, BorderLayout.CENTER);
        this.add(topPanel, BorderLayout.NORTH);
        this.add(mainPanel, BorderLayout.CENTER);
        this.add(southPanel, BorderLayout.SOUTH);
//        btnPanel.setBorder(border);


        Client.getInstance().getDesktop().add(this);
        setDefaultCloseOperation(HIDE_ON_CLOSE);
        setResizable(true);
        setClosable(true);
        setIconifiable(true);
        this.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
//        GUISettings.applyOrientation(getContentPane(),ComponentOrientation.LEFT_TO_RIGHT);
        GUISettings.applyOrientation(getContentPane());
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
//        super.applySettings();

    }

    public void getWords(){
        int charAt = 0;
        String word = null;
        StringTokenizer token = new StringTokenizer(textArea.getText().trim(),(operatorsString+additonalSeperators),true);
        while(token.hasMoreElements()){
            word = token.nextToken();
            charAt = textArea.getText().indexOf(word, charAt);
            if(table.containsKey(word.toUpperCase())){
                String name = ((FormularObject)table.get(word.toUpperCase())).getID();
                try {
                    if(!name.equals(word)){
                        doc.remove(charAt,word.length());
                        if(((FormularObject)table.get(word.toUpperCase())).isOperator())
                            doc.insertString(charAt,name,textArea.getStyle("BLACK"));
                        else
                            doc.insertString(charAt,name,textArea.getStyle("BLUE"));
                    }
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
                if(((FormularObject)table.get(word.toUpperCase())).isOperator())
                    doc.setCharacterAttributes(charAt,word.length(),textArea.getStyle("BLACK"),true);
                else
                    doc.setCharacterAttributes(charAt,word.length(),textArea.getStyle("BLUE"),true);
//                doc.setCharacterAttributes(charAt,word.length(),((FormularObject)table.get(word.toUpperCase())).getStyle(),true);
            }else{
                try {
                    Double value = (Double.parseDouble(word));
                    doc.setCharacterAttributes(charAt,word.length(),textArea.getStyle("GREEN"),true);
                } catch (NumberFormatException e) {
                    doc.setCharacterAttributes(charAt,word.length(),textArea.getStyle("RED"),true);
                }
            }
        }
        if(!nameField.hasFocus())
            textArea.requestFocus();
    }

    public void keyTyped(KeyEvent e) {
        isModified = true;
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
        getWords();
    }

    private boolean createFormula(boolean setDisposable){
        boolean status = false;
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Tools, "CustomColumn-Create");
        try{
//                interpreter.eval(textArea.getText().toUpperCase().trim());
            if(nameField.getText().equals("")){
                JOptionPane.showMessageDialog(this,Language.getString("MSG_EMPTY_FORMULA_NAME"),Language.getString("ERROR"),JOptionPane.ERROR_MESSAGE);
                nameField.requestFocus();
                return false;
            }else if(!SharedMethods.isValidName(nameField.getText())){
                JOptionPane.showMessageDialog(this,Language.getString("MSG_INVALID_CHARS"),Language.getString("ERROR"),JOptionPane.ERROR_MESSAGE);
                nameField.requestFocus();
                return false;
            }
            if((type == EDIT_FORMULA) && nameField.getText().equals(editFormulaName)){
//                    this.setVisible(false);
                if(type == NEW_FORMULA){
                    status = CustomFormulaStore.getSharedInstance().createFile(type,nameField.getText(),textArea.getText().trim(),null,true, false);
                }else{
                    status = CustomFormulaStore.getSharedInstance().createFile(type,nameField.getText(),textArea.getText().trim(),editFormulaName,true, false);
                }
                if(!status){
                    JOptionPane.showMessageDialog(this,Language.getString("FORMULAR_EDITOR_ERROR"),Language.getString("ERROR"),JOptionPane.ERROR_MESSAGE);
                    textArea.requestFocus();
                    return false;
                }
                if(setDisposable)
                    exitWindow();
                return true;
            }else{
                if(CustomFormulaStore.getSharedInstance().isValidColumnName(nameField.getText())){
//                        this.setVisible(false);
                    if(type == NEW_FORMULA){
                        status = CustomFormulaStore.getSharedInstance().createFile(type,nameField.getText(),textArea.getText().trim(),null,true, false);
                    }else{
                        status = CustomFormulaStore.getSharedInstance().createFile(type,nameField.getText(),textArea.getText().trim(),editFormulaName,true, false);
                    }
                    if(!status){
                        JOptionPane.showMessageDialog(this,Language.getString("FORMULAR_EDITOR_ERROR"),Language.getString("ERROR"),JOptionPane.ERROR_MESSAGE);
                        textArea.requestFocus();
                        return false;
                    }
                    if(setDisposable)
                        exitWindow();
                    return true;
                }else{
                    JOptionPane.showMessageDialog(this,Language.getString("MSG_DUPLICATE_FORMULA"),Language.getString("ERROR"),JOptionPane.ERROR_MESSAGE);
                    nameField.requestFocus();
                    return false;
                }
            }
        } catch (Exception evalError) {
            JOptionPane.showMessageDialog(this,Language.getString("FORMULAR_EDITOR_ERROR"),Language.getString("ERROR"),JOptionPane.ERROR_MESSAGE);
            evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        return false;
    }

    public void actionPerformed(ActionEvent e) {
        if(e.getSource()== btnGen){

        } else if(e.getSource() ==  btnClose){
            this.setVisible(false);
        } else if(e.getSource() == newMenuItem){
            if(saveOpenedFormula()){
                isModified = false;
                setType(FormularEditor.NEW_FORMULA);
                clearField();
            }
        } else if(e.getSource() == importMenuItem){
            OpenZipFile ImSelect = new OpenZipFile();
            ImportUI UI = ImSelect.getObject();
            int[] disable = {0,1};
            UI.disableTabs(disable);
            UI.setSelectedTab(2);
            UI.setVisible(true);
            OpenZipFile.clearStaticVectors();
            ImSelect = null;
            UI = null;
        } else if(e.getSource() == exportMenuItem){
            DataSelection ExSelect = new DataSelection();
            int[] disable = {0,1,3,4};
            ExSelect.disableTabs(disable);
            ExSelect.setSelectedTab(2);
            ExSelect.setVisible(true);
            ExSelect = null;
        } else if(e.getSource() == btnDelete){
            int option = SharedMethods.showConfirmMessage(Language.getString("CONFIRM_DELETE_IN_CUST_FORMULA"),
                    JOptionPane.WARNING_MESSAGE,JOptionPane.YES_NO_OPTION);
            if(option == JOptionPane.YES_OPTION){
                CustomFormulaStore.getSharedInstance().removeCustomeFormula(editFormulaName);
                setType(NEW_FORMULA);
                clearField();
            }
        } else if(e.getSource() == saveOnly){
            getWords();
            if(createFormula(false)){
                exitWindow();
                showMessage();
            }
//            getWords();
//            createFormula(true);
        } else if(e.getSource() == addToAll){
            getWords();
            if(createFormula(false)){
                JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
                for (int i = 0; i < frames.length; i++) {
    //                if (frames[i].isVisible() && !frames[i].isIcon()) {
                        if(frames[i] instanceof CustomFormulaListener){
                            ClientTable table = (ClientTable)frames[i];
                            table.addColumn(nameField.getText());
                            table = null;
                        }
    //                }
                }
                frames = null;
                exitWindow();
                showMessage();
            }
        } else if(e.getSource() == addToshowings){
            getWords();
            if(createFormula(false)){
                JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
                for (int i = 0; i < frames.length; i++) {
                    if (frames[i].isVisible()) {
                        if(frames[i] instanceof CustomFormulaListener){
                            ClientTable table = (ClientTable)frames[i];
                            table.addColumn(nameField.getText());
                            table = null;
                        }
                    }
                }
                frames = null;
                exitWindow();
                showMessage();
            }
        } else if(e.getSource() == helpMenu){
//            System.out.println("help clicked");
            NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_CUSTOM_COLUMNS"));
        }
    }

    private void showMessage(){
        if(type == EDIT_FORMULA){
            SharedMethods.showMessage(Language.getString("MSG_COMPLETE_EDIT_CUSTOM_COLUMN"),JOptionPane.INFORMATION_MESSAGE);
        }else{
            SharedMethods.showMessage(Language.getString("MSG_COMPLETE_NEW_CUSTOM_COLUMN"),JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void saveDialog(){
        JFileChooser chooser = new SmartFileChooser(Settings.getAbsolutepath());
        String[] extentions = new String[1];
        extentions[0] = "html";
        TWFileFilter oFilter = new TWFileFilter("HTML", extentions);
        chooser.setFileFilter(oFilter);
        chooser.setAcceptAllFileFilterUsed(false);
//            chooser.s
        if (chooser.showSaveDialog(this) != JFileChooser.APPROVE_OPTION)
            return;
        File file = chooser.getSelectedFile();
        if (file == null)
            return;
        String sFile = file.getAbsolutePath();
        if (!sFile.toLowerCase().endsWith(".html")){
                sFile += ".html";
        }
        FileWriter writer = null;
        try {
            writer = new FileWriter(sFile);

//            MinimalHTMLWriter htmlWriter = new MinimalHTMLWriter(writer,chatTextArea.getStyledDocument());
//            htmlWriter.write();
        }
        catch (IOException ex) {
//                JOptionPane.showMessageDialog(this, "HTML File Not Saved", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        catch (Exception ex) {
//                JOptionPane.showMessageDialog(this, "HTML File Corrupt", "ERROR", JOptionPane.ERROR_MESSAGE);
        }
        if (writer != null) {
            try {
                writer.close();
            } catch (IOException x) {
            }
        }
    }

    public void mouseClicked(MouseEvent e) {
        if(e.getSource()==wordList){
            if (!SwingUtilities.isRightMouseButton(e)) {
            if(e.getClickCount()>1){
                isModified = true;
                int index = textArea.getCaretPosition();
                try {
                        if (wordList.getSelectedValue()!=null) {
                            doc.insertString(index," "+((ListObject)wordList.getSelectedValue()).getKey(),textArea.getStyle("BLUE"));
                        }
                } catch (BadLocationException e1) {
                        Toolkit.getDefaultToolkit().beep();
                }
                getWords();
            }
            }
        } else if(e.getSource() == btnGen){
            if(!textArea.getText().trim().equals("") && !nameField.getText().trim().equals("")){
                if(type == NEW_FORMULA){
                GUISettings.showPopup(popupMenu, btnGen, e.getX(), (e.getY()- (int)popupMenu.getPreferredSize().getHeight()-1));
                } else {
                    getWords();
                    if(createFormula(false)){
                        exitWindow();
                        showMessage();
                    }
                }
//            getWords();
//            createFormula(true);
            }
        }
        textArea.requestFocus();
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void loadImages(String sThemeID){
        btnPlus.setIcon(new ImageIcon("images/Theme" + sThemeID + "/plus.gif"));
        btnMinus.setIcon(new ImageIcon("images/Theme" + sThemeID + "/minus.gif"));
        btnMulti.setIcon(new ImageIcon("images/Theme" + sThemeID + "/multi.gif"));
        btnDiv.setIcon(new ImageIcon("images/Theme" + sThemeID + "/divide.gif"));
        btnfbrac.setIcon(new ImageIcon("images/Theme" + sThemeID + "/fbracket.gif"));
        btnbbrac.setIcon(new ImageIcon("images/Theme" + sThemeID + "/bbracket.gif"));

        newMenuItem.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/newformula.gif"));
        modifyMenu.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/editformula.gif"));
        deleteMenu.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/deleteformula.gif"));
        importMenuItem.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/importformula.gif"));
        exportMenuItem.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/exportformula.gif"));
        saveOnly.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/formulasave.gif"));
        addToWindows.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/formuladdone.gif"));
        addToshowings.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/formuladdshowing.gif"));
        addToAll.setIcon(new ImageIcon("images/Theme" + sThemeID + "/menu/formuladdall.gif"));
//        btnmod.setIcon(new ImageIcon("images/Theme" + sThemeID + "/mod.gif"));
    }

    public JPanel createToolbar() {
        oToolBar = new JPanel();
        oToolBar.setOpaque(false);
        oToolBar.setLayout(new FlowLayout(FlowLayout.LEADING, 2, 2));

        btnPlus = new ToolBarButton();
        btnPlus.setFocusable(false);
        btnPlus.setPreferredSize(new Dimension(20, 20));
        btnPlus.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
//        btnPlus.setToolTipText(Language.getString("ADD"));
        btnPlus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int index = textArea.getCaretPosition();
                try {
                    doc.insertString(index," +",textArea.getStyle("BLACK"));
                } catch (BadLocationException e1) {
                    java.awt.Toolkit.getDefaultToolkit().beep();
                }
                textArea.requestFocus();
            }
        });
        oToolBar.add(btnPlus);

        btnMinus = new ToolBarButton();
        btnMinus.setFocusable(false);
        btnMinus.setPreferredSize(new Dimension(20, 20));
        btnMinus.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
//        btnMinus.setToolTipText(Language.getString("SUBTRACT"));
        btnMinus.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int index = textArea.getCaretPosition();
                try {
                    doc.insertString(index," -",textArea.getStyle("BLACK"));
                } catch (BadLocationException e1) {
                    java.awt.Toolkit.getDefaultToolkit().beep();
                }
                textArea.requestFocus();
            }
        });
        oToolBar.add(btnMinus);

        btnMulti = new ToolBarButton();
        btnMulti.setFocusable(false);
        btnMulti.setPreferredSize(new Dimension(20, 20));
        btnMulti.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
//        btnMulti.setToolTipText(Language.getString("MULTIPLY"));
        btnMulti.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int index = textArea.getCaretPosition();
                try {
                    doc.insertString(index," *",textArea.getStyle("BLACK"));
                } catch (BadLocationException e1) {
                    java.awt.Toolkit.getDefaultToolkit().beep();
                }
                textArea.requestFocus();
            }
        });
        oToolBar.add(btnMulti);

        btnDiv = new ToolBarButton();
        btnDiv.setFocusable(false);
        btnDiv.setPreferredSize(new Dimension(20, 20));
        btnDiv.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
//        btnDiv.setToolTipText(Language.getString("DIVIDE"));
        btnDiv.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int index = textArea.getCaretPosition();
                try {
                    doc.insertString(index," /",textArea.getStyle("BLACK"));
                } catch (BadLocationException e1) {
                    java.awt.Toolkit.getDefaultToolkit().beep();
                }
                textArea.requestFocus();
            }
        });
        oToolBar.add(btnDiv);

        btnfbrac = new ToolBarButton();
        btnfbrac.setFocusable(false);
        btnfbrac.setPreferredSize(new Dimension(20, 20));
        btnfbrac.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
//        btnfbrac.setToolTipText(Language.getString("TIME_AND_SALES"));
        btnfbrac.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int index = textArea.getCaretPosition();
                try {
                    doc.insertString(index," (",textArea.getStyle("BLACK"));
                } catch (BadLocationException e1) {
                    java.awt.Toolkit.getDefaultToolkit().beep();
                }
                textArea.requestFocus();
            }
        });
        oToolBar.add(btnfbrac);

        btnbbrac = new ToolBarButton();
        btnbbrac.setFocusable(false);
        btnbbrac.setPreferredSize(new Dimension(20, 20));
        btnbbrac.setBorder(BorderFactory.createEmptyBorder(0, 0, 0, 0));
//        btnbbrac.setToolTipText(Language.getString("EXPORT_HISTORY"));
        btnbbrac.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                int index = textArea.getCaretPosition();
                try {
                    doc.insertString(index," )",textArea.getStyle("BLACK"));
                } catch (BadLocationException e1) {
                    java.awt.Toolkit.getDefaultToolkit().beep();
                }
                textArea.requestFocus();
            }
        });
        oToolBar.add(btnbbrac);


        oToolBar.setBackground(Theme.getColor("TOOLBAR_BGCOLOR"));
        oToolBar.setForeground(Theme.getColor("TOOLBAR_FGCOLOR"));
        return oToolBar;
    }

    public String getMethod(String data){
        int charAt = 0;
        String outputString = "";
        String word = null;
        StringTokenizer token = new StringTokenizer(data,(operatorsString+additonalSeperators),true);
        while(token.hasMoreElements()){
            try {
                word = token.nextToken();
                charAt = data.indexOf(word, charAt);
                if(table.containsKey(word.toUpperCase())){
                    if(((FormularObject)table.get(word.toUpperCase())).isOperator())
                        outputString = outputString + ((FormularObject)table.get(word.toUpperCase())).getMethodName();
                    else
                        outputString = outputString + "(double)"+ ((FormularObject)table.get(word.toUpperCase())).getMethodName();
                }else{
                    outputString = outputString + word;
                }
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        return outputString;
    }

    public void clearField(){
        nameField.setText("");
        textArea.setText("");
        try {
            doc.remove(0, doc.getLength());
        } catch (BadLocationException e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        SwingUtilities.updateComponentTreeUI(popupMenu);
        wordList.setSelectionBackground(Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR"));
        wordList.setSelectionForeground(Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR"));
        helpContent.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/helpcontents.gif"));
        setStyles();
    }

    private void loadPropertyFile(){
         try {
            String record;
            Decompress decompress = new Decompress();
//            ByteArrayOutputStream out = decompress.setFiles(Settings.CONFIG_DATA_PATH +"/customformula_"+Language.getSelectedLanguage()+".msf");
            ByteArrayOutputStream out = decompress.setFiles(Settings.CONFIG_DATA_PATH +"/customformula.msf");
            decompress.decompress();
            DataInputStream in = new DataInputStream(new ByteArrayInputStream(out.toByteArray()));
            do{
                record = SharedMethods.readLine(in);
                if (record != null) {
                    record = record.trim();
                    String[] fields = record.split(Meta.DS);
                    String key = fields[0];
                    FormularObject object = new FormularObject(key);
                    object.setStyle(textArea.getStyle("BLUE"));
                    object.setMethodName(fields[1]);
                    object.setName(Language.getString(fields[2]));
//                    object.setName(UnicodeUtils.getNativeString(fields[2]));
//                    object.setName(Language.getLanguageSpecificString(fields[2]));
                    object.setOperator(false);
                    table.put(key.toUpperCase(), object);

                    nameArray.add(new ListObject(key, object.getName()));
                    try {
                        interpreter.set(key.toUpperCase(),1);
                    } catch (EvalError evalError) {
                        evalError.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                    object = null;
                }
            } while (record != null);
            out = null;
            decompress = null;
            in.close();
            in = null;
             try {
                 Collections.sort(nameArray );
             } catch (Exception e) {
                 e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
             }
         } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void setStyles(){

        try {
            textArea.removeStyle("RED");
            textArea.removeStyle("BLUE");
            textArea.removeStyle("BLACK");
            textArea.removeStyle("GREEN");
        } catch (Exception e) {
        }
        Style style = textArea.addStyle("RED", null);
        StyleConstants.setForeground(style, Theme.getColor("FORMULA_EDITOR_STYLE_RED"));
        StyleConstants.setBold(style,false);
        style = textArea.addStyle("BLUE",null);
        StyleConstants.setBold(style,true);
        StyleConstants.setForeground(style, Theme.getColor("FORMULA_EDITOR_STYLE_BLUE"));
        style = textArea.addStyle("BLACK",null);
        StyleConstants.setBold(style,true);
        StyleConstants.setForeground(style, Theme.getColor("FORMULA_EDITOR_STYLE_BLACK"));
        style = textArea.addStyle("GREEN",null);
        StyleConstants.setBold(style,true);
        StyleConstants.setForeground(style, Theme.getColor("FORMULA_EDITOR_STYLE_GREEN"));
        getWords();
    }

    public void menuSelected(MenuEvent e) {
        if(e.getSource() == modifyMenu){
            createFormulaList(true);
            GUISettings.applyOrientation(modifyMenu);
        } else if( e.getSource() == deleteMenu){
            createFormulaList(false);
            GUISettings.applyOrientation(deleteMenu);
        } else if( e.getSource() == formulaMenu){
            if(CustomFormulaStore.getSharedInstance().getColumnCount() <=0){
                modifyMenu.setVisible(false);
                deleteMenu.setVisible(false);
            } else {
                modifyMenu.setVisible(true);
                deleteMenu.setVisible(true);
            }
        } else if(e.getSource() == addToWindows){
            addToWindows.removeAll();
            JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
            for (int i = 0; i < frames.length; i++) {
                if (frames[i].isVisible() && !frames[i].isIcon()) {
                    if(frames[i] instanceof CustomFormulaListener){
                        TWMenuItem item = new TWMenuItem((frames[i].getTitle()));
                        item.setText((frames[i].getTitle()));
                        final ClientTable table = (ClientTable)frames[i];
                        item.addActionListener(new ActionListener() {
                            public void actionPerformed(ActionEvent e) {
                                getWords();
                                if(createFormula(false)){
                                    table.addColumn(nameField.getText());
                                    exitWindow();
                                    showMessage();
                                }
                            }
                        });
                        addToWindows.add(item);
                        item = null;
                    }
                }
            }
            frames = null;
            GUISettings.applyOrientation(addToWindows);

        } else if(e.getSource() == helpMenu){
            NativeMethods.showHelpItem(HelpManager.getFileNameFromItemId("HELP_CUSTOM_COLUMNS"));
        }
    }

    public void exitWindow(){
//        self = null;
        clearField();
        this.setVisible(false);
    }

    private void createPopup(){
        popupMenu = new JPopupMenu();

        saveOnly = new TWMenuItem(Language.getString("SAVE_ONLY"),"formulasave.gif");
        saveOnly.addActionListener(this);
        popupMenu.add(saveOnly);

        addToWindows = new TWMenu(Language.getString("ADD_TO_WINDOWS"),"formuladdone.gif");
        addToWindows.addMenuListener(this);
        popupMenu.add(addToWindows);

        addToshowings = new TWMenuItem(Language.getString("ADD_TO_SHOWINGS"),"formuladdshowing.gif");
        addToshowings.addActionListener(this);
        popupMenu.add(addToshowings);

        addToAll = new TWMenuItem(Language.getString("ADD_TO_ALL"),"formuladdall.gif");
        addToAll.addActionListener(this);
        popupMenu.add(addToAll);
    }

    public void menuDeselected(MenuEvent e) {
    }

    public void menuCanceled(MenuEvent e) {
    }

    private class ListObject implements Comparable{
        String key;
        String text;
        public ListObject(String key, String name){
            this.key = key;
            this.text = name;
        }

        @Override
        public String toString() {
//            return text + " : " + key;
            return key;
        }

        public String getText(){
//            return text;
            return key;
        }

        public String getKey(){
            return key;
        }

        public int compareTo(Object o) {
            return this.getText().compareTo(((ListObject)o).getText());  //To change body of implemented methods use File | Settings | File Templates.
        }
    }

    private class NameComapre implements Comparator{
        public int compare(Object o1, Object o2) {
            return ((ListObject ) o1).getText().compareTo(((ListObject ) o2).getText() );
        }
    }
}
