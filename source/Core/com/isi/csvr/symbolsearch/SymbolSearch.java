// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.symbolsearch;

import com.isi.csvr.Client;
import com.isi.csvr.DataDisintegrator;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.bandwidthoptimizer.OptimizedTreeRenderer;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.datastore.*;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.*;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Enumeration;
import java.util.Vector;
import java.io.*;

/**
 * A Swing-based dialog class to show companies.
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class
        SymbolSearch extends JDialog implements MouseListener,
        ActionListener, WindowListener, KeyListener, WindowWrapper,
        Themeable, ExchangeListener, InternalFrameListener {

    private JRadioButton radioCompNameContain;
    private JRadioButton radioCompNameStart;
    private JRadioButton radioSymbol;
    private ButtonGroup btnGroup;
    private JLabel lblSector;
    private JList lstSelected = new JList();
    //    private JList lstAvailable;
    private Table tblAll;
    private TWButton btnCancel;
    private TWButton btnApply;
    private TWButton btnSearch;
    //private JScrollPane oScrollPane1;
    private JScrollPane oScrollPane2;
    private JPanel contentPane;

    private TWComboBox cmbExchanges;
    private TWComboModel exchangeModel;
    private ArrayList<TWComboItem> exchangeList;

    private TWComboBox g_cmbSymbolTypes;
    private TWTextField g_txtDescription;

    private Symbols g_oSymbols;
    private String[] g_sAllData;
    private Vector g_vecAllData;
    private Vector g_vecSelectedData;
    private TWButton btnAdd;
    private TWButton btnRemove = new TWButton();
    TWButton prevButton;
    TWButton nextButton;

    private boolean singleMode = false;

    private static SymbolSearch self = null;
    private String windowTitle = "";

    private byte currentPage = 0;
    private DynamicArray searchIndex;
    private boolean searchInProgress;
    private boolean showDefaultExchangesOnly;

    private int searchType = 0;
    private int symbolType = 0;
    private String selectedExchange;
    private int selectedIndex;
    private ViewSetting oSettings;
    private boolean isIndexMode=false;
    private boolean isDefaultMarketOnly=false;

    /**
     * Constructs a new instance.
     *
     * @param title
     */
    private SymbolSearch(String title, boolean singleMode) {
//        super(title, false);
//        super((Window)SwingUtilities.getAncestorOfClass(Window.class,invoker), title, Dialog.ModalityType.APPLICATION_MODAL);
        super(Client.getInstance().getFrame(), title, true);
        try {
            this.singleMode = singleMode;
            this.windowTitle = title;
            searchIndex = new DynamicArray();
            jbInit();
            setButtonsEnabled(true);
//            setClosable(true);
            prevButton.setEnabled(false);
            nextButton.setEnabled(false);
            addWindowListener(this);
//            addInternalFrameListener(this);
//            progressIndicator = new Timer(100, this);
            Theme.registerComponent(this);
            ExchangeStore.getSharedInstance().addExchangeListener(this);
            isIndexMode=false;
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setSingleMode(boolean mode) {
        this.singleMode = mode;
        if (this.singleMode)
            tblAll.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        else
            tblAll.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
    }

    public boolean isShowDefaultExchangesOnly() {
        return showDefaultExchangesOnly;
    }

    public void setShowDefaultExchangesOnly(boolean showDefaultExchangesOnly) {
        this.showDefaultExchangesOnly = showDefaultExchangesOnly;
    }

    public void setDefaultMarketOnly(boolean isDefaultMarketOnly){
        this.isDefaultMarketOnly = isDefaultMarketOnly;
    }

//	public SymbolSearch(Frame parent, String title, boolean modal){
//		   this(parent, title, modal, false);
//	}
//
//
//    public void setExtraSymbol(boolean isExtraSymbol) {
//        this.isExtraSymbol = isExtraSymbol;
//    }

//    public void setSubMode(boolean subMode) {
//        this.isSubMode = subMode;
//    }

    public void setButtonsEnabled(boolean status) {
        btnApply.setEnabled(status);
        btnCancel.setEnabled(status);
        btnSearch.setEnabled(status);
        setSearchInProgress(!status);
//        prevButton.setEnabled(status);
//        nextButton.setEnabled(status);
    }

    /*public void setSearchCompleted(boolean haveData) {
        stopProgressAnimation(haveData);
        setSearchInProgress(false);
    }*/

    public boolean isSearchInProgress() {
        return searchInProgress;
    }

    public void setSearchInProgress(boolean searchInProgress) {
        this.searchInProgress = searchInProgress;
    }

    /*
   public void setSymbolType(byte type) {
       this.symbolType = type;
   } */

    /**
     * Constructs a new non-modal unparented instance with a blank title.
     *
     public Companies() {
     this(Client.getInstance(), "", true, false, Meta.QUOTE);
     } */

    /**
     * Constructs a new non-modal unparented instance with a blank title.
     */
//	public SymbolSearch(String title,boolean isFromQuoteStoreIn){//, byte symbolType) {
//		this(Client.getInstance(), Language.getString("WINDOW_TITLE_SEARCH")+ " - " + title, true, false);
//	}
    public synchronized static SymbolSearch getSharedInstance() {
        if (self == null) {
            self = new SymbolSearch("", false);
        }
        return self;
    }

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {

        btnAdd = new TWButton();
        btnCancel = new TWButton();
        btnApply = new TWButton();
        btnSearch = new TWButton();

        contentPane = new JPanel();
        contentPane.setPreferredSize(new Dimension(650, 400));
        final DataDisintegrator oTags = new DataDisintegrator();
        oTags.setSeperator(Meta.FS);

        g_vecAllData = new Vector(100);

        final ImageIcon icon = new ImageIcon("images/theme" + Theme.getID() + "/busy.gif");
        final String searching = Language.getString("SEARCHING");

        tblAll = new Table() {
            public void paint(Graphics g) {
                super.paint(g);
                if (isSearchInProgress()) {
                    FontMetrics fontMetrics = tblAll.getFontMetrics(getFont());
                    g.drawString(searching, (getWidth() - (fontMetrics.stringWidth(searching)) / 2)-275 ,
                            (getHeight()) / 2);
  // g.drawString(searching, (getWidth() - fontMetrics.stringWidth(searching)) / 2,
  //                          (getHeight()) / 2);
                    g.drawImage(icon.getImage(),
                            (getWidth() - (fontMetrics.stringWidth(searching)) / 2)-275 ,
                            (getHeight() - icon.getImage().getHeight(this)) / 2+ 15, this);
                    //+ fontMetrics.stringWidth(searching) + 10
                    fontMetrics = null;
                }
            }
        };
        tblAll.setPopupActive(false);
        tblAll.setHeaderPopupActive(false);
        tblAll.setSortingEnabled();

        SymbolSearchTableModel allModel = new SymbolSearchTableModel();
        ViewSetting viewSetting = ViewSettingsManager.getSummaryView("SYMBOL_SEARCH");
        allModel.setViewSettings(viewSetting);
        viewSetting.setParent(this);
//        GUISettings.setColumnSettings(allModel.getViewSettings(), TWColumnSettings.getItem("SYMBOL_SEARCH_COLS"));
        tblAll.setModel(allModel);
        allModel.setTable(tblAll);
        allModel.setDataStore(g_vecAllData);
        allModel.applySettings();
        tblAll.killThread(); //no need of a thread
        tblAll.getTable().setRowSelectionAllowed(true);
//        tblAll.getTable().setColumnSelectionAllowed(true);
//        tblAll.setSortingEnabled();
        tblAll.getTable().addMouseListener(this);
//        lstAvailable = new JList(g_vecAllData);
//        lstAvailable.addMouseListener(this);
        JPanel tablePanel = new JPanel(new BorderLayout());
        tablePanel.setPreferredSize(new Dimension(500, 250));

        if (isSingleMode())
            tblAll.getTable().setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
        else
            tblAll.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);

//        oScrollPane1 = new JScrollPane(lstAvailable);
//        oScrollPane1.setAutoscrolls(true);
//        oScrollPane1.setPreferredSize(new Dimension(200, 250));
        JPanel oPanel1 = new JPanel(new BorderLayout());
        oPanel1.add(new JLabel(Language.getString("LBL_UNSELECTED_ITEMS")), BorderLayout.NORTH);
        tablePanel.add(tblAll, BorderLayout.CENTER);
        oPanel1.add(tablePanel, BorderLayout.CENTER);


        JPanel nextPanel = new JPanel();
        nextPanel.setLayout(new BoxLayout(nextPanel, BoxLayout.X_AXIS));
        prevButton = new TWButton(Language.getString("PREVIOUS"));
        prevButton.setBorder(BorderFactory.createEmptyBorder());
        prevButton.setGradientPainted(false);
        prevButton.setActionCommand("PREV");
        prevButton.addActionListener(this);
        nextButton = new TWButton(Language.getString("NEXT"));
        nextButton.setBorder(BorderFactory.createEmptyBorder());
        nextButton.setGradientPainted(false);
        nextButton.setActionCommand("NEXT");
        nextButton.addActionListener(this);
        nextPanel.add(prevButton);
        nextPanel.add(Box.createHorizontalGlue());
        nextPanel.add(nextButton);
        tablePanel.add(nextPanel, BorderLayout.SOUTH);
        GUISettings.applyOrientation(nextPanel);
        tablePanel.setBorder(BorderFactory.createEtchedBorder());

        g_vecSelectedData = new Vector();

        lstSelected = new JList(g_vecSelectedData);
        lstSelected.addMouseListener(this);

        oScrollPane2 = new JScrollPane(lstSelected);
        oScrollPane2.setPreferredSize(new Dimension(100, 250));
        oScrollPane2.setAutoscrolls(true);
        JPanel oPanel2 = new JPanel(new BorderLayout());
        oPanel2.add(new JLabel(Language.getString("LBL_SELECTED_ITEMS")), BorderLayout.NORTH);
        oPanel2.add(oScrollPane2, BorderLayout.CENTER);

        //btnAdd.addKeyListener(this);
        btnAdd.addActionListener(this);
        btnAdd.setActionCommand("D");
        btnAdd.setPreferredSize(new Dimension(60, 20));

        //btnRemove.addKeyListener(this);

        btnRemove.addActionListener(this);
        btnRemove.setActionCommand("R");
        btnRemove.setPreferredSize(new Dimension(60, 20));

        btnCancel.setText(Language.getString("CANCEL"));
        //btnCancel.addKeyListener(this);
        btnCancel.setPreferredSize(new Dimension(100, 25));
        btnCancel.addActionListener(this);
        btnCancel.setActionCommand("C");


        btnApply.setText(Language.getString("OK"));
        //btnApply.addKeyListener(this);
        btnApply.setPreferredSize(new Dimension(100, 25));
        btnApply.addActionListener(this);
        btnApply.setActionCommand("A");

        contentPane.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));


        JPanel oSearchPanel = getSearchPanel();
        contentPane.add(oSearchPanel);

        contentPane.add(oPanel1);
        JPanel addRemovePanel = new JPanel();
        addRemovePanel.setLayout(new FlowLayout(FlowLayout.CENTER, 0, 5));

        /* Add a transparent label to fill a vertical gap to move buttons dowmn*/
        JLabel blankLabel = new JLabel();
        blankLabel.setPreferredSize(new Dimension(60, 100));
        addRemovePanel.add(blankLabel);

        if (Language.isLTR())
            btnAdd.setText(">>");
        else
            btnAdd.setText("<<");

        addRemovePanel.add(btnAdd);

        if (Language.isLTR())
            btnRemove.setText("<<");
        else
            btnRemove.setText(">>");

        addRemovePanel.add(btnRemove);
        addRemovePanel.add(Box.createVerticalGlue());
        addRemovePanel.setPreferredSize(new Dimension(70, 250));
        contentPane.add(addRemovePanel);
        contentPane.add(oPanel2);

        JPanel oButtons = new JPanel();
        oButtons.setPreferredSize(new Dimension(680, 25));
        oButtons.setLayout(new FlowLayout(FlowLayout.TRAILING, 0, 0));

        oButtons.add(btnApply);

        // add a dummy label to get a gap between two buttons
        JLabel dummy = new JLabel();
        dummy.setPreferredSize(new Dimension(5, 25));
        oButtons.add(dummy);

        oButtons.add(btnCancel);
        contentPane.add(oButtons);

        this.getContentPane().add(contentPane);

        this.setDefaultCloseOperation(HIDE_ON_CLOSE);
        this.applySettings();
    }

    public void initScrollPanes() {
        if (!Language.isLTR()) {
//            oScrollPane1.getHorizontalScrollBar().setValue(oScrollPane1.getHorizontalScrollBar().getMaximum());
            oScrollPane2.getHorizontalScrollBar().setValue(oScrollPane2.getHorizontalScrollBar().getMaximum());
        }
    }

    /**
     * Create the Search Panel
     */
    private JPanel getSearchPanel() {
        JPanel oPanel = new JPanel();
        oPanel.setPreferredSize(new Dimension(680, 28));
        oPanel.setLayout(new FlexGridLayout(new String[]{"120","100","60","150","60","80","3","100%"},new String[]{"0"},2,3));
//        oPanel.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 5));

        // ---- symbol and description
        JPanel descPanel = new JPanel(new FlowLayout(FlowLayout.LEADING, 5, 0));
        radioCompNameStart = new JRadioButton(Language.getString("COMPANY_NAME_START"));
        radioCompNameContain = new JRadioButton(Language.getString("COMPANY_NAME_CONTAIN"));
        radioSymbol = new JRadioButton(Language.getString("SYMBOL"));
        btnGroup = new ButtonGroup();
        btnGroup.add(radioCompNameContain);
        btnGroup.add(radioCompNameStart);
        btnGroup.add(radioSymbol);
        radioCompNameStart.setSelected(true);
        descPanel.setPreferredSize(new Dimension(470, 22));

        descPanel.add(radioSymbol);
        descPanel.add(radioCompNameStart);
        descPanel.add(radioCompNameContain);
        descPanel.addKeyListener(this);
        //high
//        oPanel.add(descPanel);
        JLabel label = new JLabel(Language.getString("SYMBOL_DESCRIPTION"));
        label.setPreferredSize(new Dimension(470, 22));
        oPanel.add(label);
        oPanel.addKeyListener(this);
        radioCompNameStart.addKeyListener(this);
        radioCompNameContain.addKeyListener(this);
        radioSymbol.addKeyListener(this);

        g_txtDescription = new TWTextField();
        g_txtDescription.setPreferredSize(new Dimension(200, 22));
        oPanel.add(g_txtDescription);
        g_txtDescription.addKeyListener(this);

        // ----  exchange
        lblSector = new JLabel(Language.getString("EXCHANGE"));
        lblSector.setHorizontalAlignment(JLabel.LEADING);
        lblSector.setPreferredSize(new Dimension(470, 22));
        oPanel.add(lblSector);


        exchangeList = new ArrayList<TWComboItem>();
        exchangeModel = new TWComboModel(exchangeList);
        cmbExchanges = new TWComboBox(exchangeModel);
        cmbExchanges.addActionListener(this);
        cmbExchanges.setActionCommand("EXCHANGES");
        cmbExchanges.setPreferredSize(new Dimension(200, 22));
        oPanel.add(cmbExchanges);

        //cmbExchanges.addItem(Language.getString("COMMON_ALL"));

//        for (int i=0; i<excgArray.length; i++) {
//            cmbExchanges.addItem(excgArray[i]); // (String)excgArray.elementAt(i));
//        }
        /*if (storeType == Meta.CMEOPRAQUOTE){
           cmbExchanges.setSelectedItem("CME");
           cmbExchanges.setEnabled(false);
        }else if (storeType == Meta.FOREXOPRAQUOTE){
           cmbExchanges.setSelectedItem("PFCO");
           cmbExchanges.setEnabled(false);
        }*/

        // ----  symbol type
        JLabel lblSymbolType = new JLabel(Language.getString("CATEGORY"));
        lblSymbolType.setHorizontalAlignment(JLabel.LEADING);
        lblSymbolType.setPreferredSize(new Dimension(470, 22));
        oPanel.add(lblSymbolType);

        Object[] symbolTypes = new Object[7];
        SymbolTypeRecord record = new SymbolTypeRecord(Language.getString("ALL"), Meta.SEARCH_ALL);
        symbolTypes[0] = record;
        record = new SymbolTypeRecord(Language.getString("SYMBOL_TYPE_EQUITY"), Meta.SYMBOL_TYPE_EQUITY);
        symbolTypes[1] = record;
        record = new SymbolTypeRecord(Language.getString("SYMBOL_TYPE_INDEX"), Meta.SYMBOL_TYPE_INDEX);
        symbolTypes[2] = record;
        record = new SymbolTypeRecord(Language.getString("SYMBOL_TYPE_OPTION"), Meta.SYMBOL_TYPE_OPTIONS);
        symbolTypes[3] = record;
        record = new SymbolTypeRecord(Language.getString("SYMBOL_TYPE_BONDS"), Meta.SYMBOL_TYPE_BONDS);
        symbolTypes[4] = record;
        record = new SymbolTypeRecord(Language.getString("SYMBOL_TYPE_FOREX"), Meta.SYMBOL_TYPE_FOREX);
        symbolTypes[5] = record;
        record = new SymbolTypeRecord(Language.getString("SYMBOL_TYPE_MUTUAL_FUND"), Meta.SYMBOL_TYPE_MUTUALFUND);
        symbolTypes[6] = record;
//		record = new SymbolTypeRecord(Language.getString("FOREX_SPOT"),Meta.FOREX_SPOT);
//		symbolTypes[5] = record;
//		record = new SymbolTypeRecord(Language.getString("FOREX_FORWARDS"),Meta.FORWARDS);
//		symbolTypes[6] = record;
//		record = new SymbolTypeRecord(Language.getString("COMMODITY"),Meta.ICAP_COMMODITY);
//		symbolTypes[7] = record;
//		record = new SymbolTypeRecord(Language.getString("EUROBOND"),Meta.EUROBOND);
//		symbolTypes[8] = record;
//		record = new SymbolTypeRecord(Language.getString("EURO_GOVVIE"),Meta.EURO_GOVVIE);
//		symbolTypes[9] = record;

        g_cmbSymbolTypes = new TWComboBox(symbolTypes);
        g_cmbSymbolTypes.setSelectedIndex(1);
        //g_cmbSymbolTypes.set
        g_cmbSymbolTypes.setPreferredSize(new Dimension(200, 22));
        oPanel.add(g_cmbSymbolTypes);
        // ---- search button

        btnSearch.setText(Language.getString("SEARCH"));
        //btnSearch.addKeyListener(this);

        btnSearch.setActionCommand("S");
        btnSearch.setPreferredSize(new Dimension(200, 22));
        btnSearch.addActionListener(this);

//        JPanel serchButtonPanel = new JPanel(new FlowLayout(FlowLayout.TRAILING, 5, 0));
//
//        serchButtonPanel.setPreferredSize(new Dimension(680, 26));
//        serchButtonPanel.add(btnSearch);
        oPanel.add(new JLabel());
        oPanel.add(btnSearch);
        return oPanel;
    }

    /**
     * Sets the symbol object's reference to this
     */
    public void setSymbols(Symbols oSymbols) {
        g_oSymbols = oSymbols;
    }

    public void init() {
        String sKey = "";
        String dataArray[] = null;
        Stock oStock = null;
        Enumeration enumSymbols = null;
        //Vector      dataVector  = new Vector(1000, 1);

        try {
            //if (isFromQuoteStore) {
            //radioCompName.setEnabled(false);
            //radioSymbol.setEnabled(false);
            //cmbExchanges.setEnabled(false);
            //g_txtDescription.setEnabled(false);
            //btnSearch.setEnabled(false);
            //lblSector.setEnabled(false);

            oStock = null;

//                enumSymbols = QuoteStore.getSymbols();
//                while (enumSymbols.hasMoreElements()) {
//                    sKey   = (String)enumSymbols.nextElement();
//                    oStock = QuoteStore.getStockObject(sKey);
//
//					SymbolSearchListItem item = new SymbolSearchListItem(oStock.getExchangeCode(),
//							oStock.getSymbol(),oStock.getCompanyName(),""+oStock.getInstrumentType());
//                    g_vecAllData .addElement(item);
//                    dataArray 	= null;
//                    oStock    	= null;
//					item 		= null;
//                }
//                sKey        = null;
//                enumSymbols = null;

            //lstSelected.rem
            populateExchanges();

            g_vecAllData.clear();
            g_vecAllData.trimToSize();
            g_vecSelectedData.clear();
            g_vecSelectedData.trimToSize();
//
            String[] symbols = g_oSymbols.getSymbols();
            for (int i = 0; i < symbols.length; i++) {
                Stock stock = DataStore.getSharedInstance().getStockObject(symbols[i]);
                if (stock != null) {
                    //todo - changed 26/12/2006
                    SymbolSearchListItem item = new SymbolSearchListItem(stock.getExchange(),
                            stock.getSymbolCode(), stock.getLongDescription(), "" + stock.getInstrumentType(), "", stock.getShortDescription(),stock.getSymbolCodeWithMarkets());
                    g_vecSelectedData.add(item);
                    item = null;
                    stock = null;
                }
            }

            try {
                if (selectedExchange != null) {
                    cmbExchanges.setSelectedIndex(selectedIndex);
                } else {
                    cmbExchanges.setSelectedIndex(0);
                }
                 g_txtDescription.setText("");
            } catch (Exception e) {
                e.printStackTrace();
            }

//            lstAvailable.updateUI();
            lstSelected.updateUI();
            tblAll.getTable().updateUI();
            nextButton.setEnabled(false);
            prevButton.setEnabled(false);
            currentPage = 0;
            searchIndex.clear();
            //}
            //processDataVector(dataVector);
            //dataVector  = null;
        } catch (Exception e) {
        }
    }

    public void setSelectedExchange(String exchangeID) {
        if (exchangeList.size() < 1)
            populateExchanges();
        this.selectedExchange = exchangeID;
//        try {
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if (exchange.getSymbol().equals(exchangeID)) {
                selectedIndex = exchangeList.indexOf(new TWComboItem(exchange.getSymbol(), exchange.getDescription()));
                cmbExchanges.setSelectedIndex(selectedIndex);
                break;
            }
                exchange = null;
        }
        exchanges=null;

    }

    private void populateExchanges() {
        exchangeList.clear();
//        cmbExchanges.removeAllItems();
        StringBuilder allExchanges = new StringBuilder();
        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exchange = (Exchange) exchanges.nextElement();
            if ((!isShowDefaultExchangesOnly()) || ((isShowDefaultExchangesOnly()) && (exchange.isDefault()))) {
                exchangeList.add(new TWComboItem(exchange.getSymbol(), exchange.getDescription()));
                allExchanges.append(",");
                allExchanges.append(exchange.getSymbol());
            }
            exchange = null;
        }
        exchanges = null;
        Collections.sort(exchangeList);
        if (!isShowDefaultExchangesOnly() && exchangeList.size() > 1) {
            exchangeList.add(0, new TWComboItem(allExchanges.toString().substring(1), Language.getString("ALL")));
        }
//        exchangeList.add(new TWComboItem(allExchanges.toString().substring(1), "new"));
        cmbExchanges.updateUI();
        tblAll.getTable().updateUI();
    }

    public void setSearchedSymbols(String sData) {
        String dataRecord = null;
        String[] stFields = null;
        short count = 0;
        String description = g_txtDescription.getText().trim();
        String tempFilter = description;

        String align;
        String width;

        if (Language.isLTR()) {
            align = " align=left ";
        } else {
            align = " align=right ";
        }

        if (Language.isLTR()) {
            width = " style=\"width: 2000\" ";
        } else {
            width = "";
        }

        try {
            String[] stRecords = sData.split(Meta.FD);
            boolean searchCompleted = stRecords[0].equals("0");
            boolean searchFailed=false;
            if(stRecords[0].equals("-1")){
               searchFailed=true;
            }
            g_vecAllData.clear();
            g_vecAllData.trimToSize();
            if ((stRecords.length > 1)&&(!searchFailed)) {
                for (int i = 1; i < stRecords.length; i++) {
                    try {
                        count++;
                        dataRecord = stRecords[i];
                        stFields = dataRecord.split(Meta.ID);
                        String shortDesc = "";
                        try {
                            shortDesc = stFields[8];
                        } catch (Exception e) {
                            shortDesc = "";
                        }

                        SymbolSearchListItem item = new SymbolSearchListItem(stFields[0],
                                stFields[1], Language.getLanguageSpecificString(stFields[4]), stFields[2], dataRecord, shortDesc,stFields[1]);
                        g_vecAllData.addElement(item);
                        if (tempFilter.length() > 0) {
                            int filterIndex = SharedMethods.getTextFromHTML(tempFilter).length();
                            int index = 0;
                            if ((index = item.getShortDescription().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                String str = SharedMethods.getTextFromHTML(item.getShortDescription());
                                item.setShortDescription(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + filterIndex) + "</span>" + str.substring(index + filterIndex) + "</div></html>"));
                                str = null;
                            } else {
                                String str = SharedMethods.getTextFromHTML(item.getShortDescription());
                                item.setShortDescription(str);
                            }
                            index = 0;
                            if ((index = item.getDescription().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                String str = SharedMethods.getTextFromHTML(item.getDescription());
                                item.setDescription(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + filterIndex) + "</span>" + str.substring(index + filterIndex) + "</div></html>"));
                                str = null;
                            } else {
                                String str = SharedMethods.getTextFromHTML(item.getDescription());
                                item.setDescription(str);
                            }
                            index = 0;
                            if ((index = item.getSymbol().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                String str = SharedMethods.getTextFromHTML(item.getSymbol());
                                item.setSymbol(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + filterIndex) + "</span>" + str.substring(index + filterIndex) + "</div></html>"));
                                str = null;
                            } else {
                                String str = SharedMethods.getTextFromHTML(item.getSymbol());
                                item.setSymbol(str);
                            }
                        }
                        stFields = null;
                        dataRecord = null;
                        item = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
                setSearchInProgress(false);
            } else {
                setSearchInProgress(false);
                if (searchFailed) {
                    String str=Language.getString("SEARCH_FAILED");
                    try {
                        str = str.replace("[REASON]", stRecords[1].split(":")[1]);
                    } catch (Exception e) {
                        str = str.replace("[REASON]", stRecords[1]);
                    }

                    SharedMethods.showMessage(str, JOptionPane.INFORMATION_MESSAGE);
                } else {
                SharedMethods.showMessage(Language.getString("NO_RESULTS_FOUND"), JOptionPane.INFORMATION_MESSAGE);
                }
            }
            updateSearchIndex(count);

            if (!searchCompleted) {
                nextButton.setEnabled(true);
            } else {
                nextButton.setEnabled(false);
            }


            setPrevStatusAfterSearch();
            setButtonsEnabled(true);
            tblAll.getTable().updateUI();
            stRecords = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadDefaultExchangeData(String exchange) {
        String description = g_txtDescription.getText().trim().toUpperCase();
        String tempFilter = description;
        boolean selected;
        Exchange excg = ExchangeStore.getSharedInstance().getExchange(exchange);
        boolean isNeedSubmarketCheck = isDefaultMarketOnly && excg.hasSubMarkets() && excg.isUserSubMarketBreakdown();
        Enumeration<Stock> stocks = DataStore.getSharedInstance().getExchangeSymbolStore(exchange).elements();
        searchType = Meta.SEARCH_ALL_NAMES;

        String align;
        String width;

        if (Language.isLTR()) {
            align = " align=left ";
        } else {
            align = " align=right ";
        }

        if (Language.isLTR()) {
            width = " style=\"width: 500\" ";
        } else {
            width = "";
        }

        while (stocks.hasMoreElements()) {
            Stock stock = stocks.nextElement();
            if(isNeedSubmarketCheck){
                if(!excg.getDefaultMarket().getSymbol().equals(stock.getMarketID())){
                    continue;
                }
            }
            selected = false;
            switch (searchType) {
                case Meta.SEARCH_SYMBOL:
                    if ((stock.getSymbolCode().toUpperCase().startsWith(description)) &&
                            ((symbolType == Meta.SEARCH_ALL) || (symbolType == SharedMethods.getSymbolType(stock.getInstrumentType())))) {
                        selected = true;
                        SymbolSearchListItem item = new SymbolSearchListItem(exchange,
                                stock.getSymbolCode(), stock.getLongDescription(), "" + stock.getInstrumentType(), null, stock.getShortDescription(),stock.getSymbolCodeWithMarkets());
                        g_vecAllData.addElement(item);
                        item = null;
//                        stock = null;
                    }
                    break;
                case Meta.SEARCH_CONTAINS_DESCRIPTION:
                    if ((stock.getLongDescription().toUpperCase().indexOf(description) >= 0) &&
                            ((symbolType == Meta.SEARCH_ALL) || (symbolType == SharedMethods.getSymbolType(stock.getInstrumentType())))) {
                        selected = true;
                        SymbolSearchListItem item = new SymbolSearchListItem(exchange,
                                stock.getSymbolCode(), stock.getLongDescription(), "" + stock.getInstrumentType(), null, stock.getShortDescription(),stock.getSymbolCodeWithMarkets());
                        g_vecAllData.addElement(item);
                        item = null;
//                        stock = null;
                    }
                    break;
                case Meta.SEARCH_STARTS_WITH_DESCRIPTION:
                    if ((stock.getLongDescription().toUpperCase().startsWith(description)) &&
                            ((symbolType == Meta.SEARCH_ALL) || (symbolType == SharedMethods.getSymbolType(stock.getInstrumentType())))) {
                        selected = true;
                        SymbolSearchListItem item = new SymbolSearchListItem(exchange,
                                stock.getSymbolCode(), stock.getLongDescription(), "" + stock.getInstrumentType(), null, stock.getShortDescription(),stock.getSymbolCodeWithMarkets());
                        g_vecAllData.addElement(item);
                        item = null;
//                        stock = null;
                    }
                    break;
                case Meta.SEARCH_ALL_NAMES:
                    if ((stock.getLongDescription().toUpperCase().indexOf(description) >= 0) || (stock.getSymbolCode().toUpperCase().indexOf(description) >= 0)
                            || (stock.getShortDescription().toUpperCase().indexOf(description) >= 0)) {
                        if (((symbolType == Meta.SEARCH_ALL) || (symbolType == SharedMethods.getSymbolType(stock.getInstrumentType())))) {
                            boolean isAdded = false;
                            SymbolSearchListItem item = new SymbolSearchListItem(exchange,
                                    stock.getSymbolCode(), stock.getLongDescription(), "" + stock.getInstrumentType(), null, stock.getShortDescription(),stock.getSymbolCodeWithMarkets());
                            if ((stock.getShortDescription().toUpperCase().indexOf(description)) >= 0) {
                                if (!isAdded) {
                                    g_vecAllData.addElement(item);
                                    isAdded = true;
                                }
                                if (tempFilter.length() > 0) {
                                    int index = 0;
                                    if ((index = item.getShortDescription().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                        String str = item.getShortDescription();
                                        item.setShortDescription(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length()) + "</div></html>"));
                                        str = null;
                                    }
                                }
                            }
                            if ((stock.getLongDescription().toUpperCase().indexOf(description)) >= 0) {
                                if (!isAdded) {
                                    g_vecAllData.addElement(item);
                                    isAdded = true;
                                }
                                if (tempFilter.length() > 0) {
                                    int index = 0;
                                    if ((index = item.getDescription().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                        String str = item.getDescription();
                                        item.setDescription(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length()) + "</div></html>"));
                                        str = null;
                                    }
                                }
                            }
                            if ((stock.getSymbolCode().toUpperCase().indexOf(description)) >= 0) {
                                if (!isAdded) {
                                    g_vecAllData.addElement(item);
                                    isAdded = true;
                                }
                                if (tempFilter.length() > 0) {
                                    int index = 0;
                                    if ((index = item.getSymbol().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                        String str = item.getSymbol();
                                        item.setSymbol(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length()) + "</div></html>"));
                                        str = null;
                                    }
                                }
                            }
                            item = null;
                            selected = true;
                        }
//                        stock = null;
                    }
                    break;
            }
            stock = null;
        }
        tblAll.getTable().updateUI();
        stocks = null;
    }

    public void searchAllIndexes(String exchanges){
        String description = g_txtDescription.getText().trim().toUpperCase();
        String tempFilter = description;
        SmartProperties indexMaster;
        boolean selected;

        String align;
        String width;
        if (Language.isLTR()) {
            align = " align=left ";
        } else {
            align = " align=right ";
        }
        if (Language.isLTR()) {
            width = " style=\"width: 500\" ";
        } else {
            width = "";
        }
        //Loading all indexes from exchange wise property files
        System.out.println("Test Exchange List : "+exchanges);
        try {
                indexMaster = new SmartProperties(Meta.DS);
                String key = null;
                String value = null;
                String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH +"/" + exchanges + "/index_" + Language.getSelectedLanguage() + ".msf";
                File f = new File(sLangSpecFileName);
                boolean bIsLangSpec = f.exists();
                if (bIsLangSpec) {
                    indexMaster.loadCompressed(sLangSpecFileName);
                }

                //Searching indexes
                Enumeration indexEnum=indexMaster.elements();
                while(indexEnum.hasMoreElements()){
                    String data=(String)indexEnum.nextElement();
                    try {
                    String[] field=data.split(Meta.FS);
                    if ((field[2].toUpperCase().indexOf(description) >= 0) || (field[5].toUpperCase().indexOf(description) >= 0)) {
                        if ((symbolType == Meta.SEARCH_ALL) ) {
                            boolean isAdded = false;
                            String dataRec=field[5]+Meta.ID+exchanges+Meta.ID+field[0]+Meta.ID+field[5]+Meta.ID+field[2]+Meta.ID+field[3]+Meta.ID+field[4]+Meta.ID+""+Meta.ID+field[5];
                            SymbolSearchListItem item = new SymbolSearchListItem(exchanges,field[5], field[2], "" + field[0],dataRec, field[5],field[5]);
                            if ((field[2].toUpperCase().indexOf(description)) >= 0) {
                                if (!isAdded) {
                                    DataStore.getSharedInstance().createStockObject(exchanges,field[2],Integer.parseInt(field[0]));
                                    g_vecAllData.addElement(item);
                                    isAdded = true;
                                }
                                if (tempFilter.length() > 0) {
                                    int index = 0;
                                    if ((index = item.getShortDescription().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                        String str = item.getShortDescription();
                                        item.setShortDescription(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length()) + "</div></html>"));
                                        str = null;
                                    }
                                }
                            }
                            if ((field[5].toUpperCase().indexOf(description)) >= 0) {
                                if (!isAdded) {
                                    DataStore.getSharedInstance().createStockObject(exchanges,field[2],Integer.parseInt(field[0]));
                                    g_vecAllData.addElement(item);
                                    isAdded = true;
                                }
                                if (tempFilter.length() > 0) {
                                    int index = 0;
                                    if ((index = item.getDescription().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                        String str = item.getDescription();
                                        item.setDescription(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length()) + "</div></html>"));
                                        str = null;
                                    }
                                }
                            }

                            item = null;
                            selected = true;
                        }
                    }
                    } catch (Exception e) {
                       // e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }


        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }


    }

    private void updateSearchIndex(short count) {
        if (searchIndex.size() >= currentPage) {
            return; // index is already there
        }

        SearchIndexRecord searchIndexRecord = new SearchIndexRecord();
        searchIndexRecord.lot = count;
        if (searchIndex.size() > 0) {
            SearchIndexRecord prevSearchIndexRecord = (SearchIndexRecord) searchIndex.get(searchIndex.size() - 1);
            searchIndexRecord.start = prevSearchIndexRecord.start + count;
        } else {
            searchIndexRecord.start = 0;
        }
        searchIndex.add(searchIndexRecord);
    }

    private int getNextID() {
        SearchIndexRecord searchIndexRecord = (SearchIndexRecord) searchIndex.get(currentPage - 1);
        return searchIndexRecord.start + searchIndexRecord.lot;
    }

    private int getPreviousID() {
        try {
            SearchIndexRecord searchIndexRecord = (SearchIndexRecord) searchIndex.get(currentPage - 2);
            return searchIndexRecord.start;// + searchIndexRecord.lot;
        } catch (Exception e) {
            return 1;
        }
    }

    /*private void searchSymbols(int startPos) {

        String exCode = null;
        String desc = null;
        String reqMesg = null;
        boolean success = false;


        try {
            if (radioSymbol.isSelected())
                searchType = Meta.SEARCH_SYMBOL;
            else if (radioCompNameContain.isSelected())
                searchType = Meta.SEARCH_CONTAINS_DESCRIPTION;
            else
                searchType = Meta.SEARCH_STARTS_WITH_DESCRIPTION;

            symbolType = ((SymbolTypeRecord) g_cmbSymbolTypes.getSelectedItem()).getType();
            exCode = ((TWComboItem) cmbExchanges.getSelectedItem()).getId();
            if (ExchangeStore.getSharedInstance().isDefault(exCode)) {
                loadDefaultExchangeData(exCode);
            } else {
                desc = UnicodeUtils.getUnicodeString(g_txtDescription.getText().trim().toUpperCase());
                if (desc.length() < 4) {
                    if (desc.length() == 0) {
                        new ShowMessage(Client.getInstance(), Language.getString("MSG_INVALID_SEARCH_DESCRIPTION"), "I", false);
                        success = false;
                    } else if (searchType != Meta.SYMBOL_SEARCH) {
                        new ShowMessage(Client.getInstance(), Language.getString("MSG_INVALID_SEARCH_DESCRIPTION_LENGTH"), "I", false);
                        success = false;
                    } else
                        success = true;
                } else {
                    success = true;
                }

                if (success) {
                    setButtonsEnabled(false);
                    reqMesg = Meta.SYMBOL_SEARCH + Meta.DS +
                            exCode + Meta.FD + searchType +
                            Meta.FD + desc + Meta.FD + symbolType + Meta.FD + startPos + Meta.FD + Language.getLanguageTag();
                    if (Settings.isConnected()) {
                        System.out.println(reqMesg);
                        SendQFactory.addData(reqMesg);
                    }
                    setSearchInProgress(true);
                    tblAll.repaint();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    private void searchSymbols(int startPos) {

        String exCode = null;
//        String desc = null;
//        String reqMesg = null;


        try {
            if (radioSymbol.isSelected())
                searchType = Meta.SEARCH_SYMBOL;
            else if (radioCompNameContain.isSelected())
                searchType = Meta.SEARCH_CONTAINS_DESCRIPTION;
            else
                searchType = Meta.SEARCH_STARTS_WITH_DESCRIPTION;

            searchType = Meta.SEARCH_ALL_NAMES;

            symbolType = ((SymbolTypeRecord) g_cmbSymbolTypes.getSelectedItem()).getType();
            exCode = ((TWComboItem) cmbExchanges.getSelectedItem()).getId();

            String selectedExchange = ((TWComboItem) cmbExchanges.getSelectedItem()).getValue();
            boolean allSelected = selectedExchange.equals(Language.getString("ALL"));

            if (allSelected) {
                if (isShowDefaultExchangesOnly()) {
                    doLocalSearch(exCode);
                } else {
                    if (ExchangeStore.isNoneDefaultExchangesAvailable()) {

                        if (checkForNonDefaultFileStatus()) {
                            doLocalFileSearch(exCode);
                        } else {
                        doServerSideSearch(exCode, startPos);
                        }
                    } else {
                        doLocalSearch(exCode);
                    }
                }
            } else {
                if (ExchangeStore.getSharedInstance().isDefault(exCode)) {
                    doLocalSearch(exCode);
                    nextButton.setEnabled(false);
                    prevButton.setEnabled(false);
                } else {
                    if (ExchangeStore.getSharedInstance().getExchange(exCode).isMarketFileAvailable()) {
                        if (ExchangeStore.getSharedInstance().getExchange(exCode).getMasterFileStaus() == Meta.FULL_EXCG_FILE_AVAILABLE /*Meta.PART_EXCG_FILE_AVAILABLE*/) {
                            doLocalFileSearch(exCode);
                        } else {
                            doServerSideSearch(exCode, startPos);
                        }
                    } else {
                    doServerSideSearch(exCode, startPos);
                }
            }
            }

            /*if (ExchangeStore.getSharedInstance().isDefault(exCode)) {
                loadDefaultExchangeData(exCode);
            } else {*/
            /*desc = UnicodeUtils.getUnicodeString(g_txtDescription.getText().trim().toUpperCase());
            if (desc.length() < 4) {
                if (desc.length() == 0) {
                    new ShowMessage(Client.getInstance(), Language.getString("MSG_INVALID_SEARCH_DESCRIPTION"), "I", false);
                    success = false;
                } else if (searchType != Meta.SYMBOL_SEARCH) {
                    new ShowMessage(Client.getInstance(), Language.getString("MSG_INVALID_SEARCH_DESCRIPTION_LENGTH"), "I", false);
                    success = false;
                } else
                    success = true;
            } else {
                success = true;
            }

            if (success) {
                setButtonsEnabled(false);
                reqMesg = Meta.SYMBOL_SEARCH + Meta.DS +
                        exCode + Meta.FD + searchType +
                        Meta.FD + desc + Meta.FD + symbolType + Meta.FD + startPos + Meta.FD + Language.getLanguageTag();
                if (Settings.isConnected()) {
                    System.out.println(reqMesg);
                    SendQFactory.addData(reqMesg);
                }
                setSearchInProgress(true);
                tblAll.repaint();
            }*/
            //}
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private boolean checkForNonDefaultFileStatus() {
        boolean fullFile = true;

        Enumeration exchanges = ExchangeStore.getSharedInstance().getExchanges();
        while (exchanges.hasMoreElements()) {
            Exchange exg = (Exchange) exchanges.nextElement();
            if (!exg.isDefault()) {
                if (exg.isMarketFileAvailable()) {
                    if (exg.getMasterFileStaus() == Meta.PART_EXCG_FILE_AVAILABLE) {
                        fullFile = false;
                        break;
                    }
                } else{
                    fullFile = false;
                    break;
                }
            }
        }

        return fullFile;

    }

    private void doLocalFileSearch(String exchanges) {
        String[] exchangeIDs = exchanges.split(",");
        for (int i = 0; i < exchangeIDs.length; i++) {
            try {
                loadExchangeDataFromFile(exchangeIDs[i]);
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        if (g_vecAllData.size() == 0) {
            SharedMethods.showMessage(Language.getString("NO_RESULTS_FOUND"), JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void loadExchangeDataFromFile(String exchange) {
        String description = g_txtDescription.getText().trim().toUpperCase();
        String tempFilter = description;
        boolean selected;
        Exchange excg = ExchangeStore.getSharedInstance().getExchange(exchange);
        boolean isNeedSubmarketCheck = isDefaultMarketOnly && excg.hasSubMarkets() && excg.isUserSubMarketBreakdown();

         searchType = Meta.SEARCH_ALL_NAMES;

        String align;
        String width;

        if (Language.isLTR()) {
            align = " align=left ";
        } else {
            align = " align=right ";
        }

        if (Language.isLTR()) {
            width = " style=\"width: 500\" ";
        } else {
            width = "";
        }

      /*  if (excg.getMasterFileType() == Meta.FILE_TYPE_MSF) {
            SmartProperties stockMaster = new SmartProperties(Meta.DS);
            String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/market_" + Language.getSelectedLanguage() + ".msf";
                                       try {
                stockMaster.loadCompressed(sLangSpecFileName);
            } catch (IOException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                       }

                                       try {
                stockMaster.remove("VERSION");
            } catch (Exception e) {

                                       }

            //Read File Line By Line
            Enumeration exchageSymbols = stockMaster.keys();
            while (exchageSymbols.hasMoreElements()) {
                String symbol = (String) exchageSymbols.nextElement();
                String insType;
                String shortDec = null;
                String category = null;
                String currency = null;

                                       try {
                    insType = ((String) stockMaster.get(symbol)).split(Meta.FS)[0];

                    category = ((String) stockMaster.get(symbol)).split(Meta.FS)[1];
                    shortDec = ((String) stockMaster.get(symbol)).split(Meta.FS)[2];
                    currency = ((String) stockMaster.get(symbol)).split(Meta.FS)[4];
                                       } catch (NumberFormatException e) {
                    shortDec = "";
                                           category = "";
                    insType = "";
                                       }

                *//*  Enumeration<Stock> stocks = DataStore.getSharedInstance().getExchangeSymbolStore(exchange).elements();
              while (stocks.hasMoreElements()) {*//*
//                        Stock stock = stocks.nextElement();
                *//*if (isNeedSubmarketCheck) {
                    if (!excg.getDefaultMarket().getSymbol().equals(stock.getMarketID())) {
                        continue;
                    }
                }*//*
                selected = false;
                switch (searchType) {
                    case Meta.SEARCH_SYMBOL:
                        if ((symbol.toUpperCase().startsWith(description)) &&
                                ((symbolType == Meta.SEARCH_ALL) || (symbolType == Integer.parseInt(insType)))) {
                            selected = true;
                            String record = exchange + Meta.ID + symbol + Meta.ID +
                                    insType + Meta.ID + category + Meta.ID + shortDec +
                                    Meta.ID + "" + Meta.ID + currency + Meta.ID + symbol + Meta.ID + symbol;
                            SymbolSearchListItem item = new SymbolSearchListItem(exchange,
                                    symbol, shortDec, insType, record, shortDec, symbol);
                            g_vecAllData.addElement(item);
                            item = null;
//                        stock = null;
                        }
                        break;
                    case Meta.SEARCH_CONTAINS_DESCRIPTION:
                        if ((shortDec.toUpperCase().indexOf(description) >= 0) &&
                                ((symbolType == Meta.SEARCH_ALL) || (symbolType == Integer.parseInt(insType)))) {
                            selected = true;
                            String record = exchange + Meta.ID + symbol + Meta.ID +
                                    insType + Meta.ID + category + Meta.ID + shortDec +
                                    Meta.ID + "" + Meta.ID + currency + Meta.ID + symbol + Meta.ID + symbol;
                            SymbolSearchListItem item = new SymbolSearchListItem(exchange,
                                    symbol, shortDec, insType, record, shortDec, symbol);
                            g_vecAllData.addElement(item);
                            item = null;
//                        stock = null;
                        }
                        break;
                    case Meta.SEARCH_STARTS_WITH_DESCRIPTION:
                        if ((shortDec.toUpperCase().startsWith(description)) &&
                                ((symbolType == Meta.SEARCH_ALL) || (symbolType == Integer.parseInt(insType)))) {
                            selected = true;
                            String record = exchange + Meta.ID + symbol + Meta.ID +
                                    insType + Meta.ID + category + Meta.ID + shortDec +
                                    Meta.ID + "" + Meta.ID + currency + Meta.ID + symbol + Meta.ID + symbol;
                            SymbolSearchListItem item = new SymbolSearchListItem(exchange,
                                    symbol, shortDec, insType, record, shortDec, symbol);
                            g_vecAllData.addElement(item);
                            item = null;
//                        stock = null;
                        }
                        break;
                    case Meta.SEARCH_ALL_NAMES:
                        if ((shortDec.toUpperCase().indexOf(description) >= 0) || (symbol.toUpperCase().indexOf(description) >= 0)
                                || (shortDec.toUpperCase().indexOf(description) >= 0)) {
                            if (((symbolType == Meta.SEARCH_ALL) || (symbolType == Integer.parseInt(insType)))) {
                                boolean isAdded = false;

                                //NSDQGOOG61ctegoryshotdeccurrencysysy
                                String record = exchange + Meta.ID + symbol + Meta.ID +
                                        insType + Meta.ID + category + Meta.ID + shortDec +
                                        Meta.ID + "" + Meta.ID + currency + Meta.ID + symbol + Meta.ID + symbol;

                                SymbolSearchListItem item = new SymbolSearchListItem(exchange,
                                        symbol, shortDec, insType, record, shortDec, symbol);

                                if ((shortDec.toUpperCase().indexOf(description)) >= 0) {
                                    if (!isAdded) {
                                        g_vecAllData.addElement(item);
                                        isAdded = true;
                                    }
                                    if (tempFilter.length() > 0) {
                                        int index = 0;
                                        if ((index = item.getShortDescription().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                            String str = item.getShortDescription();
                                            item.setShortDescription(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length()) + "</div></html>"));
                                            str = null;
                                        }
                                    }
                                }
                                if ((shortDec.toUpperCase().indexOf(description)) >= 0) {
                                    if (!isAdded) {
                                        g_vecAllData.addElement(item);
                                        isAdded = true;
                                    }
                                    if (tempFilter.length() > 0) {
                                        int index = 0;
                                        if ((index = item.getDescription().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                            String str = item.getDescription();
                                            item.setDescription(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length()) + "</div></html>"));
                                            str = null;
                                        }
                                    }
                                }
                                if ((shortDec.toUpperCase().indexOf(description)) >= 0) {
                                    if (!isAdded) {
                                        g_vecAllData.addElement(item);
                                        isAdded = true;
                                    }
                                    if (tempFilter.length() > 0) {
                                        int index = 0;
                                        if ((index = item.getSymbol().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                            String str = item.getSymbol();
                                            item.setSymbol(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length()) + "</div></html>"));
                                            str = null;
                                        }
                                    }
                                }
                                item = null;
                                selected = true;
                            }
                        }
                        break;
        }
        }
            tblAll.getTable().updateUI();
        }*/

//        if (excg.getMasterFileType() == Meta.FILE_TYPE_CSV) {
        String sLangSpecFileName = Settings.EXCHANGES_DATA_PATH + "/" + exchange + "/market_" + Language.getSelectedLanguage() + ".csv";

        FileInputStream fstream = null;
        try {
            fstream = new FileInputStream(sLangSpecFileName);
        } catch (FileNotFoundException e) {
                e.printStackTrace();

        }

        DataInputStream in = new DataInputStream(fstream);
        BufferedReader br = new BufferedReader(new InputStreamReader(in));
        String strLine;
        //Read File Line By Line
        try {
            while ((strLine = br.readLine()) != null) {
//                                    String ssymbol = (String) exchageSymbols.nextElement();
                String ssymbol = strLine;
                String symbol;
                String insType;

                String iType = ((String) ssymbol).split(Meta.FS)[0];
                symbol = iType.split(Meta.DS)[0];

                    if (!symbol.equals("VERSION") && !symbol.contains("#")) {

                insType = iType.split(Meta.DS)[1];
                    String shortDec = null;
                    String category = null;
                    String currency = null;

                    try {
                        category = (((String) ssymbol).split(Meta.FS)[1]);
                        shortDec = (((String) ssymbol).split(Meta.FS))[2];
                        currency = (((String) ssymbol).split(Meta.FS))[4];
                    } catch (NumberFormatException e) {
                        shortDec = "";
                        category = "";
                    }

                    /*  Enumeration<Stock> stocks = DataStore.getSharedInstance().getExchangeSymbolStore(exchange).elements();
                  while (stocks.hasMoreElements()) {*/
//                        Stock stock = stocks.nextElement();
                    /*if (isNeedSubmarketCheck) {
                        if (!excg.getDefaultMarket().getSymbol().equals(stock.getMarketID())) {
                            continue;
                        }
                    }*/


                    selected = false;
                    switch (searchType) {
                        case Meta.SEARCH_SYMBOL:
                            if ((symbol.toUpperCase().startsWith(description)) &&
                                    ((symbolType == Meta.SEARCH_ALL) || (symbolType == Integer.parseInt(insType)))) {
                                selected = true;
                                String record = exchange + Meta.ID + symbol + Meta.ID +
                                            insType + Meta.ID + category + Meta.ID + shortDec +
                                            Meta.ID + "" + Meta.ID + currency + Meta.ID + symbol + Meta.ID + symbol;
                                SymbolSearchListItem item = new SymbolSearchListItem(exchange,
                                        symbol, shortDec, insType, record, shortDec, symbol);
                                g_vecAllData.addElement(item);
                                item = null;
//                        stock = null;
                            }
                            break;
                        case Meta.SEARCH_CONTAINS_DESCRIPTION:
                            if ((shortDec.toUpperCase().indexOf(description) >= 0) &&
                                    ((symbolType == Meta.SEARCH_ALL) || (symbolType == Integer.parseInt(insType)))) {
                                selected = true;
                                String record = exchange + Meta.ID + symbol + Meta.ID +
                                            insType + Meta.ID + category + Meta.ID + shortDec +
                                            Meta.ID + "" + Meta.ID + currency + Meta.ID + symbol + Meta.ID + symbol;
                                SymbolSearchListItem item = new SymbolSearchListItem(exchange,
                                        symbol, shortDec, insType, record, shortDec, symbol);
                                g_vecAllData.addElement(item);
                                item = null;
//                        stock = null;
                            }
                            break;
                        case Meta.SEARCH_STARTS_WITH_DESCRIPTION:
                            if ((shortDec.toUpperCase().startsWith(description)) &&
                                    ((symbolType == Meta.SEARCH_ALL) || (symbolType == Integer.parseInt(insType)))) {
                                selected = true;
                                String record = exchange + Meta.ID + symbol + Meta.ID +
                                            insType + Meta.ID + category + Meta.ID + shortDec +
                                            Meta.ID + "" + Meta.ID + currency + Meta.ID + symbol + Meta.ID + symbol;
                                SymbolSearchListItem item = new SymbolSearchListItem(exchange,
                                        symbol, shortDec, insType, record, shortDec, symbol);
                                g_vecAllData.addElement(item);
                                item = null;
//                        stock = null;
                            }
                            break;
                        case Meta.SEARCH_ALL_NAMES:
                            if ((shortDec.toUpperCase().indexOf(description) >= 0) || (symbol.toUpperCase().indexOf(description) >= 0)
                                    || (shortDec.toUpperCase().indexOf(description) >= 0)) {
                                if (((symbolType == Meta.SEARCH_ALL) || (symbolType == Integer.parseInt(insType)))) {
                                    boolean isAdded = false;

                                    //NSDQGOOG61ctegoryshotdeccurrencysysy
                                    String record = exchange + Meta.ID + symbol + Meta.ID +
                                            insType + Meta.ID + category + Meta.ID + shortDec +
                                            Meta.ID + "" + Meta.ID + currency + Meta.ID + symbol + Meta.ID + symbol;

                                    SymbolSearchListItem item = new SymbolSearchListItem(exchange,
                                            symbol, shortDec, insType, record, shortDec, symbol);

                                    if ((shortDec.toUpperCase().indexOf(description)) >= 0) {
                                        if (!isAdded) {
                                            g_vecAllData.addElement(item);
                                            isAdded = true;
                                        }
                                        if (tempFilter.length() > 0) {
                                            int index = 0;
                                            if ((index = item.getShortDescription().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                                String str = item.getShortDescription();
                                                item.setShortDescription(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length()) + "</div></html>"));
                                                str = null;
                                            }
                                        }
                                    }
                                    if ((shortDec.toUpperCase().indexOf(description)) >= 0) {
                                        if (!isAdded) {
                                            g_vecAllData.addElement(item);
                                            isAdded = true;
                                        }
                                        if (tempFilter.length() > 0) {
                                            int index = 0;
                                            if ((index = item.getDescription().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                                String str = item.getDescription();
                                                item.setDescription(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length()) + "</div></html>"));
                                                str = null;
                                            }
                                        }
                                    }
                                    if ((shortDec.toUpperCase().indexOf(description)) >= 0) {
                                        if (!isAdded) {
                                            g_vecAllData.addElement(item);
                                            isAdded = true;
                                        }
                                        if (tempFilter.length() > 0) {
                                            int index = 0;
                                            if ((index = item.getSymbol().toUpperCase().indexOf(tempFilter.toUpperCase())) >= 0) {
                                                String str = item.getSymbol();
                                                item.setSymbol(("<html><div " + align + width + ">" + str.substring(0, index) + "<span style='background-color: " + Theme.getHighlightedColor() + "'>" + str.substring(index, index + tempFilter.length()) + "</span>" + str.substring(index + tempFilter.length()) + "</div></html>"));
                                                str = null;
                                            }
                                        }
                                    }
                                    item = null;
                                    selected = true;
                                }
                            }
                            break;
                    }
                }
            }

        } catch (IOException e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        tblAll.getTable().updateUI();

//        }
    }

    public void indexSearch(){
        String exCode = null;
        try {
            if (radioSymbol.isSelected())
                searchType = Meta.SEARCH_SYMBOL;
            else if (radioCompNameContain.isSelected())
                searchType = Meta.SEARCH_CONTAINS_DESCRIPTION;
            else
                searchType = Meta.SEARCH_STARTS_WITH_DESCRIPTION;

            searchType = Meta.SEARCH_ALL_NAMES;
            symbolType = ((SymbolTypeRecord) g_cmbSymbolTypes.getSelectedItem()).getType();
            exCode = ((TWComboItem) cmbExchanges.getSelectedItem()).getId();
            String selectedExchange = ((TWComboItem) cmbExchanges.getSelectedItem()).getValue();
            boolean allSelected = selectedExchange.equals(Language.getString("ALL"));
            if (allSelected) {
                if (!isShowDefaultExchangesOnly()) {
                    doLocalSearchOnIndex(exCode);
                }
            } else {
                if (!ExchangeStore.getSharedInstance().isDefault(exCode)) {
                    doLocalSearchOnIndex(exCode);
                }
            }
            } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void doServerSideSearch(String exchanges, int startPos) {
        boolean success = false;

        String desc = UnicodeUtils.getUnicodeString(g_txtDescription.getText().trim().toUpperCase());
        if ((ExchangeStore.getSharedInstance().nonDefaultCount() > 0) && (g_txtDescription.getText().trim().length() < 2)) {
            if (desc.length() == 0) {
                new ShowMessage(Client.getInstance(), Language.getString("MSG_INVALID_SEARCH_DESCRIPTION"), "I", false);
                success = false;
            } else {
                new ShowMessage(Client.getInstance(), Language.getString("MSG_INVALID_SEARCH_DESCRIPTION_LENGTH"), "I", false);
                success = false;
            }
        } else {
            success = true;
        }

        if (success) {
//            setButtonsEnabled(false);           
            btnApply.setEnabled(false);
            btnSearch.setEnabled(false);
            btnCancel.setEnabled(true);

            String reqMesg = Meta.SYMBOL_SEARCH + Meta.DS +
                    exchanges + Meta.FD + searchType +
                    Meta.FD + desc + Meta.FD + symbolType + Meta.FD + startPos + Meta.FD + Language.getLanguageTag() + Meta.EOL;
            String errMsg=Meta.SYMBOL_SEARCH + Meta.DS + Meta.FD + -1;
            if (Settings.isConnected()) {
                System.out.println(reqMesg);
                SendQFactory.addContentData(errMsg,Constants.CONTENT_PATH_SECONDARY, reqMesg, null);
//                new ContentCMConnector(reqMesg, null, Constants.CONTENT_PATH_SECONDARY);
            }
            setSearchInProgress(true);
            tblAll.repaint();
        }
    }

    private void doLocalSearch(String exchanges) {
        String[] exchangeIDs = exchanges.split(",");
        for (int i = 0; i < exchangeIDs.length; i++) {
            try {
            loadDefaultExchangeData(exchangeIDs[i]);
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        if (g_vecAllData.size() == 0) {
            SharedMethods.showMessage(Language.getString("NO_RESULTS_FOUND"), JOptionPane.INFORMATION_MESSAGE);
        }
    }

    private void doLocalSearchOnIndex(String exchanges){
        String[] exchangeIDs = exchanges.split(",");
        for (int i = 0; i < exchangeIDs.length; i++) {
            try {
            searchAllIndexes(exchangeIDs[i]);
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
        if (g_vecAllData.size() == 0) {
            SharedMethods.showMessage(Language.getString("NO_RESULTS_FOUND"), JOptionPane.INFORMATION_MESSAGE);
        }
    }

    public String getSelectedExchanges() {
        StringBuffer selectedItems = new StringBuffer("");
        try {
            if (cmbExchanges.getSelectedIndex() == 0) {
                for (int i = 1; i < cmbExchanges.getItemCount(); i++) {
                    selectedItems.append(",");
                    selectedItems.append(((TWComboItem) cmbExchanges.getItemAt(i)).getId());
                }
            }
            return selectedItems.toString().substring(1);
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        }

    }

    /* private void processDataVector(Vector dataVector) {
int i = 0;
String sKey = "";
String[] saKnownKeys = null;
String dataArray[] = null;

try {
g_sAllData = new String[dataVector.size()];
for (int j = 0; j < dataVector.size(); j++) {
    dataArray = (String[]) dataVector.elementAt(j);
    sKey = SharedMethods.getKey(dataArray[0], dataArray[1]);
    if (dataArray[2] == null)
        dataArray[2] = "";
    if (dataArray[2].equals("")) {
        g_sAllData[j] = sKey + Meta.FS + sKey;
    } else {
        g_sAllData[j] = dataArray[2] + Meta.FS + sKey;
    }
}
} catch (Exception e) {
e.printStackTrace();
}

//g_vecAllData = null;
//g_vecAllData = new Vector(g_sAllData.length);

i = 0;
while (i < g_sAllData.length) {
g_vecAllData.add(g_sAllData[i]);
i++;
}

g_vecSelectedData = null;
g_vecSelectedData = new Vector(15, 1);
//        if (!isSubMode)
//            g_vecRemovedData = new Vector(15, 1);

saKnownKeys = g_oSymbols.getSymbols();
i = 0;


if (saKnownKeys != null) {
while (i < saKnownKeys.length) {
    if (saKnownKeys[i].equals("")) {
        i++;
        continue;
    }

    *//*try {
                    switch (storeType) {
                        case Meta.QUOTE :
                            if ((QuoteStore.getCompanyName(saKnownKeys[i]) == null) || QuoteStore.getCompanyName(saKnownKeys[i]).equals("")) {
                                g_vecSelectedData.add(saKnownKeys[i] + Meta.FS + saKnownKeys[i]);
                            } else {
                                g_vecSelectedData.add(QuoteStore.getCompanyName(saKnownKeys[i]) + Meta.FS + saKnownKeys[i]);
                            }
                            break;
                        case Meta.MUTUALFUND :
                            if (((MFundStore.getMFundObject(saKnownKeys[i])).getDescription() == null) || (MFundStore.getMFundObject(saKnownKeys[i])).getDescription().equals("")) {
                                g_vecSelectedData.add(saKnownKeys[i] + Meta.FS + saKnownKeys[i]);
                            } else {
                                g_vecSelectedData.add((MFundStore.getMFundObject(saKnownKeys[i])).getDescription() + Meta.FS + saKnownKeys[i]);
                            }
                            break;
						case Meta.CMEOPRAQUOTE:
							CMEOpraQuote item = CMEOptionsStore.getCMEOpraObject(saKnownKeys[i]);
							if ((item.getDescription() != null) && (!item.getDescription().equals("")))
							    g_vecSelectedData.add(item.getDescription() + Meta.FS + saKnownKeys[i]);
							else
								g_vecSelectedData.add(saKnownKeys[i]);
							item = null;
							break;
                        case Meta.INDEX :
							g_vecSelectedData.add(saKnownKeys[i]);
                            break;
                    }
                } catch (Exception e) {

                }*//*
                i++;
            }
        }
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
//                lstAvailable.updateUI();
                lstSelected.updateUI();
//                oScrollPane1.updateUI();
                oScrollPane2.updateUI();
            }
        });
    }*/

    public void setTitle(String title) {
        windowTitle = Language.getString("WINDOW_TITLE_SEARCH") + " - " + title;
        super.setTitle(windowTitle);
    }

    /*private void startProgressAnimation() {
        progressIndicator.start();
    }*/

    /*private void stopProgressAnimation(boolean haveData) {
        progressIndicator.stop();
        if (!haveData) {
            super.setTitle(windowTitle);
            new ShowMessage(Language.getString("SEARCH_NO_DATA"), "I");
        } else {
            super.setTitle(windowTitle);
        }
    }*/

    public void showDialog(boolean modal, boolean singleExchange) {
        init();
//        try {
//            if(singleExchange){
//                setSelectedExchange(selectedExchange);
//            }else{
//                cmbExchanges.setEnabled(true);
//                this.selectedExchange = ".";
//            }
//        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
//        }
        if (Language.isLTR()) {
            GUISettings.applyOrientation(this, ComponentOrientation.LEFT_TO_RIGHT);
        } else {
            GUISettings.applyOrientation(this, ComponentOrientation.RIGHT_TO_LEFT);
//            JScrollBar oScrollBar = oScrollPane1.getHorizontalScrollBar();
//            oScrollBar.setValue(oScrollBar.getMaximum());
//            oScrollBar = oScrollPane2.getHorizontalScrollBar();
//            oScrollBar.setValue(oScrollBar.getMaximum());
        }

        //this.setTitle(Language.getString("WINDOW_TITLE_COMPANIES"));
        this.setResizable(false);
        this.setSize(700, 357);
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        this.setBounds((int) (oDm.getWidth() / 2 - (this.getWidth() / 2)),
                (int) (oDm.getHeight() / 2 - (this.getHeight() / 2)),
                this.getWidth(), this.getHeight());
        initScrollPanes();
        setModal(modal);
        try {
            g_txtDescription.setText("");
//            cmbExchanges.setSelectedIndex(0);
//            g_cmbSymbolTypes.setSelectedIndex(0);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        this.setVisible(true);
    }

    /**
     * Show the dialog window
     */
    public void showDialog(boolean modal) {
        tblAll.getPopup().setTMTavailable(false);
        showDialog(modal, false);
    }

    /**
     * When add button is clicked
     */
    void btnAdd_actionPerformed() {
        int[] aiSelected = null;
        SymbolSearchListItem newItem;
        SymbolSearchListItem selectedItem;

        aiSelected = tblAll.getTable().getSelectedRows();
        if (aiSelected.length >= 0) {
            for (int i = 0; i < aiSelected.length; i++) {
                newItem = (SymbolSearchListItem) g_vecAllData.elementAt(tblAll.getUnsortedRow(aiSelected[i]));
                if (!contains(newItem)) {
                    if (isSingleMode())
                        g_vecSelectedData.clear();
                    g_vecSelectedData.addElement(newItem);
                }
                selectedItem = null;

                newItem = null;
            }
        }
//        lstAvailable.setSelectedIndices(new int[0]);
        lstSelected.updateUI();
    }

    public boolean contains(SymbolSearchListItem newItem) {
        SymbolSearchListItem selectedItem;

        for (int j = 0; j < g_vecSelectedData.size(); j++) {
            selectedItem = (SymbolSearchListItem) g_vecSelectedData.elementAt(j);
            if ((newItem.getOriginalSymbol().equals(selectedItem.getOriginalSymbol())) &&
                    (newItem.getExchange().equals(selectedItem.getExchange()))) {
                return true;
            }
            selectedItem = null;
        }
        return false;
    }

    /**
     * When remove button is clicked
     */
    void btnRemove_actionPerformed() {
        int[] aiSelected = null;

        aiSelected = lstSelected.getSelectedIndices();
        if (aiSelected.length >= 0) {
            for (int i = 0; i < aiSelected.length; i++) {
//if (!isSubMode)
//    g_vecRemovedData.addElement(g_vecSelectedData.elementAt(aiSelected[i]-i));
                g_vecSelectedData.removeElementAt(aiSelected[i] - i);
            }
        }
        lstSelected.setSelectedIndices(new int[0]);
        lstSelected.updateUI();

    }

    /**
     * When apply button is clicked
     */
    void btnApply_actionPerformed() {
        int i = 0;
        SymbolSearchListItem item = null;
        Symbols newSymbols = new Symbols((byte) Meta.INSTRUMENT_QUOTE);


        if ((isSingleMode()) && (g_vecSelectedData.size() == 0)) {
            new ShowMessage(Language.getString("MSG_COMPANY_LIST_CANNOT_EMPTY"), "E");
            return;
        }

        String[] asSymbols = new String[g_vecSelectedData.size()];

        if (g_vecSelectedData.size() != 0) {
            while (i < g_vecSelectedData.size()) {
                item = (SymbolSearchListItem) g_vecSelectedData.elementAt(i);

                asSymbols[i] = item.getKey();
                i++;
            }
            newSymbols.setSymbols(asSymbols);
        }

        String[] oldSymbols = g_oSymbols.getSymbols();
        for (int p = 0; p < oldSymbols.length; p++) {
            if (!newSymbols.isAlreadyIn(oldSymbols[p])) {
                try {
                    DataStore.getSharedInstance().removeSymbolRequest(oldSymbols[p]);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

// check for newly added symbols
        String key = null;
        for (int p = 0; p < g_vecSelectedData.size(); p++) {
            item = (SymbolSearchListItem) g_vecSelectedData.elementAt(p);
            key = item.getKey();
            if (!g_oSymbols.isAlreadyIn(key)) {
                if (!ExchangeStore.getSharedInstance().isDefault(item.getExchange())) {
                    ValidatedSymbols.getSharedInstance().addSymbol(key, item.getRecord());
                    DataStore.getSharedInstance().addSymbolRequest(key);
                    OptimizedTreeRenderer.selectedSymbols.put(key,true);
                    Stock st = DataStore.getSharedInstance().getStockObject(key);
                    if (st != null) {
                    st.setSymbolEnabled(true);
                }
            }
            }
            item = null;
        }
        g_oSymbols.setSymbols(asSymbols);
        this.hide();

        item = null;
    }


    /**
     * When cancel button is clicked
     */
    void btnCancel_actionPerformed() {
        /*ArrayList list = new ArrayList();
        if (oldList != null){
            try {
                for (int p = 0; p < oldList.length; p++) {
                    String[] symbolData = oldList[p].split("\\|");
                    QuoteStore.setSymbol(false,symbolData[0],null,symbolData[1]);
                    list.add(symbolData[0]);
                    symbolData = null;
                }
                g_oSymbols.setSymbols(SharedMethods.objectToStringArray(list.toArray()));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        list = null;*/
        if(isSearchInProgress()) {
            setSearchInProgress(false);
            setButtonsEnabled(true);
        }
        this.hide();
    }

 public   void btnSearch_actionPerformed() {
        searchIndex.clear();
        currentPage = 1;
        search_Clicked(0);
    }

    void btnPrev_actionPerformed() {
        if (isConnected()) {
            search_Clicked(getPreviousID());
            currentPage--;
        }
    }

    void btnNext_actionPerformed() {
        if (isConnected()) {
            search_Clicked(getNextID());
            currentPage++;
        }
    }

    private void setPrevStatusAfterSearch() {
        if (currentPage == 1)
            prevButton.setEnabled(false);
        if (currentPage > 1)
            prevButton.setEnabled(true);
    }


    /**
     * Returns the action listener for the search button
     */
    public void search_Clicked(int startPos) {
        if (isConnected()) {
            if (!isIndexMode) {
                g_vecAllData.clear();
                g_vecAllData.trimToSize();
                tblAll.getTable().updateUI();
                searchSymbols(startPos);
            } else {
                g_vecAllData.clear();
                g_vecAllData.trimToSize();
                tblAll.getTable().updateUI();
                indexSearch();
            }
        }
    }

//	public void removeListenersx(){
//		btnSearch.removeActionListener(this);
//		btnAdd.removeActionListener(this);
//		btnRemove.removeActionListener(this);
//		btnCancel.removeActionListener(this);
//		btnApply.removeActionListener(this);
//
//		btnAdd.removeKeyListener(this);
//		btnRemove.removeKeyListener(this);
//		btnCancel.removeKeyListener(this);
//		btnApply.removeKeyListener(this);
//		btnSearch.removeKeyListener(this);
//	}

    /**
     * Returns the selected search patterd
     */
    private String getPattern() {
        return g_txtDescription.getText().trim();
    }

    public void mouseClicked(MouseEvent e) {
        if (e.getClickCount() == 2) {
            if (e.getSource() == tblAll.getTable()) {
                btnAdd_actionPerformed();
            } else if (e.getSource() == lstSelected) {
                btnRemove_actionPerformed();
            }
        }
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("A"))
            btnApply_actionPerformed();
        else if (e.getActionCommand().equals("C"))
            btnCancel_actionPerformed();
        else if (e.getActionCommand().equals("D"))
            btnAdd_actionPerformed();
        else if (e.getActionCommand().equals("R"))
            btnRemove_actionPerformed();
        else if (e.getActionCommand().equals("S"))
            btnSearch_actionPerformed();
        else if (e.getActionCommand().equals("PREV"))
            btnPrev_actionPerformed();
        else if (e.getActionCommand().equals("NEXT"))
            btnNext_actionPerformed();
        else if (e.getActionCommand().equals("EXCHANGES")) {
//            System.out.println("exchange lsit count = "+exchangeList.size());
//            System.out.println("exchange combo count = "+cmbExchanges.getItemCount());
        }
//            btnNext_actionPerformed();

    }

    /*private void changeProgress() {
        super.setTitle(windowTitle + " - " + Language.getString("SEARCH_BUSY") + getProgressIndicatorValue(progressIndicatorType++));
    }*/

    /*public String getProgressIndicatorValue(int type) {
        if (!isConnected()) {
            stopProgressAnimation(false);
            setButtonsEnabled(true);
        }
        if (progressIndicatorType == 4) {
            progressIndicatorType = 0;
        }
        switch (type) {
            case 0:
                return " |";
            case 1:
                return " /";
            case 2:
                return " -";
            case 3:
                return " \\";
        }
        return ".";
    }*/

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    /*-- Window listeners --*/
    public void windowOpened(WindowEvent e) {
        try {
            g_txtDescription.setText("");
        } catch (Exception e1) {
//            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    /**
     * Invoked when the user attempts to close the window
     * from the window's system menu.  If the program does not
     * explicitly hide or dispose the window while processing
     * this event, the window close operation will be cancelled.
     */
    public void windowClosing(WindowEvent e) {
        SharedMethods.printLine("**Closing window....");
        try {
            g_txtDescription.setText("");
        } catch (Exception e1) {
//            e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
//        stopProgressAnimation(true);
        setButtonsEnabled(true);
        //removeListeners();
    }

    /**
     * Invoked when a window has been closed as the result
     * of calling dispose on the window.
     */
    public void windowClosed(WindowEvent e) {
//        stopProgressAnimation(true);
        //removeListeners();
        //SharedMethods.printLine("Closed window");
    }

    /**
     * Invoked when a window is changed from a normal to a
     * minimized state. For many platforms, a minimized window
     * is displayed as the icon specified in the window's
     * iconImage property.
     *
     * @see java.awt.Frame#setIconImage
     */
    public void windowIconified(WindowEvent e) {
    }

    /**
     * Invoked when a window is changed from a minimized
     * to a normal state.
     */
    public void windowDeiconified(WindowEvent e) {
    }

    /**
     * Invoked when the Window is set to be the active Window. Only a Frame or
     * a Dialog can be the active Window. The native windowing system may
     * denote the active Window or its children with special decorations, such
     * as a highlighted title bar. The active Window is always either the
     * focused Window, or the first Frame or Dialog that is an owner of the
     * focused Window.
     */
    public void windowActivated(WindowEvent e) {
    }

    /**
     * Invoked when a Window is no longer the active Window. Only a Frame or a
     * Dialog can be the active Window. The native windowing system may denote
     * the active Window or its children with special decorations, such as a
     * highlighted title bar. The active Window is always either the focused
     * Window, or the first Frame or Dialog that is an owner of the focused
     * Window.
     */
    public void windowDeactivated(WindowEvent e) {
    }

    /**
     * Invoked when a key has been typed.
     * See the class description for {@link KeyEvent} for a definition of
     * a key typed event.
     */
    public void keyTyped(KeyEvent e) {
    }

    /**
     * Invoked when a key has been pressed.
     * See the class description for {@link KeyEvent} for a definition of
     * a key pressed event.
     */
    public void keyPressed(KeyEvent e) {

    }

    /**
     * Invoked when a key has been released.
     * See the class description for {@link KeyEvent} for a definition of
     * a key released event.
     */
    public void keyReleased(KeyEvent e) {
         if (e.getKeyChar() == KeyEvent.VK_ENTER) {
            if (e.getSource() == btnAdd) {
                btnAdd_actionPerformed();
            } else if (e.getSource() == btnRemove) {
                btnRemove_actionPerformed();
            } else if (e.getSource() == btnCancel) {
                btnCancel_actionPerformed();
            } else if (e.getSource() == btnApply) {
                btnApply_actionPerformed();
            } else if (e.getSource() == btnSearch) {
                search_Clicked(0);
            } else {
                search_Clicked(0);
            }
        }
    }

    public boolean isSingleMode() {
        return singleMode;
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
    }

    private boolean isConnected() {
        if (!Settings.isConnected()) {
//            this.hide();
//            new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
//            return false;
            setShowDefaultExchangesOnly(true);
            return true;
        } else {
            return true;
        }
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange,long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {
        populateExchanges();
    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    class SearchIndexRecord {
        int start;
        int lot;
    }

    public void internalFrameActivated(InternalFrameEvent e) {

    }

    public void internalFrameClosed(InternalFrameEvent e) {

    }

    public void internalFrameClosing(InternalFrameEvent e) {
        SharedMethods.printLine("**Closing window....");
//        stopProgressAnimation(true);
        setButtonsEnabled(true);
        //removeListeners();
    }

    public void internalFrameDeactivated(InternalFrameEvent e) {

    }

    public void internalFrameDeiconified(InternalFrameEvent e) {

    }

    public void internalFrameIconified(InternalFrameEvent e) {

    }

    public void internalFrameOpened(InternalFrameEvent e) {

    }

    public void applySettings() {
        if (tblAll != null) {
            oSettings = tblAll.getModel().getViewSettings();
            this.setSize(oSettings.getSize());
            tblAll.getModel().applySettings();
        } else if (oSettings != null) {
            this.setSize(oSettings.getSize());
        }

        if (oSettings != null)
            this.setLocation(oSettings.getLocation());

        /* try {
           if (oSettings.isVisible()) {
               this.setVisible(true);
           }
        } catch (Exception e) {
           this.setVisible(oSettings.isVisible());
        }*/

        if (tblAll != null) {
            tblAll.updateGUI();
            tblAll.updateUI();
        }
    }

    public JTable getTable1() {
        return tblAll.getTable();
    }

    public JTable getTable2() {
        return null;
    }

    public JTable getTabFoler(String id) {
        return null;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getWindowStyle() {
        return 0;
    }

    public int getZOrder() {
        return 0;
    }

    public boolean isTitleVisible() {
        return true;
    }

    public boolean isWindowVisible() {
        return true;
    }

    public void printTable() {

    }

    public void setTitleVisible(boolean status) {

    }

    public void windowClosing() {

    }

    public Object getDetachedFrame() {
        return null;
    }

    public boolean isDetached() {
        return false;
    }

    public boolean isIndexSearchMode(){
        return isIndexMode;
    }

    public void setIndexSearchMode(boolean mode){
        this.isIndexMode=mode;
        if(mode){
            g_cmbSymbolTypes.setEnabled(false);
            cmbExchanges.setEnabled(false);
            g_cmbSymbolTypes.setSelectedIndex(0);
        }else{
            g_cmbSymbolTypes.setEnabled(true);
            cmbExchanges.setEnabled(true);
            g_cmbSymbolTypes.setSelectedIndex(1);
        }

    }
    
 
}


