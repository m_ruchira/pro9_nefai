package com.isi.csvr.symbolsearch;

import javax.swing.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class SymbolTypesComboModel extends DefaultComboBoxModel {

    //public static final int
    private Object[][] data;
    private int selectedType;

    public SymbolTypesComboModel(Object[][] data) {
        this.data = data;
    }

    public void setSelectedType(int type) {
        this.selectedType = type;
    }

    public Object getElementAt(int index) {
        return null;
    }

    public int size() {
        return 0;
    }
}