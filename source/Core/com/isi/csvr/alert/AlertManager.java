package com.isi.csvr.alert;

import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.TWSeparator;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.win32.NativeMethods;

import javax.sound.sampled.Clip;
import javax.swing.*;
import javax.swing.border.EtchedBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class AlertManager implements MouseListener, ActionListener, Themeable {
    private JPanel contentPanel = new JPanel();
    private static AlertManager self;
    private Table table;
    private SimpleDateFormat dateTimeFormat;
    //private Date date;
    private Clip clip;
    private InternalFrame frame;
    private long lastAlrtTime = 0;
    private static SimpleDateFormat formatter = new SimpleDateFormat("yyyyMMddHHmmss");

    private AlertManager() {
        try {
            jbInit();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public synchronized static AlertManager getSharedInstance() {
        if (self == null) {
            self = new AlertManager();
        }
        return self;
    }


    private void jbInit() throws Exception {

        dateTimeFormat = new SimpleDateFormat(" dd/MM/yyyy HH:mm ");

        contentPanel.setLayout(new BorderLayout());
        contentPanel.setOpaque(true);
        contentPanel.setBackground(Color.red);


        table = new Table(true);
        AlertTableModel model = new AlertTableModel();
        ViewSetting oSetting=ViewSettingsManager.getSummaryView("ALERT");
        model.setViewSettings(oSetting);
//        GUISettings.setColumnSettings(model.getViewSettings(), TWColumnSettings.getItem("ALERT_COLS"));
        table.setModel(model);
        model.setTable(table);
        table.killThread(); // no threaded update for table
        table.setBorder(BorderFactory.createEtchedBorder(EtchedBorder.LOWERED));

        contentPanel.add(table, BorderLayout.CENTER);

        table.getPopup().add(new TWSeparator());

        TWMenuItem mnuDelete = new TWMenuItem(Language.getString("DELETE"), "delete.gif");
        mnuDelete.addActionListener(this);
        mnuDelete.setActionCommand("DEL");
        table.getPopup().setMenuItem(mnuDelete);
        table.getPopup().hideTitleBarMenu();
        TWMenuItem mnuEdit = new TWMenuItem(Language.getString("EDIT"), "edit.gif");
        mnuEdit.addActionListener(this);
        mnuEdit.setActionCommand("EDIT");
        table.getPopup().setMenuItem(mnuEdit);

        table.getTable().addMouseListener(this);

        frame = new InternalFrame(table);
        Client.getInstance().getDesktop().add(frame);
        frame.setViewSetting(oSetting);
        frame.getContentPane().add(contentPanel);
        frame.setSize(model.getViewSettings().getSize());
        frame.setLocation(model.getViewSettings().getLocation());
        frame.setClosable(true);
        frame.setIconifiable(true);
        frame.setResizable(true);
        frame.setMaximizable(true);
        frame.setColumnResizeable(true);
        frame.setDetachable(true);
        frame.setPrintable(true);
        frame.setLayer(GUISettings.TOP_LAYER);
        frame.setTitle(Language.getString("ALERT_MANAGER"));
        model.getViewSettings().setParent(frame);
        frame.hideTitleBarMenu();
        Theme.registerComponent(this);
        GUISettings.applyOrientation(frame);
    }

    public Date getDateFromString(String sDate) throws Exception {
        //"yyyyMMddHHmmss";
        //return formatter.parse(sDate);
        try {
            Calendar calendar = Calendar.getInstance();
            calendar.set(Calendar.YEAR, Integer.parseInt(sDate.substring(0, 4)));
            calendar.set(Calendar.MONTH, Integer.parseInt(sDate.substring(4, 6)) - 1);
            calendar.set(Calendar.DAY_OF_MONTH, Integer.parseInt(sDate.substring(6, 8)));
            calendar.set(Calendar.HOUR_OF_DAY, Integer.parseInt(sDate.substring(8, 10)));
            calendar.set(Calendar.MINUTE, Integer.parseInt(sDate.substring(10, 12)));
            calendar.set(Calendar.SECOND, Integer.parseInt(sDate.substring(12, 14)));

            return calendar.getTime();
        } catch (Exception e) {
            return new Date(0L);
        }
    }

    public String getFormattedDate(Date date) {
        return formatter.format(date);
    }

    public void setAlert(AlertFrame alert) {
        AlertRecord record = new AlertRecord();
        record.setSymbol(alert.getSymbol());
        record.setExchange(alert.getExcahge());
        record.setActive(true);
        record.setField(alert.getField());
        record.setOperator("" + alert.getCriteria());
        record.setValue(alert.getValue());
        record.setVlidity(alert.getValidity());
        record.setExpiration(alert.getExpiration());
        record.setID(alert.getClientAlertID());
        record.setDataType(alert.getDataType());
        record.setDate(alert.getInsertDate());
        record.setTriggered(alert.getType().equals(Meta.ALERT_TRIGGERED));//alert.getAlertStatus()==Meta.ALERT_STATUS_TRIGGERED);
        record.setTriggeredDate(alert.getTriggeredDate());
        record.setExpirationDate(alert.getInsertDate() + ((long) alert.getExpiration() * 24 * 60 * 60 * 1000));
        record.setSendMode(alert.getSend_Method());
        record.setTriggeredValue(alert.getTriggeredValue());
        record.setSource(alert.getSource());
        record.setInstrumentType(alert.getInstrumentType());

        if (alert.getType().equals(Meta.ALERT_TRIGGERED)) {
            AlertRecord currentRecord = AlertStore.getSharedInstance().getRecord(alert.getClientAlertID());
            if (currentRecord == null) {
                AlertStore.getSharedInstance().setRecord(record);
                AlertFrame returnAlertFrame = (AlertFrame) alert.clone();
                returnAlertFrame.setType("DS");
                SendQFactory.addData(Constants.PATH_PRIMARY, returnAlertFrame.getMsgString());
                returnAlertFrame = null;
                if (record.getSource() == AlertRecord.SOURCE_SERVER) {
                    playSound();
                    showAlertMessage(record);
                }
            } else {
                if (record.getSource() == AlertRecord.SOURCE_SERVER) {
                    record.setTriggered(true);
                    AlertFrame returnAlertFrame = (AlertFrame) alert.clone();
                    returnAlertFrame.setType("DS");
                    SendQFactory.addData(Constants.PATH_PRIMARY, returnAlertFrame.getMsgString());
                    playSound();
                    showAlertMessage(record);
                    returnAlertFrame = null;
                }
                updateRecord(record);
            }
            currentRecord = null;
        } else if (alert.getType().equals(Meta.ALERT_TW_CONFIG)) {
            Settings.setAlertModes(record.getSendMode());
        } else if (alert.getType().equals(Meta.ALERT_DELETE)) {
            AlertStore.getSharedInstance().deleteRecord(alert.getClientAlertID());
        } else if (alert.getType().equals(Meta.ALERT_READ_RECIEPT)) {
            updateRecord(record);
        } else if (alert.getType().equals(Meta.ALERT_TW_REJECTED)) {
            playSound();
            showRejectedMessage(record);
        } else {
            updateRecord(record);
        }
        table.getTable().updateUI();
        record = null;
    }

    private void updateRecord(AlertRecord newRecord) {
        AlertRecord currentRecord = AlertStore.getSharedInstance().getRecord(newRecord.getID());
        if (currentRecord != null) {
            if (newRecord.getField() > 0) {
                currentRecord.setField(newRecord.getField());
            }
            currentRecord.setOperator("" + newRecord.getOperator());
            if (newRecord.getValue() > 0) {
                currentRecord.setValue(newRecord.getValue());
            }
            if (newRecord.getValidity() > 0) {
                currentRecord.setVlidity(newRecord.getValidity());
            }
            if (newRecord.getExpiration() > 0) {
                currentRecord.setExpiration(newRecord.getExpiration());
            }
            if (newRecord.getDataType() > 0) {
                currentRecord.setDataType(newRecord.getDataType());
            }
            if (newRecord.getDate() > 0) {
                currentRecord.setDate(newRecord.getDate());
            }
//            if (newRecord.getTriggeredDate() > 0) {
                currentRecord.setTriggeredDate(newRecord.getTriggeredDate());
//            }
            currentRecord.setTriggered(newRecord.isTriggered());
            if (newRecord.getExpirationDate() > 0) {
                currentRecord.setExpirationDate(newRecord.getExpirationDate());
            }
            currentRecord.setSendMode(newRecord.getSendMode());
            //if (newRecord.getTriggeredValue() > 0) {
                currentRecord.setTriggeredValue(newRecord.getTriggeredValue());
            //}
        } else {
            System.err.println("Record not found");
            AlertStore.getSharedInstance().setRecord(newRecord);
        }
    }

    private void showRejectedMessage(AlertRecord record) {
        StringBuffer message = new StringBuffer();

        String popupTemplate = Language.getString("ALERT_REJECT");
        popupTemplate = popupTemplate.replaceFirst("\\[SYMBOL\\]", record.getSymbol());
        popupTemplate = popupTemplate.replaceFirst("\\[FIELD\\]", AlertUtils.getFieldItem(record.getField()).toString());
        //popupTemplate = popupTemplate.replaceFirst("\\[VALUE\\]", AlertUtils.getFormattedString(record.getTriggeredValue(), record.getField())); //Bug Id <#0025>
        popupTemplate = popupTemplate.replaceFirst("\\[VALUE\\]", AlertUtils.getFormattedString(record.getValue(), record.getField(),record.getDecimalCount()));
        message.append(popupTemplate);
        popupTemplate = null;

        final JDialog popup = new JDialog(Client.getInstance().getFrame(), Language.getString("ALERT"), false);
        JButton okButton = new JButton(Language.getString("OK"));
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                popup.dispose();
            }
        });
        Object[] options = {okButton};
        JOptionPane optionPane = new JOptionPane(message, JOptionPane.ERROR_MESSAGE, JOptionPane.OK_OPTION,
                UIManager.getIcon("OptionPane.informationIcon"), options);
        popup.setContentPane(optionPane);
        popup.pack();
        popup.setLocationRelativeTo(Client.getInstance().getFrame());
        popup.setVisible(true);
    }

    private void showAlertMessage(AlertRecord record) {
        StringBuffer message = new StringBuffer();

        float setValue;
        try {
            setValue = AlertStore.getSharedInstance().getRecord(record.getID()).getValue();
        } catch (Exception e) {
            setValue = 0;
        }
        /*
        <HTML>[SYMBOL] [FIELD] [VALUE]<BR>Alert Value: <B>[TRIG_VALUE]</B><BR>Date & Time Alert was met <B>[TRIG_DATE]</B>
        */
        Date triggered = new Date(Settings.getLocalTimeFor(record.getTriggeredDate()));
        String popupTemplate = Language.getString("ALERT_POPUP");
        popupTemplate = popupTemplate.replaceFirst("\\[SYMBOL\\]", record.getSymbol());
        popupTemplate = popupTemplate.replaceFirst("\\[FIELD\\]", AlertUtils.getFieldItem(record.getField()).toString());
        popupTemplate = popupTemplate.replaceFirst("\\[VALUE\\]", AlertUtils.getFormattedString(record.getTriggeredValue(), record.getField(),record.getDecimalCount()));
        popupTemplate = popupTemplate.replaceFirst("\\[TRIG_VALUE\\]", AlertUtils.getFormattedString(setValue, record.getField(),record.getDecimalCount()));
        popupTemplate = popupTemplate.replaceFirst("\\[TRIG_DATE\\]", dateTimeFormat.format(triggered));
        message.append(popupTemplate);
        popupTemplate = null;
        triggered = null;

        final JDialog popup = new JDialog(Client.getInstance().getFrame(), Language.getString("ALERT"), false);
        JButton okButton = new JButton(Language.getString("OK"));
        okButton.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                popup.dispose();
            }
        });
        Object[] options = {okButton};
        JOptionPane optionPane = new JOptionPane(message, JOptionPane.INFORMATION_MESSAGE, JOptionPane.OK_OPTION,
                UIManager.getIcon("OptionPane.informationIcon"), options);
        popup.setContentPane(optionPane);
        popup.pack();
        popup.setLocationRelativeTo(Client.getInstance().getFrame());
        popup.setVisible(true);

        //new ShowMessage(true, message.toString(),"I");
    }

    public void requestHistory() {
        AlertStore.getSharedInstance().clear();
        AlertFrame msg = new AlertFrame();
        msg.setType(Meta.ALERT_HIS);
        msg.setClientAlertID(System.currentTimeMillis() + "" + Settings.getUserID()); // time+user
        msg.setLogingID(Settings.getUserID());
        SendQFactory.addData(Constants.PATH_PRIMARY, msg.getMsgString());
        msg = null;
    }

    public void deleteAlert() {

        try {
            if (table.getTable().getSelectedRow() >=0){
                ShowMessage oMessage = new ShowMessage(Language.getString("DELETE_ALERT_RECORD"), "W", true);
                int i = oMessage.getReturnValue();
                oMessage = null;
                if (i == 1) {
                    AlertRecord record = ((AlertTableModel) table.getTable().getModel()).getRecordAtRow(table.getTable().getSelectedRow());
                    AlertFrame msg = new AlertFrame();
                    msg.setType("" + Meta.ALERT_MESSAGE);

                    msg.setSend_Method(Meta.ALERT_MODE_TW);
                    msg.setType(Meta.ALERT_DELETE);
                    msg.setClientAlertID(record.getID());
                    msg.setLogingID(Settings.getUserID());

                    SendQFactory.addData(Constants.PATH_PRIMARY, msg.getMsgString());
                    SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Alert, "Delete");
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void editAlert() {
        try {
            if (table.getTable().getSelectedRow() >=0){
                AlertRecord record = ((AlertTableModel) table.getTable().getModel()).getRecordAtRow(table.getTable().getSelectedRow());
                AlertSetup alert = new AlertSetup(record);
                Client.getInstance().getDesktop().add(alert);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     * Invoked when the mouse button has been clicked (pressed
     * and released) on a component.
     */
    public void mouseClicked(MouseEvent e) {
        if (SwingUtilities.isLeftMouseButton(e)) {
            if (e.getClickCount() > 1) {
                editAlert();
            }
        }
    }

    /**
     * Invoked when a mouse button has been pressed on a component.
     */
    public void mousePressed(MouseEvent e) {

    }

    /**
     * Invoked when a mouse button has been released on a component.
     */
    public void mouseReleased(MouseEvent e) {

    }

    /**
     * Invoked when the mouse enters a component.
     */
    public void mouseEntered(MouseEvent e) {

    }

    /**
     * Invoked when the mouse exits a component.
     */
    public void mouseExited(MouseEvent e) {

    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("DEL")) {
            deleteAlert();
        } else if (e.getActionCommand().equals("EDIT")) {
            editAlert();
        } else if (e.getActionCommand().equals("CLOSE")) {
            frame.setVisible(false);
        }
    }

    public void show() {
        frame.show();
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(frame.getContentPane());
        table.getTable().getTableHeader().setBackground(Theme.getColor("BOARD_TABLE_HEAD_BGCOLOR"));
        table.getTable().getTableHeader().setForeground(Theme.getColor("BOARD_TABLE_HEAD_FGCOLOR"));

    }

    public void playSound() {
        SwingUtilities.invokeLater(new Runnable() {
            public void run() {
                try {
                    if ((System.currentTimeMillis() - lastAlrtTime) > 2000) { // gap between 2 consecative sounds
                        lastAlrtTime = System.currentTimeMillis();
                        NativeMethods.play(System.getProperties().get("user.dir") + "\\Sounds\\Alert.wav", 1);
                        System.out.println("sound played");
                    }
                } catch (Exception e) {
                    Toolkit.getDefaultToolkit().beep();
                    e.printStackTrace();
                }
            }
        });
    }

    public void createRequest(String sKey, String request) {
        String reqString = null;
        String symbol = null;
        String exCode = null;
        int instrument = -1;

        exCode = SharedMethods.getExchangeFromKey(sKey);
        symbol = SharedMethods.getSymbolFromKey(sKey);
        instrument = SharedMethods.getInstrumentTypeFromKey(sKey);
        reqString = request + Meta.DS + exCode + Meta.FD + symbol + Meta.FD + instrument + Meta.ERD;
        SendQFactory.addData(Constants.PATH_PRIMARY, reqString);

        SharedMethods.printLine(reqString);

        reqString = null;
        symbol = null;
        exCode = null;
//        symbolData = null;
    }
}