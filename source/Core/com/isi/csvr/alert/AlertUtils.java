package com.isi.csvr.alert;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;

import java.text.DecimalFormat;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 13, 2003
 * Time: 4:32:01 PM
 * To change this template use Options | File Templates.
 */
public class AlertUtils {

    private static Hashtable fieldFormats;
    private static Hashtable conditions;
    private static Hashtable validity;
    private static Hashtable expiration;
    private static String statusPending;
    private static String statusTriggered;
    private static String[] modes;

    private static DecimalFormat priceFormatter_1;
    private static DecimalFormat priceFormatter_2;
    private static DecimalFormat priceFormatter_3;
    private static DecimalFormat quantityFormatter;

    static {


        fieldFormats = new Hashtable();
        fieldFormats.put("" + Meta.ALERT_LASTTRADEPRICE, Language.getString("LAST"));
        fieldFormats.put("" + Meta.ALERT_LASTTRADEVOL, Language.getString("LASTTRADE_QTY"));
        fieldFormats.put("" + Meta.ALERT_BID, Language.getString("BID"));
        fieldFormats.put("" + Meta.ALERT_BIDQTY, Language.getString("BID_QTY"));
        fieldFormats.put("" + Meta.ALERT_ASK, Language.getString("OFFER"));
        fieldFormats.put("" + Meta.ALERT_OFFERQTY, Language.getString("OFFER_QTY"));
        fieldFormats.put("" + Meta.ALERT_CUMVOLUME, Language.getString("T_VOLUME"));
        fieldFormats.put("" + Meta.ALERT_CHANGE, Language.getString("PCT_CHANGE"));
        fieldFormats.put("" + Meta.ALERT_PERCENTCHANGE, Language.getString("CHANGE_SHORT"));

        conditions = new Hashtable();
        conditions.put("" + Meta.ALERT_GRATERTHAN, Language.getString("GREATER_THAN"));
        conditions.put("" + Meta.ALERT_GREATERTHANOREQUAL, Language.getString("GREATER_THAN_OR_EQUAL"));
        conditions.put("" + Meta.ALERT_LESSTHAN, Language.getString("LESS_THAN"));
        conditions.put("" + Meta.ALERT_LESSTHANEQUAL, Language.getString("LESS_THAN_OR_EQUAL"));
        conditions.put("" + Meta.ALERT_EQUAL, Language.getString("EQUAL"));

        validity = new Hashtable();
        validity.put("" + Meta.ALERT_EXPIRED_ONE_TIME, Language.getString("ONCE_ONLY"));
        validity.put("" + Meta.ALERT_EXPIRED_UNTIL_CANCEL, Language.getString("ONCE_A_DAY"));

        expiration = new Hashtable();
        expiration.put("" + 1, Language.getString("1_DAY"));
        expiration.put("" + 2, Language.getString("2_DAYS"));
        expiration.put("" + 3, Language.getString("3_DAYS"));

        statusPending = Language.getString("PENDING");
        statusTriggered = Language.getString("TRIGGERED");

        modes = new String[8];
        modes[0] = Language.getString("NA");
        modes[1] = Language.getString("POPUP");
        modes[2] = Language.getString("SMS");
        modes[3] = Language.getString("POPUP") + "/" + Language.getString("SMS");
        modes[4] = Language.getString("EMAIL");
        modes[5] = Language.getString("POPUP") + "/" + Language.getString("EMAIL");
        modes[6] = Language.getString("SMS") + "/" + Language.getString("EMAIL");
        modes[7] = Language.getString("POPUP") + "/" + Language.getString("SMS") + "/" + Language.getString("EMAIL");

        priceFormatter_1 = new DecimalFormat("#,##0.0");
        priceFormatter_2 = new DecimalFormat("#,##0.00");
        priceFormatter_3 = new DecimalFormat("#,##0.000");
        quantityFormatter = new DecimalFormat("#,##0");

    }

    public static FieldComboItem getFieldItem(int value) {
        return new FieldComboItem((String) fieldFormats.get("" + value), value);
    }

    public static FieldComboItem getConditionItem(int value) {
        return new FieldComboItem((String) conditions.get("" + value), value);
    }

    public static FieldComboItem getValidityItem(int value) {
        return new FieldComboItem((String) validity.get("" + value), value);
    }

    public static FieldComboItem getExpirationItem(int value) {
        return new FieldComboItem((String) expiration.get("" + value), value);
    }

    public static String getCriteria(String value) {
        try {
            return (String) fieldFormats.get("" + value);
        } catch (NumberFormatException e) {
            return "";
        }
    }

    public static String getAlertMode(int mode) {
        try {
            return modes[mode];
        } catch (Exception e) {
            return modes[0];
        }
    }

    public static String getStatusString(boolean triggered) {
        if (triggered)
            return statusTriggered;
        else
            return statusPending;
    }

    private static String formatQuantity(float number) {
        try {
            return quantityFormatter.format(number);
        } catch (Exception e) {
            return "0";
        }
    }

    private static String formatPrice(float number,int decimals) {
        try {
            switch (decimals){
                case 1:
                    return priceFormatter_1.format(number);
                case 2:
                    return priceFormatter_2.format(number);
                case 3:
                    return priceFormatter_3.format(number);
                default:
                    return priceFormatter_2.format(number);
            }

        } catch (Exception e) {
            return "0";
        }
    }

    public static String getFormattedString(float number, int column, int decimals) {
        switch (column) {
            case Meta.ALERT_LASTTRADEPRICE:
            case Meta.ALERT_BID:
            case Meta.ALERT_ASK:
            case Meta.ALERT_CHANGE:
            case Meta.ALERT_PERCENTCHANGE:
                return formatPrice(number,decimals);
            case Meta.ALERT_LASTTRADEVOL:
            case Meta.ALERT_BIDQTY:
            case Meta.ALERT_OFFERQTY:
            case +Meta.ALERT_CUMVOLUME:
                return formatQuantity(number);
            default:
                return "" + number;
        }
    }
}
