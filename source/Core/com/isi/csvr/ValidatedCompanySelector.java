package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.AssetsStore;
import com.isi.util.ColumnLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.KeyListener;
import java.awt.event.KeyEvent;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 11, 2005
 * Time: 4:32:12 PM
 */
public class ValidatedCompanySelector extends JDialog implements ActionListener, KeyListener {
    //    private ArrayList list;
    private String selectedRecord;
    private int mode;

    public static final int MODE_EXTERNAL_VALIDATION = 0;
    public static final int MODE_INTERNAL_VALIDATION = 1;
    public static byte CANCELPRESSED = 0;
    ButtonGroup buttonGroup;
    private ArrayList<JRadioButton> buttonList;
    private int selectedindex = 0;

    public ValidatedCompanySelector(String frame, int mode) {
        super(Client.getInstance().getFrame(), true);
        this.mode = mode;
        setUndecorated(true);
        createUI(frame);
        GUISettings.applyOrientation(this);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setLocationRelativeTo(Client.getInstance().getFrame());
    }

    private void createUI(String frame) {
        setLayout(new BorderLayout());

        JLabel title = new JLabel(Language.getString("SELECT_VALIDATED_COMPANY"));
        add(title, BorderLayout.NORTH);

        JPanel companypanel = new JPanel();
        add(companypanel, BorderLayout.CENTER);

        buttonGroup = new ButtonGroup();
        buttonList = new ArrayList<JRadioButton>();
        ColumnLayout columnLayout = new ColumnLayout();
        companypanel.setLayout(columnLayout);

        if (mode == MODE_EXTERNAL_VALIDATION) {
            String[] records = frame.split(Meta.FD);
            for (int i = 1; i < records.length; i++) {
                String[] fields = records[i].split(Meta.ID);
                String exchangeCode = fields[0].trim();
                String symbolCode = fields[1].trim();

//                if (ExchangeStore.getSharedInstance().isValidExchange(exchangeCode) && !ExchangeStore.isExpired(exchangeCode) && !ExchangeStore.isInactive(exchangeCode)) {
                Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
                JRadioButton label;
                /*try {
                        if ((exchange.hasSubMarkets()) && (symbolCode.indexOf(Constants.MARKET_SEPERATOR_CHARACTER) > 0)){
                            String marktCode = symbolCode.split(Constants.MARKET_SEPERATOR_CHARACTER)[1];
                            String marketcaption = ExchangeStore.getSharedInstance().getMarket(exchangeCode, marktCode).getDescription();
                            label = new JRadioButton(exchangeCode + " - " +
                                UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])) + " - " +marketcaption + " - " +"("+ AssetsStore.getSharedInstance().getAssetDescription(fields[2])+")");
                        } else {
                            label = new JRadioButton(exchangeCode+ " - " +
                                UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])) + " - "  +"("+ AssetsStore.getSharedInstance().getAssetDescription(fields[2])+")");
                        }
                    } catch (Exception e) {
                        label = new JRadioButton(exchangeCode + " - " +
                                UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4]))+ " - "  +"("+ AssetsStore.getSharedInstance().getAssetDescription(fields[2])+")");
                }*/


                try {
                    if ((exchange.hasSubMarkets()) && (symbolCode.indexOf(Constants.MARKET_SEPERATOR_CHARACTER) > 0)) {
                        String marktCode = symbolCode.split(Constants.MARKET_SEPERATOR_CHARACTER)[1];
                        String marketcaption = ExchangeStore.getSharedInstance().getMarket(exchangeCode, marktCode).getDescription();
//                        label = new JRadioButton(exchangeCode + " - " +
//                                UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])) + " - " + marketcaption + " - " + "(" + SharedMethods.getInstrumentType(Short.valueOf(fields[2])) + ")");
                        if (Language.isLTR()) {
                            label = new JRadioButton(ExchangeStore.getSharedInstance().getExchange(exchangeCode.trim()).getDisplayExchange() + " - " +
                                    UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])) + " - " + marketcaption + " - " + "(" + SharedMethods.getInstrumentType(Short.valueOf(fields[2])) + ")");
                        } else {
                            label = new JRadioButton("(" + SharedMethods.getInstrumentType(Short.valueOf(fields[2])) + ")" + " - " + marketcaption + " - " + UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])) + " - " + ExchangeStore.getSharedInstance().getExchange(exchangeCode.trim()).getDisplayExchange());

                        }

                    } else {
                        if (Language.isLTR()) {
                            label = new JRadioButton(ExchangeStore.getSharedInstance().getExchange(exchangeCode.trim()).getDisplayExchange() + " - " +
                                    UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])) + " - " + "(" + SharedMethods.getInstrumentType(Short.valueOf(fields[2])) + ")");
                        } else {
                            label = new JRadioButton("(" + SharedMethods.getInstrumentType(Short.valueOf(fields[2])) + ")" + " - " + UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])) +
                                    " - " + ExchangeStore.getSharedInstance().getExchange(exchangeCode.trim()).getDisplayExchange());
                        }

                        //chandika
                        /* if (Language.isLTR()) {
                        label = new JRadioButton(ExchangeStore.getSharedInstance().getExchange(exchangeCode.trim()).getDisplayExchange() + " - " +
                                UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])) + " - " + "(" + SharedMethods.getInstrumentType(Short.valueOf(fields[2])) + ")");
                        } else {
                            label = new JRadioButton("(" + SharedMethods.getInstrumentType(Short.valueOf(fields[2])) + ")"+"-"+UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4]))+ " - "+ExchangeStore.getSharedInstance().getExchange(exchangeCode.trim()).getDisplayExchange()
                                                            );

                        }*/

                    }
                } catch (Exception e) {
//                    label = new JRadioButton(exchangeCode + " - " +
//                            UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])) + " - " + "(" + SharedMethods.getInstrumentType(Short.valueOf(fields[2])) + ")");
                    label = new JRadioButton(ExchangeStore.getSharedInstance().getExchange(exchangeCode.trim()).getDisplayExchange() + " - " +
                            UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])) + " - " + "(" + SharedMethods.getInstrumentType(Short.valueOf(fields[2])) + ")");

                }
                String key = null;
                try {
                    key = SharedMethods.getKey(exchangeCode, symbolCode, Integer.parseInt(fields[2])); //Issue Adding symbols to Watchlist after validate.
                } catch (Exception e) {
                    key = records[i];
                }
                label.setActionCommand(records[i]);
                label.addActionListener(this);
                if (i == 1) {
                    label.setSelected(true);
                    selectedRecord = records[i];
                }
                buttonGroup.add(label);
                buttonList.add(label);
                companypanel.add(label);
                label.addKeyListener(this);
            }
//            }
        } else {
            String[] records = frame.split(Meta.FD);
            for (int i = 0; i < records.length; i++) {
                String[] fields = records[i].split(Meta.ID);
                String exchange = fields[0].trim();
                String symbol = fields[1].trim();
                String description = fields[2].trim();
                int instrument = Integer.parseInt(fields[3].trim());
                if (ExchangeStore.getSharedInstance().isValidExchange(exchange)) {
                    JRadioButton label = new JRadioButton(exchange + " - " + description + " - " + SharedMethods.getInstrumentType((short) instrument));

                    //JRadioButton label = new JRadioButton(exchange + " - " + description);
                    label.addKeyListener(this);
                    label.setActionCommand(SharedMethods.getKey(exchange, symbol, instrument));
                    label.addActionListener(this);
                    if (i == 0) {
                        label.setSelected(true);
                        selectedRecord = SharedMethods.getKey(exchange, symbol, instrument);
                    }
                    buttonGroup.add(label);
                    companypanel.add(label);
                    buttonList.add(label);
                }
            }
        }

        JPanel buttonpanel = new JPanel();
        buttonpanel.setLayout(new FlowLayout(FlowLayout.CENTER, 2, 5));
        add(buttonpanel, BorderLayout.SOUTH);
        TWButton okButton = new TWButton(Language.getString("OK"));
        okButton.setActionCommand("OK");
        okButton.addActionListener(this);
        buttonpanel.add(okButton);
        TWButton cancelButton = new TWButton(Language.getString("CANCEL"));
        cancelButton.setActionCommand("CANCEL");
        buttonpanel.add(cancelButton);
        cancelButton.addActionListener(this);

        ((JPanel) getContentPane()).setBorder(BorderFactory.createCompoundBorder(
                GUISettings.getTWFrameBorder(),
                BorderFactory.createEmptyBorder(10, 10, 10, 10)));
        companypanel.addKeyListener(this);

        pack();
    }

    public String getSelectedRecord() {
        setVisible(true);
        if (!selectedRecord.equals("-1"))
            return selectedRecord;
        throw new NullPointerException();
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("OK")) {
            try {
                dispose();
            } catch (Exception e1) {
                e1.printStackTrace();
            }
        } else if (e.getActionCommand().equals("CANCEL")) {
            CANCELPRESSED = 1;
            selectedRecord = "-1";
            dispose();
        } else {
            selectedRecord = e.getActionCommand();
        }
    }

    public void keyTyped(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            if (selectedindex > 0) {
                selectedindex -= selectedindex;
            } else {
                selectedindex = buttonList.size() - 1;
            }
            buttonGroup.setSelected(buttonList.get(selectedindex).getModel(), true);

        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            if (selectedindex < buttonList.size() - 2) {
                selectedindex += selectedindex;
            } else {
                selectedindex = 0;
            }
            buttonGroup.setSelected(buttonList.get(selectedindex).getModel(), true);
        }
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_UP) {
            if (selectedindex > 0) {
                selectedindex = selectedindex - 1;
            } else {
                selectedindex = buttonList.size() - 1;
            }
            buttonGroup.setSelected(buttonList.get(selectedindex).getModel(), true);
            selectedRecord = buttonList.get(selectedindex).getActionCommand();

        } else if (e.getKeyCode() == KeyEvent.VK_DOWN) {
            if (selectedindex < buttonList.size() - 1) {
                selectedindex = selectedindex + 1;
            } else {
                selectedindex = 0;
            }
            buttonGroup.setSelected(buttonList.get(selectedindex).getModel(), true);
            selectedRecord = buttonList.get(selectedindex).getActionCommand();
        } else if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            try {
                dispose();
            } catch (Exception e1) {
                e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

    public void keyReleased(KeyEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
