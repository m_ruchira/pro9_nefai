package com.isi.csvr;

/*
 *  WindowArranger
 *
 *   Copyright (C) 2000  Michael Coury, Vincent Dupont
 *
 *   This program is free software; you can redistribute it and/or modify
 *   it under the terms of the GNU General Public License as published by
 *   the Free Software Foundation; either version 2 of the License, or
 *   (at your option) any later version.
 *
 *   This program is distributed in the hope that it will be useful,
 *   but WITHOUT ANY WARRANTY; without even the implied warranty of
 *   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *   GNU General Public License for more details.
 *
 *   You should have received a copy of the GNU General Public License
 *   along with this program; if not, write to the Free Software
 *   Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
 */


import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.NonResizeable;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.Vector;

/**
 * @author Michael Coury
 * @author Vincent Dupont
 * @version 1.0 07/24/2000
 */
public class WindowArranger implements java.io.Serializable {
    public static final int HORIZONTAL = 0,
            VERTICAL = 1,
            CASCADE = 2,
            ARRANGE = 3;

    // empty constructor.  Class methods are accessed statically
    public WindowArranger() {
    }

    // method to tile open windows in various styles:
    //    HORIZONTAL = horizontal tiling
    //    VERTICAL = vertical tiling
    //    CASCADE = cascade windows, resizing based on desktop size
    //    ARRANGE = arranges windows in a grid
    public static void tileFrames(JDesktopPane desk, int style) {
        Dimension deskDim = desk.getSize();
        int deskWidth = deskDim.width;
        int deskHeight = deskDim.height;
        JInternalFrame[] frames = desk.getAllFrames();
        int frameCount = frames.length;
        int frameWidth = 0;
        int frameHeight = 0;
        int xpos = 0;
        int ypos = 0;
        double scale = 0.6;
        int spacer = 30;
        int frameCounter = 0;
        Vector frameVec = new Vector(1, 1);
        boolean areIcons = false;
        int tempy = 0, tempx = 0;

        for (int i = 0; i < frameCount; i++) {
            if (frames[i].isVisible() && !frames[i].isIcon()) {
                if (frames[i].isMaximum()) {
                    try {
                        frames[i].setMaximum(false);
                    } catch (Exception e) {
                    }
                }
                frameVec.addElement(frames[i]);
                frameCounter++;
            } else if (frames[i].isIcon()) areIcons = true;
        }
        if (areIcons) deskHeight = deskHeight - 50;
        switch (style) {
            case (HORIZONTAL):
                for (int i = 0; i < frameCounter; i++) {
                    JInternalFrame temp = (JInternalFrame) frameVec.elementAt(i);
                    frameWidth = deskWidth;
                    frameHeight = (deskHeight / frameCounter);
                    if (temp.isResizable())
                        temp.reshape(xpos, ypos, frameWidth, frameHeight);
                    else
                        temp.setLocation(xpos, ypos);
                    ypos = ypos + frameHeight;
                    temp.moveToFront();
                }
                break;
            case (VERTICAL):
                for (int i = 0; i < frameCounter; i++) {
                    JInternalFrame temp = (JInternalFrame) frameVec.elementAt(i);
                    frameWidth = (int) (deskWidth / frameCounter);
                    frameHeight = deskHeight;
                    if (temp.isResizable())
                        temp.reshape(xpos, ypos, frameWidth, frameHeight);
                    else
                        temp.setLocation(xpos, ypos);
                    xpos = xpos + frameWidth;
                    temp.moveToFront();
                }
                break;
            case (CASCADE):
                for (int i = 0; i < frameCounter; i++) {
                    JInternalFrame temp = (JInternalFrame) frameVec.elementAt(i);
                    frameWidth = (int) (deskWidth * scale);
                    frameHeight = (int) (deskHeight * scale);
                    if ((temp.isResizable()) && (!(temp instanceof NonResizeable)))
                        temp.reshape(xpos, ypos, frameWidth, frameHeight);
                    else
                        temp.setLocation(xpos, ypos);
                    temp.moveToFront();
                    xpos = xpos + spacer;
                    ypos = ypos + spacer;
                    if ((xpos + frameWidth > deskWidth) || (ypos + frameHeight > deskHeight - 50)) {
                        xpos = 0;
                        ypos = 0;
                    }
                    /*if ((temp instanceof InternalFrame) && (temp.isResizable())) { //Bug Id <#0014> errorneous code removed 
                        ((InternalFrame) temp).resizeFrame();
                    }*/
                    temp = null;
                }
                break;
            case (ARRANGE):
                int row = new Long(Math.round(Math.sqrt(new Integer(frameCounter).doubleValue()))).intValue();
                if (row == 0) break;
                int col = frameCounter / row;
                if (col == 0) break;
                int rem = frameCounter % row;
                int rowCount = 1;
                frameWidth = (int) deskWidth / col;
                frameHeight = (int) deskHeight / row;

                for (int i = 0; i < frameCounter; i++) {
                    JInternalFrame temp = (JInternalFrame) frameVec.elementAt(i);
                    if (rowCount <= row - rem) {
                        if (temp.isResizable())
                            temp.reshape(xpos, ypos, frameWidth, frameHeight);
                        else
                            temp.setLocation(xpos, ypos);
                        if (xpos + 10 < deskWidth - frameWidth)
                            xpos = xpos + frameWidth;
                        else {
                            ypos = ypos + frameHeight;
                            xpos = 0;
                            rowCount++;
                        }
                    } else {
                        frameWidth = (int) deskWidth / (col + 1);
                        if (temp.isResizable())
                            temp.reshape(xpos, ypos, frameWidth, frameHeight);
                        else
                            temp.setLocation(xpos, ypos);
                        if (xpos + 10 < deskWidth - frameWidth)
                            xpos = xpos + frameWidth;
                        else {
                            ypos = ypos + frameHeight;
                            xpos = 0;
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    // method to tile open windows in various styles:
    //    HORIZONTAL = horizontal tiling
    //    VERTICAL = vertical tiling
    //    CASCADE = cascade windows, resizing based on desktop size
    //    ARRANGE = arranges windows in a grid
    public static void tileAllFrames(LinkedList<JInternalFrame> frames, JDesktopPane desk, int style) {
        Dimension deskDim = desk.getSize();
        int deskWidth = deskDim.width;
        int deskHeight = deskDim.height;
        //JInternalFrame[] frames = desk.getAllFrames();
        //int frameCount = frames.length;
        int frameWidth = 0;
        int frameHeight = 0;
        int xpos = 0;
        int ypos = 0;
        double scale = 0.6;
        int spacer = 30;
        int frameCounter = 0;
        Vector<JInternalFrame> frameVec = new Vector<JInternalFrame>(1, 1);
        boolean areIcons = false;
//        JInternalFrame frame;
//        while(frames.hasMoreElements()){
//            frame = frames.nextElement();
        for (JInternalFrame frame : frames) {
            //if (frame.isVisible() && !frame.isIcon()) {
            if (frame.isMaximum()) {
                try {
                    frame.setMaximum(false);
                } catch (Exception e) {
                }
            }
            frameVec.addElement(frame);
            frameCounter++;
            // } else if (frame.isIcon()) areIcons = true;
        }

        if (areIcons) deskHeight = deskHeight - 50;
        switch (style) {
            case (HORIZONTAL):
                for (int i = 0; i < frameCounter; i++) {
                    JInternalFrame temp = frameVec.elementAt(i);
                    frameWidth = deskWidth;
                    frameHeight =(deskHeight / frameCounter);
                    if (temp.isResizable()){
                        if (temp.equals(frameVec.lastElement())){
                            temp.reshape(xpos, ypos, frameWidth, deskHeight - ypos);
                        }else {
                            temp.reshape(xpos, ypos, frameWidth, frameHeight);
                        }
                    }else{
                        temp.setLocation(xpos, ypos);
                    }
                    ypos = ypos + frameHeight;
                    temp.setVisible(true);
                    temp.moveToFront();
                }
                break;
            case (VERTICAL):
                for (int i = 0; i < frameCounter; i++) {
                    JInternalFrame temp = (JInternalFrame) frameVec.elementAt(i);
                    frameWidth = (int) (deskWidth / frameCounter);
                    frameHeight = deskHeight;
                    if (temp.isResizable())
                        temp.reshape(xpos, ypos, frameWidth, frameHeight);
                    else
                        temp.setLocation(xpos, ypos);
                    xpos = xpos + frameWidth;
                    temp.setVisible(true);
                    temp.moveToFront();
                }
                break;
            case (CASCADE):
                for (int i = 0; i < frameCounter; i++) {
                    JInternalFrame temp = (JInternalFrame) frameVec.elementAt(i);
                    frameWidth = (int) (deskWidth * scale);
                    frameHeight = (int) (deskHeight * scale);
                    if (temp.isResizable())
                        temp.reshape(xpos, ypos, frameWidth, frameHeight);
                    else
                        temp.setLocation(xpos, ypos);
                    temp.moveToFront();
                    xpos = xpos + spacer;
                    ypos = ypos + spacer;
                    if ((xpos + frameWidth > deskWidth) || (ypos + frameHeight > deskHeight - 50)) {
                        xpos = 0;
                        ypos = 0;
                    }
                    if (temp instanceof InternalFrame) {
                        ((InternalFrame) temp).resizeFrame();
                    }
                    temp.setVisible(true);
                    temp = null;
                }
                break;
            case (ARRANGE):
                int row = new Long(Math.round(Math.sqrt(new Integer(frameCounter).doubleValue()))).intValue();
                if (row == 0) break;
                int col = frameCounter / row;
                if (col == 0) break;
                int rem = frameCounter % row;
                int rowCount = 1;
                frameWidth = (int) deskWidth / col;
                frameHeight = (int) deskHeight / row;

                for (int i = 0; i < frameCounter; i++) {
                    JInternalFrame temp = (JInternalFrame) frameVec.elementAt(i);
                    if (rowCount <= row - rem) {
                        if (temp.isResizable())
                            temp.reshape(xpos, ypos, frameWidth, frameHeight);
                        else
                            temp.setLocation(xpos, ypos);
                        if (xpos + 10 < deskWidth - frameWidth)
                            xpos = xpos + frameWidth;
                        else {
                            ypos = ypos + frameHeight;
                            xpos = 0;
                            rowCount++;
                        }
                    } else {
                        frameWidth = (int) deskWidth / (col + 1);
                        if (temp.isResizable())
                            temp.reshape(xpos, ypos, frameWidth, frameHeight);
                        else
                            temp.setLocation(xpos, ypos);
                        if (xpos + 10 < deskWidth - frameWidth)
                            xpos = xpos + frameWidth;
                        else {
                            ypos = ypos + frameHeight;
                            xpos = 0;
                        }
                    }
                }
                break;
            default:
                break;
        }
    }

    // method to minimize all windows that are iconifiable
    public static void minimizeWindows(JDesktopPane desktopPane) {
        JInternalFrame[] openWindows = desktopPane.getAllFrames();
        for (int i = 0; i < openWindows.length; i++)
            if (openWindows[i].isIconifiable()) {
                try {
                    openWindows[i].setIcon(true);
                } catch (java.beans.PropertyVetoException pve) {
                    pve.printStackTrace();
                }
            }
    }

    // method to restore all minimized windows
    public static void restoreAll(JDesktopPane desktopPane) {
        JInternalFrame[] openWindows = desktopPane.getAllFrames();
        for (int i = 0; i < openWindows.length; i++) {
            if (openWindows[i].isIcon())
                try {
                    openWindows[i].setIcon(false);
                } catch (java.beans.PropertyVetoException pve) {
                    pve.printStackTrace();
                }
        }
    }

    // method to close all open windows
    public static void closeAllWindows(JDesktopPane desktopPane) {
        JInternalFrame[] openWindows = desktopPane.getAllFrames();
        for (int i = 0; i < openWindows.length; i++) {
            openWindows[i].dispose();
        }
    }

    private static void _windowSelected(ActionEvent e, JInternalFrame f) {
        try {
            if (f.isIcon()) f.setIcon(false);
            f.moveToFront();
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }


    //Comparator dedicated to sorting objects (File or JInternalFrame) by name
    //used to sort files in windowMenu so as to display dynamicaly files currently contained in desktop.
    public static final Comparator FILE_NAME = new Comparator() {
        public int compare(Object one, Object two) {
            String name1 = "", name2 = "";
            if ((one instanceof JInternalFrame) && (two instanceof JInternalFrame)) {
                name1 = ((JInternalFrame) one).getTitle();
                name2 = ((JInternalFrame) two).getTitle();
            }
            int index = 0;
            int test = 0;
            while (test == 0) {
                if (name1.charAt(index) == (name2.charAt(index))) {
                    //System.out.println(name1+" "+name1.charAt(index)+"    "+name2+" "+name2.charAt(index)+"  ("+index+")");
                    test = 0;
                    index++;
                    if ((index >= name1.length()) | (index >= name2.length())) break;
                } else if (name1.charAt(index) > name2.charAt(index))
                    test = 1;
                else
                    test = -1;
            }//while
            return test;
        }

        public boolean equals(Object object) {
            return object.equals(this);
        }

        public String toString() {
            return "FILE_NAME";
        }
    };
}


