package com.isi.csvr.tradebacklog;

import com.isi.csvr.Client;
import com.isi.csvr.WorkInProgressIndicator;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.UnicodeUtils;
import com.isi.csvr.shared.Settings;

import javax.swing.*;
import java.io.*;
import java.util.zip.ZipInputStream;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.ParseException;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Oct 6, 2005
 * Time: 2:37:16 PM
 * To change this template use File | Settings | File Templates.
 */
public class TradeExporter extends Thread {
    private String exchange;
    private String fileName;
    private boolean openAfterExtract;

    private SimpleDateFormat fileDateFormat = new SimpleDateFormat("HHmmss");
    private SimpleDateFormat outputDateFormat = new SimpleDateFormat("HH:mm:ss");

    public TradeExporter(String exchange, String fileName, boolean openAfterExtract) {
        super("TradeExporter");
        this.exchange = exchange;
        this.fileName = fileName;
        this.openAfterExtract = openAfterExtract;
    }

    public void run() {
        WorkInProgressIndicator workInProgressIndicator = null;
        try {
            String archive = Settings.getTradeArchivePath() + "\\" + exchange + "\\" + fileName;
            JFileChooser fileChooser = new JFileChooser();
            fileChooser.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
            int status = fileChooser.showSaveDialog(Client.getInstance().getFrame());
            if (status == JFileChooser.APPROVE_OPTION) {
                workInProgressIndicator = new WorkInProgressIndicator(WorkInProgressIndicator.EXPOPRTING);
                workInProgressIndicator.setVisible(true);
                Thread.yield();
                File folder = fileChooser.getSelectedFile();

                String newFileName = getNewFilename(fileName); // get the name of the file to be created
                FileOutputStream out = new FileOutputStream(folder + "/" + exchange + "_" + newFileName); // open the out file
                String headings = Language.getString("EXPORT_ALL_TRADES_COLS"); // write column headings
                out.write(UnicodeUtils.getNativeString(headings).getBytes());
                out.write("\r\n".getBytes());

                /* write file data */
                ZipInputStream zIn = new ZipInputStream(new FileInputStream(archive));
                DataInputStream in = new DataInputStream(zIn);
                zIn.getNextEntry();
                String data;
                int count = 0;
                while (true) {
                    count++;
                    data = in.readLine();
                    if (data != null) {
                        out.write(getFormattedData(exchange, data));
                        out.write("\r\n".getBytes());
                        if ((count % 100) == 0) {
                            Thread.sleep(1);
                        }
                    } else {
                        break;
                    }
                }
                out.flush();
                out.close();
                out = null;
                zIn.close();
                zIn = null;
                data = null;

                if (openAfterExtract) {
                    try {
                        Runtime.getRuntime().exec("start notepad " + "\"" + folder + "/" + exchange + "_" + newFileName + "\"");
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                }
                workInProgressIndicator.dispose();
                workInProgressIndicator = null;
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        if (workInProgressIndicator != null) {
            workInProgressIndicator.dispose();
        }
        Runtime.getRuntime().gc();
    }

    private byte[] getFormattedData(String exchange, String record) throws Exception {
        StringBuilder buffer = new StringBuilder();
        String[] fields = record.split(",");
        Date date = new Date(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, fileDateFormat.parse(fields[1]).getTime()));
        fields[1] = fields[0];
        fields[0] = outputDateFormat.format(date);

        for (int i = 0; i < fields.length ; i++) {
            buffer.append(",");
            buffer.append(fields[i]);
        }
        fields = null;
        date = null;
        return buffer.toString().substring(1).getBytes();
    }

    private String getNewFilename(String fileName) {
        SimpleDateFormat dateFormatOut = new SimpleDateFormat("yyyyMMdd'.txt'");
        SimpleDateFormat dateFormatIn = new SimpleDateFormat("yyyyMMdd");

        try {
            return dateFormatOut.format(dateFormatIn.parse(fileName.substring(0, 8)));
        } catch (ParseException e) {
            return null;
        }
    }
}
