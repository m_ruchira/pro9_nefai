package com.isi.csvr.tradebacklog;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 21, 2003
 * Time: 11:07:14 AM
 * To change this template use Options | File Templates.
 */
public interface TradeDataChangeListener {

    public void dataChanged();

    public void listDownloaded();
}
