package com.isi.csvr.tradebacklog;

import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.history.HistorySettings;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.datastore.*;

import javax.swing.*;
import javax.swing.event.ListDataListener;
import java.io.File;
import java.io.FilenameFilter;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 21, 2003
 * Time: 9:26:58 AM
 * To change this template use Options | File Templates.
 */
public class TradeBacklogStore implements ListModel, Comparator, ConnectionListener, FilenameFilter {

    private Hashtable<String, ArrayList<TradeFileListItem>> dataStore;
    private static TradeBacklogStore self = null;
    private TradeDataChangeListener dataListener;
    private SimpleDateFormat dateFormatOut;
    private SimpleDateFormat dateFormatIn;
    private boolean autoDownloadRecent = false;
    public String currentFile;
    private String currentExchange;

    private TradeBacklogStore() {
        dataStore = new Hashtable<String, ArrayList<TradeFileListItem>>();
        //dataStore.setComparator(this);
        ConnectionNotifier.getInstance().addConnectionListener(this);
//        requestData();
        dateFormatIn = new SimpleDateFormat("yyyyMMdd");
        dateFormatOut = new SimpleDateFormat("dd/MM/yyyy");
        String value = HistorySettings.getProperty("AUTO_DOWNLOAD_LAST_TNS");
        if ((value == null) || (value.equals("")) || (value.equals("0"))) {
            setAutoDownloadRecent(false);
        } else {
            setAutoDownloadRecent(true);
        }
        value = null;
    }

    public static TradeBacklogStore getSharedInstance() {
        if (self == null) {
            self = new TradeBacklogStore();
        }
        return self;
    }

    public ArrayList<TradeFileListItem> getDataStore(String exchange) {
        ArrayList<TradeFileListItem> list = dataStore.get(exchange);
        if (list == null) {
            list = new ArrayList<TradeFileListItem>();
            dataStore.put(exchange, list);
        }
        return list;
    }

    public void clear() {
        dataStore.clear();
    }

    public synchronized void setData(String data) {
        try {
            String[] tokens = data.split(Meta.FD);
            String exchange = tokens[0];
            //System.out.println(" ===>  TnS History " + exchange + " " + data);
            if ((exchange!=null)&&!exchange.equals("")) {
            try {
                File path = new File(Settings.getAbsolutepath()+"trades/" + exchange);
                path.mkdirs();
            } catch (Exception e) {
            }
//            }
//            TradeBacklogStore.getSharedInstance().updateListFromFiles(exchange);

            for (int i = 1; i < tokens.length; i++) {
                String[] file = tokens[i].split(Meta.ID);
                TradeFileListItem item = new TradeFileListItem();
                try {
                    item.setDescription(dateFormatOut.format(dateFormatIn.parse(file[0].substring(0, 8))));
                } catch (Exception e) {
                    item.setDescription(file[i]);
                }
                currentFile = file[0];
                item.setFileName(currentFile);
                if(Integer.parseInt(file[1])==0){
                    item.setSize(1);
                }else{
                    item.setSize(Integer.parseInt(file[1]));
                }
                item = updateStore(exchange, item);
                File[] files = getDownloadedFiles(exchange);
                checkAvalability(files, item); // check if the file is already downloaded
                file = null;
                files = null;
                item = null;
            }
            dataListener.listDownloaded();
            if (Settings.isTCPMode()) {
                clearDumpFolder(exchange);
            }
            fireDataChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void updateListFromFiles(String exchange) {

        try {
            File path = new File(Settings.getTradeArchivePath() + exchange);
            File[] files = path.listFiles();
            path = null;

            getDataStore(exchange).clear();

            for (File file : files) {
                TradeFileListItem item = new TradeFileListItem();
                try {
                    item.setDescription(dateFormatOut.format(dateFormatIn.parse(file.getName().substring(0, 8))));
                    item.setFileName(file.getName());
                if(file.length()==0){
                    item.setSize(1);
                }else{
                    item.setSize((int)file.length());
                }
                    item.setStatus(TradeFileList.STATUS_SAVED);
                    updateStore(exchange, item);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

    }

    //public void downloadRecentFile(String exchange){
    /*try {
        if (isAutoDownloadRecent() && (Settings.getMarketStatus() != Settings.MARKET_OPEN)){
            checkRecentFile(exchange);
        }
    } catch (Exception e) {
        e.printStackTrace();  //To change body of catch statement use Options | File Templates.
    }*/
    //}

    public synchronized boolean checkRecentFile(String exchange) {
        try {
            currentFile = getRecentFilename(exchange);
            File[] files = getDownloadedFiles(exchange);
            TradeFileListItem ref = (getDataStore(exchange)).get(0);
            if ((ref.getStatus() != TradeFileList.STATUS_QUEUED) &&
                    (ref.getStatus() != TradeFileList.STATUS_DOWNLOADENG) &&
                    (ref.getStatus() != TradeFileList.STATUS_SAVED) &&
                    (!isSameSize(files, ref))) {
                ref.setStatus(TradeFileList.STATUS_QUEUED);
                TradesHistoryDownloader.getSharedInstance().startDownload();
                return true;
            }
            files = null;
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use Options | File Templates.
        }
        return false;
    }

    private void clearDumpFolder(String exchange) {
        File folder;
        File[] files;
        try {
//            if (exchange.equals("MSM")){
//            }
            ArrayList<TradeFileListItem>  items = getDataStore(exchange);
            folder = new File(Settings.getTradeArchivePath() + exchange);
            files = folder.listFiles();
            boolean found;
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                found = false;
                for (int j = 0; j < items.size(); j++) {
                    TradeFileListItem item = items.get(j);
                    if (file.getName().equals(item.getFileName())) {
                        item = null;
                        found = true;
                        break;
                    }
                    item = null;
                }
                if (!found) {
                    file.delete();
                }
                file = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            files = null;
            folder = null;
        }
    }

    public void reValidateRecentFile(String exchange) {
        currentFile = getRecentFilename(exchange);
        File[] files = getDownloadedFiles(exchange);
        TradeFileListItem item = getRecentFile(exchange);
        checkAvalability(files, item);
        files = null;
    }

    public String getRecentFilename(String exchange) {
        try {
            TradeFileListItem ref = (getDataStore(exchange)).get(0);
            return ref.getFileName();
        } catch (Exception e) {
            return null;
        }
    }

    public TradeFileListItem getRecentFile(String exchange) {
        try {
            TradeFileListItem ref = (getDataStore(exchange)).get(0);
            return ref;
        } catch (Exception e) {
            return null;
        }
    }

    public void fireDataChanged() {
        if (dataListener != null) {
            dataListener.dataChanged();
        }
    }

    public File[] getDownloadedFiles(String exchange) {
        File path = new File(Settings.getTradeArchivePath() + exchange);
        File[] files = path.listFiles(this);
        path = null;
        return files;
    }

    private boolean isSameSize(File[] files, TradeFileListItem item) {
        try {
            for (int i = 0; i < files.length; i++) {
                File file = files[i];
                if (file.getName().equals(item.getFileName())) {
                    if (file.length() == item.getSize()) {
                        return true;
                    } else {
                        return false;
                    }
                }
            }
        } catch (Exception e) {
        }
        return false;
    }

    public void checkAvalability(File[] files, TradeFileListItem item) {
        if ((item.getStatus() == TradeFileList.STATUS_NOT_SAVED) ||
                (item.getStatus() == TradeFileList.STATUS_SAVED) ||
                (item.getStatus() == TradeFileList.STATUS_ERROR)) {
            if (isSameSize(files, item)) { // check if the file is downloaded
                item.setStatus(TradeFileList.STATUS_SAVED);
            } else {
                item.setStatus(TradeFileList.STATUS_NOT_SAVED);
            }
        }
    }

    private TradeFileListItem updateStore(String exchange, TradeFileListItem item) {
        int index = Collections.binarySearch(getDataStore(exchange), item, this);   //indexOfContainingValue(item);
        if (index >= 0) { // item already in. Update fields only
            TradeFileListItem ref = (getDataStore(exchange)).get(index);
            ref.setSize(item.getSize());
            return ref;
        } else { // insert the item
            (getDataStore(exchange)).add((index + 1) * -1, item);
            return item;
        }
    }

    public void requestData() {
//        if (Settings.isConnected() && ExchangeStore.getSharedInstance().defaultCount()>0) {
//            SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, Meta.TRADE_HISTORY_LIST_REQUEST + Meta.DS + "0" + Meta.EOL,null); // time & sales file list
//        }
    }

    public void removeDataListener() {
        dataListener = null;
    }

    public void addDataListener(TradeDataChangeListener dataListener) {
        this.dataListener = dataListener;
    }

    public boolean isAutoDownloadRecent() {
        return autoDownloadRecent;
    }

    public void setAutoDownloadRecent(boolean autoDownloadRecent) {
        this.autoDownloadRecent = autoDownloadRecent;
    }

    public String getCurrentExchange() {
        return currentExchange;
    }

    public void setCurrentExchange(String currentExchange) {
        this.currentExchange = currentExchange;
    }

    /*---- List Model methods ----*/
    public int getSize() {
        try {
            return (getDataStore(getCurrentExchange())).size();
        } catch (Exception e) {
            return 0;
        }
    }

    public Object getElementAt(int index) {
        return (getDataStore(getCurrentExchange())).get(index);
    }

    public void addListDataListener(ListDataListener l) {
    }

    public void removeListDataListener(ListDataListener l) {
    }

    /*---- Comparator Methods ----*/
    public int compare(Object o1, Object o2) {
        TradeFileListItem i1 = (TradeFileListItem) o1;
        TradeFileListItem i2 = (TradeFileListItem) o2;

        return i2.getFileName().compareTo(i1.getFileName());
    }

    /*---- Connection Listener methods ----*/
    public void twConnected() {
//        if(ExchangeStore.getSharedInstance().defaultCount()>0){
//            SendQFactory.addContentData(Constants.CONTENT_PATH_PRIMARY, Meta.TRADE_HISTORY_LIST_REQUEST + Meta.DS + "0" + Meta.EOL, null);
//        }
    }

    public void twDisconnected() {
    }

    /*---- File Name filter methods ----*/
    public boolean accept(File dir, String name) {
        try {
            if (currentFile.equals(name))
                return true;
            else
                return false;
        } catch (Exception e) {
            return false;
        }
    }
}
