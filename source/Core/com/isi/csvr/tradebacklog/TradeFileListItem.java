package com.isi.csvr.tradebacklog;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 20, 2003
 * Time: 1:57:30 PM
 * To change this template use Options | File Templates.
 */
public class TradeFileListItem {
    private boolean checked = false;
    String description = null;
    String fileName;
    String progress;
    byte status = TradeFileList.STATUS_NOT_SAVED;
    int size;

    public boolean isChecked() {
        return checked;
    }

    public void setChecked(boolean checked) {
        this.checked = checked;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public byte getStatus() {

        return status;
    }

    public void setStatus(byte status) {
        this.status = status;
    }

    public int getSize() {
        return size;
    }

    public void setSize(int size) {
        this.size = size;
    }

    public String getProgress() {
        return progress;
    }

    public void setProgress(String progress) {
        this.progress = progress;
    }
}


