package com.isi.csvr.plugin;

import com.isi.csvr.plugin.event.PluginEvent;
import com.isi.csvr.plugin.loader.Tracker;

import java.util.Enumeration;
import java.util.Hashtable;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Mar 13, 2008
 * Time: 10:24:34 AM
 */
public class PluginStore {
    private static Hashtable<String, Plugin> store = new Hashtable<String, Plugin>();

    public static void putPlugin(String id, Plugin plugin){
        store.put(id, plugin);
    }

    public static Plugin getPlugin(String id){
        return store.get(id);
    }

    public static Plugin activate(String id){
        try {
            Plugin plugin = (Plugin) Tracker.get(id).newInstance();
            PluginStore.putPlugin(id, plugin);
            plugin.load();
            return plugin;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    public static void save(){
        Enumeration<Plugin> plugins = store.elements();
        while (plugins.hasMoreElements()){
            try {
                plugins.nextElement().save();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }
    public static Enumeration getPlugins(){
        return store.elements();
    }

    public static Hashtable<String, Plugin> getPluginStore(){
        return store;
    }

    public static synchronized void fireExchangesAdded(){
           try{
            Enumeration pluginEnum = store.elements();
            while(pluginEnum.hasMoreElements()){
                Plugin plugin=(Plugin)pluginEnum.nextElement();
                plugin.pluginEvent(PluginEvent.APPLICATION_LOADED);
            }
        }catch(Exception e1){
        }
    }

    public static synchronized void firePluginEvent(PluginEvent event, Object... data){
        Enumeration<Plugin> plugins = store.elements();
        Plugin plugin = null;
        while(plugins.hasMoreElements()){
            try {
                plugin = plugins.nextElement();
                plugin.pluginEvent(event, data);
                plugin = null;
            } catch(Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

   public static synchronized void fireApplyWorkspace(String viewSetting, String data){
        Enumeration<Plugin> plugins = store.elements();
        Plugin plugin = null;
        while(plugins.hasMoreElements()){
            try {
                plugin = plugins.nextElement();
                plugin.applyWorkspace(viewSetting, data);
                plugin = null;
            } catch(Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }

   public static synchronized String getWorkspaceString(String id){
        Enumeration<Plugin> plugins = store.elements();
        Plugin plugin = null;
        while(plugins.hasMoreElements()){
            try {
                plugin = plugins.nextElement();
                if(id.equals(plugin.getViewSettingID())){
                    return plugin.getWorkspaceString(id);
                }
            } catch(Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
       return null;
    }

   public static synchronized void setData(int id,Object... data){
        Enumeration<Plugin> plugins = store.elements();
        Plugin plugin = null;
        while(plugins.hasMoreElements()){
            try {
                plugin = plugins.nextElement();
                if(plugin.isDataNeeded()){
                    plugin.setData(id,data);
                }
                plugin = null;
            } catch(Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }
    }
}
