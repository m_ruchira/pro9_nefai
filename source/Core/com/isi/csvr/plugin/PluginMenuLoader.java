package com.isi.csvr.plugin;

import com.isi.csvr.TWMenu;
import com.isi.csvr.TWMenuItem;
import com.isi.csvr.TWSeparator;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.shared.nanoxml.XMLElement;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.io.FileInputStream;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Feb 28, 2008
 * Time: 9:44:07 AM
 */
public class PluginMenuLoader {

    private static ArrayList<JComponent> menus;
    private static boolean loaded = false;


    public synchronized static ArrayList<JComponent> loadMenu() {
        try {
            if (loaded) return null;

            FileInputStream in = new FileInputStream("plugin\\config.xml");
            StringBuilder buffer = new StringBuilder();
            menus = new ArrayList<JComponent>();
            byte[] data = new byte[1000];
            int len = 0;
            while (len >= 0) {
                len = in.read(data);
                if (len > 0) {
                    buffer.append(new String(data, 0, len));
                }
            }

            XMLElement root = new XMLElement();
            root.parseString(buffer.toString());
            ArrayList<XMLElement> rootElements = root.getChildren();

            for (XMLElement rootItem : rootElements) {
                if (rootItem.getName().equals("PLUGIN")) {
                    final String id = rootItem.getAttribute("ID");
                    //final String cLass = rootItem.getAttribute("CLASS");
                    if ((id == null)/* || (cLass == null)*/) continue; // bad plugin definition

                    final String activation = rootItem.getAttribute("ACTIVATION");
                    if ((activation != null) && (activation.equals("ONLOAD"))) {
                        try {
                            PluginStore.activate(id);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                    ArrayList<XMLElement> elements = rootItem.getChildren();
                    for (XMLElement element : elements) {
                        if (element.getAttribute("CATEGORY").equals("MAINMENU")) {
                            createMenuItems(element, null, menus, id);
                        } else if (element.getAttribute("CATEGORY").equals("SUBMENU")) {
                            for (JComponent component : menus) {
                                if (component instanceof TWMenu) {
                                    if (((TWMenu) component).getId() == Integer.parseInt(element.getAttribute("MENU_ID"))) {
                                        createMenuItems(element, (TWMenu) component, menus, id);
                                        break;
                                    }
                                }
                            }
                        }
                        element = null;
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        loaded = true;
        return menus;
    }

    private static void createMenuItems(XMLElement element, TWMenu menu, ArrayList<JComponent> store, final String id) {
        if (element.getName().equals("MENU")) {
            String icon = element.getAttribute("ICON");
            if (element.getAttribute("TYPE").equals("MENU_ITEM")) {
                String menuItemID;
                try {
                    menuItemID = element.getAttribute("ITEM_ID");
                } catch (Exception e) {
                    menuItemID = null;
                }
                final String itemID = menuItemID;
                final TWMenuItem item;
                if (icon == null) {
                    item = new TWMenuItem(UnicodeUtils.getNativeString(Language.getString(element.getAttribute("CAPTION"))));
                } else {
                    item = new TWMenuItem(UnicodeUtils.getNativeString(Language.getString(element.getAttribute("CAPTION"))), icon);
                }
                GUISettings.applyOrientation(item);
                String location = element.getAttribute("LOCATION");
                if (location != null) {
                    item.putClientProperty("LOCATION", location);
                }
                String sequence = element.getAttribute("SEQUENCE");
                if (sequence != null) {
                    item.putClientProperty("SEQUENCE", sequence);
                }
                item.putClientProperty("PLUGIN_ID", id);
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        try {
                            Plugin plugin = PluginStore.getPlugin(id);
                            if (plugin == null) {
                                plugin = PluginStore.activate(id);
                            }
                            plugin.showUI(true);
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                });
                if (menu != null) {
                    menu.add(item);
                } else {
                    store.add(item);
                }
            } else if(element.getAttribute("TYPE").equals("MENU")){
                final TWMenu item;
                int menuID = 0;
                try {
                    menuID = Integer.parseInt(element.getAttribute("MENU_ID"));
                } catch (Exception e) {
                }
                if (icon == null) {
                    item = new TWMenu(UnicodeUtils.getNativeString(Language.getString(element.getAttribute("CAPTION"))), menuID);
                } else {
                    item = new TWMenu(UnicodeUtils.getNativeString(Language.getString(element.getAttribute("CAPTION"))), icon, menuID);
                }
                GUISettings.applyOrientation(item);
                String location = element.getAttribute("LOCATION");
                if (location != null) {
                    item.putClientProperty("LOCATION", location);
                }
                String sequence = element.getAttribute("SEQUENCE");
                if (sequence != null) {
                    item.putClientProperty("SEQUENCE", sequence);
                }
                item.putClientProperty("PLUGIN_ID", id);
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        try {
                            Plugin plugin = PluginStore.getPlugin(id);
                            if (plugin == null) {
                                plugin = PluginStore.activate(id);
                            }
                            plugin.showUI();
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                });
                if (menu != null) {
                    menu.add(item);
                } else {
                    store.add(item);
                }
            } else if(element.getAttribute("TYPE").equals("SEPARATOR")){
                TWSeparator item = new TWSeparator();
                String location = element.getAttribute("LOCATION");
                if (location != null) {
                    item.putClientProperty("LOCATION", location);
                }
                String sequence = element.getAttribute("SEQUENCE");
                if (sequence != null) {
                    item.putClientProperty("SEQUENCE", sequence);
                }
                item.putClientProperty("PLUGIN_ID", id);
                if (menu != null) {
                    menu.add(item);
                } else {
                    store.add(item);
                }
            }
        }
    }

    public synchronized static void validateMenu() {
        if(menus == null) return;
         for (JComponent comp : menus){
            if(comp instanceof JMenu){
                String id = (String)comp.getClientProperty("PLUGIN_ID");
                Plugin plugin = PluginStore.getPlugin(id);
                if(plugin == null) continue;
                int windowType = plugin.getWindowTypeID();
                int windowTypeCategory = plugin.getWindowTypeCategory();
                switch(windowTypeCategory){
                    case Meta.WINDOW_TYPE_CATEGORY_INVALID:
                        break;
                    case Meta.WINDOW_TYPE_CATEGORY_EXCHANGE:
                        //Todo - need to implement this feature fully - Dilum
                        break;
                    case Meta.WINDOW_TYPE_CATEGORY_SYSTEM:
                        comp.setVisible(ExchangeStore.isValidSystemFinformationType(windowType));
                        break;
                    case Meta.WINDOW_TYPE_CATEGORY_BOTH:
                        comp.setVisible(ExchangeStore.isValidSystemFinformationType(windowType));
                        //Todo - need to implement this feature fully  - Dilum
                        break;
                    default:
                        break;
                }
            } else if(comp instanceof JMenuItem){
                String id = (String)comp.getClientProperty("PLUGIN_ID");
                Plugin plugin = PluginStore.getPlugin(id);
                if(plugin == null) continue;
                int windowType = plugin.getWindowTypeID();
                int windowTypeCategory = plugin.getWindowTypeCategory();
                switch(windowTypeCategory){
                    case Meta.WINDOW_TYPE_CATEGORY_INVALID:
                        break;
                    case Meta.WINDOW_TYPE_CATEGORY_EXCHANGE:
                        //Todo - need to implement this feature fully - Dilum
                        break;
                    case Meta.WINDOW_TYPE_CATEGORY_SYSTEM:
                        comp.setVisible(ExchangeStore.isValidSystemFinformationType(windowType));
                        break;
                    case Meta.WINDOW_TYPE_CATEGORY_BOTH:
                        comp.setVisible(ExchangeStore.isValidSystemFinformationType(windowType));
                        //Todo - need to implement this feature fully  - Dilum
                        break;
                    default:
                        break;
                }
            }
         }
    }

    /*public static List<Class> loadPlugins(String packageName) throws Exception {

        String packagePath = packageName.replace('.', '/');
        URL[] classpath = ((URLClassLoader) ClassLoader.getSystemClassLoader()).getURLs();
        List<Class> result = new ArrayList<Class>();
        for (URL url : classpath) {
            File file = new File(url.toURI());
            System.out.println("file path " + file.getPath());
            if (file.getPath().endsWith(".mlf")) {
                JarFile jarFile = new JarFile(file);
                for (Enumeration<JarEntry> entries = jarFile.entries(); entries.hasMoreElements();) {
                    String entryName = (entries.nextElement()).getName();
//                    System.out.println( entryName);
                    if (entryName.matches(packagePath + "/\\w*\\.class")) { // get only class files in package dir
//                    if (entryName.endsWith(".class")) { // get only class files in package dir
                        try {
                            ClassLoader classLoader = new URLClassLoader(new URL[] { url });
                            String className = entryName.replace('/', '.').substring(0, entryName.lastIndexOf('.'));
                            Class clazz = classLoader.loadClass(className);
                            Plugin plugin = (Plugin)clazz.newInstance();
                            if (plugin.isActivateOnLoad()){
                                plugin.load();
                            }
                            System.out.println("Loaded Plugin : " + clazz.toString());
                        } catch (Exception e) {
                            System.out.println("Not Loaded " + entryName);
                        }
//                        result.add(clazz);
                    }
                }
            } else { // directory
                File packageDirectory = new File(file.getPath() + "/" + packagePath);
                try {
                    for (File f: packageDirectory.listFiles()) {
                        try {
                            if (f.getPath().endsWith(".class")) {
                                String className = packageName + "." + f.getName().substring(0, f.getName().lastIndexOf('.'));
                                ClassLoader classLoader = new URLClassLoader(new URL[] { url });
                                Class clazz = classLoader.loadClass(className);
                                Plugin plugin = (Plugin)clazz.newInstance();
                                if (plugin.isActivateOnLoad()){
                                    plugin.load();
                                }
                                System.out.println("Loaded Plugin : " + clazz.toString());
//                                result.add(clazz);
                            }
                        } catch (Exception e) {
                            //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
                    }
                } catch (Exception e) {
                    //e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
        return result;
    }*/
}
