package com.isi.csvr.plugin;

import com.isi.csvr.table.Table;

import javax.swing.*;
import javax.swing.table.TableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Mar 13, 2008
 * Time: 11:34:04 AM
 */
public class PluginShared {


    public static void setRenderer(Table table, TableCellRenderer renderer) {
           table.getTable().setDefaultRenderer(Double.class, renderer);
           table.getTable().setDefaultRenderer(String.class, renderer);
           table.getTable().setDefaultRenderer(Float.class, renderer); //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
           table.getTable().setDefaultRenderer(Long.class, renderer);   //new PortfolioRenderer(g_asHeadings,g_aiRendIDs));
           table.getTable().setDefaultRenderer(Byte.class, renderer);
           table.getTable().setShowGrid(false);
           table.getTable().setIntercellSpacing(new Dimension(0, 0));
           table.getTable().setCellSelectionEnabled(true);
           table.getTable().setSelectionMode(ListSelectionModel.SINGLE_INTERVAL_SELECTION);
       }
}
