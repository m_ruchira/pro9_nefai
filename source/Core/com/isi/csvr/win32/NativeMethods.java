package com.isi.csvr.win32;

import com.isi.csvr.metastock.HistoryRecord;
import com.isi.csvr.metastock.SymbolRecord;
import com.isi.csvr.metastock.MetaStockSecurityInfo;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.help.HelpManager;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.util.MD5;
import com.jniwrapper.Int;
import com.jniwrapper.SingleFloat;
import com.jniwrapper.win32.automation.types.BStr;
import com.jniwrapper.win32.com.ComException;
import com.jniwrapper.win32.com.types.ClsCtx;
import com.jniwrapper.win32.ole.OleFunctions;
import com.mubasher.metaliblib.*;
import com.neva.Coroutine;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileReader;
import java.text.DecimalFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.Properties;
import java.util.List;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 *
 * @author unascribed
 * @version 1.0
 */

public class NativeMethods {
    private static final int NULL = 183;
    public static int HKEY_CLASSES_ROOT = 0x80000000;
    public static int HKEY_CURRENT_USER = 0x80000001;
    public static int HKEY_LOCAL_MACHINE = 0x80000002;
    public static int HKEY_USERS = 0x80000003;
    public static int HKEY_PERFORMANCE_DATA = 0x80000004;
    public static int HKEY_CURRENT_CONFIG = 0x80000005;
    public static int HKEY_DYN_DATA = 0x80000006;
    public static int KEY_ACCESS_ALL_ACCESS = 0xF003F;
    public static int KEY_READ = 0x20019;
    public static int KEY_QUERY_VALUE = 0x0001;
    public static int KEY_ENUMERATE_SUB_KEYS = 0x0008;

    public static int REG_NONE = 0;
    public static int REG_SZ = 1;
    public static int REG_EXPAND_SZ = 2;
    public static int REG_BINARY = 3;
    public static int REG_DWORD = 4;
    public static int REG_MULTI_SZ = 5;

    private static final String METASTOCK_INVOKE = "METASTOCK_INVOKE";

    public NativeMethods() {
    }


    public static boolean createMutex() {
        //* find the main window of the already running instance *//*
        Coroutine coro;
        try {
            String key = MD5.getHashString((String) System.getProperties().get("user.dir"));
            coro = new Coroutine("Kernel32", "CreateMutexA");
            coro.addArgNull();
            coro.addArg(true);
            coro.addArg(key);
            coro.invoke();
            if (coro.lastOsError() == NULL) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        } finally {
            coro = null;
        }
    }


   /* public static boolean createMutex() {
    /*//* find the main window of the already running instance *//**//*
        Coroutine coro;
        try {
            coro = new Coroutine("Kernel32", "CreateMutexA");
            coro.addArgNull();
            coro.addArg(true);
            coro.addArg("TWRegionalMutex");
            coro.invoke();
            if (coro.lastOsError() == NULL) {
                return false;
            } else {
                return true;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        } finally {
            coro = null;
        }
    }*/


    public static float getFreeDiskSpace(String drive) {
        float retVal = -1;
        Coroutine coro;
        try {
            coro = new Coroutine("Kernel32", "GetDiskFreeSpaceA");
            coro.addArg(drive);
            coro.addArg(new byte[8]);
            coro.addArg(new byte[8]);
            coro.addArg(new byte[8]);
            coro.addArg(new byte[8]);
            coro.invoke();
            long value = coro.longFromParameterAt(1) * coro.longFromParameterAt(2) * coro.longFromParameterAt(3);
            retVal = ((value * 1000) / (1024 * 1024 * 1024)) / 1000F;
        } catch (Exception e) {
        } finally {
            coro = null;
        }
        return retVal;
    }

    public static String getVolumeSerial() {
        long retVal = 0;
        Coroutine coro;
        try {
            String drive = ((String) System.getProperties().get("user.dir")).substring(0, 3);
            coro = new Coroutine("Kernel32", "GetVolumeInformationA");
            coro.addArg(drive);
            coro.addArg(new byte[10]);
            coro.addArg(10);
            coro.addArg(new byte[8]);
            coro.addArg(new byte[8]);
            coro.addArg(new byte[8]);
            coro.addArg(new byte[10]);
            coro.addArg(10);
            coro.invoke();
            retVal = (long) coro.longFromParameterAt(3);
        } catch (Exception e) {
        } finally {
            coro = null;
        }
        return getHex(retVal);
    }

    public static void showTutorial() {
        try {
            Runtime.getRuntime().exec("\"" + System.getProperties().get("user.dir") +
                    "\\Tutorial\\Tutorial.exe\"");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static void showHelp(String path) {
        Coroutine coro;
        try {
            coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/hhctrl.ocx", "HtmlHelpA");
            coro.addArg(getHandle());
            coro.addArg(path);
            coro.addArg(0);
            coro.addArg(0);
            coro.invoke();
        } catch (SecurityException e) {
            e.printStackTrace();
        } finally {
            coro = null;
        }
    }

    private static int getHandle() {
        Coroutine coro;
        try {
            coro = new Coroutine("USER32", "FindWindowA");
            coro.addArg("com.isi.csvr.Client");
            coro.addArg((String) null);
            coro.invoke();

            int hwnd = coro.answerAsInteger();
            coro = null;
            if (hwnd != 0) {
                return hwnd;
            } else {
                return 0;
            }
        } catch (SecurityException e) {
            e.printStackTrace();
            return 0;
        } finally {
            coro = null;
        }
    }

    /**
     * Checks if there are too many instances of application running
     *
     * @return true if more than 1 instance running
     */
    public static boolean tooManyInstances() {
        try {
            /* C++ definitions
                    #define SW_HIDE             0
                    #define SW_SHOWNORMAL       1
                    #define SW_NORMAL           1
                    #define SW_SHOWMINIMIZED    2
                    #define SW_SHOWMAXIMIZED    3
                    #define SW_MAXIMIZE         3
                    #define SW_SHOWNOACTIVATE   4
                    #define SW_SHOW             5
                    #define SW_MINIMIZE         6
                    #define SW_SHOWMINNOACTIVE  7
                    #define SW_SHOWNA           8
                    #define SW_RESTORE          9
                    #define SW_SHOWDEFAULT      10
                   */
            boolean tooManyInstances = false;

//			/* check for multiple instances */
//			Coroutine coro=new Coroutine(".\\system\\TWApi","CheckIfAppIsLoaded");
//			coro.addArg("TWt.exe");
//			coro.invoke();

            tooManyInstances = !createMutex(); // if mutex is created, this is the first instance
            tooManyInstances = false;         // hard coded by Randula
            //coro.answerAsBoolean();

            if (!tooManyInstances) {
                return false;
            }

            /* find the main window of the already running instance */
            Coroutine coro = new Coroutine("USER32", "FindWindowA");
//            coro.addArg("SunAwtFrame");
            coro.addArg("com.isi.csvr.Client");
            coro.addArg((String) null);
            coro.invoke();

            int hwnd = coro.answerAsInteger();
            if (hwnd != 0) {
                /* show the window if it is iconized */
                Coroutine coro2 = new Coroutine("USER32", "ShowWindow");
                coro2.addArg(hwnd);
                coro2.addArg(3);
                coro2.invoke();

                /* bring the window to focus */
                Coroutine coro3 = new Coroutine("USER32", "SetForegroundWindow");
                coro3.addArg(hwnd);
                coro3.invoke();
            }
            return true;
        }
        catch (Exception ex) {
            ex.printStackTrace();
            return true;
        }
    }

    public static String getOSName() {
        try {
            return System.getProperty("os.name");
        } catch (Exception e) {
            return "N/A";
        }
    }

    public static int getMemorySize() {
        /* find the main window of the already running instance */
        int[] data = new int[8];
        try {
            Coroutine coro = new Coroutine("Kernel32", "GlobalMemoryStatus");
            coro.addArg(data);
            coro.invoke();
            data = coro.intArrayFromParameterAt(0, 8);
            coro = null;
            return (data[2] / (1024 * 1024)) + 1; // add 1 to add the base (64k) memory
        } catch (Exception e) {
            return 0;
        }
    }

    public static String getCPUSpeed() {
        /* check for multiple instances */
        Coroutine coro;
        try {
            int i = 0;
            DecimalFormat formatM = new DecimalFormat("#,##0'M'");
            DecimalFormat formatG = new DecimalFormat("#,##0.00'G'");
            coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/TWApi", "GetCPUSpeed");
            coro.addArg(i);
            coro.invoke();

            i = coro.answerAsInteger();
            coro = null;
            if (i < 1000)
                return formatM.format(i);
            else
                return formatG.format((double) i / 1000);
        } catch (Exception e) {
            return "N/A";
        } finally {
            coro = null;
        }
    }

    public static String getRegValue(String key) {
        /* check for multiple instances */
        Coroutine coro;
        try {
            String value = null;
            coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/TWApi", "getRegValue");
            coro.addArg(key);
            coro.invoke();

            value = coro.answerAsString();
            return value;
        } catch (Exception e) {
            return null;
        } finally {
            coro = null;
        }
    }

    public static String getBuildDate() {

        File file = new File(System.getProperties().get("user.dir") + "/lib/mubasher.mlf");
        if (!file.exists()) {
            file = new File(System.getProperties().get("user.dir") + "/Pro.exe");
        }
        Date date = new Date(file.lastModified());
        file = null;
        SimpleDateFormat format = new SimpleDateFormat("dd/MM/yyyy");
        return format.format(date);
    }

    public static String getHex(long value) {
        int rem = 0;
        String hex = "";
        char hexDigit[] =
                {
                        '0', '1', '2', '3', '4', '5', '6', '7',
                        '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
                };
        try {
            while (value > 16) {
                rem = (int) (value % 16);
                value = value / 16;
                hex = hexDigit[rem] + hex;
            }
            hex = hexDigit[(int) value] + hex;
            return hex;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        } finally {
            hex = null;
            hexDigit = null;
        }

    }

    public static String getJetVersion() {
        File jetFolder;
        String[] files;

        try {
            jetFolder = new File("xjre/jetrt");
            files = jetFolder.list();
            for (int i = 0; i < files.length; i++) {
                if (files[i].toLowerCase().startsWith("xkrn")) {
                    return files[i];
                }
            }
            return "N/A";
        } catch (Exception e) {
            //e.printStackTrace();
            return "Sun " + System.getProperties().getProperty("java.version");
        } finally {
            jetFolder = null;
            files = null;
        }
    }

    public static String getEnvironmentVariable(String var) {
        Coroutine coro;
        String value = new String();
        int length = 10;
        try {
            coro = new Coroutine("kernel32", "GetEnvironmentVariableA");
            coro.addArg(var);
            coro.addArg(value);
            coro.addArg(length);
            coro.invoke();
            value = coro.StringFromParameterAt(1);
            return value;
        } catch (Exception e) {
            e.printStackTrace();
            return "";
        } finally {
            coro = null;
        }

    }

    public static void setEnvironmentVariable(String var, String value) {
        Coroutine coro;
        try {
            coro = new Coroutine("kernel32", "SetEnvironmentVariableA");
            coro.addArg(var);
            coro.addArg(value);
            coro.invoke();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            coro = null;
        }
    }

    public static void writeRegValue(int root, String path, String id, String value) {
        int[] handle = new int[1];
        int sucess = RegOpenKey(root, path, KEY_ACCESS_ALL_ACCESS, handle);
        if (sucess == 0) {
            sucess = RegSetValue(handle[0], id, value);
            RegCloseKey(handle[0]);
        }
    }

    public static int RegCloseKey(int key) throws SecurityException {
        Coroutine co = new Coroutine("advapi32", "RegCloseKey");
        co.addArg(key);
        if (co.invoke() != 0)
            return -1;
        int ret = co.answerAsInteger();
        if (ret != 0)
            return ret;
        return 0;
    }

    public static int RegSetValue(int key, String valueName, String data) throws SecurityException {
        Coroutine co = new Coroutine("advapi32", "RegSetValueExA");
        co.addArg(key);
        co.addArg(valueName);
        co.addArgNull();
        co.addArg(REG_SZ);
        co.addArg(data);
        co.addArg(data.length() + 1);
        if (co.invoke() != 0)
            return -1;
        int ret = co.answerAsInteger();
        if (ret != 0)
            return ret;
        return 0;

    }

    public static String getTradeStationPath() throws SecurityException {
        String valueName = "SOFTWARE\\TradeStation Technologies\\TradeStation\\Versions";
        int[] handle = new int[1];
        int sucess;
        String path = null;
        String subKey = getSubkey(NativeMethods.HKEY_LOCAL_MACHINE, valueName, 0);
        if (subKey != null) {
            valueName = valueName + "\\" + subKey;
            sucess = RegOpenKey(NativeMethods.HKEY_LOCAL_MACHINE, valueName, KEY_QUERY_VALUE, handle);
            if (sucess == 0) {
                byte[] val = RegQueryValue(handle[0], "RootDirectory", new int[1]);
                if (val != null) {
                    path = new String(val).trim();
                }
                RegCloseKey(handle[0]);
            }
        }
        return path;

    }

    public static String getSubkey(int key, String valueName, int index) {
        int[] handle = new int[1];
        int sucess = RegOpenKey(key, valueName, KEY_ENUMERATE_SUB_KEYS, handle);
        String nameStr = null;
        if (sucess == 0) {
            byte[] name = regEnumKey(handle[0], index);
            if (name != null)
                nameStr = new String(name).trim();
        }
        RegCloseKey(handle[0]);
        return nameStr;
    }

    public static int getSubKeyCount(int key, String valueName) throws SecurityException {
        int[] handle = new int[1];
        int count = 0;
        int sucess = RegOpenKey(key, valueName, KEY_QUERY_VALUE, handle);
        if (sucess == 0) {
            count = regQueryInfoKey(handle[0]);
        }
        RegCloseKey(handle[0]);
        return count;
    }

    public static byte[] regEnumKey(int key, int index) {
        Coroutine co;
        int bufferSize = 64;
        while (true) {
            co = new Coroutine("advapi32", "RegEnumKeyExA");
            co.addArg(key);
            co.addArg(0);
            co.addArg(new byte[bufferSize]);
            co.addArg("");
            co.addArgNull();
            co.addArgNull();
            co.addArgNull();
            co.addArgNull();
            if (co.invoke() != 0)
                return null;
            break;
        }
        return co.byteArrayFromParameterAt(2, bufferSize);
    }


    //returns the number of sub keys

    public static int regQueryInfoKey(int key) {
        Coroutine co;
        int ret;
        int[] cSubKeys = new int[1];
        while (true) {
            co = new Coroutine("advapi32", "RegQueryInfoKeyA");
            co.addArg(key);
            co.addArgNull();
            co.addArgNull();
            co.addArgNull();
            co.addArg(cSubKeys);
            co.addArgNull();
            co.addArgNull();
            co.addArgNull();
            co.addArgNull();
            co.addArgNull();
            co.addArgNull();
            co.addArgNull();
            if (co.invoke() != 0)
                return -1;
            ret = co.intFromParameterAt(4);
            return ret;
        }
    }

    public static byte[] RegQueryValue(int key, String valueName, int[] type) throws SecurityException {

        int ERROR_MORE_DATA = 234;
        int bufferSize = 10;
        boolean success = false;
        Coroutine co;
        int ret;
        while (true) {
            co = new Coroutine("advapi32", "RegQueryValueExA");
            co.addArg(key);
            co.addArg(valueName);
            co.addArgNull();
            co.addArg(new byte[4]);
            co.addArg(new byte[bufferSize]);
            byte[] sz = new byte[4];
            Coroutine.setDWORDAtOffset(sz, bufferSize, 0);
            co.addArg(sz);
            if (co.invoke() != 0)
                return null;
            ret = co.answerAsInteger();
            if (ret == 0) {
                success = true;
                bufferSize = co.intFromParameterAt(5);
                break;
            } else if (ERROR_MORE_DATA == ret)
                //try again using correct buffer size
                bufferSize = co.intFromParameterAt(5);
            else
                break;
        }
        if (!success)
            return null;
        type[0] = co.intFromParameterAt(3);
        return co.byteArrayFromParameterAt(4, bufferSize);
    }

    public static int RegOpenKey(int key, String subkey, int accessMask, int[] handle) throws SecurityException {
        Coroutine co = new Coroutine("advapi32", "RegOpenKeyExA");
        co.addArg(key);
        co.addArg(subkey);
        co.addArg(0);
        co.addArg(accessMask);
        co.addArg(new byte[4]);
        if (co.invoke() != 0)
            return -1;
        int ret = co.answerAsInteger();
        if (ret != 0)
            return ret;
        handle[0] = co.intFromParameterAt(4);
        return 0;
    }

    public static int RegDeleteKey(int key, String subkey) throws SecurityException {
        Coroutine co = new Coroutine("advapi32", "RegDeleteKeyA");
        co.addArg(key);
        co.addArg(subkey);
        if (co.invoke() != 0)
            return -1;
        int ret = co.answerAsInteger();
        if (ret != 0)
            return ret;
        return 0;
    }

    public static int RegDeleteValue(int key, String valueName) throws SecurityException {
        Coroutine co = new Coroutine("advapi32", "RegDeleteValueA");
        co.addArg(key);
        co.addArg(valueName);
        if (co.invoke() != 0)
            return -1;
        int ret = co.answerAsInteger();
        if (ret != 0)
            return ret;
        return 0;
    }

    public static boolean play(String path, int how) {
        try {
            Coroutine coro = new Coroutine("WINMM", "PlaySound");
            coro.addArg(path);
            coro.addArg(0);
            coro.addArg(how);
            coro.invoke();
            return coro.answerAsBoolean();
        } catch (SecurityException e) {
            return false;
        }
    }

    // ------------ MetaStock Interface methods ----------------

    static {
        //System.loadLibrary("config" + "/MetaStock");             //Can't add the Settings.CONFIG_DATA_PATH  here because this is calling before the Setting class
//        System.loadLibrary(Settings.getAbsolutepath()+"system/MetaStock");
    }

    public static int MSOpenFiles(String path) {
        //System.out.println("In Open files " + path);
        synchronized (METASTOCK_INVOKE) {
            Coroutine coro;
            try {
                coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MetaStock.dll", "OpenFiles");
                coro.addArg(path);
                int sucess = coro.invoke();
                //System.out.println("Invoke status " + sucess);
                //System.out.println("Invoke answer " + coro.answerAsInteger());
                if (coro.lastOsError() == NULL) {
                    return -1;
                } else {
                    return coro.answerAsInteger();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            } finally {
                coro = null;
                //System.out.println("Exit openfiles " + path);
            }
        }
    }

    public static SymbolRecord MSGetFirstSecurityInfo(int dirNumber) {
        synchronized (METASTOCK_INVOKE) {
            Coroutine coro;
            try {
                coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MetaStock.dll", "getFirstSecurity");
                byte[] key = new byte[19];
                Coroutine.setWORDAtOffset(key, key.length, 0);

                coro.addArg(dirNumber);
                coro.addArg(key);
                coro.invoke();

                boolean sucess = coro.answerAsBoolean();

                if (sucess) {
                    byte[] handle = coro.byteArrayFromParameterAt(1, 19);
                    SymbolRecord symbolRecord = new SymbolRecord();
                    symbolRecord.handle = Coroutine.getDWORDAtOffset(handle, 0);
                    symbolRecord.symbol = new String(handle, 4, 15);
                    return symbolRecord;
                }
                return null;
                //}
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                coro = null;
            }
        }
    }

    public static int MSGetSecurityCount(int dirNumber) {
        synchronized (METASTOCK_INVOKE) {
            Coroutine coro;
            try {
                coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MetaStock.dll", "getSecurityCount");
                coro.addArg(dirNumber);
                coro.invoke();
                return coro.answerAsInteger();

            } catch (Exception e) {
                e.printStackTrace();
                return 0;
            } finally {
                coro = null;
            }
        }
    }

    public static SymbolRecord[] MSGetSecurityInfo(int dirNumber) {
        synchronized (METASTOCK_INVOKE) {
            Coroutine coro;
            try {
                int count = MSGetSecurityCount(dirNumber);
                if ((count < 10000) && (count > 0)) {
                    System.out.println("Count " + count);
                    SymbolRecord[] symbolRecords = new SymbolRecord[count];

                    coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MetaStock.dll", "getSecurityInfo");
                    coro.addArg(dirNumber);

                    byte[] key = new byte[20 * count];
                    coro.addArg(key);
                    coro.invoke();

                    byte[] handle = coro.byteArrayFromParameterAt(1, 20 * count);
                    int i = 0;
                    try {
                        while (true) {
                            symbolRecords[i] = new SymbolRecord();
                            symbolRecords[i].handle = Coroutine.getDWORDAtOffset(handle, 20 * i);
                            symbolRecords[i].symbol = new String(handle, (20 * i) + 4, 15);
                            i++;
                        }
                    } catch (Exception e) {
                    }
                    return symbolRecords;
                } else {
                    return null;
                }

            } catch (Exception e) {
                e.printStackTrace();
                return null;
            } finally {
                coro = null;
            }
        }
    }

    public static int MSAddSecutiry(int dirNum, String description,
                                    String symbol, int type, short interval, byte displayUnits) {
        synchronized (METASTOCK_INVOKE) {
            Coroutine coro;
            try {
                coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MetaStock.dll", "AddNewSecurity");
                coro.addArg(dirNum);
                coro.addArg(description);
                coro.addArg(symbol);
                coro.addArg(type);
                coro.addArg(interval);
                coro.addArg(displayUnits);
                coro.invoke();
                if (coro.lastOsError() == NULL) {
                    return -1;
                } else {
                    return coro.answerAsInteger();
                }
            } catch (Exception e) {
                e.printStackTrace();
                return -1;
            } finally {
                coro = null;
            }
        }
    }

    //AddRecords(short /*[in]*/ _FileType , int /*[in]*/ _hSecurity , String /*[in]*/ _Path ,
    // String /*[in]*/ _StartDate , String /*[in]*/ _EndDate )  throws com.neva.COMException {

    public static int MSAddRecords(int type, int hSecurity, String path,
                                   String startDate, String endDate) {
        synchronized (METASTOCK_INVOKE) {
            Coroutine coro;
            try {
                coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MetaStock.dll", "AddRecords");
                coro.addArg(type);
                coro.addArg(hSecurity);
                coro.addArg(path);
                coro.addArg(startDate);
                coro.addArg(endDate);
                coro.invoke();
                if (coro.lastOsError() == NULL) {
                    return -1;
                } else {
                    return coro.answerAsInteger();
                }

            } catch (Exception e) {

                e.printStackTrace();
                return -1;
            } finally {
                coro = null;
            }
        }
    }

    public static int MSLockSecurity(int hSecurity) {
        synchronized (METASTOCK_INVOKE) {
            Coroutine coro;
            try {
                coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MetaStock.dll", "LockSecurity");
                coro.addArg(hSecurity);
                coro.invoke();
                if (coro.lastOsError() == NULL) {
                    return -1;
                } else {
                    return coro.answerAsInteger();
                }

            } catch (Exception e) {

                e.printStackTrace();
                return -1;
            } finally {
                coro = null;
            }
        }
    }

    public static int MSUnLockSecurity(int hSecurity) {
        synchronized (METASTOCK_INVOKE) {
            Coroutine coro;
            try {
                coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MetaStock.dll", "UnLockSecurity");
                coro.addArg(hSecurity);
                coro.invoke();
                if (coro.lastOsError() == NULL) {
                    return -1;
                } else {
                    return coro.answerAsInteger();
                }

            } catch (Exception e) {

                e.printStackTrace();
                return -1;
            } finally {
                coro = null;
            }
        }
    }

    public static int MSSort(int hSecurity) {
        synchronized (METASTOCK_INVOKE) {
            Coroutine coro;
            try {
                coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MetaStock.dll", "SortData");
                coro.addArg(hSecurity);
                coro.invoke();
                if (coro.lastOsError() == NULL) {
                    return -1;
                } else {
                    return coro.answerAsInteger();
                }

            } catch (Exception e) {

                e.printStackTrace();
                return -1;
            } finally {
                coro = null;
            }
        }
    }

    public static synchronized boolean MSInit(String app, String user, String key) {
        synchronized (METASTOCK_INVOKE) {
            Coroutine coro;
            try {
                coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MetaStock.dll", "InitMSFL");
                coro.addArg(app);
                coro.addArg(user);
                coro.addArg(key);
                coro.invoke();
                if (coro.lastOsError() == NULL) {
                    return false;
                } else {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            } finally {
                coro = null;
            }
        }
    }

    public static boolean MSDeleteDB(int dirNumber) {
        synchronized (METASTOCK_INVOKE) {
            Coroutine coro;
            try {
                coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MetaStock.dll", "DeleteDB");
                coro.addArg(dirNumber);
                coro.invoke();
                if (coro.lastOsError() == NULL) {
                    return false;
                } else {
                    return true;
                }
            } catch (Exception e) {
                e.printStackTrace();
                return false;
            } finally {
                coro = null;
            }
        }
    }

    public static void MSCloseDirectory(int dirNum) {
        synchronized (METASTOCK_INVOKE) {
            Coroutine coro;
            try {
                coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MetaStock.dll", "CloseDirectory");
                coro.addArg(dirNum);
                coro.invoke();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                coro = null;
            }
        }
    }

    public static void MSCloseSession() {
        synchronized (METASTOCK_INVOKE) {
            Coroutine coro;
            try {
                coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MetaStock.dll", "CloseSession");
                coro.invoke();
            } catch (Exception e) {
                e.printStackTrace();
            } finally {
                coro = null;
            }
        }
    }

    public static int MSAddIntraRecord2(int hSecurity, int year, int month, int day,
                                        int hour, int minute, double OpenPrice, double High, double Low, double ClosePrice,
                                        int Volume, double OpenInt) {
        synchronized (METASTOCK_INVOKE) {
            return MSAddIntraRecord(hSecurity, year, month, day,
                    hour, minute, OpenPrice, High, Low, ClosePrice,
                    Volume, OpenInt);
        }
    }

    private static native int MSAddIntraRecord(int hSecurity, int year, int month, int day,
                                               int hour, int minute, double OpenPrice, double High, double Low, double ClosePrice,
                                               int Volume, double OpenInt);

    /* --- End of MetaStock --- */


    public static void openDialerConnection(String number, String user, String pass) {
        Coroutine coro;
        try {
            coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MubasherDialer.dll", "openConnection");
            coro.addArg(number);
            coro.addArg(user);
            coro.addArg(pass);
            coro.invoke();
            System.out.println(coro.lastError());
            System.out.println(coro.lastOsError());
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            coro = null;
        }
    }


    public static int checkDialerStatus() {
        Coroutine coro;
        try {
            coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MubasherDialer.dll", "getConnectionStatus");
            coro.invoke();
            return coro.answerAsInteger();
        } catch (Exception e) {
            e.printStackTrace();
            return -1;
        } finally {
            coro = null;
        }
    }

    public static void showInsertAnalysisHelp(String section) {
        Coroutine coro;
        try {
            coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/hhctrl.ocx", "HtmlHelpA");
            coro.addArg(getHandle());
//                coro.addArg("./help/PROCHART_HLP_" + Language.getLanguageTag() + ".chm::/index.htm");

            coro.addArg("./help/" + Language.getLanguageTag() + "/" + HelpManager.getFileNameFromItemId("HELP_CHART_ANALISYS") + "/" + section);
//                coro.addArg("./help/PROCHART_HLP_" + Language.getLanguageTag() + ".chm::/html/index.htm");
            coro.addArg(0);
            coro.addArg(0);
            coro.invoke();
        } catch (SecurityException e) {
            e.printStackTrace();
        } finally {
            coro = null;
        }
    }


    public static void disconnectDialer() {
        Coroutine coro;
        try {
            coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/MubasherDialer.dll", "closeConnection");
            coro.invoke();
        } catch (Exception e) {
            e.printStackTrace();
        } finally {
            coro = null;
        }
    }

    /*public static void loadCompilerLibrary() {
        System.out.println("Loading Compiler lib");
        Coroutine loader = new Coroutine("Kernel32", "LoadLibraryA");
        loader.addArg("Compiler.dll");
        loader.invoke();
        System.err.println("> " + loader.lastError());
        System.err.println("> " + loader.lastOsError());
        loader = null;
        System.out.println("Compiler Lib Loaded");
    }*/

    public static void showHelpItem(String fileName) {
        Coroutine coro;
        try {
            coro = new Coroutine(Settings.CONFIG_DATA_PATH + "/hhctrl.ocx", "HtmlHelpA");
            coro.addArg(getHandle());
            coro.addArg("./help/" + Language.getLanguageTag() + "/" + fileName);
            coro.addArg(0);
            coro.addArg(0);
            coro.invoke();
        } catch (SecurityException e) {
            e.printStackTrace();
        } finally {
            coro = null;
        }
    }


    //MetaLib Methods----------------------------------------------------------------------

    public static void MLInit() {
        OleFunctions.oleInitialize();
        try {
            IMLRegistration reg = MLRegistration.create(ClsCtx.ALL);
            reg.setRegistrationInfo(new BStr("walid al-ballaa"), new BStr("611841f"));
        } catch (ComException ex) {
            ex.printStackTrace();
        }

    }

    public static void MLCloseConnection() {
        OleFunctions.oleUninitialize();
    }

    public static int MLGetSecurityCount(String path) {
        BStr dirPath = new BStr(path);
        IMLReader reader = MLReader.create(ClsCtx.ALL);
        try {
            reader.openDirectory(dirPath);
            return Integer.parseInt(reader.getIMaRecords().toString());
        } catch (Exception ex) {
            ex.printStackTrace();
            return 0;
        } finally {
            reader.closeDirectory();
        }
    }

    public static void MLDeleteDB(String path) {
        //-----------------------------

        BStr dirPath = new BStr(path);
        IMLWriter writer = MLWriter.create(ClsCtx.ALL);
        IMLReader reader = MLReader.create(ClsCtx.ALL);
        ArrayList secList = new ArrayList();

        //reading the security list
        try {
            reader.openDirectory(dirPath);
            while (reader.getIMaRecordsLeft().toLong() > 0) {
                reader.readMaster();
                secList.add(reader.getSMaSecSymbol());
            }
        } catch (ComException ex) {
            ex.printStackTrace();
        } finally {
            reader.closeDirectory();
        }

        //deleting the securities
        writer.openDirectory(dirPath);
        for (int i = 0; i < secList.size(); i++) {
            try {
                writer.deleteSecurity((BStr) secList.get(i));
            } catch (ComException ex) {
                System.out.println("--------- MS error cannot delete security file " + secList.get(i));
            }
        }
        writer.closeDirectory();
    }

    public static ArrayList<String> MLGetSecurityInfo(String path) {

        BStr dirPath = new BStr(path);
        int count = MLGetSecurityCount(path);
        ArrayList<String> compList = new ArrayList<String>(count);

        if (count > 0) {

            IMLReader reader = MLReader.create(ClsCtx.ALL);
            try {
                reader.openDirectory(dirPath);
                while (reader.getIMaRecordsLeft().toLong() > 0) {
                    reader.readMaster();
                    compList.add(reader.getSMaSecSymbol().toString());
                }
                return compList;

            } catch (Exception ex) {
                //ex.printStackTrace();
                return compList;
            }
            finally {
                reader.closeDirectory();
            }
        } else {
            return compList;
        }

    }

    public static int MLAddSecutiry(String dirPath, String description, String symbol) {

        BStr path = new BStr(dirPath);
        IMLWriter writer = MLWriter.create(ClsCtx.ALL);
        try {
            writer.openDirectory(path);
            writer.appendSecurity(new BStr(symbol), new BStr(description), new PERIODICITY(PERIODICITY.Daily));
            return 0;
        } catch (ComException ex) {
            //ex.printStackTrace();
            System.out.println("------ errror while creating file " + symbol);
            return -1;
        }
        finally {
            writer.closeDirectory();
        }

    }

    public static void MLUpdateHistoryChart(String securityName, String dirPath, IntraDayOHLC record, String exchange) {
        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");

        BStr path = new BStr(dirPath);
        IMLWriter writer = null;
        Date dateTime = new Date();

        try {
            writer = MLWriter.create(ClsCtx.ALL);
            writer.openDirectory(path);
            writer.openSecurityBySymbol(new BStr(securityName));

            dateTime.setTime(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, record.getTime() * 60000));
            Int date = new Int(Long.parseLong(dateFormatter.format(dateTime)));

            boolean exists = Boolean.parseBoolean(writer.getBDateExists(date).toString());
            if (exists) {
                try {
                    writer.deleteSecRecord(date);
                } catch (Exception ex) {

                }
            }
            writer.appendDataRec(date, new Int(0), new SingleFloat(record.getOpen()), new SingleFloat(record.getHigh()), new SingleFloat(record.getLow()),
                    new SingleFloat(record.getClose()), new SingleFloat(record.getVolume()), new SingleFloat(0));
            /*if (exists) {
                writer.changePriceFields(date, new Int(-1), new Int(-1), new SingleFloat(record.getOpen()), new SingleFloat(record.getHigh()), new SingleFloat(record.getLow()),
                        new SingleFloat(record.getClose()), new SingleFloat(record.getVolume()), new SingleFloat(0));
            } else {
                writer.appendDataRec(date, new Int(0), new SingleFloat(record.getOpen()), new SingleFloat(record.getHigh()), new SingleFloat(record.getLow()),
                        new SingleFloat(record.getClose()), new SingleFloat(record.getVolume()), new SingleFloat(0));
            }*/

            writer.updateChart();

        } catch (ComException ex) {
            ex.printStackTrace();
        } finally {
            writer.closeSecurity();
            writer.closeDirectory();
        }
    }

    public static int MLAddIntraRecords(String securityName, String dirPath, List records, String exchange) {

        SimpleDateFormat dateFormatter = new SimpleDateFormat("yyyyMMdd");
        SimpleDateFormat timeFormatter = new SimpleDateFormat("HHmmss");

        BStr path = new BStr(dirPath);
        IMLWriter writer = null;
        Date dateTime = new Date();

        try {
            writer = MLWriter.create(ClsCtx.ALL);
            writer.openDirectory(path);
            writer.openSecurityBySymbol(new BStr(securityName));

            while (true) {
                IntraDayOHLC ohlc = (IntraDayOHLC) records.remove(0);
                dateTime.setTime(ExchangeStore.getSharedInstance().getZoneAdjustedTimeFor(exchange, ohlc.getTime() * 60000));
                Int date = new Int(Long.parseLong(dateFormatter.format(dateTime)));
                Int time = new Int(Long.parseLong(timeFormatter.format(dateTime)));

                writer.appendDataRec(date, time, new SingleFloat(ohlc.getOpen()), new SingleFloat(ohlc.getHigh()), new SingleFloat(ohlc.getLow()),
                        new SingleFloat(ohlc.getClose()), new SingleFloat(ohlc.getVolume()), new SingleFloat(0));

                ohlc = null;
            }
        } catch (IndexOutOfBoundsException e) { // indicates no more records
            writer.sort();
            writer.updateChart();
            return 0;
        } catch (ComException ex) { //writer error
            System.out.println("----error writing data on symbol " + securityName); //todo
            return -1;
        } finally {
            writer.closeSecurity();
            writer.closeDirectory();
        }

    }

    public static void MLAddRecords(String securityName, String securityFilePath, String dirPath) {

        //Read from csv file
        ArrayList<HistoryRecord> records = new ArrayList<HistoryRecord>();
        try {
            File source = new File(securityFilePath);
            String data = null;
            String[] ohlcData = null;
            if (source.exists()) {
                BufferedReader in = new BufferedReader(new FileReader(source));
                while (true) {
                    data = in.readLine();
                    if (data == null) break;
                    ohlcData = data.split(Meta.FS);
                    HistoryRecord record = new HistoryRecord(Integer.parseInt(ohlcData[0]), Float.parseFloat(ohlcData[1]), Float.parseFloat(ohlcData[2]), Float.parseFloat(ohlcData[3]),
                            Float.parseFloat(ohlcData[4]), Float.parseFloat(ohlcData[8]));
                    records.add(record);
                }
                //in = null;
                in.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }


        //write to metastock file
        BStr path = new BStr(dirPath);
        IMLWriter writer = MLWriter.create(ClsCtx.ALL);

        try {
            writer.openDirectory(path);
            writer.openSecurityBySymbol(new BStr(securityName));
            for (HistoryRecord record : records) {
                writer.appendDataRec(new Int(record.date), new Int(0), new SingleFloat(record.open), new SingleFloat(record.high), new SingleFloat(record.low),
                        new SingleFloat(record.close), new SingleFloat(record.volume), new SingleFloat(0));
            }
        } catch (ComException ex) {
            ex.printStackTrace();
        }
        finally {
            writer.closeSecurity();
            writer.closeDirectory();
        }

    }

    public static void MLAddSymbols(String path, ArrayList<MetaStockSecurityInfo> infoList, boolean isIntraday) {
        if (infoList.size() > 0) {
            IMLWriter writer = MLWriter.create(ClsCtx.ALL);
            try {
                writer.openDirectory(new BStr(path));
                BStr symbol, desc;
                for (int i = 0; i < infoList.size(); i++) {
                    MetaStockSecurityInfo info = infoList.get(i);
                    symbol = new BStr(info.getSymbol());
                    desc = new BStr(info.getDescription());
                    boolean exists = Boolean.parseBoolean(writer.getBSymbolExists(symbol).toString());
                    if (!exists) {
                        if (isIntraday) {
                            writer.appendIntradaySecurity(symbol, desc, new INTERVAL(INTERVAL.Minute1), new Int(0), new Int(0));
                            //writer.appendSecurity(symbol, desc, new PERIODICITY(PERIODICITY.Intraday));
                        } else {
                            writer.appendSecurity(symbol, desc, new PERIODICITY(PERIODICITY.Daily));
                        }
                    }
                }

            } catch (ComException ex) {
                ex.printStackTrace();
            } finally {
                writer.closeDirectory();
            }
        }

    }
}