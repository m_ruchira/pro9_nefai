package com.isi.csvr.win32;

import com.isi.csvr.shared.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Sep 1, 2007
 * Time: 12:04:04 PM
 */
public class DemoGlass extends JComponent implements ComponentListener {

    private Container contentPane;

    public DemoGlass(/*Container contentPane*/) {
        this.contentPane = contentPane;
        setForeground(Color.red);
        setFont(new TWFont("Arial", Font.BOLD, 200));
        setVisible(true);
        addComponentListener(this);
    }

    public void paint(Graphics g) {
        System.out.println("pasint");
        Graphics2D g2D = (Graphics2D) g;
        g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.1f));
        g2D.setColor(Color.red);
        g2D.drawString("Demo", 300, 300);
    }


    public void componentHidden(ComponentEvent e) {
        System.out.println("hidden");
//        setVisible(true);
    }

    public void componentMoved(ComponentEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void componentResized(ComponentEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void componentShown(ComponentEvent e) {
        System.out.println("Shown");
    }


}
