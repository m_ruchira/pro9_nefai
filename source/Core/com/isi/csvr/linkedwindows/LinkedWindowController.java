package com.isi.csvr.linkedwindows;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Dec 18, 2008
 * Time: 9:42:58 AM
 * To change this template use File | Settings | File Templates.
 */
public interface LinkedWindowController {


    public void fireSymbolChangeInLinkedWindows(String group, String sKey);

    public boolean isLinkWindowControlEnabled();

    public void setLinkWindowControl(boolean isCtrlEnabled);

}
