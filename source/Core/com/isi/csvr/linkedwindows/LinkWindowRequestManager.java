package com.isi.csvr.linkedwindows;

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.marketdepth.DepthStore;
import com.isi.csvr.Client;

/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Oct 21, 2008
 * Time: 10:48:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class LinkWindowRequestManager {

    private static LinkWindowRequestManager self;

    private LinkWindowRequestManager(){

    }

    public static LinkWindowRequestManager getSharedInstance(){
        if(self==null){
            self = new LinkWindowRequestManager();
        }
        return self;
    }

    public void addDepthRequestForSnapQuote(InternalFrame frame, String sKey){
        ViewSetting oSetting  = frame.getViewSetting();
        String oldsKey=  oSetting.getProperty(ViewConstants.VC_SYMBOLWISEVIEW_KEY).trim();
         if (!ExchangeStore.getSharedInstance().isDefault(SharedMethods.getExchangeFromKey(oldsKey)) && oldsKey!= null && !oldsKey.isEmpty()) {
                DataStore.getSharedInstance().removeSymbolRequest(oldsKey);
         }
         String requestID = Meta.PRICE_DEPTH + "|" + sKey;
         Client.getInstance().addDepthRequest(requestID, sKey, Meta.PRICE_DEPTH);
         frame.setDataRequestID(DepthStore.getInstance(), sKey, requestID);

    }
}
