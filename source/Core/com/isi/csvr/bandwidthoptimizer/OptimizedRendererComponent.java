package com.isi.csvr.bandwidthoptimizer;

import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.TWCustomCheckBox;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseListener;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;


/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Sep 8, 2008
 * Time: 6:27:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class OptimizedRendererComponent extends JPanel {
    private JLabel left;
    private TWCustomCheckBox checkLable;
    private JLabel middle;
    private JLabel right;
    private final static int EXG_MODE = 0;
    private final static int SYM_MODE = 1;

    OptimizedRendererComponent() {
        this.setOpaque(false);
        left = new JLabel();
        checkLable = new TWCustomCheckBox("", SwingConstants.LEADING, TWCustomCheckBox.CHECK_ONLY);
        middle = new JLabel();
        right = new JLabel();


        ItemListener itemListener = new ItemListener() {
            public void itemStateChanged(ItemEvent itemEvent) {
                TWCustomCheckBox abstractButton = (TWCustomCheckBox) itemEvent.getSource();
                int state = itemEvent.getStateChange();
                if (state == ItemEvent.SELECTED) {
                    abstractButton.setSelected(true);
                }
            }
        };
        checkLable.addItemListener(itemListener);
        left.setOpaque(false);
        middle.setOpaque(false);
        right.setOpaque(false);
        this.setLayout(new FlowLayout(FlowLayout.LEADING, 5, 0));
        this.add(left);
        this.add(checkLable);
        this.add(middle);
        this.add(right);
        GUISettings.applyOrientation(this);
    }


    public void setMode(int mode) {
        if (mode == EXG_MODE) {
            checkLable.setForeground(Color.black);
            middle.setForeground(Color.black);
            right.setForeground(Color.black);
        } else {
            checkLable.setForeground(Color.blue);
            middle.setForeground(Color.blue);
            right.setForeground(Color.blue);
        }
    }

    public void setLeftString(String s1) {
        middle.setText(s1);
    }

    public void setRightString(String s2) {
        right.setText(s2);
    }

    public void setcheckLableString(boolean s2) {
        checkLable.setSelected(s2);
    }

    public void setEnabledcheckLable(boolean enabled) {
        checkLable.setEnabled(enabled);
    }

    public void setImage(ImageIcon ico) {
        left.setIcon(ico);
    }

    public TWCustomCheckBox getCheckBox() {
        return this.checkLable;
    }

    public void setColor(int mode, Color c1, Color c2) {
        if (mode == EXG_MODE) {
            checkLable.setForeground(c1);
            middle.setForeground(c1);
            right.setForeground(c2);
        } else {
            checkLable.setForeground(c1);
            middle.setForeground(c1);
            right.setForeground(c2);
        }
    }

    public void setSelectedColor(Color c3) {
        this.setOpaque(true);
        this.setBackground(c3);
    }

    public void setUnselected() {
        this.setOpaque(false);
    }

}
