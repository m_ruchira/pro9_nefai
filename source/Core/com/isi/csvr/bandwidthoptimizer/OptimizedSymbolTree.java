package com.isi.csvr.bandwidthoptimizer;

import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.datastore.*;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.Client;

import javax.swing.*;
import javax.swing.tree.TreeModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreePath;
import java.awt.dnd.*;
import java.awt.*;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Sep 8, 2008
 * Time: 5:19:37 PM
 * To change this template use File | Settings | File Templates.
 */
public class OptimizedSymbolTree extends JTree implements NonNavigatable {

    private TreeModel checkingModel;

    public OptimizedSymbolTree(TreeModel newModel) {        //
        super();
        DragSource dragSource = DragSource.getDefaultDragSource();
        dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY_OR_MOVE, new TreeDragGestureListener());
        checkingModel = newModel;
        setModel(newModel);
        this.addMouseListener(new NodeCheckListener(this));
    }


    public void setModel(Vector<?> value) {
        super.setModel(createTreeModel(value));    //To change body of overridden methods use File | Settings | File Templates.

    }

    protected static TreeModel createTreeModel(Object value) {
        DefaultMutableTreeNode root;

        if ((value instanceof Object[]) || (value instanceof Hashtable) ||
                (value instanceof Vector)) {
            root = new DefaultMutableTreeNode("root");
            DynamicUtilTreeNode.createChildren(root, value);
        } else {
            root = new DynamicUtilTreeNode("root", value);
        }
        return new DefaultTreeModel(root, false);
    }

    private static class TreeDragGestureListener implements DragGestureListener {
        public void dragGestureRecognized(DragGestureEvent dragGestureEvent) {
            // Can only drag leafs
            JTree tree = (JTree) dragGestureEvent.getComponent();
            TreePath path = tree.getSelectionPath();
            if (path == null) {
                // Nothing selected, nothing to drag
                System.out.println("Nothing selected - beep");
                tree.getToolkit().beep();
            } else {
//                DefaultMutableTreeNode selection = (DefaultMutableTreeNode) path.getLastPathComponent();
                try {
                    OptimizedSymbolNode selection = (OptimizedSymbolNode) path.getLastPathComponent();
                    if (selection.isLeaf()) {
                        OptimizedTreeNode node = new OptimizedTreeNode(selection);
                        dragGestureEvent.startDrag(DragSource.DefaultCopyDrop, node, new MyDragSourceListener());
                    } else {
                        System.out.println("Not a leaf - beep");
                        tree.getToolkit().beep();
                    }
                } catch (Exception e) {
//                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }
    }

    private static class MyDragSourceListener implements DragSourceListener {
        public void dragDropEnd(DragSourceDropEvent dragSourceDropEvent) {
            if (dragSourceDropEvent.getDropSuccess()) {
                int dropAction = dragSourceDropEvent.getDropAction();
                if (dropAction == DnDConstants.ACTION_MOVE) {
                    System.out.println("MOVE: remove node");
                }
            }
        }

        public void dragEnter(DragSourceDragEvent dragSourceDragEvent) {
            DragSourceContext context = dragSourceDragEvent
                    .getDragSourceContext();
            int dropAction = dragSourceDragEvent.getDropAction();
            if ((dropAction & DnDConstants.ACTION_COPY) != 0) {
                context.setCursor(DragSource.DefaultCopyDrop);
            } else if ((dropAction & DnDConstants.ACTION_MOVE) != 0) {
                context.setCursor(DragSource.DefaultMoveDrop);
            } else {
                context.setCursor(DragSource.DefaultCopyNoDrop);
            }
        }

        public void dragExit(DragSourceEvent dragSourceEvent) {
        }

        public void dragOver(DragSourceDragEvent dragSourceDragEvent) {
        }

        public void dropActionChanged(DragSourceDragEvent dragSourceDragEvent) {
        }
    }

    public void paint(Graphics g) {
        if ((Theme.SIDEBAR_LIGHT_COLOR != null) && (Theme.SIDEBAR_DARK_COLOR != null)) {
            Rectangle rect = getVisibleRect();
            int width = getWidth();
            int height = (int) rect.getHeight();

            for (int i = 0; i < height; i++) {
                g.setColor(new Color(((Theme.SIDEBAR_LIGHT_COLOR.getRed() * i) + (Theme.SIDEBAR_DARK_COLOR.getRed() * (height - i))) / height,
                        ((Theme.SIDEBAR_LIGHT_COLOR.getRed() * i) + (Theme.SIDEBAR_DARK_COLOR.getGreen() * (height - i))) / height,
                        ((Theme.SIDEBAR_LIGHT_COLOR.getRed() * i) + (Theme.SIDEBAR_DARK_COLOR.getBlue() * (height - i))) / height));

                g.drawLine(rect.x, rect.y + i, width, rect.y + i);
            }
        }
        super.paint(g);
    }

    private class NodeCheckListener extends MouseAdapter {

        public boolean isCheckBox = false;

        public OptimizedSymbolTree tree;

        //----- For symbol
        private String selectedSymbol = "";
        private String selectedExchange = "";
        private int selectedSymbolsIntType = -1;

        //----- For sector
        private String sectorDescription;
        private String sectorId;

        //------ For exchange
        private String exchangeDescription;

        public NodeCheckListener(OptimizedSymbolTree optimizedSymbolTree) {
            tree = optimizedSymbolTree;
        }

        @Override
        public void mousePressed(MouseEvent e) {
            // we use mousePressed instead of mouseClicked for performance
            int x = e.getX();
            int y = e.getY();
            int row = getRowForLocation(x, y);
            if (row == -1) {
                // click outside any node
                return;
            }
            Rectangle rect = getRowBounds(row);
            if (rect == null) {
                // clic on an invalid node
                return;
            }
//        OptimizedRendererComponent comp = new OptimizedRendererComponent();
            if (((OptimizedTreeRenderer) getCellRenderer()).isOnHotspot(x - rect.x, y - rect.y)) {
                isCheckBox = true;
                //================
                {
                    TreePath path = tree.getPathForLocation(e.getX(), e.getY());
                    if (path != null) {
                        tree.setSelectionPath(path);

//                        Client.IS_BANDOP_VISIBLE = false;
                        try {
                            OptimizedExchangeNode node = (OptimizedExchangeNode) path.getLastPathComponent();
                            if (node.isExchangeType()) {
                                OptimizedExchangeNode exchange = (OptimizedExchangeNode) node;
                                exchangeDescription = exchange.getExchangeSymbol();
//                            exchange.setSelected(!exchange.isSelected());
                                String submarketNode = exchange.getSubmarket();

                                Enumeration keyss = OptimizedTreeRenderer.selectedExchanges.keys();
                                while (keyss.hasMoreElements()) {
                                    String key = (String) keyss.nextElement();
                                    Boolean stockRet = OptimizedTreeRenderer.selectedExchanges.get(key);
                                    if (key.equals(exchange.getExchangeSymbol())) {
                                        exchange.setSelected(false);
                                    }
                                }

                                if (exchange.isSelected()) {
                                    exchange.setSelected(!exchange.isSelected());

                                    Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();

                                    OptimizedTreeRenderer.selectedExchanges.put(exchange.getExchangeSymbol(), true);
                                    OptimizedTreeRenderer.tempselectedExchanges.put(exchange.getExchangeSymbol(), true);

                                    Exchange exch = ExchangeStore.getSharedInstance().getExchange(exchange.getExchangeSymbol());


                                    for (Market subMarket : exch.getSubMarkets()) {
                                        OptimizedTreeRenderer.selectedSubMarkets.put(subMarket.getMarketID() + exchange.getExchangeSymbol(), true);
                                        OptimizedTreeRenderer.tempselectedSubMarkets.put(subMarket.getMarketID() + exchange.getExchangeSymbol(), true);

                                    }


                                    Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(exchange.getExchangeSymbol());
                                    if (sectorsEnum.hasMoreElements()) {
                                        while (sectorsEnum.hasMoreElements()) {
                                            Sector sector = (Sector) sectorsEnum.nextElement();
                                            if (!OptimizedTreeRenderer.selectedSectors.containsKey(sector.getDescription() + exchangeDescription)) {
                                                OptimizedTreeRenderer.selectedSectors.put(sector.getDescription() + exchangeDescription, true);
                                                OptimizedTreeRenderer.tempselectedSectors.put(sector.getDescription() + exchangeDescription, true);
                                            }
                                        }
                                    }
                                    Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exchange.getExchangeSymbol());
                                    while (exchageSymbols.hasMoreElements()) {
                                        String symbol = (String) exchageSymbols.nextElement();
                                        Stock st = DataStore.getSharedInstance().getStockObject(exchange.getExchangeSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));

                                        if (!OptimizedTreeRenderer.selectedSymbols.containsKey(st.getKey())) {
                                            int instrument_type = SharedMethods.getInstrumentFromExchangeKey(symbol);
                                            if (exchangeRequests.containsKey(instrument_type)) {
                                                ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(instrument_type);
                                                gettingArrayFromHash.add(st);
                                            } else {
                                                ArrayList<Stock> freshArray = new ArrayList<Stock>();
                                                freshArray.add(st);
                                                exchangeRequests.put(instrument_type, freshArray);
                                            }
                                        }
                                        OptimizedTreeRenderer.selectedSymbols.put(st.getKey(), true);

                                        st.setSymbolEnabled(true);
                                        MainUI.selectedSymbols.put(st.getKey(), true);

                                        OptimizedTreeRenderer.tempselectedSymbols.put(st.getKey(), true);
                                    }
                                    Enumeration<Integer> keys = exchangeRequests.keys();
                                    while (keys.hasMoreElements()) {
                                        Integer key = keys.nextElement();
                                        ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(key);
                                        ArrayList exchangeSymbols = new ArrayList();
                                        for (int i = 0; i < gettingArrayFromHash.size(); i++) {
                                            Stock fulKey = gettingArrayFromHash.get(i);

                                            exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
                                        }
                                        String selectedExchangesSymbols = "";
                                        for (int f = 0; f < exchangeSymbols.size(); f++) {
                                            selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
                                        }
                                        String exchangeAddRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);
                                        int messageType = SharedMethods.getSymbolType(key);

                                    }
                                } else if (!exchange.isSelected()) {
                                    exchange.setSelected(!exchange.isSelected());

                                    Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();
                                    OptimizedTreeRenderer.selectedExchanges.remove(exchange.getExchangeSymbol());
                                    if (OptimizedTreeRenderer.tempselectedExchanges.containsKey(exchange.getExchangeSymbol())) {
                                        OptimizedTreeRenderer.tempselectedExchanges.remove(exchange.getExchangeSymbol());
                                    }
//                                    OptimizedTreeRenderer.tempunselectedExchanges.put(exchange.getExchangeSymbol(), true);
                                    Enumeration sectorsEnum = SectorStore.getSharedInstance().getSectors(exchange.getExchangeSymbol());

                                    if (sectorsEnum.hasMoreElements()) {
                                        while (sectorsEnum.hasMoreElements()) {
                                            Sector sector = (Sector) sectorsEnum.nextElement();
                                            if (OptimizedTreeRenderer.selectedSectors.containsKey(sector.getDescription() + exchangeDescription)) {

                                                OptimizedTreeRenderer.selectedSectors.remove(sector.getDescription() + exchangeDescription);
                                                if (OptimizedTreeRenderer.tempselectedSectors.containsKey(sector.getDescription() + exchangeDescription)) {
                                                    OptimizedTreeRenderer.tempselectedSectors.remove(sector.getDescription() + exchangeDescription);
                                                }

                                            }
                                        }
                                    }

                                    Exchange exch = ExchangeStore.getSharedInstance().getExchange(exchange.getExchangeSymbol());


                                    for (Market subMarket : exch.getSubMarkets()) {
                                        OptimizedTreeRenderer.selectedSubMarkets.remove(subMarket.getMarketID() + exchange.getExchangeSymbol());

                                        if (OptimizedTreeRenderer.tempselectedSubMarkets.containsKey(subMarket.getMarketID() + exchange.getExchangeSymbol())) {
                                            OptimizedTreeRenderer.tempselectedSubMarkets.remove(subMarket.getMarketID() + exchange.getExchangeSymbol());
                                        }
//                                        OptimizedTreeRenderer.tempunselectedSubMarkets.put(subMarket.getMarketID() + exchange.getExchangeSymbol(), true);

                                    }

                                    Enumeration exchageSymbols = DataStore.getSharedInstance().getSymbols(exchange.getExchangeSymbol());
                                    while (exchageSymbols.hasMoreElements()) {
                                        String symbol = (String) exchageSymbols.nextElement();
                                        Stock st = DataStore.getSharedInstance().getStockObject(exchange.getExchangeSymbol(), SharedMethods.getSymbolFromExchangeKey(symbol), SharedMethods.getInstrumentFromExchangeKey(symbol));

                                        if (OptimizedTreeRenderer.selectedSymbols.containsKey(st.getKey())) {
                                            int instrument_type = SharedMethods.getInstrumentFromExchangeKey(symbol);
                                            if (exchangeRequests.containsKey(instrument_type)) {
                                                ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(instrument_type);
                                                gettingArrayFromHash.add(st);
                                            } else {
                                                ArrayList<Stock> freshArray = new ArrayList<Stock>();
                                                freshArray.add(st);
                                                exchangeRequests.put(instrument_type, freshArray);
                                            }
                                            OptimizedTreeRenderer.selectedSymbols.remove(st.getKey());
                                            st.setSymbolEnabled(false);

                                            MainUI.selectedSymbols.put(st.getKey(), false);

                                            if (OptimizedTreeRenderer.tempselectedSymbols.containsKey(st.getKey())) {
                                                OptimizedTreeRenderer.tempselectedSymbols.remove(st.getKey());
                                            }
//                                            OptimizedTreeRenderer.tempunselectedSymbols.put(st.getKey(), true);
                                        }
                                    }
                                    Enumeration<Integer> keys = exchangeRequests.keys();
                                    while (keys.hasMoreElements()) {
                                        String selectedExchangesSymbols = "";
                                        ArrayList exchangeSymbols = new ArrayList();

                                        Integer key = keys.nextElement();
                                        ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(key);
                                        for (int i = 0; i < gettingArrayFromHash.size(); i++) {
                                            Stock fulKey = gettingArrayFromHash.get(i);

                                            int messageType = SharedMethods.getSymbolType(fulKey.getInstrumentType());
                                            exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
                                        }

                                        for (int f = 0; f < exchangeSymbols.size(); f++) {
                                            selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
                                        }
                                        String exchangeRemoveRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);
                                    }
                                }

                                MainUI.IS_EXCHANGE_SELECT = true;
                                //-------------------------------- For Sectors ------------------------------------
                            } else if (node.isSectorType()) {
                                OptimizedSectorNode sector = (OptimizedSectorNode) node;
                                sectorDescription = sector.getDescription();
//                            sector.setSelected(!sector.isSelected());
                                Enumeration keys = OptimizedTreeRenderer.selectedSectors.keys();
                                while (keys.hasMoreElements()) {
                                    String key = (String) keys.nextElement();
                                    Boolean stockRet = OptimizedTreeRenderer.selectedSectors.get(key);
                                    if (key.equals(sector.getDescription() + sector.getExchange())) {
                                        sector.setSelected(false);
                                    }
                                }
                                if (sector.isSelected()) {
                                    sector.setSelected(!sector.isSelected());

                                    Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();
                                    OptimizedTreeRenderer.selectedSectors.put(sector.getDescription() + sector.getExchange(), true);
                                    OptimizedTreeRenderer.tempselectedSectors.put(sector.getDescription() + sector.getExchange(), true);

                                    String sectorExchange = sector.getExchangeSymbol();
                                    String secString = sector.getExchangeSymbol();
                                    ExchangeStore.setSelectedExchangeID(sector.getExchange());
                                    Symbols symbols = DataStore.getSharedInstance().getSymbolsObject(ExchangeStore.getSelectedExchangeID());

                                    String[] symbolArray = symbols.getSymbols();

                                    for (int i = 0; i < symbolArray.length; i++) {
                                        String combined = symbolArray[i];
                                        StringTokenizer fields = new StringTokenizer(combined, "~");
                                        String first = fields.nextToken();
                                        String second = fields.nextToken();
                                        String third = fields.nextToken();
                                        Stock stock = DataStore.getSharedInstance().getStockObject(first, second, Integer.parseInt(third));
                                        if (secString.equals(stock.getSectorCode())) {

                                            //------------------------------ Added by Shanika for new Change ----------
                                            if (!OptimizedTreeRenderer.selectedSymbols.containsKey(combined)) {
                                                int instrument_type = SharedMethods.getInstrumentFromExchangeKey(second);
                                                if (exchangeRequests.containsKey(instrument_type)) {
                                                    ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(instrument_type);
                                                    gettingArrayFromHash.add(stock);
                                                } else {
                                                    ArrayList<Stock> freshArray = new ArrayList<Stock>();
                                                    freshArray.add(stock);
                                                    exchangeRequests.put(instrument_type, freshArray);
                                                }
                                                OptimizedTreeRenderer.selectedSymbols.put(combined, true);
                                                Stock st = DataStore.getSharedInstance().getStockObject(combined);
                                                st.setSymbolEnabled(true);
                                                MainUI.selectedSymbols.put(combined, true);

                                                OptimizedTreeRenderer.tempselectedSymbols.put(combined, true);

                                            }
                                        }
                                        stock = null;
                                    }

                                    Enumeration<Integer> exkeys = exchangeRequests.keys();
                                    while (exkeys.hasMoreElements()) {
                                        Integer key = exkeys.nextElement();
                                        ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(key);
                                        ArrayList exchangeSymbols = new ArrayList();
                                        for (int i = 0; i < gettingArrayFromHash.size(); i++) {
                                            Stock fulKey = gettingArrayFromHash.get(i);
                                            int messageType = SharedMethods.getSymbolType(fulKey.getInstrumentType());
                                            exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
                                        }
                                        String selectedExchangesSymbols = "";
                                        for (int f = 0; f < exchangeSymbols.size(); f++) {
                                            selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
                                        }
                                        String exchangeAddRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);

                                    }
                                } else if (!sector.isSelected()) {
                                    sector.setSelected(!sector.isSelected());
                                    Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();

                                    OptimizedTreeRenderer.selectedSectors.remove(sector.getDescription() + sector.getExchange());

                                    if (OptimizedTreeRenderer.tempselectedSectors.containsKey(sector.getDescription() + exchangeDescription)) {
                                        OptimizedTreeRenderer.tempselectedSectors.remove(sector.getDescription() + exchangeDescription);
                                    }
//                                    OptimizedTreeRenderer.tempunselectedSectors.put(sector.getDescription() + exchangeDescription, true);

                                    String sectorExchange = sector.getExchangeSymbol();
                                    String secString = sector.getExchangeSymbol();
                                    ExchangeStore.setSelectedExchangeID(sector.getExchange());
                                    Symbols symbols = DataStore.getSharedInstance().getSymbolsObject(ExchangeStore.getSelectedExchangeID());

                                    String[] symbolArray = symbols.getSymbols();

                                    for (int i = 0; i < symbolArray.length; i++) {
                                        String combined = symbolArray[i];
                                        StringTokenizer fields = new StringTokenizer(combined, "~");
                                        String first = fields.nextToken();
                                        String second = fields.nextToken();
                                        String third = fields.nextToken();
                                        Stock stock = DataStore.getSharedInstance().getStockObject(first, second, Integer.parseInt(third));
                                        if (secString.equals(stock.getSectorCode())) {
                                            if (OptimizedTreeRenderer.selectedSymbols.containsKey(combined)) {

                                                int instrument_type = SharedMethods.getInstrumentFromExchangeKey(second);
                                                if (exchangeRequests.containsKey(instrument_type)) {
                                                    ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(instrument_type);
                                                    gettingArrayFromHash.add(stock);
                                                } else {
                                                    ArrayList<Stock> freshArray = new ArrayList<Stock>();
                                                    freshArray.add(stock);
                                                    exchangeRequests.put(instrument_type, freshArray);
                                                }

                                            }
                                            OptimizedTreeRenderer.selectedSymbols.remove(combined);
                                            Stock st = DataStore.getSharedInstance().getStockObject(combined);
                                            st.setSymbolEnabled(false);
                                            MainUI.selectedSymbols.put(combined, false);

//                                            OptimizedTreeRenderer.tempunselectedSymbols.put(combined, true);
                                            if (OptimizedTreeRenderer.tempselectedSymbols.containsKey(combined)) {
                                                OptimizedTreeRenderer.tempselectedSymbols.remove(combined);
                                            }

                                        }
                                        stock = null;
                                    }


                                    Enumeration<Integer> keysx = exchangeRequests.keys();
                                    while (keysx.hasMoreElements()) {
                                        Integer key = keysx.nextElement();
                                        ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(key);

                                        ArrayList exchangeSymbols = new ArrayList();
//                                    ArrayList exchangeSymbols = new ArrayList();
                                        for (int i = 0; i < gettingArrayFromHash.size(); i++) {
                                            Stock fulKey = gettingArrayFromHash.get(i);

                                            int messageType = SharedMethods.getSymbolType(fulKey.getInstrumentType());
                                            exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
                                        }
                                        String selectedExchangesSymbols = "";


                                        for (int f = 0; f < exchangeSymbols.size(); f++) {
                                            selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
                                        }
                                        String exchangeRemoveRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);
                                    }
                                }
                            }

                            //--------------------------------- End Of Sectors -------------------------------------

                            //------------------------------------Begin Of SubMarkets-------------------

                            else if (node.isSubMarketType()) {
                                OptimizedSubMarketNode sector = (OptimizedSubMarketNode) node;
                                sectorDescription = sector.getDescription();
//                            sector.setSelected(!sector.isSelected());
                                Enumeration keys = OptimizedTreeRenderer.selectedSubMarkets.keys();
                                while (keys.hasMoreElements()) {
                                    String key = (String) keys.nextElement();
                                    Boolean stockRet = OptimizedTreeRenderer.selectedSubMarkets.get(key);
                                    if (key.equals(sector.getSubMarket() + sector.getExchange())) {
                                        sector.setSelected(false);
                                    }
                                }
                                if (sector.isSelected()) {
                                    sector.setSelected(!sector.isSelected());

                                    Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();
                                    OptimizedTreeRenderer.selectedSubMarkets.put(sector.getSubMarket() + sector.getExchange(), true);


                                    OptimizedTreeRenderer.tempselectedSubMarkets.put(sector.getSubMarket() + sector.getExchange(), true);

                                    String sectorExchange = sector.getExchangeSymbol();
                                    String secString = sector.getSubMarket();
                                    ExchangeStore.setSelectedExchangeID(sector.getExchange());

                                     Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(sectorExchange, sector.getSubMarket());
                                    String[] symbolArray = subSymbols.getSymbols();

                                    for (int i = 0; i < symbolArray.length; i++) {
                                        String combined = symbolArray[i];
                                        StringTokenizer fields = new StringTokenizer(combined, "~");
                                        String first = fields.nextToken();
                                        String second = fields.nextToken();
                                        String third = fields.nextToken();
                                        Stock stock = DataStore.getSharedInstance().getStockObject(first, second, Integer.parseInt(third));
                                        if (secString.equals(stock.getMarketID())) {

                                            //------------------------------ Added by Shanika for new Change ----------
                                            if (!OptimizedTreeRenderer.selectedSymbols.containsKey(combined)) {
                                                int instrument_type = SharedMethods.getInstrumentFromExchangeKey(second);
                                                if (exchangeRequests.containsKey(instrument_type)) {
                                                    ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(instrument_type);
                                                    gettingArrayFromHash.add(stock);
                                                } else {
                                                    ArrayList<Stock> freshArray = new ArrayList<Stock>();
                                                    freshArray.add(stock);
                                                    exchangeRequests.put(instrument_type, freshArray);
                                                }
                                                OptimizedTreeRenderer.selectedSymbols.put(combined, true);
                                                Stock st = DataStore.getSharedInstance().getStockObject(combined);
                                                st.setSymbolEnabled(true);
                                                MainUI.selectedSymbols.put(combined, true);
                                                OptimizedTreeRenderer.tempselectedSymbols.put(combined, true);

                                            }
                                        }
                                        stock = null;
                                    }

                                    Enumeration<Integer> exkeys = exchangeRequests.keys();
                                    while (exkeys.hasMoreElements()) {
                                        Integer key = exkeys.nextElement();
                                        ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(key);
                                        ArrayList exchangeSymbols = new ArrayList();
                                        for (int i = 0; i < gettingArrayFromHash.size(); i++) {
                                            Stock fulKey = gettingArrayFromHash.get(i);
                                            int messageType = SharedMethods.getSymbolType(fulKey.getInstrumentType());
                                            exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
                                        }
                                        String selectedExchangesSymbols = "";
                                        for (int f = 0; f < exchangeSymbols.size(); f++) {
                                            selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
                                        }
                                        String exchangeAddRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);

                                    }
                                } else if (!sector.isSelected()) {
                                    sector.setSelected(!sector.isSelected());
                                    Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();

                                    OptimizedTreeRenderer.selectedSubMarkets.remove(sector.getSubMarket() + sector.getExchange());
                                    if (OptimizedTreeRenderer.tempselectedSubMarkets.containsKey(sector.getSubMarket() + sector.getExchange())) {
                                        OptimizedTreeRenderer.tempselectedSubMarkets.remove(sector.getSubMarket() + sector.getExchange());
                                    }
//                                    OptimizedTreeRenderer.tempunselectedSubMarkets.put(sector.getSubMarket() + sector.getExchange(), true);

                                    String sectorExchange = sector.getExchange();
                                    String secString = sector.getSubMarket();
                                    ExchangeStore.setSelectedExchangeID(sector.getExchange());
                                     Symbols subSymbols = DataStore.getSharedInstance().getSymbolsObject(sectorExchange, sector.getSubMarket());
                                    String[] symbolArray = subSymbols.getSymbols();

                                    for (int i = 0; i < symbolArray.length; i++) {
                                        String combined = symbolArray[i];
                                        StringTokenizer fields = new StringTokenizer(combined, "~");
                                        String first = fields.nextToken();
                                        String second = fields.nextToken();
                                        String third = fields.nextToken();
                                        Stock stock = DataStore.getSharedInstance().getStockObject(first, second, Integer.parseInt(third));
                                        if (secString.equals(stock.getMarketID())) {
                                            if (OptimizedTreeRenderer.selectedSymbols.containsKey(combined)) {

                                                int instrument_type = SharedMethods.getInstrumentFromExchangeKey(second);
                                                if (exchangeRequests.containsKey(instrument_type)) {
                                                    ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(instrument_type);
                                                    gettingArrayFromHash.add(stock);
                                                } else {
                                                    ArrayList<Stock> freshArray = new ArrayList<Stock>();
                                                    freshArray.add(stock);
                                                    exchangeRequests.put(instrument_type, freshArray);
                                                }
                                            }
                                            OptimizedTreeRenderer.selectedSymbols.remove(combined);
                                            Stock st = DataStore.getSharedInstance().getStockObject(combined);
                                            st.setSymbolEnabled(false);
                                            MainUI.selectedSymbols.put(combined, false);

//                                            OptimizedTreeRenderer.tempunselectedSymbols.put(combined, true);
                                            if (OptimizedTreeRenderer.tempselectedSymbols.containsKey(combined)) {
                                                OptimizedTreeRenderer.tempselectedSymbols.remove(combined);
                                            }

                                        }
                                        stock = null;
                                    }

                                    Enumeration<Integer> keysx = exchangeRequests.keys();
                                    while (keysx.hasMoreElements()) {
                                        Integer key = keysx.nextElement();
                                        ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(key);

                                        ArrayList exchangeSymbols = new ArrayList();
//                                    ArrayList exchangeSymbols = new ArrayList();
                                        for (int i = 0; i < gettingArrayFromHash.size(); i++) {
                                            Stock fulKey = gettingArrayFromHash.get(i);

                                            int messageType = SharedMethods.getSymbolType(fulKey.getInstrumentType());
                                            exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
                                        }
                                        String selectedExchangesSymbols = "";


                                        for (int f = 0; f < exchangeSymbols.size(); f++) {
                                            selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
                                        }
                                        String exchangeRemoveRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);
                                    }
                                }
                            }
                            //-------------------------------- End Of SubMarkets----------------

                            //---------------------------- For Symbols -------------------------------

                            else {
                                OptimizedSymbolNode sto = (OptimizedSymbolNode) node;

                                selectedExchange = sto.getExchange();
                                selectedSymbol = sto.getSymbol();
                                selectedSymbolsIntType = sto.getInstrumentType();
                                String ins = String.valueOf(sto.getInstrumentType());
                                Stock stock = DataStore.getSharedInstance().getStockObject(sto.getExchange(), sto.getSymbol(), sto.getInstrumentType());
                                Enumeration keys = OptimizedTreeRenderer.selectedSymbols.keys();
                                while (keys.hasMoreElements()) {
                                    String key = (String) keys.nextElement();
                                    Boolean stockRet = OptimizedTreeRenderer.selectedSymbols.get(key);
                                    if (key.equals(sto.getKey())) {
                                        sto.setSelected(false);
                                    }
                                }
                                if (sto.isSelected()) {
                                    sto.setSelected(!sto.isSelected());
                                    OptimizedTreeRenderer.selectedSymbols.put(sto.getKey(), true);
                                    Stock st = DataStore.getSharedInstance().getStockObject(sto.getKey());
                                    st.setSymbolEnabled(true);
                                    MainUI.selectedSymbols.put(sto.getKey(), true);
                                    OptimizedTreeRenderer.tempselectedSymbols.put(sto.getKey(), true);

                                    int messageType = SharedMethods.getSymbolType(selectedSymbolsIntType);

                                } else if (!sto.isSelected()) {
                                    sto.setSelected(!sto.isSelected());
                                    OptimizedTreeRenderer.selectedSymbols.remove(sto.getKey());
                                    Stock st = DataStore.getSharedInstance().getStockObject(sto.getKey());
                                    st.setSymbolEnabled(false);
                                    MainUI.selectedSymbols.put(sto.getKey(), false);

//                                    OptimizedTreeRenderer.tempunselectedSymbols.put(sto.getKey(), true);
                                    if (OptimizedTreeRenderer.tempselectedSymbols.containsKey(sto.getKey())) {
                                        OptimizedTreeRenderer.tempselectedSymbols.remove(sto.getKey());
                                    }
                                    int messageType = SharedMethods.getSymbolType(selectedSymbolsIntType);

                                }
                            }

                            //------------------------------------- End Of Symbols ----------------------------------

                        } catch (Exception ee) {
                        }
                    }
                }
            }
        }

        //---------------------------- adding for activate

        public void mouseClicked(MouseEvent ev) {
            TreePath path = tree.getPathForLocation(ev.getX(), ev.getY());
            if (path == null) {
                return;
            }
            /*if (ev.getClickCount() > 1) {
                OptimizedSymbolNode node = (OptimizedSymbolNode) path.getLastPathComponent();
                if (node != null && !ev.isPopupTrigger()) {    //&& node.isLeaf()
                    if (node instanceof OptimizedSymbolNode) {
                        selectedExchange = node.getExchange();
                        selectedSymbol = node.getSymbol();
                        selectedSymbolsIntType = node.getInstrumentType();
                        Client.getInstance().mnuDetailQuoteSymbol(SharedMethods.getKey(selectedExchange, selectedSymbol, selectedSymbolsIntType), false, false, false);
                    }
                }
            }*/
        }

        public void mouseReleased(MouseEvent ev) {
            try {
                TreePath path = tree.getPathForLocation(ev.getX(), ev.getY());
                if (path == null) {
                    return;
                }
                //IF we're on a path, then we can check the node that its holding
                OptimizedSymbolNode node = (OptimizedSymbolNode) path.getLastPathComponent();
                if (node != null && ev.isPopupTrigger()) {    //&& node.isLeaf()
                    if (node instanceof OptimizedSymbolNode) {
//                            getPopupMenu1().show(tree, ev.getX(), ev.getY());
                    }
                }
            } catch (Exception e) {

            }
        }


    }

    public TreeModel getCheckingModel() {
        return this.checkingModel;
    }
}
