package com.isi.csvr.history;

import java.io.File;
import java.io.FileFilter;
import java.util.Comparator;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Feb 1, 2005
 * Time: 10:51:26 AM
 */
public class HistoryFileFilter implements FileFilter, Comparator {

    // file filter for the history files root
    public boolean accept(File pathname) {
        if ((pathname.isDirectory()) &&
                ((pathname.getName().toUpperCase().startsWith("ARCHIVE")) ||
                        (pathname.getName().toUpperCase().startsWith("HISTORY")))) {
            return true;
        } else {
            return false;
        }
    }

    /*public int compare(Object o1, Object o2) {
        File f1 = (File) o1;
        File f2 = (File) o2;

        return f1.getName().toUpperCase().compareTo(f2.getName().toUpperCase());
    }*/

    //    modified by Shanaka - To avoid total String comparison
    public int compare(Object o1, Object o2) {
        File f1 = (File) o1;
        File f2 = (File) o2;
        String name1 = f1.getName();
        String name2 = f2.getName();
        if(name1.equalsIgnoreCase("archive") || name2.equalsIgnoreCase("archive") || name1.equalsIgnoreCase("history")||name2.equalsIgnoreCase("history")){
            return f1.getName().toUpperCase().compareTo(f2.getName().toUpperCase());
        }else{
            int file1 = Integer.parseInt(name1.substring(7));
            int file2 = Integer.parseInt(name2.substring(7));
            if(file1>file2)return 1;
            else if(file1<file2) return -1;
            else return 0;
        }
    }
}
