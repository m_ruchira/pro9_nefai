package com.isi.csvr.history;

/*
 * HistoryFilesManager.java
 * Version 1.0_beta
 * 10th May 2001
 * Copyright (c) 2001 Integrated Systems International (Pvt)Ltd. All Rights Reserved.
 */

import com.isi.csvr.shared.DynamicArray;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.TWDateFormat;
import com.isi.csvr.shared.Settings;

import java.io.*;
import java.text.ParseException;
import java.util.Arrays;
import java.util.Date;
import java.util.StringTokenizer;
import java.util.Vector;

/**
 * This class is used to read and write to History flat files
 * database. This is synchronize methods. Only one file can be
 * opened for read or write
 */
public class HistoryFilesManager {
    private static HistoryFileFilter historyFileFilter;
    private static Date minDate;

    /**
     * Constructor
     */
    public HistoryFilesManager() {
        //new Settings();
        //historyFlatDatabasePath =Settings.HISTORY_FLAT_DB_PATH;
    }

    public synchronized static DynamicArray readHistoryData(String key) {
        String sline;
        boolean isdone;
        DynamicArray stringVector = new DynamicArray();
        FileInputStream fileIN;
        BufferedReader in;

        String symbol = SharedMethods.getSymbolFromKey(key);
        String exchange = SharedMethods.getExchangeFromKey(key);

        try {
            synchronized (HistorySettings.HISTORY_SYNC) {
                File[] historyPaths = getHistoryPaths(exchange);
                for (int i = 0; i < historyPaths.length; i++) {
                    try {
                        isdone = false;
                        fileIN = new FileInputStream(historyPaths[i].getAbsolutePath() + "\\" + symbol + ".csv");
                        in = new BufferedReader(new InputStreamReader(fileIN));
                        isdone = false;
                        while (!isdone) {
                            sline = in.readLine();
                            if (sline != null) {
                                stringVector.add(sline);
                            } else {
                                isdone = true;
                            }
                        }

                        in.close();
                        fileIN.close();
                        in = null;
                        fileIN = null;
                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            }
            if (stringVector.size() == 0) {
                return null;
            } else {
                return stringVector;
            }
        } catch (Exception e) {
            return null;
        }
    }

    public static File[] getHistoryPaths(String exchange) {
        if (historyFileFilter == null) {
            historyFileFilter = new HistoryFileFilter();
        }
        File root = new File(Settings.getAbsolutepath()+"history/" + exchange + "/");
        File[] folders = root.listFiles(historyFileFilter);
        Arrays.sort(folders, historyFileFilter);
        root = null;

        return folders;
    }

    public synchronized static Vector readHistoryDataForDate(String symbol, String exCode, String sDate) {
           String sline;
           boolean isdone;
           Vector stringVector = new Vector();
           FileInputStream fileIN = null;
           BufferedReader in = null;
           Date fromDate = null;
           TWDateFormat formatter = null;
           String lineBeforeStart = null;
           Date lineDate = null;

           try {
               formatter = new TWDateFormat("yyyyMMdd");
               fromDate = formatter.parse(sDate);
           } catch (ParseException p) {
               fromDate = null;
           }

           if (fromDate != null) {
               try {
                   synchronized (HistorySettings.HISTORY_SYNC) {
                       File[] historyPaths = getHistoryPaths(exCode);
                       for (int i = 0; i < historyPaths.length; i++) {
                           try {
                               isdone = false;
                               fileIN = new FileInputStream(historyPaths[i].getAbsolutePath() + "/" + symbol + ".csv");
                               in = new BufferedReader(new InputStreamReader(fileIN));
                               isdone = false;
                               while (!isdone) {
                                   sline = in.readLine();
                                   if (sline != null) {
                                       lineDate = getDateHistoryRecord(sline);
                                       if (lineDate != null) {
                                            if(minDate != null){
                                                minDate = (minDate.compareTo(lineDate )>0)?lineDate:minDate;
                                            }
                                            else
                                                minDate = lineDate;

                                           if (lineDate.compareTo(fromDate) == 0) {
//                                               if (lineDate.compareTo(toDate) <= 0)
//                                               {                    // add to vector until toDate
//                                               if (lineDate.compareTo(fromDate) == 0) { // exact start found
//                                                   lineBeforeStart = null;
//                                               } else { // exact start not found. Use the line before start
//                                                   if (lineBeforeStart != null) {
//                                                       stringVector.insertElementAt(lineBeforeStart, 0);
//                                                       lineBeforeStart = null;
//                                                   }
//                                               }
                                               stringVector.add(sline);
                                               isdone = true;
//                                               } else {
//                                                   isdone = true;
//                                               }
                                           }else if(lineDate.compareTo(fromDate) > 0){
                                               if(lineBeforeStart!=null)
                                                    stringVector.add(lineBeforeStart);
                                               isdone = true;
                                           } else {
                                               lineBeforeStart = sline;
                                           }
                                       } else {
                                           isdone = true;
                                       }
                                   } else {
                                       isdone = true;
                                   }
                               } // End While

                               in.close();
                               fileIN.close();
                               in = null;
                               fileIN = null;
                           } catch (Exception e) {
//                                e.printStackTrace();
                           }
                       }
                   }
                   if (stringVector.size() == 0) {
                       return null;
                   } else {
                       return stringVector;
                   }
               } catch (Exception e) {
                   return null;
               }
           }
           return null;
       }

    public synchronized static Vector readHistoryData(String symbol, String exCode, String sfromDate, String stoDate) {
        String sline;
        boolean isdone;
        Vector stringVector = new Vector();
        FileInputStream fileIN = null;
        BufferedReader in = null;
        Date fromDate = null;
        Date toDate = null;
        TWDateFormat formatter = null;
        String lineBeforeStart = null;
        Date lineDate = null;

        try {
            formatter = new TWDateFormat("yyyyMMdd");
            fromDate = formatter.parse(sfromDate);
            toDate = formatter.parse(stoDate);
        } catch (ParseException p) {
            fromDate = null;
            toDate = null;
        }

        if (fromDate != null && toDate != null) {
            if (toDate.compareTo(fromDate) >= 0) {
                try {
                    synchronized (HistorySettings.HISTORY_SYNC) {
                        File[] historyPaths = getHistoryPaths(exCode);
                        for (int i = 0; i < historyPaths.length; i++) {
                            try {
                                isdone = false;
                                fileIN = new FileInputStream(historyPaths[i].getAbsolutePath() + "/" + symbol + ".csv");
                                in = new BufferedReader(new InputStreamReader(fileIN));
                                isdone = false;
                                while (!isdone) {
                                    sline = in.readLine();
                                    if (sline != null) {
                                        lineDate = getDateHistoryRecord(sline);
                                        if (lineDate != null) {
                                            if(minDate != null){
                                                minDate = (minDate.compareTo(lineDate )>0)?lineDate:minDate;
                                            }
                                            else
                                                minDate = lineDate;
                                            if (lineDate.compareTo(fromDate) >= 0) {
                                                if (lineDate.compareTo(toDate) <= 0)
                                                {                    // add to vector until toDate
                                                    if (lineDate.compareTo(fromDate) == 0) { // exact start found
                                                        lineBeforeStart = null;
                                                    } else { // exact start not found. Use the line before start
                                                        if (lineBeforeStart != null) {
                                                            stringVector.insertElementAt(lineBeforeStart, 0);
                                                            lineBeforeStart = null;
                                                        }
                                                    }
                                                    stringVector.add(sline);
                                                } else {
                                                    isdone = true;
                                                }
                                            } else {
                                                lineBeforeStart = sline;
                                            }
                                        } else {
                                            isdone = true;
                                        }
                                    } else {
                                        isdone = true;
                                    }
                                } // End While

                                in.close();
                                fileIN.close();
                                in = null;
                                fileIN = null;
                            } catch (Exception e) {
//                                e.printStackTrace();
                            }
                        }
                    }
                    if (stringVector.size() == 0) {
                        return null;
                    } else {
//                        System.out.println("size " + stringVector.size());
                        return stringVector;
                    }
                } catch (Exception e) {
                    return null;
                }
            }
        }
        return null;
    }

    public static Date getMinimumDate(){
        return minDate;
    }

    /*
    * This method is used to read history graph data
    * Return : Vector of read lines ( Strings)
    */
    // bandu - This method has been modified to support multi exchanges
/*
    public synchronized static Vector readHistoryDatax(String symbol, String exCode, String sfromDate, String stoDate) {
        String sline;
        boolean isdone;
        Date lineDate;
        Vector stringVector = new Vector();
        FileInputStream fileIN;
        BufferedReader in;
        Date fromDate;
        Date toDate;
        TWDateFormat formatter;
        String lineBeforeStart = null;
//        String symbol = SharedMethods.getSymbolFromKey(key);
//        String exchange = SharedMethods.getExchangeFromKey(key);

        try {
            formatter = new TWDateFormat("yyyyMMdd");
            fromDate = formatter.parse(sfromDate);
            toDate = formatter.parse(stoDate);
        } catch (ParseException p) {
            fromDate = null;
            toDate = null;
        }

        if (fromDate != null && toDate != null) {
            if (toDate.compareTo(fromDate) >= 0) {
                try {
                    synchronized (HistorySettings.HISTORY_SYNC) {
//                        System.out.println("@@@@ " + Settings.HISTORY_FLAT_DB_PATH + "/" + exCode + "/history/" + symbol + ".csv");
                        fileIN = new FileInputStream(Settings.HISTORY_FLAT_DB_PATH + "/" + exCode + "/history/" + symbol + ".csv");  ////historyFlatDatabasePath
                        in = new BufferedReader(new InputStreamReader(fileIN));
                        isdone = false;
                        while (!isdone) {
                            sline = in.readLine();
                            if (sline != null) {
                                lineDate = getDateHistoryRecord(sline);
                                if (lineDate != null) {
                                    if (lineDate.compareTo(fromDate) >= 0) {
                                        if (lineDate.compareTo(toDate) <= 0) {                    // add to vector until toDate
                                            if (lineDate.compareTo(fromDate) == 0) { // exact start found
                                                lineBeforeStart = null;
                                            } else { // exact start not found. Use the line before start
                                                if (lineBeforeStart != null) {
                                                    stringVector.insertElementAt(lineBeforeStart, 0);
                                                    lineBeforeStart = null;
                                                }
                                            }
                                            stringVector.add(sline);
                                        } else {
                                            isdone = true;
                                        }
                                    } else {
                                        lineBeforeStart = sline;
                                    }
                                } else {
                                    isdone = true;
                                }
                            } else {
                                isdone = true;
                            }
                        } // End While
                        in.close();
                        fileIN.close();
                    } // End Synchronized
                } catch (IOException e) {
                }
            }   // End CompareTo
        }
        return stringVector;
    }
*/

    /**
     * Writes trade data to the current data file using the
     * symbol
     */
    public static void writeHistoryData(FileOutputStream out, String sData) {
        try {
            synchronized (HistorySettings.HISTORY_SYNC) {
                out.write((sData + "\r\n").getBytes());
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public static FileOutputStream openHistoryFile(String exchange, String sSymbol) {
        try {
            synchronized (HistorySettings.HISTORY_SYNC) {
                FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath()+"history/" + exchange + "/history/" + sSymbol, true);
                return out;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    /* This method identified Date format for History Records*/
    public static Date getDateHistoryRecord(String sLine) {
        String sDate = null;
        Date dDate;

        StringTokenizer st = new StringTokenizer(sLine, ",");
        sDate = st.nextToken();

        TWDateFormat formatter = new TWDateFormat("yyyyMMdd");

        try {
            dDate = formatter.parse(sDate);
        } catch (ParseException p) {
            dDate = null;
        }
        return dDate;
    }

    /*
    * Close the output file
    */
    /*public static void closeOutFile()
    {
        try
      {
          g_oOut.flush();
          g_oOut.close();
      }
      catch(Exception e)
      {
          //e.printStackTrace();
      }
    }*/

    public static synchronized boolean isHistoryAvailable() {
        File path = new File("flatdatabase/history");
        String[] files = path.list(new FilenameFilter() {
            public boolean accept(File dir, String name) {
                if (name.toUpperCase().endsWith(".CSV"))
                    return true;
                else
                    return false;
            }
        });
        if ((files == null) || (files.length <= 0)) {
            return false;
        } else {
            return true;
        }
    }

    public static void removeExchange(String exchange) {
        try {
            File root = new File(Settings.getAbsolutepath()+"history/" + exchange + "/");
            File[] files = root.listFiles();
            for (int i = 0; i < files.length; i++) {
                files[i].delete();
            }
            root.delete();
            root = null;
            files = null;
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }
}