package com.isi.csvr.history;

// Copyright (c) 2000 Home

import com.isi.csvr.TWFileFilter;
import com.isi.csvr.Client;
import com.isi.csvr.announcement.DateCombo;
import com.isi.csvr.util.PageSetup;
import com.isi.csvr.util.WordWriter;
import com.isi.csvr.calendar.CalDialog;
import com.isi.csvr.calendar.DateSelectedListener;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.metastock.MetaStockHistoryUpdator;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.symbolselector.SymbolListener;
import com.isi.csvr.table.Customizer;
import com.isi.csvr.table.SmartFileChooser;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.awt.event.*;
import java.math.BigInteger;
import java.io.File;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;

/**
 * A Swing-based dialog class.
 * <p/>
 *
 * @author Uditha Nagahawatta
 *         Modified Shanika
 */
public class ExportDialog extends InternalFrame
        implements ActionListener, MouseListener,
        DateSelectedListener, ItemListener, ExportListener,
        KeyListener, SymbolListener, Themeable, InternalFrameListener {
    private JPanel mainPanel = new JPanel();
    JPanel fromDatePanel;
    JPanel toDatePanel;
    JPanel symbolsPanel;
    JPanel exportPanel;
    JComboBox cmdExportTypes;
    GridLayout mainLayout = new GridLayout();
    CalDialog fromDate = null;
    CalDialog toDate = null;
    JLabel lblFromDate;
    JLabel lblToDate;
    JPanel panel3;
    JPanel panel4;
    JTextField lblSelectedSymbols;
    JLabel lblExportLocation;
    TWButton btnFromDate;
    TWButton btnToDate;
    JLayeredPane oLayer;
    String[] g_asSymbols;
    Symbols g_oSymbols;
    int g_iFromDate = -1;
    int g_iToDate = -1;
    DataExporter g_oDataExporter = null;
    MetaStockHistoryUpdator metaStockHistoryUpdator = null;
    boolean g_bCustomRange = false;
    JProgressBar g_oJProgressBar;
    JPanel g_oProgressPanel;
    TWButton btnExport;
    TWButton btnCancel;
    TWButton btnCustomize;
    ViewSetting settings = null;
    int selectedType = 0;
    boolean isMultipleSelected = false;


    /**
     * Constructs a new instance.
     *
     * @param parent
     * @param modal
     */
    public ExportDialog(JFrame parent, boolean modal) {
        fromDate = new CalDialog(Language.getList("MONTH_NAMES"), Language.getList("DAY_NAMES"));
        toDate = new CalDialog(Language.getList("MONTH_NAMES"), Language.getList("DAY_NAMES"));
        settings = new ViewSetting();
        settings.setColumnHeadings(Language.getList("EXPORTER_CSV_COLS"));
        //change start
        settings.setColumns(BigInteger.valueOf(Integer.MAX_VALUE));
        g_oSymbols = new Symbols();

        try {
            jbInit();
            //pack();
        } catch (Exception e) {
            e.printStackTrace();
        }
        createGUI();
        pack();
        centerUI();
    }

    /**
     * Constructs a new non-modal unparented instance with a blank title.
     */

    /**
     * Initializes the state of this instance.
     */
    private void jbInit() throws Exception {
        mainPanel.setLayout(mainLayout);
        mainLayout.setRows(8);
        mainLayout.setColumns(2);
        mainLayout.setVgap(3);

        mainPanel.setOpaque(true);
        mainPanel.setForeground(Color.red);
    }

    Point origin = new Point(10, 20);

    private void createGUI() {
        Color color1 = UIManager.getColor("controlLtHighlight");
        Color color2 = UIManager.getColor("controlDkShadow");

        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        setTitle(Language.getString("EXPORT_HISTORY"));
        oLayer = new JLayeredPane();
        oLayer.setPreferredSize(new Dimension(420, 270));

        oLayer.setBorder(BorderFactory.createLineBorder(Color.black));
        oLayer.add(mainPanel, new Integer(0));
        mainPanel.setBounds(3, 3, 415, 265);
        oLayer.setOpaque(true);
        oLayer.addMouseListener(this);
        getContentPane().add(oLayer);
        JPanel panel1 = new JPanel();

        panel1.setLayout(new FlowLayout());
        JLabel lblSectSymbols = new JLabel(Language.getString("SELECT_SYMBOLS"));
        lblSectSymbols.setPreferredSize(new Dimension(200, 25));
        panel1.add(lblSectSymbols);
        symbolsPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        symbolsPanel.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        symbolsPanel.setPreferredSize(new Dimension(175, 25));
        symbolsPanel.addMouseListener(this);
        lblSelectedSymbols = new JTextField("");
        lblSelectedSymbols.setEditable(false);
        lblSelectedSymbols.setPreferredSize(new Dimension(150, 20));
        symbolsPanel.add(lblSelectedSymbols);
        TWButton btnSelectSymbol = new TWButton(new DownArrow());
        btnSelectSymbol.setBorder(null);
        btnSelectSymbol.setGradientPainted(false);
        btnSelectSymbol.setPreferredSize(new Dimension(20, 20));
        btnSelectSymbol.addActionListener(this);
        btnSelectSymbol.setActionCommand("SS");
        symbolsPanel.add(btnSelectSymbol);
        panel1.add(symbolsPanel);
        mainPanel.add(panel1);

        ButtonGroup buttonGroup = new ButtonGroup();
        JPanel panel8 = new JPanel();
        panel8.setLayout(new FlowLayout());
        JRadioButton chkFullRange = new JRadioButton(Language.getString("COMPLETE_HISTORY"), true);
        chkFullRange.addActionListener(this);
        chkFullRange.setActionCommand("FULL");
        chkFullRange.setPreferredSize(new Dimension(400, 25));
        panel8.add(chkFullRange);
        buttonGroup.add(chkFullRange);
        mainPanel.add(panel8);

        JPanel panel2 = new JPanel();
        panel2.setLayout(new FlowLayout());
        JRadioButton chkCustomRange = new JRadioButton(Language.getString("CUSTOM_DATE_RANGE"), false);
        chkCustomRange.addActionListener(this);
        chkCustomRange.setActionCommand("CUSTOM");
        chkCustomRange.setPreferredSize(new Dimension(400, 25));
        panel2.add(chkCustomRange);
        buttonGroup.add(chkCustomRange);
        mainPanel.add(panel2);

        panel3 = new JPanel();
        panel3.setLayout(new FlowLayout());
        JLabel lblNull1 = new JLabel("");
        lblNull1.setPreferredSize(new Dimension(40, 20));
        panel3.add(lblNull1);
        lblFromDate = new JLabel("");
        JLabel lblFromDateLabel = new JLabel(Language.getString("FROM_DATE"));
        lblFromDateLabel.setPreferredSize(new Dimension(156, 25));
        panel3.add(lblFromDateLabel);
        fromDatePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        fromDatePanel.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        fromDatePanel.setPreferredSize(new Dimension(175, 25));
        lblFromDate.setPreferredSize(new Dimension(150, 20));
        lblFromDate.setOpaque(true);
        fromDatePanel.add(lblFromDate);
        btnFromDate = new TWButton(new DownArrow());
        btnFromDate.setGradientPainted(false);
        btnFromDate.setBorder(null);
        btnFromDate.addActionListener(this);
        btnFromDate.setActionCommand("FD");
        btnFromDate.setPreferredSize(new Dimension(20, 20));
        fromDatePanel.add(btnFromDate);
        fromDatePanel.addMouseListener(this);
        panel3.add(fromDatePanel);
        mainPanel.add(panel3);

        fromDate.setVisible(false);
        fromDate.setBorder(BorderFactory.createEtchedBorder());
        fromDate.addMouseListener(this);
        fromDate.addDateSelectedListener(this);
        oLayer.add(fromDate, new Integer(1));

        panel4 = new JPanel();
        panel4.setLayout(new FlowLayout());
        JLabel lblNull2 = new JLabel("");
        lblNull2.setPreferredSize(new Dimension(40, 20));
        panel4.add(lblNull2);
        JLabel lblToDateLabel = new JLabel(Language.getString("TO_DATE"));
        lblToDateLabel.setPreferredSize(new Dimension(156, 25));
        panel4.add(lblToDateLabel);
        lblToDate = new JLabel("");
        toDatePanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        toDatePanel.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        toDatePanel.setPreferredSize(new Dimension(175, 25));
        lblToDate.setPreferredSize(new Dimension(150, 20));
        toDatePanel.add(lblToDate);
        btnToDate = new TWButton(new DownArrow());
        btnToDate.setBorder(null);
        btnToDate.setGradientPainted(false);
        btnToDate.addActionListener(this);
        btnToDate.setActionCommand("TD");
        btnToDate.setPreferredSize(new Dimension(20, 20));
        toDatePanel.add(btnToDate);
        toDatePanel.addMouseListener(this);
        panel4.add(toDatePanel);
        mainPanel.add(panel4);

        toDate.setVisible(false);
        toDate.setBorder(BorderFactory.createEtchedBorder());
        toDate.addMouseListener(this);
        toDate.addDateSelectedListener(this);
        oLayer.add(toDate, new Integer(1));

        JPanel panel5 = new JPanel();
        panel5.setLayout(new FlowLayout());
        JLabel lblExportLabel = new JLabel(Language.getString("EXPORT_TYPE"));
        lblExportLabel.setPreferredSize(new Dimension(200, 25));
        panel5.add(lblExportLabel);
        cmdExportTypes = new JComboBox();
        cmdExportTypes.addItem(Language.getString("CSV_FORMAT"));
        cmdExportTypes.addItem(Language.getString("META_STOCK_TXT"));

        cmdExportTypes.addItemListener(this);
        cmdExportTypes.addActionListener(this);
        cmdExportTypes.setActionCommand("TYPE");
        cmdExportTypes.setPreferredSize(new Dimension(175, 25));
        panel5.add(cmdExportTypes);

        JPanel panel6 = new JPanel();
        panel6.setLayout(new FlowLayout());
        JLabel lblExportLocationLabel = new JLabel(Language.getString("EXPORT_TO"));
        lblExportLocationLabel.setPreferredSize(new Dimension(200, 25));
        panel6.add(lblExportLocationLabel);
        exportPanel = new JPanel(new FlowLayout(FlowLayout.CENTER, 0, 0));
        exportPanel.setBorder(BorderFactory.createEtchedBorder(color1, color2));
        exportPanel.setPreferredSize(new Dimension(175, 25));
        exportPanel.addMouseListener(this);
        lblExportLocation = new JLabel("");
        lblExportLocation.setPreferredSize(new Dimension(150, 20));
        exportPanel.add(lblExportLocation);
        TWButton btnExportLocation = new TWButton(new DownArrow());
        btnExportLocation.setBorder(null);
        btnExportLocation.setGradientPainted(false);
        btnExportLocation.addActionListener(this);
        btnExportLocation.setPreferredSize(new Dimension(20, 20));
        btnExportLocation.setActionCommand("DESTINATION");
        exportPanel.add(btnExportLocation);
        panel6.add(exportPanel);
        JPanel panel7 = new JPanel();
        panel7.setLayout(new FlowLayout());

        btnCustomize = new TWButton(Language.getString("CUSTOMIZE"));
        btnCustomize.addActionListener(this);
        btnCustomize.addKeyListener(this);
        btnCustomize.setActionCommand("CUSTOMIZE");
        btnCustomize.setPreferredSize(new Dimension(133, 25));
        panel7.add(btnCustomize);

        btnExport = new TWButton(Language.getString("EXPORT"));
        btnExport.addActionListener(this);
        btnExport.addKeyListener(this);
        btnExport.setActionCommand("EXPORT");
        btnExport.setPreferredSize(new Dimension(133, 25));
        panel7.add(btnExport);

        btnCancel = new TWButton(Language.getString("CLOSE"));
        btnCancel.setActionCommand("CANCEL");
        btnCancel.addActionListener(this);
        btnCancel.addKeyListener(this);
        btnCancel.setPreferredSize(new Dimension(133, 25));
        panel7.add(btnCancel);
        mainPanel.add(new JLabel());

        mainPanel.add(panel7);

        g_oProgressPanel = new JPanel(new FlowLayout());

        UIManager.put("ProgressBar.selectionBackground",
                new javax.swing.plaf.ColorUIResource(Color.BLACK));
        UIManager.put("ProgressBar.selectionForeground",
                new javax.swing.plaf.ColorUIResource(Color.WHITE));

        UIManager.put("ProgressBar.background", new javax.swing.plaf.ColorUIResource(Color.GRAY));
        UIManager.put("ProgressBar.foreground", new javax.swing.plaf.ColorUIResource(Color.BLUE));

        g_oJProgressBar = new JProgressBar();
        g_oJProgressBar.setPreferredSize(new Dimension(400, 20));
        g_oJProgressBar.setStringPainted(true);
        g_oJProgressBar.setBorderPainted(true);

        g_oJProgressBar.setBorder(null);
        g_oJProgressBar.setMaximum(100);
        g_oJProgressBar.setMinimum(0);
        g_oJProgressBar.setValue(0);
        g_oJProgressBar.setOpaque(true);
        g_oProgressPanel.add(g_oJProgressBar);
        g_oProgressPanel.setVisible(false);

        Theme.registerComponent(this);
        this.addInternalFrameListener(this);

        mainPanel.add(g_oProgressPanel);
        selectFullRange();
        if (!Language.isLTR())
            GUISettings.applyOrientation(mainPanel, ComponentOrientation.RIGHT_TO_LEFT);

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setOrientation();
        this.setResizable(false);
        this.setClosable(true);
    }

    private void centerUI() {
        Toolkit oToolkit = Toolkit.getDefaultToolkit();
        Dimension oDm = oToolkit.getScreenSize();
        this.setBounds((int) (oDm.getWidth() / 2 - (this.getWidth() / 2)),
                (int) (oDm.getHeight() / 2 - (this.getHeight() / 2)),
                this.getWidth(), this.getHeight());
    }

    public static void main(String[] args) {
        //(new ExportDialog()).show();
    }

    public void itemStateChanged(ItemEvent e) {
        //System.out.println("item changed " );
        initDestination();
        initCustomize();
    }

    public void actionPerformed(ActionEvent e) {
        //System.out.println("Com :" + e.getActionCommand());
        if (e.getActionCommand().equals("FD"))
            showFromDateCal();
        else if (e.getActionCommand().equals("TD"))
            showToDateCal();
        else if (e.getActionCommand().equals("SS")) {
            hideCals();
            getSymbols();
        } else if (e.getActionCommand().equals("EXPORT")) {
            hideCals();
            getDestination();
//            exportData(sFile);
        } else if (e.getActionCommand().equals("CANCEL")) {
            this.dispose();
        } else if (e.getActionCommand().equals("DESTINATION")) {
            hideCals();
            lblExportLocation.setText(getDestination());
        } else if (e.getActionCommand().equals("FULL")) {
            hideCals();
            selectFullRange();
        } else if (e.getActionCommand().equals("CUSTOM")) {
            hideCals();
            selectCustomRange();
        } else if (e.getActionCommand().equals("TYPE")) {
            hideCals();
        } else if (e.getActionCommand().equals("CUSTOMIZE")) {
            settings = showSelectColumns();
        }
    }

    private void initDestination() {
        lblExportLocation.setText("");
    }

    private void initCustomize() {
        if (cmdExportTypes.getSelectedIndex() == 0)
            btnCustomize.setEnabled(true);
        else
            btnCustomize.setEnabled(false);
    }

    private void selectFullRange() {
        g_iFromDate = 0;
        g_iToDate = 99999999;
        btnToDate.setEnabled(false);
        btnFromDate.setEnabled(false);
        btnToDate.setIcon(null);
        btnFromDate.setIcon(null);
        lblFromDate.setText("");
        lblToDate.setText("");
        g_bCustomRange = false;
    }

    private void selectCustomRange() {
        g_iFromDate = -1;
        g_iToDate = -1;
        btnToDate.setEnabled(true);
        btnFromDate.setEnabled(true);
        btnToDate.setIcon(new DownArrow());
        btnFromDate.setIcon(new DownArrow());
        g_bCustomRange = true;
    }

    private void exportData(String sFile, boolean multipleSelected) {
        if (!validateInput()) return;

        if (!validateFile(sFile, multipleSelected)) return;

        g_asSymbols = g_oSymbols.getSymbols();
        switch (selectedType) {
            case 0:
                g_oProgressPanel.setVisible(true);
                g_oDataExporter = new DataExporter(settings, g_asSymbols, "" + g_iFromDate,
                        "" + g_iToDate, DataExporter.CSV, sFile, isMultipleSelected);
                g_oDataExporter.addExportListener(this);
                g_oDataExporter.start();
                break;
            case 1:
                g_oProgressPanel.setVisible(true);
                g_oDataExporter = new DataExporter(null, g_asSymbols, "" + g_iFromDate,
                        "" + g_iToDate, DataExporter.META_STOCK_TXT, sFile, isMultipleSelected);
                g_oDataExporter.addExportListener(this);
                g_oDataExporter.start();
                break;
            case 2:
                PageSetup setup = new PageSetup(Client.getInstance().getWindow());
                setup.setLocationRelativeTo(Client.getInstance().getWindow());
                setup.requestFocus();
                int val = setup.showDialog();
                if (val == PageSetup.STATUS_OK) {
                    g_oProgressPanel.setVisible(true);
                    g_oDataExporter = new DataExporter(settings, g_asSymbols, "" + g_iFromDate,
                            "" + g_iToDate, DataExporter.DOC, sFile, isMultipleSelected);
                    g_oDataExporter.addExportListener(this);
                    g_oDataExporter.start();
                }
                break;
            case 3:
                g_oProgressPanel.setVisible(true);
                //todo xx
//                metaStock = new MetaStock(MetaStock.HISTORY, g_asSymbols, MetaStock.MANUAL, "" + g_iFromDate, "" + g_iToDate, lblExportLocation.getText(), this);
//                metaStock.start();
                break;

        }
        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Tools, "ExportHistory");

        this.setEnabled(false);
    }

    private ViewSetting showSelectColumns() {
        if (settings == null) {
            settings = new ViewSetting();
            settings.setColumnHeadings(Language.getList("EXPORTER_CSV_COLS"));
            //change start
            settings.setColumns(BigInteger.valueOf(Integer.MAX_VALUE));
//            settings.setColumns(Long.MAX_VALUE); //select all columnd
        }
        Customizer c = new Customizer(settings);
        c.showDialog();
        //System.out.println(settings.getColumns());
        return settings;
    }

    private boolean validateFile(String sFile, boolean multipleSelected) {
        boolean isOverRiding = true;
        if (!isMultipleSelected) {
            File comparingFile = new File(sFile);
            if (comparingFile.exists()) {
                int respond = SharedMethods.showConfirmMessage(Language.getString("REPLACE_FILE_MSG"), JOptionPane.YES_NO_OPTION);
                if (respond == 0) {
                    isOverRiding = true;
                } else {
                    isOverRiding = false;
                }
            }
        }
        return isOverRiding;
    }

    private boolean validateInput() {
        boolean bReturnValue = false;

//        if (lblSelectedSymbols.getText().trim().equals(""))
        if (g_oSymbols.getSymbols().length == 0)
            JOptionPane.showMessageDialog(this, Language.getString("MSG_SELECT_A_SYMBOL"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
        else if (g_iFromDate == -1)
            JOptionPane.showMessageDialog(this, Language.getString("MSG_SELECT_FROM_DATE"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
        else if (g_iToDate == -1)
            JOptionPane.showMessageDialog(this, Language.getString("MSG_SELECT_TO_DATE"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
        else if (g_iFromDate > g_iToDate)
            JOptionPane.showMessageDialog(this, Language.getString("MSG_INVALID_DATE_RANGE"),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
//        else if (lblExportLocation.getText().trim().equals(""))
//            JOptionPane.showMessageDialog(this, Language.getString("MSG_SELECT_DESTINATION"),
//                    Language.getString("ERROR"), JOptionPane.OK_OPTION);
        else if (validateSymbols(g_oSymbols.getSymbols()) != null)
            JOptionPane.showMessageDialog(this, Language.getString("INVALID_SYMBOL")
                    + validateSymbols(g_oSymbols.getSymbols())
                    , Language.getString("ERROR"), JOptionPane.OK_OPTION);
        else
            bReturnValue = true;

        return bReturnValue;
    }

    private void getSymbols() {
        SymbolSearch oCompanies = SymbolSearch.getSharedInstance();// new SymbolSearch(g_oTempTable.getTitle(), false,false);//, Meta.QUOTE);
        oCompanies.setSingleMode(false);
        oCompanies.setIndexSearchMode(false);
        oCompanies.init();
        oCompanies.setSymbols(g_oSymbols);
        oCompanies.setShowDefaultExchangesOnly(true);
        oCompanies.setTitle(Language.getString("EXPORT_HISTORY"));
        oCompanies.showDialog(true);
        if (g_oSymbols.getSymbols().length > 1) {
            isMultipleSelected = true;
        } else {
            isMultipleSelected = false;
        }
        lblSelectedSymbols.setText(g_oSymbols.toString());
    }

    /**
     * Check if all symbols in the array are valid
     */
    private String validateSymbols(String[] symbols) {
        int count = symbols.length;

        for (int index = 0; index < count; index++) {
            if (!DataStore.getSharedInstance().isValidSymbol(symbols[index])) {
//                if (!IndexStore.isValidIndex(symbols[index])) {
//                    return symbols[index];
//                }
                return symbols[index];
            }
        }
        return null; // sucess, all symbols valid
    }

    private void showFromDateCal() {
        toDate.setVisible(false);
        if (!fromDate.isVisible()) {
            Point location = SwingUtilities.convertPoint(panel3, fromDatePanel.getLocation(), mainPanel);
            fromDate.setLocation((int) location.getX() + 3,
                    (int) (location.getY() - 20));
            fromDate.setVisible(true);

        } else {
            fromDate.setVisible(false);
        }
    }

    private void showToDateCal() {
        fromDate.setVisible(false);
        if (!toDate.isVisible()) {
            Point location = SwingUtilities.convertPoint(panel4, toDatePanel.getLocation(), mainPanel);
            toDate.setLocation((int) location.getX() + 3,
                    (int) (location.getY() - 55));
            toDate.setVisible(true);
        } else {
            toDate.setVisible(false);
        }
    }

    private JLabel createColoredLabel(String text,
                                      Color color,
                                      Point origin) {
        JLabel label = new JLabel(text);
        label.setVerticalAlignment(JLabel.TOP);
        label.setHorizontalAlignment(JLabel.CENTER);
        label.setOpaque(true);
        label.setBackground(color);
        label.setForeground(Color.black);
        label.setBorder(BorderFactory.createLineBorder(Color.black));
        label.setBounds(origin.x, origin.y, 140, 140);
        return label;
    }

    private void hideCals() {
        fromDate.setVisible(false);
        toDate.setVisible(false);
    }

    public void mouseClicked(MouseEvent e) {
        //if(e.getSource() == oLayer)
        {
            hideCals();
        }
        if (e.getSource() == symbolsPanel) {
            getSymbols();
        } else if (e.getSource() == exportPanel) {
            lblExportLocation.setText(getDestination());
        } else if (e.getSource() == fromDatePanel) {
            if (g_bCustomRange)
                showFromDateCal();
        } else if (e.getSource() == toDatePanel) {
            if (g_bCustomRange)
                showToDateCal();
        }
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
    }

    public void mouseExited(MouseEvent e) {
    }

    public void dateSelected(Object source, int iYear, int iMonth, int iDay) {
        if (source == fromDate) {
            lblFromDate.setText(" " + iDay + " / " + (iMonth + 1) + " / " + iYear);
            g_iFromDate = (iYear * 10000) + ((iMonth + 1) * 100) + iDay;
        } else {
            lblToDate.setText(" " + iDay + " / " + (iMonth + 1) + " / " + iYear);
            g_iToDate = (iYear * 10000) + ((iMonth + 1) * 100) + iDay;
        }
    }

    public String getDestination() {
        int status = 0;
        String[] extentions = new String[1];

        SmartFileChooser oFC;

        // Reset the UIManager for the folder name
//        UIManager.put("FileChooser.fileNameLabelText", Language.getString("FILECHOOSER_FILENAME_LABEL_TEXT"));


        UIManager.put("FileChooser.fileNameLabelText", Language.getString("FILENAME"));
        oFC = new SmartFileChooser(Settings.getAbsolutepath());
        GUISettings.localizeFileChooserHomeButton(oFC);
        GUISettings.applyOrientation(oFC);
        if (!isMultipleSelected) {
            oFC.setFileSelectionMode(JFileChooser.FILES_ONLY);
        } else {
            oFC.setFileSelectionMode(JFileChooser.DIRECTORIES_ONLY);
        }
        oFC.setDialogTitle(Language.getString("EXPORT_HISTORY"));
        oFC.setAcceptAllFileFilterUsed(false);

        String[] extentions1 = new String[1];
        String[] extentions2 = new String[1];
        String[] extentions3 = new String[1];
        extentions1[0] = "txt";
        extentions2[0] = "csv";
        extentions3[0] = "doc";

        TWFileFilter oFilterTXT = new
                TWFileFilter(Language.getString("META_STOCK_TXT"), extentions1);
        TWFileFilter oFilterCSV = new
                TWFileFilter(Language.getString("CSV_FORMAT"), extentions2);
        TWFileFilter oFilterDOC = new
                TWFileFilter(Language.getString("DOC_FORMAT"), extentions3);

        if (isMultipleSelected) {
            oFC.hideContainerOf((String) UIManager.get("FileChooser.fileNameLabelText"));
        }

        oFC.setFileFilter(oFilterCSV);
        oFC.setFileFilter(oFilterTXT);

        if (Constants.isMSWordAvailable == 0) {
            WordWriter.isMSWordAvailable();
        }
        if (Constants.isMSWordAvailable == 1) {
            oFC.setFileFilter(oFilterDOC);
        }
        if (!isMultipleSelected && (g_oSymbols.getSymbols().length>0)) {
            File defaultFile = new File(Settings.getAbsolutepath() + "\\" + SharedMethods.getSymbolFromKey(g_oSymbols.getSymbols()[0]));
            oFC.setSelectedFile(defaultFile);
        }
        status = oFC.showSaveDialog(this);


        if (status == JFileChooser.APPROVE_OPTION) {
            TWFileFilter fileFilter = (TWFileFilter) oFC.getFileFilter();
            String sFile = oFC.getSelectedFile().getAbsolutePath();
            if (fileFilter == oFilterTXT) {
                if (!sFile.trim().endsWith(".txt"))
                    if (!isMultipleSelected) {
                        sFile += ".txt";
                    } else {
                        sFile += "\\" + SharedMethods.getSymbolFromKey(g_oSymbols.getSymbols()[0]) + ".txt";
                        SharedMethods.showMessage(Language.getString("METASTOCK_FILE_DESTINATION") + sFile, INFORMATION_MESSAGE);
                    }
                selectedType = 1;
            } else if (fileFilter == oFilterCSV) {
                if (!sFile.trim().endsWith(".csv"))
                    if (!isMultipleSelected) {
                        sFile += ".csv";
                    }
                selectedType = 0;
            } else if (fileFilter == oFilterDOC) {
                if (!sFile.trim().endsWith(".doc"))
                    if (!isMultipleSelected) {
                        sFile += ".doc";
                    }
                selectedType = 2;
            }
            hideCals();
            if (!sFile.trim().equals("")) {
                exportData(sFile, isMultipleSelected);
            } else {
                SharedMethods.showMessage(Language.getString("MSG_SELECT_FILE_DESTINATION"), INFORMATION_MESSAGE);
            }
            return sFile;

        } else {
            return "";
        }
    }

    public void exportCompleted(Exception e) {
        if (e != null)
            JOptionPane.showMessageDialog(this, e.toString(),
                    Language.getString("ERROR"), JOptionPane.OK_OPTION + JOptionPane.ERROR_MESSAGE);
        else
            JOptionPane.showMessageDialog(this, Language.getString("MSG_EXPORT_COMPLETED"),
                    Language.getString("INFORMATION"), JOptionPane.OK_OPTION + JOptionPane.INFORMATION_MESSAGE);

        this.setEnabled(true);
        g_oJProgressBar.setValue(0);
        g_oProgressPanel.setVisible(false);
        g_oDataExporter = null;
    }

    public void exportAborted(Exception e) {
        this.setEnabled(true);
        g_oJProgressBar.setValue(0);
        g_oProgressPanel.setVisible(false);
        g_oDataExporter = null;
    }

    public void setEnabled(boolean b) {
        super.setEnabled(b);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void exportProgressChanged(int iProgress, int iMax) {
        g_oJProgressBar.setValue(iProgress * 100 / iMax);
        g_oJProgressBar.repaint();
        System.out.println(iProgress * 100 / iMax);
    }

    public void keyTyped(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    public void keyPressed(KeyEvent e) {
        if (e.getKeyCode() == KeyEvent.VK_ENTER) {
            if (e.getSource() == btnCancel) {
                this.dispose();
            } else {
                hideCals();
//                exportData(sFile);
            }
        }
    }

    public void actionCanceled() {

    }

    public void symbolSelected(String key) {
        lblSelectedSymbols.setText(g_oSymbols.toString());
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(this);
        fromDate.applyTheme();
        fromDate.setBorder(GUISettings.getTWFrameBorder());
        toDate.applyTheme();
        toDate.setBorder(GUISettings.getTWFrameBorder());
    }

    /*public void windowActivated(WindowEvent e) {
    }

    public void windowClosed(WindowEvent e) {
        Theme.unRegisterComponent(this);
    }

    public void windowClosing(WindowEvent e) {
    }

    public void windowDeactivated(WindowEvent e) {
    }

    public void windowDeiconified(WindowEvent e) {
    }

    public void windowIconified(WindowEvent e) {
    }

    public void windowOpened(WindowEvent e) {
    }*/

    /* Printable method */

    @Override
    public void internalFrameClosed(InternalFrameEvent e) {
        super.internalFrameClosed(e);
        Theme.unRegisterComponent(this);
    }
}

