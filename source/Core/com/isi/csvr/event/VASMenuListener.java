package com.isi.csvr.event;

import javax.swing.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 16, 2006
 * Time: 3:54:31 PM
 */
public interface VASMenuListener {

    public void VASDataDownloadStarted();

    public void VASDataDownloadSucess(ArrayList<JComponent> menus, ArrayList<JComponent> links);

    public void VASDataDownloadSucess(String id, ArrayList<String> links);

    public void VASDataDownloadFailed();
}


