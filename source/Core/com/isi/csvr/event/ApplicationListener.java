package com.isi.csvr.event;

import com.isi.csvr.datastore.Exchange;

import java.util.TimeZone;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 22, 2005
 * Time: 5:38:06 PM
 */
public interface ApplicationListener {

    /**
     * Application loading in progress
     *
     * @param percentage
     */
    public void applicationLoading(int percentage);

    /**
     * Fires after application loaded and main window is shown
     */
    public void applicationLoaded();

    /**
     * Fires after Application authenticated and initialized
     */
    public void applicationReadyForTransactions();

    /**
     * Application exit in progress
     */
    public void applicationExiting();

    /**
     * Fires after application time zone change
     *
     * @param zone
     */
    public void applicationTimeZoneChanged(TimeZone zone);

    /**
     * Workspace about to load
     */
    public void workspaceWillLoad();

    /**
     * Workspace load completed
     */
    public void workspaceLoaded();

    /**
     *  Workspace about to Save
    */
    public void workspaceWillSave();

    /**
     * Workspace save completed
    */
    public void workspaceSaved();

    /**
     * Processing of snapshot started
     *
     * @param exchange
     */
    public void snapshotProcessingStarted(Exchange exchange);

    /**
     * Processing of snapshot ended
     *
     * @param exchange
     */
    public void snapshotProcessingEnded(Exchange exchange);

    /**
     * Load the Offline data
     *
     */
    public void loadOfflineData();

    /**
     * Selected Exchange changed
     *
     * @param exchange
     */
    public void selectedExchangeChanged(Exchange exchange);


}
