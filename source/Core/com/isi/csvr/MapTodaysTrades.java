package com.isi.csvr;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.event.ExchangeListener;

import java.util.Enumeration;
import java.util.HashMap;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 2, 2005
 * Time: 10:10:49 AM
 */
public class MapTodaysTrades implements ExchangeListener {
    HashMap<String, Symbols> store;

    public MapTodaysTrades() {
        this.store = new HashMap<String, Symbols>();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
    }

    public void initMap(String exchangeCode) {
        try {
            if (exchangeCode != null) {
                Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeCode);
                if (exchange != null){
                    if ((exchange.isUserSubMarketBreakdown()) && (exchange.hasSubMarkets())){
                        for (int i = 0; i< exchange.getSubMarketCount(); i++){
                            initMap(exchangeCode, exchange.getSubMarkets()[i].getSymbol());
                        }
                    } else {
                        initMap(exchangeCode, null);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void initMap(String exchangeCode, String marketCode) {
        try {
            if (exchangeCode != null) {
                Enumeration stocks = DataStore.getSharedInstance().getExchangeSymbolStore(exchangeCode).keys();
                Symbols symbols;
                if(marketCode != null){
                    symbols = getSymbols(SharedMethods.getExchWithSubMktKey(exchangeCode ,marketCode));
                }else {
                    symbols = getSymbols(exchangeCode);
                }
                symbols.clear();

                while (stocks.hasMoreElements()) {
                    String data = (String) stocks.nextElement();
                    String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                    int instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                    Stock hashStock = DataStore.getSharedInstance().getExchangeSymbolStore(exchangeCode).get(SharedMethods.getKey(exchangeCode, symbol, instrument));
                    if ((hashStock != null) && (hashStock.getLastTradeValue() > 0D)) {
                        if ((marketCode == null) || ((hashStock.getMarketID() != null) && (hashStock.getMarketID().equals(marketCode)))){
                            symbols.appendSymbol(SharedMethods.getKey(exchangeCode, symbol, instrument));
                        }
                    }
                    hashStock = null;
                    symbol = null;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public Symbols getSymbols(String exchange) {
        Symbols symbols = store.get(exchange);
        if (symbols == null) {
            synchronized (this) {
                symbols = store.get(exchange);
                if (symbols == null) {
                    symbols = new Symbols();
                    store.put(exchange, symbols);
                }
            }
        }
        return symbols;
    }

   /* public void insertSymbol(String exchange, String symbol, int instrumentType) {
        try {
            String[] symboldata = symbol.split(Constants.MARKET_SEPERATOR_CHARACTER);
            Symbols symbols ;
            if (symboldata.length > 1){
                symbols = getSymbols(SharedMethods.getExchWithSubMktKey(exchange ,symboldata[1]));
            }else {
                symbols = getSymbols(exchange);
            }
            symbols.appendSymbol(SharedMethods.getKey(exchange, symbol, instrumentType));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }*/

    public void insertSymbol(String exchange, String symbol, String subMarket, int instrumentType) {
            try {
                Symbols symbols;
                Exchange exchangeObj = ExchangeStore.getSharedInstance().getExchange(exchange);
                if ((subMarket != null) && (exchangeObj.isUserSubMarketBreakdown())) {
                    symbols = getSymbols(SharedMethods.getExchWithSubMktKey(exchange, subMarket));
                } else {
                    symbols = getSymbols(exchange);
    }
                symbols.appendSymbol(SharedMethods.getKey(exchange, symbol, instrumentType));
            } catch (Exception e) {
                e.printStackTrace();
            }
        }


    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange,long newDate, long oldDate) {
        initMap(exchange.getSymbol());
    }

    public void exchangeRemoved(Exchange exchange) {

    }

    public void exchangesAdded(boolean offlineMode) {

    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }
}
