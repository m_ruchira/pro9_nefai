package com.isi.csvr;

import com.isi.csvr.shared.Language;

import javax.swing.*;
import javax.swing.plaf.basic.BasicHTML;
import javax.swing.text.View;
import javax.swing.text.Document;
import javax.swing.text.BadLocationException;
import java.awt.*;
import java.awt.event.MouseMotionListener;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 10, 2007
 * Time: 10:01:30 AM
 */
public class SystemTrayManager {

    private static TrayIcon trayIcon;

    public static void createSystemTrayIcon(){
        /*try {
            trayIcon = new TrayIcon((new ImageIcon("images/common/ClientServer.gif")).getImage());
            trayIcon.setToolTip(Language.getString("WINDOW_TITLE_CLIENT"));
            SystemTray.getSystemTray().add(trayIcon);
            trayIcon.addMouseListener(new MouseListener(){

                public void mouseClicked(MouseEvent e) {
                    Client.getInstance().getFrame().setExtendedState(JFrame.NORMAL);
                }

                public void mouseEntered(MouseEvent e) {
                }

                public void mouseExited(MouseEvent e) {
                }

                public void mousePressed(MouseEvent e) {
                }

                public void mouseReleased(MouseEvent e) {
                }
            });
        } catch (AWTException e) {
            e.printStackTrace();
        }*/
    }

    public static boolean isSupported(){
        return (SystemTray.isSupported());
    }

    public static void showMessage(String message){
        /*try {
            trayIcon.displayMessage(Language.getString("WINDOW_TITLE_CLIENT"), getTextFromHTML(message), TrayIcon.MessageType.INFO);
        } catch (Exception e) {
            
        }*/
    }

    private static String getTextFromHTML(String html){
        try {
            html = html.replaceAll("<BR>|<br>", "\n");
            JLabel lbl = new JLabel(html);
            View v = (View) lbl.getClientProperty(BasicHTML.propertyKey);
            if (v != null) {
                Document d = v.getDocument();
                return d.getText(0, d.getLength()).trim();
            } else {
                return html;
            }
        } catch (BadLocationException e) {
            return html;
        }
    }

    public static void unloadFromTray(){
        //SystemTray.getSystemTray().remove(trayIcon);
    }
}
