package com.isi.csvr.properties;

// Copyright (c) 2000 Integrated Systems International (ISI)

import com.isi.csvr.util.Decompress;
import com.isi.csvr.util.Compress;

import java.io.*;
import java.util.*;

public class SmartProperties extends Properties {
    char g_cSeperator;
    boolean sequential;
    ArrayList<String> seqKeyList;
    private static final int KEYS_ENU = 0;
    private static final int VALUES_ENU = 1;

    /**
     * Constructor
     */
    public SmartProperties(String sSeperator) {
        g_cSeperator = sSeperator.toCharArray()[0];
    }

    public SmartProperties(String sSeperator, boolean sequential) {
        this.sequential = sequential;
        g_cSeperator = sSeperator.toCharArray()[0];
        if (sequential) {
            seqKeyList = new ArrayList<String>();
        }
    }

    public synchronized void loadCompressed(String fileName) throws IOException {
        if (sequential) {
            seqKeyList.clear();
        }
        Decompress decompress = new Decompress();
        ByteArrayOutputStream out = decompress.setFiles(fileName);
        decompress.decompress();
        load(new ByteArrayInputStream(out.toByteArray()));
        decompress = null;
    }

    public synchronized void load(InputStream inStream) throws IOException {
        /**
         * Use char array to collect the key and val chars.  Use an initial
         * size of 80 chars and double the array during expansion.
         */
        int buflen = 80;
        char[] buf = new char[buflen];
        int bufindx = 0;

        BufferedReader in = new BufferedReader(new InputStreamReader(inStream, "8859_1"));

        int ch = in.read();
        while (true) {
            switch (ch) {
                case-1:
                    return;

                case'#':
                case'!':
                    do {
                        ch = in.read();
                    } while ((ch >= 0) && (ch != '\n') && (ch != '\r'));
                    continue;

                case'\n':
                case'\r':
                case' ':
                case'\t':
                    ch = in.read();
                    continue;
            }

            /* Read the key into buf */
            bufindx = 0;
            while ((ch >= 0) && (ch != g_cSeperator) &&
                    //(ch != ' ') && (ch != ':') &&
                    (ch != '\t') && (ch != '\n') && (ch != '\r')) {
                /* append ch to buf */
                if (bufindx >= buflen) {
                    /* expand buf */
                    buflen *= 2;
                    char[] nbuf = new char[buflen];
                    System.arraycopy(buf, 0, nbuf, 0, buf.length);
                    buf = nbuf;
                }
                buf[bufindx++] = (char) ch;
                ch = in.read();
            }

            while ((ch == ' ') || (ch == '\t')) {
                ch = in.read();
            }
            if ((ch == g_cSeperator) || (ch == ':')) {
                ch = in.read();
            }
            while ((ch == ' ') || (ch == '\t')) {
                ch = in.read();
            }

            /* create the key */
            String key = new String(buf, 0, bufindx);

            /* Read the value into buf, reuse buf */
            bufindx = 0;
            while ((ch >= 0) && (ch != '\n') && (ch != '\r')) {
                int next = 0;
                if (ch == '\\') {
                    switch (ch = in.read()) {
                        case'\r':
                            if (((ch = in.read()) == '\n') ||
                                    (ch == ' ') || (ch == '\t')) {
                                // fall thru to '\n' case
                            } else
                                continue;
                        case'\n':
                            while (((ch = in.read()) == ' ') || (ch == '\t')) ;
                            continue;
                        case't':
                            ch = '\t';
                            next = in.read();
                            break;
                        case'n':
                            ch = '\n';
                            next = in.read();
                            break;
                        case'r':
                            ch = '\r';
                            next = in.read();
                            break;
                        case'u': {
                            while ((ch = in.read()) == 'u') ;
                            int d = 0;
                            loop:
                            for (int i = 0; i < 4; i++) {
                                next = in.read();
                                switch (ch) {
                                    case'0':
                                    case'1':
                                    case'2':
                                    case'3':
                                    case'4':
                                    case'5':
                                    case'6':
                                    case'7':
                                    case'8':
                                    case'9':
                                        d = (d << 4) + ch - '0';
                                        break;
                                    case'a':
                                    case'b':
                                    case'c':
                                    case'd':
                                    case'e':
                                    case'f':
                                        d = (d << 4) + 10 + ch - 'a';
                                        break;
                                    case'A':
                                    case'B':
                                    case'C':
                                    case'D':
                                    case'E':
                                    case'F':
                                        d = (d << 4) + 10 + ch - 'A';
                                        break;
                                    default:
                                        break loop;
                                }
                                ch = next;
                            }
                            ch = d;
                            break;
                        }
                        default:
                            next = in.read();
                            break;
                    }
                } else {
                    next = in.read();
                }
                /* append ch to buf */
                if (bufindx >= buflen) {
                    /* expand buf */
                    buflen *= 2;
                    char[] nbuf = new char[buflen];
                    System.arraycopy(buf, 0, nbuf, 0, buf.length);
                    buf = nbuf;
                }
                buf[bufindx++] = (char) ch;
                ch = next;
            }
            /* create the val */
            String val = new String(buf, 0, bufindx);

            put(key, val);
            if (sequential) {
                seqKeyList.add(key);

            }
        }
    }

    public synchronized void store(OutputStream out, String comments) throws IOException {
        BufferedWriter awriter;
        awriter = new BufferedWriter(new OutputStreamWriter(out, "8859_1"));
        if (comments != null)
            writeln(awriter, "#" + comments);
        writeln(awriter, "#" + new Date().toString());
        for (Enumeration e = keys(); e.hasMoreElements();) {
            String key = (String) e.nextElement();
            String val = (String) get(key);
            key = saveConvert(key, true);

            /* No need to escape embedded and trailing spaces for value, hence
            * pass false to flag.
            */
            val = saveConvert(val, false);
            writeln(awriter, key + g_cSeperator + val);
        }
        awriter.flush();
    }

    public synchronized void storeCompressed(OutputStream out, String comments) throws IOException {
        StringBuffer buffer = new StringBuffer();
        Compress compress = new Compress();
        if (comments != null){
            buffer.append("#" + comments);
            buffer.append("\n");
//            writeln(awriter, "#" + comments);
        }
//        writeln(awriter, "#" + new Date().toString());
        buffer.append("#" + new Date().toString());
        buffer.append("\n");
        for (Enumeration e = keys(); e.hasMoreElements();) {
            String key = (String) e.nextElement();
            String val = (String) get(key);
            key = saveConvert(key, true);

            /* No need to escape embedded and trailing spaces for value, hence
            * pass false to flag.
            */
            val = saveConvert(val, false);
//            writeln(awriter, key + g_cSeperator + val);
            buffer.append(key + g_cSeperator + val);
            buffer.append("\n");
        }
        //awriter.flush();
        compress.setFiles(buffer.toString(), out);
        compress.compress();
        compress = null;
    }

    /*
     * Converts unicodes to encoded &#92;uxxxx and escapes
     * special characters with a preceding slash
     */
    private String saveConvert(String theString, boolean escapeSpace) {
        int len = theString.length();
        int bufLen = len * 2;
        if (bufLen < 0) {
            bufLen = Integer.MAX_VALUE;
        }
        StringBuffer outBuffer = new StringBuffer(bufLen);

        for (int x = 0; x < len; x++) {
            char aChar = theString.charAt(x);
            // Handle common case first, selecting largest block that
            // avoids the specials below
            if ((aChar > 61) && (aChar < 127)) {
                if (aChar == '\\') {
                    outBuffer.append('\\');
                    outBuffer.append('\\');
                    continue;
                }
                outBuffer.append(aChar);
                continue;
            }
            switch (aChar) {
                case' ':
                    if (x == 0 || escapeSpace)
                        outBuffer.append('\\');
                    outBuffer.append(' ');
                    break;
                case'\t':
                    outBuffer.append('\\');
                    outBuffer.append('t');
                    break;
                case'\n':
                    outBuffer.append('\\');
                    outBuffer.append('n');
                    break;
                case'\r':
                    outBuffer.append('\\');
                    outBuffer.append('r');
                    break;
                case'\f':
                    outBuffer.append('\\');
                    outBuffer.append('f');
                    break;
                case'=': // Fall through
                case':': // Fall through
                case'#': // Fall through
                case'!':
                    outBuffer.append('\\');
                    outBuffer.append(aChar);
                    break;
                default:
                    if ((aChar < 0x0020) || (aChar > 0x007e)) {
                        outBuffer.append('\\');
                        outBuffer.append('u');
                        outBuffer.append(toHex((aChar >> 12) & 0xF));
                        outBuffer.append(toHex((aChar >> 8) & 0xF));
                        outBuffer.append(toHex((aChar >> 4) & 0xF));
                        outBuffer.append(toHex(aChar & 0xF));
                    } else {
                        outBuffer.append(aChar);
                    }
            }
        }
        return outBuffer.toString();
    }

    private static char toHex(int nibble) {
        return hexDigit[(nibble & 0xF)];
    }

    /**
     * A table of hex digits
     */
    private static final char[] hexDigit = {
            '0', '1', '2', '3', '4', '5', '6', '7', '8', '9', 'A', 'B', 'C', 'D', 'E', 'F'
    };

    private static void writeln(BufferedWriter bw, String s) throws IOException {
        bw.write(s);
        bw.newLine();
    }

    public Enumeration<Object> keys() {
        if (!sequential) {
            return super.keys();    //To change body of overridden methods use File | Settings | File Templates.
        } else {
            return new SeqEnumerator(KEYS_ENU);
        }
    }

    public Enumeration<Object> elements() {
        if (!sequential) {
            return super.elements();    //To change body of overridden methods use File | Settings | File Templates.
        } else {
            return new SeqEnumerator(VALUES_ENU);
        }
    }

    private class SeqEnumerator implements Enumeration<Object> {
        Iterator<String> seqIterator;
        int type;

        private SeqEnumerator(int type) {
            seqIterator = seqKeyList.iterator();
            this.type = type;
        }

        public boolean hasMoreElements() {
            return seqIterator.hasNext();
        }

        public Object nextElement() {
            if (type == KEYS_ENU) {
                return seqIterator.next();
            } else {
                return get(seqIterator.next());
            }
        }
    }


}