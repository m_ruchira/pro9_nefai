// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr.properties;

/**
 * Manages loading and saving view settings for table views.
 * <P>
 * @author Uditha Nagahawatta
 */

import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.event.ExchangeListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.shared.*;
import com.isi.csvr.linkedwindows.LinkStore;

import java.awt.*;
import java.io.BufferedReader;
import java.io.DataInputStream;
import java.io.FileReader;
import java.math.BigInteger;
import java.util.*;

public class ViewSettingsManager implements ExchangeListener {
    public static final String DEFAULF_WINDOW_SETTINGS = "0,0,500,400,1,0,0";

    //Main Types
    public static final byte CUSTOM_VIEW    = 0;
    public static final byte MARKET_VIEW    = 1;
    public static final byte SECTOR_VIEW    = 2;
    public static final byte SUMMARY_VIEW   = 3;
    public static final byte SYMBOL_VIEW    = 4;
    public static final byte PORTFOLIO_VIEW = 5;
    public static final byte MIST_VIEW      = 6;
    public static final byte FILTERED_VIEW  = 7;
    public static final byte CHART_VIEW     = 19;
    public static final byte BASKET_VIEW    = 99;
    public static final byte FOREX_VIEW     = 8;

    //View Types
    public static final byte TIMEnSALES_VIEW2 = 1;
    public static final byte FULL_TIMEnSALES_VIEW = 2;
    public static final byte FILTERED_TIMEnSALES_VIEW = 21;
    public static final byte DEPTH_BID_VIEW = 3;
    public static final byte DEPTH_ASK_VIEW = 4;
    public static final byte BROWSER_VIEW = 6;
    public static final byte FULL_DEPTH_BID_VIEW = 8;
    public static final int TICKER_OR_INDEX = 9;
    public static final byte DEPTH_ASK_SUMMARY_VIEW = 10;
    public static final byte DETAIL_QUOTE = 11;
    public static final byte DEPTH_PRICE_BID_VIEW = 12;
    public static final byte DEPTH_PRICE_ASK_VIEW = 13;
    public static final byte PULSE_VIEW = 14;
    public static final byte ANNOUNCEMENT_VIEW = 15;
    public static final byte DETAIL_QUOTE_MFUND = 16;
    public static final byte DETAIL_QUOTE_ORDER = 17;
    public static final byte DETAIL_QUOTE_ACCOUNT = 18;
    public static final byte COMPANY_TABLE_VIEW = 19;
    public static final byte MARKET_DEPTH_CALCULATOR_VIEW = 20;
    public static final byte DETAIL_QUOTE_CASH_FLOW = 21;
    public static final byte DEPTH_SPECIAL_BID_VIEW = 22;
    public static final byte DEPTH_SPECIAL_ASK_VIEW = 23;
    public static final byte SNAP_QUOTE = 24;
    public static final byte OUTER_CHART = 25;
    public static final byte TIMEnSALES_SUMMARY_VIEW = 26;
    public static final byte TRADE_QUEUED_TRADES = 27;
    public static final byte TRADE_PENDING_ORDERS = 28;
    public static final byte TRADE_PROGRAMED_ORDERS = 29;
    public static final byte ORDER_SEARCH = 30;
    public static final byte MARKET_TIMEnSALES_VIEW = 31;
    public static final byte CURRENCY_VIEW = 32;
    public static final byte EXCHANGE_VIEW = 33;
    public static final byte COMBINED_DEPTH_VIEW = 34;
    public static final byte FUND_TRANSFER_FRAME = 40;
    public static final byte STOCK_TRASFER_VIEW = 41;
    public static final byte ACCOUNT_SUMMARY_VIEW = 42;
    public static final byte COMBINED_WINDOW_VIEW = 43;
    public static final byte BROADCAST_MESSAGE_VIEW = 44;
    public static final byte SECTORMAP_VIEW = 45;
    public static final byte NEWS_VIEW = 46;
    public static final byte BROKERS_VIEW = 47;
    public static final byte CORPORATE_ACTION_VIEW = 48;
    public static final byte OPTION_CALLS_VIEW   = 49;
    public static final byte FUTURES_CALLS_VIEW    = 50;
//    public static final byte OPTION_STRIKE_VIEW  = 51;
    public static final byte REGIONAL_QUOTE_VIEW = 52;
    public static final byte FUNDAMENTAL_DATA_VIEW = 53;
    public static final byte OPEN_POSITIONS = 54;
    public static final byte CASHLOG_SEARCH = 55;
    public static final byte IPO_SUBSCRIPTION = 56;
    public static final byte DETAIL_QUOTE_FOREX = 51;
    public static final byte PORTFOLIO_VALUATION = 57;
    public static final byte PORTFOLIO_TXN_HISTORY = 58;
    public static final byte CHART_WINDOW_VIEW = 59;
    public static final byte DEPTH_ODDLOT_VIEW = 60;
    public static final byte SPLIT_ADJUSTMENT_VIEW = 61;
    public static final byte PORTFOLIO_IMPORT = 62;
    public static final byte SECTOR_OVERVIEW = 63;
    public static final byte FULL_QUOTE_VIEW = 64;
    public static final byte MINI_TRADE_VIEW = 70;
    public static final byte INVESTER_WINDOW_VIEW = 100;
    public static final byte MEDIAPLAYER = 101;
    public static final byte RADAR_WATCH_LIST = 102;
    public static final byte DETAILED_MARKET_SUMMARY = 103;

    //added by Shanaka - New portfolio creation - SABB CR
    public static final byte NEW_PF_CREATION = 104;

    public static final int MAIN_DIVIDER = 7;
    public static final int SUB_DIVIDER = 8;

    public static final int WIN1_PANEL = 10;
    public static final int WIN2_PANEL = 11;
    public static final int WIN1_ON = 12;
    public static final int WIN2_ON = 13;
    public static final int WIN3_ON = 14;
    public static final int THEME = 15;
    public static final int TOOLBAR = 16;
    public static final int TICKER = 17;
    public static final int TOP_INDEX = 18;
    public static final int FULL_QUOTE_MDEPTH_TYPE = 19;
    public static final int SYMBOL_INDEX = 49;
    public static final int SELECTED_MARKET = 31;
    public static final int RAPID_ORDER_WINDOW = 32;
    public static final int PORTFOLIO_SIMULATOR_DEFAULT_CURRENCY = 43;


    public static final int STYLE_MINIMIZED = 0;
    public static final int STYLE_MAXIMIZED = 1;
    public static final int STYLE_NORMAL = 2;
    public static final int STYLE_ICONIFIED = 3;

// ---- Added by Shanika -- for new tickers ----
    public static final int UPPER_TICKER = 64;
    public static final int MIDDLE_TICKER = 65;
    public static final int LOWER_TICKER = 66;
    public static final int COMMON_TICKER = 67;

    // ---- Added by Shanika -- For sidebar op---
    public static final int SIDE_BAR_OP = 68;
    public static final int CHART_SIDE_BAR_OP = 69;

    private static final String g_sDefaultRederingIDs =
            //        0         10        20        30        40        50        60        70        80        90        100       110       120        130       140        150      160
                //    "1008FQ9563VD4433q333F4F4333333L34444BS3S3bsrct00043L3033033333333434333333333333310040333333o4444443hBv043LL00vvvvQvvLV3VVV333";
                      //"1008FQ9563Vw44nnq333F4F4333333L34444BS3S3bsrct00044L3033033333333434333333333333310040333333o4434433hBv043LL00ddddlddLV3VVV33356LDLDL550LL4L44000p404L4LLL44p44ppp40Lp0pp6"; //==new rendering id -2009-07-30
            //   "1008FQ9563VD4433q333F4F4333333L34444BS3S3bsrct00043L3033033333333434333333333333310040333333o4444443hBv043LL00ddddlddLV3VVV33356LDLDL550LL4L44000p004LpLLL44p44ppp40Lp0pp"; //==new render

            // "1008FQ9563VD4433q333F4F4333333L34444BS3S3bsrct00043L3033033333333434333333333333310040333333o4444443hBv043LL00ddddlddLV3VVV33356xxx";
            "1008FQ9563Vw44nnq333F4F4333333L34444BS3S3bsrct00044L3033003333333434333333333333310040333333o4434433hBv043LL00ddddlddLV3VVV33356LDLDL550LL4L44000p404L4LLL44p44ppp40Lp0pp600FF3332233333333333333333333333333333333333A3V3V3V33"; //==new rendering id -2010-12-08 Modified for the CR > MORMUBMKT-1041

    //    private static final String g_sDefaultRederingIDs =
//            //                 10        20        30        40        50        60
//                    "1028FQ9563VD44333333F4F4333333L34444BS3S3bsrct01143L3133133333333434";
    private static final String g_sDefaultPortfolioRendIDs =
            //       012345678901
            "222343P3qppP";

    private static final String g_sDefaultMISTRendIDs =
            //       0123456789012345678901
            "22334P343P43WQPQ434434";

    private DataInputStream g_oIn;

    //Vector                  g_oCustomViewsVec;
    static Vector g_oViewsVector;
    static Vector mainBoardViews;
    //static Vector customViews;
    static Vector g_oPortfolioVector;
    static Vector g_oMISTVector;
    //static Vector           g_oMarketViewsVec;
    static Hashtable g_oTempSectors;
    static Hashtable<String, ViewSetting> g_oSummaryViews;
    private Hashtable<String, ViewSetting> g_oDepthViews;
    private Hashtable<String, ViewSetting> g_oTimenSalesViews;
    private Hashtable<String, ViewSetting> g_oMarketTimenSalesViews;
    private Hashtable<String, ViewSetting> g_oAnnouncementViews;
    private Hashtable<String, ViewSetting> g_oDTQViews;
    private Hashtable<String, ViewSetting> g_oSnapViews;
    private Hashtable<String, ViewSetting> g_oOrderDepthViews;
    private Hashtable g_oWindowList;
    private Hashtable<String, ViewSetting> g_oSymbolViews;
    private Hashtable g_oPulseViews;
    private Hashtable<String, ViewSetting> g_oDepthCalViews;
    private Hashtable<String, ViewSetting> g_ochartViews;
    private static ArrayList<ViewSetting> removedViews;
    private Hashtable<String, ViewSetting> g_oFulQuoteViews;
    static Hashtable<String, ViewSetting> g_oForexViews;
    private Hashtable<String, ViewSetting>   g_oOptionsViews;
    private Hashtable<String, ViewSetting>   g_oRQuoteViews;
    private Hashtable<String, ViewSetting>   g_oFD2Views;
    private Hashtable<String, ViewSetting> g_secOverViews;
    private Hashtable<String, ViewSetting> g_miniTradeViews;

    /**
     * Constructor
     */
    public ViewSettingsManager() {
        //g_oCustomViewsVec = new Vector();
        g_oViewsVector = new Vector();
        mainBoardViews = new Vector();
        //customViews = new Vector();
        g_oPortfolioVector = new Vector();
        g_oMISTVector = new Vector();
        g_oTempSectors = new Hashtable();
        g_oSummaryViews = new Hashtable<String, ViewSetting>();
        g_oForexViews = new Hashtable<String, ViewSetting>();
        g_oDepthViews = new Hashtable<String, ViewSetting>();
        g_oTimenSalesViews = new Hashtable<String, ViewSetting>();
        g_oMarketTimenSalesViews = new Hashtable<String, ViewSetting>();
        g_oAnnouncementViews = new Hashtable<String, ViewSetting>();
        g_oDTQViews = new Hashtable<String, ViewSetting>();
        g_oSnapViews = new Hashtable<String, ViewSetting>();
        g_oOrderDepthViews = new Hashtable<String, ViewSetting>();
        g_oWindowList = new Hashtable();
        g_oSymbolViews = new Hashtable<String, ViewSetting>();
        g_oPulseViews = new Hashtable();
        g_oDepthCalViews = new Hashtable<String, ViewSetting>();
        g_ochartViews = new Hashtable<String, ViewSetting>();
        removedViews = new ArrayList<ViewSetting>();
        g_oFulQuoteViews = new Hashtable<String, ViewSetting>();
        g_oOptionsViews   = new Hashtable<String, ViewSetting>();
        g_oRQuoteViews    = new Hashtable<String, ViewSetting>();
        g_oFD2Views       = new Hashtable<String, ViewSetting>();
        g_secOverViews = new Hashtable<String, ViewSetting>();
        g_miniTradeViews = new Hashtable<String, ViewSetting>();
        loadSettings();
        ExchangeStore.getSharedInstance().addExchangeListener(this);
        loadColumnSettings();
    }

    /**
     * Returns the custom views vector
     *
     public Vector getCustomVector()
     {
     return g_oCustomViewsVec;
     }*/

    public static void addToremoveFromMainBoardSettingList(ViewSetting oSetting) {
        /*int i = mainBoardViews.indexOf(oSetting);

        if (i >= 0) {*/
            removedViews.add(oSetting);
//        }
    }

    public static void removeViewsFromList(){
        for(int i =0; i<removedViews.size(); i++){
            mainBoardViews.removeElement((removedViews.get(i)));
//            mainBoardViews.removeElementAt((Integer.parseInt((String)removedViews.get(i)))); // Bug id <?????>
        }
        removedViews.clear();
        removedViews.trimToSize();
    }

    /**
     * Returns the sector views vector
     */
    public static Vector getViewsVector() {
        return g_oViewsVector;
    }

    public static Vector getMainBoardViews() {
        return mainBoardViews;
    }

//    public static Vector getCustomViews() {
//        return customViews;
//    }

    /**
     * Returns the non-market views vector
     * <p/>
     * public static Vector getNonMarketVector()
     * {
     * return g_oNonMarketViewsVec;
     * }
     * <p/>
     * /**
     * Returns the summary views hashteble
     */
    public static Hashtable getSummaryViews() {
        return g_oSummaryViews;
    }
     public static Hashtable getForexViews() {
        return g_oForexViews;
    }

    /**
    * Returns the FD 2  views hashteble
    */
    public Hashtable getFD2Views() {
        return g_oFD2Views;
    }

    /**
    * Returns the OptionChain  views hashtable
    */
    public Hashtable getOptionChainViews() {
        return g_oOptionsViews;
    }

    /**
    * Returns the Regional Quotes views hashteble
    */
    public Hashtable getRQuotesViews() {
        return g_oRQuoteViews;
    }

    /**
     * Returns the timeNsales views hashteble
     */
    public Hashtable getTimenSalesViews() {
        return g_oTimenSalesViews;
    }

    public Hashtable getMarketTimeNSalesViews() {
        return g_oMarketTimenSalesViews;
    }

    /**
     * Returns the alert views hashteble
     */
    public Hashtable getAnnouncementViews() {
        return g_oAnnouncementViews;
    }

    /**
     * Returns the DETAIL QUOTES  views hashteble
     */
    public Hashtable getDTQViews() {
        return g_oDTQViews;
    }

    /**
     * Returns the Snap QUOTES  views hashteble
     */
    public Hashtable getSnapViews() {
        return g_oSnapViews;
    }

    public Hashtable getOrderDepthViews() {
        return g_oOrderDepthViews;
    }

    public Hashtable getsecOverViews() {
        return g_secOverViews;
    }

    public Hashtable getminiTradeViews() {
        return g_miniTradeViews;
    }

    /**
     * Returns the DEPTH CALCULATOR  views hashteble
     * Bandula
     */
    public Hashtable getDepthCalViews() {
        return g_oDepthCalViews;
    }

    /**
     * Returns the depth views hashteble
     */
    public Hashtable getDepthViews() {
        return g_oDepthViews;
    }

    /**
     * Returns the FullQuote views hashteble
     */
    public Hashtable getFullQuoteViews() {
        return g_oFulQuoteViews;
    }
    public ViewSetting getFullQuoteView(String sKey){
        if(g_oFulQuoteViews.containsKey(sKey)) {
            return  g_oFulQuoteViews.get(sKey);
        }else{
            return null;
        }
    }

    /**
     * Returns the Pulse views hashtable
     */
    public Hashtable getPulseViews() {
        return g_oPulseViews;
    }

    /**
     * remove all opened time n sales windows from the list
     */
    public void removePulseViews() {
        g_oPulseViews.clear();
    }

    /**
     * remove all opened time n sales windows from the list
     */
    public void removeChartView(String sKey) {
        //g_oPulseViews.remove("5|" + sKey);
        g_ochartViews.remove(OUTER_CHART + "|" + sKey);
    }

    /**
   * remove all opened Regional Quotes windows from the list
   */
   public void removeRQuoteViews() {
       g_oRQuoteViews.clear();
   }


     /**
   * remove all opened Fundamental Data windows from the list
   */
   public void removeFD2Views() {
       g_oFD2Views.clear();
   }


     /**
   * remove all opened Option Chain windows from the list
   */
   public void removeOptionChainViews() {
       g_oOptionsViews.clear();
   }


    /**
     * Returns the Pulse view for the give key
     */
    public ViewSetting getChartView(String sKey) {
        try {
            return g_ochartViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Store the Pulse view for the given view
     */
    public void putChartView(String sKey, ViewSetting oSetting) {
        g_ochartViews.put(sKey, oSetting);
    }

    /**
     * remove all opened DEPTH CAL windows from the list
     */
    public void removeDepthCalViews() {
        g_oDepthCalViews.clear();
    }

    /**
     * remove all opened time n sales windows from the list
     */
    public void removePulseView(String sKey) {
        g_oPulseViews.remove("5|" + sKey);
    }

    /**
     * Returns the Pulse view for the give key
     */
    public ViewSetting getPulseView(String sKey) {
        try {
            return (ViewSetting) g_oPulseViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns the DETAILQUOTE view for the give key
     */
    public ViewSetting getDepthCalView(String sKey) {
        try {
            return g_oDepthCalViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Store the Pulse view for the given view
     */
    public void putPulseView(String sKey, ViewSetting oSetting) {
        g_oPulseViews.put(sKey, oSetting);
    }

    /**
     * Store the DEPTH CAL view for the given view
     */
    public void putDepthCalView(String sKey, ViewSetting oSetting) {
        g_oDepthCalViews.put(sKey, oSetting);
    }

    public void adjustDepthCalcViewToLinked(String sKey, byte type){
         if(g_oDepthCalViews.containsKey(sKey)){
          ViewSetting oSetting=  g_oDepthCalViews.remove(sKey);
         // oSetting.setID(System.currentTimeMillis()+"" );
          String key = type + "|"+"Linked";
           g_oDepthCalViews.put(key,oSetting);

        }
    }

    public void changeDepthCalcViewKey(String oldsKey, String newSkey){
        if(g_oDepthCalViews.containsKey(oldsKey)){
          ViewSetting oSetting=  g_oDepthCalViews.remove(oldsKey);
         // oSetting.setID(System.currentTimeMillis()+"" );
          //String key = type + "|"+"Linked";
           g_oDepthCalViews.put(newSkey,oSetting);

        }

    }

    /**
    * Returns the Regional Quotes view for the give key
    */
    public ViewSetting getRQuotesView(String sKey) {
        try {
            return g_oRQuoteViews.get(sKey);
        } catch(Exception e) {
            return null;
        }
    }

     /**
    * Returns the Fundamental Data view for the give key
    */
    public ViewSetting getFD2View(String sKey) {
        try {
            return g_oFD2Views.get(sKey);
        } catch(Exception e) {
            return null;
        }
    }

     /**
    * Returns the FOption CHain view for the give key
    */
    public ViewSetting getOptionChainView(String sKey) {
        try {
            return g_oOptionsViews.get(sKey);
        } catch(Exception e) {
            return null;
        }
    }

    /**
    * Store the Regional Quote view for the given view
    */
    public void putRQuotesView(String sKey, ViewSetting oSetting) {
        g_oRQuoteViews.put(sKey, oSetting);
    }

    /**
     * remove all opened Regional Quote windows from the list
     */
    public void removeRQuotesViews() {
        g_oRQuoteViews.clear();
    }

    public  void removeRQuotesView(String key){
        g_oRQuoteViews.remove(key);
    }

    /**
    * Store the Fundamental Data view for the given view
    */
    public void putFD2View(String sKey, ViewSetting oSetting) {
        g_oFD2Views.put(sKey, oSetting);
    }


    public  void removeFD2View(String key){
        g_oFD2Views.remove(key);
    }

    /**
    * Store the Option Chain view for the given view
    */
    public void putOptionChainView(String sKey, ViewSetting oSetting) {
        g_oOptionsViews.put(sKey, oSetting);
    }


    public  void removeOptionChainView(String key){
        g_oOptionsViews.remove(key);
    }

    /**
     * remove all opened time n sales windows from the list
     */
    public void removeTimenSalesViews() {
        g_oTimenSalesViews.clear();
    }


    public  void removeTimenSalesView(String key){
        g_oTimenSalesViews.remove(key);
    }

    /**
     * remove all opened time n sales windows from the list
     */
    public void removeAnnouncementViews() {
        g_oAnnouncementViews.clear();
    }

    /**
     * remove all opened DETAIL QUOTE windows from the list
     */
    public void removeDTQViews() {
        g_oDTQViews.clear();
    }

    /**
     * remove all opened Snap QUOTE windows from the list
     */
    public void removeSnapViews() {
        g_oSnapViews.clear();
    }

    public void removeOrderDepthViews() {
        g_oOrderDepthViews.clear();
    }

    /**
     * remove all opened market depth windows from the list
     */
    public void removeDepthSalesViews() {
        g_oDepthViews.clear();
    }

    /**
     *
     */
    public InternalFrame getWindow(String sKey) {
        try {
            return (InternalFrame) g_oWindowList.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    public void adjustWindowToLinkedType(String sKey, byte subType){
         if(g_oWindowList.containsKey(sKey)){
          InternalFrame frame=  (InternalFrame)g_oWindowList.remove(sKey);

         // String key = ViewSettingsManager.DETAIL_QUOTE + "|"+"Linked";
          String key = subType + "|"+"Linked";
           g_oWindowList.put(key,frame);

        }

    }

      public void adjustWindowToLinkedType(String sKey, byte subType,String group){
         if(g_oWindowList.containsKey(sKey)){
          InternalFrame frame=  (InternalFrame)g_oWindowList.remove(sKey);

         // String key = ViewSettingsManager.DETAIL_QUOTE + "|"+"Linked";
          String key = subType + "|"+"Linked"+"_"+group;
           g_oWindowList.put(key,frame);

        }

    }

    public void changeKeyOfFrame(String oldSkey, String newSkey){
        if(g_oWindowList.containsKey(oldSkey)){

             InternalFrame frame=  (InternalFrame)g_oWindowList.remove(oldSkey);

         // String key = ViewSettingsManager.DETAIL_QUOTE + "|"+"Linked";
           g_oWindowList.put(newSkey,frame);

        }

    }

    public void removeWindow(String sKey) {
        try {
            g_oWindowList.remove(sKey);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /**
     *
     */
    public void setWindow(String sKey, InternalFrame oWindow) {
        g_oWindowList.put(sKey, oWindow);
    }

    /**
     *
     */
    public Hashtable getWindowList() {
        return g_oWindowList;
    }

    public void removeWindows() {
        g_oWindowList.clear();
    }

    /**
     * Returns the timeNsales view for the give key
     */
    public ViewSetting getTimenSalesView(String sKey) {
        try {
            return g_oTimenSalesViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    public ViewSetting getMarketTimenSalesView(String sKey) {
        try {
            return g_oMarketTimenSalesViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns the Alert view for the give key
     */
    public ViewSetting getAnnouncementView(String sKey) {
        try {
            return g_oAnnouncementViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns the DETAILQUOTE view for the give key
     */
    public ViewSetting getDTQView(String sKey) {
        try {
            return g_oDTQViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns the DETAILQUOTE view for the give key
     */
    public ViewSetting getSnapView(String sKey) {
        try {
            return g_oSnapViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    public ViewSetting getOrderDepthView(String sKey) {
        try {
            return g_oOrderDepthViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns the depth view for the give key
     */
    public ViewSetting getDepthView(String sKey) {
        try {
            return g_oDepthViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns the depth view for the give key
     */
    public ViewSetting getFullQuoteSubView(String sKey) {
        try {
            return g_oFulQuoteViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns the symbol view for the give key
     */
    public ViewSetting getSymbolView(String sKey) {
        try {
            return g_oSymbolViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    public ViewSetting getSectorOverView(String sKey) {
        try {
            return g_secOverViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }

    /**
     * Returns the MINITRADEVIEW view for the give key
     */

    public ViewSetting getMiniTradeView(String sKey) {
      try{
            return g_miniTradeViews.get(sKey);
        } catch (Exception e) {
            return null;
        }
    }
    public static ViewSetting getView(int iType, String sID) {
        for (int i = 0; i < g_oViewsVector.size(); i++) {
            ViewSetting oSetting = (ViewSetting) (g_oViewsVector.elementAt(i));
            if ((oSetting.getType() == iType) && (oSetting.getID().equals(sID))) {
                return oSetting;
            }
        }
        return null;
    }

    public static ViewSetting getMainBoardView(int iType, String sID) {
        for (int i = 0; i < mainBoardViews.size(); i++) {
            ViewSetting oSetting = (ViewSetting) (mainBoardViews.elementAt(i));
            if ((oSetting.getType() == iType) && (oSetting.getID().equals(sID))) {
                return oSetting;
            }
        }
        return null;
    }

    public ViewSetting getPortfolioView(int iType, String sID) {
        for (int i = 0; i < g_oPortfolioVector.size(); i++) {
            ViewSetting oSetting = (ViewSetting) (g_oPortfolioVector.elementAt(i));
            if ((oSetting.getType() == iType) && (oSetting.getID().equals(sID))) {
                return oSetting;
            }
        }
        return null;
    }

    public ViewSetting getMISTView(int iType, String sID) {
        for (int i = 0; i < g_oMISTVector.size(); i++) {
            ViewSetting oSetting = (ViewSetting) (g_oMISTVector.elementAt(i));
            if ((oSetting.getType() == iType) && (oSetting.getID().equals(sID))) {
                return oSetting;
            }
        }
        return null;
    }


    public Hashtable getChartViews() {
        return g_ochartViews;
    }

    /**
     * remove all opened time n sales windows from the list
     */
    public void removeChartViews() {
        g_ochartViews.clear();
    }

    /**
     * Store the timeNsales view for the given view
     */
    public void putTimenSalesView(String sKey, ViewSetting oSetting) {
        g_oTimenSalesViews.put(sKey, oSetting);
    }

    public void adjustTimenSalesViewToLinked(String sKey, byte type){
        if(g_oTimenSalesViews.containsKey(sKey)){
          ViewSetting oSetting=  g_oTimenSalesViews.remove(sKey);
          oSetting.setID(System.currentTimeMillis()+"" );
          String key = type + "|"+"Linked";
           g_oTimenSalesViews.put(key,oSetting);

        }

    }

    public void changeTimenSalesViewKey(String oldsKey, String newsKey){
        if(g_oTimenSalesViews.containsKey(oldsKey)){
          ViewSetting oSetting=  g_oTimenSalesViews.remove(oldsKey);
          oSetting.setID(System.currentTimeMillis()+"" );
        //  String key = type + "|"+"Linked";
           g_oTimenSalesViews.put(newsKey,oSetting);

        }

    }

    public void putMarketTimenSalesView(String sKey, ViewSetting oSetting) {
        g_oMarketTimenSalesViews.put(sKey, oSetting);
    }

    /**
     * Store the timeNsales view for the given view
     */
    public void putAnnouncementView(String sKey, ViewSetting oSetting) {
        g_oAnnouncementViews.put(sKey, oSetting);
    }

     public void putSectorOverView(String sKey, ViewSetting oSetting) {
        g_secOverViews.put(sKey, oSetting);
    }

    /**
     * Store the MINITRADE view for the given view
     */
    public void putMiniTradeView(String sKey, ViewSetting oSetting) {
        g_miniTradeViews.put(sKey, oSetting);
    }

     /**
     * Store the MINITRADE view for the given view
     */
    public void removeMiniTradeView(String sKey) {
        g_miniTradeViews.remove(sKey);
    }
    /**
     * Store the DETAILQUOTE view for the given view
     */
    public void putDTQView(String sKey, ViewSetting oSetting) {
        g_oDTQViews.put(sKey, oSetting);
    }

    public void adjustDTQViewToLinked(String sKey){
        if(g_oDTQViews.containsKey(sKey)){
          ViewSetting oSetting=  g_oDTQViews.remove(sKey);
          oSetting.setID(System.currentTimeMillis()+"" );
          String key = ViewSettingsManager.DETAIL_QUOTE + "|"+"Linked";
           g_oDTQViews.put(key,oSetting);

        }

    }

     public void adjustDTQViewToLinked(String sKey, String group){
        if(g_oDTQViews.containsKey(sKey)){
          ViewSetting oSetting=  g_oDTQViews.remove(sKey);
          oSetting.setID(System.currentTimeMillis()+"" );
          String key = ViewSettingsManager.DETAIL_QUOTE + "|"+"Linked_"+group;
           g_oDTQViews.put(key,oSetting);

        }

    }

    public void changeDTQViewKey(String oldsKey, String newsKey){
        if(g_oDTQViews.containsKey(oldsKey)){
                 ViewSetting oSetting=  g_oDTQViews.remove(oldsKey);
                 oSetting.setID(System.currentTimeMillis()+"" );
                 //String key = ViewSettingsManager.DETAIL_QUOTE + "|"+"Linked_"+group;
                  g_oDTQViews.put(newsKey,oSetting);

        }

    }

    public void changeFullQuoteViewKey(String oldsKey, String newsKey){
        if(g_oFulQuoteViews.containsKey(oldsKey)){
                 ViewSetting oSetting=  g_oFulQuoteViews.remove(oldsKey);
                 oSetting.setID(System.currentTimeMillis()+"" );
                 //String key = ViewSettingsManager.DETAIL_QUOTE + "|"+"Linked_"+group;
                  g_oFulQuoteViews.put(newsKey,oSetting);

        }
    }

    /**
     * Store the DETAILQUOTE view for the given view
     */
    public void putSnapView(String sKey, ViewSetting oSetting) {
        g_oSnapViews.put(sKey, oSetting);
    }

    public void adjustSnapViewToLinked(String sKey){
         if(g_oSnapViews.containsKey(sKey)){
          ViewSetting oSetting=  g_oSnapViews.remove(sKey);
          oSetting.setID(System.currentTimeMillis()+"" );
          String key = ViewSettingsManager.SNAP_QUOTE + "|"+"Linked";
           g_oSnapViews.put(key,oSetting);

        }
    }

    public void changeSnapViewKey(String oldsKey, String newsKey){
        if(g_oSnapViews.containsKey(oldsKey)){
          ViewSetting oSetting=  g_oSnapViews.remove(oldsKey);
          oSetting.setID(System.currentTimeMillis()+"" );
         // String key = ViewSettingsManager.SNAP_QUOTE + "|"+"Linked";
           g_oSnapViews.put(newsKey,oSetting);

        }

    }

    public void putOrderDepthView(String sKey, ViewSetting oSetting) {
        g_oOrderDepthViews.put(sKey, oSetting);
    }

    /**
     * Store the timeNsales view for the given view
     */
    public void putDepthView(String sKey, ViewSetting oSetting) {
        g_oDepthViews.put(sKey, oSetting);
    }

    public void adjustDepthViewToLinked(String sKey, byte type){
        if(g_oDepthViews.containsKey(sKey)){
          ViewSetting oSetting=  g_oDepthViews.remove(sKey);
         // oSetting.setID(System.currentTimeMillis()+"" );
          String key = type + "|"+"Linked";
           g_oDepthViews.put(key,oSetting);

        }
    }

    public void changeDepthViewKey(String oldsKey,String newsKey){
         if(g_oDepthViews.containsKey(oldsKey)){
          ViewSetting oSetting=  g_oDepthViews.remove(oldsKey);
         // oSetting.setID(System.currentTimeMillis()+"" );
          //String key = type + "|"+"Linked";
           g_oDepthViews.put(newsKey,oSetting);

        }
    }

    public void removeDepthView(String key){
        g_oDepthViews.remove(key);
    }

    public void putFullQuoteSubView(String sKey, ViewSetting oSetting) {
        g_oFulQuoteViews.put(sKey, oSetting);
    }

    public void putFullQuoteMainView(String sKey, ViewSetting oSetting) {
        g_oFulQuoteViews.put(sKey, oSetting);
    }


    /**
     * Returns the view setting object identified
     * by the given ID
     */
    public static ViewSetting getSummaryView(String sID) {
        return g_oSummaryViews.get(sID);
    }

    public static ViewSetting getForexView(String sID) {
        return g_oForexViews.get(sID);
    }

    public static Vector getPortfolioViews() {
        return g_oPortfolioVector;
    }

    public static Vector getMISTViews() {
        return g_oMISTVector;
    }

    public void removeExchangeSpecificViews(String exchange){
        removeExchangeSpecificViews(exchange, g_oDepthViews);
        removeExchangeSpecificViews(exchange, g_oTimenSalesViews);
        removeExchangeSpecificViews(exchange, g_oMarketTimenSalesViews);
        removeExchangeSpecificViews(exchange, g_oAnnouncementViews);
        removeExchangeSpecificViews(exchange, g_oDTQViews);
        removeExchangeSpecificViews(exchange, g_oSnapViews);
        removeExchangeSpecificViews(exchange, g_oOrderDepthViews);
        removeExchangeSpecificViews(exchange, g_oSymbolViews);
        removeExchangeSpecificViews(exchange, g_oDepthCalViews);
        removeExchangeSpecificViews(exchange, g_oRQuoteViews);
        removeExchangeSpecificViews(exchange, g_oFD2Views);
        removeExchangeSpecificViews(exchange, g_oOptionsViews);
    }

    public void removeExchangeSpecificViews(String exchange, Hashtable<String, ViewSetting> table){
        try {
            Enumeration<String> keys = table.keys();
            while (keys.hasMoreElements()){
                String key = keys.nextElement();
                try {
                    String symbolKey = key.split("\\|")[1];
                    if (SharedMethods.getExchangeFromKey(symbolKey).equals(exchange)){
                        ViewSetting viewSetting =  table.remove(key);
                        if (viewSetting.getParent() instanceof InternalFrame)
                            ((InternalFrame)viewSetting.getParent()).dispose();
                    }
                } catch (Exception e) {
                    //e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadColumnSettings(){
        Enumeration<ViewSetting> summViews = g_oSummaryViews.elements();
        while(summViews.hasMoreElements()){
            ViewSetting sett = summViews.nextElement();
            try {
                GUISettings.setColumnSettings(sett,TWColumnSettings.getItem(sett.getID()));
            } catch (Exception e) {
                System.err.println("Column setting loading failed : " + sett.getID() ) ;
//                e.printStackTrace();
            }
            sett = null;
        }
        summViews = null;
    }

    private void loadSettings() {
        try {
            String sSetting = null;
            BufferedReader oIn = new BufferedReader(new FileReader(Settings.CONFIG_DATA_PATH+"/ViewSettings.ini"));
            while (true) {
                sSetting = oIn.readLine();
                if (sSetting == null) break;
                if (sSetting.trim().equals("")) continue;
                if (sSetting.trim().startsWith("#")) continue;
                tokenize(sSetting);
            }
            oIn.close();
            oIn = null;
        } catch (Exception e) {
            e.printStackTrace();
            // to do error handeling
        }
    }

    private void tokenize(String sSetting) {
        try {
            int iType;

            StringTokenizer oTokens = new StringTokenizer(sSetting, "|");
            //    ViewType|ID|Hidden Cols|sel cols|fixed cols|fnt size|window settings               |captions     |symbols
            // format -> 0|0 |0          |127     |127       |12      |T,L,W,H,Status,INDX,Visibility|Uditha,Uditha|emob.ca;mprc.ca

            ViewSetting oSetting = new ViewSetting();

            /* Read the view type */
            iType = intValue(oTokens.nextToken());
            //if (isValidType(iType))
            oSetting.setType(iType);
            //else
            //    throw (new Exception("Invalid view type"));
            /* read the view id */
            oSetting.setID(oTokens.nextToken());
            /* read hidden columns */
            //change start
            BigInteger hiddencols = new BigInteger(oTokens.nextToken());
            oSetting.setHiddenColumns(hiddencols);
            hiddencols = null;
            /* read selected columns */
            //change start
            BigInteger cols = new BigInteger(oTokens.nextToken());
            oSetting.setColumns(cols);
            cols = null;
            /* read Fixed columns */
            //change start
            BigInteger fixedcols = new BigInteger(oTokens.nextToken());
            oSetting.setFixedColumns(fixedcols);
            fixedcols = null;
            /* read font size */
            oSetting.setFont(getFontObject(oTokens.nextToken()));
            /* window settings */
            setWindowSettings(oSetting, oTokens.nextToken());
            /* rendering IDs */
            if ((oSetting.getMainType() == CUSTOM_VIEW) ||
                    (oSetting.getMainType() == MARKET_VIEW) ||
                    (oSetting.getMainType() == SECTOR_VIEW) ||
                    (oSetting.getMainType() == FILTERED_VIEW)) {
                oSetting.setRenderingIDs(getRenderingIDs(oSetting.getType()));
            } else if (oSetting.getMainType() == MIST_VIEW) {
                oSetting.setRenderingIDs(g_sDefaultMISTRendIDs);
            } else
                oSetting.setRenderingIDs(oTokens.nextToken());
            /* read captions */
            if (oSetting.getMainType() != SECTOR_VIEW) {
                String sCaptions = oTokens.nextToken();
                if ((oSetting.getMainType() == CUSTOM_VIEW) ||
                        (oSetting.getMainType() == PORTFOLIO_VIEW) ||
                        (oSetting.getMainType() == FILTERED_VIEW) ||
                        (oSetting.getMainType() == MIST_VIEW)) {
                    oSetting.setCaptions(UnicodeUtils.getNativeString(sCaptions));
                } else
                    oSetting.setCaptionID(UnicodeUtils.getNativeString(sCaptions));
            }
            /* read symbols */
            if ((oSetting.getMainType() == CUSTOM_VIEW)
//                || (oSetting.getGroup() == this.PORTFOLIO_VIEW)
                    || (oSetting.getMainType() == MIST_VIEW))
                try {
                    /* This token will is not available if
                        there are no symbols added. Hence this
                        raise an exception */
                    oSetting.setSymbols(oTokens.nextToken());
                } catch (Exception e) {
                    oSetting.setSymbols("");
                }
            /* Read column headings */
            if ((oSetting.getMainType() == SECTOR_VIEW) ||
                    (oSetting.getMainType() == MARKET_VIEW) ||
                    (oSetting.getMainType() == FILTERED_VIEW) ||
                    (oSetting.getMainType() == CUSTOM_VIEW)) {
                oSetting.setColumnHeadings(Language.getList("TABLE_COLUMNS"));
            } else if (oSetting.getMainType() == MIST_VIEW) {
                oSetting.setColumnHeadings(makeHTML(Language.getList("COL_MIST_VIEW")));
            }else if(oSetting.getMainType() == FOREX_VIEW){

            } else {
                String sHeadingsID = oTokens.nextToken();
                oSetting.setColumnHeadingsID(sHeadingsID);
                oSetting.setColumnHeadings(Language.getList(sHeadingsID));
            }

            switch (oSetting.getMainType()) {
                case CUSTOM_VIEW:
                    g_oViewsVector.addElement(oSetting);
                    break;
                case MARKET_VIEW:
                    g_oViewsVector.addElement(oSetting);
                    break;
                case FILTERED_VIEW:
                    g_oViewsVector.addElement(oSetting);
                    break;
                case SECTOR_VIEW:
                    g_oTempSectors.put(oSetting.getID(), oSetting);
                    break;
                case SUMMARY_VIEW:
                    g_oSummaryViews.put(oSetting.getID(), oSetting);
                    break;
                case SYMBOL_VIEW:
                    g_oSymbolViews.put(oSetting.getID(), oSetting);
                    break;
                case PORTFOLIO_VIEW:
                    g_oPortfolioVector.addElement(oSetting);
                    break;
                case MIST_VIEW:
                    g_oMISTVector.addElement(oSetting);
                    break;
                case FOREX_VIEW:
                    g_oForexViews.put(oSetting.getID(),oSetting);
                    break;
            }

        } catch (Exception e) {
            System.out.println(sSetting);
            e.printStackTrace();
            // to do error handeling
        }

    }

    public static String[] getList(String sID) {
        String sAllCols = null;
        String sCols = "";
        String[] saCols = null;
        int i = 0;

        sAllCols = Settings.getItem(sID);
        StringTokenizer oTokens = new StringTokenizer(sAllCols, ",");

        saCols = new String[oTokens.countTokens()];

        i = 0;
        while (oTokens.hasMoreTokens()) {
            saCols[i] = oTokens.nextToken().trim();
            i++;
        }
        return saCols;

    }

    /**
     * Converts the given value to an int
     */
    protected static int intValue(String sValue) throws Exception {
        return (Integer.parseInt(sValue.trim()));
    }

    protected Font getFontObject(String sFont) throws Exception {
        StringTokenizer oTokens = new StringTokenizer(sFont, ",");
        if (oTokens.countTokens() != 3)
            return null;
        else
            return new TWFont(oTokens.nextToken(),
                    intValue(oTokens.nextToken()), intValue(oTokens.nextToken()));
    }

    /**
     * Converts the given value to a byte
     */
    protected static byte byteValue(String sValue) throws Exception {
        return (Byte.parseByte(sValue.trim()));
    }

    protected static long longValue(String sValue) throws Exception {
        return (Long.parseLong(sValue.trim()));
    }

    /**
     * Add a new stiing
     */
    public void addSetting(ViewSetting oSetting) {
        g_oViewsVector.addElement(oSetting);
    }

    public static void addMainBoardSetting(ViewSetting oSetting) {
        mainBoardViews.addElement(oSetting);
    }

    public static void addForexSetting(ViewSetting oSetting){
       g_oForexViews.put(oSetting.getID(),oSetting);
    }

    /**
     * Set window settings values
     */
    protected void setWindowSettings(ViewSetting oSetting, String sSettings) throws Exception {
        StringTokenizer oTokens = new StringTokenizer(sSettings, ",");
        if (oTokens.countTokens() != 7) {
            oSetting.setLocation(new Point(0, 0));
            oSetting.setSize(new Dimension(500, 500));
            oSetting.setStyle(STYLE_NORMAL);
            oSetting.setIndex(-1);
            oSetting.setVisible(false);
        } else {
            oSetting.setLocation(new Point(intValue(oTokens.nextToken()), intValue(oTokens.nextToken())));
            oSetting.setSize(new Dimension(intValue(oTokens.nextToken()), intValue(oTokens.nextToken())));
            oSetting.setStyle(intValue(oTokens.nextToken()));
            oSetting.setIndex(intValue(oTokens.nextToken()));
            oSetting.setVisible(intValue(oTokens.nextToken()) == 1);
        }

    }

    /**
     * Removes the given element from settings collection
     */
    public void removeSetting(ViewSetting oSetting) {
        int i = g_oViewsVector.indexOf(oSetting);

        if (i >= 0) {
            g_oViewsVector.removeElementAt(i);
        }
    }

    public static void removeMainBoardSetting(ViewSetting oSetting) {
        int i = mainBoardViews.indexOf(oSetting);

        if (i >= 0) {
            mainBoardViews.removeElementAt(i);
        }
    }

   public static void removeForexWatchSettings(ViewSetting oSetting){
       g_oForexViews.remove(oSetting.getID());

   }

    public static String getRenderingIDs(int iID) {
        int iMainType = (iID >> 8);

        if ((iMainType == MARKET_VIEW) || (iMainType == CUSTOM_VIEW)
                || (iMainType == SECTOR_VIEW) || (iMainType == FILTERED_VIEW)) {
            return g_sDefaultRederingIDs;
        } else
            return "";
    }

     public static String getMainBoardRenderingIDs(){
         return g_sDefaultRederingIDs;
    }

    private String[] makeHTML(String[] list) {
        for (int i = 0; i < list.length; i++) {
            list[i] = "<html><p align=center>" + list[i].replaceAll("/", "<br>");
        }
        return list;
    }

    /**
     * Add a new Portfolio Table settiing
     */
    public void addPortfolioSetting(ViewSetting oSetting) {
        g_oPortfolioVector.addElement(oSetting);
    }

    public void exchangeAdded(Exchange exchange) {

    }

    public void exchangeCurrencyChanged(Exchange exchange) {

    }

    public void exchangeDowngraded(Exchange exchange) {

    }

    public void exchangeInformationTypesChanged() {

    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void exchangeMasterFileLoaded(String exchange) {

    }

    public void exchangeMustInitialize(Exchange exchange,long newDate, long oldDate) {

    }

    public void exchangeRemoved(Exchange exchange) {
       removeExchangeSpecificViews(exchange.getSymbol());
    }

    public void exchangesAdded(boolean offlineMode) {

    }

    public void exchangesLoaded() {

    }

    public void exchangeTimeZoneChanged(Exchange exchange) {

    }

    public void exchangeUpgraded(Exchange exchange) {

    }

    public void removeViewSettingOnReset(ViewSetting oSetting ){
        if(oSetting!= null){
            byte subType= oSetting.getSubType();
            if(subType== DETAIL_QUOTE){
                InternalFrame frame = (InternalFrame) oSetting.getParent();
                if(frame!= null){
                    String key="";
                    String linkgrp = frame.getLinkedGroupID();
                    if(linkgrp== LinkStore.LINK_NONE){
                        key = DETAIL_QUOTE + "|" + oSetting.getID();
                    }else{
                        key = DETAIL_QUOTE +"|"+ LinkStore.linked+ "_"+ frame.getLinkedGroupID();
                    }
                    g_oDTQViews.remove(key);
                }


            }


        }


    }
}

