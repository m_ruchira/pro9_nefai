package com.isi.csvr.properties;

import com.isi.csvr.shared.Settings;

import java.util.Hashtable;
import java.io.FileInputStream;
import java.io.ObjectInputStream;
import java.io.FileOutputStream;
import java.io.ObjectOutputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Dilum Jagoda
 * Date: May 15, 2007
 * Time: 2:30:34 PM
 * To change this template use File | Settings | File Templates.
 */
public class DefaultSettingsManager {

    private static DefaultSettingsManager self = null;
    private Hashtable<String,String> settings;

    private DefaultSettingsManager(){
        settings = new Hashtable<String, String>();    
    }

    public void addDefaultSetting(String type, String data){
        settings.put(type,data);
    }

    public void removeDefaultSetting(String type){
        try {
            settings.remove(type);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public void applyDefaultSetting(int type, ViewSetting vSetting){
        String data = settings.get(""+type);
        if(data != null){
            vSetting.setWorkspaceRecord(data);
        }
    }

    public static DefaultSettingsManager getSharedInstance(){
        if(self == null){
            self = new DefaultSettingsManager();
        }
        return self;
    }

    public void saveSettings(){
        try
        {
            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath()+"datastore/defaultSettings.mdf");
            ObjectOutputStream oObjOut = new ObjectOutputStream(out);
            oObjOut.writeObject(settings);
            out.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
    }

    public void loadSettings(){
        try
        {
            FileInputStream oIn = new FileInputStream(Settings.getAbsolutepath()+"datastore/defaultSettings.mdf");
            ObjectInputStream oObjIn = new ObjectInputStream(oIn);
            settings = (Hashtable<String, String>)oObjIn.readObject();
            oIn.close();
        }
        catch(Exception e)
        {
            e.printStackTrace();
        }
        if (settings == null){
            settings = new Hashtable<String, String>();
        }
    }

}
