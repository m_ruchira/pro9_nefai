package com.isi.csvr.properties;

// Copyright (c) 2000 Home

import java.util.Comparator;

public class ViewSettingComparator implements Comparator {

    public static final int ASCENDING = 1;
    public static final int DESCENDING = -1;

    public static final int Z_ORDER = 0;
    public static final int CAPTION = 1;

    public int column = 0;
    private int sortOrder = ASCENDING;


    public void setColumn(int column) {
        this.column = column;
    }

    public int compare(Object o1, Object o2) {
        ViewSetting setting1 = (ViewSetting) o1;
        ViewSetting setting2 = (ViewSetting) o2;

        switch (column) {
            case CAPTION:
                return compareValues(setting1.getCaption(), setting2.getCaption());
            default: // Z_ORDER
                return compareValues(setting1.getIndex(), setting2.getIndex());
        }
    }

    public boolean equals(Object obj) {
        return super.equals(obj);
    }

    private int compareValues(int val1, int val2) {
        return (int) ((val1 - val2) * this.sortOrder);
    }

    private int compareValues(long val1, long val2) {
        if (val1 > val2)
            return this.sortOrder;
        else if (val1 == val2)
            return 0;
        else
            return this.sortOrder * -1;
    }

    private int compareValues(double val1, double val2) {
        if (val1 > val2)
            return this.sortOrder;
        else if (val1 == val2)
            return 0;
        else
            return this.sortOrder * -1;
    }

    private int compareValues(float val1, float val2) {
        if (val1 > val2)
            return this.sortOrder;
        else if (val1 == val2)
            return 0;
        else
            return this.sortOrder * -1;
    }

    private int compareValues(String val1, String val2) {
        return (val1.compareTo(val2) * this.sortOrder);
    }


    public void setSortOrder(int sortOrder) {
        this.sortOrder = sortOrder;
    }
}