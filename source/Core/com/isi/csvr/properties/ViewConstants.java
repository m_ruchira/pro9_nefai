package com.isi.csvr.properties;

public interface ViewConstants {
    public static final int VC_TYPE         = 0;
    public static final int VC_ID           = 1;
    public static final int VC_COLUMNS      = 2;
    public static final int VC_FONT         = 3;
    public static final int VC_HEADER_FONT  = 4;
    public static final int VC_WINDOW_SETTINGS = 5;
    public static final int VC_SORT_COLUMN  = 6;
    public static final int VC_SORT_ORDER   = 7;
    public static final int VC_SPLIT_LOCATION = 8;
    public static final int VC_COLUMN_SETTINGS = 9;
    public static final int VC_CAPTIONS     = 10;
    public static final int VC_SYMBOLS      = 11;
    public static final int VC_WINDOW_LOCATION = 12;
    public static final int VC_WINDOW_SIZE  = 13;
    public static final int VC_WINDOW_STYLE = 14;
    public static final int VC_WINDOW_INDEX = 15;
    public static final int VC_WINDOW_VISIBLE = 16;
    public static final int VC_WINDOW_HEADER = 17;
    public static final int VC_FILTER       = 18;
    public static final int VC_FILTER_TYPE  = 19;
    public static final int VC_PORTFOLIO_DATA = 20;
    public static final int VC_TnS_SUMMARY  = 21;
    public static final int VC_TICKER_FIXED = 22;
    public static final int VC_GRAPH_BASE   = 23;
    public static final int VC_GRAPH_COMPARE = 24;
    public static final int VC_GRAPH_MODE   = 25;
    public static final int VC_GRAPH_PERIOD = 26;
    public static final int VC_GRAPH_INTERVAL = 27;
    public static final int VC_GRAPH_STYLE  = 28;
    public static final int VC_GRAPH_VOLUME = 29;
    public static final int VC_GRAPH_INDEXED = 30;
    public static final int VC_GRAPH_DESCRIPTION = 31;
    public static final int VC_GRAPH_DETACHED = 32;
    public static final int VC_MKTMAP_SORTING = 36;
    public static final int VC_MKTMAP_THEME     = 37;
    public static final int VC_MKTMAP_COLUMNS = 38;
    public static final int VC_MKTMAP_STYLE     = 39;
    public static final int VC_MKTMAP_CRITERIA = 40;
    public static final int VC_MKTMAP_ID        = 41;
    public static final int VC_DEPTH_CALC_VALUE = 42;
    public static final int VC_DEPTH_CALC_TYPE  = 45;
    public static final int VC_GRAPH_DATA_MODE  = 43;
    public static final int VC_TOP_STOCKS_TYPE  = 44;
    public static final int VC_TOP_STOCK_EXCHANGE   = 46;
    public static final int VC_CURRENCY             = 47;
    public static final int VC_WATCHLIST_FILTER     = 48;
    public static final int VC_CURRENCY_LIST        = 49;
    public static final int VC_DECIMAL_PLACES       = 50;
    public static final int VC_DESKTOP_TYPE         = 51;
    public static final int VC_CHART_LAYOUT         = 52;
    public static final int VC_DETACH               = 53;
    public static final int VC_HEATMAP_MODE         = 54;
    public static final int VC_HEATMAP_VALONOFF     = 55;
    public static final int VC_OPTION_MONTH         = 56;
    public static final int VC_OPTION_TYPE          = 57;
    public static final int VC_TITLE_LANG_ID        = 58;
    public static final int VC_TOP_STOCK_INSTRUMENT = 59;
    public static final int VC_MARKETINDICES_EXCHANGE = 60;
    public static final int VC_SYMBOLWISEVIEW_KEY = 61;
    public static final int VC_LINKED = 62;
    public static final int VC_LINKED_GROUP = 63;
    public static final int VC_BID_ASK_CHART_VISIBLE = 64;
    public static final int VC_CHART_NAVIGAION_BAR_VISIBLE = 65;
    public static final int VC_CHART_NAVIGAION_BAR_LINK_ID = 66;
    public static final int VC_CHART_WINDOW_ARRANGE_MODE= 67;
    public static final int VC_TICKER_FIXED_TO_TOP = 68;
    public static final int VC_TICKER_CUSTOM_VISIBLE = 69;
    public static final int VC_TICK_MODE = 70;
    public static final int VC_TITLE_CAPTION        = 101;


    public static final int CS_CUSTOMIZED = 99;
    public static final int CS_ROW1_FG = 100;
    public static final int CS_ROW1_BG = 101;
    public static final int CS_ROW2_FG = 102;
    public static final int CS_ROW2_BG = 103;
    public static final int CS_DOWN_FG = 104;
    public static final int CS_DOWN_BG = 105;
    public static final int CS_UP_FG = 106;
    public static final int CS_UP_BG = 107;
    public static final int CS_SELECTED_FG = 108;
    public static final int CS_SELECTED_BG = 109;
    public static final int CS_TRADE_ROW1_FG = 110;
    public static final int CS_TRADE_ROW1_BG = 111;
    public static final int CS_TRADE_ROW2_FG = 112;
    public static final int CS_TRADE_ROW2_BG = 113;
    public static final int CS_BID_ROW1_FG = 114;
    public static final int CS_BID_ROW1_BG = 115;
    public static final int CS_BID_ROW2_FG = 116;
    public static final int CS_BID_ROW2_BG = 117;
    public static final int CS_ASK_ROW1_FG = 118;
    public static final int CS_ASK_ROW1_BG = 119;
    public static final int CS_ASK_ROW2_FG = 120;
    public static final int CS_ASK_ROW2_BG = 121;
    public static final int CS_CHANGE_UP_FG = 122;
    public static final int CS_CHANGE_DOWN_FG = 123;
    public static final int CS_GRID_FG = 124;
    public static final int CS_TABLE_SETTINGS = 125;
    public static final int CS_GRID_ON = 126;
    public static final int CS_BODY_GAP = 127;

    
   public static final int VC_WATCHLIST_TYPE   = 140;

    //----------Added by shanika ------

    public static final int CS_MIN_FG = 128;
    public static final int CS_MIN_BG = 129;

    public static final int CS_MAX_FG = 130;
    public static final int CS_MAX_BG = 131;

    public static final int CS_WEEK52LOW_FG = 132;
    public static final int CS_WEEK52LOW_BG = 133;

    public static final int CS_WEEK52HIGH_FG = 134;
    public static final int CS_WEEK52HIGH_BG = 135;

    //-------- To Enable/disable advanced colorsettings
    public static final int CS_MIN_ENABLED = 136;
    public static final int CS_MAX_ENABLED = 137;
    public static final int CS_WK52LOW_ENABLED = 138;
    public static final int CS_WK52HIGH_ENABLED = 139;


    public static final String VC_FIELD_DELIMETER = "=";
    public static final String VC_RECORD_DELIMETER = ";";
    public static final String VC_DATA_SEPERATOR = "$";

    public static final int WINDOW_BOARD = 0;
    public static final int WINDOW_PORTFOLIO = 1;
    public static final int WINDOW_CLASSIC = 2;

    public static final int SELECTED_SYMBOL = 200;
    public static final int FIRST_SPLIT_LOCATION = 201;
    public static final int SECOND_SPLIT_LOCATION = 202;
    public static final int SUB_WINDOW_TYPES = 203;
    public static final int SUB_WINDOW_VIEWS = 204;

    public static final int VC_CUSTOMIZED_DATA = 125;
    public static final String SM_START_REC    = "^";
    public static final String SM_FIELD_DELIM  = ",";
    public static final int VC_OPTION_NEAR_THE_MONEY= 35;


    //for radar screen window
    public static final int SELECTED_EXCHANGE = 999;
    public static final int SELECTED_EXCHANGE_INDEX = 998;
    public static final int SELECTED_INTERVAL = 997;
    public static final int SELECTED_TAB_INDEX = 996;

    public static final int MINI_TRADE_PARAM = 205;
    public static final int ANN_SELECTED_EXG = 311;
}