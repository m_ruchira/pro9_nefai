package com.isi.csvr.sideBar;

import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreeModel;
import javax.swing.tree.TreePath;
import javax.swing.event.TreeModelListener;
import java.util.Hashtable;
import java.util.Vector;

/**
 * Created by IntelliJ IDEA. User: chandika Date: 26-Mar-2008 Time: 14:58:01 To change this template use File | Settings
 * | File Templates.
 */
public class SearchTreeModel implements TreeModel {
     private ExchangeNode map;
    public SearchTreeModel(ExchangeNode map){
        this.map=map;
    }
    public Object getRoot() {
        return map;
    }


    public Object getChild(Object parent, int index) {
        return ((ExchangeNode)parent).getChild(index);

    }

    public int getChildCount(Object parent) {
        return ((ExchangeNode)parent).getChildCount();

    }

    public boolean isLeaf(Object node) {
       if(node instanceof ExchangeNode){
            return false;
        }else {
           return true;
       }

    }

    public void valueForPathChanged(TreePath path, Object newValue) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public int getIndexOfChild(Object parent, Object child) {
        return ((ExchangeNode)parent).getIndex(child);
    }

    public void addTreeModelListener(TreeModelListener l) {
    }

    public void removeTreeModelListener(TreeModelListener l) {
    }
}
