package com.isi.csvr.volumewatcher;

import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.TWDateFormat;
import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.history.HistoryFilesManager;
import com.isi.csvr.downloader.HistoryDownloadListner;
import com.isi.csvr.downloader.HistoryDownloadManager;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ExchangeListener;

import java.util.*;
import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith 
 * Date: Feb 6, 2008
 * Time: 9:40:07 AM
 */

/**
 * CLASS: VolumeWatcher
 * PURPOSE :  at the start (constructor ) calulate avgvolume for selected exchanges
 * : fire check triggable symbols
 */

//
public class VolumeWatcher implements HistoryDownloadListner, Runnable, ApplicationListener, ExchangeListener {

    double triggerPercentage = 10;

    //keep exchange wise information
    Hashtable<String, ExchangeInfo> hashExInfo;

    private static VolumeWatcher selfRef = null;

    //********************excchange wise customizations *****************
    final static int TRIGGER_BY_VOLUME = 0; //wherther volume  or last trade etc
    final static int TRIGGER_BY_NOF_TRADES = 1;
    final int HISTORY_VOLUME_INDEX = 8;
    final int HISTORY_NO_TRADES_INDEX = 10;
    public static final int TRIGGER_BY_PERCENTAGE = 0;
    public static final int TRIGGER_BY_QUNTITIY = 1;

    int iNOOF_EXCHANGES = 3; //using customizing panel selected exhanges


    private Hashtable<String, Long> hashSymbolAvgVolumes;
    private Hashtable<String, Long> hashSymbolAvgNoTrade;
    private Thread thread;
    private VolumeWatcherWindow ui;

    public boolean isBThreadDone() {
        return bThreadStop;
    }

    public void setBThreadStop(boolean bThreadDone) {
        this.bThreadStop = bThreadDone;
    }

    private boolean bThreadStop = false;

    public VolumeWatcher(VolumeWatcherWindow ui) {
        this.ui = ui;
        hashExInfo = new Hashtable<String, ExchangeInfo>();
        loadSettings();

        //data Strores
        hashSymbolAvgVolumes = new Hashtable<String, Long>();
        hashSymbolAvgNoTrade = new Hashtable<String, Long>();

        // history download listner added
        HistoryDownloadManager.getSharedInstance().addHistoryDownloadListner(this);
        Application.getInstance().addApplicationListener(this);
        ExchangeStore.getSharedInstance().addExchangeListener(this);
    }

    public synchronized void start() {
        thread = new Thread(this, "volume watcher");
        thread.setPriority(Thread.MIN_PRIORITY);
        thread.start();
    }

    public synchronized void stopCurrentThread() {
        thread.interrupt();
    }

    private void loadSettings() {
        boolean bIsFilefound = true;
        BufferedReader oIn = null;

        try {
            oIn = new BufferedReader(new FileReader("DataStore\\VolumeWatchSeetings.txt"));
        } catch (FileNotFoundException e) {
            bIsFilefound = false;
        }

        if (bIsFilefound) {
            try {
                String sSetting = null;
                int iIndex = 0;
                while (true) {
                    sSetting = oIn.readLine();
                    if (sSetting == null) break;
                    if (sSetting.trim().equals("")) continue;
                    if (sSetting.trim().startsWith("#")) continue;
                    tokenize(sSetting, iIndex);
                    iIndex++;
                }
                oIn.close();
                oIn = null;
            } catch (Exception e) {
                e.printStackTrace();
                // to do error handeling
            }
        }
    }


    public void tokenize(String line, int iIndex) {
        StringTokenizer oTokens = new StringTokenizer(line, "|");

        String exchangeName = oTokens.nextToken();
        ExchangeInfo exInfo = new ExchangeInfo(exchangeName);

        int triggerUnit = Integer.parseInt(oTokens.nextToken());
        exInfo.setTriggerUnit(triggerUnit);
        int triggerMethod = Integer.parseInt(oTokens.nextToken());
        exInfo.setTriggerMethod(triggerMethod);
        Long triggerAmount = Long.parseLong(oTokens.nextToken());
        exInfo.setTriggerAmount(triggerAmount);
        int iIsExchEanble = Integer.parseInt(oTokens.nextToken());
        boolean bIsExchEanble = false;

        if (iIsExchEanble == 1) {
            bIsExchEanble = true;
        }
        exInfo.setCustomizationEnable(bIsExchEanble);
        hashExInfo.put(exchangeName, exInfo);
    }

    public void run() {
        try {
            //run once
            while (!Application.isLoaded()) {
                Thread.sleep(1000);
            }
            Thread.sleep(10000); // wait for 10 sec before starting the thread

            while (true) {
                analyseVolumes();
                Thread.sleep(10000);
            }
        } catch (Exception e) {

        }
    }


    public void analyseVolumes() {

        //selected exchanges
        Enumeration eExchanges = hashExInfo.keys();

        while (eExchanges.hasMoreElements()) {
            String sExchName = (String) eExchanges.nextElement();
            ExchangeInfo exInfo = hashExInfo.get(sExchName);
            List<VolumeWatcherData> aTriggersymbol = exInfo.getTriggersymbolArray();
            List<VolumeWatcherData> aTempTriggersymbol = exInfo.getTempTriggersymbolArray();
            aTempTriggersymbol.clear();//again clear to reduce errors

            if (exInfo.isCustomizationEnable()) {
                if (!exInfo.isDataLoaded()) {
                    prepareAvgVolumes(sExchName);
                }

                Hashtable<String, Stock> hashSymbolStock = DataStore.getSharedInstance().getExchangeSymbolStore(sExchName);
                int symbolNumber = 0;
                for (Enumeration<String> e = hashSymbolStock.keys(); e.hasMoreElements();) { //go throgh symbols

                    synchronized (exInfo) {
                        long lAvgVolume = 0l;
                        long lAvgNOOfTrades = 0l;
                        long lCurrentVolume = 0l;
                        long lCurrentNoOfTrades = 0l;
                        boolean bIsTriggable = false;
                        String data = e.nextElement();
                        String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                        int instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                        Stock symbolStock = hashSymbolStock.get(symbol);
                        String key = SharedMethods.getKey(sExchName, symbol, instrument);

                        if (exInfo.getTriggerUnit() == TRIGGER_BY_VOLUME) {
//                            System.out.println(sExchName + " : triggerByVolume:   " + SharedMethods.getKey(sExchName, symbol));
                            if (hashSymbolAvgVolumes.containsKey(key)) {
                                lAvgVolume = hashSymbolAvgVolumes.get(key);
                            } else {
                                continue;
                            }

                            lCurrentVolume = symbolStock.getVolume();
//                            System.out.println(symbol + " " + lAvgVolume + " " + lCurrentVolume);
                            bIsTriggable = isSymbolTriggable(lAvgVolume, lCurrentVolume, exInfo);
                            if (bIsTriggable) {
                                String symbolDescription = symbolStock.getLongDescription();
                                String sExchange = symbolStock.getExchange();
                                VolumeWatcherData vWData = new VolumeWatcherData(symbol, symbolDescription, lAvgVolume, lCurrentVolume, sExchange);
//                                System.out.println("trigger - volume >>>>>>");
                                aTempTriggersymbol.add(vWData);
                            }
                        } else if (exInfo.getTriggerUnit() == TRIGGER_BY_NOF_TRADES) {    //no of trades

                            if (hashSymbolAvgNoTrade.containsKey(key)) {
                                lAvgNOOfTrades = hashSymbolAvgNoTrade.get(key);
                            } else {
                                continue;
                            }
                            lCurrentNoOfTrades = symbolStock.getNoOfTrades();
                            bIsTriggable = isSymbolTriggable(lAvgNOOfTrades, lCurrentNoOfTrades, exInfo);
//                            System.out.println("analyse by no of trades for = " + sExchName + "/" + symbol);
                            if (bIsTriggable) {
//                                System.out.println("trigger - noOf trades>>>>>>");
                                String symbolDescription = symbolStock.getLongDescription();
                                String sExchange = symbolStock.getExchange();
                                VolumeWatcherData vWData = new VolumeWatcherData(symbol, symbolDescription, lAvgNOOfTrades, lCurrentNoOfTrades, sExchange);
                                aTempTriggersymbol.add(vWData);
                            }

                        }
                    }//synchronize

                    symbolNumber++;
                    if (symbolNumber % 10 == 0) {
                        try {
                            Thread.sleep(100);
                        } catch (InterruptedException ex) {

                        }
                    }
                }
                // hashExTriggeredSymbol.clear();
                aTriggersymbol.clear();
                for (int i = 0; i < aTempTriggersymbol.size(); i++) {
                    aTriggersymbol.add(aTempTriggersymbol.get(i));
                }
                aTempTriggersymbol.clear();//clear after copying
            }
        }
    }

    //can be genarilized more  can be either volume or ltd or anyother
    private boolean isSymbolTriggable(long lAvgValue, long lCurrentValue, ExchangeInfo exInfo) {
        //check condition
        boolean bIsTriggable = false;
        if (exInfo.getTriggerMethod() == TRIGGER_BY_PERCENTAGE) {
            try {
                if (lCurrentValue > (lAvgValue * ((100f + exInfo.getTriggerAmount()) / 100f))) {
                    bIsTriggable = true;
                }
            } catch (Exception e) {
                bIsTriggable = false;
            }
        } else if (exInfo.getTriggerMethod() == TRIGGER_BY_QUNTITIY) {
            if (lCurrentValue > (lAvgValue + exInfo.getTriggerAmount())) {
                bIsTriggable = true;
            }
        }
        return bIsTriggable;
    }


    //    todo - get last 10 records   -if given date range is not enough
    public Vector getRecentHistoryRecords(String sSymbol, String sExchange, Stock symbStock) {  //key = symbol + ~ + exchange

        final int iConcernEntriesForAvg = 10;
        final int iNOOF_DATES_LOOKING = -15;
        long lAvgVolume = 0l;

        Vector histrecords = new Vector();
        TWDateFormat formatter = new TWDateFormat("yyyyMMdd");
        String toDate = formatter.format(new Date(symbStock.getLastTradedDate()));

        //define a date range at first time
        Calendar c1 = Calendar.getInstance();     //new GregorianCalendar();
        Date startFromDate = new Date(symbStock.getLastTradedDate());
        final int MaxIterations = 4; //but atmost 4 iterations
        int iNOOfItr = 0;
        c1.setTime(startFromDate);     //syubstract 15 datys
        c1.add(Calendar.DATE, iNOOF_DATES_LOOKING);
        Date dFromDate = c1.getTime();
        String fromDate = formatter.format(dFromDate);
        histrecords = HistoryFilesManager.readHistoryData(sSymbol, sExchange, fromDate, toDate);

        if (histrecords == null) {
            c1.setTime(dFromDate);     //syubstract 15 datys
            c1.add(Calendar.DATE, iNOOF_DATES_LOOKING);
            fromDate = formatter.format(c1.getTime());
            histrecords = HistoryFilesManager.readHistoryData(sSymbol, sExchange, fromDate, toDate);

        } else {

            if (histrecords.size() < 10) {
                c1.setTime(dFromDate);     //syubstract 15 datys
                c1.add(Calendar.DATE, iNOOF_DATES_LOOKING);
                fromDate = formatter.format(c1.getTime());
                histrecords = HistoryFilesManager.readHistoryData(sSymbol, sExchange, fromDate, toDate);
            }
        }
        return histrecords;

    }

    private long calculateAvg(Vector histrecords, int unitIndex) {

        long lAvg = 0l;
        long lTotal = 0l;

        int iVolCalcStart = 0;
        if (histrecords.size() > 10) {
            iVolCalcStart = histrecords.size() - 10;
        } else {
            iVolCalcStart = 0;
        }


        for (int j = iVolCalcStart; j < histrecords.size(); j++) {
            String histRecord = (String) histrecords.elementAt(j);
            String[] asHistentires = histRecord.split(Meta.FS);
            TWDateFormat formatter = new TWDateFormat("yyyyMMdd");
            try {

                long unitHistoryValue = Long.parseLong(asHistentires[unitIndex]); //date
                lTotal = lTotal + unitHistoryValue;

            } catch (Exception e) {
                e.printStackTrace();
            }

        }

        //avg volume
        if (histrecords.size() < 10) {
            if (histrecords.size() == 0) {
                lAvg = 0l;
            } else {
                lAvg = lTotal / histrecords.size();
            }

        } else {       //if  > 10 avg volume calculate for 10 records
            lAvg = lTotal / 10;
        }

        return lAvg;
    }


    //this should be called at the startup as well as when new history download complete event happened.
    public synchronized void prepareAvgVolumes(String exchange) {
        hashSymbolAvgVolumes.clear();
        hashSymbolAvgNoTrade.clear();
//        Enumeration eExchanges = hashExInfo.keys();
//        ExchangeInfo exchangeInfo = hashExInfo.get(exchange);
        //System.out.println("prepareavg" +);
        //while (eExchanges.hasMoreElements()) {
//            String sExchName = (String) eExchanges.nextElement();
        ExchangeInfo exInfo = hashExInfo.get(exchange);
        System.out.println("prepareavg" + exchange);
        Hashtable<String, Stock> hashSymbolStock = DataStore.getSharedInstance().getExchangeSymbolStore(exchange);

        for (Enumeration e = hashSymbolStock.keys(); e.hasMoreElements();) { //go throgh symbols
//            String symbol = (String) e.nextElement();
            String data =(String) e.nextElement();
            String symbol = SharedMethods.getSymbolFromExchangeKey(data);
            int instrument = SharedMethods.getInstrumentFromExchangeKey(data);
            Stock symbolStock = hashSymbolStock.get(symbol);
            //get last histroy records
            Vector histrecords = getRecentHistoryRecords(symbol, exchange, symbolStock);
            String key = SharedMethods.getKey(exchange, symbol, instrument);

            //if ony histoyr is avilable check for volume sudden changes
            if (histrecords == null) {
                continue; //if histroy unavilable check for next symbol
            }

            //calculate avg volume
            long lAvgVolume = 0l;
            long lAvNOOfTrades = 0l;

            lAvgVolume = calculateAvg(histrecords, HISTORY_VOLUME_INDEX);
            hashSymbolAvgVolumes.put(key, lAvgVolume);

            lAvNOOfTrades = calculateAvg(histrecords, HISTORY_NO_TRADES_INDEX);
            hashSymbolAvgNoTrade.put(key, lAvNOOfTrades);

            histrecords = null;//get symbol sotck

            try {
                Thread.sleep(5);
            } catch (InterruptedException ex) {
                ex.printStackTrace();
            }
        }
        testAvgHashTable();
        exInfo.setDataLoaded(true);

    }

    private void testAvgHashTable() {
        //test hashtable
        for (Enumeration e = hashSymbolAvgVolumes.keys(); e.hasMoreElements();) {
            String sSymbol = (String) e.nextElement();
            System.out.println("symbol:  " + sSymbol + "avgVolume: " + hashSymbolAvgVolumes.get(sSymbol));
        }
    }


    public void historyDownloaded(String exchange) {
        System.out.println("fire history downloaded & prepare avg volume");
        prepareAvgVolumes(exchange);

    }

    public void historicalIntradayDownload(String exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public Hashtable<String, ExchangeInfo> getHashExInfo() {
        return hashExInfo;
    }

    public void setHashExInfo(Hashtable<String, ExchangeInfo> hashExInfo) {
        this.hashExInfo = hashExInfo;
    }


    public Hashtable<String, Long> getHashSymbolAvgVolumes() {
        return hashSymbolAvgVolumes;
    }

    public void setHashSymbolAvgVolumes(Hashtable<String, Long> hashSymbolAvgVolumes) {
        this.hashSymbolAvgVolumes = hashSymbolAvgVolumes;
    }

    public void save() {

        Enumeration eExchanges = hashExInfo.keys();
        try {
            FileOutputStream fExchSettings = new FileOutputStream("DataStore\\VolumeWatchSeetings.txt");

            while (eExchanges.hasMoreElements()) {
                String toExchangeSettFile = "";
                String sExchName = (String) eExchanges.nextElement();
                ExchangeInfo exInfo = (ExchangeInfo) hashExInfo.get(sExchName);

                int iEnableDisable = 0;

                if (exInfo.isCustomizationEnable()) {
                    iEnableDisable = 1;
                } else {
                    iEnableDisable = 0;
                }

                toExchangeSettFile = sExchName + "|" + exInfo.getTriggerUnit() + "|" + exInfo.getTriggerMethod() +
                        "|" + exInfo.getTriggerAmount() + "|" + iEnableDisable + "|";
                //enumeration
                new PrintStream(fExchSettings).println(toExchangeSettFile);
            }
            fExchSettings.close();
        } catch (FileNotFoundException ex) {
            ex.printStackTrace();
        } catch (IOException eIO) {
            eIO.printStackTrace();
        }
    }

    public void exchangeAdded(Exchange exchange) {
        if (exchange.isDefault() && (!hashExInfo.contains(exchange.getSymbol()))) {
            String exhangeSymbol = exchange.getSymbol();
            ExchangeInfo exInfo = new ExchangeInfo(exhangeSymbol);
            exInfo.setTriggerUnit(0);
            exInfo.setTriggerMethod(0);
            exInfo.setTriggerAmount(0);
            exInfo.setCustomizationEnable(false);
            hashExInfo.put(exhangeSymbol, exInfo);
            ui.addTab(exInfo);
        }
    }

    public void exchangeCurrencyChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void exchangeDowngraded(Exchange exchange) {
        if (hashExInfo.containsKey(exchange.getSymbol())) {
            ExchangeInfo exInfo = new ExchangeInfo(exchange.getSymbol());
            hashExInfo.remove(exchange.getSymbol());
            ui.removeTab(exInfo);
        }
    }

    public void exchangeInformationTypesChanged() {
    }

    public void exchangeMasterFileLoaded(String exchange) {
    }

    public void exchangeMustInitialize(Exchange exchange,long newDate, long oldDate) {
    }

    public void exchangeRemoved(Exchange exchange) {
        if (hashExInfo.containsKey(exchange.getSymbol())) {
            ExchangeInfo exInfo = new ExchangeInfo(exchange.getSymbol());
            hashExInfo.remove(exchange.getSymbol());
            ui.removeTab(exInfo);
        }
    }

    public void exchangesAdded(boolean offlineMode) {
    }

    public void exchangesLoaded() {
    }

    public void exchangeTimeZoneChanged(Exchange exchange) {
    }

    public void exchangeTradingInformationTypesChanged() {
    }

    public void marketStatsChanged(int oldStatus, Exchange exchange) {
    }

    public void exchangeUpgraded(Exchange exchange) {
    }

    public void applicationExiting() {
    }

    public void applicationLoaded() {
//        this.start();
    }

    public void applicationLoading(int percentage) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationReadyForTransactions() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void applicationTimeZoneChanged(TimeZone zone) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void selectedExchangeChanged(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingEnded(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void loadOfflineData() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void snapshotProcessingStarted(Exchange exchange) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceLoaded() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceSaved() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillLoad() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void workspaceWillSave() {
        //To change body of implemented methods use File | Settings | File Templates.
    }
}
