package com.isi.csvr.volumewatcher;

import com.isi.csvr.plugin.PluginShared;
import com.isi.csvr.print.PrintManager;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.Table;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Feb 24, 2008
 * Time: 11:52:42 PM
 * To change this template use File | Settings | File Templates.
 */
public class ExchangeInfoGUI {

    //exchange name
    private String exchangeName = "";

    JPanel wholeExpnl;

    //lable unit (volume , LTD)
    private  JLabel lblTriggerUNit;
    private  JLabel lblUnitMethodGap;
    //lale TRIGGER  method  PERCENTAGE,quantity
    private  JLabel lblTriggerMethod;
    private  JLabel lblMethodButtonGap;


    private ArrayList<TWComboItem> listTriggerUnit;
    private ArrayList<TWComboItem> listTUnitMethod;

    //combo trigger unit
    public  TWComboBox cmbTriggerTypeUnit;
    //combo trigger method
    public TWComboBox cmbTriggerMethod;
    //text box trigger amount
    public  TWTextField txtTriggerAmount;
    // btnCustomActivate;

    //TWImageLabel lblExchangeRunStatus;
    public  JButton btnStatus;
    public  JButton btnApplySettings;


    //back end data store
    ExchangeInfo exInfo;

    //table
    Table volumeWatchTable;
    // tble model
    VolumeWatchModel vWModel;

    //view settings
    ViewSetting volWatchSettings;
    //constructor
    VolumeWatcherWindow parent;


    public JPanel getExInfoPnl() {
        return exInfoPnl;
    }

    public void setExInfoPnl(JPanel exInfoPnl) {
        this.exInfoPnl = exInfoPnl;
    }//contian exhange Infomation

    JPanel exInfoPnl;

    public JPanel getWholeExpnl() {
        return wholeExpnl;
    }

    public void setWholeExpnl(JPanel wholeExpnl) {
        this.wholeExpnl = wholeExpnl;
    }

    public ExchangeInfoGUI(String exchangeName, ExchangeInfo exInfoIn, ViewSetting volWatchSettingsIn, VolumeWatcherWindow parent) {
        this.exchangeName = exchangeName;
        this.exInfo = exInfoIn;
        this.volWatchSettings = volWatchSettingsIn;
        this.parent = parent;
        createVolumeWatchTable();
        createExchnageInfoPane();
        wholeExpnl = new JPanel();
        wholeExpnl.setLayout(new BorderLayout());
        wholeExpnl.add(volumeWatchTable, BorderLayout.CENTER);
        wholeExpnl.add(exInfoPnl, BorderLayout.NORTH);
        wholeExpnl.putClientProperty("id", exchangeName);


    }
    //get table

    //get exchange info panel

    public JPanel getExchangeInfoPanel() {
        return exInfoPnl;
    }


    public void createExchnageInfoPane() {
        exInfoPnl = new JPanel();
        //exInfoPnl.setPreferredSize(new Dimension(550, 30));

        listTriggerUnit = new ArrayList<TWComboItem>(); //initialize each individually
        listTUnitMethod = new ArrayList<TWComboItem>(); //initialize each individually

        listTriggerUnit.add(new TWComboItem(0, Language.getString("VOLUME")));
        listTriggerUnit.add(new TWComboItem(1, Language.getString("NO_OF_TRADES")));


        lblTriggerUNit = new JLabel(Language.getString("TYPE"));
        lblTriggerUNit.setPreferredSize(new Dimension(lblTriggerUNit.getWidth(), 10));
        listTUnitMethod.add(new TWComboItem(0, Language.getString("TRIGGER_PERCENTAGE")));

        lblUnitMethodGap = new JLabel("");

        lblTriggerMethod = new JLabel(Language.getString("TRIGGER_UNIT"));
        listTUnitMethod.add(new TWComboItem(1, Language.getString("TRIGGER_QUANTITY")));

        lblMethodButtonGap = new JLabel("");
        // combo box for Trigger type
        cmbTriggerTypeUnit = new TWComboBox(new TWComboModel(listTriggerUnit));


        cmbTriggerTypeUnit.setSelectedIndex(0);

        cmbTriggerMethod = new TWComboBox(new TWComboModel(listTUnitMethod));

        cmbTriggerMethod.setSelectedIndex(0);

        txtTriggerAmount = new TWTextField();
        txtTriggerAmount.addFocusListener(parent);


        txtTriggerAmount.setText(Long.toString(exInfo.getTriggerAmount()));
//            //btnCustomActivate[i] =  new JButton("Enable");//todo get from language file


        cmbTriggerTypeUnit.setSelectedIndex(exInfo.triggerUnit);
        cmbTriggerMethod.setSelectedIndex(exInfo.triggerMethod);
        txtTriggerAmount.setText(Long.toString(exInfo.getTriggerAmount()));
//            }else { //false
//                cmbTriggerTypeUnit.setSelectedIndex(0);
//                cmbTriggerMethod.setSelectedIndex(0);
//                txtTriggerAmount.setText("");
//            }

        btnApplySettings = new JButton(Language.getString("APPLY"));
        btnApplySettings.setPreferredSize(new Dimension(100, 1));

        btnApplySettings.setEnabled(false);
        btnApplySettings.addActionListener(parent);


        cmbTriggerTypeUnit.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent e) {
                btnApplySettings.setEnabled(true);
            }
        });

        cmbTriggerMethod.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent e) {
                btnApplySettings.setEnabled(true);


            }
        });

//        lblExchangeRunStatus = new TWImageLabel();
        btnStatus = new JButton();
        if (exInfo.isCustomizationEnable() == true) {
            exchangeActSettings();
        } else {
            exchangeInActSettings();
        }
//            btnToggleExAct.setPreferredSize(new Dimension(100,25 ) );
        // btnToggleCustomActivate[i].setBorder(new EmptyBorder(0, 0, 0, 0));
        btnStatus.addMouseListener(parent);
        btnStatus.addActionListener(parent);

        //set the layout for cusotmize panel
        String[] widths = {"6%", "19%", "1%", "6%", "15%", "17%", "15%", "4%", "15%"};//, "100", "40"};

        String[] heights = {"20"};
        exInfoPnl.setLayout(new FlexGridLayout(widths, heights, 3, 3));
        //  String[] widths = {"25%", "25%", "25%","25%"};
        // exInfoPnl.setLayout(new FlexFlowLayout(widths)); //new GridLayout(2, 1));
        exInfoPnl.add(lblTriggerUNit);
        exInfoPnl.add(cmbTriggerTypeUnit);
        exInfoPnl.add(lblUnitMethodGap);

        exInfoPnl.add(lblTriggerMethod);
        exInfoPnl.add(txtTriggerAmount);
        exInfoPnl.add(cmbTriggerMethod);

        //CustomExchangeinfoPanels[i].add(  //btnCustomActivate[i] );
        exInfoPnl.add(btnApplySettings);
        exInfoPnl.add(lblMethodButtonGap);
        exInfoPnl.add(btnStatus);
        //exInfoPnl.add( lblExchangeRunStatus);

        exInfoPnl.setBorder(new EmptyBorder(5, 0, 5, 0));

        //items

        //set initial data from loaded exchange info object
    }

    public void exchangeInActSettings() {
        btnStatus.setText(Language.getString("START"));
        btnStatus.setToolTipText(Language.getString("TIP_START_SCAN"));

        //lblExchangeRunStatus.setImageFileName("red_bulb.gif");
//        lblExchangeRunStatus.setToolTipText(Language.getString("SET_SCAN_ENABLE"));


    }

    public void exchangeActSettings() {
        btnStatus.setText(Language.getString("STOP"));

//        lblExchangeRunStatus.setImageFileName("green_bulb.gif");
        btnStatus.setToolTipText(Language.getString("TIP_STOP_SCAN"));

    }

    //create table
    public void createVolumeWatchTable() {
        vWModel = new VolumeWatchModel(exInfo.getTriggersymbolArray());
        vWModel.setViewSettings(volWatchSettings);
        volumeWatchTable = new Table();
        volumeWatchTable.setSortingEnabled();
//        volumeWatchTable.setWindowType(ViewSettingsManager.VOLUME_WATCHER);
        volumeWatchTable.setModel(vWModel);
        vWModel.setTable(volumeWatchTable);
        PluginShared.setRenderer(volumeWatchTable, new VolumeWatchRenderer(vWModel.getRendIDs()));
        volumeWatchTable.getPopup().setPrintTarget(volumeWatchTable.getTable(), PrintManager.PRINT_JTABLE);
        volumeWatchTable.getPopup().enableUnsort(false);
        volumeWatchTable.getScrollPane().setBorder(BorderFactory.createEmptyBorder());
        volumeWatchTable.getModel().updateGUI();
        GUISettings.applyOrientation(volumeWatchTable.getPopup());
        //set model

    }

    //get table
    public Table getVolumeWatchTable() {
        return volumeWatchTable;
    }


}
