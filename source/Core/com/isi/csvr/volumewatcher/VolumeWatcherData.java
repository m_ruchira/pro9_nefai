package com.isi.csvr.volumewatcher;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Feb 6, 2008
 * Time: 5:10:35 PM
 * To change this template use File | Settings | File Templates.
 */
public class VolumeWatcherData {
    private String symbol = "";
    private String symbDescription = "";
    private long avgVolume;
    private long currentVolume;
    private String sExchange;

    public String getExchange() {
        return sExchange;
    }

    public void setExchange(String sExchange) {
        this.sExchange = sExchange;
    }

    public VolumeWatcherData(String symbol, String symbDescription, long avgVolume, long currentVolume, String sExchange) {
        this.symbol = symbol;
        this.symbDescription = symbDescription;
        this.avgVolume = avgVolume;
        this.currentVolume = currentVolume;
        this.sExchange = sExchange;

    }


    public long getAvgVolume() {
        return avgVolume;
    }

    public long getCurrentVolume() {
        return currentVolume;
    }

    public String getSymbDescription() {
        return symbDescription;
    }

    public String getSymbol() {
        return symbol;
    }

    public void setAvgVolume(long avgVolume) {
        this.avgVolume = avgVolume;
    }

    public void setCurrentVolume(long currentVolume) {
        this.currentVolume = currentVolume;
    }

    public void setSymbDescription(String symbDescription) {
        this.symbDescription = symbDescription;
    }

    public void setSymbol(String symbol) {
        this.symbol = symbol;
    }


}
