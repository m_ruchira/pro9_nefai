package com.isi.csvr.volumewatcher;


import com.isi.csvr.Client;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.util.Enumeration;
import java.util.Hashtable;
import java.util.List;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Feb 6, 2008
 * Time: 9:40:45 AM
 */


public class VolumeWatcherWindow extends InternalFrame
        implements ActionListener, MouseListener, FocusListener {

    private Hashtable<String, ExchangeInfoGUI> hashExchGUI = new Hashtable<String, ExchangeInfoGUI>();
    private ViewSetting volWatchSettings;
    private VolumeWatcher vWManger;
    private int iNOOF_EXCHANGES = 3;
    private TWTabbedPane exchangTabPane;

    public VolumeWatcherWindow() {
        super();
        setLayer(GUISettings.TOP_LAYER);
    }

    public void load(Object[]... params) {

        //back end functions
        vWManger = new VolumeWatcher(this);


//        this.setFont(new Font("Arial", Font.BOLD, 12));
        this.setTitle(Language.getString("VOLUME_WATCHER"));
        this.setSize(new Dimension(500, 700));
        this.setClosable(true);
        this.setIconifiable(true);
        this.setResizable(true);
        Client.getInstance().getDesktop().add(this);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());


        createVolWatchGUI();
        //start prepare volume and comparing for filling the table
        vWManger.start();
        this.getContentPane().setLayout(new BorderLayout());
        this.getContentPane().add(exchangTabPane, BorderLayout.CENTER);
        GUISettings.applyOrientation(this);
        setDefaultCloseOperation(HIDE_ON_CLOSE);


    }

    public void save(Object[]... params) {
        vWManger.save();
    }

    public boolean isLoaded() {
        return true;  
    }

    public void createVolWatchGUI() {

        //todo
        try {
            // Read the default view in the view settings file
            // get the default view

            volWatchSettings = ViewSettingsManager.getSummaryView("VOLUME_WATCH");
            if (volWatchSettings == null)
                throw (new Exception("View not found"));
        } catch (Exception e) {
            e.printStackTrace();
            return;
        }

        GUISettings.setColumnSettings(volWatchSettings, TWColumnSettings.getItem("VOLUME_WATCHC_COLS"));
//        volWatchSettings.setTableNumber(1);
        volWatchSettings.setParent(this);
        setViewSetting(volWatchSettings);
//        setTable();
        //get back end data
        Hashtable<String, ExchangeInfo> oHashExchInfo = vWManger.getHashExInfo();
        exchangTabPane = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute,"vw");
        Enumeration<ExchangeInfo> eExchanges = oHashExchInfo.elements();

        while (eExchanges.hasMoreElements()) {
            addTab(eExchanges.nextElement());
        }
    }

    public void addTab(ExchangeInfo exchangeInfo){
        String sExchName = exchangeInfo.getId();
        ViewSetting viewSetting = volWatchSettings.getObject(); // clone the original
        viewSetting.setID("VOLUME_WATCH_" + sExchName);
        ViewSettingsManager.getSummaryViews().put(viewSetting.getID(), viewSetting);
        ExchangeInfoGUI exchGUI = new ExchangeInfoGUI(sExchName, exchangeInfo, viewSetting, this);
        exchangTabPane.addTab(sExchName, exchGUI.getWholeExpnl());
        addTable(viewSetting.getID(), exchGUI.getVolumeWatchTable());
        hashExchGUI.put(sExchName, exchGUI);
    }

    public void removeTab(ExchangeInfo exchangeInfo){
        try {
            for (int i = 0; i <exchangTabPane.getTabCount(); i++) {
                Component component = exchangTabPane.getComponentAt(i);
                if (((JPanel)component).getClientProperty("id").equals(exchangeInfo.getId())){
                    exchangTabPane.removeTab(i);
                    break;
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public JTable getTable1() {
        Table temp = new Table();
        return temp.getTable();
    }

    public void actionPerformed(ActionEvent e) {
        Hashtable oHashExchInfo = vWManger.getHashExInfo();
        Enumeration eExchanges = oHashExchInfo.keys();

        while (eExchanges.hasMoreElements()) {
            String sExchName = (String) eExchanges.nextElement();
            ExchangeInfo exInfo = (ExchangeInfo) oHashExchInfo.get(sExchName);
            ExchangeInfoGUI exGUI = (ExchangeInfoGUI) hashExchGUI.get(sExchName);

            if (e.getSource() == exGUI.btnApplySettings) {
                setCustomizedData(exInfo, exGUI);
                exGUI.btnApplySettings.setEnabled(false);
                //more to do for the table

                //clear the exsiting triggered data
                //Hashtable<String, VolumeWatcherData> hashExTriggeredSymbol =  exInfo.getTrigeredSymbolsHash();
                List<VolumeWatcherData> aTriggersymbol = exInfo.getTriggersymbolArray();

                //hashExTriggeredSymbol.clear();
                aTriggersymbol.clear();
                List<VolumeWatcherData> aTempTriggersymbol = exInfo.getTempTriggersymbolArray();

                //hashExTriggeredSymbol.clear();
                aTempTriggersymbol.clear();

                // if the middle of the thread analyse volumes
            }
            if (e.getSource() == exGUI.btnStatus) {
                boolean isExEnable = exInfo.isCustomizationEnable();
                if (isExEnable) {
                    exGUI.exchangeInActSettings();
                    exInfo.setCustomizationEnable(false);
                } else {
                    exGUI.exchangeActSettings();
                    exInfo.setCustomizationEnable(true);
                }
            }
        }
    }

    //mouse events

    public void mouseClicked(MouseEvent e) {
        Hashtable oHashExchInfo = vWManger.getHashExInfo();
        Enumeration eExchanges = oHashExchInfo.keys();

        while (eExchanges.hasMoreElements()) {
            String sExchName = (String) eExchanges.nextElement();
            ExchangeInfoGUI exGUI =  hashExchGUI.get(sExchName);

            if (e.getSource() == exGUI.cmbTriggerMethod) {
                exGUI.btnApplySettings.setEnabled(true);
            } else if (e.getSource() == exGUI.cmbTriggerTypeUnit) {
                exGUI.btnApplySettings.setEnabled(true);
            } else if (e.getSource() == exGUI.txtTriggerAmount) {
                exGUI.btnApplySettings.setEnabled(true);
            }
        }
    }

    private void resetCustomData(ExchangeInfo exInfo, ExchangeInfoGUI exGUI) {
        //set back end data from gui
        exInfo.setTriggerAmount(Long.parseLong(exGUI.txtTriggerAmount.getText()));
        exInfo.setTriggerMethod((exGUI.cmbTriggerMethod.getSelectedIndex()));
        exInfo.setTriggerUnit(exGUI.cmbTriggerTypeUnit.getSelectedIndex());
        exInfo.setCustomizationEnable(false);
    }

    private void setCustomizedData(ExchangeInfo exInfo, ExchangeInfoGUI exGUI) {

        //set back end data from gui
        exInfo.setTriggerAmount(Long.parseLong(exGUI.txtTriggerAmount.getText()));
        exInfo.setTriggerMethod((exGUI.cmbTriggerMethod.getSelectedIndex()));
        exInfo.setTriggerUnit(exGUI.cmbTriggerTypeUnit.getSelectedIndex());

    }

    public void mousePressed(MouseEvent e) {

    }

    public void mouseReleased(MouseEvent e) {

    }

    public void mouseEntered(MouseEvent e) {

    }

    public void mouseExited(MouseEvent e) {

    }


    public void focusGained(FocusEvent e) {
        Hashtable oHashExchInfo = vWManger.getHashExInfo();
        Enumeration eExchanges = oHashExchInfo.keys();

        while (eExchanges.hasMoreElements()) {
            String sExchName = (String) eExchanges.nextElement();
            ExchangeInfo exInfo = (ExchangeInfo) oHashExchInfo.get(sExchName);
            ExchangeInfoGUI exGUI = (ExchangeInfoGUI) hashExchGUI.get(sExchName);

            if (e.getSource() == exGUI.txtTriggerAmount) {
                exGUI.btnApplySettings.setEnabled(true);
            }
        }
    }

    public void focusLost(FocusEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

}