package com.isi.csvr.watchlist;

import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.datastore.ValidatedSymbols;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.shared.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Mar 7, 2003
 * Time: 12:19:25 PM
 * To change this template use Options | File Templates.
 */
public class WatchListStore extends Symbols {

    public static final byte NORMAL_LIST_TABLE_TYPE = 0;
    public static final byte MIST_LIST_TABLE_TYPE = 1;
    public static final byte CLASSIC_LIST_TABLE_TYPE = 1;
    public static final byte FOREX_TYPE_TYPE = 8;
    private byte listType;
    private int selectedType = 0;


    private String caption = null;
    private String id = null;
    private String captions = null;
    private String captionsByLanguage = null;

    public WatchListStore(byte symbolTypeIn) {
        super(symbolTypeIn);
    }

    public WatchListStore(String symbols) {
        super(symbols, true);
    }

    public String getCaption() {
        return caption;
    }

    public void setCaption(String caption) {
        this.caption = caption;
    }

    public String getCaptions() {
        return captions;
    }

    public void setCaptions(String captions) {
        if((this.caption != null) && !(this.caption.equals(""))){
            setCaption(filterCaption(UnicodeUtils.getNativeString(captions)));
            this.captions = UnicodeUtils.getNativeString(captions);
            WatchListManager.getInstance().fireWatchlistRenamed(id);
        } else {
            setCaption(filterCaption(UnicodeUtils.getNativeString(captions)));
            this.captions = UnicodeUtils.getNativeString(captions);
        }
    }
     public void setCaptionsBylanguage(String captions) {
        if((captions != null) && !(captions.equals(""))){
            setCaption(filterCaptionByLanguage(UnicodeUtils.getNativeString(captions)));
            captionsByLanguage=UnicodeUtils.getNativeString(captions);
            this.captions = UnicodeUtils.getNativeString(captions);
            WatchListManager.getInstance().fireWatchlistRenamed(id);
        }
    }
    public String getId() {
        return id;                                                           
    }

    public void setId(String id) {
        this.id = id;
    }

    public int getSelectedType() {
        return selectedType;
    }

    public void setSelectedType(int selectedType) {
        this.selectedType = selectedType;
    }

    public void setSymbols(String[] symbols) {
        super.clear();
        for (int i = 0; i < symbols.length; i++) {
            if (!symbols[i].trim().equals(Constants.NULL_STRING)){
                appendSymbol(symbols[i]); // apend the symbol without the instrument type
            }
        }
        WatchListManager.getInstance().fireSymbolAdded(null, id);
    }

    public synchronized boolean appendSymbol(String sSymbol) {
        boolean result = super.appendSymbol(sSymbol);
        WatchListManager.getInstance().fireSymbolAdded(sSymbol, id);
        return result;
    }

    public boolean insertSymbol(String sSymbol, int location) {
        boolean result = super.insertSymbol(sSymbol, location);
        WatchListManager.getInstance().fireSymbolAdded(sSymbol, id);
        return result;
    }

    public synchronized void removeSymbol(String sSymbol) {
        super.removeSymbol(sSymbol);
        WatchListManager.getInstance().fireSymbolRemoved(sSymbol, id);
    }

    /**
     * Returns the string representation of the store
     *
     * @return string representation of the store
     */
    public String toString() {
        StringBuffer tempString = new StringBuffer();
        //PortfolioRecord record = null;
        tempString.append(ViewConstants.VC_TYPE);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(listType);
        tempString.append(ViewConstants.VC_RECORD_DELIMETER);

        tempString.append(ViewConstants.VC_ID);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(id);
        tempString.append(ViewConstants.VC_RECORD_DELIMETER);

        tempString.append(ViewConstants.VC_CAPTIONS);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(UnicodeUtils.getUnicodeString(captions));
        tempString.append(ViewConstants.VC_RECORD_DELIMETER);

        tempString.append(ViewConstants.VC_SYMBOLS);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(super.toString());
        tempString.append(ViewConstants.VC_RECORD_DELIMETER);


        tempString.append(ViewConstants.VC_WATCHLIST_TYPE);
        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
        tempString.append(selectedType);
        tempString.append(ViewConstants.VC_RECORD_DELIMETER);
//        tempString.append(ViewConstants.VC_TITLE_CAPTION);
//        tempString.append(ViewConstants.VC_FIELD_DELIMETER);
//        tempString.append(UnicodeUtils.getUnicodeString(captionsByLanguage));
        return tempString.toString();
    }

    private String filterCaptionByLanguage(String caption) {
        try {
            String[] captions = caption.split("\\,");
            if (captions.length > 1) {
                String tag=Language.getSelectedLanguage();
                String match="";
                for(int i=0;i<captions.length;i++){
                    String stag=captions[i].split("\\:")[0];
                    if(tag.equals(stag)){
                       match= captions[i].split("\\:")[1];
                    }

                }
                return match;
            } else
                return caption;
        } catch (Exception e) {
            return caption;
        }
    }
    private String filterCaption(String caption) {
        try {
            String[] captions = caption.split("\\,");
            if (captions.length > 1) {
                return captions[Language.getLanguageID()];
            } else
                return caption;
        } catch (Exception e) {
            return caption;
        }
    }
    public byte getListType() {
        return listType;
    }

    public void setListType(byte listType) {
        this.listType = listType;
    }

}
