// Copyright (c) 2000 Integrated Systems International (ISI)
package com.isi.csvr;

import com.isi.csvr.alert.AlertFrame;
import com.isi.csvr.alert.AlertManager;
import com.isi.csvr.announcement.Announcement;
import com.isi.csvr.announcement.AnnouncementDownloadManager;
import com.isi.csvr.announcement.AnnouncementStore;
import com.isi.csvr.announcement.SearchedAnnouncementStore;
import com.isi.csvr.brokers.Broker;
import com.isi.csvr.chart.GraphFrame;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.companyprofile.CompanyProfileAnalyser;
import com.isi.csvr.datastore.*;
import com.isi.csvr.dde.DDEServer;
import com.isi.csvr.downloader.*;
import com.isi.csvr.event.Application;
import com.isi.csvr.event.ProcessListener;
import com.isi.csvr.history.HistorySettings;
import com.isi.csvr.marketdepth.DepthObject;
import com.isi.csvr.marketdepth.DepthStore;
import com.isi.csvr.marketdepth.OddLotStore;
import com.isi.csvr.news.News;
import com.isi.csvr.news.NewsStore;
import com.isi.csvr.news.SearchedNewsStore;
import com.isi.csvr.ohlc.IntraDayOHLC;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.radarscreen.RadarScreenInterface;
import com.isi.csvr.shared.*;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.ticker.custom.TickerObject;
import com.isi.csvr.ticker.custom.TradeFeeder;
import com.isi.csvr.ticker.SharedSettings;
import com.isi.csvr.ticker.advanced.*;
import com.isi.csvr.topstocks.TopStocksWindow;
import com.isi.csvr.trade.CompanyTradeStore;
import com.isi.csvr.trade.Trade;
import com.isi.csvr.trade.TradeStore;
import com.isi.csvr.trade.TradeSummaryStore;
import com.isi.csvr.tradebacklog.TradeBacklogStore;
import com.isi.csvr.trading.TradeMethods;
import com.isi.csvr.trading.datastore.Brokers;
import com.isi.csvr.trading.shared.TradingShared;
import com.isi.csvr.trading.ui.TransactionDialog;
import com.isi.csvr.trading.ui.orderentry.MiniTradingFrame;
import com.isi.csvr.wicalculator.WhatifCalculatorFrame;
import com.isi.csvr.ohlcbacklog.*;
import com.isi.csvr.forex.FXObject;
import com.isi.csvr.forex.ForexStore;
import com.isi.csvr.forex.ForexBoard;
import com.isi.csvr.financialcalender.*;
import com.isi.csvr.stockreport.*;
import com.isi.csvr.globalIndex.GlobalIndexObject;
import com.isi.csvr.options.*;
import com.isi.csvr.globalMarket.MarketSummaryLoader;
import com.isi.csvr.portfolio.PFImportDialogInterface;
import com.isi.csvr.portfolio.SplitFactorWindow;
import com.isi.csvr.bandwidthoptimizer.OptimizedTreeRenderer;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.plugin.*;
import com.isi.csvr.util.TWFileWriter;
import com.isi.csvr.smartalerts.SmartAlertManager;
import com.isi.csvr.smartalerts.SmartAlertWindow;
import com.isi.csvr.iframe.InternalFrame;

import javax.swing.*;
import static javax.swing.JOptionPane.INFORMATION_MESSAGE;
import java.util.*;
import java.util.List;
import java.io.*;
import java.awt.*;


/**
 * This class analyses incomming frames and invokes
 * method calls depending on the useage of the tag
 * <p/>
 *
 * @author Uditha Nagahawatta
 */
public class FrameAnalyser extends Thread implements ProcessListener {
    private List<String> receiveBuffer;
    private DataDisintegrator g_oDataDisintegrator;
    private String g_sTag;
    private int g_iTag;
    private String value;
    private MapTodaysTrades g_oTodaysTrades;
    private MessageList g_oMessages;
    private Trade g_oTrade;
    private Client g_oClient;
    public static long frameCount;
    TWFileWriter fileWriter = null;
//    boolean lastTradeUpdated;

    /**
     * Constructor
     */
    public FrameAnalyser(List<String> qReceiveBuffer) {
        super("FrameAnalyser");
        g_oMessages = MessageList.getSharedInstance();
        receiveBuffer = qReceiveBuffer;
        g_oTrade = null;
        frameCount = 0;
    }

    public void setClient(Client oClient) {
        g_oClient = oClient;
    }

    public MessageList getMessageList() {
        return g_oMessages;
    }

    /**
     * Sets the todays trades object
     */
    public void setTodaysTrades(MapTodaysTrades oTodaysTrades) {
        g_oTodaysTrades = oTodaysTrades;
    }

    public void addFrame(String frame) {
        receiveBuffer.add(frame);
    }

    /**
     * run method of the thread. removes frames from the shared queue
     * with the receiveQ and send them to the analyser method for turther
     * analisis
     */
    public void run() {
        String frame = null;
        StringBuffer stringBuffer=new StringBuffer();

        /* Take the authentication result first*/
        while ((receiveBuffer.size() == 0) && (Settings.isConnected())) {
            Thread.yield();
            Sleep(100);
        }

        g_oDataDisintegrator = new DataDisintegrator();
        g_oDataDisintegrator.setSeperator(Meta.DS);

        try {

            while (Settings.isConnected()) {
                try {
                    while (!receiveBuffer.isEmpty()) {
                        try {
                            frame = receiveBuffer.remove(0);
                            if (frame != null) {
//                                System.out.println("Size " + receiveBuffer.size());
                                analyse(frame);
                            }
//                            frame = null;
                            frameCount++;
                        } catch (NoSuchElementException e) {
                            System.out.println("Frame Analyser Read Error " + e.toString());
                            if (!Settings.isTCPMode()) {
                                receiveBuffer.clear();
                            }
                            Sleep(500); //Bug ID <#0003>`
                            // this is a stupid javabug. this error should not raise since we check the size first
                        }
                    }
                    try{

                       // outPeriodic.write("\n");

                    }catch (Exception exp1) {
                        exp1.printStackTrace();
                    }
                    Sleep(500);
                } catch (Exception e) {
                    Sleep(1000);
                }
            }
            System.out.println("Frame Analyser Disconnected");
        } catch (Exception e) {
            System.out.println("Error :" + e);
        }
    }

    /**
     * Analyse the given frame for their tags.
     */
    private void analyse(String sFrame) {
        try {
            String symbol = null;
            String symbolCode = null;
            String exchange = null;
            String exchangeCode = null;
            short shtSymbolType = -1;
            int instrumentType = -1;
            int decimalFactor = 1;
            double g_dLastTradePrice = 0;
            BidAsk oBid = null;
            BidAsk oAsk = null;
            Exchange exchangeObject;
            Market subMarket;
            Broker broker;
            IntraDayOHLC ohlcRecord = null;
            Stock oStock = null;
            GlobalIndexObject oGIndex = null;
            FXObject fxStock = null;
            boolean snapshotFrame = false;
            boolean backlogFrame = false;
//            boolean fullFrame = false;
            String marketCenter = null;

            int iDepth;

            StringTokenizer oRecords = new StringTokenizer(sFrame, Meta.RS);
            while (oRecords.hasMoreElements()) {

                iDepth = 0;
                oBid = null;
                oAsk = null;

                String rs = oRecords.nextToken();
                shtSymbolType = -1;
                instrumentType = -1;
                decimalFactor = 1;
                g_dLastTradePrice = 0;
                StringTokenizer oFields = new StringTokenizer(rs, Meta.FS);
                symbol = null;
                symbolCode = null;
                exchange = null;
                exchangeCode = null;
                oStock = null;
                exchangeObject=null;
                subMarket=null;
                broker=null;
                ohlcRecord = null;
                oGIndex = null;
                fxStock = null;
                Announcement oAnnouncement = null;
                snapshotFrame = false;
                backlogFrame = false;
                snapshotFrame = false;
                backlogFrame = false;
                marketCenter = null;
                //int priceCorrectionFactor;
//                fullFrame = false;

                while (oFields.hasMoreElements()) {
                    try {
                        g_oDataDisintegrator.setData(oFields.nextToken());
                        g_sTag = g_oDataDisintegrator.getTag();

                        /* If the tag is null, information is invalid */
                        if (g_sTag == null) continue;

                        g_iTag = Integer.parseInt(g_sTag);
                        value = g_oDataDisintegrator.getData();
                        switch (g_iTag) {
                            case Meta.FULL_FRAME:
                                snapshotFrame = (value.equals("1"));
//                                fullFrame = true;
                                break;
                            case Meta.MESSAGE_TYPE:
                                shtSymbolType = toShort(value);
                                break;
                            case Meta.SYMBOL:
                                symbol = value;
                                break;
                            case Meta.SYMBOL_CODE:
                                symbolCode = value;
                                break;
                            case Meta.EXCHANGE:
                                exchange = value;
                                break;
                            case Meta.EXCHANGE_CODE:
                                exchangeCode = value;
                                break;
                            case Meta.INSTRUMENT_TYPE:
                                instrumentType = toInt(value);
                                break;
                            case Meta.DECIMAL_CORRECTION_FACTOR:
                                decimalFactor = toInt(value);
                                break;
                            case Meta.MARKET_CENTER:
                                marketCenter = value;
                                break;
                            case Meta.FRAME_DATA:
                                exchange = getExchange(exchange, exchangeCode);
                                symbol = getSymbol(exchange, symbol, symbolCode);
//                                if (exchange.equals("ASE")){
//                                    System.out.println(sFrame);
//                                }

//                                System.out.println("frame data " + symbol + " " +value);
                                switch (shtSymbolType) {
                                    case Meta.MESSAGE_TYPE_EQUITY:
//                                    case Meta.MESSAGE_TYPE_OPTION:
//                                    case Meta.MESSAGE_TYPE_INDEX:
                                        if ((symbol == null) || (exchange == null)) continue;
                                        if (instrumentType < 0) {
                                            instrumentType = Meta.INSTRUMENT_EQUITY;
                                        }
                                        if (instrumentType == Meta.INSTRUMENT_FOREX) {
                                            fxStock = ForexStore.getSharedInstance().getForexObject(exchange, symbol, instrumentType);
                                            long blockSize = fxStock.setData(value.split(Meta.FD), decimalFactor, snapshotFrame);
                                            if (!fxStock.isSmallestBlockSize(blockSize)) {
                                                continue;
                                            }
                                        }
                                        if (marketCenter != null) {
                                            symbol = symbol + "." + marketCenter;
                                        }

                                        //  ---- Shanika ---------
                                        boolean isStockNot = false;
                                        Stock st = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrumentType);
                                        if (st == null) {
                                            isStockNot = true;

                                        }

                                        oStock = DataStore.getSharedInstance().createStockObject(exchange, symbol, instrumentType);
                                        oStock.setData(value/*.split(Meta.FD)*/, decimalFactor, snapshotFrame);
                                        oStock.setSnapShotUpdatedTime(System.currentTimeMillis());


                                        if ((oStock.getSectorCode().equals("") || oStock.getSectorCode().equals("N/A") || oStock.getSectorCode().equals("0")) && (oStock.getInstrumentType() != 7)) {
                                            oStock.setSectorCode(Constants.DEFAULT_SECTOR);
//                                             oStock.setSectorCode(Constants.DEFAULT_SECTORS);
                                        }
                                        Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);

                                        try {
//                                        if(isStockNot && ex.isExchageMode() && oStock.getInstrumentType()!=7){
                                            if (isStockNot && ex.isExchageMode()) {
                                                OptimizedTreeRenderer.selectedSymbols.put(oStock.getKey(), true);
                                                oStock.setSymbolEnabled(true);
                                            }
                                        } catch (Exception e) {
                                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                                        }
                                        oStock.print();
//                                        if (((instrumentType == Meta.INSTRUMENT_FUTURE)) && (!oStock.isBaseSymbolUpdated())) {
//                                            updateBaseSymbolData(oStock, instrumentType);
//                                        }
                                        g_dLastTradePrice = oStock.getLastTradeValue();
                                        break;
//                                    case Meta.MESSAGE_TYPE_GLOBAL_INDEX_RESPONCE:
//                                        if ((symbol == null) || (exchange == null)) continue;
//                                        oGIndex= GlobalIndexStore.getSharedInstance().getGlobalIndexFromStore(SharedMethods.getKey(exchange,symbol));
//                                        oGIndex.setExchange(exchange);
//                                        oGIndex.setSymbol(symbol);
//                                        oGIndex.setData(value.split(Meta.FD), decimalFactor);
//                                        break;
                                    case Meta.MESSAGE_TYPE_REGIONAL_QUOTE:
                                        if ((symbol == null) || (exchange == null)) continue;
                                        if (instrumentType < 0) {
                                            instrumentType = Meta.INSTRUMENT_EQUITY;
                                        }
//                                        oStock = DataStore.getSharedInstance().getRegionalQuoteStock(symbol, exchange, instrumentType, marketCenter);
//                                        oStock.setData(value.split(Meta.FD), decimalFactor, snapshotFrame);
                                        String sSymbol = null;
                                        if (marketCenter != null) {
                                            sSymbol = symbol + "." + marketCenter;
                                        }
                                        oStock = DataStore.getSharedInstance().createStockObject(exchange, sSymbol, instrumentType);
                                        oStock.setData(value/*.split(Meta.FD)*/, decimalFactor, snapshotFrame);
                                        oStock.setMarketCenter(marketCenter);
                                        oStock.setSnapShotUpdatedTime(System.currentTimeMillis());
                                        DataStore.getSharedInstance().addToRegQuoteStore(SharedMethods.getKey(exchange, symbol, instrumentType), marketCenter);
                                        break;
                                    case Meta.MESSAGE_TYPE_FUNDAMENTAL_DATA:
                                        if (instrumentType == -1) {
                                            instrumentType = Meta.INSTRUMENT_EQUITY;
                                        }
                                        FDTable ftable = DataStore.getSharedInstance().getFDTable(SharedMethods.getKey(exchange, symbol, instrumentType));
                                        ftable.setData(value, decimalFactor);
//                                        oStock=DataStore.getSharedInstance().getStockObject(SharedMethods.getKey(exchange,symbol, instrumentType), true);
//                                        oStock.setFDTable(ftable);
                                        break;
                                    case Meta.MESSAGE_TYPE_INDEX:
                                    case Meta.MESSAGE_TYPE_CUST_INDEX:
                                        if ((symbol == null) || (exchange == null)) continue;
                                        if (instrumentType == -1) {
                                            instrumentType = Meta.INSTRUMENT_INDEX;
                                        }
                                        oStock = DataStore.getSharedInstance().createStockObject(exchange, symbol, instrumentType);
                                        oStock.setData(value/*.split(Meta.FD)*/, decimalFactor, snapshotFrame);
                                        oStock.setSnapShotUpdatedTime(System.currentTimeMillis());
                                        g_dLastTradePrice = oStock.getLastTradeValue();
                                        break;
                                    case Meta.MESSAGE_TYPE_COMBINED_TRADE:
                                        if ((symbol == null) || (exchange == null)) continue;
                                        if (instrumentType == -1) {
                                            instrumentType = Meta.INSTRUMENT_QUOTE;
                                        }
                                        g_oTrade = CompanyTradeStore.getSharedInstance().getLastTrade(exchange, symbol, instrumentType, backlogFrame);

                                        if (g_oTrade == null) {
                                            try {
                                                g_oTrade = new Trade(exchangeCode, symbol, instrumentType);
                                            } catch (Exception e) {
                                                g_oTrade = new Trade(ExchangeMap.getIDFor(exchange), symbol, instrumentType);
                                            }
                                        }
//                                        g_oTrade.setIndexType(false);
                                        g_oTrade.setData(value.split(Meta.FD), decimalFactor);
                                        break;
                                    case Meta.MESSAGE_TYPE_DEPTH_PRICE:
                                    case Meta.MESSAGE_TYPE_DEPTH_REGULAR:
                                    case Meta.MESSAGE_TYPE_DEPTH_SPECIAL:
                                        if ((symbol == null) || (exchange == null)) continue;
                                        if (instrumentType == -1) {
                                            instrumentType = Meta.INSTRUMENT_EQUITY;
                                        }
                                        setDepthFrame(shtSymbolType, SharedMethods.getKey(exchange, symbol, instrumentType), value, decimalFactor);
                                        break;
                                    case Meta.MESSAGE_TYPE_DEPTH_PRICE_ODDLOT:
                                        if ((symbol == null) || (exchange == null)) continue;
                                        if (instrumentType == -1) {
                                            instrumentType = Meta.INSTRUMENT_EQUITY;
                                        }
                                        OddLotStore.getSharedInstance().populateOddlotStore(SharedMethods.getKey(exchange, symbol, instrumentType), value, decimalFactor);
                                        break;
                                    case Meta.MESSAGE_TYPE_OHLC_TRADE:
                                        if ((symbol == null) || (exchange == null)) continue;
                                        if (instrumentType == -1) {
                                            instrumentType = Meta.INSTRUMENT_EQUITY;
                                        }
                                        ohlcRecord = OHLCStore.getInstance().getLastRecord(exchange, symbol, instrumentType);
                                        ohlcRecord.setData(exchange, instrumentType, value.split(Meta.FD), decimalFactor);
                                        OHLCStore.getInstance().addIntradayRecord(ohlcRecord);
                                        /*if (symbol.equalsIgnoreCase("ELKA")) {
                                            System.out.println("exchange "+ exchange +" , symbol "+ symbol + " , instrumentType "+ instrumentType + " , data "+ value);
                                            System.out.println("ohlcRecord "+ ohlcRecord.toString());
                                        }*/
                                        RadarScreenInterface.getInstance().addFrame(ohlcRecord, SharedMethods.getKey(exchange, symbol, instrumentType));
                                        ohlcRecord.setSnapShotUpdatedTime(System.currentTimeMillis());
                                        break;
                                    case Meta.MESSAGE_TYPE_MARKET:
                                        exchangeObject = ExchangeStore.getSharedInstance().getExchange(exchange);
                                        exchangeObject.setData(value.split(Meta.FD), decimalFactor);
                                        break;
                                    case Meta.MESSAGE_TYPE_SUB_MARKET:
                                        subMarket = ExchangeStore.getSharedInstance().getMarket(exchange, symbol);
                                        if (subMarket != null) {
                                            subMarket.setData(value.split(Meta.FD), decimalFactor);
                                        }
                                        break;
                                    case Meta.MESSAGE_TYPE_ANNOUNCEMENT:
                                        if (instrumentType == -1) {
                                            instrumentType = Meta.INSTRUMENT_EQUITY;
                                        }
                                        oAnnouncement = new Announcement(exchange);
                                        oAnnouncement.setData(value.split(Meta.FD));
                                        System.out.println(oAnnouncement);
                                        if ((oAnnouncement.getLanguage() != null) && (oAnnouncement.getLanguage().equals(Language.getLanguageTag()))) {
                                            AnnouncementStore.getSharedInstance().addAnnouncement(oAnnouncement);  //SharedMethods.getKey(exchange, symbol, instrumentType),
                                        }
                                        oAnnouncement = null;
                                        break;
                                    case Meta.MESSAGE_TYPE_BROKER:
                                        broker = ExchangeStore.getSharedInstance().getBroker(exchange, symbol);
                                        if (broker != null) {
                                            broker.setData(value.split(Meta.FD));
                                        }
                                        break;
                                    case Meta.OPTION_REQUEST:
                                        if (instrumentType == -1) {
                                            instrumentType = Meta.INSTRUMENT_OPTION;
                                        }
                                        OptionStore.getSharedInstance().setOptionSymbolList(symbol, exchange, instrumentType, value);
                                        break;
                                    case Meta.GLOBAL_MARKET_ADD_REQUEST:
                                        MarketSummaryLoader.getSharedInstance().setData(exchange, symbol, instrumentType, decimalFactor, value);
                                        break;
                                        //case Plugin ID:
                                        //PluginStore.setData(Meta.BHAV_COPY_FILE_LIST, value);
                                        //break;
                                    case Meta.MESSAGE_TYPE_INVEST:
                                        if ((symbol == null) || (exchange == null)) continue;
                                        //System.out.println("********* symbol ******* "+symbol);
                                        //System.out.println("********* exchange ******* "+exchange);
                                        //System.out.println("********* investor value ******* "+value);
                                        PluginStore.setData(Meta.MESSAGE_TYPE_INVEST, exchange, symbol, value);

                                        break;
                                    case Meta.MESSAGE_TYPE_DETAILDE_MARKET_SUMMARY:
                                        if ((symbol == null || symbol.equals("null")) || (exchange == null)) continue;
                                        //System.out.println("********* symbol ******* "+symbol);
                                        //System.out.println("********* exchange ******* "+exchange);
                                        PluginStore.setData(Meta.MESSAGE_TYPE_DETAILDE_MARKET_SUMMARY, exchange, symbol, value);

                                        break;
                                    case Meta.MESSAGE_TYPE_PERIODIC_INDICATOR:
//                                        if ((symbol == null || symbol.equals("null")) || (exchange == null)) continue;
                                           // PeriodicIndicator.getSharedInstance().setData( value.split(Meta.FD),1, false);
                                           // periodicIndicator.setData( value, decimalFactor ,false,symbol);
//                                        Stock stock= new Stock(symbol,exchange,instrumentType);
//                                        stock.setData( value, decimalFactor ,false, symbol);
                                        oStock = DataStore.getSharedInstance().createStockObject(exchange, symbol, instrumentType);
                                        oStock.getPeriodicIndicator().setData(value, decimalFactor, false, exchange);

//                                        PeriodicIndicator.getSharedInstance().setData( value, decimalFactor ,false);

                                        break;
                                }
                                break;
                                /*case Meta.FRAME_DATA2:
                                oStock = DataStore.getSharedInstance().createStockObject(exchange, symbol, instrumentType);
                                oStock.setData2(value.split(Meta.FD), decimalFactor, snapshotFrame);
//                                System.out.println("Frame2 " + value);
                                break;*/
                            case Meta.ANNOUNCEMENT_SEARCH:
                                SearchedAnnouncementStore.getSharedInstance().setData(value);

                                break;
                            case Meta.ANNOUNCEMENT_BODY_SEARCH:
                                SearchedAnnouncementStore.getSharedInstance().setBody(value);
                                break;
                                /*case Meta.WINDOW_TYPES:
                                Settings.setAllowedWindowTypes(value);
                                Client.getInstance().getMenus().checkMenuAuthentication();
                                Client.getInstance().validatePopupMenuItems();
                                Client.getInstance().closeInvalidFrames();
                                break;*/

                                /* Bid ask info */
                            case Meta.MARKET_DEPTH:
                                iDepth = toInt(value);
                                switch (shtSymbolType) {
                                    case Meta.MESSAGE_TYPE_DEPTH_REGULAR:
                                        oBid = getRegularOrder(symbol, iDepth, DepthObject.BID);
                                        oAsk = getRegularOrder(symbol, iDepth, DepthObject.ASK);
                                        oBid.setDepthSequance(iDepth);
                                        oAsk.setDepthSequance(iDepth);
                                        break;
                                    case Meta.MESSAGE_TYPE_DEPTH_PRICE:
                                        oBid = getPriceOrder(symbol, iDepth, DepthObject.BID);
                                        oAsk = getPriceOrder(symbol, iDepth, DepthObject.ASK);
                                        oBid.setDepthSequance(iDepth);
                                        oAsk.setDepthSequance(iDepth);
                                        break;
                                    case Meta.MESSAGE_TYPE_DEPTH_SPECIAL:
                                        oBid = getSpecialOrder(symbol, iDepth, DepthObject.BID);
                                        oAsk = getSpecialOrder(symbol, iDepth, DepthObject.ASK);
                                        oBid.setDepthSequance(iDepth);
                                        oAsk.setDepthSequance(iDepth);
                                        break;
                                }
                                break;
                            case Meta.LAST_MARKET_DATE:
                                setMarketDate(value);
                                break;
                            case Meta.CHANGE_PASSWORD:
                                processAuthMessage(value);
                                break;
                            case Meta.USER_VERSION:
                                break;
//                            case Meta.ZIP_SIZE:
//                                setZipSize(value);
//                                break;
//                            case Meta.ZIP_STATUS:
//                                setZipStatus(value);
//                                break;
//                            case Meta.ZIP_TYPE:
//                                Settings.setFullZip(value.equals("" + Meta.FULL_ZIP));
//                                break;
                            case Meta.SNAPSHOT_FRAME:
                                snapshotFrame = true;
                                break;
                            case Meta.BACKLOG_FRAME:
                                backlogFrame = true;
                                break;
                                /*case Meta.HISTORY_DATA:
                                setHistoryCondition(value);
                                break;*/
                            case Meta.HISTORY_UPDATE_AVAILABLE:
                                requestHistoryCondition();
                                break;
//                            case Meta.BROADCAST_MEG:
//                                g_oMessages.addMessage(value);
//                                break;
                            case Meta.BROADCAST_MEG_NEW:
                            case Meta.BROADCAST_MSG_HISTORY:
//                                System.out.println("Broadcast message data ="+value);
                                if (!value.trim().equals(""))
                                    g_oMessages.addMessageNew(value);
                                break;
                            case Meta.PULSE:
                                Client.setMarketTime(value);
                                break;
                            case Meta.LAST_EOD_DATE:
                                processLastEODDate(value);
                                break;
//                            case Meta.SHOW_ANNOUNCEMENTS:
//                                setShowAnnouncements(value);
//                                break;
//                            case Meta.SHOW_INTRADAY_CHART:
//                                setShowIntradayChart(value);
//                                break;
                            case Meta.USER_SESSION:
                                Settings.setSessionID(value);
                                break;
                            case Meta.USERID:
                                Settings.setUserID(value);
                                break;
//                            case Meta.SUBSCRIPTION_ID:
//                                Settings.setSubsciptionID(value);
//                                break;
                            case Meta.ALERT_MESSAGE:
                                AlertFrame alert = new AlertFrame(value);
                                AlertManager.getSharedInstance().setAlert(alert);
                                alert = null;
                                 System.out.println("Recieved = " + value);
  //                               SmartAlertManager.getSharedInstance().processMessage(value);
                                break;
                            case Meta.SMART_ALERT_MESSAGE:
//                                AlertFrame alert = new AlertFrame(value);
//                                AlertManager.getSharedInstance().setAlert(alert);
//                                alert = null;
                                System.out.println("Recieved 220 = " + value);
                                SmartAlertManager.getSharedInstance().processMessage(value);

                                break;
                            case Meta.TRADING_BROKERS:
                                analyseBrokers(value);
                                break;
                                /*case Meta.USER_NAME:
                                validateSatUser(value);
                                break;*/
                            case Meta.SNAPSHOT_BEGIN:
                                processStartOfShapShot(value);
//                                TodaysTradeBacklogDownloadManager.getSharedInstance().startNewDownloadSession(value);
                                break;
                            case Meta.SNAPSHOT_END:
                                processEndOfShapShot(value);
                                break;
//                            case Meta.HISTORY_FILE_LIST:
//                                HistoryDownloadManager.getSharedInstance().setFileList(value);
//                                break;
                            case Meta.ADV_HISTORY_FILE_LIST:
                                HistoryDownloadManager.getSharedInstance().setAdvFileList(value);
//                                System.out.println("********************> History files " + value);
                                break;
                            case Meta.INTRADAY_FILE_LIST:
//                                System.out.println("********************> Intra files " + value);
                                setIntradayFileList(value);
                                break;
                            case Meta.COMPRESSED_ANNOUNCEMENT_BODY_REQUEST:
                                showAnnouncement(value);
                                break;
                            case Meta.CURRENCY_DATA:
                                CurrencyStore.getSharedInstance().addCurrency(value);
                                break;
                            case Meta.VALIDATE_SYMBOL:
//                                System.out.println("data for symbol validation ==="+value);
                                analyseValidateResponse(value);
                                break;
                            case Meta.TOP_STOCKS:
                                TopStocksWindow.getSharedInstance().setRecords(value);
                                break;
                            case Meta.DAILY_TRADES: // timeand sales history, file list
                                TradeBacklogStore.getSharedInstance().setData(value);
                                break;
                            case Meta.SYMBOL_SEARCH:
                                SymbolSearch.getSharedInstance().setSearchedSymbols(value);
                                System.out.println(value);
                                break;
                            case Meta.TRADES_SUMMARY:
                                downloadTradeSummary(value);
                                break;
                            case Meta.CUSTOMER_SUPPORT_REQUEST:
                                SharedMethods.showNonBlockingMessage(Language.getString("CUSTOMER_SUPPORT_COMFIRMATION_MESSAGE"),
                                        JOptionPane.OK_OPTION | JOptionPane.INFORMATION_MESSAGE);
                                break;
                            case Meta.COMPANY_PROFILE:
                                CompanyProfileAnalyser.getSharedInstance().show(value);
                                break;
                            case Meta.NEWS:
//                                System.out.println("News " + value);
                                NewsStore.getSharedInstance().addNews(value);
                                NewsStore.getSharedInstance().applySourceFilter();
                                break;
                            case Meta.COMPRESSED_NEWS_BODY_REQUEST:
//                                System.out.println("Body >" + value + "<");
                                showNews(value);
                                break;
                            case Meta.CALENDER_REQUEST:
                                String[] data = value.split(Meta.FD);
                                String body = UnicodeUtils.getNativeStringFromCompressed(data[1]);
                                body = SharedMethods.getTextFromHTML(body);
//                                System.out.println("calender body=="+body);
                                Client.getInstance().getBrowserFrame().setFont(new TWFont("Courier", Font.PLAIN, 12));
                                Client.getInstance().getBrowserFrame().setContentType("text/html");
                                Client.getInstance().getBrowserFrame().setText(body);
//                                showNews(value);
                                break;
                            case Meta.NEWS_SEARCH_ALL:
                                //System.out.println("News Search -- " + value);
                                SearchedNewsStore.getSharedInstance().setAllData(value);
                                break;
                            case Meta.DDE_OFF:
                                DDEServer.setDdeOn(false);
                                break;
                            case Meta.DDE_ON:
                                DDEServer.setDdeOn(true);
                                break;
                            case Meta.EXCHANGE_TIME_REQUEST:
//                                System.out.println("Data Time = "+ value);
                                LatancyIndicator.getSharedInstance().setData(value);
                                break;
                            case Meta.EXCHANGE_TIME_LAG_THRESHOLD_REQUEST:
//                                System.out.println("Threshold Time = "+ value);
                                LatancyIndicator.getSharedInstance().setThreshold(value);
                                break;
                            case Meta.OHLC_LIST:
                                OHLCBacklogStore.getSharedInstance().setData(value);
//                                OHLCHistoryDownloader.getSharedInstance().clearOlderFiles();
                                break;
                            case Meta.CORP_ACTION_SEARCH_REQUEST:
//                                System.out.println("CORPORATE ACTIONS =="+value);
                                FinancialCalenderStore.getSharedInstance().setSearchInProgress(false);
                                FinancialCalenderStore.getSharedInstance().setData(value);
//                                FinancialCalenderFrame.getFinancialCalenderFrame().show();
                                FinancialCalenderWindow.getSharedInstance().show();
                                break;
                            case Meta.CORP_ACTION_BODY_REQUEST:
//                                System.out.println("VAS_EVENT_BODY_REQUEST=="+value);
                                //todo
                                CABodyProcessor.getInstance().process(value);
                                break;
                            case Meta.STOCK_REPORT_SEARCH:
                                StockReportWindow.setData(value);

//                                SharedMethods.printLine(value);
                                break;
                            case Meta.STOCK_REPORT_BODY: {
                                /* Read the pdf file */
                                FileOutputStream out = null;
                                String fileName = "" + System.currentTimeMillis();
                                try {
                                    File f = new File(".\\temp\\" + fileName + ".pdf");
                                    out = new FileOutputStream(f);
                                    f.deleteOnExit();
                                    f = null;
                                }
                                catch (Exception ex) {
                                    ex.printStackTrace();
                                }
//                                SharedMethods.printLine("pdf received");
                                try {
                                    out.flush();
                                    out.close();
                                }
                                catch (Exception ex) {
                                    ex.printStackTrace();
                                }
                                Client.getInstance().savePDF(fileName);
                                break;
                            }
//                            case Meta.OPTION_REQUEST:   // option chain symbols for the month
//                                OptionStore.getSharedInstance().setSymbolList(value);
//                                break;
//                            case Meta.MESSAGE_TYPE_REGIONAL_QUOTE :
//                                System.out.println("Regional Quote : "+value);
//                                break;
                            case Meta.ADD_EXCHANGE_EXPIARYDATE_REQUEST:
                                System.out.println("EXP REQ" + value);
                                ExchangeStore.getSharedInstance().setExpiaryDates(value);
                                break;
                            case Meta.OPTION_SYMBOL_SEARCH:
                                System.out.println("OPTION_SYMBOL_SEARC===" + value);
                                OptionSymbolBuilder.setOptionSymbol(value);
//                              Client.getInstance().getOptionSymbolBuilder().set
//                              ExchangeStore.getSharedInstance().setExpiaryDates(value);
                                break;
                            case Meta.VALIDATE_SYMBOL_BULK:
                                analyseValidateResponseForBulk(value);
                                break;
                        }
                    } catch (Exception e) {
                        System.out.println("Error > " + sFrame);
                        e.printStackTrace();
                    }

                }
                rs = null;

                if ((symbol != null) && (exchange != null)) {
                    switch (shtSymbolType) {
                        case Meta.MESSAGE_TYPE_EQUITY:
                            if (g_dLastTradePrice > 0F) {
                                g_oTodaysTrades.insertSymbol(exchange, symbol.trim(), oStock.getMarketID(), instrumentType);
                            }
                            //SymbolsRegistry.getSharedInstance().checkSymbol(exchange + ESPConstants.KEY_SEPERATOR_CHARACTER + symbol );
                            // System.out.println(symbol + " " + exchange + " " + value);
                            UpdateNotifier.setShapshotUpdated();

                            // run the ticker form the snapshot data
                            /*if (TradeFeeder.isVisible()) {
                                if (((TradeFeeder.getMode() == TWTypes.TickerFilter.MODE_ALL) && (exchange.equals(ExchangeStore.getSelectedExchangeID()))) ||
                                    (((TradeFeeder.getMode() == TWTypes.TickerFilter.MODE_WATCHLIST) && (TradeFeeder.contains(oStock.getKey()))))){
                                    if ((!snapshotFrame) && (!fullFrame) && (Settings.isShowSummaryTicker())) {
                                        if (lastTradeUpdated && (oStock != null)) {
                                            if (TradeFeeder.size() < TradeFeeder.getSnapshotBufferSize()){
                                                TickerObject oTickerData = new TickerObject();
                                                int tickerStatus;
                                                if (oStock.getChange() == 0)
                                                    tickerStatus = Constants.TICKER_STATUS_NOCHANGE;
                                                else if (oStock.getChange() > 0)
                                                    tickerStatus = Constants.TICKER_STATUS_UP;
                                                else
                                                    tickerStatus = Constants.TICKER_STATUS_DOWN;
                                                oTickerData.setData(exchange, symbol, oStock.getLastTradeValue(),
                                                        oStock.getTradeQuantity(), (float) oStock.getChange(),
                                                        (float) oStock.getPercentChange(), tickerStatus, 0);
                                                TradeFeeder.addData(oTickerData);
                                            }
                                        }
                                    }
                                }
                            }*/

                            /*if ((!snapshotFrame) && (!fullFrame) && (Settings.isShowSummaryTicker())) {
                                if (TradeFeeder.isVisible()) {// && (Settings.isMarketOpen())) {
                                    if (lastTradeUpdated && (oStock != null)) {
                                        if (TradeFeeder.size() < TradeFeeder.getSnapshotBufferSize()){
                                            TickerObject oTickerData = new TickerObject();
                                            int tickerStatus;
                                            if (oStock.getChange() == 0)
                                                tickerStatus = Constants.TICKER_STATUS_NOCHANGE;
                                            else if (oStock.getChange() > 0)
                                                tickerStatus = Constants.TICKER_STATUS_UP;
                                            else
                                                tickerStatus = Constants.TICKER_STATUS_DOWN;
                                            oTickerData.setData(exchange, symbol, oStock.getLastTradeValue(),
                                                    oStock.getTradeQuantity(), (float) oStock.getChange(),
                                                    (float) oStock.getPercentChange(), tickerStatus, 0);
                                            TradeFeeder.addData(oTickerData);
                                        }
                                    }
                                }
                            }*/
                            break;
                        case Meta.MESSAGE_TYPE_OHLC_TRADE:
                            OHLCStore.getInstance().addOHLCRecord(ohlcRecord);
                            break;

                        case Meta.MESSAGE_TYPE_DEPTH_PRICE:
                            break;
                        case Meta.MESSAGE_TYPE_COMBINED_TRADE:
//                            if(exchange.equals("TDWL"))
//                                System.out.println("Trade = "+symbol + " , "+g_oTrade.getTradePrice());
/*
                                isTradeEnabledMode() zippedFrame  AddToTrades
                                        0                   0           1
                                        0                   1           0
                                        1                   0           1
                                        1                   1           1
                                */
                            // if (Settings.isTradeEnabledMode() || (!zippedFrame)) {
                            if ((ExchangeStore.getSharedInstance().getExchange(exchange)).isTimensalesEnabled()) {

                                boolean bVaidTrade = TradeStore.addTrade(g_oTrade, backlogFrame);
                                if (bVaidTrade) {
                                    g_oTrade.setSnapShotUpdatedTime(System.currentTimeMillis());
                                }
//                                TradeStore.addTrade((Trade)g_oTrade.clone(), backlogFrame);
//                                TradeStore.addTrade((Trade)g_oTrade.clone(), backlogFrame);
//                                TradeStore.addTrade((Trade)g_oTrade.clone(), backlogFrame);
                                /*if (bVaidTrade) {// && (Settings.isTradeEnabledMode())) {
                                    OHLCStore.getInstance().addTrade(g_oTrade);
                                }*/

                                if (SharedSettings.IS_CUSTOM_SELECTED) {
                                    if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_TradeTicker)) {   //
                                        if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketTimeAndSales)) {
//                                            if ((ExchangeStore.getSelectedExchangeID() != null) && ((ExchangeStore.getSelectedExchangeID().equals(exchange)) || TradeFeeder.getMode() == TWTypes.TickerFilter.MODE_WATCHLIST)) {
                                             if ((ExchangeStore.getSelectedExchangeID() != null) && (ExchangeStore.getSelectedExchangeID().equals(exchange))){
                                    if ((!snapshotFrame) && (!backlogFrame) && (!Settings.isShowSummaryTicker())) {
                                        //if (Settings.isTradeEnabledMode() && (!Settings.isShowSummaryTicker())) {
                                        if ((!g_oTrade.isIndexType()) && (bVaidTrade) && TradeFeeder.isVisible()) {
                                            //if (TradeFeeder.isPreferredExchange(exchange)) {
                                            TickerObject oTickerData = new TickerObject();
                                            int tickerStatus;
                                            if ((g_oTrade != null)) {
                                                if (g_oTrade.getOddlot() == 1)
                                                    tickerStatus = Constants.TICKER_STATUS_SMALLTRADE;
                                                else if (g_oTrade.getNetChange() == 0)
                                                    tickerStatus = Constants.TICKER_STATUS_NOCHANGE;
                                                else if (g_oTrade.getNetChange() > 0)
                                                    tickerStatus = Constants.TICKER_STATUS_UP;
                                                else
                                                    tickerStatus = Constants.TICKER_STATUS_DOWN;

                                                oTickerData.setData(exchange, symbol, g_oTrade.getTradePrice(),
                                                        g_oTrade.getTradeQuantity(), g_oTrade.getNetChange(),
                                                        g_oTrade.getPrecentNetChange(), tickerStatus, g_oTrade.getSplits(), instrumentType);//
                                                TradeFeeder.addData(oTickerData);

                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                } else {
                                    if ((!snapshotFrame) && (!backlogFrame) && ((!g_oTrade.isIndexType()) && (bVaidTrade))) {

                                        ArrayList<String> upperexchangeList = UpperPanelSettings.getExchangeList();
                                        if (upperexchangeList.contains(exchange)) /*|| UpperFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)*/ {
                                            if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_TradeTicker)) {
                                                if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketTimeAndSales)) {
                                                    if ((!UpperPanelSettings.showSummaryTicker) && UpperTickerFeeder.isVisible()) {
                                                        if (UpperTickerFeeder.isExchangeModeSelected || UpperTickerFeeder.isWatchListModeSelected) {
                                                            CommonTickerObject upperTickerOb = new CommonTickerObject();
                                                            upperTickerOb.setData(exchange, symbol, g_oTrade.getTradePrice(),
                                                                    g_oTrade.getTradeQuantity(), g_oTrade.getNetChange(),
                                                                    g_oTrade.getPrecentNetChange(), 0
                                                                    , g_oTrade.getSplits(), instrumentType);//
                                                            UpperTickerFeeder.addData(upperTickerOb);
                                                        }
                                                    }
                                                }
                                            }
                                        }


                                        ArrayList<String> middleexchangeList = MiddlePanelSettings.getExchangeList();
                                        if (middleexchangeList.contains(exchange)) /*|| MiddleFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)*/ {
                                            if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_TradeTicker)) {
                                                if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketTimeAndSales)) {
                                                    if ((!MiddlePanelSettings.showSummaryTicker) && MiddleTickerFeeder.isVisible()) {
                                                        if (MiddleTickerFeeder.isExchangeModeSelected || MiddleTickerFeeder.isWatchListModeSelected) {
                                                            CommonTickerObject upperTickerOb = new CommonTickerObject();
                                                            upperTickerOb.setData(exchange, symbol, g_oTrade.getTradePrice(),
                                                                    g_oTrade.getTradeQuantity(), g_oTrade.getNetChange(),
                                                                    g_oTrade.getPrecentNetChange(), 0
                                                                    , g_oTrade.getSplits(), instrumentType);//
                                                            MiddleTickerFeeder.addData(upperTickerOb);
                                                        }
                                                    }
                                                }
                                            }
                                        }

                                        ArrayList<String> lowerexchangeList = LowerPanelSettings.getExchangeList();
                                        if (lowerexchangeList.contains(exchange) )/*|| LowerFilterPanel.mode == TWTypes.TickerFilter.MODE_WATCHLIST)*/ {
                                            if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_TradeTicker)) {
                                                if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_MarketTimeAndSales)) {
                                                    if ((!LowerPanelSettings.showSummaryTicker) && LowerTickerFeeder.isVisible()) {
                                                        if (LowerTickerFeeder.isExchangeModeSelected || LowerTickerFeeder.isWatchListModeSelected) {
                                                            CommonTickerObject upperTickerOb = new CommonTickerObject();
                                                            upperTickerOb.setData(exchange, symbol, g_oTrade.getTradePrice(),
                                                                    g_oTrade.getTradeQuantity(), g_oTrade.getNetChange(),
                                                                    g_oTrade.getPrecentNetChange(), 0
                                                                    , g_oTrade.getSplits(), instrumentType);//
                                                            LowerTickerFeeder.addData(upperTickerOb);
                                                        }
                                                    }
                                                }
                                            }
                                        }
                                    }
                                }
                                break;
                            }
                    }
                }
                oFields = null;
                oStock = null;
            }
        } catch (Exception e) {
            System.out.println("Frame Error > " + sFrame);
            e.printStackTrace();
        }
    }

    private void downloadTradeSummary(String frame) {
        try {
            String[] fields = frame.split(Meta.FD);
            String exchange = fields[0];
            long size = Long.parseLong(fields[1]);

            if (size > 0) { //if the length is valid
                if (ExchangeStore.getSharedInstance().isValidExchange(exchange)) { // never trust CMs. They send all the garbage in the world
                    if (ExchangeStore.isValidIinformationType(exchange, Meta.IT_SymbolTimeAndSales)) {
                        if (!HistoryArchiveRegister.getSharedInstance().isSameSize(exchange, "TradeSummary", size)) {
                            if (!HistoryArchiveRegister.getSharedInstance().isInProgress(exchange, "TradeSummary", size)) {
                                TradeSummaryStore.getSharedInstance().clear(exchange); // clear the store first
//                            System.out.println("Trade summary downloader started ="+exchange);
                                new TradeSummaryDownloader(exchange, size);
//                                TradeSummaryDownloader tradeSummaryDownloader = new TradeSummaryDownloader(exchange, size);
//                                tradeSummaryDownloader = null;
                                TradeStore.getSharedInstance().clear(exchange);
//                        }else{
//                            System.out.println("Trade summary download in progress");
                            }
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private synchronized void updateBaseSymbolData(Stock stock, int type) {
        Stock baseStock = null;

        baseStock = DataStore.getSharedInstance().getBaseSymbol(stock.getExchange(), stock.getOptionBaseSymbol(), type);
        if (baseStock != null) {
            stock.setInstrumentType(type);
//            stock.setLongDescription(baseStock.getLongDescription() + " (" + SharedMethods.toDisplayDateFormat(stock.getExpirationDate()) + ")");
//            stock.setShortDescription(baseStock.getShortDescription() + " (" + SharedMethods.toDisplayDateFormat(stock.getExpirationDate()) + ")");
            stock.setAssetClass(baseStock.getAssetClass());
//            stock.setCurrencyCode(baseStock.getCurrencyCode());
            stock.setCompanyCode(baseStock.getCompanyCode());
            stock.setSectorCode(baseStock.getSectorCode());
            stock.setMarketID(baseStock.getMarketID());
            stock.setDecimalCount(baseStock.getDecimalCount());
//            stock.setLotSize(baseStock.getLotSize());
            stock.setUnit(baseStock.getUnit());
            stock.setBaseSymbolUpdated(true);
        }
    }

    private String getPreferredRecord(String exchange, String[] records) {
        if (ExchangeStore.getSharedInstance().isValidExchange(exchange)) {
            for (int i = 1; i < records.length; i++) {
                String record = records[i];
                String[] fields = record.split(Meta.ID);
                if (fields[0].trim().equals(exchange)) {
                    return record;
                }
            }
        }
        return null;
    }

    private void analyseValidateResponse(String frame) {
        try {
            frame = removeUnsubscribedExchanges(frame);
            String[] records = frame.split(Meta.FD);
            String requestID = records[0];

            if (records.length == 1) { // invalid symbol

                if (requestID.startsWith("OrderList:")) {
                    // Do nothing
                    System.out.println("Invalid Symbol in Order List");
                } else if (requestID.startsWith("PF_SIM_DLG")) { // request was from the Portfolio Simulator Dialog
                    TransactionDialogInterface transactionDialog = (TransactionDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                    transactionDialog.notifyInvalidSymbol(null);
                }  else if (requestID.startsWith("PF_IMPRT_DLG")) { // request was from the Portfolio Simulator Dialog
                    PFImportDialogInterface pfsymbolvalidator = (PFImportDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                    pfsymbolvalidator.notifyInvalidSymbol(null);
                }else if (requestID.startsWith("PF_RESTORE_DLG")) { // request was from the Portfolio Simulator Dialog
                    PFImportDialogInterface pfsymbolvalidator = (PFImportDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                    pfsymbolvalidator.notifyInvalidSymbol(null);
                } else if (requestID.startsWith("PF_SPLIT_DLG")) { // request was from the Portfolio Simulator Dialog
                    SplitFactorWindow.getsharedInstance().notifyInvalidSymbol(null);
                } else if (requestID.startsWith("TransactionDialog")) {
                    TransactionDialogInterface transactionDialog = (TransactionDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                    transactionDialog.notifyInvalidSymbol(null);
                } else if (requestID.startsWith("RapidOrder")) {
                    TransactionDialogInterface rapidOrderPanel = (TransactionDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                    rapidOrderPanel.notifyInvalidSymbol(null);
                } else if (requestID.startsWith("RuleGenerator")) {
                    RuleGeneratorInterface transactionDialog = (RuleGeneratorInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                    transactionDialog.notifyInvalidSymbol(null);
                } else if (requestID.startsWith("OnceaDayValidation")) {
                    System.out.println("Once a day validation failed for " + frame);
                    // do nothing
                } else if (requestID.startsWith("WhatifCalculatorFrame")) {
                    WhatifCalculatorFrame wical = WhatifCalculatorFrame.getSharedInstannce();
                    wical.setInvalidSymbol();
                }else if (requestID.startsWith("SmartAlertFrame")) {
                    SmartAlertWindow alertFrame = SmartAlertWindow.getSharedInstance();
                    alertFrame.clearAll();
                }else if (requestID.startsWith("Commodities")) {
                    System.out.println("Commodities Invalid Symbol");
                } else {
                    new ShowMessage(Language.getString("MSG_INVLID_SYMBOL"), "E");
                }

            } else {
                String record;
                if (records.length > 2) {
                    if (requestID.startsWith("OrderList:")) { // no need to check for the exchange selection. exchange comes with the request id
                        String requestIDdata[] = requestID.split(":");
                        String exchange = requestIDdata[1];
                        record = getPreferredRecord(exchange, records);
                    } else if (requestID.startsWith("PF_IMPRT_DLG") || requestID.startsWith("PF_RESTORE_DLG")) {
                        String original = requestID.split(":")[2];
                       record= processValidationFromPFImport(frame,original);
                        if (record == null && requestID.startsWith("PF_IMPRT_DLG")) {
                            PFImportDialogInterface pfsymbolvalidator = (PFImportDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                            pfsymbolvalidator.notifyInvalidSymbol(null);
                        } else if (record == null && requestID.startsWith("PF_RESTORE_DLG")) {
                            PFImportDialogInterface pfsymbolvalidator = (PFImportDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                            pfsymbolvalidator.notifyInvalidSymbol(null);
                    } 
                    } else {
                        ValidatedCompanySelector companySelector = new ValidatedCompanySelector(frame, ValidatedCompanySelector.MODE_EXTERNAL_VALIDATION);
                        record = companySelector.getSelectedRecord();
                        if (record == null && requestID.startsWith("PF_SIM_DLG")) {
                            TransactionDialogInterface transactionDialog = (TransactionDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                            transactionDialog.notifyInvalidSymbol(null);
                        } else if (record == null && requestID.startsWith("PF_IMPRT_DLG")) {
                            PFImportDialogInterface pfsymbolvalidator = (PFImportDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                            pfsymbolvalidator.notifyInvalidSymbol(null);
                        }
                    }
                } else {
                    record = records[1];
                }
                if (record != null) {
                    String[] fields = record.split(Meta.ID);
                    String exchange = fields[0].trim();
                    if (ExchangeStore.isExpired(exchange)) {
                        SharedMethods.showExchangeExpiryMesage(exchange);
                        return;
                    } else if (ExchangeStore.isInactive(exchange)) {
                        SharedMethods.showExchangeInactiveMesage(exchange);
                        return;
                    }
                    String symbol = fields[1];
                    String marketCenter = fields[fields.length - 1];
                    int instrumentType = Integer.parseInt(fields[2]);
                    String key = SharedMethods.getKey(exchange, symbol, instrumentType);

                    if (requestID.startsWith("MainBoard")) { // request was from main board
//                        System.out.println("Symbol search response ===="+record);
//                        if (!ExchangeStore.isExpired(exchange)) {
                        createStock(exchange, symbol, instrumentType, record);
// ---------------------- Shanika --------------------------------


                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                        Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);
                        if (ex.isDefault() && ex.isExchageMode()) {
                            Stock st = DataStore.getSharedInstance().getStockObject(key);
                            OptimizedTreeRenderer.selectedSymbols.put(key, true);
                            st.setSymbolEnabled(true);

                        } else {
                            Stock st = DataStore.getSharedInstance().getStockObject(key);
                            if (OptimizedTreeRenderer.selectedSymbols.containsKey(key)) {
                                st.setSymbolEnabled(true);
                            }
                        }
                        try {
                            ClientTable table = (ClientTable) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                            table.getSymbols().appendSymbol(key);
                        } catch (Exception e) {
                            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                        }
//                        }else{
//                            SharedMethods.showMessage(Language.getString("SUBSCRIPTION_EXP"),INFORMATION_MESSAGE);
//                        }
                    } else if (requestID.startsWith("ClassicView")) { // request was from main board
                        createStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                        Symbols symbols = (Symbols) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                        symbols.appendSymbol(key);
                    } else if (requestID.startsWith("DetailQuote")) { // request was from detail Quote
                        createStock(exchange, symbol, instrumentType, record);
                        //DataStore.getSharedInstance().addSymbolRequest(exchange, symbol);
                        Client.getInstance().showDetailQuote(key, true);
                    } else if (requestID.startsWith("SummaryQuote")) { // request was from Snap Quote
                        if (ExchangeStore.getSharedInstance().checkFeatureAvailability(exchange, Meta.IT_SnapQuote)) {
                            if (Client.getInstance().isValidInfoType(key, Meta.IT_SnapQuote, true)) {
                                createStock(exchange, symbol, instrumentType, record);
                                //DataStore.getSharedInstance().addSymbolRequest(exchange, symbol);
                                Client.getInstance().showSnapQuote(key, true, false, false, LinkStore.LINK_NONE);
                            } else {
                                String message = Language.getString("FEATURE_NOT_AVAILABLE");
                                message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(exchange).getDescription());
                                SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                            }
                        } else {
                            String message = Language.getString("FEATURE_NOT_AVAILABLE");
                            message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(exchange).getDescription());
                            SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                        }
                    } else if (requestID.startsWith("CashFlowDetailQuote")) { // request was from Snap Quote
                        createStock(exchange, symbol, instrumentType, record);
                        //DataStore.getSharedInstance().addSymbolRequest(exchange, symbol);
                        Client.getInstance().showCashFlowDetailQuote(key, true, false, false, LinkStore.LINK_NONE);
                    } else if (requestID.startsWith("DepthPrice")) { // request was from Depth By Price
                        if (ExchangeStore.getSharedInstance().checkFeatureAvailability(exchange, Meta.IT_MarketDepthByPrice)) {
                            createStock(exchange, symbol, instrumentType, record); //Bug ID <#0015> line added to close the bug
                            //DataStore.getSharedInstance().addSymbolRequest(exchange, symbol);
                            Client.getInstance().depthByPrice(key, false, Constants.MAINBOARD_TYPE);
                        } else {
                            String message = Language.getString("FEATURE_NOT_AVAILABLE");
                            message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(exchange).getDescription());
                            SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                        }
                    } else if (requestID.startsWith("DepthOrder")) { // request was from Depth By Order
                        createStock(exchange, symbol, instrumentType, record); //Bug ID <#0015> line added to close the bug
                        //DataStore.getSharedInstance().addSymbolRequest(exchange, symbol);
                        Client.getInstance().depthByOrder(key, false, false, LinkStore.LINK_NONE);
                    } else if (requestID.startsWith("DepthSpecial")) { // request was from Depth Special
                        createStock(exchange, symbol, instrumentType, record); //Bug ID <#0015> line added to close the bug
                        //DataStore.getSharedInstance().addSymbolRequest(exchange, symbol);
                        Client.getInstance().depthSpecialByOrder(key, false, false, LinkStore.LINK_NONE);
                    } else if (requestID.startsWith("Trades")) { // request was from Time & Sales
                        createStock(exchange, symbol, instrumentType, record);
                        //DataStore.getSharedInstance().addSymbolRequest(exchange, symbol);
                        Client.getInstance().mnu_TimeNSalesSymbol(key, Constants.MAINBOARD_TYPE, false, false, LinkStore.LINK_NONE);
                    } else if (requestID.startsWith("Chart_New_Symbol")) { // request was from Chart
                        String description = Language.getLanguageSpecificString(record.split(Meta.ID)[4]);
                        OHLCStore.setDecription(key, description);
                        GraphFrame graphFrame = (GraphFrame) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                        graphFrame.procesSymbolResponse(key, false);
                        description = null;
                    } else if (requestID.startsWith("Chart_Navigator")) { // request was for Chart from Navigator
                        g_oClient.showChart(key, null, null);
                    } else if (requestID.startsWith("PF_SIM_DLG")) { // request was from Portfolio Simulator Dialog
                        createStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                        TransactionDialogInterface transactionDialog = (TransactionDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                        transactionDialog.setSymbol(key);
                    }else if (requestID.startsWith("PF_UNCHECKED_ADDSYMBOL")) { // request was from Portfolio Simulator Dialog
                        createStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                       // TransactionDialogInterface transactionDialog = (TransactionDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                       // transactionDialog.setSymbol(key);
                    }
                    else if (requestID.startsWith("PF_IMPRT_DLG")) { // request was from the Portfolio Import Dialog
                        createStock(exchange, symbol, instrumentType, record);
                       // DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                        PFImportDialogInterface pfsymbolvalidator = (PFImportDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                        pfsymbolvalidator.setSymbol(key, instrumentType);
                    } else
                    if (requestID.startsWith("PF_SPLIT_DLG")) { // request was from the Portfolio Split Factor Dialog
                        createStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                        SplitFactorWindow.getsharedInstance().setSymbol(key);
                    } else
                    if (requestID.startsWith("PF_RESTORE_DLG")) { // request was from the Portfolio Split Factor Dialog
                        createStock(exchange, symbol, instrumentType, record);
                       // DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                         PFImportDialogInterface transactionDialog = (PFImportDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                        transactionDialog.setSymbol(key,instrumentType);
                        //SplitFactorWindow.getsharedInstance().setSymbol(key);
                    } else
                    if (requestID.startsWith("TransactionDialog")) { // request was from Portfolio Simulator Dialog
                        createStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                        TransactionDialogInterface transactionDialog = (TransactionDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                        transactionDialog.setSymbol(key);
                    } else if (requestID.startsWith("RapidOrder")) { // request was from Portfolio Simulator Dialog
                        createStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                        TransactionDialogInterface rapidOrderPanel = (TransactionDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                        rapidOrderPanel.setSymbol(key);
                    } else
                    if (requestID.startsWith("WhatifCalculatorFrame")) { // request was from Portfolio Simulator Dialog
                        createStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                        //TransactionDialogInterface transactionDialog = (TransactionDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                        //transactionDialog.setSymbol(key);
//                        System.out.println("what if request came....");
                        WhatifCalculatorFrame wical = WhatifCalculatorFrame.getSharedInstannce();
                        wical.setCurrentPrice(key);
                    } else if (requestID.startsWith("SmartAlertFrame")) { // request was from Smart Alert Dialog
                        createStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                       SmartAlertWindow alertFrame = SmartAlertWindow.getSharedInstance();
                        alertFrame.upDateWindow(key);
                    } else if (requestID.startsWith("MiniTradeFrame")) { // request was from Mini Trade Dialog
                        createStock(exchange, symbol, instrumentType, record);  //"MiniTradeFrame"+"-"+ parent.getViewSetting().getID() +selectedKey+
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                        JInternalFrame[] frames = Client.getInstance().getDesktop().getAllFrames();
                        for (int i = 0; i < frames.length; i++) {
                            if (frames[i] instanceof MiniTradingFrame) {
                                if (((InternalFrame) frames[i]).getViewSetting().getID().equals(requestID.split("-")[1])) {
                                    ((MiniTradingFrame) frames[i]).resetSymbolDetails(key);
                                }
                            }
                        }
                    } else if (requestID.startsWith("OrderList")) { // request was from Portfolio Simulator Dialog
                        createStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                        //TransactionDialogInterface transactionDialog = (TransactionDialogInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                        //transactionDialog.setSymbol(key);
                    } else if (requestID.startsWith("TradingBuy")) { // request was from Trading Buy screen
                        createStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                        TransactionDialog transactionDialog = TradeMethods.getSharedInstance().showBuyScreen(key);
                        if (transactionDialog != null) {
                            transactionDialog.setSymbol(key);
                        }
                    } else if (requestID.startsWith("TradingSell")) { // request was from Trading Buy screen
                        createStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                        TransactionDialog transactionDialog = TradeMethods.getSharedInstance().showSellScreen(key);
                        if (transactionDialog != null) {
                            transactionDialog.setSymbol(key);
                        }
                    } else if (requestID.startsWith("RuleGenerator")) {
                        RuleGeneratorInterface transactionDialog = (RuleGeneratorInterface) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                        transactionDialog.setSymbol(key);
                    } else if (requestID.startsWith("FullQuote")) {
                        if (ExchangeStore.getSharedInstance().checkFeatureAvailability(exchange, Meta.IT_FullQuote)) {
                            createStock(exchange, symbol, instrumentType, record);
                            DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
//                        System.out.println("from frame analyser");
                            boolean isEnabled = DataStore.getSharedInstance().getStockObject(key).isSymbolEnabled();
                            if (isEnabled) {
                                Client.getInstance().showFullQuote(key, false, Constants.FULL_QUOTE_TYPE, false, LinkStore.LINK_NONE);
                            }
//                            MultiDialogWindow.getSharedInstance().setVisible(true);
//                            MultiDialogWindow.getSharedInstance().setSymbol(key);
//                            MultiDialogWindow.getSharedInstance().updateUI();
                        } else {
                            String message = Language.getString("FEATURE_NOT_AVAILABLE");
                            message = message.replaceAll("\\[EXCHANGE\\]", ExchangeStore.getSharedInstance().getExchange(exchange).getDescription());
                            SharedMethods.showMessage(message, INFORMATION_MESSAGE);
                        }
                    } else if (requestID.startsWith("ForexSearch")) {
                        createForexStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                        String[] arr = requestID.split(":");
                        String watchListID = arr[1].substring(0, 13);
                        ForexBoard.getInstance().addSymbolsForWatchlistStore(SharedMethods.getKey(exchange, symbol, instrumentType), watchListID);
                        arr = null;
                        watchListID = null;
//                       ForexBoard fxBoard = (ForexBoard) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
//                       fxBoard.getSymbols().appendSymbol(key);
                    } else if (requestID.startsWith("OpenPositions")) {
                        createStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                    }else if (requestID.startsWith("Commodities")) {
                        createStock(exchange, symbol, instrumentType, record);
                        DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                    } else if (requestID.startsWith("OnceaDayValidation")) {
                        updateValidatedSymbols(exchange, symbol, instrumentType, record);
                    } else {
                        createStock(exchange, symbol, instrumentType, record);
                    }
//                    else if(requestID.startsWith("HistoryAnalyzer")){
//                       createStock(exchange, symbol, instrumentType, record);
//                       DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
////                       sNewSymbol = SharedMethods.getKey(exchange, symbol,instrumentType);
//                       NetCalcModel.getSharedInstance().setNewRecord(SharedMethods.getKey(exchange, symbol,instrumentType));
//                        SharedMethods.updateGoogleAnalytics(SharedMethods.GoogleSection.Tools, "HistoryAnalyser-Add");
//                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void analyseValidateResponseForBulk(String frame) {
        try {
            frame = removeUnsubscribedExchanges(frame);
            String[] records = frame.split(Meta.FD);
            String requestID = records[0];
            if (records.length == 1) {
                // invalid symbol
                // new ShowMessage(Language.getString("MSG_INVLID_SYMBOL"), "E");
            } else {
                String record = records[1];
                String[] symbolFrames=record.split(Meta.GS);
                for (int i=0;i<symbolFrames.length;i++) {
                    if (record != null) {
                        String[] fields = symbolFrames[i].split(Meta.ID);
                        String exchange = fields[0].trim();
                        if (ExchangeStore.isExpired(exchange)) {
                            SharedMethods.showExchangeExpiryMesage(exchange);
                            return;
                        } else if (ExchangeStore.isInactive(exchange)) {
                            SharedMethods.showExchangeInactiveMesage(exchange);
                            return;
                        }
                        String symbol = fields[1];
                        String marketCenter = fields[fields.length - 1];
                        int instrumentType = Integer.parseInt(fields[2]);
                        String key = SharedMethods.getKey(exchange, symbol, instrumentType);
                        if (requestID.startsWith("MainBoard")) { // request was from main board
                            createStock(exchange, symbol, instrumentType, symbolFrames[i]);

    // ---------------------- Shanika --------------------------------
                            DataStore.getSharedInstance().addSymbolRequest(exchange, symbol, instrumentType);
                            Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);
                            if (ex.isDefault() && ex.isExchageMode()) {
                                Stock st = DataStore.getSharedInstance().getStockObject(key);
                                OptimizedTreeRenderer.selectedSymbols.put(key, true);
                                st.setSymbolEnabled(true);

                            } else {
                                Stock st = DataStore.getSharedInstance().getStockObject(key);
                                if (OptimizedTreeRenderer.selectedSymbols.containsKey(key)) {
                                    st.setSymbolEnabled(true);
                                }
                            }
                            try {
                                ClientTable table = (ClientTable) SymbolsRegistry.getSharedInstance().removeRememberedRequest(requestID);
                                table.getSymbols().appendSymbol(key);
                            } catch (Exception e) {
                                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                            }

                        } else {
                            createStock(exchange, symbol, instrumentType, symbolFrames[i]);
                        }
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private String processValidationFromPFImport(String frame, String originalExchange) {
        String[] records = frame.split(Meta.FD);
        Hashtable<String, String> validEntries = new Hashtable<String, String>();
        for (int i = 1; i < records.length; i++) {
            String[] fields = records[i].split(Meta.ID);
                String exchangeCode = fields[0].trim();
                String symbolCode = fields[1].trim();
            String description = fields[2].trim();
        //    int instrument = Integer.parseInt(fields[3].trim());
            if (ExchangeStore.getSharedInstance().isValidExchange(exchangeCode) && originalExchange.equalsIgnoreCase(exchangeCode)) {
                //  JRadioButton label = new JRadioButton(exchange + " - " + description);
                //  label.setActionCommand(SharedMethods.getKey(exchange, symbol, instrument));
                String key;
                 try {
                        key = SharedMethods.getKey(exchangeCode,symbolCode, Integer.parseInt(fields[5]));
                    } catch(Exception e) {
                        key = records[i];
                    }
              validEntries.put(key, records[i]);
                //   label.addActionListener(this);
            }
        }

        if(validEntries.size()==1){
            return validEntries.elements().nextElement();
        }else{
            ValidatedCompanySelector companySelector = new ValidatedCompanySelector(frame, ValidatedCompanySelector.MODE_EXTERNAL_VALIDATION);
            return companySelector.getSelectedRecord();
        }

    }

    private void createStock(String exchange, String symbol, int instrumentType, String record) {
        if (!ExchangeStore.getSharedInstance().isDefault(exchange)) { // not a default exchange company. need to create

            DataStore.getSharedInstance().createStockObject(exchange, symbol, instrumentType).setSymbolEnabled(true);
            ValidatedSymbols.getSharedInstance().addSymbol(SharedMethods.getKey(exchange, symbol, instrumentType), record);
//            DataStore.getSharedInstance().addSymbolRequest(exchange, symbol);
            UpdateNotifier.setShapshotUpdated();

            Stock st = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrumentType);
//            if(st.getInstrumentType()!=7){
            OptimizedTreeRenderer.selectedSymbols.put(st.getKey(), true);
            st.setSymbolEnabled(true);
//            }
        }
    }

    private void createForexStock(String exchange, String symbol, int instrumentType, String record) {
        if (instrumentType == Meta.INSTRUMENT_FOREX) {
            ValidatedSymbols.getSharedInstance().addSymbol(SharedMethods.getKey(exchange, symbol, instrumentType), record);
            UpdateNotifier.setShapshotUpdated();
        }
    }

    private void updateValidatedSymbols(String exchange, String symbol, int instrumentType, String record) {
        ValidatedSymbols.getSharedInstance().addSymbol(SharedMethods.getKey(exchange, symbol, instrumentType), record);
        UpdateNotifier.setShapshotUpdated();
        String key = SharedMethods.getKey(exchange, symbol, instrumentType);
        String metaData = ValidatedSymbols.getSharedInstance().getFrame(key);
        if (metaData != null) {
            String[] fields = metaData.split(Meta.ID);
            int iType = Integer.parseInt(fields[2]);
            Stock stock = DataStore.getSharedInstance().createStockObject(exchange, symbol, iType);
            stock.setLongDescription(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[4])));
            stock.setShortDescription(UnicodeUtils.getNativeString(Language.getLanguageSpecificString(fields[8])));
        }

    }

    private String removeUnsubscribedExchanges(String frame) {
        StringBuilder buffer = new StringBuilder();
        String[] records = frame.split(Meta.FD);

        buffer.append(records[0]); // request ID
        buffer.append(Meta.FD);
        for (int i = 1; i < records.length; i++) {
            String[] fields = records[i].split(Meta.ID);
            String exchange = fields[0].trim();
            if (ExchangeStore.getSharedInstance().isValidExchange(exchange)) {       // && !ExchangeStore.isExpired(exchange) && !ExchangeStore.isInactive(exchange)
                buffer.append(records[i]); // request ID
                buffer.append(Meta.FD);
            }
        }
        return buffer.toString();
    }

    private String getSymbol(String exchange, String symbol, String code) {
        if ((symbol != null) && (code != null)) {
            SymbolMap.getSharedInstance().setMapData(exchange, code, symbol);
        } else if ((symbol == null) && (code != null)) {
            symbol = SymbolMap.getSharedInstance().getSymbolFor(exchange, code);
        }
        return symbol;
    }

    private String getExchange(String exchange, String code) {
        if ((exchange != null) && (code != null)) {
            ExchangeMap.setMapData(code, exchange);
        } else if ((exchange == null) && (code != null)) {
            exchange = ExchangeMap.getSymbolFor(code);
        }
        return exchange;
    }

    private void processStartOfShapShot(String exchangeSymbol) {
        System.out.println("Snapshot Receiveing for " + exchangeSymbol);
    }

    private void processEndOfShapShot(String exchangeSymbol) {
        System.out.println("Snapshot Received for " + exchangeSymbol);
        Exchange exchange = ExchangeStore.getSharedInstance().getExchange(exchangeSymbol);
        if ((exchange.isValidIinformationType(Meta.IT_SymbolTimeAndSales)) && (exchange.isValidIinformationType(Meta.IT_TimeAndSalesBacklog))) {
            TodaysTradeBacklogDownloadManager.getSharedInstance().startNewDownloadSession(value);
        }
//        } else {
        TodaysOHLCBacklogDownloadManager.getSharedInstance().startNewDownloadSession(exchange.getSymbol());
//        }
        Application.getInstance().fireSnapshotEnded(exchange);
        exchange = null;
    }

    /*private void validateSatUser(String user) {
        if (!Authenticator.USER_VALID) {
            Authenticator.getSharedInstance().validateUSer(user);
        }
    }*/

    private void requestBrokerList() {
        SendQFactory.addData(Constants.PATH_PRIMARY, (Meta.TRADING_BROKERS + Meta.DS + Settings.getUserID()));
    }

    public static void analyseBrokers(String frame) {
        try {
            if (!TradingShared.isConnected() && (!Settings.isSingleSignOnMode() || (Settings.isSingleSignOnMode() && Settings.isSsoTradingEnabled()))) {
                //sabb 1|sabb2,212.162.191.1,212.162.191.2!shb 1|shb2,212.162.162.1
                String[] frames = frame.split("!");
                int count = frames.length;

                if (count > 0) {
                    for (int i = 0; i < count; i++) {
                        String[] fields = frames[i].split(",");
                        String id = "" + i;//fields[0];
                        String descr = Language.getLanguageSpecificString(fields[0], "|");
//                        String[] ips = new String[fields.length - 1];
                        ArrayList<String> ipList = new ArrayList<String>();
                        short type = 0;
                        for (int j = 1; j < fields.length; j++) {
//                            ips[j - 1] = fields[j];
                            if(!fields[j].equalsIgnoreCase("")){
                                ipList.add(fields[j]);
                        }
                        }
                        Brokers.getSharedInstance().addBroker(id, descr, ipList, type);
                        System.out.println("BROKER LIST id : " + id);
                        System.out.println("BROKER LIST descrition : " + descr);
                        for (int k = 0; k < ipList.size(); k++) {
                            System.out.println("BROKER LIST ip : " + ipList.get(k));
                        }
                        fields = null;
                        id = null;
                        descr = null;
                        ipList = null;
                    }
                    TradingShared.setOkForTradeConnection(true);
                    Client.getInstance().getMenus().unlockTradeServerConnection();
                    if (!TWControl.isEnableWebLogin()) {
                        Client.getInstance().initiateTradeLogin();
                    }else{
                        Client.getInstance().mnu_TradingConnect_ActionPerformed(false);
                    }
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Web)) {
            TradingShared.setBrokerForSingleSignOn();
        }
//        else  if(Settings.isSingleSignOnMode() && (TWControl.getSSOType() == TWControl.SSO_TYPES.Direct)){
//            TradingShared.setBrokerForSSO();
//        }
    }

    private void showAnnouncement(String data) {
        try {
            String[] fields = data.split(Meta.FD);

            Announcement announcement;
            announcement = SearchedAnnouncementStore.getSharedInstance().getSearchedAnnouncement(fields[1]);
            if (announcement != null) {
                announcement.setMessage(UnicodeUtils.getNativeStringFromCompressed(fields[2]));
                try {
                    announcement.setHeadLine(UnicodeUtils.getNativeStringFromCompressed(fields[3]));
                } catch (Exception e) {
                }
                try {
                    announcement.setLanguage(UnicodeUtils.getNativeStringFromCompressed(fields[4]));
                } catch (Exception e) {
                }
                SearchedAnnouncementStore.getSharedInstance().showAnnouncement(announcement.getAnnouncementNo());
                announcement = null;
            } else {
                announcement = AnnouncementStore.getSharedInstance().getAnnouncement(fields[1]);
                announcement.setMessage(UnicodeUtils.getNativeStringFromCompressed(fields[2]));
                try {
                    announcement.setHeadLine(UnicodeUtils.getNativeStringFromCompressed(fields[3]));
                } catch (Exception e) {
                }
                try {
                    announcement.setLanguage(UnicodeUtils.getNativeStringFromCompressed(fields[4]));
                } catch (Exception e) {
                }
                AnnouncementStore.getSharedInstance().showAnnouncement(announcement.getAnnouncementNo());
                announcement.setOldMessage();
                announcement = null;
            }

            /*Announcement announcement;
            announcement = AnnouncementStore.getSharedInstance().getAnnouncement(fields[1]);
            if (announcement != null) {
                announcement.setMessage(UnicodeUtils.getNativeStringFromCompressed(fields[2]));
                AnnouncementStore.getSharedInstance().showAnnouncement(announcement.getAnnouncementNo());
                announcement.setOldMessage();
                announcement = null;
            } else {
                announcement = SearchedAnnouncementStore.getSharedInstance().getSearchedAnnouncement(fields[1]);
                announcement.setMessage(UnicodeUtils.getNativeStringFromCompressed(fields[2]));
                SearchedAnnouncementStore.getSharedInstance().showAnnouncement(announcement.getAnnouncementNo());
            }*/
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void showNews(String data) {
        try {
            String[] fields = data.split(Meta.FD);

            News news;
            news = NewsStore.getSharedInstance().getNews(fields[1]);

            if (news != null) {
                news.setBody(UnicodeUtils.getNativeStringFromCompressed(fields[3]));
                NewsStore.getSharedInstance().showNews(news.getNewsID());
                news.setNewMessage(false);
                news = null;
            } else {
                news = SearchedNewsStore.getSharedInstance().getSearchedNews(fields[1]);
                news.setBody(UnicodeUtils.getNativeStringFromCompressed(fields[3]));
                SearchedNewsStore.getSharedInstance().showNews(fields[1]);
                news.setNewMessage(false);
                news = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setDepthFrame(int type, String key, String frame, int decimalFactor) {

        BidAsk oBid = null;
        BidAsk oAsk = null;

        String[] data = frame.split(Meta.FD);
        char tag;
        int depth = -1;

        for (int i = 0; i < data.length; i++) {
            tag = data[i].charAt(0);
            switch (tag) {
                case '4':
                    depth = toInt(data[i].substring(1));
                    if (type == Meta.MESSAGE_TYPE_DEPTH_PRICE) {
                        oBid = getPriceOrder(key, depth, DepthObject.BID);
                        oAsk = getPriceOrder(key, depth, DepthObject.ASK);
                    } else if (type == Meta.MESSAGE_TYPE_DEPTH_REGULAR) {
                        oBid = getRegularOrder(key, depth, DepthObject.BID);
                        oAsk = getRegularOrder(key, depth, DepthObject.ASK);
                    } else if (type == Meta.MESSAGE_TYPE_DEPTH_SPECIAL) {
                        oBid = getSpecialOrder(key, depth, DepthObject.BID);
                        oAsk = getSpecialOrder(key, depth, DepthObject.ASK);
                    }
                    oBid.setDepthSequance((depth));
                    oAsk.setDepthSequance((depth));
                    break;
                case '5':
                    if (oAsk != null)
                        oAsk.setPrice(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    break;
                case '6':
                    if (oBid != null)
                        oBid.setPrice(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    break;
                case '7':
                    if (oAsk != null)
                        oAsk.setPrice(SharedMethods.getDouble(data[i].substring(1)) / decimalFactor);
                    if (oBid != null)
                        oBid.setPrice(oAsk.getPrice());
                    break;
                case '8':
                    if (oAsk != null) {
                        oAsk.setQuantity(SharedMethods.getLong(data[i].substring(1)));
                        setDepthSize(key, oAsk.getQuantity(), depth, DepthObject.ASK, (short) type);
                    }
                    break;
                case '9':
                    if (oBid != null) {
                        oBid.setQuantity(SharedMethods.getLong(data[i].substring(1)));
                        setDepthSize(key, oBid.getQuantity(), depth, DepthObject.BID, (short) type);
                    }
                    break;
                case ':':
                    if (oAsk != null) {
                        oAsk.setQuantity(SharedMethods.getLong(data[i].substring(1)));
                        setDepthSize(key, oAsk.getQuantity(), depth, DepthObject.ASK, (short) type);
                    }
                    if (oBid != null) {
                        oBid.setQuantity(oAsk.getQuantity());
                        setDepthSize(key, oBid.getQuantity(), depth, DepthObject.BID, (short) type);
                    }
                    break;
                case ';':
                    if (oAsk != null)
                        oAsk.setSplits(toInt(data[i].substring(1)));
                    break;
                case '<':
                    if (oBid != null)
                        oBid.setSplits(toInt(data[i].substring(1)));
                    break;
                case '=':
                    oAsk.setSplits(toInt(data[i].substring(1)));
                    oBid.setSplits(oAsk.getSplits());
                    break;
                case Stock.BATE_DEPTH_ASK_ORDER_NO:
                    if (oAsk != null) {
                        oAsk.setOrderNo(toLong(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BID_ORDER_NO:
                    if (oBid != null) {
                        oBid.setOrderNo(toLong(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BIDASK_ORDER_NO:
                    if (oAsk != null) {
                        oAsk.setOrderNo(toLong(data[i].substring(1)));
                    }
                    if (oBid != null) {
                        oBid.setOrderNo(toLong(data[i].substring(1)));
                    }
                    break;

                case Stock.BATE_DEPTH_ASK_MKT_CODE:
                    if (oAsk != null) {
                        oAsk.setMarketCode(data[i].substring(1));
                    }
                    break;
                case Stock.BATE_DEPTH_BID_MKT_CODE:
                    if (oBid != null) {
                        oBid.setMarketCode(data[i].substring(1));
                    }
                    break;
                case Stock.BATE_DEPTH_BIDASK_MKT_CODE:
                    if (oAsk != null) {
                        oAsk.setMarketCode(data[i].substring(1));
                    }
                    if (oBid != null) {
                        oBid.setMarketCode(data[i].substring(1));
                    }
                    break;

                case Stock.BATE_DEPTH_ASK_FILL_FLAG:
                    if (oAsk != null) {
                        oAsk.setFILL_flags(toInt(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BID_FILL_FLAG:
                    if (oBid != null) {
                        oBid.setFILL_flags(toInt(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BIDASK_FILL_FLAG:
                    if (oAsk != null) {
                        oAsk.setFILL_flags(toInt(data[i].substring(1)));
                    }
                    if (oBid != null) {
                        oBid.setFILL_flags(toInt(data[i].substring(1)));
                    }
                    break;

                case Stock.BATE_DEPTH_ASK_TIFF_FLAG:
                    if (oAsk != null) {
                        oAsk.setTIF_flags(toInt(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BID_TIFF_FLAG:
                    if (oBid != null) {
                        oBid.setTIF_flags(toInt(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BIDASK_TIFF_FLAG:
                    if (oAsk != null) {
                        oAsk.setTIF_flags(toInt(data[i].substring(1)));
                    }
                    if (oBid != null) {
                        oBid.setTIF_flags(toInt(data[i].substring(1)));
                    }
                    break;

                case Stock.BATE_DEPTH_ASK_MIN_AMOUNT:
                    if (oAsk != null) {
                        oAsk.setReg_Time(SharedMethods.getLong(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BID_MIN_AMOUNT:
                    if (oBid != null) {
                        oBid.setReg_Time(SharedMethods.getLong(data[i].substring(1)));
                    }
                    break;
                case Stock.BATE_DEPTH_BIDASK_MIN_AMOUNT:
                    if (oAsk != null) {
                        oAsk.setReg_Time(SharedMethods.getLong(data[i].substring(1)));
                    }
                    if (oBid != null) {
                        oBid.setReg_Time(SharedMethods.getLong(data[i].substring(1)));
                    }
                    break;
            }
        }
        oAsk = null;
        oBid = null;
    }

    public void clearTrailer(String symbol, short symbolType, BidAsk bid, BidAsk ask) {
        ArrayList list;

        switch (symbolType) {
            case Meta.MESSAGE_TYPE_DEPTH_REGULAR:
                /*if ((bid != null) && (bid.getQuantity() == 0)){
                   list = DepthStore.getInstance().getDepthFor(symbol).getOrderList(DepthObject.BID).getList();
                   removeZeros(bid.getDepthSequance(),list);
                }*/
                if ((ask != null) && (ask.getQuantity() == 0)) {
                    list = DepthStore.getInstance().getDepthFor(symbol).getOrderList(DepthObject.ASK).getList();
                    removeZeros(ask.getDepthSequance(), list);
                }

        }
    }

    private void removeZeros(int start, ArrayList list) {
        try {
            int size = list.size();
            //SharedMethods.printLine("size " + size + " loc " + start );
            for (int i = start; i < size; i++) {
                list.remove(list.size() - 1);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }

    /**
     * Extract the bid object fot the given depth
     */
    private BidAsk getRegularOrder(String symbol, int iDepth, byte type) {
        BidAsk bidask = null;
        try {
            //if (iDepth== -1) iDepth = 0;
            //return (BidAsk)oStock.getAsk(iDepth).clone();
            bidask = ((BidAsk) DepthStore.getInstance().getDepthFor(symbol).getOrderList(type).get(iDepth));
            if (bidask == null) {
                throw new Exception("Not found");
            }
        } catch (Exception e) {
            bidask = new BidAsk(symbol);
            //bidask.setDepthSequance((byte)iDepth);
            DepthStore.getInstance().getDepthFor(symbol).getOrderList(type).add(bidask);
        }
        return bidask;
    }

    /**
     * Extract the bid object fot the given depth
     */
    private BidAsk getPriceOrder(String symbol, int iDepth, byte type) {
        BidAsk bidask = null;
        try {
            bidask = ((BidAsk) DepthStore.getInstance().getDepthFor(symbol).getPriceList(type).get(iDepth));
            if (bidask == null) {
                throw new Exception("Not found");
            }
        } catch (Exception e) {
            bidask = new BidAsk(symbol);
            DepthStore.getInstance().getDepthFor(symbol).getPriceList(type).add(bidask);
        }
        return bidask;
    }

    private BidAsk getSpecialOrder(String symbol, int iDepth, byte type) {
        BidAsk bidask = null;
        try {
            bidask = ((BidAsk) DepthStore.getInstance().getDepthFor(symbol).getSpecialList(type).get(iDepth));
            if (bidask == null) {
                throw new Exception("Not found");
            }
        } catch (Exception e) {
            bidask = new BidAsk(symbol);
            DepthStore.getInstance().getDepthFor(symbol).getSpecialList(type).add(bidask);
        }
        return bidask;
    }


    private void setDepthSize(String symbol, long lQuantity, int depth, byte type, short shtSymbolType) {
        int currentSize = 0;
        switch (shtSymbolType) {
            case Meta.MESSAGE_TYPE_DEPTH_REGULAR:
                currentSize = DepthStore.getInstance().getDepthFor(symbol).getOrderListSize(type);
                break;
            case Meta.MESSAGE_TYPE_DEPTH_PRICE:
                currentSize = DepthStore.getInstance().getDepthFor(symbol).getPriceListSize(type);
                break;
            case Meta.MESSAGE_TYPE_DEPTH_SPECIAL:
                currentSize = DepthStore.getInstance().getDepthFor(symbol).getSpecialListSize(type);
        }

        if (lQuantity == 0) {
            if (currentSize >= (depth + 1)) {
                switch (shtSymbolType) {
                    case Meta.MESSAGE_TYPE_DEPTH_REGULAR:
                        DepthStore.getInstance().getDepthFor(symbol).setOrderListSize(type, depth);
                        break;
                    case Meta.MESSAGE_TYPE_DEPTH_PRICE:
                        DepthStore.getInstance().getDepthFor(symbol).setPriceListSize(type, depth);
                        break;
                    case Meta.MESSAGE_TYPE_DEPTH_SPECIAL:
                        DepthStore.getInstance().getDepthFor(symbol).setSpecialListSize(type, depth);
                }
            }
        } else {
            if (currentSize < (depth + 1)) {
                currentSize = (depth + 1);
                switch (shtSymbolType) {
                    case Meta.MESSAGE_TYPE_DEPTH_REGULAR:
                        DepthStore.getInstance().getDepthFor(symbol).setOrderListSize(type, currentSize);
                        break;
                    case Meta.MESSAGE_TYPE_DEPTH_PRICE:
                        DepthStore.getInstance().getDepthFor(symbol).setPriceListSize(type, currentSize);
                        break;
                    case Meta.MESSAGE_TYPE_DEPTH_SPECIAL:
                        DepthStore.getInstance().getDepthFor(symbol).setSpecialListSize(type, currentSize);
                }
            }
        }

    }

    /**
     * Sets the current market date
     */
    private void setMarketDate(String sDate) {
        HistorySettings.setLastConnectedDate(sDate);
        Client.setCurrentdate(sDate);

    }

    /**
     * Process the authentication message reply
     */
    private void processAuthMessage(String sMessageType) {
        int iMessage = Integer.parseInt(sMessageType);
        if (iMessage == 0) {
            new ShowMessage(false, Language.getString("PASSWORD_CHANGE_ERROR"), "E");
            Client.doChangePass();
        } else {
            new ShowMessage(Language.getString("MSG_PASSWORD_CHANGED"), "I");
            Settings.setUserDetails(Settings.getUserName(), "");
            Settings.setItemInBulk("PASSWORD", "");
        }
    }

    private void processLastEODDate(String sValue) {
        Client.setLatEodDate(toLong(sValue));
    }

    /**
     * Converts the given string to a long value
     */
    private long toLong(String sValue) {
        try {
            return Long.parseLong(sValue);
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Converts the given string to an int value
     */
    private int toInt(String sValue) {
        try {
            return Integer.parseInt(sValue);
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * Converts the given string to a short value
     */
    private short toShort(String sValue) throws Exception {

        return Short.parseShort(sValue);
    }

    /**
     * Sleeps the threag for a given delay
     */
    private void Sleep(long lDelay) {
        try {
            sleep(lDelay);
        } catch (Exception e) {
        }
    }


    public void requestHistoryCondition() {
        SendQFactory.addData(Constants.PATH_PRIMARY, Meta.LAST_EOD_DATE + Meta.DS + HistorySettings.getLastEODDate());
    }

    public void processCompleted(boolean ohlcDownloaded, Object object) {
        //SharedMethods.printLine("ohlc -- "+ ohlcDownloaded);
        //OHLCStore.getInstance().loadOHLCHistory();
    }

    public void processSarted(Object object) {

    }

    public String getBroker(String sBroker) {
        if (sBroker.trim().equals("")) {
            return Language.getString("NA");
        } else {
            return sBroker.trim();
        }
    }

    public String formatAnnouncementBody(String body) {
        return "&#" + body.replaceAll("\\|", ";&#");
    }

    public void setIntradayFileList(String fileList) {

        try {
            String[] fields = fileList.split(Meta.FD);

            String exchange = fields[0];
            int type;
            String name;
            long size;
            String record;
            String[] fileData;

            Exchange exchangeObject = ExchangeStore.getSharedInstance().getExchange(exchange);
            if ((exchangeObject == null) || (!exchangeObject.isDefault())) { // check if the exchange is subscribed to
                return; // u are not subscribed or not a default market
            }

            for (int i = 2; i < fields.length; i++) {
                record = fields[i];
                fileData = record.split(",");
                type = Integer.parseInt(fileData[0]);
                name = fileData[1];
                size = Long.parseLong(fileData[2]);


                switch (type) {
                    case Meta.ALL_ANNOUNCMENTS:
                        if (name.endsWith("_" + Language.getLanguageTag())) { // check if the correct language
                            if (!HistoryArchiveRegister.getSharedInstance().isSameSize(exchange, "Announcements_" + Language.getLanguageTag(), size)) {
                                if (!HistoryArchiveRegister.getSharedInstance().isInProgress(exchange, "Announcements_" + Language.getLanguageTag(), size)) {
                                    AnnouncementDownloadManager.getSharedInstance().addExchange(exchange, size);
                                }
                            }
                        } else {
                            HistoryArchiveRegister.getSharedInstance().removeEntry(exchange, name);
                        }
                        break;

                }

                /*if (!HistoryArchiveRegister.getSharedInstance().isSameSize(exchange, name, size)) {
                    if (!HistoryArchiveRegister.getSharedInstance().isInProgress(exchange, name, size)) {
                        System.out.println("Archive file [" + exchange + " " + name + "]. need to download");
                        switch (type){
                            case Meta.ALL_ANNOUNCMENTS:
                                if (name.endsWith("_" +  Language.getLanguageTag())){ // check if the correct language
                                    AnnouncementDownloadManager.getSharedInstance().addExchange(exchange);
                                }
                                break;

                        }
                    } else {
                        System.out.println("Archive file [" + exchange + " " + name + "]. download in progress. Ignored");
                    }
                } else {
                    System.out.println("Archive file [" + exchange + " " + name + "]. Same size, no need to download");
                }*/
                fileData = null;
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}

