package com.isi.csvr.smartalerts;

import com.isi.csvr.alert.AlertRecord;
import com.isi.csvr.shared.SharedMethods;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jan 18, 2009
 * Time: 10:07:32 PM
 * To change this template use File | Settings | File Templates.
 */
public class SmartAlertStore {
    private static ArrayList<SmartAlertFrame> store;
    private static SmartAlertStore self = null;

    private SmartAlertStore() {
        store = new ArrayList<SmartAlertFrame>();
//        SmartAlertFrame frame = new SmartAlertFrame("A1\u0019B1233062594733-1148500\u0019DA\u0019UTDWL~1010~0\u0019I1148500\u0019JTDWL~1010~0|4|2|100\u0019K1\u0019M1\u0019NT\u0019P1\u0019Q20090127132314\u0019V2");
//        SmartAlertFrame frame3 = new SmartAlertFrame("A1\u0019B1233062594736-1148500\u0019DA\u0019UTDWL~1010~0\u0019I1148500\u0019JTDWL~1010~0|4|2|100\u0019K1\u0019M3\u0019NP\u0019P1\u0019Q20090127132314\u0019V2");
//        SmartAlertFrame frame2 = new SmartAlertFrame("A1\u0019B1233125248626-123312311\u0019DA\u0019UTDWL~1010~0\u0019I123312311\u0019JTDWL~1010~0|0|3|2|1.11\u0018AND\u0018TDWL~1010~0|4|2|100\u0018AND\u0018TDWL~1010~0|2|3|1.01\u0019K1\u0019M2\u0019NP\u0019P1\u0019Q20090128064728\u0019V1");
//         new String("220\u0002A1\u0019B1233225538638-123312311\u0019DA\u0019UDFM~TAMWEEL~0\u0019I123312311\u0019JDFM~TAMWEEL~0|3|2|1.12\u0019K1\u0019M3\u0019NP\u0019P0\u0019Q20090129103858\u0019V1");
//
//        store.add(frame);
//        store.add(frame2);
//        store.add(frame3);
    }

    public synchronized static SmartAlertStore getSharedInstance() {
        if (self == null) {
            self = new SmartAlertStore();
        }
        return self;
    }

    public void setRecord(SmartAlertFrame record) {
        if (getRecord(record.getClientAlertID()) == null) {
            store.add(0,record);
        } else {
            updateRecord(record);
        }
    }

   
    public SmartAlertFrame getRecord(String ID) {
        for (int i = 0; i < store.size(); i++) {
            if ((store.get(i)).getClientAlertID().equals(ID)) {
                return store.get(i);
            }
        }
        return null;
    }

//      public boolean isAlreadyTriggered(String ID) {
//          for (int i = 0; i < store.size(); i++) {
//              if ((store.get(i)).getID().equals(ID)) {
//                  if (store.get(i).isTriggered()) {
//                      return true;
//                  }
//              }
//          }
//          return false;
//      }

    public void updateRecord(SmartAlertFrame record) {
        for (int i = 0; i < store.size(); i++) {
            if ((store.get(i)).getClientAlertID().equals(record.getClientAlertID())) {
                store.set(i, record);
            }
        }
    }

    public void deleteRecord(String id) {
        for (int i = 0; i < store.size(); i++) {
            if ((store.get(i)).getClientAlertID().equals(id)) {
                String sKey= store.get(i).getSKey();

                store.remove(i);
                updateAleartWindow(sKey, id);

            }
        }
    }

    public SmartAlertFrame getRecord(int i) {
        return store.get(i);
    }

    public ArrayList<SmartAlertFrame> getSmartAlertsStoreForSymbols() {
        return store;
    }

    public void clear() {
        store.clear();
        store.trimToSize();
    }

    public int size() {
        return store.size();
    }

    public ArrayList<SmartAlertFrame> getObjectFromsymbol(String symbol){
        ArrayList frameList = new ArrayList();
        for(int i=0;i<store.size();i++){
            SmartAlertFrame alertframetOb = store.get(i);
            String key = alertframetOb.getSKey();
           // if(key.contains(symbol)){
            if(key.equals(symbol)){
               frameList.add(alertframetOb);
            }
        }
        return frameList;
    }

    private void updateAleartWindow(String sKey, String id){
        SmartAlertWindow.getSharedInstance().updateActiveAlertTable(sKey, id);
    }
}
