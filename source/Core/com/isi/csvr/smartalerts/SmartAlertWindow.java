package com.isi.csvr.smartalerts;

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.tabbedpane.TWTabbedPane;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.Table;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.table.SmartFileChooser;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.event.ApplicationListener;
import com.isi.csvr.event.Application;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.symbolsearch.SymbolSearch;
import com.isi.csvr.datastore.Symbols;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.SymbolsRegistry;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.*;
import javax.swing.table.TableColumn;
import java.util.ArrayList;
import java.util.Date;
import java.awt.event.*;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.io.File;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jan 16, 2009
 * Time: 2:32:10 PM
 * To change this template use File | Settings | File Templates.
 * <p/>
 * Modified: Acording to new Reqs - 2009.10
 */
public class SmartAlertWindow extends InternalFrame implements ActionListener, Themeable, MouseListener, FocusListener, KeyListener, ApplicationListener {
    public static SmartAlertWindow self;
    private TWTabbedPane smartAlertTabPanel;
    private JPanel mainPanel;
    private JPanel upperTitlePanel;

    private JPanel stockdataPanel;
    private JPanel upperPanel;
    private JPanel upperLeftPanel;
    private JPanel upperRightPanel;
    private JPanel upperRightPanel1;
    private JPanel upperRightPanel2;
    private JPanel textboxPanel;

    private JPanel middleLeftPanel;
    private JPanel middleRightPanel;
    private JPanel middlePanel;
    private JPanel lowerPanel;
    private JPanel buttonPanel;
    private Table stocktable;
    private Table alerttable;
    private SmartAlertModel model;
    private SmartAlertSummaryModel smodel;
    private JPanel middleTopPanel;
    private JPanel middleBottomPanel;

    private JPanel middlePanel_1;
    private JPanel middlePanel_2;
    private JPanel middlePanel_3;
    private JPanel middlePanel_4;

    private JButton addLevelBtn;
    private JButton showSymbolBtn;
    private TWButton delLevelBtn;
    private TWButton summaryBtn;
    private JComboBox comboBox1;
    private TWComboBox cmbNotification;
    private TWComboBox cmbExpration;
    private TWComboBox cmbParameter;
    private TWComboBox cmbCriteria;
    private TWComboBox cmbConfig;

    private ToolBarButton btnSearch;
    private JLabel symbolLbl;
    private JLabel longDesLbl;
    private TWTextField textSymbol;


    private JLabel symbolBracketLbl;
    private TWTextField text;
    private ToolBarButton btnUp;
    private ToolBarButton btnDown;
    private TWCustomCheckBox popup;
    private TWCustomCheckBox playMedia;
    // private TWTextField symbolField;
    String selectedKey;
    private String exchange;                  // = "";
    private String skey = "";
    private String ssymbol;                   // = "";
    private int instrument = -1;

    private TWButton btnOk;
    private TWButton btnReset;
    private TWButton btnClose;
    private TWButton btnAdjuster;

    private JCheckBox chkPopup;
    private JCheckBox chkSMS;
    private JCheckBox chkEMail;

    private boolean isPopUp = false;
    private boolean isPlayMovie = false;
    public ArrayList<SmartAlertObject> dataArray1 = new ArrayList();
    private ArrayList<SmartAlertObject> dataArray2 = new ArrayList();

    private ArrayList<TWComboItem> criteriaList;
    private ArrayList<TWComboItem> parmList;
    private ArrayList<TWComboItem> expireList;
    private ArrayList<TWComboItem> notificationList;
    private ArrayList<SmartAlertObject> alertArray = new ArrayList<SmartAlertObject>();
    private ArrayList<TWComboItem> operatorList;
    private SmartAlertFrame frame;
    private boolean isEditMode = false;
    private SpecialTWTextField symbolField;
    private String baseSymbol;

    ViewSetting oSetting1;
    ViewSetting oSetting2;
    private boolean isSetValErrorShown = false;
    private boolean isPanelVisible = false;
    private boolean keyTyped;
    private ArrayList<SmartAlertObject> oldList;

    public static SmartAlertWindow getSharedInstance() {
        if (self == null) {
            self = new SmartAlertWindow();
        }
        return self;
    }

    private SmartAlertWindow() {
        smartAlertTabPanel = new TWTabbedPane(TWTabbedPane.TAB_PLACEMENT.Top, TWTabbedPane.CONTENT_PLACEMENT.Absolute, "nw");
        mainPanel = new JPanel();
        mainPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        createUI();
        getContentPane().add(mainPanel);
        setTitle(Language.getString("SMART_ALERT"));
        setSize(oSetting1.getSize());
        this.setMaximumSize(new Dimension(oSetting1.getSize().width, oSetting1.getSize().height));
        this.setMinimumSize(new Dimension(oSetting1.getSize().width, oSetting1.getSize().height));

        setResizable(true);
        setClosable(true);
        setDefaultCloseOperation(JFrame.HIDE_ON_CLOSE);
        this.setTable(stocktable);
        this.applySettings();
//        setLayer(GUISettings.TOP_LAYER);
        Client.getInstance().getDesktop().add(this);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        cmbExpration.setSelectedIndex(0);
        cmbNotification.setSelectedIndex(0);
        cmbConfig.setSelectedIndex(0);
        this.setPreferredSize(getPreferredSize());
        updateUI();
        doLayout();
        repaint();
        this.pack();
        this.doLayout();
        applyTheme();
        setLayer(GUISettings.TOP_LAYER);
        Application.getInstance().addApplicationListener(this);
    }

    private void createUI() {
        oSetting1 = ViewSettingsManager.getSummaryView("SMART_ALERT");
        oSetting1.setParent(this);
        oSetting1.setTableNumber(1);
        oSetting2 = ViewSettingsManager.getSummaryView("SMART_ALERT_SET");
        oSetting2.setParent(this);
        oSetting2.setTableNumber(2);
        mainPanel.add(createStockDataPanel());
        GUISettings.applyOrientation(this);
        GUISettings.applyOrientation(mainPanel);
        setLayer(GUISettings.TOP_LAYER);
    }

    private JPanel createStockDataPanel() {
        fillComboBoxes();
        cmbCriteria = new TWComboBox();
        cmbParameter = new TWComboBox();
        criteriaList = new ArrayList<TWComboItem>();
        parmList = new ArrayList<TWComboItem>();

        cmbCriteria.setModel(new TWComboModel(criteriaList));
        cmbParameter.setModel(new TWComboModel(parmList));

        dataArray1 = new ArrayList<SmartAlertObject>();
        stockdataPanel = new JPanel();
        stockdataPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"27", "120", "80", "25", "160"}, 0, 0));
        upperTitlePanel = new JPanel(new FlexGridLayout(new String[]{"5", "60", "5", "122", "10", "100%", "2", "140", "5"}, new String[]{"22"}, 0, 2));

        btnSearch = new ToolBarButton();
        btnSearch.setPreferredSize(new Dimension(20, 19));
        btnSearch.setToolTipText(Language.getString("SEARCH"));
        btnSearch.setEnabled(true);
        btnSearch.setVisible(true);
        btnSearch.setIcon(getIconFromString("search"));
        btnSearch.addMouseListener(this);
        upperTitlePanel.add(new JLabel());
        JLabel symbolLbl = new JLabel(Language.getString("SMART_ALERT_SYMBOL"));
        symbolLbl.setFont(new Font("Arial", 1, 12));
        upperTitlePanel.add(symbolLbl);
        upperTitlePanel.add(new JLabel());
        symbolLbl = new JLabel();
        textboxPanel = new JPanel(new FlexGridLayout(new String[]{"100", "22"}, new String[]{"20"}, 0, 0));
        textSymbol = new TWTextField();
        textboxPanel.add(textSymbol);
        textSymbol.setPreferredSize(new Dimension(100, 10));
        textSymbol.addFocusListener(this);
        textSymbol.addKeyListener(this);
        upperTitlePanel.add(textboxPanel);
        textboxPanel.add(btnSearch);
        upperTitlePanel.add(new JLabel());

        longDesLbl = new JLabel();
        longDesLbl.setFont(new Font("Arial", 1, 12));
        upperTitlePanel.add(longDesLbl);
        upperTitlePanel.add(new JLabel());
        upperPanel = new JPanel();
        upperPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"120"}, 0, 0));

        upperLeftPanel = new JPanel();
        upperLeftPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 2, 2));
        upperLeftPanel.add(getDataTable());
        setUpParameterColumn(stocktable, stocktable.getTable().getColumnModel().getColumn(2));
        setUpCriteriaColumn(stocktable, stocktable.getTable().getColumnModel().getColumn(3));  //--To add editable components
        setUpValueColumn(stocktable, stocktable.getTable().getColumnModel().getColumn(4));
        upperRightPanel = new JPanel();
        upperRightPanel.setLayout(new FlexGridLayout(new String[]{"80"}, new String[]{"90", "30"}, 2, 2));
        upperRightPanel1 = new JPanel();
        upperRightPanel1.setLayout(new FlexGridLayout(new String[]{"70"}, new String[]{"20", "20", "20"}, 2, 6));


        addLevelBtn = new JButton(Language.getString("ADD_FUNCTION_UNDERLINE"));
        addLevelBtn.setBorder(BorderFactory.createEmptyBorder());
        addLevelBtn.setCursor(new Cursor(Cursor.HAND_CURSOR));
        addLevelBtn.setContentAreaFilled(false);
        addLevelBtn.setIcon(getIconFromString("function_add"));
        addLevelBtn.setHorizontalAlignment(SwingConstants.LEFT);
        addLevelBtn.setMargin(new Insets(1, 1, 1, 1));
        addLevelBtn.addActionListener(this);
        addLevelBtn.setEnabled(false);

        delLevelBtn = new TWButton(Language.getString("DEL_LEVEL"));
        delLevelBtn.setIcon(getIconFromString("delete_level"));
        delLevelBtn.setHorizontalAlignment(SwingConstants.LEFT);
        delLevelBtn.setMargin(new Insets(1, 1, 1, 1));
        delLevelBtn.addActionListener(this);
        upperRightPanel1.add(delLevelBtn);
        btnReset = new TWButton(Language.getString("RESET"));
        btnReset.setIcon(getIconFromString("reset_level"));
        btnReset.setHorizontalAlignment(SwingConstants.LEFT);
        btnReset.setMargin(new Insets(1, 1, 1, 1));
        upperRightPanel1.add(btnReset);

        upperRightPanel.add(upperRightPanel1);
        upperRightPanel2 = new JPanel();
        upperRightPanel2.setLayout(new FlexGridLayout(new String[]{"15", "30%", "30%", "5"}, new String[]{"25"}, 5, 2));
        btnUp = new ToolBarButton();
        btnUp.setPreferredSize(new Dimension(24, 24));
        btnUp.setEnabled(true);
        btnUp.setVisible(true);
        btnUp.setIcon(getIconFromString("up"));
        btnUp.setRollOverIcon(getIconFromString("up"));
        btnUp.addActionListener(this);
        btnUp.addMouseListener(this);
        upperRightPanel2.add(new JLabel());
        btnDown = new ToolBarButton();
        btnDown.setPreferredSize(new Dimension(24, 24));
        btnDown.setEnabled(true);
        btnDown.setVisible(true);
        btnDown.setIcon(getIconFromString("down"));
        btnDown.setRollOverIcon(getIconFromString("down"));
        btnDown.addActionListener(this);
        btnDown.addMouseListener(this);
        upperRightPanel2.add(new JLabel());
        upperRightPanel.add(upperRightPanel2);
        upperPanel.add(upperLeftPanel);
        upperTitlePanel.add(addLevelBtn);
        upperTitlePanel.add(new JLabel());

        stockdataPanel.add(upperTitlePanel);
        stockdataPanel.add(upperPanel);
        stockdataPanel.setBorder(BorderFactory.createEtchedBorder());
        middlePanel = new JPanel();
        middlePanel.setLayout(new FlexGridLayout(new String[]{"190", "250", "150"}, new String[]{"80"}, 0, 2));

        JPanel middlePanel_2_bulk = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"73", "1"}, 3, 0));
        middlePanel_1 = new JPanel(new FlexGridLayout(new String[]{"85", "85"}, new String[]{"20", "20", "20"}, 4, 4));
        middlePanel_2 = new JPanel(new FlexGridLayout(new String[]{"78", "76", "80"}, new String[]{"100%"}, 0, 2));
        middlePanel_3 = new JPanel(new FlexGridLayout(new String[]{"90"}, new String[]{"20", "20"}, 10, 13));
        middlePanel_4 = new JPanel(new FlexGridLayout(new String[]{"5%", "90%", "5%"}, new String[]{"25"}, 0, 0));
        middleTopPanel = new JPanel(new FlexGridLayout(new String[]{"75", "85", "5", "95", "85", "5", "75", "85"}, new String[]{"100%"}, 4, 4));
        middleBottomPanel = new JPanel(new FlexGridLayout(new String[]{"75", "85", "70", "70", "40", "80", "5", "85"}, new String[]{"20"}, 4, 2));
        middlePanel_2_bulk.add(middlePanel_2);
        middlePanel_2_bulk.add(new JLabel());
        middleLeftPanel = new JPanel();
        middleLeftPanel.setLayout(new FlexGridLayout(new String[]{"75", "85"}, new String[]{"20", "20"}, 4, 4));

        JLabel notyfyLbl = new JLabel(Language.getString("NOTIFICATION"));
        notyfyLbl.setFont(new Font("Arial", 1, 12));
        middlePanel_1.add(notyfyLbl);
        middlePanel_1.add(cmbNotification);
        middleTopPanel.add(new JLabel());
        middleRightPanel = new JPanel();
        middleRightPanel.setLayout(new FlexGridLayout(new String[]{"10", "85", "85", "13", "100%"}, new String[]{"20", "24"}, 2, 2));

        middleRightPanel.add(new JLabel());
        middleRightPanel.add(new JLabel(Language.getString("ALERT_CONFIG")));
        setAlertConditions();
        popup = new TWCustomCheckBox(Language.getString("POPUP"), SwingConstants.LEADING, TWCustomCheckBox.CHECK_ONLY);
        popup.addActionListener(this);
        popup.addMouseListener(this);

        JLabel configLbl = new JLabel(Language.getString("ALERT_CONFIG"));
        configLbl.setFont(new Font("Arial", 1, 12));
        middlePanel_1.add(configLbl);
        middlePanel_1.add(cmbConfig);
        middleTopPanel.add(new JLabel());

        JLabel expireLbl = new JLabel(Language.getString("EXPIRATION"));
        expireLbl.setFont(new Font("Arial", 1, 12));
        middlePanel_1.add(expireLbl);
        middlePanel_1.add(cmbExpration);
        middlePanel_2.setBorder(BorderFactory.createTitledBorder(Language.getString("ALERT_MODE")));

        middlePanel.add(middlePanel_1);
        middlePanel.add(middlePanel_2_bulk);
        middlePanel.add(middlePanel_3);

        chkPopup = new JCheckBox(Language.getString("POPUP"));
        chkPopup.setSelected(true);
        chkPopup.addActionListener(this);
        chkPopup.setActionCommand("MODE");
        chkSMS = new JCheckBox(Language.getString("SMS"));
        chkSMS.addActionListener(this);
        chkSMS.setActionCommand("MODE");
        chkEMail = new JCheckBox(Language.getString("EMAIL"));
        chkEMail.addActionListener(this);
        chkEMail.setActionCommand("MODE");
        setAlertModeValidity();

        middleBottomPanel.add(new JLabel(Language.getString("ALERT_MODE")));
        middlePanel_2.add(chkPopup);
        middlePanel_2.add(chkSMS);
        middlePanel_2.add(chkEMail);
        middleBottomPanel.add(new JLabel());

        summaryBtn = new TWButton(Language.getString("VIEW_ALL_ALERTS"));
        summaryBtn.addActionListener(this);
        summaryBtn.setMargin(new Insets(1, 1, 1, 1));
        JPanel summarypan = new JPanel(new FlexGridLayout(new String[]{"100%", "70"}, new String[]{"20"}, 3, 0));
        summarypan.add(new JPanel());
        middleRightPanel.add(new JLabel());
        middleRightPanel.add(new JLabel(""));
        playMedia = new TWCustomCheckBox(Language.getString("PLAY_MEDIA"), SwingConstants.LEADING, TWCustomCheckBox.CHECK_ONLY);
        playMedia.addActionListener(this);
        playMedia.addMouseListener(this);
        middleRightPanel.add(new JLabel(""));
        text = new TWTextField();
        middleRightPanel.add(new JLabel(""));

        JPanel okpanel = new JPanel(new FlexGridLayout(new String[]{"100%", "70"}, new String[]{"20"}, 3, 4));
        okpanel.add(new JPanel());
        btnOk = new TWButton(Language.getString("APPLY"));
        btnOk.setMargin(new Insets(1, 1, 1, 1));

        middlePanel_3.add(summaryBtn);
        middleBottomPanel.add(new JLabel());
        middlePanel_3.add(btnOk);
        btnAdjuster = new TWButton();
        btnAdjuster.addActionListener(this);
        showSymbolBtn = new JButton();
        if (isPanelVisible) {
            getShowSymbolBtn().setText(Language.getString("HIDE_ALERT_SYMBOL_RESULTS"));
        } else {
            getShowSymbolBtn().setText(Language.getString("SEE_ALERT_SYMBOL_RESULTS"));
        }
        showSymbolBtn.setBorder(BorderFactory.createEmptyBorder());
        showSymbolBtn.setCursor(new Cursor(Cursor.HAND_CURSOR));
        showSymbolBtn.setContentAreaFilled(false);
        showSymbolBtn.setHorizontalAlignment(SwingConstants.CENTER);
        showSymbolBtn.setMargin(new Insets(1, 1, 1, 1));
        showSymbolBtn.addActionListener(this);
        symbolBracketLbl = new JLabel();
        middlePanel_4.add(new JLabel());
        middlePanel_4.add(showSymbolBtn);
        middlePanel_4.add(new JLabel());

        stockdataPanel.add(middlePanel);
        stockdataPanel.add(middlePanel_4);
        lowerPanel = new JPanel();
        lowerPanel.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}, 0, 0));
        lowerPanel.add(getAlertDataTable());
        lowerPanel.setBorder(BorderFactory.createTitledBorder(Language.getString("ACTIVE_ALERTS")));
        stockdataPanel.add(lowerPanel);
        buttonPanel = new JPanel();
        buttonPanel.setLayout(new FlexGridLayout(new String[]{"100%", "80", "80", "80"}, new String[]{"20"}, 3, 3));
        buttonPanel.add(new JLabel());

        btnClose = new TWButton(Language.getString("CLOSE"));
        addListeners();
        Theme.registerComponent(this);
        return stockdataPanel;
    }

    private boolean doInternalValidation(String symbol) {
        symbol = symbol.toUpperCase();
        this.selectedKey = SymbolMaster.getExchangeForSymbol(symbol, false);
        if (selectedKey != null) {
            this.exchange = SharedMethods.getExchangeFromKey(selectedKey);
            this.ssymbol = SharedMethods.getSymbolFromKey(selectedKey);
            instrument = SharedMethods.getInstrumentTypeFromKey(selectedKey);
            if (this.ssymbol != null) {
                Stock st = DataStore.getSharedInstance().getStockObject(selectedKey);
                baseSymbol = st.getSymbol();
                if (dataArray1 != null && dataArray1.size() > 0) {
                    for (int i = 0; i < dataArray1.size(); i++) {
                        SmartAlertObject sOb = dataArray1.get(i);
                        sOb.setSymbol(baseSymbol);
                        sOb.setSKey(selectedKey);
                        sOb.setSymbolDescription(DataStore.getSharedInstance().getShortDescription(selectedKey));
                    }
                }

                showSymbolTree(selectedKey);
                getTextSymbol().setText(SharedMethods.getSymbolFromKey(selectedKey));
                if (st != null) {
                    getLongDesLbl().setText(st.getLongDescription());
                    addLevelBtn.setEnabled(true);
                    showSymbolBtn.setEnabled(true);
                    if (isPanelVisible) {
                        getShowSymbolBtn().setText(Language.getString("HIDE_ALERT_SYMBOL_RESULTS") + "<html><u><font color=blue>" + "  " + "(" + SharedMethods.getSymbolFromKey(selectedKey) + ")" + "</font></u>");
                    } else {
                        getShowSymbolBtn().setText(Language.getString("SEE_ALERT_SYMBOL_RESULTS") + "<html><u><font color=blue>" + "  " + "(" + SharedMethods.getSymbolFromKey(selectedKey) + ")" + "</font></u>");
                    }
                }

            }
            return true;
        } else {
            return false;
        }
    }

    public JLabel getSymbolLbl() {
        return symbolLbl;
    }

    public void setSymbolLbl(JLabel symbolLbl) {
        this.symbolLbl = symbolLbl;
    }

    public TWTextField getTextSymbol() {
        return textSymbol;
    }

    public void setTextSymbol(TWTextField textSymbol) {
        this.textSymbol = textSymbol;
    }

    public JLabel getLongDesLbl() {
        return longDesLbl;
    }

    public void setLongDesLbl(JLabel longDesLbl) {
        this.longDesLbl = longDesLbl;
    }

    public JLabel getSymbolBracketLbl() {
        return symbolBracketLbl;
    }

    public void setSymbolBracketLbl(JLabel symbolBracketLbl) {
        this.symbolBracketLbl = symbolBracketLbl;
    }

    public void addListeners() {
        btnClose.addActionListener(this);
        btnOk.addActionListener(this);
        btnReset.addActionListener(this);
    }

    public void applyTheme() {
        btnUp.setIcon(getIconFromString("up"));
        btnDown.setIcon(getIconFromString("down"));
        addLevelBtn.setIcon(getIconFromString("function_add"));
        delLevelBtn.setIcon(getIconFromString("delete_level"));
    }

    private void setAlertConditions() {
        cmbConfig = new TWComboBox();
        operatorList = new ArrayList<TWComboItem>();
        cmbConfig.setModel(new TWComboModel(operatorList));
        SmartAlertUtils.getFilledOperatorCombo(operatorList);
    }

    public void actionPerformed(ActionEvent e) {
        if (stocktable.getTable().isEditing()) {
            if (stocktable.getTable().getCellEditor() != null) {
                isSetValErrorShown = false;
                stocktable.getTable().getCellEditor().stopCellEditing();
            }
        } else {
            isSetValErrorShown = false;
        }
        if (isSetValErrorShown) {
            isSetValErrorShown = false;
            return;
        }
        if (e.getSource() == addLevelBtn) {
            showSettingsDialog();

        } else if (e.getSource() == delLevelBtn) {
            deleteRow();
        } else if (e.getSource() == btnUp) {
            moveUp();
        } else if (e.getSource() == btnDown) {
            moveDown();
        } else if (e.getSource().equals(btnOk)) {
            if ((!Settings.isConnected())) {
                new ShowMessage(Language.getString("MSG_NOT_CONNECTED_MESSAGE"), "E");
                return;
            }
            sendMessage();
        } else if (e.getSource().equals(btnReset)) {
            resetStore();
        } else if (e.getSource().equals(btnClose)) {
            closeSmartWindow();
        } else if (e.getActionCommand().equals("MODE")) {
            validateCheckBoxes(e.getSource());
        } else if (e.getSource().equals(summaryBtn)) {
            if (!SmartAlertSummeryWindow.getSharedInstance().isVisible()) {
                SmartAlertSummeryWindow.getSharedInstance().setVisible(true);
            } else {
                SmartAlertSummeryWindow.getSharedInstance().setVisible(false);
            }
        } else if (e.getSource() == showSymbolBtn) {
            isPanelVisible = !isPanelVisible;
            this.setPreferredSize(getPreferredSize());
            if (selectedKey != null && !getTextSymbol().getText().trim().equals("")) {
                if (isPanelVisible) {
                    getShowSymbolBtn().setText(Language.getString("HIDE_ALERT_SYMBOL_RESULTS") + "<html><u><font color=blue>" + "  " + "(" + SharedMethods.getSymbolFromKey(selectedKey) + ")" + "</font></u>");
                } else {
                    getShowSymbolBtn().setText(Language.getString("SEE_ALERT_SYMBOL_RESULTS") + "<html><u><font color=blue>" + "  " + "(" + SharedMethods.getSymbolFromKey(selectedKey) + ")" + "</font></u>");
                }
            } else {
                if (isPanelVisible) {
                    getShowSymbolBtn().setText(Language.getString("HIDE_ALERT_SYMBOL_RESULTS"));
                } else {
                    getShowSymbolBtn().setText(Language.getString("SEE_ALERT_SYMBOL_RESULTS"));
                }
            }
            updateUI();
            doLayout();
            repaint();
            this.pack();
            this.doLayout();
        }
    }

    private void showSettingsDialog() {
        SmartAlertSettingsDialog rUI = new SmartAlertSettingsDialog(Client.getInstance().getFrame(), this, Language.getString("SMART_ALERT_ADD_LEVEL"), true, selectedKey, true);
        rUI.reloadParameterList(SharedMethods.getInstrumentTypeFromKey(selectedKey));
    }

    private int getAlertMode() {
        int result = 0;

        if (chkEMail.isSelected()) {
            result += Meta.ALERT_MODE_EMAIL;
        }
        if (chkPopup.isSelected()) {
            result += Meta.ALERT_MODE_TW;
        }
        if (chkSMS.isSelected()) {
            result += Meta.ALERT_MODE_SMS;
        }

        return result;
    }

    private void setAlertMode(int mode) {
        if ((mode & Meta.ALERT_MODE_EMAIL) > 0) {
            chkEMail.setSelected(true);
        } else {
            chkEMail.setSelected(false);
        }
        if ((mode & Meta.ALERT_MODE_TW) > 0) {
            chkPopup.setSelected(true);
        } else {
            chkPopup.setSelected(false);
        }
        if ((mode & Meta.ALERT_MODE_SMS) > 0) {
            chkSMS.setSelected(true);
        } else {
            chkSMS.setSelected(false);
        }
    }

    private void setAlertModeValidity() {
        /*if((mode & Meta.ALERT_MODE_EMAIL)>0){
            chkEMail.setEnabled(true);
        }else{
            chkEMail.setEnabled(false);
        }

        chkPopup.setEnabled(true);

        if((mode & Meta.ALERT_MODE_SMS)>0){
            chkSMS.setEnabled(true);
        }else{
            chkSMS.setEnabled(false);
        }*/

        chkEMail.setEnabled(Client.getInstance().isValidSystemWindow(Meta.IT_Alert_EMail, true));
        chkSMS.setEnabled(Client.getInstance().isValidSystemWindow(Meta.IT_Alerts_SMS, true));
    }

    private void validateCheckBoxes(Object source) {
        if (!(chkEMail.isSelected() || chkPopup.isSelected() || chkSMS.isSelected())) { // if none is selected
            ((JCheckBox) source).setSelected(true); // tick the last clicked checkbox
        }
    }

    private void closeSmartWindow() {
        try {
            for (int i = 0; i <= dataArray1.size(); i++) {
                dataArray1.remove(0);
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        this.setVisible(false);
    }

    private void resetStore() {
        clearAll();
        dataArray2.clear();

    }

    public void mouseClicked(MouseEvent e) {
        if (e.getSource().equals(popup)) {
            popup.setSelected(!isPopUp);
            isPopUp = !isPopUp;
        } else if (e.getSource().equals(playMedia)) {
            playMedia.setSelected(!isPlayMovie);
            isPlayMovie = !isPlayMovie;
        }
        if (e.getSource().equals(btnSearch)) {
            if (!isEditMode) {
                searchSymbol(3);
            } else {
                new ShowMessage(Language.getString("ALERT_SEARCH_WARNING"), "W");
            }
        }
    }


    private void fileSelector() {
        File file = getSelecedFile();
        text.setText(file.getName());
    }

    private void deleteRow() {
        int rowindex = stocktable.getTable().getSelectedRow();
        dataArray1.remove(rowindex);
    }
                                                                                                    
    private void editRow() {
        int rowindex = stocktable.getTable().getSelectedRow();
        SmartAlertObject editOb = dataArray1.get(rowindex);
        editSelectedCondition(editOb);
    }

    private void editSelectedCondition(SmartAlertObject editOb) {
        SmartAlertSettingsDialog rUI = new SmartAlertSettingsDialog(Client.getInstance().getFrame(), this, Language.getString("SMART_ALERT_ADD_LEVEL"), true, editOb, false);
        rUI.reloadParameterList(SharedMethods.getInstrumentTypeFromKey(selectedKey));
    }

    public void moveUp() {
        int rowindex = stocktable.getTable().getSelectedRow();
        if (rowindex != 0) {
            SmartAlertObject movingupOb = (SmartAlertObject) dataArray1.get(rowindex);
            dataArray1.remove(rowindex);
            SmartAlertObject movingdwnOb = (SmartAlertObject) dataArray1.get(rowindex - 1);
            dataArray1.remove(rowindex - 1);
            dataArray1.add(rowindex - 1, movingupOb);
            dataArray1.add(rowindex, movingdwnOb);
            setRowStockdataTable(rowindex - 1);
        }
    }

    public void moveDown() {
        int rowindex = stocktable.getTable().getSelectedRow();
        if (rowindex < dataArray1.size() - 1) {
            SmartAlertObject movingupOb = (SmartAlertObject) dataArray1.get(rowindex + 1);
            dataArray1.remove(rowindex + 1);
            SmartAlertObject movingdwnOb = (SmartAlertObject) dataArray1.get(rowindex);
            dataArray1.remove(rowindex);
            dataArray1.add(rowindex, movingupOb);
            dataArray1.add(rowindex + 1, movingdwnOb);
            setRowStockdataTable(rowindex + 1);
        }
    }

    public void setRowStockdataTable(int rowIndex) { // --Data window--
        stocktable.getTable().setRowSelectionInterval(rowIndex, rowIndex);
        Rectangle r = stocktable.getTable().getCellRect(rowIndex, 0, true);
        stocktable.getTable().scrollRectToVisible(stocktable.getTable().getCellRect(stocktable.getTable().getRowCount() - 1, 0, true));
        stocktable.getTable().scrollRectToVisible(r);
        stocktable.scrollRectToVisible(stocktable.getTable().getCellRect(rowIndex - 1, 0, true));
    }

    public void addNewObject() {

        if (dataArray1.size() > 0 && (dataArray1.get(0).getSymbol() == null || dataArray1.get(0).getSymbol().isEmpty())) {
            new ShowMessage(Language.getString("SMART_ALERT_NO_SYMBOL"), "E");
            return;
        } else if (dataArray1.size() > 0 && getParameterLogicString() == null) {
            new ShowMessage(Language.getString("SMART_ALERT_INVALID_FIST_ROW"), "E");
            return;
        }
        SmartAlertObject s1 = new SmartAlertObject();
        if (dataArray1.size() > 0) {
            s1.setSymbol(dataArray1.get(0).getSymbol());
            s1.setSKey(dataArray1.get(0).getSKey());
            s1.setSymbolDescription(DataStore.getSharedInstance().getShortDescription(dataArray1.get(0).getSKey()));
        }
        dataArray1.add(s1);
    }


    public void addNewAlertObject(SmartAlertObject alertObject) {
        if (dataArray1.size() > 4) {
            new ShowMessage(Language.getString("ALERT_CONDITION_WARNING"), "W");
        } else {
            dataArray1.add(alertObject);
        }
    }

    public Icon getIconFromString(String iconFile) {
        Icon icn = null;
        try {
            icn = new ImageIcon(Theme.getTheamedImagePath(iconFile));
        } catch (Exception ex) {

        }
        return icn;
    }

    private Table getDataTable() {
        stocktable = new Table(new int[]{2});  //-Table 1
        model = new SmartAlertModel(dataArray1);
        model.setViewSettings(oSetting1);
        oSetting1.setParent(this);
        stocktable.setModel(model);
        model.setTable(stocktable, new SmartAlertRenderer());
     //   stocktable.getTable().getTableHeader().setReorderingAllowed(false);

        this.setTable(stocktable);
        stocktable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
//				super.mouseClicked(e);	//To change body of overridden methods use File | Settings | File Templates.
                int col = stocktable.getTable().columnAtPoint(e.getPoint());
                int row = stocktable.getTable().rowAtPoint(e.getPoint());

                if ((col == 1) && (row == 0)) {
                }
                if (col == 0) {
                    deleteRow();
                }
                if (col == 1) {
                    editRow();
                }
            }
        });
//
//
//        stocktable.getTable().addKeyListener(new KeyAdapter() {
//            public void keyReleased(KeyEvent e) {
//                int column = stocktable.getTable().getSelectedColumn();
//                if (e.getKeyCode() == KeyEvent.VK_ENTER) {
//                    if (column == 4) {
//                    }
//                }
//            }
//        });

        return stocktable;
    }

    private Table getAlertDataTable() {
        alerttable = new Table();  //-Table 2
        smodel = new SmartAlertSummaryModel(dataArray2);
        smodel.setViewSettings(oSetting2);
        oSetting2.setParent(this);
        alerttable.setModel(smodel);
        smodel.setTable(alerttable, new SmartRenderer());
        alerttable.getTable().getTableHeader().setReorderingAllowed(false);
        this.setTable2(new Table[]{alerttable});

        alerttable.getTable().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                int col = alerttable.getTable().columnAtPoint(e.getPoint());
                int row = alerttable.getTable().rowAtPoint(e.getPoint());
                if (col == 1) { // expander column
                    Object state = smodel.getValueAt(row, 1);
                    int status = Integer.parseInt(state.toString());
                    String clientid = (String) smodel.getValueAt(row, -1);
                    if (status == 2) {
                        showExpandedTree(clientid);
                    } else if (status == 1) {
                        showCollapsedTree(clientid);
                    }
                }
                repaint();
            }
        });

        alerttable.getModel().updateGUI();
        return alerttable;
    }

    public void setUpSymbolColumn(Table table, TableColumn ctiteriaColumn) {
        comboBox1 = new TWComboBox();
        comboBox1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                searchSymbol(3);
                if (stocktable.getTable().getCellEditor() != null) {
                    stocktable.getTable().getCellEditor().stopCellEditing();
                }
            }
        });
        ctiteriaColumn.setCellEditor(new DefaultCellEditor(comboBox1));
    }

    public void searchSymbol(int index) {
        try {
            SimpleDateFormat dateFormat = new SimpleDateFormat("yyyy/MM/dd");
            Symbols symbols = new Symbols();
            SymbolSearch oCompanies = SymbolSearch.getSharedInstance();
            oCompanies.setTitle(Language.getString("SMART_ALERT"));
            oCompanies.setSingleMode(true);
            oCompanies.setIndexSearchMode(false);
            oCompanies.init();
            oCompanies.setSymbols(symbols);
            oCompanies.setShowDefaultExchangesOnly(false);
            oCompanies.showDialog(true);
            oCompanies = null;
            String key = null;
            try {
                key = symbols.getSymbols()[0];
            } catch (Exception e) {
                return;
            }
            selectedKey = key;

            Stock st = DataStore.getSharedInstance().getStockObject(key);
            baseSymbol = st.getSymbolCode();
            if (dataArray1 != null && dataArray1.size() > 0) {
                for (int i = 0; i < dataArray1.size(); i++) {
                    SmartAlertObject sOb = dataArray1.get(i);
                    sOb.setSymbol(baseSymbol);
                    sOb.setSKey(key);
                    sOb.setSymbolDescription(DataStore.getSharedInstance().getShortDescription(key));
                }
            }

            showSymbolTree(selectedKey);
            getTextSymbol().setText(SharedMethods.getSymbolFromKey(selectedKey));
            if (st != null) {
                getLongDesLbl().setText(st.getLongDescription());
                addLevelBtn.setEnabled(true);
                showSymbolBtn.setEnabled(true);
                if (isPanelVisible) {
                    getShowSymbolBtn().setText(Language.getString("HIDE_ALERT_SYMBOL_RESULTS") + "<html><u><font color=blue>" + "  " + "(" + SharedMethods.getSymbolFromKey(selectedKey) + ")" + "</font></u>");
                } else {
                    getShowSymbolBtn().setText(Language.getString("SEE_ALERT_SYMBOL_RESULTS") + "<html><u><font color=blue>" + "  " + "(" + SharedMethods.getSymbolFromKey(selectedKey) + ")" + "</font></u>");
                }
            }
            symbols = null;
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public JButton getShowSymbolBtn() {
        return showSymbolBtn;
    }

    public void setShowSymbolBtn(JButton showSymbolBtn) {
        this.showSymbolBtn = showSymbolBtn;
    }

    public void showSymbolTree(String baseSymbol) {
        dataArray2.clear();
        int rownum = 0;
        ArrayList<SmartAlertFrame> frameList = SmartAlertStore.getSharedInstance().getObjectFromsymbol(baseSymbol);
        ArrayList<SmartAlertObject> subObList;
        for (int i = 0; i < frameList.size(); i++) {
            SmartAlertFrame alertFrame = frameList.get(i);
            subObList = alertFrame.getSubAlertObjects();
            int noOfchildren = 0;
            boolean isBaseob = false;
            if (subObList.size() > 1) {
                noOfchildren = 1;
                isBaseob = true;
            }
            for (int j = 0; j < subObList.size(); j++) {  //--- To add all abjects
                SmartAlertObject smartOb = subObList.get(j);
                smartOb.setRownum(rownum);
                if (noOfchildren == 1) {
                    if (isBaseob) {
                        smartOb.setStatus(2);
                    } else {
                        smartOb.setStatus(0);
                    }
                } else {
                    smartOb.setStatus(0);
                }
                if (j == 0) {
                    if (alertFrame.getAleartStatus().equals("P")) {
                        dataArray2.add(smartOb);
                    }
                    isBaseob = false;
                }
            }
            rownum = rownum + 1;
        }
    }

    private void showExpandedTree(String clientid) {
        dataArray2.clear();
        ArrayList<SmartAlertFrame> frameList = SmartAlertStore.getSharedInstance().getObjectFromsymbol(selectedKey);
        ArrayList<SmartAlertObject> subObList;
        for (int i = 0; i < frameList.size(); i++) {
            SmartAlertFrame alertFrame = frameList.get(i);
            subObList = alertFrame.getSubAlertObjects();
            int noOfchildren = 0;
            if (subObList.size() > 1) {
                noOfchildren = 1;
            }
            int childNum = 2;
            boolean isExpanded = false;
            int status = 0;
            String id = alertFrame.getClientAlertID();
            if (id.equals(clientid)) {
                for (int j = 0; j < subObList.size(); j++) {  //--- To add all abjects
                    SmartAlertObject smartOb = subObList.get(j);
                    status = smartOb.getStatus();
                    if (status == 2) {
                        smartOb.setStatus(1);
                    } else {
                        smartOb.setStatus(0);
                    }
                    if (alertFrame.getAleartStatus().equals("P")) {
                        dataArray2.add(smartOb);
                    }
                }
            } else {
                for (int j = 0; j < subObList.size(); j++) {  //--- To add all abjects
                    SmartAlertObject smartOb = subObList.get(j);
                    status = smartOb.getStatus();
                    if (status == 2) {
                        smartOb.setStatus(2);
                    } else if (status == 1) {
                        smartOb.setStatus(1);
                        isExpanded = true;
                    } else {
                        smartOb.setStatus(0);
                    }
                    if (isExpanded) {
                        if (alertFrame.getAleartStatus().equals("P")) {
                            dataArray2.add(smartOb);
                        }
                    } else {
                        if (j == 0) {
                            if (alertFrame.getAleartStatus().equals("P")) {
                                dataArray2.add(smartOb);
                            }
                        }
                    }
                }
            }
        }
    }

    private void showCollapsedTree(String clientid) {
        dataArray2.clear();
        ArrayList<SmartAlertFrame> frameList = SmartAlertStore.getSharedInstance().getObjectFromsymbol(selectedKey);
        ArrayList<SmartAlertObject> subObList;
        for (int i = 0; i < frameList.size(); i++) {
            SmartAlertFrame alertFrame = frameList.get(i);
            subObList = alertFrame.getSubAlertObjects();
            int noOfchildren = 0;
            if (subObList.size() > 1) {
                noOfchildren = 1;
            }
            boolean isExpanded = true;
            int status = 0;
            String id = alertFrame.getClientAlertID();
            if (id.equals(clientid)) {
                for (int j = 0; j < subObList.size(); j++) {  //--- To add all abjects
                    SmartAlertObject smartOb = subObList.get(j);
                    status = smartOb.getStatus();
                    if (status == 1) {
                        smartOb.setStatus(2);
                        isExpanded = false;
                    } else {
                        smartOb.setStatus(0);
                    }
                    if (isExpanded) {
                        if (alertFrame.getAleartStatus().equals("P")) {
                            dataArray2.add(smartOb);
                        }
                    } else {
                        if (j == 0) {
                            if (alertFrame.getAleartStatus().equals("P")) {
                                dataArray2.add(smartOb);
                            }
                        }
                    }
                }
            } else {
                for (int j = 0; j < subObList.size(); j++) {  //--- To add all abjects
                    SmartAlertObject smartOb = subObList.get(j);
                    status = smartOb.getStatus();
                    if (status == 2) {
                        smartOb.setStatus(2);
                        isExpanded = false;
                    } else if (status == 1) {
                        smartOb.setStatus(1);
                        isExpanded = true;
                    } else {
                        smartOb.setStatus(0);
                    }
                    if (isExpanded) {
                        if (alertFrame.getAleartStatus().equals("P")) {
                            dataArray2.add(smartOb);
                        }
                    } else {
                        if (j == 0) {
                            if (alertFrame.getAleartStatus().equals("P")) {
                                dataArray2.add(smartOb);
                            }
                        }
                    }
                }
            }
        }
    }


    public void setUpCriteriaColumn(Table table, TableColumn ctiteriaColumn) {
        SmartAlertUtils.getFilledCondtionCombo(criteriaList);
        ctiteriaColumn.setCellEditor(new DefaultCellEditor(cmbCriteria));
    }

    public void setUpParameterColumn(Table table, TableColumn ctiteriaColumn) {
        SmartAlertUtils.getFilledParameterComboForStock(parmList);
        ctiteriaColumn.setCellEditor(new DefaultCellEditor(cmbParameter));
    }

    public void reloadParameterList(int instrument) {
        parmList.clear();
        if (instrument == Meta.INSTRUMENT_EQUITY) {
            SmartAlertUtils.getFilledParameterComboForStock(parmList);
        } else if (instrument == Meta.INSTRUMENT_INDEX) {

            SmartAlertUtils.getFilledParameterComboForStockIndex(parmList);
        } else if (instrument == Meta.INSTRUMENT_OPTION || instrument == Meta.INSTRUMENT_FUTURE) {

            SmartAlertUtils.getFilledParameterComboForStockDerivative(parmList);
        } else {
            SmartAlertUtils.getFilledParameterComboForStock(parmList);
        }

    }


    public void setUpValueColumn(Table table, final TableColumn ctiteriaColumn) {
        final TWTextField text = new TWTextField();
        ctiteriaColumn.setCellEditor(new DefaultCellEditor(text));

        /*   stocktable.getTable().getTableHeader().addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                //  if(stocktable.getTable().isEditing() && stocktable.getTable().getEditingColumn()==3){
                int row = stocktable.getTable().getSelectedRow();
                String currentVal = (String) stocktable.getTable().getValueAt(row, 3);
                String txtVal = text.getText();


                if (stocktable.getTable().getValueAt(stocktable.getTable().getSelectedRow(), 1) != null) {
                    // stocktable.getModel().setValueAt(text.getText(),stocktable.getTable().getSelectedRow(),3);
                    if (currentVal == null && text.getText() != null && !text.getText().isEmpty()) {
                        stocktable.getModel().setValueAt(text.getText(), row, 3);
                    } else
                    if (currentVal != null && !currentVal.isEmpty() && txtVal != null && !currentVal.equals(txtVal)) {
                        stocktable.getModel().setValueAt(text.getText(), row, 3);
                    }
                }
            }
        });*/
    }

    private File getSelecedFile() {
        SmartFileChooser chooser = new SmartFileChooser(Settings.getAbsolutepath());
        GUISettings.localizeFileChooserHomeButton(chooser);
        chooser.setAcceptAllFileFilterUsed(false);
        chooser.setDialogTitle("Choose Media File");
        int option = chooser.showOpenDialog(Client.getInstance().getFrame());
        if (option == JFileChooser.CANCEL_OPTION) {
            return null;
        } else if (option == JFileChooser.APPROVE_OPTION) {
            return chooser.getSelectedFile();
        } else {
            return null;
        }
    }

    public InternalFrame getFrame() {
        return this;
    }

    private void sendMessage() {
        if (getParameterLogicString() != null) {
            if (!isEditMode) {
                SmartAlertFrame frame = new SmartAlertFrame();
                frame.setType("1");
                frame.setClientAlertID(System.currentTimeMillis() + "-" + Settings.getUserID());
                frame.setMessageType("A");
                frame.setSymbol(SharedMethods.getSymbolFromKey(selectedKey));
                frame.setInstrumentType(SharedMethods.getInstrumentTypeFromKey(selectedKey));
                frame.setExchange(SharedMethods.getExchangeFromKey(selectedKey));
                frame.setSKey(selectedKey);
                frame.setLogingID(Settings.getUserID());
                frame.setParamString(getParameterLogicString());
                frame.setNotifyMethod(getAlertMode());
                frame.setNotification(Integer.parseInt(((TWComboItem) cmbNotification.getSelectedItem()).getId()));
                frame.setExpirationDate(Integer.parseInt(((TWComboItem) cmbExpration.getSelectedItem()).getId()));
                frame.setAcStatus("P");
                frame.setMessageShown(0);
                frame.setInsertDate(System.currentTimeMillis());
                boolean validates = ValidateFrame(frame, selectedKey);
                if (!validates) {
                    return;
                }
                String req = frame.getCreateNewAleartMessage();
                SendQFactory.addData(Constants.PATH_PRIMARY, req);
                System.out.println("message= " + req);
                clearWindow();
            } else {
                frame.setMessageType(Meta.ALERT_UPDATE);
                if (selectedKey != null && !selectedKey.isEmpty()) {
                    frame.setSKey(selectedKey);
                }
                frame.setParamString(getParameterLogicString());
                frame.setNotifyMethod(getAlertMode());
                frame.setNotification(Integer.parseInt(((TWComboItem) cmbNotification.getSelectedItem()).getId()));
                frame.setExpirationDate(Integer.parseInt(((TWComboItem) cmbExpration.getSelectedItem()).getId()));
                frame.setAcStatus("P");
                frame.setMessageShown(0);

                String req = frame.getUpdateExsistingAleartMessage();
                SendQFactory.addData(Constants.PATH_PRIMARY, req);
                System.out.println("message= " + req);
                clearAll();
            }
        } else {
            new ShowMessage(Language.getString("SMART_ALERT_INVALID_ROWS"), "E");
        }

    }

    public String getParameterLogicString() {
        String params = null;
        String opr;
        opr = ((TWComboItem) cmbConfig.getSelectedItem()).getId();
        if (opr == null) {
            opr = Meta.ALERT_OPERATOR_AND;
        }
        if (!dataArray1.isEmpty()) {
            for (int i = 0; i < dataArray1.size(); i++) {
                SmartAlertObject obj = dataArray1.get(i);
                if (obj.getCriteriaString() != null) {
                    if (i == 0) {
                        params = obj.getCriteriaString();
                    } else {
                        params = params + Meta.ID + opr + Meta.ID + obj.getCriteriaString();
                    }
                } else {
                    return null;
                }
            }
        }
        return params;

    }

    private void fillComboBoxes() {
        cmbExpration = new TWComboBox();
        expireList = new ArrayList<TWComboItem>();
        cmbExpration.setModel(new TWComboModel(expireList));
        SmartAlertUtils.getFilledExpirationCombo(expireList);
        cmbNotification = new TWComboBox();
        notificationList = new ArrayList<TWComboItem>();
        cmbNotification.setModel(new TWComboModel(notificationList));
        SmartAlertUtils.getFilledNotificationCombo(notificationList);
    }

    public void setVisible(boolean value) {
        if (this.isVisible() && !value) {
            selectedKey = null;
            if (dataArray1 != null) {
                dataArray1.clear();
            }
            if (dataArray2 != null) {
                dataArray2.clear();
            }
        }
        super.setVisible(value);    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void clearWindow() {
        try {
            dataArray1.clear();
            dataArray2.clear();
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
        cmbExpration.setSelectedIndex(0);
        cmbNotification.setSelectedIndex(0);
        cmbConfig.setSelectedIndex(0);

        try {
//            ((TWTextField) ((DefaultCellEditor) (stocktable.getTable().getColumnModel().getColumn(4).getCellEditor())).getComponent()).setText("");
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        getTextSymbol().setText("");
        getLongDesLbl().setText("");
        addLevelBtn.setEnabled(false);
        getTextSymbol().setEditable(true);
        if (isPanelVisible) {
            getShowSymbolBtn().setText(Language.getString("HIDE_ALERT_SYMBOL_RESULTS"));
        } else {
            getShowSymbolBtn().setText(Language.getString("SEE_ALERT_SYMBOL_RESULTS"));
        }
    }

    public void setAlertFrame(SmartAlertFrame frame) {
        isEditMode = true;
        this.frame = frame;
        dataArray1.clear();
        getTextSymbol().setEditable(false);
        if (frame.getSubAlertObjects() != null) {
//            ArrayList<SmartAlertObject> list =  frame.getSubAlertObjects();
            ArrayList<SmartAlertObject> list = new ArrayList<SmartAlertObject>();
            oldList = frame.getSubAlertObjects();
            //selectedIDForEditting = frame.getClientAlertID();
            for(int i=0; i< oldList.size(); i ++){
                SmartAlertObject oo  = new SmartAlertObject();
                oo.setAlertCondtion(oldList.get(i).getAlertCondtion());
                oo.setAlertExpiaration(oldList.get(i).getAlertExpiaration());
            //    oo.setAlertInsertDate(oldList.get(i).getAlertInsertDate());
                oo.setAlertNotification(oldList.get(i).getAlertNotification());
                oo.setAlertrownum(oldList.get(i).getAlertrownum());
                oo.setAlertstatus(oldList.get(i).getAlertstatus());
                oo.setAlertTriggeredDate(oldList.get(i).getAlertTriggeredDate());
                oo.setCBox(oldList.get(i).getCBox());
                oo.setClientId(oldList.get(i).getClientId());
                oo.setCriteria(oldList.get(i).getCriteria());
                oo.setCriteriaID(oldList.get(i).getCriteriaID());
                oo.setFirstElement(oldList.get(i).isFirstElement());
                oo.setMessageStatus(oldList.get(i).getMessageStatus());
                oo.setNoOfBaseSymbol(oldList.get(i).getNoOfBaseSymbol());
                oo.setNoOfChildren(oldList.get(i).getNoOfChildren());
                oo.setParameter(oldList.get(i).getParameter());
                oo.setParameterID(oldList.get(i).getParameterID());
                oo.setRownum(oldList.get(i).getRownum());
                oo.setSKey(oldList.get(i).getSKey());
                oo.setStatus(oldList.get(i).getStatus());
                oo.setSymbol(oldList.get(i).getSymbol());
                oo.setSymbolDescription(oldList.get(i).getSymbolDescription());
          //      oo.setStatus(oldList.get(i).getStatus());
                oo.setValue(oldList.get(i).getValue());
                list.add(oo);
            }
            copyArrayLists(list, dataArray1);
            model.setRecords(dataArray1);
        }
        stocktable.getTable().updateUI();
        setSelectedExpiration(frame.getExpirationDate() + "");
        setSelectedNotification(frame.getNotification() + "");
        setSelectedOperator(frame.getOperator());
        adjustTitle(isEditMode);
    }

    private void setSelectedNotification(String i) {
        for (int j = 0; j < notificationList.size(); j++) {
            TWComboItem item = notificationList.get(j);
            if (item.getId().equals(i)) {
                cmbNotification.setSelectedItem(item);
            }
        }
    }

    private void setSelectedExpiration(String i) {
        for (int j = 0; j < expireList.size(); j++) {
            TWComboItem item = expireList.get(j);
            if (item.getId().equals(i)) {
                cmbExpration.setSelectedItem(item);
            }
        }
    }

    private void setSelectedOperator(String i) {
        for (int j = 0; j < operatorList.size(); j++) {
            TWComboItem item = operatorList.get(j);
            if (item.getId().equals(i)) {
                cmbConfig.setSelectedItem(item);
            }
        }
    }

    public void internalFrameClosing(InternalFrameEvent e) {
        if (e.getSource() == self) {
            frame = null;
            clearWindow();
            isEditMode = false;
            adjustTitle(isEditMode);
            this.setVisible(false);
        }
    }

    public void clearAll() {
        frame = null;
        clearWindow();
        isEditMode = false;
        adjustTitle(isEditMode);
    }

    public void focusGained(FocusEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void focusLost(FocusEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void keyTyped(KeyEvent e) {

    }

    public void keyPressed(KeyEvent e) {
        int code = e.getKeyCode();
        if (code == KeyEvent.VK_ENTER) {
            e.consume();
            if (!isEditMode) {
                if (!ExchangeStore.isNoneDefaultExchangesAvailable()) {
                    if (keyTyped) {
                        boolean validated = doInternalValidation(textSymbol.getText());
                        if (!validated) {
                        }
                    }
                } else {
                    if (keyTyped) {
                        validateSymbol(textSymbol.getText());
                    }
                }
            } else {
                new ShowMessage(Language.getString("ALERT_SEARCH_WARNING"), "W");
            }
        }
    }

    public void keyReleased(KeyEvent e) {
        if (e.getSource() == textSymbol) {
            if (!ExchangeStore.isNoneDefaultExchangesAvailable()) {
                doInternalValidation(textSymbol.getText());
            } else {
                keyTyped = true;
            }
        }
    }

    private void validateSymbol(String symbol) {
        if ((ExchangeStore.isNoneDefaultExchangesAvailable())) { // must validate from server
            String requestID = "SmartAlertFrame" + ":" + System.currentTimeMillis();
            SendQFactory.addValidateRequest(symbol.toUpperCase(), requestID, null, 0);
            SymbolsRegistry.getSharedInstance().rememberRequest(requestID, this);
        }
    }

    public void upDateWindow(String key) {
        this.selectedKey = key;
        if (selectedKey != null) {
            this.exchange = SharedMethods.getExchangeFromKey(selectedKey);
            this.ssymbol = SharedMethods.getSymbolFromKey(selectedKey);
            instrument = SharedMethods.getInstrumentTypeFromKey(selectedKey);
            if (this.ssymbol != null) {
                Stock st = DataStore.getSharedInstance().getStockObject(selectedKey);
                baseSymbol = st.getSymbol();
                if (dataArray1 != null && dataArray1.size() > 0) {
                    for (int i = 0; i < dataArray1.size(); i++) {
                        SmartAlertObject sOb = dataArray1.get(i);
                        sOb.setSymbol(baseSymbol);
                        sOb.setSKey(selectedKey);
                        sOb.setSymbolDescription(DataStore.getSharedInstance().getShortDescription(selectedKey));
                    }
                }

                showSymbolTree(selectedKey);
                getTextSymbol().setText(SharedMethods.getSymbolFromKey(selectedKey));
                if (st != null) {
                    getLongDesLbl().setText(st.getLongDescription());
                    addLevelBtn.setEnabled(true);
                    showSymbolBtn.setEnabled(true);
                    if (isPanelVisible) {
                        getShowSymbolBtn().setText(Language.getString("HIDE_ALERT_SYMBOL_RESULTS") + "<html><u><font color=blue>" + "  " + "(" + SharedMethods.getSymbolFromKey(selectedKey) + ")" + "</font></u>");
                    } else {
                        getShowSymbolBtn().setText(Language.getString("SEE_ALERT_SYMBOL_RESULTS") + "<html><u><font color=blue>" + "  " + "(" + SharedMethods.getSymbolFromKey(selectedKey) + ")" + "</font></u>");
                    }
                }

            }
        }
    }

    public class SpecialTWTextField extends TWTextField {
        private TWComboItem item;

        public TWComboItem getItem() {
            return item;
        }

        public void setItem(TWComboItem item) {
            this.item = item;
        }
    }

    private void copyArrayLists(ArrayList src, ArrayList dest) {
        if (src != null && dest != null) {
            dest.clear();
            for (int i = 0; i < src.size(); i++) {
                dest.add(i, src.get(i));
            }
        }
    }

    private void adjustTitle(boolean editmode) {
        if (editmode) {
            setTitle(Language.getString("SMART_ALERT") + "  " + Language.getString("SMART_ALERT_EDIT_MODE"));
        } else {
            setTitle(Language.getString("SMART_ALERT"));
        }
    }

    public boolean isSetValErrorShown() {
        return isSetValErrorShown;
    }

    public void setSetValErrorShown(boolean setValErrorShown) {
        isSetValErrorShown = setValErrorShown;
    }

    public void loadSmartAleartForSymbol(String sKey) {
        selectedKey = sKey;

        if (dataArray1 != null && dataArray1.size() > 0) {
            for (int i = 0; i < dataArray1.size(); i++) {
                SmartAlertObject sOb = dataArray1.get(i);
                sOb.setSymbol(SharedMethods.getSymbolFromKey(sKey));
                sOb.setSKey(sKey);
                sOb.setSymbolDescription(DataStore.getSharedInstance().getShortDescription(sKey));
            }
        }
        Stock st = DataStore.getSharedInstance().getStockObject(sKey);
        getTextSymbol().setText(SharedMethods.getSymbolFromKey(sKey));
        if (st != null) {
            getLongDesLbl().setText(st.getLongDescription());
        }
        addLevelBtn.setEnabled(true);
        showSymbolBtn.setEnabled(true);
        if (isPanelVisible) {
            getShowSymbolBtn().setText(Language.getString("HIDE_ALERT_SYMBOL_RESULTS") + "<html><u><font color=blue>" + "   " + "(" + SharedMethods.getSymbolFromKey(selectedKey) + ")" + "</font></u>");
        } else {
            getShowSymbolBtn().setText(Language.getString("SEE_ALERT_SYMBOL_RESULTS") + "<html><u><font color=blue>" + "   " + "(" + SharedMethods.getSymbolFromKey(selectedKey) + ")" + "</font></u>");

        }
        showSymbolTree(selectedKey);
    }

    private boolean ValidateFrame(SmartAlertFrame frame, String sKey) {
        int instrument = SharedMethods.getInstrumentTypeFromKey(sKey);
        if (instrument == Meta.INSTRUMENT_OPTION || instrument == Meta.INSTRUMENT_FUTURE) {
            Stock putcall = DataStore.getSharedInstance().getStockObject(sKey);
            if (putcall == null) {
                return false;
            }
            int days = SharedMethods.getDateDiff(new Date(System.currentTimeMillis()), new Date(putcall.getExpirationDate()));
            if (days <= 0 || days < frame.getExpirationDate()) {
                new ShowMessage(Language.getString("SMART_ALERT_EXPIARY_DATE"), "E");
                return false;
            }

        } else {
            return true;
        }
        return true;
    }

    public void updateActiveAlertTable(String sKey, String id) {
        try {
            if (selectedKey != null && !getTextSymbol().getText().trim().equals("") && selectedKey.equals(sKey)) {
                showSymbolTree(sKey);
            }
            if (isEditMode && selectedKey.equals(sKey) && frame != null && frame.getClientAlertID().equals(id)) {
                clearAll();
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public Dimension getPreferredSize() {
        Dimension dim;
        if (isPanelVisible) {
            dim = new Dimension(oSetting1.getSize().width, (upperTitlePanel.getPreferredSize().height + upperPanel.getPreferredSize().height + middlePanel.getPreferredSize().height + middlePanel_4.getPreferredSize().height + 162 + GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT)); //dataTable.getPreferredSize().height+ dataTable.getTable().getTableHeader().getHeight()

        } else {
            dim = new Dimension(oSetting1.getSize().width, (upperTitlePanel.getPreferredSize().height + upperPanel.getPreferredSize().height + middlePanel.getPreferredSize().height + middlePanel_4.getPreferredSize().height + GUISettings.INTERNAL_FRAME_TITLEBAR_HEIGHT - 3));
        }
        return dim;
    }
    public void workspaceLoaded(){
        try {
            setLayer(GUISettings.TOP_LAYER);
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

}
