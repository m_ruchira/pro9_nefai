package com.isi.csvr.smartalerts;

import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.ShowMessage;
import com.isi.csvr.datastore.DataStore;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.MouseListener;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Sep 22, 2009
 * Time: 11:20:21 AM
 * To change this template use File | Settings | File Templates.
 */
public class SmartAlertSettingsDialog extends JDialog implements MouseListener, ActionListener {
    private JPanel mainPanel;
    private JPanel topPanel;
    private JPanel buttonPanel;


    private TWComboBox cmbParameter;
    private TWComboBox cmbCriteria;

    private TWTextField txtValue;

    private TWButton btnOK;
    private TWButton btnCancel;

    private ArrayList<TWComboItem> criteriaList;
    private ArrayList<TWComboItem> parmList;

    public SmartAlertObject alertObject;
    private String key;

    public SmartAlertWindow parent;
    private boolean status;

    public SmartAlertSettingsDialog(JFrame owner, SmartAlertWindow smartAlertWindow, String title, boolean b, String key, boolean status) {
        super(owner, title, b);
        this.parent = smartAlertWindow;
        this.key = key;
        this.status = status;
        createUI(status);

    }

    public SmartAlertSettingsDialog(JFrame frame, SmartAlertWindow smartAlertWindow, String title, boolean b, SmartAlertObject editOb, boolean status) {
        super(frame, title, b);
        this.parent = smartAlertWindow;
        this.alertObject = editOb;
        this.status = status;
        createUI(status);


    }


    private void createUI(boolean b) {
        cmbCriteria = new TWComboBox();
        cmbParameter = new TWComboBox();
        criteriaList = new ArrayList<TWComboItem>();
        parmList = new ArrayList<TWComboItem>();
        txtValue = new TWTextField();
        txtValue.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));

        cmbCriteria.setModel(new TWComboModel(criteriaList));
        cmbParameter.setModel(new TWComboModel(parmList));
        cmbParameter.addActionListener(this);

        mainPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%", "25"}, 0, 0));
        topPanel = new JPanel(new FlexGridLayout(new String[]{"70", "5", "150"}, new String[]{"20", "20", "20"}, 5, 5, true, true));
        buttonPanel = new JPanel(new FlexGridLayout(new String[]{"80", "72", "72"}, new String[]{"100%"}, 5, 3));

        topPanel.add(new JLabel(Language.getString("SMART_ALERT_PARAMETER")));
        topPanel.add(new JLabel());
        topPanel.add(cmbParameter);

        topPanel.add(new JLabel(Language.getString("SMART_ALERT_CRITERIA")));
        topPanel.add(new JLabel());
        topPanel.add(cmbCriteria);


        topPanel.add(new JLabel(Language.getString("SMART_ALERT_VALUE")));
        topPanel.add(new JLabel());
        topPanel.add(txtValue);


        btnOK = new TWButton(Language.getString("OK"));
        btnOK.addActionListener(this);
        btnCancel = new TWButton(Language.getString("CANCEL"));
        btnCancel.addActionListener(this);

        buttonPanel.add(new JLabel());
        buttonPanel.add(btnOK);
        buttonPanel.add(btnCancel);
        mainPanel.add(topPanel);
        mainPanel.add(buttonPanel);

        setParametersList();
        reloadParameterList(SharedMethods.getInstrumentTypeFromKey(key));
        
        setCriteriaList();
        if (b) {
            cmbParameter.setSelectedIndex(0);
            cmbCriteria.setSelectedIndex(0);
        } else {
            for (int i = 0; i < parmList.size(); i++) {
                TWComboItem item = parmList.get(i);
                if (item.getValue().equals(alertObject.getParameter())) {
                    cmbParameter.setSelectedIndex(i);
                    break;
                }
            }
            for (int i = 0; i < criteriaList.size(); i++) {
                TWComboItem item = criteriaList.get(i);
                if (item.getValue().equals(alertObject.getCriteria())) {
                    cmbCriteria.setSelectedIndex(i);
                    break;
                }
            }
            txtValue.setText(alertObject.getValue());
        }

        this.setSize(250, 140);
        this.setResizable(false);
        this.getContentPane().add(mainPanel);
        setLocationRelativeTo(getOwner());
        GUISettings.applyOrientation(this);
        setVisible(true);


    }

    public Icon getIconFromString(String iconFile) {
        Icon icn = null;
        try {
            icn = new ImageIcon(Theme.getTheamedImagePath(iconFile));
        } catch (Exception ex) {

        }
        return icn;
    }

    private void setParametersList() {
        SmartAlertUtils.getFilledParameterComboForStock(parmList);
    }

    private void setCriteriaList() {
        SmartAlertUtils.getFilledCondtionCombo(criteriaList);
    }

    public void reloadParameterList(int instrument) {
        parmList.clear();
        if (instrument == Meta.INSTRUMENT_EQUITY) {
            SmartAlertUtils.getFilledParameterComboForStock(parmList);
        } else if (instrument == Meta.INSTRUMENT_INDEX) {
            SmartAlertUtils.getFilledParameterComboForStockIndex(parmList);
        } else if (instrument == Meta.INSTRUMENT_OPTION || instrument == Meta.INSTRUMENT_FUTURE) {
            SmartAlertUtils.getFilledParameterComboForStockDerivative(parmList);
        } else {
            SmartAlertUtils.getFilledParameterComboForStock(parmList);
        }

    }

    public void mouseClicked(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mousePressed(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseReleased(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseEntered(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void mouseExited(MouseEvent e) {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public void actionPerformed(ActionEvent e) {
        if (e.getSource().equals(btnOK)) {
            addNewObject();
        } else if (e.getSource().equals(btnCancel)) {
            this.setVisible(false);
        }else if(e.getSource().equals(cmbParameter)){
            String selectedId = ((TWComboItem) cmbParameter.getSelectedItem()).getId();
            int tagId = Integer.parseInt(selectedId);
            if(tagId==4 || tagId==5 || tagId==6 || tagId==7 || tagId ==10){
                 txtValue.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));
            }else{
                 txtValue.setDocument(new ValueFormatter(ValueFormatter.DECIMAL));
            }
        }
    }

    private void addNewObject() {
        if (status) {
            alertObject = new SmartAlertObject();
            alertObject.setSymbol(SharedMethods.getSymbolFromKey(key));
            alertObject.setSymbolDescription(DataStore.getSharedInstance().getShortDescription(key));
            alertObject.setSKey(key);
        }

        alertObject.setParameter(cmbParameter.getSelectedItem().toString());
        alertObject.setParameterID(((TWComboItem) cmbParameter.getSelectedItem()).getId());

        alertObject.setCriteria(cmbCriteria.getSelectedItem().toString());
        alertObject.setCriteriaID(((TWComboItem) cmbCriteria.getSelectedItem()).getId());

        if (!txtValue.getText().trim().equals("")) {
            alertObject.setValue(txtValue.getText());
            if (status) {
                parent.addNewAlertObject(alertObject);
            }
            this.setVisible(false);
        } else {
            new ShowMessage(Language.getString("ALERT_VALUE_WARNING"), "W");
        }

//        if (status) {
//            parent.addNewAlertObject(alertObject);
//        }
//        parent.showSymbolTree(SharedMethods.getSymbolFromKey(key));
//        this.setVisible(false);
    }
}
