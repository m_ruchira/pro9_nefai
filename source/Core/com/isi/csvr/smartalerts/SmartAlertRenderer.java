package com.isi.csvr.smartalerts;

import com.isi.csvr.table.TWBasicTableRenderer;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.ExchangeFormatInterface;
import com.isi.csvr.ImageBorder;
import com.isi.csvr.TableSorter;
import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.variationmap.VariationImage;
import com.isi.csvr.shared.*;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jan 18, 2009
 * Time: 9:59:00 PM
 * To change this template use File | Settings | File Templates.
 */
public class SmartAlertRenderer extends TWBasicTableRenderer {
    private int[] g_asRendIDs;
    private int g_iStringAlign;
    private int g_iNumberAlign;
    private int g_iCenterAlign;

    private static Color g_oSelectedFG;
    private static Color g_oSelectedBG;
    private static Color g_oFG1;
    private static Color g_oBG1;
    private static Color g_oFG2;
    private static Color g_oBG2;
    private static Color g_oEditableCellBorder;

    private static ImageIcon deleteImage;
    private static ImageIcon editImage;
    private double doubleValue;
    private TWDecimalFormat oPriceFormat;
    private TWDecimalFormat oQuantityFormat;
    private TWDecimalFormat oNumericFormat;

    public SmartAlertRenderer() {
//        g_oDateTimeFormatHMS = new TWDateFormat(Language.getString("SYMBOL_DATE_TIME_HMS_FORMAT"));
    }

    public void propertyChanged(int property) {

    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
        g_asRendIDs = asRendIDs;
        reload();

        if (Language.isLTR()) {
            g_iStringAlign = JLabel.LEFT;
        } else {
            g_iStringAlign = JLabel.RIGHT;
        }
        g_iNumberAlign = JLabel.RIGHT;
        g_iCenterAlign = JLabel.CENTER;

        oPriceFormat = new TWDecimalFormat(" ###,##0.00 ");
        oQuantityFormat = new TWDecimalFormat(" ###,##0 ");
        oNumericFormat = new TWDecimalFormat(" ########## ");
    }

    public static void reloadForPrinting() {
        g_oSelectedFG = Color.black;
        g_oSelectedBG = Color.white;
        g_oFG1 = Color.black;
        g_oBG1 = Color.white;
        g_oFG2 = Color.black;
        g_oBG2 = Color.white;
    }

    public static void reload() {
        reloadRenderer();
        try {
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");
            g_oEditableCellBorder = Theme.getColor("SMART_ALERT_EDITABLE_CELL_BORDERCOLOR");
        } catch (Exception e) {
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
            g_oEditableCellBorder = Color.black;
        }

        try {
            deleteImage = new ImageIcon("images/Theme" + Theme.getID() + "/smartalert-delete.gif");
        } catch (Exception e) {
            deleteImage = null;
        }
        try {
            editImage = new ImageIcon("images/Theme" + Theme.getID() + "/smartalert-edit.gif");
        } catch (Exception e) {
            editImage = null;
        }
    }

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {

        JLabel lblRenderer = (DefaultTableCellRenderer) super.getTableCellRendererComponent(table, value, isSelected, hasFocus, row, column);
        lblRenderer.setOpaque(true);

        Color foreground, background;
        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
        }

        lblRenderer.setForeground(foreground);
        lblRenderer.setBackground(background);

//        try {
//            deleteImage = new ImageIcon("images/Theme" + Theme.getID() + "/smartalert-delete.gif");
//        } catch (Exception e) {
//            deleteImage = null;
//        }

//        try {
//            editImage = new ImageIcon("images/Theme" + Theme.getID() + "/smartalert-edit.gif");
//        } catch (Exception e) {
//            editImage = null;
//        }
        int iRendID = 0;
        iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        try {
            lblRenderer.setIcon(null);
            lblRenderer.setToolTipText(null);
//            lblRenderer.setBorder(null);
//             if (column != 0 && table.getModel().getValueAt(row, 1) != null && !((String) table.getModel().getValueAt(row, 1)).isEmpty() && !((String) table.getModel().getValueAt(row, 1)).equals(SmartAlertObject.CLICK_MSG)) {
            if (table.getModel().getValueAt(row, 1) != null) {
                switch (column) {
                    case 0:
                        lblRenderer.setToolTipText(Language.getString("DELETE"));
                        break;
                    case 1:
                        lblRenderer.setToolTipText(Language.getString("EDIT"));
                        break;
                    default:
                        break;
                }
            }
            switch (iRendID) {
                case 0: // DEFAULT
                    lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 1: // SYMBOL
                    if (Settings.isShowArabicNumbers())
                        lblRenderer.setText(GUISettings.arabize((String) value));
                    else
                        lblRenderer.setText((String) value);
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);
                    break;
                case 2: // Value
                    doubleValue = toDoubleValue(value);
                    lblRenderer.setText(oPriceFormat.format(doubleValue) + " ");
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 4:
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    lblRenderer.setIcon(deleteImage);
                    break;
                case 5:
                    lblRenderer.setHorizontalAlignment(g_iCenterAlign);
                    lblRenderer.setIcon(editImage);
                    break;
                default:
                    lblRenderer.setText("");
            }
        }
        catch (Exception e) {
            e.printStackTrace();
            lblRenderer.setText("");
        }


        return lblRenderer;
    }

    private long toLongValue(Object oValue) {
        try {
            return Long.parseLong((String) oValue);
        }
        catch (Exception e) {
            return 0;
        }
    }


    private int toIntValue(Object oValue) {
        try {
            return Integer.parseInt((String) oValue);
        }
        catch (Exception e) {
            return 0;
        }
    }

    private double toDoubleValue(Object oValue) {
        try {
            return Double.parseDouble((String) oValue);
        }
        catch (Exception e) {
            return 0;
        }
    }
}
