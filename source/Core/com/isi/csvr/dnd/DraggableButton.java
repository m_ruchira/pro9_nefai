package com.isi.csvr.dnd;

import javax.swing.*;
import java.awt.datatransfer.StringSelection;
import java.awt.dnd.*;

public class DraggableButton extends JButton
        implements DragSourceListener, DragGestureListener {

    private DragSource dragSource = null;

    public DraggableButton() {
        super();
        dragSource = new DragSource();
        dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY, this);
    }

    public void dragEnter(DragSourceDragEvent dsde) {
    }

    public void dragOver(DragSourceDragEvent dsde) {
    }

    public void dropActionChanged(DragSourceDragEvent dsde) {
    }

    public void dragExit(DragSourceEvent dse) {
    }

    public void dragDropEnd(DragSourceDropEvent dsde) {
    }

    public void dragGestureRecognized(DragGestureEvent dge) {
        StringSelection text = new StringSelection("Print");
        dragSource.startDrag(dge, DragSource.DefaultMoveDrop, text, this);
    }

}