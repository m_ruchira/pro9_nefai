package com.isi.csvr.dnd;

import com.isi.csvr.*;
import com.isi.csvr.communication.SendQFactory;
import com.isi.csvr.bandwidthoptimizer.OptimizedTreeRenderer;
import com.isi.csvr.bandwidthoptimizer.MainUI;
import com.isi.csvr.table.TWTableHeader;
import com.isi.csvr.table.TWTableColumnModel;
import com.isi.csvr.variationmap.VariationImage;
import com.isi.csvr.datastore.StockData;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.TableThemeSettings;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.*;
import java.awt.*;
import java.awt.datatransfer.*;
import java.awt.dnd.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.MouseEvent;
import java.util.Arrays;
import java.util.ArrayList;
import java.util.Hashtable;
import java.util.Enumeration;
import java.beans.PropertyChangeListener;
import java.beans.PropertyChangeEvent;

/**
 * This class enables drag and drop feature to the JTable
 */

public class DraggableTable extends JTable
        implements ActionListener, ClipboardOwner, TableThemeSettings,
        DragSourceListener, DragGestureListener, DropTargetListener, PropertyChangeListener {

    private Font tableFont;
    private Font tableSelectedFont;
    private Color upFGColor = new java.awt.Color(255,255,255);
    private Color upBGColor = new java.awt.Color(10,130,49);
    private Color downFGColor = new java.awt.Color(255,255,255);
    private Color downBGColor = new java.awt.Color(232,3,3);
    private Color row1BGColor = new java.awt.Color(246,246,246);
    private Color row2BGColor = new java.awt.Color(232,237,245);
    private Color row1FGColor = new java.awt.Color(38,37,37);
    private Color row2FGColor = new java.awt.Color(38,37,37);
    private Color highlighterFGColor = new java.awt.Color(0,0,0);
    private Color highlighterBGColor = new java.awt.Color(255,181,22);
    private Color headingFGColor;
    private Color headingBGColor;
    private Color gridColor;
    private Color tradeBGColor1 = new java.awt.Color(254,255,129);
    private Color tradeFGColor1 = new java.awt.Color(38,37,37);
    private Color tradeBGColor2 = new java.awt.Color(254,255,129);
    private Color tradeFGColor2 = new java.awt.Color(38,37,37);
    private Color bidBGColor1 = new java.awt.Color(190,240,176);
    private Color bidFGColor1 = new java.awt.Color(38,37,37);
    private Color bidBGColor2 = new java.awt.Color(110,185,89);
    private Color bidFGColor2 = new java.awt.Color(38,37,37);
    private Color askBGColor1 = new java.awt.Color(247,205,217);
    private Color askFGColor1 = new java.awt.Color(38,37,37);
    private Color askBGColor2 = new java.awt.Color(210,155,171);
    private Color askFGColor2 = new java.awt.Color(38,37,37);
    private Color changeUpFG = new java.awt.Color(10,130,49);
    private Color changeDownFG = new java.awt.Color(232,3,3);
    private Font bodyFont;
    private int bodyGap = 2;

    private Color maxBG = new java.awt.Color(197,244,185); //-- Shanika--
    private Color maxFG = new java.awt.Color(22,22,22);
    private Color minBG = new java.awt.Color(255,146,146);
    private Color minFG = new java.awt.Color(22,22,22);
    private Color week52HighBG = new java.awt.Color(120,244,88);
    private Color week52HighFG = new java.awt.Color(245,239,239);
    private Color week52LowBG = new java.awt.Color(251,130,130);
    private Color week52LowFG = new java.awt.Color(245,239,239);

    //---- Added by Shanika--- to enable/disable settings
    private boolean minEnabled = true;
    private boolean maxEnabled = true;
    private boolean wk52LowEnabled = true;
    private boolean wk52HighEnabled = true;

    //private Font  originalBodyFont = null;
    private Font headingFont;
    //private Font  originalHeadingFont = null;
    private boolean customThemeEnabled = false;
    private int gridType = TableThemeSettings.GRID_NONE;
    private Border cellBorder;


    private DragSource dragSource = null;
    private DropTarget dropTarget = null;
    private Clipboard clip;
    private boolean dragging;
    private int dragRow = -1;
    private int mouseRow;
    private Object source;
    private boolean dragInitiater;
    private ClientTableInterface clientTable;
    private int sortedColumn = -1;
    private byte decimalPlaces = Constants.UNASSIGNED_DECIMAL_PLACES;
    private TWTableUI ui;
    private String[] symbolStatuses;

    public static final String LOCK = "LOCK";

    public DraggableTable(ClientTableInterface parent, TableModel oDataModel) {
        super(oDataModel);
//        super(oDataModel, new TWTableColumnModel());
        ui = new TWTableUI();
        setUI(ui);
        this.clientTable = parent;
        //setGdidOn(true);
        super.setShowGrid(false);
        dragSource = new DragSource();
        dragSource.createDefaultDragGestureRecognizer(this, DnDConstants.ACTION_COPY, this);
        dropTarget = new DropTarget(this, this);

        clip = getToolkit().getSystemClipboard();
        // register the keyboard action to copy cells
        this.registerKeyboardAction(this, "COPY", KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.CTRL_DOWN_MASK), JComponent.WHEN_FOCUSED);
        // register the keyboard action to copy cells with the column heading
        this.registerKeyboardAction(this, "COPYwHEAD", KeyStroke.getKeyStroke(KeyEvent.VK_C, KeyEvent.ALT_DOWN_MASK), JComponent.WHEN_FOCUSED);
        // register the keyboard action to link cells
        this.registerKeyboardAction(this, "LINK", KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.CTRL_DOWN_MASK), JComponent.WHEN_FOCUSED);
        // register the keyboard action to link cells with the heading
        this.registerKeyboardAction(this, "LINKwHEAD", KeyStroke.getKeyStroke(KeyEvent.VK_L, KeyEvent.ALT_DOWN_MASK), JComponent.WHEN_FOCUSED);

        setIntercellSpacing(new Dimension(0, 0));

        symbolStatuses = new String[7];
        symbolStatuses[0] = null;
        symbolStatuses[1] = Language.getString("SYMBOL_STATUS_EXPIRED");
        symbolStatuses[2] = Language.getString("SYMBOL_STATUS_DELIETED");
        symbolStatuses[3] = Language.getString("SYMBOL_STATUS_SUSPENDED");
        symbolStatuses[4] = Language.getString("SYMBOL_STATUS_HALTED");
        symbolStatuses[5] = Language.getString("SYMBOL_STATUS_DELAYED");
        symbolStatuses[6] = Language.getString("SYMBOL_STATUS_SHUTTING_DOWN");

        getTableHeader().addPropertyChangeListener("tableColumnResizeEvent", this);

    }

    /*public void dragEnter(DragSourceDragEvent dsde){}

    public void dragOver(DragSourceDragEvent dsde){}

    public void dropActionChanged(DragSourceDragEvent dsde){}

    public void dragExit(DragSourceEvent dse){}

    public void dragDropEnd(DragSourceDropEvent dsde){}

    public void dragGestureRecognized(DragGestureEvent dge){
        /*  Must send the drag text as
        *   =StockNetDdeServer|Symbol!Column
        *   e.g.
        *       =StockNetDdeServer|'1010'!'2'
        *       numeric valus must be within quotes --> e.g. '2'
        *
        int col = this.convertColumnIndexToModel(this.getSelectedColumn());
        int row = this.getSelectedRow();
        String symbol = (String)this.getModel().getValueAt(row,0);
        StringSelection text = new StringSelection("=StockNetDdeServer|'" + symbol + "'!'" + col + "'");
        System.out.println("=StockNetDdeServer|'" + symbol + "'!'" + col + "'");
        dragSource.startDrag (dge, DragSource.DefaultMoveDrop, text, this);
    }*/

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("COPY"))
            copyCells(false);
        else if (e.getActionCommand().equals("COPYwHEAD"))
            copyCells(true);
        else if (e.getActionCommand().equals("LINK"))
            linkCells(false);
        else if (e.getActionCommand().equals("LINKwHEAD"))
            linkCells(true);
    }

    public void lostOwnership(Clipboard clipboard, Transferable contents) {
    }

    public void copyCells(boolean copyHead) {
        int col;
        int cols;
        int row = this.getSelectedRow();
        int rows = this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        String symbol = null;
        String exchange = null;
        int instrument = -1;
        if (getCellSelectionEnabled()) {
            col = this.getSelectedColumn();
            cols = this.getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
        if (copyHead)
            buffer.append(copyHeaders());

        int modelIndex;
        for (int r = 0; r < rows; r++) {
            try {
                symbol = ((StringTransferObject) this.getModel().getValueAt(row + r, -1)).getValue();
                exchange = ((StringTransferObject) this.getModel().getValueAt(row + r, -2)).getValue();
                instrument = (int) ((LongTransferObject) this.getModel().getValueAt(row + r, -11)).getValue();
                for (int c = 0; c < cols; c++) {
                    modelIndex = this.convertColumnIndexToModel(col + c);
                    if (this.getColumn("" + modelIndex).getWidth() != 0) {
                        try {
                            buffer.append(StockData.getValue(exchange, symbol, modelIndex, instrument));
                        } catch (Exception e) {
//                        e.printStackTrace();
                            buffer.append("");
                        }
                        if (c != (cols - 1))  // do not append the tab char to the last item of the row
                            buffer.append("\t");
                    }
                }
                buffer.append("\n");
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        symbol = null;
        buffer = null;
    }


    public void disableMultipleSybols(boolean copyHead) {
        System.out.println("----------------------disable MultipleSybols---");
        int col;
        int cols;
        int row = this.getSelectedRow();
        int rows = this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        String symbol = null;
        String exchange = null;
        int instrument = -1;
        if (getCellSelectionEnabled()) {
            col = this.getSelectedColumn();
            cols = this.getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
        Hashtable<String, String> selectedExchanges = new Hashtable<String, String>();
        ArrayList filteredSymbols = new ArrayList();
        if (rows > 1) {
            for (int r = 0; r < rows; r++) {
                try {
                    symbol = ((StringTransferObject) this.getModel().getValueAt(row + r, -1)).getValue();
                    exchange = ((StringTransferObject) this.getModel().getValueAt(row + r, -2)).getValue();
                    instrument = (int) ((LongTransferObject) this.getModel().getValueAt(row + r, -11)).getValue();

                    Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);
                    selectedExchanges.put(stock.getExchange(), stock.getExchange());
                    filteredSymbols.add(stock);
                    Client.needToReconnect = false;

                    buffer.append("\n");
                } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        }

        Enumeration<String> exkeys = selectedExchanges.keys();
        while (exkeys.hasMoreElements()) {
            Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();

            String currenetEx = exkeys.nextElement();
            for (int j = 0; j < filteredSymbols.size(); j++) {
                Stock newStock = (Stock) filteredSymbols.get(j);
                if (newStock.getExchange().equals(currenetEx)) {
                    if (OptimizedTreeRenderer.selectedSymbols.containsKey(newStock.getKey())) {
                        int instrument_type = SharedMethods.getInstrumentFromExchangeKey(symbol);
                        if (exchangeRequests.containsKey(instrument_type)) {
                            ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(instrument_type);
                            gettingArrayFromHash.add(newStock);
                        } else {
                            ArrayList<Stock> freshArray = new ArrayList<Stock>();
                            freshArray.add(newStock);
                            exchangeRequests.put(instrument_type, freshArray);
                        }
                        MainUI.selectedSymbols.put(newStock.getKey(), false);
                        OptimizedTreeRenderer.selectedSymbols.remove(newStock.getKey());
                        newStock.setSymbolEnabled(false);
                    }
                }
            }
        Enumeration<Integer> keys = exchangeRequests.keys();
        while (keys.hasMoreElements()) {
            String stockExchange = "";
            Integer key = keys.nextElement();
            ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(key);

            int arraySize = (gettingArrayFromHash.size() / 100);
            int reqDevider = arraySize + 1;

            int position = 0;
            int max = gettingArrayFromHash.size();

            for (int j = 1; j <= reqDevider; j++) {
                ArrayList exchangeSymbols = new ArrayList();

                int currentPos;
                if ((j * 100) > max) {
                    currentPos = max;
                } else {
                    currentPos = (j * 100);
                }
                for (int i = position; i < currentPos; i++) {
                    Stock fulKey = gettingArrayFromHash.get(i);
                    stockExchange = fulKey.getExchange();
                    int messageType = SharedMethods.getSymbolType(fulKey.getInstrumentType());
                    Exchange exx = ExchangeStore.getSharedInstance().getExchange(fulKey.getExchange());
                    if ((exx.isUserSubMarketBreakdown()) && (exx.getSubMarketCount() > 0)) {
                        exchangeSymbols.add(fulKey.getSymbolCode() + ":" + fulKey.getInstrumentType());
                    } else {
                        exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
                    }
                    position = position + 1;
                }
                String selectedExchangesSymbols = "";
                for (int f = 0; f < exchangeSymbols.size(); f++) {
                    selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
                }

                String exchangeAddRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);
                SendQFactory.removeOptimizationAllRecord(key, stockExchange, exchangeAddRequest);
            }
        }

        }

        if (Client.needToReconnect) {
            MainUI.save_BandWidthOptimized_Symbols();
            MainUI.saveSettings();
            MainUI.selectedSymbols = new Hashtable<String, Boolean>();
            Client.needToReconnect = false;
            Client.WATCHLIST_SELECT = false;
            Client.getInstance().disconnectFromServer();
        }
        symbol = null;
        buffer = null;
    }


    public void setEnableMultipleSymbols() {
        System.out.println("----------------------enableMultipleSybols---");
        int col;
        int cols;
        int row = this.getSelectedRow();
        int rows = this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        String symbol = null;
        String exchange = null;
        int instrument = -1;
        if (getCellSelectionEnabled()) {
            col = this.getSelectedColumn();
            cols = this.getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
//        if (copyHead)
//            buffer.append(copyHeaders());

        int modelIndex;
        Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();

        if (rows > 1) {
            for (int r = 0; r < rows; r++) {
                try {
                    symbol = ((StringTransferObject) this.getModel().getValueAt(row + r, -1)).getValue();
                    exchange = ((StringTransferObject) this.getModel().getValueAt(row + r, -2)).getValue();
                    instrument = (int) ((LongTransferObject) this.getModel().getValueAt(row + r, -11)).getValue();
                    Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);

                    if(!ExchangeStore.getSharedInstance().getExchange(exchange).isDefault()){
                        Client.isAllDefault = false;
                    }
                    if (!OptimizedTreeRenderer.selectedSymbols.containsKey(stock.getKey())) {
                        Client.isSelected = true;
                    }
                    buffer.append("\n");
                } catch (Exception e) {
                }
            }
        }


        symbol = null;
        buffer = null;
    }


    public void enableMultipleSybols(boolean copyHead) {
        System.out.println("----------------------enableMultipleSybols---");
        int col;
        int cols;
        int row = this.getSelectedRow();
        int rows = this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        String symbol = null;
        String exchange = null;
        int instrument = -1;
        if (getCellSelectionEnabled()) {
            col = this.getSelectedColumn();
            cols = this.getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
        Hashtable<String, String> selectedExchanges = new Hashtable<String, String>();
        ArrayList filteredSymbols = new ArrayList();
        for (int r = 0; r < rows; r++) {
            try {
                symbol = ((StringTransferObject) this.getModel().getValueAt(row + r, -1)).getValue();
                exchange = ((StringTransferObject) this.getModel().getValueAt(row + r, -2)).getValue();
                instrument = (int) ((LongTransferObject) this.getModel().getValueAt(row + r, -11)).getValue();

                Stock stock = DataStore.getSharedInstance().getStockObject(exchange, symbol, instrument);

                selectedExchanges.put(stock.getExchange(), stock.getExchange());
                filteredSymbols.add(stock);
                Client.needToReconnect = false;

                buffer.append("\n");
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }

        Enumeration<String> exkeys = selectedExchanges.keys();
        while (exkeys.hasMoreElements()) {
            Hashtable<Integer, ArrayList<Stock>> exchangeRequests = new Hashtable<Integer, ArrayList<Stock>>();

            String currenetEx = exkeys.nextElement();
            for (int j = 0; j < filteredSymbols.size(); j++) {
                Stock newStock = (Stock) filteredSymbols.get(j);
                if (newStock.getExchange().equals(currenetEx)) {
//                    if (!OptimizedTreeRenderer.selectedSymbols.containsKey(stock.getKey())) {
                    int instrument_type = SharedMethods.getInstrumentFromExchangeKey(symbol);
                    if (exchangeRequests.containsKey(instrument_type)) {
                        ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(instrument_type);
                        gettingArrayFromHash.add(newStock);
                    } else {
                        ArrayList<Stock> freshArray = new ArrayList<Stock>();
                        freshArray.add(newStock);
                        exchangeRequests.put(instrument_type, freshArray);
                    }

                    MainUI.selectedSymbols.put(newStock.getKey(), true);
                    OptimizedTreeRenderer.selectedSymbols.put(newStock.getKey(), true);
                    newStock.setSymbolEnabled(true);
//                                }
            }
        }

        Enumeration<Integer> keys = exchangeRequests.keys();
        while (keys.hasMoreElements()) {
            String stockExchange = "";
            Integer key = keys.nextElement();
            ArrayList<Stock> gettingArrayFromHash = exchangeRequests.get(key);

            int arraySize = (gettingArrayFromHash.size() / 100);
            int reqDevider = arraySize + 1;

            int position = 0;
            int max = gettingArrayFromHash.size();

            for (int j = 1; j <= reqDevider; j++) {
                ArrayList exchangeSymbols = new ArrayList();

                int currentPos;
                if ((j * 100) > max) {
                    currentPos = max;
                } else {
                    currentPos = (j * 100);
                }
                for (int i = position; i < currentPos; i++) {
                    Stock fulKey = gettingArrayFromHash.get(i);
                    stockExchange = fulKey.getExchange();
                    int messageType = SharedMethods.getSymbolType(fulKey.getInstrumentType());
                    Exchange exx = ExchangeStore.getSharedInstance().getExchange(fulKey.getExchange());
                    if ((exx.isUserSubMarketBreakdown()) && (exx.getSubMarketCount() > 0)) {
                        exchangeSymbols.add(fulKey.getSymbolCode() + ":" + fulKey.getInstrumentType());
                    } else {
                        exchangeSymbols.add(fulKey.getSymbol() + ":" + fulKey.getInstrumentType());
                    }
                    position = position + 1;
                }
                String selectedExchangesSymbols = "";
                for (int f = 0; f < exchangeSymbols.size(); f++) {
                    selectedExchangesSymbols = selectedExchangesSymbols + exchangeSymbols.get(f) + ",";
                }
                String exchangeAddRequest = (String) selectedExchangesSymbols.subSequence(0, selectedExchangesSymbols.length() - 1);
            SendQFactory.addOptimizationAllRecord(key, stockExchange, exchangeAddRequest);
        }
        }
        }
        if (Client.needToReconnect) {
            MainUI.save_BandWidthOptimized_Symbols();
            MainUI.saveSettings();
            Client.needToReconnect = false;
            Client.WATCHLIST_SELECT = false;
            Client.getInstance().disconnectFromServer();
        }

        symbol = null;
        buffer = null;
    }

    public void linkCells(boolean copyHead) {
        int col;
        int cols;
        int row = this.getSelectedRow();
        int rows = this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");
        String symbol = null;
        String exchange = null;
        int instrument = -1;
        if (getCellSelectionEnabled()) {
            col = this.getSelectedColumn();
            cols = this.getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }

        if (copyHead)
            buffer.append(copyHeaders());
        int modelIndex;
        try {
            for (int r = 0; r < rows; r++) {
                symbol = ((StringTransferObject) this.getModel().getValueAt(row + r, -1)).getValue();
                exchange = ((StringTransferObject) this.getModel().getValueAt(row + r, -2)).getValue();
                instrument = (int) ((LongTransferObject) this.getModel().getValueAt(row + r, -11)).getValue();
                //            symbol = (String) this.getModel().getValueAt(row + r, -1);
                //            exchange = (String) this.getModel().getValueAt(row + r, -2);
                if (symbol.equals(Constants.NULL_STRING)) {
                    buffer.append("\n");
                    continue;
                }
                for (int c = 0; c < cols; c++) {
                    modelIndex = this.convertColumnIndexToModel(col + c);
                    if (this.getColumn("" + modelIndex).getWidth() != 0) {
                        if (!StockData.isDynamicData(modelIndex)) {
                            buffer.append(StockData.getValue(exchange, symbol, modelIndex, instrument));
                        } else if (StockData.isNumericData(modelIndex)) {
                            buffer.append("=MRegionalDdeServer|'BO");
                            buffer.append(SharedMethods.getKey(exchange, symbol, instrument));
                            buffer.append("'!'");
                            buffer.append(modelIndex);
                            buffer.append("'*1");
                            /*buffer.append("=if(isnumber(MProTadawulDdeServer|'");
                            buffer.append("BO");
                            buffer.append(exchange);
                            buffer.append(ESPConstants.KEY_SEPERATOR_CHARACTER);
                            buffer.append(symbol);
                            buffer.append("'!'");
                            buffer.append(modelIndex);
                            buffer.append( "'),MProTadawulDdeServer|'BO");
                            buffer.append(exchange);
                            buffer.append(ESPConstants.KEY_SEPERATOR_CHARACTER);
                            buffer.append(symbol);
                            buffer.append("'!'");
                            buffer.append(modelIndex);
                            buffer.append("'*1,MProTadawulDdeServer|'BO");
                            buffer.append(exchange);
                            buffer.append(ESPConstants.KEY_SEPERATOR_CHARACTER);
                            buffer.append(symbol);
                            buffer.append("'!'");
                            buffer.append(modelIndex);
                            buffer.append("')");*/
                        } else {
                            buffer.append("=MRegionalDdeServer|'");
                            buffer.append("BO");
                            buffer.append(SharedMethods.getKey(exchange, symbol, instrument));
                            buffer.append("'!'");
                            buffer.append(modelIndex);
                            buffer.append("'");
                        }
                        if (c != (cols - 1))  // do not append the tab char to the last item of the row
                            buffer.append("\t");
                    }
                }
                buffer.append("\n");
            }
        } catch (Exception e) {
//            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }

        StringSelection text = new StringSelection(buffer.toString());
        clip.setContents(text, this);

        buffer = null;
        symbol = null;
    }

    public String copyHeaders() {
        int col;
        int cols;
        //int row = this.getSelectedRow();
        //int rows= this.getSelectedRowCount();
        StringBuffer buffer = new StringBuffer("");

        if (getCellSelectionEnabled()) {
            col = this.getSelectedColumn();
            cols = this.getSelectedColumnCount();
        } else {
            col = 0;
            cols = this.getColumnCount();
        }
        int modelIndex;
        for (int c = 0; c < cols; c++) {
            modelIndex = this.convertColumnIndexToModel(col + c);
            if (this.getColumn("" + modelIndex).getWidth() != 0) {
                buffer.append((String) this.getColumn("" + modelIndex).getHeaderValue());
                if (c != (cols - 1))  // do not append the tab char to the last item of the row
                    buffer.append("\t");
            }
        }
        buffer.append("\n");

        return buffer.toString();
    }

    public void setDecimalPlaces(byte places) {
        decimalPlaces = places;
    }

    public byte getDecimalPlaces() {
        return decimalPlaces;
    }

    // table theme settings

    public java.awt.Font getTableFont() {
        return tableFont;
    }

    public void setTableFont(java.awt.Font tableFont) {
        this.tableFont = tableFont;

    }

    public java.awt.Font getSelectedTableFont() {
        if (tableSelectedFont == null) {
            return super.getFont().deriveFont(Font.BOLD);
        } else {
            return tableSelectedFont;
        }
    }

    public void setSelectedTableFont(java.awt.Font tableFont) {
        this.tableSelectedFont = tableFont;
    }

    public void setUpFGColor(java.awt.Color upFGColor) {
        this.upFGColor = upFGColor;
    }

    public java.awt.Color getUpFGColor() {
        return upFGColor;
    }

    public void setDownFGColor(java.awt.Color downFGColor) {
        this.downFGColor = downFGColor;
    }

    public java.awt.Color getDownFGColor() {
        return downFGColor;
    }


    public void setRow1BGColor(java.awt.Color row1BGColor) {
        this.row1BGColor = row1BGColor;
    }

    public java.awt.Color getRow1BGColor() {
        return row1BGColor;
    }

    public void setRow2BGColor(java.awt.Color row2BGColor) {
        this.row2BGColor = row2BGColor;
    }

    public java.awt.Color getRow2BGColor() {
        return row2BGColor;
    }

    public void setHighlighterFGColor(java.awt.Color highlighterFGColor) {
        this.highlighterFGColor = highlighterFGColor;
    }

    public java.awt.Color getHighlighterFGColor() {
        return highlighterFGColor;
    }

    public void setHighlighterBGColor(java.awt.Color highlighterBGColor) {
        this.highlighterBGColor = highlighterBGColor;
    }

    public java.awt.Color getHighlighterBGColor() {
        return highlighterBGColor;
    }

    public void setGdidOn(int gridType) {
        this.gridType = gridType;
        switch (gridType) {
            case TableThemeSettings.GRID_NONE:
                cellBorder = null;
                break;
            case TableThemeSettings.GRID_HORIZONTAL:
                cellBorder = BorderFactory.createMatteBorder(0, 0, 1, 0, gridColor);
                break;
            case TableThemeSettings.GRID_VERTICAL:
                if (Language.isLTR()) {
                    cellBorder = BorderFactory.createMatteBorder(0, 0, 0, 1, gridColor);
                } else {
                    cellBorder = BorderFactory.createMatteBorder(0, 1, 0, 0, gridColor);
                }
                break;
            case TableThemeSettings.GRID_BOTH:
                if (Language.isLTR()) {
                    cellBorder = BorderFactory.createMatteBorder(0, 0, 1, 1, gridColor);
                } else {
                    cellBorder = BorderFactory.createMatteBorder(0, 1, 1, 0, gridColor);
                }
                break;
        }

        //if (gridType > 0) {
        /*switch(gridType){
            case TableThemeSettings.GRID_HORIZONTAL:
                setIntercellSpacing(new Dimension(0, 1));
                setBackground(getTableGridColor());
//                    super.setShowGrid(true);
                break;
            case TableThemeSettings.GRID_VERTICAL:
                setIntercellSpacing(new Dimension(1, 0));
//                    setBackground(getTableGridColor());
        super.setShowGrid(true);
        setGridColor(getTableGridColor());
//                    super.setShowGrid(true);
                break;
            case TableThemeSettings.GRID_BOTH:
                setIntercellSpacing(new Dimension(1, 1));
                setBackground(getTableGridColor());
//                    super.setShowGrid(true);
                break;
            case TableThemeSettings.GRID_NONE:
        setIntercellSpacing(new Dimension(0, 0));
//                    super.setShowGrid(false);
        setBackground(this.getBackground());
                break;
    }

        repaint();*/
    }

    private boolean tag;

    public void setBackground(Color bg) {
        super.setBackground(bg);    //To change body of overridden methods use File | Settings | File Templates.
        if (bg.getBlue() == 255) {
            tag = true;
        }
        //if (tag){
//            SharedMethods.printLine("+++++++++++ " + bg, true);
        //}
    }

    public boolean isGdidOn() {
        return gridType > TableThemeSettings.GRID_NONE;
    }

    public Border getCellBorder() {
        return cellBorder;
    }

    public int getGdidType() {
        return gridType;
    }

    public void setTableGridColor(java.awt.Color gridColor) {
        this.gridColor = gridColor;
        setGdidOn(gridType);
    }

    public java.awt.Color getTableGridColor() {
        if (gridColor == null) {
            return Color.black;
        } else {
            return gridColor;
        }
    }

    public Color getHeadingFGColor() {
        return headingFGColor;
    }

    public void setHeadingFGColor(java.awt.Color headFGColor) {
        this.headingFGColor = headFGColor;
    }

    public java.awt.Color getHeadingBGColor() {
        return headingBGColor;
    }

    public void setHeadingBGColor(java.awt.Color headingBGColor) {
        this.headingBGColor = headingFGColor;
    }

    public boolean isCuatomThemeEnabled() {
        return customThemeEnabled;
    }

    public void setCustomThemeEnabled(boolean status) {
        customThemeEnabled = status;
        if (gridType > TableThemeSettings.GRID_NONE) {
            if (isCuatomThemeEnabled()) {
                setGridColor(getTableGridColor());
            } else {
                setGridColor(Theme.getColor("BOARD_TABLE_GRIDCOLOR"));
            }
        }
        updateUI();
    }

    public void setTradeFGColor1(java.awt.Color tradeFGColor) {
        this.tradeFGColor1 = tradeFGColor;
    }

    public java.awt.Color getTradeFGColor1() {
        return tradeFGColor1;
    }

    public void setTradeBGColor1(java.awt.Color tradeBGColor) {
        this.tradeBGColor1 = tradeBGColor;
    }

    public java.awt.Color getTradeBGColor1() {
        return tradeBGColor1;
    }

    public void setTradeFGColor2(java.awt.Color tradeFGColor) {
        this.tradeFGColor2 = tradeFGColor;
    }

    public java.awt.Color getTradeFGColor2() {
        return tradeFGColor2;
    }

    public void setTradeBGColor2(java.awt.Color tradeBGColor) {
        this.tradeBGColor2 = tradeBGColor;
    }

    public java.awt.Color getTradeBGColor2() {
        return tradeBGColor2;
    }

    public void setBidFGColor1(java.awt.Color bidFGColor) {
        this.bidFGColor1 = bidFGColor;
    }

    public java.awt.Color getBidFGColor1() {
        return bidFGColor1;
    }

    public void setBidFGColor2(java.awt.Color bidFGColor) {
        this.bidFGColor2 = bidFGColor;
    }

    public java.awt.Color getBidFGColor2() {
        return bidFGColor2;
    }

    public void setBidBGColor1(java.awt.Color bidBGColor) {
        this.bidBGColor1 = bidBGColor;
    }

    public java.awt.Color getBidBGColor1() {
        return bidBGColor1;
    }

    public void setBidBGColor2(java.awt.Color bidBGColor) {
        this.bidBGColor2 = bidBGColor;
    }

    public java.awt.Color getBidBGColor2() {
        return bidBGColor2;
    }

    public void setAskFGColor1(java.awt.Color askFGColor) {
        this.askFGColor1 = askFGColor;
    }

    public java.awt.Color getAskFGColor1() {
        return askFGColor1;
    }

    public void setAskBGColor1(java.awt.Color askBGColor) {
        this.askBGColor1 = askBGColor;
    }

    public java.awt.Color getAskBGColor1() {
        return askBGColor1;
    }

    public void setAskFGColor2(java.awt.Color askFGColor) {
        this.askFGColor2 = askFGColor;
    }

    public java.awt.Color getAskFGColor2() {
        return askFGColor2;
    }

    public void setAskBGColor2(java.awt.Color askBGColor) {
        this.askBGColor2 = askBGColor;
    }

    public java.awt.Color getAskBGColor2() {
        return askBGColor2;
    }


    public void setRow1FGColor(java.awt.Color row1FGColor) {
        this.row1FGColor = row1FGColor;
    }

    public java.awt.Color getRow1FGColor() {
        return row1FGColor;
    }


    public void setRow2FGColor(java.awt.Color row2FGColor) {
        this.row2FGColor = row2FGColor;
    }

    public java.awt.Color getRow2FGColor() {
        return row2FGColor;
    }


    public void setUpBGColor(java.awt.Color upBGColor) {
        this.upBGColor = upBGColor;
    }

    public java.awt.Color getUpBGColor() {
        return upBGColor;
    }


    public void setDownBGColor(java.awt.Color downBGColor) {
        this.downBGColor = downBGColor;
    }

    public java.awt.Color getDownBGColor() {
        return downBGColor;
    }

    public java.awt.Color getChangeUPFGColor() {
        return changeUpFG;
    }

    public void setChangeUPFGColor(java.awt.Color color) {
        this.changeUpFG = color;
    }

    public java.awt.Color getChangeDownFGColor() {
        return changeDownFG;
    }

    public void setChangeDownFGColor(java.awt.Color color) {
        this.changeDownFG = color;
    }


    public void setBodyGap(int gap) {
        bodyGap = gap;
    }

    public int getBodyGap() {
        return bodyGap;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setMinBGColor(java.awt.Color minBGColor) {
        this.minBG = minBGColor;
    }

    public Color getMinBGColor() {
        return minBG;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setMinFGColor(java.awt.Color minFGColor) {
        this.minFG = minFGColor;
    }

    public Color getMinFGColor() {
        return minFG;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setMaxBGColor(java.awt.Color maxBGColor) {
        this.maxBG = maxBGColor;
    }

    public Color getMaxBGColor() {
        return maxBG;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setMaxFGColor(java.awt.Color maxFGColor) {
        this.maxFG = maxFGColor;
    }

    public Color getMaxFGColor() {
        return maxFG;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setWeek52HighBGColor(java.awt.Color week52HighBGColor) {
        this.week52HighBG = week52HighBGColor;

    }

    public Color getWeek52HighBGColor() {
        return week52HighBG;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setWeek52HighFGColor(java.awt.Color week52HighFGColor) {
        this.week52HighFG = week52HighFGColor;

    }

    public Color getWeek52HighFGColor() {
        return week52HighFG;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setWeek52LowBGColor(java.awt.Color week52LowBGColor) {
        this.week52LowBG = week52LowBGColor;
    }

    public Color getWeek52LowBGColor() {
        return week52LowBG;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public void setWeek52LowFGColor(java.awt.Color week52LowFGColor) {
        this.week52LowFG = week52LowFGColor;

    }

    public Color getWeek52LowFGColor() {
        return week52LowFG;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public boolean isMinEnabled() {
        return minEnabled;
    }

    public void setMinEnabled(boolean minEnabled) {
        this.minEnabled = minEnabled;
    }

    public boolean isMaxEnabled() {
        return maxEnabled;
    }

    public void setMaxEnabled(boolean maxEnabled) {
        this.maxEnabled = maxEnabled;
    }

    public boolean isWk52LowEnabled() {
        return wk52LowEnabled;
    }

    public void setWk52LowEnabled(boolean wk52LowEnabled) {
        this.wk52LowEnabled = wk52LowEnabled;
    }

    public boolean isWk52HighEnabled() {
        return wk52HighEnabled;
    }

    public void setWk52HighEnabled(boolean wk52HighEnabled) {
        this.wk52HighEnabled = wk52HighEnabled;
    }

    public Font getBodyFont() {
        return super.getFont();
    }


    public void setBodyFont(Font font) {
        if (font != null) {
            bodyFont = font;
            this.tableSelectedFont = font.deriveFont(Font.BOLD);
            super.setFont(font);
        }
    }

    public Font getHeadingFont() {
        return getCurrentHeaderFont();
    }

    private Font getCurrentHeaderFont() {
        Object rend = getColumnModel().getColumn(0).getHeaderRenderer();
        if (rend instanceof SortButtonRenderer) {
            Font font = ((SortButtonRenderer) rend).getFont();
            rend = null;
            return font;
        } else {
            return getTableHeader().getFont();
        }
    }

    public void setHeadingFont(Font font) {
        headingFont = font;

        Object rend = getColumnModel().getColumn(0).getHeaderRenderer();
        if (rend instanceof SortButtonRenderer)
            ((SortButtonRenderer) rend).setHeaderFont(font);
        else
            getTableHeader().setFont(font);
    }


    public void updateHeaderUI() {
        Object rend;

        if (headingFont == null) return;

        for (int i = 0; i < getColumnCount(); i++) {
            rend = getColumnModel().getColumn(i).getHeaderRenderer();
            if (!(rend instanceof SortButtonRenderer)) break;
            ((SortButtonRenderer) rend).setHeaderFont(headingFont);
            rend = null;
        }
        getTableHeader().updateUI();
    }

    public int adjustColumnWidthsToFit() {
        return adjustColumnWidthsToFit(0, this.getColumnCount());
    }

    public int adjustColumnWidthsToFit(int start, int end) {
        int[] colWidths = new int[this.getColumnCount()];
        int model;

        setCursor(Cursor.getPredefinedCursor(Cursor.WAIT_CURSOR));
        Arrays.fill(colWidths, 0);
        int spacing = (int) getIntercellSpacing().getWidth();


        JLabel comp = (JLabel) getCellRenderer(0, 0).getTableCellRendererComponent(
                this, getModel().getValueAt(0, 0), false, false, 0, 0);
        Insets insets = comp.getInsets();
        int gap = insets.left + insets.right;
        //insets = null;

        SortButtonRenderer headerRenderer = (SortButtonRenderer) getColumnModel().getColumn(0).getHeaderRenderer();
        FontMetrics fMetrics = getFontMetrics(headerRenderer.getFont());
        gap += 15;
        //(headerRenderer.getBorder().getBorderInsets(headerRenderer).left*2);

        for (int column = start; column < end; column++) {
            //model = this.convertColumnIndexToModel(column);

            try {
                if ((fMetrics.stringWidth(getColumnName(column)) + gap) > colWidths[column]) {
                    colWidths[column] = fMetrics.stringWidth(getColumnName(column)) + gap;
                }
            }
            catch (Exception ex) {
            }
        }
        headerRenderer = null;

        fMetrics = getFontMetrics(getFont());
        gap = insets.left + insets.right + 2;
        int icongap = 0;

        for (int row = 0; row < getRowCount(); row++) {
            for (int column = start; column < end; column++) {
                if (this.getColumnModel().getColumn(column).getPreferredWidth() > 0) {
                    model = this.convertColumnIndexToModel(column);
                    comp = (JLabel) getCellRenderer(row, column).getTableCellRendererComponent(
                            this, getModel().getValueAt(row, model), false, false, row, column);

                    try {
                        icongap = 0;
                        Icon icon = comp.getIcon();
                        if (icon instanceof VariationImage) {
                            colWidths[column] = 100;
                        } else {
                            if (icon != null) {
                                if (icon.getIconWidth() > 0) {
                                    icongap = icon.getIconWidth() + comp.getIconTextGap();
                                }
                            }
                            if ((fMetrics.stringWidth(comp.getText()) + gap + icongap) > colWidths[column]) {
                                colWidths[column] = fMetrics.stringWidth(comp.getText()) + gap + icongap;
                            }
                        }
                        icon = null;
                    }
                    catch (Exception ex) {
                    }
                }
            }
        }

        for (int column = start; column < end; column++) {
            if (this.getColumnModel().getColumn(column).getPreferredWidth() > 0)
                this.getColumnModel().getColumn(column).setPreferredWidth(colWidths[column]);
        }

        updateUI();
        setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        return (int) getTableHeader().getPreferredSize().getWidth();
    }

    /* Drag n Drop */

    public void dragGestureRecognized(DragGestureEvent dge) {
        try {
            if (TWActions.isDragPermited(dge.getComponent(), this)) {
                System.out.println("Dragging");
                System.out.println(getSelectedRow());
                String symbol = SharedMethods.getKey(
                        ((StringTransferObject) getModel().getValueAt(getSelectedRow(), -2)).getValue(),
                        ((StringTransferObject) getModel().getValueAt(getSelectedRow(), -1)).getValue(),
                        (int) ((LongTransferObject) getModel().getValueAt(getSelectedRow(), -11)).getValue());
                if (!symbol.equals("")) {
                    Transferable transferable = new StringSelection(symbol);
                    dragSource.startDrag(dge, null, null, null, transferable, this);
                    GUISettings.createDragImage(((StringTransferObject) getModel().getValueAt(getSelectedRow(), 0)).getValue(), getGraphics());
                    setDragRow(getSelectedRow());
                    setDragInitiater(true);
                    setDragging(true);
                    source = this;
                } else {
                    TWActions.setDragBlocked();
                    GUISettings.destroyBufferedImage();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isDragging() {
        if (!TWActions.isDragPermited(source, this)) {
            return false;
        } else {
            return dragging;
        }
    }

    public void setDragging(boolean dragging) {
        this.dragging = dragging;
    }

    /* Drag Source Interface Methods */

    public void dragEnter(DragSourceDragEvent dsde) {
    }

    public void dragOver(DragSourceDragEvent dsde) {
    }

    public void dropActionChanged(DragSourceDragEvent dsde) {
    }

    public void dragExit(DragSourceEvent dse) {
    }

    public void dragDropEnd(DragSourceDropEvent dsde) {
        GUISettings.destroyBufferedImage();
        TWActions.setDragBlocked();
        source = null;
    }

    /* Drop Target Interface Methods */

    private Rectangle rect2D = new Rectangle();

    public void dragEnter(DropTargetDragEvent dtde) {
        if (!TWActions.isDragPermited(source, this)) return;
        Point pt = dtde.getLocation();
        paintImmediately(rect2D.getBounds());
        if (GUISettings.getBufferedImage() != null) {
            rect2D.setRect((int) pt.getX(), (int) pt.getY(), GUISettings.getBufferedImage().getWidth(),
                    GUISettings.getBufferedImage().getHeight());
            getGraphics().drawImage(GUISettings.getBufferedImage(), (int) pt.getX(), (int) pt.getY(), this);
        }
    }

    public void dragExit(DropTargetEvent dte) {
        if (!TWActions.isDragPermited(source, this)) return;
        mouseRow = -1;
        repaint();

    }

    public void dragOver(DropTargetDragEvent dtde) {
        if (!TWActions.isDragPermited(source, this))
            return;
        setDragging(true);
        if (mouseRow != rowAtPoint(dtde.getLocation())) {
            mouseRow = rowAtPoint(dtde.getLocation());
            repaint();
        }

        Point pt = dtde.getLocation();
        paintImmediately(rect2D.getBounds());
        if (GUISettings.getBufferedImage() != null) {
            rect2D.setRect((int) pt.getX(), (int) pt.getY(), GUISettings.getBufferedImage().getWidth(),
                    GUISettings.getBufferedImage().getHeight());
            (getGraphics()).drawImage(GUISettings.getBufferedImage(),
                    (int) pt.getX(), (int) pt.getY(), this);
        }
        pt = null;

    }

    public void dropActionChanged(DropTargetDragEvent dtde) {
        if (!TWActions.isDragPermited(source, this)) return;
        source = null;
        repaint();
    }

    public void drop(DropTargetDropEvent dtde) {
        try {
            if (TWActions.isDragPermited(source, this)) { //TWActions.isDragPermited()
                Transferable data = dtde.getTransferable();
                String symbol = (String) data.getTransferData(DataFlavor.stringFlavor);
                if (source == this) {
                    moveSymbol();
                } else {
                    addSymbol(symbol);
                }
                source = null;
            }
            mouseRow = -1; // clear the inset point

        } catch (Exception e) {
            e.printStackTrace();
        }
        repaint();
        TWActions.setDragBlocked();
    }

    private void addSymbol(String key) {
        if (clientTable.isCustomType()) {
            if (clientTable.getSymbols() != null) {
                clientTable.getSymbols().insertSymbol(key, mouseRow);
                updateUI();
                clientTable.cancelSorting();
                DataStore.getSharedInstance().addSymbolRequest(key);
            }
        } else {
            clientTable.showInvalidDropMessage();
        }
    }

    private void moveSymbol() {
        if (clientTable.getSymbols() != null) {
            clientTable.getSymbols().moveSymbol(dragRow, mouseRow);
            mouseRow = -1;
            dragRow = -1;
            clientTable.cancelSorting();
            updateUI();
        }
    }

    public int getDragRow() {
        return dragRow;
    }

    public int getMouseRow() {
        return mouseRow;
    }

    public void setMouseRow(int mouseRow) {
        this.mouseRow = mouseRow;
    }


    public void setDragRow(int dragRow) {
        this.dragRow = dragRow;
    }

    public void mouseDragged(MouseEvent e) {

    }

    public void mouseMoved(MouseEvent e) {

    }

    public void setDragInitiater(boolean dragInitiater) {
        this.dragInitiater = dragInitiater;
    }

    public int getSortedColumn() {
        return sortedColumn;
    }

    public void setSortedColumn(int sortedColumn) {
        this.sortedColumn = sortedColumn;
    }

    public void updateUI() {
        try {
        super.updateUI();
        setUI(ui);
        } catch (Exception e) {
        }
    }

    public String getToolTipText(MouseEvent event) {
        Point point = event.getPoint();
        int col1= columnAtPoint(point);
        int col = convertColumnIndexToModel(columnAtPoint(point));
        int row = rowAtPoint(point);
        if (col == 0) {
            int status = (Integer) getModel().getValueAt(row, -5);
            int week52HLTracker = ((IntTransferObject) getModel().getValueAt(row, -10)).getValue();
            int minMaxTracker = ((IntTransferObject)getModel().getValueAt(row, 47)).getValue();
            if ((status > 0) /*&& (status < 7)*/) {
                try {
                    return "<html>" + getModel().getValueAt(row, 2).toString() + " - " + symbolStatuses[status] + " ";
                } catch (Exception e) { // oops! I have no idea why
                    return getModel().getValueAt(row, 2).toString();
                }
            } else if (week52HLTracker == Stock.BEYOND52WEEKHIGH) {
                return "<html>" + getModel().getValueAt(row, 2).toString() + "<br>" + Language.getString("WEEK52HIGH_TRACKER");

            } else if (week52HLTracker == Stock.BEYOND52WEEKLOW) {
                return "<html>" + getModel().getValueAt(row, 2).toString() + "<br>" + Language.getString("WEEK52LOW_TRACKER");
            } else if (minMaxTracker == Stock.MIN) {
                return "<html>" + getModel().getValueAt(row, 2).toString() + "<br>" + Language.getString("MIN_TRACKER");
            } else if (minMaxTracker == Stock.MAX) {
                return "<html>" + getModel().getValueAt(row, 2).toString() + "<br>" + Language.getString("MAX_TRACKER");
            }
            //tempInt = ((IntTransferObject) table.getModel().getValueAt(row, 47)).getValue();
//                    if (Menus.isShowMinMax) {
//                        if (tempInt == Stock.MAX) {
            else {
                return getModel().getValueAt(row, 2).toString();
            }
        }else if(col==214) {    // Added by Nishantha, Tooltip for comp. losses
            int i =   (int)(((DoubleTransferObject) (this.getModel().getValueAt(row, 214))).getValue());
            String cellValue="";
            if(i==1){
               cellValue= Language.getString("COMPNAY_IS_SUFFERING_FROM_CAPITAL_EROSION_50_74");
            }else if(i==2){
               cellValue= Language.getString("COMPNAY_IS_SUFFERING_FROM_CAPITAL_EROSION_74_100");
            }else if(i==3){
               cellValue= Language.getString("COMPNAY_IS_SUFFERING_FROM_CAPITAL_EROSION_MORE_THAN_100");
            }
             return cellValue;    
        } else {
            return null;
        }
        //getcol getColumnModel().getColumnIndexAtX((int)point.getX());
    }

    public TableCellRenderer getCellRenderer(int row, int column) {
        return super.getCellRenderer(row, column);    //To change body of overridden methods use File | Settings | File Templates.
//        return new TWTableCellRendererComponent();
    }

    protected JTableHeader createDefaultTableHeader() {
        return new TWTableHeader(columnModel);
    }

    public TableColumnModel getColumnModel() {
        return columnModel;    //To change body of overridden methods use File | Settings | File Templates.
    }

    /* public void paint(Graphics g) {
        super.paint(g);


    }*/

    public static String watermark = "Beta Version";
    public static Font watermarkFont = new TWFont("Arial", Font.BOLD, 120);

    public void paint(Graphics g) {
        super.paint(g);
        if (TWControl.isBetaVersion()) {
       FontMetrics fontMetrics = getFontMetrics(watermarkFont);
        Rectangle rectangle =  getVisibleRect();
//      System.out.println(rectangle);
        Graphics2D g2D = (Graphics2D) g;
        g2D.setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.2f));
        g2D.setColor(Color.red);
        g2D.setFont(watermarkFont);
        g2D.drawString(watermark, (int)rectangle.getX() +  (int)((rectangle.getWidth()  - fontMetrics.stringWidth(watermark))/ 2),
                (int)rectangle.getY() +  (int)(rectangle.getHeight() / 2)  );
    }
    }

    public void propertyChange(PropertyChangeEvent evt) {
        System.out.println(evt.getNewValue());
        if (getTableHeader().getComponentOrientation().isLeftToRight()) {
            adjustColumnWidthsToFit((Integer) evt.getNewValue(), (Integer) evt.getNewValue() + 1);
        } else {
            adjustColumnWidthsToFit((Integer) evt.getNewValue() - 1, (Integer) evt.getNewValue());
        }
    }

    /*public void createDefaultColumnsFromModel() {
//        SharedMethods.printLine("createDefaultColumnsFromModel==",true);
        TableModel m = getModel();
        if (m != null) {
            // Remove any current columns
            synchronized (LOCK) {
                TableColumnModel cm = getColumnModel();
                while (cm.getColumnCount() > 0) {
                    cm.removeColumn(cm.getColumn(0));
                }
            }

            // Create new columns from the data model info
            for (int i = 0; i < m.getColumnCount(); i++) {
                TableColumn newColumn = new TableColumn(i);
                addColumn(newColumn);
            }
        }
    }*/
}