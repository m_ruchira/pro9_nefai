package com.isi.csvr;

import java.io.File;
import java.io.InputStream;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 6, 2006
 * Time: 1:13:20 PM
 * To change this template use File | Settings | File Templates.
 */
public class TutorialExtractor implements Runnable{

    public boolean exists() {
        try {
            File file = new File("tutorial.rar");
            if (file.exists()){
                //file.deleteOnExit();
                return true;
            }else {
                return false;
            }
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
    }

    public void run() {

        // check if OS is Win98 (or Windows Me)
        String os = System.getProperty("os.name");
        boolean osIs98;
        try {
            osIs98 = false;
            if (os == null)
                osIs98 = false;
            else if ((os.indexOf("98") >= 0) || (os.toUpperCase().indexOf("WINDOWS ME") >= 0))
                osIs98 = true;
        } catch (Exception e) {
            osIs98 = false;
        }

        try {
            Process p;
            if (osIs98)
                p = Runtime.getRuntime().exec("command.com /C .\\autoupdate\\tutorial.bat");
            else
                p = Runtime.getRuntime().exec("cmd /C .\\autoupdate\\tutorial.bat");


            if (!osIs98) {
                InputStream in = p.getInputStream();
                byte[] data = new byte[1000];
                while (true) {
                    int i = in.read(data);
                    if (i > 0)
                        System.out.println(new String(data, 0, i));
                    else
                        break;
                }
            }
            p.waitFor();

            File file = new File("tutorial.rar");
            if (file.exists()){
                file.deleteOnExit();
            }
        } catch (Exception e2) {
            e2.printStackTrace();
        }
    }
}
