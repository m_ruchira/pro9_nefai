package com.isi.csvr.shortcutkeys;

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;

import javax.swing.event.InternalFrameListener;
import javax.swing.event.InternalFrameEvent;
import javax.swing.*;
import java.awt.event.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: shanikal
 * Date: Jun 15, 2009
 * Time: 1:21:23 PM
 * To change this template use File | Settings | File Templates.
 */
public class ShortCutViewer extends InternalFrame
        implements ActionListener, MouseListener,
        Themeable, InternalFrameListener {

    ViewSetting settings = null;
    JLayeredPane oLayer;
    GridLayout mainLayout = new GridLayout(0, 1);
    GridBagLayout gridLayout = new GridBagLayout();
    private JPanel mainPanel = new JPanel();
    private static ShortCutViewer self = null;


    public ShortCutViewer() {
        settings = new ViewSetting();
        pack();
        createUI();
        self = this;
    }

    public static ShortCutViewer getInstance(){
        if(self ==  null){
            self = new ShortCutViewer();
        }
        return self;
    }

    private void createUI() {
        setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        setTitle(Language.getString("KEY_FUNCTIONS"));

        oLayer = new JLayeredPane();
        oLayer.setPreferredSize(new Dimension(375, 305));

        oLayer.setBorder(BorderFactory.createLineBorder(Color.black));
        oLayer.add(mainPanel, new Integer(0));
//        mainPanel.setBounds(3, 3, 415, 300);
        mainPanel.setBounds(3, 3, 370, 300);

        oLayer.setOpaque(true);
        oLayer.addMouseListener(this);

        getContentPane().add(oLayer);

        setContentPane(new ShortCutAdjuster());

        this.setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        this.hideTitleBarMenu();
        
        setOrientation();
        pack();
        this.setResizable(false);
        this.setSize(470, 400);
        this.setClosable(true);
    }

     public void internalFrameClosed(InternalFrameEvent e){
        AcceleratorStore.getInstance().fireAcceleratorChanged();
    }
}