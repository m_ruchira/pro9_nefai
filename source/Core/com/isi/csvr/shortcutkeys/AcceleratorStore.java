package com.isi.csvr.shortcutkeys;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sandarekaf
 * Date: Jul 8, 2009
 * Time: 3:07:05 PM
 * To change this template use File | Settings | File Templates.
 */
public class AcceleratorStore {
      static AcceleratorStore self;
    private static ArrayList<AcceleratorListener> listeners;

    public AcceleratorStore() {
        listeners = new ArrayList<AcceleratorListener>();
    }

    public static AcceleratorStore getInstance() {
        if (self == null) {
            self = new AcceleratorStore();
        }
        return self;
    }

    public void addAcceleratorListener(AcceleratorListener listener) {
        listeners.add(listener);
    }

    public void fireAcceleratorChanged(){
        try {
            for (int i = 0; i < listeners.size(); i++) {
                AcceleratorListener listener = listeners.get(i);
                listener.acceleratorChanged();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
