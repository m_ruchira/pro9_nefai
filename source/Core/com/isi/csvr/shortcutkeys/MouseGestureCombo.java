package com.isi.csvr.shortcutkeys;

import com.isi.csvr.shared.Language;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;

/**
 * Created by IntelliJ IDEA.
 * User: sandarekaf
 * Date: Jun 24, 2009
 * Time: 4:15:21 PM
 * To change this template use File | Settings | File Templates.
 */

public class MouseGestureCombo extends JComboBox implements ActionListener, ItemListener {


    public static boolean isUICreate = true;
    private String name;
    private String tag;

    public MouseGestureCombo(String tag, String name) {
        super();

        this.setTag(tag);
        this.setName(name);
        addActionListener(this);
        addItemListener(this);
    }

    public MouseGestureCombo() {
        addActionListener(this);

    }

    public MouseGestureCombo(String[] comboText) {
        //To change body of created methods use File | Settings | File Templates.
    }


    public void actionPerformed(ActionEvent ex) {
        /*int items = this.getItemCount();
        int lastIndex = items-1;
         if(this.getSelectedItem().equals(Language.getString("DEFINE_MOUSE_GESTURE"))){
             MouseGestureDefiner mousegesture = new MouseGestureDefiner(this);
         }*/
    }



    public void actionMethod() {
    }

    public void itemStateChanged(ItemEvent e) {
       if(e.getItem().equals(Language.getString("DEFINE_MOUSE_GESTURE"))
               && e.getStateChange() == ItemEvent.SELECTED){
          MouseGestureDefiner mousegesture = new MouseGestureDefiner(this);
           setSelectedIndex(0);
       }
        /* if(this.getSelectedItem().equals(Language.getString("DEFINE_MOUSE_GESTURE"))){
             MouseGestureDefiner mousegesture = new MouseGestureDefiner(this);
         }*/
    }

    public void settingText(String text) {
        this.addItem(text);
    }

    public String getTag() {
        return tag;
    }

    public void setTag(String tag) {
        this.tag = tag;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }
}

