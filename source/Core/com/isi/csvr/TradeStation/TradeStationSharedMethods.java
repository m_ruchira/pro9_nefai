package com.isi.csvr.TradeStation;

import com.isi.csvr.shared.Meta;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.datastore.Exchange;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.metastock.MetaStockSecurityInfo;
import com.isi.csvr.SymbolMaster;

import java.io.*;
import java.util.ArrayList;
import java.util.Enumeration;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Oct 18, 2010
 * Time: 3:59:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class TradeStationSharedMethods {

    public static boolean TSaddSecurity(String filePath, boolean isIntraday) {
        boolean status = false;
        File file = new File(filePath);
        try {
            status = file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }


        //writing the first line
        if (status) {
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(file, true));
                if (isIntraday) {
                    writer.append("\"Date\",\"Time\",O,H,L,C,V\n");
                } else {
                    writer.append("\"Date\",O,H,L,C,V\n");
                }
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }
        file = null;
        return status;
    }


    public static String getTSFileName(String symbol, String desc) {
        return symbol + ".txt";
    }

    public static void addTSRecords(String securityFilePath, String filePath) {
        ArrayList<TradeStationHistoryRecord> records = new ArrayList<TradeStationHistoryRecord>();
        try {
            File source = new File(securityFilePath);
            String data;
            String[] ohlcData;
            if (source.exists()) {
                BufferedReader in = new BufferedReader(new FileReader(source));
                while (true) {
                    data = in.readLine();
                    if (data == null) break;
                    ohlcData = data.split(Meta.FS);
                    TradeStationHistoryRecord record = new TradeStationHistoryRecord(Integer.parseInt(ohlcData[0]), Float.parseFloat(ohlcData[1]), Float.parseFloat(ohlcData[2]), Float.parseFloat(ohlcData[3]),
                            Float.parseFloat(ohlcData[4]), Long.parseLong(ohlcData[8]));
                    records.add(record);
                }
                in.close();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        //Append the data to the file
        File file = new File(filePath);
        if (file.exists()) {
            BufferedWriter writer = null;
            try {
                writer = new BufferedWriter(new FileWriter(file, true));
                for (TradeStationHistoryRecord record : records) {
                    writer.append(record.getString());
                }
                writer.flush();
                writer.close();
            } catch (IOException e) {
                e.printStackTrace();
            } finally {
                if (writer != null) {
                    try {
                        writer.close();
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                }
            }
        }

    }

    public static void writeAttribFile(String dir, String exchange) {
        //Creating the attrib file after clearing folder
        String attribFile = dir + "\\" + TradeStationConstants.ATTRIB_FILE_NAME;
        File file = new File(attribFile);
        if (!file.exists()) {
            boolean status = false;
            try {
                status = file.createNewFile();
            } catch (IOException e) {
                e.printStackTrace();
            }

            if (status) {
                Enumeration stocks = DataStore.getSharedInstance().getExchangeSymbolStore(exchange).keys();
                ArrayList<TradeStationSecurityInfo> infoList = new ArrayList<TradeStationSecurityInfo>();
                while (stocks.hasMoreElements()) {
                    String data = stocks.nextElement().toString();
                    String symbol = SharedMethods.getSymbolFromExchangeKey(data);
                    int instrument = SharedMethods.getInstrumentFromExchangeKey(data);
                    String key = SharedMethods.getKey(exchange, symbol, instrument);
                    infoList.add(new TradeStationSecurityInfo(symbol, getCompanyName(key)));
                }

                BufferedWriter writer = null;
                try {
                    writer = new BufferedWriter(new FileWriter(file));
                    writer.append(TradeStationConstants.ATTRIB_FILE_LINE_1);
                    String line2 = TradeStationConstants.ATTRIB_FILE_LINE_2;
                    line2 = line2.replaceAll("<EXCHANGE>", exchange);
                    line2 = line2.replaceAll("<PRICE_SCALE>", getPriceScaleForExchange(exchange));
                    String lineTemp;

                    for (TradeStationSecurityInfo info : infoList) {
                        lineTemp = line2;
                        lineTemp = lineTemp.replaceAll("<SYMBOL>", info.getSymbol());
                        lineTemp = lineTemp.replaceAll("<DESC>", info.getDescription());
                        writer.append(lineTemp);
                        writer.flush();
                    }
                    writer.close();
                } catch (IOException e) {
                    e.printStackTrace();
                } finally {
                    if (writer != null) {
                        try {
                            writer.close();
                        } catch (IOException e) {
                            e.printStackTrace();
                        }
                    }
                }
            }

        }
        file = null;
    }

    public static String getCompanyName(String key) {
        try {
            if (DataStore.getSharedInstance().isValidSymbol(key)) {
                String companyName = SymbolMaster.getEnglishCompanyName(key);
                if (companyName != null) {
                    return companyName;
                } else {
                    return SharedMethods.getSymbolFromKey(key);
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return "";
    }

    public static String getPriceScaleForExchange(String exchange) {
        Exchange ex = ExchangeStore.getSharedInstance().getExchange(exchange);
        String decimalFormat = "1/100";
        if (ex != null) {
            byte decimalNumbers = ex.getPriceDecimalPlaces();
            switch (decimalNumbers) {
                case 1:
                    decimalFormat = "1/10";
                    break;
                case 2:
                    decimalFormat = "1/100";
                    break;
                case 3:
                    decimalFormat = "1/1000";
                    break;
                default:
                    decimalFormat = "1/100";
                    break;

            }
        }
        return decimalFormat;
    }

    public static void TSdeleteOldData(String dir, String exchange) {
        try {
            File file = new File(dir);
            for (File f : file.listFiles()) {
                f.delete();
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        writeAttribFile(dir, exchange);
    }
}
