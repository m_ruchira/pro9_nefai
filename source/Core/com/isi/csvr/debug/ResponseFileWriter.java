package com.isi.csvr.debug;

import com.dfn.mtr.mix.beans.MIXObject;
import com.dfn.mtr.mix.sabb.beans.MIXExtendedObject;
import com.isi.csvr.Client;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.GUISettings;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.ValueFormatter;
import com.isi.csvr.trading.SendQueue;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.net.URI;
import java.net.URISyntaxException;
import java.util.Calendar;

/**
 * Created by IntelliJ IDEA.
 * User: shanakak
 * Date: Jul 26, 2012
 * Time: 1:11:19 PM
 * To change this template use File | Settings | File Templates.
 */
public class ResponseFileWriter {
    private static ResponseFileWriter self;

    private ResponseFileWriter() {
    }

    public static synchronized ResponseFileWriter getSharedInstance() {
        if (self == null) {
            self = new ResponseFileWriter();
        }
        return self;
    }

    public void writeToFileRQ(String response) {
        try {
            String write = "[Time: %s]  Response: %s\n";
            // Create file
            Calendar c = Calendar.getInstance();

//            FileWriter fstream = new FileWriter(System.getProperties().get("user.dir")+"receive_out_pro.txt",true);
            FileWriter fstream = new FileWriter("D:/receive_out_pro.txt",true);
//            FileWriter fstream = new FileWriter("/System/out.txt",true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(String.format(write,c.getTime().toString(),response));
//            out.write("hiiiiii");
            //Close the output stream
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void writeToFileReceiveQueue(String response) {
        writeToFileRQ(response);
    }

    public void writeToFileSQ(String response) {
        try {
            String write = "[Time: %s] Request: %s\n";
            // Create file
            Calendar c = Calendar.getInstance();

            FileWriter fstream = new FileWriter("D:/send_out_pro.txt",true);
//            FileWriter fstream = new FileWriter("/System/out.txt",true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(String.format(write,c.getTime().toString(),response));
            //Close the output stream
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void writeToFileSendQueue(String resObject) {
        writeToFileSQ(resObject);
    }




    /*private ResponseFileWriter() {
    }

    public static synchronized ResponseFileWriter getSharedInstance() {
        if (self == null) {
            self = new ResponseFileWriter();
        }
        return self;
    }

    public void writeToFile(MIXObject response, Byte path) {
        try {
            String write = "[Time: %s] [Path is: %s] Response: %s\n";
            // Create file
            Calendar c = Calendar.getInstance();

            FileWriter fstream = new FileWriter("D:/ResponseSABB/out.txt",true);
//            FileWriter fstream = new FileWriter("/System/out.txt",true);
            BufferedWriter out = new BufferedWriter(fstream);
            out.write(String.format(write,c.getTime().toString(),path,response.toString()));
            //Close the output stream
            out.close();
        } catch (Exception e) {//Catch exception if any
            System.err.println("Error: " + e.getMessage());
        }
    }

    public void writeToFile(MIXObject resObject) {
        writeToFile(resObject, null);
    }*/
    ///////////////////////////
    /*
    public static void main(String[] args) {
        String mob = "0773606568";
        String duration = "1 minute";
        new ResponseFileWriter().otpPane();
    }

    public void otpPane() {
        final JDialog di = new JDialog();
        di.setTitle("OTP");
        di.setSize(450, 130);
        di.setResizable(false);
        di.setAlwaysOnTop(true);
        di.setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
        di.addWindowListener(new WindowAdapter() {
            public void windowClosing(WindowEvent e) {
                Client.getInstance().setTradeSessionDisconnected(true);
            }
        });
        JPanel msgPanel = new JPanel();
        JPanel inputPanel = new JPanel();
        JPanel linkPanel = new JPanel();

//        String mob2 = "";
//        int length = mob.length();
//        String lastDigits = mob.substring(length - 3, length);
//        String xDigits = Language.getString("MOB_NUM_FORMAT");
//        mob2 = xDigits.concat(lastDigits);

//        JLabel msg1 = new JLabel(Language.getString("OTP_LINE1") + " " + mob2);
        JLabel msg1 = new JLabel("OTP_LINE1");
        JLabel msg2 = new JLabel("OTP_LINE2");
        JLabel msg3 = new JLabel("OTP_LINE3");
        JLabel link = new JLabel("OTP_LINE4");
        JLabel lblOTP = new JLabel("OTP_NUMBER");
        JLabel lblClick = new JLabel("CLICK_HERE");
        lblClick.setForeground(Color.BLUE);
        JButton btnSend = new JButton("SEND");
        JButton btnCancel = new JButton("CANCEL");
        final JTextField otpNumber = new JTextField(6);
        otpNumber.setDocument(new ValueFormatter(ValueFormatter.POSITIVE_INTEGER));
        msgPanel = new JPanel(new FlexGridLayout(new String[]{"100%"}, new String[]{"15", "15"}, 2, 3));
        inputPanel = new JPanel(new FlexGridLayout(new String[]{"25%", "30%", "20%", "20%"}, new String[]{"20"}, 2, 8));
        linkPanel = new JPanel(new FlexGridLayout(new String[]{"15%", "80%"}, new String[]{"15"}, 2, 3));


        lblClick.setCursor(Cursor.getPredefinedCursor(Cursor.HAND_CURSOR));
        lblClick.addMouseListener(new MouseAdapter() {
            public void mouseClicked(MouseEvent e) {
                if (e.getClickCount() > 0) {
                    try {
//                        MIXObject mixBasicObject = otpResend(userID, username);
//                        SendQueue.getSharedInstance().writeData(mixBasicObject.getMIXString(), Constants.PATH_PRIMARY);
//                        di.dispose();
                    } catch (Exception e1) {
                        e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        });

        msgPanel.add(msg1);
        msgPanel.add(msg2);
//        msgPanel.add(msg3);
        inputPanel.add(lblOTP);
        inputPanel.add(otpNumber);
        inputPanel.add(btnSend);
        inputPanel.add(btnCancel);
        linkPanel.add(lblClick);
        linkPanel.add(link);

        btnSend.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Object[] option = {"OK"};
                if (new String(otpNumber.getText()).trim().equals("")) {
                    JOptionPane.showOptionDialog(di,
                            "OTP_NULL_VALUE",
                            "ERROR",
                            JOptionPane.OK_OPTION,
                            JOptionPane.ERROR_MESSAGE,
                            null,
                            option,
                            option[0]);
                } else {
                    try {
//                        MIXObject mixBasicObject = TradeMethods.getSharedInstance().authenticateRequestGenerator(username, null, otpNumber.getText());
//                        SendQueue.getSharedInstance().writeData(mixBasicObject.getMIXString(), Constants.PATH_PRIMARY);
                        di.dispose();
                    } catch (Exception e1) {
                        e1.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                    }
                }
            }
        });
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().setTradeSessionDisconnected(true);
                di.dispose();
//                LoginWindow loginDialog = new LoginWindow(Client.getInstance().getFrame(), Login.MODE_TRADING_SERVERS);
            }
        });
//        Object[] array = {msgPanel, linkPanel};
        Object[] array = {msgPanel};
        Object[] options = {inputPanel};
        JOptionPane otpPane = new JOptionPane(array,
                -1,
                JOptionPane.OK_OPTION,
                null,
                options,
                options[0]);

        otpPane.setName("OTP");
        otpPane.add(msgPanel);
        otpPane.add(inputPanel);
//        otpPane.add(linkPanel);
//        di.setLocationRelativeTo(Client.getInstance().getFrame());
        GUISettings.applyOrientation(di);
        GUISettings.applyOrientation(otpPane);
        di.setContentPane(otpPane);
        di.setVisible(true);
    } */
}
