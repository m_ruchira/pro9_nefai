package com.isi.csvr.chart;

/**
 * <p>Title: </p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: </p>
 * @author unascribed
 * @version 1.0
 */

import com.isi.csvr.shared.Language;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;


public class Volume extends ChartProperties implements Indicator, Serializable {
	private ChartProperties innerSource;

    private static final long serialVersionUID = UID_Volume;
    private String tableColumnHeading;

	public Volume(ArrayList data, String symbl, byte anID, Color c, WindowPanel r){
		super(data, Language.getString("VOLUME") + Indicator.FD + symbl, anID, c, r);
		setOHLCPriority(GraphDataManager.INNER_Volume);
	}

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout){
        super.loadTemplate(xpath, document, preExpression, isLayout);
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document){
        super.saveTemplate(chart, document);
    }

	public void assignValuesFrom(ChartProperties cp){
		super.assignValuesFrom(cp);
		if (cp instanceof Volume){
			Volume vol = (Volume)cp;
			this.innerSource = vol.innerSource;
		}
	}

	public String toString(){
		String[] sa = getSymbol().split(Indicator.FD);
		String parent;
		if (sa.length>=2){
			parent = "("+StockGraph.extractSymbolFromStr(sa[1])+") ";
		}else{
			parent = "";
		}
		return Language.getString("VOLUME") +parent;
	}
	//############################################
	//implementing Indicator
	public boolean hasItsOwnScale(){
		return true;
	}
	public ChartProperties getInnerSource(){
		return innerSource;
	}
	public void setInnerSource(ChartProperties cp){
		innerSource = cp;
	}
	public int getInnerSourceIndex(ArrayList Sources){
		if (Sources!=null)
		for (int i=0; i<Sources.size(); i++) {
			ChartProperties cp = (ChartProperties)Sources.get(i);
			if (cp==innerSource) return i;
		}
		return 0;
	}

	public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index){
		//long entryTime = System.currentTimeMillis();
        ChartRecord cr;
		for (int i=0; i<al.size(); i++) {
            cr = (ChartRecord)al.get(i);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint!=null){
//                if (cr.Volume > 0) {
                    aPoint.setIndicatorValue(cr.Volume);
//                } else {
//                    GDM.removeIndicatorPoint(cr.Time, index, getID()); // TO FIX A BUG
//                }
            }
		}
		//System.out.println("**** Volume Calc time " + (entryTime - System.currentTimeMillis()));
	}
	//############################################

	public String getShortName(){
		return Language.getString("VOLUME");
	}


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
}