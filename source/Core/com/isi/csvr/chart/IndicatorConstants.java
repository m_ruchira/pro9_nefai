package com.isi.csvr.chart;

/**
 * Created by IntelliJ IDEA.
 * User: shashikaw
 * Date: Apr 17, 2009
 * Time: 10:38:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorConstants {

    public static String[] keyWords = {"and", "do", "else", "end", "for", "if", "or", "param", "then", "while",
            "drawsymbol", "markpattern", "plot",
            "abs", "acos", "asin", "atan", "atan2", "ceiling", "cos", "cosh", "cum", "exp",
            "floor", "frac", "ln", "log", "max", "min", "pow", "prec", "ref", "round",
            "sign", "sin", "sinh", "sqr", "sqrt", "sum", "tanh", "trunc",
            "highest", "highestbars", "hhv", "hhvbars", "highestsince", "highestsincebars",
            "lowest", "lowestbars", "llv", "llvbars", "lowestsince", "lowestsincebars",
            "int", "double", "MAMethod", "CalcMethod", "DataArray", "bool", "RGB", "buy", "sell", "sellshort", "buytocover", "CopCurvePeriod"
    };

    public static String[] literals = {
            "true", "false", "Open", "High", "Low", "Close", "Volume", "Percent", "Price",
            "Exponential", "Simple", "TimeSeries", "Triangular", "Variable", "VolumeAdjusted", "Weighted",
            "Black", "Blue", "Navy", "Gray", "Silver", "Red", "Green", "Yellow", "Pink",
            "Orange", "Brown", "White", "Olive", "Cyan", "Purple", "Magenta", "Lime", "Maroon",
            "BuyArrow", "SellArrow", "ThumbsUp", "ThumbsDown", "HappyFace", "SadFace", "Flag", "Bomb", "Circle", "Diamond",
            "Solid", "Dot", "Dash", "DashDot", "DashDotDot", "Above", "Middle", "Center", "Below", "Monthly", "Daily", "Hidden", ""
    };

    public static String[] indicators = {
            "MovingAverage", "StandardDeviation", "LinearRegression", "CommodityChannelIndex", "DEMA",
            "AccumulationDistribution", "AverageTrueRange", "BollingerBands", "ChaikinMoneyFlow",
            "ChaikinADOscillator", "ChandeMomentumOscillator", "IntradayMomentumIndex", "LinearRegression",
            "MACD", "MACDHistogram", "Momentum", "MovingAverage", "ParabolicSAR", "PriceROC",
            "RelativeStrengthIndex", "StochasticOscillator", "TimeSeriesForecast", "DetrendedPriceOscillator",
            "WildersSmoothing", "ZigZag", "ZigZagSlope",
            "MedianPrice", "MoneyFlowIndex", "NegativeVolumeIndex", "Qstick",
            "RelativeMomentumIndex", "RelativeVolatilityIndex", "StochasticMomentumIndex", "SwingIndex", "TEMA", "TimeSeriesForcast", "TRIX", "TypicalPrice",
            "VerticalHorizontalFilter", "VolumeOscillator", "VolumeROC", "WeightedClose", "WilliamsR", "WillsAccumDistribution", "ZigZag", "KlingerOscillator",
            "MassIndex", "PriceOscillator" ,"CoppockCurve"

            //TODO:add later
            //"MACDVolume",   "MarketFacilitationIndex","EaseOfMovement",  "Envelope","ForcastOscillator","IchimokuKinkoHyo","OnBalanceVolume", "StandardError"
            //  "Performance",,"PositiveVolumeIndex", "PriceChannel","PriceVolumeTrend",  "Aroon","Volatility"
    };
}
