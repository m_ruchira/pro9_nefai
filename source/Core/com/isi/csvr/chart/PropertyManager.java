package com.isi.csvr.chart;

import com.isi.csvr.chart.chartobjects.AbstractObject;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public interface PropertyManager {
	public void setTargetCP(ChartProperties target);
	public void setTargetOR(AbstractObject target);
	public ChartProperties getTargetCP();
	public AbstractObject getTargetOR();
//	public void getPropertyValuesFromTarget();
//	public void setPropertyValuesToTarget();
}