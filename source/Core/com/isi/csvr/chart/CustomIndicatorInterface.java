package com.isi.csvr.chart;

import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Apr 11, 2007
 * Time: 4:03:21 PM
 */
public class CustomIndicatorInterface {        

    public static String getIndicatorBaseClassName() {
        return IndicatorBase.class.getName();
    }

    public static String getWindowPanelClassName() {
        return WindowPanel.class.getName();
    }

    public static String getChartPropertiesClassName() {
        return ChartProperties.class.getName();
    }

    public static String getChartRecordClassName() {
        return ChartRecord.class.getName();
    }

    public static String getChartPointClassName() {
        return ChartPoint.class.getName();
    }

    public static String getTemplateFactoryClassName() {
        return TemplateFactory.class.getName();
    }

    public static String getBollingerBandsClassName() {
        return BollingerBand.class.getName();
    }

    public static String getChandeMomentOcslClassName() {
        return ChandeMomentumOscillator.class.getName();
    }

    public static String getDEMAClassName() {
        return DEMA.class.getName();
    }

    public static String getDetrendPriceOcslClassName() {
        return DetrendPriceOscillator.class.getName();
    }

    public static String getEnvelopesClassName() {
        return Envelope.class.getName();
    }

    public static String getForcastOcslClassName() {
        return ForcastOscillator.class.getName();
    }

    public static String getLinearRegressionClassName() {
        return LinearRegression.class.getName();
    }

    public static String getMACDClassName() {
        return MACD.class.getName();
    }

    public static String getMomentumClassName() {
        return Momentum.class.getName();
    }

    public static String getMovingAverageClassName() {
        return MovingAverage.class.getName();
    }

    public static String getOBVClassName() {
        return OnBalanceVolume.class.getName();
    }

    public static String getParabolicSARClassName() {
        return ParabolicSAR.class.getName();
    }
    public static String getPerformance() {
        return Performance.class.getName();
    }

    public static String getPriceOcslClassName() {
        return PriceOscillator.class.getName();
    }

    public static String getPriceROCClassName() {
        return PriceROC.class.getName();
    }

    public static String getRelativeMomentumClassName() {
        return RelativeMomentumIndex.class.getName();
    }

    public static String getRelativeStrengthClassName() {
        return RSI.class.getName();
    }

    public static String getStandardDeviationClassName() {
        return StandardDeviation.class.getName();
    }

    public static String getStandardErrorClassName() {
        return StandardError.class.getName();
    }

    public static String getStochasticOscillatorClassName() {
        return StocasticOscillator.class.getName();
    }

    public static String getTEMAClassName() {
        return TEMA.class.getName();
    }

    public static String getTSFClassName() {
        return TimeSeriesForcast.class.getName();
    }

    public static String getTRIXClassName() {
        return TRIX.class.getName();
    }

    public static String getVHFClassName() {
        return VerticalHorizontalFilter.class.getName();
    }

    public static String getWildersSmoothingClassName() {
        return WildersSmoothing.class.getName();
    }

    public static String getZigZagClassName() {
        return ZigZag.class.getName();
    }

    public static String getCopockCurveClassName() {
        return CoppockCurve.class.getName();
    }

    public static String getZigZagSlopeClassName() {
        return ZigZagSlope.class.getName();
    }

     public static String getCommodityChannelIndexClassName() {
        return CommodityChannelIndex.class.getName();
    }

    public static String getAccumulationDistributionClassName() {
        return AccumulationDistribution.class.getName();
    }

    public static String getAroonClassName() {
        return Aroon.class.getName();
    }

    public static String getAverageTrueRangeClassName() {
        return AverageTrueRange.class.getName();
    }

    public static String getChaikinADOscillatorClassName() {
        return ChaikinOscillator.class.getName();
    }

    public static String getChaikinMoneyFlowClassName() {
        return ChaikinMoneyFlow.class.getName();
    }

    public static String getEaseOfMovementClassName() {
        return EaseOfMovement.class.getName();
    }

    public static String getForcastOscillatorClassName() {
        return ForcastOscillator.class.getName();
    }

    public static String getIchimokuKinkoHyoClassName() {
        return IchimokuKinkoHyo.class.getName();
    }

    public static String getIntradayMomentumIndexClassName() {
        return IntradayMomentumIndex.class.getName();
    }

    public static String getKlingerOscillatorClassName() {
        return KlingerOscillator.class.getName();
    }

    public static String getMACDVolumeClassName() {
        return MACDVolume.class.getName();
    }

    public static String getMarketFacilitationIndexClassName() {
        return MarketFacilitationIndex.class.getName();
    }

    public static String getMassIndexClassName() {
        return MassIndex.class.getName();
    }

    public static String getMedianPriceClassName() {
        return MedianPrice.class.getName();
    }

    public static String getMoneyFlowIndexClassName() {
        return MoneyFlowIndex.class.getName();
    }

    public static String getNegativeVolumeIndexClassName() {
        return NegativeVolumeIndex.class.getName();
    }

    public static String getPositiveVolumeIndexClassName() {
        return PositiveVolumeIndex.class.getName();
    }

    public static String getPriceChannelClassName() {
        return PriceChannel.class.getName();
    }

    public static String getPriceVolumeTrendClassName() {
        return PriceVolumeTrend.class.getName();
    }

    public static String getQstickClassName() {
        return Qstick.class.getName();
    }

    public static String getRelativeVolatilityIndexClassName() {
        return RelativeVolatilityIndex.class.getName();
    }

    public static String getStochasticMomentumIndexClassName() {
        return StochasticMomentumIndex.class.getName();
    }

    public static String getSwingIndexClassName() {
        return SwingIndex.class.getName();
    }

    public static String getTypicalPriceClassName() {
        return TypicalPrice.class.getName();
    }

    public static String getVolatilityClassName() {
        return Volatility.class.getName();
    }

    public static String getVolumeOscillatorClassName() {
        return VolumeOscillator.class.getName();
    }

    public static String getVolumeROCClassName() {
        return VolumeROC.class.getName();
    }

    public static String getWeightedCloseClassName() {
        return WeightedClose.class.getName();
    }

    public static String getWilliamsRClassName() {
        return WilliamsR.class.getName();
    }

    public static String getWillsAccumDistributionClassName() {
        return WillsAccumDistribution.class.getName();
    }

    /*public static String getZigZagClassName() {
        return ZigZag.class.getName();
    }*/

    public static void calculateIndicator(String className, GraphDataManagerIF GDM, ArrayList al, int bIndex, Object... oa ) {
        try {
            if (className.equals(MovingAverage.class.getName())) {
                System.out.println("MAVG");
                MovingAverage.calculateIndicator(GDM, al, bIndex, (Integer)oa[0], (MAMethod)oa[1], (Byte)oa[2], (Byte)oa[3], (Byte)oa[4], (Byte)oa[5]);
            } else if (className.equals(StandardDeviation.class.getName())) {
                System.out.println("SDEV");
                double deviations;
                if (oa[1] instanceof Integer) {
                    deviations = (Integer)oa[1];
                } else {
                    deviations = (Double)oa[1];
                }
                StandardDeviation.calculateIndicator(GDM, al, bIndex, (Integer)oa[0], deviations, (Byte)oa[2], (Byte)oa[3]);
            } else if (className.equals(LinearRegression.class.getName())) {
                System.out.println("LRE");
                LinearRegression.calculateIndicator(GDM, al, bIndex, (Integer)oa[0], (Byte)oa[1], (Byte)oa[2]);
            } else if (className.equals(ZigZag.class.getName())) {
                System.out.println("ZZS");
                double reversalAmount;
                if (oa[1] instanceof Integer) {
                    reversalAmount = (Integer)oa[0];
                } else {
                    reversalAmount = (Double)oa[0];
                }
                ZigZag.calculateIndicator(GDM, al, bIndex, reversalAmount, (CalcMethod)oa[1], (Byte)oa[2], (Byte)oa[3]);
            } else if (className.equals(ZigZagSlope.class.getName())) {
                System.out.println("ZZS");
                double reversalAmount;
                if (oa[1] instanceof Integer) {
                    reversalAmount = (Integer)oa[0];
                } else {
                    reversalAmount = (Double)oa[0];
                }
                ZigZagSlope.calculateIndicator(GDM, al, bIndex, reversalAmount, (CalcMethod)oa[1], (Byte)oa[2], (Byte)oa[3]);
            }else if (className.equals(CommodityChannelIndex.class.getName())) {
                System.out.println("CCI");
                double reversalAmount;
                if (oa[1] instanceof Integer) {
                    reversalAmount = (Integer)oa[0];
                } else {
                    reversalAmount = (Double)oa[0];
                }
                ZigZagSlope.calculateIndicator(GDM, al, bIndex, reversalAmount, (CalcMethod)oa[1], (Byte)oa[2], (Byte)oa[3]);
            }else if (className.equals(CoppockCurve.class.getName())) {
                System.out.println("MICC");
                CoppockCurve.calculateIndicator(GDM, al, bIndex, (CopCurvePeriod)oa[0], (Byte)oa[1],(Byte)oa[2], (Byte)oa[3],(Byte)oa[4]);
            }
            //TODO: Do for the other inbuilt indicators
        } catch (Exception e) {
            e.printStackTrace();
        }
    }
}
