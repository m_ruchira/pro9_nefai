package com.isi.csvr.chart;

import java.awt.*;

/**
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 1:09:03 PM
 */
public interface TextContainer {
    Font getFont();
    String getText();
    void setFont(Font f);
    void setText(String s);
}
