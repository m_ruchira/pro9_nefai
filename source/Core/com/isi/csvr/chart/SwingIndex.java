package com.isi.csvr.chart;

import com.isi.csvr.shared.Language;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;

import java.util.ArrayList;
import java.awt.*;

/**
 * User: Pramoda
 * Date: Mar 27, 2006
 * Time: 2:47:58 PM
 */
public class SwingIndex extends ChartProperties implements Indicator {

    //Fields
    private int limitMove;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    private static final long serialVersionUID = UID_SWING_INDEX;

    public SwingIndex(ArrayList data, String symbl, byte anID, Color c, WindowPanel r){
        super(data, Language.getString("IND_SWING_INDEX") + Indicator.FD+symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_SWING_INDEX");
        this.limitMove = 3;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get( anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setLimitMove(idp.getTimePeriods());//intentional
        }
    }
    public void assignDefaultValues(){
        super.assignDefaultValues();
        this.limitMove = 3;
    }
    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout){
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.limitMove = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression+"/LimitMove"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document){
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "LimitMove", Integer.toString(this.limitMove));
    }

    public void assignValuesFrom(ChartProperties cp){
        super.assignValuesFrom(cp);
        if (cp instanceof SwingIndex){
            SwingIndex si = (SwingIndex)cp;
            this.limitMove = si.limitMove;
            this.innerSource = si.innerSource;
        }
    }

    public String toString(){
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length>=2){
            parent = "("+StockGraph.extractSymbolFromStr(sa[1])+") ";
        }else{
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
//        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_SWING_INDEX") + " " + parent;
    }

    public int getLimitMove() {
        return limitMove;
    }

    public void setLimitMove(int limitMove) {
        this.limitMove = limitMove;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale(){
        return true;
    }

    public ChartProperties getInnerSource(){
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp){
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources){
        if (Sources!=null)
        for (int i=0; i<Sources.size(); i++) {
            ChartProperties cp = (ChartProperties)Sources.get(i);
            if (cp==innerSource) return i;
        }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index){
        long entryTime = System.currentTimeMillis();
        ChartRecord cr, crOld;

        for(int i=1; i<al.size(); i++){
            cr = (ChartRecord)al.get(i);
            crOld = (ChartRecord)al.get(i-1);

            double F = Math.abs(cr.High - crOld.Close);
            double G = Math.abs(cr.Low - crOld.Close);
            double H = Math.abs(cr.High - cr.Low);
            double I = Math.abs(crOld.Close - crOld.Open);
            double J = cr.Close - crOld.Close;
            double K = cr.Close - cr.Open;
            double L = crOld.Close - crOld.Open;

            double M = J + 0.5f*K + 0.25f*L;
            double N = Math.max(F, G);
            double O = Math.max(H, N) + 0.25f*I;

            double swing = (O==0)?0f:50f * M/O * N/limitMove;
            swing = Math.min(100f, Math.max(-100f, swing));

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint!=null){
                aPoint.setIndicatorValue(swing);
            }
        }
        //System.out.println("**** SwingIndex Calc time " + (entryTime - System.currentTimeMillis()));
    }
    //############################################

    public String getShortName(){
        return Language.getString("IND_SWING_INDEX");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }
    
    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 3;
    }
}
