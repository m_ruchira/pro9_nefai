package com.isi.csvr.chart;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Jan 18, 2010
 * Time: 5:33:28 PM
 * Version: 0.1
 * To change this template use File | Settings | File Templates.
 */
public enum CopCurvePeriod {
    MONTHLY, DAILY
}
