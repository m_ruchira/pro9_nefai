/*
 * Classname:     ZoomingSlider.java
 * @author Udaka Liyanapathirana
 * Version:       Chart 1.0_beta
 * Date:          22nd August 2001
 * Copyright:     (c) 2001 Integrated Systems International (Pvt)Ltd. All Rights Reserved.
 */

package com.isi.csvr.chart;


import com.isi.csvr.shared.*;

import javax.swing.*;
import java.awt.*;
import java.awt.event.*;
import java.io.Serializable;


public class ZoomingSlider extends JLabel implements Serializable {

    //public final int MIN_HANDLE_WIDTH = 50*7;
    public final int MIN_HANDLE_WIDTH = 50;

    public SizableHandle handle;
    protected Rectangle ClipRect;
    protected int minLeft;
    protected int maxLeft;
    protected int minRight;
    protected int maxRight;
    protected int currX;
    public boolean sliderVisible = true;
    public boolean sizingLeft = false;
    public boolean sizingRight = false;
    public boolean sliderdragged = false;
    public boolean Moving = false;
    private float zoom = 100;        // Initial Slider Size
    private float beginPos = 0;
    private float totalX = 0;
    private float shownX = 0;
    private float beginX = 0;
    private String caption = "";      // Caption on SLider
    private float minZoom = 0;

    private Color sliderFGColor = new Color(230, 230, 230);
    private Color sliderBGColor = new Color(203, 204, 205);
    //private float minZoom = 6;

    public class SizableHandle {
        private int Left, Top, Width, Height, availableW;
        public double dWidth, dLeft;
        final private Rectangle leftRect = new Rectangle();
        final private Rectangle rightRect = new Rectangle();

        public SizableHandle(int W, int H) {
            availableW = W - MIN_HANDLE_WIDTH;
            Width = (int) Math.round(availableW * zoom / 100);
            dWidth = availableW * zoom / 100;
            Height = H - 2;
            Left = (int) Math.round(availableW * beginPos / 100);
            dLeft = availableW * beginPos / 100;
            Top = 1;
            setLeftRect();
            setRightRect();
        }

        public int getLeft() {
            return Left;
        }

        public int getTop() {
            return Top;
        }

        public int getWidth() {
            return Width;
        }

        public int getHeight() {
            return Height;
        }

        public void setLeft(int L) {
            Left = L;
        }

        public void setTop(int T) {
            Top = T;
        }

        public void setWidth(int W) {
            Width = W;
        }

        public void setHeight(int H) {
            Height = H;
        }

        public Rectangle getLeftRect() {
            return leftRect;
        }

        public Rectangle getRightRect() {
            return rightRect;
        }

        public Rectangle getBoundsRect() {
            return new Rectangle(Left, Top, Width + MIN_HANDLE_WIDTH, Height);
        }

        public void setLeftRect() {
            leftRect.setBounds(Left + 2, Top + 2, 2, Height - 4);
        }

        public void setRightRect() {
            rightRect.setBounds(Left + Width + MIN_HANDLE_WIDTH - 4, Top + 2, 2, Height - 4);
        }

        public void MoveLeft(int Movement) {
            Width = Width - Movement;
            dWidth = dWidth - Movement;
            Left = Left + Movement;
            dLeft = dLeft + Movement;
            //if (Width-Movement<20){
            //  Width = 20;
            //}
            //if (Left+Movement<0){
            //  Left = 0;
            //}
            setLeftRect();
        }

        public void MoveRight(int Movement) {
            Width = Width + Movement;
            dWidth = dWidth + Movement;
            //if (Width+Movement<20){
            //  Width = 20;
            //}
            setRightRect();
        }

        public void MoveHandle(int Movement) {
            Left = Left + Movement;
            dLeft = dLeft + Movement;
            //if (Left+Movement<0){
            //  Left = 0;
            //}
            setLeftRect();
            setRightRect();
        }

        public void refreshHandle(int W) {
            availableW = W - MIN_HANDLE_WIDTH - 2;
            setWidth((int) Math.round(availableW * zoom / 100));
            dWidth = availableW * zoom / 100;
            setLeft((int) Math.round(availableW * beginPos / 100) + 1);
            dLeft = availableW * beginPos / 100 + 1;
            setLeftRect();
            setRightRect();
        }

    }

    //###############################################################
    // ZoomingSlider methods
    //###############################################################
    public ZoomingSlider(int W, int H, float zoom, float beginPos) {
        try {
            jbInit();
            this.zoom = zoom;
            this.beginPos = beginPos;
            handle = new SizableHandle(W, H);
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void jbInit() throws Exception {
        this.setLayout(null);
        this.addMouseMotionListener(new MouseMotionAdapter() {

            public void mouseMoved(MouseEvent e) {
                this_mouseMoved(e);
            }

            public void mouseDragged(MouseEvent e) {
                this_mouseDragged(e);
            }
        });
        this.addMouseListener(new MouseAdapter() {

            public void mousePressed(MouseEvent e) {
                this_mousePressed(e);
            }

            public void mouseReleased(MouseEvent e) {
                this_mouseReleased(e);
            }
        });
//        this.addMouseWheelListener(new MouseWheelListener() {
//            public void mouseWheelMoved(MouseWheelEvent e) {
//                this_mouseWheelMoved(e);
//            }
//        });
    }

    public void resetSlider(float all, float shown, float begin) { //float zoom, float beginPos, int width){

        totalX = all;
        shownX = shown;
        beginX = begin;
        //System.out.println("shownX "+shownX+" totalX "+totalX+" beginX "+beginX);
        zoom = ((float) shownX / (float) totalX) * 100;
        beginPos = ((float) beginX / (float) totalX) * 100;
        handle.refreshHandle(getWidth());
        repaint();

    }

    public void setSliderFGColor(Color c) {
        sliderFGColor = c;
        this.repaint();
    }

    public void setSliderBGColor(Color c) {
        sliderBGColor = c;
        this.repaint();
    }

    public float getZoom() {
        return zoom;
    }

    public float getBeginPos() {
        return beginPos;
    }

    public void setMinZoom(float zoom) {
        minZoom = zoom;
    }


    // public long getTotalX(){
    //   return totalX;
    // }

    //public long getShownX(){
    //  return shownX;
    //}

    public void setZoom() {

//    System.out.println("handleWidth " + handle.getWidth()+
//					   " Width " + (getWidth()-MIN_HANDLE_WIDTH)+
//					   " handleLeft " + handle.getLeft());
        zoom = (float) handle.getWidth() * 100f / (getWidth() - MIN_HANDLE_WIDTH - 2);
        if (zoom < minZoom)
            zoom = minZoom;

        beginPos = (handle.getLeft() - 1f) * 100f / (getWidth() - MIN_HANDLE_WIDTH - 2);
        //sliderVisible = true;
        repaint();
    }

    public void setCaption(String s) {
        caption = s;
        sliderVisible = true;
        repaint();
    }

    void this_mousePressed(MouseEvent e) {
        currX = e.getX();
        minLeft = currX - handle.getLeft() + 1;
        maxLeft = (int) handle.getRightRect().getX() - MIN_HANDLE_WIDTH;
        minRight = handle.getLeft() + handle.getWidth() + MIN_HANDLE_WIDTH - currX + 1;
        maxRight = (int) handle.getLeftRect().getX() + 1 + MIN_HANDLE_WIDTH;

        if (isOnLeftRect(e.getX(), e.getY())) {
            sizingLeft = true;
        } else if (isOnRightRect(e.getX(), e.getY())) {
            sizingRight = true;
        } else if (isOnHandle(e.getX(), e.getY())) {
            Moving = true;
            setCursor(new Cursor(Cursor.MOVE_CURSOR));
        }
    }

    void this_mouseReleased(MouseEvent e) {
        sizingLeft = false;
        sizingRight = false;
        if (Moving) {
            setCursor(Cursor.getDefaultCursor());
            Moving = false;
        }
    }

    private boolean isOnLeftRect(int x, int y) {
        Rectangle r = new Rectangle(handle.getLeft(), 0, 4, getHeight());
        if (r.contains(x, y)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isOnRightRect(int x, int y) {
        Rectangle r = new Rectangle(handle.getLeft() + handle.getWidth() + MIN_HANDLE_WIDTH - 4, 0, 4, getHeight());
        if (r.contains(x, y)) {
            return true;
        } else {
            return false;
        }
    }

    private boolean isOnHandle(int x, int y) {
        if (handle.getBoundsRect().contains(x, y)) {
            return true;
        } else {
            return false;
        }
    }

    void this_mouseMoved(MouseEvent e) {
        if (isOnLeftRect(e.getX(), e.getY())) {
            setCursor(new Cursor(Cursor.W_RESIZE_CURSOR));
        } else if (isOnRightRect(e.getX(), e.getY())) {
            setCursor(new Cursor(Cursor.E_RESIZE_CURSOR));
        } else {
            setCursor(Cursor.getDefaultCursor());
        }
    }

    void this_mouseDragged(MouseEvent e) {
        int tmpX = e.getX();
        if (sizingLeft) {
            if (tmpX < minLeft) {
                tmpX = minLeft;
            } else if (tmpX > maxLeft) {
                tmpX = maxLeft;
            }
            handle.MoveLeft(tmpX - currX);
            sliderdragged = false;
            setZoom();
        } else if (sizingRight) {
            if (tmpX < maxRight) {
                tmpX = maxRight;
            } else if (tmpX > getWidth() - minRight) {
                tmpX = getWidth() - minRight;
            }
            handle.MoveRight(tmpX - currX);
            sliderdragged = false;
            setZoom();
        } else if (Moving) {
            if (tmpX < minLeft) {
                tmpX = minLeft;
            } else if (tmpX > getWidth() - minRight) {
                tmpX = getWidth() - minRight;
            }
            handle.MoveHandle(tmpX - currX);
            sliderdragged = true;
            setZoom();
        }
        currX = tmpX;
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
        int newPos = (int) beginPos - e.getWheelRotation();
        if ((newPos < 0) && e.getWheelRotation() > 0) {
            return;
        }
        if ((handle.getLeft() > (getWidth() - handle.getWidth() - MIN_HANDLE_WIDTH - 4)) && e.getWheelRotation() < 0) {
            return;
        }

        beginPos = newPos;
        handle.refreshHandle(getWidth());
        repaint();
    }


    public void paint(Graphics g) {
        super.paint(g);

        Graphics2D g2D = (Graphics2D) g;
        g2D.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        //shading = new GradientPaint(3*YvImgWd/4,0,Color.white,YvImgWd,0,axisColor, false); //  topImgHt
        //gYv.setPaint(shading);

        g2D.setColor(sliderBGColor);  //new Color(205,203,204)
        g2D.fill3DRect(0, 0, this.getWidth(), this.getHeight(), false);
        g2D.setColor(sliderFGColor);  //new Color(230,230,230)
        g2D.fill3DRect(handle.getLeft(), handle.getTop(),
                handle.getWidth() + MIN_HANDLE_WIDTH, handle.getHeight(), true);

        g2D.setColor(new Color(50, 50, 50));
        g2D.setFont(new TWFont("Dialog", 0, 9));
        g2D.drawString(caption, handle.getLeft() + (handle.getWidth() + MIN_HANDLE_WIDTH) / 2 - 16, handle.getHeight() / 2 + 4);

//    g2D.setColor(sliderFGColor);
//    Rectangle LR = handle.getLeftRect();
//    g2D.fill3DRect((int)LR.getX(), (int)LR.getY(), (int)LR.getWidth(), (int)LR.getHeight(),true);
//    LR = handle.getRightRect();
//    g2D.fill3DRect((int)LR.getX(), (int)LR.getY(), (int)LR.getWidth(), (int)LR.getHeight(),true);
        drawResizeHandles(g2D);
    }

    private void drawResizeHandles(Graphics g2D) {
        Rectangle LR = handle.getLeftRect();
        Rectangle RR = handle.getRightRect();
        int nPoints = 3;
        g2D.setColor(StockGraph.clLtShadow);
        int[] xPoints = {(int) LR.getX(), (int) LR.getX(), (int) LR.getX() + (int) LR.getWidth()};
        int[] yPoints = {(int) LR.getY() + (int) LR.getHeight(), (int) LR.getY(), (int) LR.getY()};
        g2D.drawPolyline(xPoints, yPoints, nPoints);
        xPoints = null;
        yPoints = null;
        int[] xPoints1 = {(int) RR.getX(), (int) RR.getX(), (int) RR.getX() + (int) RR.getWidth()};
        int[] yPoints1 = {(int) RR.getY() + (int) RR.getHeight(), (int) RR.getY(), (int) RR.getY()};
        g2D.drawPolyline(xPoints1, yPoints1, nPoints);
        xPoints1 = null;
        yPoints1 = null;

        g2D.setColor(StockGraph.clHighlight);
        int[] xPoints2 = {(int) LR.getX(), (int) LR.getX() + (int) LR.getWidth(), (int) LR.getX() + (int) LR.getWidth()};
        int[] yPoints2 = {(int) LR.getY() + (int) LR.getHeight(), (int) LR.getY() + (int) LR.getHeight(), (int) LR.getY()};
        g2D.drawPolyline(xPoints2, yPoints2, nPoints);
        xPoints2 = null;
        yPoints2 = null;
        int[] xPoints3 = {(int) RR.getX(), (int) RR.getX() + (int) RR.getWidth(), (int) RR.getX() + (int) RR.getWidth()};
        int[] yPoints3 = {(int) RR.getY() + (int) RR.getHeight(), (int) RR.getY() + (int) RR.getHeight(), (int) RR.getY()};
        g2D.drawPolyline(xPoints3, yPoints3, nPoints);
        xPoints3 = null;
        yPoints3 = null;

        int w = (int) LR.getHeight() / 2;
        int xL = (int) LR.getX() + (int) LR.getWidth() + 2;
        int xR = (int) RR.getX() - 2;
        int y = (int) LR.getY() + (int) LR.getHeight() / 2;
        int[] xPoints4 = {xL, xL + 2 * w, xL + 2 * w};
        int[] yPoints4 = {y, y - w, y + w};
        int[] xPoints5 = {xR, xR - 2 * w, xR - 2 * w};
        g2D.fillPolygon(xPoints4, yPoints4, nPoints);
        g2D.fillPolygon(xPoints5, yPoints4, nPoints);
        g2D.setColor(StockGraph.clLtHighlight);
        g2D.drawLine(xL, y, xL + 2 * w, y - w);
        g2D.drawLine(xR - 2 * w, y - w, xR - 2 * w, y + w);
        g2D.setColor(StockGraph.clHighlight);
        g2D.drawLine(xL, y, xL + 2 * w, y + w);
        g2D.drawLine(xR - 2 * w, y - w, xR, y);
        g2D.setColor(StockGraph.clDkShadow);
        g2D.drawLine(xL + 2 * w, y - w, xL + 2 * w, y + w);
        g2D.drawLine(xR - 2 * w, y + w, xR, y);
        xPoints4 = null;
        yPoints4 = null;
        xPoints5 = null;
    }

    public void setBounds(int x, int y, int width, int height) {
        handle.refreshHandle(width);
        super.setBounds(x, y, width, height);
    }

    public void setZoomAndPos(float z, float p) {
        zoom = z;
        beginPos = p;
        handle.refreshHandle(getWidth());
        repaint();
    }

    public void setPos(float p) {
        beginPos = p;
        handle.refreshHandle(getWidth());
        repaint();
    }

}
