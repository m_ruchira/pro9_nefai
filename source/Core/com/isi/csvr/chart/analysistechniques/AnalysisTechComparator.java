package com.isi.csvr.chart.analysistechniques;

import com.isi.csvr.shared.Language;

import java.util.Comparator;
import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Jun 18, 2009
 * Time: 11:20:17 AM
 * To change this template use File | Settings | File Templates.
 */
public class AnalysisTechComparator implements Comparator, Serializable {

    private int type = ChartConstants.TYPE_INDICATOR ;
    public AnalysisTechComparator(int type) {
        this.type = type;
    }

    public int compare(Object o1, Object o2) {
        AnalysTechBase a1 = (AnalysTechBase) o1;
        AnalysTechBase a2 = (AnalysTechBase) o2;

        if (type  == ChartConstants.TYPE_INDICATOR ){
            try{
            return (Language.getString( a1.getName()).compareTo(Language.getString( a2.getName())));
            }catch(Exception e){
                return (a1.getName().compareTo(a2.getName()));
            }
        }else {
            return (a1.getName().compareTo(a2.getName()));
        }
    }

}
