package com.isi.csvr.chart.analysistechniques;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Sep 9, 2008
 * Time: 10:31:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class AnaTechDefaultEntries {




public    ArrayList<AnalysTechBase>  patArr = new ArrayList <AnalysTechBase>();
        AnalysTechBase a = new AnalysTechBase("movAvg", "moving Average", "standard", "movingAverage()", false);
        AnalysTechBase b = new AnalysTechBase("BolBand", "BolingerBand", "standard", "Bolinger Band()", false);
        AnalysTechBase c = new AnalysTechBase("RegLine", "RegressionLine", "custom", "RegressionLines()", false);
        AnalysTechBase d = new AnalysTechBase("DEMA", "DEMA", "standard", "DEMA()", false);


    public AnaTechDefaultEntries() {



        patArr.add(a);
        patArr.add(b);
        patArr.add(c);
        patArr.add(d);


    }


    public ArrayList<AnalysTechBase> getPatArr() {
        return patArr;
    }

}
