package com.isi.csvr.chart.analysistechniques;

import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.shared.Language;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Sep 4, 2008
 * Time: 4:23:12 PM
 * To change this template use File | Settings | File Templates.
 */

public class AnalysisModel extends CommonTable
        implements TableModel, CommonTableInterface {
    private ArrayList<AnalysTechBase> analysTechStore;
    //constructor
    int Type = ChartConstants.TYPE_INDICATOR;

    public int getType() {
        return Type;
    }

    public AnalysisModel(ArrayList<AnalysTechBase> analysTechStore, int Type) {
        this.analysTechStore = analysTechStore;
        this.Type = Type;
    }

    //---------------------------------overide methods from common table ---------------------

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public int getRowCount() {

        //todo for now : should use a datastore
        return analysTechStore.size();
    }

    public Object getValueAt(int row, int col) {

        //todo for now
        AnalysTechBase annData = (AnalysTechBase) analysTechStore.get(row);

        switch (col) {
            case -1:     // SYMBOL
                return annData.getID();
            case -2:     // SYMBOL
                return annData.getSyntax();

            case 0:     // image
                return "" + 1;
            case 1:     // image
                return "" + 1;
            case 2:     // SYMBOL
//                return annData.getName();

                if (Type == ChartConstants.TYPE_INDICATOR) {
                    return Language.getString(annData.getName());
                } else {
                    return annData.getName();
                }


            case 3:     // SYMBOL
                return annData.isFaviourite(); //ann data favourites ischecked


            default:
                return "";
        }


    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public boolean isCellEditable(int row, int col) {
        switch (super.getViewSettings().getRendererID(col)) {
            case 'B':
                return true;
            default:
                return false;
        }
    }

    @Override
    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
        if (columnIndex == 3) {
            AnalysTechBase annData = (AnalysTechBase) analysTechStore.get(rowIndex);
            annData.setFaviourite(!annData.isFaviourite());
        }

    }//---------------------------------overide methods from common table ---------------------

    public void setSymbol(String symbol) {

    }

}