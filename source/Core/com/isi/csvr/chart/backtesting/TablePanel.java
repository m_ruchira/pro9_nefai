package com.isi.csvr.chart.backtesting;

import com.isi.csvr.chart.ToolTipDataRow;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 25, 2008
 * Time: 3:18:36 PM
 * To change this template use File | Settings | File Templates.
 */
public class TablePanel extends JPanel implements Themeable {

    private Color fontColor = Theme.getColor("BT_TABLE_FONT_COLOR");
    private Color gridColor = Theme.getColor("BT_TABLE_GRID_COLOR"); 

    private ArrayList<ToolTipDataRow> rows = null;

    public static int ROW_HEIGHT = 20;

    public TablePanel(ArrayList<ToolTipDataRow> rows) {
        super();
        this.rows = rows;
        Theme.registerComponent(this);
    }

    public void paint(Graphics g) {
        super.paint(g);

        g.setColor(gridColor);
        g.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);

        int x = (int) (this.getWidth() * .7);

        if (rows.size() > 0) {

            ROW_HEIGHT = this.getHeight() / (rows.size());
            int top = 0;

            int xx = 5, yy = ROW_HEIGHT - 5;
            for (int i = 0; i < rows.size(); i++) {
                g.setColor(gridColor);
                g.drawLine(0, top, this.getWidth() - 1, top);
                g.setColor(fontColor);
                g.setFont(new Font("Arial", Font.PLAIN, 11));
                if (rows.get(i) != null) {
                    g.drawString(rows.get(i).getDataRowName(), xx, yy);
                    g.drawString(rows.get(i).getDataRowValue(), xx + x, yy);
                    yy = yy + ROW_HEIGHT;
                    top = top + ROW_HEIGHT;
                }
            }
        }
        g.setColor(gridColor);
        g.drawLine(x, 0, x, this.getHeight());
    }


    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public static void main(String[] args) {

        ArrayList<ToolTipDataRow> rows = new ArrayList<ToolTipDataRow>();

        rows.add(new ToolTipDataRow("Buy and Hold Performance", "192.5 %"));
        rows.add(new ToolTipDataRow("Intrest Credited", "192.5 %"));
        rows.add(new ToolTipDataRow("Highest Prtfolio value", "192.5 %"));
        rows.add(new ToolTipDataRow("Final Equity", "192.5 %"));

        JFrame frame = new JFrame("test");
        frame.setSize(300, 120);

        frame.setLayout(new FlexGridLayout(new String[]{"10%", "80%", "10%"}, new String[]{"10%", "80%"}));

        frame.add(new JLabel(""));
        frame.add(new JLabel(""));
        frame.add(new JLabel(""));
        frame.add(new JLabel(""));
        frame.add(new TablePanel(rows));
        frame.setLocation(300, 80);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);


    }
}

