/*
package com.isi.csvr.chart.backtesting;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;

*/
/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 13, 2008
 * Time: 1:53:17 PM
 * To change this template use File | Settings | File Templates.
 */
/*

public class CustomPanel extends JPanel implements Themeable {

    //colors
    private Color borderColor;
    private Color innerColor;
    private Color titleColor;

    private int titleHeight = 10;
    private int arcRadius = 5;

    private float borderWidth = 1f;
    private String titleText = "Test Result Panel";
    private Font titleFont;

    private boolean isShowBorder = true;
    private boolean isShowTitle = true;

    final float[] dashArr = {0.01f};
    private BasicStroke stroke;

    private int W, H, X1, Y1, X2, Y2;

    public static final int TITLE_ALIGNMENT_LEFT = 0;
    public static final int TITLE_ALIGNMENT_RIGHT = 1;
    public static final int TITLE_ALIGNMENT_CENNTER = 2;
    public static final int TITLE_ALIGNMENT_NONE= 3;

    private int alignmnet = TITLE_ALIGNMENT_CENNTER;

    public static final int TITLE_BORDER_HEIGHT = 20;
    private final int ARC_WIDTH = 25;
    private final int ARC_HEIGHT = 25;

    public Color getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    public Color getInnerColor() {
        return innerColor;
    }

    public void setInnerColor(Color innerColor) {
        this.innerColor = innerColor;
    }

    public Color getTitleColor() {
        return titleColor;
    }

    public void setTitleColor(Color tiColor) {
        this.titleColor = tiColor;
    }

    public int getTitleHeight() {
        return titleHeight;
    }

    public void setTitleHeight(int titleHeight) {
        this.titleHeight = titleHeight;
    }

    public int getArcRadius() {
        return arcRadius;
    }

    public void setArcRadius(int arcRadius) {
        this.arcRadius = arcRadius;
    }

    public float getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
    }

    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public Font getTitleFont() {
        return titleFont;
    }

    public void setTitleFont(Font titleFont) {
        this.titleFont = titleFont;
    }

    public boolean isShowBorder() {
        return isShowBorder;
    }

    public void setShowBorder(boolean showBorder) {
        isShowBorder = showBorder;
    }

    public boolean isShowTitle() {
        return isShowTitle;
    }

    public void setShowTitle(boolean showTitle) {
        isShowTitle = showTitle;
    }

    //constructor
    public CustomPanel() {
        super();

        this.borderColor = new Color(208, 138, 136);
        //this.borderColor = Color.DARK_GRAY;
        //this.innerColor = new Color(229, 230, 250);
        this.innerColor = new Color(207, 209, 240);
        //this.innerColor = Color.GRAY;
        this.titleColor = Color.BLACK;
        //this.isShowBorder = true;
        //this.isShowTitle = true;
        this.arcRadius = 10;
        //this.borderWidth = 2;
        this.titleFont = new Font("Arial", Font.BOLD, 12);
    }

    public CustomPanel(String title, int align) {
        super();
        this.titleText = title;
        this.alignmnet = align;
        this.borderColor = new Color(208, 138, 136);
        //this.borderColor = Color.DARK_GRAY;
        this.innerColor = new Color(229, 230, 250);
        //this.innerColor = Color.GRAY;
        this.titleColor = Color.BLACK;
        //this.isShowBorder = true;
        //this.isShowTitle = true;
        this.arcRadius = 10;
        //this.borderWidth = 2;
        this.titleFont = new Font("Arial", Font.BOLD, 11);
    }


    public void paint(Graphics g) {
        super.paint(g);

        if (isShowBorder) {            
            g.setClip(new Rectangle(0, 0, this.getWidth(), TITLE_BORDER_HEIGHT));
            g.setColor(innerColor);
            g.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), ARC_WIDTH, ARC_HEIGHT);

            g.setClip(null);
            //g.fillRoundRect(0, 0, this.getWidth(), this.getHeight(), ARC_WIDTH, ARC_HEIGHT);

            //Graphics2D g2D = (Graphics2D) g;
            //g2D.setStroke(new BasicStroke(borderWidth));
            //g2D.setColor(borderColor);
            //g2D.drawRoundRect(0, 0, this.getWidth() - 1, this.getHeight(), ARC_WIDTH, ARC_HEIGHT);
        }

        //g.setClip(null);
        g.setColor(borderColor);
        //g.setClip(0, TITLE_BORDER_HEIGHT, this.getWidth(), this.getHeight() - TITLE_BORDER_HEIGHT);
        g.drawRoundRect(0, 0, this.getWidth() - 1, this.getHeight() - 1, ARC_WIDTH, ARC_HEIGHT);
        // g.setClip(null);
        //filling the title FILL color       

        //drawing the title
        if (isShowTitle) {
            g.drawLine(0, TITLE_BORDER_HEIGHT, this.getWidth() - 1, TITLE_BORDER_HEIGHT);
            g.setColor(titleColor);
            g.setFont(titleFont);
            if (alignmnet == TITLE_ALIGNMENT_LEFT) {
                g.drawString(titleText, ARC_WIDTH, TITLE_BORDER_HEIGHT - 5);
            } else if (alignmnet == TITLE_ALIGNMENT_CENNTER) {
                int length = g.getFontMetrics().stringWidth(titleText);
                int start = (this.getWidth() - length) / 2;
                g.drawString(titleText, start, TITLE_BORDER_HEIGHT - 5);
            }//else if(alignmnet)
        }
    }

    public void applyTheme() {
        this.borderColor = Theme.getColor("CUSTOM_PANEL_BORDER");
        this.innerColor = Theme.getColor("CUSTOM_PANEL_FILL_COLOR");
        this.titleColor = Theme.getColor("CUSTOM_PANEL_TITLE_COLOR");
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("test");
        frame.setSize(300, 250);

        frame.setLayout(new FlexGridLayout(new String[]{"10%", "80%", "10%"}, new String[]{"10%", "80%"}));

        frame.add(new JLabel(""));
        frame.add(new JLabel(""));
        frame.add(new JLabel(""));
        frame.add(new JLabel(""));
        frame.add(new CustomPanel());
        frame.setLocation(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}


*/


package com.isi.csvr.chart.backtesting;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 13, 2008
 * Time: 1:53:17 PM
 * To change this template use File | Settings | File Templates.
 */

public class CustomPanel extends JPanel implements Themeable {
                                                                                      
    //colors
    private Color borderColor = Theme.getColor("BT_PANEL_BORDER_COLOR");
    private Color fillColor = Theme.getColor("BT_PANEL_FILL_COLOR");
    private Color titleColor = Theme.getColor("BT_PANEL_TITLE_COLOR"); 

    private int titleHeight = 10;
    private int arcRadius = 10;

    private float borderWidth = 1f;
    private String titleText = "Test Result Panel";
    private Font titleFont = new Font("Arial", Font.BOLD, 11);

    private boolean isShowBorder = true;
    private boolean isShowTitle = true;

    final float[] dashArr = {0.01f};
    private BasicStroke stroke;

    private int W, H, X1, Y1, X2, Y2;

    public static final int TITLE_ALIGNMENT_LEFT = 0;
    public static final int TITLE_ALIGNMENT_RIGHT = 1;
    public static final int TITLE_ALIGNMENT_CENNTER = 2;
    public static final int TITLE_ALIGNMENT_NONE = 3;

    private int alignmnet = TITLE_ALIGNMENT_CENNTER;

    public static final int TITLE_BORDER_HEIGHT = 20;
    private final int ARC_WIDTH = 25;
    private final int ARC_HEIGHT = 25;

    public Color getBorderColor() {
        return borderColor;
    }

    public void setBorderColor(Color borderColor) {
        this.borderColor = borderColor;
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    public Color getTitleColor() {
        return titleColor;
    }

    public void setTitleColor(Color tiColor) {
        this.titleColor = tiColor;
    }

    public int getTitleHeight() {
        return titleHeight;
    }

    public void setTitleHeight(int titleHeight) {
        this.titleHeight = titleHeight;
    }

    public int getArcRadius() {
        return arcRadius;
    }

    public void setArcRadius(int arcRadius) {
        this.arcRadius = arcRadius;
    }

    public float getBorderWidth() {
        return borderWidth;
    }

    public void setBorderWidth(float borderWidth) {
        this.borderWidth = borderWidth;
    }

    public String getTitleText() {
        return titleText;
    }

    public void setTitleText(String titleText) {
        this.titleText = titleText;
    }

    public Font getTitleFont() {
        return titleFont;
    }

    public void setTitleFont(Font titleFont) {
        this.titleFont = titleFont;
    }

    public boolean isShowBorder() {
        return isShowBorder;
    }

    public void setShowBorder(boolean showBorder) {
        isShowBorder = showBorder;
    }

    public boolean isShowTitle() {
        return isShowTitle;
    }

    public void setShowTitle(boolean showTitle) {
        isShowTitle = showTitle;
    }

    //constructor
    public CustomPanel() {
        super();

    }

    public CustomPanel(String title, int align) {
        super();
        this.titleText = title;
        this.alignmnet = align;
        Theme.registerComponent(this);
    }


    public void paint(Graphics g) {
        super.paint(g);

        g.setColor(borderColor);
        g.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);
        g.drawLine(0, TITLE_BORDER_HEIGHT + 1, this.getWidth() - 1, TITLE_BORDER_HEIGHT + 1);

        //drawing the title
        g.setColor(fillColor);
        g.fillRect(1, 1, this.getWidth() - 2, TITLE_BORDER_HEIGHT);

        g.setColor(titleColor);
        g.setFont(titleFont);

        if (alignmnet == TITLE_ALIGNMENT_LEFT) {
            g.drawString(titleText, ARC_WIDTH, TITLE_BORDER_HEIGHT - 5);
        } else if (alignmnet == TITLE_ALIGNMENT_CENNTER) {
            int length = g.getFontMetrics().stringWidth(titleText);
            int start = (this.getWidth() - length) / 2;
            g.drawString(titleText, start, TITLE_BORDER_HEIGHT - 5);
        }
    }

    public void applyTheme() {
        this.borderColor = Theme.getColor("CUSTOM_PANEL_BORDER");
        this.fillColor = Theme.getColor("CUSTOM_PANEL_FILL_COLOR");
        this.titleColor = Theme.getColor("CUSTOM_PANEL_TITLE_COLOR");
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("test");
        frame.setSize(300, 250);

        frame.setLayout(new FlexGridLayout(new String[]{"10%", "80%", "10%"}, new String[]{"10%", "80%"}));

        frame.add(new JLabel(""));
        frame.add(new JLabel(""));
        frame.add(new JLabel(""));
        frame.add(new JLabel(""));
        frame.add(new CustomPanel());
        frame.setLocation(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}


