/*
package com.isi.csvr.chart.backtesting;

import javax.swing.*;
import java.awt.*;

*/
/**
 * Created by IntelliJ IDEA.
 * User: charithn
 * Date: Dec 5, 2008
 * Time: 11:45:26 AM
 * To change this template use File | Settings | File Templates.
 */
/*
public class VerticalTabButton extends JButton {

    Image tabRight = (new ImageIcon("tabRight.jpg")).getImage();
    Image tabLeft = (new ImageIcon("tabLeft.jpg")).getImage();
    Image tabCenter = (new ImageIcon("tabCenter.jpg")).getImage();

    int tabLeftWidth;
    int tabRightWidth;
    int tabCenterWidth;


    public VerticalTabButton(String text) {
        super(text);
        this.setForeground(Color.BLACK);
    }

    public void paint(Graphics g) {

        super.paint(g);

        int tabLeftWidth = tabLeft.getWidth(this);
        int tabRightWidth = tabRight.getWidth(this);
        int tabCenterWidth = tabCenter.getWidth(this);

        g.drawImage(tabRight, tabRight.getWidth(this), 0, 0, tabRight.getHeight(this), 0, 0, tabRight.getWidth(this), tabRight.getHeight(this), this);

        for (int i = tabRightWidth; i < (getWidth() - tabLeftWidth); i += tabCenterWidth) {
            g.drawImage(tabCenter, i, 0, this);
        }

        g.translate(getWidth() - tabLeftWidth, 0);

        g.drawImage(tabLeft, tabLeft.getWidth(this), 0, 0, tabLeft.getHeight(this), 0, 0, tabLeft.getWidth(this), tabLeft.getHeight(this), this);

        g.translate(-getWidth() + tabLeftWidth, 0);

        Graphics2D g2 = (Graphics2D)g;
        //int y1 = rect.y;
        g.translate(0, 0);
        g2.rotate( Math.toRadians(270) );
        String str = "charith";
        int len = g.getFontMetrics().stringWidth(str);
        g.drawString(str, -(this.getHeight() + len)/2 , this.getWidth()/2);
        //g2.rotate(Math.toRadians(270));
        g.translate(0, 0);

        //g.drawString("Charith", 5, 15);


    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("test");
        frame.setSize(100, 200);
        frame.setLayout(new BorderLayout(10, 10));
        frame.add(new JLabel(), BorderLayout.NORTH);
        frame.add(new JLabel(), BorderLayout.WEST);
        frame.add(new VerticalTabButton(""), BorderLayout.CENTER);
        frame.add(new JLabel(), BorderLayout.SOUTH);
        frame.add(new JLabel(), BorderLayout.EAST);
        frame.setLocation(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
*/


package com.isi.csvr.chart.backtesting;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Dec 5, 2008
 * Time: 11:45:26 AM
 * To change this template use File | Settings | File Templates.
 */
public class VerticalTabButton extends JButton implements Themeable {

    private boolean selected = false;

    Image tabRight;
    Image tabLeft;
    Image tabCenter;

    /* Image tabRight = (new ImageIcon("_tabrightselected.jpg")).getImage();
         Image      tabLeft = (new ImageIcon("_tableftselected.jpg")).getImage();
         Image    tabCenter = (new ImageIcon("_tabcenterselected.jpg")).getImage();
    */
    int tabLeftWidth;
    int tabRightWidth;
    int tabCenterWidth;
    int tabHeight;

    private String text = "";

    public VerticalTabButton(String text) {
        super();
        this.text = text;
        this.setForeground(Color.BLACK);
    }

    public VerticalTabButton(String text, boolean sel) {
        super();
        this.text = text;
        this.setForeground(Theme.getColor("VERTICAL_BUTTON_FOREGROUND"));
        this.selected = sel;

        if (selected) {
            tabRight = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "_tabrightselected.jpg")).getImage();
            tabLeft = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "_tableftselected.jpg")).getImage();
            tabCenter = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "_tabcenterselected.jpg")).getImage();
        } else {
            tabRight = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "_tabright.jpg")).getImage();
            tabLeft = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "_tableft.jpg")).getImage();
            tabCenter = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "_tabcenter.jpg")).getImage();

        }

        tabHeight = tabCenter.getHeight(this);

        Theme.registerComponent(this);
    }

    public void applyTheme() {

        /*
        tabRight = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "dt_tabrightselected.jpg")).getImage();
        tabLeft = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "dt_tableftselected.JPG")).getImage();
        tabCenter = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "dt_tabcenterselected.jpg")).getImage();
        */

        if (selected) {
            tabRight = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "_tabrightselected.jpg")).getImage();
            tabLeft = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "_tableftselected.jpg")).getImage();
            tabCenter = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "_tabcenterselected.jpg")).getImage();
        } else {
            tabRight = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "_tabright.jpg")).getImage();
            tabLeft = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "_tableft.jpg")).getImage();
            tabCenter = (new ImageIcon("images/theme" + Theme.getID() + "/tab/" + "_tabcenter.jpg")).getImage();
        }

        repaint();
    }

    public void paint(Graphics g) {

        super.paint(g);

        Graphics2D g2 = (Graphics2D) g;
        g2.rotate(Math.toRadians(-90));

        int tabLeftWidth = tabLeft.getWidth(this);
        int tabRightWidth = tabRight.getWidth(this);
        int tabCenterWidth = tabCenter.getWidth(this);

        g.drawImage(tabRight, -tabRight.getWidth(this), 0, this);

        for (int i = tabRightWidth; i <= (getHeight() - tabLeftWidth); i += tabCenterWidth) {
            g.drawImage(tabCenter, -i, 0, this);
        }

        g.drawImage(tabLeft, -this.getHeight(), 0, this);

        if (selected) {
            g.setColor(Theme.getColor("VERTICAL_BUTTON_SELECTED_FOREGROUND"));
        } else {
            g.setColor(Theme.getColor("VERTICAL_BUTTON_UNSELECTED_FOREGROUND"));
        }
        int len = g.getFontMetrics().stringWidth(text);
        g.drawString(text, -(this.getHeight() + len) / 2, this.getWidth() / 2 + 4);
        g2.rotate(Math.toRadians(90));

    }


    public Dimension getPreferredSize() {
        int width = (int) super.getPreferredSize().getWidth();
        return new Dimension(tabHeight, width + tabLeftWidth + tabRightWidth);
    }

    public Dimension getMinimumSize() {
        return getPreferredSize();
    }

    public Dimension getMaximumSize() {
        return getPreferredSize();
    }

    public Dimension getSize() {
        return getPreferredSize();
    }

    public static void main(String[] args) {

        JFrame frame = new JFrame("test");
        frame.setSize(300, 250);
        frame.setLayout(new FlexGridLayout(new String[]{"100", "22", "100"}, new String[]{"162"}, 10, 20));
        frame.add(new JLabel());
        frame.add(new VerticalTabButton("charith"));
        frame.add(new JLabel());
        frame.setLocation(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);

    }
}
