package com.isi.csvr.chart.backtesting;

import com.isi.csvr.shared.TWDateFormat;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 25, 2008
 * Time: 11:14:52 AM
 * To change this template use File | Settings | File Templates.
 */
public class EquityDetailRecord {

    private int barID;
    private long time; // this contains time in ticks
    private String date; // this contains time formatted as a date
    private double cash = -Double.MAX_VALUE;
    private double portfolio;
    private double interest;
    private double total;
    private double price;
    private int quantity;

    TWDateFormat formatter = new TWDateFormat("dd MMM yyyy");

    public EquityDetailRecord(int BarID, long Time, double Price) {
        this.barID = BarID;
        this.time = Time;
        this.price = Price;
    }

    public void setValues(int qty, double cashInHand) {
        this.quantity = qty;
        this.cash = cashInHand;
    }

    public EquityDetailRecord(int barID, long time, String date, double cash, double portfolio, double interest, double total) {
        this.barID = barID;
        this.time = time;
        this.date = date;
        this.cash = cash;
        this.portfolio = portfolio;
        this.interest = interest;
        this.total = total;
    }

    public void setValues(int qty, double cashInHand, double interest) {
        this.quantity = qty;
        this.cash = cashInHand;
        this.portfolio = this.quantity * this.price;
        this.interest = interest;
        this.total = this.cash + this.portfolio;
        this.date = formatter.format(time);
    }

    public int getBarID() {
        return barID;
    }

    public void setBarID(int barID) {
        this.barID = barID;
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public String getDate() {
        return date;
    }

    public void setDate(String date) {
        this.date = date;
    }

    public double getCash() {
        return cash;
    }

    public void setCash(double cash) {
        this.cash = cash;
    }

    public double getPortfolio() {
        return portfolio;
    }

    public void setPortfolio(double portfolio) {
        this.portfolio = portfolio;
    }

    public double getInterest() {
        return interest;
    }

    public void setInterest(double interest) {
        this.interest = interest;
    }

    public double getTotal() {
        return total;
    }

    public void setTotal(double total) {
        this.total = total;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public int getQuantity() {
        return quantity;
    }

    public void setQuantity(int quantity) {
        this.quantity = quantity;
    }
}


