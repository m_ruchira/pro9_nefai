package com.isi.csvr.chart.backtesting;

import com.isi.csvr.chart.ToolTipDataRow;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.util.ArrayList;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 25, 2008
 * Time: 10:04:02 PM
 * To change this template use File | Settings | File Templates.
 */
public class TablePanel2 extends JPanel implements Themeable {

    private ArrayList<ToolTipDataRow> rows = null;

    public static int ROW_HEIGHT = 20;
    public static int COLUMN_WDITH = 20;

    private int noOfRows = 2;
    private int noOfCols = 4;


    public TablePanel2(ArrayList<ToolTipDataRow> rows) {
        super();
        this.rows = rows;
        Theme.registerComponent(this);
    }

    public TablePanel2() {
        super();
    }

    public void paint(Graphics g) {
        super.paint(g);

        g.setColor(Theme.getColor("BT_TABLE_PANEL_GRID_COLOR"));
        g.setClip(0, 0, this.getWidth(), this.getHeight());
        g.drawRect(0, 0, this.getWidth() - 1, this.getHeight() - 1);


        ROW_HEIGHT = this.getHeight() / noOfRows;
        COLUMN_WDITH = this.getWidth() / noOfCols;

        int y = 0;
        int x = 0;

        int xx = 5, yy = ROW_HEIGHT - 5;

        for (int j = 0; j < noOfCols; j++) {

            g.setColor(Theme.getColor("BT_TABLE_PANEL_GRID_COLOR"));
            g.drawLine(x, 0, x, this.getHeight());
            ToolTipDataRow row = rows.get(j);

            int top = ROW_HEIGHT - 5;
            for (int i = 0; i < noOfRows; i++) {
                g.setColor(Theme.getColor("BT_TABLE_PANEL_GRID_COLOR"));
                g.drawLine(0, y, this.getWidth() - 1, y);
                y = y + ROW_HEIGHT;

                String str = "Charith";

                g.setColor(Theme.getColor("BT_TABLE_PANEL_FONT_COLOR"));
                if (i % 2 == 0) {
                    g.setFont(new Font("Arial", Font.BOLD, 11));
                    str = row.getDataRowName();
                } else {
                    g.setFont(new Font("Arial", Font.PLAIN, 11));
                    str = row.getDataRowValue();
                }

                int width = g.getFontMetrics().stringWidth(str);
                int tempx = (COLUMN_WDITH - width) / 2;
                g.drawString(str, x + tempx, top);
                top = top + ROW_HEIGHT;
            }
            x = x + COLUMN_WDITH;
        }

        g.setClip(null);
    }

    public void applyTheme() {
        //To change body of implemented methods use File | Settings | File Templates.
    }

    public static void main(String[] args) {

        ArrayList<ToolTipDataRow> rows = new ArrayList<ToolTipDataRow>();

        rows.add(new ToolTipDataRow("Buy and Hold Performance", "324234"));
        rows.add(new ToolTipDataRow("Intrest Credited", "98794"));
        rows.add(new ToolTipDataRow("Highest Prtfolio value", "545453"));
        rows.add(new ToolTipDataRow("Final Equity", "767628"));


        JFrame frame = new JFrame("test");
        frame.setSize(800, 100);

        frame.setLayout(new FlexGridLayout(new String[]{"5%", "90%", "5%"}, new String[]{"10%", "80%"}));

        frame.add(new JLabel(""));
        frame.add(new JLabel(""));
        frame.add(new JLabel(""));
        frame.add(new JLabel(""));
        frame.add(new TablePanel2(rows));
        frame.setLocation(300, 80);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);


    }
}

