package com.isi.csvr.chart.backtesting;

import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;

import javax.swing.table.TableModel;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 23, 2008
 * Time: 8:53:43 AM
 * To change this template use File | Settings | File Templates.
 */
public class BackTestingSymbolTableModel extends CommonTable implements TableModel, CommonTableInterface {

    private ArrayList<Key> keyStore;

    public BackTestingSymbolTableModel(ArrayList<Key> store) {
        this.keyStore = store;
    }

    public int getRowCount() {
        return keyStore.size();
    }

    public int getColumnCount() {
        return super.getViewSettings().getColumnHeadings().length;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {

        Key key = keyStore.get(rowIndex);

        switch (columnIndex) {

            case -1:
                return key;
            case 0:
                return key.getSymbol();

            case 1:
                return key.getExchange();

            case 2:
                return key.getCompany();

            default:
                return "";
        }
    }

    public String getColumnName(int iCol) {
        return super.getViewSettings().getColumnHeadings()[iCol];
    }

    public Class getColumnClass(int iCol) {
        return getValueAt(0, iCol).getClass();
    }

    public void setSymbol(String symbol) {

    }
}
