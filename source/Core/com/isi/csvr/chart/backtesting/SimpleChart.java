package com.isi.csvr.chart.backtesting;

import com.isi.csvr.chart.GraphicsPath;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.geom.Point2D;
import java.util.ArrayList;


/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Nov 13, 2008
 * Time: 1:53:30 PM
 * To change this template use File | Settings | File Templates.
 */

public class SimpleChart extends JPanel implements Themeable {

    //properties
    private int yAxisWidth = 80;
    private int xAxisHeight = 25;
    private Color gridColor = Theme.getColor("SIMPLE_CHART_GRID_COLOR");
    private Color chartBackColor = Theme.getColor("SIMPLE_CHART_BACK_COLOR");
    private Color chartAxisFontColor = Theme.getColor("SIMPLE_CHART_AXIS_FONT_COLOR");
    private boolean showLegend = true;

    private GraphicsPath gp = new GraphicsPath();

    //variables
    private float width = 450;
    private float height = 100;
    private float legendHeight = 30;

    private double minY = Float.MAX_VALUE;
    private double maxY = -Float.MIN_VALUE;

    private double minX = Float.MAX_VALUE;
    private double maxX = -Float.MIN_VALUE;

    private double bestXPitch = 10;
    private double bestYPitch = 100;
    private double xStart = 1;
    private double yStart = 1;
    private float penWidth = 1.2f;

    //data structures
    private double xFactor = 1;
    private double yFactor = 1;

    private ArrayList<ChartSeries> chartSeriesCollection = new ArrayList<ChartSeries>();
    private boolean isFilled = true;


    public SimpleChart() {

    }

    public void addSeries(ChartSeries series) {
        chartSeriesCollection.add(series);
        calculatePixelValues();
        //applyChartColors();
    }

    private void calculatePixelValues() {

        Graphics g = super.getGraphics();
        if (showLegend) {
            //legendHeight = g.getFontMetrics(new Font("Arial", Font.PLAIN, 12)).getHeight() + 6;
        } else {
            legendHeight = 0;
        }

        width = Math.max(this.getWidth() - 2 * yAxisWidth, 400);
        height = Math.max(this.getHeight() - xAxisHeight - legendHeight, 10);

        if (chartSeriesCollection.size() > 0) {
            for (int i = 0; i < chartSeriesCollection.size(); i++) {
                ChartSeries s = chartSeriesCollection.get(i);
                for (int m = 0; m < s.count; m++) {

                    minY = Math.min(minY, s.YData[m]);
                    maxY = Math.max(maxY, s.YData[m]);

                    minX = Math.min(minX, s.XData[m]);
                    maxX = Math.max(maxX, s.XData[m]);
                }
            }

            if (minX == maxX) {
                minX = minX - 1;
                maxX = maxX + 1;
            }
            if (minY == maxY) {
                minY = minY - 10;
                maxY = maxY + 10;
            }
        } else {
            minX = 0;
            maxX = 100;
            minY = 0;
            maxY = 100;
        }

        xFactor = width / (maxX - minX);
        yFactor = height / (maxY - minY);

        for (int j = 0; j < chartSeriesCollection.size(); j++) {
            ChartSeries series = chartSeriesCollection.get(j);
            if (series.count > 0) {
                Point2D.Float[] points = new Point2D.Float[series.count];
                for (int k = 0; k < points.length; k++) {
                    float x = (float) ((series.XData[k] - minX) * xFactor) + yAxisWidth;
                    float y = (height + legendHeight) - (float) ((series.YData[k] - minY) * yFactor);
                    points[k] = new Point2D.Float(x, y);
                }
                series.points = points;
            } else {
                series.points = null;
            }
        }

        bestXPitch = getBestYInterval((maxX - minX) * 2, width, g);
        bestYPitch = getBestYInterval(maxY - minY, height, g);
        xStart = getStart(minX, bestXPitch);
        yStart = getStart(minY, bestYPitch);


    }

    public static float[] PriceVolPitch = {1f, 2f, 4f, 5f, 8f};

    // calculates the best pitch for Y-axis
    private double getBestYInterval(double val, float pixLen, Graphics g) {
        double HALF_INCH = 72 / 3d;

        if (chartSeriesCollection.size() < 1) return 40;

        val = val / pixLen * HALF_INCH;

        float pitch = SimpleChart.PriceVolPitch[0];
        for (long j = 1; j < Long.MAX_VALUE; j *= 10) {
            float Multiplier = ((float) j / 1000000000);
            for (int i = 0; i < SimpleChart.PriceVolPitch.length; i++) {
                if (val >= (double) Multiplier * SimpleChart.PriceVolPitch[i]) {
                    pitch = (Multiplier * SimpleChart.PriceVolPitch[i]);
                } else {
                    return pitch;
                }
            }
            if (j > Long.MAX_VALUE / 10f) {
                return pitch;
            }
        }
        return pitch;
    }

    private double getStart(double min, double bestPitch) {
        long i = 0;
        while (i < Double.MAX_VALUE / bestPitch) {
            if (i * bestPitch >= min) return i * bestPitch;
            i++;
        }
        return i * bestPitch;
    }

    private float getPixelForXValue(double value) {
        // return 0;
        return (float) ((value - minX) * xFactor) + yAxisWidth;
    }

    private float getPixelForYValue(double value) {
        return height + legendHeight - (float) ((value - minY) * yFactor);
    }

    private void drawLegend(Graphics g) {

        float currX = yAxisWidth;
        float y = legendHeight - 20;
        for (int s = 0; s < chartSeriesCollection.size(); s++) {
            ChartSeries series = chartSeriesCollection.get(s);
            g.setColor(series.color);
            g.fillRect((int) currX, (int) y, 10, 10);
            g.drawRect((int) currX, (int) y, 10, 10);
            currX += 15;
            g.setColor(this.getForeground());
            g.drawString(series.name, (int) currX, (int) (y + 10));
            currX += g.getFontMetrics().stringWidth(String.valueOf(series.name)) + 10;
        }
    }

    public void drawXAxisLabels(Graphics g) {

        Graphics2D g2D = (Graphics2D) g;
        float labelLength = 0;
        double currentVal = xStart;
        float y = legendHeight + height + 15;

        //curr
        while (currentVal <= maxX) {
            float x = getPixelForXValue(currentVal);
            g.setColor(gridColor);
            g.drawLine((int) x, (int) legendHeight, (int) x, (int) (legendHeight + height));
            g2D.setStroke(new BasicStroke(.8f));
            g.setColor(chartAxisFontColor);
            g.drawLine((int) x, (int) (legendHeight + height), (int) x, (int) (legendHeight + height + 3));
            labelLength = g.getFontMetrics().stringWidth(String.valueOf(currentVal));
            //g.setColor(new Color(208, 138, 136));
            g.setColor(chartAxisFontColor);
            g.drawString(String.valueOf((int) currentVal), (int) (x - (labelLength) / 3), (int) y);
            currentVal = currentVal + bestXPitch;
        }
    }

    private TWDecimalFormat priceFormatter = new TWDecimalFormat("###,###,###");

    private void drawYAxisLabels(Graphics g) {
        float labelLength = 0;
        g.setColor(this.getForeground());
        double currVal = yStart;
        float x = yAxisWidth - 5;
        float yAdj = -1;

        Graphics2D g2D = (Graphics2D) g;

        while (currVal <= maxY) {
            float y = getPixelForYValue(currVal);
            g.setColor(gridColor);
            g2D.setStroke(new BasicStroke(.4f));
            g.drawLine((int) yAxisWidth, (int) y, (int) (yAxisWidth + width), (int) y);
            g.setColor(chartAxisFontColor);
            g.drawLine(yAxisWidth - 3, (int) y, (int) yAxisWidth, (int) y);
            labelLength = g.getFontMetrics().stringWidth(String.valueOf(priceFormatter.format((int) currVal)));
            g.setColor(Color.BLACK);
            g.drawString(priceFormatter.format((int) currVal), (int) (x - labelLength), (int) (y + yAdj));
            currVal += bestYPitch;
        }
    }

    public void applyTheme() {
        chartAxisFontColor = Theme.getColor("SIMPLE_CHART_AXIS_FONT_COLOR");
        gridColor = Theme.getColor("SIMPLE_CHART_GRID_COLOR");
        chartBackColor = Theme.getColor("SIMPLE_CHART_BACK_COLOR");
    }

    public void paint(Graphics g) {

        calculatePixelValues();
        //System.out.println("*********** painting *************");

        g.setColor(this.getBackground());
        g.fillRect(0, 0, this.getWidth(), this.getHeight());

        g.setColor(chartBackColor);
        g.fillRect(yAxisWidth, (int) legendHeight, (int) (width), (int) height);

        drawLegend(g);

        for (int s = 0; s < chartSeriesCollection.size(); s++) {
            ChartSeries series = chartSeriesCollection.get(s);
            if (series.count > 0) {
                //area painting

                g.setColor(series.color);
                ((Graphics2D) g).setStroke(new BasicStroke(penWidth));
                gp.drawPath(g, series.points);

                if (isFilled) {
                    gp.fillPath(g, series.points, (int) (height + legendHeight), (int) width);
                }
            }
        }

        drawXAxisLabels(g);
        drawYAxisLabels(g);

        g.setColor(chartAxisFontColor);
        Graphics2D g2D = (Graphics2D) g;
        g2D.setStroke(new BasicStroke());

        //drawing the chart outline of the rectangle
        g.drawRect(yAxisWidth, (int) legendHeight, (int) (width), (int) height);

    }


    public static void main(String[] args) {

        int COUNT = 250;
        double[] x = new double[COUNT + 1];
        double[] y = new double[COUNT + 1];

        double[] x2 = new double[COUNT + 1];
        double[] y2 = new double[COUNT + 1];

        double[] x3 = new double[COUNT + 1];
        double[] y3 = new double[COUNT + 1];


        for (int i = 0; i < COUNT; i++) {

            x[i] = i;
            y[i] = (int) (Math.random() * 100000000) % 1000 + 4000;
        }

        for (int i = 0; i < COUNT; i++) {

            x2[i] = i;
            y2[i] = (int) (Math.random() * 100000000) % 1000 + 3500;
        }


        for (int i = 0; i < COUNT; i++) {

            x3[i] = i;
            y3[i] = (int) (Math.random() * 100000000) % 1000 + 3000;
        }

        ChartSeries series1 = new ChartSeries("Portfolio", COUNT, new Color(102, 102, 0), x, y);
        ChartSeries series2 = new ChartSeries("Cash", COUNT, Color.DARK_GRAY, x2, y2);
        ChartSeries series3 = new ChartSeries("Equity", COUNT, new Color(0, 100, 100), x3, y3);

        SimpleChart chart = new SimpleChart();
        chart.addSeries(series1);
        //chart.addSeries(series2);
        chart.addSeries(series3);

        JFrame frame = new JFrame("test");
        frame.setSize(700, 400);
        //frame.setSize(1000, 600);
        frame.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"100%"}));

        //frame.add(new JLabel(""));
        frame.add(chart);
        frame.setLocation(300, 200);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setVisible(true);


    }
}

