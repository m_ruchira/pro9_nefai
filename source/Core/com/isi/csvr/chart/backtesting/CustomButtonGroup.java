package com.isi.csvr.chart.backtesting;

import com.isi.csvr.theme.Theme;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Dec 2, 2008
 * Time: 4:34:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomButtonGroup {

    private ArrayList<CustomButton> buttons;

    public CustomButtonGroup(ArrayList<CustomButton> buttons) {
        this.buttons = buttons;
          for (CustomButton button : buttons) {
             button.setBackground(Theme.getColor("BT_NOT_SELECTED_LABEL_BACKGROUND"));
        }
    }

    public void add(CustomButton btn) {
        buttons.add(btn);
        addActions();
    }

    public void addActions() {

        for (CustomButton button1 : buttons) {

            final CustomButton button = button1;
            button.addActionListener(new ActionListener() {

                public void actionPerformed(ActionEvent e) {
                    applyColors(button);
                }
            });
        }
    }

    public void applyColors(JButton btn) {

        for (CustomButton button : buttons) {
            if (button == btn) {   //selected buttons
                JButton b = new JButton();
                button.setForeground(Theme.getColor("BT_SELECTED_LABEL_FOREGROUND"));
                button.setBackground(b.getBackground());

            } else {  //other buttons
                button.setBackground(Theme.getColor("BT_NOT_SELECTED_LABEL_BACKGROUND"));
                button.setForeground(Theme.getColor("BT_NOT_SELECTED_LABEL_FOREGROUND"));
            }
        }
    }
}
