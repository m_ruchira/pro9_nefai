package com.isi.csvr.chart;

import java.awt.event.MouseEvent;

/**
 * <p>Title: TW International</p>
 * <p>Description: Data structuer for StockGraph </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public interface GraphMouseListener {
	/**
	 * Invoked when a mouse button has been pressed on a component.
	 */
	public void graphMousePressed(MouseEvent e);

	/**
	 * Invoked when a mouse button has been released on a component.
	 */
	public void graphMouseReleased(MouseEvent e);

	/**
	 * Invoked when a mouse button has been moved on a component.
	 */
	public void graphMouseMoved(MouseEvent e);

	/**
	 * Invoked when a mouse button has been dragged on a component.
	 */
	public void graphMouseDragged(MouseEvent e);

	/**
     * Invoked when the mouse cursor exits the component
     * @param e
     */
    public void graphMouseExited(MouseEvent e);

    /**
	 * Invoked when a volume Panel has been Deleted.
	 */
	public void volumePanelDeleted();

}