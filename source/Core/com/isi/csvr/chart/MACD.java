package com.isi.csvr.chart;

import com.isi.csvr.shared.Language;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorDefaultProperty;
import com.isi.csvr.chart.indicatorproperties.IndicatorMACDProperty;

import java.awt.*;
import java.io.Serializable;
import java.util.ArrayList;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public class MACD extends ChartProperties implements Indicator, Serializable {

    /**
     * use as a genaral property for parameterize these two instead of constants **********
     */
    private int MACD_TimePeriods_High = 26;
    private int MACD_TimePeriods_Low = 12;

    //Fields
    private int signalTimePeriods;
    private byte signalStyle;
    private Color signalColor;
    private ChartProperties innerSource;
    //private int innerSourceIndex;
    private MovingAverage signalLine;
    private MACDHistogram histogram;
    private String tableColumnHeading;

    private static final long serialVersionUID = UID_MACD;

    public MACD(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("MACD") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_MACD");
        signalTimePeriods = 9;
        signalStyle = GraphDataManager.STYLE_DASH;
        signalColor = Color.DARK_GRAY;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorMACDProperty idp = (IndicatorMACDProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setOHLCPriority(idp.getOHLCPriority());

            this.setSignalTimePeriods(idp.getSignalTimePeriods());
            this.setSignalColor(idp.getSignalColor());
            this.setSignalStyle(idp.getSignalStyle());
        }
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.signalTimePeriods = 9;
        this.signalStyle = GraphDataManager.STYLE_DASH;
        this.signalColor = Color.DARK_GRAY;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.signalTimePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalTimePeriods"));
        this.signalStyle = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalStyle"));
        String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalColor").split(",");
        this.signalColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "SignalTimePeriods", Integer.toString(this.signalTimePeriods));
        TemplateFactory.saveProperty(chart, document, "SignalStyle", Byte.toString(this.signalStyle));
        String strColor = this.signalColor.getRed() + "," + this.signalColor.getGreen() + "," + this.signalColor.getBlue();
        TemplateFactory.saveProperty(chart, document, "SignalColor", strColor);
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof MACD) {
            MACD macd = (MACD) cp;
            this.signalTimePeriods = macd.signalTimePeriods;
            this.signalStyle = macd.signalStyle;
            this.signalColor = macd.signalColor;
            this.innerSource = macd.innerSource;
            this.MACD_TimePeriods_High = macd.MACD_TimePeriods_High;
            this.MACD_TimePeriods_Low = macd.MACD_TimePeriods_Low;
            if ((this.signalLine != null) && (macd.signalLine != null)) {
                this.signalLine.assignValuesFrom(macd.signalLine);
            }
            if ((this.histogram != null) && (macd.histogram != null)) {
                this.histogram.assignValuesFrom(macd.histogram);
            }
        }
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    // this calculation includes 2 intermediate steps: EMA26 and EMA12
    //check for conditions of low & high interelation ..usually should be HIgh > low  
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        long entryTime = System.currentTimeMillis();
        int loopBegin = MACD_TimePeriods_High - 1;
        if (loopBegin >= al.size()) return;
        ChartRecord cr;

        //setting step size 5
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(5);
        }
        // steps involved
        byte stepEMA26 = ChartRecord.STEP_1;
        byte stepEMA12 = ChartRecord.STEP_2;

        MovingAverage.getMovingAverage(al, 0, MACD_TimePeriods_High, MovingAverage.METHOD_EXPONENTIAL, getOHLCPriority(), stepEMA26);
        MovingAverage.getMovingAverage(al, 0, MACD_TimePeriods_Low, MovingAverage.METHOD_EXPONENTIAL, getOHLCPriority(), stepEMA12);

        for (int i = loopBegin - 1; i >= 0; i--) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(cr.getStepValue(stepEMA12) - cr.getStepValue(stepEMA26));
            }
        }

        // System.out.println("**** MACD Calc time " + (entryTime - System.currentTimeMillis()));
    }
    //############################################

/*
    protected float getExpMovAvg(ArrayList al, float preVal, int bIndex, float ratio){
        Object tmpObj = al.get(bIndex);
		int[] fa = (int[])tmpObj;
		float val = fa[1]/10000f;
		return val*ratio + (1-ratio)*preVal;
	}
*/
    //////////////////////////////////////////////

//    // This method has no usages 
//    public static String[] getMethodArray(){
//        String[] methodArr = {Language.getString("METHOD_SIMPLE"),
//                Language.getString("METHOD_WEIGHTED"),
//                Language.getString("METHOD_EXPONENTIAL")};
//		return methodArr;
//	}

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        //parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + signalTimePeriods;
        return Language.getString("MACD") + " " + parent;
    }

    //signalTimePeriods
    public int getSignalTimePeriods() {
        return signalTimePeriods;
    }

    public void setSignalTimePeriods(int tp) {
        signalTimePeriods = tp;
        if (signalLine != null)
            signalLine.setTimePeriods(tp);
    }

    //signalStyle;
    public byte getSignalStyle() {
        return signalStyle;
    }

    public void setSignalStyle(byte ss) {
        signalStyle = ss;
        if (signalLine != null)
            signalLine.setPenStyle(ss);
    }

    public void setSignalStyle(Byte ss) {
        setSignalStyle(ss.byteValue());
    }

    //signalColor;
    public Color getSignalColor() {
        return signalColor;
    }

    public void setSignalColor(Color c) {
        signalColor = c;
        if (signalLine != null)
            signalLine.setColor(c);
    }

    //signalLine
    public MovingAverage getSignalLine() {
        return signalLine;
    }

    public void setSignalLine(MovingAverage ma) {
        signalLine = ma;
    }

    public int getMACD_TimePeriods_Low() {
        return MACD_TimePeriods_Low;
    }

    public void setMACD_TimePeriods_Low(int MACD_TimePeriods_Low) {
        this.MACD_TimePeriods_Low = MACD_TimePeriods_Low;
    }

    public int getMACD_TimePeriods_High() {
        return MACD_TimePeriods_High;
    }

    public void setMACD_TimePeriods_High(int MACD_TimePeriods_High) {
        this.MACD_TimePeriods_High = MACD_TimePeriods_High;
    }

    //record size - override
//	public void setRecordSize(int size){
//		super.setRecordSize(size);
//		if (signalLine!=null){
//			signalLine.setRecordSize(size);
//		}
//	}

    public static void calculateIndicator(GraphDataManager GDM, ArrayList al, int bIndex,
                                          byte stepEMA12, byte stepEMA26, byte srcIndex, byte destIndex) {

        ChartRecord cr;
        long entryTime = System.currentTimeMillis();

        /////////// this is done to avoid null points ////////////
        int tmpIndex = al.size();
        for (int i = bIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                tmpIndex = i;
                break;
            }
        }
        bIndex = tmpIndex;
        //////////////////////////////////////////////////////////

        //TODO:
        int MACD_TimePeriods_High = 26;
        int MACD_TimePeriods_Low = 9;
        int loopBegin = bIndex + MACD_TimePeriods_High - 1;

        if (loopBegin >= al.size()) { //not enough data
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        MovingAverage.getMovingAverage(al, bIndex, MACD_TimePeriods_Low, MovingAverage.METHOD_EXPONENTIAL, srcIndex, stepEMA12);
        MovingAverage.getMovingAverage(al, bIndex, MACD_TimePeriods_High, MovingAverage.METHOD_EXPONENTIAL, srcIndex, stepEMA26);

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            double val = cr.getValue(stepEMA12) - cr.getValue(stepEMA26);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                cr.setValue(destIndex, val);
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }
        //System.out.println("**** Inner MACD Calc time " + (entryTime - System.currentTimeMillis()));

    }

    public String getShortName() {
        return Language.getString("MACD");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }


    public MACDHistogram getHistogram() {
        return histogram;
    }

    public void setHistogram(MACDHistogram histogram) {
        this.histogram = histogram;
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 2;
    }


}


/** Enable Macd parameters***********************
 * timeperiodshigh low use as a property  - assign values from
 * do have to check any condition to check ... timeperiodsHight > timeperiods_low ?
 *
 * */