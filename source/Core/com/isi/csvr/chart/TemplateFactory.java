package com.isi.csvr.chart;

import javax.xml.parsers.*;
import javax.xml.xpath.*;
import javax.xml.transform.*;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;

import org.w3c.dom.*;

import java.io.*;
import java.util.Hashtable;
import java.util.ArrayList;
import java.util.Enumeration;
import java.awt.*;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.chart.customindicators.IndicatorMenuObject;
import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.ChartInterface;

//import com.sun.org.apache.xml.internal.serialize.XMLSerializer;

//import com.isi.csvr.shared.Language;

/**
 * User: Udaka
 * Date: Mar 1, 2006
 * Time: 1:39:36 PM
 */
public class TemplateFactory {

    public static final int SHORT_TERM = 0;
    public static final int MID_TERM = 1;
    public static final int LONG_TERM = 2;

    public static final String DEFAULT_TEMPLATE_FILE = Settings.CHART_DATA_PATH + "/default.mct";
    public static final String INTRADAY_DEFAULT_TEMPLATE_FILE = Settings.CHART_DATA_PATH + "/Intraday_default.mct";
    public static final String HISTORY_DEFAULT_TEMPLATE_FILE = Settings.CHART_DATA_PATH + "/history_default.mct";
    public static final String TEMP_DEFAULT_TEMPLATE_FILE = Settings.CHART_DATA_PATH + "/tmpDefault.mct";
    public static final String CASHFLOW_TEMPLATE_FILE = Settings.CHART_DATA_PATH + "/cfd.mct";

    public static String[] TemplatePaths = {Settings.CHART_DATA_PATH + "/templates/tmpltS.dll", Settings.CHART_DATA_PATH + "/templates/tmpltM.dll", Settings.CHART_DATA_PATH + "/templates/tmpltL.dll"};
    public static String[] TemplateNames = {Language.getString("TEMPLATE_SHORT_TERM"),
            Language.getString("TEMPLATE_MID_TERM"),
            Language.getString("TEMPLATE_LONG_TERM")
    };

    public static final String HIDDEN_PANEL_TEMP_ID = "10000";

    public static boolean LoadTemplate(GraphFrame originalGraphFrame, File tmpltFile, String symbl, boolean detached) {
        ChartFrame cFrame = ChartFrame.getSharedInstance();
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(tmpltFile);

            XPath xpath = XPathFactory.newInstance().newXPath();

            String expression = "Template/ChartFrame";
            boolean isShowSnapBall = false;
            boolean isShowCrossHair = false;
            Node chartFrame = null;
            try {
                chartFrame = (Node) xpath.evaluate(expression, document, XPathConstants.NODE);
            } catch (Exception ex) {
                // do nothing
            } finally {
                if (chartFrame != null) {
                    try {
            NamedNodeMap chartFrameAttributes = chartFrame.getAttributes();
                        isShowSnapBall = Boolean.parseBoolean(
                    chartFrameAttributes.getNamedItem("isShowSnapBall").getNodeValue());
                        isShowCrossHair = Boolean.parseBoolean(
                    chartFrameAttributes.getNamedItem("isShowCrossHairs").getNodeValue());
                    } catch (NullPointerException ex) { // ignore - backward compatibility}
                    }
                }
            }


            ChartFrame.getSharedInstance().setShowSnapBall(isShowSnapBall);
            ChartFrame.getSharedInstance().setShowCrossHairs(isShowCrossHair);


            expression = "Template/GraphWindow";
            // First, obtain the element as a node.

            NodeList graphWindows = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);

            //System.out.println("NodeList size: " + graphWindows.getLength());

            if ((graphWindows != null) && (graphWindows.getLength() > 0)) {
                for (int i = 0; i < graphWindows.getLength(); i++) {
                    Node graphWindow = graphWindows.item(i);
                    NamedNodeMap attributes = graphWindow.getAttributes();
                    boolean isCurrentMode = Boolean.parseBoolean(attributes.getNamedItem("Mode").getNodeValue());
                    int period = Integer.parseInt(attributes.getNamedItem("Period").getNodeValue());
                    long interval = Long.parseLong(attributes.getNamedItem("Interval").getNodeValue());
                    String[] saBounds = attributes.getNamedItem("Bounds").getNodeValue().split(",");
                    //System.out.println("mode, period, interval = " + isCurrentMode + ", " + period + ", " + interval);

                    GraphFrame gf;
                    if (i == 0) {
                        gf = originalGraphFrame;
                        gf.graph.clearAll();
                    } else {
                        gf = cFrame.addNewGraph(false, true);
                    }
                    gf.setDetached(detached);
                    StockGraph graph = gf.graph;
                    gf.CurrentMode = isCurrentMode;
                    graph.setCurrentMode(isCurrentMode);
                    graph.setPeriodFlag(period);
                    graph.setIntervalFlag(interval);

                    boolean isShowCurrentPrice = Boolean.parseBoolean(getAttribute(attributes, "isShowCurrentPriceLine"));
                    boolean isShowBidAskTags = Boolean.parseBoolean(getAttribute(attributes, "isShowBidAskTags"));
                    boolean isShowOrderLines = Boolean.parseBoolean(getAttribute(attributes, "isShowOrderLines"));
                    boolean isShowMinMaxPrices = Boolean.parseBoolean(getAttribute(attributes, "isShowMinMaxLines"));
                    boolean bIsShowVolume = Boolean.parseBoolean(getAttribute(attributes, "isShowVolumeInProchart"));
                    boolean bIsShowTurnOver = Boolean.parseBoolean(getAttribute(attributes, "isShowTurnOverInProChart"));
                    boolean bIsShowAnnouncecment = Boolean.parseBoolean(getAttribute(attributes, "isShowAnnouncements"));
                    boolean bIsShowSplits = Boolean.parseBoolean(getAttribute(attributes, "isShowSplits"));
                    boolean bIsShowVolByPrice = Boolean.parseBoolean(getAttribute(attributes, "isShowVolumeByPrice"));
                    boolean isShowSupportResistanceLines = Boolean.parseBoolean(getAttribute(attributes, "isShowSupportResistanceLines"));
                    byte volByPrice = Byte.parseByte(getAttribute(attributes, "showVolumeByPrice"));
                    byte crossHairType = Byte.parseByte(getAttribute(attributes, "crossHairType"));
                    boolean isshowPreviousCloseLine = Boolean.parseBoolean(getAttribute(attributes, "isShowPreviousCloseLine"));


                        graph.GDMgr.setShowCurrentPriceLine(isShowCurrentPrice);
                        graph.GDMgr.setShowBidAskTags(isShowBidAskTags);
                        graph.GDMgr.setShowOrderLines(isShowOrderLines);
                        graph.GDMgr.setShowMinMaxPriceLines(isShowMinMaxPrices);
                        graph.setVolumeGraphflag(bIsShowVolume);
                        graph.setTurnOverGraphFlag(bIsShowTurnOver);
                    graph.GDMgr.setShowAnnouncements(bIsShowAnnouncecment);
                        graph.GDMgr.setShowSplits(bIsShowSplits);
                    graph.GDMgr.setShowVolumeByPrice(bIsShowVolByPrice);
                        graph.GDMgr.setShowResistanceLine(isShowSupportResistanceLines);
                    graph.GDMgr.setShowPreviousCloseLine(isshowPreviousCloseLine);
                        if (volByPrice > -1) graph.GDMgr.setVolumeByPriceType(volByPrice);
                        if (crossHairType > -1) gf.graph.GDMgr.setCrossHairType(crossHairType);


                    if (graph.getPeriod() == StockGraph.CUSTOM_PERIODS) {
                        int customPeriod = Integer.parseInt(getAttribute(attributes, "periodType"));
                        int customrange = Integer.parseInt(getAttribute(attributes, "range"));

                        if (customPeriod > -1) graph.GDMgr.currentCustomPeriod = customPeriod;
                        if (customrange > -1) graph.GDMgr.currentCustomRange = customrange;
                    }

                    long nCustomIntervalType = Long.parseLong(getAttribute(attributes, "customIntervalType"));
                    long ncustomIntervalFactor = Long.parseLong(getAttribute(attributes, "customIntervalFactor"));

                    if (nCustomIntervalType > -1) graph.GDMgr.currentCustomIntervalType = nCustomIntervalType;
                    if (ncustomIntervalFactor > -1) graph.GDMgr.currentCustomIntervalFactor = ncustomIntervalFactor;


                    int graphIndex = (i + 1);
                    expression = "Template/GraphWindow[" + graphIndex + "]/Chart";
                    NodeList charts = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
                    //System.out.println("Chart count: " + charts.getLength());
                    if ((charts != null) && (charts.getLength() > 0)) {
                        Hashtable htSources = new Hashtable();
                        Hashtable htPanels = new Hashtable();
                        Hashtable<Integer, ChartProperties> indicators = new Hashtable<Integer, ChartProperties>();
                        ChartProperties srcCP = null;
                        for (int j = 0; j < charts.getLength(); j++) {
                            Node chart = charts.item(j);
                            int chartIndex = (j + 1);
                            byte ID = Byte.parseByte(chart.getAttributes().getNamedItem("ID").getNodeValue());
                            htSources.put(chart.getAttributes().getNamedItem("SourceID").getNodeValue(),
                                    Integer.toString(graph.GDMgr.getSources().size()));
                            if (ID == GraphDataManager.ID_BASE) {
                                //add base symbol
                                ChartProperties cp = gf.setBaseSymbol(symbl);
                                cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", false);
                                gf.setBasicPropertiesFromCP();
                                htPanels.put(chart.getAttributes().getNamedItem("PanelID").getNodeValue(),
                                        Integer.toString(graph.getIDforPanel(cp.getRect())));
                            } else if (ID == GraphDataManager.ID_VOLUME) {
                                //showWindow volume window
                                //graph.setVolumeGraphflag(true);
                                ChartProperties cp = graph.GDMgr.getVolumeCP();
                                cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", false);
                                htPanels.put(chart.getAttributes().getNamedItem("PanelID").getNodeValue(),
                                        Integer.toString(graph.getIDforPanel(cp.getRect())));
                            } else if (ID == GraphDataManager.ID_TURNOVER) {
                                //showWindow volume window
                                //graph.setVolumeGraphflag(true);
                                ChartProperties cp = graph.GDMgr.getTurnOverCP();
                                cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", false);
                                htPanels.put(chart.getAttributes().getNamedItem("PanelID").getNodeValue(),
                                        Integer.toString(graph.getIDforPanel(cp.getRect())));
                            } else {
                                //add indicator
                                String innerSourceID = chart.getAttributes().getNamedItem("InnerSource").getNodeValue();
                                int mappedID = Integer.parseInt(htSources.get(innerSourceID).toString());
                                ChartProperties innerSource = (ChartProperties) graph.GDMgr.getSources().get(mappedID);
                                String strPnlID = chart.getAttributes().getNamedItem("PanelID").getNodeValue();

                                WindowPanel r = null;
                                boolean isNewPanel = false;
                                if (htPanels.containsKey(strPnlID)) {
                                    r = (WindowPanel) graph.panels.get(Integer.parseInt(
                                            htPanels.get(strPnlID).toString()));
                                } else {
                                    r = new WindowPanel();
                                    isNewPanel = true;
                                }

                                ChartProperties cp;
                                if (ID == GraphDataManager.ID_CUSTOM_INDICATOR) {
                                    ChartProperties dummyCP = new ChartProperties(null, null, ID, null, r);
                                    dummyCP.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", false);
                                    String longName = dummyCP.longName;
                                    IndicatorMenuObject indicatorMenuObject = null;
                                    for (IndicatorMenuObject imo : IndicatorDetailStore.CustomIndicators) {
                                        if (imo.getIndicatorName().equals(longName)) {
                                            indicatorMenuObject = imo;
                                            break;
                                        }
                                    }
                                    if (indicatorMenuObject == null && ChartInterface.isSystemStrategiesEnabled()) {
                                        for (IndicatorMenuObject imo : ChartFrame.getSharedInstance().alStrategies) {
                                            if (imo.getIndicatorName().equals(longName)) {
                                                indicatorMenuObject = imo;
                                                break;
                                            }
                                        }
                                    }
                                    if (indicatorMenuObject == null  && ChartInterface.isSystemPatternsEnabled()) {
                                        for (IndicatorMenuObject imo : ChartFrame.getSharedInstance().alPatterns) {
                                            if (imo.getIndicatorName().equals(longName)) {
                                                indicatorMenuObject = imo;
                                                break;
                                            }
                                        }
                                    }
                                    if (indicatorMenuObject == null) {
                                        //MessageBox.Show(ResourceCollection.MSG_CANNOT_FIND_CUSTOM_INDICATOR + " '" + longName + "'");
                                        continue;
                                    } else {
                                        cp = graph.GDMgr.addCustomIndicator(indicatorMenuObject.getIndicatorName(),
                                                indicatorMenuObject.getIndicatorClass(), true, innerSource, r);
                                    }
                                } else {
                                    cp = graph.GDMgr.addIndicator(ID, true, innerSource, r, false);
                                }


                                cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", false);
                                indicators.put(graph.GDMgr.getSources().size() - 1, cp);

                                /*if (cp.isHidden()) {
                                    gf.isHideAll = false;
                                }*/

                                if (cp.isHidden() && graph.getIDforPanel(cp.getRect()) != 0) {
                                    int indexCp = graph.GDMgr.getIndexOfTheRect(graph.panels, r);
                                    if (allHidden(cp, graph)) {
                                        graph.deleteISPPanel(graph.GDMgr.getIndexOfTheRect(graph.panels, cp.getRect()));
                                    } else {
                                        int pnlID = getPnlIDForHiddenCp(cp, graph);
                                        cp.setRect((WindowPanel) graph.panels.get(pnlID));
                                    }

                                    graph.reDrawAll();

                                } else {
                                    if (!htPanels.containsKey(strPnlID)) {
                                        htPanels.put(strPnlID, Integer.toString(graph.getIDforPanel(r)));
                                    }
                                }
                                if ((cp instanceof IndicatorBase) && ((IndicatorBase) cp).IsStrategy()) {
                                    if (((IndicatorBase) cp).isDrawSymbol()) {
                                        ((IndicatorBase) cp).setTargetCP(srcCP);
                                    } else {
                                        srcCP = cp;
                                    }
                                }
                                try {
                                    ((Indicator) cp).setTableColumnHeading(chart.getAttributes().
                                            getNamedItem("tableColumnHeading").getNodeValue());
                                } catch (Exception e) {
                                    // Ignore for backwards compatibility
                                }
                                graph.GDMgr.addSignalLines(cp);
                                if (isNewPanel) {
                                    r.setTitle(cp.toString());
                                }
                            }
                        }

                        //Following is to attache the proper signal indicators to the signal parent indicators
                        // This was not done in earlier templating (before 8/12/2006)
                        try {
                            for (int j = 0; j < charts.getLength(); j++) {
                                Node chart = charts.item(j);
                                Boolean isSignalIndicator = Boolean.parseBoolean(
                                        chart.getAttributes().getNamedItem("isSignalIndicator").getNodeValue());
                                if (isSignalIndicator) {
                                    int signalParentIndex = Integer.parseInt(chart.getAttributes().
                                            getNamedItem("signalParent").getNodeValue());
                                    byte toID = Byte.parseByte(chart.getAttributes().
                                            getNamedItem("toID").getNodeValue());
                                    int thisID = Integer.parseInt(chart.getAttributes().
                                            getNamedItem("SourceID").getNodeValue());
                                    ChartProperties parentCP = indicators.get(signalParentIndex);
                                    parentCP.setSignalParent(true);
                                    (indicators.get(thisID)).setSignalIndicator(true);
                                    (indicators.get(thisID)).setSignalSourceID(parentCP.getID());
                                    switch (toID) {
                                        case GraphDataManager.ID_MACD:
                                            if (indicators.get(thisID) instanceof MovingAverage) {
                                                ((MACD) parentCP).setSignalLine((MovingAverage) indicators.get(thisID));
                                            } else if (indicators.get(thisID) instanceof MACDHistogram) {
                                                ((MACD) parentCP).setHistogram((MACDHistogram) indicators.get(thisID));
                                            }
                                            break;
                                        case GraphDataManager.ID_BOLLINGER_BANDS:
                                            BollingerBand bollingerBand = ((BollingerBand) indicators.get(thisID));
                                            byte bbtype = bollingerBand.getBandType();

                                            if (bollingerBand.getGroupID().equals(parentCP.getGroupID())) {
                                                if (bbtype == BollingerBand.BT_UPPER) {
                                                    ((BollingerBand) parentCP).setUpper(bollingerBand);
                                                } else if (bbtype == BollingerBand.BT_LOWER) {
                                                    ((BollingerBand) parentCP).setLower(bollingerBand);
                                                }
                                            }

                                            break;
                                        case GraphDataManager.ID_KELTNER_CHANNELS:
                                            KeltnerChannels keltnerChannels = ((KeltnerChannels) indicators.get(thisID));
                                            byte kcType = keltnerChannels.getKeltnerChannelType();

                                            if (keltnerChannels.getGroupID().equals(parentCP.getGroupID())) {
                                                if (kcType == KeltnerChannels.KC_UPPER) {
                                                    ((BollingerBand) parentCP).setUpper(keltnerChannels);
                                                } else if (kcType == KeltnerChannels.KC_LOWER) {
                                                    ((BollingerBand) parentCP).setLower(keltnerChannels);
                                                }
                                            }

                                            break;
                                        case GraphDataManager.ID_STOCHAST_OSCILL:
                                            ((StocasticOscillator) parentCP).setSignalLine((MovingAverage) indicators.get(thisID));
                                            break;
                                        case GraphDataManager.ID_KLINGER_OSCILL:
                                            ((KlingerOscillator) parentCP).setSignalLine((MovingAverage) indicators.get(thisID));
                                            break;
                                        case GraphDataManager.ID_PRICE_CHANNELS:
                                            break;
                                        case GraphDataManager.ID_FO:
                                            ((ForcastOscillator) parentCP).setSignalLine((MovingAverage) indicators.get(thisID));
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }

                        } catch (NullPointerException e) {
                            //Ignore for backward compatibility
                        }

                        //added by shashika to save horizontal line
                        expression = "Layout/GraphWindow[" + graphIndex + "]/Object";
                        NodeList objects = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
                        System.out.println("Loading object count: " + objects.getLength());
                        if ((objects != null) && (objects.getLength() > 0)) {
                            for (int j = 0; j < objects.getLength(); j++) {
                                Node xmlobj = objects.item(j);
                                int objIndex = (j + 1);

                                int ID = Integer.parseInt(xmlobj.getAttributes().getNamedItem("Type").getNodeValue());
                                ChartProperties innerSource = null;
                                String innerSourceID = xmlobj.getAttributes().getNamedItem("Target").getNodeValue();
                                if (!innerSourceID.equals("")) {
                                    int mappedID = Integer.parseInt(htSources.get(innerSourceID).toString());
                                    innerSource = (ChartProperties) graph.GDMgr.getSources().get(mappedID);
                                }
                                String strPnlID = xmlobj.getAttributes().getNamedItem("PanelID").getNodeValue();
                                int pnlID = Integer.parseInt(htPanels.get(strPnlID).toString());
                                WindowPanel r = (WindowPanel) graph.panels.get(pnlID);
                                AbstractObject ao;
                                if (ID == AbstractObject.INT_REGRESSION || ID == AbstractObject.INT_STD_ERROR_CHANNEL ||
                                        ID == AbstractObject.INT_RAFF_REGRESSION || ID == AbstractObject.INT_STD_DEV_CHANNEL ||
                                        ID == AbstractObject.INT_EQUI_DIST_CHANNEL) {
                                    ao = AbstractObject.CreateObject(ID, null, null, innerSource, r, graph.panels, graph);
                                } else {
                                    ao = AbstractObject.CreateObject(ID, null, null, null, innerSource, r, graph.panels, graph);
                                }
                                ao.loadFromLayout(xpath, document, expression + "[" + objIndex + "]");
                                ao.pnlID = pnlID;
                                graph.GDMgr.addToObjectArray(ao);
                            }
                        }//////////////////////

                        //Loading Highlighted Areas
                        expression = "Template/GraphWindow[" + graphIndex + "]/HighlightedArea";
                        NodeList highlights = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
                        if ((highlights != null) && (highlights.getLength() > 0)) {
                            ChartProperties topCp, botCp;
                            int topID, botID;
                            Color fillColor;
                            float alpha;
                            graph.GDMgr.getHighLightedSources().clear();
                            for (int j = 0; j < highlights.getLength(); j++) {
                                Node xmlobj = highlights.item(j);
                                topID = Integer.parseInt(xmlobj.getAttributes().getNamedItem("TopCP").getNodeValue());
                                botID = Integer.parseInt(xmlobj.getAttributes().getNamedItem("BottomCP").getNodeValue());
                                topCp = (ChartProperties) graph.GDMgr.getSources().get(topID);
                                botCp = (ChartProperties) graph.GDMgr.getSources().get(botID);
                                fillColor = new Color(Integer.parseInt(xmlobj.getAttributes().getNamedItem("Color").getNodeValue()));
                                alpha = Float.parseFloat(xmlobj.getAttributes().getNamedItem("Alpha").getNodeValue());
                                HighlightedArea ha = new HighlightedArea(topCp, botCp, fillColor, alpha);
                                graph.GDMgr.getHighLightedSources().add(ha);
                            }
                        }
                        //graph.GDMgr.reConstructGraphStore();
                        graph.GDMgr.reloadGraphStoreData();
                        graph.reDrawAll();
                    } else {
                        return false;
                    }

                    if (i > 0) {
                        gf.setBounds(Integer.parseInt(saBounds[0]), Integer.parseInt(saBounds[1]),
                                Integer.parseInt(saBounds[2]), Integer.parseInt(saBounds[3]));
                        gf.show();
                    }
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        /* if (!cFrame.isBIsTabbedMode()) {
            cFrame.tileWindows();
        }*/
        cFrame.tileWindows();

        return true;
    }

    public static void SaveTemplate(File tmpltFile, GraphFrame[] graphFrames) throws Exception {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.newDocument();
            Element root = document.createElement("Template");
            root.setAttribute("Name", tmpltFile.getName());
            document.appendChild(root);

            // chart frame specific
            Element chartFrame = document.createElement("ChartFrame");
            chartFrame.setAttribute("isShowSnapBall", Boolean.toString(ChartFrame.getSharedInstance().isShowSnapBall()));
            chartFrame.setAttribute("isShowCrossHairs", Boolean.toString(ChartFrame.getSharedInstance().isShowCrossHairs()));
            root.appendChild(chartFrame);

            for (int i = 0; i < graphFrames.length; i++) {
                GraphFrame gf = graphFrames[i];
                StockGraph graph = gf.graph;

                Element graphWindow = document.createElement("GraphWindow");
                String strBounds = new StringBuffer().append(gf.getX() + ",").append(gf.getY()).append(",").
                        append(gf.getWidth()).append(",").append(gf.getHeight()).toString();
                graphWindow.setAttribute("Bounds", strBounds);
                graphWindow.setAttribute("Mode", Boolean.toString(graph.isCurrentMode()));
                graphWindow.setAttribute("Period", Integer.toString(graph.getPeriod()));

                graphWindow.setAttribute("isShowCurrentPriceLine", Boolean.toString(graph.GDMgr.isShowCurrentPriceLine()));
                graphWindow.setAttribute("isShowBidAskTags", Boolean.toString(graph.GDMgr.isShowBidAskTags()));
                graphWindow.setAttribute("isShowOrderLines", Boolean.toString(graph.GDMgr.isShowOrderLines()));
                graphWindow.setAttribute("isShowMinMaxLines", Boolean.toString(graph.GDMgr.isShowMinMaxPriceLines()));
                graphWindow.setAttribute("isOpenInProchart", Boolean.toString(ChartSettings.getSettings().isOpenInProChart()));
                graphWindow.setAttribute("isShowVolumeInProchart", Boolean.toString(graph.isShowingVolumeGrf()));
                graphWindow.setAttribute("isShowTurnOverInProChart", Boolean.toString(graph.isShowingTurnOverGraph()));
                graphWindow.setAttribute("isShowAnnouncements", Boolean.toString(graph.GDMgr.isShowingAnnouncements()));
                graphWindow.setAttribute("isShowSplits", Boolean.toString(graph.GDMgr.isShowingSplits()));
                graphWindow.setAttribute("isShowVolumeByPrice", Boolean.toString(graph.GDMgr.isShowVolumeByPrice()));
                graphWindow.setAttribute("showVolumeByPrice", Byte.toString(graph.GDMgr.getVolumeByPriceType()));
                graphWindow.setAttribute("isShowSupportResistanceLines", Boolean.toString(graph.GDMgr.isShowResistanceLines()));
                graphWindow.setAttribute("crossHairType", Byte.toString(graph.GDMgr.getCrossHairType()));
                graphWindow.setAttribute("isShowPreviousCloseLine", Boolean.toString(graph.GDMgr.isshowPreviousCloseLine()));

                if (graph.getPeriod() == StockGraph.CUSTOM_PERIODS) {
                    graphWindow.setAttribute("periodType", Integer.toString(graph.GDMgr.currentCustomPeriod));
                    graphWindow.setAttribute("range", Integer.toString(graph.GDMgr.currentCustomRange));
                }
                if (graph.getInterval() ==
                        (graph.GDMgr.currentCustomIntervalType * graph.GDMgr.currentCustomIntervalFactor)) {
                    graphWindow.setAttribute("customIntervalType", Long.toString(graph.GDMgr.currentCustomIntervalType));
                    graphWindow.setAttribute("customIntervalFactor", Long.toString(graph.GDMgr.currentCustomIntervalFactor));
                }
                graphWindow.setAttribute("Interval", Long.toString(graph.getInterval()));
                root.appendChild(graphWindow);


                ArrayList alSources = graph.GDMgr.getSources();
                Hashtable<Integer, ChartProperties> signalParents = new Hashtable<Integer, ChartProperties>();
                for (int j = 0; j < alSources.size(); j++) {
                    ChartProperties cp = (ChartProperties) alSources.get(j);
                    if (cp.isSignalParent() && !cp.isSignalIndicator()) {
                        signalParents.put(j, cp);
                    }
                    if (cp.getID() == GraphDataManager.ID_COMPARE) continue;

                    Element chart = document.createElement("Chart");
                    chart.setAttribute("ID", Byte.toString(cp.getID()));
                    chart.setAttribute("SourceID", Integer.toString(j));
                    chart.setAttribute("PanelID", Integer.toString(graph.getIDforPanel(cp.getRect())));
                    chart.setAttribute("isSignalIndicator", Boolean.toString(cp.isSignalIndicator()));
                    if (cp.isSignalIndicator()) {
                        chart.setAttribute("toID", Byte.toString(cp.getSignalSourceID()));
                    }
                    if ((cp.getID() == GraphDataManager.ID_BASE)) {
                        Color colorUpNow = gf.graph.graphUpColor;
                        Color colorDownNow = gf.graph.graphDownColor;

                        if (!colorUpNow.equals(cp.getColor()) || !colorDownNow.equals(cp.getWarningColor())) {
                            cp.setUserSaved(true);
                        }
                    } else if ((cp.getID() == GraphDataManager.ID_VOLUME)) {
                        Color colorUpNow = gf.graph.graphVolumeUpColor;
                        Color colorDownNow = gf.graph.graphVolumeDownColor;

                        if (!colorUpNow.equals(cp.getColor()) || !colorDownNow.equals(cp.getWarningColor())) {
                            cp.setUserSaved(true);
                        }
                    } else if ((cp.getID() == GraphDataManager.ID_TURNOVER)) {
                        Color colorUpNow = gf.graph.graphTurnOverUpColor;
                        Color colorDownNow = gf.graph.graphTurnOverDownColor;

                        if (!colorUpNow.equals(cp.getColor()) || !colorDownNow.equals(cp.getWarningColor())) {
                            cp.setUserSaved(true);
                        }
                    } else {//INDICATOR

                        if (cp.isHidden() && graph.getIDforPanel(cp.getRect()) != 0) {
                            chart.setAttribute("PanelID", HIDDEN_PANEL_TEMP_ID);
                        }
                        int innersourceID = ((Indicator) cp).getInnerSourceIndex(alSources);
                        chart.setAttribute("InnerSource", Integer.toString(innersourceID));
                        chart.setAttribute("tableColumnHeading", ((Indicator) cp).getTableColumnHeading());

                    }
                    cp.saveTemplate(chart, document);
                    graphWindow.appendChild(chart);
                }

                //Following is to save signal indicator relationships
                Enumeration<Integer> keys = signalParents.keys();
                while (keys.hasMoreElements()) {
                    int signalParentId = keys.nextElement();
                    NodeList chartNodeList = graphWindow.getElementsByTagName("Chart");
                    for (int k = 0; k < chartNodeList.getLength(); k++) {
                        Node chartNode = chartNodeList.item(k);
                        Node attribute = chartNode.getAttributes().getNamedItem("toID");
                        if (attribute == null) {
                            continue;
                        }
                        Byte toId = Byte.parseByte(attribute.getNodeValue());
                        if (toId == signalParents.get(signalParentId).getID()) {
                            ((Element) chartNode).setAttribute("signalParent", Integer.toString(signalParentId));
                        }
                    }
                }

                //Saving Highlighted Areas
                ArrayList<HighlightedArea> alHighlights = graph.GDMgr.getHighLightedSources();
                for (int j = 0; j < alHighlights.size(); j++) {
                    HighlightedArea ha = alHighlights.get(j);
                    Element xmlobj = document.createElement("HighlightedArea");
                    xmlobj.setAttribute("TopCP", Integer.toString(graph.GDMgr.getIndexForSourceCP(ha.getTopCP())));
                    xmlobj.setAttribute("BottomCP", Integer.toString(graph.GDMgr.getIndexForSourceCP(ha.getBottomCP())));
                    xmlobj.setAttribute("Color", Integer.toString(ha.getFillColor().getRGB()));
                    xmlobj.setAttribute("Alpha", Float.toString(ha.getAlpha()));
                    graphWindow.appendChild(xmlobj);
                }

                //added by shashika to save horizontal line
                ArrayList alOblects = graph.GDMgr.getObjArray();
                for (int j = 0; j < alOblects.size(); j++) {
                    AbstractObject ao = (AbstractObject) alOblects.get(j);
                    if (ao.getObjType() == AbstractObject.INT_LINE_HORIZ || ao.getObjType() == AbstractObject.INT_LINE_SLOPE) { //only saving horizontal line
                        Element xmlobj = document.createElement("Object");
                        xmlobj.setAttribute("Type", Integer.toString(ao.getObjType()));
                        String innersourceID = "";
                        if (ao.getTarget() != null) {
                            innersourceID = Integer.toString(graph.GDMgr.getIndexForSourceCP(ao.getTarget()));
                        }
                        xmlobj.setAttribute("Target", innersourceID);
                        xmlobj.setAttribute("PanelID", Integer.toString(graph.getIDforPanel(ao.getRect())));
                        ao.saveToLayout(xmlobj, document);
                        graphWindow.appendChild(xmlobj);
                    }
                }
                //added by shashika to save resistance lines
                /*ObjectHorizontalPriceLine[] lines = graph.GDMgr.getResistanceLines();
                for (int j = 0; j < lines.length; j++) {
                    AbstractObject ao = (AbstractObject) lines[j];
                    Element xmlobj = document.createElement("Object");
                    xmlobj.setAttribute("Type", Integer.toString(ao.getObjType()));
                    String innersourceID = "";
                    if (ao.getTarget() != null) {
                        innersourceID = Integer.toString(graph.GDMgr.getIndexForSourceCP(ao.getTarget()));
                    }
                    xmlobj.setAttribute("Target", innersourceID);
                    xmlobj.setAttribute("PanelID", Integer.toString(graph.getIDforPanel(ao.getRect())));
                    ao.saveToLayout(xmlobj, document);
                    graphWindow.appendChild(xmlobj);

                }*/
                /////////////////////////////////////////////////////////////
            }

            TransformerFactory tranFactory = TransformerFactory.newInstance();
            Transformer aTransformer = tranFactory.newTransformer();

            Source src = new DOMSource(document);
            Result dest = new StreamResult(tmpltFile);
            aTransformer.transform(src, dest);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    public static int getPnlIDForHiddenCp(ChartProperties cp, StockGraph graph) {
        int pnlID = -1;
        int tmpCpPnlID, cpPnlID;
        cpPnlID = graph.getIDforPanel(cp.getRect());
        for (int i = 0; i < graph.GDMgr.getSources().size(); i++) {
            ChartProperties tmpCp = (ChartProperties) graph.GDMgr.getSources().get(i);
            tmpCpPnlID = graph.getIDforPanel(tmpCp.getRect());

            if ((cpPnlID == tmpCpPnlID) && !(tmpCp.isHidden()) && (tmpCp.getGroupID().equals(cp.getGroupID())) &&
                    ((graph.GDMgr.getIndexForSourceCP(cp)) != (graph.GDMgr.getIndexForSourceCP(tmpCp)))) {
                pnlID = tmpCpPnlID;
            }

        }
        return pnlID;
    }

    public static boolean allHidden(ChartProperties cp, StockGraph graph) {
        boolean allHidden = true;
        int tmpCpPnlID, cpPnlID;
        cpPnlID = graph.getIDforPanel(cp.getRect());
        for (int i = 0; i < graph.GDMgr.getSources().size(); i++) {
            ChartProperties tmpCp = (ChartProperties) graph.GDMgr.getSources().get(i);
            tmpCpPnlID = graph.getIDforPanel(tmpCp.getRect());

            if ((cpPnlID == tmpCpPnlID) && !(tmpCp.isHidden()) && (tmpCp.getGroupID().equals(cp.getGroupID())) &&
                    ((graph.GDMgr.getIndexForSourceCP(cp)) != (graph.GDMgr.getIndexForSourceCP(tmpCp)))) {
                allHidden = false;
            }

        }
        return allHidden;
    }

    public static String loadProperty(XPath xpath, Document document, String expression) {
        try {
            return xpath.evaluate(expression, document, XPathConstants.STRING).toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    public static void saveProperty(Element chart, Document document, String name, String value) {
        Element property = document.createElement(name);
        property.setTextContent(value);
        chart.appendChild(property);
    }

    public static void modifyProperty(Element chart, String name, String value) {
        try {
            NodeList nodeList = chart.getElementsByTagName(name);
            Element property = (Element) nodeList.item(0);
            if (property != null) {
            property.setTextContent(value);
            } 
        } catch (Exception e) {
            System.out.println(" exception in modifyTemplate() TemplateFactory");
        }
    }


    /* **************************** multiple templates  related *******************************/
    //added by charithn

    public static void SaveMultipleTemplates(File tmpltFile, GraphFrame[] graphFrames) throws Exception {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.newDocument();
            Element root = document.createElement("Template");
            root.setAttribute("Name", tmpltFile.getName());
            document.appendChild(root);

            // chart frame specific
            Element chartFrame = document.createElement("ChartFrame");
            chartFrame.setAttribute("isShowSnapBall", Boolean.toString(ChartFrame.getSharedInstance().isShowSnapBall()));
            chartFrame.setAttribute("isShowCrossHairs", Boolean.toString(ChartFrame.getSharedInstance().isShowCrossHairs()));
            root.appendChild(chartFrame);

            for (int i = 0; i < graphFrames.length; i++) {
                GraphFrame gf = graphFrames[i];
                StockGraph graph = gf.graph;

                Element graphWindow = document.createElement("GraphWindow");
                String strBounds = new StringBuffer().append(gf.getX() + ",").append(gf.getY()).append(",").
                        append(gf.getWidth()).append(",").append(gf.getHeight()).toString();
                graphWindow.setAttribute("Bounds", strBounds);
                graphWindow.setAttribute("Mode", Boolean.toString(graph.isCurrentMode()));
                graphWindow.setAttribute("Period", Integer.toString(graph.getPeriod()));

                graphWindow.setAttribute("isShowCurrentPriceLine", Boolean.toString(graph.GDMgr.isShowCurrentPriceLine()));
                graphWindow.setAttribute("isShowBidAskTags", Boolean.toString(graph.GDMgr.isShowBidAskTags()));
                graphWindow.setAttribute("isShowOrderLines", Boolean.toString(graph.GDMgr.isShowOrderLines()));
                graphWindow.setAttribute("isShowMinMaxLines", Boolean.toString(graph.GDMgr.isShowMinMaxPriceLines()));
                graphWindow.setAttribute("isOpenInProchart", Boolean.toString(ChartSettings.getSettings().isOpenInProChart()));
                graphWindow.setAttribute("isShowVolumeInProchart", Boolean.toString(graph.isShowingVolumeGrf()));
                graphWindow.setAttribute("isShowTurnOverInProChart", Boolean.toString(graph.isShowingTurnOverGraph()));
                graphWindow.setAttribute("isShowAnnouncements", Boolean.toString(graph.GDMgr.isShowingAnnouncements()));
                graphWindow.setAttribute("isShowSplits", Boolean.toString(graph.GDMgr.isShowingSplits()));
                graphWindow.setAttribute("isShowVolumeByPrice", Boolean.toString(graph.GDMgr.isShowVolumeByPrice()));
                graphWindow.setAttribute("showVolumeByPrice", Byte.toString(graph.GDMgr.getVolumeByPriceType()));
                graphWindow.setAttribute("isShowSupportResistanceLines", Boolean.toString(graph.GDMgr.isShowResistanceLines()));
                graphWindow.setAttribute("crossHairType", Byte.toString(graph.GDMgr.getCrossHairType()));
                graphWindow.setAttribute("isShowPreviousCloseLine", Boolean.toString(graph.GDMgr.isshowPreviousCloseLine()));

                if (graph.getPeriod() == StockGraph.CUSTOM_PERIODS) {
                    graphWindow.setAttribute("periodType", Integer.toString(graph.GDMgr.currentCustomPeriod));
                    graphWindow.setAttribute("range", Integer.toString(graph.GDMgr.currentCustomRange));
                }
                if (graph.getInterval() ==
                        (graph.GDMgr.currentCustomIntervalType * graph.GDMgr.currentCustomIntervalFactor)) {
                    graphWindow.setAttribute("customIntervalType", Long.toString(graph.GDMgr.currentCustomIntervalType));
                    graphWindow.setAttribute("customIntervalFactor", Long.toString(graph.GDMgr.currentCustomIntervalFactor));
                }
                graphWindow.setAttribute("Interval", Long.toString(graph.getInterval()));
                root.appendChild(graphWindow);

                ArrayList alSources = graph.GDMgr.getSources();
                Hashtable<Integer, ChartProperties> signalParents = new Hashtable<Integer, ChartProperties>();
                for (int j = 0; j < alSources.size(); j++) {
                    ChartProperties cp = (ChartProperties) alSources.get(j);
                    if (cp.isSignalParent() && !cp.isSignalIndicator()) {
                        signalParents.put(j, cp);
                    }
                    if (cp.getID() == GraphDataManager.ID_COMPARE) continue;

                    Element chart = document.createElement("Chart");
                    chart.setAttribute("ID", Byte.toString(cp.getID()));
                    chart.setAttribute("SourceID", Integer.toString(j));
                    chart.setAttribute("PanelID", Integer.toString(graph.getIDforPanel(cp.getRect())));
                    chart.setAttribute("isSignalIndicator", Boolean.toString(cp.isSignalIndicator()));
                    if (cp.isSignalIndicator()) {
                        chart.setAttribute("toID", Byte.toString(cp.getSignalSourceID()));
                    }
                    if ((cp.getID() == GraphDataManager.ID_BASE)) {
                        Color colorUpNow = gf.graph.graphUpColor;
                        Color colorDownNow = gf.graph.graphDownColor;

                        if (!colorUpNow.equals(cp.getColor()) || !colorDownNow.equals(cp.getWarningColor())) {
                            cp.setUserSaved(true);
                        }
                    } else if ((cp.getID() == GraphDataManager.ID_VOLUME)) {
                        Color colorUpNow = gf.graph.graphVolumeUpColor;
                        Color colorDownNow = gf.graph.graphVolumeDownColor;

                        if (!colorUpNow.equals(cp.getColor()) || !colorDownNow.equals(cp.getWarningColor())) {
                            cp.setUserSaved(true);
                        }
                    } else if ((cp.getID() == GraphDataManager.ID_TURNOVER)) {
                        Color colorUpNow = gf.graph.graphTurnOverUpColor;
                        Color colorDownNow = gf.graph.graphTurnOverDownColor;

                        if (!colorUpNow.equals(cp.getColor()) || !colorDownNow.equals(cp.getWarningColor())) {
                            cp.setUserSaved(true);
                        }
                    } else {                                               //INDICATOR

                        if (cp.isHidden() && graph.getIDforPanel(cp.getRect()) != 0) {
                            chart.setAttribute("PanelID", HIDDEN_PANEL_TEMP_ID);
                        }
                        int innersourceID = ((Indicator) cp).getInnerSourceIndex(alSources);
                        chart.setAttribute("InnerSource", Integer.toString(innersourceID));
                        chart.setAttribute("tableColumnHeading", ((Indicator) cp).getTableColumnHeading());

                    }
                    cp.saveTemplate(chart, document);
                    graphWindow.appendChild(chart);
                }

                //Following is to save signal indicator relationships
                Enumeration<Integer> keys = signalParents.keys();
                while (keys.hasMoreElements()) {
                    int signalParentId = keys.nextElement();
                    NodeList chartNodeList = graphWindow.getElementsByTagName("Chart");
                    for (int k = 0; k < chartNodeList.getLength(); k++) {
                        Node chartNode = chartNodeList.item(k);
                        Node attribute = chartNode.getAttributes().getNamedItem("toID");
                        if (attribute == null) {
                            continue;
                        }
                        Byte toId = Byte.parseByte(attribute.getNodeValue());
                        if (toId == signalParents.get(signalParentId).getID()) {
                            ((Element) chartNode).setAttribute("signalParent", Integer.toString(signalParentId));
                        }
                    }
                }
                //Saving Highlighted Areas
                ArrayList<HighlightedArea> alHighlights = graph.GDMgr.getHighLightedSources();
                for (int j = 0; j < alHighlights.size(); j++) {
                    HighlightedArea ha = alHighlights.get(j);
                    Element xmlobj = document.createElement("HighlightedArea");
                    xmlobj.setAttribute("TopCP", Integer.toString(graph.GDMgr.getIndexForSourceCP(ha.getTopCP())));
                    xmlobj.setAttribute("BottomCP", Integer.toString(graph.GDMgr.getIndexForSourceCP(ha.getBottomCP())));
                    xmlobj.setAttribute("Color", Integer.toString(ha.getFillColor().getRGB()));
                    xmlobj.setAttribute("Alpha", Float.toString(ha.getAlpha()));
                    graphWindow.appendChild(xmlobj);
                }
            }

            TransformerFactory tranFactory = TransformerFactory.newInstance();
            Transformer aTransformer = tranFactory.newTransformer();

            Source src = new DOMSource(document);
            Result dest = new StreamResult(tmpltFile);
            aTransformer.transform(src, dest);
        } catch (Exception ex) {
            ex.printStackTrace();
            throw ex;
        }
    }

    public static String getAttribute(NamedNodeMap attributes, String tagName) {
        String val = "-1";
        try {
            val = attributes.getNamedItem(tagName).getNodeValue();
        } catch (Exception e) {

        }
        return val;
    }

    public static boolean LoadMultipleTemplates(GraphFrame originalGraphFrame, File tmpltFile, String symbl, boolean detached, int index) {
        ChartFrame cFrame = ChartFrame.getSharedInstance();
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(tmpltFile);

            XPath xpath = XPathFactory.newInstance().newXPath();

            String expression = "Template/ChartFrame";
            boolean isShowSnapBall = false;
            boolean isShowCrossHair = false;
            Node chartFrame = null;
            try {
                chartFrame = (Node) xpath.evaluate(expression, document, XPathConstants.NODE);
            } catch (Exception ex) {
                // do nothing
            } finally {
                if (chartFrame != null) {
                    try {
            NamedNodeMap chartFrameAttributes = chartFrame.getAttributes();
                        isShowSnapBall = Boolean.parseBoolean(
                    chartFrameAttributes.getNamedItem("isShowSnapBall").getNodeValue());
                        isShowCrossHair = Boolean.parseBoolean(
                    chartFrameAttributes.getNamedItem("isShowCrossHairs").getNodeValue());
                    } catch (NullPointerException ex) { // ignore - backward compatibility}
                    }
                }
            }

            ChartFrame.getSharedInstance().setShowSnapBall(isShowSnapBall);
            ChartFrame.getSharedInstance().setShowCrossHairs(isShowCrossHair);

            expression = "Template/GraphWindow";

            // First, obtain the element as a node.

            NodeList graphWindows = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);

            //System.out.println("NodeList size: " + graphWindows.getLength());

            if ((graphWindows != null) && (graphWindows.getLength() > 0)) {
                for (int i = 0; i < graphWindows.getLength(); i++) {
                    if (i != index) {
                        continue;
                    }
                    Node graphWindow = graphWindows.item(i);
                    NamedNodeMap attributes = graphWindow.getAttributes();
                    boolean isCurrentMode = Boolean.parseBoolean(attributes.getNamedItem("Mode").getNodeValue());
                    int period = Integer.parseInt(attributes.getNamedItem("Period").getNodeValue());
                    long interval = Long.parseLong(attributes.getNamedItem("Interval").getNodeValue());
                    String[] saBounds = attributes.getNamedItem("Bounds").getNodeValue().split(",");
                    //System.out.println("mode, period, interval = " + isCurrentMode + ", " + period + ", " + interval);

                    GraphFrame gf;
                    gf = originalGraphFrame;
                    gf.graph.clearAll();
                    /*if (i == 0) {
                        gf = originalGraphFrame;
                        gf.graph.clearAll();
                    } else {
                        gf = cFrame.addNewGraph(false, true);
                    }*/
                    gf.setDetached(detached);
                    StockGraph graph = gf.graph;
                    gf.CurrentMode = isCurrentMode;
                    graph.setCurrentMode(isCurrentMode);
                    graph.setPeriodFlag(period);
                    graph.setIntervalFlag(interval);

                    boolean isShowCurrentPrice = Boolean.parseBoolean(getAttribute(attributes, "isShowCurrentPriceLine"));
                    boolean isShowBidAskTags = Boolean.parseBoolean(getAttribute(attributes, "isShowBidAskTags"));
                    boolean isShowOrderLines = Boolean.parseBoolean(getAttribute(attributes, "isShowOrderLines"));
                    boolean isShowMinMaxPrices = Boolean.parseBoolean(getAttribute(attributes, "isShowMinMaxLines"));
                    boolean bIsShowVolume = Boolean.parseBoolean(getAttribute(attributes, "isShowVolumeInProchart"));
                    boolean bIsShowTurnOver = Boolean.parseBoolean(getAttribute(attributes, "isShowTurnOverInProChart"));
                    boolean bIsShowAnnouncecment = Boolean.parseBoolean(getAttribute(attributes, "isShowAnnouncements"));
                    boolean bIsShowSplits = Boolean.parseBoolean(getAttribute(attributes, "isShowSplits"));
                    boolean bIsShowVolByPrice = Boolean.parseBoolean(getAttribute(attributes, "isShowVolumeByPrice"));
                    boolean isShowSupportResistanceLines = Boolean.parseBoolean(getAttribute(attributes, "isShowSupportResistanceLines"));
                    byte volByPrice = Byte.parseByte(getAttribute(attributes, "showVolumeByPrice"));
                    byte crossHairType = Byte.parseByte(getAttribute(attributes, "crossHairType"));
                    boolean isshowPreviousCloseLine = Boolean.parseBoolean(getAttribute(attributes, "isShowPreviousCloseLine"));

                        graph.GDMgr.setShowCurrentPriceLine(isShowCurrentPrice);
                        graph.GDMgr.setShowBidAskTags(isShowBidAskTags);
                        graph.GDMgr.setShowOrderLines(isShowOrderLines);
                        graph.GDMgr.setShowMinMaxPriceLines(isShowMinMaxPrices);
                        graph.setVolumeGraphflag(bIsShowVolume);
                        graph.setTurnOverGraphFlag(bIsShowTurnOver);
                        graph.GDMgr.setShowAnnouncements(bIsShowAnnouncecment);
                        graph.GDMgr.setShowSplits(bIsShowSplits);
                        graph.GDMgr.setShowVolumeByPrice(bIsShowVolByPrice);
                        graph.GDMgr.setShowResistanceLine(isShowSupportResistanceLines);
                        if (volByPrice > -1) graph.GDMgr.setVolumeByPriceType(volByPrice);
                        if (crossHairType > -1) gf.graph.GDMgr.setCrossHairType(crossHairType);
                    graph.GDMgr.setShowPreviousCloseLine(isshowPreviousCloseLine);

                    if (graph.getPeriod() == StockGraph.CUSTOM_PERIODS) {
                        int customPeriod = Integer.parseInt(getAttribute(attributes, "periodType"));
                        int customrange = Integer.parseInt(getAttribute(attributes, "range"));

                        if (customPeriod > -1) graph.GDMgr.currentCustomPeriod = customPeriod;
                        if (customrange > -1) graph.GDMgr.currentCustomRange = customrange;
                    }

                    long nCustomIntervalType = Long.parseLong(getAttribute(attributes, "customIntervalType"));
                    long ncustomIntervalFactor = Long.parseLong(getAttribute(attributes, "customIntervalFactor"));

                    if (nCustomIntervalType > -1) graph.GDMgr.currentCustomIntervalType = nCustomIntervalType;
                    if (ncustomIntervalFactor > -1) graph.GDMgr.currentCustomIntervalFactor = ncustomIntervalFactor;

                    int graphIndex = (i + 1);
                    expression = "Template/GraphWindow[" + graphIndex + "]/Chart";
                    NodeList charts = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
                    //System.out.println("Chart count: " + charts.getLength());
                    if ((charts != null) && (charts.getLength() > 0)) {
                        Hashtable htSources = new Hashtable();
                        Hashtable htPanels = new Hashtable();
                        Hashtable<Integer, ChartProperties> indicators = new Hashtable<Integer, ChartProperties>();
                        ChartProperties srcCP = null;
                        for (int j = 0; j < charts.getLength(); j++) {
                            Node chart = charts.item(j);
                            int chartIndex = (j + 1);
                            byte ID = Byte.parseByte(chart.getAttributes().getNamedItem("ID").getNodeValue());
                            htSources.put(chart.getAttributes().getNamedItem("SourceID").getNodeValue(),
                                    Integer.toString(graph.GDMgr.getSources().size()));
                            if (ID == GraphDataManager.ID_BASE) {
                                //add base symbol
                                ChartProperties cp = gf.setBaseSymbol(symbl);
                                cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", false);
                                gf.setBasicPropertiesFromCP();
                                htPanels.put(chart.getAttributes().getNamedItem("PanelID").getNodeValue(),
                                        Integer.toString(graph.getIDforPanel(cp.getRect())));
                            } else if (ID == GraphDataManager.ID_VOLUME) {
                                //showWindow volume window
                                //graph.setVolumeGraphflag(true);
                                ChartProperties cp = graph.GDMgr.getVolumeCP();
                                cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", false);
                                htPanels.put(chart.getAttributes().getNamedItem("PanelID").getNodeValue(),
                                        Integer.toString(graph.getIDforPanel(cp.getRect())));
                            } else if (ID == GraphDataManager.ID_TURNOVER) {
                                //showWindow volume window
                                //graph.setVolumeGraphflag(true);
                                ChartProperties cp = graph.GDMgr.getTurnOverCP();
                                cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", false);
                                htPanels.put(chart.getAttributes().getNamedItem("PanelID").getNodeValue(),
                                        Integer.toString(graph.getIDforPanel(cp.getRect())));
                            } else {
                                //add indicator
                                String innerSourceID = chart.getAttributes().getNamedItem("InnerSource").getNodeValue();
                                int mappedID = Integer.parseInt(htSources.get(innerSourceID).toString());
                                ChartProperties innerSource = (ChartProperties) graph.GDMgr.getSources().get(mappedID);
                                String strPnlID = chart.getAttributes().getNamedItem("PanelID").getNodeValue();//
                                WindowPanel r = null;
                                boolean isNewPanel = false;
                                if (htPanels.containsKey(strPnlID)) {
                                    r = (WindowPanel) graph.panels.get(Integer.parseInt(
                                            htPanels.get(strPnlID).toString()));
                                } else {
                                    r = new WindowPanel();
                                    isNewPanel = true;
                                }
                                ChartProperties cp;
                                if (ID == GraphDataManager.ID_CUSTOM_INDICATOR) {
                                    ChartProperties dummyCP = new ChartProperties(null, null, ID, null, r);
                                    dummyCP.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", false);
                                    String longName = dummyCP.longName;
                                    IndicatorMenuObject indicatorMenuObject = null;
                                    for (IndicatorMenuObject imo : ChartFrame.getSharedInstance().customIndicators) {
                                        if (imo.getIndicatorName().equals(longName)) {
                                            indicatorMenuObject = imo;
                                            break;
                                        }
                                    }
                                    if (indicatorMenuObject == null && ChartInterface.isSystemStrategiesEnabled()) {
                                        for (IndicatorMenuObject imo : ChartFrame.getSharedInstance().alStrategies) {
                                            if (imo.getIndicatorName().equals(longName)) {
                                                indicatorMenuObject = imo;
                                                break;
                                            }
                                        }
                                    }
                                    if (indicatorMenuObject == null  && ChartInterface.isSystemPatternsEnabled()) {
                                        for (IndicatorMenuObject imo : ChartFrame.getSharedInstance().alPatterns) {
                                            if (imo.getIndicatorName().equals(longName)) {
                                                indicatorMenuObject = imo;
                                                break;
                                            }
                                        }
                                    }
                                    if (indicatorMenuObject == null) {
                                        //MessageBox.Show(ResourceCollection.MSG_CANNOT_FIND_CUSTOM_INDICATOR + " '" + longName + "'");
                                        continue;
                                    } else {
                                        cp = graph.GDMgr.addCustomIndicator(indicatorMenuObject.getIndicatorName(),
                                                indicatorMenuObject.getIndicatorClass(), true, innerSource, r);
                                    }
                                } else {
                                    cp = graph.GDMgr.addIndicator(ID, true, innerSource, r, false);
                                }
                                cp.loadTemplate(xpath, document, expression + "[" + chartIndex + "]", false);
                                indicators.put(graph.GDMgr.getSources().size() - 1, cp);

                                if (cp.isHidden() && graph.getIDforPanel(cp.getRect()) != 0) {
                                    int indexCp = graph.GDMgr.getIndexOfTheRect(graph.panels, r);
                                    if (allHidden(cp, graph)) {
                                        graph.deleteISPPanel(graph.GDMgr.getIndexOfTheRect(graph.panels, cp.getRect()));
                                    } else {
                                        int pnlID = getPnlIDForHiddenCp(cp, graph);
                                        cp.setRect((WindowPanel) graph.panels.get(pnlID));
                                    }

                                    graph.reDrawAll();

                                } else {
                                    if (!htPanels.containsKey(strPnlID)) {
                                        htPanels.put(strPnlID, Integer.toString(graph.getIDforPanel(r)));
                                    }
                                }

                                if ((cp instanceof IndicatorBase) && ((IndicatorBase) cp).IsStrategy()) {
                                    if (((IndicatorBase) cp).isDrawSymbol()) {
                                        ((IndicatorBase) cp).setTargetCP(srcCP);
                                    } else {
                                        srcCP = cp;
                                    }
                                }
                                try {
                                    ((Indicator) cp).setTableColumnHeading(chart.getAttributes().
                                            getNamedItem("tableColumnHeading").getNodeValue());
                                } catch (Exception e) {
                                    // Ignore for backwards compatibility
                                }
                                graph.GDMgr.addSignalLines(cp);
                                if (isNewPanel) {
                                    r.setTitle(cp.toString());
                                }
                            }
                        }

                        //Following is to attache the proper signal indicators to the signal parent indicators
                        // This was not done in earlier templating (before 8/12/2006)
                        try {
                            for (int j = 0; j < charts.getLength(); j++) {
                                Node chart = charts.item(j);
                                Boolean isSignalIndicator = Boolean.parseBoolean(
                                        chart.getAttributes().getNamedItem("isSignalIndicator").getNodeValue());
                                if (isSignalIndicator) {
                                    int signalParentIndex = Integer.parseInt(chart.getAttributes().
                                            getNamedItem("signalParent").getNodeValue());
                                    byte toID = Byte.parseByte(chart.getAttributes().
                                            getNamedItem("toID").getNodeValue());
                                    int thisID = Integer.parseInt(chart.getAttributes().
                                            getNamedItem("SourceID").getNodeValue());
                                    ChartProperties parentCP = indicators.get(signalParentIndex);
                                    parentCP.setSignalParent(true);
                                    (indicators.get(thisID)).setSignalIndicator(true);
                                    (indicators.get(thisID)).setSignalSourceID(parentCP.getID());
                                    switch (toID) {
                                        case GraphDataManager.ID_MACD:
                                            if (indicators.get(thisID) instanceof MovingAverage) {
                                                ((MACD) parentCP).setSignalLine((MovingAverage) indicators.get(thisID));
                                            } else if (indicators.get(thisID) instanceof MACDHistogram) {
                                                ((MACD) parentCP).setHistogram((MACDHistogram) indicators.get(thisID));
                                            }
                                            break;
                                        case GraphDataManager.ID_BOLLINGER_BANDS:
                                            BollingerBand bollingerBand = ((BollingerBand) indicators.get(thisID));
                                            byte bbtype = bollingerBand.getBandType();
                                            if (bbtype == BollingerBand.BT_UPPER) {
                                                ((BollingerBand) parentCP).setUpper(bollingerBand);
                                            } else if (bbtype == BollingerBand.BT_LOWER) {
                                                ((BollingerBand) parentCP).setLower(bollingerBand);
                                            }
                                            break;
                                        case GraphDataManager.ID_KELTNER_CHANNELS:
                                            KeltnerChannels keltnerChannels = ((KeltnerChannels) indicators.get(thisID));
                                            byte kcType = keltnerChannels.getKeltnerChannelType();
                                            if (kcType == KeltnerChannels.KC_UPPER) {
                                                ((KeltnerChannels) parentCP).setUpper(keltnerChannels);
                                            } else if (kcType == KeltnerChannels.KC_LOWER) {
                                                ((KeltnerChannels) parentCP).setLower(keltnerChannels);
                                            }
                                            break;
                                        case GraphDataManager.ID_STOCHAST_OSCILL:
                                            ((StocasticOscillator) parentCP).setSignalLine(
                                                    (MovingAverage) indicators.get(thisID));
                                            break;
//                                            break;
                                        case GraphDataManager.ID_KLINGER_OSCILL:
                                            ((KlingerOscillator) parentCP).setSignalLine(
                                                    (MovingAverage) indicators.get(thisID));
                                            break;
                                        case GraphDataManager.ID_PRICE_CHANNELS:
                                            break;
                                        case GraphDataManager.ID_FO:
                                            ((ForcastOscillator) parentCP).setSignalLine(
                                                    (MovingAverage) indicators.get(thisID));
                                            break;
                                        default:
                                            break;
                                    }
                                }
                            }
                        } catch (NullPointerException e) {
                            //Ignore for backward compatibility
                        }

                        //Loading Highlighted Areas
                        expression = "Template/GraphWindow[" + graphIndex + "]/HighlightedArea";
                        NodeList highlights = (NodeList) xpath.evaluate(expression, document, XPathConstants.NODESET);
                        if ((highlights != null) && (highlights.getLength() > 0)) {
                            ChartProperties topCp, botCp;
                            int topID, botID;
                            Color fillColor;
                            float alpha;
                            graph.GDMgr.getHighLightedSources().clear();
                            for (int j = 0; j < highlights.getLength(); j++) {
                                Node xmlobj = highlights.item(j);
                                topID = Integer.parseInt(xmlobj.getAttributes().getNamedItem("TopCP").getNodeValue());
                                botID = Integer.parseInt(xmlobj.getAttributes().getNamedItem("BottomCP").getNodeValue());
                                topCp = (ChartProperties) graph.GDMgr.getSources().get(topID);
                                botCp = (ChartProperties) graph.GDMgr.getSources().get(botID);
                                fillColor = new Color(Integer.parseInt(xmlobj.getAttributes().getNamedItem("Color").getNodeValue()));
                                alpha = Float.parseFloat(xmlobj.getAttributes().getNamedItem("Alpha").getNodeValue());
                                HighlightedArea ha = new HighlightedArea(topCp, botCp, fillColor, alpha);
                                graph.GDMgr.getHighLightedSources().add(ha);
                            }
                        }
                        //graph.GDMgr.reConstructGraphStore();
                        graph.GDMgr.reloadGraphStoreData();
                        graph.reDrawAll();
                    } else {
                        return false;
                    }

                    if (i > 0) {
                        gf.setBounds(Integer.parseInt(saBounds[0]), Integer.parseInt(saBounds[1]),
                                Integer.parseInt(saBounds[2]), Integer.parseInt(saBounds[3]));
                        //gf.show();
                    }
                }
            } else {
                return false;
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            return false;
        }
        if (!cFrame.isBIsTabbedMode()) {
            //cFrame.tileWindows();   //TODO:CHAIRTH
        }

        return true;
    }

    /* **************************** end of multiple templates  related *******************************/
}

