package com.isi.csvr.chart.indicatorproperties;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: mevana
 * Date: 4/20/11
 * Time: 3:54 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorKeltnerChannelsProperties extends IndicatorFillAreaProperty {

    private int timePeriods;
    private float deviations;

    private final String TIME_PERIODS = "timePeriods";
    private final String DEVIATIONS = "deviations";

    public IndicatorKeltnerChannelsProperties() {
        super();
    }

    public IndicatorKeltnerChannelsProperties(int objectId) {
        super(objectId);
    }

    public int getTimePeriods() {
        return timePeriods;
    }

    public IndicatorDataItem setTimePeriods(int timePeriods) {
        this.timePeriods = timePeriods;
        return new IndicatorDataItem(TIME_PERIODS, timePeriods);
    }

    public float getDeviations() {
        return deviations;
    }

    public IndicatorDataItem setMethod(float deviations) {
        this.deviations = deviations;
        return new IndicatorDataItem(DEVIATIONS, deviations);
    }


    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {
        super.setValues(propertyName, propertyValue);
        if (propertyName.equals(TIME_PERIODS)) {
            this.timePeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(DEVIATIONS)) {
            this.deviations = Float.parseFloat(propertyValue.toString());
        }
    }
}
