package com.isi.csvr.chart.indicatorproperties;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 4, 2009
 * Time: 12:53:57 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorParabolicSARProperties extends IndicatorDefaultProperty {
    private float step;    //:TODO Enter the default values
    private float max;


    private final String STEP = "step";
    private final String MAX = "max";


    public IndicatorParabolicSARProperties() {
        super();
    }

    public IndicatorParabolicSARProperties(int objectId) {
        super(objectId);
    }

    public float getStep() {
        return step;
    }

    public IndicatorDataItem setReversalAmount(float revAmount) {
        this.step = revAmount;
        return new IndicatorDataItem(STEP, revAmount);
    }

    public float getMax() {
        return max;
    }

    public IndicatorDataItem setMethod(float method) {
        this.max = method;
        return new IndicatorDataItem(MAX, method);
    }


    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(STEP)) {
            this.step = Float.parseFloat(propertyValue.toString());
        } else if (propertyName.equals(MAX)) {
            this.max = Float.parseFloat(propertyValue.toString());
        }
    }
}
