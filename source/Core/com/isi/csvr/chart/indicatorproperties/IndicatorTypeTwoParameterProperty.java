package com.isi.csvr.chart.indicatorproperties;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 4, 2009
 * Time: 9:13:47 AM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorTypeTwoParameterProperty extends IndicatorDefaultProperty {

    private int param1 = 14;    //:TODO Enter the default values
    private byte param2 = 1;


    private final String PARAMETER1 = "param1";
    private final String PARAMETER2 = "param2";


    public IndicatorTypeTwoParameterProperty() {
        super();
    }

    public IndicatorTypeTwoParameterProperty(int objectId) {
        super(objectId);
    }

    public int getParam1() {
        return param1;
    }

    public IndicatorDataItem setParam1(int param1) {
        this.param1 = param1;
        return new IndicatorDataItem(PARAMETER1, param1);
    }

    public byte getParam2() {
        return param2;
    }

    public IndicatorDataItem setParam2(byte param2) {
        this.param2 = param2;
        return new IndicatorDataItem(PARAMETER2, param2);
    }


    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(PARAMETER1)) {
            this.param1 = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(PARAMETER2)) {
            this.param2 = Byte.parseByte(propertyValue.toString());
        }
    }
}

