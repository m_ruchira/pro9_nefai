package com.isi.csvr.chart.indicatorproperties;

import java.util.ArrayList;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 3, 2009
 * Time: 5:11:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorTypeThreeParameterProperty extends IndicatorDefaultProperty {

    private int timePeriods = 14;    //:TODO Enter the default values
    private Color lineColor;
    private byte lineStyle2;

    private final String TIME_PERIODS = "timePeriods";
    private final String LINE_COLOR = "lineColor";
    private final String LINE_STYLE_2 = "lineStyle2";

    public IndicatorTypeThreeParameterProperty() {
        super();
    }

    public IndicatorTypeThreeParameterProperty(int objectId) {
        super(objectId);
    }

    public int getTimePeriods() {
        return timePeriods;
    }

    public IndicatorDataItem setTimePeriods(int timePeriods) {
        this.timePeriods = timePeriods;
        return new IndicatorDataItem(TIME_PERIODS, timePeriods);
    }

    public Color getLineColor() {
        return lineColor;
    }

    public IndicatorDataItem setLineColor(Color lineColor) {
        this.lineColor = lineColor;
        return new IndicatorDataItem(LINE_COLOR, lineColor.getRGB());
    }

    public byte getLineStyle2() {
        return lineStyle2;
    }

    public IndicatorDataItem setLineStyle2(byte lineStyle) {
        this.lineStyle2 = lineStyle;
        return new IndicatorDataItem(LINE_STYLE_2, lineStyle);
    }


    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(TIME_PERIODS)) {
            this.timePeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(LINE_COLOR)) {
            this.lineColor = new Color(Integer.parseInt(propertyValue.toString()));
        } else if (propertyName.equals(LINE_STYLE_2)) {
            this.lineStyle2 = Byte.parseByte(propertyValue.toString());
        }
    }
}
