package com.isi.csvr.chart.indicatorproperties;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 4, 2009
 * Time: 11:49:29 AM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorZigZagProperty extends IndicatorDefaultProperty {
    private float reversalAmount;    //:TODO Enter the default values
    private int method;


    private final String REVERSAL_AMOUNT = "reversalAmount";
    private final String METHOD = "method";


    public IndicatorZigZagProperty() {
        super();
    }

    public IndicatorZigZagProperty(int objectId) {
        super(objectId);
    }

    public float getReversalAmount() {
        return reversalAmount;
    }

    public IndicatorDataItem setReversalAmount(float revAmount) {
        this.reversalAmount = revAmount;
        return new IndicatorDataItem(REVERSAL_AMOUNT, revAmount);
    }

    public int getMethod() {
        return method;
    }

    public IndicatorDataItem setMethod(int method) {
        this.method = method;
        return new IndicatorDataItem(METHOD, method);
    }


    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(REVERSAL_AMOUNT)) {
            this.reversalAmount = Float.parseFloat(propertyValue.toString());
        } else if (propertyName.equals(METHOD)) {
            this.method = Byte.parseByte(propertyValue.toString());
        }
    }


//    private ZigZag zz;
//	protected float reversalAmount;
//    protected int method;
//
//    protected final static String REVERSAL_AMOUNT = "reversalAmount";
//
//    protected static final String METHOD = "method";
//
//    //
//    public IndicatorZigZagProperty(int objectID) {
//        super(objectID);
//    }
//
//    public IndicatorZigZagProperty() {
//        super();
//    }
//
//    //
//    public IndicatorZigZagProperty(ChartProperties cP) {
//        this.zz = (ZigZag) cP;
//        this.reversalAmount = zz.getTimePeriods();
//        this.method = zz.getDeviations();
//
//    }
////
//    public ArrayList<ObjectDataItem> getDataItems() {
//
//        ArrayList<IndicatorDataItem> items = new ArrayList<IndicatorDataItem>();
//
//            items.add(op.setObjectID());
//        items.add(op.setUpColor(cp.getColor()));
//        items.add(op.setDownColor(cp.getWarningColor()));
//        items.add(op.setLineStyle(cp.getPenStyle()));
//        items.add(op.setLineThickness(cp.getPenWidth()));
//        items.add(op.setChartStyle(cp.getChartStyle()));
//        items.add(op.setOHLCPriority(cp.getOHLCPriority()));
//
//
//        items.add(new IndicatorDataItem(OBJECT_ID, objectId));
//        items.add(new IndicatorDataItem(UP_COLOR, lineColor.getRGB()));
//                items.add(new IndicatorDataItem(DOWN_COLOR, lineColor.getRGB()));
//                        items.add(new IndicatorDataItem(
//                                items.add(new IndicatorDataItem(
//                                        items.add(new IndicatorDataItem(
//
//
//        Integer[] UIkeys = (Integer[]) table.keySet().toArray(new Integer[0]);
//        Arrays.sort(UIkeys);
//
//        for (int key : UIkeys) {
//            //int rowID = (Integer) ids.nextElement();
//            FibonacciRetracementiLine line = (FibonacciRetracementiLine) table.get(key);
//            String lineItem = "" + line.isEnabled() + ";" + line.getPercentageValue() + ";" +
//                    line.getLineColor().getRGB() + ";" + line.getPenStyle() + ";" + line.getPenWidth();
//            items.add(new ObjectDataItem(String.valueOf(STR_LINE + key), lineItem));
//        }
//
//        return items;
//    }
//
//    public void setTable(Hashtable table) {
//        this.table = table;
//    }
//
//    public Hashtable getTable() {
//        return table;
//    }
//
//    public boolean isUsingSameProperties() {
//        return isUsingSameProperties;
//    }
//
//    public void setUsingSameProperties(boolean usingSameProperties) {
//        isUsingSameProperties = usingSameProperties;
//    }
//
//    public boolean isFreeStyleMode() {
//        return isFreeStyle;
//    }
//
//    public void setFreeStyleMode(boolean free) {
//        isFreeStyle = free;
//    }

}
