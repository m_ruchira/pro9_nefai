package com.isi.csvr.chart.indicatorproperties;

import java.util.ArrayList;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Mevan Alles
 * Date: Aug 10, 2009
 * Time: 10:13:40 AM
 * To change this template use File | Settings | File Templates.
 */
public class IndicatorMACDProperty extends IndicatorDefaultProperty {

    private int MACD_TimePeriods_High;
    private int MACD_TimePeriods_Low;
    private int signalTimePeriods;
    private byte signalStyle;
    private Color signalColor;

    private final String MACD_TIME_HIGH = "MACD_TimePeriods_High";
    private final String MACD_TIME_LOW = "MACD_TimePeriods_Low";
    private final String SIGNAL_TIME_PERIODS = "signalTimePeriods";
    private final String SIGNAL_STYLE = "signalStyle";
    private final String SIGNAL_COLOR = "signalColor";


    public IndicatorMACDProperty() {
        super();
    }

    public IndicatorMACDProperty(int objectId) {
        super(objectId);
    }

    public int getMACD_TimePeriods_High() {
        return MACD_TimePeriods_High;
    }

    public IndicatorDataItem setMACD_TimePeriods_High(int timePeriods) {
        this.MACD_TimePeriods_High = timePeriods;
        return new IndicatorDataItem(MACD_TIME_HIGH, timePeriods);
    }

    public int getMACD_TimePeriods_Low() {
        return MACD_TimePeriods_Low;
    }

    public IndicatorDataItem setMACD_TimePeriods_Low(int sp) {
        this.MACD_TimePeriods_Low = sp;
        return new IndicatorDataItem(MACD_TIME_LOW, sp);
    }

    public int getSignalTimePeriods() {
        return signalTimePeriods;
    }

    public IndicatorDataItem setSignalTimePeriods(int dsp) {
        this.signalTimePeriods = dsp;
        return new IndicatorDataItem(SIGNAL_TIME_PERIODS, dsp);
    }

    public byte getSignalStyle() {
        return signalStyle;
    }

    public IndicatorDataItem setSignalStyle(byte sp) {
        this.signalStyle = sp;
        return new IndicatorDataItem(SIGNAL_STYLE, sp);
    }

    public Color getSignalColor() {
        return signalColor;
    }

    public IndicatorDataItem setSignalColor(Color sc) {
        this.signalColor = sc;
        return new IndicatorDataItem(SIGNAL_COLOR, sc.getRGB());
    }

    protected void assignvaluesFrom(ArrayList<IndicatorDataItem> items) {
        super.assignvaluesFrom(items);
        for (int i = 0; i < items.size(); i++) {
            IndicatorDataItem item = items.get(i);
            setValues(item.getName(), item.getValue());
        }
    }

    public void setValues(String propertyName, Object propertyValue) {

        if (propertyName.equals(MACD_TIME_HIGH)) {
            this.MACD_TimePeriods_High = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(MACD_TIME_LOW)) {
            this.MACD_TimePeriods_Low = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(SIGNAL_TIME_PERIODS)) {
            this.signalTimePeriods = Integer.parseInt(propertyValue.toString());
        } else if (propertyName.equals(SIGNAL_STYLE)) {
            this.signalStyle = Byte.parseByte(propertyValue.toString());
        } else if (propertyName.equals(SIGNAL_COLOR)) {
            this.signalColor = new Color(Integer.parseInt(propertyValue.toString()));
        }
    }
}
