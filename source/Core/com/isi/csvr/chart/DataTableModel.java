package com.isi.csvr.chart;

import com.isi.csvr.ChartInterface;
import com.isi.csvr.customizer.CustomizerRecord;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.CommonTable;
import com.isi.csvr.table.CommonTableInterface;
import com.isi.csvr.theme.Theme;

import javax.swing.event.TableModelListener;
import javax.swing.table.TableModel;

/**
 * User: Uditha Nagahawatta
 * Date: Aug 28, 2003
 * Time: 11:57:39 AM
 */
public class DataTableModel extends CommonTable implements TableModel, CommonTableInterface {
    private GraphFrame frame;
    private String exchange;
    private GraphDataManager gdm;
    int noOfIndicators = 0;
    TWDecimalFormat indicatorFormatter = null;

    public DataTableModel(GraphFrame frame) {
        this.frame = frame;
    }


    public int getRowCount() {
        try {
            if (gdm == null) {
                return 0;
            } else {
                return gdm.getLastNotNullIndexOfBaseSymbolPoints() + 1;
            }
        } catch (Exception e) {
            return 0;
        }
    }

    public int getColumnCount() {
        /* if (getViewSettings() == null) {
            return 0;
        } else if (gdm == null) {
            return getViewSettings().getColumnHeadings().length;
        } else {
            //if (noOfIndicators != gdm.getIndicators().size()) {
            if (true) {
                int viewHeadingLength = getViewSettings().getColumnHeadings().length;
                String[] colHeadings = new String[(viewHeadingLength - noOfIndicators)];
                for (int j = 0; j < (viewHeadingLength - noOfIndicators); j++) {
                    colHeadings[j] = getViewSettings().getColumnHeadings()[j];
                }
                getViewSettings().setColumnHeadings(colHeadings);
                colHeadings = null;

                noOfIndicators = gdm.getIndicators().size();
                viewHeadingLength = getViewSettings().getColumnHeadings().length;
                colHeadings = new String[(viewHeadingLength + noOfIndicators)];
                for (int j = 0; j < viewHeadingLength; j++) {
                    colHeadings[j] = getViewSettings().getColumnHeadings()[j];
                }
                for (int i = 0; i < noOfIndicators; i++) {
                   // System.out.println("=========== table ========== " + ((Indicator) gdm.getIndicators().get(i)).getTableColumnHeading());
                    colHeadings[viewHeadingLength + i] = ((Indicator) gdm.getIndicators().get(i)).getTableColumnHeading();
                }
                getViewSettings().setColumnHeadings(colHeadings);
                colHeadings = null;
                return (viewHeadingLength + noOfIndicators);
            } else {
                return getViewSettings().getColumnHeadings().length;
            }

        }*/
        if (getViewSettings() == null) {
            return 0;
        } else if (gdm == null) {
            return getViewSettings().getColumnHeadings().length;
        } else {
            //if (noOfIndicators != gdm.getIndicators().size()) {
            //if (true) {
            int viewHeadingLength = getViewSettings().getColumnHeadings().length;
            String[] colHeadings = new String[(viewHeadingLength - noOfIndicators)];
            for (int j = 0; j < (viewHeadingLength - noOfIndicators); j++) {
                colHeadings[j] = getViewSettings().getColumnHeadings()[j];
            }
            getViewSettings().setColumnHeadings(colHeadings);
            colHeadings = null;

            noOfIndicators = gdm.getIndicators().size();

            viewHeadingLength = getViewSettings().getColumnHeadings().length;
            colHeadings = new String[(viewHeadingLength + noOfIndicators)];
            for (int j = 0; j < viewHeadingLength; j++) {
                colHeadings[j] = getViewSettings().getColumnHeadings()[j];
            }
            for (int i = 0; i < noOfIndicators; i++) {
                // System.out.println("=========== table ========== " + ((Indicator) gdm.getIndicators().get(i)).getTableColumnHeading());
                colHeadings[viewHeadingLength + i] = ((Indicator) gdm.getIndicators().get(i)).getTableColumnHeading();
            }
            getViewSettings().setColumnHeadings(colHeadings);
            colHeadings = null;
            return (viewHeadingLength + noOfIndicators);
            //} else {
            //   return getViewSettings().getColumnHeadings().length;
            //}

        }
    }

    public String getColumnName(int columnIndex) {
        try {
            //System.out.println("=====column Index ==== "+columnIndex+ " ====== name ===== "+getViewSettings().getColumnHeadings()[columnIndex]);
            return getViewSettings().getColumnHeadings()[columnIndex];
        } catch (ArrayIndexOutOfBoundsException e) {
            //System.out.println("=====column Index ==== "+columnIndex+ " ====== name ===== "+((Indicator) gdm.getIndicators().get(columnIndex - 9)).getTableColumnHeading());
            return ((Indicator) gdm.getIndicators().get(columnIndex - 9)).getTableColumnHeading();
        }
    }

    public Class getColumnClass(int col) {
        if (col == 0) {
            return Long.class;
        } else if (col == 5) {
            return Long.class;
        } else if (col == 6) {
            return Double.class;
        } else {
            return Float.class;
        }
    }

    public boolean isCellEditable(int rowIndex, int columnIndex) {
        return false;
    }

    public Object getValueAt(int rowIndex, int columnIndex) {
        try {
            int offset;
            ChartRecord record = null;

            record = gdm.getChartRecordForTable(getRowCount() - rowIndex - 1, 0); //0 is for base graph
            if (record != null) {
                switch (columnIndex) {
                    case -1:
                        return  gdm;
                   case -2:
                       return frame;
                    case 0:
                        return record.Time;
                    //return new Long((record.getTime()*60000) + ChartInterface.getTimeZoneAdjustment(frame.getGraph().Symbol, record.getTime()*60000)); //Bug id <#0010>
                    case 1:
                        return new Float(record.Open);
                    case 2:
                        return new Float(record.High);
                    case 3:
                        return new Float(record.Low);
                    case 4:
                        return new Float(record.Close);
                    case 5:
                        return (long) record.Volume;
                    case 6:
                        return new Double(record.TurnOver);
                    case 7:
                        return new Float(getNetChg(getRowCount() - 1 - rowIndex, (float) record.Close));
                    case 8:
                        return new Float(getPctChg(getRowCount() - 1 - rowIndex, (float) record.Close));
                    default:
                        ChartProperties cp = ((ChartProperties) gdm.getIndicators().get(columnIndex - 9));
                        int index = gdm.getIndexForSourceCP(cp);
                        record = gdm.getChartRecordForTable(getRowCount() - rowIndex - 1, index); //TODO

                        //int panelIndex = gdm.getIndexOfTheRect(frame.graph.panels, cp.getRect());
                        //double minMaxDiff = gdm.getMinMaxDifference(panelIndex);

                        if (record != null) {
                            //indicatorFormatter = getFormatter(minMaxDiff);
                           // return Double.parseDouble(indicatorFormatter.format(record.Open));
                            return record.Open;
                        } else {
                            return Double.MAX_VALUE;
                        }
                }
            } else {
                return 0;
            }
        } catch (Exception ex) {
            return 0;
        }
    }

    private TWDecimalFormat getFormatter(double minMaxDiff) {

        TWDecimalFormat formatter;

        if (minMaxDiff < 0.000000009d) {
            formatter = new TWDecimalFormat("##0.0000000000");
        } else if (minMaxDiff < 0.00000009d) {
            formatter = new TWDecimalFormat("##0.000000000");
        } else if (minMaxDiff < 0.0000009d) {
            formatter = new TWDecimalFormat("##0.00000000");
        } else if (minMaxDiff < 0.000009d) {
            formatter = new TWDecimalFormat("##0.0000000");
        } else if (minMaxDiff < 0.00009d) {
            formatter = new TWDecimalFormat("##0.000000");
        } else if (minMaxDiff < 0.0009d) {
            formatter = new TWDecimalFormat("##0.00000");
        } else if (minMaxDiff < 0.009d) {
            formatter = new TWDecimalFormat("##0.0000");
        } else if (minMaxDiff < 0.09d) {
            formatter = new TWDecimalFormat("##0.000");
        } else if (minMaxDiff < 0.9d) {
            formatter = new TWDecimalFormat("##0.00");
        } else {
            formatter = new TWDecimalFormat("###########0.00");
        }

        return formatter;
    }

    //No usagages found in this method - sathyajith
    public boolean isCurrentDataAvalable() {
        Stock stock = ChartInterface.getStockObject(frame.getGraph().getGDMgr().getBaseGraph());
//        if ((stock != null) && (stock.getLastTradeValue() != 0)){ //Bug id <#0010>
        try {
            if (stock != null) {
                if (stock.getVolume() == 0) {
                    stock = null;
                    return false;
                } else { // must not allow to duplicate the last record (from history and intraday last data point)
                    ChartRecord record = gdm.getChartRecordForTable(gdm.getLastNotNullIndexOfBaseSymbolPoints(), 0);
                    if (record == null) return false;
                    long time = record.Time;
                    long stockDate = ((stock.getLastTradeTime() / 86400000) * 1440); // get date up to the minute
                    if ((stockDate == 0) || (time == stockDate)) { // check if dates are the same
                        stock = null;
                        return false;
                    } else {
                        stock = null;
                        return true;
                    }
                }
            } else {
                stock = null;
                return false;
            }
        } catch (Exception e) {
            stock = null;
            e.printStackTrace();
            return false;
        }
    }

    private Object getCurrentDayData(int columnIndex) {
        Stock stock = ChartInterface.getStockObject(frame.getGraph().getGDMgr().getBaseGraph());
        if (stock != null) {
            try {
                switch (columnIndex) {
                    case 0:
//                        return new Long(((stock.getLastTradeTime() / 86400000) * 86400000) + ChartInterface.getDataTimeZoneAdjustment(frame.getGraph().Symbol, stock.getLastTradeTime(),frame.CurrentMode));
                        return new Long(((stock.getLastTradeTime() / 86400000) * 86400000) + ChartInterface.getTimeZoneAdjustment(frame.getGraph().Symbol, stock.getLastTradeTime(), frame.CurrentMode));
//                        return new Long(stock.getLastTradeTime() + stock.getTimeOffset());  //Bug id <#0010>
                    case 1:
                        return new Float(stock.getTodaysOpen());
                    case 2:
                        return new Float(stock.getHigh());
                    case 3:
                        return new Float(stock.getLow());
                    case 4:
                        return new Float(stock.getLastTradeValue());
                    case 5:
                        return new Long(stock.getVolume());
                    case 6:
                        return new Float(stock.getChange());
                    case 7:
                        return new Float(stock.getPercentChange());
                    default:
                        ChartProperties cp = ((ChartProperties) gdm.getIndicators().get(columnIndex - 8));
                        int index = gdm.getIndexForSourceCP(cp);
                        ChartRecord record = gdm.getChartRecordForTable(getRowCount() - 1, index); //TODO
                        return new Float(record.Open);
                }
            } catch (Exception e) {
                //e.printStackTrace();
                return "";
            }
        } else {
            return "N/A";
        }
    }

    private float getNetChg(int row, float close) {
        float value;
        try {
            if (row == 0) { //period begin day
                Object[] preRecord = gdm.getPrePeriodRecord();
                if (preRecord[1] != null) {
                    ChartPoint preChartPoint = (ChartPoint) preRecord[1];
                    value = close - (float) preChartPoint.Close;
                } else {
                    value = close - 0;
                }
            } else {
                ChartPoint prevDay = gdm.getChartRecordForTable(row - 1, 0);
                value = close - (float) prevDay.Close;
                prevDay = null;

            }

            return value;
        } catch (Exception e) {
            return 0;
        }

    }

    private float getPctChg(int row, float close) {
        float value;

        try {
            if (row == 0) { //period begin day
                Object[] prePeriodRecord = gdm.getPrePeriodRecord();
                if (prePeriodRecord[1] != null) {
                    ChartPoint prePeriodDay = (ChartPoint) prePeriodRecord[1];
                    if (prePeriodDay.Close != 0) {
                        value = close - (float) prePeriodDay.Close;
                        value = (value * 100) / (float) prePeriodDay.Close;
                    } else {
                        value = 0;// actually Not a Number
                    }
                } else {
                    value = 0;
                }
            } else {
                ChartPoint prevDay = gdm.getChartRecordForTable(row - 1, 0);
                if (prevDay.Close != 0) {
                    value = close - (float) prevDay.Close;
                    value = (value * 100)/ (float) prevDay.Close;
                } else {
                    value = 0; //actually Not a number
                }
                prevDay = null;
            }

            return value;
        } catch (Exception e) {
            return 0;
        }

    }
//    private float getPctChg(int row, float close) {
//        try {
//            ChartPoint prevDay = gdm.getChartRecordForTable(row - 1, 0);
//            float value = close - (float) prevDay.Close;
//            value = (value * 100) / (float) prevDay.Close;
//            prevDay = null;
//            return value;
//        } catch (Exception e) {
//            return 0;
//        }
//
//    }

    public void setSymbol(String symbol) {
    }

    public void setValueAt(Object aValue, int rowIndex, int columnIndex) {
    }

    public void addTableModelListener(TableModelListener l) {
    }

    public void removeTableModelListener(TableModelListener l) {
    }

    public void setStore(DynamicArray store, String key, GraphDataManager gdm) {
        this.exchange = SharedMethods.getSymbolFromKey(key);
        this.gdm = gdm;
    }

    public CustomizerRecord[] getCustomizerRecords() {
        CustomizerRecord[] baseRecs = super.getCustomizerRecords();
        CustomizerRecord[] customizerRecords = new CustomizerRecord[6];
        System.arraycopy(baseRecs, 0, customizerRecords, 0, 4);
        customizerRecords[4] = new CustomizerRecord(Language.getString("POSITIVE_NET_CHANGE"), FIELD_POSITIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR"));
        customizerRecords[5] = new CustomizerRecord(Language.getString("NEGATIVE_NET_CHANGE"), FIELD_NEGATIVE_CHANGE_ROW, null, Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR"));
        return customizerRecords;
    }

    public GraphFrame getFrame() {
        return frame;
    }
}
