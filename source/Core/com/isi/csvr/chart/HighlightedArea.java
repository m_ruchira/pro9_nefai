package com.isi.csvr.chart;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jun 8, 2009
 * Time: 12:27:55 PM
 * To change this template use File | Settings | File Templates.
 */
public class HighlightedArea {

    private ChartProperties topCP;
    private ChartProperties bottomCP;
    private Color fillColor;
    private float alpha = 0.1f;

    public HighlightedArea(ChartProperties topCP, ChartProperties bottomCP, Color fillColor, float alpha) {
        this.topCP = topCP;
        this.bottomCP = bottomCP;
        this.fillColor = fillColor;
        this.alpha = alpha;
    }

    public HighlightedArea(ChartProperties topCP, ChartProperties bottomCP, Color fillColor) {
        this.topCP = topCP;
        this.bottomCP = bottomCP;
        this.fillColor = fillColor;
    }

    public HighlightedArea( Color fillColor) {
        this.fillColor = fillColor;
    }

    public ChartProperties getTopCP() {
        return topCP;
    }

    public void setTopCP(ChartProperties topCP) {
        this.topCP = topCP;
    }

    public ChartProperties getBottomCP() {
        return bottomCP;
    }

    public void setBottomCP(ChartProperties bottomCP) {
        this.bottomCP = bottomCP;
    }

    public Color getFillColor() {
        return fillColor;
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    public float getAlpha() {
        return alpha;
    }

    public void setAlpha(float alpha) {
        this.alpha = alpha;
    }

    public Color getTransparentColor() {
        return new Color(fillColor.getRed(), fillColor.getGreen(), fillColor.getBlue(), Math.round(alpha * 255));
    }
}
