package com.isi.csvr.chart;

import com.isi.csvr.shared.Language;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;

import java.util.ArrayList;
import java.awt.*;

/**
 * User: Pramoda
 * Date: Mar 30, 2006
 * Time: 4:28:51 PM
 */
public class DEMA extends ChartProperties implements Indicator {
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    private static final long serialVersionUID = UID_DEMA;

    public DEMA(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_DEMA") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_DEMA");
        this.timePeriods = 5;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get( anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public void assignDefaultValues(){
        super.assignDefaultValues();
        this.timePeriods = 5;        
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof DEMA) {
            DEMA dema = (DEMA) cp;
            this.timePeriods = dema.timePeriods;
            this.innerSource = dema.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_DEMA") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        if (innerSource instanceof Indicator) {
            return ((Indicator) innerSource).hasItsOwnScale();
        }
        return false;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    // this calculation includes 2 intermediate steps EMA, EMA of EMA
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        long entryTime = System.currentTimeMillis();

        ChartRecord cr;
        //setting step size 2
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(2);
        }
        // steps involved
        byte stepEMA = ChartRecord.STEP_1;
        byte stepEMAofEMA = ChartRecord.STEP_2;
        // stepEMA is smoothen only once
        MovingAverage.getMovingAverage(al, 0, timePeriods, MovingAverage.METHOD_EXPONENTIAL, getOHLCPriority(), stepEMA);
        // stepEMAofEMA is smoothen twice
        MovingAverage.getMovingAverage(al, timePeriods - 1, timePeriods, MovingAverage.METHOD_EXPONENTIAL, stepEMA, stepEMAofEMA);

        int loopbegin = timePeriods * 2 - 2;
        for (int i = 0; i < loopbegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }

        for (int i = loopbegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(cr.getStepValue(stepEMA) * 2f - cr.getStepValue(stepEMAofEMA));
            }
        }

        //System.out.println("**** DEMA Calc time " + (entryTime - System.currentTimeMillis()));
    }
    //############################################

    public String getShortName() {
        return Language.getString("IND_DEMA");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods, byte step1, byte step2, byte srcIndex, byte destIndex) {
        ChartRecord cr;
        int startIndex = bIndex + 2 * t_im_ePeriods - 2;
        if (al.size() < startIndex || t_im_ePeriods == 0) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        byte stepEMA = step1;
        byte stepEMAofEMA = step2;
        MovingAverage.getMovingAverage(al, bIndex, t_im_ePeriods, MovingAverage.METHOD_EXPONENTIAL, srcIndex, stepEMA);
        MovingAverage.getMovingAverage(al, bIndex + t_im_ePeriods - 1, t_im_ePeriods, MovingAverage.METHOD_EXPONENTIAL, stepEMA, stepEMAofEMA);

        for (int i = 0; i < startIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }

        for (int i = startIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, 2 * cr.getValue(stepEMA) - cr.getValue(stepEMAofEMA));
        }
    }

    public static int getAuxStepCount() {
        return 2;
    }

}
