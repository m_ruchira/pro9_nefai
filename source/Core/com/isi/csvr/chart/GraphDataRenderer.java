package com.isi.csvr.chart;


// Copyright (c) 2000 Integrated Systems International (ISI)

import com.isi.csvr.TableSorter;
import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.Settings;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.table.ExchangeFormatInterface;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWTableRenderer;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.TableThemeSettings;

import javax.swing.*;
import javax.swing.border.Border;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;
import java.awt.*;
import java.text.SimpleDateFormat;
import java.util.Date;

public class GraphDataRenderer implements TableCellRenderer, TWTableRenderer {

    String[] g_asColumns;
    int[] g_asRendIDs;
    boolean g_bLTR;
    int g_iStringAlign;
    int g_iNumberAlign;
    SimpleDateFormat g_oDateFormat = new SimpleDateFormat(" yyyy/MM/dd ");
    SimpleDateFormat g_oDateTimeFormat = new SimpleDateFormat(" dd/MM/yyyy HH:mm ");
    SimpleDateFormat g_oTimeFormat = new SimpleDateFormat(" HH:mm:ss ");

    String g_sNA = "NA";

    static Color g_oUpFGColor;
    static Color g_oDownFGColor;
    static Color g_oUpBGColor;
    static Color g_oDownBGColor;
    static Color g_oUpColor;
    static Color g_oDownColor;
    static Color g_oSelectedFG;
    static Color g_oSelectedBG;
    static Color g_oFG1;
    static Color g_oBG1;
    static Color g_oFG2;
    static Color g_oBG2;
    Color upColor;
    Color downColor;

    TWDecimalFormat oPriceFormat = new TWDecimalFormat(" ###,##0.00  ");
    //    TWDecimalFormat oPriceEnhFormat = new TWDecimalFormat(" ###,##0.0000  ");
    TWDecimalFormat oQuantityFormat = new TWDecimalFormat(" ###,##0  ");
    //    TWDecimalFormat oChangeFormat = new TWDecimalFormat(" ###,##0.000  ");
    TWDecimalFormat oPChangeFormat = new TWDecimalFormat(" ###,##0.00  ");
    TWDecimalFormat oPDecimalFormat = new TWDecimalFormat(" ###,##0.00000");

    private float floatValue;
    //private float[] floatValuePair;
    private long longValue;
    private double doubleValue;

    private int intValue;
    private Date date;
    private Color foreground = null;
    private Color background = null;
    private String columnName = null;

    private static ImageIcon upImage = null;
    private static ImageIcon downImage = null;
    private byte decimalPlaces = Constants.TWO_DECIMAL_PLACES;
    private static Border cellBorder;
    private static Border themeCellBorder;

    public GraphDataRenderer(String[] asColumns, int[] asRendIDs) {

        g_asColumns = asColumns;
        g_asRendIDs = asRendIDs;
        g_bLTR = Language.isLTR();

        date = new Date();
        reload();
        try {
            g_sNA = " " + Language.getString("NA") + " ";
        } catch (Exception e) {
        }

        if (Language.isLTR()) {
            g_iStringAlign = JLabel.LEFT;
            g_iNumberAlign = JLabel.RIGHT;
        } else {
            g_iStringAlign = JLabel.RIGHT;
            g_iNumberAlign = JLabel.LEFT;
        }

        try {
            if (upImage != null) {
                upImage = new ImageIcon("images/common/up.gif");
            }

            if (downImage != null) {
                downImage = new ImageIcon("images/common/down.gif");
            }
        }
        catch (Exception ex) {
        }
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
    }

    public static void reload() {
        try {
            g_oUpFGColor = Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR");
            g_oDownFGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR");
            g_oUpBGColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownBGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oUpColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("BOARD_TABLE_CELL_BGCOLOR2");


            Color gridColor = Theme.getOptionalColor("BOARD_TABLE_GRID_COLOR");
            int gridStyle;
            if (gridColor != null) {
                gridStyle = Theme.getInt("BOARD_TABLE_GRID_STYLE", TableThemeSettings.GRID_NONE);
            } else {
                gridStyle = TableThemeSettings.GRID_NONE;
            }

            switch (gridStyle) {
                case TableThemeSettings.GRID_NONE:
                    themeCellBorder = null;
                    break;
                case TableThemeSettings.GRID_HORIZONTAL:
                    themeCellBorder = BorderFactory.createMatteBorder(0, 0, 1, 0, gridColor);
                    break;
                case TableThemeSettings.GRID_VERTICAL:
                    if (Language.isLTR()) {
                        themeCellBorder = BorderFactory.createMatteBorder(0, 0, 0, 1, gridColor);
                    } else {
                        themeCellBorder = BorderFactory.createMatteBorder(0, 1, 0, 0, gridColor);
                    }
                    break;
                case TableThemeSettings.GRID_BOTH:
                    if (Language.isLTR()) {
                        themeCellBorder = BorderFactory.createMatteBorder(0, 0, 1, 1, gridColor);
                    } else {
                        themeCellBorder = BorderFactory.createMatteBorder(0, 1, 1, 0, gridColor);
                    }
                    break;
            }


        } catch (Exception e) {
            g_oUpBGColor = Color.green;
            g_oDownBGColor = Color.red;
            g_oUpFGColor = Color.black;
            g_oDownFGColor = Color.black;
            g_oUpColor = Color.green;
            g_oDownColor = Color.red;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public void propertyChanged(int property) {
    }

    public static final DefaultTableCellRenderer DEFAULT_RENDERER =
            new DefaultTableCellRenderer();

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        Component renderer = DEFAULT_RENDERER.getTableCellRendererComponent(table, value,
                isSelected, hasFocus, row, column);

        /*if (decimalPlaces != ((SmartTable)table).getDecimalPlaces()){
            applyDecimalPlaces(((SmartTable)table).getDecimalPlaces());
        }*/

        JLabel lblRenderer = (JLabel) renderer;
        lblRenderer.setOpaque(true);

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
            cellBorder = sett.getCellBorder();
            upColor = sett.getPositiveChangeFG();
            downColor = sett.getNegativeChangeFG();

        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
            upColor = g_oUpColor;
            downColor = g_oDownColor;
            cellBorder = themeCellBorder;
        }

        renderer.setForeground(foreground);
        renderer.setBackground(background);


        columnName = table.getColumnName(column);
        int iRendID = 'I';
        try {
            iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        } catch (Exception e) {
        }

        if (decimalPlaces != ((ExchangeFormatInterface) ((TableSorter) table.getModel()).getModel()).getDecimalCount()) {
            applyDecimalPlaces(((ExchangeFormatInterface) ((TableSorter) table.getModel()).getModel()).getDecimalCount());
        }
        try {

            lblRenderer.setIcon(null);
            switch (iRendID) {

                case 3: // PRICE
                    floatValue = toFloatValue(value);
                    if (floatValue == Double.POSITIVE_INFINITY || floatValue < 0) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText(oPriceFormat.format(floatValue));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    }
                    break;
                case 'I': // INDICATORS
                    TableModel tableModel = table.getModel();
                    GraphDataManager gdm = (GraphDataManager) tableModel.getValueAt(row, -1);
                    if (gdm != null) {
                        ChartProperties cp = ((ChartProperties) gdm.getIndicators().get(column - 9));
                        int index = gdm.getIndexForSourceCP(cp);
                        ChartRecord record = gdm.getChartRecordForTable(tableModel.getRowCount() - row - 1, index);

                        GraphFrame frame = (GraphFrame) tableModel.getValueAt(row, -2);
                        int panelIndex = gdm.getIndexOfTheRect(frame.graph.panels, cp.getRect());
                        double minMaxDiff = gdm.getMinMaxDifference(panelIndex);

                        if (record != null) {
                            TWDecimalFormat indicatorFormatter = getFormatter(minMaxDiff);                              
                    doubleValue = toDoubleValue(value);
                    if (doubleValue == Double.MAX_VALUE || doubleValue == Double.NaN) {
                        lblRenderer.setText(g_sNA);
                    } else {
                                //lblRenderer.setText(String.valueOf(value));
                                lblRenderer.setText(indicatorFormatter.format(doubleValue));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    }
                        } else {
                            lblRenderer.setText(g_sNA);
                        }
                    } else {
                        lblRenderer.setText(g_sNA);
                    }
                    break;

                case 4: // QUANTITY
                    longValue = toLongValue(value);
                    if (longValue <= 0) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText(oQuantityFormat.format(longValue));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    }
                    break;
                case 5: // TurnOver
                    doubleValue = toDoubleValue(value);
                    if (doubleValue == Double.MAX_VALUE) {
                        lblRenderer.setText("");
                    } else if (doubleValue <= 0) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText(oQuantityFormat.format(doubleValue));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    }
                    break;
                case 'V': // QUANTITY
                    longValue = toLongValue(value);
                    if (longValue <= 0) {
                        lblRenderer.setText(g_sNA);
                    } else {
                        lblRenderer.setText(oQuantityFormat.format(longValue));
                    }
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;
                case 8: // DATE TIME
                    longValue = toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue + Settings.getTimeOffset(longValue));
                        lblRenderer.setText(g_oDateTimeFormat.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(JLabel.CENTER); // g_iNumberAlign);
                    break;
                case 7: // DATE TIME
                    longValue = toLongValue(value);
                    if (longValue == 0)
                        lblRenderer.setText(g_sNA);
                    else {
                        date.setTime(longValue + Settings.getTimeOffset(longValue));
                        lblRenderer.setText(g_oDateFormat.format(date));
                    }
                    lblRenderer.setHorizontalAlignment(JLabel.CENTER); // g_iNumberAlign);
                    break;
                case 'C': // % CHANGE
                    //adPrice = (float[])value;
                    floatValue = toFloatValue(value);
                    if (floatValue == Double.POSITIVE_INFINITY) {
                        lblRenderer.setText("");
                    } else {
                        //lblRenderer.setText(oPriceFormat.format(floatValue));
                        floatValue = Math.round((floatValue * 100)) / 100.0f;   // to fix the bug. showing -0.00 values in %change column
                        //System.out.println("===== float value ===== "+floatValue);
                        lblRenderer.setText(oPChangeFormat.format(floatValue));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        if (floatValue > 0)
                            lblRenderer.setForeground(upColor);
                        else if (floatValue < 0)
                            lblRenderer.setForeground(downColor);
                    }
                    break;
                case 'c': // CHANGE
                    //adPrice = (float[])value;
                    //floatValue = Math.round((floatValue * 100)) / 100.0f;
                    floatValue = toFloatValue(value);
                    if (floatValue == Double.POSITIVE_INFINITY) {
                        lblRenderer.setText("");
                    } else {
                        //floatValue = Math.round((floatValue * 100)) / 100.0f;   // to fix the bug. showing -0.00 values in %change column
                        lblRenderer.setText(oPriceFormat.format(floatValue));
                        lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                        if (floatValue > 0)
                            lblRenderer.setForeground(upColor);
                        else if (floatValue < 0)
                            lblRenderer.setForeground(downColor);
                    }
                    break;
                default:
                    lblRenderer.setText("");
            }
        } catch (Exception e) {
            lblRenderer.setText("");
        }
        lblRenderer.setBorder(cellBorder);
        foreground = null;
        background = null;
        lblRenderer = null;

        return renderer;
    }

    private long toLongValue(Object oValue) throws Exception {
        try {
            return (Long) oValue;
        } catch (Exception e) {
            return Long.MAX_VALUE;
        }
    }

    private double toDoubleValue(Object oValue) throws Exception {
        try {
            return (Double) oValue;
        } catch (Exception e) {
            return Double.MAX_VALUE;
        }
    }

    private float toFloatValue(Object oValue) throws Exception {
        try {
            return (Float) oValue;
        } catch (Exception e) {
            return Float.POSITIVE_INFINITY;
        }
    }

    private void applyDecimalPlaces(byte decimalPlaces) {
        switch (decimalPlaces) {
            case Constants.ONE_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL);
                break;
            case Constants.TWO_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL);
                break;
            case Constants.THREE_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL);
                break;
            case Constants.FOUR_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL);
                break;
            case Constants.FIVE_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL);
                break;
            case Constants.SIX_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL);
                break;
            case Constants.SEVEN_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_SEVEN_DECIMAL);
                break;
            case Constants.EIGHT_DECIMAL_PLACES:
                oPriceFormat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL);
                break;
            case Constants.ONE_DECIMAL_PLACES_NOZERO:
                oPriceFormat.applyPattern(Constants.PATTERN_ONE_DECIMAL_NOZERO);
                break;
            case Constants.TWO_DECIMAL_PLACES_NOZERO:
                oPriceFormat.applyPattern(Constants.PATTERN_TWO_DECIMAL_NOZERO);
                break;
            case Constants.THREE_DECIMAL_PLACES_NOZERO:
                oPriceFormat.applyPattern(Constants.PATTERN_THREE_DECIMAL_NOZERO);
                break;
            case Constants.FOUR_DECIMAL_PLACES_NOZERO:
                oPriceFormat.applyPattern(Constants.PATTERN_FOUR_DECIMAL_NOZERO);
                break;
            case Constants.FIVE_DECIMAL_PLACES_NOZERO:
                oPriceFormat.applyPattern(Constants.PATTERN_FIVE_DECIMAL_NOZERO);
                break;
            case Constants.SIX_DECIMAL_PLACES_NOZERO:
                oPriceFormat.applyPattern(Constants.PATTERN_SIX_DECIMAL_NOZERO);
                break;
            case Constants.SEVEN_DECIMAL_PLACES_NOZERO:
                oPriceFormat.applyPattern(Constants.PATTERN_SEVEN_DECIMAL_NOZERO);
                break;
            case Constants.EIGHT_DECIMAL_PLACES_NOZERO:
                oPriceFormat.applyPattern(Constants.PATTERN_EIGHT_DECIMAL_NOZERO);
                break;
        }
        this.decimalPlaces = decimalPlaces;
    }

    private TWDecimalFormat getFormatter(double minMaxDiff) {

        TWDecimalFormat formatter;

        if (minMaxDiff < 0.000000009d) {
            formatter = new TWDecimalFormat("##0.0000000000");
        } else if (minMaxDiff < 0.00000009d) {
            formatter = new TWDecimalFormat("##0.000000000");
        } else if (minMaxDiff < 0.0000009d) {
            formatter = new TWDecimalFormat("##0.00000000");
        } else if (minMaxDiff < 0.000009d) {
            formatter = new TWDecimalFormat("##0.0000000");
        } else if (minMaxDiff < 0.00009d) {
            formatter = new TWDecimalFormat("##0.000000");
        } else if (minMaxDiff < 0.0009d) {
            formatter = new TWDecimalFormat("##0.00000");
        } else if (minMaxDiff < 0.009d) {
            formatter = new TWDecimalFormat("##0.0000");
        } else if (minMaxDiff < 0.09d) {
            formatter = new TWDecimalFormat("##0.000");
        } else if (minMaxDiff < 0.9d) {
            formatter = new TWDecimalFormat("##0.00");
        } else {
            formatter = new TWDecimalFormat("###,###,###,##0.00");
        }

        return formatter;
    }


}

//1049|OUTER_CHART|0|1023|0|Arial,0,14|-1,0,650,400,2,0,0|83333V5cC|WINDOW_TITLE_GRAPH|GRAPH_DATA_COLS