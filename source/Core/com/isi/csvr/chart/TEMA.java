package com.isi.csvr.chart;

import com.isi.csvr.shared.Language;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;

import java.util.ArrayList;
import java.awt.*;

/**
 * User: Pramoda
 * Date: May 31, 2006
 * Time: 12:09:23 PM
 */
public class TEMA extends ChartProperties implements Indicator {
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    private static final long serialVersionUID = UID_TEMA;

    public TEMA(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_TEMA") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_TEMA");
        this.timePeriods = 21;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get( anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public void assignDefaultValues(){
        super.assignDefaultValues();
        this.timePeriods = 21;        
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof TEMA) {
            TEMA tema = (TEMA) cp;
            this.timePeriods = tema.timePeriods;
            this.innerSource = tema.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length == 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_TEMA") + " " + parent;
    }
//############################################
//implementing Indicator

    public boolean hasItsOwnScale() {
        if (innerSource instanceof Indicator) {
            return ((Indicator) innerSource).hasItsOwnScale();
        }
        return false;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }
//timePeriods

    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    // this calculation includes 3 intermediate steps EMA, EMA of EMA, EMA of EMA of EMA
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        long entryTime = System.currentTimeMillis();

        ChartRecord cr;
        //setting step size 3
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(3);
        }
        // steps involved
        byte stepEMA = ChartRecord.STEP_1;
        byte stepEMAofEMA = ChartRecord.STEP_2;
        byte stepEMAofEMAofEMA = ChartRecord.STEP_3;
        // stepEMA is smoothen only once
        MovingAverage.getMovingAverage(al, 0, timePeriods, MovingAverage.METHOD_EXPONENTIAL, getOHLCPriority(), stepEMA);
        // stepEMAofEMA is smoothen twice
        MovingAverage.getMovingAverage(al, timePeriods - 1, timePeriods, MovingAverage.METHOD_EXPONENTIAL, stepEMA, stepEMAofEMA);
        // stepEMAofEMAofEMA
        MovingAverage.getMovingAverage(al, timePeriods * 2 - 2, timePeriods, MovingAverage.METHOD_EXPONENTIAL, stepEMAofEMA, stepEMAofEMAofEMA);

        int loopbegin = timePeriods * 3 - 3;
        for (int i = 0; i < loopbegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }

        for (int i = loopbegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(cr.getStepValue(stepEMA) * 3f - cr.getStepValue(stepEMAofEMA) * 3f
                        + cr.getStepValue(stepEMAofEMAofEMA));
            }
        }

        //System.out.println("**** TEMA Calc time " + (entryTime - System.currentTimeMillis()));
    }
//############################################

    public String getShortName() {
        return Language.getString("IND_TEMA");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods, byte step1, byte step2, byte step3, byte srcIndex, byte destIndex) {
        byte stepEMA = step1;
        byte stepEMAofEMA = step2;
        byte stepEMAofEMAofEMA = step3;
        ChartRecord cr;
        int startIndex = bIndex + t_im_ePeriods * 3 - 3;
        if (al.size() < t_im_ePeriods || t_im_ePeriods == 0) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        MovingAverage.getMovingAverage(al, bIndex, t_im_ePeriods, MovingAverage.METHOD_EXPONENTIAL, srcIndex, stepEMA);
        MovingAverage.getMovingAverage(al, bIndex + t_im_ePeriods - 1, t_im_ePeriods, MovingAverage.METHOD_EXPONENTIAL, stepEMA, stepEMAofEMA);
        MovingAverage.getMovingAverage(al, bIndex + t_im_ePeriods * 2 - 2, t_im_ePeriods, MovingAverage.METHOD_EXPONENTIAL, stepEMAofEMA, stepEMAofEMAofEMA);

        //(3 * EMA) � (3 * EMA of EMA) + (EMA of EMA of EMA)

        for (int i = startIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            if (Double.isNaN(cr.getValue(stepEMA)) || Double.isNaN(cr.getValue(stepEMAofEMA)) || Double.isNaN(cr.getValue(stepEMAofEMAofEMA)))
                cr.setValue(destIndex, Indicator.NULL);
            else
                cr.setValue(destIndex, 3 * cr.getValue(stepEMA) - (3 * cr.getValue(stepEMAofEMA)) + cr.getValue(stepEMAofEMAofEMA));
        }
    }

    public static int getAuxStepCount() {
        return 3;
    }

}
