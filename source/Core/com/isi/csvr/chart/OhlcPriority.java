package com.isi.csvr.chart;

/**
 * User: Pramoda
 * Date: Mar 13, 2007
 * Time: 3:42:08 PM
 */
public enum OhlcPriority {
    OPEN, HIGH, LOW, CLOSE, VOLUME
}
