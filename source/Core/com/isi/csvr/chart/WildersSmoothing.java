package com.isi.csvr.chart;

import com.isi.csvr.shared.Language;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;

import java.util.ArrayList;
import java.awt.*;

/**
 * User: P.Dissanayake
 * Date: Feb 21, 2006
 * Time: 4:04:52 PM
 */
public class WildersSmoothing extends ChartProperties implements Indicator {

    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    private static final long serialVersionUID = UID_WILDERS_SMOOTHING;

    public WildersSmoothing(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_WILDERS_SMOOTHING") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_WILDERS_SMOOTHING");
        this.timePeriods = 14;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get( anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public void assignDefaultValues(){
        super.assignDefaultValues();
        this.timePeriods = 14;
    }
    
    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof WildersSmoothing) {
            WildersSmoothing ws = (WildersSmoothing) cp;
            this.timePeriods = ws.timePeriods;
            this.innerSource = ws.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_WILDERS_SMOOTHING") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        int loopBegin = timePeriods;
        if (loopBegin > al.size()) return;

        long entryTime = System.currentTimeMillis();
        ChartRecord cr;
        double sum = 0;

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            sum += cr.getValue(getOHLCPriority());
            if (i == loopBegin - 1) {
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(sum / timePeriods);
                }
            } else {
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
        }
        double prevAvg = sum / timePeriods;
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            prevAvg = prevAvg + (cr.getValue(getOHLCPriority()) - prevAvg) / timePeriods;

            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(prevAvg);
            }
        }

        //System.out.println("**** Wilder's Smoothing Calc time " + (entryTime - System.currentTimeMillis()));
    }
    //############################################

    public String getShortName() {
        return Language.getString("IND_WILDERS_SMOOTHING");
    }


    //this is to be used by other OHLC indicators those calculate a Wilder's Smoothing as an intermediate step
    public static void getWildersSmoothing(ArrayList al, int bIndex, int timePeriods, byte srcIndex, byte destIndex) {
        int loopBegin = bIndex + timePeriods;
        double sumValue = 0;
        ChartRecord cr;

        long entryTime = System.currentTimeMillis();
        int alSize = al.size();

        for (int i = 0; i < bIndex; i++) {
            if (i >= alSize) return;
            cr = (ChartRecord) al.get(i);
            cr.setStepValue(destIndex, Indicator.NULL);
        }
        for (int i = bIndex; i < loopBegin; i++) {
            if (i >= alSize) return;
            cr = (ChartRecord) al.get(i);
            sumValue += cr.getStepValue(srcIndex);
            if (i == loopBegin - 1) {
                cr.setStepValue(destIndex, sumValue / timePeriods);
            } else {
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        }
        double prevAvg = sumValue / timePeriods;

        for (int i = loopBegin; i < alSize; i++) {
            cr = (ChartRecord) al.get(i);
            prevAvg = prevAvg + (cr.getStepValue(srcIndex) - prevAvg) / timePeriods;
            cr.setStepValue(destIndex, prevAvg);
        }
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          int t_im_ePeriods, byte srcIndex, byte destIndex) {
        getWildersSmoothing(al, bIndex, t_im_ePeriods, srcIndex, destIndex);
    }

    public static int getAuxStepCount() {
        return 0;
    }

}

