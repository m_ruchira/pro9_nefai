package com.isi.csvr.chart;

import com.isi.csvr.shared.*;
import com.isi.csvr.table.TWTextField;
import com.isi.csvr.symbolselector.CompanyTable;
import com.isi.csvr.symbolselector.SymbolListener;
import com.isi.csvr.datastore.ExchangeStore;
import com.isi.csvr.ChartInterface;
import com.isi.csvr.SymbolMaster;
import com.isi.csvr.ShowMessage;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.event.*;
import java.awt.*;
import java.io.File;

/**
 * User: Pramoda
 * Date: Mar 19, 2007
 * Time: 2:27:06 PM
 */
public class ChartSymbolSearchComponent extends JPanel
        implements ActionListener, KeyListener, FocusListener, SymbolListener {

    private TWButton selector;
    private TWTextField text;
    private BottomToolBar parent;
    private boolean isCompareSymbol;

    public ChartSymbolSearchComponent(BottomToolBar parent, boolean isCompareSymbol) {
        this.parent = parent;
        this.isCompareSymbol = isCompareSymbol;
        createUI();
    }

    private void createUI() {
        selector = new TWButton(new DownArrow());
        selector.setFocusable(false);
        selector.setFocusPainted(false);
        selector.addActionListener(this);

        text = new TWTextField();
        text.setFont(new TWFont("Dialog", 1, 10));
        text.setOpaque(true);
        text.setBackground(Color.white);
        text.setBorder(BorderFactory.createLineBorder(Color.black));
        text.addKeyListener(this);
        text.addFocusListener(this);

        String[] widths = {"100%", "20"};
        String[] heights = {"17"};
        FlexGridLayout layout = new FlexGridLayout(widths, heights);
        setLayout(layout);
        add(text);
        add(selector);
        GUISettings.applyOrientation(this);
    }

    public TWTextField getTextFiled() {
        return text;
    }

//    public void setFont(Font font) {
//        text.setFont(font);
//    }

    public void setEnabled(boolean isEnabled) {
        text.setEnabled(isEnabled);
        selector.setEnabled(isEnabled);
    }

    public void setEditable(boolean isEditable) {
        text.setEditable(isEditable);
    }

    public void actionPerformed(ActionEvent e) {
        if (ExchangeStore.getSharedInstance().defaultCount() > 0) {
            int x;
            int y;
            Rectangle editorRect = text.getBounds();
            Point origin;
            if (Language.isLTR()) {
                origin = editorRect.getLocation();
            } else {
                origin = editorRect.getLocation();
                origin.x = origin.x + text.getWidth();
            }

            Toolkit oToolkit = Toolkit.getDefaultToolkit();
            Dimension screenSize = oToolkit.getScreenSize();

            SwingUtilities.convertPointToScreen(origin, this);

            x = origin.x;
            if (Language.isLTR()) {
                if ((origin.x + CompanyTable.getSharedInstance().getPanelWidth(CompanyTable.WINDOW_FIXED)) > screenSize.getWidth()) {
                    x = (int) (x - CompanyTable.getSharedInstance().getPanelWidth(CompanyTable.WINDOW_FIXED) + editorRect.getWidth());
                }
            } else {
                x = (x - CompanyTable.getSharedInstance().getPanelWidth(CompanyTable.WINDOW_FIXED));// + editorRect.getWidth());
                if (x < 0) {
                    x = (origin.x - this.getWidth());
                }
            }

            y = origin.y + selector.getHeight() + 1;
            if ((origin.y + CompanyTable.getSharedInstance().getPanelHeight(CompanyTable.WINDOW_FIXED)) > screenSize.getHeight()) {
                y = (int) (y - CompanyTable.getSharedInstance().getPanelHeight(CompanyTable.WINDOW_FIXED) - editorRect.getHeight());
            }
            CompanyTable.getSharedInstance().show(x, y, false, CompanyTable.WINDOW_FIXED);
            CompanyTable.getSharedInstance().addSymbolListener(this);
        } else {
            String key = null;
            String[] sa = ChartInterface.searchSymbols();
            if ((sa != null) && (sa.length > 0)) {
                key = sa[0];
            }
            if (key == null) return;

            text.setText(SharedMethods.getSymbolFromKey(key));
            GraphFrame gf = ChartFrame.getSharedInstance().getActiveGraph();
            if (gf != null) {
                if (isCompareSymbol) {
                    gf.setComparisonSymbol(key);
                } else {
                    try {
                        TemplateFactory.SaveTemplate(new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE),
                                new GraphFrame[]{gf});
                    } catch (Exception e1) {
                        e1.printStackTrace();
                    }
                    TemplateFactory.LoadTemplate(gf, new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE),
                            key, gf.isGraphDetached());
                }
            }
        }
    }

    public void keyTyped(KeyEvent e) {
        try {
            if (e.getKeyChar() == KeyEvent.VK_ESCAPE) {
                if (ExchangeStore.getSharedInstance().defaultCount() > 0) {
                    CompanyTable.getSharedInstance().hide();
                    CompanyTable.getSharedInstance().removeSymbolListener();
                }
            } else if (e.getKeyChar() == KeyEvent.VK_ENTER) {
                GraphFrame activeGraph = parent.getActiveGraph();
                if (isCompareSymbol && activeGraph != null) {
                    activeGraph.cmbCompare_actionPerformed(e);
                } else {
                    //activeGraph.cmbSymbol_actionPerformed(e);
                    cmbSymbol_actionPerformed(e);
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
            System.out.println("**************** error in chartSymbolSearchComponent KeyTyped() ************");
        }
    }

    public void cmbSymbol_actionPerformed(KeyEvent e) {
        if (!ChartInterface.isOfflineDataAvailable()) {
            return;
        }

        JTextField cmbSymbol;
        if (e.getSource() instanceof JTextField) {
            cmbSymbol = (JTextField) e.getSource();
        } else {
            return;
        }

        try {
            GraphFrame gf = ChartFrame.getSharedInstance().getActiveGraph();
            if (gf != null) {
                TemplateFactory.SaveTemplate(new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE), new GraphFrame[]{gf});
                String symbol = cmbSymbol.getText().trim().toUpperCase();
                String key = SymbolMaster.getExchangeForSymbol(symbol, false); // find the key for this symbol                           
                if (key != null && !key.equals("")) {
                    TemplateFactory.LoadTemplate(gf, new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE), key, gf.isGraphDetached());                    
                } else {
                    new ShowMessage(Language.getString("MSG_INVLID_SYMBOL"), "E");
                }
            } else {
                String symbol = cmbSymbol.getText().trim().toUpperCase();
                String key = SymbolMaster.getExchangeForSymbol(symbol, false); // find the key for this symbol
                if (key != null && !key.equals("")) {
                    ChartFrame.getSharedInstance().addNewGraph(true, false, key);
                } else {
                    new ShowMessage(Language.getString("MSG_INVLID_SYMBOL"), "E");
                }
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    public void keyPressed(KeyEvent e) {
    }

    public void keyReleased(KeyEvent e) {
    }

    public void focusGained(FocusEvent e) {
        text.repaint();
        selector.repaint();
    }

    public void focusLost(FocusEvent e) {
    }

    public void symbolSelected(String key) {
        text.setText(SharedMethods.getSymbolFromKey(key));
        CompanyTable.getSharedInstance().hide();
        CompanyTable.getSharedInstance().removeSymbolListener();
        GraphFrame gf = ChartFrame.getSharedInstance().getActiveGraph();
        if (gf != null) {
            if (isCompareSymbol) {
                gf.setComparisonSymbol(key);
            } else {
                try {
                    TemplateFactory.SaveTemplate(new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE),
                            new GraphFrame[]{gf});
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                TemplateFactory.LoadTemplate(gf, new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE),
                        key, gf.isGraphDetached());
            }
        }
    }

    public void actionCanceled() {
    }
}