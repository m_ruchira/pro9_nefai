package com.isi.csvr.chart;

import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * User: Pramoda
 * Date: Feb 8, 2007
 * Time: 12:55:16 PM
 */
public class MACDHistogram extends ChartProperties implements Indicator {
    //SignalTimePeriods
    private int signalTimePeriods;
    //Method
    private byte maCalcMethod;
    //SignalMethod
    private byte signalMACalcMethod;

    private int MACD_TimePeriods_High = 26;
    private int MACD_TimePeriods_Low = 12;

    private ChartProperties innerSource;
    private String tableColumnHeading;

    public MACDHistogram(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_MACD_HISTOGRAM") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_HISTOGRAM_MACD");
        setChartStyle((byte) StockGraph.INT_GRAPH_STYLE_BAR);
        signalTimePeriods = 9;
        maCalcMethod = MovingAverage.METHOD_EXPONENTIAL;
        signalMACalcMethod = MovingAverage.METHOD_EXPONENTIAL;
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof MACDHistogram) {
            MACDHistogram macdHistogram = (MACDHistogram) cp;
            this.signalTimePeriods = macdHistogram.signalTimePeriods;
            this.maCalcMethod = macdHistogram.maCalcMethod;
            this.signalMACalcMethod = macdHistogram.signalMACalcMethod;
            this.innerSource = macdHistogram.innerSource;
        }
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.signalTimePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalTimePeriods"));
        this.maCalcMethod = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/Method"));
        this.signalMACalcMethod = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/SignalMethod"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "SignalTimePeriods", Integer.toString(signalTimePeriods));
        TemplateFactory.saveProperty(chart, document, "Method", Byte.toString(maCalcMethod));
        TemplateFactory.saveProperty(chart, document, "SignalMethod", Byte.toString(signalMACalcMethod));
    }

    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        this.innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    // this calculation includes 10 intermediate steps: EMA26 and EMA12
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF gdm, int index) {
        long entryTime = System.currentTimeMillis();
        //int loopBegin = MACD.MACD_TimePeriods_High - 1 + signalTimePeriods - 1;

        int loopBegin = MACD_TimePeriods_High - 1 + signalTimePeriods - 1;
        if (loopBegin >= al.size()) return;
        ChartRecord cr;


        byte ohlcPrio = getOHLCPriority();
        //setting step size 10
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(10);
        }
        // steps involved
        byte stepEMA26 = ChartRecord.STEP_1;
        byte stepEMA12 = ChartRecord.STEP_2;
        byte stepMACD = ChartRecord.STEP_3;
        byte stepMA = ChartRecord.STEP_4;
        byte stepRes5 = ChartRecord.STEP_5;
        byte stepRes6 = ChartRecord.STEP_6;
        byte stepRes7 = ChartRecord.STEP_7;
        byte stepRes8 = ChartRecord.STEP_8;
        byte stepRes9 = ChartRecord.STEP_9;
        byte stepRes10 = ChartRecord.STEP_10;


        MovingAverage.getVariableMovingAverage(al, 0, MACD_TimePeriods_High, maCalcMethod, gdm, stepRes5, stepRes6, ohlcPrio, stepEMA26);
        MovingAverage.getVariableMovingAverage(al, 0, MACD_TimePeriods_Low, maCalcMethod, gdm, stepRes7, stepRes8, ohlcPrio, stepEMA12);

        int tmpBeginIndex = MACD_TimePeriods_High - 1;
        for (int i = MACD_TimePeriods_High - 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            if ((cr.getStepValue(stepEMA12) == Indicator.NULL) || Double.isNaN(cr.getStepValue(stepEMA12)) ||
                    (cr.getStepValue(stepEMA26) == Indicator.NULL) || Double.isNaN(cr.getStepValue(stepEMA26))
                    ) {
                cr.setStepValue(stepMACD, Indicator.NULL);
                tmpBeginIndex = i + 1;
            } else {
                break;
            }
        }
        for (int i = tmpBeginIndex; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepValue(stepMACD, cr.getStepValue(stepEMA12) - cr.getStepValue(stepEMA26));
        }
        MovingAverage.getVariableMovingAverage(al, tmpBeginIndex, signalTimePeriods, signalMACalcMethod, gdm, stepRes9, stepRes10, stepMACD, stepMA);

        for (int i = tmpBeginIndex + signalTimePeriods - 2; i >= 0; i--) {
            cr = (ChartRecord) al.get(i);
            gdm.removeIndicatorPoint(cr.Time, index, getID());
        }
        for (int i = tmpBeginIndex + signalTimePeriods - 1; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            ChartPoint aPoint = gdm.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                if ((cr.getStepValue(stepMA) == Indicator.NULL) || Double.isNaN(cr.getStepValue(stepMA))) {
                    gdm.removeIndicatorPoint(cr.Time, index, getID());
                } else {
                    aPoint.setIndicatorValue(cr.getStepValue(stepMACD) - cr.getStepValue(stepMA));
                }
            }
        }

        //System.out.println("**** MACD Histogram calc time " + (entryTime - System.currentTimeMillis()));
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public static int getAuxStepCount() {
        return 10;
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        return Language.getString("MACD_HISTOGRAM") + " " + parent;
    }

    public String getShortName() {
        return Language.getString("MACD_HISTOGRAM");
    }


    public byte getMaCalcMethod() {
        return maCalcMethod;
    }

    public void setMaCalcMethod(byte maCalcMethod) {
        this.maCalcMethod = maCalcMethod;
    }

    public byte getSignalMACalcMethod() {
        return signalMACalcMethod;
    }

    public void setSignalMACalcMethod(byte signalMACalcMethod) {
        this.signalMACalcMethod = signalMACalcMethod;
    }

    public int getSignalTimePeriods() {
        return signalTimePeriods;
    }

    public void setSignalTimePeriods(int signalTimePeriods) {
        this.signalTimePeriods = signalTimePeriods;
    }

    public int getMACD_TimePeriods_High() {
        return MACD_TimePeriods_High;
    }

    public void setMACD_TimePeriods_High(int MACD_TimePeriods_High) {
        this.MACD_TimePeriods_High = MACD_TimePeriods_High;
    }

    public int getMACD_TimePeriods_Low() {
        return MACD_TimePeriods_Low;
    }

    public void setMACD_TimePeriods_Low(int MACD_TimePeriods_Low) {
        this.MACD_TimePeriods_Low = MACD_TimePeriods_Low;
    }
}
