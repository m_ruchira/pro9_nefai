package com.isi.csvr.chart.options;

import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.chart.chartobjects.AbstractObject;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Mar 9, 2009
 * Time: 3:43:51 PM
 * To change this template use File | Settings | File Templates.
 */
public class ChartObjectElementList {


    public final static int DRAWING_TOOLS = 5;
    public final static int FIBONACI_TOOLS = 4;
    public final static int CHANNEL_TOOLS = 3;
    public final static int TERM_TOOLS = 2;
    public final static int GANN_TOOLS = 1;
    public final static int ZOOM_TOOLS = 0;
    public final static int ALL_TOOLS = 6;

    public ChartObjectElementList() {

    }

    public static Object[][] getDrawingElements() {

        //object id also included


        Object drawingelements[][] = {
                {
                        Language.getString("TRND_LINE"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_slope.gif"), AbstractObject.INT_LINE_SLOPE,DRAWING_TOOLS

                },
                {
                        Language.getString("ARROW_HEAD_TRND_LINE"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Arrow_slope.gif"), AbstractObject.INT_ARROW_LINE_SLOPE,DRAWING_TOOLS
                },
                {
                        Language.getString("HIRIZONTAL_LINE"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_horiz.gif"), AbstractObject.INT_LINE_HORIZ,DRAWING_TOOLS
                },
                {
                        Language.getString("VERTICAL_LINE"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_verti.gif"), AbstractObject.INT_LINE_VERTI,DRAWING_TOOLS
                },
                {
                        Language.getString("RECTANGLE"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_rect.gif"), AbstractObject.INT_RECT,DRAWING_TOOLS
                },
                {
                        Language.getString("ELLIPSE"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ellipse.gif"), AbstractObject.INT_ELLIPSE,DRAWING_TOOLS
                },
                {
                        Language.getString("TEXT"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_textnote.gif"), AbstractObject.INT_TEXT,DRAWING_TOOLS
                },
                {
                        Language.getString("REGRESSION_LINE"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_regression.gif"), AbstractObject.INT_REGRESSION,DRAWING_TOOLS
                },{
                        Language.getString("GRAPH_OBJ_ZIG_ZAG"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zig_zag.gif"), AbstractObject.INT_ZIG_ZAG,DRAWING_TOOLS
                },
                {
                        Language.getString("SIGN"), new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_obj_ellipse.gif")), AbstractObject.INT_SYMBOL,DRAWING_TOOLS
                },
                {
                        Language.getString("GRAPH_OBJ_ARC"), new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_arc.gif")), AbstractObject.INT_ARC, DRAWING_TOOLS
                },
                {
                        Language.getString("GRAPH_OBJ_CYCLE_LINE"), new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_cycle.gif")), AbstractObject.INT_CYCLE_LINE, DRAWING_TOOLS
                },
                {
                        Language.getString("GRAPH_OBJ_PITCHFORK"), new ImageIcon(Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_pitchfork.gif")), AbstractObject.INT_ANDREWS_PITCHFORK, DRAWING_TOOLS
                }
        };

        return drawingelements;
    }

    public static Object[][] getFibonacciElements() {


        Object fibonaciElements[][] = {
                {
                        Language.getString("GRAPH_OBJ_FIB_ARCS"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibArcs.gif"), AbstractObject.INT_FIBONACCI_ARCS,FIBONACI_TOOLS
                },
                {
                        Language.getString("GRAPH_OBJ_FIB_FANS"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibFans.gif"), AbstractObject.INT_FIBONACCI_FANS,FIBONACI_TOOLS
                },
                {
                        Language.getString("GRAPH_OBJ_FIB_RETRC"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibRetrc.gif"), AbstractObject.INT_FIBONACCI_RETRACEMENTS,FIBONACI_TOOLS
                },
                {
                        Language.getString("GRAPH_OBJ_FIB_EXTENSIONS"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fib_extensions.gif"), AbstractObject.INT_FIBONACCI_EXTENSIONS,FIBONACI_TOOLS
                },
                {
                        Language.getString("GRAPH_OBJ_FIB_ZONES"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibZone.gif"), AbstractObject.INT_FIBONACCI_ZONES,FIBONACI_TOOLS
                }


        };


        return fibonaciElements;
    }

    public static Object[][] getChanelElements() {

        Object chanelElements[][] = {
                {
                        Language.getString("GRAPH_OBJ_STD_ERROR_CHANNEL"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdErr.gif"), AbstractObject.INT_STD_ERROR_CHANNEL,CHANNEL_TOOLS
                },
                {
                        Language.getString("OBJECT_RAFF_REGRESSION"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_raffRegChannel.gif"), AbstractObject.INT_RAFF_REGRESSION,CHANNEL_TOOLS
                },
                {
                        Language.getString("OBJECT_STD_DEV_CHANNEL"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdDevChannel.gif"), AbstractObject.INT_STD_DEV_CHANNEL,CHANNEL_TOOLS
                },
                {
                        Language.getString("OBJECT_EQU_DIST_CHANNEL"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_equDistChannel.gif"), AbstractObject.INT_EQUI_DIST_CHANNEL,CHANNEL_TOOLS
                }

        };

        return chanelElements;
    }


    public static Object[][] getZoomElements() {


        Object zoomElements[][] = {
                {
                        Language.getString("GRAPH_HAND_DRAG"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_hand_drag.gif"),104,ZOOM_TOOLS
                },
                {
                        Language.getString("GRAPH_SMART_ZOOM"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_smartzoom.gif"),105,ZOOM_TOOLS
                },
                {
                        Language.getString("GRAPH_ZOOM_IN"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomin.gif"),106,ZOOM_TOOLS
                },
                {
                        Language.getString("GRAPH_ZOOM_OUT"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomout.gif"),107,ZOOM_TOOLS
                },
                {
                        Language.getString("GRAPH_BOX_ZOOM"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_boxzoom.gif"),108,ZOOM_TOOLS
                },
                {
                        Language.getString("GRAPH_DRAG_ZOOM"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_dragzoom.gif"),109,ZOOM_TOOLS
                },
                {
                        Language.getString("GRAPH_XAXIS_RESET"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_resetHzoom.gif"),110,ZOOM_TOOLS
                },
                {
                        Language.getString("GRAPH_YAXIS_RESET"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_resetVzoom.gif"),111,ZOOM_TOOLS
                },
                {
                        Language.getString("GRAPH_ZOOM_RESET"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomreset.gif"),112,ZOOM_TOOLS
                }
        };

        return zoomElements;
    }


    public static Object[][] getGannElements() {
        Object Gannelements[][] = {
                {Language.getString("OBJECT_GAN_LINE"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannLine.gif"), AbstractObject.INT_GANN_LINE,GANN_TOOLS},
                {Language.getString("OBJECT_GANN_FAN"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannFan.gif"), AbstractObject.INT_GANN_FAN,GANN_TOOLS},
                {Language.getString("OBJECT_GANN_GRID"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannGrid.gif"), AbstractObject.INT_GANN_GRID,GANN_TOOLS}};
        return Gannelements;
    }

    public static Object[][] getTermElements() {
        Object termElements[][] = {
                {Language.getString("TEMPLATE_SHORT_TERM"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template1.gif"),101,TERM_TOOLS},
                {Language.getString("TEMPLATE_MID_TERM"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template2.gif"),102,TERM_TOOLS},
                {Language.getString("TEMPLATE_LONG_TERM"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template3.gif"),103,TERM_TOOLS}};
        return termElements;
    }

    public static Object[][] getAllElements() {
        Object Gannelements[][] = {
                {Language.getString("OBJECT_GAN_LINE"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannLine.gif")},
                {Language.getString("OBJECT_GANN_FAN"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannFan.gif")},
                {Language.getString("OBJECT_GANN_GRID"), new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_horiz.gif")}};
        return Gannelements;
    }


    public static Object[][] getToolset(int toolSetID) {
        Object[][] elements;
        switch (toolSetID) {
            case DRAWING_TOOLS:

                return getDrawingElements();
            case FIBONACI_TOOLS:
                return getFibonacciElements();

            case CHANNEL_TOOLS:
                return getChanelElements();

            case TERM_TOOLS:
                return getTermElements();

            case GANN_TOOLS:
                return getGannElements();
            case ZOOM_TOOLS:
                return getZoomElements();

//            case ALL_TOOLS:
//                return getAllElements();
            default:
                return getDrawingElements();

        }

    }


}
