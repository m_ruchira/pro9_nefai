package com.isi.csvr.chart.options;

import com.isi.csvr.chart.*;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.shared.Language;

import javax.swing.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 11, 2008
 * Time: 6:38:50 PM
 * To change this template use File | Settings | File Templates.
 */
public class UIHelper {

    public static JComboBox getGraphStyleCombo(boolean isVolume,final JComponent[] subComps) {
        JComboBox cmbStyle = new JComboBox();
        cmbStyle.setEditable(false);
        String[] sa = GraphFrame.getGraphStyles();
        Icon[] ia = GraphToolBar.styleIcon;
        cmbStyle.setRenderer(new ComboRenderer());
        cmbStyle.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        int breakIndex = 4;

        if (!isVolume) {
            for (int i = 0; i < sa.length; i++) {
                ci = new ComboItem(sa[i], sa[i], ia[i], ComboItem.ITEM_NORMAL);
                cmbStyle.addItem(ci);

                if (i >= breakIndex) break;
            }
        } else {
            for (int i = 0; i < 2; i++) {
                ci = new ComboItem(sa[i], sa[i], ia[i], ComboItem.ITEM_NORMAL);
                cmbStyle.addItem(ci);

                if (i >= breakIndex) break;
            }
        }

        cmbStyle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                if (subComps != null) {
                    JComboBox cmb = (JComboBox) e.getSource();
                    if (cmb.getSelectedIndex() == StockGraph.INT_GRAPH_STYLE_LINE) {
                        setCompsEnabled(subComps, true);
                    } else {
                        setCompsEnabled(subComps, false);
                    }
                }
            }
        });
        return cmbStyle;
    }

    private static void setCompsEnabled(JComponent[] ca, boolean enabled) {
        for (int i = 0; i < ca.length; i++) {
            ca[i].setEnabled(enabled);
        }
    }
    //public static String[] lineStyles = {"Solid", "Dot", "Dash", "DashDot", "DashDotDot"};
    public static String[] lineStyles = {Language.getString("CS_SOLID"), Language.getString("CS_DOT"), Language.getString("CS_DASH")
            , Language.getString("CS_DASH_DOT"), Language.getString("CS_DASH_DOT_DOT")};
    static Icon[] penStyles = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_solid.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dot.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dash.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dash_dot.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_style_dash_dot_dot.gif")
    };
    static Icon[] penWidths = {
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_0p5.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_1p0.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_1p5.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_2p0.gif"),
            new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_line_width_3p0.gif")
    };

    static String[] saPriority = {Language.getString("OPEN"),
            Language.getString("HIGH"), Language.getString("LOW"),
            Language.getString("CLOSE")};

    public static JComboBox getPriorityCombo() {
        JComboBox cmbPrio = new JComboBox();

        cmbPrio.setFont(StockGraph.font_BOLD_10);

        for (int i = 0; i < saPriority.length; i++) {
            cmbPrio.addItem(saPriority[i]);


        }
        cmbPrio.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            }
        });
        return cmbPrio;
    }

    public static JComboBox getPenStyleCombo() {

        JComboBox cmbLineStyle = new JComboBox();
        cmbLineStyle.setEditable(false);
        cmbLineStyle.setRenderer(new ComboRenderer());
        cmbLineStyle.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;

        for (int i = 0; i < lineStyles.length; i++) {
            ci = new ComboItem(lineStyles[i], lineStyles[i], penStyles[i], ComboItem.ITEM_NORMAL);
            cmbLineStyle.addItem(ci);

        }
        cmbLineStyle.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            }
        });
        return cmbLineStyle;
    }

    public static String[] lineWidths = {"1.0", "1.5", "2.0", "3.0", "5.0"};

    public static JComboBox getPenWidthCombo() {
        JComboBox cmbLineWidth = new JComboBox();
        cmbLineWidth.setEditable(false);
        cmbLineWidth.setRenderer(new ComboRenderer());
        cmbLineWidth.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        for (int i = 0; i < lineWidths.length; i++) {
            ci = new ComboItem(lineWidths[i], lineWidths[i], penWidths[i], ComboItem.ITEM_NORMAL);
            cmbLineWidth.addItem(ci);

        }
        cmbLineWidth.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            }
        });
        return cmbLineWidth;
    }

    public static Color[] colors = {Color.BLACK, Color.BLUE, Color.CYAN, Color.DARK_GRAY,
            Color.GRAY, Color.LIGHT_GRAY, Color.GREEN, Color.MAGENTA, Color.ORANGE, Color.PINK, Color.RED,
            Color.WHITE, Color.YELLOW};
    static String[] colorNames = extractColorNames();

    public static String[] extractColorNames() {
        String[] sa = null;
        try {
            sa = Language.getString("COLOR_NAMES").split(",");
        }
        catch (Exception ex) {
        }
        return sa;
    }

    public static JComboBox getColorCombo() {
        JComboBox cmbColor = new JComboBox();

        cmbColor.setEditable(false);
        cmbColor.setRenderer(new ComboRenderer());
        cmbColor.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        boolean customColor = true;
        for (int i = 0; i < colors.length; i++) {
            ci = new ComboItem(colorNames[i], colors[i], new ComboImage(colors[i]), ComboItem.ITEM_NORMAL);
            //ci.setDisabledImage(ComboImage.getDisabledImage());
            cmbColor.addItem(ci);

        }
        //ci = new ComboItem(Language.getString("CUSTOM"), oldColor, new ComboImage(oldColor), ComboItem.ITEM_CUSTOM_COLOR);
        ci = new ComboItem(Language.getString("CUSTOM"), Color.DARK_GRAY, new ComboImage(Color.DARK_GRAY), ComboItem.ITEM_CUSTOM_COLOR);
        cmbColor.addItem(ci);

        /*   cmbColor.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                JComboBox cmb = (JComboBox) e.getSource();
                Color color = (Color) (((ComboItem) cmb.getSelectedItem()).getValue());

            }
        });*/
        return cmbColor;
    }

    public static JComboBox getGraphTypeCombo() {
        JComboBox cmbGraphType = new JComboBox();

        cmbGraphType.setFont(StockGraph.font_BOLD_10);


        cmbGraphType.addItem(Language.getString("NET_VALUES"));
        cmbGraphType.addItem(Language.getString("PERCENT_VALUES"));

        cmbGraphType.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

            }
        });
        return cmbGraphType;
    }


}
