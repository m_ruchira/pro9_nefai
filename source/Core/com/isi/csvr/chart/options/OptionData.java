package com.isi.csvr.chart.options;


/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jun 25, 2008
 * Time: 11:06:35 AM
 * To change this template use File | Settings | File Templates.
 */

public class OptionData {

    /*general settings*/
    public ChartOptions.AddStudiesOption gen_add_studies = ChartOptions.AddStudiesOption.CONTINUE_WITH_SHIFT;

    public boolean gen_confirm_del_chart = false;

    public boolean gen_confirm_del_indicator = false;

    public boolean gen_show_tool_tip = false;

    public boolean gen_is_free_style = false;

    public boolean gen_is_shw_last_ind_val = false;

    public ChartOptions.SymbolNameOptions misc_symbol_Name = ChartOptions.SymbolNameOptions.SYMBOL_AND_SHORT_NAME;

    /*chart settings*/
    public boolean[] chart_main_title = {true, true, false};  

    public ChartOptions.LegendStyle chart_legend_style = ChartOptions.LegendStyle.SYMBOL;

    public ChartOptions.YAxisPosition chart_y_axis_poition = ChartOptions.YAxisPosition.RIGHT_ONLY;

    public ChartOptions.WindowTileMode chart_window_tile = ChartOptions.WindowTileMode.TILE_NORMAL;

    public int chart_right_margin_history = 3;
    public int chart_right_margin_intraday = 1;

    public ChartOptions.ImageType save_image_type = ChartOptions.ImageType.jpeg;

    /*other settings*/
    public ChartOptions.ChartColorPreferences chart_color_preferences = ChartOptions.ChartColorPreferences.THEME_COLORS;

    //default constructor
    public OptionData() {
        //do nothing here
    }

    public void printProperties() {

        System.out.println("\n========== general settings =========\n");
        System.out.println("gen_add_studies : " + this.gen_add_studies);
        System.out.println("gen_confirm_del_chart : " + this.gen_confirm_del_chart);
        System.out.println("gen_confirm_del_indicator : " + this.gen_confirm_del_indicator);
        System.out.println("\n========== chart settings =========\n");
        System.out.println("chart_main_title MODE : " + this.chart_main_title[0]);
        System.out.println("chart_main_title PERIOD : " + this.chart_main_title[1]);
        System.out.println("chart_main_title INTERVAL : " + this.chart_main_title[2]);
        System.out.println("chart_legend_style : " + this.chart_legend_style);
        System.out.println("chart_y_axis_poition : " + this.chart_y_axis_poition);
        System.out.println("chart_right_margin_history : " + this.chart_right_margin_history);
        System.out.println("chart_right_margin_intraday : " + this.chart_right_margin_intraday);
        System.out.println("\n========== save settings =========\n");
        System.out.println("save_image_type : " + this.save_image_type);
        System.out.println("\n========== scan settings =========\n");
        System.out.println("\n========== miscellaneous settings =========\n");
        System.out.println("misc_symbol_Name : " + this.misc_symbol_Name);
        System.out.println("color preference : " + this.chart_color_preferences);
    }


}
