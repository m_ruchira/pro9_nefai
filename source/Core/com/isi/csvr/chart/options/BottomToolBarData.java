package com.isi.csvr.chart.options;

import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Text;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.DocumentBuilder;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.Transformer;
import javax.xml.transform.OutputKeys;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathFactory;
import javax.xml.xpath.XPathConstants;
import java.util.Hashtable;
import java.util.Arrays;
import java.io.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: May 11, 2009
 * Time: 12:07:13 PM
 * To change this template use File | Settings | File Templates.
 */
public class BottomToolBarData {

    private static final String ROOT_ELEMENT_TOOLBAR = "BottomToolbarData";
    private static final String OBJECT = "object";
    private static final String OBJECT_ID = "objectID";
    private static final String IS_VISIBLE = "isVisble";

    private static final String DRAWING_TOOLS = "drawingTools";
    private static final String FIBONACI_TOOLS = "fibonacciTools";
    private static final String CHANNEL_TOOLS = "channelTools";
    private static final String TERM_TOOLS = "termTools";
    private static final String GANN_TOOLS = "gannTools";
    private static final String ZOOM_TOOLS = "zoomTools";

    private static Hashtable toolBarData = null;

    public static void saveToolBarData(Hashtable hash) {

        //creating an empty XML document
        DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
        DocumentBuilder docBuilder = null;
        try {
            docBuilder = dbFactory.newDocumentBuilder();

            Document doc = docBuilder.newDocument();
            Element root = doc.createElement(ROOT_ELEMENT_TOOLBAR);
            doc.appendChild(root);

            Integer[] groupKeys = (Integer[]) hash.keySet().toArray(new Integer[0]);
            Arrays.sort(groupKeys);

            for (int j = 0; j < groupKeys.length; j++) {

                if (groupKeys[j] == ChartObjectElementList.ALL_TOOLS) {
                    continue;
                }
                Hashtable insideTable = (Hashtable) hash.get(groupKeys[j]);

                Integer[] objectKeys = (Integer[]) insideTable.keySet().toArray(new Integer[0]);
                Arrays.sort(objectKeys);
                Element eleGrp = doc.createElement(getXMLTagName(groupKeys[j]));
                root.appendChild(eleGrp);

                for (int i = 0; i < objectKeys.length; i++) {
                    Element ele = doc.createElement(OBJECT);
                    eleGrp.appendChild(ele);

                    Element eleName = doc.createElement(OBJECT_ID);
                    String str = String.valueOf(objectKeys[i]);
                    Text text = doc.createTextNode(str);
                    ele.appendChild(eleName);
                    eleName.appendChild(text);

                    Element eleVal = doc.createElement(IS_VISIBLE);
                    String strVal = String.valueOf(insideTable.get(objectKeys[i]));

                    text = doc.createTextNode(strVal);
                    ele.appendChild(eleVal);
                    eleVal.appendChild(text);
                }
            }


            //setup a transformer
            TransformerFactory transFactory = TransformerFactory.newInstance();
            Transformer transformer = transFactory.newTransformer();

            transformer.setOutputProperty(OutputKeys.OMIT_XML_DECLARATION, "no");
            transformer.setOutputProperty(OutputKeys.INDENT, "true");

            //saving the XML file
            StringWriter writer = new StringWriter();
            StreamResult result = new StreamResult(writer);
            DOMSource source = new DOMSource(doc);

            transformer.transform(source, result);

            String xmlString = writer.toString();

            FileWriter fw = new FileWriter(ChartConstants.BOTTOM_TOOLBAR_INFO_PATH);
            BufferedWriter bufWriter = new BufferedWriter(fw);
            bufWriter.write(xmlString);
            bufWriter.close();
            fw.close();

            //refresh the store after saving new properties(in memory)
            toolBarData = null;
            toolBarData = getSavedToolBarData();

        }
        catch (Exception e) {
            e.printStackTrace();
            System.out.println("=== BottomToolBarData class ======");
        }
    }

    private static Hashtable<Integer, Boolean> getSavedToolBarData() {

        Hashtable<Integer, Boolean> table = new Hashtable<Integer, Boolean>();

        Hashtable<Integer, Boolean> table1 = getSavedToolBarDataForGroup(ChartObjectElementList.DRAWING_TOOLS);
        Hashtable<Integer, Boolean> table2 = getSavedToolBarDataForGroup(ChartObjectElementList.ZOOM_TOOLS);
        Hashtable<Integer, Boolean> table3 = getSavedToolBarDataForGroup(ChartObjectElementList.CHANNEL_TOOLS);
        Hashtable<Integer, Boolean> table4 = getSavedToolBarDataForGroup(ChartObjectElementList.GANN_TOOLS);
        Hashtable<Integer, Boolean> table5 = getSavedToolBarDataForGroup(ChartObjectElementList.FIBONACI_TOOLS);
        Hashtable<Integer, Boolean> table6 = getSavedToolBarDataForGroup(ChartObjectElementList.TERM_TOOLS);

        table.putAll(table1);
        table.putAll(table2);
        table.putAll(table3);
        table.putAll(table4);
        table.putAll(table5);
        table.putAll(table6);

        return table;
    }

    public static Hashtable<Integer, Boolean> getSavedToolBarDataForGroup(int groupID) {

        Hashtable<Integer, Boolean> table = new Hashtable<Integer, Boolean>();

        File file = new File(ChartConstants.BOTTOM_TOOLBAR_INFO_PATH);
        if (!file.exists()) {
            return null;
        }
        XPath xpath = XPathFactory.newInstance().newXPath();
        String expression = ROOT_ELEMENT_TOOLBAR + "/" + getXMLTagName(groupID) + "/" + OBJECT;

        try {
            DocumentBuilderFactory dbFactory = DocumentBuilderFactory.newInstance();
            DocumentBuilder docBuilder = dbFactory.newDocumentBuilder();

            Document dom = docBuilder.parse(new File(ChartConstants.BOTTOM_TOOLBAR_INFO_PATH).toURI().toString());
            NodeList list = (NodeList) xpath.evaluate(expression, dom, XPathConstants.NODESET);
            int count = list.getLength();

            for (int i = 0; i < count; i++) {

                Element elem = (Element) list.item(i);
                NodeList nlName = elem.getElementsByTagName(OBJECT_ID);
                NodeList nlVale = elem.getElementsByTagName(IS_VISIBLE);

                String name = nlName.item(0).getTextContent();
                String value = nlVale.item(0).getTextContent();

                table.put(new Integer(name), Boolean.valueOf(value));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return table;
    }

    public static boolean isButtonVisible(int objectID) {

        try {
            if (toolBarData == null) {
                toolBarData = getSavedToolBarData();
            }

            if (toolBarData != null) {
                return Boolean.parseBoolean(toolBarData.get(new Integer(objectID)).toString());
            }
        } catch (Exception e) {
            e.printStackTrace();
            return true;
        }
        return true;
    }

    private static String getXMLTagName(int id) {
        switch (id) {

            case ChartObjectElementList.DRAWING_TOOLS:
                return DRAWING_TOOLS;
            case ChartObjectElementList.FIBONACI_TOOLS:
                return FIBONACI_TOOLS;
            case ChartObjectElementList.CHANNEL_TOOLS:
                return CHANNEL_TOOLS;
            case ChartObjectElementList.TERM_TOOLS:
                return TERM_TOOLS;
            case ChartObjectElementList.GANN_TOOLS:
                return GANN_TOOLS;
            case ChartObjectElementList.ZOOM_TOOLS:
                return ZOOM_TOOLS;
        }
        return "";
    }
}


