package com.isi.csvr.chart.options;

import com.isi.csvr.shared.Settings;


/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 3, 2008
 * Time: 9:32:57 AM
 * To change this template use File | Settings | File Templates.
 */
public class ChartConstants {

    //    public static final String CHART_CUSTOMIZED_THEME_FILE_PATH = "./Charts/custom_theme.dll";
    public static final String CHART_CUSTOMIZED_THEME_FILE_PATH = Settings.CHART_DATA_PATH + "/custom_theme.dll";

    //    public static final String CHART_OPTION_FILE_PATH = "./Charts/chart_options.dll";
    public static final String CHART_OPTION_FILE_PATH = Settings.CHART_DATA_PATH + "/chart_options.dll";

    //    public static final String CHART_CUSTOMIZED_THEME_TEMP_FILE_PATH = "./Charts/custom_theme_temp.dll";
    public static final String CHART_CUSTOMIZED_THEME_TEMP_FILE_PATH = Settings.CHART_DATA_PATH + "/custom_theme_temp.dll";

    public static final String BOTTOM_TOOLBAR_INFO_PATH = Settings.CHART_DATA_PATH + "/btbinfo.dll";

    //    public static final String ENCRYPTION_KEY_FILE_PATH = "./Charts/AES.dll";
    public static final String ENCRYPTION_KEY_FILE_PATH = Settings.CHART_DATA_PATH + "/AES.dll";

    //    public static final String CHART_OPTIONS_TEMP_FILE_PATH = "./Charts/chart_options_temp.dll";
    public static final String CHART_OPTIONS_TEMP_FILE_PATH = Settings.CHART_DATA_PATH + "/chart_options_temp.dll";

    public static final String[] IMAGE_EXTENSIONS = new String[]{"jpg", "png", "bmp", "jpeg", "gif", "tiff", "wmf", "emf", "exif"};


}
