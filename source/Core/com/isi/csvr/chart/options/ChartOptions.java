package com.isi.csvr.chart.options;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jun 25, 2008
 * Time: 11:06:54 AM
 * To change this template use File | Settings | File Templates.
 */
public class ChartOptions {

    //general settings
    public static enum AddStudiesOption {
        ALWAYS_CONTINUE,
        CONTINUE_WITH_SHIFT,          //same as continue with CTRL. now this is the default setting. no need to include in to the option dialog
        ALWAYS_SINGLE
    }

    public static enum SymbolNameOptions {
        SYMBOL_ONLY,
        SHORT_NAME,
        LONG_NAME,
        SYMBOL_AND_SHORT_NAME,
        SYMBOL_AND_LONG_NAME
    }

    //chart settings
    public static enum MainTitleOptions {
        MODE,
        PERRIOD,
        IINTERVAL,
        LAST_VALUE
    }

    public static enum LegendPosition {
        NONE,
        TOP_LEFT,
        TOP_RIGHT,
        BOTTOM_LEFT,
        BOTTOM_RIGHT
    }

    public static enum YAxisPosition {
        LEFT_ONLY,
        RIGHT_ONLY,
        BOTH
    }

    public static enum WindowTileMode {
        TILE_NORMAL,
        TABBED,
        TILE_HORIZONTAL,
        TILE_VERTICAL
    }

    public static enum LegendStyle {
        SYMBOL,
        COMPANY_NAME
    }

    public static enum ImageType {
        jpg,
        png,
        bmp,
        jpeg,
        gif,
        tiff,
        wmf,
        emf,
        exif
    }

    //other settings
    public static enum ChartColorPreferences {
        DEFAULT_COLORS,
        THEME_COLORS,
        CUSTOM_COLORS
    }

    private static OptionData currentOptions;

    private static XMLSerilaizationService service = new XMLSerilaizationService();

    public static OptionData getCurrentOptions() {

        if (currentOptions == null) {
            currentOptions = service.deserializeOptionData(ChartConstants.CHART_OPTION_FILE_PATH);
        }
        return currentOptions;
    }

    public static void saveOptionData(OptionData optData) {

        if (optData == null) {
            return;
        }
        service.serializeOptionData(optData, ChartConstants.CHART_OPTION_FILE_PATH);
        currentOptions = optData;
    }


    /*  public static void main(String[] args) {

            ChartOptions.getCurrentOptions();

            OptionData d = new OptionData();
            d.chart_y_axis_poition = YAxisPosition.BOTH;
            d.save_image_type = ImageType.TIFF;
            d.chart_right_margin_history = 139;
            d.chart_right_margin_intraday = 545;
            d.misc_symbol_Name = SymbolNameOptions.LONG_NAME;
            ChartOptions.saveOptionData(d);
        }
    */
}
