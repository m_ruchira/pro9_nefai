package com.isi.csvr.chart;

import com.isi.csvr.shared.Language;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeTwoParameterProperty;

import java.awt.*;
import java.io.Serializable;
import java.util.*;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public class MovingAverage extends ChartProperties implements Indicator, Serializable {

    public final static byte METHOD_SIMPLE = 0;
    public final static byte METHOD_WEIGHTED = 1;
    public final static byte METHOD_EXPONENTIAL = 2;
    public final static byte METHOD_VARIABLE = 3;
    public final static byte METHOD_TIME_SERIES = 4;
    public final static byte METHOD_TRANGULAR = 5;
    public final static byte METHOD_VOLUME_ADJUSTED = 6;
    //Fields
    protected int timePeriods;
    protected byte method;
    private ChartProperties innerSource;
    //private int innerSourceIndex;
    private String tableColumnHeading;

    private static final long serialVersionUID = UID_MovingAvg;

    public MovingAverage(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, getName() + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_MOVING_AVERAGE");
        timePeriods = 21;
        method = METHOD_SIMPLE;

        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault && !(this instanceof BollingerBand)) {
            IndicatorTypeTwoParameterProperty idp = (IndicatorTypeTwoParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getParam1());
            this.setMethod(idp.getParam2());
        }

    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 21;
        this.method = MovingAverage.METHOD_SIMPLE;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
        this.method = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/Method"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
        TemplateFactory.saveProperty(chart, document, "Method", Byte.toString(this.method));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof MovingAverage) {
            MovingAverage ma = (MovingAverage) cp;
            this.timePeriods = ma.timePeriods;
            this.method = ma.method;
            this.innerSource = ma.innerSource;
            //this.innerSourceIndex = ma.innerSourceIndex;
        }
    }

    protected static String getName() {
        return Language.getString("MOVING_AVERAGE");
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
//        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("MOVING_AVERAGE") + " " + parent + getClose_Periods_MethodStr(sa.length != 2);
    }

    protected String getClose_Periods_MethodStr(boolean isIndicator) {

        return "(" + getPriorityStr(isIndicator) + ", " + timePeriods + ", "
                + getMethodString() + ")";
    }

    protected String getPriorityStr(boolean isIndicator) {
        if (isIndicator) {
            return Language.getString("INDICATOR");
        } else {
            switch (getOHLCPriority()) {
                case GraphDataManager.INNER_Open:
                    return Language.getString("OPEN");
                case GraphDataManager.INNER_High:
                    return Language.getString("HIGH");
                case GraphDataManager.INNER_Low:
                    return Language.getString("LOW");
                default:
                    return Language.getString("CLOSE");
            }
        }
    }

    protected String getMethodString() {
        switch (method) {
            case METHOD_SIMPLE:
                return Language.getString("METHOD_SIMPLE");
            case METHOD_WEIGHTED:
                return Language.getString("METHOD_WEIGHTED");
            case METHOD_EXPONENTIAL:
                return Language.getString("METHOD_EXPONENTIAL");
            case METHOD_VARIABLE:
                return Language.getString("METHOD_VARIABLE");
            case METHOD_VOLUME_ADJUSTED:
                return Language.getString("METHOD_VOLUME_ADJUSTED");
            case METHOD_TRANGULAR:
                return Language.getString("METHOD_TRANGULAR");
            case METHOD_TIME_SERIES:
                return Language.getString("METHOD_TIME_SERIES");
            default:
                return Language.getString("METHOD_SIMPLE");
        }
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        if (innerSource instanceof Indicator) {
            return ((Indicator) innerSource).hasItsOwnScale();
        }
        return false;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        int loopBegin = timePeriods;
        double sumValue = 0;
        ChartRecord cr, crOld;

        long entryTime = System.currentTimeMillis();

        if (loopBegin > al.size()) return;

        switch (method) {
            case METHOD_SIMPLE:
                for (int i = 0; i < loopBegin; i++) {
                    cr = (ChartRecord) al.get(i);
                    sumValue += cr.getValue(getOHLCPriority());
                    if (i >= loopBegin - 1) {
                        ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                        if (aPoint != null) {
                            aPoint.setIndicatorValue(sumValue / timePeriods);
                        }
                    } else {
                        GDM.removeIndicatorPoint(cr.Time, index, getID());
                    }
                }
                for (int i = loopBegin; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    crOld = (ChartRecord) al.get(i - loopBegin);
                    sumValue += cr.getValue(getOHLCPriority());
                    sumValue -= crOld.getValue(getOHLCPriority());
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue(sumValue / timePeriods);
                    }
                }
                break;

            case METHOD_WEIGHTED:

                loopBegin = timePeriods - 1;
                int weightSum = Math.round((timePeriods + 1f) * timePeriods / 2f);
                for (int i = 0; i < loopBegin; i++) {
                    cr = (ChartRecord) al.get(i);
                    GDM.removeIndicatorPoint(cr.Time, index, getID());
                }
                for (int i = loopBegin; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue(getWeightedMovAvg(al, 0, i, weightSum));
                    }
                }
                break;

            case METHOD_EXPONENTIAL:

                cr = (ChartRecord) al.get(0);
                double prevValue = cr.getValue(getOHLCPriority());
                float expPercentage = 2f / (timePeriods + 1f);
                for (int i = 1; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        prevValue = getExpMovAvg(al, prevValue, i, expPercentage);
                        aPoint.setIndicatorValue(prevValue);
                    }
                }
                for (int i = 0; i < timePeriods - 1; i++) {
                    cr = (ChartRecord) al.get(i);
                    GDM.removeIndicatorPoint(cr.Time, index, getID());
                }
                break;

            // this uses 1 intermediate step: TimeSeriesForecast
            case METHOD_TIME_SERIES:

                //setting step size 1
                for (int i = 0; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    cr.setStepSize(1);
                }
                // steps involved
                byte stepTimeSeriesForecast = ChartRecord.STEP_1;

                TimeSeriesForcast.getForcast(al, 0, timePeriods, GDM, getOHLCPriority(), stepTimeSeriesForecast);

                loopBegin = timePeriods - 1;
                for (int i = 0; i < loopBegin; i++) {
                    cr = (ChartRecord) al.get(i);
                    GDM.removeIndicatorPoint(cr.Time, index, getID());
                }
                for (int i = loopBegin; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue(cr.getStepValue(stepTimeSeriesForecast));
                    }
                }
                break;

            // this uses 2 intermediate steps: SMA1, SMA2
            case METHOD_TRANGULAR:

                //setting step size 2
                for (int i = 0; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    cr.setStepSize(2);
                }
                // steps involved
                byte stepSMA1 = ChartRecord.STEP_1;
                byte stepSMA2 = ChartRecord.STEP_2;

                //calculating triangular time periods
                int triangularPeriod = (int) Math.ceil(timePeriods / 2f);
                MovingAverage.getMovingAverage(al, 0, triangularPeriod, METHOD_SIMPLE, getOHLCPriority(), stepSMA1);
                MovingAverage.getMovingAverage(al, triangularPeriod - 1, triangularPeriod, METHOD_SIMPLE, stepSMA1, stepSMA2);

                loopBegin = 2 * triangularPeriod - 2;
                for (int i = 0; i < loopBegin; i++) {
                    cr = (ChartRecord) al.get(i);
                    GDM.removeIndicatorPoint(cr.Time, index, getID());
                }
                for (int i = loopBegin; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue(cr.getStepValue(stepSMA2));
                    }
                }
                break;

            // this uses 1 intermediate step: CMO
            case METHOD_VARIABLE:

                //setting step size 1
                for (int i = 0; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    cr.setStepSize(1);
                }
                // steps involved
                byte stepCMO = ChartRecord.STEP_1;

                ChandeMomentumOscillator.getChandeMomentumOscillator(al, 0, 9, getOHLCPriority(), stepCMO);

                loopBegin = 21;
                float SM = 2f / (timePeriods + 1f);
                for (int i = 0; i < loopBegin; i++) {
                    cr = (ChartRecord) al.get(i);
                    GDM.removeIndicatorPoint(cr.Time, index, getID());
                }
                // setting first value
                cr = (ChartRecord) al.get(loopBegin);
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                double preVal = cr.getValue(getOHLCPriority());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(preVal);
                }
                for (int i = loopBegin + 1; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    double VR = Math.abs(cr.getStepValue(stepCMO) / 100f);
                    preVal = SM * VR * cr.getValue(getOHLCPriority()) + (1 - SM * VR) * preVal;
                    aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                    if (aPoint != null) {
                        aPoint.setIndicatorValue(preVal);
                    }
                }
                break;

            // this uses 1 intermediate step: VolumeIncreament
            case METHOD_VOLUME_ADJUSTED:

                //setting step size 1
                for (int i = 0; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    cr.setStepSize(1);
                }
                // steps involved
                byte stepVolumeIncreament = ChartRecord.STEP_1;

                // calculating Average Volume Increament
                double volumeSum = 0;
                for (int i = 0; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    volumeSum += cr.Volume;
                }
                double avgVolumeIncreament = 0.67f * volumeSum / al.size();

                for (int i = 0; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    cr.setStepValue(stepVolumeIncreament, cr.Volume / avgVolumeIncreament);
                }

                for (int i = 0; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    double val = getVolumeAdjustedMA(al, timePeriods, i, 0, stepVolumeIncreament, getOHLCPriority());
                    //if (val==Indicator.NULL){
                    if (Double.isNaN(val) || (val == Indicator.NULL)) {
                        GDM.removeIndicatorPoint(cr.Time, index, getID());
                    } else {
                        aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                        if (aPoint != null) {
                            aPoint.setIndicatorValue(val);
                        }
                    }
                }
                break;
        }
        //System.out.println("**** MA Calc time " + (entryTime - System.currentTimeMillis()));
    }
    //############################################

/*
    private static double getVolumeAdjustedMA(ArrayList al, int timePeriods, int index, int bIndex, byte stepVolumeIncreament, byte srcIndex){
        double sumVI=0, sumVC=0d, currVI;
        int currIndex=index;
        ChartRecord cr;
        //System.out.println("####### "+System.currentTimeMillis()+" calculating VAMA for index "+index);
        while (sumVI<=timePeriods){
            if (currIndex<bIndex) return Indicator.NULL;
            cr = (ChartRecord)al.get(currIndex);
            currVI = cr.getStepValue(stepVolumeIncreament);
            if (sumVI+currVI>=timePeriods){
                sumVC += cr.getStepValue(srcIndex)*(timePeriods-sumVI);
                //System.out.println("$$$$$$$ returning close "+cr.Close+", currVI "+currVI+", sumVI "+sumVI+", SumVC "+sumVC+", value "+sumVC/timePeriods);
                return sumVC/timePeriods;
            }else{
                sumVC += cr.getStepValue(srcIndex)*currVI;
                //System.out.println("$$$$$$$ close "+cr.Close+", currVI "+currVI+", sumVI "+sumVI+", SumVC "+sumVC);
            }
            sumVI += currVI;
            currIndex--;
        }
        return Indicator.NULL;
    }
*/

    private static double getVolumeAdjustedMA(ArrayList al, int timePeriods, int index, int bIndex, byte stepVolumeIncreament, byte srcIndex) {
        double sumVI = 0, sumVC = 0d, currVI;
        int currIndex = index;
        ChartRecord cr;
        //System.out.println("####### "+System.currentTimeMillis()+" calculating VAMA for index "+index);
        while (sumVI <= timePeriods) {
            if (currIndex < bIndex) return Indicator.NULL;
            cr = (ChartRecord) al.get(currIndex);
            double val = cr.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    (Indicator.NULL != val)) {
                currVI = cr.getStepValue(stepVolumeIncreament);
                if (sumVI + currVI >= timePeriods) {
                    sumVC += val * (timePeriods - sumVI);
                    //System.out.println("$$$$$$$ returning close "+cr.Close+", currVI "+currVI+", sumVI "+sumVI+", SumVC "+sumVC+", value "+sumVC/timePeriods);
                    return sumVC / timePeriods;
                } else {
                    sumVC += val * currVI;
                    //System.out.println("$$$$$$$ close "+cr.Close+", currVI "+currVI+", sumVI "+sumVI+", SumVC "+sumVC);
                }
                sumVI += currVI;
            }
            currIndex--;
        }
        return Indicator.NULL;
    }

    protected double getWeightedMovAvg(ArrayList al, int count, int bIndex, int weight) {
        ChartRecord cr = (ChartRecord) al.get(bIndex - count);
        double val = 0;
        if (cr != null) {
            val = cr.getValue(this.getOHLCPriority()) * (timePeriods - count) / weight;
        }
        if (count >= timePeriods - 1) { // end of recursive call
            return val;
        } else { // calculate recursively
            return val + getWeightedMovAvg(al, count + 1, bIndex, weight);
        }
    }

    protected double getExpMovAvg(ArrayList al, double preVal, int bIndex, float ratio) {
        ChartRecord cr = (ChartRecord) al.get(bIndex);
        return cr.getValue(this.getOHLCPriority()) * ratio + (1 - ratio) * preVal;
    }
    //////////////////////////////////////////////

    public static String[] getMethodArray() {
        return new String[]{Language.getString("METHOD_SIMPLE"),
                Language.getString("METHOD_WEIGHTED")
                , Language.getString("METHOD_EXPONENTIAL")
                , Language.getString("METHOD_VARIABLE")
                , Language.getString("METHOD_TIME_SERIES")
                , Language.getString("METHOD_TRANGULAR")
                , Language.getString("METHOD_VOLUME_ADJUSTED")
        };
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    //method
    public byte getMethod() {
        return method;
    }

    public void setMethod(byte tp) {
        method = tp;
    }

    public String getShortName() {
        return Language.getString("MOVING_AVERAGE");
    }

    // #################### Important !!!!!!! ############################
    // this is to be used by all indicators with MA that has a user changeable "method" parameter
    // this has 2 intermediate steps (reserved) to be used by Time Series, Variable, Traingular and Volume Adjusted methods
    /* public static void getVariableMovingAverage(ArrayList al, int bIndex, int timePeriods, byte method, GraphDataManagerIF GDM,
                                                byte stepReserved1, byte stepReserved2, byte srcIndex, byte destIndex){
        switch (method){
            case METHOD_SIMPLE:
            case METHOD_WEIGHTED:
            case METHOD_EXPONENTIAL:
                getMovingAverage(al, bIndex, timePeriods, method, srcIndex, destIndex);
                return;
        }

        //long currentBeginTime;
        int loopBegin = bIndex+timePeriods;
        //double sumValue = 0;
        ChartRecord cr;

        long entryTime = System.currentTimeMillis();

        if (loopBegin>al.size()){
            for(int i=0; i<al.size(); i++){
                cr = (ChartRecord)al.get(i);
                cr.setStepValue(destIndex, Indicator.NULL);
            }
            return;
        }
        if (timePeriods<2){
            for(int i=0; i<al.size(); i++){
                cr = (ChartRecord)al.get(i);
                cr.cloneStep(srcIndex, destIndex);
            }
            return;
        }

        switch (method){

            case METHOD_TIME_SERIES:

                TimeSeriesForcast.getForcast(al, bIndex, timePeriods, GDM, srcIndex, destIndex);

                break;

                // this uses 1 intermediate step: SMA1 (SMA2 = destIndex)
            case METHOD_TRANGULAR:

                byte stepSMA1 = stepReserved1;

                //calculating triangular time periods
                int triangularPeriod = (int)Math.ceil(timePeriods/2f);
                MovingAverage.getMovingAverage(al, bIndex, triangularPeriod, METHOD_SIMPLE, srcIndex, stepSMA1);
                MovingAverage.getMovingAverage(al, bIndex+triangularPeriod-1, triangularPeriod, METHOD_SIMPLE, stepSMA1, destIndex);

                break;

                // this uses 1 intermediate step: CMO
            case METHOD_VARIABLE:

                byte stepCMO = stepReserved1;

                ChandeMomentumOscillator.getChandeMomentumOscillator(al, bIndex, 9, srcIndex, stepCMO);

                loopBegin = bIndex+21;
                float SM = 2f/(timePeriods+1f);
                for(int i=0; i<loopBegin; i++){
                    cr = (ChartRecord)al.get(i);
                    cr.setStepValue(destIndex, Indicator.NULL);
                }
                // setting first value
                cr = (ChartRecord)al.get(loopBegin);
                double preVal = cr.getStepValue(srcIndex);
                cr.setStepValue(destIndex, preVal);
                // calculating the rest
                for(int i=loopBegin+1; i<al.size(); i++){
                    cr = (ChartRecord)al.get(i);
                    double VR = Math.abs(cr.getStepValue(stepCMO)/100f);
                    preVal = SM*VR*cr.getStepValue(srcIndex) + (1-SM*VR)*preVal;
                    cr.setStepValue(destIndex, preVal);
                }
                break;

                // this uses 1 intermediate step: VolumeIncreament
            case METHOD_VOLUME_ADJUSTED:

                // steps involved
                byte stepVolumeIncreament = stepReserved1;

                // calculating Average Volume Increament
                double volumeSum = 0;
                for (int i=0; i<al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    volumeSum += cr.Volume;
                }
                double avgVolumeIncreament = 0.67f*volumeSum/al.size();

                for (int i=0; i<al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    cr.setStepValue(stepVolumeIncreament, cr.Volume/avgVolumeIncreament);
                }

                for(int i=0; i<al.size(); i++){
                    cr = (ChartRecord)al.get(i);
                    cr.setStepValue(destIndex, getVolumeAdjustedMA(al, timePeriods, i, bIndex, stepVolumeIncreament, srcIndex));
                }
                break;
        }
        System.out.println("**** Inter MA Calc time " + (entryTime - System.currentTimeMillis()));
    }*/

    public static void getVariableMovingAverage(ArrayList al, int bIndex, int timePeriods, byte method, GraphDataManagerIF GDM,
                                                byte stepReserved1, byte stepReserved2, byte srcIndex, byte destIndex) {
        switch (method) {
            case METHOD_SIMPLE:
            case METHOD_EXPONENTIAL:
            case METHOD_WEIGHTED:
                getMovingAverage(al, bIndex, timePeriods, method, srcIndex, destIndex);
                return;
        }

        //long currentBeginTime;
        int loopBegin = bIndex + timePeriods;
        //double sumValue = 0;
        ChartRecord cr;

        long entryTime = System.currentTimeMillis();

        if ((loopBegin > al.size()) || (timePeriods < 1)) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setStepValue(destIndex, Indicator.NULL);
            }
            return;
        }
        if (timePeriods < 2) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.cloneStep(srcIndex, destIndex);
            }
            return;
        }

        switch (method) {

            case METHOD_TIME_SERIES:

                TimeSeriesForcast.getForcast(al, bIndex, timePeriods, GDM, srcIndex, destIndex);

                break;

            // this uses 1 intermediate step: SMA1 (SMA2 = destIndex)
            case METHOD_TRANGULAR:

                byte stepSMA1 = stepReserved1;

                //calculating triangular time periods
                int triangularPeriod = (int) Math.ceil(timePeriods / 2f);
                MovingAverage.getMovingAverage(al, bIndex, triangularPeriod, METHOD_SIMPLE, srcIndex, stepSMA1);
                MovingAverage.getMovingAverage(al, bIndex + triangularPeriod - 1, triangularPeriod, METHOD_SIMPLE, stepSMA1, destIndex);

                break;

            // this uses 1 intermediate step: CMO
            case METHOD_VARIABLE:

                byte stepCMO = stepReserved1;

                ChandeMomentumOscillator.getChandeMomentumOscillator(al, bIndex, 9, srcIndex, stepCMO);

                loopBegin = bIndex + 21;
                /////////// this is done to avoid null points ////////////
                int tmpIndex = al.size();
                for (int i = loopBegin; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    double val = cr.getStepValue(srcIndex);
                    if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                            (Indicator.NULL != val)) {
                        tmpIndex = i;
                        break;
                    }
                }
                loopBegin = tmpIndex;
                //////////////////////////////////////////////////////////
                if (loopBegin >= al.size()) {
                    for (int i = 0; i < al.size(); i++) {
                        cr = (ChartRecord) al.get(i);
                        cr.setStepValue(destIndex, Indicator.NULL);
                    }
                    return;
                } else {
                    for (int i = 0; i < loopBegin; i++) {
                        cr = (ChartRecord) al.get(i);
                        cr.setStepValue(destIndex, Indicator.NULL);
                    }
                }
                float SM = 2f / (timePeriods + 1f);
                // setting first value
                cr = (ChartRecord) al.get(loopBegin);
                // no need to validata as loopBegin is already validated
                double preVal = cr.getStepValue(srcIndex);
                cr.setStepValue(destIndex, preVal);
                // calculating the rest
                for (int i = loopBegin + 1; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    double val = cr.getStepValue(srcIndex);
                    double cmo = cr.getStepValue(stepCMO);
                    if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) && (Indicator.NULL != val) &&
                            !Double.isNaN(cmo) && (Double.NEGATIVE_INFINITY != cmo) && (Double.POSITIVE_INFINITY != cmo) && (Indicator.NULL != cmo)) {
                        double VR = Math.abs(cmo / 100f);
                        preVal = SM * VR * val + (1 - SM * VR) * preVal;
                        cr.setStepValue(destIndex, preVal);
                    } else {
                        cr.setStepValue(destIndex, Indicator.NULL);
                    }
                }
                break;

            // this uses 1 intermediate step: VolumeIncreament
            case METHOD_VOLUME_ADJUSTED:

                // steps involved
                byte stepVolumeIncreament = stepReserved1;

                // calculating Average Volume Increament
                // RECHECK: should this start at 0 or bIndex??
                double volumeSum = 0;
                for (int i = 0; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    volumeSum += cr.Volume;
                }
                double avgVolumeIncreament = 0.67f * volumeSum / (al.size()); //- bIndex

                for (int i = 0; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    cr.setStepValue(stepVolumeIncreament, cr.Volume / avgVolumeIncreament);
                }
                for (int i = 0; i < bIndex; i++) {
                    cr = (ChartRecord) al.get(i);
                    cr.setStepValue(destIndex, Indicator.NULL);
                }
                // volume adjusted moving average
                for (int i = bIndex; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    cr.setStepValue(destIndex, getVolumeAdjustedMA(al, timePeriods, i, bIndex, stepVolumeIncreament, srcIndex));
                }
                break;
        }
        //System.out.println("**** Inter MA Calc time " + (entryTime - System.currentTimeMillis()));
    }

    // #################### Important !!!!!!! ############################
    //this is to be used only by Simple, Weighted and Exponential methods (Fixed Moving Averages with no steps)
    //this is to be used by other OHLC indicators those calculate a moving average as an intermediate step
    /*public static void getMovingAverage(ArrayList al, int bIndex, int timePeriods, byte method, byte srcIndex, byte destIndex){
        //long currentBeginTime;
        int loopBegin = bIndex+timePeriods;
        double sumValue = 0;
        ChartRecord cr, crOld;

        long entryTime = System.currentTimeMillis();

        if (loopBegin>al.size()){
            for(int i=0; i<al.size(); i++){
                cr = (ChartRecord)al.get(i);
                cr.setStepValue(destIndex, Indicator.NULL);
            }
            return;
        }
        if (timePeriods<2){
            for(int i=0; i<al.size(); i++){
                cr = (ChartRecord)al.get(i);
                cr.cloneStep(srcIndex, destIndex);
            }
            return;
        }

        switch (method){

            case METHOD_SIMPLE:

                for (int i=bIndex; i<loopBegin; i++) {
                    cr = (ChartRecord)al.get(i);
                    sumValue += cr.getStepValue(srcIndex);
                    if (i==loopBegin-1){
                        cr.setStepValue(destIndex, sumValue/timePeriods);
                    }else{
                        cr.setStepValue(destIndex, Indicator.NULL);
                    }
                }
                for(int i=loopBegin; i<al.size(); i++){
                    cr = (ChartRecord)al.get(i);
                    crOld = (ChartRecord)al.get(i-timePeriods);
                    sumValue += cr.getStepValue(srcIndex);
                    sumValue -= crOld.getStepValue(srcIndex);
                    cr.setStepValue(destIndex, sumValue/timePeriods);
                }
                break;

            case METHOD_WEIGHTED:

                loopBegin = bIndex+timePeriods-1;
                int weightSum = Math.round((timePeriods+1f)*timePeriods/2f);

                for(int i=loopBegin; i<al.size(); i++){
                    cr = (ChartRecord)al.get(i);
                    cr.setStepValue(destIndex, getWeightedMovAvg(al, 0, i, weightSum, timePeriods, srcIndex));
                }
                for(int i=0; i<loopBegin; i++){
                    cr = (ChartRecord)al.get(i);
                    cr.setStepValue(destIndex, Indicator.NULL);
                }
                break;

            case METHOD_EXPONENTIAL:

                loopBegin = bIndex;
                cr = (ChartRecord)al.get(loopBegin);
                double prevValue = cr.getStepValue(srcIndex);
                float expPercentage = 2f/(timePeriods+1f);

                for(int i=loopBegin+1; i<al.size(); i++){
                    cr = (ChartRecord)al.get(i);
                    prevValue = getExpMovAvgExt(al, prevValue, i, expPercentage, srcIndex);
                    if (i<bIndex+timePeriods-1){
                        cr.setStepValue(destIndex, Indicator.NULL);
                    }else{
                        cr.setStepValue(destIndex, prevValue);
                    }
                }
                for(int i=0; i<=loopBegin; i++){
                    cr = (ChartRecord)al.get(i);
                    cr.setStepValue(destIndex, Indicator.NULL);
                }
                break;
        }
//        System.out.println("**** Inter MA Calc time " + (entryTime - System.currentTimeMillis()));
    }
*/


    public static void getMovingAverage(ArrayList al, int bIndex, int timePeriods, byte method, byte srcIndex, byte destIndex) {
        //long currentBeginTime;
        int loopBegin = bIndex + timePeriods;
        double sumValue = 0;
        ChartRecord cr, crOld;

        long entryTime = System.currentTimeMillis();

        if (loopBegin > al.size()) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setStepValue(destIndex, Indicator.NULL);
            }
            return;
        }
        if (timePeriods < 2) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.cloneStep(srcIndex, destIndex);
            }
            return;
        }

        switch (method) {

            case METHOD_SIMPLE:

                for (int i = bIndex; i < loopBegin; i++) {
                    cr = (ChartRecord) al.get(i);
                    double val = cr.getStepValue(srcIndex);
                    if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                            (Indicator.NULL != val)) {
                        sumValue += val;
                    }
                    if (i == loopBegin - 1) {
                        cr.setStepValue(destIndex, sumValue / timePeriods);
                    } else {
                        cr.setStepValue(destIndex, Indicator.NULL);
                    }
                }
                for (int i = loopBegin; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    crOld = (ChartRecord) al.get(i - timePeriods);
                    double val = cr.getStepValue(srcIndex);
                    if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                            (Indicator.NULL != val)) {
                        sumValue += val;
                    }
                    val = crOld.getStepValue(srcIndex);
                    if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                            (Indicator.NULL != val)) {
                        sumValue -= val;
                    }
                    cr.setStepValue(destIndex, sumValue / timePeriods);
                }
                break;

            case METHOD_WEIGHTED:

                loopBegin = bIndex + timePeriods - 1;
                int weightSum = (int) Math.round((timePeriods + 1f) * timePeriods / 2f);

                for (int i = loopBegin; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    cr.setStepValue(destIndex, getWeightedMovAvg(al, 0, i, weightSum, timePeriods, srcIndex));
                }
                for (int i = 0; i < loopBegin; i++) {
                    cr = (ChartRecord) al.get(i);
                    cr.setStepValue(destIndex, Indicator.NULL);
                }
                break;

            case METHOD_EXPONENTIAL:

                loopBegin = bIndex;
                cr = (ChartRecord) al.get(loopBegin);
                // no need to validate as bIndex is already validated
                double prevValue = cr.getStepValue(srcIndex);
                float expPercentage = 2f / (timePeriods + 1f);

                for (int i = loopBegin + 1; i < al.size(); i++) {
                    cr = (ChartRecord) al.get(i);
                    double val = getExpMovAvgExt(al, prevValue, i, expPercentage, srcIndex);
                    if ((Indicator.NULL == val) || Double.isNaN(val)) {
                        cr.setStepValue(destIndex, Indicator.NULL);
                    } else {
                        if (i < bIndex + timePeriods - 1) {
                            cr.setStepValue(destIndex, Indicator.NULL);
                        } else {
                            prevValue = val;
                            cr.setStepValue(destIndex, prevValue);
                        }
                    }
                }
                for (int i = 0; i <= loopBegin; i++) {
                    cr = (ChartRecord) al.get(i);
                    cr.setStepValue(destIndex, Indicator.NULL);
                }
                break;
        }
        //System.out.println("**** Inter MA Calc time " + (entryTime - System.currentTimeMillis()));
    }

    /*protected static double getWeightedMovAvg(ArrayList al, int count, int bIndex, int weight, int timePeriods, byte srcIndex){
        ChartRecord cr = (ChartRecord)al.get(bIndex-count);
        double val = cr.getStepValue(srcIndex);
        val = val*(timePeriods-count)/weight;
        if (count>=timePeriods-1) { // end of recursive call
            return val;
        } else { // calculate recursively
            return val + getWeightedMovAvg(al, count+1, bIndex, weight, timePeriods, srcIndex);
        }
    }*/
    protected static double getWeightedMovAvg(ArrayList al, int count, int bIndex, int weight, int timePeriods, byte srcIndex) {
        ChartRecord cr = (ChartRecord) al.get(bIndex - count);
        double val = cr.getStepValue(srcIndex);
        if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                (Indicator.NULL != val)) {
            val = val * (timePeriods - count) / weight;
        } else {
            val = 0;
        }
        if (count >= timePeriods - 1) { // end of recursive call
            return val;
        } else { // calculate recursively
            return val + getWeightedMovAvg(al, count + 1, bIndex, weight, timePeriods, srcIndex);
        }
    }

/*
    protected static double getExpMovAvgExt(ArrayList al, double preVal, int bIndex, float ratio, byte srcIndex){
        ChartRecord cr = (ChartRecord)al.get(bIndex);
        double val = cr.getStepValue(srcIndex);
        return val*ratio + (1-ratio)*preVal;
    }
*/

    protected static double getExpMovAvgExt(ArrayList al, double preVal, int bIndex, float ratio, byte srcIndex) {
        ChartRecord cr = (ChartRecord) al.get(bIndex);
        double val = cr.getStepValue(srcIndex);
        if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                (Indicator.NULL != val)) {
            return val * ratio + (1 - ratio) * preVal;
        } else {
            return Indicator.NULL;
        }
    }


    /**
     * NOTE: For Radar Screen
     *
     * @param ll
     * @param timePeriods
     * @param method
     * @param srcIndex
     * @param destIndex
     * @return movingAvg
     */
    public static double getMovingAverage(LinkedList<ChartRecord> ll, int timePeriods,
                                          byte method, byte srcIndex, byte destIndex) {
        double sumValue = 0;
        double indValue = 0.0;
        ChartRecord cr, crOld;

        if (timePeriods < 2) {
            for (Object aLl : ll) {
                cr = (ChartRecord) aLl;
                cr.cloneStep(srcIndex, destIndex);
            }
            return Indicator.NULL;
        }

        int bIndex = 0;
        int loopBegin = bIndex + timePeriods;
        switch (method) {
            case METHOD_SIMPLE:
                for (int i = bIndex; i < loopBegin; i++) {
                    cr = (ChartRecord) ll.get(i);
                    double val = cr.getStepValue(srcIndex);
                    if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                            (Indicator.NULL != val)) {
                        sumValue += val;
                }
                    if (i == loopBegin - 1) {
                        cr.setStepValue(destIndex, sumValue / timePeriods);
                    } else {
                        cr.setStepValue(destIndex, Indicator.NULL);
                    }
                }
                for (int i = loopBegin; i < ll.size(); i++) {
                    cr = (ChartRecord) ll.get(i);
                    crOld = (ChartRecord) ll.get(i - timePeriods);
                    double val = cr.getStepValue(srcIndex);
                    if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                            (Indicator.NULL != val)) {
                        sumValue += val;
                    }
                    val = crOld.getStepValue(srcIndex);
                    if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                            (Indicator.NULL != val)) {
                        sumValue -= val;
                    }
                    cr.setStepValue(destIndex, sumValue / timePeriods);

                }

                indValue = ll.get(ll.size() - 1).getStepValue(destIndex);
                break;

            case METHOD_WEIGHTED:
                /*int weightSum = Math.round((timePeriods + 1f) * timePeriods / 2f);
                for (int i = (timePeriods - 1); i < ll.size(); i++) {
                    cr = ll.get(i);
                    cr.setStepValue(destIndex, getWeightedMovAvg(ll, 0, i, weightSum, timePeriods, srcIndex));
                }
                indValue = ll.get(ll.size() - 1).getStepValue(destIndex);*/

                loopBegin = bIndex + timePeriods - 1;
                int weightSum = (int) Math.round((timePeriods + 1f) * timePeriods / 2f);

                for (int i = loopBegin; i < ll.size(); i++) {
                    cr = (ChartRecord) ll.get(i);
                    cr.setStepValue(destIndex, getWeightedMovAvg(ll, 0, i, weightSum, timePeriods, srcIndex));
                }
                for (int i = 0; i < loopBegin; i++) {
                    cr = (ChartRecord) ll.get(i);
                    cr.setStepValue(destIndex, Indicator.NULL);
                }
                indValue = ll.get(ll.size() - 1).getStepValue(destIndex);

                break;

            case METHOD_EXPONENTIAL:
                /*cr = ll.get(0);
                double prevValue = cr.getStepValue(srcIndex);
                float expPercentage = 2f / (timePeriods + 1f);
                for (int i = 1; i < ll.size(); i++) {
                    cr = ll.get(i);
                    prevValue = getExpMovAvgExt(ll, prevValue, i, expPercentage, srcIndex);
                    cr.setStepValue(destIndex, prevValue);
                }
                indValue = ll.get(ll.size() - 1).getStepValue(destIndex);*/

                loopBegin = bIndex;
                cr = (ChartRecord) ll.get(loopBegin);
                // no need to validate as bIndex is already validated
                double prevValue = cr.getStepValue(srcIndex);
                float expPercentage = 2f / (timePeriods + 1f);

                for (int i = loopBegin + 1; i < ll.size(); i++) {
                    cr = (ChartRecord) ll.get(i);
                    double val = getExpMovAvgExt(ll, prevValue, i, expPercentage, srcIndex);
                    if ((Indicator.NULL == val) || Double.isNaN(val)) {
                        cr.setStepValue(destIndex, Indicator.NULL);
                    } else {
                        if (i < bIndex + timePeriods - 1) {
                            cr.setStepValue(destIndex, Indicator.NULL);
                        } else {
                            prevValue = val;
                            cr.setStepValue(destIndex, prevValue);
                        }
                    }
                }
                for (int i = 0; i <= loopBegin; i++) {
                    cr = (ChartRecord) ll.get(i);
                    cr.setStepValue(destIndex, Indicator.NULL);
                }
                indValue = ll.get(ll.size() - 1).getStepValue(destIndex);
                break;
        }

        return indValue;
    }

    protected static double getWeightedMovAvg(LinkedList<ChartRecord> ll, int count, int bIndex,
                                              int weight, int timePeriods, byte srcIndex) {
        /*ChartRecord cr = ll.get(bIndex - count);
        double val = cr.getStepValue(srcIndex);
        val = val * (timePeriods - count) / weight;
        if (count >= timePeriods - 1) { // end of recursive call
            return val;
        } else { // calculate recursively
            return val + getWeightedMovAvg(ll, count + 1, bIndex, weight, timePeriods, srcIndex);
        }*/

        ChartRecord cr = (ChartRecord) ll.get(bIndex - count);
        double val = cr.getStepValue(srcIndex);
        if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                (Indicator.NULL != val)) {
            val = val * (timePeriods - count) / weight;
        } else {
            val = 0;
        }
        if (count >= timePeriods - 1) { // end of recursive call
            return val;
        } else { // calculate recursively
            return val + getWeightedMovAvg(ll, count + 1, bIndex, weight, timePeriods, srcIndex);
        }

    }

    protected static double getExpMovAvgExt(LinkedList ll, double preVal, int bIndex, float ratio, byte srcIndex) {
        /*ChartRecord cr = (ChartRecord) ll.get(bIndex);
        double val = cr.getStepValue(srcIndex);
        return val * ratio + (1 - ratio) * preVal;*/

        ChartRecord cr = (ChartRecord) ll.get(bIndex);
        double val = cr.getStepValue(srcIndex);
        if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                (Indicator.NULL != val)) {
        return val * ratio + (1 - ratio) * preVal;
        } else {
            return Indicator.NULL;
        }

    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

    // To be used by custom indicators
    /*public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int timePeriods,
                  MAMethod method, byte stepReserved1, byte stepReserved2, byte srcIndex, byte destIndex) {
        int tempIndex = 0;
        for(int i=0; i<al.size(); i++){
            ChartRecord cR = (ChartRecord)al.get(i);
            double d = cR.getStepValue(srcIndex);
            if ((d != Indicator.NULL) && !Double.isNaN(d)) {
                tempIndex = i;
                break;
            }
        }
        bIndex = Math.max(tempIndex, bIndex);
        MovingAverage.getVariableMovingAverage(al, bIndex, timePeriods, (byte)method.ordinal(), GDM, stepReserved1, stepReserved2, srcIndex, destIndex);
    }*/
    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int timePeriods,
                                          MAMethod method, byte stepReserved1, byte stepReserved2, byte srcIndex, byte destIndex) {
        /////////// this is done to avoid null points ////////////
        int tmpIndex = al.size();
        for (int i = bIndex; i < al.size(); i++) {
            ChartRecord cr = (ChartRecord) al.get(i);
            double val = cr.getStepValue(srcIndex);
            if (!Double.isNaN(val) && (Double.NEGATIVE_INFINITY != val) && (Double.POSITIVE_INFINITY != val) &&
                    !Double.isInfinite(val) && (Indicator.NULL != val)) {
                tmpIndex = i;
                break;
            }
        }
        bIndex = tmpIndex;
        //////////////////////////////////////////////////////////
        if (bIndex >= al.size()) {
            for (int i = 0; i < al.size(); i++) {
                ChartRecord cr = (ChartRecord) al.get(i);
                cr.setStepValue(destIndex, Indicator.NULL);
            }
        } else {
            MovingAverage.getVariableMovingAverage(al, bIndex, timePeriods, (byte) method.ordinal(), GDM, stepReserved1, stepReserved2, srcIndex, destIndex);
        }
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 2;
    }

}