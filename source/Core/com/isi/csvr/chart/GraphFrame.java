/*
* Classname:     GraphFrame.java
* @author Udaka Liyanapathirana
* Version:       Chart 1.0_beta
* Date:          22nd August 2001
* Copyright:     (c) 2001 Integrated Systems International (Pvt)Ltd. All Rights Reserved.
*/

package com.isi.csvr.chart;

import com.isi.csvr.*;
import com.isi.csvr.chart.analysistechniques.AnalysisTechWindow;
import com.isi.csvr.chart.customindicators.IndicatorMenuObject;
import com.isi.csvr.chart.options.ChartConstants;
import com.isi.csvr.chart.options.*;
import com.isi.csvr.chart.chartobjects.*;
import com.isi.csvr.event.ConnectionListener;
import com.isi.csvr.event.ConnectionNotifier;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.iframe.TWDesktopInterface;
import com.isi.csvr.iframe.WindowWrapper;
import com.isi.csvr.linkedwindows.LinkStore;
import com.isi.csvr.ohlc.OHLCStore;
import com.isi.csvr.print.PrintPreview;
import com.isi.csvr.properties.ViewConstants;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.shared.*;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.Table;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.theme.Themeable;
import com.isi.csvr.trading.connection.TradingConnectionNotifier;
import com.isi.csvr.trading.datastore.OrderStore;
import com.isi.csvr.trading.shared.TradeMeta;
import com.isi.util.ColumnLayout;
import com.sun.image.codec.jpeg.JPEGCodec;
import com.sun.image.codec.jpeg.JPEGEncodeParam;
import com.sun.image.codec.jpeg.JPEGImageEncoder;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.plaf.basic.BasicInternalFrameUI;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import javax.swing.table.TableColumnModel;
import java.awt.*;
import java.awt.datatransfer.DataFlavor;
import java.awt.dnd.DropTargetDragEvent;
import java.awt.dnd.DropTargetDropEvent;
import java.awt.dnd.DropTargetEvent;
import java.awt.dnd.DropTargetListener;
import java.awt.event.*;
import java.awt.geom.Point2D;
import java.awt.geom.QuadCurve2D;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Printable;
import java.awt.print.PrinterException;
import java.beans.PropertyVetoException;
import java.io.*;
import java.text.DecimalFormat;
import java.util.*;
import java.util.List;
import java.math.BigInteger;


public class GraphFrame extends InternalFrame
        implements Serializable, WindowWrapper, ToolTipChangeListener, ConnectionListener,
        Printable, DropTargetListener, GraphMouseListener, Themeable, ActionListener, PopupMenuListener,
        ComponentListener, TWDesktopInterface, MouseWheelListener, DragWindowListner, DataWindowTableListener {

    private int windowIndex = TWDesktopInterface.UNASSIGNED;
    private int windowType;

    private final int INDEX_VOLUME = 0;
    private final int INDEX_MOVING_AVG = 1;
    private final int INDEX_RSI = 2;
    private final int INDEX_MACD = 3;
    private final int INDEX_MOMENTUM = 4;
    private final int INDEX_LRI = 5;
    private final int INDEX_SDI = 6;
    private final int INDEX_BOLLINGER_BANDS = 7;
    private final int INDEX_ACCUM_DISTRI = 8;
    private final int INDEX_WILLIAMS_R = 9;
    private final int INDEX_STOCAS_OSCIL = 10;
    private final int INDEX_AVG_TRUE_RANGE = 11;
    private final int INDEX_MASS_INDEX = 12;
    private final int INDEX_MEDIAN_PRICE = 13;
    private final int INDEX_MONEY_FLOW_INDEX = 14;
    private final int INDEX_TRIX = 15;
    private final int INDEX_VOLATILITY = 16;
    private final int INDEX_ZIGZAG = 17;
    private final int INDEX_RMI = 18;
    private final int INDEX_AROON = 19;
    private final int INDEX_CHAIKINS_OSCILL = 20;
    private final int INDEX_CCI = 21;
    private final int INDEX_CHAIKINS_MONEY_FLOW = 22;
    private final int INDEX_ICHIMOKU_KINKO_HYO = 23;
    private final int INDEX_ENVILOPES = 24;
    private final int INDEX_KLINGER_OSCILL = 25;
    private final int INDEX_PARABOLIC_SAR = 26;
    private final int INDEX_PRICE_VOLUME_TREND = 27;
    private final int INDEX_VOLUME_OSCILL = 28;
    private final int INDEX_WILLS_ACCU_DISTRI = 29;
    private final int INDEX_PRICE_OSCILL = 30;
    private final int INDEX_INTRADAY_MOMENTUM_INDEX = 31;
    private final int INDEX_ON_BALANCE_VOLUME = 32;
    private final int INDEX_ROC = 33;
    private final int INDEX_WILDERS_SMOOTHING = 34;
    private final int INDEX_PLUS_DI = 35;
    private final int INDEX_MINUS_DI = 36;
    private final int INDEX_DX = 37;
    private final int INDEX_ADX = 38;
    private final int INDEX_ADXR = 39;

    private final int INDEX_DPO = 40;
    private final int INDEX_SWING = 41;
    private final int INDEX_RVI = 42;
    private final int INDEX_MFI = 43;
    private final int INDEX_PERFORMANCE = 44;
    private final int INDEX_PVI = 45;
    private final int INDEX_QSTICK = 46;
    private final int INDEX_TYPICAL_PRICE = 47;
    private final int INDEX_VERT_HORIZ_FILTER = 48;
    private final int INDEX_WEIGHTED_CLOSE = 49;
    private final int INDEX_VOLUME_ROC = 50;
    private final int INDEX_DEMA = 51;
    private final int INDEX_PRICE_CHANNEL = 52;
    private final int INDEX_TSF = 53;
    private final int INDEX_FO = 54;
    private final int INDEX_NVI = 55;
    private final int INDEX_CMO = 56;
    private final int INDEX_SMO = 57;
    private final int INDEX_EASE_OF_MOVEMENT = 58;
    private final int INDEX_TEMA = 59;
    private final int INDEX_STD_ERR = 60;
    private final int INDEX_MACD_VOLUME = 61;
    private final int INDEX_NET_ORDERS = 62;
    private final int INDEX_NET_TURNOVER = 63;
    private final int INDEX_ALLIGATOR = 64;
    private final int INDEX_COPP_CURVE = 65;
    private final int INDEX_DISPARITY_IDX = 66;
    private final int INDEX_HULL_MOVING_AVG = 67;
    private final int INDEX_KELTNER_CHANNEL = 68;

    final float[] dashArr = {2.0f, 2.0f};
    final BasicStroke BS_1p0f_2by2_dashed = new BasicStroke(1.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 1.0f, dashArr, 1.0f);
    //public static String filePathCharts = "./charts/";
    //public static final String FILEPATH_CHART_TEMPLATES = "./charts/";
    public static String filePathCharts = Settings.CHART_DATA_PATH + "/userdata";
    public static final String FILEPATH_CHART_TEMPLATES = Settings.CHART_DATA_PATH + "/userdata";

    private final byte ReqID_BaseSymbol = 0;
    private final byte ReqID_CompareSymbol = 1;
    private boolean ZoomDragEnabled = false;
    private boolean detached = false;
    private static int Left = 5;
    protected DataWindowListener dwListener = null;
    protected DragWindowListner dragWindowListener = null;
    protected DataWindowTableListener dwTableListener = null;

    private ArrayList alLayuotIDs = new ArrayList();

    // vars for initiallizing (to hold the data taken from ini file)
    public String[] strSymbols;
    public String[] strCompareSymbols;
    String[] strCurrentIntervals;
    String[] strHistoryIntervals;
    String[] strGraphStyles;
    String[] strHistoryPeriods;
    String[] strCurrentPeriods;
    Color[] graphColors;
    SymbolsEx g_oBaseSymbols = new SymbolsEx((byte) Meta.INSTRUMENT_QUOTE);
    SymbolsEx g_oComparedSymbols = new SymbolsEx((byte) Meta.INSTRUMENT_QUOTE);
    ViewSetting g_oViewSettings;
    private String fileName = null;
    public boolean tableMode = false;

    public static HashMap<String, Integer> indicatorMap; //Added to sort the indicator subMenu and preserve
    private String tmpLayoutName = null;
    // the existing architecture as much as possible! - Pramoda
    public boolean isSendOHLCRequest = true;
    private DragWindow dragWindow = new DragWindow(this);
    public boolean isFibCalMode = false;
    public boolean isErasorMode = false; //TODO use get & set methods
    public boolean isRectangularSelection = false;

    public int clickCount = 0;
    public double[] clickPoints = new double[3];
    private FibonacciPriceCalculator calculator = null;

    protected int noOfMousePressed = 0;
    private boolean isCashFlowMode = false;

    private boolean usingDefaultTemplate = true;
    public boolean isHideAll;
    private final int NO_OF_ROWS_ADJUSTED = 10;

    public boolean isFibCalMode() {
        return isFibCalMode;
    }

    public boolean isErasorMode() {
        return isErasorMode;
    }

    public void setFibCalMode(boolean fibCalMode) {
        isFibCalMode = fibCalMode;
    }

    public void resetClickCount() {
        this.clickCount = 0;
    }

    public static String[] getSymbols() {

        String[] sa = new String[0];//{"NSDQ~MSFT","NYSE~IBM","NSDQ~DELL","NSDQ~ORCL","NSDQ~INTC"}; //
        return sa;
    }

    /**
     * When adding a new indicator, entries should go to follwoing 2 methods And the
     * CustomDialogs.getInsertIndicatorsDialog() method also should be updated.
     *
     * @return string[]
     */
    public static String[] getIndicators() {
        String[] sa = {Language.getString("MOVING_AVERAGE"), //0
                Language.getString("INDI_RSI"), //1
                Language.getString("MACD"), //2
                Language.getString("MOMENTUM"), //3
                Language.getString("LRI"), //4
                Language.getString("STD_DEVIATION"), //5
                Language.getString("BOLLINGER_BANDS"), //6
                Language.getString("IND_ACCUM_DISTRI"), //7
                Language.getString("IND_WILLS_R"), //8
                Language.getString("IND_STOCHAS_OSCIL"), //9
                Language.getString("IND_AVG_TRUE_RANGE"), //10
                Language.getString("IND_MASS_INDEX"), //11
                Language.getString("IND_MEDIAN_PRICE"), //12
                Language.getString("IND_MONEY_FLOW"), //13
                Language.getString("IND_TRIX"), //14
                Language.getString("IND_VOLATILITY"), //15
                Language.getString("IND_ZIGZAG"), //16
                Language.getString("IND_RMI"), //17
                Language.getString("IND_AROON"), //18
                Language.getString("IND_CHAIKINS_OSCILL"), //19
                Language.getString("IND_CCI"), //20
                Language.getString("IND_CHAIKINS_MONEY_FLOW"), //21
                Language.getString("IND_ICHIMOKU_KINKO_HYO"), //22
                Language.getString("IND_ENVILOPES"), //23
                Language.getString("IND_KLINGER_OSCILL"), //24
                Language.getString("IND_PARABOLIC_SAR"), //25
                Language.getString("IND_PRICE_VOLUME_TREND"), //26
                Language.getString("IND_VOLUME_OSCILL"), //27
                Language.getString("IND_WILLS_ACCU_DISTRI"), //28
                Language.getString("IND_PRICE_OSCILL"), //29
                Language.getString("IND_IMI"), //30
                Language.getString("IND_OBV"), //31
                Language.getString("IND_ROC"), //32
                Language.getString("IND_WILDERS_SMOOTHING"), //33
                Language.getString("IND_PLUS_DI"), //34
                Language.getString("IND_MINUS_DI"), //35
                Language.getString("IND_DX"), //36
                Language.getString("IND_ADX"), //37
                Language.getString("IND_ADXR"), //38
                Language.getString("IND_DPO"), //39
                Language.getString("IND_SWING_INDEX"), //40
                Language.getString("IND_RVI"), //41
                Language.getString("IND_MFI"), //42
                Language.getString("IND_PERFORMANCE"), //43
                Language.getString("IND_PVI"), //44
                Language.getString("IND_QSTICK"), //45
                Language.getString("IND_TYPICAL_PRICE"), //46
                Language.getString("IND_VERT_HORI_FILTER"), //47
                Language.getString("IND_WEIGHTED_CLOSE"), //48
                Language.getString("IND_VOLUME_ROC"), //49
                Language.getString("IND_DEMA"), //50
                Language.getString("IND_PRICE_CHANNEL"), //51
                Language.getString("IND_TSF"), //52
                Language.getString("IND_FO"), //53
                Language.getString("IND_NVI"), //54
                Language.getString("IND_CMO"), //55
                Language.getString("IND_SMO"), //56
                Language.getString("IND_EASE_OF_MOVEMENT"), //57
                Language.getString("IND_TEMA"), //58
                Language.getString("IND_STD_ERR"), //59
                Language.getString("IND_MACD_VOLUME"), //60
                Language.getString("IND_NET_ORDERS"), //61
                Language.getString("IND_NET_TURNOVER"), //62
                Language.getString("IND_ALLIGATOR"), //64
                Language.getString("IND_COPPOCK_CURVE"), //65
                Language.getString("IND_DISPARITY_INDEX"), //66
                Language.getString("IND_HULL_MOVING_AVERAGE"), //67
                Language.getString("IND_KELTNER_CHANNELS") //68

        };

        // Following code is to preserve the original index and sort - Pramoda
        indicatorMap = new HashMap<String, Integer>();
        for (int i = 0; i < sa.length; i++) {
            indicatorMap.put(sa[i], new Integer(i));
        }
        Arrays.sort(sa);

        return sa;
    }

    public static HashMap<String, Byte> getIndicatorStringAndID() {
        HashMap<String, Byte> indicators = new HashMap<String, Byte>();
        indicators.put(Language.getString("MOVING_AVERAGE"), GraphDataManager.ID_MOVING_AVERAGE);
        indicators.put(Language.getString("INDI_RSI"), GraphDataManager.ID_RSI);
        indicators.put(Language.getString("MACD"), GraphDataManager.ID_MACD);
        indicators.put(Language.getString("MOMENTUM"), GraphDataManager.ID_MOMENTUM);
        indicators.put(Language.getString("LRI"), GraphDataManager.ID_LRI);
        indicators.put(Language.getString("STD_DEVIATION"), GraphDataManager.ID_SDI);
        indicators.put(Language.getString("BOLLINGER_BANDS"), GraphDataManager.ID_BOLLINGER_BANDS);
        indicators.put(Language.getString("IND_ACCUM_DISTRI"), GraphDataManager.ID_ACCU_DISTRI);
        indicators.put(Language.getString("IND_WILLS_R"), GraphDataManager.ID_WILLS_R);
        indicators.put(Language.getString("IND_STOCHAS_OSCIL"), GraphDataManager.ID_STOCHAST_OSCILL);
        indicators.put(Language.getString("IND_AVG_TRUE_RANGE"), GraphDataManager.ID_AVG_TRUE_RANGE);
        indicators.put(Language.getString("IND_MASS_INDEX"), GraphDataManager.ID_MASS_INDEX);
        indicators.put(Language.getString("IND_MEDIAN_PRICE"), GraphDataManager.ID_MEDIAN_PRICE);
        indicators.put(Language.getString("IND_MONEY_FLOW"), GraphDataManager.ID_MONEY_FLOW_INDEX);
        indicators.put(Language.getString("IND_TRIX"), GraphDataManager.ID_TRIX);
        indicators.put(Language.getString("IND_VOLATILITY"), GraphDataManager.ID_VOLATILITY);
        indicators.put(Language.getString("IND_ZIGZAG"), GraphDataManager.ID_ZIGZAG);
        indicators.put(Language.getString("IND_RMI"), GraphDataManager.ID_RMI);
        indicators.put(Language.getString("IND_AROON"), GraphDataManager.ID_AROON);
        indicators.put(Language.getString("IND_CHAIKINS_OSCILL"), GraphDataManager.ID_CHAIKINS_OSCILL);
        indicators.put(Language.getString("IND_CCI"), GraphDataManager.ID_CCI);
        indicators.put(Language.getString("IND_CHAIKINS_MONEY_FLOW"), GraphDataManager.ID_CHAIKIN_MONEY_FLOW);
        indicators.put(Language.getString("IND_ICHIMOKU_KINKO_HYO"), GraphDataManager.ID_ICHIMOKU_KINKO_HYO);
        indicators.put(Language.getString("IND_ENVILOPES"), GraphDataManager.ID_ENVILOPES);
        indicators.put(Language.getString("IND_KLINGER_OSCILL"), GraphDataManager.ID_KLINGER_OSCILL);
        indicators.put(Language.getString("IND_PARABOLIC_SAR"), GraphDataManager.ID_PARABOLIC_SAR);
        indicators.put(Language.getString("IND_PRICE_VOLUME_TREND"), GraphDataManager.ID_PRICE_VOLUME_TREND);
        indicators.put(Language.getString("IND_VOLUME_OSCILL"), GraphDataManager.ID_VOLUME_OSCILL);
        indicators.put(Language.getString("IND_WILLS_ACCU_DISTRI"), GraphDataManager.ID_WILLS_ACCU_DISTRI);
        indicators.put(Language.getString("IND_PRICE_OSCILL"), GraphDataManager.ID_PRICE_OSCILL);
        indicators.put(Language.getString("IND_IMI"), GraphDataManager.ID_INTRADAY_MOMENTUM_INDEX);
        indicators.put(Language.getString("IND_OBV"), GraphDataManager.ID_ON_BALANCE_VOLUME);
        indicators.put(Language.getString("IND_ROC"), GraphDataManager.ID_ROC);
        indicators.put(Language.getString("IND_WILDERS_SMOOTHING"), GraphDataManager.ID_WILDERS_SMOOTHING);
        indicators.put(Language.getString("IND_PLUS_DI"), GraphDataManager.ID_PLUS_DI);
        indicators.put(Language.getString("IND_MINUS_DI"), GraphDataManager.ID_MINUS_DI);
        indicators.put(Language.getString("IND_DX"), GraphDataManager.ID_DX);
        indicators.put(Language.getString("IND_ADX"), GraphDataManager.ID_ADX);
        indicators.put(Language.getString("IND_ADXR"), GraphDataManager.ID_ADXR);
        indicators.put(Language.getString("IND_DPO"), GraphDataManager.ID_DPO);
        indicators.put(Language.getString("IND_SWING_INDEX"), GraphDataManager.ID_SWING_INDEX);
        indicators.put(Language.getString("IND_RVI"), GraphDataManager.ID_RVI);
        indicators.put(Language.getString("IND_MFI"), GraphDataManager.ID_MFI);
        indicators.put(Language.getString("IND_PERFORMANCE"), GraphDataManager.ID_PERFORMANCE);
        indicators.put(Language.getString("IND_PVI"), GraphDataManager.ID_PVI);
        indicators.put(Language.getString("IND_QSTICK"), GraphDataManager.ID_QSTICK);
        indicators.put(Language.getString("IND_TYPICAL_PRICE"), GraphDataManager.ID_TYPICAL_PRICE);
        indicators.put(Language.getString("IND_VERT_HORI_FILTER"), GraphDataManager.ID_VERT_HORI_FILTER);
        indicators.put(Language.getString("IND_WEIGHTED_CLOSE"), GraphDataManager.ID_WEIGHTED_CLOSE);
        indicators.put(Language.getString("IND_VOLUME_ROC"), GraphDataManager.ID_VOLUME_ROC);
        indicators.put(Language.getString("IND_DEMA"), GraphDataManager.ID_DEMA);
        indicators.put(Language.getString("IND_PRICE_CHANNEL"), GraphDataManager.ID_PRICE_CHANNELS);
        indicators.put(Language.getString("IND_TSF"), GraphDataManager.ID_TSF);
        indicators.put(Language.getString("IND_FO"), GraphDataManager.ID_FO);
        indicators.put(Language.getString("IND_NVI"), GraphDataManager.ID_NVI);
        indicators.put(Language.getString("IND_CMO"), GraphDataManager.ID_CMO);
        indicators.put(Language.getString("IND_SMO"), GraphDataManager.ID_SMO);
        indicators.put(Language.getString("IND_EASE_OF_MOVEMENT"), GraphDataManager.ID_EASE_OF_MOVEMENT);
        indicators.put(Language.getString("IND_TEMA"), GraphDataManager.ID_TEMA);
        indicators.put(Language.getString("IND_STD_ERR"), GraphDataManager.ID_STD_ERR);
        indicators.put(Language.getString("IND_MACD_VOLUME"), GraphDataManager.ID_MACD_VOLUME);
        indicators.put(Language.getString("IND_NET_ORDERS"), GraphDataManager.ID_NET_ORDERS);
        indicators.put(Language.getString("IND_NET_TURNOVER"), GraphDataManager.ID_NET_TURNOVER);
        indicators.put(Language.getString("IND_ALLIGATOR"), GraphDataManager.ID_ALLIGATOR);
        indicators.put(Language.getString("IND_COPPOCK_CURVE"), GraphDataManager.ID_COPP_CURVE);
        indicators.put(Language.getString("IND_DISPARITY_INDEX"), GraphDataManager.ID_DISPARITY_INDEX);
        indicators.put(Language.getString("IND_HULL_MOVING_AVERAGE"), GraphDataManager.ID_HULL_MOVING_AVG);
        indicators.put(Language.getString("IND_KELTNER_CHANNELS"), GraphDataManager.ID_KELTNER_CHANNELS);

        return indicators;
    }

    public static String[] otherIndicators = {
            Language.getString("INDI_RSI"), //1
            Language.getString("MACD"), //2
            Language.getString("MOMENTUM"), //3
            Language.getString("STD_DEVIATION"), //5
            Language.getString("IND_ACCUM_DISTRI"), //7
            Language.getString("IND_WILLS_R"), //8
            Language.getString("IND_STOCHAS_OSCIL"), //9
            Language.getString("IND_AVG_TRUE_RANGE"), //10
            Language.getString("IND_MASS_INDEX"), //11
            Language.getString("IND_MEDIAN_PRICE"), //12
            Language.getString("IND_MONEY_FLOW"), //13
            Language.getString("IND_TRIX"), //14
            Language.getString("IND_VOLATILITY"), //15
            Language.getString("IND_ZIGZAG"), //16
            Language.getString("IND_RMI"), //17
            Language.getString("IND_AROON"), //18
            Language.getString("IND_CHAIKINS_OSCILL"), //19
            Language.getString("IND_CCI"), //20
            Language.getString("IND_CHAIKINS_MONEY_FLOW"), //21
            Language.getString("IND_ICHIMOKU_KINKO_HYO"), //22
            Language.getString("IND_ENVILOPES"), //23
            Language.getString("IND_KLINGER_OSCILL"), //24
            Language.getString("IND_PARABOLIC_SAR"), //25
            Language.getString("IND_PRICE_VOLUME_TREND"), //26
            Language.getString("IND_VOLUME_OSCILL"), //27
            Language.getString("IND_WILLS_ACCU_DISTRI"), //28
            Language.getString("IND_PRICE_OSCILL"), //29
            Language.getString("IND_IMI"), //30
            Language.getString("IND_OBV"), //31
            Language.getString("IND_ROC"), //32
            Language.getString("IND_PLUS_DI"), //34
            Language.getString("IND_MINUS_DI"), //35
            Language.getString("IND_DX"), //36
            Language.getString("IND_ADX"), //37
            Language.getString("IND_ADXR"), //38
            Language.getString("IND_DPO"), //39
            Language.getString("IND_SWING_INDEX"), //40
            Language.getString("IND_RVI"), //41
            Language.getString("IND_MFI"), //42
            Language.getString("IND_PERFORMANCE"), //43
            Language.getString("IND_PVI"), //44
            Language.getString("IND_QSTICK"), //45
            Language.getString("IND_TYPICAL_PRICE"), //46
            Language.getString("IND_VERT_HORI_FILTER"), //47
            Language.getString("IND_WEIGHTED_CLOSE"), //48
            Language.getString("IND_VOLUME_ROC"), //49
            Language.getString("IND_FO"), //53
            Language.getString("IND_NVI"), //54
            Language.getString("IND_CMO"), //55
            Language.getString("IND_SMO"), //56
            Language.getString("IND_EASE_OF_MOVEMENT"), //57
            Language.getString("IND_STD_ERR"), //59
            Language.getString("IND_MACD_VOLUME"), //60
            Language.getString("IND_NET_ORDERS"), //61
            Language.getString("ID_NET_TURNOVER"), //62
            Language.getString("IND_ALLIGATOR"), //63
            Language.getString("IND_DISPARITY_INDEX") //63
    };

    public static String[] marketIndicatorNames = new String[]{Language.getString("IND_COPPOCK_CURVE")};

    public static String[] averageIndicatorNames = new String[]{Language.getString("IND_DEMA"),
            Language.getString("MOVING_AVERAGE"), Language.getString("IND_TEMA"), Language.getString("LRI"),
            Language.getString("IND_TSF"), Language.getString("IND_WILDERS_SMOOTHING"), Language.getString("IND_HULL_MOVING_AVERAGE")};

    public static String[] bandsIndicatorNames = new String[]{Language.getString("BOLLINGER_BANDS"),
            Language.getString("IND_PRICE_CHANNEL"),
            Language.getString("IND_KELTNER_CHANNELS")};


    public static String[] getCurrentIntervals() {
        String[] sa = {Language.getString("EVERY_MINUTE"),
                Language.getString("5_MIN"),
                Language.getString("10_MIN"),
                Language.getString("15_MIN"),
                Language.getString("30_MIN"),
                Language.getString("60_MIN"),
                Language.getString("OTHER_PERIODS")
        };
        return sa;
    }

    public static String[] getHistoryIntervals() {
        String[] sa = {Language.getString("DAILY"),
                Language.getString("WEEKLY"),
                Language.getString("MONTHLY"),
                Language.getString("OTHER_PERIODS")
        };
        return sa;
    }

    public static String[] getGraphStyles() {
        String[] sa = {Language.getString("LINE"),
                Language.getString("BAR"),
                Language.getString("HLC"),
                Language.getString("OHLC"),
                Language.getString("CANDLE")
        };

        return sa;
    }

    public static String[] getHistoryPeriods() {
        String[] sa = {
                Language.getString("1_MONTH"),
                Language.getString("3_MONTH"),
                Language.getString("6_MONTH"),
                Language.getString("YTD"),
                Language.getString("1_YEAR"),
                Language.getString("2_YEAR"),
                Language.getString("3_YEAR"),
                Language.getString("5_YEAR"),
                Language.getString("10_YEAR"),
                Language.getString("ALL"),
                Language.getString("OTHER_PERIODS")};
        return sa;
    }


    public static String[] getCurrentPeriods() {
        String[] sa = {
                Language.getString("1_DAY"),
                Language.getString("6_DAYS"),
                Language.getString("ALL"),
                Language.getString("OTHER_PERIODS")};
        return sa;
    }

    public static Color[] getGraphColors() {
        Color[] ca = {new Color(213, 122, 91), new Color(116, 156, 237),
                new Color(14, 168, 71), new Color(214, 90, 79),
                new Color(121, 113, 153), new Color(83, 38, 143),
                new Color(0, 74, 99), new Color(97, 214, 219),
                new Color(43, 35, 35), new Color(196, 156, 115),
                new Color(247, 115, 173), new Color(155, 131, 128),
                new Color(117, 208, 23), new Color(2, 132, 85),
                new Color(255, 132, 0), new Color(97, 42, 25),
                new Color(74, 201, 234), new Color(99, 0, 49),
                new Color(0, 0, 66), new Color(74, 201, 234)
        };
        return ca;
    }

    JPopupMenu jPopupMenu1 = new JPopupMenu(); //Right click popup
    TWMenuItem miPrint = new TWMenuItem();
    TWMenuItem mnuDataMode = new TWMenuItem();
    TWMenuItem miDetach = new TWMenuItem();
    private TWMenuItem mnuBuy = new TWMenuItem();
    private TWMenuItem mnuSell = new TWMenuItem();
    TWCheckBoxMenuItem chkmiPercent = new TWCheckBoxMenuItem();
    TWCheckBoxMenuItem chkmiShowVolume = new TWCheckBoxMenuItem();
    TWCheckBoxMenuItem chkmiShowVolumeByPrice = new TWCheckBoxMenuItem();
    TWCheckBoxMenuItem chkmiShowTurnOver = new TWCheckBoxMenuItem();
    TWCheckBoxMenuItem chkmiShowCurrentPrice = new TWCheckBoxMenuItem();
    TWCheckBoxMenuItem chkmiShowPreviousClose = new TWCheckBoxMenuItem();
    TWCheckBoxMenuItem chkmiShowOrderLines = new TWCheckBoxMenuItem();
    TWCheckBoxMenuItem chkmiShowBidAskTags = new TWCheckBoxMenuItem();
    TWCheckBoxMenuItem chkmiShowMinMaxLines = new TWCheckBoxMenuItem();
    TWMenuItem miRightMargin = new TWMenuItem();
    TWMenuItem miRefresh = new TWMenuItem();
    //submenus
    TWMenu mnuSubBaseSymbol;
    TWMenu mnuSubCompare;
    TWMenu mnuIndicators;
    TWMenu mnuCI1;
    TWMenu mnuCI2;
    TWMenu mnuSubMode;
    TWMenu mnuSubPeriod;
    TWMenu mnuSubInterval;
    TWMenu mnuSubStyle;
    TWMenu mnuZooming;
    TWMenuItem miSemiLogScale = new TWMenuItem();
    TWMenu mnuOptions;
    TWMenuItem mnuChartOptions = new TWMenuItem();
    TWMenuItem mnuLinkTOsidebar = new TWMenuItem();
    TWMenuItem mnuAnalysisTchnique = new TWMenuItem();
    TWMenu mnuSubLegend;
    TWMenu mnuSubYAxis;
    TWMenu mnuSubTemplate;

    TWMenuItem miNewBase;
    TWMenuItem miNewCompare;
    TWMenu mnuNew;
    TWMenuItem miNewChart = new TWMenuItem();
    TWMenuItem miNewLayout = new TWMenuItem();
    TWMenuItem miOpen = new TWMenuItem();
    TWMenuItem miSave = new TWMenuItem();
    TWMenuItem miSaveAs = new TWMenuItem();
    TWMenuItem miSaveAsDefaultTemplate = new TWMenuItem();

    private String currentBaseSymbol = "";
    private String currentCompareSymbol = "";
    protected String[] saComparingSymbols = null;
    private ChartFrame parent = null;

    transient final Point2D.Float MouseDownPoint = new Point2D.Float(0, 0);

    //added by charithn-new mouse point locations
    transient final Point2D.Float firstMouseUpPoint = new Point2D.Float(0, 0);
    transient final Point2D.Float secondMouseUpPoint = new Point2D.Float(0, 0);
    transient final Point2D.Float thirdMouseUpPoint = new Point2D.Float(0, 0);

    float MouseDownZoom;
    float MouseDownBeginPos;

    transient final Point2D.Float DragStartPoint = new Point2D.Float(0, 0);
    private Point CurrentMousePos = new Point(0, 0);
    private Point PreCurrentMousePos = new Point(0, 0);

    final Point2D.Float MouseUpPoint = new Point2D.Float(0, 0);
    protected boolean isDrawingALine = false;
    transient Point2D.Float MouseDragPoint = new Point2D.Float(0, 0);
    transient protected Point2D.Float PreMouseDragPoint = new Point2D.Float(0, 0);
    transient private JPanel hintPane;

    protected boolean isYAxisRescaling = false;
    protected boolean isRightMarginRescaling = false;
    protected boolean isDragZooming = false;
    private boolean boxZoomEnabled = false;
    protected boolean isBoxZooming = false;
    private boolean zoomInEnabled = false;
    private boolean zoomOutEnabled = false;
    private boolean isPointZooming = true; //TODO
    boolean isLineMoving = false;
    boolean isLineSizing = false;
    boolean firstTimeDrawing = true;
    boolean chartBodyDraggingEnabled = false;
    final private float BODY_DRAG_SMOOTHING_FACTOR = 0.8f;
    final Point LinePoint1 = new Point(0, 0);
    final Point LinePoint2 = new Point(0, 0);

    int m_maxNumPage = 1;

    public DynamicArray records = new DynamicArray();
    private String dataReqSymbol = null;
    private int requestedMode = 0;

    // vars to keep user changable fields
    long beginTime, endTime;
    boolean CurrentMode;

    JPanel graphPanel = new JPanel();  // Graph Panel
    public StockGraph graph = new StockGraph();
    CardLayout graphFrameLayout = new CardLayout();
    BorderLayout graphPanelLayout = new BorderLayout();

    /* tooltip panel components */
    JLabel lblInfoSymbol = new JLabel();
    JLabel lblInfoDate = new JLabel();
    JLabel lblInfoOpen = new JLabel();
    JLabel lblInfoHigh = new JLabel();
    JLabel lblInfoLow = new JLabel();
    JLabel lblInfoClose = new JLabel();
    JLabel lblInfoVolume = new JLabel();
    JLabel lblInfoTurnOver = new JLabel();

    JLabel lblInfoDataSymbol = new JLabel();
    JLabel lblInfoDataDate = new JLabel("", JLabel.RIGHT);
    JLabel lblInfoDataOpen = new JLabel("", JLabel.RIGHT);
    JLabel lblInfoDataHigh = new JLabel("", JLabel.RIGHT);
    JLabel lblInfoDataLow = new JLabel("", JLabel.RIGHT);
    JLabel lblInfoDataClose = new JLabel("", JLabel.RIGHT);
    JLabel lblInfoDataVolume = new JLabel("", JLabel.RIGHT);
    JLabel lblInfoDataTurnOver = new JLabel("", JLabel.RIGHT);

    String g_sLblDate = null;
    String g_sLblOpen = null;
    String g_sLblHigh = null;
    String g_sLblLow = null;
    String g_sLblClose = null;
    String g_sLblVolume = null;
    String g_sLblTime = null;
    String g_sLblPrice = null;
    String g_sLblValue = null;

    String baseSymbol = null;
    String registeredBaseSymbol = null;
    int compareGraphCount = 0;
    List<String> requestedSymbolQueue; // Stores the non validated symbols until its validated

    JPanel pnlInfoPanel = new JPanel();
    DataRowPanel rowSymbol;
    DataRowPanel rowDate;
    DataRowPanel rowOpen;
    DataRowPanel rowHigh;
    DataRowPanel rowLow;
    DataRowPanel rowClose;
    DataRowPanel rowVolume;
    DataRowPanel rowTurnOver;

    SplitTooltip splitTooltip = new SplitTooltip();
    boolean showSplitTooltip;

    DataTableModel tableModel;
    GraphDataTableModel graphTableModel;

    Table dataTable;
    Table graphDataTable;

    JPanel tablePanel;
    String baseKey;
    boolean staticMode = false;

    //added by charithn
    private JPopupMenu linkGroupMenu;
    private String linkID = LinkStore.LINK_NONE;
    private TWMenuItem redmenu;
    private TWMenuItem greenmenu;
    private TWMenuItem bluemenu;
    private TWMenuItem notlinkedmenu;


    public void LoadTheme() {
        try {

            ChartProperties cpBase = ChartSettings.getSettings().getDefaultCP(null, "", GraphDataManager.ID_BASE, Color.BLACK, null);
            ChartProperties cpVolume = ChartSettings.getSettings().getDefaultCP(null, "", GraphDataManager.ID_VOLUME, Color.BLACK, null);
            ChartProperties cpTurnOver = ChartSettings.getSettings().getDefaultCP(null, "", GraphDataManager.ID_TURNOVER, Color.BLACK, null);

            graph.setGraphAreaRightColor(Theme.getColor("GRAPH_AREA_RIGHT_COLOR"));
            graph.setGraphAreaLeftColor(Theme.getColor("GRAPH_AREA_LEFT_COLOR"));
            graph.setGraphPnltitleLeftColor(Theme.getColor("GRAPH_PNLTITLE_LEFT_COLOR"));
            graph.setGraphPnltitleRightColor(Theme.getColor("GRAPH_PNLTITLE_RIGHT_COLOR"));
            graph.setYearSeparatorColor(Theme.getColor("GRAPH_YEAR_SEPARATOR_COLOR"));
            graph.setAxisBGInnerColor(Theme.getColor("GRAPH_AXISBG_INNER_COLOR"));
            graph.setAxisBGOuterColor(Theme.getColor("GRAPH_AXISBG_OUTER_COLOR"));
            graph.setTitleForeGroundColor(Theme.getColor("GRAPH_TITLE_FOREGROUND_COLOR"));

            graph.setBarColor(Theme.getColor("GRAPH_BAR_COLOR"));
            graph.setBackColor(Theme.getColor("GRAPH_BGCOLOR"));  //legened background
            graph.setBackground(Theme.getColor("GRAPH_BACKGROUND_COLOR"));

            graph.setBorderColor(Theme.getColor("GRAPH_BORDER_COLOR"));
            graph.setAxisColor(Theme.getColor("GRAPH_AXIS_COLOR"));
            graph.setFontColor(Theme.getColor("GRAPH_FONT_COLOR"));
            graph.setCrossColor(Theme.getColor("GRAPH_CROSS_COLOR"));

            graph.setLabelColor(Color.red);
            graph.setTitleColor(Theme.getColor("GRAPH_TITLE_COLOR"));
            graph.setGridColor(Theme.getColor("GRAPH_GRID_COLOR"));

            graph.zSlider.setSliderBGColor(Theme.getColor("WINDOW_TITLE_BGCOLOR"));

            graph.zSlider.setSliderFGColor(Theme.getColor("BACKGROUND_COLOR"));
            graph.chartSelectedColor = (Theme.getColor("CHART_SELECTED_COLOR"));

            //base chart
            if (!cpBase.isUserSaved()) {
                graph.GDMgr.getBaseCP().setUseSameColor(false);
                graph.setGraphUpColor(Theme.getColor("GRAPH_UP_COLOR"));
                graph.setGraphDownColor(Theme.getColor("GRAPH_DOWN_COLOR"));

            } else {
                graph.setGraphUpColor(graph.GDMgr.getBaseCP().getColor());
                graph.setGraphDownColor(graph.GDMgr.getBaseCP().getWarningColor());
            }
            //volume chart
            if (!cpVolume.isUserSaved()) {
                graph.GDMgr.getVolumeCP().setUseSameColor(false);
                graph.setVolumeGraphUpColor(Theme.getColor("GRAPH_VOLUME_UP_COLOR"));
                graph.setVolumeGraphDownColor(Theme.getColor("GRAPH_VOLUME_DOWN_COLOR"));

            } else {
                graph.setVolumeGraphUpColor(graph.GDMgr.getVolumeCP().getColor());
                graph.setVolumeGraphDownColor(graph.GDMgr.getVolumeCP().getWarningColor());
            }
            //turnover chart
            if (!cpTurnOver.isUserSaved()) {
                graph.GDMgr.getTurnOverCP().setUseSameColor(false);
                graph.setTurnOverGraphUpColor(Theme.getColor("GRAPH_TURNOVER_UP_COLOR"));
                graph.setTurnOverGraphDownColor(Theme.getColor("GRAPH_TURNOVER_DOWN_COLOR"));

            } else {
                graph.setTurnOverGraphUpColor(graph.GDMgr.getTurnOverCP().getColor());
                graph.setTurnOverGraphDownColor(graph.GDMgr.getTurnOverCP().getWarningColor());
            }

        } catch (Exception e) {

            graph.setGraphAreaRightColor(new Color(100, 100, 190));
            graph.setGraphAreaLeftColor(new Color(100, 100, 190));
            graph.setYearSeparatorColor(new Color(100, 100, 190));
            graph.setGraphUpColor(new Color(100, 100, 190));
            graph.setGraphDownColor(new Color(100, 100, 190));
            graph.setBarColor(new Color(100, 100, 190));
            graph.setBackColor(new Color(255, 239, 223));
            graph.setBorderColor(new Color(250, 70, 70));
            graph.setAxisColor(new Color(210, 205, 205));  // 190,200,180
            graph.setFontColor(new Color(50, 100, 60));
            graph.setCrossColor(new Color(50, 80, 80));
            graph.setLabelColor(new Color(219, 219, 229));   //170,180,160
            graph.setTitleColor(new Color(235, 230, 215));
            graph.setGridColor(new Color(220, 220, 220));
        }
    }

    private void initStockGraph() {

        //charithn-IMPORTANT
        ChartSettings cs = ChartSettings.getSettings();
        CurrentMode = cs.isCurrentMode();
        int period = cs.getPeriod();
        long interval = cs.getInterval();
        boolean showVolumeGrf = cs.isShowVolume();
        boolean showTurnoverGrf = cs.isShowTurnOver();

        graph.NotifyChange = false;
        graph.showVolumeGraph(showVolumeGrf);
        graph.showTurnOverGraph(showTurnoverGrf);

        chkmiShowVolume.setState(!showVolumeGrf);
        chkmiShowTurnOver.setState(!showTurnoverGrf);
        chkmiShowCurrentPrice.setState(!cs.isShowLastPriceLine());
        chkmiShowPreviousClose.setState(!cs.isShowPreviousCloseLine());
        chkmiShowOrderLines.setState(!cs.isShowOrderLines());
        chkmiShowBidAskTags.setState(!cs.isShowBidAskTags());
        chkmiShowMinMaxLines.setState(!cs.isShowMinMaxLines());
        graph.addGraphMouseListener(this);

        applyTheme();

        //apply custom colors which is saved by the user
        graph.setInterval(interval);
        graph.setPeriodFlag(period);

        graph.setCurrentMode(CurrentMode);

        graph.newsImagePath = "News.jpg";
        graph.XaxisPitch = 0.8;
        graph.YaxisPitch = 0.4;
        graph.SpotRadius = 2;
        graph.drawSpot = false;
        graph.NotifyChange = true;

    }

    // initiallize combobox values in this
    private void initComboBoxVars() {
        strSymbols = getSymbols();
        if (strSymbols == null) {
            g_oBaseSymbols.setSymbols(new String[0]);
        } else {
            g_oBaseSymbols.setSymbols(getSymbols());
        }
        strCompareSymbols = getSymbols(); // to be modified
        if (strCompareSymbols == null) {
            g_oComparedSymbols.setSymbols(new String[0]);
        } else {
            g_oComparedSymbols.setSymbols(getSymbols());
        }
        strCurrentIntervals = getCurrentIntervals();
        strHistoryIntervals = getHistoryIntervals();
        strGraphStyles = getGraphStyles();
        strHistoryPeriods = getHistoryPeriods();
        strCurrentPeriods = getCurrentPeriods();
        graphColors = getGraphColors();
    }

    //private GraphFrame gf = null;
    //private String uniqueName;

    //Construct the frame

    public GraphFrame(ChartFrame parent) {
        long entryTime = System.currentTimeMillis();
        this.parent = parent;
        baseSymbol = null;
        requestedSymbolQueue = Collections.synchronizedList(new LinkedList<String>());
        enableEvents(AWTEvent.WINDOW_EVENT_MASK);
        this.addComponentListener(this);
        addMouseWheelListener(this);
        this.setBorder(GUISettings.getTWFrameBorder());
        ConnectionNotifier.getInstance().addConnectionListener(this);
        this.hideTitleBarMenu();
        try {
            Theme.registerComponent(this);
            jbInit();
            GUISettings.applyOrientation(this);
        } catch (Exception e) {
            e.printStackTrace();
        }
        addDragWindowListener(this);
        addDataWindowListener(this);
        graph.GDMgr.fibCalBounds = new Point[3];
        //System.out.println("**** GraphframeSuper Calc time " + (entryTime - System.currentTimeMillis()));
        setLinkGroupsEnabled(true);
    }

    public void applyTheme() {

        SwingUtilities.updateComponentTreeUI(jPopupMenu1);
        LoadTheme();
        StockGraph.clDkShadow = UIManager.getColor("controlDkShadow");
        StockGraph.clLtShadow = UIManager.getColor("controlHighlight");
        StockGraph.clHighlight = UIManager.getColor("controlShadow");
        StockGraph.clLtHighlight = UIManager.getColor("controlLtHighlight");

        mnuBuy.setBackground(Theme.getColor("BOARD_TABLE_CELL_BID_BGCOLOR1"));
        mnuSell.setBackground(Theme.getColor("BOARD_TABLE_CELL_ASK_BGCOLOR1"));

        graph.GDMgr.updateTheme();

        applyCustomColors(ChartTheme.getCustomThemeDataToApply());

        try {
            dataTable.updateGUI();
            graphDataTable.updateGUI();
            adjustGraphTableRenderer();
            adjustDataTableRenderer();
        } catch (Exception e) {
            //e.printStackTrace();
        }
    }

    //important : apply all the values to the current graph colors
    public void applyCustomColors(ThemeData optionData) {

        graph.setGraphAreaLeftColor(optionData.GraphAreaLeft);
        graph.setGraphPnltitleLeftColor(optionData.TitleBGLeft);
        graph.setGraphPnltitleRightColor(optionData.TitleBGRight);
        graph.setTitleForeGroundColor(optionData.TitleForeground);
        graph.setYearSeparatorColor(optionData.YearSeperator);
        graph.setGraphAreaRightColor(optionData.GraphAreaRight);
        graph.setAxisColor(optionData.AxisColor);
        graph.setFontColor(optionData.FontColor);
        graph.setTitleColor(optionData.TitleBGLeft);
        graph.setGridColor(optionData.GridColor);
        graph.setChartSelectedColor(optionData.SelectedColor);
        graph.setDragZoomColor(optionData.DragZoomColor);
        graph.setCrossHairColor(optionData.CrosshairsColor);
        graph.setAxisBGInnerColor(optionData.AxisBGInner);
        graph.setAxisBGOuterColor(optionData.AxisBGOuter);

        graph.setAxisFont(optionData.AxisFont);
        graph.setLegendFont(optionData.LegendFont);
        graph.setGraphTitleFont(optionData.TitleFont);

        Graphics g = graph.getGraphics();
        if (g != null) {
            //graph.titleHeight = g.getFontMetrics(graph.graphTitleFont).getHeight();
            //graph.GDMgr.titleHeight = g.getFontMetrics(graph.graphTitleFont).getHeight();
        }

        /* ==============  chart settings =========== */

        //Y-Axis position
        if (ChartOptions.getCurrentOptions().chart_y_axis_poition == ChartOptions.YAxisPosition.LEFT_ONLY) {
            graph.yAxisPos = SplitPanel.AXIS_LEFT_ONLY;
        } else if (ChartOptions.getCurrentOptions().chart_y_axis_poition == ChartOptions.YAxisPosition.RIGHT_ONLY) {
            graph.yAxisPos = SplitPanel.AXIS_RIGHT_ONLY;
        } else if (ChartOptions.getCurrentOptions().chart_y_axis_poition == ChartOptions.YAxisPosition.BOTH) {
            graph.yAxisPos = SplitPanel.AXIS_BOTH_ENDS;
        }

        //legend style
        if (ChartOptions.getCurrentOptions().chart_legend_style == ChartOptions.LegendStyle.COMPANY_NAME) {
            graph.setLegendType(StockGraph.INT_LEGEND_COMPANY_NAME);
        } else if (ChartOptions.getCurrentOptions().chart_legend_style == ChartOptions.LegendStyle.SYMBOL) {
            graph.setLegendType(StockGraph.INT_LEGEND_SYMBOL);
        }

        //right margin settings
        int histoyMargin = ChartOptions.getCurrentOptions().chart_right_margin_history;
        int intraDayMargin = ChartOptions.getCurrentOptions().chart_right_margin_intraday;
        graph.GDMgr.setRightMargin(histoyMargin, intraDayMargin);

        graph.reDrawAll();

    }

    //Component initialization
    private void jbInit() throws Exception {
        // "this" (first param below) implements ActionListner
        registerKeyboardAction(this, "DEL", KeyStroke.getKeyStroke(KeyEvent.VK_DELETE, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        registerKeyboardAction(this, "ENTER", KeyStroke.getKeyStroke(KeyEvent.VK_ENTER, 0), JComponent.WHEN_IN_FOCUSED_WINDOW);
        registerKeyboardAction(this, "CTRL", KeyStroke.getKeyStroke(KeyEvent.VK_CONTROL, 0, true), JComponent.WHEN_IN_FOCUSED_WINDOW);

        this.getContentPane().setLayout(graphFrameLayout);
        this.setSize(new Dimension(775, 557));
        this.setTitle(Language.getString("STOCK_GRAPH"));

        // get these info from an ini file??
        initComboBoxVars();
        initStockGraph();

        graph.addToolTipChangeListener(this);

        g_sLblDate = Language.getString("DATE");
        g_sLblOpen = Language.getString("OPEN");
        g_sLblHigh = Language.getString("HIGH");
        g_sLblLow = Language.getString("LOW");
        g_sLblClose = Language.getString("CLOSE");
        g_sLblVolume = Language.getString("CHANGE");
        g_sLblTime = Language.getString("TIME");
        g_sLblPrice = Language.getString("PRICE");
        g_sLblVolume = Language.getString("VOLUME");
        g_sLblValue = Language.getString("VALUE");

        graphPanel.setBorder(BorderFactory.createLoweredBevelBorder());
        graphPanel.setLayout(graphPanelLayout);

        /* ========================= creating the tooltip the info panel =============== */
        pnlInfoPanel.setLayout(new ColumnLayout());

        lblInfoSymbol.setText("");
        lblInfoSymbol.setOpaque(true);
        lblInfoDataSymbol.setFont(ChartTheme.getCustomThemeDataToApply().ToolTipFont);
        lblInfoDataSymbol.setOpaque(false);

        lblInfoDate.setText(Language.getString("DATE"));
        lblInfoDate.setFont(ChartTheme.getCustomThemeDataToApply().ToolTipFont);
        lblInfoDate.setOpaque(false);
        lblInfoDataDate.setFont(ChartTheme.getCustomThemeDataToApply().ToolTipFont);
        lblInfoDataDate.setOpaque(false);

        lblInfoOpen.setText(Language.getString("OPEN"));
        lblInfoOpen.setOpaque(false);
        lblInfoDataOpen.setOpaque(false);

        lblInfoHigh.setText(Language.getString("HIGH"));
        lblInfoHigh.setOpaque(false);
        lblInfoDataHigh.setOpaque(false);

        lblInfoLow.setText(Language.getString("LOW"));
        lblInfoLow.setOpaque(false);
        lblInfoDataLow.setOpaque(false);

        lblInfoClose.setText(Language.getString("CLOSE"));
        lblInfoClose.setOpaque(false);
        lblInfoDataClose.setOpaque(false);

        lblInfoVolume.setText(Language.getString("VOLUME"));
        lblInfoVolume.setOpaque(false);
        lblInfoDataVolume.setOpaque(false);

        lblInfoTurnOver.setText(Language.getString("GRAPH_TURNOVER"));
        lblInfoTurnOver.setOpaque(false);
        lblInfoDataTurnOver.setOpaque(false);

        pnlInfoPanel.setSize(new Dimension(graph.infoPnlWidth, graph.infoPnlHeight));
        pnlInfoPanel.setBorder(BorderFactory.createLineBorder(ChartTheme.getCustomThemeDataToApply().TooltipBorderColor));
        /*==================================== end of tool tip =================================*/

        jPopupMenu1.addPopupMenuListener(this);

        mnuNew = new TWMenu(Language.getString("NEW"), "graph_New.GIF");

        miNewChart.setText(Language.getString("CHART"));
        miNewChart.setIconFile("graph_Newchart.GIF");
        miNewChart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                newChartClicked();
            }
        });
        miNewLayout.setText(Language.getString("MENUITEM_LAYOUT"));
        miNewLayout.setIconFile("graph_Newlayout.GIF");
        miNewLayout.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                newLayoutClicked();
            }
        });
        miSave.setText(Language.getString("SAVE_CHART"));
        miSave.setIconFile("graph_Save.GIF");
        miSave.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveClicked(false);
            }
        });
        miSaveAs.setText(Language.getString("SAVE_CHART_AS"));
        miSaveAs.setIconFile("graph_Save As.GIF");
        miSaveAs.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                saveClicked(true);
            }
        });
        miSaveAsDefaultTemplate.setText(Language.getString("SAVE_CHART_AS_DEFAULT"));
        miSaveAsDefaultTemplate.setIconFile("graph_Save_as_default.GIF");
        final GraphFrame[] gfArray = new GraphFrame[]{this};
        miSaveAsDefaultTemplate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                try {
                    TemplateFactory.SaveTemplate(new File(TemplateFactory.DEFAULT_TEMPLATE_FILE), gfArray);
                    /***********************default template loading -sathyajith***********************/
                    //save mode template as well according to the mode
                    if (CurrentMode) {
                        TemplateFactory.SaveTemplate(new File(TemplateFactory.INTRADAY_DEFAULT_TEMPLATE_FILE), gfArray);
                    } else {
                        TemplateFactory.SaveTemplate(new File(TemplateFactory.HISTORY_DEFAULT_TEMPLATE_FILE), gfArray);
                    }
                    //todo Layout?

                    /***********************default template loading -sathyajith***********************/
                    ChartSettings.getSettings().loadSettingsFromTemplateFile();
                    ChartSettings.isUserSavedColors = true;
                    ChartSettings.userSavingGraphColors = true;

                } catch (Exception e1) {
                    e1.printStackTrace();
                }
            }
        });
        miOpen.setText(Language.getString("OPEN_CHART"));
        miOpen.setIconFile("graph_Open.gif");
        miOpen.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                openClicked();
            }
        });

        mnuNew.add(miNewChart);
        mnuNew.add(miNewLayout);
        jPopupMenu1.add(mnuNew);
        jPopupMenu1.add(miOpen);
        jPopupMenu1.add(miSave);
        jPopupMenu1.add(miSaveAs);
        jPopupMenu1.add(miSaveAsDefaultTemplate);
        jPopupMenu1.add(miPrint);
        miPrint.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                printChart();
            }
        });
        jPopupMenu1.add(new TWSeparator());
        jPopupMenu1.add(miDetach);
        miDetach.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                toggleDetach();
            }
        });
        jPopupMenu1.add(new TWSeparator());

        //-----------------------------------------------------------
        mnuBuy.setIconFile("buy.gif");
        mnuBuy.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().doTransaction(TradeMeta.BUY, graph.GDMgr.getBaseGraph(), 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
            }
        });
        if (ChartInterface.isBuySellEnabled()) jPopupMenu1.add(mnuBuy);

        mnuSell.setIconFile("sell.gif");
        mnuSell.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                Client.getInstance().doTransaction(TradeMeta.SELL, graph.GDMgr.getBaseGraph(), 0, null, false, 0, TradeMeta.MODE_NORMAL, TradeMeta.SOURCE_BOARD, null, null);
            }
        });
        if (ChartInterface.isBuySellEnabled()) jPopupMenu1.add(mnuSell);
        if (ChartInterface.isBuySellEnabled()) jPopupMenu1.add(new TWSeparator());

        jPopupMenu1.setLabel("graphPopupMenu");

        miPrint.setText(Language.getString("PRINT_CHART"));
        miDetach.setText(Language.getString("DETACH"));
        chkmiPercent.setText(Language.getString("INDEXED"));
        chkmiShowVolume.setText(Language.getString("SHOW_VOLUME"));
        chkmiShowVolumeByPrice.setText(Language.getString("SHOW_VOLUME_BY_PRICE"));
        chkmiShowTurnOver.setText(Language.getString("SHOW_TURNOVER"));
        chkmiShowCurrentPrice.setText(Language.getString("SHOW_CURRENT_PRICE_LINE"));
        chkmiShowPreviousClose.setText(Language.getString("SHOW_PREVIOUS_CLOSE_LINE"));
        chkmiShowOrderLines.setText(Language.getString("SHOW_ORDER_LINES"));
        chkmiShowBidAskTags.setText(Language.getString("SHOW_BID_ASK_TAGS"));
        chkmiShowMinMaxLines.setText(Language.getString("SHOW_MIN_MAX_LINES"));
        miRightMargin.setText(Language.getString("CHART_RIGHT_MARGIN"));
        miRefresh.setText(Language.getString("REFRESH"));
        mnuDataMode.setText(Language.getString("DATA_MODE"));
        mnuChartOptions.setText(Language.getString("CHART_OPTIONS"));
        mnuLinkTOsidebar.setText(Language.getString("LINK_TO_SIDEBAR"));
        mnuAnalysisTchnique.setText(Language.getString("INSERT_ANALYSIS_TECHNIQUE"));
        mnuSell.setText(Language.getString("SELL"));
        mnuBuy.setText(Language.getString("BUY"));

        miPrint.setIconFile("graph_Print.gif");
        miDetach.setIconFile("graph_Detach.gif");
        chkmiPercent.setIconFile("graph_Indexed.GIF");
        chkmiShowVolume.setIconFile("graph_show volume.GIF");
        chkmiShowVolumeByPrice.setIconFile("graph_show_volume_by_price.GIF");
        chkmiShowTurnOver.setIconFile("graph_tb_show_turnover.gif");
        chkmiShowCurrentPrice.setIconFile("graph_current_price_line.GIF");
        chkmiShowPreviousClose.setIconFile("graph_previous_close_line.GIF");
        chkmiShowOrderLines.setIconFile("graph_order_lines.GIF");
        chkmiShowBidAskTags.setIconFile("graph_bidAsk_tags.GIF");
        chkmiShowMinMaxLines.setIconFile("graph_minMax_lines.GIF");

        mnuDataMode.setIconFile("graph_DataMode.GIF");
        mnuChartOptions.setIconFile("graph_set_defaults.GIF");
        mnuAnalysisTchnique.setIconFile("graph_IAT.GIF");

        chkmiPercent.setSelected(false);
        chkmiShowVolume.setSelected(true);
        chkmiShowVolumeByPrice.setSelected(true);
        chkmiShowTurnOver.setSelected(true);

        miNewBase = new TWMenuItem(Language.getString("SEARCH_3_DOTS"));
        miNewCompare = new TWMenuItem(Language.getString("SEARCH_3_DOTS"));
        miNewBase.addActionListener(this);
        miNewCompare.addActionListener(this);

        //adding subMenus
        try {
            String sname;

            sname = Language.getString("BASE_SYMBOL");
            mnuSubBaseSymbol = getRadioSubMenu(sname, extractSymbolArray(strSymbols), "baseSymbolClicked", true);
            mnuSubBaseSymbol.add(miNewBase);
            jPopupMenu1.add(mnuSubBaseSymbol);
            mnuSubBaseSymbol.setIconFile("graph_base_Symbol.gif");
            mnuSubBaseSymbol.updateUI();

            sname = Language.getString("COMPARE");
            mnuSubCompare = getRadioSubMenu(sname, extractSymbolArray(strCompareSymbols), "compareSymbolClicked", false);
            mnuSubCompare.add(miNewCompare);
            jPopupMenu1.add(mnuSubCompare);
            mnuSubCompare.setIconFile("graph_compare_Symbol.gif");
            mnuSubCompare.updateUI();

            jPopupMenu1.add(new TWSeparator());

            sname = Language.getString("INDICATORS");
            String[] saI = getIndicators();
            mnuIndicators = getIndicatorSubMenu(sname, saI);
            mnuIndicators.setIconFile("graph_Indicators.GIF");
            jPopupMenu1.add(mnuIndicators);

            mnuSubTemplate = getTemplateSubMenu();
            jPopupMenu1.add(mnuSubTemplate);

            jPopupMenu1.add(new TWSeparator());

            sname = Language.getString("GRAPH_MODE");
            String[] sa = {Language.getString("HISTORY"),
                    Language.getString("INTRADAY")};
            mnuSubMode = getRadioSubMenu(sname, sa, "modeClicked", true);
            mnuSubMode.setIconFile("graph_Mode.GIF");
            jPopupMenu1.add(mnuSubMode);

            sname = Language.getString("INTRADAY");
            mnuSubPeriod = getRadioSubMenu(sname, getCurrentPeriods(), "periodClicked", true);
            jPopupMenu1.add(mnuSubPeriod);

            sname = Language.getString("INTERVAL");
            mnuSubInterval = getRadioSubMenu(sname, getCurrentIntervals(), "intervalClicked", true);
            jPopupMenu1.add(mnuSubInterval);

            sname = Language.getString("STYLE");
            mnuSubStyle = getRadioSubMenu(sname, getGraphStyles(), "styleClicked", true);
            jPopupMenu1.add(mnuSubStyle);

            jPopupMenu1.add(new TWSeparator());

            mnuDataMode.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    switchPanel();
                }
            });
            jPopupMenu1.add(mnuDataMode);

            chkmiPercent.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    percentClicked(e);
                }
            });
            jPopupMenu1.add(chkmiPercent);

            chkmiShowVolume.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    showVolumeClicked(e);
                }
            });
            jPopupMenu1.add(chkmiShowVolume);

            chkmiShowVolumeByPrice.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    showVolumeByPriceClicked(e);
                }
            });
            //jPopupMenu1.add(chkmiShowVolumeByPrice);

            chkmiShowTurnOver.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    showTurnoverClicked(e);
                }
            });
            jPopupMenu1.add(chkmiShowTurnOver);


            chkmiShowCurrentPrice.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    showCurrentPriceClicked();
                }
            });
            jPopupMenu1.add(chkmiShowCurrentPrice);

            chkmiShowPreviousClose.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    showPreviousCloseLineClicked();
                }
            });
            jPopupMenu1.add(chkmiShowPreviousClose);

            chkmiShowOrderLines.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    showOrderLinesClicked();
                }
            });
            if (ChartInterface.isBuySellEnabled()) jPopupMenu1.add(chkmiShowOrderLines);
            chkmiShowBidAskTags.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    showBidAskTagsClicked();
                }
            });
            jPopupMenu1.add(chkmiShowBidAskTags);
            chkmiShowMinMaxLines.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    showMinMaxLinesClicked();
                }
            });
            if (ChartInterface.isMinMaxEnabled()) jPopupMenu1.add(chkmiShowMinMaxLines);

            jPopupMenu1.add(new TWSeparator());

            mnuZooming = getZoomingSubMenu();
            jPopupMenu1.add(mnuZooming);

            jPopupMenu1.add(miSemiLogScale);
            miSemiLogScale.setText(Language.getString("GRAPH_SEMI_LOG_SCALE"));
            miSemiLogScale.setIconFile("graph_Semi_Log.GIF");
            miSemiLogScale.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    setSemiLogScales();
                }
            });

            mnuOptions = new TWMenu(Language.getString("OPTIONS"), "graph_Option.GIF");

            miRightMargin.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    rightMarginClicked(e);
                }
            });
            mnuOptions.add(miRightMargin);

            sname = Language.getString("LEGEND");
            String s1 = Language.getString("SYMBOL");
            String s3 = Language.getString("COMPANY_NAME");
            String[] saLeg = {s1, s3}; //,s2
            mnuSubLegend = getRadioSubMenu(sname, saLeg, "legendClicked", true);
            mnuOptions.add(mnuSubLegend);

            sname = Language.getString("GRAPH_Y_AXIS");
            String strLeft = Language.getString("Y_AXIS_LEFT");
            String strRight = Language.getString("Y_AXIS_RIGHT");
            String strBoth = Language.getString("Y_AXIS_BOTH");
            String[] saYAxis = {strLeft, strRight, strBoth}; //,s2
            mnuSubYAxis = getRadioSubMenu(sname, saYAxis, "yAxisClicked", true);
            mnuOptions.add(mnuSubYAxis);

            //changed by charithn
            mnuChartOptions.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    ChartOptionsWindow.getSharedInstance(0).setVisible(true);
                }
            });
            jPopupMenu1.add(mnuChartOptions);
            mnuLinkTOsidebar.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    mnuLinkToSidebarClicked();
                }
            });

            mnuLinkTOsidebar.setVisible(false);
            mnuAnalysisTchnique.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    AnalysisTechWindow.getSharedInstance().setVisible(true);
                }
            });
            jPopupMenu1.add(mnuAnalysisTchnique);

        } catch (Exception expn) {
            expn.printStackTrace();
        }

        this.getContentPane().add(graphPanel, "GRAPH");
        hintPane = (JPanel) this.getGlassPane();
        hintPane.setVisible(false);
        hintPane.setLayout(null);
        hintPane.add(pnlInfoPanel);
        hintPane.add(splitTooltip);

        splitTooltip.setBackground(new Color(240, 240, 235));
        splitTooltip.setBorder(BorderFactory.createLineBorder(new Color(100, 100, 100), 1));

        tablePanel = new JPanel();
        tablePanel.setLayout(new BorderLayout());
        this.getContentPane().add(tablePanel, "TABLE");

        graphPanel.add(graph, BorderLayout.CENTER);
        GUISettings.applyOrientation(pnlInfoPanel);

        SwingUtilities.updateComponentTreeUI(jPopupMenu1);
        GUISettings.applyOrientation(jPopupMenu1);

    } // end of jbinit

    public static String[] extractSymbolArray(String[] sa) {
        if ((sa != null) && (sa.length > 0)) {
            int len = sa.length;
            String[] symbSa = new String[len];
            String[] keyData;
            for (int i = 0; i < len; i++) {
                keyData = sa[i].split(Constants.KEY_SEPERATOR_CHARACTER);
                if ((keyData != null) && (keyData.length > 1))
                    symbSa[i] = keyData[1];
                else
                    symbSa[i] = sa[i];
            }
            return symbSa;
        }
        return null;
    }

    private ChartProperties processDataForSetGraphData(String smbl) {
        ChartProperties cp = null;
        if (CurrentMode) {
            graph.setPercentagesflag(false);   //TODO : FUNNY here
            //graph.setPeriodFlag(graph.ONE_DAY);
            //graph.setIntervalFlag(graph.EVERYMINUTE);
        } else {
            graph.setPercentagesflag(false);
            //graph.setPeriodFlag(graph.ONE_YEAR);
            //graph.setIntervalFlag(graph.DAILY);
        }

        if (records != null) {
            baseSymbol = dataReqSymbol;
            graph.NotifyChange = true;
            cp = graph.setBaseRecords(records, smbl);
            setGraphTitle();
            ChartFrame.getSharedInstance().refreshToolBars();
            this.show();
        } else {
            graph.clearAll();
            new ShowMessage(false, Language.getString("SYMBOL_NOT_FOUND"), "E");
            this.setVisible(false);
        }
        records = null;
        return cp;
    }

    private void processDataForCompareGraphData(DynamicArray records) {
        if (records != null) {
            graph.NotifyChange = true;
            graph.setComparisonRecords(records, null, currentCompareSymbol, getNewColor());
        } else {
            new ShowMessage(false, Language.getString("SYMBOL_NOT_FOUND"), "E");
        }
        records = null;
    }

    public TWMenu getRadioSubMenu(String sname, String[] sa, final String method, final boolean isRadio) {
        TWMenu oMenu = new TWMenu(sname);
        ButtonGroup oGroup = new ButtonGroup();

        if (sa != null)
            for (int i = 0; i < sa.length; i++) {
                if (sa[i].trim().equals("")) continue;
                TWMenuItem item = new TWMenuItem(sa[i]);
                oGroup.add(item);
                oMenu.add(item);
                final int index = i;
                final ButtonGroup bg = oGroup;
                final TWMenuItem currMi = item;
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        if (isRadio) {
                            for (int n = 0; n < bg.getButtonCount(); n++) {
                                Enumeration en = bg.getElements();
                                while (en.hasMoreElements()) {
                                    TWMenuItem mi = (TWMenuItem) (en.nextElement());
                                    mi.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
                                }
                            }
                            currMi.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_checked.gif"));
                        } else {
                            if (currMi.isSelected()) {
                                currMi.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
                                currMi.setSelected(false);
                            } else {
                                currMi.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_checked.gif"));
                                currMi.setSelected(true);
                            }
                        }
                        if (method.equals("baseSymbolClicked")) {
                            baseSymbolClicked(e.getSource(), index);
                        } else if (method.equals("compareSymbolClicked")) {
                            compareSymbolClicked(e.getSource(), index);
                        } else if (method.equals("modeClicked")) {
                            modeClicked(e.getSource(), index);
                        } else if (method.equals("periodClicked")) {
                            periodClicked(e.getSource(), index);
                        } else if (method.equals("intervalClicked")) {
                            intervalClicked(e.getSource(), index);
                        } else if (method.equals("styleClicked")) {
                            styleClicked(e.getSource(), index);
                        } else if (method.equals("legendClicked")) {
                            legendClicked(e.getSource(), index);
                        } else if (method.equals("yAxisClicked")) {
                            yAxisClicked(e.getSource(), index);
                        }
                        graph.GDMgr.refreshDataWindow();
                    }
                });
                item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            }
        GUISettings.applyOrientation(oMenu);
        return oMenu;
    }

    private TWMenu getIndicatorSubMenu(String sname, String[] sa) {
        TWMenu oMenu = new TWMenu(sname);
        String iconPath = "images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif";

        TWMenu averagesMenu = new TWMenu(Language.getString("AVERAGES"), iconPath);
        oMenu.add(averagesMenu);
        Arrays.sort(averageIndicatorNames);
        for (int i = 0; i < averageIndicatorNames.length; i++) {
            final String indicatorName = averageIndicatorNames[i];
            TWMenuItem item = new TWMenuItem(indicatorName);
            averagesMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = indicatorMap.get(indicatorName);
                    indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }

        TWMenu bandsMenu = new TWMenu(Language.getString("BANDS"), iconPath);
        oMenu.add(bandsMenu);
        Arrays.sort(bandsIndicatorNames);
        for (int i = 0; i < bandsIndicatorNames.length; i++) {
            final String indicatorName = bandsIndicatorNames[i];
            TWMenuItem item = new TWMenuItem(indicatorName);
            bandsMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = indicatorMap.get(indicatorName);
                    indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }

        TWMenu marketIndMenu = new TWMenu(Language.getString("MARKET_INDICATOR"), iconPath);
        oMenu.add(marketIndMenu);
        Arrays.sort(marketIndicatorNames);
        for (int i = 0; i < marketIndicatorNames.length; i++) {
            final String indicatorName = marketIndicatorNames[i];
            TWMenuItem item = new TWMenuItem(indicatorName);
            marketIndMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = indicatorMap.get(indicatorName);
                    indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }

        TWMenu indicatorsMenu = new TWMenu(Language.getString("OTHER_INDICATORS"), iconPath);
        int menuRows = otherIndicators.length / 2;
        VerticalGridLayout menuGrid = new VerticalGridLayout(menuRows, 2);
        indicatorsMenu.getPopupMenu().setLayout(menuGrid);
        oMenu.add(indicatorsMenu);
        Arrays.sort(otherIndicators);
        for (int i = 0; i < otherIndicators.length; i++) {
            if (otherIndicators[i].trim().equals("")) continue;
            if (otherIndicators[i].trim().equals(Language.getString("IND_NET_ORDERS").trim()) || otherIndicators[i].trim().equals(Language.getString("IND_NET_TURNOVER").trim())) {
                continue;
            }

            final String indicatorName = otherIndicators[i];
            TWMenuItem item = new TWMenuItem(indicatorName);
            indicatorsMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = indicatorMap.get(indicatorName);
                    indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }

        mnuCI1 = new TWMenu(Language.getString("CUSTOM_INDICATORS"), iconPath);
        oMenu.add(mnuCI1);

        TWMenu allMenu = new TWMenu(Language.getString("ALL"), iconPath);
        menuRows = sa.length / 2;
        menuGrid = new VerticalGridLayout(menuRows, 2);
        allMenu.getPopupMenu().setLayout(menuGrid);
        oMenu.add(allMenu);
        if (sa != null) {
            for (int i = 0; i < sa.length; i++) {
                if (sa[i].trim().equals("")) continue;
                if (sa[i].trim().equals(Language.getString("IND_NET_ORDERS").trim()) || sa[i].trim().equals(Language.getString("IND_NET_TURNOVER").trim())) {
                    continue;
                }

                TWMenuItem item = new TWMenuItem(sa[i]);
                allMenu.add(item);
                final String indicatorName = sa[i]; //Get the indicator name to search in hashmap(need for sorting) - Pramoda
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        Integer ObjInt = indicatorMap.get(indicatorName); // get the original index after sorting - Pramoda
                        indicatorClicked(e.getSource(), ObjInt);
                    }
                });
                item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
            }
        }

        return oMenu;
    }

    private void populateIndicatorPopupMenu() {

        /*  mnuIndicatorPopup.addPopupMenuListener(this);
        String iconPath = "images/Theme" + Theme.getID() + "/graph_unchecked.gif";

        *//************************************Add Favourites menu ******************//*
        //Data structor - favouoirte indicator names arraylist
        TWMenu favIndMenu = new TWMenu(Language.getString("FAVOURITE"), iconPath);
        mnuIndicatorPopup.add(favIndMenu);

        for (int i = 0; i < ChartFrame.getSharedInstance().alFavouiteIndicators.size(); i++) {
            final String indicatorName = ChartFrame.getSharedInstance().alFavouiteIndicators.get(i);
            TWMenuItem item = new TWMenuItem(indicatorName);
            favIndMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = indicatorMap.get(indicatorName);
                    indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/graph_unchecked.gif"));
        }

        //faviourite custom indicators
        for (int i = 0; i < ChartFrame.getSharedInstance().alFaviouriteCustIndicators.size(); i++) {
            final String custIndicatorName = ChartFrame.getSharedInstance().alFaviouriteCustIndicators.get(i);
            //Todo if custom indicators include a separator
            TWMenuItem item = new TWMenuItem(custIndicatorName);
            favIndMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Class custIndClass = getIndicatorClassByName(custIndicatorName, ChartFrame.getSharedInstance().customIndicators);
                    graph.GDMgr.addCustomIndicator(custIndicatorName, custIndClass, false);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/graph_unchecked.gif"));
        }

        *//************************************Add Averages indictor menu ******************//*
        TWMenu averagesMenu = new TWMenu(Language.getString("AVERAGES"), iconPath);
        mnuIndicatorPopup.add(averagesMenu);
        Arrays.sort(averageIndicatorNames);
        for (int i = 0; i < averageIndicatorNames.length; i++) {
            final String indicatorName = averageIndicatorNames[i];
            TWMenuItem item = new TWMenuItem(indicatorName);
            averagesMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = indicatorMap.get(indicatorName);
                    indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/graph_unchecked.gif"));
        }

        TWMenu bandsMenu = new TWMenu(Language.getString("BANDS"), iconPath);
        mnuIndicatorPopup.add(bandsMenu);
        Arrays.sort(bandsIndicatorNames);
        for (int i = 0; i < bandsIndicatorNames.length; i++) {
            final String indicatorName = bandsIndicatorNames[i];
            TWMenuItem item = new TWMenuItem(indicatorName);
            bandsMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = indicatorMap.get(indicatorName);
                    indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/graph_unchecked.gif"));
        }

        TWMenu indicatorsMenu = new TWMenu(Language.getString("OTHER_INDICATORS"), iconPath);
        int menuRows = otherIndicators.length / 2;
        VerticalGridLayout menuGrid = new VerticalGridLayout(menuRows, 2);
        indicatorsMenu.getPopupMenu().setLayout(menuGrid);
        mnuIndicatorPopup.add(indicatorsMenu);
        Arrays.sort(otherIndicators);
        for (int i = 0; i < otherIndicators.length; i++) {
            if (otherIndicators[i].trim().equals("")) continue;
            if (otherIndicators[i].trim().equals(Language.getString("IND_NET_ORDERS")) || otherIndicators[i].trim().equals(Language.getString("IND_NET_TURNOVER"))) {
                continue;
            }
            final String indicatorName = otherIndicators[i];
            TWMenuItem item = new TWMenuItem(indicatorName);
            indicatorsMenu.add(item);
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    Integer ObjInt = indicatorMap.get(indicatorName);
                    indicatorClicked(e.getSource(), ObjInt);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/graph_unchecked.gif"));
        }

        mnuCI2 = new TWMenu(Language.getString("CUSTOM_INDICATORS"), iconPath);
        mnuIndicatorPopup.add(mnuCI2);

        TWMenu allMenu = new TWMenu(Language.getString("ALL"), iconPath);
        String[] sa = getIndicators();
        menuRows = sa.length / 2;
        menuGrid = new VerticalGridLayout(menuRows, 2);
        allMenu.getPopupMenu().setLayout(menuGrid);
        mnuIndicatorPopup.add(allMenu);
        if (sa != null) {
            for (int i = 0; i < sa.length; i++) {
                if (sa[i].trim().equals("")) continue;
                if (sa[i].trim().equals(Language.getString("IND_NET_ORDERS")) || sa[i].trim().equals(Language.getString("IND_NET_TURNOVER"))) {
                    continue;
                }
                TWMenuItem item = new TWMenuItem(sa[i]);
                allMenu.add(item);
                final String indicatorName = sa[i]; //Get the indicator name to search in hashmap(need for sorting) - Pramoda
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        Integer ObjInt = indicatorMap.get(indicatorName); // get the original index after sorting - Pramoda
                        indicatorClicked(e.getSource(), ObjInt);
                    }
                });
                item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/graph_unchecked.gif"));
            }
        }
        GUISettings.applyOrientation(mnuIndicatorPopup);*/


    }

    public TWMenu getTemplateSubMenu(String sname, final File[] faTemplates) {
        TWMenu oMenu = new TWMenu(sname, "graphApplyTemplate.gif");
        // saFileNames must contain all default templates first and then all the user templates
        for (int i = 0; i < faTemplates.length; i++) {
            String fileName = faTemplates[i].getName();
            if (fileName.trim().equals("")) continue;
            if (i == TemplateFactory.TemplatePaths.length) { // seperator between default and user templates
                oMenu.add(new TWSeparator());
            }
            String itemName = (i < TemplateFactory.TemplatePaths.length) ? TemplateFactory.TemplateNames[i] : fileName.substring(0, fileName.indexOf("."));
            TWMenuItem item = new TWMenuItem(itemName);
            oMenu.add(item);
            final int index = i;
            item.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    File file = faTemplates[index];
                    templateApplied(e.getSource(), file);
                }
            });
            item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }

        oMenu.add(new TWSeparator());

        TWMenuItem item = new TWMenuItem(Language.getString("OTHER_TEMPLATES"));
        item.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                applyTemplate();
            }
        });
        item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        oMenu.add(item);

        GUISettings.applyOrientation(oMenu);

        return oMenu;
    }


    public TWMenu getZoomingSubMenu() {
        TWMenu oMenu = new TWMenu(Language.getString("GRAPH_RESET_ZOOM"), "graphResetZooming.gif");

        TWMenuItem item1 = new TWMenuItem(Language.getString("GRAPH_RESET_H_ZOOM"));
        item1.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        oMenu.add(item1);

        item1.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                int histoyMargin = ChartOptions.getCurrentOptions().chart_right_margin_history;
                int intraDayMargin = ChartOptions.getCurrentOptions().chart_right_margin_intraday;
                graph.GDMgr.setRightMargin(histoyMargin, intraDayMargin);
                
                graph.zSlider.setZoomAndPos(100f, 0f);
                graph.GDMgr.setZoomAndPos(100f, 0f);

            }
        });


        TWMenuItem item2 = new TWMenuItem(Language.getString("GRAPH_RESET_V_ZOOM"));
        item2.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        oMenu.add(item2);

        item2.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                graph.GDMgr.resetCustomYBounds();
                graph.reDrawAll();
            }
        });

        TWMenuItem item3 = new TWMenuItem(Language.getString("GRAPH_RESET_BOTH"));
        item3.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        oMenu.add(item3);

        item3.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                int histoyMargin = ChartOptions.getCurrentOptions().chart_right_margin_history;
                int intraDayMargin = ChartOptions.getCurrentOptions().chart_right_margin_intraday;
                graph.GDMgr.setRightMargin(histoyMargin, intraDayMargin);

                graph.zSlider.setZoomAndPos(100f, 0f);
                graph.GDMgr.setZoomAndPos(100f, 0f);
                graph.GDMgr.resetCustomYBounds();
                graph.reDrawAll();
            }
        });

        GUISettings.applyOrientation(oMenu);

        return oMenu;
    }


    public void baseSymbolClicked(java.lang.Object source, java.lang.Integer index) {
        if (!ChartInterface.isOfflineDataAvailable()) {
            paintMenuItemAsUnSelected(index.intValue(), mnuSubBaseSymbol);
            return;
        }
        setGraphBaseSymbol(g_oBaseSymbols.getSymbols()[index.intValue()]);
    }

    public void compareSymbolClicked(java.lang.Object source, java.lang.Integer index) {
        if (!ChartInterface.isOfflineDataAvailable()) {
            paintMenuItemAsUnSelected(index.intValue(), mnuSubCompare);
            return;
        }
        TWMenuItem mi = (TWMenuItem) source;
        if (mi.isSelected()) {
            setComparisonSymbol(g_oComparedSymbols.getSymbols()[index.intValue()]);
        } else {
            removeCompareSymbol(g_oComparedSymbols.getSymbols()[index.intValue()]);
        }
    }

    public void indicatorClicked(java.lang.Object source, java.lang.Integer index) {
        if (!ChartInterface.isIndicatorsEnabled()) {
            new ShowMessage(Language.getString("MSG_NOT_SUBSCRIBED_FOR_INDICATORS"), "I");
            return;
        }
        // TODO : put the market indicator mode. - Mevan
        addIndicator(index.intValue() + 1);  // not zero based 1, 2, 3 ..
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
            //todo: need to add listener
            DataWindow window = parent.getDataWindow();
            graph.GDMgr.refreshDataWindow();
        }
    }

    public void indicatorClicked(String indicatorName) {
        Integer ObjInt = indicatorMap.get(indicatorName);
        indicatorClicked(null, ObjInt);
    }

    public void templateApplied(java.lang.Object source, File templateFile) {
        if (!ChartInterface.isIndicatorsEnabled()) return;
        //File templateFile = new File(GraphFrame.FILEPATH_CHART_TEMPLATES + fileName);
        if (!TemplateFactory.LoadTemplate(this, templateFile, this.graph.Symbol, detached)) {
            JOptionPane.showMessageDialog(this, Language.getString("MSG_INVALID_CHART_TEMPLATE"),
                    Language.getString("GRAPH_OPEN_TEMPLATE"), JOptionPane.WARNING_MESSAGE);
        }
    }

    public void modeClicked(java.lang.Object source, java.lang.Integer index) {
        setGraphMode(index.equals(new Integer(1)), graph.isShowPercentages(), true); //toggling -isRemoveOHLCRequest = true
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
        }
    }

    public void periodClicked(java.lang.Object source, java.lang.Integer index) {
        setGraphPeriod(index.intValue());
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
        }
    }

    public void intervalClicked(java.lang.Object source, java.lang.Integer index) {

        setGraphInterval(index.intValue());
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
        }
    }

    public void styleClicked(java.lang.Object source, java.lang.Integer index) {

        setGraphStyle(index.intValue());
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
        }
    }

    public void ShowHideISPClicked(java.lang.Object source, java.lang.Integer index) {
//         setGraphStyle(index.intValue());
//        if (parent != null) {
//            refreshTopToolBar(parent.topToolBar);
//        }
    }

    public void percentClicked(ActionEvent e) {
        if ((graph == null) || (graph.GDMgr == null)) return;
        TWCheckBoxMenuItem mi = (TWCheckBoxMenuItem) e.getSource();
        if (mi.isSelected()) {
            graph.GDMgr.setIndexed(true);
        } else {
            graph.GDMgr.setIndexed(false);
        }
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
        }
    }

    public void showVolumeClicked(ActionEvent e) {

        JMenuItem mi = (JMenuItem) e.getSource();
        graph.setVolumeGraphflag(mi.isSelected());
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
        }
    }

    public void showVolumeByPriceClicked(ActionEvent e) {

        JMenuItem mi = (JMenuItem) e.getSource();
        graph.GDMgr.setShowVolumeByPrice(mi.isSelected());
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
        }
    }

    public void showTurnoverClicked(ActionEvent e) {

        JMenuItem mi = (JMenuItem) e.getSource();
        graph.setTurnOverGraphFlag(mi.isSelected());
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
        }
    }

    public void showCurrentPriceClicked() {
        graph.GDMgr.toggleShowCurrentPriceLine();
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
        }
    }

    public void showPreviousCloseLineClicked() {
        graph.GDMgr.toggleShowPreviousCloseLine();
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
        }
    }

    public void showBidAskTagsClicked() {
        graph.GDMgr.toggleShowBidAskTags();
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
        }
    }

    public void showMinMaxLinesClicked() {
        graph.GDMgr.toggleShowMinMaxPriceLines();
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
        }
    }

    public void showOrderLinesClicked() {
        graph.GDMgr.toggleShowOrderLines();
        if (parent != null) {
            refreshTopToolBar(parent.topToolBar);
        }
    }

    public void refreshClicked(ActionEvent e) {
        graph.reDrawAll();
    }

    public void legendClicked(java.lang.Object source, java.lang.Integer index) {
        graph.setLegendType(index.intValue());
    }

    public void yAxisClicked(java.lang.Object source, java.lang.Integer index) {
        graph.yAxisPos = (byte) index.intValue();
        graph.reDrawAll();
    }

    public void rightMarginClicked(ActionEvent e) {
        graph.GDMgr.setRightMargin();
    }

    public void setGraphMode(boolean isIntraday, boolean isRemoveOHLCRequest) {//boolean isRemoveOHLCRequest
        //load interval , period from the relevat template
        setGraphMode(isIntraday, graph.isShowPercentages(), false, isRemoveOHLCRequest);
        prepareTable();
        graph.GDMgr.refreshDataWindow();
    }

    public void setGraphMode(boolean isIntraday, boolean fromWSP, boolean isRemoveOHLCRequest) {

        setGraphMode(isIntraday, graph.isShowPercentages(), fromWSP, isRemoveOHLCRequest);
        prepareTable();
        //graph.GDMgr.refreshDataWindow();
    }

    private void setGraphMode(boolean isIntraday, boolean isPercent, boolean fromWSP, boolean isRemoveOHLCRequest) {
        if ((!isIntraday) && (!ChartInterface.isHistoryEnabled()) && (Settings.isConnected())) {
            new ShowMessage(Language.getString("MSG_NOT_SUBSCRIBED_FOR_HISTORY"), "I");
            return;
        }

        /***********************mode based features ******************/
        if (isIntraday) {  //no need to show the VWAP values in intraday mode
            graph.GDMgr.setVWAPEnable(false);
        } else {
            //graph.getGDMgr().setShowResistanceLine(false);
        }
        /***********************mode based features ******************/


        GraphDataManager gdm = graph.GDMgr;
        if (gdm == null) return;
        graph.NotifyChange = false;
        graph.GraphReset = false;
        graph.setGraphVisible(true);
        graph.isZoomed = false;


        CurrentMode = isIntraday;
        graph.setCurrentMode(isIntraday);
        graph.NotifyChange = false;
        graph.setPercentagesflag(isPercent);
        /***********************default template loading -sathyajith***********************/
        graph.setIntervalFlag(isIntraday ? StockGraph.EVERYMINUTE : StockGraph.DAILY);
        graph.setPeriodFlag(isIntraday ? StockGraph.ONE_DAY : StockGraph.ONE_YEAR);
        /***********************default template loading -sathyajith***********************/

        synchronized (gdm.getGraphLock()) {
            gdm.clearEntireGraphStore();
            //gdm.getObjArray().clear();
            ////////added to save horizontal line
            ArrayList<AbstractObject> tmpLst = (ArrayList<AbstractObject>) gdm.getObjArray().clone();
            for (int i = 0; i < gdm.getObjArray().size(); i++) {
                AbstractObject ao = (AbstractObject) gdm.getObjArray().get(i);
                switch (ao.getObjType()) {
                    case AbstractObject.INT_LINE_HORIZ:
                    case AbstractObject.INT_LINE_SLOPE:
                        break;
                    default:
                        tmpLst.remove(ao);
                }
            }
            gdm.setObjArray(tmpLst);
            //////////////////////////
            gdm.tmpLatestTime = 0;
            for (int i = 0; i < gdm.getSources().size(); i++) {
                ChartProperties cp = (ChartProperties) gdm.getSources().get(i);
                if (cp.getID() <= GraphDataManager.ID_COMPARE) {
                    if (isRemoveOHLCRequest) {
                        ChartInterface.removeOHLCRequest(!isIntraday, cp.getSymbol());
                    }
                    if (isIntraday) {
                        records = OHLCStore.getInstance().addIntradayRequest(cp.getSymbol());
                    } else {
                        records = OHLCStore.getInstance().addHistoryRequest(cp.getSymbol());
                    }
                    cp.setDataSource(records.getList());
                } else {
                    cp.setDataSource(((Indicator) cp).getInnerSource().getDataSource());
                }
            }

            for (int i = 0; i < gdm.getSources().size(); i++) {
                ChartProperties cp = (ChartProperties) gdm.getSources().get(i);
                cp.setRecordSize(0);
            }

            //IMPORTANT-charithn
            //System.out.println("GraphMode update datasources interval: " + graph.GDMgr.getInterval());
            gdm.updateDataSources();

            gdm.requestLastRecord();
            gdm.setBeginIndex(0);
            gdm.setEndIndex(0);
            gdm.setPeriodBeginIndex(graph.getPeriod(), true);
            //System.out.println("GraphMode interval: " + graph.GDMgr.getInterval());
            gdm.insertDummyPoints(gdm.getTimeMillisec(gdm.getGraphStore().size() - 1));
        }
        graph.GDMgr.resetCustomYBounds();
        graph.NotifyChange = true;
        if (isIntraday) {  //no need to show the VWAP values in intraday mode
            graph.GDMgr.setVWAP(false);
        }
        graph.reDrawAll();

    }

    /**
     * **Toggling the mode ,specifying period ,etc not happining here ,Those were pereextracted from the templates -
     * sathyajith 16-01-2009  * *****************
     */
    public void setClientGraphMode(boolean isRemoveOHLCRequest) {//boolean isRemoveOHLCRequest
        //graph.isCurrentMode(); becuase this has set now using the template

        setClientGraphMode(graph.isCurrentMode(), graph.isShowPercentages(), false, isRemoveOHLCRequest);
        prepareTable();
    }

    private void setClientGraphMode(boolean isIntraday, boolean isPercent, boolean fromWSP, boolean isRemoveOHLCRequest) {
        if ((!isIntraday) && (!ChartInterface.isHistoryEnabled()) && (Settings.isConnected())) {
            new ShowMessage(Language.getString("MSG_NOT_SUBSCRIBED_FOR_HISTORY"), "I");
            return;
        }

        GraphDataManager gdm = graph.GDMgr;
        if (gdm == null) return;
        graph.NotifyChange = false;
        graph.GraphReset = false;
        graph.setGraphVisible(true);
        graph.isZoomed = false;

        CurrentMode = isIntraday; // this is needed as this is for setiing the mode force fully
        graph.setCurrentMode(isIntraday); //these are not needed acutually since the mode is already set during template loading

        /***********************mode based features ******************/
        if (isIntraday) {  //no need to show the VWAP values in intraday mode
            graph.GDMgr.setVWAPEnable(false);
        } else {
            graph.getGDMgr().setShowResistanceLine(false);
        }
        /***********************mode based features ******************/


        graph.NotifyChange = false;
        graph.setPercentagesflag(isPercent);
        /***********************default template loading -sathyajith***********************/
//        graph.setIntervalFlag(isIntraday ? StockGraph.EVERYMINUTE : StockGraph.DAILY);
//        graph.setPeriodFlag(isIntraday ? StockGraph.ONE_DAY : StockGraph.ONE_YEAR);

        /***********************default template loading -sathyajith***********************/

        synchronized (gdm.getGraphLock()) {
            gdm.clearEntireGraphStore();
            gdm.getObjArray().clear();
            for (int i = 0; i < gdm.getSources().size(); i++) {
                ChartProperties cp = (ChartProperties) gdm.getSources().get(i);
                if (cp.getID() <= GraphDataManager.ID_COMPARE) {

                    if (isRemoveOHLCRequest) {
                        ChartInterface.removeOHLCRequest(!isIntraday, cp.getSymbol());
                    }

                    if (isIntraday) {
                        records = OHLCStore.getInstance().addIntradayRequest(cp.getSymbol());
                    } else {
                        records = OHLCStore.getInstance().addHistoryRequest(cp.getSymbol());
                    }
                    cp.setDataSource(records.getList());
                } else {
                    cp.setDataSource(((Indicator) cp).getInnerSource().getDataSource());
                }
            }

            for (int i = 0; i < gdm.getSources().size(); i++) {
                ChartProperties cp = (ChartProperties) gdm.getSources().get(i);
                cp.setRecordSize(0);
            }

            //IMPORTANT-charithn
            gdm.updateDataSources();

            gdm.requestLastRecord();
            gdm.setBeginIndex(0);
            gdm.setEndIndex(0);
            gdm.setPeriodBeginIndex(graph.getPeriod(), true);
            gdm.insertDummyPoints(gdm.getTimeMillisec(gdm.getGraphStore().size() - 1));
        }
        graph.GDMgr.resetCustomYBounds();
        graph.NotifyChange = true;
        if (isIntraday) {  //no need to show the VWAP values in intraday mode
            graph.GDMgr.setVWAP(false);
        }
        graph.reDrawAll();

    }


    public void setCashFlowGraphMode(boolean isIntraday, boolean isRemoveOHLCRequest) {//boolean isRemoveOHLCRequest
        setCashFlowGraphMode(isIntraday, graph.isShowPercentages(), false, isRemoveOHLCRequest);
        prepareTable();  //tODO PREPARE TABLE IS NOT NEEDED
    }

    private void setCashFlowGraphMode(boolean isIntraday, boolean isPercent, boolean fromWSP, boolean isRemoveOHLCRequest) {
        if ((!isIntraday) && (!ChartInterface.isHistoryEnabled()) && (Settings.isConnected())) {
            new ShowMessage(Language.getString("MSG_NOT_SUBSCRIBED_FOR_HISTORY"), "I");
            return;
        }
        GraphDataManager gdm = graph.GDMgr;
        if (gdm == null) return;
        graph.NotifyChange = false;
        graph.GraphReset = false;
        graph.setGraphVisible(true);
        graph.isZoomed = false;

        CurrentMode = isIntraday;
        graph.setCurrentMode(isIntraday);
        graph.NotifyChange = false;
        graph.setPercentagesflag(isPercent);
        graph.setIntervalFlag(isIntraday ? StockGraph.EVERYMINUTE : StockGraph.DAILY);
        graph.setPeriodFlag(isIntraday ? StockGraph.ONE_DAY : StockGraph.CUSTOM_PERIODS);

        synchronized (gdm.getGraphLock()) {
            gdm.clearEntireGraphStore();
            gdm.getObjArray().clear();
            for (int i = 0; i < gdm.getSources().size(); i++) {
                ChartProperties cp = (ChartProperties) gdm.getSources().get(i);
                if (cp.getID() <= GraphDataManager.ID_COMPARE) {
                    if (isRemoveOHLCRequest) {
                        ChartInterface.removeOHLCRequest(!isIntraday, cp.getSymbol());
                    }
                    if (isIntraday) {
                        records = OHLCStore.getInstance().addIntradayRequest(cp.getSymbol());
                    } else {
                        records = OHLCStore.getInstance().addHistoryRequest(cp.getSymbol());
                    }
                    cp.setDataSource(records.getList());
                } else {
                    cp.setDataSource(((Indicator) cp).getInnerSource().getDataSource());
                }
            }

            for (int i = 0; i < gdm.getSources().size(); i++) {
                ChartProperties cp = (ChartProperties) gdm.getSources().get(i);
                cp.setRecordSize(0);
            }

            //IMPORTANT-charithn
            gdm.updateDataSources();

            gdm.requestLastRecord();
            gdm.setBeginIndex(0);
            gdm.setEndIndex(0);
            gdm.setPeriodBeginIndex(graph.getPeriod(), true);
            gdm.insertDummyPoints(gdm.getTimeMillisec(gdm.getGraphStore().size() - 1));
        }
        graph.GDMgr.resetCustomYBounds();
        graph.NotifyChange = true;
        graph.reDrawAll();
    }

    public int getIndexOfString(String aStr, String[] sa) {
        if (sa == null) return -1;
        int elements = sa.length;
        if (elements > 0) {
            for (int i = 0; i < elements; i++) {
                if (sa[i].equals(aStr)) {
                    return i;
                }
            }
        }
        return -1;
    }

    private void setGraphInterval(int index) {
        String Interval;
        //long bTime = graph.GDMgr.getBeginTime();
        //long eTime = graph.GDMgr.getEndTime();

        float zm = graph.zSlider.getZoom();
        float ps = graph.zSlider.getBeginPos();

        try {
            if (CurrentMode) {
                Interval = strCurrentIntervals[index];
            } else {
                Interval = strHistoryIntervals[index];
            }
            long Intvl;
            if (Interval.equals(Language.getString("OTHER_PERIODS"))) {
                CustomDialog dlg = CustomDialogs.getCustomIntervalsDialog(graph.GDMgr.currentCustomIntervalType,
                        graph.GDMgr.currentCustomIntervalFactor, CurrentMode);
                dlg.setModal(true);
                dlg.setLocationRelativeTo(this);
                dlg.pack();
                dlg.setResizable(false);
                dlg.setVisible(true);
                long[] intervalSettings;
                if (dlg.result != null) {
                    intervalSettings = (long[]) dlg.result;
                } else {
                    return;
                }
                ChartSettings.getSettings().setCustomIntervalProperties(intervalSettings[0], intervalSettings[1]);
                graph.GDMgr.currentCustomIntervalType = intervalSettings[0];
                graph.GDMgr.currentCustomIntervalFactor = intervalSettings[1];
            }
            Intvl = graph.getIntervalConst(Interval);
            graph.setInterval(Intvl);
        } catch (Exception ex) {
        }
        graph.setPeriod(graph.getPeriod());
        //graph.GDMgr.setBeginAndEndIndecies(bTime, eTime);
        graph.zSlider.setZoomAndPos(zm, ps);
        graph.GDMgr.refreshDataWindow();

    }

    public void popupGraphData(String sSymbol, boolean forcedModeIntraday) {

        long entryTime = System.currentTimeMillis();
        if (!ChartInterface.isOfflineDataAvailable()) {
            return;
        }
        /***********************default template loading -sathyajith***********************/
        //just checking for new chart
        String tempFilePath = TemplateFactory.DEFAULT_TEMPLATE_FILE;

        if (!usingDefaultTemplate) {
            if (forcedModeIntraday) {
                tempFilePath = TemplateFactory.INTRADAY_DEFAULT_TEMPLATE_FILE;
            } else {
                tempFilePath = TemplateFactory.HISTORY_DEFAULT_TEMPLATE_FILE;
            }
        }

        long entryTimeLoadTemplate = System.currentTimeMillis();
        TemplateFactory.LoadTemplate(this, new File(tempFilePath), sSymbol, detached);
        //System.out.println("**** loadTemplatepop  Calc time " + (entryTimeLoadTemplate - System.currentTimeMillis()));

        /***********************default template loading -sathyajith*******************remove the below comment****/

        //TemplateFactory.LoadTemplate(this, new File(TemplateFactory.DEFAULT_TEMPLATE_FILE), sSymbol, detached);


        try {
            tableModel.setDecimalCount(SharedMethods.getDecimalPlaces(sSymbol));
        } catch (Exception e) {
            e.printStackTrace();
        }
        //System.out.println("**** popGraphData Calc time " + (entryTime - System.currentTimeMillis()));
    }

    public void popupCashFlowGraphData(String sSymbol) {
        //set graph data included here
        if (!ChartInterface.isOfflineDataAvailable()) {
            return;
        }
        TemplateFactory.LoadTemplate(this, new File(TemplateFactory.DEFAULT_TEMPLATE_FILE), sSymbol, detached);

        try {
            tableModel.setDecimalCount(SharedMethods.getDecimalPlaces(sSymbol));
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public void setGraphData(String sSymbol) {

    }

    public void setCashFlowGraphData(String sSymbol) {
        if (!ChartInterface.isOfflineDataAvailable()) {
            return;
        }
        TemplateFactory.LoadTemplate(this, new File(TemplateFactory.DEFAULT_TEMPLATE_FILE), sSymbol, detached);
    }

    public static int getIndexOf(String s, String[] sa) {
        if (sa != null)
            for (int i = 0; i < sa.length; i++) {
                if (sa[i].equals(s)) {
                    return i;
                }
            }
        return -1;
    }

    public void selectMenuItemByIndex(int index, TWMenu aMenu) {
        TWMenuItem miSelected = null;
        try {
            miSelected = (TWMenuItem) aMenu.getItem(index);
        } catch (Exception ex) {
        }
        if (miSelected != null)
            miSelected.doClick();
    }

    public void paintMenuItemAsSelected(int index, TWMenu aMenu) {
        paintMenuItemAsSelected(index, aMenu, null);
    }

    public void paintMenuItemAsSelected(int index, TWMenu aMenu, Icon ico) {
        if (index < 0) return;
        try {
            TWMenuItem miSelected = (TWMenuItem) aMenu.getItem(index);
            miSelected.setSelected(true);
            if (ico == null) {
                miSelected.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_checked.gif"));
            } else {
                miSelected.setIcon(ico);
            }
        } catch (Exception ex) {
        }
    }

    public void paintMenuItemAsUnSelected(int index, TWMenu aMenu) {
        if (index < 0) return;
        try {
            TWMenuItem miUnSelected = (TWMenuItem) aMenu.getItem(index);
            miUnSelected.setSelected(false);
            miUnSelected.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        } catch (Exception ex) {
        }
    }


    public void addCompareSymboltoStrArray(String aSymbol) {
        int preLen = strCompareSymbols.length;
        int symbolIndex = getIndexOf(aSymbol, strCompareSymbols);
        if (symbolIndex < 0) {
            String[] tmpSA = new String[preLen + 1];
            for (int i = 0; i < preLen; i++) {
                tmpSA[i] = strCompareSymbols[i];
            }
            tmpSA[preLen] = aSymbol;
            strCompareSymbols = tmpSA;
            tmpSA = null;
            symbolIndex = preLen;
            refreshCompareSymbolsSubMenu();
            markComparingSymbols();
        }
        selectMenuItemByIndex(symbolIndex, mnuSubCompare);
    }

    public String validateAndSetBaseSymbol(String symbol) {
        if (!ChartInterface.isOfflineDataAvailable()) {
            return "";
        }

        if ((symbol == null) || symbol.trim().equals("")) {
            return "";
        }
        graph.GraphReset = false;
        graph.NotifyChange = false;
        graph.setBaseGraph(false);
        graph.isZoomed = false;

        if (!isAlreadyValidated(symbol)) {

            baseSymbol = null;
            registeredBaseSymbol = symbol;
            currentBaseSymbol = symbol;
            if (!isAlreadyAddedforValidation(ReqID_BaseSymbol + Meta.FD + symbol)) {
                requestedSymbolQueue.add(ReqID_BaseSymbol + Meta.FD + symbol);
                return ChartInterface.validateSymbol(false, Constants.IndGraphWindow, (byte) Meta.INSTRUMENT_QUOTE, symbol, this, true);
            }

            try {
                tableModel.setDecimalCount(SharedMethods.getDecimalPlaces(symbol)); //high priority add to Pramoda
            } catch (Exception e) {
            }
        }
        return "";
    }

    // base symbol
    public void cmbSymbol_actionPerformed(KeyEvent e) {
        if (!ChartInterface.isOfflineDataAvailable()) {
            return;
        }

        JTextField cmbSymbol;
        if (e.getSource() instanceof JTextField) {
            cmbSymbol = (JTextField) e.getSource();
        } else {
            return;
        }

        try {
            GraphFrame gf = ChartFrame.getSharedInstance().getActiveGraph();
            if (gf != null) {
                TemplateFactory.SaveTemplate(new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE), new GraphFrame[]{gf});
                String symbol = cmbSymbol.getText().trim().toUpperCase();
                String key = validateAndSetBaseSymbol(symbol);
                if (key != null && !key.equals("")) {
                    TemplateFactory.LoadTemplate(gf, new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE), key, gf.isGraphDetached());
                }
            } else {
                String symbol = cmbSymbol.getText().trim().toUpperCase();
                String key = validateAndSetBaseSymbol(symbol);
                ChartFrame.getSharedInstance().addNewGraph(true, false, key);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
    }

    private boolean isAlreadyAddedforValidation(String ValidateRequest) {
        String s;
        for (String element : requestedSymbolQueue) {
            if (element.equals(ValidateRequest)) return true;
        }
        return false;
    }

    private boolean isAlreadyValidated(String symbol) {
        return g_oBaseSymbols.isAlreadyIn(symbol);
    }

    void cmbStyle_itemStateChanged(ItemEvent e) {
    }

    private void setGraphStyle(int index) {
        // items must be ordered correctly in the submenu
        //correct order is as follows
        //  0 - line
        //  1 - bar
        //  2 - hlc
        //  3 - ohlc
        //  4 - candle
        graph.setGraphStyle(index);
    }

    void cmbPeriod_itemStateChanged(ItemEvent e) {
    }

    private void setGraphPeriod(int index) {
        graph.isZoomed = false;
        String[] saPrds = strHistoryPeriods;
        if (CurrentMode) saPrds = strCurrentPeriods;
        String period = saPrds[index];
        int prd = graph.getPeriodConst(period);
        if (prd == StockGraph.CUSTOM_PERIODS) {
            CustomDialog dlg = CustomDialogs.getCustomTimePeriodsDialog(graph.GDMgr.currentCustomPeriod,
                    graph.GDMgr.currentCustomRange, CurrentMode);
            dlg.setModal(true);
            dlg.setLocationRelativeTo(this);
            dlg.pack();
            dlg.setResizable(false);
            dlg.setVisible(true);
            int[] periodSettings;
            if (dlg.result != null) {
                periodSettings = (int[]) dlg.result;
            } else {
                return;
            }
            ChartSettings.getSettings().setCustomPeriodProperties(periodSettings[0], periodSettings[1]);
            graph.GDMgr.setCustomBeginPeriods(periodSettings[0], periodSettings[1]);
        }
        graph.setPeriod(prd);
        graph.NotifyChange = true;
        graph.GDMgr.refreshDataWindow();
    }

    boolean isSymbolAlreadyComparing(String selectedSymbol) {
        if (saComparingSymbols == null) return false;
        int elements = saComparingSymbols.length;
        if (elements > 0) {
            for (int i = 0; i < elements; i++) {
                if (saComparingSymbols[i].equals(selectedSymbol)) {
                    return true;
                }
            }
        }
        return false;
    }

    private void addIndicator(int index) {
        StockGraph g = graph;
        if ((g == null) || (g.GDMgr == null)) return;
        switch (index) {
            case INDEX_MOVING_AVG:
                g.GDMgr.addIndicator(GraphDataManager.ID_MOVING_AVERAGE);
                break;
            case INDEX_RSI:
                g.GDMgr.addIndicator(GraphDataManager.ID_RSI);
                break;
            case INDEX_MACD:
                g.GDMgr.addIndicator(GraphDataManager.ID_MACD);
                break;
            case INDEX_MOMENTUM:
                g.GDMgr.addIndicator(GraphDataManager.ID_MOMENTUM);
                break;
            case INDEX_LRI:
                g.GDMgr.addIndicator(GraphDataManager.ID_LRI);
                break;
            case INDEX_SDI:
                g.GDMgr.addIndicator(GraphDataManager.ID_SDI);
                break;
            case INDEX_BOLLINGER_BANDS:
                g.GDMgr.addIndicator(GraphDataManager.ID_BOLLINGER_BANDS);
                break;
            case INDEX_VOLUME:
                g.GDMgr.addIndicator(GraphDataManager.ID_VOLUME);
                break;
            case INDEX_ACCUM_DISTRI:
                g.GDMgr.addIndicator(GraphDataManager.ID_ACCU_DISTRI);
                break;
            case INDEX_WILLIAMS_R:
                g.GDMgr.addIndicator(GraphDataManager.ID_WILLS_R);
                break;
            case INDEX_STOCAS_OSCIL:
                g.GDMgr.addIndicator(GraphDataManager.ID_STOCHAST_OSCILL);
                break;
            case INDEX_AVG_TRUE_RANGE:
                g.GDMgr.addIndicator(GraphDataManager.ID_AVG_TRUE_RANGE);
                break;
            case INDEX_MASS_INDEX:
                g.GDMgr.addIndicator(GraphDataManager.ID_MASS_INDEX);
                break;
            case INDEX_MEDIAN_PRICE:
                g.GDMgr.addIndicator(GraphDataManager.ID_MEDIAN_PRICE);
                break;
            case INDEX_MONEY_FLOW_INDEX:
                g.GDMgr.addIndicator(GraphDataManager.ID_MONEY_FLOW_INDEX);
                break;
            case INDEX_TRIX:
                g.GDMgr.addIndicator(GraphDataManager.ID_TRIX);
                break;
            case INDEX_VOLATILITY:
                g.GDMgr.addIndicator(GraphDataManager.ID_VOLATILITY);
                break;
            case INDEX_ZIGZAG:
                g.GDMgr.addIndicator(GraphDataManager.ID_ZIGZAG);
                break;
            case INDEX_RMI:
                g.GDMgr.addIndicator(GraphDataManager.ID_RMI);
                break;
            case INDEX_AROON:
                g.GDMgr.addIndicator(GraphDataManager.ID_AROON);
                break;
            case INDEX_CHAIKINS_OSCILL:
                g.GDMgr.addIndicator(GraphDataManager.ID_CHAIKINS_OSCILL);
                break;
            case INDEX_CHAIKINS_MONEY_FLOW:
                g.GDMgr.addIndicator(GraphDataManager.ID_CHAIKIN_MONEY_FLOW);
                break;
            case INDEX_CCI:
                g.GDMgr.addIndicator(GraphDataManager.ID_CCI);
                break;
            case INDEX_EASE_OF_MOVEMENT:
                g.GDMgr.addIndicator(GraphDataManager.ID_EASE_OF_MOVEMENT);
                break;
            case INDEX_ENVILOPES:
                g.GDMgr.addIndicator(GraphDataManager.ID_ENVILOPES);
                break;
            case INDEX_ICHIMOKU_KINKO_HYO:
                g.GDMgr.addIndicator(GraphDataManager.ID_ICHIMOKU_KINKO_HYO);
                break;
            case INDEX_KLINGER_OSCILL:
                g.GDMgr.addIndicator(GraphDataManager.ID_KLINGER_OSCILL);
                break;
            case INDEX_PARABOLIC_SAR:
                g.GDMgr.addIndicator(GraphDataManager.ID_PARABOLIC_SAR);
                break;
            case INDEX_PRICE_VOLUME_TREND:
                g.GDMgr.addIndicator(GraphDataManager.ID_PRICE_VOLUME_TREND);
                break;
            case INDEX_PRICE_OSCILL:
                g.GDMgr.addIndicator(GraphDataManager.ID_PRICE_OSCILL);
                break;
            case INDEX_VOLUME_OSCILL:
                g.GDMgr.addIndicator(GraphDataManager.ID_VOLUME_OSCILL);
                break;
            case INDEX_WILLS_ACCU_DISTRI:
                g.GDMgr.addIndicator(GraphDataManager.ID_WILLS_ACCU_DISTRI);
                break;
            case INDEX_INTRADAY_MOMENTUM_INDEX:
                g.GDMgr.addIndicator(GraphDataManager.ID_INTRADAY_MOMENTUM_INDEX);
                break;
            case INDEX_ON_BALANCE_VOLUME:
                g.GDMgr.addIndicator(GraphDataManager.ID_ON_BALANCE_VOLUME);
                break;
            case INDEX_ROC:
                g.GDMgr.addIndicator(GraphDataManager.ID_ROC);
                break;
            case INDEX_WILDERS_SMOOTHING:
                g.GDMgr.addIndicator(GraphDataManager.ID_WILDERS_SMOOTHING);
                break;
            case INDEX_PLUS_DI:
                g.GDMgr.addIndicator(GraphDataManager.ID_PLUS_DI);
                break;
            case INDEX_MINUS_DI:
                g.GDMgr.addIndicator(GraphDataManager.ID_MINUS_DI);
                break;
            case INDEX_DX:
                g.GDMgr.addIndicator(GraphDataManager.ID_DX);
                break;
            case INDEX_ADX:
                g.GDMgr.addIndicator(GraphDataManager.ID_ADX);
                break;
            case INDEX_ADXR:
                g.GDMgr.addIndicator(GraphDataManager.ID_ADXR);
                break;
            case INDEX_DPO:
                g.GDMgr.addIndicator(GraphDataManager.ID_DPO);
                break;
            case INDEX_SWING:
                g.GDMgr.addIndicator(GraphDataManager.ID_SWING_INDEX);
                break;
            case INDEX_RVI:
                g.GDMgr.addIndicator(GraphDataManager.ID_RVI);
                break;
            case INDEX_MFI:
                g.GDMgr.addIndicator(GraphDataManager.ID_MFI);
                break;
            case INDEX_PERFORMANCE:
                g.GDMgr.addIndicator(GraphDataManager.ID_PERFORMANCE);
                break;
            case INDEX_PVI:
                g.GDMgr.addIndicator(GraphDataManager.ID_PVI);
                break;
            case INDEX_QSTICK:
                g.GDMgr.addIndicator(GraphDataManager.ID_QSTICK);
                break;
            case INDEX_TYPICAL_PRICE:
                g.GDMgr.addIndicator(GraphDataManager.ID_TYPICAL_PRICE);
                break;
            case INDEX_VERT_HORIZ_FILTER:
                g.GDMgr.addIndicator(GraphDataManager.ID_VERT_HORI_FILTER);
                break;
            case INDEX_WEIGHTED_CLOSE:
                g.GDMgr.addIndicator(GraphDataManager.ID_WEIGHTED_CLOSE);
                break;
            case INDEX_VOLUME_ROC:
                g.GDMgr.addIndicator(GraphDataManager.ID_VOLUME_ROC);
                break;
            case INDEX_DEMA:
                g.GDMgr.addIndicator(GraphDataManager.ID_DEMA);
                break;
            case INDEX_PRICE_CHANNEL:
                g.GDMgr.addIndicator(GraphDataManager.ID_PRICE_CHANNELS);
                break;
            case INDEX_TSF:
                g.GDMgr.addIndicator(GraphDataManager.ID_TSF);
                break;
            case INDEX_FO:
                g.GDMgr.addIndicator(GraphDataManager.ID_FO);
                break;
            case INDEX_NVI:
                g.GDMgr.addIndicator(GraphDataManager.ID_NVI);
                break;
            case INDEX_CMO:
                g.GDMgr.addIndicator(GraphDataManager.ID_CMO);
                break;
            case INDEX_SMO:
                g.GDMgr.addIndicator(GraphDataManager.ID_SMO);
                break;
            case INDEX_TEMA:
                g.GDMgr.addIndicator(GraphDataManager.ID_TEMA);
                break;
            case INDEX_ALLIGATOR:
                g.GDMgr.addIndicator(GraphDataManager.ID_ALLIGATOR);
                break;
            case INDEX_STD_ERR:
                g.GDMgr.addIndicator(GraphDataManager.ID_STD_ERR);
                break;
            case INDEX_MACD_VOLUME:
                g.GDMgr.addIndicator(GraphDataManager.ID_MACD_VOLUME);
                break;
            case INDEX_NET_ORDERS:
                g.GDMgr.addIndicator(GraphDataManager.ID_NET_ORDERS);
                break;
            case INDEX_NET_TURNOVER:
                g.GDMgr.addIndicator(GraphDataManager.ID_NET_TURNOVER);
                break;
            case INDEX_COPP_CURVE:
                g.GDMgr.addIndicator(GraphDataManager.ID_COPP_CURVE);
                break;
            case INDEX_DISPARITY_IDX:
                g.GDMgr.addIndicator(GraphDataManager.ID_DISPARITY_INDEX);
                break;
            case INDEX_HULL_MOVING_AVG:
                g.GDMgr.addIndicator(GraphDataManager.ID_HULL_MOVING_AVG);
                break;
            case INDEX_KELTNER_CHANNEL:
                g.GDMgr.addIndicator(GraphDataManager.ID_KELTNER_CHANNELS);
                break;
        }
        graph.GDMgr.refreshDataWindow();
    }

    public boolean isAnIndex(){
      return true;
    }

    boolean isItemInCmb(String selectedSymbol, JComboBox cmb) {

        int elements = cmb.getItemCount();
        for (int i = 0; i < elements; i++) {
            if (cmb.getItemAt(i).equals(selectedSymbol)) {
                return true;
            }
        }
        return false;
    }

    public void cmbCompare_actionPerformed(KeyEvent e) {
        if (!ChartInterface.isOfflineDataAvailable()) {
            return;
        }
        JTextField cmbCompare;
        if (e.getSource() instanceof JTextField) {
            cmbCompare = (JTextField) e.getSource();
        } else {
            return;
        }

        StockGraph g = graph;
        if (g == null) return;
        String selectedSymbol = cmbCompare.getText().trim().toUpperCase();
        if (selectedSymbol.trim().equals("")) {
            return;
        }

        try {
            if (g.GDMgr.isSymbolAlreadyDrawn(selectedSymbol)) {
            } else if (isAlreadyValidated(selectedSymbol)) {
                setComparisonSymbol(selectedSymbol);
            } else {
                baseSymbol = null;
                if (!isAlreadyAddedforValidation(ReqID_CompareSymbol + Meta.FD + selectedSymbol)) {
                    requestedSymbolQueue.add(ReqID_CompareSymbol + Meta.FD + selectedSymbol);
                    String key = ChartInterface.validateSymbol(false, Constants.IndGraphWindow, (byte) Meta.INSTRUMENT_QUOTE, selectedSymbol, this, false);
                    if (key == null || key.equals("")) {
                        //new ShowMessage(Language.getString("MSG_INVLID_SYMBOL"), "E");
                    }
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void removeCompareSymbol(String symbl) {
        String selected = symbl.toUpperCase();
        if (selected != " ") {
            graph.removeCompGraph(selected);
            graph.NotifyChange = false;
            graph.NotifyChange = true;
            graph.reDrawAll();
        }
    }


    public void setGraphBaseSymbol(String symbl) {
        graph.GraphReset = false;
        graph.setGraphVisible(true);
        graph.NotifyChange = false;
        graph.setBaseGraph(false);
        graph.isZoomed = false;
        currentBaseSymbol = symbl;

        try {
            TemplateFactory.SaveTemplate(new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE), new GraphFrame[]{this});
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        TemplateFactory.LoadTemplate(this, new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE), symbl, detached);
    }

    public boolean isAlreadyDrawn(String sKey) {
        if ((graph != null) && (graph.GDMgr != null)) {
            return graph.GDMgr.isSymbolAlreadyDrawn(sKey);
        }
        return false;
    }

    public void addGraphCompareSymbol(String symbl) {

        records = OHLCStore.getInstance().addIntradayRequest(symbl);
        currentCompareSymbol = symbl;
        processDataForCompareGraphData(records);
        records = null;
    }

    public void procesSymbolResponse(String sKey, boolean isFromTemplate) {
        String sRecord = null;
        int index = -1;

        sRecord = requestedSymbolQueue.remove(0);

        if (sKey != null) {
            index = Integer.parseInt(sRecord.substring(0, sRecord.indexOf(Meta.FD)));
            switch (index) {
                case ReqID_BaseSymbol:
                    sKey = sKey.substring(sKey.indexOf("|") + 1);
                    if (sKey.length() < 1)
                        sKey = null;
                    if (!isFromTemplate) {
                        TemplateFactory.LoadTemplate(this, new File(TemplateFactory.DEFAULT_TEMPLATE_FILE), sKey, detached);
                    }
                    break;
                case ReqID_CompareSymbol:

                    sKey = sKey.substring(sKey.indexOf("|") + 1);
                    if (sKey.length() < 1)
                        sKey = null;
                    setComparisonSymbol(sKey);
                    break;
            }
        } else {
            //graph.clearAll();
            //initialize();
        }
        sRecord = null;
    }


    public void searchClicked() {
    }

    public void compareClicked() {
    }

    public String[] addStringIfNotInArray(String symb, String[] sa) {
        if (getIndexOf(symb, sa) < 0) {
            if (sa != null) {

                int newCount = sa.length + 1;
                String[] tmpStrs = new String[newCount];
                tmpStrs[0] = symb;
                for (int i = 1; i < newCount; i++) {
                    tmpStrs[i] = sa[i - 1];
                }
                return tmpStrs;
            } else {
                sa = new String[1];
                sa[0] = symb;
                return sa;
            }
        } else
            return sa;
    }

    public void refreshBaseSymbolSubMenu() {
        int ind = jPopupMenu1.getComponentIndex(mnuSubBaseSymbol);
        jPopupMenu1.remove(ind);// mnuSubBaseSymbol.
        mnuSubBaseSymbol.remove(miNewBase);
        mnuSubBaseSymbol = null;
        mnuSubBaseSymbol = getBaseSymbolSubMenu();
        mnuSubBaseSymbol.setIconFile("graph_base_Symbol.gif");
        mnuSubBaseSymbol.add(miNewBase);
        mnuSubBaseSymbol.updateUI();
        jPopupMenu1.insert(mnuSubBaseSymbol, ind);
    }

    public TWMenu getBaseSymbolSubMenu() {
        String sname;
        try {
            sname = Language.getString("BASE_SYMBOL");

            return getRadioSubMenu(sname, extractSymbolArray(g_oBaseSymbols.getSymbols()), "baseSymbolClicked", true);
        } catch (Exception e) {
            return new TWMenu(Language.getString("BASE_SYMBOL"), "graph_base_Symbol.gif");
        }
    }

    public void refreshCompareSymbolsSubMenu() {
        int ind = jPopupMenu1.getComponentIndex(mnuSubCompare);
        jPopupMenu1.remove(ind);// mnuSubCompare
        mnuSubCompare.remove(miNewCompare);
        mnuSubCompare = null;
        mnuSubCompare = getCompareSymbolSubMenu();
        mnuSubCompare.setIconFile("graph_compare_Symbol.gif");
        mnuSubCompare.add(miNewCompare);
        mnuSubCompare.updateUI();
        jPopupMenu1.insert(mnuSubCompare, ind);
    }

    public TWMenu getCompareSymbolSubMenu() {
        try {
            String sname;
            sname = Language.getString("COMPARE");
            return getRadioSubMenu(sname, extractSymbolArray(g_oComparedSymbols.getSymbols()), "compareSymbolClicked", false);
        } catch (Exception e) {
            return new TWMenu(Language.getString("COMPARE"));
        }
    }

    // Added - Pramoda (14-03-2006)
    public void refreshTemplateSubMenu() {
        int ind = jPopupMenu1.getComponentIndex(mnuSubTemplate);
        jPopupMenu1.remove(ind);
        mnuSubTemplate = getTemplateSubMenu();
        mnuSubTemplate.updateUI();
        jPopupMenu1.insert(mnuSubTemplate, ind);
    }

    private TWMenu getTemplateSubMenu() {

        TWMenu oMenu=new TWMenu(Language.getString("GRAPH_APPLY_TEMPLATE"));
        try {
            File templateDir = new File(FILEPATH_CHART_TEMPLATES);
            FilenameFilter filter = new FilenameFilter() {
                public boolean accept(File dir, String name) {
                    return name.endsWith(".mct");
                }
            };
            File[] files = templateDir.listFiles(filter);
            int fixedLen = TemplateFactory.TemplatePaths.length;
            File[] faTemplates = new File[files.length + fixedLen];
            for (int i = 0; i < fixedLen; i++) {
                faTemplates[i] = new File(TemplateFactory.TemplatePaths[i]);
            }
            for (int i = 0; i < files.length; i++) {
                faTemplates[i + fixedLen] = files[i];
            }
            String sname = Language.getString("GRAPH_APPLY_TEMPLATE");
            return getTemplateSubMenu(sname, faTemplates);
        } catch (Exception e) {
            GUISettings.applyOrientation(oMenu);
            return oMenu;
        }
    }

    public void refreshIntervalSubMenu() {
        try {
            String sname;
            String[] sa = strHistoryIntervals;
            if (graph.isCurrentMode()) sa = strCurrentIntervals;

            sname = Language.getString("INTERVAL");
            int ind = jPopupMenu1.getComponentIndex(mnuSubInterval);
            jPopupMenu1.remove(ind);// mnuSubInterval
            mnuSubInterval = getRadioSubMenu(sname, sa, "intervalClicked", true);
            mnuSubInterval.setIconFile("graph_Interval.GIF");
            mnuSubInterval.updateUI();
            jPopupMenu1.insert(mnuSubInterval, ind);
        } catch (Exception e) {
        }
    }

    public void refreshPeriodSubMenu() {
        try {
            String sname;
            String[] sa = strHistoryPeriods;
            if (graph.isCurrentMode()) sa = strCurrentPeriods;

            sname = Language.getString("PERIOD");
            int ind = jPopupMenu1.getComponentIndex(mnuSubPeriod);
            jPopupMenu1.remove(ind);// mnuSubPeriod
            mnuSubPeriod = getRadioSubMenu(sname, sa, "periodClicked", true);
            mnuSubPeriod.setIconFile("graph_Period.GIF");
            mnuSubPeriod.updateUI();
            jPopupMenu1.insert(mnuSubPeriod, ind);
        } catch (Exception e) {
        }
    }

    public void refreshStyleSubMenu() {
        try {
            String sname;
            String[] sa = strGraphStyles;

            sname = Language.getString("STYLE");
            int ind = jPopupMenu1.getComponentIndex(mnuSubStyle);
            jPopupMenu1.remove(ind);// mnuSubPeriod
            mnuSubStyle = getRadioSubMenu(sname, sa, "styleClicked", true);
            mnuSubStyle.setIconFile("graph_Style.GIF");
            mnuSubStyle.updateUI();
            jPopupMenu1.insert(mnuSubStyle, ind);
        } catch (Exception e) {
        }
    }

    public void markComparingSymbols() {

        if (saComparingSymbols == null) return;
        int itmCnt = mnuSubCompare.getItemCount();
        for (int j = 0; j < itmCnt; j++) {
            TWMenuItem mi = (TWMenuItem) mnuSubCompare.getItem(j);
            String miText = mi.getText();
            for (int i = 0; i < saComparingSymbols.length; i++) {
                String searchStr = saComparingSymbols[i];
                if (miText.equals(searchStr)) {
                    mi.setSelected(true);
                    mi.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_checked.gif"));
                }
            }
        }
    }

    public String toString() {
        return graph.GDMgr.getMainGraphTitle();
    }

    /**
     * Sets the view settings object for this table
     */
    public void setViewSettings(ViewSetting oViewSettings) {
        g_oViewSettings = oViewSettings;
        if (tableModel == null) {
            tableModel = new DataTableModel(this);
            try {
                GUISettings.setColumnSettings(g_oViewSettings, ChartInterface.getColumnSettings("CHART_DATA_COLS"));
                tableModel.setViewSettings(g_oViewSettings);
            } catch (Exception e) {
                e.printStackTrace();
            }
            dataTable = new Table();
            dataTable.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
            dataTable.setSortingEnabled();
            dataTable.setModel(tableModel);
            tableModel.setTable(dataTable, new GraphDataRenderer(g_oViewSettings.getColumnHeadings(), tableModel.getRendIDs()));
            tablePanel.add(dataTable);

            TWMenuItem chartMode = new TWMenuItem(Language.getString("CHART_MODE"), "graph_ChartMode.GIF");
            chartMode.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    switchPanel();
                }
            });

            TWMenuItem exportData = new TWMenuItem(Language.getString("EXPORT_DATA"), "exporttrades.gif");
            exportData.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    exportData();
                }
            });

            dataTable.getPopup().setMenuItem(chartMode);
            dataTable.getPopup().setMenuItem(exportData);
            dataTable.getPopup().hideTitleBarMenu();
        }

        g_oViewSettings.setParent(this);
        applySettings();

        /*//important
        g_oViewSettings = oViewSettings;
        if (graphTableModel == null) {
            graphTableModel = new GraphDataTableModel(this);
            try {
                GUISettings.setColumnSettings(g_oViewSettings, ChartInterface.getColumnSettings("CHART_DATA_COLS"));
                graphTableModel.setViewSettings(g_oViewSettings);
            } catch (Exception e) {
                e.printStackTrace();
            }
            graphDataTable = new Table();
            graphDataTable.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
            graphDataTable.setSortingEnabled();
            graphDataTable.setModel(graphTableModel);
            graphTableModel.setTable(graphDataTable, new GraphDataRenderer(g_oViewSettings.getColumnHeadings(), graphTableModel.getRendIDs()));

            TWMenuItem exportData = new TWMenuItem(Language.getString("EXPORT_DATA"), "exporttrades.gif");
            exportData.addActionListener(new ActionListener() {
                public void actionPerformed(ActionEvent e) {
                    exportGraphData();
                }
            });

            graphDataTable.getPopup().setMenuItem(exportData);
            graphDataTable.getPopup().hideTitleBarMenu();
        }

        g_oViewSettings.setParent(this);
        applySettings();*/
    }

    public Table getGraphDataTable() {
        return graphDataTable;
    }

    public ViewSetting getViewSettings() {
        return g_oViewSettings;
    }

    //changed by charithn to support showing the object information...
    public void ToolTipDataChanged(boolean bTipActive, byte bytMode, int dataRows,
                                   Color aColor, String sSymbol, String sDate, String sOpen,
                                   String sHigh, String sLow, String sClose, String sVolume, String sTurnOver,
                                   ArrayList<ToolTipDataRow> toolTipRows, boolean isMove, String aoTitle) {

        if (isErasorMode) { // no need to show the tool tip in erasor mode
            return;
        }
        if (sOpen != null && sOpen.equals("")) {
            return;
        }
        if ((!isMove && toolTipRows == null)) {
            return;
        }

        //IMPORTANT - never change these final parameters
        int rowHeight = 13;
        final int HGap = DataRowPanel.EMPTY_LABEL_WIDTH;
        final int rectWidth = 2 * DataRowPanel.EMPTY_LABEL_WIDTH;
        final int correctionFactor = 0; //TODO : need to finalize the correction factor

        if (bTipActive) {
            lblInfoSymbol.setBackground(aColor);
            if (!isMove) {
                sSymbol = aoTitle;  //when mouse dragging  override the abstract object title
            }
            lblInfoDataSymbol.setText(sSymbol);
            lblInfoDataSymbol.setIcon(new ImageIcon(""));//icon needed only for alerts
            lblInfoDataDate.setText(sDate);
            lblInfoDataOpen.setText(sOpen);
            lblInfoDataHigh.setText(sHigh);
            lblInfoDataLow.setText(sLow);
            lblInfoDataClose.setText(sClose);
            lblInfoDataVolume.setText(sVolume);
            lblInfoDataTurnOver.setText(sTurnOver);

            int widthRight = 50, widthLeft = 50;
            int pnlHeight = (dataRows + 1) * rowHeight;
            int pnlWidth = 50;

            FontMetrics fontMetrics = lblInfoDataSymbol.getFontMetrics(lblInfoDataSymbol.getFont());
            //calculating maximum width of right side label
            widthRight = Math.max(fontMetrics.stringWidth(lblInfoDataDate.getText()), fontMetrics.stringWidth(lblInfoDataOpen.getText()));
            widthRight = Math.max(fontMetrics.stringWidth(lblInfoDataHigh.getText()), widthRight);
            widthRight = Math.max(fontMetrics.stringWidth(lblInfoDataLow.getText()), widthRight);
            widthRight = Math.max(fontMetrics.stringWidth(lblInfoDataClose.getText()), widthRight);
            widthRight = Math.max(fontMetrics.stringWidth(lblInfoDataVolume.getText()), widthRight);
            widthRight = Math.max(fontMetrics.stringWidth(lblInfoDataTurnOver.getText()), widthRight);

            //calculating maximum width of left side label
            widthLeft = Math.max(fontMetrics.stringWidth(lblInfoDate.getText()), fontMetrics.stringWidth(lblInfoOpen.getText()));
            widthLeft = Math.max(fontMetrics.stringWidth(lblInfoHigh.getText()), widthLeft);
            widthLeft = Math.max(fontMetrics.stringWidth(lblInfoLow.getText()), widthLeft);
            widthLeft = Math.max(fontMetrics.stringWidth(lblInfoClose.getText()), widthLeft);
            widthLeft = Math.max(fontMetrics.stringWidth(lblInfoVolume.getText()), widthLeft);
            widthLeft = Math.max(fontMetrics.stringWidth(lblInfoTurnOver.getText()), widthLeft);

            //calculating maximum height of both left sides labels-no need to worry about the right side value lables
            rowHeight = Math.max(lblInfoDataSymbol.getFontMetrics(lblInfoDataSymbol.getFont()).getHeight(), lblInfoDataDate.getFontMetrics(lblInfoDataDate.getFont()).getHeight());
            rowHeight = Math.max(lblInfoDataOpen.getFontMetrics(lblInfoDataOpen.getFont()).getHeight(), rowHeight);
            rowHeight = Math.max(lblInfoDataHigh.getFontMetrics(lblInfoDataHigh.getFont()).getHeight(), rowHeight);
            rowHeight = Math.max(lblInfoDataLow.getFontMetrics(lblInfoDataLow.getFont()).getHeight(), rowHeight);
            rowHeight = Math.max(lblInfoDataClose.getFontMetrics(lblInfoDataClose.getFont()).getHeight(), rowHeight);
            rowHeight = Math.max(lblInfoDataVolume.getFontMetrics(lblInfoDataVolume.getFont()).getHeight(), rowHeight);
            rowHeight = Math.max(lblInfoDataTurnOver.getFontMetrics(lblInfoDataTurnOver.getFont()).getHeight(), rowHeight);

            //int widthTitle = fontMetrics.stringWidth(lblInfoDataSymbol.getText()) + HGap;
            int widthTitle = fontMetrics.stringWidth(SharedMethods.getTextFromHTML(lblInfoDataSymbol.getText())) + HGap;
            widthTitle = Math.max(widthTitle + rectWidth, widthLeft + widthRight + 2 * HGap);

            if (toolTipRows != null && toolTipRows.size() > 0) {  //tooltip on an object - does not show OHLS values
                widthLeft = 0;
                widthRight = 0;
                //widthTitle = fontMetrics.stringWidth(lblInfoDataSymbol.getText()) + HGap;
                widthTitle = fontMetrics.stringWidth(SharedMethods.getTextFromHTML(lblInfoDataSymbol.getText())) + HGap;
                for (ToolTipDataRow toolTipRow : toolTipRows) {
                    widthRight = Math.max(fontMetrics.stringWidth(toolTipRow.getDataRowValue()), widthRight);
                    widthLeft = Math.max(fontMetrics.stringWidth(toolTipRow.getDataRowName()), widthLeft);
                }
                widthTitle = Math.max(widthTitle + rectWidth, widthLeft + widthRight + 2 * HGap);
            }

            switch (bytMode) {
                case 0: // Top Graph Current Mode
                    lblInfoDate.setText(g_sLblTime);
                    break;
                case 1: // Top Graph History Mode
                    lblInfoDate.setText(g_sLblDate);
                    break;
                default:
                    lblInfoDate.setText(g_sLblTime);
                    break;
            }
            if (toolTipRows != null && toolTipRows.size() > 1) {   //on an object
                pnlInfoPanel.removeAll();

                rowSymbol = new DataRowPanel(new CustomRectangularDrawingLabel(rectWidth, rowHeight, aColor), lblInfoDataSymbol, rectWidth, widthTitle - rectWidth, rowHeight, true);//adding the title of the object
                pnlInfoPanel.add(rowSymbol);

                //alram tooltip-shashika ----------------------------------------------------------------
                boolean aralmEnabled = false;
                for (ToolTipDataRow toolTipRow : toolTipRows) {

                    String name = toolTipRow.getDataRowName();
                    String value = toolTipRow.getDataRowValue();

                    JLabel lblName = new JLabel(name);
                    JLabel lblValue = new JLabel(value);

                    if (name.equals(ObjectLineSlope.ALARM_TMESSAGE)) {
                        aralmEnabled = Boolean.parseBoolean(value);
                    } else {
                        pnlInfoPanel.add(new DataRowPanel(lblName, lblValue, widthLeft + HGap, widthTitle - widthLeft - HGap, rowHeight, false));
                    }
                }


                String alarmTitle = "<html>" + lblInfoDataSymbol.getText() + " <font color=\"#ff0000\">" + Language.getString("TREND_LINE_ALERT_SET") + "</font></html>";
                if (aralmEnabled) {
                    lblInfoDataSymbol.setText(alarmTitle);
                    //lblInfoDataSymbol.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/alert.gif"));
                    lblInfoDataSymbol.setHorizontalAlignment(JLabel.LEADING);
                    lblInfoDataSymbol.setHorizontalTextPosition(JLabel.LEADING);
                }

                //-------------------------------------------------------------------------------------------

                pnlHeight = (toolTipRows.size() + 1) * (rowHeight + DataRowPanel.VGAP) + HGap;
                pnlWidth = widthTitle + 2 * HGap + correctionFactor + 4 * DataRowPanel.HGAP;
                pnlInfoPanel.setSize(new Dimension(pnlWidth, pnlHeight));

            } else if (dataRows == 0) {   // static object- current price lines- min-max lines, bid ask tags
                pnlInfoPanel.removeAll();
                rowSymbol = new DataRowPanel(new CustomRectangularDrawingLabel(rectWidth, rowHeight, aColor), lblInfoDataSymbol, rectWidth, widthTitle - rectWidth, rowHeight, true);
                pnlInfoPanel.add(rowSymbol);

                pnlHeight = (rowHeight + DataRowPanel.VGAP) + HGap;
                pnlWidth = widthTitle + 2 * HGap + correctionFactor + 4 * DataRowPanel.HGAP;
                pnlInfoPanel.setSize(new Dimension(pnlWidth, pnlHeight));


            } else if (dataRows == 1) {//Announcements
                lblInfoOpen.setText(g_sLblValue); // sets the open value label text as "Value"
                pnlInfoPanel.removeAll();

                widthRight = 0;
                widthLeft = 0;

                widthLeft = Math.max(fontMetrics.stringWidth(lblInfoDate.getText()), widthLeft);  //String "Date :"
                widthRight = Math.max(fontMetrics.stringWidth(lblInfoDataDate.getText()), widthRight); //exact date

                JLabel pnlTitle = new JLabel(Language.getString("ANNOUNCEMENT"));
                widthTitle = fontMetrics.stringWidth(pnlTitle.getText()) + HGap;
                widthTitle = Math.max(widthTitle + rectWidth, widthLeft + widthRight + 2 * HGap);

                rowSymbol = new DataRowPanel(new CustomRectangularDrawingLabel(rectWidth, rowHeight, aColor), pnlTitle, rectWidth, widthTitle - rectWidth, rowHeight, true);
                rowDate = new DataRowPanel(lblInfoDate, lblInfoDataDate, widthLeft + HGap, widthTitle - widthLeft - HGap, rowHeight, false);

                pnlInfoPanel.add(rowSymbol);
                pnlInfoPanel.add(rowDate);

                pnlHeight = (2) * (rowHeight + DataRowPanel.VGAP) + HGap;
                pnlWidth = widthTitle + 2 * HGap + correctionFactor + 4 * DataRowPanel.HGAP;
                pnlInfoPanel.setSize(new Dimension(pnlWidth, pnlHeight));
            } else if (dataRows < 5 && dataRows >= 1) {  // for indicators, stratergies - only 2 data rows

                //alram tooltip for Zigzag line - shashika --------------------------------------------------------------
                boolean aralmEnabled = false;
                if (toolTipRows != null) {
                    for (ToolTipDataRow toolTipRow : toolTipRows) {
                        String name = toolTipRow.getDataRowName();
                        String value = toolTipRow.getDataRowValue();

                        JLabel lblName = new JLabel(name);
                        JLabel lblValue = new JLabel(value);

                        if (name.equals(ObjectLineSlope.ALARM_TMESSAGE)) {
                            aralmEnabled = Boolean.parseBoolean(value);
                        } else {
                            pnlInfoPanel.add(new DataRowPanel(lblName, lblValue, widthLeft + HGap, widthTitle - widthLeft - HGap, rowHeight, false));
                        }
                    }


                    String alarmTitle = "<html>" + lblInfoDataSymbol.getText() + " <font color=\"#ff0000\">" + Language.getString("TREND_LINE_ALERT_SET") + "</font></html>";
                    if (aralmEnabled) {
                        lblInfoDataSymbol.setText(alarmTitle);
                        //lblInfoDataSymbol.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/alert.gif"));
                        lblInfoDataSymbol.setHorizontalAlignment(JLabel.LEADING);
                        lblInfoDataSymbol.setHorizontalTextPosition(JLabel.LEADING);
                    }
                }
                //----------------------------------------------------------------------------------------------------------

                lblInfoOpen.setText(g_sLblValue); // sets the open value label text as "Value"
                pnlInfoPanel.removeAll();

                widthRight = 0;
                widthLeft = 0;

                widthLeft = Math.max(fontMetrics.stringWidth(lblInfoDate.getText()), widthLeft);  //String "Date :"
                widthLeft = Math.max(fontMetrics.stringWidth(lblInfoOpen.getText()), widthLeft); // Sting "Value : "

                widthRight = Math.max(fontMetrics.stringWidth(lblInfoDataDate.getText()), widthRight); //exact date
                widthRight = Math.max(fontMetrics.stringWidth(lblInfoDataOpen.getText()), widthRight); //exact value

                //widthTitle = fontMetrics.stringWidth(lblInfoDataSymbol.getText()) + HGap;
                widthTitle = fontMetrics.stringWidth(SharedMethods.getTextFromHTML(lblInfoDataSymbol.getText())) + HGap;
                widthTitle = Math.max(widthTitle + rectWidth, widthLeft + widthRight + 2 * HGap);

                rowSymbol = new DataRowPanel(new CustomRectangularDrawingLabel(rectWidth, rowHeight, aColor), lblInfoDataSymbol, rectWidth, widthTitle - rectWidth, rowHeight, true);
                rowDate = new DataRowPanel(lblInfoDate, lblInfoDataDate, widthLeft + HGap, widthTitle - widthLeft - HGap, rowHeight, false);
                rowOpen = new DataRowPanel(lblInfoOpen, lblInfoDataOpen, widthLeft + HGap, widthTitle - widthLeft - HGap, rowHeight, false);

                pnlInfoPanel.add(rowSymbol);
                pnlInfoPanel.add(rowDate);
                pnlInfoPanel.add(rowOpen);

                pnlHeight = (3) * (rowHeight + DataRowPanel.VGAP) + HGap;
                pnlWidth = widthTitle + 2 * HGap + correctionFactor + 4 * DataRowPanel.HGAP;
                pnlInfoPanel.setSize(new Dimension(pnlWidth, pnlHeight));

            } else {  // normal OHLCV values  + patterns  showing in the tool tip window
                pnlInfoPanel.removeAll();

                lblInfoOpen.setText(Language.getString("OPEN")); // sets the open value label text as "Open"
                rowSymbol = new DataRowPanel(new CustomRectangularDrawingLabel(rectWidth, rowHeight + DataRowPanel.VGAP, aColor), lblInfoDataSymbol, rectWidth, widthTitle - rectWidth, rowHeight, true);
                rowDate = new DataRowPanel(lblInfoDate, lblInfoDataDate, widthLeft + HGap, widthTitle - widthLeft - HGap, rowHeight, false);
                rowOpen = new DataRowPanel(lblInfoOpen, lblInfoDataOpen, widthLeft + HGap, widthTitle - widthLeft - HGap, rowHeight, false);
                rowHigh = new DataRowPanel(lblInfoHigh, lblInfoDataHigh, widthLeft + HGap, widthTitle - widthLeft - HGap, rowHeight, false);
                rowLow = new DataRowPanel(lblInfoLow, lblInfoDataLow, widthLeft + HGap, widthTitle - widthLeft - HGap, rowHeight, false);
                rowClose = new DataRowPanel(lblInfoClose, lblInfoDataClose, widthLeft + HGap, widthTitle - widthLeft - HGap, rowHeight, false);
                rowVolume = new DataRowPanel(lblInfoVolume, lblInfoDataVolume, widthLeft + HGap, widthTitle - widthLeft - HGap, rowHeight, false);
                rowTurnOver = new DataRowPanel(lblInfoTurnOver, lblInfoDataTurnOver, widthLeft + HGap, widthTitle - widthLeft - HGap, rowHeight, false);

                pnlInfoPanel.add(rowSymbol);
                pnlInfoPanel.add(rowDate);
                pnlInfoPanel.add(rowOpen);
                pnlInfoPanel.add(rowHigh);
                pnlInfoPanel.add(rowLow);
                pnlInfoPanel.add(rowClose);

                double volume = 0;
                try {
                    volume = Double.parseDouble(sVolume.replaceAll(",", "")); //removes the formatting.
                } catch (Exception ex) {
                    //ex.printStackTrace();
                }
                //if he volume is  > 0, then turn over must be > 0
                if (volume > 0) {  // in pattterns does not show the voume
                    pnlInfoPanel.add(rowVolume);
                    pnlInfoPanel.add(rowTurnOver);
                    pnlHeight = (8) * (rowHeight + DataRowPanel.VGAP) + HGap;
                } else {
                    pnlHeight = (6) * (rowHeight + DataRowPanel.VGAP) + HGap;
                }

                pnlWidth = widthTitle + 2 * HGap + correctionFactor + 4 * DataRowPanel.HGAP;
                pnlInfoPanel.setSize(new Dimension(pnlWidth, pnlHeight));
            }

            GUISettings.applyOrientation(pnlInfoPanel);
            pnlInfoPanel.setBorder(BorderFactory.createLineBorder(ChartTheme.getCustomThemeDataToApply().TooltipBorderColor));
            pnlInfoPanel.setLocation(getToolTipPos((int) CurrentMousePos.x, (int) CurrentMousePos.y, pnlWidth, pnlHeight));
            // System.out.println(CurrentMousePos.y);
            pnlInfoPanel.setBackground(ChartTheme.getCustomThemeDataToApply().TooltipBackground);
            pnlInfoPanel.setVisible(!showSplitTooltip);
            hintPane.setVisible(true);

        } else { //hide the hintpane here.
            if (!showSplitTooltip) {

            } else {
                pnlInfoPanel.setSize(0, 0);
            }
            lblInfoDataSymbol.setText("");
            lblInfoDataDate.setText("");
            lblInfoDataOpen.setText("");
            lblInfoDataHigh.setText("");
            lblInfoDataLow.setText("");
            lblInfoDataClose.setText("");
            lblInfoDataVolume.setText("");
            lblInfoDataTurnOver.setText("");
        }
    }

    public void applySettings() {

        this.setSize(g_oViewSettings.getSize());
        if (g_oViewSettings.isVisible()) {
            if (!graph.isCashFlowMode()) {
                this.getDesktopPane().setLayer(this, GUISettings.TOP_LAYER, g_oViewSettings.getIndex());
            }

            this.setVisible(true);
            try {
                this.setSelected(true);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            if (!graph.isCashFlowMode()) {
                this.getDesktopPane().setLayer(this, GUISettings.TOP_LAYER, 0);
            }
        }

        try {
            switch (g_oViewSettings.getStyle()) {
                case ViewSettingsManager.STYLE_ICONIFIED:
                    this.setIcon(true);
                    break;
                case ViewSettingsManager.STYLE_MAXIMIZED:
                    this.setMaximum(true);
                    break;
                default:
                    this.setMaximum(false);
                    this.setIcon(false);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }

        this.setLocation(g_oViewSettings.getLocation());
    }

    /* Table wrapper methods */
    public boolean isWindowVisible() {
        return isVisible();
    }

    public int getWindowStyle() {
        if (isMaximum())
            return ViewSettingsManager.STYLE_MAXIMIZED;
        else if (isIcon())
            return ViewSettingsManager.STYLE_ICONIFIED;
        else
            return ViewSettingsManager.STYLE_NORMAL;
    }

    public JTable getTable1() {
        return dataTable.getTable();
    }

    public JTable getTable2() {
        return null;
    }

    public int getZOrder() {
        return getDesktopPane().getPosition(this);
    }

    public void printChart() {
        new PrintPreview(this, "Preview");
    }

    public boolean isTitleVisible() {

        if (ChartFrame.getSharedInstance().isBIsTabbedMode() && (!isGraphDetached())) {//this point not hit
            return false;
        } else {
            return true;
        }

    }

    public void setTitleVisible(boolean status) {
        super.setTitleVisible(status);
    }

    public void windowClosing() {
        removeWindowtab(this);

    }

    public Object getDetachedFrame() {
        return null;
    }

    public static void removeWindowtab(InternalFrame iframe) {
        try {
            ChartFrame.getWindowTabs().removeTab(iframe);

            if (ChartFrame.getWindowTabs().getTabCount() == 0) {

            }
        } catch (Exception e) {
            // e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            System.out.println("Tab removing failed....");
        }
    }

    public int print(Graphics pg, PageFormat pageFormat, int pageIndex) throws PrinterException {
        if (pageIndex >= m_maxNumPage) {
            return NO_SUCH_PAGE;
        }
        pg.translate((int) pageFormat.getImageableX(), (int) pageFormat.getImageableY());

        int wPage = (int) pageFormat.getImageableWidth();
        int hPage = (int) pageFormat.getImageableHeight();
        int w = graph.getWidth();  // chartWidth + 60; //this.getWidth(); // m_bi.getWidth(this);
        int h = graph.getHeight(); // panelHeight-chartLeftY; //this.getHeight(); // m_bi.getHeight(this);
        if (w == 0 || h == 0)
            return NO_SUCH_PAGE;
        float frac = Math.min((float) wPage / w, (float) (hPage - StockGraph.sliderHeight) / h);
        int wImage = (int) Math.round(w * frac); //400; //
        int hImage = (int) Math.round(h * frac); //300; //

        graph.paintOnTheGraphicsObject(pg, wImage, hImage, true);
        return PAGE_EXISTS;
    }
    // end added by udaka

    public void saveAsImage(String fileName) {
        try {
            int imageWidth = 1024;
            int imageHeight = 768;
            String waterMarkFilePath = "images/common/print_footer_" + Language.getLanguageTag() + ".gif";

            File waterMarkFile = new File(waterMarkFilePath);
            if (waterMarkFile.exists()) {
                BufferedImage image = new BufferedImage(imageWidth, imageHeight, BufferedImage.TYPE_INT_RGB);
                Graphics g = image.getGraphics();
                g.setColor(Theme.getColor("BACKGROUND_COLOR"));
                g.fillRect(0, 0, imageWidth, imageHeight);
                graph.paintOnTheGraphicsObject(g, imageWidth, imageHeight, true);

                BufferedImage watermark = ImageIO.read(waterMarkFile);

                ((Graphics2D) g).setComposite(AlphaComposite.getInstance(AlphaComposite.SRC_OVER, 0.3f));
                /*for (int w = watermark.getWidth(); w < image.getWidth(); w += (watermark.getWidth() + 50)) {
                    for (int h = watermark.getHeight() * 2; h < image.getHeight() - 50; h += watermark.getHeight() * 3) {
                        g.drawImage(watermark, w, h, null);
                    }
                }*/

                int xPos = imageWidth - watermark.getWidth() - 50;
                int yPos = imageHeight - watermark.getHeight() - 60;
                g.drawImage(watermark, xPos, yPos, null);


                OutputStream os = new FileOutputStream(fileName);
                JPEGImageEncoder encoder = JPEGCodec.createJPEGEncoder(os);
                JPEGEncodeParam param = JPEGCodec.getDefaultJPEGEncodeParam(image);
                param.setQuality(1.0F, true);
                encoder.setJPEGEncodeParam(param);
                encoder.encode(image);
                os.close();
                image = null;
                watermark = null;
                g = null;
                os = null;
            }
            waterMarkFile = null;
            waterMarkFilePath = null;
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    // Drag and Drop Listeners

    public void dragEnter(DropTargetDragEvent dtde) {
    }

    public void dragOver(DropTargetDragEvent dtde) {
    }

    public void dropActionChanged(DropTargetDragEvent dtde) {
    }

    public void dragExit(DropTargetEvent dte) {
    }

    public void drop(DropTargetDropEvent event) {
        /* Check if the drag came form prit button */
        try {
            String key = (String) event.getTransferable().getTransferData(DataFlavor.stringFlavor);
            if (key.equals("Print")) {
                event.getDropTargetContext().dropComplete(true);
            } else {
                event.rejectDrop(); // not from the print button. reject drop
                return;
            }
        } catch (Exception e) {
            e.printStackTrace();
            event.rejectDrop();
            return;
        }
        //PrintUtilities.printComponent(this);
        //Dimension d = this.getSize(); // just to fix a _bug in java
        //this.setSize(10,10); // just to fix a _bug in java
        printChart();
    }

    private Point2D.Double getAdjustedMouseUpPoint(Point2D.Double mdPt, Point2D.Double muPt) {
        Point2D.Double newMuPt = new Point2D.Double();
        if (muPt.x < 0) {
            newMuPt.x = 0;
            newMuPt.y = (muPt.x * mdPt.y - muPt.y * mdPt.x) / (muPt.x - mdPt.x);
        } else if (muPt.x > graph.GDMgr.getGraphStoreSize() - 1) {
            newMuPt.x = graph.GDMgr.getGraphStoreSize() - 1;
            newMuPt.y = muPt.y + (mdPt.y - muPt.y) * (muPt.x - newMuPt.x) / (muPt.x - mdPt.x);
        }
        return newMuPt;
    }

    private void addObjectOnGraph(int pnlID) {
        StockGraph grf = this.graph;
        AbstractObject anObj = null;
        long[] laX;
        Point2D.Double mdPt = grf.getActualPoint2D(MouseDownPoint, pnlID); //returns the index(float) and Actual Yvalue
        Point2D.Double muPt = grf.getActualPoint2D(MouseUpPoint, pnlID);
        if ((muPt.x < 0) || (muPt.x > grf.GDMgr.getGraphStoreSize() - 1)) {
            muPt = getAdjustedMouseUpPoint(mdPt, muPt);
        }
        Point2D.Double firstUpPoint = grf.getActualPoint2D(firstMouseUpPoint, pnlID);
        Point2D.Double secondUpPoint = grf.getActualPoint2D(secondMouseUpPoint, pnlID);
        Point2D.Double thirdUpPoint = grf.getActualPoint2D(thirdMouseUpPoint, pnlID);

        String text = "";

        int ObjectTobeDrawn;
        if (parent == null) {
            ObjectTobeDrawn = AbstractObject.INT_NONE;
        } else {
            ObjectTobeDrawn = parent.bottomToolBar.ObjectTobeDrawn;
        }

        switch (ObjectTobeDrawn) {
            case AbstractObject.INT_TEXT:
                text = showTextInputDialog(Language.getString("TEXT_NOTE"));

                //check minimum size -shashika
                if (Math.abs(MouseDownPoint.y - MouseUpPoint.y) < ObjectTextNote.MIN_SIZE) {
                    if (MouseUpPoint.y < MouseDownPoint.y) {
                        MouseUpPoint.y = MouseDownPoint.y - ObjectTextNote.MIN_SIZE;
                    } else {
                        MouseUpPoint.y = MouseDownPoint.y + ObjectTextNote.MIN_SIZE;
                    }
                }

                if (Math.abs(MouseDownPoint.x - MouseUpPoint.x) < ObjectTextNote.MIN_SIZE) {
                    if (MouseUpPoint.x < MouseDownPoint.x) {
                        MouseUpPoint.x = MouseDownPoint.x - ObjectTextNote.MIN_SIZE;
                    } else {
                        MouseUpPoint.x = MouseDownPoint.x + ObjectTextNote.MIN_SIZE;
                    }
                }

                mdPt = grf.getActualPoint2D(MouseDownPoint, pnlID); //returns the index(float) and Actual Yvalue
                muPt = grf.getActualPoint2D(MouseUpPoint, pnlID);
                float[] faXT = {(float) mdPt.x, (float) muPt.x};
                double[] faYT = {mdPt.y, muPt.y};
                laX = graph.GDMgr.convertXArrayIndexToTime(faXT);
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, laX, faYT, faXT, null, (Rectangle) graph.panels.get(pnlID), null, graph);
                ((ObjectTextNote) anObj).setText(text);
                break;
            case AbstractObject.INT_LINE_SLOPE:
                /***************************snap To price implementation********************************************************/
                float[] faXLineSlope = {(float) mdPt.x, (float) muPt.x}; //indexes
                double[] faYLineSlope = {mdPt.y, muPt.y};
                laX = graph.GDMgr.convertXArrayIndexToTime(faXLineSlope);

                //check whether object is adding for Base chart panel
                if ((graph.GDMgr.isSnapToPrice()) && (pnlID == graph.getIDforPanel(graph.GDMgr.getBaseCP().getRect()))) {
                    //ohlc priority
                    double[] faYRelOHLc;
                    if (graph.GDMgr.getSnapToPricePriority() == 0) { //automatic
                        faYRelOHLc = graph.GDMgr.convertXArrayIndexToNearestOHLC(faXLineSlope, faYLineSlope);
                    } else {
                        faYRelOHLc = graph.GDMgr.convertXArrayIndexToSnapSelectedOHLC(faXLineSlope);
                    }
                    faYLineSlope = faYRelOHLc;
                }
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, laX, faYLineSlope, faXLineSlope, null, (Rectangle) graph.panels.get(pnlID), null, graph);
                break;
                /***************************snap To price implementation********************************************************/
            case AbstractObject.INT_ARROW_LINE_SLOPE:
                /***************************snap To price implementation********************************************************/
                float[] faXArrowLineSlope = {(float) mdPt.x, (float) muPt.x}; //indexes
                double[] faYArrowLineSlope = {mdPt.y, muPt.y};
                laX = graph.GDMgr.convertXArrayIndexToTime(faXArrowLineSlope);

                //check whether object is adding for Base chart panel
                if ((graph.GDMgr.isSnapToPrice()) && (pnlID == graph.getIDforPanel(graph.GDMgr.getBaseCP().getRect()))) {
                    //ohlc priority
                    double[] faYRelOHLc;
                    if (graph.GDMgr.getSnapToPricePriority() == 0) { //automatic
                        faYRelOHLc = graph.GDMgr.convertXArrayIndexToNearestOHLC(faXArrowLineSlope, faYArrowLineSlope);
                    } else {
                        faYRelOHLc = graph.GDMgr.convertXArrayIndexToSnapSelectedOHLC(faXArrowLineSlope);
                    }
                    faYLineSlope = faYRelOHLc;
                }
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, laX, faYArrowLineSlope, faXArrowLineSlope, null, (Rectangle) graph.panels.get(pnlID), null, graph);
                break;
                /***************************snap To price implementation //Todo :ALS need it for arrowline slpe ? ********************************************************/
            case AbstractObject.INT_RECT:
            case AbstractObject.INT_RECT_SELECTION:
            case AbstractObject.INT_ELLIPSE:
            case AbstractObject.INT_FIBONACCI_ARCS:
            case AbstractObject.INT_FIBONACCI_FANS:
            case AbstractObject.INT_FIBONACCI_RETRACEMENTS:
                float[] faX = {(float) mdPt.x, (float) muPt.x};
                double[] faY = {mdPt.y, muPt.y};
                laX = graph.GDMgr.convertXArrayIndexToTime(faX);
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, laX, faY, faX, null, (Rectangle) graph.panels.get(pnlID), null, graph);
                break;
                /* ========================== testing the fibonacii extensiions here ======================== */
            case AbstractObject.INT_FIBONACCI_EXTENSIONS:
                float[] faXX = {(float) firstUpPoint.x, (float) secondUpPoint.x, (float) thirdUpPoint.x};
                double[] faYY = {firstUpPoint.y, secondUpPoint.y, thirdUpPoint.y};
                laX = graph.GDMgr.convertXArrayIndexToTime(faXX);
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, laX, faYY, faXX, null, (Rectangle) graph.panels.get(pnlID), null, graph);
                break;
            case AbstractObject.INT_ZIG_ZAG:
                float[] xx = new float[xPoints.size()];
                float[] yy = new float[yPoints.size()];

                float[] xIndex = new float[xx.length];
                double[] yyy = new double[xx.length];
                for (int i = 0; i < xx.length; i++) {
                    xx[i] = Float.parseFloat(String.valueOf(xPoints.get(i)));
                    yy[i] = Float.parseFloat(String.valueOf(yPoints.get(i)));

                    Point2D.Double p = grf.getActualPoint2D(new Point2D.Float(xx[i], yy[i]), pnlID);
                    xIndex[i] = (float) p.x;
                    yyy[i] = p.y;
                }

                laX = graph.GDMgr.convertXArrayIndexToTime(xIndex);
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, laX, yyy, xIndex, null, (Rectangle) graph.panels.get(pnlID), null, graph);

            case AbstractObject.INT_POLYGON:
                break;
            case AbstractObject.INT_SYMBOL:
                float[] faXx = {(float) muPt.x};
                double[] faYy = {muPt.y};
                laX = graph.GDMgr.convertXArrayIndexToTime(faXx);
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, laX, faYy, null, null, (Rectangle) graph.panels.get(pnlID), null, graph);
                text = new String(BottomToolBar.caTextSymbols, parent.bottomToolBar.symbolTobeDrawn, 1);
                ((ObjectSymbol) anObj).setFont(SharedMethods.getSymbolFont("fonts/LucidaSansRegular.ttf", 18));
                ((ObjectSymbol) anObj).setColor(Theme.getColor("GRAPH_SYMBOL_COLOR"));
                ((ObjectSymbol) anObj).setText(text);
                break;
            case AbstractObject.INT_FIBONACCI_ZONES:
                float[] faXz = {(float) muPt.x};
                double[] faYz = {muPt.y};
                laX = graph.GDMgr.convertXArrayIndexToTime(faXz);
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, laX, faYz, null, null, (Rectangle) graph.panels.get(pnlID), graph.panels, graph);
                break;
            case AbstractObject.INT_GANN_LINE:
            case AbstractObject.INT_GANN_FAN:
            case AbstractObject.INT_GANN_GRID:
                int MaxX = graph.GDMgr.getMaxAllowableRightPixelForDragObject(ObjectTobeDrawn);
                int MinX = graph.GDMgr.getMinAllowableLeftPixelForDragObject();
                int mdownX = (int) Math.min(MaxX, MouseUpPoint.x);
                mdownX = Math.max(MinX, mdownX);
                int gannGap = graph.GDMgr.getPixelGapFortheIndexGap(Math.max(Math.round(graph.GDMgr.getIndexGapForthePixelGap(100)), 1f));

                long[] xa = graph.GDMgr.convertXArrayIndexToTime(
                        new float[]{graph.GDMgr.getIndexForthePixel(mdownX),
                                graph.GDMgr.getIndexForthePixel(mdownX + gannGap)});
                if (gannGap == 0) gannGap = 50;
                //if (gannGap <=50) gannGap = 50;
                double[] ya = new double[]{graph.GDMgr.getYValueForthePixel(Math.round(MouseUpPoint.y), pnlID), graph.GDMgr.getYValueForthePixel(Math.round(MouseUpPoint.y) - gannGap, pnlID)};
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, xa, ya, null, null, (Rectangle) graph.panels.get(pnlID), null, graph);
                break;
            case AbstractObject.INT_REGRESSION:
            case AbstractObject.INT_STD_ERROR_CHANNEL:
            case AbstractObject.INT_RAFF_REGRESSION:
            case AbstractObject.INT_STD_DEV_CHANNEL:
            case AbstractObject.INT_EQUI_DIST_CHANNEL:
                float[] xArray = {(float) mdPt.x, (float) muPt.x};
                double[] yArray = {mdPt.y, muPt.y};
                laX = graph.GDMgr.convertXArrayIndexToTime(xArray);
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, laX, yArray, null, (Rectangle) graph.panels.get(pnlID), null, graph);
                break;
            case AbstractObject.INT_CYCLE_LINE:
                Point2D.Double tempPt = grf.getActualPoint2D(new Point2D.Float(MouseDownPoint.x + ObjectCycleLine.DEFAULT_GAP, MouseDownPoint.y), pnlID); //index ,Actual Yvalue
                float[] xArr = {(float) mdPt.x, (float) tempPt.x};
                double[] yArr = {mdPt.y};
                laX = graph.GDMgr.convertXArrayIndexToTime(xArr);
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, laX, yArr, xArr, null, (Rectangle) graph.panels.get(pnlID), null, graph);
                break;
            case AbstractObject.INT_ARC:
                float[] xxArr = {(float) firstUpPoint.x, (float) secondUpPoint.x, (float) thirdUpPoint.x};
                double[] yyArr = {firstUpPoint.y, secondUpPoint.y, thirdUpPoint.y};

                laX = graph.GDMgr.convertXArrayIndexToTime(xxArr);
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, laX, yyArr, xxArr, null, (Rectangle) graph.panels.get(pnlID), null, graph);
                break;
            case AbstractObject.INT_ANDREWS_PITCHFORK:
                xxArr = new float[]{(float) firstUpPoint.x, (float) secondUpPoint.x, (float) thirdUpPoint.x};
                yyArr = new double[]{firstUpPoint.y, secondUpPoint.y, thirdUpPoint.y};

                laX = graph.GDMgr.convertXArrayIndexToTime(xxArr);
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, laX, yyArr, xxArr, null, (Rectangle) graph.panels.get(pnlID), null, graph);
                break;
            default:
                float[] faX1 = {(float) muPt.x};
                double[] faY1 = {muPt.y};
                laX = graph.GDMgr.convertXArrayIndexToTime(faX1);
                anObj = AbstractObject.CreateObject(ObjectTobeDrawn, laX, faY1, faX1, null, (Rectangle) graph.panels.get(pnlID), graph.panels, graph);
                break;
        }
        anObj.setObjType(ObjectTobeDrawn);
        anObj.pnlID = pnlID;
        if (grf.GDMgr != null)
            grf.GDMgr.addToObjectArray(anObj);
    }

    public static String showTextInputDialog(String initialText) {
        JTextArea scroll = new JTextArea(initialText);
        Object[] msg = {Language.getString("ENTER_TEXT"),
                scroll};
        scroll.setPreferredSize(new Dimension(250, 100));
        scroll.setLineWrap(true);

        if (JOptionPane.showConfirmDialog(ChartInterface.getChartFrameParentComponent(),
                msg,
                Language.getString("TEXT_NOTE"),
                JOptionPane.OK_CANCEL_OPTION,
                JOptionPane.PLAIN_MESSAGE) == JOptionPane.OK_OPTION) {
            return scroll.getText();
        } else {
            return initialText;
        }
    }

    public void hideGlassPane() {
        hintPane.setVisible(false);
    }

    /////////////////////////////////////////////////////////////////////
    /// internal frame implementation //

    /**
     * Invoked when a internal frame has been opened.
     *
     * @see javax.swing.JInternalFrame#show
     */
    public void internalFrameOpened(InternalFrameEvent e) {
        //hideGlassPane();
    }

    /**
     * Invoked when an internal frame is in the process of being closed. The close operation can be overridden at this
     * point.
     *
     * @see javax.swing.JInternalFrame#setDefaultCloseOperation
     */
    public void internalFrameClosing(InternalFrameEvent e) {
        getDesktopPane().remove(this);
        ConnectionNotifier.getInstance().removeConnectionListener(this);

    }

    /**
     * Invoked when an internal frame has been closed.
     *
     * @see javax.swing.JInternalFrame#setClosed
     */
    public void internalFrameClosed(InternalFrameEvent e) {
        removeLinkedWindow(false);
        ChartProperties baseCP = graph.GDMgr.getBaseCP();
        if (baseCP != null && !baseCP.isSavedAndOpened()) {
            ChartInterface.removeOHLCRequest(graph.isCurrentMode(), baseCP.getSymbol());
        }
        for (int i = 0; i < graph.GDMgr.getSources().size(); i++) {
            ChartProperties compCP = (ChartProperties) graph.GDMgr.getSources().get(i);
            if (compCP.getID() == GraphDataManager.ID_COMPARE && !compCP.isSavedAndOpened()) {
                ChartInterface.removeOHLCRequest(graph.isCurrentMode(), compCP.getSymbol());
            }
        }

        Client.getInstance().getWindowDaemon().notifyThread();


    }

    /**
     * Invoked when an internal frame is iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameIconified(InternalFrameEvent e) {
        //hideGlassPane();


    }

    /**
     * Invoked when an internal frame is de-iconified.
     *
     * @see javax.swing.JInternalFrame#setIcon
     */
    public void internalFrameDeiconified(InternalFrameEvent e) {
        //hideGlassPane();


    }

    /**
     * Invoked when an internal frame is activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameActivated(InternalFrameEvent e) {

        if (parent != null) {
            parent.setActiveGraph(this);
            /*DataWindow window = parent.getDataWindow();
            if (window != null && window.isVisible() && !window.isWindowMode()) {
                window.getBtnDataModeImaginary().doClick();
            }*/
            graph.GDMgr.refreshDataWindow();
        }

        ChartFrame.getSharedInstance().refreshMainMenus(!tableMode);
    }

    /**
     * Invoked when an internal frame is de-activated.
     *
     * @see javax.swing.JInternalFrame#setSelected
     */
    public void internalFrameDeactivated(InternalFrameEvent e) {
        hideGlassPane();
        graph.removeFocus();
        if ((parent != null) && (parent.getActiveGraph() == this)) {
            //parent.setActiveGraph(null);
        }
    }

    //use action performed del for erasor

    public void actionPerformed(ActionEvent e) {
        if (e.getActionCommand().equals("DEL")) {

            if (parent != null && parent.bottomToolBar.ObjectTobeDrawn == AbstractObject.INT_ZIG_ZAG) {
                return; // do not want to delete the object until adding the whole object
            }
            if (detached) {
                graph.removeActiveObject(false);
                hintPane.setVisible(false); // no need to show the  tooltip after delete it
            } else {
                if ((parent != null) && (parent.getActiveGraph() != null)) {
                    parent.getActiveGraph().graph.removeActiveObject(isRectangularSelection);
                    hintPane.setVisible(false); // no need to show the  tooltip after delete it
                    /*try {
                        ChartProperties cp = (ChartProperties)graph.GDMgr.getSources().get(graph.GDMgr.getHighlightedSource());
                        if(cp != null && cp.getID() == GraphDataManager.ID_COMPARE){
                            parent.bottomToolBar.getSearchComponent().getTextFiled().setText("");
                        }
                    } catch (Exception e1) {
                        e1.printStackTrace();  
                    }*/

                    JInternalFrame[] frames = ChartFrame.getSharedInstance().getDesktop().getAllFrames();
                    if (frames != null) {
                        for (JInternalFrame frame : frames) {
                            if (frame instanceof GraphFrame) {
                                // ((GraphFrame) frame).isRectangularSelection = false;
                                // ((GraphFrame) frame).graph.setCursor(Cursor.getDefaultCursor());
                            }
                        }
                    }
                }
                refreshTopToolBar(parent.topToolBar);
            }
        } else if (e.getActionCommand().equals("ENTER")) {
            Component activeComp = KeyboardFocusManager.getCurrentKeyboardFocusManager().getFocusOwner();
            if (activeComp instanceof AbstractButton) {
                AbstractButton btn = (AbstractButton) activeComp;
                btn.doClick();
            }
        } /* else if (e.getActionCommand().equals("CTRL") && parent != null && (ChartOptions.getCurrentOptions().gen_add_studies == ChartOptions.AddStudiesOption.CONTINUE_WITH_SHIFT)) {
                  parent.bottomToolBar.btnNeutral.doClick();
        }*/
        else if (e.getActionCommand().equals("CTRL") && parent != null) {

            /* if (parent.bottomToolBar.ObjectTobeDrawn == AbstractObject.INT_ZIG_ZAG) {

              //int pnlID = graph.getPanelIDatPoint(CurrentMousePos.x, CurrentMousePos.y);
              //addObjectOnGraph(pnlID);
              parent.bottomToolBar.ObjectTobeDrawn = AbstractObject.INT_NONE;
              parent.bottomToolBar.btnNeutral.doClick();
              graph.setCursor(Cursor.getDefaultCursor());

              xPoints.clear();
              yPoints.clear();
          }
          parent.bottomToolBar.btnNeutral.doClick();
          BottomToolBar.CURSOR_STATUS = BottomToolBar.STATUS_NONE;*/
        }

        Object obj = e.getSource();
        if (obj == miNewBase)
            ChartFrame.insertBaseSymbol(this);
        else if (obj == miNewCompare) ChartFrame.insertCompareSymbol(this);
    }

    public void addDataWindowListener(DataWindowListener dwl) {
        this.dwListener = dwl;
    }

    public void addDragWindowListener(DragWindowListner dragWondowl) {
        this.dragWindowListener = dragWondowl;
    }

    public void addDataWindowListener(DataWindowTableListener dragWondowl) {
        this.dwTableListener = dragWondowl;
    }

    public void removeDataWindowListener() {
        this.dwListener = null;
    }

    public void updateUI() {
        super.updateUI();
        this.setBorder(GUISettings.getTWFrameBorder());
        if (graph != null) {
            if (graph.isCashFlowMode()) {
                this.setTitleVisible(false);
            } else {
                if (!detached && ChartFrame.getSharedInstance().getCurrentArrangeMode() == ChartFrame.TABBED) {
                    this.setTitleVisible(false);
                }
            }
        }
    }

    public ChartProperties setBaseSymbol(String symbol) {
        ChartProperties cp = null;
        if ((symbol != null) && (symbol.length() != 0)) {
            GraphDataManager gdm = graph.GDMgr;
            currentBaseSymbol = gdm.getBaseGraph();
            if (currentBaseSymbol != null) {
                String[] saComparingSymbols = gdm.getCompareGraphs();
                ChartInterface.removeOHLCRequest(CurrentMode, currentBaseSymbol);
                ChartInterface.removeOHLCRequest(CurrentMode, saComparingSymbols);
            }

            insertToSymbolsEx(g_oBaseSymbols, symbol);
            graph.setPercentagesflag(false);
            chkmiPercent.setState(false);
            if (isSendOHLCRequest) {
                if (CurrentMode) {
                    records = OHLCStore.getInstance().addIntradayRequest(symbol);
                } else {
                    records = OHLCStore.getInstance().addHistoryRequest(symbol);
                }
            }
            isSendOHLCRequest = true;
            registeredBaseSymbol = symbol;
            cp = processDataForSetGraphData(symbol);

            //added by charithn. IMPORtANT
            tableModel.setStore(graph.GDMgr.getGraphStore(), symbol, graph.GDMgr);
            graph.GDMgr.resetCustomYBounds();
            baseKey = SharedMethods.getKey(symbol);
            staticMode = false;
            try {
                tableModel.setDecimalCount(SharedMethods.getDecimalPlaces(symbol)); //high priority add to pramoda

            } catch (Exception e) {
                e.printStackTrace();
            }
        } else {
            records = null;
            processDataForSetGraphData(null);
        }
        records = null;
        return cp;
    }

    public ChartProperties setBaseSymbol(String symbol, File dataFile) {
        ChartProperties cp = null;
        if ((symbol != null) && (symbol.length() != 0)) {
            GraphDataManager gdm = graph.GDMgr;
            currentBaseSymbol = gdm.getBaseGraph();
            if (currentBaseSymbol != null) {
                String[] saComparingSymbols = gdm.getCompareGraphs();
                ChartInterface.removeOHLCRequest(CurrentMode, currentBaseSymbol);
                ChartInterface.removeOHLCRequest(CurrentMode, saComparingSymbols);
            }

            insertToSymbolsEx(g_oBaseSymbols, symbol);
            graph.setPercentagesflag(false);
            chkmiPercent.setState(false);

            try {
                records = LayoutFactory.loadRecords(symbol, dataFile);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            registeredBaseSymbol = symbol;
            cp = processDataForSetGraphData(symbol);
            cp.setSavedAndOpened(true);
            //added by charithn
            tableModel.setStore(graph.GDMgr.getGraphStore(), symbol, graph.GDMgr);
            graph.GDMgr.resetCustomYBounds();
            baseKey = SharedMethods.getKey(symbol);
            staticMode = false;
            try {
                tableModel.setDecimalCount(SharedMethods.getDecimalPlaces(symbol));
            } catch (Exception e) {
            }
        } else {
            records = null;
            processDataForSetGraphData(null);
        }
        records = null;

        return cp;
    }

    private Color getNewColor() {
        boolean newColor;
        if ((graph != null) && (graph.GDMgr != null))
            for (int j = 0; j < graphColors.length; j++) {
                ArrayList al = graph.GDMgr.graphColors;
                newColor = true;
                for (int i = 0; i < al.size(); i++) {
                    Color c = (Color) al.get(i);
                    if (c.equals(graphColors[j])) {
                        newColor = false;
                        break;
                    }
                }
                if (newColor) return graphColors[j];
            }
        return graphColors[0];
    }

    public ChartProperties setComparisonSymbol(String sKey) {
        ChartProperties cp = null;
        insertToSymbolsEx(g_oComparedSymbols, sKey);
        if (parent != null) {
            parent.bottomToolBar.resetCompareCmb();
        }
        if ((sKey == null) || (sKey.trim().length() == 0)) {
        } else if (isAlreadyDrawn(sKey)) {
        } else if (compareGraphCount == graph.MAX_ALLOWED_GRAPHS) {
        } else {
            if (CurrentMode) {
                records = OHLCStore.getInstance().addIntradayRequest(sKey);
            } else {
                records = OHLCStore.getInstance().addHistoryRequest(sKey);
            }
            currentCompareSymbol = sKey;
            if (records != null) {
                graph.NotifyChange = true;
                cp = graph.setComparisonRecords(records, null, currentCompareSymbol, getNewColor());
            } else {
                new ShowMessage(false, Language.getString("SYMBOL_NOT_FOUND"), "E");
            }
            records = null;
        }
        return cp;
    }

    public ChartProperties setComparisonSymbol(String sKey, File dataFile) {
        ChartProperties cp = null;
        insertToSymbolsEx(g_oComparedSymbols, sKey);
        if (parent != null) {
            parent.bottomToolBar.resetCompareCmb();
        }
        if ((sKey == null) || (sKey.trim().length() == 0)) {
        } else if (isAlreadyDrawn(sKey)) {
        } else if (compareGraphCount == graph.MAX_ALLOWED_GRAPHS) {
        } else {
            try {
                records = LayoutFactory.loadRecords(sKey, dataFile);
            } catch (Exception e) {
                e.printStackTrace();
                return null;
            }

            currentCompareSymbol = sKey;
            if (records != null) {
                graph.NotifyChange = true;
                cp = graph.setComparisonRecords(records, null, currentCompareSymbol, getNewColor());
                cp.setSavedAndOpened(true);
            } else {
                new ShowMessage(false, Language.getString("SYMBOL_NOT_FOUND"), "E");
            }
            records = null;
        }
        return cp;
    }

    /**
     * This method is called before the popup menu becomes visible
     */
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
        Object obj = e.getSource();
        if (obj instanceof JComboBox) {

        } else if (obj.equals(jPopupMenu1)) {
            updatePopupMenu();
        }

        if (obj.equals(jPopupMenu1)) {
            mnuCI1.removeAll();
            for (final IndicatorMenuObject imo : IndicatorDetailStore.CustomIndicators) {
                TWMenuItem miInd = new TWMenuItem(imo.getIndicatorName());
                miInd.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        graph.GDMgr.addCustomIndicator(imo.getIndicatorName(), imo.getIndicatorClass(), false);
                    }
                });
                miInd.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
                mnuCI1.add(miInd);
            }
            GUISettings.applyOrientation(mnuCI1);
        }
        //mnuBuy.setEnabled(ChartInterface.isTradingConnected());   //changed by charithn
        mnuBuy.setEnabled(ChartInterface.isTradingConnected(SharedMethods.getExchangeFromKey(baseKey)));
        mnuBuy.updateUI();
        //mnuSell.setEnabled(ChartInterface.isTradingConnected());  //changed by charithn
        mnuSell.setEnabled(ChartInterface.isTradingConnected(SharedMethods.getExchangeFromKey(baseKey)));
        mnuSell.updateUI();
        //chkmiShowOrderLines.setEnabled(ChartInterface.isTradingConnected());
        chkmiShowOrderLines.setEnabled(ChartInterface.isTradingConnected(SharedMethods.getExchangeFromKey(baseKey)));
        mnuChartOptions.setEnabled(!detached);
        if (detached) {
            mnuLinkTOsidebar.setVisible(detached);
            mnuLinkTOsidebar.setEnabled(LinkStore.getSharedInstance().canLinkToSidebar(ViewSettingsManager.OUTER_CHART));
        } else {
            mnuLinkTOsidebar.setVisible(false);
            mnuLinkTOsidebar.setEnabled(false);
        }
        mnuAnalysisTchnique.setEnabled(!detached);
        chkmiShowOrderLines.updateUI();

        if (graph.isCashFlowMode()) {
            for (int i = 0; i < jPopupMenu1.getComponentCount(); i++) {
                jPopupMenu1.getComponent(i).setVisible(false);
            }
        }
    }


    /**
     * This method is called before the popup menu becomes invisible _Note that a JPopupMenu can become invisible any
     * time
     */

    public Class getIndicatorClassByName(String IndName, ArrayList<IndicatorMenuObject> alIndArray) {
        Class indClass = null;
        for (final IndicatorMenuObject imo : alIndArray) {
            if (IndName.equals(imo.getIndicatorName())) {
                indClass = imo.getIndicatorClass();
            }
        }
        return indClass;


    }

    public void insertPattern(String patternNam) {
        try {
            for (final IndicatorMenuObject imo : ChartFrame.getSharedInstance().alPatterns) {
                if (patternNam.equals(imo.getIndicatorName())) {
                    graph.GDMgr.addCustomIndicator(imo.getIndicatorName(), imo.getIndicatorClass(), false);
                }
            }
        } catch (Exception e) {//pattern not found eXpctopin
            e.printStackTrace();
        }
    }

    public ArrayList<String> getCustomIndicatorNames() {
        ArrayList<String> custIndNames = new ArrayList<String>();
        for (final IndicatorMenuObject imo : IndicatorDetailStore.CustomIndicators) {
            custIndNames.add(imo.getIndicatorName());
        }
        return custIndNames;
    }

    public void insertStrategy(String StratetyName) {
        try {
            for (final IndicatorMenuObject imo : ChartFrame.getSharedInstance().alStrategies) {
                if (StratetyName.equals(imo.getIndicatorName())) {
                    graph.GDMgr.addCustomIndicator(imo.getIndicatorName(), imo.getIndicatorClass(), false);

                }
            }
        } catch (Exception e) {//pattern not found eXpctopin
            e.printStackTrace();
        }
    }

    public void insertCustomIndicator(String custIndName) {
        try {
            for (final IndicatorMenuObject imo : IndicatorDetailStore.CustomIndicators) {
                if (custIndName.equals(imo.getIndicatorName())) {
                    graph.GDMgr.addCustomIndicator(imo.getIndicatorName(), imo.getIndicatorClass(), false);
                }
            }
        } catch (Exception e) {//pattern not found eXpctopin
            e.printStackTrace();
        }
    }


    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
        Object obj = e.getSource();
        if (obj.equals(jPopupMenu1)) {
            if (parent != null) {
                refreshTopToolBar(parent.topToolBar);
            }
        }
    }

    public void refreshTopToolBar(GraphToolBar toolbar) {
        toolbar.refreshToolBar(detached, ChartInterface.isHistoryEnabled(),
                (graph.GDMgr.getBaseGraph() != null), CurrentMode,
                graph.getPeriodString(graph.getPeriod()),
                graph.getIntervalString(), graph.getGraphStyle(),
                ZoomDragEnabled, boxZoomEnabled, graph.isShowingVolumeGrf(),
                graph.GDMgr.isShowVolumeByPrice(), graph.isShowingTurnOverGraph(), graph.GDMgr.isIndexed(), graph.getLegendType(),
                graph.GDMgr.isShowingSplits(), graph.GDMgr.getGridType(), tableMode, staticMode, parent.isDataWinVisible(),
                graph.GDMgr.isSplitAdjusted(), graph.GDMgr.isShowingAnnouncements(),
                graph.GDMgr.isShowCurrentPriceLine(),graph.GDMgr.isshowPreviousCloseLine(), graph.GDMgr.isShowOrderLines(), graph.GDMgr.isShowBidAskTags(),
                graph.GDMgr.isShowMinMaxPriceLines(),
                (parent != null) ? parent.isShowCrossHairs() : false, graph.GDMgr.isShowResistanceLines(), graph.GDMgr.isSnapToPrice(), isFibCalMode, isRectangularSelection,
                (parent != null) ? parent.isShowSnapBall() : false, (parent != null) ? parent.isChatNavigationBarVisible() : false, isErasorMode, graph.GDMgr.getSplitAdjustMethod());
    }

    public void setBasicPropertiesFromCP() {
        ChartProperties cp = graph.GDMgr.getBaseCP();
        graph.setGraphStyle(cp.getChartStyle());
    }

    public void refreshBottomToolBar(BottomToolBar toolbar) {
        toolbar.refreshToolBar(graph.GDMgr.getBaseGraph() != null,
                ChartInterface.isIndicatorsEnabled());
    }

    /**
     * This method is called when the popup menu is canceled
     */
    public void popupMenuCanceled(PopupMenuEvent e) {
    }
    //end interface PopupMenuListener extends EventListener {
    //###################################################

    public void updatePopupMenu() {

        mnuNew.setVisible(!detached);
        miOpen.setVisible(!detached);
        StockGraph g = graph;

        refreshBaseSymbolSubMenu();
        refreshCompareSymbolsSubMenu();
        refreshTemplateSubMenu();
        if ((g != null) && (g.GDMgr != null)) {
            markBaseGraph(mnuSubBaseSymbol);
            markCompareGraphs(mnuSubCompare);
            chkmiPercent.setState(g.GDMgr.isIndexed());
            chkmiShowVolume.setState(graph.isShowingVolumeGrf());
            chkmiShowTurnOver.setState(graph.isShowingTurnOverGraph());
            chkmiShowCurrentPrice.setState(graph.GDMgr.isShowCurrentPriceLine());
            chkmiShowPreviousClose.setState(graph.GDMgr.isshowPreviousCloseLine());
            chkmiShowOrderLines.setState(graph.GDMgr.isShowOrderLines());
            chkmiShowBidAskTags.setState(graph.GDMgr.isShowBidAskTags());
            chkmiShowMinMaxLines.setState(graph.GDMgr.isShowMinMaxPriceLines());
            boolean baseAdded = graph.GDMgr.getBaseGraph() != null;
            mnuSubCompare.setEnabled(baseAdded);
            miPrint.setEnabled(baseAdded);
            miDetach.setEnabled(baseAdded);
            miSemiLogScale.setEnabled(baseAdded);
            mnuSubTemplate.setEnabled(baseAdded);
        }
        if (detached) {
            miDetach.setText(Language.getString("SEND_TO_UNICHARTS"));
        } else {
            miDetach.setText(Language.getString("DETACH"));
        }
        mnuIndicators.setEnabled(graph.GDMgr.getBaseGraph() != null);
        refreshIntervalSubMenu();
        refreshPeriodSubMenu();
        refreshStyleSubMenu();
        markMode(mnuSubMode);
        markPeriod(mnuSubPeriod);
        markInterval(mnuSubInterval);
        markStyle(mnuSubStyle);
        markShowVolume(chkmiShowVolume);
        markShowTurnOver(chkmiShowTurnOver);
        chkmiShowCurrentPrice.setState(graph.GDMgr.isShowCurrentPriceLine());
        chkmiShowPreviousClose.setState(graph.GDMgr.isshowPreviousCloseLine());
        chkmiShowOrderLines.setState(graph.GDMgr.isShowOrderLines());
        chkmiShowBidAskTags.setState(graph.GDMgr.isShowBidAskTags());
        chkmiShowMinMaxLines.setState(graph.GDMgr.isShowMinMaxPriceLines());
        markIndexed(chkmiPercent);
        markShowDescription(mnuSubLegend);
        markYAxis(mnuSubYAxis);
        mnuDataMode.setEnabled(!staticMode);
    }

    public void markBaseGraph(TWMenu mnuSymbol) {
        TWMenuItem miUnSelected;
        String smbl = graph.GDMgr.getBaseGraph();
        for (int i = 0; i < mnuSymbol.getItemCount(); i++) {
            miUnSelected = (TWMenuItem) mnuSymbol.getItem(i);
            miUnSelected.setSelected(false);
            miUnSelected.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }
        if (smbl == null) return;
        int ind = g_oBaseSymbols.getIndexOf(smbl);
        if (ind < 0) return;
        paintMenuItemAsSelected(ind, mnuSymbol);
    }

    public void markCompareGraphs(TWMenu mnuCompare) {
        TWMenuItem miUnSelected;
        String[] smbls = graph.GDMgr.getCompareGraphs();
        for (int i = 0; i < mnuCompare.getItemCount(); i++) {
            miUnSelected = (TWMenuItem) mnuCompare.getItem(i);
            miUnSelected.setSelected(false);
            miUnSelected.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
        }
        if (smbls == null) return;
        int ind;
        for (int i = 0; i < smbls.length; i++) {
            ind = g_oComparedSymbols.getIndexOf(smbls[i]);
            if (ind >= 0) {
                paintMenuItemAsSelected(ind, mnuCompare);
            }
        }
    }

    public void markMode(TWMenu mnuMode) {
        if (CurrentMode) {
            paintMenuItemAsSelected(1, mnuMode);
            paintMenuItemAsUnSelected(0, mnuMode);
        } else {
            paintMenuItemAsSelected(0, mnuMode);
            paintMenuItemAsUnSelected(1, mnuMode);
        }
    }

    public void markShowDescription(TWMenu mnuSubLegend) {
        if (graph.getLegendType() > StockGraph.INT_LEGEND_SYMBOL) {
            paintMenuItemAsSelected(1, mnuSubLegend);
            paintMenuItemAsUnSelected(0, mnuSubLegend);
        } else {
            paintMenuItemAsSelected(0, mnuSubLegend);
            paintMenuItemAsUnSelected(1, mnuSubLegend);
        }
    }

    public void markPeriod(TWMenu mnuPeriod) {
        if (graph != null) {
            String strI = graph.getPeriodString(graph.getPeriod());
            String[] strPeriods;
            if (CurrentMode) {
                strPeriods = strCurrentPeriods;
            } else {
                strPeriods = strHistoryPeriods;
            }
            int ind = getIndexOf(strI, strPeriods);
            paintMenuItemAsSelected(ind, mnuPeriod);
        }
    }

    public void markYAxis(TWMenu mnuSubYAxis) {
        if (graph != null) {
            int ind = graph.yAxisPos;
            paintMenuItemAsSelected(ind, mnuSubYAxis);
        }
    }

    public void markInterval(TWMenu mnuInterval) {
        if (graph != null) {
            String strI = graph.getIntervalString();
            String[] strIntervals;
            if (CurrentMode) {
                strIntervals = strCurrentIntervals;
            } else {
                strIntervals = strHistoryIntervals;
            }
            int ind = getIndexOf(strI, strIntervals);
            paintMenuItemAsSelected(ind, mnuInterval);
        }
    }

    public void markStyle(TWMenu mnuStyle, boolean setIcon) {
        int style = graph.getGraphStyle();
        if (setIcon) {
            Icon ico = GraphToolBar.styleActiveIcon[style];
            paintMenuItemAsSelected(style, mnuStyle, ico);
        } else {
            paintMenuItemAsSelected(style, mnuStyle);
        }
    }

    public void markStyle(TWMenu mnuStyle) {
        paintMenuItemAsSelected(graph.getGraphStyle(), mnuStyle);
    }

    public void markShowVolume(JCheckBoxMenuItem chkmiShowVol) {
        chkmiShowVol.setState(graph.isShowingVolumeGrf());
    }

    public void markShowVolumeByPrice(JCheckBoxMenuItem chkmiShowVol) {
        chkmiShowVol.setState(graph.GDMgr.isShowVolumeByPrice());
    }


    public void markShowTurnOver(JCheckBoxMenuItem chkmiShowTurnover) {
        chkmiShowTurnover.setState(graph.isShowingTurnOverGraph());
    }

    public void markIndexed(JCheckBoxMenuItem chkmiInd) {
        chkmiInd.setState(graph.GDMgr.isIndexed());
    }

    //######################################################
    //implementing ComponentListener
    /**
     * Invoked when the component's size changes.
     */
    public void componentResized(ComponentEvent e) {
//		System.out.println("frame resized");
    }

    /**
     * Invoked when the component's position changes.
     */
    public void componentMoved(ComponentEvent e) {

    }

    /**
     * Invoked when the component has been made visible.
     */
    public void componentShown(ComponentEvent e) {

    }

    /**
     * Invoked when the component has been made invisible.
     */
    public void componentHidden(ComponentEvent e) {

    }

    private ArrayList xPoints = new ArrayList();
    private ArrayList yPoints = new ArrayList();

    //end implementing ComponentListener
    //######################################################
    // implementing GraphMouseListener
    /**
     * Invoked when a mouse button has been pressed on graph.
     */
    public void graphMousePressed(MouseEvent e) {

        int ObjectTobeDrawn;
        if (parent == null) {
            ObjectTobeDrawn = AbstractObject.INT_NONE;
        } else {
            ObjectTobeDrawn = parent.bottomToolBar.ObjectTobeDrawn;
        }
        try {
            this.setSelected(true);
        } catch (PropertyVetoException ex) {
            ex.printStackTrace();
        }
        if ((ObjectTobeDrawn > AbstractObject.INT_NONE) && (SwingUtilities.isLeftMouseButton(e))) {
            isErasorMode = false; // if user tries to draw a new object then should clear the erasor mode
            MouseDownPoint.setLocation(e.getX(), e.getY());
            PreMouseDragPoint.setLocation(MouseDownPoint);

            MouseDownBeginPos = graph.zSlider.getBeginPos();
            //MouseDownZoom = graph.zSlider.getZoom();

            isDrawingALine = true;

            // drawing a preview line
            Graphics2D gg = (Graphics2D) graph.getGraphics();
            gg.setXORMode(Color.gray);
            gg.setStroke(BS_1p0f_2by2_dashed);
            int x0 = (int) MouseDownPoint.getX();
            int y0 = (int) MouseDownPoint.getY();
            int pnlID = graph.getPanelIDatPoint(x0, y0);
            Rectangle r = (Rectangle) graph.panels.get(pnlID);

            // all with mouse dowm has
            switch (ObjectTobeDrawn) {
                case AbstractObject.INT_LINE_HORIZ:
                    gg.drawLine(r.x, y0, r.x + r.width, y0);
                    break;
                case AbstractObject.INT_LINE_VERTI:
                    gg.drawLine(x0, r.y + StockGraph.titleHeight, x0, r.y + r.height);
                    break;
                case AbstractObject.INT_DATA_LINE_VERTI:
                    if (dwTableListener != null && parent != null && parent.getDataWindow() != null) {
                        graph.GDMgr.verticalX = -1;
                        graph.repaint();
                        parent.getDataWindow().removeSelectionListener();
                    }
                    break;
                case AbstractObject.INT_FIBONACCI_ZONES:
                    int x, x1, y1, y2;
                    y1 = r.y + StockGraph.titleHeight;
                    y2 = r.y + r.height;
                    x1 = x0;
                    int preVal = 0;
                    int curVal = 1;
                    int tmpVal = 0;
                    int sum = 0;
                    gg.drawLine(x1, y1, x1, y2);
                    while (true) {
                        sum += curVal;
                        if (graph.GDMgr.xFactor <= 0.0)
                            break;//addded by sathyajith to avoid  unended looping for no data charts
                        x = x1 + Math.round(sum * graph.GDMgr.xFactor);
                        if (x > r.x + r.width) break;
                        gg.drawLine(x, y1, x, y2);
                        tmpVal = curVal;
                        curVal += preVal;
                        preVal = tmpVal;
                    }
                    break;
            }
        }
        if ((ObjectTobeDrawn == AbstractObject.INT_NONE) && (SwingUtilities.isLeftMouseButton(e))) {
            int ex = e.getX();
            int wy = e.getY();
            MouseDownPoint.setLocation(ex, wy);
            PreMouseDragPoint.setLocation(ex, wy);
            MouseDownBeginPos = graph.zSlider.getBeginPos();
            if (isOnYaxis(ex, wy)) {
                isYAxisRescaling = true;
            } else if (isOnXaxis(ex, wy)) {
                isRightMarginRescaling = true;
            } else {
                if (e.isControlDown()) {
                    System.out.println("====== CTRL clicked =====");
                    graph.setActiveObjectIndexTest(ex, wy); //IMPORTANT
                } else {
                    graph.setActiveObjectIndex(ex, wy); //IMPORTANT
                }
                if (graph.GDMgr.getObjectHiLighted() > -1) {
                    isLineMoving = graph.GDMgr.isObjectReadyToMove();
                    isLineSizing = !isLineMoving;
                    firstTimeDrawing = true;
                    hintPane.setVisible(false);
                } else if (ZoomDragEnabled) {
                    isDragZooming = true;
                }
            }
        }
        //GRAPH DOUBLE CLICKED
        if (e.getClickCount() > 1) {
            if ((graph != null) && (graph.GDMgr != null)) {
                int annInd = graph.GDMgr.isOnAnAnnouncement(e.getX(), e.getY(), false);
                if (annInd > -1) {
                    try {
                        long annDate = graph.GDMgr.getAnnouncementDate(e.getX(), e.getY(), annInd);
                        TWDateFormat formatter = new TWDateFormat("yyyy MM dd");
                        String date = formatter.format(annDate);
                        System.out.println("***************** announcement Date *********** " + date);
                        ChartProperties cp = (ChartProperties) graph.GDMgr.getSources().get(annInd);
                        ChartInterface.showAnnouncements(cp.getSymbol(), annDate - 1, annDate + graph.getInterval() * 1000 - 1);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                } else {
                    isLineMoving = false;
                    hintPane.setVisible(false);
                    // if the user double click on the graph it should perform the zooming(priority to zooming).
                    // dont popup the property dialog in this case 
                    if (!(zoomInEnabled || zoomOutEnabled || ZoomDragEnabled || boxZoomEnabled || ObjectTobeDrawn != AbstractObject.INT_NONE)) {
                        JFrame parent = ChartInterface.getChartFrameParentComponent();
                        if ((graph.GDMgr.sourceHiLighted >= 0) && (graph.GDMgr.sourceHiLighted < graph.GDMgr.getSources().size()) ||
                                (graph.GDMgr.objectHiLighted > -1) && (graph.GDMgr.objectHiLighted < graph.GDMgr.getObjArray().size())) {
                            graph.GDMgr.popActiveObjectProperties();
                        } else if (!detached && isInsideRectangle(MouseDownPoint.x, MouseDownPoint.y)) {
                            AnalysisTechWindow.getSharedInstance().setVisible(true);
                        }
                    }
                }
            }
            if (ObjectTobeDrawn == AbstractObject.INT_ZIG_ZAG) {
                fireEscapeAction();
                ObjectTobeDrawn = AbstractObject.INT_NONE;
            }
        } else if (SwingUtilities.isRightMouseButton(e)) {
            GUISettings.showPopup(jPopupMenu1, graph, e.getX(), e.getY());
        }

        /* =========== fibonacci price retracements calculator - added by chatithn ============ */
        if (isFibCalMode && SwingUtilities.isLeftMouseButton(e)) {

            if (graph.GDMgr.fibCalBounds == null) {
                graph.GDMgr.fibCalBounds = new Point[3];
            }
            int pnlID = graph.getPanelIDatPoint(e.getX(), e.getY());
            WindowPanel r = (WindowPanel) graph.panels.get(pnlID);
            if (r != graph.mainRect) {
                return;
            }
            double yValue = graph.GDMgr.getYValueForthePixel(e.getY(), pnlID);

            if (clickCount < 2) {
                clickPoints[clickCount] = yValue;
                graph.GDMgr.fibCalBounds[clickCount] = new Point(e.getX(), e.getY());
                clickCount++;
            } else if (clickCount == 2) {
                graph.GDMgr.fibCalBounds[clickCount] = new Point(e.getX(), e.getY());
                graph.setCursor(Cursor.getDefaultCursor());
                clickPoints[clickCount] = yValue;
                isFibCalMode = false;
                clickCount = 0;
                refreshTopToolBar(parent.topToolBar);
                calculator = new FibonacciPriceCalculator(ChartInterface.getChartFrameParentComponent());
                calculator.setGraph(graph);
                calculator.showReults(clickPoints);
            }
            graph.repaint();
        } else {  //reset the click count
            graph.GDMgr.fibCalBounds = null;
            clickCount = 0;
            graph.repaint();
        }

        /* ========= fibonacci extension lines ========== */
        if (ObjectTobeDrawn == AbstractObject.INT_FIBONACCI_EXTENSIONS) {

            BottomToolBar.CURSOR_STATUS = BottomToolBar.STATUS_FIB_EXT;
            if (noOfMousePressed == 0) {
                firstMouseUpPoint.setLocation(e.getX(), e.getY());
                noOfMousePressed++;

            } else if (noOfMousePressed == 1) {
                secondMouseUpPoint.setLocation(e.getX(), e.getY());
                noOfMousePressed++;

            } else if (noOfMousePressed == 2) {

                thirdMouseUpPoint.setLocation(e.getX(), e.getY());
                noOfMousePressed = 0;
                int pnlID = graph.getPanelIDatPoint(e.getX(), e.getY());
                addObjectOnGraph(pnlID);
                parent.bottomToolBar.ObjectTobeDrawn = AbstractObject.INT_NONE;
                parent.bottomToolBar.btnNeutral.doClick();
                graph.setCursor(Cursor.getDefaultCursor());
            }
        }

        /* ========= arc object  ========== */
        if (ObjectTobeDrawn == AbstractObject.INT_ARC) {

            BottomToolBar.CURSOR_STATUS = BottomToolBar.STATUS_ARC;
            if (noOfMousePressed == 0) {
                firstMouseUpPoint.setLocation(e.getX(), e.getY());
                noOfMousePressed++;

            } else if (noOfMousePressed == 1) {
                secondMouseUpPoint.setLocation(e.getX(), e.getY());
                noOfMousePressed++;

            } else if (noOfMousePressed == 2) {

                thirdMouseUpPoint.setLocation(e.getX(), e.getY());
                noOfMousePressed = 0;
                int pnlID = graph.getPanelIDatPoint(e.getX(), e.getY());
                addObjectOnGraph(pnlID);
                parent.bottomToolBar.ObjectTobeDrawn = AbstractObject.INT_NONE;
                parent.bottomToolBar.btnNeutral.doClick();
                graph.setCursor(Cursor.getDefaultCursor());
            }
        }

        /* ========= andrew's pitchfork object  ========== */
        if (ObjectTobeDrawn == AbstractObject.INT_ANDREWS_PITCHFORK) {

            BottomToolBar.CURSOR_STATUS = BottomToolBar.STATUS_ARC;
            if (noOfMousePressed == 0) {
                firstMouseUpPoint.setLocation(e.getX(), e.getY());
                noOfMousePressed++;

            } else if (noOfMousePressed == 1) {
                secondMouseUpPoint.setLocation(e.getX(), e.getY());
                noOfMousePressed++;

            } else if (noOfMousePressed == 2) {

                thirdMouseUpPoint.setLocation(e.getX(), e.getY());
                noOfMousePressed = 0;
                int pnlID = graph.getPanelIDatPoint(e.getX(), e.getY());
                addObjectOnGraph(pnlID);
                parent.bottomToolBar.ObjectTobeDrawn = AbstractObject.INT_NONE;
                parent.bottomToolBar.btnNeutral.doClick();
                graph.setCursor(Cursor.getDefaultCursor());
            }
        }

        /* ========= fibonacci extension lines ========== */
        if (ObjectTobeDrawn == AbstractObject.INT_ZIG_ZAG) {

            BottomToolBar.CURSOR_STATUS = BottomToolBar.STATUS_ZIG_ZAG;
            xPoints.add(e.getX());
            yPoints.add(e.getY());

            int pnlID = graph.getPanelIDatPoint(CurrentMousePos.x, CurrentMousePos.y);

            if (xPoints.size() == 2) {
                addObjectOnGraph(pnlID);
            } else if (xPoints.size() > 2) {
                //get the zig zag object
                ArrayList objArray = graph.GDMgr.getObjArray();
                ObjectZigZag objZig = (ObjectZigZag) objArray.get(objArray.size() - 1);

                float[] xx = new float[xPoints.size()];
                float[] yy = new float[yPoints.size()];

                float[] xIndex = new float[xx.length];
                double[] yyy = new double[xx.length];
                for (int i = 0; i < xx.length; i++) {
                    xx[i] = Float.parseFloat(String.valueOf(xPoints.get(i)));
                    yy[i] = Float.parseFloat(String.valueOf(yPoints.get(i)));

                    Point2D.Double p = graph.getActualPoint2D(new Point2D.Float(xx[i], yy[i]), pnlID);
                    xIndex[i] = (float) p.x;
                    yyy[i] = p.y;
                }

                //update x & y arrays of the object
                long[] laX = graph.GDMgr.convertXArrayIndexToTime(xIndex);
                objZig.setXArr(laX);
                objZig.setIndexArray(xIndex);
                objZig.setYArr(yyy);

                graph.repaint();
            }
        }

        /* ========= eraser buttton feature ========== */
        if (isErasorMode) {

            ArrayList ObjectArray = graph.GDMgr.getObjArray();
            if (ObjectArray != null) {
                for (int i = ObjectArray.size() - 1; i >= 0; i--) {
                    AbstractObject oR = (AbstractObject) ObjectArray.get(i);
                    int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, oR.getRect());
                    //oR.setSelected(false);

                    //float[] xArr = graph.GDMgr.convertXArrayTimeToIndex(oR.getXArr());
                    float[] xArr = null;
                    if (oR.isFreeStyle()) {
                        xArr = oR.getIndexArray();
                    } else {
                        xArr = graph.GDMgr.convertXArrayTimeToIndex(oR.getXArr());
                    }
                    boolean isOnObj = false;
                    if (oR.isShownOnAllPanels()) {
                        isOnObj = oR.isCursorOnObject(e.getX(), e.getY(), xArr, true, graph.GDMgr.getBeginIndex(), 0, graph.GDMgr.xFactor, 0, true);
                    } else if (pnlID > -1) {
                        isOnObj = oR.isCursorOnObject(e.getX(), e.getY(), xArr, true, graph.GDMgr.getBeginIndex(), graph.GDMgr.minY[pnlID], graph.GDMgr.xFactor, graph.GDMgr.yFactor[pnlID], true);
                    }
                    if (isOnObj) {
                        ObjectArray.remove(i);
                        graph.GDMgr.setObjArray(ObjectArray);
                        graph.repaint();
                    }
                }
            }
        }

        hintPane.setVisible(false); //no need to show here

    }


    /**
     * Invoked when a mouse button has been released on graph.
     */

    public void graphMouseReleased(MouseEvent e) {
        MouseUpPoint.setLocation(e.getX(), e.getY());
        isDrawingALine = false;
        int ObjectTobeDrawn;
        if (parent == null) {
            ObjectTobeDrawn = AbstractObject.INT_NONE;
        } else {
            ObjectTobeDrawn = parent.bottomToolBar.ObjectTobeDrawn;
        }
        //added by charithn
        if (ObjectTobeDrawn == AbstractObject.INT_LINE_HORIZ || ObjectTobeDrawn == AbstractObject.INT_LINE_VERTI) {
            dragWindow.setVisible(false);
        } else if (ObjectTobeDrawn == AbstractObject.INT_DATA_LINE_VERTI && parent.getDataWindow() != null) {
            parent.getDataWindow().addSelectionListener();
            parent.getDataWindow().setDragMode(false);
            parent.getDataWindow().refreshButtons();
            //remove vertical data lines
            ArrayList<AbstractObject> array = (ArrayList<AbstractObject>) graph.GDMgr.getObjArray();
            ArrayList<AbstractObject> temp = (ArrayList<AbstractObject>) array.clone();

            for (int i = 0; i < array.size(); i++) {
                AbstractObject tmpObj = array.get(i);
                if (tmpObj.getObjType() == AbstractObject.INT_DATA_LINE_VERTI) {
                    temp.remove(tmpObj);
                }
            }
            graph.GDMgr.setObjArray(temp);
        }
        if (isYAxisRescaling) {
            isYAxisRescaling = false;
            graph.GDMgr.rescaleYAxis(MouseDownPoint, MouseUpPoint);
        } else if (isRightMarginRescaling) {
            isRightMarginRescaling = false;
            graph.GDMgr.rescaleRightMargin(MouseDownPoint, MouseUpPoint);
        } else if (isLineMoving || isLineSizing) {
            isLineMoving = false;
            isLineSizing = false;
            if (dragWindow != null) {
                dragWindow.setVisible(false);
            }
            graph.GDMgr.setNewBoundsForHiLightedObject(MouseDownPoint, MouseUpPoint, firstMouseUpPoint, secondMouseUpPoint, thirdMouseUpPoint); //existing object
        } else if (isDragZooming) {
            graph.GDMgr.dragStartX = -1;
            graph.GDMgr.dragEndX = -1;
            isDragZooming = false;
            int x1 = (int) Math.min(PreMouseDragPoint.getX(), MouseDownPoint.getX());
            int x2 = (int) Math.max(PreMouseDragPoint.getX(), MouseDownPoint.getX());
            if (x2 > x1) {
                graph.setDragZoom(x1, x2);
            }
        }
        if (SwingUtilities.isLeftMouseButton(e)) {
            if (ObjectTobeDrawn > AbstractObject.INT_NONE && ObjectTobeDrawn != AbstractObject.INT_FIBONACCI_EXTENSIONS
                    && ObjectTobeDrawn != AbstractObject.INT_ZIG_ZAG && ObjectTobeDrawn != AbstractObject.INT_ARC && ObjectTobeDrawn != AbstractObject.INT_ANDREWS_PITCHFORK) {
                int xMouse;
                int yMouse;
                if (AbstractObject.isMouseDownHasHighPriority(ObjectTobeDrawn)) {
                    xMouse = (int) MouseDownPoint.getX();
                    yMouse = (int) MouseDownPoint.getY();
                } else {
                    xMouse = (int) MouseUpPoint.getX();
                    yMouse = (int) MouseUpPoint.getY();
                }
                int pnlID = graph.getPanelIDatPoint(xMouse, yMouse);

                //TODO: IMPORTANT-commnted by charithn-needs to uncomment here....
                addObjectOnGraph(pnlID); //adding new new objects on graph

                if (parent != null && (ChartOptions.getCurrentOptions().gen_add_studies == ChartOptions.AddStudiesOption.CONTINUE_WITH_SHIFT)) {
                    if (!((e.getModifiersEx() & e.CTRL_DOWN_MASK) == e.CTRL_DOWN_MASK)) {
                        parent.bottomToolBar.btnNeutral.doClick();
                    }
                } else {
                    parent.bottomToolBar.btnNeutral.doClick();
                }
            } else if (boxZoomEnabled && isBoxZooming) {
                isBoxZooming = false;
                int x1 = (int) Math.min(PreMouseDragPoint.getX(), MouseDownPoint.getX());
                int y1 = (int) Math.min(PreMouseDragPoint.getY(), MouseDownPoint.getY());
                int x2 = (int) Math.max(PreMouseDragPoint.getX(), MouseDownPoint.getX());
                int y2 = (int) Math.max(PreMouseDragPoint.getY(), MouseDownPoint.getY());
                graph.setBoxZoom(x1, y1, x2, y2);
            } /*else if (isPointZooming && e.isControlDown()) {     //TODO: needs to uncomment here..
                int xMouse = (int) MouseUpPoint.getX();
                int yMouse = (int) MouseUpPoint.getY();
                int pnlID = graph.getPanelIDatPoint(xMouse, yMouse);
                Rectangle rect = (Rectangle) graph.panels.get(pnlID);
                graph.setDragZoom(rect.x + rect.width * 15 / 100, rect.x + rect.width * 85 / 100);
            } */
            else if (zoomInEnabled) {
                //added by charithn
                int x = (int) MouseUpPoint.getX();
                graph.setZoomIn(x);
            } else if (zoomOutEnabled) {
                //added by charithn
                int x = (int) MouseUpPoint.getX();
                graph.setZoomOut(x);
            }
        }
        if (parent != null) {
            parent.bottomToolBar.btnSlopeLine.requestFocus();
        }

        if (BottomToolBar.CURSOR_STATUS == BottomToolBar.STATUS_NONE) {
            setCursor(Cursor.getPredefinedCursor(Cursor.DEFAULT_CURSOR));
        }

        /* =============== multiple selection of objets ================ */
        if (ObjectTobeDrawn == AbstractObject.INT_RECT_SELECTION) {

            ArrayList<AbstractObject> array = (ArrayList<AbstractObject>) graph.GDMgr.getObjArray();
            ArrayList<AbstractObject> temp = (ArrayList<AbstractObject>) array.clone();
            Rectangle r = null;

            for (int i = 0; i < array.size(); i++) {
                AbstractObject ao = array.get(i);
                if (ao instanceof ObjectRectangularSelection) {
                    ObjectRectangularSelection ors = (ObjectRectangularSelection) ao;
                    long[] xarr = ors.getXArr();
                    double[] yarr = ors.getYArr();

                    int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, ors.getRect());
                    double y0 = graph.GDMgr.getPixelFortheYValue(yarr[0], pnlID);
                    double y1 = graph.GDMgr.getPixelFortheYValue(yarr[1], pnlID);

                    float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(xarr);
                    double x0 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);
                    double x1 = graph.GDMgr.getPixelFortheIndex(indexArr[1]);

                    if (x1 > x0) {
                        r = new Rectangle((int) x0, (int) y0, (int) Math.abs(x0 - x1), (int) Math.abs(y0 - y1));
                    } else {
                        r = new Rectangle((int) x1, (int) y1, (int) Math.abs(x0 - x1), (int) Math.abs(y0 - y1));
                    }
                    if (i < (temp.size() - 1)) {
                        temp.remove(i);  //removes the temporary drawn recangle selection
                    }
                }
            }

            for (AbstractObject aTemp : temp) {
                boolean result = aTemp.isInsideTheRectangle(r, graph);
                aTemp.setSelected(result);
            }

            graph.GDMgr.setObjArray(temp);
            isRectangularSelection = true;
            graph.repaint();
        }


    }

    /**
     * Invoked when a mouse button has been moved on graph.
     */
    public void graphMouseMoved(MouseEvent e) {
        //any object
        if (!isDrawingALine) {
            Point screenP = new Point(e.getPoint());
            SwingUtilities.convertPointToScreen(screenP, graph);
            SwingUtilities.convertPointFromScreen(screenP, this.getContentPane());
            CurrentMousePos.setLocation(screenP.x, screenP.y);
            screenP = null;
        }

        int splitId = graph.GDMgr.isOnASplit(e.getX(), e.getY());
        if (splitId > -1) {
            showSplitTooltip = true;
            long splitDate = graph.GDMgr.getSplitDate(e.getX(), e.getY(), splitId);
            ChartProperties cp = (ChartProperties) graph.GDMgr.getSources().get(splitId);
            ArrayList<ChartSplitPoints> alSplits = cp.getSplitPointsForInterval(splitDate, graph.getInterval());
            splitTooltip.setTooltipData(alSplits); //setSize(110, 20);
            splitTooltip.setLocation(getToolTipPos(e.getX(), e.getY(), splitTooltip.getWidth(), splitTooltip.getHeight()));
            pnlInfoPanel.setVisible(false);
            hintPane.setVisible(true);
        } else {
            showSplitTooltip = false;
            splitTooltip.setSize(0, 0);
            hintPane.setVisible(false);
        }

        if (dwListener != null && parent != null && parent.getDataWindow() != null && parent.getDataWindow().isWindowMode()) {
            dwListener.dataPointChanged(e, CurrentMousePos, graph);
        } else if (dwTableListener != null && parent != null && parent.getDataWindow() != null && !parent.getDataWindow().isWindowMode() && parent.isShowCrossHairs()) {
            //dwTableListener.dataPointChangedOnGraph(e, CurrentMousePos, graph);
        }

        if (parent != null && parent.isShowCrossHairs()) { // 3 cross hair types

            if (graph.GDMgr.getCrossHairType() == GraphDataManager.NORMAL_CROSS_HAIR) {
                graph.GDMgr.crossHairX = e.getX();
                graph.GDMgr.crossHairY = e.getY();

                graph.GDMgr.pinBallX = -1;
                graph.GDMgr.highLowCrossX = -1;
                graph.GDMgr.highLowCrossY = -1;
            } else if (graph.GDMgr.getCrossHairType() == GraphDataManager.SNAP_BALL_CROSS_HAIR) {
                graph.GDMgr.pinBallX = e.getX();

                graph.GDMgr.crossHairX = e.getX();
                graph.GDMgr.crossHairY = e.getY();
                graph.GDMgr.highLowCrossX = -1;
                graph.GDMgr.highLowCrossY = -1;
            } else if (graph.GDMgr.getCrossHairType() == GraphDataManager.HIGH_LOW_CROSS_HAIR) {
                graph.GDMgr.highLowCrossX = e.getX();
                graph.GDMgr.highLowCrossY = e.getY();

                graph.GDMgr.crossHairX = -1;
                graph.GDMgr.crossHairY = -1;
                graph.GDMgr.pinBallX = -1;
            }
            graph.repaint();
        }

        if (parent != null && parent.isShowSnapBall()) {
            graph.GDMgr.pinBallX = e.getX();
            graph.repaint();
        }

        drawMoveImageForNewObject();
        PreCurrentMousePos.setLocation(CurrentMousePos);
    }

    /**
     * Invoked when a mouse button has been dragged on graph.
     */
    public void graphMouseDragged(MouseEvent e) {
        MouseDragPoint.setLocation(e.getX(), e.getY());    // Modified 25-03-2006-Udaka (line added)

        if (isYAxisRescaling) {
            markYaxisRescaleArea();
        } else if (isRightMarginRescaling) {
            markXaxisRescaleArea();
        } else if (isDrawingALine) {
            int ObjectTobeDrawn = AbstractObject.INT_NONE;
            if (parent != null) {
                ObjectTobeDrawn = parent.bottomToolBar.ObjectTobeDrawn;
            }

            dragWindow.setVisible(false);
            hintPane.setVisible(false); // no need of tooltip here
            drawDragImageForNewObject();

            /* shows the small data window with mouse dragging in horizonal and vertical lines*/
            if (ObjectTobeDrawn == AbstractObject.INT_LINE_HORIZ) {
                dragWindowListener.dataDragPointChanged(MouseDragPoint, graph, AbstractObject.INT_LINE_HORIZ);
            } else if (ObjectTobeDrawn == AbstractObject.INT_LINE_VERTI) {
                dragWindowListener.dataDragPointChanged(MouseDragPoint, graph, AbstractObject.INT_LINE_VERTI);
            } else if (ObjectTobeDrawn == AbstractObject.INT_DATA_LINE_VERTI) {
                if (dwTableListener != null && parent != null && parent.getDataWindow() != null) {

                    //ListSelectionListener[] listenrs = parent.getDataWindow().getTable().getListeners(ListSelectionListener.class);
                    //System.out.println("======== SelectionListener listenrs count :  " + listenrs.length);

                    graph.GDMgr.verticalX = -1;
                    parent.getDataWindow().removeSelectionListener();
                    dwTableListener.dataPointChangedOnGraph(e, CurrentMousePos, graph);
                }
            } else if (dragWindow != null) {
                dragWindow.setVisible(false);
            }

        } else if (isLineMoving || isLineSizing) {
            firstTimeDrawing = false;
            //MouseDragPoint.setLocation(e.getX(), e.getY());    // Modified 25-03-2006-Udaka (line commented)
            //PreMouseDragPoint.setLocation(MouseDragPoint);     // Modified 25-03-2006-Udaka (line commented)

            graph.GDMgr.drawDraggedImageForHiLightedObject(MouseDownPoint, PreMouseDragPoint, MouseDragPoint, firstTimeDrawing, dragWindowListener);

            //added to show the tooltip when object dragging on the graph
            if (ChartOptions.getCurrentOptions().gen_show_tool_tip) {
                CurrentMousePos.setLocation(e.getX(), e.getY());
                AbstractObject or = (AbstractObject) graph.GDMgr.getObjArray().get(graph.GDMgr.getObjectHiLighted());
                showSplitTooltip = false;

                if (or != null && or.getToolTipRows() != null && or.getToolTipRows().size() > 0 && or.isShowingDragToolTip()) {
                    graph.updateToolTip(true, or.getToolTipRows().size(), or.getToolTipRows(), false, or.getShortName());
                } else {
                    graph.updateToolTip(false, 2, null, false, "");
                    hintPane.setVisible(false);
                }
            } else {
                hintPane.setVisible(false); //show only if the user requires
            }
            //added by shashika
            AbstractObject obj = (AbstractObject) graph.GDMgr.getObjArray().get(graph.GDMgr.getObjectHiLighted());
            if (obj.getObjType() == AbstractObject.INT_DATA_LINE_VERTI) {
                if (dwTableListener != null && parent != null && parent.getDataWindow() != null && !parent.getDataWindow().isWindowMode()) {
                    parent.getDataWindow().addSelectionListener();
                    graph.GDMgr.verticalX = -1;
                    //parent.getDataWindow().removeSelectionListener();
                    dwTableListener.dataPointChangedOnGraph(e, CurrentMousePos, graph);
                }
            }
            /*if (dwTableListener != null && parent != null && parent.getDataWindow() != null) {
                graph.GDMgr.verticalX = -1;
                parent.getDataWindow().removeSelectionListener();
                dwTableListener.dataPointChangedOnGraph(e, CurrentMousePos, graph);
            }*/
            firstTimeDrawing = false;
        } else if (isDragZooming) {
            graph.GDMgr.dragStartX = (int) MouseDownPoint.getX();
            graph.GDMgr.dragEndX = e.getX();
            graph.repaint();
        } else if (boxZoomEnabled) {
            markZoomBox();
        } else if (chartBodyDraggingEnabled && graph.zSlider.getZoom() < 100.0f) {
            float beginPosistion;

            float movedFactor;
            movedFactor = (float) ((e.getX() - MouseDownPoint.getX()) / (float) this.getWidth());

            movedFactor = movedFactor * BODY_DRAG_SMOOTHING_FACTOR;

            float movedPercent = movedFactor * graph.zSlider.getZoom();
            beginPosistion = MouseDownBeginPos - movedPercent;

            if (movedFactor > 0) { //move right
                beginPosistion = Math.max(beginPosistion, 0);
            } else if (movedFactor == 0) {//move right
                //do nothing
            } else {//move right
                beginPosistion = Math.min(beginPosistion, 100f - graph.zSlider.getZoom());
            }

            graph.zSlider.setPos(beginPosistion);
            graph.repaintZoomedGraph();
        }
        if (parent != null && parent.isShowCrossHairs()) {
            /**************************/

            /*********************************/
            if (graph.GDMgr.getCrossHairType() == graph.GDMgr.NORMAL_CROSS_HAIR) {

                if (!isDragZooming) {
                    graph.GDMgr.crossHairX = e.getX();
                    graph.GDMgr.crossHairY = e.getY();
                    graph.repaint();
                } else {
                    graph.GDMgr.crossHairX = (int) PreMouseDragPoint.getX();
                    graph.GDMgr.crossHairY = (int) PreMouseDragPoint.getY();
                    graph.GDMgr.drawCrossHairs(graph.getGraphics());
                    graph.GDMgr.crossHairX = e.getX();
                    graph.GDMgr.crossHairY = e.getY();
                    graph.GDMgr.drawCrossHairs(graph.getGraphics());
                }

                graph.GDMgr.pinBallX = -1;
                graph.GDMgr.highLowCrossX = -1;
            } else if (graph.GDMgr.getCrossHairType() == graph.GDMgr.SNAP_BALL_CROSS_HAIR) {

                if (!isDragZooming) {
                    graph.GDMgr.pinBallX = e.getX();

                    //cross
                    graph.GDMgr.crossHairX = e.getX();
                    graph.GDMgr.crossHairY = e.getY();
                    graph.repaint();

                } else {
                    graph.GDMgr.pinBallX = (int) PreMouseDragPoint.getX();

                    graph.GDMgr.drawPriceBall(graph.getGraphics());
                    graph.GDMgr.pinBallX = e.getX();
                    graph.GDMgr.drawPriceBall(graph.getGraphics());

                    // cross hair
                    graph.GDMgr.crossHairX = (int) PreMouseDragPoint.getX();
                    graph.GDMgr.crossHairY = (int) PreMouseDragPoint.getY();
                    graph.GDMgr.drawCrossHairs(graph.getGraphics());
                    graph.GDMgr.crossHairX = e.getX();
                    graph.GDMgr.crossHairY = e.getY();
                    graph.GDMgr.drawCrossHairs(graph.getGraphics());
                }

                // graph.GDMgr.crossHairX = -1;
                //graph.GDMgr.crossHairY = -1;
                graph.GDMgr.highLowCrossX = -1;
            } else if (graph.GDMgr.getCrossHairType() == graph.GDMgr.HIGH_LOW_CROSS_HAIR) {


                if (!isDragZooming) {
                    graph.GDMgr.highLowCrossX = e.getX();
                    graph.repaint();
                } else {
                    graph.GDMgr.highLowCrossX = (int) PreMouseDragPoint.getX();
                    graph.GDMgr.drawHighLowCrossHair(graph.getGraphics());
                    graph.GDMgr.highLowCrossX = e.getX();
                    graph.GDMgr.drawHighLowCrossHair(graph.getGraphics());
                }

                graph.GDMgr.crossHairX = -1;
                graph.GDMgr.crossHairY = -1;
                graph.GDMgr.pinBallX = -1;
            }
            /*********************************/

            /**************************/
        } else {
            graph.GDMgr.crossHairX = -1;
            graph.GDMgr.crossHairY = -1;
        }

        if (parent != null && parent.isShowSnapBall()) {
            if (!isDragZooming) {
                graph.GDMgr.pinBallX = e.getX();
                graph.repaint();
            } else {
                graph.GDMgr.pinBallX = (int) PreMouseDragPoint.getX();

                graph.GDMgr.drawPriceBall(graph.getGraphics());
                graph.GDMgr.pinBallX = e.getX();
                graph.GDMgr.drawPriceBall(graph.getGraphics());
            }
        }

        PreMouseDragPoint.setLocation(MouseDragPoint);
    }


    public void graphMouseExited(MouseEvent e) {
        graph.GDMgr.crossHairX = -1;
        graph.GDMgr.crossHairY = -1;
        graph.GDMgr.pinBallX = -1;
        graph.GDMgr.highLowCrossX = -1;
        graph.repaint();
    }

    /**
     * Invoked when a volume Panel has been Deleted.
     */
    public void volumePanelDeleted() {
        //chkmiShowVolume.setState(false);
        ChartFrame.getSharedInstance().refreshToolBars();
    }

    //end implementing GraphMouseListener
    //######################################################

    private boolean isOnYaxis(int x, int y) {
        //System.out.println("dragging pos x:"+x+" y:"+y);
        return graph.isOnYaxis(x, y);
    }

    // added by Mevan on 2009.11.26
    private boolean isOnXaxis(int x, int y) {
        Rectangle r = new Rectangle(graph.mainRect.x, graph.getHeight() - graph.sliderHeight - graph.borderWidth - graph.heightXaxis, graph.mainRect.width, graph.heightXaxis);
        return r.contains(x, y);
    }

    private void markYaxisRescaleArea() {
        Graphics2D gg = (Graphics2D) graph.getGraphics();
        gg.setXORMode(Color.darkGray);
        int x1 = (int) MouseDownPoint.getX();
        int y1 = (int) MouseDownPoint.getY();
        int yOld = (int) PreMouseDragPoint.getY();
        int yNew = (int) MouseDragPoint.getY();
        int pnlID = graph.getPanelIDatPoint(x1, y1);
        if (pnlID < 0) return;
        Rectangle r = (Rectangle) graph.panels.get(pnlID);
        int left = StockGraph.borderWidth;
        if (graph.yAxisPos != SplitPanel.AXIS_LEFT_ONLY) {
            if (r.x + r.width <= x1) {
                left = r.x + r.width;
            }
        }
        //determining center point
        int yC = r.y + r.height / 2;

        try {
            int yMax = r.y;
            int yMin = r.y + r.height;
            if (y1 > yC) {
                if (graph.isyAxisRescaleDirty()) {
                    gg.fillRect(left, y1, graph.widthYaxis, yMin - y1);
                    gg.drawLine(left + graph.widthYaxis / 2, yMax, left + graph.widthYaxis / 2, Math.max(y1, yOld));
                    if (yOld <= yMax) yOld = yMax + 1;
                    gg.drawLine(left, yOld, left + graph.widthYaxis, yOld);
                }
                gg.fillRect(left, y1, graph.widthYaxis, yMin - y1);
                gg.drawLine(left + graph.widthYaxis / 2, yMax, left + graph.widthYaxis / 2, Math.max(y1, yNew));
                if (yNew <= yMax) yNew = yMax + 1;
                gg.drawLine(left, yNew, left + graph.widthYaxis, yNew);

            } else {
                if (graph.isyAxisRescaleDirty()) {
                    gg.fillRect(left, yMax, graph.widthYaxis, y1 - yMax);
                    gg.drawLine(left + graph.widthYaxis / 2, Math.min(y1, yOld), left + graph.widthYaxis / 2, yMin);
                    if (yOld >= yMin) yOld = yMin - 1;
                    gg.drawLine(left, yOld, left + graph.widthYaxis, yOld);
                }
                gg.fillRect(left, yMax, graph.widthYaxis, y1 - yMax);
                gg.drawLine(left + graph.widthYaxis / 2, Math.min(y1, yNew), left + graph.widthYaxis / 2, yMin);
                if (yNew >= yMin) yNew = yMin - 1;
                gg.drawLine(left, yNew, left + graph.widthYaxis, yNew);
            }
            graph.setyAxisRescaleDirty(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    // added by Mevan on 2009.11.26
    private void markXaxisRescaleArea() {
        Graphics2D gg = (Graphics2D) graph.getGraphics();
        gg.setXORMode(Color.darkGray);
        int x1 = (int) MouseDownPoint.getX();
        int y1 = (int) MouseDownPoint.getY();

        // TODO: return if out of bounds for X-axis

        int xOld = (int) PreMouseDragPoint.getX();
        int xNew = (int) MouseDragPoint.getX();

        // rectangle marking the X-axis
        Rectangle r = new Rectangle(graph.mainRect.x, graph.getHeight() - graph.sliderHeight - graph.borderWidth - graph.heightXaxis, graph.mainRect.width, graph.heightXaxis);
        gg.setClip(r);

        //determining center point
        int xC = r.x + r.width / 2;

        try {
            int xMax = r.x + r.width;
            int xMin = r.x;
            if (x1 > xC) {
                if (graph.isRightMarginRescaleDirty()) {
                    gg.fillRect(x1, r.y, r.width, r.y + r.height);
                    gg.drawLine(xOld, r.y, xOld, r.y + r.height);
                    if (xOld <= xMax) xOld = xMax + 1;
                    gg.drawLine(r.x, r.y + r.height / 2, xOld, r.y + r.height / 2);
                }
                gg.fillRect(x1, r.y, r.width, r.y + r.height);
                gg.drawLine(xOld, r.y, xOld, r.y + r.height);
                if (xNew <= xMax) xNew = xMax + 1;
                gg.drawLine(r.x, r.y + r.height / 2, xNew, r.y + r.height / 2);

            } else {
                if (graph.isRightMarginRescaleDirty()) {
                    gg.fillRect(r.x, r.y, x1, r.y + r.height);
                    gg.drawLine(xOld, r.y, xOld, r.y + r.height);
                    if (xOld >= xMin) xOld = xMin - 1;
                    gg.drawLine(xOld, r.y + r.height / 2, r.x + r.width, r.y + r.height / 2);
                }
                gg.fillRect(r.x, r.y, x1, r.y + r.height);
                gg.drawLine(xOld, r.y, xOld, r.y + r.height);
                if (xNew >= xMin) xNew = xMin - 1;
                gg.drawLine(xNew, r.y + r.height / 2, r.x + r.width, r.y + r.height / 2);
            }
            graph.setRightMarginRescaleDirty(true);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void markZoomBox() {
        int x0 = (int) MouseDownPoint.getX();
        int y0 = (int) MouseDownPoint.getY();
        int xP = (int) PreMouseDragPoint.getX();
        int yP = (int) PreMouseDragPoint.getY();
        int xN = (int) MouseDragPoint.getX();
        int yN = (int) MouseDragPoint.getY();
        Graphics2D gg = (Graphics2D) graph.getGraphics();
//        gg.setClip((Rectangle)graph.panels.get(graph.getPanelIDatPoint(x0, y0)));
        gg.setXORMode(Color.GRAY);
//        gg.setColor(new Color(102,255,255));
        gg.setStroke(BS_1p0f_2by2_dashed);
//        gg.fillRect(Math.min(xP, x0), Math.min(yP, y0), Math.abs(xP - x0), Math.abs(yP - y0));
//        gg.fillRect(Math.min(xN, x0), Math.min(yN, y0), Math.abs(xN - x0), Math.abs(yN - y0));
        gg.drawRect(Math.min(xP, x0), Math.min(yP, y0), Math.abs(xP - x0), Math.abs(yP - y0));
        gg.drawRect(Math.min(xN, x0), Math.min(yN, y0), Math.abs(xN - x0), Math.abs(yN - y0));
        isBoxZooming = true;
    }

    //added by charithn
    /**
     * **********drawing move image before adding a new  object *******************
     */
    private void drawMoveImageForNewObject() {
        try {
            int x1, y1, x2, y2, x, y;
            int objectTobeDrawn;
            if (parent == null) {
                objectTobeDrawn = AbstractObject.INT_NONE;
            } else {
                objectTobeDrawn = parent.bottomToolBar.ObjectTobeDrawn;
            }

            Graphics2D gg = (Graphics2D) graph.getGraphics();
            gg.setXORMode(Color.gray);
            gg.setStroke(BS_1p0f_2by2_dashed);

            int x0, y0, xP, yP, xN, yN, xFirst, yFirst, xSecond, ySecond;

            x0 = (int) MouseDownPoint.getX();
            y0 = (int) MouseDownPoint.getY();

            xP = (int) PreCurrentMousePos.getX();
            yP = (int) PreCurrentMousePos.getY();

            xN = (int) CurrentMousePos.getX();
            yN = (int) CurrentMousePos.getY();

            xFirst = (int) firstMouseUpPoint.getX();
            yFirst = (int) firstMouseUpPoint.getY();

            xSecond = (int) secondMouseUpPoint.getX();
            ySecond = (int) secondMouseUpPoint.getY();

            int pnlID = 0;
            try {
                pnlID = graph.getPanelIDatPoint(x0, y0);
            } catch (Exception ex) {
                //ex.printStackTrace();
            }
            Rectangle r = (Rectangle) graph.panels.get(pnlID);
            // gg.setClip(r); //TODO:

            switch (objectTobeDrawn) {
                case AbstractObject.INT_FIBONACCI_EXTENSIONS:

                    if (noOfMousePressed == 1) {
                        gg.drawLine(xFirst, yFirst, xP, yP);
                        gg.drawLine(xFirst, yFirst, xN, yN);
                    }
                    if (noOfMousePressed == 2) {

                        gg.drawLine(xFirst, yFirst, xSecond, ySecond);
                        Hashtable lines = new ObjectFibonacciExtensions().getSavedLines();
                        gg.drawLine(xP, yP, x0, y0);
                        gg.drawLine(xN, yN, x0, y0);

                        if ((xP != x0)) {
                            int len = (ySecond - yFirst);
                            double maxPct = 0;
                            for (int i = ObjectFibonacciExtensions.ID_BEGIN; i < lines.size(); i++) {
                                FibonacciExtensionLine line = (FibonacciExtensionLine) lines.get(i);
                                if (line.isEnabled()) {
                                    double pecentage = line.getPercentageValue();
                                    y = yN + (int) Math.round(pecentage * len);
                                    gg.drawLine(xN, y, r.x + r.width, y);
                                    if (!(Double.isInfinite(pecentage) || Double.compare(Double.POSITIVE_INFINITY, pecentage) == 0
                                            || Double.compare(Double.NEGATIVE_INFINITY, pecentage) == 0 ||
                                            Double.compare(ObjectFibonacciExtensions.EXCLUDE_VALUE, pecentage) == 0)) {
                                        maxPct = Math.max(maxPct, pecentage);
                                    }
                                }
                            }
                            int yMax = yN + (int) Math.round(maxPct * len);
                            gg.drawLine(xN, yN, xN, yMax);
                        }
                        if ((xN != x0)) {
                            int len = (ySecond - yFirst);
                            double maxPct = 0;
                            for (int i = ObjectFibonacciExtensions.ID_BEGIN; i < lines.size(); i++) {
                                FibonacciExtensionLine line = (FibonacciExtensionLine) lines.get(i);
                                if (line.isEnabled()) {
                                    double pecentage = line.getPercentageValue();
                                    y = yN + (int) Math.round(pecentage * len);
                                    gg.drawLine(xN, y, r.x + r.width, y);
                                    if (!(Double.isInfinite(pecentage) || Double.compare(Double.POSITIVE_INFINITY, pecentage) == 0
                                            || Double.compare(Double.NEGATIVE_INFINITY, pecentage) == 0 ||
                                            Double.compare(ObjectFibonacciExtensions.EXCLUDE_VALUE, pecentage) == 0)) {
                                        maxPct = Math.max(maxPct, pecentage);
                                    }
                                }
                            }
                            int yMax = yN + (int) Math.round(maxPct * len);
                            gg.drawLine(xN, yN, xN, yMax);
                        }
                    }
                case AbstractObject.INT_ZIG_ZAG:

                    if (xPoints.size() == 0) {
                        return;
                    }
                    int[] xx = new int[xPoints.size()];
                    int[] yy = new int[yPoints.size()];
                    for (int i = 0; i < xPoints.size(); i++) {
                        xx[i] = Integer.parseInt(String.valueOf(xPoints.get(i)));
                        yy[i] = Integer.parseInt(String.valueOf(yPoints.get(i)));
                    }

                    //gg.drawPolyline(xx, yy, xx.length);

                    gg.drawLine(xx[xx.length - 1], yy[yy.length - 1], xP, yP);
                    gg.drawLine(xx[xx.length - 1], yy[yy.length - 1], xN, yN);
                    break;
                case AbstractObject.INT_ARC:

                    if (noOfMousePressed == 2) {
                        QuadCurve2D q = new QuadCurve2D.Float();
//                        q.setCurve(xFirst, yFirst, xSecond, ySecond, xP, yP);
                        q.setCurve(xFirst, yFirst, xP, yP, xSecond, ySecond);
                        gg.draw(q);
//                        q.setCurve(xFirst, yFirst, xSecond, ySecond, xN, yN);
                        q.setCurve(xFirst, yFirst, xN, yN, xSecond, ySecond);
//                        gg.draw(q);
                        gg.draw(q);
                    }
                    break;
                case AbstractObject.INT_ANDREWS_PITCHFORK:
                    int len = 5;
                    gg.setPaintMode();
                    gg.setColor(Color.BLUE);
                    gg.setStroke(new BasicStroke(1f));
                    if (noOfMousePressed == 1) {

                        gg.drawLine(xFirst - len, yFirst, xFirst + len, yFirst);
                        gg.drawLine(xFirst, yFirst - len, xFirst, yFirst + len);
                    } else if (noOfMousePressed == 2) {
                        gg.drawLine(xFirst - len, yFirst, xFirst + len, yFirst);
                        gg.drawLine(xFirst, yFirst - len, xFirst, yFirst + len);

                        gg.drawLine(xSecond - len, ySecond, xSecond + len, ySecond);
                        gg.drawLine(xSecond, ySecond - len, xSecond, ySecond + len);
                    }
                    gg.setPaintMode();
                    break;

            }
        } catch (Exception ex) {
            //System.out.println("====== exception in drawMoveImageForNewObject() ======= ");
        }
    }

    /**
     * **********drawing drag image before adding new  obj3ect *******************
     */
    private void drawDragImageForNewObject() {
        int ObjectTobeDrawn;
        int x1, y1, x2, y2, x, y;
        if (parent == null) {
            ObjectTobeDrawn = AbstractObject.INT_NONE;
        } else {
            ObjectTobeDrawn = parent.bottomToolBar.ObjectTobeDrawn;
        }
        //MouseDragPoint.setLocation(e.getX(), e.getY());    // Modified 25-03-2006-Udaka (line commented)
        Graphics2D gg = (Graphics2D) graph.getGraphics();

        gg.setXORMode(Color.gray);
        gg.setStroke(BS_1p0f_2by2_dashed);
        int x0, y0, xP, yP, xN, yN;
        x0 = (int) MouseDownPoint.getX();
        y0 = (int) MouseDownPoint.getY();
        xP = (int) PreMouseDragPoint.getX();
        yP = (int) PreMouseDragPoint.getY();
        xN = (int) MouseDragPoint.getX();
        yN = (int) MouseDragPoint.getY();
        int pnlID = graph.getPanelIDatPoint(x0, y0);
        Rectangle r = (Rectangle) graph.panels.get(pnlID);

        gg.setClip(r);
        switch (ObjectTobeDrawn) {

            case AbstractObject.INT_LINE_HORIZ:

                gg.drawLine(r.x, yP, r.x + r.width, yP);
                gg.drawLine(r.x, yN, r.x + r.width, yN);

                break;
            case AbstractObject.INT_LINE_VERTI:

                for (int i = 0; i < graph.panels.size(); i++) {
                    WindowPanel rect = (WindowPanel) graph.panels.get(i);
                    gg.setClip(rect);
                    gg.drawLine(xP, rect.y + StockGraph.titleHeight, xP, rect.y + rect.height);
                    gg.drawLine(xN, rect.y + StockGraph.titleHeight, xN, rect.y + rect.height);
                }
                break;
            case AbstractObject.INT_DATA_LINE_VERTI:

                if (dwTableListener != null && parent != null && parent.getDataWindow() != null && !parent.getDataWindow().isWindowMode()) {
                    parent.getDataWindow().removeSelectionListener();
                    //dwTableListener.dataPointChangedOnGraph(e, CurrentMousePos, graph);
                }

                BasicStroke BS_1p0f_2by2_dashed2 = new BasicStroke(2.0f, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 1.0f, dashArr, 0.0f);
                gg.setStroke(BS_1p0f_2by2_dashed2);
                for (int i = 0; i < graph.panels.size(); i++) {
                    WindowPanel rect = (WindowPanel) graph.panels.get(i);
                    gg.drawLine(xP, rect.y + StockGraph.titleHeight, xP, rect.y + rect.height);
                    gg.drawLine(xN, rect.y + StockGraph.titleHeight, xN, rect.y + rect.height);
                }
                break;
            case AbstractObject.INT_REGRESSION:
                x0 = (int) Math.round(graph.GDMgr.getActualPoint2D(MouseDownPoint, pnlID).x);
                xP = (int) Math.round(graph.GDMgr.getActualPoint2D(PreMouseDragPoint, pnlID).x);
                xN = (int) Math.round(graph.GDMgr.getActualPoint2D(MouseDragPoint, pnlID).x);
                ObjectRegression objectRegression = new ObjectRegression();
                int[] faX0 = new int[2];
                int[] faY0 = new int[2];
                if (ObjectRegression.calculateRegressionLine(graph, Math.min(x0, xP), Math.max(x0, xP), faX0, faY0,
                        objectRegression.getOHLCPriority(), pnlID, true)) {
                    gg.drawLine(faX0[0], faY0[0], faX0[1], faY0[1]);
                    gg.drawLine(faX0[0], r.y + StockGraph.titleHeight, faX0[0], r.y + r.height);
                    gg.drawLine(faX0[1], r.y + StockGraph.titleHeight, faX0[1], r.y + r.height);
                }
                if (ObjectRegression.calculateRegressionLine(graph, Math.min(x0, xN), Math.max(x0, xN), faX0, faY0,
                        objectRegression.getOHLCPriority(), pnlID, true)) {
                    gg.drawLine(faX0[0], faY0[0], faX0[1], faY0[1]);
                    gg.drawLine(faX0[0], r.y + StockGraph.titleHeight, faX0[0], r.y + r.height);
                    gg.drawLine(faX0[1], r.y + StockGraph.titleHeight, faX0[1], r.y + r.height);
                }
                break;
            case AbstractObject.INT_STD_ERROR_CHANNEL:
                int xEnd = graph.GDMgr.getPixelFortheIndex(graph.GDMgr.getLastNotNullIndexOfGraphStore());
                Point2D.Float endPoint;
                if (MouseDragPoint.getX() > xEnd) {
                    endPoint = new Point2D.Float(xEnd, 0);
                } else {
                    endPoint = MouseDragPoint;
                }
                x0 = (int) Math.round(graph.GDMgr.getActualPoint2D(MouseDownPoint, pnlID).x);
                xP = (int) Math.round(graph.GDMgr.getActualPoint2D(PreMouseDragPoint, pnlID).x);
                xN = (int) Math.round(graph.GDMgr.getActualPoint2D(endPoint, pnlID).x);
                ObjectStandardError objectStandardError = new ObjectStandardError();
                int[] iaX = new int[2];
                int[] iaY = new int[2];
                double[] maxDistance = new double[1];
                maxDistance[0] = 0d;
                if (ObjectStandardError.calculateStandardError(graph, Math.min(x0, xP), Math.max(x0, xP), iaX, iaY,
                        maxDistance, r, objectStandardError.getOHLCPriority())) {
                    int maxDistPix = graph.GDMgr.getPixelGapFortheYValueGap(
                            objectStandardError.getUnits() * maxDistance[0], pnlID);
                    gg.drawLine(iaX[0], iaY[0], iaX[1], iaY[1]);
                    gg.drawLine(iaX[0], iaY[0] + maxDistPix, iaX[1], iaY[1] + maxDistPix);
                    gg.drawLine(iaX[0], iaY[0] - maxDistPix, iaX[1], iaY[1] - maxDistPix);
                    gg.drawLine(iaX[0], r.y + GraphDataManager.titleHeight, iaX[0], r.y + r.height);
                    gg.drawLine(iaX[1], r.y + GraphDataManager.titleHeight, iaX[1], r.y + r.height);
                }
                if (ObjectStandardError.calculateStandardError(graph, Math.min(x0, xN), Math.max(x0, xN), iaX, iaY,
                        maxDistance, r, objectStandardError.getOHLCPriority())) {
                    int maxDistPix = graph.GDMgr.getPixelGapFortheYValueGap(
                            objectStandardError.getUnits() * maxDistance[0], pnlID);
                    gg.drawLine(iaX[0], iaY[0], iaX[1], iaY[1]);
                    gg.drawLine(iaX[0], iaY[0] + maxDistPix, iaX[1], iaY[1] + maxDistPix);
                    gg.drawLine(iaX[0], iaY[0] - maxDistPix, iaX[1], iaY[1] - maxDistPix);
                    gg.drawLine(iaX[0], r.y + GraphDataManager.titleHeight, iaX[0], r.y + r.height);
                    gg.drawLine(iaX[1], r.y + GraphDataManager.titleHeight, iaX[1], r.y + r.height);
                }
                break;
            case AbstractObject.INT_RAFF_REGRESSION:
                x0 = (int) Math.round(graph.GDMgr.getActualPoint2D(MouseDownPoint, pnlID).x);
                xP = (int) Math.round(graph.GDMgr.getActualPoint2D(PreMouseDragPoint, pnlID).x);
                xN = (int) Math.round(graph.GDMgr.getActualPoint2D(MouseDragPoint, pnlID).x);
                ObjectRaffRegression objectRaffRegression = new ObjectRaffRegression();
                iaX = new int[2];
                iaY = new int[2];
                maxDistance = new double[1];
                maxDistance[0] = 0d;
                if (ObjectRaffRegression.calculateRaffRegression(graph, Math.min(x0, xP), Math.max(x0, xP), iaX, iaY,
                        maxDistance, r, objectRaffRegression.getOHLCPriority())) {
                    int maxDistPix = graph.GDMgr.getPixelGapFortheYValueGap(maxDistance[0], pnlID);
                    gg.drawLine(iaX[0], iaY[0], iaX[1], iaY[1]);
                    gg.drawLine(iaX[0], iaY[0] + maxDistPix, iaX[1], iaY[1] + maxDistPix);
                    gg.drawLine(iaX[0], iaY[0] - maxDistPix, iaX[1], iaY[1] - maxDistPix);
                    gg.drawLine(iaX[0], r.y + GraphDataManager.titleHeight, iaX[0], r.y + r.height);
                    gg.drawLine(iaX[1], r.y + GraphDataManager.titleHeight, iaX[1], r.y + r.height);
                }
                if (ObjectRaffRegression.calculateRaffRegression(graph, Math.min(x0, xN), Math.max(x0, xN), iaX, iaY,
                        maxDistance, r, objectRaffRegression.getOHLCPriority())) {
                    int maxDistPix = graph.GDMgr.getPixelGapFortheYValueGap(maxDistance[0], pnlID);
                    gg.drawLine(iaX[0], iaY[0], iaX[1], iaY[1]);
                    gg.drawLine(iaX[0], iaY[0] + maxDistPix, iaX[1], iaY[1] + maxDistPix);
                    gg.drawLine(iaX[0], iaY[0] - maxDistPix, iaX[1], iaY[1] - maxDistPix);
                    gg.drawLine(iaX[0], r.y + GraphDataManager.titleHeight, iaX[0], r.y + r.height);
                    gg.drawLine(iaX[1], r.y + GraphDataManager.titleHeight, iaX[1], r.y + r.height);
                }
                break;
            case AbstractObject.INT_STD_DEV_CHANNEL:
                x0 = (int) Math.round(graph.GDMgr.getActualPoint2D(MouseDownPoint, pnlID).x);
                xP = (int) Math.round(graph.GDMgr.getActualPoint2D(PreMouseDragPoint, pnlID).x);
                xN = (int) Math.round(graph.GDMgr.getActualPoint2D(MouseDragPoint, pnlID).x);
                ObjectStandardDeviationChannel objectStandardDeviationChannel = new ObjectStandardDeviationChannel();
                iaX = new int[2];
                iaY = new int[2];
                maxDistance = new double[1];
                maxDistance[0] = 0d;
                if (ObjectStandardDeviationChannel.calculateStandardDeviation(graph, Math.min(x0, xP), Math.max(x0, xP), iaX, iaY,
                        maxDistance, r, objectStandardDeviationChannel.getOHLCPriority())) {
                    int maxDistPix = Math.round(objectStandardDeviationChannel.getUnits() *
                            graph.GDMgr.getPixelGapFortheYValueGap(maxDistance[0], pnlID));
                    gg.drawLine(iaX[0], iaY[0], iaX[1], iaY[1]);
                    gg.drawLine(iaX[0], iaY[0] + maxDistPix, iaX[1], iaY[1] + maxDistPix);
                    gg.drawLine(iaX[0], iaY[0] - maxDistPix, iaX[1], iaY[1] - maxDistPix);
                    gg.drawLine(iaX[0], r.y + GraphDataManager.titleHeight, iaX[0], r.y + r.height);
                    gg.drawLine(iaX[1], r.y + GraphDataManager.titleHeight, iaX[1], r.y + r.height);
                }
                if (ObjectStandardDeviationChannel.calculateStandardDeviation(graph, Math.min(x0, xN), Math.max(x0, xN), iaX, iaY,
                        maxDistance, r, objectStandardDeviationChannel.getOHLCPriority())) {
                    int maxDistPix = Math.round(objectStandardDeviationChannel.getUnits() *
                            graph.GDMgr.getPixelGapFortheYValueGap(maxDistance[0], pnlID));
                    gg.drawLine(iaX[0], iaY[0], iaX[1], iaY[1]);
                    gg.drawLine(iaX[0], iaY[0] + maxDistPix, iaX[1], iaY[1] + maxDistPix);
                    gg.drawLine(iaX[0], iaY[0] - maxDistPix, iaX[1], iaY[1] - maxDistPix);
                    gg.drawLine(iaX[0], r.y + GraphDataManager.titleHeight, iaX[0], r.y + r.height);
                    gg.drawLine(iaX[1], r.y + GraphDataManager.titleHeight, iaX[1], r.y + r.height);
                }
                break;
            case AbstractObject.INT_EQUI_DIST_CHANNEL:
                int maxDistPix = ObjectEquiDistantChannel.INITIAL_DISTANCE;
                gg.drawLine(xP, yP, x0, y0);
                gg.drawLine(xP, yP + maxDistPix, x0, y0 + maxDistPix);
                gg.drawLine(xN, yN, x0, y0);
                gg.drawLine(xN, yN + maxDistPix, x0, y0 + maxDistPix);
                break;
            case AbstractObject.INT_LINE_SLOPE:

                if ((graph.GDMgr.isSnapToPrice()) && (pnlID == graph.getIDforPanel(graph.GDMgr.getBaseCP().getRect()))) {
                    int[] closePixArr = getSnapYPixValues(pnlID);

                    int YpOHLC, y0OHLC, yNOHLC;
                    y0OHLC = closePixArr[0]; //mouse down
                    yNOHLC = closePixArr[1];//drag
                    YpOHLC = closePixArr[2];//pre drag

                    //then draw the line
                    gg.drawLine(xP, YpOHLC, x0, y0OHLC);   //mouse down point to  premouse drag point
                    gg.drawLine(xN, yNOHLC, x0, y0OHLC);   //mouse down point to  mouse drag point
                } else {
                    gg.drawLine(xP, yP, x0, y0);
                    gg.drawLine(xN, yN, x0, y0);
                }


                break;
            case AbstractObject.INT_ARROW_LINE_SLOPE:

//                if ((graph.GDMgr.isSnapToPrice()) && (pnlID == graph.getIDforPanel(graph.GDMgr.getBaseCP().getRect()))) {
//                    //TODO : ALS - need snap to price .discard if doesnet need
//                    int[] closePixArr = getSnapYPixValues(pnlID);
//
//                    int YpOHLC, y0OHLC, yNOHLC;
//                    y0OHLC = closePixArr[0]; //mouse down
//                    yNOHLC = closePixArr[1];//drag
//                    YpOHLC = closePixArr[2];//pre drag
//
//                    //then draw the line
//                    gg.drawLine(xP, YpOHLC, x0, y0OHLC);   //mouse down point to  premouse drag point
//                    gg.drawLine(xN, yNOHLC, x0, y0OHLC);   //mouse down point to  mouse drag point
//                } else {
                gg.drawLine(xP, yP, x0, y0);
                gg.drawLine(xN, yN, x0, y0);
//                }


                break;
            case AbstractObject.INT_TEXT:
            case AbstractObject.INT_RECT:
                gg.drawRect(Math.min(xP, x0), Math.min(yP, y0), Math.abs(xP - x0), Math.abs(yP - y0));
                gg.drawRect(Math.min(xN, x0), Math.min(yN, y0), Math.abs(xN - x0), Math.abs(yN - y0));
                break;
            case AbstractObject.INT_RECT_SELECTION:
                gg.setStroke(PropertyDialogFactory.getBasicStroke((byte) PropertyDialogFactory.PEN_DOTTED, 2.0f));
                gg.drawRect(Math.min(xP, x0), Math.min(yP, y0), Math.abs(xP - x0), Math.abs(yP - y0));
                gg.drawRect(Math.min(xN, x0), Math.min(yN, y0), Math.abs(xN - x0), Math.abs(yN - y0));
                break;
            case AbstractObject.INT_SYMBOL:
                gg.drawRect(xP - 10, yP - 10, 20, 20);
                gg.drawRect(xN - 10, yN - 10, 20, 20);
                break;
            case AbstractObject.INT_ELLIPSE:
                gg.drawOval(Math.min(xP, x0), Math.min(yP, y0), Math.abs(xP - x0), Math.abs(yP - y0));
                gg.drawOval(Math.min(xN, x0), Math.min(yN, y0), Math.abs(xN - x0), Math.abs(yN - y0));
                break;
            case AbstractObject.INT_FIBONACCI_ARCS:
                gg.drawLine(xP, yP, x0, y0);
                gg.drawLine(xN, yN, x0, y0);
                float len;
                int r1, r2, r3, start;
                len = (float) Math.sqrt((x0 - xP) * (x0 - xP) + (y0 - yP) * (y0 - yP));
                r1 = Math.round(len * 0.618f);
                r2 = Math.round(len * 0.5f);
                r3 = Math.round(len * 0.382f);
                start = (yP > y0) ? 0 : 180;
                gg.drawArc(xP - r1, yP - r1, 2 * r1, 2 * r1, start, 180);
                gg.drawArc(xP - r2, yP - r2, 2 * r2, 2 * r2, start, 180);
                gg.drawArc(xP - r3, yP - r3, 2 * r3, 2 * r3, start, 180);

                len = (float) Math.sqrt((x0 - xN) * (x0 - xN) + (y0 - yN) * (y0 - yN));
                r1 = Math.round(len * 0.618f);
                r2 = Math.round(len * 0.5f);
                r3 = Math.round(len * 0.382f);
                start = (yN > y0) ? 0 : 180;
                gg.drawArc(xN - r1, yN - r1, 2 * r1, 2 * r1, start, 180);
                gg.drawArc(xN - r2, yN - r2, 2 * r2, 2 * r2, start, 180);
                gg.drawArc(xN - r3, yN - r3, 2 * r3, 2 * r3, start, 180);
                break;
            case AbstractObject.INT_FIBONACCI_FANS:
                gg.setClip(r.x, r.y + GraphDataManager.titleHeight, r.width, r.height - GraphDataManager.titleHeight);
                gg.drawLine(xP, yP, x0, y0);
                gg.drawLine(xN, yN, x0, y0);
                float ratio = 1f;
                int yP1, yP2, yP3;
                //for old point
                if (xP != x0) {
                    if (xP < x0) {
                        x1 = xP;
                        y1 = yP;
                    } else {
                        x1 = x0;
                        y1 = y0;
                    }
                    ratio = (float) (yP - y0) / ((float) xP - x0);
                    x = x1 + r.x + r.width;
                    y = y1 + Math.round(ratio * (r.x + r.width));
                    yP1 = Math.round(0.618f * y1 + 0.382f * y);
                    yP2 = Math.round(0.5f * y1 + 0.5f * y);
                    yP3 = Math.round(0.382f * y1 + 0.618f * y);
                    gg.drawLine(x1, y1, x, yP1);
                    gg.drawLine(x1, y1, x, yP2);
                    gg.drawLine(x1, y1, x, yP3);
                }
                //for new point
                if (xN != x0) {
                    if (xN < x0) {
                        x1 = xN;
                        y1 = yN;
                    } else {
                        x1 = x0;
                        y1 = y0;
                    }
                    ratio = (float) (yN - y0) / ((float) xN - x0);
                    x = x1 + r.x + r.width;
                    y = y1 + Math.round(ratio * (r.x + r.width));
                    yP1 = Math.round(0.618f * y1 + 0.382f * y);
                    yP2 = Math.round(0.5f * y1 + 0.5f * y);
                    yP3 = Math.round(0.382f * y1 + 0.618f * y);
                    gg.drawLine(x1, y1, x, yP1);
                    gg.drawLine(x1, y1, x, yP2);
                    gg.drawLine(x1, y1, x, yP3);
                }
                break;
            case AbstractObject.INT_FIBONACCI_RETRACEMENTS:

                //added charithn- get saved list if user has saved or get the default
                ObjectFibonacciRetracements ofr = new ObjectFibonacciRetracements();
                Hashtable fibonacciLines = ofr.getSavedLines();

                //float[] fibArr = ObjectFibonacciRetracements.defaultLevels;
                gg.drawLine(xP, yP, x0, y0);
                gg.drawLine(xN, yN, x0, y0);
                if ((xP != x0)) {
                    if (xP < x0) {
                        x1 = xP;
                        y1 = yP;
                        y2 = y0;
                    } else {
                        x1 = x0;
                        y1 = y0;
                        y2 = yP;
                    }
                    len = (y2 - y1);
                    /*for (int i = 0; i < fibArr.length; i++) {
                        y = y2 - Math.round(fibArr[i] * len);
                        gg.drawLine(x1, y, r.x + r.width, y);
                    }*/
                    for (int i = ObjectFibonacciRetracements.ID_BEGIN; i < fibonacciLines.size(); i++) {

                        FibonacciRetracementiLine line = (FibonacciRetracementiLine) fibonacciLines.get(i);
                        if (line.isEnabled()) {
                            y = y2 - (int) Math.round(line.getPercentageValue() * len);
                            gg.drawLine(x1, y, r.x + r.width, y);
                        }
                    }
                    gg.drawLine(x1, y1, r.x + r.width, y1);
                    gg.drawLine(x1, y2, r.x + r.width, y2);
                }
                if ((xN != x0)) {
                    if (xN < x0) {
                        x1 = xN;
                        y1 = yN;
                        y2 = y0;
                    } else {
                        x1 = x0;
                        y1 = y0;
                        y2 = yN;
                    }
                    len = (y2 - y1);
                    /*for (int i = 0; i < fibArr.length; i++) {
                        y = y2 - Math.round(fibArr[i] * len);
                        gg.drawLine(x1, y, x1 + r.x + r.width, y);
                    }*/
                    for (int i = ObjectFibonacciRetracements.ID_BEGIN; i < fibonacciLines.size(); i++) {

                        FibonacciRetracementiLine line = (FibonacciRetracementiLine) fibonacciLines.get(i);
                        if (line.isEnabled()) {
                            y = y2 - (int) Math.round(line.getPercentageValue() * len);
                            gg.drawLine(x1, y, x1 + r.x + r.width, y);
                        }
                    }
                    gg.drawLine(x1, y1, x1 + r.x + r.width, y1);
                    gg.drawLine(x1, y2, x1 + r.x + r.width, y2);
                }
                break;
            case AbstractObject.INT_FIBONACCI_ZONES:
                for (int i = 0; i < graph.panels.size(); i++) {
                    WindowPanel rect = (WindowPanel) graph.panels.get(i);
                    y1 = rect.y + StockGraph.titleHeight;
                    y2 = rect.y + rect.height;
                    gg.drawLine(xP, y1, xP, y2);
                    gg.drawLine(xN, y1, xN, y2);
                    int preVal = 0;
                    int curVal = 1;
                    int tmpVal = 0;
                    int sum = 0;
                    while (true) {
                        sum += curVal;
                        x = xP + Math.round(sum * graph.GDMgr.xFactor);
                        if (graph.GDMgr.xFactor <= 0.0)
                            break;//addded by sathyajith to avoid  unended looping for no data charts
                        if (x > rect.x + rect.width) break;
                        gg.drawLine(x, y1, x, y2);
                        tmpVal = curVal;
                        curVal += preVal;
                        preVal = tmpVal;
                    }
                    preVal = 0;
                    curVal = 1;
                    tmpVal = 0;
                    sum = 0;
                    while (true) {
                        sum += curVal;
                        x = xN + Math.round(sum * graph.GDMgr.xFactor);
                        if (graph.GDMgr.xFactor <= 0.0)
                            break;//addded by sathyajith to avoid  unended looping for no data charts
                        if (x > rect.x + rect.width) break;
                        gg.drawLine(x, y1, x, y2);
                        tmpVal = curVal;
                        curVal += preVal;
                        preVal = tmpVal;
                    }
                }
                break;
            case AbstractObject.INT_GANN_LINE:
                x1 = x0;
                y1 = y0;
                x2 = x1 + 100;
                y2 = y1 - 100;
                int x11 = x1 - (x0 - xP);
                int y11 = y1 - (y0 - yP);
                gg.setClip(r.x, r.y + GraphDataManager.titleHeight, r.width, r.height - GraphDataManager.titleHeight);

                int tempX2, tempY2;
                tempX2 = x11 + r.width;
                tempY2 = y11 - r.width;
                gg.drawLine(x11, y11, tempX2, tempY2);

                int x12 = x1 - (x0 - xN);
                int y12 = y1 - (y0 - yN);
                tempX2 = x12 + r.width;
                tempY2 = y12 - r.width;
                gg.drawLine(x12, y12, tempX2, tempY2);
                break;
            case AbstractObject.INT_GANN_FAN:
                x1 = x0;
                y1 = y0;
                x2 = x1 + 100;
                y2 = y1 - 100;
                x11 = x1 - (x0 - xP);
                y11 = y1 - (y0 - yP);
                gg.setClip(r.x, r.y + GraphDataManager.titleHeight, r.width, r.height - GraphDataManager.titleHeight);

                tempX2 = x11 + r.width;
                tempY2 = y11 - r.width;
                gg.drawLine(x11, y11, tempX2, tempY2);
                for (int i = 0; i < ObjectGannFan.GANN_ANGLES.length; i++) {
                    tempY2 = y11 - Math.round(ObjectGannFan.GANN_ANGLES[i] * r.width);
                    gg.drawLine(x11, y11, tempX2, tempY2);
                }

                x12 = x1 - (x0 - xN);
                y12 = y1 - (y0 - yN);
                tempX2 = x12 + r.width;
                tempY2 = y12 - r.width;
                gg.drawLine(x12, y12, tempX2, tempY2);
                for (int i = 0; i < ObjectGannFan.GANN_ANGLES.length; i++) {
                    tempY2 = y12 - Math.round(ObjectGannFan.GANN_ANGLES[i] * r.width);
                    gg.drawLine(x12, y12, tempX2, tempY2);
                }
                break;
            case AbstractObject.INT_GANN_GRID:
                x1 = x0;
                y1 = y0;
                x2 = x1 + 100;
                y2 = y1 - 100;
                x11 = x1 - (x0 - xP);
                y11 = y1 - (y0 - yP);
                gg.setClip(r.x, r.y + GraphDataManager.titleHeight, r.width, r.height - GraphDataManager.titleHeight);

                int[] pointCount = new int[1];
                pointCount[0] = 1;
                Point startPt = ObjectGannGrid.getGridDimensions(r, x11, y11, 100, 100, pointCount);
                int xt, yt, xb, yb;
                int yGapCount = r.height / 100 + 2;
                yt = startPt.y;
                yb = yt + 100 * yGapCount;
                for (int i = 0; i < pointCount[0]; i++) {
                    xt = startPt.x + i * 100;
                    xb = xt + yGapCount * 100;
                    gg.drawLine(xt, yt, xb, yb);
                    gg.drawLine(xb, yt, xt, yb);
                }

                x12 = x1 - (x0 - xN);
                y12 = y1 - (y0 - yN);
                pointCount[0] = 1;
                startPt = ObjectGannGrid.getGridDimensions(r, x12, y12, 100, 100, pointCount);
                yt = startPt.y;
                yb = yt + 100 * yGapCount;
                for (int i = 0; i < pointCount[0]; i++) {
                    xt = startPt.x + i * 100;
                    xb = xt + yGapCount * 100;
                    gg.drawLine(xt, yt, xb, yb);
                    gg.drawLine(xb, yt, xt, yb);
                }
                break;
        }
        //PreMouseDragPoint.setLocation(MouseDragPoint);     // Modified 25-03-2006-Udaka (line commented)
    }

    private int[] getSnapYPixValues(int pnlID) {
        Point2D.Double mdPt = graph.GDMgr.getActualPoint2D(MouseDownPoint, pnlID);//converting pixel to the actual points
        Point2D.Double mDragPt = graph.GDMgr.getActualPoint2D(MouseDragPoint, pnlID);
        Point2D.Double mPreDragPt = graph.GDMgr.getActualPoint2D(PreMouseDragPoint, pnlID);

        if ((mDragPt.x < 0) || (mDragPt.x > graph.GDMgr.getGraphStoreSize() - 1)) {
            mDragPt = getAdjustedMouseUpPoint(mdPt, mDragPt);
        }
        if ((mPreDragPt.x < 0) || (mPreDragPt.x > graph.GDMgr.getGraphStoreSize() - 1)) {
            mPreDragPt = getAdjustedMouseUpPoint(mdPt, mPreDragPt);
        }

        float[] faXSlope = {(float) mdPt.x, (float) mDragPt.x, (float) mPreDragPt.x}; //indexes

        /*************************automatic snap to price ************************/
        double[] faYActualPrice = {(float) mdPt.y, (float) mDragPt.y, (float) mPreDragPt.y}; //indexes ;
        double[] faYRelOHLc;
        if (graph.GDMgr.getSnapToPricePriority() == 0) { //automatic

            faYRelOHLc = graph.GDMgr.convertXArrayIndexToNearestOHLC(faXSlope, faYActualPrice);
        } else {
            faYRelOHLc = graph.GDMgr.convertXArrayIndexToSnapSelectedOHLC(faXSlope);
        }
        /*************************automatic snap to price ************************/

        //get pixel values for the close value
        int[] ohlcPixArr = graph.GDMgr.convertYValueArrayToPixelArr(faYRelOHLc, pnlID);
        return ohlcPixArr;
    }

    public boolean getZoomDragEnabled() {
        return ZoomDragEnabled;
    }

    public void setDragZoomEnabled(boolean enabled) {
        ZoomDragEnabled = enabled;
    }

    public boolean isZoomDragEnabled() {
        return ZoomDragEnabled;
    }

    public void setBoxZoomEnabled(boolean enabled) {
        boxZoomEnabled = enabled;
    }

    public void setZoomInEnabled(boolean enabled) {
        zoomInEnabled = enabled;
    }

    public boolean isBoxZoomEnabled() {
        return boxZoomEnabled;
    }

    public boolean isZoomOutEnabled() {
        return zoomOutEnabled;
    }

    public void setZoomOutEnabled(boolean enabled) {
        zoomOutEnabled = enabled;
    }

    public boolean isZoomInEnabled() {
        return zoomInEnabled;
    }

    public boolean isChartBodyDraggingEnabled() {
        return chartBodyDraggingEnabled;
    }

    public void setchartBodyDraggingEnabled(boolean enabled) {
        this.chartBodyDraggingEnabled = enabled;
    }

    public void setFibPriceCalcEnabled(boolean enabled) {
        this.isFibCalMode = enabled;
    }

    public boolean isFibPriceCalcEnabled() {
        return isFibCalMode;
    }

    public void setRectangularSelection(boolean mode) {
        this.isRectangularSelection = mode;
    }

    public boolean isRectangularSelection() {
        return isRectangularSelection;
    }

    public void setRFibCalcualtorMode(boolean mode) {
        this.isFibCalMode = mode;
    }

    public void setErasorMode(boolean mode) {
        this.isErasorMode = mode;
    }

    public void setDesktopIndex(int index) {
        windowIndex = index;
    }

    public int getDesktopIndex() {
        return windowIndex;
    }

    public void closeWindow() {

        setVisible(false);
        try {
            JDesktopPane pane = this.getDesktopPane();
            if (pane != null) {

                pane.remove(this);
//                dispose();
                pane.updateUI();
            }

            if ((pane != null) && !(pane instanceof com.isi.csvr.TWDesktop)) {
                if (!ChartFrame.getSharedInstance().isBIsTabbedMode()) {
                    //ChartFrame.getSharedInstance().tileWindows();
                }
                ChartFrame.getSharedInstance().tileWindows();
                if (ChartFrame.getSharedInstance().isInTabbedPanel(this)) {
                    removeWindowtab(this);
                }
            } else {
                focusNextWindow();
            }
            pane = null;

            JInternalFrame[] frames = ChartFrame.getSharedInstance().getDesktop().getAllFrames();
            if (frames.length == 1 && frames[0] instanceof DataWindow) {
                DataWindow win = parent.getDataWindow();
                if (win != null) {
                    win.dispose();
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        if ((parent != null) && (parent.getActiveGraph() == this)) {
            JInternalFrame[] frames = parent.getDesktop().getAllFrames();
            parent.setActiveGraph(null);
            for (int i = 0; i < frames.length; i++) {
                if (frames[i] instanceof GraphFrame) {
                    parent.setActiveGraph((GraphFrame) frames[i]);
                    break;
                }
            }

        }
        if (parent != null && parent.getActiveGraph() == null) {
            JInternalFrame[] frames = parent.getDesktop().getAllFrames();
            for (int i = 0; i < frames.length; i++) {
                frames[i].doDefaultCloseAction();
            }
        } else {
            if (ChartFrame.getSharedInstance().getDataWindow() != null) {
                DataWindow window = ChartFrame.getSharedInstance().getDataWindow();
                if (!window.isWindowMode() && window.isVisible()) {
                    window.setSnapToDate(false);
                    window.setDragMode(false);
                    window.getBtnDataModeImaginary().doClick();
                }
            }
        }

        dataTable.killThread();
        graph.GDMgr.killThread();
        graph.killThread();

        TradingConnectionNotifier.getInstance().removeConnectionListener(graph.GDMgr);
        OrderStore.getSharedInstance().removeOrderStatusListener(graph.GDMgr);

        ChartLinkStore.getSharedInstance().removeFromStore(this, linkID);

        //chart count
        ChartFrame cf = ChartFrame.getSharedInstance();
        cf.setTotalChartCount(cf.getTotalChartCount() - 1);
        if (this.parent instanceof ChartFrame) { //if the chart is in pro chart
            cf.setProChartCount(cf.getProChartCount() - 1);
        }
    }

    public void doDefaultCloseAction() {
        closeWindow();
    }

    private void focusNextWindow() {
        TWDesktop desktop = (TWDesktop) Client.getInstance().getDesktop();
        desktop.focusNextWindow(desktop.getListPosition(this, true), true);
        desktop = null;
    }

    public int getDesktopItemType() {
        return POPUP_TYPE;
    }

    public void setWindowType(int type) {
        windowType = type;
    }

    public int getWindowType() {
        return windowType;
    }

    public void newChartClicked() {
        if (parent != null) {
            if (parent.getTotalChartCount() >= parent.getMaxChartCount()) {
                String message = "Graph limit exceeded. Maximum %s graphs allowed.";
                message = String.format(message, parent.getMaxChartCount());
                JOptionPane.showConfirmDialog(parent, message, "Error", JOptionPane.DEFAULT_OPTION, JOptionPane.ERROR_MESSAGE);
            } else {
                parent.addNewGraph(true, false);
                parent.tileWindows();
            }
        }
    }

    public void newLayoutClicked() {
        if (parent != null) {
            parent.createLayout();
        }
    }

    //changed by charithn
    public void saveClicked(boolean isSaveAs) {

        if ((fileName == null) || isSaveAs)
            fileName = saveChartFileAs();
        if (fileName == null) return;

        File f = new File(fileName);
        try {
            if (fileName.endsWith(".cht")) {
                LayoutFactory.saveChtFile(fileName, this);
            } else if (isValidImageExtension(fileName)) {
                saveAsImage(fileName);
                fileName = null;
            } else {  //template or layout
                GraphFrame[] gfs;
                boolean isTemplate = fileName.endsWith(".mct");
                if (alLayuotIDs.size() > 0) {
                    ChartFrame cf = ChartFrame.getSharedInstance();
                    String uniqueID = alLayuotIDs.get(alLayuotIDs.size() - 1).toString();
                    Vector graphs = cf.getLayoutForID(uniqueID);
                    gfs = new GraphFrame[graphs.size()];
                    for (int i = 0; i < gfs.length; i++) {
                        gfs[i] = (GraphFrame) graphs.get(i);
                        if (!isTemplate) {
                            gfs[i].fileName = fileName;
                            gfs[i].setGraphTitle();
                        }
                    }
                } else {
                    gfs = new GraphFrame[]{this};
                }

                if (isTemplate) {
                    TemplateFactory.SaveTemplate(f, gfs);
                    fileName = null;
                } else { // ".mcl"
                    LayoutFactory.SaveLayout(f, gfs);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            JOptionPane.showMessageDialog(this, Language.getString("MSG_ERROR_SAVING_CHT_FILE"),
                    Language.getString("SAVE_CHART_AS"), JOptionPane.WARNING_MESSAGE);
        }
        setGraphTitle();
    }

    public boolean isValidImageExtension(String fileName) {

        for (int i = 0; i < ChartConstants.IMAGE_EXTENSIONS.length; i++) {
            if (fileName.endsWith(ChartConstants.IMAGE_EXTENSIONS[i])) {
                return true;
            }
        }
        return false;

    }

    public void openClicked() {
        if (!ChartInterface.isIndicatorsEnabled()) return; // open option not available for non technichal users
        if (parent != null) {
            parent.openChart();
        }
    }

    /**
     * *************************************************************
     */
    //changed by sathyajth- apply other temply option to detatched graphs as well  2008/july/30
    public void applyTemplate() {
        if (!ChartInterface.isIndicatorsEnabled()) return;
        applyTemplate(this, this.graph.Symbol, detached);
    }

    /**
     * *************************************************************
     */

    /*changed by sathyajith   - shifted here from chart Frame class */
    public void applyTemplate(Container cont, String symbl, boolean detached) {
        String filePath = GraphFrame.openChartTemplate(cont);//"tmpSave.txt";
        if (filePath == null) return;
        filePath = filePath.trim();
        if (!filePath.toLowerCase().endsWith(".mct")) {
            filePath += ".mct";
        }
        File templateFile = new File(filePath);
        if (!TemplateFactory.LoadTemplate((GraphFrame) cont, templateFile, symbl, detached)) {
            JOptionPane.showMessageDialog(cont, Language.getString("MSG_INVALID_CHART_TEMPLATE"),
                    Language.getString("GRAPH_OPEN_TEMPLATE"), JOptionPane.WARNING_MESSAGE);
        }
    }

    public void applyTemplate(int term) {
        if (!ChartInterface.isIndicatorsEnabled()) return;
        File templateFile = new File(TemplateFactory.TemplatePaths[term]);
        TemplateFactory.LoadTemplate(this, templateFile, this.graph.Symbol, detached);
    }

    public void applyCashFlowTemplate() {
        if (!ChartInterface.isIndicatorsEnabled()) return;
        File templateFile = new File(TemplateFactory.CASHFLOW_TEMPLATE_FILE);
        TemplateFactory.LoadTemplate(this, templateFile, this.graph.Symbol, detached);
    }

    public void setFileName(String fn) {
        fileName = fn;
    }

    public boolean isStaticChart() {
        return staticMode;
    }

    public void setStaticMode(boolean status) {
        staticMode = status;
    }

    public void reorganizeAll() {

        GraphDataManager gdm = graph.GDMgr;
        if (gdm.getSources().size() > 0) {
            gdm.reConstructGraphStore();
            mnuSubCompare.setVisible(true);
            if (parent != null) {
                refreshTopToolBar(parent.topToolBar);
            }
            String[] sa = g_oBaseSymbols.getSymbols();
            String sKey = graph.GDMgr.getBaseGraph();
            if (sKey != null) {
                if ((sa == null) || (sa.length < 1)) {
                    String[] newSa = {sKey};
                    g_oBaseSymbols.setSymbols(newSa);
                } else {
                    g_oBaseSymbols.appendSymbol(sKey);
                }
            }
            graph.setPeriod(graph.getPeriod());
        }
    }

    public static String openChartTemplate(Container cont) {
        int status = 0;

        JFileChooser oFC = new JFileChooser(FILEPATH_CHART_TEMPLATES);
        oFC.setFileSelectionMode(JFileChooser.FILES_ONLY);
        oFC.setDialogTitle(Language.getString("GRAPH_OPEN_TEMPLATE"));
        ExtensionFileFilter oFilter = new
                ExtensionFileFilter(Language.getString("MUBASHER_CHART_TEMPLATE"), "mct");
        oFC.setFileFilter(oFilter);
        oFC.setAcceptAllFileFilterUsed(false);

        status = oFC.showOpenDialog(cont);
        if (status == JFileChooser.APPROVE_OPTION) {
            return oFC.getSelectedFile().getAbsolutePath();
        } else {
            return null;
        }
    }

    public static String openChartFile(Container cont) {
        int status = 0;
        String sFile;

        JFileChooser oFC = new JFileChooser(filePathCharts);
        GUISettings.applyOrientation(oFC);
        oFC.setFileSelectionMode(JFileChooser.FILES_ONLY);
        oFC.setDialogTitle(Language.getString("OPEN_CHART"));
        ExtensionFileFilter oFilter1 = new ExtensionFileFilter(Language.getString("STATIC_CHART"), "cht");
        ExtensionFileFilter oFilter2 = new ExtensionFileFilter(Language.getString("MUBASHER_CHART_LAYOUT"), "mcl");
        if (ChartInterface.isIndicatorsEnabled()) {
            oFC.setFileFilter(oFilter2);
        }
        oFC.setFileFilter(oFilter1);
        oFC.setAcceptAllFileFilterUsed(false);

        status = oFC.showOpenDialog(cont);
        if (status == JFileChooser.APPROVE_OPTION) {
            sFile = oFC.getSelectedFile().getAbsolutePath();
            filePathCharts = oFC.getCurrentDirectory().getAbsolutePath();
            if (oFC.getFileFilter() == oFilter1) {
                if (!sFile.toLowerCase().endsWith(".cht"))
                    sFile += ".cht";
            } else if (oFC.getFileFilter() == oFilter2) {
                if (!sFile.toLowerCase().endsWith(".mcl"))
                    sFile += ".mcl";
            }
            return sFile;
        } else {
            return null;
        }
    }

    public String saveChartFileAs() {
        int status = 0;
        String sFile = "";

        JFileChooser oFC = new JFileChooser(filePathCharts);
        GUISettings.applyOrientation(oFC);
        oFC.setFileSelectionMode(JFileChooser.FILES_ONLY);
        oFC.setDialogTitle(Language.getString("SAVE_CHART_AS"));
        ExtensionFileFilter oFilter1 = new ExtensionFileFilter(Language.getString("STATIC_CHART"), "cht");
        //ExtensionFileFilter oFilter2 = new ExtensionFileFilter(Language.getString("JPEG_IMAGE"), "bmp");

        //constructing the dis
        String exts = "";
        exts += "(*.";

        for (int i = 0; i < ChartConstants.IMAGE_EXTENSIONS.length; i++) {
            if (!(i == ChartConstants.IMAGE_EXTENSIONS.length - 1)) {
                exts += ChartConstants.IMAGE_EXTENSIONS[i] + ",*.";
            } else {
                exts += ChartConstants.IMAGE_EXTENSIONS[i];
            }
        }
        exts += ")";
        exts = "Image File" + exts;

        String str = ChartOptions.getCurrentOptions().save_image_type.toString();
        ExtensionFileFilter oFilter2 = new ExtensionFileFilter(exts, str);

        ExtensionFileFilter oFilter3 = new ExtensionFileFilter(Language.getString("MUBASHER_CHART_TEMPLATE"), "mct");
        ExtensionFileFilter oFilter4 = new ExtensionFileFilter(Language.getString("MUBASHER_CHART_LAYOUT"), "mcl");
        if (ChartInterface.isIndicatorsEnabled()) {
            oFC.setFileFilter(oFilter2);
            oFC.setFileFilter(oFilter3);
            oFC.setFileFilter(oFilter4);
        }
        oFC.setFileFilter(oFilter1);
        oFC.setAcceptAllFileFilterUsed(false);

        status = oFC.showSaveDialog(this);
        if (status == JFileChooser.APPROVE_OPTION) {
            sFile = oFC.getSelectedFile().getAbsolutePath();
            filePathCharts = oFC.getCurrentDirectory().getAbsolutePath();
            if (oFC.getFileFilter() == oFilter1) {
                if (!sFile.toLowerCase().endsWith(".cht"))
                    sFile += ".cht";
            } else if (oFC.getFileFilter() == oFilter2) {
                boolean append = true;
                for (int i = 0; i < ChartConstants.IMAGE_EXTENSIONS.length; i++) {
                    if (sFile.toLowerCase().endsWith("." + ChartConstants.IMAGE_EXTENSIONS[i])) {
                        append = false;
                        break;
                    }
                }
                if (append) {
                    sFile += "." + str;
                }
            } else if (oFC.getFileFilter() == oFilter3) {
                if (!sFile.toLowerCase().endsWith(".mct"))
                    sFile += ".mct";
            } else {
                if (!sFile.toLowerCase().endsWith(".mcl"))
                    sFile += ".mcl";
            }

            File[] fileList;
            try {
                fileList = oFC.getCurrentDirectory().listFiles();
                for (File file : fileList) {
                    if (file.getPath().equalsIgnoreCase(sFile)) {
                        String message = Language.getString("REPLACE_FILE_MSG");
                        int option = SharedMethods.showConfirmMessage(message, JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                        if (option == JOptionPane.YES_OPTION) {
                            break;
                        } else {
                            return null;
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
            return sFile;
        } else {
            return null;
        }
    }

    public JPopupMenu getPopupMenu(String[] sa, final String methodName,
                                   Icon[] icons, Icon[] rollIcons) {
        JPopupMenu oMenu = new JPopupMenu();

        if (sa != null) {
            for (int i = 0; i < sa.length; i++) {
                if (sa[i].trim().equals("")) continue;
                TWMenuItem item = new TWMenuItem(sa[i]);
                oMenu.add(item);
                final int index = i;
                final String menuItemString = sa[index]; // need to access the sorted indicator list properly - Pramoda
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        Integer ObjInt;
                        if (methodName.equals("indicatorClicked")) {
                            ObjInt = indicatorMap.get(menuItemString); // access the sorted indicator list - Pramoda
                        } else {
                            ObjInt = new Integer(index);
                        }
                        try {
                            if (methodName.equals("indicatorClicked")) {
                                indicatorClicked(e.getSource(), ObjInt);
                            } else if (methodName.equals("periodClicked")) {
                                periodClicked(e.getSource(), ObjInt);
                            } else if (methodName.equals("intervalClicked")) {
                                intervalClicked(e.getSource(), ObjInt);
                            } else if (methodName.equals("styleClicked")) {
                                styleClicked(e.getSource(), ObjInt);
                            }
                        } catch (Exception excp) {
                            excp.printStackTrace();
                        }
                    }
                });

                if (icons != null) {
                    item.setIcon(icons[i]);
                } else {
                    item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
                }
                if (rollIcons != null) {
                    item.setSelectedIcon(rollIcons[i]);
                }
            }
        }
        GUISettings.applyOrientation(oMenu); // added by Uditha 2 Sep 2005

        return oMenu;
    }

    private JPopupMenu getPopup(String[] sa, String methodName) {
        return getPopup(sa, methodName, null, null);
    }

    private JPopupMenu getPopup(String[] sa, String methodName, Icon[] ia, Icon[] ria) {
        try {
            return getPopupMenu(sa, methodName, ia, ria);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return new JPopupMenu();
    }

    public JPopupMenu getPeriodPopup() {
        String[] sa;
        if (CurrentMode) {
            sa = getCurrentPeriods();
        } else {
            sa = getHistoryPeriods();
        }
        return getPopup(sa, "periodClicked");
    }

    public JPopupMenu getIntervalPopup() {
        String[] sa;
        if (CurrentMode) {
            sa = getCurrentIntervals();
        } else {
            sa = getHistoryIntervals();
        }
        return getPopup(sa, "intervalClicked");
    }

    public JPopupMenu getStylePopup(Icon[] ia, Icon[] ria) {
        return getPopup(getGraphStyles(), "styleClicked", ia, ria);
    }

    public JPopupMenu getShowHideISPPopUP() {

        //todo ;; put checkboxes here
        final JPopupMenu oMenu = new JPopupMenu();
        isHideAll = true;
        final ArrayList<ChartProperties> sources = graph.GDMgr.getSources();
        for (int j = 0; j < sources.size(); j++) {
            if (!sources.get(j).isHidden() && sources.get(j).getID() != GraphDataManager.ID_BASE && sources.get(j).getID() != GraphDataManager.ID_VOLUME
                    && sources.get(j).getID() != GraphDataManager.ID_TURNOVER && sources.get(j).getID() > GraphDataManager.ID_COMPARE) {
                isHideAll = false;
            }
        }
        final TWCheckBoxMenuItem itemISPHideAll = new TWCheckBoxMenuItem(Language.getString("GRAPH_IND_HIDE_ALL"), "");

        itemISPHideAll.setSelected(isHideAll);
        itemISPHideAll.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {

                for (int j = 0; j < sources.size(); j++) {
                    final ChartProperties cp = (ChartProperties) sources.get(j);
                    ChartProperties cpTmp = null;
                    if (cp.getID() > GraphDataManager.ID_COMPARE && cp.getID() != GraphDataManager.ID_VOLUME && cp.getID() != GraphDataManager.ID_TURNOVER) {
                        isHideAll = itemISPHideAll.getState();
                        cp.setHidden(itemISPHideAll.getState());
                        if (cp.isHidden()) {
                            if ((cp.getRect() != graph.mainRect)) {
                                Rectangle rect = cp.getRect();
                                boolean rectEmpty = true;
                                for (int i = sources.size() - 1; i >= 0; i--) {
                                    ChartProperties cpEXt = (ChartProperties) sources.get(i);
                                    if (cpEXt.getRect() == rect && !cpEXt.isHidden()) {
                                        rectEmpty = false;
                                        break;
                                    }
                                }
                                if (rectEmpty) {
                                    //int index = activeGraph.graph.panels.lastIndexOf(rect);
                                    int indexCp = graph.GDMgr.getIndexOfTheRect(graph.panels, rect);
                                    if (indexCp > -1) {
                                        graph.deleteISPPanel(graph.GDMgr.getIndexOfTheRect(graph.panels, cp.getRect()));
                                    }
                                }
                            } else {
                                for (int k = 0; k < graph.GDMgr.getStaticObjArray().size(); k++) {
                                    AbstractObject or = (AbstractObject) (graph.GDMgr.getStaticObjArray().get(k));
                                    if (or.getTarget().equals(cp)) {
                                        or.isVisible = false;
                                    }
                                }
                            }
                            graph.reDrawAll();
                        } else {//cp not hidden
                            if (cp.getRect() != graph.mainRect) {

                                boolean isNewPanel = false;
                                if (cp.getGroupID().equals("") || cp.getGroupID() == null) {//added for compatibility
                                    isNewPanel = true;
                                } else {
                                    for (int i = 0; i < sources.size(); i++) {
                                        cpTmp = (ChartProperties) sources.get(i);
                                        if ((cp.getGroupID().equals(cpTmp.getGroupID())) && ((graph.GDMgr.getIndexForSourceCP(cp)) != (graph.GDMgr.getIndexForSourceCP(cpTmp)))
                                                && !cpTmp.isHidden()) {
                                            cp.setRect((WindowPanel) graph.panels.get(graph.getIDforPanel(cpTmp.getRect())));
                                        } else {
                                            isNewPanel = true;
                                        }
                                    }
                                }
                                if (isNewPanel) {
                                    graph.addNewPanel(false, cp.getRect());
                                }
                                graph.GDMgr.addSignalLines(cp);
                                /*if (cp.getGroupID().equals(cpTmp.getGroupID())) {
                                    cp.setRect((WindowPanel) graph.panels.get(graph.getIDforPanel(cpTmp.getRect())));
                                } else {
                                    graph.addNewPanel(false, cp.getRect());
                                }*/

                            } else {
                                for (int k = 0; k < graph.GDMgr.getStaticObjArray().size(); k++) {
                                    AbstractObject or = (AbstractObject) (graph.GDMgr.getStaticObjArray().get(k));
                                    if (or.getTarget().equals(cp)) {
                                        or.isVisible = true;
                                    }
                                }
                            }
                            graph.reDrawAll();
                        }

                    }
                }
            }
        }

        );

        oMenu.add(itemISPHideAll);

        String[] actISPNameArr = null;
        if (sources.size() > 0)

        {
            actISPNameArr = new String[sources.size()];
            int i = 0;
            for (int j = 0; j < sources.size(); j++) {
                final ChartProperties cp = (ChartProperties) sources.get(j);

                if (cp.getID() > GraphDataManager.ID_COMPARE && cp.getID() != GraphDataManager.ID_VOLUME && cp.getID() != GraphDataManager.ID_TURNOVER) {

                    final TWCheckBoxMenuItem itemISP = new TWCheckBoxMenuItem();
                    if (cp instanceof IndicatorBase) {
                        itemISP.setText(((IndicatorBase) cp).getTableColumnHeading());
                    } else {
                        //itemISP.setText(cp.getShortName());
                        itemISP.setText(((Indicator) cp).getTableColumnHeading());
                    }
                    if (cp.isHidden()) {
                        itemISP.setSelected(false);
                    } else {
                        itemISP.setSelected(true);
                    }
                    itemISP.addActionListener(new ActionListener() {
                        public void actionPerformed(ActionEvent e) {
                            cp.setHidden(!cp.isHidden());
                            if (cp.isHidden()) {
                                itemISP.setSelected(false);
                                if ((cp.getRect() != graph.mainRect)) {
                                    Rectangle rect = cp.getRect();
                                    boolean rectEmpty = true;
                                    for (int i = sources.size() - 1; i >= 0; i--) {
                                        ChartProperties cpEXt = (ChartProperties) sources.get(i);
                                        if (cpEXt.getRect() == rect && !cpEXt.isHidden()) {
                                            rectEmpty = false;
                                            break;
                                        }
                                    }
                                    if (rectEmpty) {
                                        //int index = activeGraph.graph.panels.lastIndexOf(rect);
                                        int indexCp = graph.GDMgr.getIndexOfTheRect(graph.panels, rect);
                                        if (indexCp > -1) {
                                            graph.deleteISPPanel(graph.GDMgr.getIndexOfTheRect(graph.panels, cp.getRect()));
                                        }
                                    }
                                } else {//check and hide the signal lines
                                    for (int k = 0; k < graph.GDMgr.getStaticObjArray().size(); k++) {
                                        AbstractObject or = (AbstractObject) (graph.GDMgr.getStaticObjArray().get(k));
                                        if (or.getTarget().equals(cp)) {
                                            or.isVisible = false;
                                        }
                                    }
                                }
                                /*if (!isShowatLeastOne) {
                                    isHideAll = true;
                                }*/
                            } else {
                                itemISP.setSelected(true);

                                if (cp.getRect() != graph.mainRect && graph.getIDforPanel(cp.getRect()) != 0) {
                                    if (getPnlIDForCp(cp, graph) > 0) {
                                        cp.setRect((WindowPanel) graph.panels.get(getPnlIDForCp(cp, graph)));
                                    } else {
                                        graph.addNewPanel(false, cp.getRect());
                                    }
                                    graph.GDMgr.addSignalLines(cp);
                                    graph.reDrawAll();
                                } else {//check and show the signal lines
                                    for (int k = 0; k < graph.GDMgr.getStaticObjArray().size(); k++) {
                                        AbstractObject or = (AbstractObject) (graph.GDMgr.getStaticObjArray().get(k));
                                        if (or.getTarget().equals(cp)) {
                                            or.isVisible = true;
                                        }
                                    }

                                }

                                //remove hide all option if hide all is selected
                                //isHideAll = false;

                            }
                            repaint();
                        }
                    });
                    i++;
                    oMenu.add(itemISP);
                }

            }

        } else

        {

        }

        return oMenu;
    }

    public int getPnlIDForCp(ChartProperties cp, StockGraph graph) {
        int pnlID = -1;
        int tmpCpPnlID, cpPnlID;
        //cpPnlID = graph.getIDforPanel(cp.getRect());
        for (int i = 0; i < graph.GDMgr.getSources().size(); i++) {
            ChartProperties tmpCp = (ChartProperties) graph.GDMgr.getSources().get(i);
            tmpCpPnlID = graph.getIDforPanel(tmpCp.getRect());
            if (!(tmpCp.isHidden()) && (tmpCp.getGroupID().equals(cp.getGroupID())) &&
                    ((graph.GDMgr.getIndexForSourceCP(cp)) != (graph.GDMgr.getIndexForSourceCP(tmpCp)))) {
                pnlID = tmpCpPnlID;
            }

        }
        return pnlID;
    }

    public JPopupMenu getSHowHideIspPopupOld(Icon[] ia, Icon[] ria) { //Todo delete if doesnt need
        ArrayList<ChartProperties> sources = graph.GDMgr.getSources();

        if (sources.size() > 0) {
            String[] actISPNameArr = new String[sources.size()];
            int i = 0;
            for (java.util.Iterator it = sources.iterator(); it.hasNext();) {
                ChartProperties cp = (ChartProperties) it.next();
                actISPNameArr[i] = cp.getShortName();
                i++;
            }
            return getPopup(actISPNameArr, "ShowHideISPClicked", ia, ria);
        } else {
            return null;
        }


    }

    public void dragZoomEnabledClicked() {
        setDragZoomEnabled(!ZoomDragEnabled);
    }

    public void boxZoomEnabledClick() {
        setBoxZoomEnabled(!boxZoomEnabled);
    }

    public void zoomInEnabledClick() {
        setZoomInEnabled(!zoomInEnabled);
    }

    public void handDragChartEnabledClick() {
        setchartBodyDraggingEnabled(!chartBodyDraggingEnabled);
    }

    public void fibPriceCalEnabledClick() {
        setFibPriceCalcEnabled(!isFibCalMode);
    }

    public void zoomOutEnabled() {
        setZoomOutEnabled(!zoomOutEnabled);
    }

    public void toggleShowVolume() {
        graph.setVolumeGraphflag(!graph.isShowingVolumeGrf());
    }

    public void toggleShowVolumeByPrice() {
        graph.GDMgr.setShowVolumeByPrice(!graph.GDMgr.isShowVolumeByPrice());
    }

    public void toggleShowTurnOver() {
        graph.setTurnOverGraphFlag(!graph.isShowingTurnOverGraph());
    }

    public void toggleIndexed() {
        if ((graph == null) || (graph.GDMgr == null)) return;
        graph.GDMgr.setIndexed(!graph.GDMgr.isIndexed());
    }

    public void toggleSplitAdjusted() {
        if ((graph == null) || (graph.GDMgr == null)) return;

        if (graph.GDMgr.getSplitAdjustMethod() == GraphDataManager.METHOD_NONE) {
            graph.GDMgr.setAdjustMethod(GraphDataManager.METHOD_UNADJUSTED);
            setAdjustStatus(graph.GDMgr.adjustTypes, false);

        } else if (graph.GDMgr.getSplitAdjustMethod() == GraphDataManager.METHOD_UNADJUSTED) {
            graph.GDMgr.setAdjustMethod(GraphDataManager.METHOD_NONE);
            setAdjustStatus(graph.GDMgr.adjustTypes, true);
        }

        setGraphTitle();
    }

    private void setAdjustStatus(boolean[] adjustTypes, boolean status) {

        for (int i = 0; i < adjustTypes.length; i++) {
            adjustTypes[i] = status;
        }
    }

    public void setGraphTitle(byte adjustMethod) {
        try {
            String title = Language.getString("STOCK_GRAPH");
            if (adjustMethod == GraphDataManager.METHOD_UNADJUSTED) {
                title = Language.getString("GRAPH_SPLIT_UNADJUSTED");
            }

            if ((graph != null) && (graph.GDMgr != null) && (graph.GDMgr.getBaseGraph() != null)) {
                String grfName = StockGraph.extractSymbolFromStr(graph.GDMgr.getBaseGraph());
                String strTitle = title + " (" + grfName + ")";
                if ((fileName != null) && (fileName.trim().length() > 0)) {
                    File f = new File(fileName);
                    strTitle += " - " + f.getName();
                } else if (tmpLayoutName != null) {
                    strTitle += " - " + tmpLayoutName;
                }
                setTitle(strTitle);
            } else {
                setTitle(Language.getString("STOCK_GRAPH"));
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void toggleShowSplits() {
        if ((graph == null) || (graph.GDMgr == null)) return;
        graph.GDMgr.setShowSplits(!graph.GDMgr.isShowingSplits());
    }

    public void toggleShowAnnouncements() {
        if ((graph == null) || (graph.GDMgr == null)) return;
        graph.GDMgr.setShowAnnouncements(!graph.GDMgr.isShowingAnnouncements());
        graph.reDrawAll(); // To  Show Annoucment in The Top if max bound exceeds //check whether scale is changed and iff call the redraw 

    }

    public void toggleGraphMode() {
        //should load from a template and set the period ,mode,interval ACORding to that - sathyajith
        setGraphMode(!CurrentMode, true); //toggeling - sendRemoveRequest -true
    }

    public void toggleLegendType() {
        if (graph.getLegendType() > StockGraph.INT_LEGEND_SYMBOL) {
            graph.setLegendType(StockGraph.INT_LEGEND_SYMBOL);
        } else {
            graph.setLegendType(StockGraph.INT_LEGEND_SHORT_DESCRIPTION);
        }
    }

    public void toggleGridType() {
        if ((graph != null) && (graph.GDMgr != null))
            graph.GDMgr.toggleGridType();
    }

    public void toggleRectangularSelection() {
        setRectangularSelection(!isRectangularSelection);
    }

    public void toggleFibCalMode() {
        setFibCalMode(!isFibCalMode);
    }

    public boolean isGraphDetached() {
        return detached;
    }

    public boolean isDetached() {
        return false;
    }

    public void setDetached(boolean d) {
        detached = d;
    }

    public void printTable() {
    }

    public Point getToolTipPos(int x, int y, int width, int height) {
        int wyy;
        int exx;
        boolean yOk = (y + height + 22) < this.getContentPane().getHeight();
        boolean xOk = (x + width) < this.getContentPane().getWidth();
        if (yOk) {
            if (xOk) {
                exx = x;
                wyy = y + 22;
            } else {
                exx = Math.max(this.getWidth() - (width + 5), 0);
                wyy = y + 22;
            }
        } else {
            if (xOk) {
                exx = x;
                wyy = Math.max(y - height, 0);
            } else {
                exx = Math.max(this.getWidth() - (width + 5), 0);

                wyy = Math.max(y - height, 0);
            }
        }
        return new Point(exx, wyy);
    }

    public void removeGraphFocus() {
        graph.removeFocus();
    }

    public ChartFrame getChartFrame() {
        return parent;
    }

    public static void insertToSymbolsEx(SymbolsEx sEx, String sKey) {
        if (sKey != null) {
            String[] sa = sEx.getSymbols();
            if ((sa == null) || (sa.length < 1)) {
                String[] newSa = {sKey};
                sEx.setSymbols(newSa);
            } else {
                if (getIndexOf(sKey, sEx.getSymbols()) < 0)
                    sEx.appendSymbol(sKey);
            }
        }
    }

    public TWMenu getMenu(String name, String[] sa, final String methodName,
                          Icon[] icons, Icon[] rollIcons) {
        TWMenu oMenu = new TWMenu(Language.getString(name));

        if (sa != null)
            for (int i = 0; i < sa.length; i++) {
                if (sa[i].trim().equals("")) continue;
                TWMenuItem item = new TWMenuItem(sa[i]);
                oMenu.add(item);
                final int index = i;
                final String menuItemString = sa[index]; // need to access the sorted indicator list properly - Pramoda
                item.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        Integer ObjInt;
                        if (methodName.equals("indicatorClicked")) {
                            ObjInt = indicatorMap.get(menuItemString); // access the sorted indicator list - Pramoda
                        } else {
                            ObjInt = new Integer(index);
                        }
                        try {
                            if (methodName.equals("indicatorClicked")) {
                                indicatorClicked(e.getSource(), ObjInt);
                            } else if (methodName.equals("periodClicked")) {
                                periodClicked(e.getSource(), ObjInt);
                            } else if (methodName.equals("intervalClicked")) {
                                intervalClicked(e.getSource(), ObjInt);
                            } else if (methodName.equals("styleClicked")) {
                                styleClicked(e.getSource(), ObjInt);
                            }
                        } catch (Exception excp) {
                            excp.printStackTrace();
                        }
                    }
                });
                if (icons != null) {
                    item.setIcon(icons[i]);
                } else {
                    item.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_unchecked.gif"));
                }
                if (rollIcons != null) {
                    item.setSelectedIcon(rollIcons[i]);
                }
            }
        GUISettings.applyOrientation(oMenu);

        return oMenu;
    }

    public void switchPanel() {
        try {
            if (tableMode) {
                pnlInfoPanel.setVisible(true);
                graphFrameLayout.show(getContentPane(), "GRAPH");

            } else {
                pnlInfoPanel.setVisible(false);
                hintPane.setVisible(false);
                graphFrameLayout.show(getContentPane(), "TABLE");
                prepareTable();
            }
            tableMode = !tableMode;
            graph.GDMgr.setTableMode(tableMode);
            if (parent != null) {
                refreshTopToolBar(parent.topToolBar);
                refreshBottomToolBar(parent.bottomToolBar);
                ChartFrame.getSharedInstance().refreshMainMenus(!tableMode);
                graph.GDMgr.refreshDataWindow();
            }

            graph.GDMgr.updateBaseSymbolRecords(0, graph.GDMgr.getGraphStore().size());
            if (tableMode) {
                //((SmartTable) dataTable.getTable()).adjustColumnWidthsToFit(40, 0, dataTable.getTable().getColumnCount(), NO_OF_ROWS_ADJUSTED);
                ((SmartTable) dataTable.getTable()).adjustColumnWidthsToFit(40);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void adjustTableColumnWidths() {
        ((SmartTable) dataTable.getTable()).adjustColumnWidthsToFit(40);
    }

    public void adjustDataTableRenderer() {

        String asHeadings[] = tableModel.getViewSettings().getColumnHeadings();
        SortButtonRenderer renderer = new SortButtonRenderer(true);
        renderer.applyTheme();

        TableColumnModel colmodel = ((SmartTable) dataTable.getTable()).getColumnModel();
        int n = asHeadings.length;

        for (int i = 0; i < n; i++) {
            colmodel.getColumn(i).setHeaderRenderer(renderer);
        }

    }

    public void adjustGraphTableRenderer() {

        String asHeadings[] = graphTableModel.getViewSettings().getColumnHeadings();
        SortButtonRenderer renderer = new SortButtonRenderer(true);
        renderer.applyTheme();

        TableColumnModel colmodel = ((SmartTable) graphDataTable.getTable()).getColumnModel();
        int n = asHeadings.length;

        for (int i = 0; i < n; i++) {
            colmodel.getColumn(i).setHeaderRenderer(renderer);
        }
    }

    public void prepareTable() {
        try {
            if (CurrentMode) {
                tableModel.getRendIDs()[0] = 8;
                //enableChangeCols(false);
            } else {
                tableModel.getRendIDs()[0] = 7;
                //enableChangeCols(true);
            }
            dataTable.getTable().createDefaultColumnsFromModel();
            adjustDataTableRenderer();

            dataTable.setColumnIdentifiers();
            ((SmartTable) dataTable.getTable()).adjustColumnWidthsToFit(40, 0, dataTable.getTable().getColumnCount(), NO_OF_ROWS_ADJUSTED);
            //adjustColSettings(dataTable.getModel().getViewSettings());
            //dataTable.saveColumnPositions(dataTable.getModel().getViewSettings());
            //adjustColumnSettings(dataTable.getModel().getViewSettings());
            dataTable.getModel().getViewSettings().setColumns(new BigInteger("" + ((int) Math.pow(2, dataTable.getModel().getViewSettings().getColumnHeadings().length) - 1)));
            dataTable.getTable().updateUI();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void adjustColumnSettings(ViewSetting viewSettings) {

        Vector g_oSelected = new Vector();
        BigInteger lSelectedPos = (viewSettings.getColumns().or(viewSettings.getFixedColumns())).and(viewSettings.getHiddenColumns().not());
        BigInteger lFixedPos = viewSettings.getFixedColumns();
        BigInteger lHiddenPos = viewSettings.getHiddenColumns();

        String[] asColumns = viewSettings.getColumnHeadings();
        for (int i = 0; i < (asColumns.length - 1); i++) {
            if (((lHiddenPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))) // make sure the col is not hidden
            {
                if ((lSelectedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0))) {
                } else {
                    if ((lFixedPos.and(BigInteger.valueOf(1))).equals(BigInteger.valueOf(0)))
                        g_oSelected.add(asColumns[i]);
                }
            }
            lSelectedPos = lSelectedPos.shiftRight(1);
            lFixedPos = lFixedPos.shiftRight(1);
            lHiddenPos = lHiddenPos.shiftRight(1);
        }
        g_oSelected.add(asColumns[(asColumns.length - 1)]);
        lSelectedPos = lSelectedPos.shiftRight(1);
        lFixedPos = lFixedPos.shiftRight(1);
        lHiddenPos = lHiddenPos.shiftRight(1);

        Object[] tempArray = g_oSelected.toArray();
        Arrays.sort(tempArray);
        g_oSelected.clear();
        for (int k = 0; k < tempArray.length; k++) {
            g_oSelected.addElement(tempArray[k]);
        }
        tempArray = null;

        BigInteger lCols = BigInteger.valueOf(0);
        for (int i = 0; i < asColumns.length; i++) {
            if (g_oSelected.indexOf(asColumns[i]) != -1) {
                //change start
                lCols = lCols.add(pwr(i));
            }
        }
        viewSettings.setColumns((lCols));

    }

    private BigInteger pwr(int iPwr) {
        BigInteger i = BigInteger.valueOf(1);
        return (i.shiftLeft(iPwr));
    }

    private void adjustColSettings(ViewSetting oSeting) {
        int columncnt = oSeting.getColumnHeadings().length;
        int[][] colSetings = new int[columncnt][3];

        for (int i = 0; i < columncnt; i++) {
            colSetings[i][0] = i;
            colSetings[i][1] = 100;
            colSetings[i][2] = i;
        }

        oSeting.setColumnSettings(colSetings);

    }

    //for the table in Data window(top tool bar)
    public void prepareGraphTable() {

        try {
            graph.GDMgr.setTableMode(true);
            graph.GDMgr.updateBaseSymbolRecords(0, graph.GDMgr.getGraphStore().size());

            if (graphTableModel == null) {
                graphTableModel = new GraphDataTableModel(this);
                graphTableModel.setStore(graph.GDMgr);

                ViewSetting viewSetting = Client.getInstance().getViewSettingsManager().getSymbolView("OUTER_CHART").getObject();
                viewSetting.setID("" + System.currentTimeMillis());

                GUISettings.setColumnSettings(viewSetting, ChartInterface.getColumnSettings("CHART_DATA_COLS"));
                graphTableModel.setViewSettings(viewSetting);

                graphDataTable = new Table();
                graphDataTable.setWindowType(ViewSettingsManager.CHART_WINDOW_VIEW);
                graphDataTable.setSortingEnabled();
                graphDataTable.setModel(graphTableModel);
                graphTableModel.setTable(graphDataTable, new GraphDataRenderer(viewSetting.getColumnHeadings(), graphTableModel.getRendIDs()));
                graphDataTable.getModel().getViewSettings().setColumns(new BigInteger("" + ((int) Math.pow(2, graphDataTable.getModel().getViewSettings().getColumnHeadings().length) - 1)));
                graphTableModel.setDecimalCount(SharedMethods.getDecimalPlaces(baseKey));
                GUISettings.applyOrientation(graphDataTable);

                TWMenuItem exportData = new TWMenuItem(Language.getString("EXPORT_DATA"), "exporttrades.gif");
                exportData.addActionListener(new ActionListener() {
                    public void actionPerformed(ActionEvent e) {
                        exportGraphData();
                    }
                });

                graphDataTable.getPopup().setMenuItem(exportData);
                graphDataTable.getPopup().hideTitleBarMenu();
            }

            if (CurrentMode) {
                graphTableModel.getRendIDs()[0] = 8;
            } else {
                graphTableModel.getRendIDs()[0] = 7;
            }
            graphTableModel.getTable().createDefaultColumnsFromModel();
            adjustGraphTableRenderer();

            graphDataTable.setColumnIdentifiers();
            //((SmartTable) graphDataTable.getTable()).adjustColumnWidthsToFit(40, 0, graphDataTable.getTable().getColumnCount(), NO_OF_ROWS_ADJUSTED);
            ((SmartTable) graphDataTable.getTable()).adjustColumnWidthsToFit(40);
            graphDataTable.getTable().updateUI();
        } catch (Exception e) {
            e.printStackTrace();
            System.out.println(" ==== prepareGraphTable() ====== ");
        }
    }

    public void exportGraphData() {
        JFileChooser fileChooser = new JFileChooser();
        String[] extentions = {"csv"};
        TWFileFilter oFilterCSV = new TWFileFilter(Language.getString("CSV_FORMAT"), extentions);
        fileChooser.setFileFilter(oFilterCSV);
        fileChooser.setAcceptAllFileFilterUsed(false);

        boolean proceed = true;
        int status = fileChooser.showSaveDialog(this);
        File[] fileList = null;
        if (status == JFileChooser.APPROVE_OPTION) {
            try {
                fileList = fileChooser.getCurrentDirectory().listFiles();
                for (File file : fileList) {
                    if (file.getName().equalsIgnoreCase((fileChooser.getSelectedFile().getName()+"."+extentions[0]))){
                        String message = Language.getString("REPLACE_FILE_MSG");
                        int option = SharedMethods.showConfirmMessage(message, JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                        if (option != JOptionPane.YES_OPTION) {
                            proceed = false;
                            break;
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        File file;
        if ((file = fileChooser.getSelectedFile()) != null && proceed) {
            saveGraphTableData(file.getAbsolutePath());
        }
    }

    public void exportData() {
        JFileChooser fileChooser = new JFileChooser();
        String[] extentions = {"csv"};
        TWFileFilter oFilterCSV = new TWFileFilter(Language.getString("CSV_FORMAT"), extentions);
        fileChooser.setFileFilter(oFilterCSV);
        fileChooser.setAcceptAllFileFilterUsed(false);

        boolean proceed = true;
        int status = fileChooser.showSaveDialog(this);
        File[] fileList = null;
        if (status == JFileChooser.APPROVE_OPTION) {
            try {
                fileList = fileChooser.getCurrentDirectory().listFiles();
                for (File file : fileList) {
                    if (file.getName().equalsIgnoreCase((fileChooser.getSelectedFile().getName()+"."+extentions[0]))) {
                        String message = Language.getString("REPLACE_FILE_MSG");
                        int option = SharedMethods.showConfirmMessage(message, JOptionPane.WARNING_MESSAGE, JOptionPane.YES_NO_OPTION);
                        if (option != JOptionPane.YES_OPTION) {
                            proceed = false;
                            break;
                        }
                    }
                }

            } catch (Exception e) {
                e.printStackTrace();
            }
        }

        File file;
        if ((file = fileChooser.getSelectedFile()) != null && proceed) {
            saveData(file.getAbsolutePath());
        }
    }

    public void saveData(String fileName) {
        try {

            if (fileName.indexOf(".") < 0)
                fileName += ".csv";

            FileOutputStream out = new FileOutputStream(fileName);
            String[] headings = g_oViewSettings.getColumnHeadings();
            StringBuffer row = new StringBuffer();

            // write headings
            for (int i = 0; i < headings.length; i++) {
                row.append(headings[i]);
                if (i != headings.length - 1)
                    row.append(",");
                else
                    row.append("\n");
            }
            out.write(row.toString().getBytes());
            row = null;

            // write data
            JLabel label = null;
            for (int r = 0; r < tableModel.getRowCount(); r++) {
                row = new StringBuffer();
                for (int c = 0; c < headings.length; c++) {
                    if (c == 0) {
                        label = (JLabel) dataTable.getTable().getCellRenderer(r, c)
                                .getTableCellRendererComponent(dataTable.getTable(),
                                        tableModel.getValueAt(r, c), false, false, r, c);
                        row.append(label.getText());
                        label = null;
                    } else {
                        TWDecimalFormat formatter = new TWDecimalFormat("#0.00");
                        TWDecimalFormat formatter_three = new TWDecimalFormat("#0.000");
                        Object obj = tableModel.getValueAt(r, c);

                        if (obj != null) {
                            switch (c) {
                                //Turnover
                                case 6:
                                    double val = Double.parseDouble(String.valueOf(obj));
                                    if (val <= 0) {
                                        obj = Language.getString("NA");
                                    } else {
                                        obj = formatter.format(val);
                                    }
                                    break;
                                // Change
                                case 7:
                                    obj = formatter_three.format(Double.parseDouble(String.valueOf(obj)));
                                    break;
                                //% Change
                                case 8:
                            obj = formatter.format(Double.parseDouble(String.valueOf(obj)));
                                    break;
                                default:
                                    break;
                            }

                        }
                        row.append(obj);
                    }

                    if (c != headings.length - 1)
                        row.append(",");
                    else
                        row.append("\n");
                }
                out.write(row.toString().getBytes());
                row = null;
            }

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public void saveGraphTableData(String fileName) {
        try {

            if (fileName.indexOf(".") < 0)
                fileName += ".csv";

            FileOutputStream out = new FileOutputStream(fileName);
            String[] headings = g_oViewSettings.getColumnHeadings();
            StringBuffer row = new StringBuffer();

            // write headings
            for (int i = 0; i < headings.length; i++) {
                row.append(headings[i]);
                if (i != headings.length - 1)
                    row.append(",");
                else
                    row.append("\n");
            }
            out.write(row.toString().getBytes());
            row = null;

            // write data
            JLabel label = null;
            for (int r = 0; r < graphTableModel.getRowCount(); r++) {
                row = new StringBuffer();
                for (int c = 0; c < headings.length; c++) {
                    if (c == 0) {
                        label = (JLabel) graphDataTable.getTable().getCellRenderer(r, c)
                                .getTableCellRendererComponent(graphDataTable.getTable(),
                                        graphTableModel.getValueAt(r, c), false, false, r, c);
                        row.append(label.getText());
                        label = null;
                    } else {
                        TWDecimalFormat formatter = new TWDecimalFormat("#0.00");
                        TWDecimalFormat formatter_three = new TWDecimalFormat("#0.000");
                        Object obj = tableModel.getValueAt(r, c);

                        if (obj != null) {
                            switch (c) {
                                //Turnover
                                case 6:
                                    double val = Double.parseDouble(String.valueOf(obj));
                                    if (val <= 0) {
                                        obj = Language.getString("NA");
                                    } else {
                                        obj = formatter.format(val);
                                    }
                                    break;
                                // Change
                                case 7:
                                    obj = formatter_three.format(Double.parseDouble(String.valueOf(obj)));
                                    break;
                                //% Change
                                case 8:
                                    obj = formatter.format(Double.parseDouble(String.valueOf(obj)));
                                    break;
                                default:
                                    break;
                            }

                        }
                        row.append(obj);
                    }

                    if (c != headings.length - 1)
                        row.append(",");
                    else
                        row.append("\n");
                }
                out.write(row.toString().getBytes());
                row = null;
            }

            out.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public boolean isTableMode() {
        return tableMode;
    }

    public void setTableMode(boolean tableMode) {
        this.tableMode = tableMode;
        graph.GDMgr.setTableMode(tableMode);
    }

    public TWMenu getPeriodSubMenu() {
        String[] sa;
        if (CurrentMode) {
            sa = getCurrentPeriods();
        } else {
            sa = getHistoryPeriods();
        }
        return getMenu("PERIOD", sa, "periodClicked", null, null);
    }

    public TWMenu getIntervalSubMenu() {
        String[] sa;
        if (CurrentMode) {
            sa = getCurrentIntervals();
        } else {
            sa = getHistoryIntervals();
        }
        return getMenu("INTERVAL", sa, "intervalClicked", null, null);
    }

    public TWMenu getStyleSubMenu(Icon[] ia, Icon[] ria) {
        return getMenu("STYLE", getGraphStyles(), "styleClicked", ia, ria);
    }

    public TWMenu getModeMenu() {
        String sname;
        try {
            sname = Language.getString("GRAPH_MODE");
            String[] sa = {Language.getString("HISTORY"),
                    Language.getString("INTRADAY")};
            return getRadioSubMenu(sname, sa, "modeClicked", true);
        } catch (Exception ex) {
        }
        return new TWMenu(Language.getString("GRAPH_MODE"));
    }

    public void toggleDetach() {
        if (detached) {
            ChartFrame cf = ChartFrame.getSharedInstance();
            int result = JOptionPane.OK_OPTION;
            int numberOfGraphsToBeDeleted = 0;
            if (cf.getTotalChartCount() + cf.getProChartCount() > cf.getMaxChartCount() && (!cf.isVisible() && !cf.isDetached())) {
                String message = "Maximum number of Graphs allowed to be open at a given time has exceeded." +
                        "\nPlease close open Graphs, or Graphs open in ProChart will be closed.\nYou have %s Graphs opened in ProChart.";
                message = String.format(message, cf.getProChartCount());
                result = JOptionPane.showConfirmDialog(this.parent, message, "Error", JOptionPane.OK_CANCEL_OPTION, JOptionPane.ERROR_MESSAGE);
                numberOfGraphsToBeDeleted = (cf.getTotalChartCount() + cf.getProChartCount()) - cf.getMaxChartCount();
            }
            if (result == JOptionPane.OK_OPTION) {
                //close the necessary graphs first from ProChart
                JInternalFrame[] frames = cf.getDesktop().getAllFrames();
                for (int i = 0; i < numberOfGraphsToBeDeleted; i++) {
                    if (frames[i] instanceof GraphFrame) {
                        ((GraphFrame) frames[i]).closeWindow();
                    }
                }
                detached = false;
                this.setVisible(false);
                Client.getInstance().getDesktop().remove(this);
                this.setLayer(GUISettings.TOP_LAYER);
                cf.getDesktop().add(this);

                cf.getWindowTabs().addTab(this.getTitle(), this);
                this.setGraphTitle();
                this.parent = cf;
                cf.setActiveGraph(this);
                this.addDataWindowListener(cf);
                this.show();
                //cf.tileWindows();

                removeLinkedWindow(false);
                cf.tileWindows();
                Client.getInstance().showProChart();
                cf.getDesktop().updateUI();

                try {
                    this.setSelected(true);
                } catch (PropertyVetoException ex1) {
                    ex1.printStackTrace();
                }

                //chart count
                cf.setTotalChartCount(cf.getTotalChartCount() + numberOfGraphsToBeDeleted);
                cf.setProChartCount(cf.getProChartCount() + 1);
            }
        } else {
            detached = true;
            this.setVisible(false);
            try {
                this.setSelected(false);
            } catch (PropertyVetoException e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            ChartFrame cf = ChartFrame.getSharedInstance();
            cf.getDesktop().remove(this);
//            if (ChartFrame.getSharedInstance().isInTabbedPanel(this)) {
//                removeWindowtab(this);
//            }
            cf.getDesktop().updateUI();

            this.removeDataWindowListener();
            this.parent = null;
            if (cf.getActiveGraph() == this)
                cf.setActiveGraph(null);

            ViewSetting defaultSettings = ViewSettingsManager.getSummaryView("CHART");
            if (defaultSettings != null) {
                this.setSize(defaultSettings.getSize());
            }
            defaultSettings = null;
            this.setLayer(GUISettings.TOP_LAYER);
            Client.getInstance().getDesktop().add(this);
            this.show();
            removeLinkedWindow(true);

            try {
//                this.setIcon(true);
                Client.getInstance().getDesktop().updateUI();
//                this.setIcon(false);

                setBoxZoomEnabled(false);
                setDragZoomEnabled(false);
                setZoomInEnabled(false);
                setZoomOutEnabled(false);
                graph.setCursor(Cursor.getDefaultCursor());
                this.setMaximum(false);

            } catch (PropertyVetoException ex) {
                ex.printStackTrace();
            }

            if (cf.isBIsTabbedMode()) {
                try {
                    cf.doMaxWindows();
                    this.setTitleVisible(true);
                } catch (Exception e) {

                }
            } else {
                cf.tileWindows();
            }

            //chart count
            cf.setProChartCount(cf.getProChartCount() - 1);
        }
    }

    public void setSemiLogScales() {
        ChartFrame.setSemiLogScales(this.graph, this);
    }

    public void setGraphTitle() {

        try {
            byte adjustMethod = graph.GDMgr.getSplitAdjustMethod();
            String title = Language.getString("STOCK_GRAPH");
            if (adjustMethod == GraphDataManager.METHOD_UNADJUSTED) {
                title = Language.getString("GRAPH_SPLIT_UNADJUSTED");
            }

            if ((graph != null) && (graph.GDMgr != null) && (graph.GDMgr.getBaseGraph() != null)) {
                String grfName = StockGraph.extractSymbolFromStr(graph.GDMgr.getBaseGraph());
                String strTitle = title + " (" + grfName + ")";
                if ((fileName != null) && (fileName.trim().length() > 0)) {
                    File f = new File(fileName);
                    //strTitle += " - " + f.getName();//commented by charithn to remove the "static chart path" from graph title bar                       
                } else if (tmpLayoutName != null) {
                    strTitle += " - " + tmpLayoutName;
                }
                setTitle(strTitle);
            } else {
                setTitle(Language.getString("STOCK_GRAPH"));
            }
            if (!detached) {
                ChartFrame.getSharedInstance().setTitle();
            }
            updateTabTitles(this);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    public static void updateTabTitles(InternalFrame frame) {

        if (ChartFrame.getWindowTabs() != null) {
            for (int i = 0; i < ChartFrame.getWindowTabs().getTabCount(); i++) {
                if (ChartFrame.getWindowTabs().getComponentAt(i).equals(frame)) {
                    ChartFrame.getWindowTabs().setTitleAt(i, frame.getTitle());
                    break;
                }
            }

            ChartFrame.getWindowTabs().repaint();
            ChartFrame.getWindowTabs().updateUI();
        }
    }

    public StockGraph getGraph() {
        return graph;
    }

    public String getWindowID() {
        return null;
    }

    public void setWindowID(String id) {

    }

    public void twConnected() {

    }

    public void twDisconnected() {
    }

    public ArrayList getAlLayuotIDs() {
        return alLayuotIDs;
    }

    public void addLayout(String uniqueID, int currentLayout) {
        alLayuotIDs.add(uniqueID);
        if (currentLayout > 0) { //automatically saved
            fileName = null;
            /********remove naming "Layout_1 etc for automactically saved layouts*******************/
            //tmpLayoutName = Language.getString("TITLE_LAYOUT") + currentLayout;
            /***************************************************************************************/
            setGraphTitle();//
        }
    }

    public void removeLayout(String uniqueID) {
        alLayuotIDs.remove(alLayuotIDs.indexOf(uniqueID));
    }

    public void mouseWheelMoved(MouseWheelEvent e) {
        graph.zSlider.mouseWheelMoved(e);
        graph.sliderMouseDragged();
    }

    public void dataDragPointChanged(Point2D.Float e, StockGraph graph, int type) {
        int x = (int) e.getX();
        int y = (int) e.getY();
        GraphDataManager GDMgr = graph.GDMgr;

        int pnlID = graph.getPanelIDatPoint(x, y);
        double value = GDMgr.getYValueForthePixel(y, pnlID);

        DecimalFormat priceFormat = new DecimalFormat("##0.00");
        String priceValue = priceFormat.format(value);

        String symbol = "";
        if (GDMgr.getSources().size() > 0) {
            ChartProperties cp = (ChartProperties) graph.GDMgr.getSources().get(0);
            symbol = StockGraph.extractSymbolFromStr(cp.getSymbol());
        }

        DynamicArray graphStore = GDMgr.getStore();
        Object[] singlePoint;
        if (dragWindow != null) {
            long longTime = Long.MIN_VALUE;
            long interval = graph.getInterval();
            int index = Math.round(GDMgr.getIndexForthePixel(x));

            if ((index >= GDMgr.getBeginIndex()) && (index < graphStore.size())) {
                singlePoint = (Object[]) (graphStore.get(index));
                if (singlePoint != null) {
                    //Long objLong = (Long) singlePoint[0];
                    longTime = (Long) singlePoint[0];
                    /*if (objLong != null) {
                        longTime = objLong.longValue();
                    }*/
                }
            }
            dragWindow.setDataValues(symbol, graph.isCurrentMode(), type, priceValue, interval, longTime, e);
        }
    }

    public void dataPointChangedOnGraph(MouseEvent e, Point CurrentMousePos, StockGraph graph) {

        int x = e.getX();
        GraphDataManager GDMgr = graph.GDMgr;
        DynamicArray graphStore = GDMgr.getStore();

        Object[] singlePoint;
        if (parent.getDataWindow() != null) {
            long longTime = Long.MIN_VALUE;
            long interval = graph.getInterval();
            int index = Math.round(GDMgr.getIndexForthePixel(x));

            if ((index >= GDMgr.getBeginIndex()) && (index < graphStore.size())) {
                singlePoint = (Object[]) (graphStore.get(index));
                if (singlePoint != null) {
                    //Long objLong = (Long) singlePoint[0];
                    longTime = (Long) singlePoint[0];
                    /*if (objLong != null) {
                        longTime = objLong.longValue();
                    }*/
                }
            }
            parent.getDataWindow().updateDataWindowTable(graph.isCurrentMode(), interval, longTime);
            parent.getDataWindow().setDragMode(false);
            parent.getDataWindow().updateToolTips();
            parent.getDataWindow().refreshButtons();
        }
    }

    public void mnuLinkToSidebarClicked() {

        if (detached) {
            LinkStore.getSharedInstance().LinkToSidebar(this);
        }
    }

    public void removeLinkedWindow(boolean isProChartLink) {

        if (ChartFrame.getSharedInstance().getCurrentArrangeMode() == ChartFrame.TABBED) {

            try {
                if (!isProChartLink) {
                    if (!(getDefaultCloseOperation() == HIDE_ON_CLOSE) && g_oViewSettings.getProperty(ViewConstants.VC_LINKED) != null
                            && ((String) g_oViewSettings.getProperty(ViewConstants.VC_LINKED)).equalsIgnoreCase("true")) {
                        LinkStore.getSharedInstance().removeLinkedWindow(g_oViewSettings.getSubType());
                        LinkStore.getSharedInstance().removeFromHashTable(getLinkedGroupID(), this);
                        setLinkedGroupID(LinkStore.LINK_NONE);
                    }
                } else {


                    super.setLinkedGroupID(LinkStore.LINK_NONE);
                    if ((((BasicInternalFrameUI) getUI()).getNorthPane()) != null) {
                        ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_NONE);
                    }
                    ChartLinkStore.getSharedInstance().removeFromStore(this, linkID);
                }

            } catch (Exception ex) {
                ex.printStackTrace();
            }
            this.updateUI();
        }

    }

    public boolean isUsingDefaultTemplate() {
        return usingDefaultTemplate;
    }

    public void setUsingDefaultTemplate(boolean usingDefaultTemplate) {
        this.usingDefaultTemplate = usingDefaultTemplate;
    }

    /* **************************** link groups related *******************************/
    //added by charithn
    public void showLinkGroup() {

        if (detached) {
            super.showLinkGroup();  //in detached mode no need to show the chart link groups.should show the pro link items
            return;
        }

        if (linkGroupMenu == null) {
            createLinkGroupMenu();
        }

        SwingUtilities.updateComponentTreeUI(linkGroupMenu);
        Point point = MouseInfo.getPointerInfo().getLocation();
        SwingUtilities.convertPointFromScreen(point, this);
        GUISettings.showPopup(linkGroupMenu, this, (int) point.getX(), (int) point.getY());
    }


    private void createLinkGroupMenu() {
        linkGroupMenu = new JPopupMenu();

        TWMenuItem redmenu = new TWMenuItem(Language.getString("RED"), "lnk_red.gif");
        redmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                //linkGroupSelected(LinkStore.LINK_RED);
                setLinkedGroupID(LinkStore.LINK_RED);
                //((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(LinkStore.LINK_RED);
                //ChartLinkStore.getSharedInstance().addToStore(gf, LinkStore.LINK_RED);
                //linkID = LinkStore.LINK_RED;
            }
        });
        linkGroupMenu.add(redmenu);

        greenmenu = new TWMenuItem(Language.getString("GREEN"), "lnk_green.gif");
        greenmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLinkedGroupID(LinkStore.LINK_GREEN);
            }
        });
        linkGroupMenu.add(greenmenu);

        bluemenu = new TWMenuItem(Language.getString("BLUE"), "lnk_blue.gif");
        bluemenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLinkedGroupID(LinkStore.LINK_BLUE);
            }
        });
        linkGroupMenu.add(bluemenu);

        notlinkedmenu = new TWMenuItem(Language.getString("NOT_LINK"), "lnk_notlink.gif");
        notlinkedmenu.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                setLinkedGroupID(LinkStore.LINK_NONE);
            }
        });
        linkGroupMenu.add(notlinkedmenu);

        linkGroupMenu.addPopupMenuListener(new PopupMenuListener() {
            public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
                /* if (oSettings != null) {
                    redmenu.setEnabled(LinkStore.getSharedInstance().isColorGrpAvailableForwindowType(oSettings.getSubType(), LinkStore.LINK_RED));
                    greenmenu.setEnabled(LinkStore.getSharedInstance().isColorGrpAvailableForwindowType(oSettings.getSubType(), LinkStore.LINK_GREEN));
                    bluemenu.setEnabled(LinkStore.getSharedInstance().isColorGrpAvailableForwindowType(oSettings.getSubType(), LinkStore.LINK_BLUE));
                }*/
            }

            public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {

            }

            public void popupMenuCanceled(PopupMenuEvent e) {

            }
        });
        GUISettings.applyOrientation(linkGroupMenu);
    }

    public String getLinkID() {
        return linkID;
    }

    public void setLinkedGroupID(String linkID) {

        super.setLinkedGroupID(linkID);
        ((TWBasicInternalFrameTitlePane) (((BasicInternalFrameUI) getUI()).getNorthPane())).setLinkGroupIDToButton(linkID);

        if (!detached) {
            this.linkID = linkID;

            if (!linkID.equals(LinkStore.LINK_NONE)) {
                ChartLinkStore.getSharedInstance().addToStore(this, linkID);
            } else {
                ChartLinkStore.getSharedInstance().removeFromStore(this, linkID);
            }
        }
    }

    /**
     * ************************** end of link groups related ******************************
     */

    public void fireEscapeAction() {

        if (parent.bottomToolBar.ObjectTobeDrawn == AbstractObject.INT_ZIG_ZAG) {

            //int pnlID = graph.getPanelIDatPoint(CurrentMousePos.x, CurrentMousePos.y);
            //addObjectOnGraph(pnlID);
            parent.bottomToolBar.ObjectTobeDrawn = AbstractObject.INT_NONE;
            parent.bottomToolBar.btnNeutral.doClick();
            graph.setCursor(Cursor.getDefaultCursor());

            xPoints.clear();
            yPoints.clear();

            BottomToolBar.CURSOR_STATUS = BottomToolBar.STATUS_NONE;
            graph.repaint();
        }
    }

    public String getBaseKey() {
        return baseKey;
    }

    private boolean isInsideRectangle(float x, float y) {

        int id = graph.getPanelIDatPoint(x, y);
        if (id < 0) return false;

        WindowPanel rect = (WindowPanel) graph.panels.get(id);
        Rectangle insideRect = new Rectangle(rect.x, rect.y + StockGraph.titleHeight, rect.width, rect.height - StockGraph.titleHeight);
        if (insideRect.contains(x, y)) {
            return true;
        }
        return false;
    }
}




