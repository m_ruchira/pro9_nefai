package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorKeltnerChannelsProperties;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: mevana
 * Date: 4/20/11
 * Time: 2:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class KeltnerChannels extends ChartProperties implements Indicator, FillArea {

    public final static byte KC_UPPER = -1;
    public final static byte KC_CENTER = 0;
    public final static byte KC_LOWER = 1;

    private int timePeriods;
    private float deviations;
    private ChartProperties innerSource;
    public byte keltnerChannelType = KC_CENTER;

    private boolean fillBands;
    private Color fillColor;
    private int transparency;

    private KeltnerChannels upper = null, lower = null;
    private String tableColumnHeading;
    private static final long serialVersionUID = UID_KELTNER_CHANNEL;

    public KeltnerChannels(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_KELTNER_CHANNELS");
        this.timePeriods = 21;
        this.deviations = 2.0f;
        this.keltnerChannelType = KC_CENTER;
        isIndicator = true;

        //fill bands related
        fillBands = true;
        fillColor = Color.blue;
        transparency = 30;

        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorKeltnerChannelsProperties idp = (IndicatorKeltnerChannelsProperties) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get(anID);
            this.setColor(idp.getUpColor());
            this.setColor(Color.GREEN);
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
            this.setDeviations(idp.getDeviations());
            
            this.setFillBands(idp.isFillBands());
            this.setFillColor(idp.getFillColor());
            this.setTransparency(idp.getTransparency());
        }
    }

    public void assignDefaultValues() {
        super.assignDefaultValues();
        this.timePeriods = 21;
        this.deviations = 2f;
        this.fillBands = true;
        this.fillColor = Color.blue;
        this.transparency = 30;

        if (upper != null && lower != null) {
            upper.assignDefaultValues();
            lower.assignDefaultValues();
        }
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof KeltnerChannels) {
            KeltnerChannels bb = (KeltnerChannels) cp;
            this.keltnerChannelType = bb.keltnerChannelType;
            this.innerSource = bb.innerSource;
            this.timePeriods = bb.timePeriods;
            this.deviations = bb.deviations;

            this.fillBands = bb.fillBands;
            this.fillColor = bb.fillColor;
            this.transparency = bb.transparency;

            if ((((KeltnerChannels) cp).getKeltnerChannelType() == KC_CENTER)) {
                this.upper = bb.upper;
                this.lower = bb.lower;
        }

            if (upper != null && lower != null) {
                this.upper.fillBands = bb.fillBands;
                this.upper.fillColor = bb.fillColor;
                this.upper.transparency = bb.transparency;

                this.lower.fillBands = bb.fillBands;
                this.lower.fillColor = bb.fillColor;
                this.lower.transparency = bb.transparency;
        }
        }
    }

    public boolean hasItsOwnScale() {
        return false;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        this.innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        byte stepMA = ChartRecord.STEP_1;
        byte stepATR = ChartRecord.STEP_2;

        float dev = this.deviations;

        ChartRecord cr;
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(2);
        }

        int loopBegin = this.timePeriods - 1;
        if (this.timePeriods > al.size()) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                GDM.removeIndicatorPoint(cr.Time, index, getID());
            }
            return;
        }

        MovingAverage.getMovingAverage(al, 0, timePeriods, MovingAverage.METHOD_EXPONENTIAL, getOHLCPriority(), stepMA);
        AverageTrueRange.getAverageTrueRange(al, 0, timePeriods, stepATR);

        switch (keltnerChannelType) {
            case KC_UPPER:
                dev = this.deviations;
                break;
            case KC_LOWER:
                dev = -this.deviations;
                break;
            case KC_CENTER:
                dev = 0.0f;
            default:
                break;
        }

        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(cr.getValue(stepMA) + dev * cr.getStepValue(stepATR));
            }
        }
    }

    /*public void setTableColumnHeading(String tableColumnHeading) {

    }*/

    public String getTableColumnHeading() {
        String suffix = " (" + timePeriods + "," + deviations + ")";
        switch (keltnerChannelType) {
            case KC_UPPER:
                return Language.getString("IND_TABLE_HEADING_KELTNER_CHANNELS_UP") + suffix;
            case KC_CENTER:
                return tableColumnHeading + suffix;
            case KC_LOWER:
                return Language.getString("IND_TABLE_HEADING_KELTNER_CHANNELS_DOWN") + suffix;
        }
        return "";
    }

    public String getShortName() {
        return getTableColumnHeading();
    }


    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.keltnerChannelType = Byte.parseByte(TemplateFactory.loadProperty(xpath, document, preExpression + "/KC_Type"));
        this.deviations = Float.parseFloat(TemplateFactory.loadProperty(xpath, document, preExpression+"/Deviations"));

        this.fillBands = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/fillBands"));
        this.transparency = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/transparency"));
        String[] saRGB = TemplateFactory.loadProperty(xpath, document, preExpression + "/fillColor").split(",");
        this.fillColor = new Color(Integer.parseInt(saRGB[0]), Integer.parseInt(saRGB[1]), Integer.parseInt(saRGB[2]));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "KC_Type", Byte.toString(this.keltnerChannelType));
        TemplateFactory.saveProperty(chart, document, "Deviations", Float.toString(this.deviations));

        TemplateFactory.saveProperty(chart, document, "fillBands", Boolean.toString(this.fillBands));
        TemplateFactory.saveProperty(chart, document, "transparency", Integer.toString(this.transparency));
        String strColor = this.fillColor.getRed() + "," + this.fillColor.getGreen() + "," + this.fillColor.getBlue();
        TemplateFactory.saveProperty(chart, document, "fillColor", strColor);
    }

    public void setUpper(ChartProperties upper) {
        if (upper instanceof KeltnerChannels) {
            this.upper = (KeltnerChannels) upper;
        }
    }

    public void setLower(ChartProperties lower) {
        if (lower instanceof KeltnerChannels) {
            this.lower = (KeltnerChannels) lower;
        }
    }

    public KeltnerChannels getUpper() {
        return upper;
    }

    public KeltnerChannels getLower() {
        return lower;
    }

    public void setKeltnerChannelType(byte alligatorType) {
        this.keltnerChannelType = alligatorType;
    }

    public byte getKeltnerChannelType() {
        return keltnerChannelType;
    }

    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public float getDeviations() {
        return deviations;
    }

    public void setDeviations(float tp) {
        deviations = tp;
    }

    public boolean isFillBands() {
        return fillBands;
    }

    public void setFillBands(boolean fillBands) {
        this.fillBands = fillBands;
    }

    public Color getFillColor() {
        return fillColor;
    }

    public Color getFillTransparentColor() {
        return new Color(fillColor.getRed(), fillColor.getGreen(), fillColor.getBlue(), transparency);
    }

    public void setFillColor(Color fillColor) {
        this.fillColor = fillColor;
    }

    public int getTransparency() {
        return transparency;
    }

    public void setTransparency(int transparency) {
        this.transparency = transparency;
    }
  
    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

}
