package com.isi.csvr.chart;

import com.isi.csvr.chart.customindicators.IndicatorConfigInfo;
import com.isi.csvr.shared.Language;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.tabbedpane.TWTabbedPane;

import javax.swing.*;
import javax.swing.border.TitledBorder;
import java.awt.*;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

/**
 * User: Pramoda
 * Date: Feb 21, 2007
 * Time: 12:48:46 PM
 */
public class InitialPropertyDialog extends JDialog {

    final static int RESULT_OK = 0;
    final static int RESULT_CANCEL = -1;

    final static Dimension DIM_DIALOG = new Dimension(320, 250);
    final static int CMB_HEIGHT = 22;
    final static int LBL_HEIGHT = 20;
    final static int GAP = 1;
    final static int LEFT = 5;
    final static int VADJ = 8;
    final static String METHOD_SET_COLOR = "setColor";
    final static String METHOD_WARNING_COLOR = "setWarningColor";
    final static String METHOD_SET_STYLE = "setStyle";
    final static String METHOD_SET_PEN_STYLE = "setPenStyle";
    final static String METHOD_SET_WIDTH = "setPenWidth";

    static ImageIcon icon = new ImageIcon("images/Theme"+Theme.getID()+"/graph_unchecked.gif");
    static Color[] colors = {Color.BLACK, Color.BLUE, Color.CYAN, Color.DARK_GRAY,
        Color.GRAY, Color.LIGHT_GRAY, Color.GREEN, Color.MAGENTA, Color.ORANGE, Color.PINK, Color.RED,
        Color.WHITE, Color.YELLOW};
    public static String COLOR_SEP = ",";
    static String[] colorNames = extractColorNames();
    static String[] lineStyles = {"Solid","Dot","Dash","DashDot","DashDotDot"};
    static Icon[] penStyles = {
        new ImageIcon("images/Theme"+Theme.getID()+"/graph_line_style_solid.gif"),
        new ImageIcon("images/Theme"+Theme.getID()+"/graph_line_style_dot.gif"),
        new ImageIcon("images/Theme"+Theme.getID()+"/graph_line_style_dash.gif"),
        new ImageIcon("images/Theme"+Theme.getID()+"/graph_line_style_dash_dot.gif"),
        new ImageIcon("images/Theme"+Theme.getID()+"/graph_line_style_dash_dot_dot.gif")
    };
    static Icon[] penWidths = {
        new ImageIcon("images/Theme"+Theme.getID()+"/graph_line_width_0p5.gif"),
        new ImageIcon("images/Theme"+Theme.getID()+"/graph_line_width_1p0.gif"),
        new ImageIcon("images/Theme"+Theme.getID()+"/graph_line_width_1p5.gif"),
        new ImageIcon("images/Theme"+Theme.getID()+"/graph_line_width_2p0.gif"),
        new ImageIcon("images/Theme"+Theme.getID()+"/graph_line_width_3p0.gif")
    };

    static String[] lineWidths = {"1.0","1.5","2.0","3.0","5.0"};
    

    private String NameSpace;
    private String Grammer;
    private String ClassName;
    private String LongName;
    private boolean isStrategy;

    private JTextField txtShortName;
    private JRadioButton rbOwnScale;
    private JRadioButton rbSourceScale;
    private JComboBox cmbUpColor;
    private JComboBox cmbDownColor;
    private JComboBox cmbChartStyle;
    private JComboBox cmbLineStyle;
    private JComboBox cmbLineThickness;
    private JCheckBox chkUseSameColor;

    private JButton btnOK;
    private JButton btnCancel;

    int result = RESULT_CANCEL;

    public InitialPropertyDialog() {
        setSize(330, 260);
    }

    public InitialPropertyDialog(IndicatorConfigInfo info) {
        setSize(330, 260);
        setTitle("Set Default Properties for " + info.LongName);
        setDefaultCloseOperation(DISPOSE_ON_CLOSE);
        setIconImage(Toolkit.getDefaultToolkit().getImage("images/Common/ClientServer.gif"));        

        this.NameSpace = info.NameSpace;
        this.Grammer = info.Grammer;
        this.ClassName = info.ClassName;
        this.LongName = info.LongName;
        this.isStrategy = info.IsStrategy;

        getContentPane().setLayout(new BorderLayout());
        TWTabbedPane tabbedPane = new TWTabbedPane();
        tabbedPane.addTab(Language.getString("GENERAL"), createGeneralPanel(info),
                Language.getString("SELECT_COLORS_AND_STYLES"));
        tabbedPane.addTab(Language.getString("ADVANCED"), createAdvancedPanel(info),
                Language.getString("ADVANCED_TOOLTIP"));
        getContentPane().add(tabbedPane, BorderLayout.CENTER);

        JPanel buttonPanel = new JPanel();
        btnOK = new JButton(Language.getString("OK"));
        btnOK.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                result = RESULT_OK;
                setVisible(false);
            }
        });
        buttonPanel.add(btnOK);
        btnCancel = new JButton(Language.getString("CANCEL"));
        btnCancel.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                result = RESULT_CANCEL;
                setVisible(false);            
            }
        });
        buttonPanel.add(btnCancel);
        getContentPane().add(buttonPanel, BorderLayout.SOUTH);
    }

    private JPanel createAdvancedPanel(IndicatorConfigInfo info) {
        JPanel panel = new JPanel();
        panel.setSize(DIM_DIALOG);
        panel.setLayout(null);

        //ShortName
        JLabel lblShortName = new JLabel(Language.getString("SHORT_NAME"));
        lblShortName.setBounds(LEFT,GAP,100,CMB_HEIGHT);
        panel.add(lblShortName);
        txtShortName = new JTextField();
        txtShortName.setText(info.ShortName);
        txtShortName.setBounds(LEFT,2+CMB_HEIGHT,100,CMB_HEIGHT);
        panel.add(txtShortName);
        JLabel lblMaxCharacters = new JLabel(Language.getString(Language.getString("MAX_10_CHAR")));
        lblMaxCharacters.setBounds(LEFT + 110,2+CMB_HEIGHT,170,CMB_HEIGHT);
        panel.add(lblMaxCharacters);

        JLabel lblInsertWindow = new JLabel(Language.getString("DEFAULT_INSERT_WINDOW"));
        lblInsertWindow.setBounds(LEFT,15+2*CMB_HEIGHT,300,CMB_HEIGHT);
        panel.add(lblInsertWindow);

        ButtonGroup buttonGroup = new ButtonGroup();
        rbSourceScale = new JRadioButton(Language.getString("SAME_INNER_WINDOW"));
        buttonGroup.add(rbSourceScale);
        rbSourceScale.setBounds(LEFT,15+3*CMB_HEIGHT,300,CMB_HEIGHT);
        panel.add(rbSourceScale);

        rbOwnScale = new JRadioButton(Language.getString("SEPERATE_INNER_WINDOW"));
        buttonGroup.add(rbOwnScale);
        rbOwnScale.setBounds(LEFT,15+4*CMB_HEIGHT,300,CMB_HEIGHT);
        panel.add(rbOwnScale);

        rbOwnScale.setSelected(Boolean.parseBoolean(info.HasItsOwnScale));
        rbSourceScale.setSelected(!rbOwnScale.isSelected());

        return panel;
    }


    public IndicatorConfigInfo getConfigInfo() {
        String hasItsOwnScale = Boolean.toString(rbOwnScale.isSelected());
        Color upColor = (Color)((ComboItem)cmbUpColor.getSelectedItem()).getValue();
        Color downColor = (Color)((ComboItem)cmbDownColor.getSelectedItem()).getValue();
        byte chartStyle = (byte)cmbChartStyle.getSelectedIndex();
        byte lineStyle = (byte)cmbLineStyle.getSelectedIndex();
        Float lineThickness = Float.parseFloat((String)((ComboItem)cmbLineThickness.getSelectedItem()).getValue());
        return new IndicatorConfigInfo(this.NameSpace, this.Grammer, this.ClassName, hasItsOwnScale,
                txtShortName.getText(), this.LongName, this.isStrategy, upColor, downColor, chartStyle,
                chkUseSameColor.isSelected(), lineStyle, lineThickness);
    }

/*
    private void txtShortName_TextChanged(object sender, EventArgs e) {
        btnOk.Enabled = ;
    }
*/


    public JPanel createGeneralPanel(final IndicatorConfigInfo target){
        JPanel panel = new JPanel();
        panel.setSize(DIM_DIALOG);
        panel.setLayout(null);

        JPanel pnlColor = new JPanel();
        pnlColor.setBorder(BorderFactory.createTitledBorder(BorderFactory.createEtchedBorder(),
                Language.getString("CHART_COLOR"), TitledBorder.LEADING,
                TitledBorder.DEFAULT_POSITION, Theme.getDefaultFont(Font.PLAIN, 12)));
        pnlColor.setLayout(null);
        pnlColor.setBounds(LEFT,VADJ+3+2*CMB_HEIGHT,160,4*CMB_HEIGHT+5+5);

        GraphLabel lblUpColor = new GraphLabel(Language.getString("UP"));
        lblUpColor.setBounds(LEFT, 2+CMB_HEIGHT,40,CMB_HEIGHT);
        cmbUpColor = getColorCombo(target, METHOD_SET_COLOR, target.Color, LEFT+40,2+CMB_HEIGHT,100,CMB_HEIGHT);

        final GraphLabel lblDownColor = new GraphLabel(Language.getString("DOWN_COLON"));
        lblDownColor.setBounds(LEFT,3+2*CMB_HEIGHT,40,CMB_HEIGHT);
        cmbDownColor = getColorCombo(target, METHOD_WARNING_COLOR, target.WarningColor, LEFT+40,3+2*CMB_HEIGHT,100,CMB_HEIGHT);

        chkUseSameColor = new JCheckBox(Language.getString("USE_SAME_COLOR"));
        chkUseSameColor.setBounds(LEFT,4+3*CMB_HEIGHT,130,CMB_HEIGHT);
        chkUseSameColor.setSelected(target.UseSameColor);
        if (target.UseSameColor){
            lblDownColor.setEnabled(false);
            cmbDownColor.setEnabled(false);
        }
        chkUseSameColor.setFont(Theme.getDefaultFont(Font.PLAIN, 12));
        chkUseSameColor.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                boolean useSame = chkUseSameColor.isSelected();
                target.UseSameColor = useSame;
                lblDownColor.setEnabled(!useSame);
                cmbDownColor.setEnabled(!useSame);
            }
        });

        GraphLabel lblLineStyle = new GraphLabel(Language.getString("LINE_STYLE"));
        lblLineStyle.setBounds(GAP+170,VADJ+3+2*CMB_HEIGHT,120,CMB_HEIGHT);
        cmbLineStyle = getPenStyleCombo(target, GAP+170,VADJ+4+3*CMB_HEIGHT,130,CMB_HEIGHT);

        GraphLabel lblLineWeight = new GraphLabel(Language.getString("LINE_WEIGHT"));
        lblLineWeight.setBounds(GAP+170,VADJ+5+4*CMB_HEIGHT,120,CMB_HEIGHT);
        cmbLineThickness = getPenWidthCombo(target, METHOD_SET_WIDTH,
                target.PenWidth,GAP+170,VADJ+6+5*CMB_HEIGHT,130,CMB_HEIGHT);

        GraphLabel lblStyle = new GraphLabel(Language.getString("CHART_STYLE"));
        lblStyle.setBounds(LEFT,GAP,100,CMB_HEIGHT);
        JComponent[] ca = {lblLineStyle,cmbLineStyle,lblLineWeight,cmbLineThickness};
        cmbChartStyle = getGraphStyleCombo(target, ca, LEFT,CMB_HEIGHT+2,100,CMB_HEIGHT);
        panel.add(lblStyle);
        panel.add(cmbChartStyle);

        pnlColor.add(lblUpColor);
        pnlColor.add(cmbUpColor);
        pnlColor.add(lblDownColor);
        pnlColor.add(cmbDownColor);
        pnlColor.add(chkUseSameColor);
        panel.add(pnlColor);
        panel.add(lblLineStyle);
        panel.add(cmbLineStyle);
        panel.add(lblLineWeight);
        panel.add(cmbLineThickness);

        return panel;
    }

    private JComboBox getPenStyleCombo(final IndicatorConfigInfo target,
                                              int x, int y, int w, int h){
        return getPenStyleCombo(target, METHOD_SET_STYLE, target.PenStyle, x, y, w, h);
    }

    private JComboBox getPenStyleCombo(final IndicatorConfigInfo target, final String method,
                                              byte penStyle, int x, int y, int w, int h){
        JComboBox cmbLineStyle = new JComboBox();
        cmbLineStyle.setBounds(x, y, w, h);
        cmbLineStyle.setEditable(false);
        cmbLineStyle.setRenderer(new ComboRenderer());
        cmbLineStyle.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        //int penWidth = getEquivalentWidth(target.getPenWidth());
        for (int i=0; i<lineStyles.length; i++) {
            ci = new ComboItem(lineStyles[i], lineStyles[i], penStyles[i], ComboItem.ITEM_NORMAL);
            cmbLineStyle.addItem(ci);
            if (i==penStyle){
                cmbLineStyle.setSelectedItem(ci);
            }
        }
        cmbLineStyle.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if (target!=null){
                    JComboBox cmb = (JComboBox)e.getSource();
                    if ((method.equals(METHOD_SET_STYLE))&&(target instanceof IndicatorConfigInfo)){
                        target.PenStyle = (byte)cmb.getSelectedIndex();
                    }
                }
            }
        });
        return cmbLineStyle;
    }


    private JComboBox getColorCombo(final IndicatorConfigInfo target, final String method,
                                           Color oldColor, int x, int y, int w, int h){
        JComboBox cmbColor = new JComboBox();
        cmbColor.setBounds(x, y, w, h);
        cmbColor.setEditable(false);
        cmbColor.setRenderer(new ComboRenderer());
        cmbColor.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        boolean customColor = true;
        for (int i=0; i<colors.length; i++) {
            ci = new ComboItem(colorNames[i], colors[i], new ComboImage(colors[i]), ComboItem.ITEM_NORMAL);
            cmbColor.addItem(ci);
            if (colors[i].equals(oldColor)){
                cmbColor.setSelectedIndex(i);
                customColor = false;
            }
        }
        ci = new ComboItem(Language.getString("CUSTOM"), oldColor, new ComboImage(oldColor), ComboItem.ITEM_CUSTOM_COLOR);
        cmbColor.addItem(ci);
        if (customColor){
            cmbColor.setSelectedItem(ci);
        }
        cmbColor.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if (target!=null){
                    JComboBox cmb = (JComboBox)e.getSource();
                    try {
                        Color color = (Color)((ComboItem)cmb.getSelectedItem()).getValue();
                        if (method.equals(METHOD_SET_COLOR)) {
                            target.Color = color;
                        } else if (method.equals(METHOD_WARNING_COLOR)) {
                            target.WarningColor = color;
                        }
                    }
                    catch (Exception ex) {
                    }
                }
            }
        });
        return cmbColor;
    }

    private JComboBox getPenWidthCombo(final Object target, final String method,
                                              float penWidth, int x, int y, int w, int h){
        JComboBox cmbLineWidth = new JComboBox();
        cmbLineWidth.setBounds(x, y, w, h);
        cmbLineWidth.setEditable(false);
        cmbLineWidth.setRenderer(new ComboRenderer());
        cmbLineWidth.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        int equiW = getEquivalentWidth(penWidth);
        for (int i=0; i<lineWidths.length; i++) {
            ci = new ComboItem(lineWidths[i], lineWidths[i], penWidths[i], ComboItem.ITEM_NORMAL);
            cmbLineWidth.addItem(ci);
            if (equiW==i){
                cmbLineWidth.setSelectedItem(ci);
            }
        }
        cmbLineWidth.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if (target!=null){
                    JComboBox cmb = (JComboBox)e.getSource();
                    Object obj = ((ComboItem)cmb.getSelectedItem()).getValue();
                    if (method.equals(METHOD_SET_WIDTH)){
                        if (target instanceof IndicatorConfigInfo){
                            ((IndicatorConfigInfo)target).PenWidth = Float.parseFloat((String)obj);
                        }
                    }
                }
            }
        });
        return cmbLineWidth;
    }

    private JComboBox getGraphStyleCombo(final IndicatorConfigInfo target, final JComponent[] subComps, int x, int y, int w, int h){
        JComboBox cmbStyle = new JComboBox();
        cmbStyle.setBounds(x, y, w, h);
        cmbStyle.setEditable(false);
        String[] sa = GraphFrame.getGraphStyles();
        Icon[] ia = GraphToolBar.styleIcon;
        cmbStyle.setRenderer(new ComboRenderer());
        cmbStyle.setFont(StockGraph.font_BOLD_10);
        ComboItem ci;
        int breakIndex = 1;

        for (int i=0; i<sa.length; i++) {
            ci = new ComboItem(sa[i],sa[i], ia[i], ComboItem.ITEM_NORMAL);
            cmbStyle.addItem(ci);
            if (i==target.ChartStyle){
                cmbStyle.setSelectedIndex(i);
            }
            if (i>=breakIndex) break;
        }
        if (cmbStyle.getSelectedIndex()==StockGraph.INT_GRAPH_STYLE_LINE){
            setCompsEnabled(subComps, true);
        }else{
            setCompsEnabled(subComps, false);
        }
        cmbStyle.addActionListener(new ActionListener(){
            public void actionPerformed(ActionEvent e){
                if (target!=null){
                    JComboBox cmb = (JComboBox)e.getSource();
                    if (cmb.getSelectedIndex()>=0){
                        target.ChartStyle = (byte)cmb.getSelectedIndex();
                    }
                    if (cmb.getSelectedIndex()==StockGraph.INT_GRAPH_STYLE_LINE){
                        setCompsEnabled(subComps, true);
                    }else{
                        setCompsEnabled(subComps, false);
                    }
                }
            }
        });
        return cmbStyle;
    }

    private void setCompsEnabled(JComponent[] ca, boolean enabled){
        for (JComponent aCa : ca) {
            aCa.setEnabled(enabled);
        }
    }


    private int getEquivalentWidth(float pw){
        if (pw<=1.1f){
            return 0;
        }
        else if (pw<=1.6f){
            return 1;
        }
        else if (pw<=2.1f){
            return 2;
        }
        else if (pw<=3.1f){
            return 3;
        }
        else return 4;
    }


    public static String[] extractColorNames(){
        String[] sa = null;
        try {
            sa = Language.getString("COLOR_NAMES").split(COLOR_SEP);
        }
        catch (Exception ex) {
        }
        return sa;
    }


}

