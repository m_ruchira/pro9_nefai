package com.isi.csvr.chart;

import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.SharedMethods;
import com.isi.csvr.shared.Stock;
import com.isi.csvr.shared.TWButton;
import com.isi.csvr.theme.Theme;
import com.isi.csvr.datastore.DataStore;
import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.chart.options.ChartObjectElementList;
import com.isi.csvr.chart.options.ChartOptionsWindow;
import com.isi.csvr.chart.options.BottomToolBarData;
import com.isi.csvr.chart.options.ChartOptions;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.event.PopupMenuEvent;
import javax.swing.event.PopupMenuListener;
import java.awt.*;
import java.awt.event.*;
import java.awt.image.BufferedImage;
import java.util.*;
import java.io.File;

/**
 * <p>Title: TW International/Tadawul</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 *
 * @author Udaka Liyanapathirana
 * @version 1.0
 */

public class BottomToolBar extends JPanel implements ActionListener,
        PopupMenuListener, FocusListener {

    private Point mousePoint;
    private final int smblCols = 5;
    public static final char[] caTextSymbols = {
            0x2022, 0x25C6, 0x261B, 0x261E, 0x263A,
            0x263B, 0x270C, 0x2714, 0x2716, 0x2762,
            0x278A, 0x278B, 0x278C, 0x278D, 0x278E,
            0x278F, 0x2790, 0x2791, 0x2792, 0x2793,
            0x2798, 0x2799, 0x279A, 0x27A0, 0x21E7,
            0x21E9, 0x224F, 0x25B2, 0x25BC, 0x25C4
    };
    private Cursor CURSOR_LINE_SLOPE = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_line_slope.gif");
    private Cursor CURSOR_ARROW_LINE_SLOPE = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_arrow_line_slope.gif");
    private Cursor CURSOR_LINE_HORIZ = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_line_horiz.gif");
    private Cursor CURSOR_LINE_VERTI = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_line_verti.gif");
    private Cursor CURSOR_RECT = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_rect.gif");
    private Cursor CURSOR_RECT_SELECTION = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_rect.gif");
    private Cursor CURSOR_TEXT = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_text.gif");
    private Cursor CURSOR_ELLIPSE = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_ellipse.gif");
    private Cursor CURSOR_POLYGON = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_rect.gif");
    private Cursor CURSOR_REGRESSION = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_regression_line.gif");
    private Cursor CURSOR_FIB_ARCS = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_fibArcs.gif");
    private Cursor CURSOR_FIB_FANS = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_fibFans.gif");
    private Cursor CURSOR_FIB_EXTENSIONS = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_fib_extensions.gif");
    private Cursor CURSOR_ZIG_ZAG = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_zig_zag.gif");
    private Cursor CURSOR_ARC = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_arc.gif");
    private Cursor CURSOR_FIB_RETRC = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_fibRetrc.gif");
    private Cursor CURSOR_FIB_ZONES = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_fibZones.gif");
    private Cursor CURSOR_STD_ERROR_CHANNEL = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_stdErr.gif");
    private Cursor CURSOR_GANN_LINE = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_gannLine.gif");
    private Cursor CURSOR_GANN_FAN = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_gannFan.gif");
    private Cursor CURSOR_GANN_GRID = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_gannGrid.gif");
    private Cursor CURSOR_RAFF_REG_CHANNEL = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_raffRegChannel.gif");
    private Cursor CURSOR_STD_DEV_CHANNEL = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_stdDevChannel.gif");
    private Cursor CURSOR_EQU_DIST_CHANNEL = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_equDistChannel.gif");
    private Cursor CURSOR_HAND_DRAGGIN = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_handDrag.gif");
    private Cursor CURSOR_CYCLE_LINE = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_cycle.gif");
    private Cursor CURSOR_PITCHFORK = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_pitchfork.gif");


    private GraphFrame activeGraph = null;
    private BorderLayout borderLayout1 = new BorderLayout(0, 1);
    public ChartFrame parent = null;
    //#######################################################################//
    // vars for control panel and cards
    protected final int NumberOfCards = 7;
    protected final int PrefHt = 20;
    protected final int PrefTxtWd = 40;
    protected final int PrefLblHt = 16;
    //protected final int PrefCmbWd = 50;
    protected final int PrefCmbWd = 80;
    protected final int PrefCmbHt = 18;

    protected final int PrefBtnWd = 20;
    protected final int PrefBtnHt = 20;

    protected int activeCard = 0;
    protected final int H_GAP = 3;

    public int ObjectTobeDrawn = AbstractObject.INT_NONE;
    public int symbolTobeDrawn;

    FlowLayout PanelContainerLayout = new FlowLayout(FlowLayout.LEADING, H_GAP, 1);
    //FlowLayout ButtonContainerLayout = new FlowLayout(FlowLayout.TRAILING, 1,0);
    BorderLayout navigatorLayout = new BorderLayout(1, 1);
    private JPanel CardPanels[] = new JPanel[NumberOfCards];
    private int CardLengths[] = new int[NumberOfCards];
    private JPanel CardContainerPanel = new JPanel();
    private JPanel rightPanel = new JPanel();
    private JPanel navigaterPanel = new JPanel();
    private JPanel nextPreviouspnl = new JPanel();

    //adding buttons to the rightmost corner of the bottom toolbar
    JButton btnResetVZoom = new JButton("");
    JButton btnResetHZoom = new JButton("");

    static JButton btnSmartZoom = new JButton("");
    JButton btnZoomIn = new JButton("");
    JButton btnZoomOut = new JButton("");
    JButton btnBoxZoom = new JButton("");
    JButton btnDragZoom = new JButton("");
    JButton btnZoomReset = new JButton("");
    JButton btnHandDragChart = new JButton("");

    JButton btnBack = new JButton("");
    JButton btnNext = new JButton("");

    JPanel pnlLineToolbar = new JPanel();
    FlowLayout lineToolbarLayout = new FlowLayout(FlowLayout.LEFT, 2, 0);
    FlowLayout lineToolbarRightLayout = new FlowLayout(FlowLayout.RIGHT, 2, 0);
    JToggleButton btnNeutral = new JToggleButton();
    JToggleButton btnArrow = new JToggleButton();
    JToggleButton btnSlopeLine = new JToggleButton();
    JToggleButton btnArrowSlopeLine = new JToggleButton();//todo do the need ful for arrow btnslope line
    JToggleButton btnHorizLine = new JToggleButton();
    JToggleButton btnVertiLine = new JToggleButton();
    JToggleButton btnRectangle = new JToggleButton();
    JToggleButton btnEllipse = new JToggleButton();
    JToggleButton btnTextNote = new JToggleButton();
    JToggleButton btnRegression = new JToggleButton();
    JToggleButton btnCycleLine = new JToggleButton();
    JToggleButton btnPitchfork = new JToggleButton();
    JToggleButton btnArc = new JToggleButton();

    JToggleButton btnTextSymbol = new JToggleButton();
    JButton btnTextSymblAro = new JButton();
    JButton btnDrawingToolsAro = new JButton();

    JPanel pnlFibonacciToolbar = new JPanel();
    JToggleButton btnFibArcs = new JToggleButton();
    JToggleButton btnFibFans = new JToggleButton();
    JToggleButton btnFibRetracements = new JToggleButton();
    JToggleButton btnFibExtensions = new JToggleButton();
    JToggleButton btnZigZag = new JToggleButton();
    JToggleButton btnFibZones = new JToggleButton();

    JPanel pnlChannelsToolbar = new JPanel();
    JToggleButton btnStdErrChannel = new JToggleButton();
    JToggleButton btnRafChannel = new JToggleButton();
    JToggleButton btnStdDevChannel = new JToggleButton();
    JToggleButton btnEquDistChannel = new JToggleButton();

    JPanel pnlTemplatesToolBar = new JPanel();
    JButton btnShortTermTemplate = new JButton();
    JButton btnMidTermTemplate = new JButton();
    JButton btnLongTermTemplate = new JButton();

    JPanel pnlGannAnalisysToolbar = new JPanel();
    JToggleButton btnGannLine = new JToggleButton();
    JToggleButton btnGannFan = new JToggleButton();
    JToggleButton btnGannGrid = new JToggleButton();

    JPanel pnlTextSymbols = new JPanel(new FlowLayout(FlowLayout.LEADING, 0, 0));
    ButtonGroup lineBtnGrp = new ButtonGroup();
    ButtonGroup lineBtnGrpPop = new ButtonGroup();

    ChartSymbolSearchComponent cmbSymbol = new ChartSymbolSearchComponent(this, false); // Base Symbol
    JButton btnSymbol = new JButton("...");    // Search Symbols
    JButton btnCompare = new JButton("...");    // Search Symbols
    ChartSymbolSearchComponent cmbCompare = new ChartSymbolSearchComponent(this, true); // Compare With Symbol
    JLabel lbSymbol = new JLabel();     // Base Symbol
    JLabel lbCompare = new JLabel();     // Compare With Symbol

    JButton btnNextSymbol = new JButton();
    JButton btnPrevSymbol = new JButton();

    private ArrayList<String> symbolList;

    SmartZoomWindow smartZoomWindow = null;
    public static boolean isSmartZoomFrameVisible = false;

    public static final int STATUS_NONE = 0;
    public static final int STATUS_SMART_ZOOM = 1;
    public static final int STATUS_ZOOM_IN = 2;
    public static final int STATUS_ZOOM_OUT = 3;
    public static final int STATUS_BOX_ZOOM = 4;
    public static final int STATUS_DRAG_ZOOM = 5;
    public static final int STATUS_CHART_DRAGGING = 6;
    public static final int STATUS_FIB_PRICE = 7;
    public static final int STATUS_FIB_EXT = 8;
    public static final int STATUS_ERASOR = 9;
    public static final int STATUS_REC_SELECTION = 10;
    public static final int STATUS_ZIG_ZAG = 11;
    public static final int STATUS_ARC = 12;

    public static int CURSOR_STATUS = STATUS_NONE;

    protected static final int INT_SHORT_TERM = 101;
    protected static final int INT_LONG_TERM = 102;
    protected static final int INT_MEDIUM_TERM = 103;

    protected static final int INT_HAND_DRAG = 104;
    protected static final int INT_SMART_ZOOM = 105;
    protected static final int INT_ZOOM_IN = 106;
    protected static final int INT_ZOOM_OUT = 107;
    protected static final int INT_BOX_ZOOM = 108;
    protected static final int INT_DRAG_ZOOM = 109;
    protected static final int INT_RESET_V_ZOOM = 110;
    protected static final int INT_RESET_H_ZOOM = 111;
    protected static final int INT_RESET_ZOOM = 112;


    /**
     * **************pop up tool buttons  ************************
     */
    //Todo later go for action commands

    //drawing
    private JToggleButton lbtnArrow;
    private JToggleButton lbtnSlopeLine;
    private JToggleButton lbtnArrowSlopeLine;
    private JToggleButton lbtnHorizLine;
    private JToggleButton lbtnVertiLine;
    private JToggleButton lbtnRectangle;
    private JToggleButton lbtnEllipse;
    private JToggleButton lbtnTextNote;
    private JToggleButton lbtnRegression;
    private JToggleButton lbtnZigZag;
    private JToggleButton lbtnArc;
    private JToggleButton lbtnPitchfork;
    private JToggleButton lbtnCycleLine;

    //fibonaci
    private JToggleButton lbtnFibArcs;
    private JToggleButton lbtnFibFans;
    private JToggleButton lbtnFibRetracements;
    private JToggleButton lbtnFibExtensions;
    private JToggleButton lbtnFibZones;

    //chanel
    private JToggleButton lbtnStdErrChannel;
    private JToggleButton lbtnRafChannel;
    private JToggleButton lbtnStdDevChannel;
    private JToggleButton lbtnEquDistChannel;

    //Term
    private JButton lbtnShortTermTemplate;
    private JButton lbtnMidTermTemplate;
    private JButton lbtnLongTermTemplate;

    //gann
    private JToggleButton lbtnGannLine;
    private JToggleButton lbtnGannFan;
    private JToggleButton lbtnGannGrid;

    //zoomm
    private JButton lbtnResetVZoom;
    private JButton lbtnResetHZoom;
    private JButton lbtnSmartZoom;
    private JButton lbtnZoomIn;
    private JButton lbtnZoomOut;
    private JButton lbtnBoxZoom;
    private JButton lbtnDragZoom;
    private JButton lbtnZoomReset;
    private JButton lbtnHandDragChart;

    public final Hashtable<Integer, Hashtable<Integer, Boolean>> hashtoolsets = new Hashtable<Integer, Hashtable<Integer, Boolean>>();
    public final Hashtable<Integer, Boolean> hashDrawing = BottomToolBarData.getSavedToolBarDataForGroup(ChartObjectElementList.DRAWING_TOOLS);
    public final Hashtable<Integer, Boolean> hashFibonaci = BottomToolBarData.getSavedToolBarDataForGroup(ChartObjectElementList.FIBONACI_TOOLS);
    public final Hashtable<Integer, Boolean> hashChanel = BottomToolBarData.getSavedToolBarDataForGroup(ChartObjectElementList.CHANNEL_TOOLS);
    public final Hashtable<Integer, Boolean> hashTerm = BottomToolBarData.getSavedToolBarDataForGroup(ChartObjectElementList.TERM_TOOLS);
    public final Hashtable<Integer, Boolean> hashGann = BottomToolBarData.getSavedToolBarDataForGroup(ChartObjectElementList.GANN_TOOLS);
    public final Hashtable<Integer, Boolean> hashZoom = BottomToolBarData.getSavedToolBarDataForGroup(ChartObjectElementList.ZOOM_TOOLS);
    public boolean isChartOptionDirty = false;

    private TWButton btnFav;
    //#######################################################################//

    public BottomToolBar(ChartFrame cf) {
        try {
            parent = cf;
            symbolTobeDrawn = 4;
            jbInit();
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    void jbInit() throws Exception {
        this.setLayout(borderLayout1);
        this.setPreferredSize(new Dimension(200, PrefHt + 2));
        addActionListeners();
        setPreferredBounds();
        //setIcons();
        setToolTips();
        setBorders();
        setOpaque(true);

        //-----------------------------------------------------------
        lbSymbol.setFont(new java.awt.Font("Dialog", 1, 10));
        lbSymbol.setText(Language.getString("BASE_SYMBOL"));

        cmbSymbol.setFont(new java.awt.Font("Dialog", 1, 10));
        //-----------------------------------------------------------

        lbCompare.setFont(new java.awt.Font("Dialog", 1, 10));
        lbCompare.setText(Language.getString("COMPARE_WITH"));
        cmbCompare.setFont(new java.awt.Font("Dialog", 1, 10));
        //-----------------------------------------------------------

        // constructing card panels
        for (int i = 0; i < NumberOfCards; i++) {
            CardPanels[i] = new JPanel();
            CardPanels[i].setName("CP" + i);
            CardPanels[i].setLayout(new FlowLayout(FlowLayout.LEFT, 3, 0));
        }

        pnlLineToolbar.setOpaque(true);

        //-----------------------------------------------------------
        pnlLineToolbar.setLayout(lineToolbarLayout);
        pnlFibonacciToolbar.setLayout(lineToolbarLayout);
        pnlChannelsToolbar.setLayout(lineToolbarLayout);
        CardContainerPanel.setLayout(PanelContainerLayout);
        navigaterPanel.setLayout(navigatorLayout);
        if (Language.isLTR()) {
            rightPanel.setLayout(lineToolbarRightLayout);
        } else {
            rightPanel.setLayout(lineToolbarLayout);
        }
        pnlTemplatesToolBar.setLayout(lineToolbarLayout);
        pnlGannAnalisysToolbar.setLayout(lineToolbarLayout);

        //-----------------------------------------------------------

        lineBtnGrp.add(btnNeutral);
        lineBtnGrp.add(btnArrow);
        lineBtnGrp.add(btnSlopeLine);
        lineBtnGrp.add(btnArrowSlopeLine);
        lineBtnGrp.add(btnHorizLine);
        lineBtnGrp.add(btnVertiLine);
        lineBtnGrp.add(btnRectangle);
        lineBtnGrp.add(btnEllipse);
        lineBtnGrp.add(btnTextNote);
        lineBtnGrp.add(btnZigZag);
        lineBtnGrp.add(btnRegression);
        lineBtnGrp.add(btnTextSymbol);
        lineBtnGrp.add(btnCycleLine);
        lineBtnGrp.add(btnArc);
        lineBtnGrp.add(btnPitchfork);

        //fibonacci
        lineBtnGrp.add(btnFibArcs);
        lineBtnGrp.add(btnFibFans);
        lineBtnGrp.add(btnFibRetracements);
        lineBtnGrp.add(btnFibExtensions);
        lineBtnGrp.add(btnZigZag);
        lineBtnGrp.add(btnFibZones);

        lineBtnGrp.add(btnStdErrChannel);
        lineBtnGrp.add(btnRafChannel);
        lineBtnGrp.add(btnStdDevChannel);
        lineBtnGrp.add(btnEquDistChannel);

        lineBtnGrp.add(btnShortTermTemplate);
        lineBtnGrp.add(btnMidTermTemplate);
        lineBtnGrp.add(btnLongTermTemplate);

        lineBtnGrp.add(btnGannLine);
        lineBtnGrp.add(btnGannFan);
        lineBtnGrp.add(btnGannGrid);

        pnlLineToolbar.add(btnNeutral);

        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_LINE_SLOPE)) {
            pnlLineToolbar.add(btnSlopeLine);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_ARROW_LINE_SLOPE)) {
            pnlLineToolbar.add(btnArrowSlopeLine);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_LINE_HORIZ)) {
            pnlLineToolbar.add(btnHorizLine);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_LINE_VERTI)) {
            pnlLineToolbar.add(btnVertiLine);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_RECT)) {
            pnlLineToolbar.add(btnRectangle);
        }

        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_ELLIPSE)) {
            pnlLineToolbar.add(btnEllipse);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_TEXT)) {
            pnlLineToolbar.add(btnTextNote);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_REGRESSION)) {
            pnlLineToolbar.add(btnRegression);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_ZIG_ZAG)) {
            pnlLineToolbar.add(btnZigZag);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_SYMBOL)) {
            pnlLineToolbar.add(pnlTextSymbols);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_CYCLE_LINE)) {
            pnlLineToolbar.add(btnCycleLine);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_ARC)) {
            pnlLineToolbar.add(btnArc);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_ANDREWS_PITCHFORK)) {
            pnlLineToolbar.add(btnPitchfork);
        }

        pnlTextSymbols.add(btnTextSymbol);
        pnlTextSymbols.add(btnTextSymblAro);
        btnNeutral.setVisible(false);

        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_FIBONACCI_ARCS)) {
            pnlFibonacciToolbar.add(btnFibArcs);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_FIBONACCI_FANS)) {
            pnlFibonacciToolbar.add(btnFibFans);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_FIBONACCI_RETRACEMENTS)) {
            pnlFibonacciToolbar.add(btnFibRetracements);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_FIBONACCI_EXTENSIONS)) {
            pnlFibonacciToolbar.add(btnFibExtensions);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_FIBONACCI_ZONES)) {
            pnlFibonacciToolbar.add(btnFibZones);
        }

        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_STD_ERROR_CHANNEL)) {
            pnlChannelsToolbar.add(btnStdErrChannel);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_RAFF_REGRESSION)) {
            pnlChannelsToolbar.add(btnRafChannel);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_STD_DEV_CHANNEL)) {
            pnlChannelsToolbar.add(btnStdDevChannel);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_EQUI_DIST_CHANNEL)) {
            pnlChannelsToolbar.add(btnEquDistChannel);
        }

        if (BottomToolBarData.isButtonVisible(BottomToolBar.INT_SHORT_TERM)) {
            pnlTemplatesToolBar.add(btnShortTermTemplate);
        }
        if (BottomToolBarData.isButtonVisible(BottomToolBar.INT_MEDIUM_TERM)) {
            pnlTemplatesToolBar.add(btnMidTermTemplate);
        }
        if (BottomToolBarData.isButtonVisible(BottomToolBar.INT_LONG_TERM)) {
            pnlTemplatesToolBar.add(btnLongTermTemplate);
        }

        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_GANN_LINE)) {
            pnlGannAnalisysToolbar.add(btnGannLine);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_GANN_FAN)) {
            pnlGannAnalisysToolbar.add(btnGannFan);
        }
        if (BottomToolBarData.isButtonVisible(AbstractObject.INT_GANN_GRID)) {
            pnlGannAnalisysToolbar.add(btnGannGrid);
        }

        //-----------------------------------------------------------

        if (BottomToolBarData.isButtonVisible(BottomToolBar.INT_HAND_DRAG)) {
            rightPanel.add(btnHandDragChart);
        }
        if (BottomToolBarData.isButtonVisible(BottomToolBar.INT_SMART_ZOOM)) {
            rightPanel.add(btnSmartZoom);
        }
        if (BottomToolBarData.isButtonVisible(BottomToolBar.INT_ZOOM_IN)) {
            rightPanel.add(btnZoomIn);
        }
        if (BottomToolBarData.isButtonVisible(BottomToolBar.INT_ZOOM_OUT)) {
            rightPanel.add(btnZoomOut);
        }
        if (BottomToolBarData.isButtonVisible(BottomToolBar.INT_BOX_ZOOM)) {
            rightPanel.add(btnBoxZoom);
        }

        if (BottomToolBarData.isButtonVisible(BottomToolBar.INT_DRAG_ZOOM)) {
            rightPanel.add(btnDragZoom);
        }

        if (BottomToolBarData.isButtonVisible(BottomToolBar.INT_RESET_V_ZOOM)) {
            rightPanel.add(btnResetVZoom);
        }
        if (BottomToolBarData.isButtonVisible(BottomToolBar.INT_RESET_H_ZOOM)) {
            rightPanel.add(btnResetHZoom);
        }

        if (BottomToolBarData.isButtonVisible(BottomToolBar.INT_RESET_ZOOM)) {
            rightPanel.add(btnZoomReset);
        }

        rightPanel.add(btnDrawingToolsAro);
        rightPanel.setBorder(BorderFactory.createEmptyBorder());

        if (Language.isLTR()) {
            navigaterPanel.add(rightPanel, BorderLayout.CENTER);
            nextPreviouspnl.setLayout(lineToolbarLayout);
            nextPreviouspnl.add(btnNext);
            nextPreviouspnl.add(btnBack);
            navigaterPanel.add(nextPreviouspnl, BorderLayout.EAST);
        } else {
            navigaterPanel.add(rightPanel, BorderLayout.CENTER);
            nextPreviouspnl.setLayout(lineToolbarLayout);
            nextPreviouspnl.add(btnBack);
            nextPreviouspnl.add(btnNext);
            navigaterPanel.add(nextPreviouspnl, BorderLayout.WEST);
        }

        //-----------------------------------------------------------
        CardPanels[0].add(lbSymbol);        //Card1
        CardPanels[0].add(cmbSymbol);       //Card1
        CardPanels[0].add(btnPrevSymbol);
        CardPanels[0].add(btnNextSymbol);

        //CardPanels[0].add(btnSymbol);       //Card1
        CardPanels[1].add(lbCompare);       //Card2
        CardPanels[1].add(cmbCompare);      //Card2

        //CardPanels[1].add(btnCompare);      //Card2
        CardPanels[2].add(pnlLineToolbar);  //Card3
        CardPanels[3].add(pnlFibonacciToolbar);  //Card4
        CardPanels[4].add(pnlChannelsToolbar);
        CardPanels[5].add(pnlTemplatesToolBar);
        CardPanels[6].add(pnlGannAnalisysToolbar);
        //-----------------------------------------------------------

        for (int i = 0; i < NumberOfCards; i++) {
            CardContainerPanel.add(CardPanels[i]);
        }

        this.add(CardContainerPanel, BorderLayout.CENTER);
        if (Language.isLTR())
            this.add(navigaterPanel, BorderLayout.EAST);
        else
            this.add(navigaterPanel, BorderLayout.WEST);


    }

    public static JButton getBtnSmartZoom() {
        return btnSmartZoom;
    }

    public void applyTheme() {
        applyBGColor(this, Theme.getColor("GRPH_BOTTOM_TOOLBAR_BGCOLOR"));
        CURSOR_LINE_SLOPE = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_line_slope.gif");
        CURSOR_ARROW_LINE_SLOPE = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_arrow_line_slope.gif");
        CURSOR_LINE_HORIZ = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_line_horiz.gif");
        CURSOR_LINE_VERTI = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_line_verti.gif");
        CURSOR_RECT = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_rect.gif");
        CURSOR_RECT_SELECTION = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_rect.gif");
        CURSOR_TEXT = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_text.gif");
        CURSOR_ELLIPSE = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_ellipse.gif");
        CURSOR_POLYGON = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_rect.gif");
        CURSOR_REGRESSION = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_regression_line.gif");
        CURSOR_FIB_ARCS = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_fibArcs.gif");
        CURSOR_FIB_FANS = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_fibFans.gif");
        CURSOR_FIB_EXTENSIONS = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_fib_extensions.gif");
        CURSOR_ZIG_ZAG = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_zig_zag.gif");
        CURSOR_ARC = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_arc.gif");
        CURSOR_FIB_RETRC = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_fibRetrc.gif");
        CURSOR_FIB_ZONES = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_fibZones.gif");
        CURSOR_STD_ERROR_CHANNEL = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_stdErr.gif");
        CURSOR_GANN_LINE = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_gannLine.gif");
        CURSOR_GANN_FAN = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_gannFan.gif");
        CURSOR_GANN_GRID = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_gannGrid.gif");
        CURSOR_RAFF_REG_CHANNEL = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_raffRegChannel.gif");
        CURSOR_STD_DEV_CHANNEL = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_stdDevChannel.gif");
        CURSOR_EQU_DIST_CHANNEL = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_equDistChannel.gif");
        CURSOR_HAND_DRAGGIN = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_handDrag.gif");
        CURSOR_CYCLE_LINE = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_cycle.gif");
        CURSOR_PITCHFORK = createCursor("images/Theme" + Theme.getID() + "/charts/graph_obj_pitchfork.gif");
        setTextSymbolIcons();
    }

    public void applyBGColor(Component c, Color color) {
        try {

            if (c instanceof JComponent) {
                ((JComponent) c).setOpaque(true);
            }
            if (c instanceof JComboBox) {
                // do nothing
            } else if (c instanceof java.awt.Container) {
                c.setBackground(color);
                java.awt.Container container = (java.awt.Container) c;

                int ncomponents = container.getComponentCount();
                for (int i = 0; i < ncomponents; ++i) {
                    applyBGColor(container.getComponent(i), color);
                }
            } else {
                c.setBackground(color);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setPreferredBounds() {
        CardContainerPanel.setPreferredSize(new Dimension(0, PrefHt + 2));

        //should multiply by 12 since there are 12 buttons in the rightPanel(zoon in + zoom out + other six buttons)
        //changed by charithn
        navigaterPanel.setPreferredSize(new Dimension((PrefBtnHt + 2) * 12, PrefHt + 2));
        navigaterPanel.setMinimumSize(navigaterPanel.getPreferredSize());
        CardContainerPanel.setMinimumSize(new Dimension(150, 17));

        btnSymbol.setBounds(new Rectangle(154, 3, 18, 18));
        btnCompare.setBounds(new Rectangle(331, 3, 18, 18));

        cmbSymbol.setPreferredSize(new Dimension(PrefCmbWd, PrefCmbHt));
        btnSymbol.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));

        cmbCompare.setPreferredSize(new Dimension(PrefCmbWd, PrefCmbHt));
        btnCompare.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnNextSymbol.setPreferredSize(new Dimension(PrefBtnWd / 2, PrefBtnHt));
        btnPrevSymbol.setPreferredSize(new Dimension(PrefBtnWd / 2, PrefBtnHt));

        btnResetVZoom.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnResetHZoom.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));

        btnSmartZoom.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnZoomIn.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnZoomOut.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnDragZoom.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnBoxZoom.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnZoomReset.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnHandDragChart.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnBack.setPreferredSize(new Dimension(PrefBtnHt / 2, PrefBtnHt));
        btnNext.setPreferredSize(new Dimension(PrefBtnHt / 2, PrefBtnHt));

        btnNeutral.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnArrow.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnSlopeLine.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnArrowSlopeLine.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnHorizLine.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnVertiLine.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnRectangle.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnEllipse.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnTextNote.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnCycleLine.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnArc.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnPitchfork.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnRegression.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnTextSymbol.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnTextSymblAro.setPreferredSize(new Dimension(PrefBtnWd / 2, PrefBtnHt));
        btnDrawingToolsAro.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        pnlTextSymbols.setPreferredSize(new Dimension(2 * PrefBtnWd, PrefBtnHt));
        //fibonacci
        btnFibArcs.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnFibFans.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnFibRetracements.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnFibExtensions.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnZigZag.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnFibZones.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));

        btnStdErrChannel.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnRafChannel.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnStdDevChannel.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnEquDistChannel.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));

        btnShortTermTemplate.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnMidTermTemplate.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnLongTermTemplate.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));

        btnGannLine.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnGannFan.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        btnGannGrid.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
    }

    //better to change for get action command
    public void actionPerformed(ActionEvent e) {
        Object obj = e.getSource();
        if (activeGraph != null) {

            if (isAbstractObject(obj)) {
                activeGraph.setBoxZoomEnabled(false);
                activeGraph.setDragZoomEnabled(false);
                activeGraph.setZoomInEnabled(false);
                activeGraph.setZoomOutEnabled(false);
                activeGraph.setchartBodyDraggingEnabled(false);
                //activeGraph.graph.setCursor(Cursor.getDefaultCursor());
            }
            if (obj == btnSymbol) activeGraph.searchClicked();
            else if (obj == btnCompare) activeGraph.compareClicked();
            else if (obj == btnBack) navigateBack();
            else if (obj == btnNext) navigateNext();
            else if (obj == btnTextSymblAro) popTextSymbols();
//            else if (obj == btnTextSymblAro)  popBottomTools();
//            else if (obj == btnDrawingToolsAro) popBottomTools();
//            else if (obj == btnDrawingToolsAro) popBottomTools();
            else if (obj == btnDrawingToolsAro) popBottomAllTools();

            else if (obj == btnShortTermTemplate || obj == lbtnShortTermTemplate)
                applyTemplate(TemplateFactory.SHORT_TERM);
            else if (obj == btnMidTermTemplate || obj == lbtnMidTermTemplate) applyTemplate(TemplateFactory.MID_TERM);
            else if (obj == btnLongTermTemplate || obj == lbtnLongTermTemplate)
                applyTemplate(TemplateFactory.LONG_TERM);

            else if (obj == btnResetVZoom || obj == lbtnResetVZoom) resetVerticalZoom();
            else if (obj == btnResetHZoom || obj == lbtnResetHZoom) resetHorizontalZoom();
            else if (obj == btnSmartZoom || obj == lbtnSmartZoom) performSmartZoom();
            else if (obj == btnZoomIn || obj == lbtnZoomIn) performZoomIn();
            else if (obj == btnHandDragChart || obj == lbtnHandDragChart) performHandDragging();
            else if (obj == btnZoomOut || obj == lbtnZoomOut) performZoomOut();
            else if (obj == btnBoxZoom || obj == lbtnBoxZoom) performBoxZoom();
            else if (obj == btnDragZoom || obj == lbtnDragZoom) performDragZoom();
            else if (obj == btnZoomReset || obj == lbtnZoomReset) performZoomReset();
            else if (obj == btnFav) showChartOptionWindow();

            else if (obj == btnNextSymbol) loadNextSymbol(true);
            else if (obj == btnPrevSymbol) loadNextSymbol(false);
            else objButtonPressed(e.getSource());
            ChartFrame.getSharedInstance().refreshToolBars();
        }
    }

    private void addActionListeners() {

        btnSymbol.addActionListener(this);
        btnCompare.addActionListener(this);
        btnResetVZoom.addActionListener(this);
        btnResetHZoom.addActionListener(this);
        btnSmartZoom.addActionListener(this);
        btnZoomIn.addActionListener(this);
        btnZoomOut.addActionListener(this);
        btnDragZoom.addActionListener(this);
        btnBoxZoom.addActionListener(this);
        btnZoomReset.addActionListener(this);
        btnHandDragChart.addActionListener(this);

        btnBack.addActionListener(this);
        btnNext.addActionListener(this);

        btnNeutral.addActionListener(this);
        btnArrow.addActionListener(this);
        btnSlopeLine.addActionListener(this);
        btnArrowSlopeLine.addActionListener(this);
        btnHorizLine.addActionListener(this);
        btnVertiLine.addActionListener(this);
        btnRectangle.addActionListener(this);
        btnEllipse.addActionListener(this);
        btnTextNote.addActionListener(this);
        btnCycleLine.addActionListener(this);
        btnArc.addActionListener(this);
        btnPitchfork.addActionListener(this);
        btnRegression.addActionListener(this);
        btnTextSymbol.addActionListener(this);
        btnTextSymblAro.addActionListener(this);
        btnDrawingToolsAro.addActionListener(this);

        //fibonacci
        btnFibArcs.addActionListener(this);
        btnFibFans.addActionListener(this);
        btnFibRetracements.addActionListener(this);
        btnFibExtensions.addActionListener(this);
        btnZigZag.addActionListener(this);
        btnFibZones.addActionListener(this);

        btnStdErrChannel.addActionListener(this);
        btnRafChannel.addActionListener(this);
        btnStdDevChannel.addActionListener(this);
        btnEquDistChannel.addActionListener(this);

        btnShortTermTemplate.addActionListener(this);
        btnMidTermTemplate.addActionListener(this);
        btnLongTermTemplate.addActionListener(this);

        btnGannLine.addActionListener(this);
        btnGannFan.addActionListener(this);
        btnGannGrid.addActionListener(this);

        btnNextSymbol.addActionListener(this);
        btnPrevSymbol.addActionListener(this);
    }

    public void setIcons() {

        btnNeutral.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_arrow.gif"));
        btnArrow.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_arrow.gif"));
        btnSlopeLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_slope.gif")); //TODO : NEW ICONS FROM PRADEEP
        btnArrowSlopeLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Arrow_slope.gif")); ///graph_Arrow_slope.gif
        btnHorizLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_horiz.gif"));
        btnVertiLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_verti.gif"));
        btnRectangle.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_rect.gif"));
        btnEllipse.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ellipse.gif"));
        btnTextNote.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_textnote.gif"));
        btnCycleLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_cycle.gif"));
        btnArc.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_arc.gif"));
        btnPitchfork.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_pitchfork.gif"));
        btnRegression.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_regression.gif"));
        btnFibArcs.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibArcs.gif"));
        btnFibFans.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibFans.gif"));
        btnFibRetracements.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibRetrc.gif"));
        btnFibExtensions.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fib_extensions.gif"));
        btnZigZag.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zig_zag.gif"));
        btnFibZones.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibZone.gif"));
        btnStdErrChannel.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdErr.gif"));
        btnShortTermTemplate.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template1.gif"));
        btnMidTermTemplate.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template2.gif"));
        btnLongTermTemplate.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template3.gif"));
        btnGannLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannLine.gif"));
        btnGannFan.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannFan.gif"));
        btnGannGrid.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannGrid.gif"));
        btnRafChannel.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_raffRegChannel.gif"));
        btnStdDevChannel.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdDevChannel.gif"));
        btnEquDistChannel.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_equDistChannel.gif"));

        btnTextSymblAro.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_up_pop.gif"));
//        btnDrawingToolsAro.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/graph_up_pop.gif"));
        btnDrawingToolsAro.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_all_tool.gif"));

        btnResetVZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_resetVzoom.gif"));
        btnResetHZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_resetHzoom.gif"));
        btnSmartZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_smartzoom.gif"));
        btnZoomIn.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomin.gif"));
        btnZoomOut.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomout.gif"));
        btnDragZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_dragzoom.gif"));
        btnBoxZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_boxzoom.gif"));
        btnZoomReset.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomreset.gif"));
        btnHandDragChart.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_hand_drag.gif"));

        btnBack.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_back.gif"));
        btnNext.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_next.gif"));

        if (Language.isLTR()) {
            btnNextSymbol.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_next_symbol.gif"));
            btnPrevSymbol.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_prev_symbol.gif"));
        } else {
            btnNextSymbol.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_prev_symbol.gif"));
            btnPrevSymbol.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_next_symbol.gif"));
        }

        btnSlopeLine.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_slope_active.gif"));
        btnArrowSlopeLine.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Arrow_slope_active.gif")); //graph_Arrow_slope_active.gif
        btnHorizLine.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_horiz_active.gif"));
        btnVertiLine.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_verti_active.gif"));
        btnRectangle.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_rect_active.gif"));
        btnEllipse.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ellipse_active.gif"));
        btnTextNote.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_textnote_active.gif"));
        btnCycleLine.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_cycle_active.gif"));
        btnArc.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_arc_active.gif"));
        btnPitchfork.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ pitchfork _active.gif"));
        btnRegression.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_regression_active.gif"));
        btnFibArcs.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibArcs_active.gif"));
        btnFibFans.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibFans_active.gif"));
        btnFibRetracements.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibRetrc_active.gif"));
        btnFibExtensions.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fib_extensions_active.gif"));
        btnZigZag.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zig_zag_active.gif"));
        btnFibZones.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibZone_active.gif"));
        btnStdErrChannel.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdErr_active.gif"));
        btnGannLine.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannLine_active.gif"));
        btnGannFan.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannFan_active.gif"));
        btnGannGrid.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannGrid_active.gif"));
        btnRafChannel.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_raffRegChannel_active.gif"));
        btnStdDevChannel.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdDevChannel_active.gif"));
        btnEquDistChannel.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_equDistChannel_active.gif"));

        btnResetVZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_resetVzoom_roll.gif"));
        btnResetHZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_resetHzoom_roll.gif"));
        btnSmartZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_smartzoom_roll.gif"));
        btnZoomIn.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomin_roll.gif"));
        btnZoomOut.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomout_roll.gif"));
        btnDragZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_dragzoom_roll.gif"));
        btnBoxZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_boxzoom_roll.gif"));
        btnZoomReset.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomreset_roll.gif"));
        btnHandDragChart.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_hand_drag_roll.gif"));

        btnSlopeLine.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_slope_roll.gif"));
        btnArrowSlopeLine.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Arrow_slope_roll.gif"));//graph_Arrow_slope_roll.gif
        btnHorizLine.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_horiz_roll.gif"));
        btnVertiLine.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_verti_roll.gif"));
        btnRectangle.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_rect_roll.gif"));
        btnEllipse.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ellipse_roll.gif"));
        btnTextNote.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_textnote_roll.gif"));
        btnCycleLine.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_cycle_roll.gif"));
        btnArc.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_arc_roll.gif"));
        btnPitchfork.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ pitchfork _roll.gif"));
        btnRegression.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_regression_roll.gif"));
        btnTextSymblAro.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_up_pop_act.gif"));
        btnDrawingToolsAro.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_all_tool_act.gif"));
        btnFibArcs.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibArcs_roll.gif"));
        btnFibFans.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibFans_roll.gif"));
        btnFibRetracements.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibRetrc_roll.gif"));
        btnFibExtensions.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fib_extensions_roll.gif"));
        btnZigZag.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zig_zag_roll.gif"));
        btnFibZones.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibZone_roll.gif"));
        btnStdErrChannel.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdErr_roll.gif"));
        btnShortTermTemplate.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template1_roll.gif"));
        btnMidTermTemplate.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template2_roll.gif"));
        btnLongTermTemplate.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template3_roll.gif"));
        btnGannLine.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannLine_roll.gif"));
        btnGannFan.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannFan_roll.gif"));
        btnGannGrid.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannGrid_roll.gif"));
        btnRafChannel.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_raffRegChannel_roll.gif"));
        btnStdDevChannel.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdDevChannel_roll.gif"));
        btnEquDistChannel.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_equDistChannel_roll.gif"));

        btnSlopeLine.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_slope_active_roll.gif"));
        btnArrowSlopeLine.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Arrow_slope_active_roll.gif"));//graph_Arrow_slope_active_roll.gif
        btnHorizLine.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_horiz_active_roll.gif"));
        btnVertiLine.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_verti_active_roll.gif"));
        btnRectangle.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_rect_active_roll.gif"));
        btnEllipse.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ellipse_active_roll.gif"));
        btnTextNote.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_textnote_active_roll.gif"));
        btnCycleLine.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_cycle_active_roll.gif"));
        btnArc.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_arc_active_roll.gif"));
        btnPitchfork.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ pitchfork _active_roll.gif"));
        btnRegression.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_regression_active_roll.gif"));
        btnFibArcs.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibArcs_active_roll.gif"));
        btnFibFans.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibFans_active_roll.gif"));
        btnFibRetracements.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibRetrc_active_roll.gif"));
        btnFibExtensions.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fib_extensions_active_roll.gif"));
        btnZigZag.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zig_zag_active_roll.gif"));
        btnFibZones.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibZone_active_roll.gif"));
        btnStdErrChannel.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdErr_active_roll.gif"));
        btnGannLine.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannLine_active_roll.gif"));
        btnGannFan.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannFan_active_roll.gif"));
        btnGannGrid.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannGrid_active_roll.gif"));
        btnRafChannel.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_raffRegChannel_active_roll.gif"));
        btnStdDevChannel.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdDevChannel_active_roll.gif"));
        btnEquDistChannel.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_equDistChannel_active_roll.gif"));

        btnSlopeLine.setRolloverEnabled(true);
        btnArrowSlopeLine.setRolloverEnabled(true);
        btnHorizLine.setRolloverEnabled(true);
        btnVertiLine.setRolloverEnabled(true);
        btnRectangle.setRolloverEnabled(true);
        btnEllipse.setRolloverEnabled(true);
        btnTextNote.setRolloverEnabled(true);
        btnCycleLine.setRolloverEnabled(true);
        btnArc.setRolloverEnabled(true);
        btnPitchfork.setRolloverEnabled(true);
        btnRegression.setRolloverEnabled(true);
        btnTextSymblAro.setRolloverEnabled(true);
        btnDrawingToolsAro.setRolloverEnabled(true);
        btnTextSymbol.setRolloverEnabled(true);
        btnFibArcs.setRolloverEnabled(true);
        btnFibFans.setRolloverEnabled(true);
        btnFibRetracements.setRolloverEnabled(true);
        btnFibExtensions.setRolloverEnabled(true);
        btnZigZag.setRolloverEnabled(true);
        btnFibZones.setRolloverEnabled(true);
        btnStdErrChannel.setRolloverEnabled(true);

        btnResetVZoom.setRolloverEnabled(true);
        btnResetHZoom.setRolloverEnabled(true);
        btnSmartZoom.setRolloverEnabled(true);
        btnZoomIn.setRolloverEnabled(true);
        btnHandDragChart.setRolloverEnabled(true);
        btnZoomOut.setRolloverEnabled(true);
        btnDragZoom.setRolloverEnabled(true);
        btnBoxZoom.setRolloverEnabled(true);
        btnZoomReset.setRolloverEnabled(true);
        btnGannLine.setRolloverEnabled(true);
        btnGannFan.setRolloverEnabled(true);
        btnGannGrid.setRolloverEnabled(true);

    }

    private void setToolTips() {

        btnArrow.setToolTipText(Language.getString("SPECIAL_NOTE"));
        btnSlopeLine.setToolTipText(Language.getString("TRND_LINE"));
        btnArrowSlopeLine.setToolTipText(Language.getString("ARROW_HEAD_TRND_LINE"));
        btnHorizLine.setToolTipText(Language.getString("HIRIZONTAL_LINE"));
        btnVertiLine.setToolTipText(Language.getString("VERTICAL_LINE"));
        btnRectangle.setToolTipText(Language.getString("RECTANGLE"));
        btnEllipse.setToolTipText(Language.getString("ELLIPSE"));
        btnTextNote.setToolTipText(Language.getString("TEXT"));
        btnCycleLine.setToolTipText(Language.getString("GRAPH_OBJ_CYCLE_LINE"));
        btnArc.setToolTipText(Language.getString("GRAPH_OBJ_ARC"));
        btnPitchfork.setToolTipText(Language.getString("GRAPH_OBJ_PITCHFORK"));
        btnRegression.setToolTipText(Language.getString("REGRESSION_LINE"));
        btnTextSymbol.setToolTipText(Language.getString("SIGN"));
        btnTextSymblAro.setToolTipText(Language.getString("TEXTSYMBOLS_HINT"));
        btnDrawingToolsAro.setToolTipText(Language.getString("BT_DRAW_TOOLS"));

        btnFibArcs.setToolTipText(Language.getString("GRAPH_OBJ_FIB_ARCS"));
        btnFibFans.setToolTipText(Language.getString("GRAPH_OBJ_FIB_FANS"));
        btnFibRetracements.setToolTipText(Language.getString("GRAPH_OBJ_FIB_RETRC"));
        btnFibExtensions.setToolTipText(Language.getString("GRAPH_OBJ_FIB_EXTENSIONS"));
        btnZigZag.setToolTipText(Language.getString("GRAPH_OBJ_ZIG_ZAG"));
        btnFibZones.setToolTipText(Language.getString("GRAPH_OBJ_FIB_ZONES"));

        btnStdErrChannel.setToolTipText(Language.getString("GRAPH_OBJ_STD_ERROR_CHANNEL"));
        btnRafChannel.setToolTipText(Language.getString("OBJECT_RAFF_REGRESSION"));
        btnStdDevChannel.setToolTipText(Language.getString("OBJECT_STD_DEV_CHANNEL"));
        btnEquDistChannel.setToolTipText(Language.getString("OBJECT_EQU_DIST_CHANNEL"));

        btnShortTermTemplate.setToolTipText(Language.getString("TEMPLATE_SHORT_TERM"));
        btnMidTermTemplate.setToolTipText(Language.getString("TEMPLATE_MID_TERM"));
        btnLongTermTemplate.setToolTipText(Language.getString("TEMPLATE_LONG_TERM"));

        btnResetVZoom.setToolTipText(Language.getString("GRAPH_YAXIS_RESET"));
        btnResetHZoom.setToolTipText(Language.getString("GRAPH_XAXIS_RESET"));
        btnSmartZoom.setToolTipText(Language.getString("GRAPH_SMART_ZOOM"));
        btnZoomIn.setToolTipText(Language.getString("GRAPH_ZOOM_IN"));
        btnZoomOut.setToolTipText(Language.getString("GRAPH_ZOOM_OUT"));
        btnDragZoom.setToolTipText(Language.getString("GRAPH_DRAG_ZOOM"));
        btnBoxZoom.setToolTipText(Language.getString("GRAPH_BOX_ZOOM"));
        btnZoomReset.setToolTipText(Language.getString("GRAPH_ZOOM_RESET"));
        btnHandDragChart.setToolTipText(Language.getString("GRAPH_HAND_DRAG"));

        btnBack.setToolTipText(Language.getString("GRAPH_PREVIOUS_TOOLBAR"));
        btnNext.setToolTipText(Language.getString("GRAPH_NEXT_TOOLBAR"));

        btnGannLine.setToolTipText(Language.getString("OBJECT_GAN_LINE"));
        btnGannFan.setToolTipText(Language.getString("OBJECT_GANN_FAN"));
        btnGannGrid.setToolTipText(Language.getString("OBJECT_GANN_GRID"));

        btnNextSymbol.setToolTipText(Language.getString("NEXT_SYMBOL"));
        btnPrevSymbol.setToolTipText(Language.getString("PREV_SYMBOL"));
    }

    private void setBorders() {
        btnResetVZoom.setBorder(BorderFactory.createEmptyBorder());
        btnResetHZoom.setBorder(BorderFactory.createEmptyBorder());

        btnSmartZoom.setBorder(BorderFactory.createEmptyBorder());
        btnZoomIn.setBorder(BorderFactory.createEmptyBorder());
        btnZoomOut.setBorder(BorderFactory.createEmptyBorder());
        btnDragZoom.setBorder(BorderFactory.createEmptyBorder());
        btnBoxZoom.setBorder(BorderFactory.createEmptyBorder());
        btnZoomReset.setBorder(BorderFactory.createEmptyBorder());
        btnHandDragChart.setBorder(BorderFactory.createEmptyBorder());

        btnBack.setBorder(BorderFactory.createEmptyBorder());
        btnNext.setBorder(BorderFactory.createEmptyBorder());
        btnNextSymbol.setBorder(BorderFactory.createEmptyBorder());
        btnPrevSymbol.setBorder(BorderFactory.createEmptyBorder());

        //----------------------------------------------------------- //

        btnNeutral.setBorder(BorderFactory.createEmptyBorder());
        btnArrow.setBorder(BorderFactory.createEmptyBorder());
        btnSlopeLine.setBorder(BorderFactory.createEmptyBorder());
        btnArrowSlopeLine.setBorder(BorderFactory.createEmptyBorder());
        btnHorizLine.setBorder(BorderFactory.createEmptyBorder());
        btnVertiLine.setBorder(BorderFactory.createEmptyBorder());
        btnRectangle.setBorder(BorderFactory.createEmptyBorder());
        btnEllipse.setBorder(BorderFactory.createEmptyBorder());
        btnTextNote.setBorder(BorderFactory.createEmptyBorder());
        btnCycleLine.setBorder(BorderFactory.createEmptyBorder());
        btnArc.setBorder(BorderFactory.createEmptyBorder());
        btnPitchfork.setBorder(BorderFactory.createEmptyBorder());
        btnRegression.setBorder(BorderFactory.createEmptyBorder());
        btnTextSymbol.setBorder(BorderFactory.createEmptyBorder());
        btnTextSymblAro.setBorder(BorderFactory.createEmptyBorder());
        btnDrawingToolsAro.setBorder(BorderFactory.createEmptyBorder());
        pnlTextSymbols.setBorder(BorderFactory.createEmptyBorder());

        btnFibArcs.setBorder(BorderFactory.createEmptyBorder());
        btnFibFans.setBorder(BorderFactory.createEmptyBorder());
        btnFibRetracements.setBorder(BorderFactory.createEmptyBorder());
        btnFibExtensions.setBorder(BorderFactory.createEmptyBorder());
        btnZigZag.setBorder(BorderFactory.createEmptyBorder());
        btnFibZones.setBorder(BorderFactory.createEmptyBorder());

        btnStdErrChannel.setBorder(BorderFactory.createEmptyBorder());
        btnRafChannel.setBorder(BorderFactory.createEmptyBorder());
        btnStdDevChannel.setBorder(BorderFactory.createEmptyBorder());
        btnEquDistChannel.setBorder(BorderFactory.createEmptyBorder());

        btnShortTermTemplate.setBorder(BorderFactory.createEmptyBorder()); // Added - Pramoda (14/03/2006)
        btnLongTermTemplate.setBorder(BorderFactory.createEmptyBorder());
        btnMidTermTemplate.setBorder(BorderFactory.createEmptyBorder());

        btnGannLine.setBorder(BorderFactory.createEmptyBorder());
        btnGannFan.setBorder(BorderFactory.createEmptyBorder());
        btnGannGrid.setBorder(BorderFactory.createEmptyBorder());
    }

    //activeGraph
    public GraphFrame getActiveGraph() {
        return activeGraph;
    }

    public void setActiveGraph(GraphFrame gf) {
        activeGraph = gf;
    }

    public void navigateBack() {
        if (activeCard > 0) {
            activeCard -= 1;
            setActiveCard(activeCard);
        }
    }

    public void navigateNext() {
        if (activeCard < NumberOfCards - 1) {
            activeCard += 1;
            setActiveCard(activeCard);
        }
    }

    private void setActiveCard(int btnIndex) {
        int fixedLength = navigaterPanel.getSize().width;
        int availableLength = this.getSize().width - fixedLength;
        int remainingLength = availableLength - H_GAP;
        int cardPnlLength = 0;
        int accLen = 0;

        for (int i = btnIndex; i < NumberOfCards; i++) {
            accLen += CardPanels[i].getSize().width + H_GAP;
        }
        if (accLen < remainingLength) {
            for (int i = btnIndex - 1; i > -1; i--) {
                accLen += CardPanels[i].getSize().width + H_GAP;
                if (accLen > remainingLength) {
                    btnIndex = i + 1;
                    break;
                } else if (i == 0) {
                    btnIndex = 0;
                    break;
                }
            }
        }

        for (int i = 0; i < NumberOfCards; i++) {
            if (i < btnIndex) {
                CardPanels[i].setVisible(false);
                //CardButtons[i].setVisible(true);
            } else {
                if (remainingLength > CardPanels[i].getSize().width) {
                    CardPanels[i].setVisible(true);
                    cardPnlLength += CardPanels[i].getSize().width + H_GAP;
                    remainingLength -= CardPanels[i].getSize().width + H_GAP;
                } else {
                    if (i == btnIndex) {
                        CardPanels[i].setVisible(true);
                        remainingLength = 0;
                    } else {
                        CardPanels[i].setVisible(false);
                        remainingLength = 0;
                    }
                }
                //CardButtons[i].setVisible(false);
            }
        }
        CardContainerPanel.setPreferredSize(new Dimension(cardPnlLength, PrefHt + 2));
        activeCard = btnIndex;
    }

    public void assignCardLengths() {
        try {
            for (int i = 0; i < NumberOfCards; i++) {
                CardLengths[i] = CardPanels[i].getPreferredSize().width;
                //System.out.println(CardLengths[i]+ " hooooooo");
            }
            //set icons for btnTextSymbol
        }
        catch (Exception ee) {
            ee.printStackTrace();
        }
    }

    public void setTextSymbolIcons() {
        try {
            btnTextSymbol.setIcon(new ImageIcon(getSymbolImage(symbolTobeDrawn, false, false)));
            btnTextSymbol.setRolloverIcon(new ImageIcon(getSymbolImage(symbolTobeDrawn, true, false)));
            btnTextSymbol.setSelectedIcon(new ImageIcon(getSymbolImage(symbolTobeDrawn, false, true)));
            btnTextSymbol.setRolloverSelectedIcon(new ImageIcon(getSymbolImage(symbolTobeDrawn, true, true)));
            btnTextSymbol.setDisabledIcon(new ImageIcon(getDisabledSymbolImage()));
            btnTextSymbol.setDisabledSelectedIcon(new ImageIcon(getDisabledSymbolImage()));
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }
    //###################################################

    /**
     * This method is called before the popup menu becomes visible
     */
    public void popupMenuWillBecomeVisible(PopupMenuEvent e) {
    }

    /**
     * This method is called before the popup menu becomes invisible
     */
    public void popupMenuWillBecomeInvisible(PopupMenuEvent e) {
    }

    /**
     * This method is called when the popup menu is canceled
     */
    public void popupMenuCanceled(PopupMenuEvent e) {

    }
    //end interface PopupMenuListener extends EventListener {
    //###################################################

    public void resetCompareCmb() {
//		cmbCompare.removeActionListener(this);
//		cmbCompare.getEditor().setItem("");
//		cmbCompare.addActionListener(this);
    }

    //#################################################
    //implementing focus listener
    /**
     * Invoked when a component gains the keyboard focus.
     */
    public void focusGained(FocusEvent e) {
        if (activeGraph != null)
            activeGraph.removeGraphFocus();
    }

    /**
     * Invoked when a component loses the keyboard focus.
     */
    public void focusLost(FocusEvent e) {
    }
    //#################################################
    // end implementing focus listener

    private void objButtonPressed(Object src) {
        if (activeGraph == null) return;
        //this is done in this way because rectangular selection moce should not be disabled in graph mouse release(like the other objects)
        // and user should manually siable the mode
        if (src == btnNeutral && ObjectTobeDrawn != AbstractObject.INT_RECT_SELECTION) {
            ObjectTobeDrawn = AbstractObject.INT_NONE;
            setGraphCursor(Cursor.getDefaultCursor());
        }
        //drawing tools
        else if (src == btnSlopeLine || src == lbtnSlopeLine) setObjectTobeDrawn(AbstractObject.INT_LINE_SLOPE);
        else if (src == btnArrowSlopeLine || src == lbtnArrowSlopeLine)
            setObjectTobeDrawn(AbstractObject.INT_ARROW_LINE_SLOPE);
        else if (src == btnHorizLine || src == lbtnHorizLine) setObjectTobeDrawn(AbstractObject.INT_LINE_HORIZ);
        else if (src == btnVertiLine || src == lbtnVertiLine) setObjectTobeDrawn(AbstractObject.INT_LINE_VERTI);
        else if (src == btnRectangle || src == lbtnRectangle) setObjectTobeDrawn(AbstractObject.INT_RECT);
        else if (src == btnEllipse || src == lbtnEllipse) setObjectTobeDrawn(AbstractObject.INT_ELLIPSE);
        else if (src == btnTextNote || src == lbtnTextNote) setObjectTobeDrawn(AbstractObject.INT_TEXT);
        else if (src == btnCycleLine || src == lbtnCycleLine) setObjectTobeDrawn(AbstractObject.INT_CYCLE_LINE);
        else if (src == lbtnArc || src == btnArc) setObjectTobeDrawn(AbstractObject.INT_ARC);
        else if (src == lbtnPitchfork || src == btnPitchfork) setObjectTobeDrawn(AbstractObject.INT_ANDREWS_PITCHFORK);
        else if (src == btnRegression || src == lbtnRegression) setObjectTobeDrawn(AbstractObject.INT_REGRESSION);
        else if (src == btnTextSymbol) setObjectTobeDrawn(AbstractObject.INT_SYMBOL);
        else if (src == btnZigZag || src == lbtnZigZag) setObjectTobeDrawn(AbstractObject.INT_ZIG_ZAG);
            //fibonaci
        else if (src == btnFibArcs || src == lbtnFibArcs) setObjectTobeDrawn(AbstractObject.INT_FIBONACCI_ARCS);
        else if (src == btnFibFans || src == lbtnFibFans) setObjectTobeDrawn(AbstractObject.INT_FIBONACCI_FANS);
        else if (src == btnFibRetracements || src == lbtnFibRetracements)
            setObjectTobeDrawn(AbstractObject.INT_FIBONACCI_RETRACEMENTS);
        else if (src == btnFibExtensions || src == lbtnFibExtensions)
            setObjectTobeDrawn(AbstractObject.INT_FIBONACCI_EXTENSIONS);
        else if (src == btnFibZones || src == lbtnFibZones) setObjectTobeDrawn(AbstractObject.INT_FIBONACCI_ZONES);
            //chanel
        else if (src == btnStdErrChannel || src == lbtnStdErrChannel)
            setObjectTobeDrawn(AbstractObject.INT_STD_ERROR_CHANNEL);
        else if (src == btnRafChannel || src == lbtnRafChannel) setObjectTobeDrawn(AbstractObject.INT_RAFF_REGRESSION);
        else if (src == btnStdDevChannel || src == lbtnStdDevChannel)
            setObjectTobeDrawn(AbstractObject.INT_STD_DEV_CHANNEL);
        else if (src == btnEquDistChannel || src == lbtnEquDistChannel)
            setObjectTobeDrawn(AbstractObject.INT_EQUI_DIST_CHANNEL);
            //Gann
        else if (src == btnGannLine || src == lbtnGannLine) setObjectTobeDrawn(AbstractObject.INT_GANN_LINE);
        else if (src == btnGannFan || src == lbtnGannFan) setObjectTobeDrawn(AbstractObject.INT_GANN_FAN);
        else if (src == btnGannGrid || src == lbtnGannGrid) setObjectTobeDrawn(AbstractObject.INT_GANN_GRID);

    }

    public boolean isAbstractObject(Object obj) {
        if ((obj == btnSymbol)
                || (obj == btnCompare)
                || (obj == btnBack)
                || (obj == btnNext)
                || (obj == btnTextSymblAro)
                || (obj == btnDrawingToolsAro)
                || (obj == btnShortTermTemplate)
                || (obj == btnMidTermTemplate)
                || (obj == btnLongTermTemplate)
                || (obj == btnResetVZoom)
                || (obj == btnResetHZoom)
                || (obj == btnSmartZoom)
                || (obj == btnZoomIn)
                || (obj == btnZoomOut)
                || (obj == btnBoxZoom)
                || (obj == btnDragZoom)
                || (obj == btnHandDragChart)
                || (obj == btnZoomReset)
                || (obj == btnNextSymbol)
                || (obj == btnPrevSymbol)) {
            return false;
        } else {
            return true;
        }

    }

    public static void setGraphCursor(Cursor cur) {
        //activeGraph.graph.setCursor
        JInternalFrame[] frames = ChartFrame.getSharedInstance().getDesktop().getAllFrames();
        if (frames != null)
            for (int i = 0; i < frames.length; i++) {
                if (frames[i] instanceof GraphFrame) {
                    ((GraphFrame) frames[i]).graph.setCursor(cur);
                }
            }
    }

    private void setObjectTobeDrawn(int objToDraw) {
        if (activeGraph == null) return;
        if (ObjectTobeDrawn == objToDraw) {
            ObjectTobeDrawn = AbstractObject.INT_NONE;
            btnNeutral.doClick();
        } else {
            ObjectTobeDrawn = objToDraw;
            //activeGraph.graph.setCursor(createCustomCursor(objToDraw));
            setGraphCursor(createCustomCursor(objToDraw));
        }
    }

    public void setNullTarget() {
        setButtonsEnabled(false, false);
    }

    private void setButtonsEnabled(boolean baseEnabled, boolean compareEnabled) {

        //cmbSymbol.setEnabled(baseEnabled);
        cmbSymbol.setEnabled(true);
        btnSymbol.setEnabled(baseEnabled);
        lbCompare.setEnabled(compareEnabled);
        cmbCompare.setEnabled(compareEnabled);
        btnCompare.setEnabled(compareEnabled);
        cmbCompare.setEnabled(compareEnabled);
        cmbCompare.setEditable(compareEnabled);
        btnNextSymbol.setEnabled(baseEnabled);
        btnPrevSymbol.setEnabled(baseEnabled);
        btnNextSymbol.setToolTipText(Language.getString("NEXT_SYMBOL") +
                ((getNextSymbol(true) != null) ? " (" + SharedMethods.getSymbolFromKey(getNextSymbol(true)) + ")" : ""));
        btnPrevSymbol.setToolTipText(Language.getString("PREV_SYMBOL") +
                ((getNextSymbol(false) != null) ? " (" + SharedMethods.getSymbolFromKey(getNextSymbol(false)) + ")" : ""));
        if (compareEnabled) {
            //cmbSymbol.setEnabled(compareEnabled);
            cmbSymbol.setEnabled(true);
            cmbCompare.setEnabled(compareEnabled);
            cmbSymbol.setEditable(compareEnabled);
            cmbCompare.setEditable(compareEnabled);
            //cmbSymbol.updateUI();
            //cmbCompare.updateUI();
        }
        btnArrow.setEnabled(compareEnabled);
        btnSlopeLine.setEnabled(compareEnabled);
        btnArrowSlopeLine.setEnabled(compareEnabled);
        btnHorizLine.setEnabled(compareEnabled);
        btnVertiLine.setEnabled(compareEnabled);
        btnRectangle.setEnabled(compareEnabled);
        btnEllipse.setEnabled(compareEnabled);
        btnTextNote.setEnabled(compareEnabled);
        btnCycleLine.setEnabled(compareEnabled);
        btnArc.setEnabled(compareEnabled);
        btnPitchfork.setEnabled(compareEnabled);
        btnRegression.setEnabled(compareEnabled);
        btnTextSymbol.setEnabled(compareEnabled);
        btnTextSymblAro.setEnabled(compareEnabled);
        btnDrawingToolsAro.setEnabled(compareEnabled);
        btnFibArcs.setEnabled(compareEnabled);
        btnFibFans.setEnabled(compareEnabled);
        btnFibRetracements.setEnabled(compareEnabled);
        btnFibExtensions.setEnabled(compareEnabled);
        btnZigZag.setEnabled(compareEnabled);
        btnFibZones.setEnabled(compareEnabled);

        btnStdErrChannel.setEnabled(compareEnabled);
        btnRafChannel.setEnabled(compareEnabled);
        btnStdDevChannel.setEnabled(compareEnabled);
        btnEquDistChannel.setEnabled(compareEnabled);

        btnShortTermTemplate.setEnabled(compareEnabled);
        btnMidTermTemplate.setEnabled(compareEnabled);
        btnLongTermTemplate.setEnabled(compareEnabled);

        btnResetHZoom.setEnabled(compareEnabled);
        btnResetVZoom.setEnabled(compareEnabled);
        btnSmartZoom.setEnabled(compareEnabled);
        btnZoomIn.setEnabled(compareEnabled);
        btnHandDragChart.setEnabled(compareEnabled);
        btnZoomOut.setEnabled(compareEnabled);
        btnDragZoom.setEnabled(compareEnabled);
        btnBoxZoom.setEnabled(compareEnabled);
        btnZoomReset.setEnabled(compareEnabled);

        btnGannLine.setEnabled(compareEnabled);
        btnGannFan.setEnabled(compareEnabled);
        btnGannGrid.setEnabled(compareEnabled);
    }

    public void refreshToolBar(boolean compareVisible, boolean indicatorEnabled) {
        boolean enabled = (activeGraph != null);
        if (enabled) {
            if (activeGraph.tableMode) {
                enabled = false;
                compareVisible = false;
            }
        }
        setButtonsEnabled(enabled, compareVisible);
        //cmbCompare.updateUI();
    }

    private Cursor createCustomCursor(int objToDraw) {
        switch (objToDraw) {
            case AbstractObject.INT_LINE_HORIZ:
                return CURSOR_LINE_HORIZ;
            case AbstractObject.INT_LINE_VERTI:
                return CURSOR_LINE_VERTI;
            case AbstractObject.INT_DATA_LINE_VERTI:
                return CURSOR_LINE_VERTI;
            case AbstractObject.INT_SYMBOL:
                Toolkit tk = Toolkit.getDefaultToolkit();
                Image img = createTransparentImage(32, 32);
                Graphics2D g = (Graphics2D) img.getGraphics();
                //ANTIALIASING is not compatible with BufferedImage ???
                //g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
                g.setColor(Theme.getColor("GRAPH_SYMBOL_COLOR"));
                g.setFont(SharedMethods.getSymbolFont("fonts/LucidaSansRegular.ttf", 18));
                FontMetrics fmx = g.getFontMetrics();
                String text = new String(caTextSymbols, symbolTobeDrawn, 1);
                Point pt = AbstractObject.getAlignedPosition(AbstractObject.ALIGNMENT_CENTER, 32, 32,
                        fmx.stringWidth(text), fmx.getHeight(), fmx.getAscent());
                g.drawString(text, pt.x, pt.y);

                Cursor cur = tk.createCustomCursor(img, new Point(16, 16), "Ellipse");
                return cur;
            case AbstractObject.INT_RECT:
                return CURSOR_RECT;
            case AbstractObject.INT_RECT_SELECTION:
                return CURSOR_RECT_SELECTION;
            case AbstractObject.INT_TEXT:
                return CURSOR_TEXT;
            case AbstractObject.INT_ELLIPSE:
                return CURSOR_ELLIPSE;
            case AbstractObject.INT_POLYGON:
                return CURSOR_POLYGON;
            case AbstractObject.INT_REGRESSION:
                return CURSOR_REGRESSION;
            case AbstractObject.INT_FIBONACCI_ARCS:
                return CURSOR_FIB_ARCS;
            case AbstractObject.INT_FIBONACCI_FANS:
                return CURSOR_FIB_FANS;
            case AbstractObject.INT_FIBONACCI_RETRACEMENTS:
                return CURSOR_FIB_RETRC;
            case AbstractObject.INT_FIBONACCI_ZONES:
                return CURSOR_FIB_ZONES;
            case AbstractObject.INT_STD_ERROR_CHANNEL:
                return CURSOR_STD_ERROR_CHANNEL;
            case AbstractObject.INT_GANN_LINE:
                return CURSOR_GANN_LINE;
            case AbstractObject.INT_GANN_FAN:
                return CURSOR_GANN_FAN;
            case AbstractObject.INT_GANN_GRID:
                return CURSOR_GANN_GRID;
            case AbstractObject.INT_RAFF_REGRESSION:
                return CURSOR_RAFF_REG_CHANNEL;
            case AbstractObject.INT_STD_DEV_CHANNEL:
                return CURSOR_STD_DEV_CHANNEL;
            case AbstractObject.INT_EQUI_DIST_CHANNEL:
                return CURSOR_EQU_DIST_CHANNEL;
            case AbstractObject.INT_FIBONACCI_EXTENSIONS:
                return CURSOR_FIB_EXTENSIONS;
            case AbstractObject.INT_ZIG_ZAG:
                return CURSOR_ZIG_ZAG;
            case AbstractObject.INT_ARROW_LINE_SLOPE:
                return CURSOR_ARROW_LINE_SLOPE;
            case AbstractObject.INT_CYCLE_LINE:
                return CURSOR_CYCLE_LINE;
            case AbstractObject.INT_ARC:
                return CURSOR_ARC;
            case AbstractObject.INT_ANDREWS_PITCHFORK:
                return CURSOR_PITCHFORK;
            default:
                return CURSOR_LINE_SLOPE;
        }
    }

    public static Cursor createCursor(String imgName) {
        Toolkit tk = Toolkit.getDefaultToolkit();
        Image img = tk.getImage(imgName);
        Cursor cur = tk.createCustomCursor(img, new Point(0, 0), "Ellipse");
        return cur;
    }

    private void popTextSymbols() {
        btnNeutral.doClick();
        final JPopupMenu pop = new JPopupMenu();
        pop.setLayout(new FlowLayout(FlowLayout.CENTER, 2, 2));
        int smblRows = (int) Math.round(Math.ceil((float) caTextSymbols.length / (float) smblCols));
        JPanel pnlTxtSmbls = new JPanel(new GridLayout(smblRows, smblCols, 2, 2));
        Font customFont = SharedMethods.getSymbolFont("fonts/LucidaSansRegular.ttf", 18);
        for (int i = 0; i < caTextSymbols.length; i++) {
            JLabel lblChar = new GraphLabel(new String(caTextSymbols, i, 1));
            lblChar.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
            lblChar.setFont(customFont);
            lblChar.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
            lblChar.setHorizontalAlignment(JLabel.CENTER);
            final int ind = i;
            lblChar.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {
                    setSelectedTextSymbol(ind);
                    pop.setVisible(false);
                }
            });
            pnlTxtSmbls.add(lblChar);
        }
        Dimension dim = new Dimension(smblCols * (PrefBtnWd + 2) - 2, smblRows * (PrefBtnHt + 2) - 2);
        pnlTxtSmbls.setPreferredSize(dim); //2+smblRows*(PrefBtnHt+2)
        pop.add(pnlTxtSmbls);
        pop.show(btnTextSymblAro, 10 - dim.width - 6, 2 - dim.height - 8);
        //GUISettings.showPopup(pop, btnTextSymblAro, 0,0);
    }

    private void popBottomTools() {
        btnNeutral.doClick();
        final JPopupMenu pop = new JPopupMenu();
        //pop.setL
//        pop.setLayout(new FlowLayout(FlowLayout.CENTER, 2, 2));
//        pop.setLayout(new FlexGridLayout(new String[]{"100%"},new String []{"100%"}));
//        pop.setLayout(new FlowLayout(FlowLayout.CENTER, 2, 2));

        /*******************/


        int smblRows = (int) Math.round(Math.ceil((float) caTextSymbols.length / (float) smblCols));
        JLabel lblDrawingTools = new JLabel(Language.getString("BT_DRAW_TOOLS"));

        JPanel drawingToolsPnl = new JPanel();
        JPanel pnlTxtSmbls = new JPanel(new GridLayout(smblRows, smblCols, 2, 2));
//        JPanel pnlTxtSmbls = new JPanel(new GridLayout(12, 1, 2, 2)); //add panels
        Font customFont = SharedMethods.getSymbolFont("fonts/LucidaSansRegular.ttf", 18);
        for (int i = 0; i < caTextSymbols.length; i++) {
            JLabel lblChar = new GraphLabel(new String(caTextSymbols, i, 1));
            lblChar.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
            lblChar.setFont(customFont);
            lblChar.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
            lblChar.setHorizontalAlignment(JLabel.CENTER);
            final int ind = i;
            lblChar.addMouseListener(new MouseAdapter() {
                public void mouseClicked(MouseEvent e) {
                    setSelectedTextSymbol(ind);
                    pop.setVisible(false);
                }
            });
            pnlTxtSmbls.add(lblChar);
        }
        Dimension dim = new Dimension(smblCols * (PrefBtnWd + 2) - 2, smblRows * (PrefBtnHt + 2) - 2);

//        lblDrawingTools.setPreferredSize(new Dimension(dim.width,30));

        String sWidth = dim.width + "";
        drawingToolsPnl.setLayout(new FlexGridLayout(new String[]{"100%"}, new String[]{"30", "100%"}, 0, 0));
        Dimension dimWholePnl = new Dimension(smblCols * (PrefBtnWd + 2) - 2, smblRows * (PrefBtnHt + 2) - 2 + 30);
        pnlTxtSmbls.setPreferredSize(dim); //2+smblRows*(PrefBtnHt+2)
        drawingToolsPnl.setPreferredSize(dimWholePnl); //2+smblRows*(PrefBtnHt+2)
        drawingToolsPnl.add(lblDrawingTools);
        drawingToolsPnl.add(pnlTxtSmbls);

//        pop.add(pnlTxtSmbls);

        pop.add(drawingToolsPnl);
        pop.show(btnDrawingToolsAro, 10 - dim.width - 6, -dim.height - 8);
//        pop.show(btnDrawingToolsAro , 0, 0);
        //GUISettings.showPopup(pop, btnTextSymblAro, 0,0);

    }

    /*============================== start of add to favourite related ========================================*/
    private void popBottomAllTools() {
        final JPopupMenu pop = new JPopupMenu();
        //main panel
        JPanel mainPnl = new JPanel();//this is added to the pop up

        /*R *************************Drawing tools***************************/
        JLabel lblDrawing = new JLabel(Language.getString("BT_DRAW_TOOLS"), JLabel.CENTER);
        lblDrawing.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
        lblDrawing.setPreferredSize(new Dimension(200, PrefBtnHt));
        JPanel lpnlLineToolbar = crateDrawingPopUppanel1();
        JPanel lpnlLineToolbar2 = crateDrawingPopUppanel2();

        Font allToolFont = lblDrawing.getFont();
        Font myfont = new Font(allToolFont.getName(), Font.BOLD, allToolFont.getSize());
        lblDrawing.setFont(myfont);

        /*R *************************fibonacci tools***************************/
        JLabel lblFibonaci = new JLabel(Language.getString("BT_FIB_TOOLS"), JLabel.CENTER);
        lblFibonaci.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
        lblFibonaci.setPreferredSize(new Dimension(200, PrefBtnHt));
        JPanel lpnlFibonaciToolbar = crateFibonaciPopUppanel();
        lblFibonaci.setFont(myfont);

        /*R *************************channel tools***************************/
        JLabel lblChanel = new JLabel(Language.getString("BT_CHAN_TOOLS"), JLabel.CENTER); //ToDo language tags
        lblChanel.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
        lblChanel.setPreferredSize(new Dimension(200, PrefBtnHt));
        lblChanel.setFont(myfont);
        JPanel lpnlChanel = crateChanelToolPopUppanel();

        /*R*************template lablels **************/
        JLabel lblTempl = new JLabel(Language.getString("BT_TERM_TOOLS"), JLabel.CENTER);
        lblTempl.setFont(myfont);
        lblTempl.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
        lblTempl.setPreferredSize(new Dimension(200, PrefBtnHt));
        JPanel lPnlTemplatesToolBar = createTermPopUpPnl();

        /*R*************Gann lablels **************/
        JLabel lblGan = new JLabel(Language.getString("BT_GANN_TOOLS"), JLabel.CENTER);
        lblGan.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
        lblGan.setPreferredSize(new Dimension(200, PrefBtnHt));
        lblGan.setFont(myfont);
        JPanel lPnlGannToolBar = createGanPopUpPnl();

        /*R*************Zoom  lablels **************/
        JLabel lblZoom = new JLabel(Language.getString("BT_ZOOM_TOOLS"), JLabel.CENTER);
        lblZoom.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
        lblZoom.setPreferredSize(new Dimension(200, PrefBtnHt));
        lblZoom.setFont(myfont);
        JPanel lPnlZoom = createZoomPopUpPnl();

/*R***************button groups****************************/
        //Todo check whether it needs only to add to the same buton grup - lineBtnGrp
        lineBtnGrpPop.add(lbtnArrow);
        lineBtnGrpPop.add(lbtnSlopeLine);
        lineBtnGrpPop.add(lbtnArrowSlopeLine);
        lineBtnGrpPop.add(lbtnHorizLine);
        lineBtnGrpPop.add(lbtnVertiLine);
        lineBtnGrpPop.add(lbtnRectangle);
        lineBtnGrpPop.add(lbtnEllipse);
        lineBtnGrpPop.add(lbtnTextNote);
        lineBtnGrpPop.add(lbtnRegression);
        lineBtnGrpPop.add(lbtnArc);
        lineBtnGrpPop.add(lbtnPitchfork);
        lineBtnGrpPop.add(lbtnCycleLine);

        //fibonacci
        lineBtnGrpPop.add(lbtnFibArcs);
        lineBtnGrpPop.add(lbtnFibFans);
        lineBtnGrpPop.add(lbtnFibRetracements);
        lineBtnGrpPop.add(lbtnFibExtensions);
        lineBtnGrpPop.add(lbtnZigZag);
        lineBtnGrpPop.add(lbtnFibZones);

        lineBtnGrpPop.add(lbtnStdErrChannel);
        lineBtnGrpPop.add(lbtnRafChannel);
        lineBtnGrpPop.add(lbtnStdDevChannel);
        lineBtnGrpPop.add(lbtnEquDistChannel);

        lineBtnGrpPop.add(lbtnShortTermTemplate);
        lineBtnGrpPop.add(lbtnMidTermTemplate);
        lineBtnGrpPop.add(lbtnLongTermTemplate);

        lineBtnGrpPop.add(lbtnGannLine);
        lineBtnGrpPop.add(lbtnGannFan);
        lineBtnGrpPop.add(lbtnGannGrid);
        /*ER***************button groups****************************/

        JPanel lPnlFavourite = new JPanel();

        //lPnlFavourite.setLayout(new FlexGridLayout(new String[]{"60", "100%"}, new String[]{"3", "100%", "3"}, 0, 0));
        lPnlFavourite.setLayout(new FlowLayout(FlowLayout.CENTER));
        btnFav = new TWButton(Language.getString("BTB_CUSTOMIZE_BTN"));
        btnFav.addActionListener(this);
        btnFav.setPreferredSize(new Dimension(125, 20));
        lPnlFavourite.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
        lPnlFavourite.add(new JLabel(""));
        lPnlFavourite.add(new JLabel(""));

        lPnlFavourite.add(new JLabel(""));
        lPnlFavourite.add(btnFav);
        lPnlFavourite.add(new JLabel(""));
        lPnlFavourite.add(new JLabel(""));
        lPnlFavourite.setPreferredSize(new Dimension(200, 26));

        //set the main panel dimention & layout
        mainPnl.setLayout(new FlexGridLayout(new String[]{"200"}, new String[]{"22", "22", "22", "22", "22", "22", "22", "22", "22", "22", "22", "22", "22", "26"}, 0, 0));
        Dimension maninPnlDim = new Dimension(200, 315);
        mainPnl.setPreferredSize(maninPnlDim);

        /*R**********add other panels to main panel **************/

        mainPnl.add(lblDrawing);
        mainPnl.add(lpnlLineToolbar);
        mainPnl.add(lpnlLineToolbar2);

        mainPnl.add(lblFibonaci);
        mainPnl.add(lpnlFibonaciToolbar);


        mainPnl.add(lblChanel);
        mainPnl.add(lpnlChanel);

        mainPnl.add(lblTempl);
        mainPnl.add(lPnlTemplatesToolBar);

        mainPnl.add(lblGan);
        mainPnl.add(lPnlGannToolBar);

        mainPnl.add(lblZoom);
        mainPnl.add(lPnlZoom);

        mainPnl.add(lPnlFavourite);

        /*ER**********add other panels to main panel **************/

        /*R*****add to popup***************************/
        pop.add(mainPnl);
//        pop.show(btnDrawingToolsAro, 0, 0);
        pop.show(btnDrawingToolsAro, 10 - maninPnlDim.width - 6, -maninPnlDim.height - 8);
        /*ER*****add to popup***************************/

    }

    private JPanel createZoomPopUpPnl() {
        JPanel lpnlZoomToolbar = new JPanel();

        lbtnResetVZoom = new JButton();
        lbtnResetHZoom = new JButton();
        lbtnSmartZoom = new JButton();
        lbtnZoomIn = new JButton();
        lbtnZoomOut = new JButton();
        lbtnBoxZoom = new JButton();
        lbtnDragZoom = new JButton();
        lbtnZoomReset = new JButton();
        lbtnHandDragChart = new JButton();


        lbtnResetVZoom.addActionListener(this);
        lbtnResetHZoom.addActionListener(this);
        lbtnSmartZoom.addActionListener(this);
        lbtnZoomIn.addActionListener(this);
        lbtnZoomOut.addActionListener(this);
        lbtnBoxZoom.addActionListener(this);
        lbtnDragZoom.addActionListener(this);
        lbtnZoomReset.addActionListener(this);
        lbtnHandDragChart.addActionListener(this);

        //set button sizes
        lbtnResetVZoom.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnResetHZoom.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));

        lbtnSmartZoom.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnZoomIn.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnZoomOut.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnDragZoom.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnBoxZoom.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnZoomReset.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnHandDragChart.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lpnlZoomToolbar.setPreferredSize(new Dimension(200, 30));

        //set border
        lpnlZoomToolbar.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
        lbtnResetVZoom.setBorder(BorderFactory.createEmptyBorder());
        lbtnResetHZoom.setBorder(BorderFactory.createEmptyBorder());

        lbtnSmartZoom.setBorder(BorderFactory.createEmptyBorder());
        lbtnZoomIn.setBorder(BorderFactory.createEmptyBorder());
        lbtnZoomOut.setBorder(BorderFactory.createEmptyBorder());
        lbtnDragZoom.setBorder(BorderFactory.createEmptyBorder());
        lbtnBoxZoom.setBorder(BorderFactory.createEmptyBorder());
        lbtnZoomReset.setBorder(BorderFactory.createEmptyBorder());
        lbtnHandDragChart.setBorder(BorderFactory.createEmptyBorder());

        //set tool tip
        lbtnResetVZoom.setToolTipText(Language.getString("GRAPH_YAXIS_RESET"));
        lbtnResetHZoom.setToolTipText(Language.getString("GRAPH_XAXIS_RESET"));
        lbtnSmartZoom.setToolTipText(Language.getString("GRAPH_SMART_ZOOM"));
        lbtnZoomIn.setToolTipText(Language.getString("GRAPH_ZOOM_IN"));
        lbtnZoomOut.setToolTipText(Language.getString("GRAPH_ZOOM_OUT"));
        lbtnDragZoom.setToolTipText(Language.getString("GRAPH_DRAG_ZOOM"));
        lbtnBoxZoom.setToolTipText(Language.getString("GRAPH_BOX_ZOOM"));
        lbtnZoomReset.setToolTipText(Language.getString("GRAPH_ZOOM_RESET"));
        lbtnHandDragChart.setToolTipText(Language.getString("GRAPH_HAND_DRAG"));

        // set icons
        lbtnResetVZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_resetVzoom.gif"));
        lbtnResetHZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_resetHzoom.gif"));
        lbtnSmartZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_smartzoom.gif"));
        lbtnZoomIn.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomin.gif"));
        lbtnZoomOut.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomout.gif"));
        lbtnDragZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_dragzoom.gif"));
        lbtnBoxZoom.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_boxzoom.gif"));
        lbtnZoomReset.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomreset.gif"));
        lbtnHandDragChart.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_hand_drag.gif"));

        lbtnResetVZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_resetVzoom_roll.gif"));
        lbtnResetHZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_resetHzoom_roll.gif"));
        lbtnSmartZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_smartzoom_roll.gif"));
        lbtnZoomIn.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomin_roll.gif"));
        lbtnZoomOut.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomout_roll.gif"));
        lbtnDragZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_dragzoom_roll.gif"));
        lbtnBoxZoom.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_boxzoom_roll.gif"));
        lbtnZoomReset.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zoomreset_roll.gif"));
        lbtnHandDragChart.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_hand_drag_roll.gif"));

        //set drawing panel size
        FlowLayout lZoomLayout = new FlowLayout(FlowLayout.LEFT, 2, 0);
        lpnlZoomToolbar.setLayout(lZoomLayout);

        lpnlZoomToolbar.add(lbtnHandDragChart);
        lpnlZoomToolbar.add(lbtnSmartZoom);
        lpnlZoomToolbar.add(lbtnZoomIn);
        lpnlZoomToolbar.add(lbtnZoomOut);
        lpnlZoomToolbar.add(lbtnBoxZoom);
        lpnlZoomToolbar.add(lbtnDragZoom);
        lpnlZoomToolbar.add(lbtnResetVZoom);
        lpnlZoomToolbar.add(lbtnResetHZoom);
        lpnlZoomToolbar.add(lbtnZoomReset);


        return lpnlZoomToolbar;
    }

    private JPanel crateFibonaciPopUppanel() {


        JPanel lpnlFibonacciToolbar = new JPanel();
        lbtnFibArcs = new JToggleButton();
        lbtnFibFans = new JToggleButton();
        lbtnFibRetracements = new JToggleButton();
        lbtnFibExtensions = new JToggleButton();
        lbtnFibZones = new JToggleButton();

        lbtnFibArcs.addActionListener(this);
        lbtnFibFans.addActionListener(this);
        lbtnFibRetracements.addActionListener(this);
        lbtnFibExtensions.addActionListener(this);
        lbtnFibZones.addActionListener(this);

        //set button sizes
        lbtnFibArcs.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnFibFans.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnFibRetracements.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnFibExtensions.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnFibZones.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
//        lpnlFibonacciToolbar.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));

        //set border
        lpnlFibonacciToolbar.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
        lbtnFibArcs.setBorder(BorderFactory.createEmptyBorder());
        lbtnFibFans.setBorder(BorderFactory.createEmptyBorder());
        lbtnFibRetracements.setBorder(BorderFactory.createEmptyBorder());
        lbtnFibExtensions.setBorder(BorderFactory.createEmptyBorder());

        lbtnFibZones.setBorder(BorderFactory.createEmptyBorder());

        //set tool tip
        lbtnFibArcs.setToolTipText(Language.getString("GRAPH_OBJ_FIB_ARCS"));
        lbtnFibFans.setToolTipText(Language.getString("GRAPH_OBJ_FIB_FANS"));
        lbtnFibRetracements.setToolTipText(Language.getString("GRAPH_OBJ_FIB_RETRC"));
        lbtnFibExtensions.setToolTipText(Language.getString("GRAPH_OBJ_FIB_EXTENSIONS"));
        lbtnFibZones.setToolTipText(Language.getString("GRAPH_OBJ_FIB_ZONES"));

        // set icons
        lbtnFibArcs.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibArcs.gif"));
        lbtnFibFans.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibFans.gif"));
        lbtnFibRetracements.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibRetrc.gif"));
        lbtnFibExtensions.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fib_extensions.gif"));
        lbtnFibZones.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibZone.gif"));

        //rollover
        lbtnFibArcs.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibArcs_roll.gif"));
        lbtnFibFans.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibFans_roll.gif"));
        lbtnFibRetracements.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibRetrc_roll.gif"));
        lbtnFibExtensions.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fib_extensions_roll.gif"));
        lbtnFibZones.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibZone_roll.gif"));

        //selected
        lbtnFibArcs.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibArcs_active.gif"));
        lbtnFibFans.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibFans_active.gif"));
        lbtnFibRetracements.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibRetrc_active.gif"));
        lbtnFibExtensions.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fib_extensions_active.gif"));
        lbtnFibZones.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibZone_active.gif"));

        //roll select
        lbtnFibArcs.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibArcs_active_roll.gif"));
        lbtnFibFans.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibFans_active_roll.gif"));
        lbtnFibRetracements.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibRetrc_active_roll.gif"));
        lbtnFibExtensions.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fib_extensions_active_roll.gif"));
        lbtnFibZones.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_fibZone_active_roll.gif"));

        //set drawing panel size
        FlowLayout lFibLayout = new FlowLayout(FlowLayout.LEFT, 2, 0);
        lpnlFibonacciToolbar.setLayout(lFibLayout);

        lpnlFibonacciToolbar.add(lbtnFibArcs);
        lpnlFibonacciToolbar.add(lbtnFibFans);
        lpnlFibonacciToolbar.add(lbtnFibRetracements);
        lpnlFibonacciToolbar.add(lbtnFibExtensions);
        lpnlFibonacciToolbar.add(lbtnFibZones);

        return lpnlFibonacciToolbar;
    }

    private JPanel crateChanelToolPopUppanel() {


        JPanel lpnlChannelsToolbar = new JPanel();

        lbtnStdErrChannel = new JToggleButton();
        lbtnRafChannel = new JToggleButton();
        lbtnStdDevChannel = new JToggleButton();
        lbtnEquDistChannel = new JToggleButton();

        lbtnStdErrChannel.addActionListener(this);
        lbtnRafChannel.addActionListener(this);
        lbtnStdDevChannel.addActionListener(this);
        lbtnEquDistChannel.addActionListener(this);

        //set button sizes
        lbtnStdErrChannel.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnRafChannel.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnStdDevChannel.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnEquDistChannel.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));

        //set border
        lpnlChannelsToolbar.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
        lbtnStdErrChannel.setBorder(BorderFactory.createEmptyBorder());
        lbtnRafChannel.setBorder(BorderFactory.createEmptyBorder());
        lbtnStdDevChannel.setBorder(BorderFactory.createEmptyBorder());
        lbtnEquDistChannel.setBorder(BorderFactory.createEmptyBorder());

        //set tool tip
        lbtnStdErrChannel.setToolTipText(Language.getString("GRAPH_OBJ_STD_ERROR_CHANNEL"));
        lbtnRafChannel.setToolTipText(Language.getString("OBJECT_RAFF_REGRESSION"));
        lbtnStdDevChannel.setToolTipText(Language.getString("OBJECT_STD_DEV_CHANNEL"));
        lbtnEquDistChannel.setToolTipText(Language.getString("OBJECT_EQU_DIST_CHANNEL"));

        // set icons
        lbtnStdErrChannel.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdErr.gif"));
        lbtnRafChannel.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_raffRegChannel.gif"));
        lbtnStdDevChannel.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdDevChannel.gif"));
        lbtnEquDistChannel.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_equDistChannel.gif"));

        //roll
        lbtnStdErrChannel.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdErr_roll.gif"));
        lbtnRafChannel.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_raffRegChannel_roll.gif"));
        lbtnStdDevChannel.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdDevChannel_roll.gif"));
        lbtnEquDistChannel.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_equDistChannel_roll.gif"));

        //selected
        lbtnStdErrChannel.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdErr_active.gif"));
        lbtnRafChannel.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_raffRegChannel_active.gif"));
        lbtnStdDevChannel.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdDevChannel_active.gif"));
        lbtnEquDistChannel.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_equDistChannel_active.gif"));

        //roll selct
        lbtnStdErrChannel.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdErr_roll.gif"));
        lbtnRafChannel.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_raffRegChannel_roll.gif"));
        lbtnStdDevChannel.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_stdDevChannel_roll.gif"));
        lbtnEquDistChannel.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_equDistChannel_roll.gif"));

        //set drawing panel size
        FlowLayout lChanelLayout = new FlowLayout(FlowLayout.LEFT, 2, 0);
        lpnlChannelsToolbar.setLayout(lChanelLayout);

        lpnlChannelsToolbar.add(lbtnStdErrChannel);
        lpnlChannelsToolbar.add(lbtnRafChannel);
        lpnlChannelsToolbar.add(lbtnStdDevChannel);
        lpnlChannelsToolbar.add(lbtnEquDistChannel);


        return lpnlChannelsToolbar;
    }

    private JPanel createTermPopUpPnl() {
        /*R*************template buttons **************/

        JPanel lPnlTemplatesToolBar = new JPanel();
        lbtnShortTermTemplate = new JButton();
        lbtnMidTermTemplate = new JButton();
        lbtnLongTermTemplate = new JButton();

        lbtnShortTermTemplate.addActionListener(this);
        lbtnMidTermTemplate.addActionListener(this);
        lbtnLongTermTemplate.addActionListener(this);

        //set button sizes
        lbtnShortTermTemplate.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnMidTermTemplate.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnLongTermTemplate.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lPnlTemplatesToolBar.setPreferredSize(new Dimension(200, 30));

        //set border
        lPnlTemplatesToolBar.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
        lbtnShortTermTemplate.setBorder(BorderFactory.createEmptyBorder()); // Added - Pramoda (14/03/2006)
        lbtnLongTermTemplate.setBorder(BorderFactory.createEmptyBorder());
        lbtnMidTermTemplate.setBorder(BorderFactory.createEmptyBorder());

        //set tool tip
        lbtnShortTermTemplate.setToolTipText(Language.getString("TEMPLATE_SHORT_TERM"));
        lbtnMidTermTemplate.setToolTipText(Language.getString("TEMPLATE_MID_TERM"));
        lbtnLongTermTemplate.setToolTipText(Language.getString("TEMPLATE_LONG_TERM"));

        // set icons
        lbtnShortTermTemplate.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template1.gif"));
        lbtnMidTermTemplate.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template2.gif"));
        lbtnLongTermTemplate.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template3.gif"));

        //roll
        lbtnShortTermTemplate.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template1_roll.gif"));
        lbtnMidTermTemplate.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template2_roll.gif"));
        lbtnLongTermTemplate.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Template3_roll.gif"));

        //set template panel size
        FlowLayout lineToolbarLayout = new FlowLayout(FlowLayout.LEFT, 2, 0);
        lPnlTemplatesToolBar.setLayout(lineToolbarLayout);
        lPnlTemplatesToolBar.add(lbtnShortTermTemplate);
        lPnlTemplatesToolBar.add(lbtnMidTermTemplate);
        lPnlTemplatesToolBar.add(lbtnLongTermTemplate);

        /*ER*************template buttons **************/
        return lPnlTemplatesToolBar;
    }

    private JPanel createGanPopUpPnl() {
        /*R*************template buttons **************/


        JPanel lpnlGannAnalisysToolbar = new JPanel();
        lbtnGannLine = new JToggleButton();
        lbtnGannFan = new JToggleButton();
        lbtnGannGrid = new JToggleButton();

        //add action listners
        lbtnGannLine.addActionListener(this);
        lbtnGannFan.addActionListener(this);
        lbtnGannGrid.addActionListener(this);

        //set button sizes
        lbtnGannLine.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnGannFan.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnGannGrid.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));

        //set border
        lpnlGannAnalisysToolbar.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
        lbtnGannLine.setBorder(BorderFactory.createEmptyBorder());
        lbtnGannFan.setBorder(BorderFactory.createEmptyBorder());
        lbtnGannGrid.setBorder(BorderFactory.createEmptyBorder());
        //set tool tip
        lbtnGannLine.setToolTipText(Language.getString("OBJECT_GAN_LINE"));
        lbtnGannFan.setToolTipText(Language.getString("OBJECT_GANN_FAN"));
        lbtnGannGrid.setToolTipText(Language.getString("OBJECT_GANN_GRID"));

        // set icons
        lbtnGannLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannLine.gif"));
        lbtnGannFan.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannFan.gif"));
        lbtnGannGrid.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannGrid.gif"));

        //roll
        lbtnGannLine.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannLine_roll.gif"));
        lbtnGannFan.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannFan_roll.gif"));
        lbtnGannGrid.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannGrid_roll.gif"));

        //selected
        lbtnGannLine.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannLine_active.gif"));
        lbtnGannFan.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannFan_active.gif"));
        lbtnGannGrid.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannGrid_active.gif"));

        //rollsecte
        lbtnGannLine.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannLine_active_roll.gif"));
        lbtnGannFan.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannFan_active_roll.gif"));
        lbtnGannGrid.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_GannGrid_active_roll.gif"));

        //set template panel size
        FlowLayout ganToolbarLayout = new FlowLayout(FlowLayout.LEFT, 2, 0);
        lpnlGannAnalisysToolbar.setLayout(ganToolbarLayout);
        lpnlGannAnalisysToolbar.add(lbtnGannLine);
        lpnlGannAnalisysToolbar.add(lbtnGannFan);
        lpnlGannAnalisysToolbar.add(lbtnGannGrid);

        /*ER*************template buttons **************/
        return lpnlGannAnalisysToolbar;
    }

    private JPanel crateDrawingPopUppanel1() {
        JPanel lpnlLineToolbar = new JPanel();

        lbtnArrow = new JToggleButton();
        lbtnSlopeLine = new JToggleButton();
        lbtnArrowSlopeLine = new JToggleButton();//todo do the need ful for arrow btnslope line
        lbtnHorizLine = new JToggleButton();
        lbtnVertiLine = new JToggleButton();
        lbtnRectangle = new JToggleButton();
        lbtnEllipse = new JToggleButton();
        lbtnTextNote = new JToggleButton();
        lbtnRegression = new JToggleButton();
        lbtnZigZag = new JToggleButton();


        lbtnArrow.addActionListener(this);
        lbtnSlopeLine.addActionListener(this);
        lbtnArrowSlopeLine.addActionListener(this);
        lbtnHorizLine.addActionListener(this);
        lbtnVertiLine.addActionListener(this);
        lbtnRectangle.addActionListener(this);
        lbtnEllipse.addActionListener(this);
        lbtnTextNote.addActionListener(this);
        lbtnRegression.addActionListener(this);
        lbtnZigZag.addActionListener(this);

//        btnTextSymbol.addActionListener(this);
//        btnTextSymblAro.addActionListener(this);
//        btnDrawingToolsAro.addActionListener(this);
        //set button sizes
        lbtnArrow.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnSlopeLine.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnArrowSlopeLine.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnHorizLine.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnVertiLine.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnRectangle.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnEllipse.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnTextNote.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnRegression.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnZigZag.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lpnlLineToolbar.setPreferredSize(new Dimension(200, 30));

        //set border
        lpnlLineToolbar.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));
        lbtnArrow.setBorder(BorderFactory.createEmptyBorder());
        lbtnSlopeLine.setBorder(BorderFactory.createEmptyBorder());
        lbtnArrowSlopeLine.setBorder(BorderFactory.createEmptyBorder());
        lbtnZigZag.setBorder(BorderFactory.createEmptyBorder());
        lbtnHorizLine.setBorder(BorderFactory.createEmptyBorder());
        lbtnVertiLine.setBorder(BorderFactory.createEmptyBorder());
        lbtnRectangle.setBorder(BorderFactory.createEmptyBorder());
        lbtnEllipse.setBorder(BorderFactory.createEmptyBorder());
        lbtnTextNote.setBorder(BorderFactory.createEmptyBorder());
        lbtnRegression.setBorder(BorderFactory.createEmptyBorder());

        //set tool tip
        lbtnArrow.setToolTipText(Language.getString("SPECIAL_NOTE"));
        lbtnSlopeLine.setToolTipText(Language.getString("TRND_LINE"));
        lbtnArrowSlopeLine.setToolTipText(Language.getString("ARROW_HEAD_TRND_LINE"));
        lbtnHorizLine.setToolTipText(Language.getString("HIRIZONTAL_LINE"));
        lbtnVertiLine.setToolTipText(Language.getString("VERTICAL_LINE"));
        lbtnRectangle.setToolTipText(Language.getString("RECTANGLE"));
        lbtnEllipse.setToolTipText(Language.getString("ELLIPSE"));
        lbtnTextNote.setToolTipText(Language.getString("TEXT"));
        lbtnRegression.setToolTipText(Language.getString("REGRESSION_LINE"));
        lbtnZigZag.setToolTipText(Language.getString("GRAPH_OBJ_ZIG_ZAG"));

        // set icons
        lbtnArrow.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_arrow.gif"));
        lbtnSlopeLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_slope.gif")); //TODO : NEW ICONS FROM PRADEEP
        lbtnArrowSlopeLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Arrow_slope.gif"));
        lbtnHorizLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_horiz.gif"));
        lbtnVertiLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_verti.gif"));
        lbtnRectangle.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_rect.gif"));
        lbtnEllipse.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ellipse.gif"));
        lbtnTextNote.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_textnote.gif"));
        lbtnRegression.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_regression.gif"));
        lbtnZigZag.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zig_zag.gif"));

        //rollover
        lbtnSlopeLine.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_slope_roll.gif"));
        lbtnArrowSlopeLine.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Arrow_slope_roll.gif"));
        lbtnHorizLine.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_horiz_roll.gif"));
        lbtnVertiLine.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_verti_roll.gif"));
        lbtnRectangle.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_rect_roll.gif"));
        lbtnEllipse.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ellipse_roll.gif"));
        lbtnTextNote.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_textnote_roll.gif"));
        lbtnRegression.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_regression_roll.gif"));
        lbtnZigZag.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zig_zag_roll.gif"));

        lbtnRegression.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_regression_active.gif"));
        lbtnSlopeLine.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_slope_active.gif"));
        lbtnArrowSlopeLine.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Arrow_slope_active.gif"));
        lbtnHorizLine.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_horiz_active.gif"));
        lbtnVertiLine.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_verti_active.gif"));
        lbtnRectangle.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_rect_active.gif"));
        lbtnEllipse.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ellipse_active.gif"));
        lbtnTextNote.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_textnote_active.gif"));
        lbtnZigZag.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zig_zag_active.gif"));

        //roll select
        lbtnSlopeLine.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_slope_active_roll.gif"));
        lbtnArrowSlopeLine.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_Arrow_slope_active_roll.gif"));
        lbtnHorizLine.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_horiz_active_roll.gif"));
        lbtnVertiLine.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_verti_active_roll.gif"));
        lbtnRectangle.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_rect_active_roll.gif"));
        lbtnEllipse.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ellipse_active_roll.gif"));
        lbtnTextNote.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_textnote_active_roll.gif"));
        lbtnRegression.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_regression_active_roll.gif"));
        lbtnZigZag.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_zig_zag_active_roll.gif"));
        //set drawing panel size
        FlowLayout lDrawingLayout = new FlowLayout(FlowLayout.LEFT, 2, 0);
        lpnlLineToolbar.setLayout(lDrawingLayout);

        lpnlLineToolbar.add(lbtnSlopeLine);
        lpnlLineToolbar.add(lbtnArrowSlopeLine);
        lpnlLineToolbar.add(lbtnHorizLine);
        lpnlLineToolbar.add(lbtnVertiLine);
        lpnlLineToolbar.add(lbtnRectangle);
        lpnlLineToolbar.add(lbtnEllipse);
        lpnlLineToolbar.add(lbtnTextNote);
        lpnlLineToolbar.add(lbtnRegression);
        lpnlLineToolbar.add(lbtnZigZag);
        return lpnlLineToolbar;
    }

    private JPanel crateDrawingPopUppanel2() {
        JPanel lpnlLineToolbar2 = new JPanel();

        lbtnArc = new JToggleButton();
        lbtnPitchfork = new JToggleButton();
        lbtnCycleLine = new JToggleButton();

        lbtnArc.addActionListener(this);
        lbtnCycleLine.addActionListener(this);
        lbtnPitchfork.addActionListener(this);

        //set button sizes
        lbtnArc.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnPitchfork.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));
        lbtnCycleLine.setPreferredSize(new Dimension(PrefBtnWd, PrefBtnHt));

        lpnlLineToolbar2.setPreferredSize(new Dimension(200, 30));

        //set border
        lpnlLineToolbar2.setBorder(BorderFactory.createLineBorder(Color.lightGray, 1));

        lbtnArc.setBorder(BorderFactory.createEmptyBorder());
        lbtnPitchfork.setBorder(BorderFactory.createEmptyBorder());
        lbtnCycleLine.setBorder(BorderFactory.createEmptyBorder());

        //set tool tip
        lbtnArc.setToolTipText(Language.getString("GRAPH_OBJ_ARC"));
        lbtnPitchfork.setToolTipText(Language.getString("GRAPH_OBJ_PITCHFORK"));
        lbtnCycleLine.setToolTipText(Language.getString("GRAPH_OBJ_CYCLE_LINE"));

        // set icons
        lbtnArc.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_arc.gif"));
        lbtnPitchfork.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_pitchfork.gif"));
        lbtnCycleLine.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_cycle.gif"));

        //rollover
        lbtnArc.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_arc_roll.gif"));
        lbtnPitchfork.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ pitchfork _roll.gif"));
        lbtnCycleLine.setRolloverIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_cycle_roll.gif"));


        lbtnArc.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_arc_active.gif"));
        lbtnCycleLine.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_cycle_active.gif"));
        lbtnPitchfork.setSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ pitchfork _active.gif"));

        //roll select
        lbtnArc.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_arc_active_roll.gif"));
        lbtnPitchfork.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_ pitchfork _active_roll.gif"));  
        lbtnCycleLine.setRolloverSelectedIcon(new ImageIcon("images/Theme" + Theme.getID() + "/charts/graph_cycle_active_roll.gif"));

        //set drawing panel size
        FlowLayout lDrawingLayout = new FlowLayout(FlowLayout.LEFT, 2, 0);
        lpnlLineToolbar2.setLayout(lDrawingLayout);
        lpnlLineToolbar2.add(lbtnArc);
        lpnlLineToolbar2.add(lbtnCycleLine);
        lpnlLineToolbar2.add(lbtnPitchfork);

        return lpnlLineToolbar2;
    }

    //dolayout tool bar
    public void dolayoutDrawingtools() {
        pnlLineToolbar.removeAll();
        pnlLineToolbar.setOpaque(true);
        pnlLineToolbar.setLayout(lineToolbarLayout);


        try {
            //if could keep the btns objects in hash table no need of doing these manually

            if ((Boolean) hashDrawing.get(AbstractObject.INT_LINE_SLOPE)) {
                btnSlopeLine.setBackground(btnDrawingToolsAro.getBackground());
                pnlLineToolbar.add(btnSlopeLine);
            }
            if ((Boolean) hashDrawing.get(AbstractObject.INT_ARROW_LINE_SLOPE)) {
                btnArrowSlopeLine.setBackground(btnDrawingToolsAro.getBackground());
                pnlLineToolbar.add(btnArrowSlopeLine);
            }
            if ((Boolean) hashDrawing.get(AbstractObject.INT_LINE_HORIZ)) {
                btnHorizLine.setBackground(btnDrawingToolsAro.getBackground());
                pnlLineToolbar.add(btnHorizLine);
            }
            if ((Boolean) hashDrawing.get(AbstractObject.INT_LINE_VERTI)) {
                btnVertiLine.setBackground(btnDrawingToolsAro.getBackground());
                pnlLineToolbar.add(btnVertiLine);
            }
            if ((Boolean) hashDrawing.get(AbstractObject.INT_RECT)) {
                btnRectangle.setBackground(btnDrawingToolsAro.getBackground());
                pnlLineToolbar.add(btnRectangle);
            }
            if ((Boolean) hashDrawing.get(AbstractObject.INT_ELLIPSE)) {
                btnEllipse.setBackground(btnDrawingToolsAro.getBackground());
                pnlLineToolbar.add(btnEllipse);
            }
            if ((Boolean) hashDrawing.get(AbstractObject.INT_TEXT)) {
                btnTextNote.setBackground(btnDrawingToolsAro.getBackground());
                pnlLineToolbar.add(btnTextNote);
            }
            if ((Boolean) hashDrawing.get(AbstractObject.INT_REGRESSION)) {
                btnRegression.setBackground(btnDrawingToolsAro.getBackground());
                pnlLineToolbar.add(btnRegression);
            }
            if ((Boolean) hashDrawing.get(AbstractObject.INT_ZIG_ZAG)) {
                btnZigZag.setBackground(btnDrawingToolsAro.getBackground());
                pnlLineToolbar.add(btnZigZag);
            }
            if ((Boolean) hashDrawing.get(AbstractObject.INT_ARC)) {
                btnArc.setBackground(btnDrawingToolsAro.getBackground()); //TODO: dont know the reason
                pnlLineToolbar.add(btnArc);
            }
            if ((Boolean) hashDrawing.get(AbstractObject.INT_CYCLE_LINE)) {
                btnCycleLine.setBackground(btnDrawingToolsAro.getBackground()); //TODO: dont know the reason
                pnlLineToolbar.add(btnCycleLine);
            }
            if ((Boolean) hashDrawing.get(AbstractObject.INT_ANDREWS_PITCHFORK)) {
                btnPitchfork.setBackground(btnDrawingToolsAro.getBackground()); //TODO: dont know the reason
                pnlLineToolbar.add(btnPitchfork);
            }
            if ((Boolean) hashDrawing.get(AbstractObject.INT_SYMBOL)) {
                pnlLineToolbar.add(pnlTextSymbols);
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }

        pnlLineToolbar.doLayout();
        CardPanels[2].doLayout();
    }

    public void dolayoutFibtools() {
        pnlFibonacciToolbar.removeAll();
        pnlFibonacciToolbar.setLayout(lineToolbarLayout);
        //if could keep the btns objects in hash table no need of doing these manually
        try {
            if ((Boolean) hashFibonaci.get(AbstractObject.INT_FIBONACCI_ARCS)) {
                btnFibArcs.setBackground(btnDrawingToolsAro.getBackground()); //TODO: dont know the reason
                pnlFibonacciToolbar.add(btnFibArcs);
            }
            if ((Boolean) hashFibonaci.get(AbstractObject.INT_FIBONACCI_FANS)) {
                btnFibFans.setBackground(btnDrawingToolsAro.getBackground());
                pnlFibonacciToolbar.add(btnFibFans);
            }
            if ((Boolean) hashFibonaci.get(AbstractObject.INT_FIBONACCI_RETRACEMENTS)) {
                btnFibRetracements.setBackground(btnDrawingToolsAro.getBackground());
                pnlFibonacciToolbar.add(btnFibRetracements);
            }
            if ((Boolean) hashFibonaci.get(AbstractObject.INT_FIBONACCI_EXTENSIONS)) {
                btnFibExtensions.setBackground(btnDrawingToolsAro.getBackground());
                pnlFibonacciToolbar.add(btnFibExtensions);
            }

            if ((Boolean) hashFibonaci.get(AbstractObject.INT_FIBONACCI_ZONES)) {
                btnFibZones.setBackground(btnDrawingToolsAro.getBackground());
                pnlFibonacciToolbar.add(btnFibZones);
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }

        pnlFibonacciToolbar.doLayout();
        CardPanels[3].doLayout();

    }

    public void dolayoutChanceltools() {

        pnlChannelsToolbar.removeAll();
        pnlChannelsToolbar.setLayout(lineToolbarLayout);
        //if could keep the btns objects in hash table no need of doing these manually
        try {
            if ((Boolean) hashChanel.get(AbstractObject.INT_STD_ERROR_CHANNEL)) {
                btnStdErrChannel.setBackground(btnDrawingToolsAro.getBackground());
                pnlChannelsToolbar.add(btnStdErrChannel);
            }
            if ((Boolean) hashChanel.get(AbstractObject.INT_RAFF_REGRESSION)) {
                btnRafChannel.setBackground(btnDrawingToolsAro.getBackground());
                pnlChannelsToolbar.add(btnRafChannel);
            }
            if ((Boolean) hashChanel.get(AbstractObject.INT_STD_DEV_CHANNEL)) {
                btnStdDevChannel.setBackground(btnDrawingToolsAro.getBackground());
                pnlChannelsToolbar.add(btnStdDevChannel);
            }
            if ((Boolean) hashChanel.get(AbstractObject.INT_EQUI_DIST_CHANNEL)) {
                btnEquDistChannel.setBackground(btnDrawingToolsAro.getBackground());
                pnlChannelsToolbar.add(btnEquDistChannel);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        pnlChannelsToolbar.doLayout();
        CardPanels[4].doLayout();
    }

    public void dolayoutGanntools() {

        pnlGannAnalisysToolbar.removeAll();
        pnlGannAnalisysToolbar.setLayout(lineToolbarLayout);
        //if could keep the btns objects in hash table no need of doing these manually
        try {
            if ((Boolean) hashGann.get(AbstractObject.INT_GANN_LINE)) {
                btnGannLine.setBackground(btnDrawingToolsAro.getBackground());
                pnlGannAnalisysToolbar.add(btnGannLine);
            }
            if ((Boolean) hashGann.get(AbstractObject.INT_GANN_FAN)) {
                btnGannFan.setBackground(btnDrawingToolsAro.getBackground());
                pnlGannAnalisysToolbar.add(btnGannFan);
            }
            if ((Boolean) hashGann.get(AbstractObject.INT_GANN_GRID)) {
                btnGannGrid.setBackground(btnDrawingToolsAro.getBackground());
                pnlGannAnalisysToolbar.add(btnGannGrid);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        pnlGannAnalisysToolbar.doLayout();
        CardPanels[6].doLayout();
    }

    public void dolayoutZoomtools() {

        rightPanel.removeAll();

        if (Language.isLTR()) {
            rightPanel.setLayout(lineToolbarRightLayout);
        } else {
            rightPanel.setLayout(lineToolbarLayout);
        }

        //if could keep the btns objects in hash table no need of doing these manually
        if ((Boolean) hashZoom.get(INT_HAND_DRAG)) {
            btnHandDragChart.setBackground(btnDrawingToolsAro.getBackground());
            rightPanel.add(btnHandDragChart);
        }
        if ((Boolean) hashZoom.get(INT_SMART_ZOOM)) {
            btnSmartZoom.setBackground(btnDrawingToolsAro.getBackground());
            rightPanel.add(btnSmartZoom);
        }
        if ((Boolean) hashZoom.get(INT_ZOOM_IN)) {
            btnZoomIn.setBackground(btnDrawingToolsAro.getBackground());
            rightPanel.add(btnZoomIn);
        }
        if ((Boolean) hashZoom.get(INT_ZOOM_OUT)) {
            btnZoomOut.setBackground(btnDrawingToolsAro.getBackground());
            rightPanel.add(btnZoomOut);
        }
        if ((Boolean) hashZoom.get(INT_BOX_ZOOM)) {
            btnBoxZoom.setBackground(btnDrawingToolsAro.getBackground());
            rightPanel.add(btnBoxZoom);
        }
        if ((Boolean) hashZoom.get(INT_DRAG_ZOOM)) {
            btnDragZoom.setBackground(btnDrawingToolsAro.getBackground());
            rightPanel.add(btnDragZoom);
        }
        if ((Boolean) hashZoom.get(INT_RESET_V_ZOOM)) {
            btnResetVZoom.setBackground(btnDrawingToolsAro.getBackground());
            rightPanel.add(btnResetVZoom);
        }
        if ((Boolean) hashZoom.get(INT_RESET_H_ZOOM)) {
            btnResetHZoom.setBackground(btnDrawingToolsAro.getBackground());
            rightPanel.add(btnResetHZoom);
        }
        if ((Boolean) hashZoom.get(INT_RESET_ZOOM)) {
            btnZoomReset.setBackground(btnDrawingToolsAro.getBackground());
            rightPanel.add(btnZoomReset);
        }

        rightPanel.add(btnDrawingToolsAro);
        rightPanel.doLayout();
        navigaterPanel.doLayout();

    }

    public void dolayoutTermtools() {

        //if could keep the btns objects in hash table no need of doing these manually
        pnlTemplatesToolBar.removeAll();
        pnlTemplatesToolBar.setLayout(lineToolbarLayout);

        if ((Boolean) hashTerm.get(INT_SHORT_TERM)) {
            btnShortTermTemplate.setBackground(btnDrawingToolsAro.getBackground()); //TODO: dont know the reason
            pnlTemplatesToolBar.add(btnShortTermTemplate);
        }

        if ((Boolean) hashTerm.get(INT_LONG_TERM)) {
            btnMidTermTemplate.setBackground(btnDrawingToolsAro.getBackground());
            pnlTemplatesToolBar.add(btnMidTermTemplate);
        }

        if ((Boolean) hashTerm.get(INT_MEDIUM_TERM)) {
            btnLongTermTemplate.setBackground(btnDrawingToolsAro.getBackground());
            pnlTemplatesToolBar.add(btnLongTermTemplate);
        }

        pnlTemplatesToolBar.doLayout();
        CardPanels[5].removeAll();
        CardPanels[5].add(pnlTemplatesToolBar);
        CardPanels[5].doLayout();
    }

    public void reDolayoutToolBar() {
        Enumeration e = hashtoolsets.keys();
        while (e.hasMoreElements()) {
            Object key = e.nextElement();
            Hashtable hashObjects = (Hashtable) hashtoolsets.get(key);
            dolayoutChartObjects((Integer) key, hashObjects);
        }

        repaint();
        updateUI();        
    }

    public void dolayoutChartObjects(Integer toolsetType, Hashtable hashChartObjets) {

        //get key by key for each objects

        //TODO:check the isDirty situation

        //todo put switch case : check whether a way using polymorphism
        //TODO : PUT CONSTANTS TO BOTTOM TOOLAR
        switch (toolsetType) {
            case ChartObjectElementList.DRAWING_TOOLS:
                dolayoutDrawingtools();
                break;
            case ChartObjectElementList.FIBONACI_TOOLS:
                dolayoutFibtools();
                break;
            case ChartObjectElementList.CHANNEL_TOOLS:
                dolayoutChanceltools();
                break;
            case ChartObjectElementList.TERM_TOOLS:
                dolayoutTermtools();
                break;
            case ChartObjectElementList.GANN_TOOLS:
                dolayoutGanntools();
                break;
            case ChartObjectElementList.ZOOM_TOOLS:
                dolayoutZoomtools();
                break;
        }

    }

    /*============================== end of add to favourite related ==========================================*/

    private void setSelectedTextSymbol(int ind) {
        //btnTextSymbol.setText(new String(caTextSymbols, ind, 1));
        btnTextSymbol.setIcon(new ImageIcon(getSymbolImage(ind, false, false)));
        btnTextSymbol.setRolloverIcon(new ImageIcon(getSymbolImage(ind, true, false)));
        btnTextSymbol.setSelectedIcon(new ImageIcon(getSymbolImage(ind, false, true)));
        btnTextSymbol.setRolloverSelectedIcon(new ImageIcon(getSymbolImage(ind, true, true)));
        symbolTobeDrawn = ind;
        btnTextSymbol.doClick();
    }

    public Image createTransparentImage(int w, int h) {
//		int[] pixels = new int[w*h];
//	    final int opaque = 0xFF << 24;
//		for (int i=0; i<w*h; i++) {
//			pixels[i] = opaque;
//		}
//		return createImage(new MemoryImageSource(w, h, pixels, 0, w));
        return new BufferedImage(w, h, BufferedImage.TYPE_INT_ARGB_PRE);
    }

    public Image getSymbolImage(int symbolTobeDrawn, boolean rollover, boolean selected) {
        if (parent == null)
            return Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_obj_ellipse.gif");
        Image img = btnTextSymbol.createImage(PrefBtnWd, PrefBtnHt);
        Graphics2D g = (Graphics2D) img.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        if (selected) {
            g.setColor(Theme.getColor("GRAPH_BOTTOM_TOOLBAR_BG_COLOR"));
            g.fillRect(0, 0, PrefBtnWd, PrefBtnHt);
            g.setColor(Color.black);

        } else {
            g.setColor(Theme.getColor("GRAPH_BOTTOM_TOOLBAR_BG_COLOR"));
            g.fillRect(0, 0, PrefBtnWd, PrefBtnHt);
            g.setColor(Color.black);
        }
        g.setFont(SharedMethods.getSymbolFont("fonts/LucidaSansRegular.ttf", 18));
        FontMetrics fmx = g.getFontMetrics();
        String text = new String(caTextSymbols, symbolTobeDrawn, 1);
        Point pt = AbstractObject.getAlignedPosition(AbstractObject.ALIGNMENT_CENTER, PrefBtnWd, PrefBtnHt,
                fmx.stringWidth(text), fmx.getHeight(), fmx.getAscent());
        if (rollover) {
            g.drawString(text, pt.x, pt.y);
            g.setColor(Theme.getColor("GRAPH_ACT_ROLL_BDRCOLOR"));
            g.drawRect(0, 0, PrefBtnWd - 1, PrefBtnHt - 1);
        } else {
            g.drawString(text, pt.x, pt.y + 1);
        }
        return img;
    }

    private Image getDisabledSymbolImage() {
        if (parent == null)
            return Toolkit.getDefaultToolkit().getImage("images/Theme" + Theme.getID() + "/charts/graph_obj_ellipse.gif");
        Image img = btnTextSymbol.createImage(PrefBtnWd, PrefBtnHt);
        Graphics2D g = (Graphics2D) img.getGraphics();
        g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_ON);
        g.setColor(Theme.getColor("BACKGROUND_COLOR"));
        g.fillRect(0, 0, PrefBtnWd, PrefBtnHt);
        g.setColor(UIManager.getColor("controlShadow"));
        g.setFont(SharedMethods.getSymbolFont("fonts/LucidaSansRegular.ttf", 18));
        FontMetrics fmx = g.getFontMetrics();
        String text = new String(caTextSymbols, 4, 1);
        Point pt = AbstractObject.getAlignedPosition(AbstractObject.ALIGNMENT_CENTER, PrefBtnWd, PrefBtnHt,
                fmx.stringWidth(text), fmx.getHeight(), fmx.getAscent());
        g.drawString(text, pt.x, pt.y + 1);
        return img;
    }

    private void applyTemplate(int term) {
        GraphFrame graphFrame = parent.getActiveGraph();
        if (graphFrame != null) {
            graphFrame.applyTemplate(term);
        }
    }

    public void resetVerticalZoom() {

        GraphFrame graphFrame = parent.getActiveGraph();
        if (graphFrame != null) {
            graphFrame.graph.GDMgr.resetCustomYBounds();
            graphFrame.graph.reDrawAll();
        }
    }

    public void resetHorizontalZoom() {
        GraphFrame graphFrame = parent.getActiveGraph();
        int histoyMargin = ChartOptions.getCurrentOptions().chart_right_margin_history;
        int intraDayMargin = ChartOptions.getCurrentOptions().chart_right_margin_intraday;
        graphFrame.graph.GDMgr.setRightMargin(histoyMargin, intraDayMargin);

        if (graphFrame != null) {
            graphFrame.graph.zSlider.setZoomAndPos(100f, 0f);
            graphFrame.graph.GDMgr.setZoomAndPos(100f, 0f);
        }
    }

    //reset all the zooming modes and the cursor only for the active graph
    public void performZoomReset() {

        resetVerticalZoom();
        resetHorizontalZoom();

        activeGraph.graph.zSlider.setZoom();

        if (activeGraph != null) {
            activeGraph.setBoxZoomEnabled(false);
            activeGraph.setDragZoomEnabled(false);
            activeGraph.setZoomInEnabled(false);
            activeGraph.setZoomOutEnabled(false);
            activeGraph.graph.setCursor(Cursor.getDefaultCursor());
        }

        CURSOR_STATUS = STATUS_NONE;
    }

    private void performSmartZoom() {

        smartZoomWindow = SmartZoomWindow.getSharedInstance();
        try {
            /*if (smartZoomWindow == null) {
                smartZoomWindow = SmartZoomWindow.getSharedInstance();
            }*/
            if (!smartZoomWindow.isSmartZoomWindowVisible() && parent.getActiveGraph() != null && parent.getActiveGraph().graph.GDMgr.getGraphStore().size() > 0) {
                GraphFrame graphFrame = parent.getActiveGraph();
                smartZoomWindow.setParameters(graphFrame);
                smartZoomWindow.getDragChartPanel().setGraph(graphFrame);

                int period = graphFrame.graph.getPeriod();
                long pBegin = graphFrame.graph.GDMgr.getPeriodBeginMillisec(period);

                StockGraph graph = graphFrame.graph;
                ChartProperties cp = graphFrame.graph.GDMgr.getBaseCP();
                long interval = graphFrame.graph.getInterval();
                float beginPos = graphFrame.graph.zSlider.getBeginPos();
                float zoom = graphFrame.graph.zSlider.getZoom();

                smartZoomWindow.getDragChartPanel().setDataArray(graph, cp, period, interval, beginPos, zoom, pBegin);
                smartZoomWindow.initButons(graphFrame.graph.isCurrentMode());

                int x, y;
                if (Language.isLTR()) {
                    x = (int) (btnSmartZoom.getLocationOnScreen().getX() - smartZoomWindow.getWidth());
                    y = (int) (btnSmartZoom.getLocationOnScreen().getY() - smartZoomWindow.getHeight());
                } else {
                    x = (int) btnSmartZoom.getLocationOnScreen().getX();
                    y = (int) (btnSmartZoom.getLocationOnScreen().getY() - smartZoomWindow.getHeight());
                }
                smartZoomWindow.setLocation(x, y);
                smartZoomWindow.setVisible(true);
                smartZoomWindow.setAWTListenerEffected(true);
                smartZoomWindow.setSmartZoomWindowVisible(true);

                CURSOR_STATUS = STATUS_SMART_ZOOM;
            } else {
                smartZoomWindow.setVisible(false);
                smartZoomWindow.setAWTListenerEffected(false);
                smartZoomWindow.setSmartZoomWindowVisible(false);
                CURSOR_STATUS = STATUS_NONE;
            }

            if (activeGraph != null) {
                activeGraph.setBoxZoomEnabled(false);
                activeGraph.setDragZoomEnabled(false);
                activeGraph.setZoomInEnabled(false);
                activeGraph.setZoomOutEnabled(false);
                activeGraph.setchartBodyDraggingEnabled(false);
                activeGraph.graph.setCursor(Cursor.getDefaultCursor());
                CURSOR_STATUS = STATUS_NONE;
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void performZoomIn() {

        try {
            if (activeGraph != null) {
                if (!activeGraph.isZoomInEnabled()) {
                    activeGraph.graph.toggleZoomingMode(AbstractObject.INT_ZOOM_IN);
                    activeGraph.zoomInEnabledClick();
                    CURSOR_STATUS = STATUS_ZOOM_IN;
                } else {
                    activeGraph.graph.setCursor(Cursor.getDefaultCursor());
                    CURSOR_STATUS = STATUS_NONE;
                    activeGraph.setZoomInEnabled(false);
                }
                activeGraph.setDragZoomEnabled(false);
                activeGraph.setBoxZoomEnabled(false);
                activeGraph.setZoomOutEnabled(false);
                activeGraph.setchartBodyDraggingEnabled(false);

            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void performHandDragging() {

        try {
            if (activeGraph != null) {
                if (!activeGraph.isChartBodyDraggingEnabled()) {

                    if (activeGraph.graph.zSlider.getZoom() == 100.0f) {
                        JOptionPane.showMessageDialog(activeGraph, Language.getString("HAND_DRAG_WARNING"));
                        return;
                    }
                    activeGraph.graph.setCursor(CURSOR_HAND_DRAGGIN);
                    activeGraph.handDragChartEnabledClick();
                    CURSOR_STATUS = STATUS_CHART_DRAGGING;
                } else {
                    activeGraph.graph.setCursor(Cursor.getDefaultCursor());
                    CURSOR_STATUS = STATUS_NONE;
                    activeGraph.setchartBodyDraggingEnabled(false);
                }
                activeGraph.setDragZoomEnabled(false);
                activeGraph.setBoxZoomEnabled(false);
                activeGraph.setZoomOutEnabled(false);
                activeGraph.setZoomInEnabled(false);

            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void performZoomOut() {

        try {
            if (activeGraph != null) {
                if (!activeGraph.isZoomOutEnabled()) {
                    activeGraph.graph.toggleZoomingMode(AbstractObject.INT_ZOOM_OUT);
                    activeGraph.zoomOutEnabled();
                    CURSOR_STATUS = STATUS_ZOOM_OUT;

                } else {
                    activeGraph.graph.setCursor(Cursor.getDefaultCursor());
                    activeGraph.setZoomOutEnabled(false);
                    CURSOR_STATUS = STATUS_NONE;
                }
                activeGraph.setDragZoomEnabled(false);
                activeGraph.setBoxZoomEnabled(false);
                activeGraph.setZoomInEnabled(false);
                activeGraph.setchartBodyDraggingEnabled(false);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void performBoxZoom() {

        try {
            if (activeGraph != null) {
                if (!activeGraph.isBoxZoomEnabled()) {
                    activeGraph.graph.toggleZoomingMode(AbstractObject.INT_BOX_ZOOM);
                    activeGraph.boxZoomEnabledClick();
                    CURSOR_STATUS = STATUS_BOX_ZOOM;
                } else {
                    activeGraph.graph.setCursor(Cursor.getDefaultCursor());
                    activeGraph.setBoxZoomEnabled(false);
                    CURSOR_STATUS = STATUS_NONE;
                }
                activeGraph.setDragZoomEnabled(false);
                activeGraph.setZoomInEnabled(false);
                activeGraph.setZoomOutEnabled(false);
                activeGraph.setchartBodyDraggingEnabled(false);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    private void performDragZoom() {

        try {
            if (activeGraph != null) {
                if (!activeGraph.isZoomDragEnabled()) {
                    activeGraph.graph.toggleZoomingMode(AbstractObject.INT_DRAG_ZOOM);
                    activeGraph.dragZoomEnabledClicked();
                    CURSOR_STATUS = STATUS_DRAG_ZOOM;
                } else {
                    activeGraph.graph.setCursor(Cursor.getDefaultCursor());
                    activeGraph.setDragZoomEnabled(false);
                    CURSOR_STATUS = STATUS_NONE;
                }
                activeGraph.setBoxZoomEnabled(false);
                activeGraph.setZoomInEnabled(false);
                activeGraph.setZoomOutEnabled(false);
                activeGraph.setchartBodyDraggingEnabled(false);
            }
        }
        catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public void loadNextSymbol(boolean forward) {
        GraphFrame graphFrame = parent.getActiveGraph();
        String key = getNextSymbol(forward);
        if (key != null && !key.equals("")) {
            TemplateFactory.LoadTemplate(graphFrame, new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE),
                    key, graphFrame.isGraphDetached());
        }
        btnNextSymbol.setToolTipText(Language.getString("NEXT_SYMBOL") +
                ((getNextSymbol(true) != null) ? " (" + SharedMethods.getSymbolFromKey(getNextSymbol(true)) + ")" : ""));
        btnPrevSymbol.setToolTipText(Language.getString("PREV_SYMBOL") +
                ((getNextSymbol(false) != null) ? " (" + SharedMethods.getSymbolFromKey(getNextSymbol(false)) + ")" : ""));
    }

    public String getNextSymbol(boolean forward) {
        GraphFrame graphFrame = parent.getActiveGraph();
        if (graphFrame != null) {
            String currentSymbol = graphFrame.graph.GDMgr.getBaseGraph();
            if (currentSymbol != null) {
                try {
                    TemplateFactory.SaveTemplate(new File(TemplateFactory.TEMP_DEFAULT_TEMPLATE_FILE),
                            new GraphFrame[]{graphFrame});
                } catch (Exception e1) {
                    e1.printStackTrace();
                }
                String exchange = SharedMethods.getExchangeFromKey(currentSymbol);

                Hashtable<String, Stock> symbols =
                        DataStore.getSharedInstance().getExchangeSymbolStore(exchange);
                symbolList = new ArrayList<String>();

                if (symbols != null) {
                    Enumeration<Stock> stocks = symbols.elements();

                    while (stocks.hasMoreElements()) {
                        try {
                            Stock stock = stocks.nextElement();
                            String key = stock.getKey();
                            symbolList.add(SharedMethods.getSymbolFromKey(key));
                        } catch (Exception e) {
                            e.printStackTrace();
                        }
                    }

                    Collections.sort(symbolList);

                    int currentIndex = symbolList.indexOf(SharedMethods.getSymbolFromKey(currentSymbol));

                    if (currentIndex > -1) { //if symbol found
                        String nextSymbol;
                        if (forward) {
                            if (currentIndex < (symbolList.size() - 1)) {
                                nextSymbol = symbolList.get(currentIndex + 1);
                            } else {
                                nextSymbol = symbolList.get(0);
                            }
                        } else {
                            if (currentIndex != 0) {
                                nextSymbol = symbolList.get(currentIndex - 1);
                            } else {
                                nextSymbol = symbolList.get(symbolList.size() - 1);
                            }
                        }
                        return SharedMethods.getKey(exchange, nextSymbol, SharedMethods.getInstrumentTypeFromKey(currentSymbol));
                    }
                }
            }
        }
        return null;
    }

    public void showChartOptionWindow() {
        ChartOptionsWindow.getSharedInstance(3).setVisible(true);

    }

    public void fillObjectStateTable() {
        try {
            hashtoolsets.put(ChartObjectElementList.DRAWING_TOOLS, hashDrawing);
            hashtoolsets.put(ChartObjectElementList.FIBONACI_TOOLS, hashFibonaci);
            hashtoolsets.put(ChartObjectElementList.CHANNEL_TOOLS, hashChanel);
            hashtoolsets.put(ChartObjectElementList.TERM_TOOLS, hashTerm);
            hashtoolsets.put(ChartObjectElementList.GANN_TOOLS, hashGann);
            hashtoolsets.put(ChartObjectElementList.ZOOM_TOOLS, hashZoom);
        } catch (Exception ex) {
            ex.printStackTrace();
        }

    }
}