package com.isi.csvr.chart;

import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;
import com.isi.csvr.shared.Language;

import java.awt.*;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: mevana
 * Date: 4/19/11
 * Time: 1:09 PM
 * To change this template use File | Settings | File Templates.
 */
public class DisparityIndex extends ChartProperties implements Indicator{

    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    private static final long serialVersionUID = UID_DISPARITY_INDEX;

    public DisparityIndex(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_DISPARITY_INDEX") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_DISPARITY_INDEX");
        setChartStyle((byte) StockGraph.INT_GRAPH_STYLE_BAR);
        this.timePeriods = 21;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get( anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public void assignDefaultValues(){
        super.assignDefaultValues();
        this.timePeriods = 21;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof DisparityIndex) {
            DisparityIndex dIndex = (DisparityIndex) cp;
            this.timePeriods = dIndex.timePeriods;
            this.innerSource = dIndex.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length == 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_DISPARITY_INDEX") + " " + parent;
    }
//############################################
//implementing Indicator

    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    // this calculation includes 3 intermediate steps EMA, EMA of EMA, EMA of EMA of EMA
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {

        ChartRecord cr;
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(1);
        }
        // steps involved
        byte stepEMA = ChartRecord.STEP_1;
        MovingAverage.getMovingAverage(al, 0, timePeriods, MovingAverage.METHOD_EXPONENTIAL, getOHLCPriority(), stepEMA);

        int loopbegin = timePeriods;
        for (int i = 0; i < loopbegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }

        for (int i = loopbegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(100 * (cr.getValue(getOHLCPriority()) - cr.getStepValue(stepEMA)) / cr.getStepValue(stepEMA));
            }
        }
    }

    public String getShortName() {
        return Language.getString("IND_DISPARITY_INDEX");
//        return "Disparity Index";
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods, byte step1, byte step2, byte step3, byte srcIndex, byte destIndex) {
        byte stepEMA = step1;
//        byte stepEMAofEMA = step2;
//        byte stepEMAofEMAofEMA = step3;
//        ChartRecord cr;
//        int startIndex = bIndex + t_im_ePeriods * 3 - 3;
//        if (al.size() < t_im_ePeriods || t_im_ePeriods == 0) {
//            for (int i = 0; i < al.size(); i++) {
//                cr = (ChartRecord) al.get(i);
//                cr.setValue(destIndex, Indicator.NULL);
//            }
//            return;
//        }
//
//        MovingAverage.getMovingAverage(al, bIndex, t_im_ePeriods, MovingAverage.METHOD_EXPONENTIAL, srcIndex, stepEMA);
//        MovingAverage.getMovingAverage(al, bIndex + t_im_ePeriods - 1, t_im_ePeriods, MovingAverage.METHOD_EXPONENTIAL, stepEMA, stepEMAofEMA);
//        MovingAverage.getMovingAverage(al, bIndex + t_im_ePeriods * 2 - 2, t_im_ePeriods, MovingAverage.METHOD_EXPONENTIAL, stepEMAofEMA, stepEMAofEMAofEMA);
//
//        //(3 * EMA) � (3 * EMA of EMA) + (EMA of EMA of EMA)
//
//        for (int i = startIndex; i < al.size(); i++) {
//            cr = (ChartRecord) al.get(i);
//            if (Double.isNaN(cr.getValue(stepEMA)) || Double.isNaN(cr.getValue(stepEMAofEMA)) || Double.isNaN(cr.getValue(stepEMAofEMAofEMA)))
//                cr.setValue(destIndex, Indicator.NULL);
//            else
//                cr.setValue(destIndex, 3 * cr.getValue(stepEMA) - (3 * cr.getValue(stepEMAofEMA)) + cr.getValue(stepEMAofEMAofEMA));
//        }
    }

    public static int getAuxStepCount() {
        return 1;
    }
}
