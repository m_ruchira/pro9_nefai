package com.isi.csvr.chart;

import com.isi.csvr.shared.Language;

import java.io.Serializable;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 11, 2005
 * Time: 10:59:56 AM
 */


public class ChartSplitPoints implements Serializable {
    /*
    group = 0 // announcements
        type  N/A
    group = 1 // corporate action
        type
            1: Stock Split
            2: Stock Dividend (Bonus)
            3: Rights Issue
            4: Reverse Split
            5: Capital Reduction
            6: Cash dividend
     */
    public long date;
    public int group; // Announcement = 0 | Capital Adjustment = 1
    public int type;
    public float value;
    public int sequence;

    public static final int TYPE_NONE = -1;
    public static final int TYPE_STOCK_SPLIT = 1;
    public static final int TYPE_STOCK_DIVIDEND = 2;
    public static final int TYPE_RIGHT_ISSUE = 3;
    public static final int TYPE_REVERSE_SPLIT = 4;
    public static final int TYPE_CAPITAL_REDUCTION = 5;
    public static final int TYPE_CASH_DIVIDEND = 6;

    public static final String[] TYPES = {"", Language.getString("TIP_STOCK_SPLIT"),
                                            Language.getString("TIP_STOCK_DIVIDEND"),
                                            Language.getString("TIP_RIGHTS_ISSUE"),
                                            Language.getString("TIP_REVERSE_SPLIT"),
                                            Language.getString("TIP_CAPITAL_REDUCTION"),
                                            Language.getString("TIP_CASH_DIVIDEND")};

    public String toString() {
        return "ChartSplitPoints{" +
                "date=" + date +
                ", group=" + group +
                ", type=" + type +
                ", value=" + value +
                ", seq=" + sequence +
                "}";
    }

    public static String getSplitPointName(int type){
        return TYPES[type];
        /*switch (type){
            case 1: return "Stock Split";
            case 2: return "Stock Dividend (Bonus)";
            case 3: return "Rights Issue";
            case 4: return "Reverse Split";
            case 5: return "Capital Reduction";
            case 6: return "Cash dividend";
        }
        return "Unknown"; // Just for testing - ignore this (this should never appear)*/
    }
}
