package com.isi.csvr.chart;

import com.isi.csvr.datastore.Symbols;

/**
 * <p>Title: TW International</p>
 * <p>Description: </p>
 * <p>Copyright: Copyright (c) 2002</p>
 * <p>Company: Integrated Systems International</p>
 * @author Uditha Nagahawatta
 * @version 1.0
 */

public class SymbolsEx extends Symbols{
	public SymbolsEx(byte type){
		super(type);
	}

	public Object[] getSymbolObjects(){
		String[] sa = getSymbols();
		if (sa==null) return null;
		Object[] objArr = new SymbolObject[sa.length];
		for (int i=0; i<objArr.length; i++) {
			objArr[i] = new SymbolObject(sa[i]);
		}
		return objArr;
	}

	public int getIndexOf(String sNewSymbol) {
		int i=0;
		String[] sa = getSymbols();
		//System.out.println("inside getIndexOf**************");
		while (i<sa.length) {
			//System.out.println("sa["+i+"] "+sa[i]+" sNewSymbol "+sNewSymbol);
			if (sa[i].equals(sNewSymbol))
				return i;
			i++;
		}
		return -1;
	}

}