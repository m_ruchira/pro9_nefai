package com.isi.csvr.chart;

import com.isi.csvr.shared.Language;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;

import java.util.ArrayList;
import java.awt.*;
import java.io.Serializable;

/**
 * Title: Mubasher Pro
 * Description:
 * Date: May 28, 2005 - Time: 6:55:33 PM
 * Copyright: Copyright (c) 2005 Integrated Systems International
 *
 * @author Udaka Liyanapathirana
 * @version 2.0
 */
public class CommodityChannelIndex extends ChartProperties implements Indicator, Serializable {
    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    private static final long serialVersionUID = UID_CCI;

    public CommodityChannelIndex(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_CCI") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_CCI");
        this.timePeriods = 14;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get( anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }

    public void assignDefaultValues(){
        super.assignDefaultValues();
        this.timePeriods = 14;
    }
    
    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof CommodityChannelIndex) {
            CommodityChannelIndex arn = (CommodityChannelIndex) cp;
            this.timePeriods = arn.timePeriods;
            this.innerSource = arn.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }
        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_CCI") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    //This calculation has 2 intermediate steps: TypicalPrice, MA
    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        double currVal;
        ChartRecord cr;
        //tmp var for sop
        long entryTime = System.currentTimeMillis();
        int loopBegin = timePeriods - 1;
        if (loopBegin > al.size()) return;

        //setting step size 2
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setStepSize(2);
        }
        // steps involved
        byte stepTypicalPrice = ChartRecord.STEP_1;
        byte stepMA = ChartRecord.STEP_2;

        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = (cr.Close + cr.Low + cr.High) / 3f;
            cr.setStepValue(stepTypicalPrice, currVal);
        }
        //calculating simple. mov. avg
        MovingAverage.getMovingAverage(al, 0, timePeriods, MovingAverage.METHOD_SIMPLE, stepTypicalPrice, stepMA);
        double denomenator, numerator;
        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            numerator = cr.getStepValue(stepTypicalPrice) - cr.getStepValue(stepMA);
            denomenator = 0;
            for (int j = 0; j < timePeriods; j++) {
                ChartRecord crLoop = (ChartRecord) al.get(i - j);
                denomenator += Math.abs(cr.getStepValue(stepMA) - crLoop.getStepValue(stepTypicalPrice));
            }
            currVal = (numerator == 0) ? 0 : (denomenator == 0) ? 10000d : numerator * timePeriods / (denomenator * 0.015d); // 10000d stands for the infinity here
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null)
                aPoint.setIndicatorValue(currVal);
        }
        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            GDM.removeIndicatorPoint(cr.Time, index, getID());
        }
        //System.out.println("**** Commodity Channel Index Calc time " + (entryTime - System.currentTimeMillis()));
    }
    //############################################

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex, int t_im_ePeriods, byte step1, byte step2, byte destIndex) {

        ChartRecord cr, crPre;
        int startIndex = bIndex + t_im_ePeriods - 1;
        if (al.size() < startIndex || t_im_ePeriods == 0) {
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }
        byte stepTypical = step1;
        byte stepSMAofTypical = step2;
        for (int i = 0; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(stepTypical, (cr.High + cr.Low + cr.Close) / 3);
        }
        MovingAverage.getMovingAverage(al, 0, t_im_ePeriods, MovingAverage.METHOD_SIMPLE, stepTypical, stepSMAofTypical);

        double preViousSub = 0;
        
        for (int i = 0; i < startIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }

        for (int i = startIndex; i < al.size(); i++) {
            preViousSub = 0;
            cr = (ChartRecord) al.get(i);

            for (int j = 0; j < t_im_ePeriods; j++) {
                crPre = (ChartRecord) al.get(i - j);
                preViousSub += Math.abs(cr.getValue(stepSMAofTypical) - crPre.getValue(stepTypical));
            }
            preViousSub = preViousSub * .015 / t_im_ePeriods;

            cr.setValue(destIndex, (cr.getValue(stepTypical) - cr.getValue(stepSMAofTypical)) / preViousSub);
        }

    }

    public String getShortName() {
        return Language.getString("IND_CCI");
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 2;
    }
}
