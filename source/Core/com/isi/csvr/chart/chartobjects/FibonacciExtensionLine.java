package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.GraphDataManager;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
* User: charith nidarsha
* Date: Mar 2, 2009
* Time: 2:53:17 PM
* To change this template use File | Settings | File Templates.
*/ /*inner class implementation - added by charithn*/
public class FibonacciExtensionLine {
    private double percentageValue = 0.1d;
    private Color lineColor = Color.RED;
    private float penWidth;
    private byte penStyle;
    private boolean enabled;

    public FibonacciExtensionLine(double percentageValue, Color lineColor, byte penstyle, float penwidth, boolean enabled) {
        this.percentageValue = percentageValue;
        this.lineColor = lineColor;
        this.penStyle = penstyle;
        this.penWidth = penwidth;
        this.enabled = enabled;
    }

    public FibonacciExtensionLine() {
        this.percentageValue = 0.15;
        this.lineColor = Color.BLUE;
        this.penStyle = GraphDataManager.STYLE_DASH;
        this.penWidth = 1f;
        this.enabled = false;
    }

    public double getPercentageValue() {
        return percentageValue;
    }

    public void setPercentageValue(double percentageValue) {
        this.percentageValue = percentageValue;
    }

    public Color getLineColor() {
        return lineColor;
    }

    public void setLineColor(Color lineColor) {
        this.lineColor = lineColor;
    }

    public float getPenWidth() {
        return penWidth;
    }

    public void setPenWidth(float penWidth) {
        this.penWidth = penWidth;
    }

    public byte getPenStyle() {
        return penStyle;
    }

    public void setPenStyle(byte penStyle) {
        this.penStyle = penStyle;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }

}
