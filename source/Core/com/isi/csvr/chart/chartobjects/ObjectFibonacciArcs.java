package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectDefaultProperty;
import com.isi.csvr.chart.chartobjects.LineStudy;
import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.PropertyDialogFactory;
import com.isi.csvr.chart.StockGraph;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 12:17:07 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectFibonacciArcs extends LineStudy {

    private static final long serialVersionUID = AbstractObject.UID_FIBONACCI_ARCS;

    public ObjectFibonacciArcs() {
        super();
        objType = AbstractObject.INT_FIBONACCI_ARCS;
    }

    public ObjectFibonacciArcs(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_FIBONACCI_ARCS;
        isShowingDragToolTip = true;
        drawingOnBackground = false;
        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectDefaultProperty op = (ObjectDefaultProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
            this.isFreeStyle = op.isFreeStyle();
        }
    }

}

