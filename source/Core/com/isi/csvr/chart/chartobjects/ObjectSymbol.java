package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.chartobjects.LineStudy;
import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.StockGraph;

import java.awt.*;

/**
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 12:02:10 PM
 */
public class ObjectSymbol extends LineStudy {

    private static final long serialVersionUID = AbstractObject.UID_SYMBOL;

    public ObjectSymbol(){
        super();
        objType = AbstractObject.INT_SYMBOL;
    }

    public ObjectSymbol(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph){
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_SYMBOL;
        isShowingDragToolTip = false;
    }

}
