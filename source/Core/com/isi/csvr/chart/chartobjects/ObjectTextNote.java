package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectTextNoteProperty;
import com.isi.csvr.chart.chartobjects.LineStudy;
import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.PropertyDialogFactory;
import com.isi.csvr.chart.StockGraph;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 12:03:44 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectTextNote extends LineStudy {

    private static final long serialVersionUID = AbstractObject.UID_TEXT;
    public static final int MIN_SIZE = 28;

    /*private boolean isShowBorder = true;

    public boolean isShowBorder() {
        return isShowBorder;
    }

    public void setShowBorder(boolean showBorder) {
        isShowBorder = showBorder;
    }*/

    public ObjectTextNote() {
        super();
        objType = AbstractObject.INT_TEXT;
    }

    public ObjectTextNote(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_TEXT;
        isShowingDragToolTip = true;

        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectTextNoteProperty op = (ObjectTextNoteProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
            this.fontColor = op.getTextColor();
            this.fillColor = op.getFillColor();
            this.transparent = op.isTransparent();
            this.font = op.getFont();
            this.isFreeStyle = op.isFreeStyle();
            this.isShowBorder = op.isShowBorder();
        }
    }

}
