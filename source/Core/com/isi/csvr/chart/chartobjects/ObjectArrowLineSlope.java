package com.isi.csvr.chart.chartobjects;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Feb 2, 2009
 * Time: 10:18:39 AM
 * To change this template use File | Settings | File Templates.
 */

import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectLineProperty;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.chart.*;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.theme.Theme;

import java.awt.*;
import java.awt.geom.Line2D;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

//public class ObjectLineSlope extends AbstractObject implements Extendible,InfoObjectDisplayble {
public class ObjectArrowLineSlope extends AbstractObject implements InfoObjectDisplayble, Alarmable {

    private static final long serialVersionUID = AbstractObject.UID_ARROW_LINE_SLOPE;

    private boolean extendedLeft;
    private boolean extendedRight;
    private boolean alarmEnabled;
    private boolean barCount;
    private boolean netChange;
    private boolean pctChange;
    private boolean timeSpan;
    private String alarmMesage;

    private double angle = -1;

    TWDecimalFormat formatter = new TWDecimalFormat("#############.00");
    private ArrayList<ToolTipDataRow> dataRows = new ArrayList<ToolTipDataRow>();
    SimpleDateFormat timeFormatter = null;
    private final int ARROW_HEAD_LENGTH = 15;
    final int ARROW_FOOT_LENGTH = 5;

    public ObjectArrowLineSlope() {
        super();
        objType = AbstractObject.INT_ARROW_LINE_SLOPE;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
    }

    public ObjectArrowLineSlope(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_ARROW_LINE_SLOPE;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR"); //added by sathyajith to make this themabl at the construction
        alarmMesage = Language.getString("TREND_LINE_POPUP_MSG");
        isShowingDragToolTip = true;

        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectLineProperty op = (ObjectLineProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
            this.extendedLeft = op.isExtendedLeft();
            this.extendedRight = op.isExtendedRight();
            this.barCount = op.isShowBarCount();
            this.pctChange = op.isShowPctChange();
            this.netChange = op.isShowNetChange();
            this.isFreeStyle = op.isFreeStyle();
        }
    }

    public void assignValuesFrom(AbstractObject ao) {
        super.assignValuesFrom(ao);
        if (ao instanceof ObjectArrowLineSlope) {
            this.extendedLeft = ((ObjectArrowLineSlope) ao).extendedLeft;
            this.extendedRight = ((ObjectArrowLineSlope) ao).extendedRight;

            this.pctChange = ((ObjectArrowLineSlope) ao).pctChange;
            this.barCount = ((ObjectArrowLineSlope) ao).barCount;
            this.timeSpan = ((ObjectArrowLineSlope) ao).timeSpan;
            this.netChange = ((ObjectArrowLineSlope) ao).netChange;
            this.alarmEnabled = ((ObjectArrowLineSlope) ao).alarmEnabled;
            this.alarmMesage = ((ObjectArrowLineSlope) ao).alarmMesage;
        }
    }

    protected void drawDragImage(Graphics gg, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {
        Graphics2D g = (Graphics2D) gg;
        g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);
        int x1, x2, y1, y2;


        if (objectMoving) {
            x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
            x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
            y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
            y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
            x1 = x1 - Xadj;
            x2 = x2 - Xadj;

            if (isSnapToPrice) {
                y1 = snapPixYArr[0];
                y2 = snapPixYArr[1];
            } else {
                y1 = y1 - (int) Yadj;
                y2 = y2 - (int) Yadj;
            }

        } else {
            x1 = Math.round((xArr[movingPtIndex] - bIndex) * xFactor) + grfLeft - Xadj;
            x2 = Math.round((xArr[1 - movingPtIndex] - bIndex) * xFactor) + grfLeft;

            if (isSnapToPrice) {
                y1 = snapPixYArr[movingPtIndex];
                y2 = snapPixYArr[1 - movingPtIndex];
            } else {
                y1 = getPixelFortheYValue(yArray[movingPtIndex], rect, minY, yFactor, grf_Ht + grf_Top - (int) Yadj);
                y2 = getPixelFortheYValue(yArray[1 - movingPtIndex], rect, minY, yFactor, grf_Ht + grf_Top);
            }
        }

        /* Edited by Mevan @ 2009-07-22
        *
        *  Fixed a bug which existed with extending left & right.
        *  Extending Left happens to the left Side of Screen & vise versa, regardless of the points 1. & 2.
        *  Y = mX + c : m=gradient , c=interceptY , m = (Y2-Y1)/(X2-X1)
        *
        */
        int tempX1 = (movingPtIndex == 0) ? x1 : x2;
        int tempX2 = (movingPtIndex == 0) ? x2 : x1;
        int tempY1 = (movingPtIndex == 0) ? y1 : y2;
        int tempY2 = (movingPtIndex == 0) ? y2 : y1;


        if (extendedLeft) {
            float gradient = (float) (tempY2 - tempY1) / (float) (tempX2 - tempX1);
            float interceptY = tempY1 - (gradient * tempX1);
            //g.drawString("grd " + gradient + "x1 " + tempX1,50, 100);
            //System.out.println("grd " + gradient +  " x1 " + tempX1 + " x2 " + tempX2);
            if ((x1 == x2)) {
                tempY2 = grf_Top - grf_Ht;
            } else {
                if (gradient != 0) {
                    if (tempX1 < tempX2) {
                        tempY1 = (int) interceptY;
                        tempX1 = (int) ((tempY2 - interceptY) / gradient);
                        tempX1 = 0;
                    } else {
                        tempX1 = grfLeft + grfWidth;
                        tempY1 = (int) (gradient * tempX1 + interceptY);
                    }
                } else {
                    tempY1 = tempY2;
                    tempX1 = (tempX1 < tempX2) ? 0 : (rect.x + rect.width);
                }
            }
        }
        if (extendedRight) {
            float gradient = (float) (tempY2 - tempY1) / (float) (tempX2 - tempX1);
            float interceptY = tempY1 - (gradient * tempX1);
            if ((x1 == x2)) {
                tempY2 = grf_Top - grf_Ht;
            } else {
                if (gradient != 0) {
                    if (tempX1 > tempX2) {
                        tempY2 = (int) interceptY;
                        tempX2 = 0;
                    } else {
                        tempX2 = grfLeft + grfWidth;
                        tempY2 = (int) (gradient * tempX2 + interceptY);
                    }
                } else {
                    tempY2 = tempY1;
                    tempX1 = (tempX1 < tempX2) ? 0 : (rect.x + rect.width);
                }
            }
        }

        g.drawLine(tempX1, tempY1, tempX2, tempY2);


        /************************arrow head ***********************************************/
//2nd attempt arrow head logic ********************/
        double angle = -(double) (y2 - y1) / (double) (x2 - x1);
        angle = Math.atan(angle);
        int pAx, pAy, p1x, p1y, p2x, p2y;
        int[] xArrValues = {x1, x2};
        int[] yArrValues = {y1, y2};
        int lx = (int) (ARROW_FOOT_LENGTH * Math.sin(angle));
        int ly = (int) (ARROW_FOOT_LENGTH * Math.cos(angle));


//        if (xArrValues[1 - movingPtIndex]  x2 >= x1) {
        if (xArrValues[1 - movingPtIndex] >= xArrValues[movingPtIndex]) {
//            pAx = x2 - (int) (ARROW_HEAD_LENGTH * Math.cos(angle));
            pAx = xArrValues[1 - movingPtIndex] - (int) (ARROW_HEAD_LENGTH * Math.cos(angle));
//            pAy = y2 + (int) (ARROW_HEAD_LENGTH * Math.sin(angle));
            pAy = yArrValues[1 - movingPtIndex] + (int) (ARROW_HEAD_LENGTH * Math.sin(angle));

//            g.drawRect(pAx,pAy,5 ,5);
            //p1

            p1x = pAx + lx;
            p1y = pAy + ly;

            p2x = pAx - lx;
            p2y = pAy - ly;
//            int[] aArrowX  = {x2,p1x,p2x};
            int[] aArrowX = {xArrValues[1 - movingPtIndex], p1x, p2x};
            int[] aArrowY = {yArrValues[1 - movingPtIndex], p1y, p2y};

            g.fillPolygon(aArrowX, aArrowY, 3);
            //drawing a border
            g.setColor(Color.black);
            //  g.drawPolygon(aArrowX, aArrowY, 3);

        } else {

            pAx = xArrValues[1 - movingPtIndex] + (int) (ARROW_HEAD_LENGTH * Math.cos(angle));
            pAy = yArrValues[1 - movingPtIndex] - (int) (ARROW_HEAD_LENGTH * Math.sin(angle));

            //p1

            p1x = pAx + lx;
            p1y = pAy + ly;

            //    p2
            p2x = pAx - lx;
            p2y = pAy - ly;

            int[] aArrowX = {xArrValues[1 - movingPtIndex], p1x, p2x};
//            int[] aArrowX  = {x2,p1x,p2x};
            int[] aArrowY = {yArrValues[1 - movingPtIndex], p1y, p2y};

            g.fillPolygon(aArrowX, aArrowY, 3);
            //drawing a border
            g.setColor(Color.black);
            // g.drawPolygon(aArrowX, aArrowY, 3);


        }


//2nd attempt arrow head logic ********************/

        /************************arrow head ***********************************************/


        //constructing tool tip data

        tempX1 = x1;
        tempX2 = x2;
        tempY1 = y1;
        tempY2 = y2;
//                int tempX1 = x1;
//        int tempX2 = x2;
//        int tempY1 = y1;
//        int tempY2 = y2;

        dataRows.clear();

        boolean currentMode = false;
        if (currentMode) {
            timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }

        double angleTemp = 0;
        angleTemp = -(double) (tempY2 - tempY1) / (double) (tempX2 - tempX1);
        angleTemp = Math.atan(angleTemp);
        if (tempY1 > tempY2 && tempX1 < tempX2) {
            angleTemp = Math.toDegrees(angleTemp);
        } else if (tempY1 > tempY2 && tempX1 > tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 180;
        } else if (tempY1 < tempY2 && tempX1 > tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 180;
        } else if (tempY1 < tempY2 && tempX1 < tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 360;
        }

        this.angle = angleTemp;
        double begin = getYValueForthePixel(y1, rect, minY, yFactor, grf_Ht + grf_Top);
        double end = getYValueForthePixel(y2, rect, minY, yFactor, grf_Ht + grf_Top);
        String beginPrice = String.valueOf(graph.GDMgr.formatPriceField(begin, ((WindowPanel) rect).isInThousands()));
        String endPrice = String.valueOf(graph.GDMgr.formatPriceField(end, ((WindowPanel) rect).isInThousands()));

        double dNetChange = end - begin;
        double dpctChange = 0d;
        if (begin != 0.0) dpctChange = dNetChange * 100 / begin;
        String sPctChg = formatter.format(dpctChange) + "%";

        float index1 = bIndex + (x1 - grfLeft) / xFactor;
        float index2 = bIndex + (x2 - grfLeft) / xFactor;

        long[] newXarray = graph.GDMgr.convertXArrayIndexToTime(new float[]{index1, index2});
        Date D1, D2 = null;
        D1 = new Date(newXarray[0]);
        if (newXarray.length > 1) {
            D2 = new Date(newXarray[1]);
        }

        String beginDate = timeFormatter.format(D1);
        String endDate = timeFormatter.format(D2);


        int barCount = Math.abs(Math.round(index2 - index1));

        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_PRICE"), beginPrice));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_DATE"), endDate));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_PRICE"), endPrice));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_ANGLE"), String.valueOf(formatter.format(angleTemp))));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_NET_CHANGE"), formatter.format(end - begin)));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_PCT_CHANGE"), sPctChg));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BARS"), String.valueOf(barCount)));

    }

    public void drawOnGraphics(Graphics gg, float[] xArr,
                               boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                               int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        boolean drawSelected = selected && !isPrinting;
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);
        g.setClip(rect);
        g.setFont(new Font("Tahoma", Font.PLAIN, 11));
        int x1, x2, y1, y2;
        x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
        y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);

        int tempX1 = x1;
        int tempX2 = x2;
        int tempY1 = y1;
        int tempY2 = y2;

        /* Edited by Mevan @ 2009-07-21
       *
       *  Fixed a bug which existed when extending arrow to left & right.
       *  Extending Left happens from ArrowBase & extending Right from ArrowHead.,
       *  Y = mX + c : m=gradient , c=interceptY , m = (Y2-Y1)/(X2-X1)
       *
        */
        if (extendedLeft) {
            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
            float interceptY = y1 - (gradient * x1);
            if ((x1 == x2)) {
                tempY2 = grf_Top - grf_Ht;
            } else {
                if (gradient != 0) {
                    if (x1 < x2) {

                        tempY1 = (int) interceptY;

                        tempX1 = (int) ((tempY2 - interceptY) / gradient);
                        tempX1 = 0;
                    } else {
                        tempX1 = grfLeft + grfWidth;
                        tempY1 = (int) (gradient * tempX1 + interceptY);
                    }
                } else {
                    tempY1 = tempY2;
                    tempX1 = (x1 < x2) ? 0 : (rect.x + rect.width);
                }
            }
        }
        if (extendedRight) {
            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
            float interceptY = y1 - (gradient * x1);
            if ((x1 == x2)) {
                tempY2 = grf_Top - grf_Ht;
            } else {
                if (gradient != 0) {
                    if (x1 > x2) {
                        tempY2 = (int) interceptY;
                        tempX2 = 0;
                    } else {
                        tempX2 = grfLeft + grfWidth;
                        tempY2 = (int) (gradient * tempX2 + interceptY);
                    }
                } else {
                    tempY2 = tempY1;
                    tempX2 = (x1 < x2) ? rect.x + rect.width : 0;
                }
            }
        }

        //// previous logic for the extend left & right. WARNING: Contains bugs, commented out - Mevan @ 2009-07-21
//        if (extendedLeft) {
//            float ratio;
//            if ((x1 == x2)) {
//                tempY1 = grf_Top - grf_Ht;
//            } else {
//                ratio = (float) (y2 - y1) / (float) (x2 - x1);
//                //tempX1 = x1 - extendX;
//                //tempY1 = y1 - Math.round(ratio * extendX);
//
//                if (ratio != 0) {
//                    float c = y1 - (ratio * x1);
//                    tempY1 = getPixelFortheYValue(minY, rect, minY, yFactor, grf_Ht + grf_Top);
//                    tempX1 = (int) ((tempY1 - c) / ratio);
//                } else {
//                    //tempY1 = getPixelFortheYValue(minY, rect, minY, yFactor, grf_Ht + grf_Top);
//                    tempX1 = rect.x;
//
//                }
//            }
//        }
//        if (extendedRight) {
//            float ratio;
//            if ((x1 == x2)) {
//                tempY2 = grf_Top - grf_Ht;
//            } else {
//                ratio = (float) (y2 - y1) / (float) (x2 - x1);
//                //tempX2 = x1 + extendX;
//                //tempY2 = y1 + Math.round(ratio * extendX);
//                if (ratio != 0) {
//                    tempY2 = rect.y;
//                    float c = y1 - (ratio * x1);
//                    tempX2 = (int) ((tempY2 - c) / ratio);
//                } else {
//                    tempX2 = rect.x + rect.width;
//                }
//            }
//        }


        g.drawLine(tempX1, tempY1, tempX2, tempY2);


        /*****************calcuation for arrow head**************
         * **consider the + & -
         * * (xn,y2) where xn = x2 - (headlength/cos@)
         * (x2,yn) where yn = y2 - (3/sin@)
         /*****************calcuation for arrow head*****************/

        double angle = -(double) (y2 - y1) / (double) (x2 - x1);
        angle = Math.atan(angle);
/*
        double angle30 = Math.toRadians(30d);

        //case 1 (x2 > x1)  && (y1 > y2) && Q > B (theta > beeta)
//        double ArrowSideLength  = ARROW_HEAD_LENGTH /Math.cos(angle);
        double ArrowSideLength  = ARROW_HEAD_LENGTH /Math.cos(angle30);

        int yN = (int) (ArrowSideLength * Math.sin( angle30 + angle));
        int xN = (int) (ArrowSideLength * Math.cos(angle30 + angle));

        int y2N = y2+  yN;
        int x2N = x2 -  xN;

        int yM = (int) (ArrowSideLength * Math.sin( angle - angle30  ));
        int xM = (int) (ArrowSideLength * Math.cos(angle - angle30 ));

        int y2M = y2 +   yM;
        int x2M = x2 - xM;

        if (y2 > y1){
           y2M =  y2 - yM;
           y2N = y2 - yN;
        }

        if (x1 > x2){
            x2N = x2  + xN;
            x2M = x2 + xM;
        }
        g.drawLine(x2,y2,x2N,y2N);
        g.drawLine(x2,y2,x2M,y2M);
//        g.drawLine(x2N,y2N,x2M,y2M);
        int [] xArrowHeadpoints = {x2,x2N,x2M};
        int [] yArrowHeadpoints = {y2,y2N,y2M};
        g.fillPolygon(xArrowHeadpoints,yArrowHeadpoints,3);

        float xdiff = (float) (x2 - x1);
        float ydiff = (float) (y2 - y1);
        float cost =  (float) (xdiff  /(Math.sqrt((xdiff * xdiff) + (ydiff * ydiff))));

        int xnUp = x2 - (int) (ARROW_HEAD_LENGTH / (cost));
//        g.drawLine(x2,y2,xnUp,y2);

        float sint = (float) (ydiff /(Math.sqrt((xdiff * xdiff) + (ydiff * ydiff))));
        int ynUp = y2 -  (int) (ARROW_HEAD_LENGTH / (sint));

       first attampt arrow head */

//        g.drawLine(x2,y2,x2,ynUp);
//        g.drawLine(xnUp,y2,x2,ynUp);

/*****************   calcuation for arrow head**************/

/************************************************************************************************************************/


//2nd attempt arrow head logic ********************/
        int pAx, pAy, p1x, p1y, p2x, p2y;
        int lx = (int) (ARROW_FOOT_LENGTH * Math.sin(angle));
        int ly = (int) (ARROW_FOOT_LENGTH * Math.cos(angle));


        if (x2 >= x1) {
            pAx = x2 - (int) (ARROW_HEAD_LENGTH * Math.cos(angle));
            pAy = y2 + (int) (ARROW_HEAD_LENGTH * Math.sin(angle));

//            g.drawRect(pAx,pAy,5 ,5);
            //p1

            p1x = pAx + lx;
            p1y = pAy + ly;

            p2x = pAx - lx;
            p2y = pAy - ly;
            int[] aArrowX = {x2, p1x, p2x};
            int[] aArrowY = {y2, p1y, p2y};

            g.fillPolygon(aArrowX, aArrowY, 3);
            //drawing a border
            //g.setColor(Color.black);
            g.drawPolygon(aArrowX, aArrowY, 3);

        } else {

            pAx = x2 + (int) (ARROW_HEAD_LENGTH * Math.cos(angle));
            pAy = y2 - (int) (ARROW_HEAD_LENGTH * Math.sin(angle));

            //p1

            p1x = pAx + lx;
            p1y = pAy + ly;

            //    p2
            p2x = pAx - lx;
            p2y = pAy - ly;

            int[] aArrowX = {x2, p1x, p2x};
            int[] aArrowY = {y2, p1y, p2y};

            g.fillPolygon(aArrowX, aArrowY, 3);
            //drawing a border
            //g.setColor(Color.black);
            g.drawPolygon(aArrowX, aArrowY, 3);


        }


//2nd attempt arrow head logic ********************/


        double angleTemp = 0;
        angleTemp = -(double) (tempY2 - tempY1) / (double) (tempX2 - tempX1);
        angleTemp = Math.atan(angleTemp);
        if (tempY1 > tempY2 && tempX1 < tempX2) {
            angleTemp = Math.toDegrees(angleTemp);
        } else if (tempY1 > tempY2 && tempX1 > tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 180;
        } else if (tempY1 < tempY2 && tempX1 > tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 180;
        } else if (tempY1 < tempY2 && tempX1 < tempX2) {
            angleTemp = Math.toDegrees(angleTemp) + 360;
        }

        this.angle = angleTemp;

        /************************************ Object information Lables ************************************/

        Point barCountDrawPoint = getBarCountDrawPoint(new Point(tempX1, tempY1), new Point(tempX2, tempY2));
        Point netChangeDrawPoint = getNetChangeDrawPoint(new Point(tempX1, tempY1), new Point(tempX2, tempY2));
        Point pctChangeDrawPoint = getPctChangeDrawPoint(new Point(tempX1, tempY1), new Point(tempX2, tempY2));
        /***********************Bar Count***********************/
        if (barCount) {
            int iBarCount = Math.round(xArr[1] - xArr[0]);
            if (tempX1 <= 20) {
                //tempX1 = grfLeft;
            }
            int x = (tempX1 + tempX2) / 2 + 10;
            int y = (tempY1 + tempY2) / 2;
            //g.drawString(Math.abs(iBarCount) + " Bars", tempX1 - 20 , tempY1 - 40);
            g.drawString(Math.abs(iBarCount) + Language.getString("GRAPH_TRENDS_BAR_COUNT"), barCountDrawPoint.x, barCountDrawPoint.y);
        }
        if (netChange) {
            double dNetChange = yArray[1] - yArray[0];
            String sNetChange = formatter.format(dNetChange);
//            g.drawString("Net Change: " + formatter.format(dNetChange), tempX1 - 20, tempY1 - 30);
            if (dNetChange > 0) sNetChange = "+" + sNetChange;
            //g.drawString(sNetChange, tempX1 - 20, tempY1 - 30);
            int x = (tempX1 + tempX2) / 2 + 10;
            int y = (tempY1 + tempY2) / 2 + 10;
            g.drawString(sNetChange, netChangeDrawPoint.x, netChangeDrawPoint.y);

        }
        /***********************Bar Count***********************/

        /***********************pctChange***********************/
        if (pctChange) {
            double dNetChange = yArray[1] - yArray[0];
            double dpctChange = 0d;
            if (yArray[0] != 0.0) dpctChange = dNetChange * 100 / yArray[0];
            String sPctChg = formatter.format(dpctChange);
            if (dpctChange > 0) sPctChg = "+" + sPctChg;
//            g.drawString("Pct change:" + formatter.format(dpctChange) + "%", tempX1 - 20, tempY1 - 20);
            int x = (tempX1 + tempX2) / 2 + 10;
            int y = (tempY1 + tempY2) / 2 + 20;
            //g.drawString(sPctChg + "%", tempX1 - 20, tempY1 - 20);
            g.drawString(sPctChg + "%", pctChangeDrawPoint.x, pctChangeDrawPoint.y);
        }

        /***********************pctChange***********************/


        /***********************timeSpan***********************/
        //ToDo need to pass the mode to drawOnGraphichs method
//        if (timeSpan) {
//            int diffTimeSpan;
//            if (true) {
//                long lTimeSpan = xArray[1] - xArray[0];
//                diffTimeSpan = (int) (lTimeSpan / (long) 1000 * 24 * 3600);
//            }
////            g.drawString("Time Span: " + diffTimeSpan , tempX1 - 20, tempY1 - 10);
//            g.drawString("" + diffTimeSpan , tempX1 - 20, tempY1 - 10);
//        }
        /***********************timeSpan***********************/

/************************************ end of Object information Lables ************************************/
        if (drawSelected) {
            g.setRenderingHint(RenderingHints.KEY_TEXT_ANTIALIASING, RenderingHints.VALUE_TEXT_ANTIALIAS_OFF);
            g.setStroke(selectPen);
            g.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            g.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
            g.setColor(Color.BLACK);
            g.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            g.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
        }

        //constructing tool tip data
        dataRows.clear();
        double dNetChange = yArray[1] - yArray[0];
        double dpctChange = 0d;
        if (yArray[0] != 0.0) dpctChange = dNetChange * 100 / yArray[0];
        String sPctChg = formatter.format(dpctChange) + "%";

        if (graph.isCurrentMode()) {
            timeFormatter = new SimpleDateFormat("dd/MM/yy '-' HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }

        Date D1, D2 = null;
        D1 = new Date(xArray[0]);
        if (xArray.length > 1) {
            D2 = new Date(xArray[1]);
        }

        String beginDate = timeFormatter.format(D1);
        String endDate = timeFormatter.format(D2);
        String beginPrice = graph.GDMgr.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands());
        String endPrice = graph.GDMgr.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());


        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_DATE"), beginDate)); // TODO: need to do for the intraday mode also. but need to pass the mode here.
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BEGIN_PRICE"), beginPrice));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_DATE"), endDate));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_END_PRICE"), endPrice));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_ANGLE"), String.valueOf(formatter.format(angleTemp))));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_NET_CHANGE"), formatter.format(yArray[1] - yArray[0])));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_PCT_CHANGE"), sPctChg));
        dataRows.add(new ToolTipDataRow(Language.getString("TREND_BARS"), String.valueOf(Math.round(xArr[1] - xArr[0]))));
    }

    private Point getBarCountDrawPoint(Point p1, Point p2) {
        Point p = new Point(0, 0);
        if ((angle >= 0 && angle <= 45) || (angle >= 180 && angle <= 225)) {
            p.x = (p1.x + p2.x) / 2 + 4;
            p.y = (p1.y + p2.y) / 2 + 15;
        } else if ((angle >= 300 && angle <= 360) || (angle >= 120 && angle <= 180)) {
            p.x = (p1.x + p2.x) / 2 + 4;
            p.y = (p1.y + p2.y) / 2 - 12;
        } else {
            p.x = (p1.x + p2.x) / 2 + 12;
            p.y = (p1.y + p2.y) / 2 + 4;
        }
        return p;
    }

    private Point getNetChangeDrawPoint(Point p1, Point p2) {
        Point p = new Point(0, 0);
        if ((angle >= 0 && angle <= 45) || (angle >= 180 && angle <= 225)) {
            p.x = (p1.x + p2.x) / 2 + 4;
            p.y = (p1.y + p2.y) / 2 + 25;
        } else if ((angle >= 300 && angle <= 360) || (angle >= 120 && angle <= 180)) {
            p.x = (p1.x + p2.x) / 2 + 4;
            p.y = (p1.y + p2.y) / 2 - 22;
        } else {
            p.x = (p1.x + p2.x) / 2 + 12;
            p.y = (p1.y + p2.y) / 2 + 14;
        }
        return p;
    }

    private Point getPctChangeDrawPoint(Point p1, Point p2) {
        Point p = new Point(0, 0);
        if ((angle >= 0 && angle <= 45) || (angle >= 180 && angle <= 225)) {
            p.x = (p1.x + p2.x) / 2 + 4;
            p.y = (p1.y + p2.y) / 2 + 35;
        } else if ((angle >= 300 && angle <= 360) || (angle >= 120 && angle <= 180)) {
            p.x = (p1.x + p2.x) / 2 + 4;
            p.y = (p1.y + p2.y) / 2 - 32;
        } else {
            p.x = (p1.x + p2.x) / 2 + 12;
            p.y = (p1.y + p2.y) / 2 + 24;
        }
        return p;
    }

    public boolean isCursorOnObject(int x, int y, float[] xArr, boolean select, int grfLeft, int grfWidth, float bIndex,
                                    int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {

        if (!rect.contains(x, y) || !isMove)
            return false;// Added to return false if out of the chart area. - Mevan @ 2009-07-21
        int x1, x2, y1, y2;
        x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;
        y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        y2 = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);

        if (isOnTheLine(x1, y1, x2, y2, x, y, select, isMove)) {
            if (!objectMoving) {
                objectMoving = false;
            }
            return true;
        }

        //Added in order to select the extension - Pramoda
        int tempX1 = x1;
        int tempX2 = x2;
        int tempY1 = y1;
        int tempY2 = y2;


        /* Edited by Mevan @ 2009-07-21
       *
       *  Fixed a bug which existed when extending arrow to left & right.
       *  Extending Left happens from ArrowBase & extending Right from ArrowHead.,
       *  Y = mX + c : m=gradient , c=interceptY , m = (Y2-Y1)/(X2-X1)
       *
        */
        if (extendedLeft) {
            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
            float interceptY = y1 - (gradient * x1);
            if ((x1 == x2)) {
                tempY2 = grf_Top - grf_Ht;
            } else {
                if (gradient != 0) {
                    if (x1 < x2) {

                        tempY1 = (int) interceptY;

                        tempX1 = (int) ((tempY2 - interceptY) / gradient);
                        tempX1 = 0;
                    } else {
                        tempX1 = grfLeft + grfWidth;
                        tempY1 = (int) (gradient * tempX1 + interceptY);
                    }
                } else {
                    tempY1 = tempY2;
                    tempX1 = (x1 < x2) ? 0 : (rect.x + rect.width);
                }
            }
        }
        if (extendedRight) {
            float gradient = (float) (y2 - y1) / (float) (x2 - x1);
            float interceptY = y1 - (gradient * x1);
            if ((x1 == x2)) {
                tempY2 = grf_Top - grf_Ht;
            } else {
                if (gradient != 0) {
                    if (x1 > x2) {
                        tempY2 = (int) interceptY;
                        tempX2 = 0;
                    } else {
                        tempX2 = grfLeft + grfWidth;
                        tempY2 = (int) (gradient * tempX2 + interceptY);
                    }
                } else {
                    tempY2 = tempY1;
                    tempX2 = (x1 < x2) ? rect.x + rect.width : 0;
                }
            }
        }

//        if (extendedLeft) {
//            float ratio;
//            if ((x1 == x2)) {
//                tempY1 = grf_Top - grf_Ht;
//            } else {
//                ratio = (float) (y2 - y1) / (float) (x2 - x1);
//                tempX1 = x1 - rect.width;
//                tempY1 = y1 - Math.round(ratio * rect.width);
//            }
//        }
//        if (extendedRight) {
//            float ratio;
//            if ((x1 == x2)) {
//                tempY2 = grf_Top - grf_Ht;
//            } else {
//                ratio = (float) (y2 - y1) / (float) (x2 - x1);
//                tempX2 = x1 + rect.width;
//                tempY2 = y1 + Math.round(ratio * rect.width);
//            }
//        }

        return isOnTheLine(tempX1, tempY1, tempX2, tempY2, x, y, select, isMove);
    }

    protected String getValueString(boolean currMode) {
        SimpleDateFormat timeFormatter;
        if (currMode) {
            timeFormatter = new SimpleDateFormat("HH:mm");
        } else {
            timeFormatter = new SimpleDateFormat("dd/MM/yy");
        }
        Date D1, D2 = null;
        D1 = new Date(xArray[0]);
        if (xArray.length > 1) {
            D2 = new Date(xArray[1]);
        }
        /* return timeFormatter.format(D1) + ", " +
      GraphDataManager.formatPriceField(yArray[0], ((WindowPanel) rect).isInThousands()) + ", " +
      timeFormatter.format(D2) + ", " +
      GraphDataManager.formatPriceField(yArray[1], ((WindowPanel) rect).isInThousands());*/
        return "";
    }

    public String getShortName() {
        return Language.getString("ARROW_HEAD_TRND_LINE");
    }

    public boolean isExtendedLeft() {
        return extendedLeft;
    }

    public void setExtendedLeft(boolean extendedLeft) {
        this.extendedLeft = extendedLeft;
    }

    public boolean isExtendedRight() {
        return extendedRight;
    }

    public void setExtendedRight(boolean extendedRight) {
        this.extendedRight = extendedRight;
    }


    public boolean isbarCount() {
        return barCount;
    }

    public boolean isnetChange() {
        return netChange;
    }

    public boolean ispctChange() {
        return pctChange;
    }

    public boolean istimeSpan() {
        return timeSpan;
    }

    public void setbarCount(boolean barCount) {
        this.barCount = barCount;
    }

    public void setnetChange(boolean netChange) {
        this.netChange = netChange;
    }

    public void setpctChange(boolean pctChange) {
        this.pctChange = pctChange;
    }

    public void settimeSpan(boolean timeSpan) {
        this.timeSpan = timeSpan;
    }

    public ArrayList<ToolTipDataRow> getToolTipRows() {
        return dataRows;
    }

    public void setAngle(boolean angle) {
        //To change body of implemented methods use File | Settings | File Templates.
    }//to check the abstract object is inside a given rectangle

    public boolean isAngle() {
        return false;  //To change body of implemented methods use File | Settings | File Templates.
    }

    public double getAngle() {
        return angle;
    }

    public boolean isAlarmEnabled() {
        return alarmEnabled;
    }

    public void setAlarmMessage(String msg) {
        this.alarmMesage = msg;
    }

    public boolean isIntesectingLine(Line2D priceLine) {

        ChartProperties cp = graph.GDMgr.getBaseCP();
        int[] yPixelArr2 = graph.GDMgr.convertYValueArrayToPixelArr(getYArr(), graph.GDMgr.getIndexOfTheRect(graph.panels, cp.getRect()));
        float[] xIndexArr2 = getIndexArray();
        int[] xPixelArr2 = {graph.GDMgr.getPixelFortheIndex(xIndexArr2[0]), graph.GDMgr.getPixelFortheIndex(xIndexArr2[1])};
        Line2D lineSlope = new Line2D.Double(new Point(xPixelArr2[0], yPixelArr2[0]), new Point(xPixelArr2[1], yPixelArr2[1]));
        return lineSlope.intersectsLine(priceLine);
    }

    public String getAlarmMessage() {
        return alarmMesage;
    }

    public void setAlarmEnabled(boolean isAlarmEnabled) {
        this.alarmEnabled = isAlarmEnabled;
    }

    public void saveToLayout(org.w3c.dom.Element xmlobj, org.w3c.dom.Document document) {
        super.saveToLayout(xmlobj, document);
        TemplateFactory.saveProperty(xmlobj, document, "AlarmEnabled", Boolean.toString(this.alarmEnabled));
        TemplateFactory.saveProperty(xmlobj, document, "AlarmMessage", this.alarmMesage);
    }

    public void loadFromLayout(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression) {
        super.loadFromLayout(xpath, document, preExpression);
        this.alarmEnabled = Boolean.parseBoolean(TemplateFactory.loadProperty(xpath, document, preExpression + "/AlarmEnabled"));
        this.alarmMesage = TemplateFactory.loadProperty(xpath, document, preExpression + "/AlarmMessage");
    }

    public boolean isInsideTheRectangle(Rectangle r, StockGraph graph) {

        int pnlID = graph.GDMgr.getIndexOfTheRect(graph.panels, getRect());
        double y1 = graph.GDMgr.getPixelFortheYValue(yArray[0], pnlID);

        float[] indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[0]});
        double x1 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

        double y2 = graph.GDMgr.getPixelFortheYValue(yArray[1], pnlID);

        indexArr = graph.GDMgr.convertXArrayTimeToIndex(new long[]{xArray[1]});
        double x2 = graph.GDMgr.getPixelFortheIndex(indexArr[0]);

        //if end points within rectangle
        if (r.contains(x1, y1)) {
            return true;
        }
        if (r.contains(x2, y2)) {
            return true;
        }

        double m = (y2 - y1) / (x2 - x1);
        double c = (y1 - m * x1);

        boolean inside = false;
        double xMin = Math.min(x1, x2);
        double xMax = Math.max(x1, x2);

        //if line line points doesnot contains inside the rectangle
        for (double i = xMin; i < xMax; i = i + 1) {
            double y = m * i + c;
            if (r.contains(i, y)) {
                inside = true;
                break;
            }
        }
        if (!inside) {
            return false;
        }

        int x0 = r.x;
        int y0 = r.y;

        double y = m * x0 + c;

        //m is postitve and intersects the rectangle
        if (y > r.y && y < (r.y + r.height)) {
            return true;
        }

        x0 = x0 + r.width;
        y = m * x0 + c;

        if (y > r.y && y < (r.y + r.height)) {
            return true;
        }

        //m is negative and intersects the rectangle
        double x = (r.y - c) / m;

        if (x > r.x && x < (r.x + r.width)) {
            return true;
        }

        x = ((r.y + r.height) - c) / m;
        if (x > r.x && x < (r.x + r.width)) {
            return true;
        }
        return false;

    }
}
