package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.GraphDataManager;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
* User: charith nidarsha
* Date: Feb 24, 2009
* Time: 9:45:27 AM
* To change this template use File | Settings | File Templates.
*/ //added by charithn
public class FibonacciRetracementiLine {
    private double percentageValue = 0.1d;
    private Color lineColor = Color.RED;
    private float penWidth;
    private byte penStyle;
    private boolean enabled;


    public FibonacciRetracementiLine(double percentageValue, Color lineColor, byte penstyle, float penwidth, boolean enabled) {
        this.percentageValue = percentageValue;
        this.lineColor = lineColor;
        this.penStyle = penstyle;
        this.penWidth = penwidth;
        this.enabled = enabled;
    }

    public FibonacciRetracementiLine() {
        this.percentageValue = .15d;
        //this.lineColor = new Color(102,102,255);
        this.lineColor = Color.BLUE;
        this.penStyle = GraphDataManager.STYLE_DASH;
        this.penWidth = 1f;
        this.enabled = false;
    }

    public double getPercentageValue() {
        return percentageValue;
    }

    public void setPercentageValue(double percentageValue) {
        this.percentageValue = percentageValue;
    }

    public Color getLineColor() {
        return lineColor;
    }

    public void setLineColor(Color lineColor) {
        this.lineColor = lineColor;
    }

    public float getPenWidth() {
        return penWidth;
    }

    public void setPenWidth(float penWidth) {
        this.penWidth = penWidth;
    }

    public byte getPenStyle() {
        return penStyle;
    }

    public void setPenStyle(byte penStyle) {
        this.penStyle = penStyle;
    }

    public boolean isEnabled() {
        return enabled;
    }

    public void setEnabled(boolean enabled) {
        this.enabled = enabled;
    }
}
