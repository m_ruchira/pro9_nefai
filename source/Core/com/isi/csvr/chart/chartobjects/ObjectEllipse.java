package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectRectangularProperty;
import com.isi.csvr.chart.chartobjects.LineStudy;
import com.isi.csvr.chart.chartobjects.AbstractObject;
import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.PropertyDialogFactory;
import com.isi.csvr.chart.StockGraph;

import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Udaka
 * Date: Mar 31, 2006
 * Time: 12:10:29 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectEllipse extends LineStudy {

    private static final long serialVersionUID = AbstractObject.UID_ELLIPSE;

    public ObjectEllipse() {
        super();
        objType = AbstractObject.INT_ELLIPSE;
        drawingOnBackground = true;
        transparent = false;
    }

    public ObjectEllipse(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_ELLIPSE;
        drawingOnBackground = true;
        transparent = false;
        isShowingDragToolTip = true;
        
        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectRectangularProperty orp = (ObjectRectangularProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = orp.getLineColor();
            this.penStyle = orp.getLineStyle();
            this.penWidth = orp.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
            this.fillColor = orp.getBackGroundColor();
            this.drawingOnBackground = orp.isSendBehindChrts();
            this.transparent = orp.isTransparent();
            this.isFreeStyle = orp.isFreeStyle();
        }
    }

}

