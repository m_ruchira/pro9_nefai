package com.isi.csvr.chart.chartobjects;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.chart.ChartProperties;
import com.isi.csvr.chart.PropertyDialogFactory;
import com.isi.csvr.chart.StockGraph;
import com.isi.csvr.chart.GraphDataManager;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectPropertyStore;
import com.isi.csvr.chart.chartobjects.chartobjectproperties.ObjectDefaultProperty;
import com.isi.csvr.shared.Language;

import java.text.SimpleDateFormat;
import java.awt.*;
import java.awt.geom.QuadCurve2D;
import java.awt.geom.AffineTransform;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Apr 21, 2009
 * Time: 4:41:24 PM
 * To change this template use File | Settings | File Templates.
 */
public class ObjectArc extends AbstractObject {

    private static final long serialVersionUID = AbstractObject.UID_LINE_VERTI;
    SimpleDateFormat timeFormatter = new SimpleDateFormat("yyyy-MM-dd-HH:mm");

    public ObjectArc() {
        super();
        objType = AbstractObject.INT_ARC;
        showOnAllPanels = false;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR");
    }

    public ObjectArc(long[] xArr, double[] yArr, float[] indexArray, ChartProperties cp, Rectangle r, StockGraph graph) {
        super(xArr, yArr, indexArray, cp, r, graph);
        objType = AbstractObject.INT_ARC;
        showOnAllPanels = false;
        color = Theme.getColor("GRAPH_SYMBOL_COLOR");
        drawingOnBackground = false;
        isUsingUserDefault = ObjectPropertyStore.getSharedInstance().hasDefauiltProperties(objType);
        if (isUsingUserDefault) {
            ObjectDefaultProperty op = (ObjectDefaultProperty) ObjectPropertyStore.getSharedInstance().getPropertyStore().get(objType);
            this.color = op.getLineColor();
            this.penStyle = op.getLineStyle();
            this.penWidth = op.getLineThickness();
            this.setPen(PropertyDialogFactory.getBasicStroke(penStyle, penWidth));
            this.isFreeStyle = op.isFreeStyle();
        }
    }

    protected void drawDragImage(Graphics g, float[] xArr,
                                 int Xadj, double Yadj, int grfLeft, int grfWidth,
                                 float bIndex, int grf_Top, int grf_Ht,
                                 double minY, float xFactor, double yFactor, boolean isSnapToPrice, int[] snapPixYArr) {

        super.drawDragImage(g, xArr, Xadj, Yadj, grfLeft, grfWidth, bIndex, grf_Top,
                grf_Ht, minY, xFactor, yFactor, false, null);

        Graphics2D gg = (Graphics2D) g;

        g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);
        int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft; // x1 in pixel coordinates
        int xCtr = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates
//        int xCtr = Math.round((xArr[2] - bIndex) * xFactor) + grfLeft; // x2 in pixel coordinates
        int x2 = Math.round((xArr[2] - bIndex) * xFactor) + grfLeft; // x3 in pixel coordinates
//        int x2 = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft; // x3 in pixel coordinates

        int y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, (grf_Top + grf_Ht)); //y1 in pixel coordinates
        int yCtr = getPixelFortheYValue(yArray[1], rect, minY, yFactor, (grf_Top + grf_Ht)); //y2 in pixel coordinates
        int y2 = getPixelFortheYValue(yArray[2], rect, minY, yFactor, (grf_Top + grf_Ht)); //y3 in pixel coordinates

        if (objectMoving) {
            x1 = x1 - Xadj;
            xCtr = xCtr - Xadj;
            x2 = x2 - Xadj;
            y1 = y1 - (int) Yadj;
            yCtr = yCtr - (int) Yadj;
            y2 = y2 - (int) Yadj;
        } else {
            if (movingPtIndex == 0) {
                x1 = x1 - Xadj;
                y1 = y1 - (int) Yadj;
            } else if (movingPtIndex == 1) {
                xCtr = xCtr - Xadj;
                yCtr = yCtr - (int) Yadj;
            } else if (movingPtIndex == 2) {
                x2 = x2 - Xadj;
                y2 = y2 - (int) Yadj;
            }
        }

        QuadCurve2D q = new QuadCurve2D.Float();
        q.setCurve(x1, y1, x2, y2, xCtr, yCtr);
//        q.setCurve(x1, y1, xCtr, yCtr, x2, y2);
        gg.draw(q);
    }

    protected void drawOnGraphics(Graphics gg, float[] xArr,
                                  boolean isPrinting, int grfLeft, int grfWidth, float bIndex,
                                  int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, float eIndex) {
        Graphics2D g = (Graphics2D) gg;
        g.setColor(color);
        g.setStroke(pen);

        g.setClip(rect.x, rect.y + GraphDataManager.titleHeight, rect.width, rect.height - GraphDataManager.titleHeight);

        int x1 = Math.round((xArr[0] - bIndex) * xFactor) + grfLeft;
        int xCtr = Math.round((xArr[1] - bIndex) * xFactor) + grfLeft;  // Control Point 
        int x2 = Math.round((xArr[2] - bIndex) * xFactor) + grfLeft;
        int y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, grf_Ht + grf_Top);
        int yCtr = getPixelFortheYValue(yArray[1], rect, minY, yFactor, grf_Ht + grf_Top);
        int y2 = getPixelFortheYValue(yArray[2], rect, minY, yFactor, grf_Ht + grf_Top);

        QuadCurve2D q = new QuadCurve2D.Float();
        q.setCurve(x1, y1, x2, y2, xCtr, yCtr);
        g.draw(q);
        q.getPathIterator(AffineTransform.getRotateInstance(Math.toRadians(30)));

        boolean drawSelected = selected && !isPrinting;

        if (drawSelected) {
            g.setStroke(selectPen);

            gg.fillRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            gg.fillRect(xCtr - halfBox, yCtr - halfBox, halfBox * 2, halfBox * 2);
            gg.fillRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);

            gg.setColor(Color.BLACK);
            gg.drawRect(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
            gg.drawRect(xCtr - halfBox, yCtr - halfBox, halfBox * 2, halfBox * 2);
            gg.drawRect(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
        }

    }

    protected boolean isCursorOnObject(int x, int y, float[] xArray, boolean select, int grfLeft, int grfWidth, float bIndex,
                                       int grf_Top, int grf_Ht, double minY, float xFactor, double yFactor, boolean isMove) {

//        movingPtIndex = -1;
        if (!rect.contains(x, y) || !isMove) return false;

        int x1 = Math.round((xArray[0] - bIndex) * xFactor) + grfLeft;
        int x2 = Math.round((xArray[2] - bIndex) * xFactor) + grfLeft;
        int xCtr = Math.round((xArray[1] - bIndex) * xFactor) + grfLeft;

        int y1 = getPixelFortheYValue(yArray[0], rect, minY, yFactor, (grf_Top + grf_Ht));
        int y2 = getPixelFortheYValue(yArray[2], rect, minY, yFactor, (grf_Top + grf_Ht));
        int yCtr = getPixelFortheYValue(yArray[1], rect, minY, yFactor, (grf_Top + grf_Ht));

        Rectangle rect1 = new Rectangle(x1 - halfBox, y1 - halfBox, halfBox * 2, halfBox * 2);
        Rectangle rect2 = new Rectangle(x2 - halfBox, y2 - halfBox, halfBox * 2, halfBox * 2);
        Rectangle rectCtr = new Rectangle(xCtr - halfBox, yCtr - halfBox, halfBox * 2, halfBox * 2);

        objectMoving = false;
        if (rect1.contains(x, y)) {
            movingPtIndex = 0;
            if (!selected)
                selected = select;
            return true;
        } else if (rectCtr.contains(x, y)) {
            movingPtIndex = 1;
            if (!selected)
                selected = select;
            return true;
        } else if (rect2.contains(x, y)) {
            movingPtIndex = 2;
            if (!selected)
                selected = select;
            return true;
        } else {

            movingPtIndex = -1;
        }

        QuadCurve2D q = new QuadCurve2D.Float();
        q.setCurve(x1, y1, x2, y2, xCtr, yCtr);

        if (q.contains(x, y)) {
            objectMoving = true;
            if (!selected)
                selected = select;
            return true;
        }
        return false;
    }

    public String getShortName() {
        return Language.getString("GRAPH_OBJ_ARC");
    }

    protected String getValueString(boolean currMode) {
        return "";
    }
}
