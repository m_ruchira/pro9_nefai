package com.isi.csvr.chart;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Sep 23, 2008
 * Time: 5:25:50 PM
 * To change this template use File | Settings | File Templates.
 */
public interface InfoObjectDisplayble {

    boolean isExtendedLeft();
    boolean isExtendedRight();
    boolean isAlarmEnabled();
    void setExtendedLeft(boolean extendedLeft);
    void setExtendedRight(boolean extendedRight);
    void setAlarmEnabled(boolean isAlarmEnabled);

    void setAngle(boolean angle);

    boolean isbarCount();
    boolean isnetChange();
    boolean ispctChange();
    boolean istimeSpan();
    
    boolean isAngle();

    void setbarCount(boolean barCount);
    void setnetChange(boolean netChange);
    void setpctChange(boolean pctChange);
    void settimeSpan(boolean timeSpan);
    
    /*void setAlarmMessage(String msg);
    String getAlarmMessage();*/

    
}
