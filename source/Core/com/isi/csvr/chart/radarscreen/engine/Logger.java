package com.isi.csvr.chart.radarscreen.engine;

import com.isi.csvr.shared.TWDateFormat;
import com.isi.csvr.shared.Settings;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 12, 2009
 * Time: 11:41:32 AM
 * To change this template use File | Settings | File Templates.
 */
public class Logger {

    private static boolean debugEnabled = true;
    private static String result = "";
    private static final TWDateFormat logFormatter = new TWDateFormat("yyyy MMM dd - HH:mm");
    private static final String LOG_FILE_PATH = Settings.getAbsolutepath() + "radarscreen/log/";


    public static void initLog() {
        deleteOldLogFiles();
    }

    public static void logMessage(String message) {
        if (debugEnabled) {
            //String log = logFormatter.format(System.currentTimeMillis()) + " ----- " + message;
            String log = "******** Radar log ******* " + "" + message;
            System.out.println(log);
            //System.out.println(result);
            result += log + "\n";
        }
    }

    public static void writeToFile() {

        System.out.println(result);
        
        String fileName = LOG_FILE_PATH + logFormatter.format(System.currentTimeMillis()) + ".txt";
        try {
            BufferedWriter writer = new BufferedWriter(new FileWriter(fileName));            
            //writer.write(result);
            writer.close();
            clearLog();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static void clearLog() {
        result = "";
    }

    private static void deleteOldLogFiles() {

        //Directory
    }
}
