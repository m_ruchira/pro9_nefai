package com.isi.csvr.chart.radarscreen.engine;

import com.isi.csvr.shared.Settings;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 11, 2009
 * Time: 7:45:08 AM
 * To change this template use File | Settings | File Templates.
 */
public class RadarConstants {

    //radar screen logic types
    protected static final int LOGIC_TYPE_BUY = 0;
    protected static final int LOGIC_TYPE_SELL = 1;

    //file paths
    public static final String RADAR_SETTINGS_FILE_PATH = Settings.getAbsolutepath() + "radarscreen/settings.xml";
    public static final String RADAR_CLASS_PATH = Settings.getAbsolutepath() + "/radarscreen/";

    //profit types
    protected static final byte PROFIT_TYPE_NONE = -1;
    public static final byte PROFIT_TYPE_REALIZED = 0;
    public static final byte PROFIT_TYPE_UNREALIZED = 1;

    //refresh rate of the scanner thread
    protected static final long REFRESH_RATE = 20000;
    protected static final long INITIAL_PULSE_RATE = 1000;

    public static final byte NO_OF_SUB_COLUMNS = 3;   

    public static final double MINIMUM_PROFIT_RATIO = -999999; //used for sorting also
}
