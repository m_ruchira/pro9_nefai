package com.isi.csvr.chart.radarscreen.engine;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Jul 11, 2009
 * Time: 3:13:21 PM
 * To change this template use File | Settings | File Templates.
 */
public class StrategyResultInfo {

    private StrategyData strategyData;
    private byte profitType = RadarConstants.PROFIT_TYPE_NONE;
    private double profitRatio = Double.NaN;
    private ArrayList<RadarTransactionPoint> transactions;

    private double buyAverage = Double.NaN;
    private double sellAverage = Double.NaN;

    public StrategyResultInfo(StrategyData strategyData, byte profitType, double profitRatio) {
        this.strategyData = strategyData;
        this.profitType = profitType;
        this.profitRatio = profitRatio;
    }

    public StrategyResultInfo(byte profitType, double profitRatio) {
        this.profitType = profitType;
        this.profitRatio = profitRatio;
    }

    public StrategyResultInfo() {
    }

    public StrategyData getStrategyData() {
        return strategyData;
    }

    public void setStrategyData(StrategyData strategyData) {
        this.strategyData = strategyData;
    }

    public byte getProfitType() {
        return profitType;
    }

    public void setProfitType(byte profitType) {
        this.profitType = profitType;
    }

    public double getProfitRatio() {
        return profitRatio;
    }

    public void setProfitRatio(double profitRatio) {
        this.profitRatio = profitRatio;
    }

    public ArrayList<RadarTransactionPoint> getTransactions() {
        return transactions;
    }

    public void setTransactions(ArrayList<RadarTransactionPoint> transactions) {
        this.transactions = transactions;
    }

    public double getBuyAverage() {
        return buyAverage;
    }

    public void setBuyAverage(double buyAverage) {
        this.buyAverage = buyAverage;
    }

    public double getSellAverage() {
        return sellAverage;
    }

    public void setSellAverage(double sellAverage) {
        this.sellAverage = sellAverage;
    }
}
