package com.isi.csvr.chart;

/**
 * Created by IntelliJ IDEA.
* User: charithn
* Date: May 14, 2009
* Time: 9:25:12 AM
* To change this template use File | Settings | File Templates.
*/
class StockFactorType {

    public int type = -1;
    public float factor = -1;

    StockFactorType(int type, float factor) {
        this.type = type;
        this.factor = factor;
    }
}
