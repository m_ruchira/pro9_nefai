package com.isi.csvr.chart;

/**
 * User: Pramoda
 * Date: Mar 15, 2007
 * Time: 2:10:39 PM
 */
public enum CalcMethod {
    PERCENT, POINTS
}
