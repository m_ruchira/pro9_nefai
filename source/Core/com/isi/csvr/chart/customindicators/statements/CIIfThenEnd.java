package com.isi.csvr.chart.customindicators.statements;

import com.isi.csvr.chart.customindicators.StatementType;
import com.isi.csvr.chart.customindicators.expressions.CIExpression;

import java.util.ArrayList;


public class CIIfThenEnd extends CIStatement {

    private CIExpression boolExprssn;
    public CIExpression getBoolExpression() {
        return boolExprssn;
    }
    private ArrayList statements;
    public ArrayList getStatements() {
        return statements;
    }

    public CIIfThenEnd(CIExpression boolExprssn, ArrayList statements){
        super(StatementType.IF_THEN_END);
        this.boolExprssn = boolExprssn;
        this.statements = statements;
    }

    public String generateCode(CIProgram program) {
        ArrayList alTemp = program.collectTempVars();

        StringBuilder sb = new StringBuilder();
        boolean looping = program.addLoopingHeader(sb);

        sb.append("if (");
        sb.append(boolExprssn.generateCode(program));
        sb.append("){\n");
        for (int i = 0; i < statements.size(); i++) {
            CIStatement stmt = (CIStatement)statements.get(i);
            sb.append(stmt.generateCode(program));
            if ((stmt.getStatementType() == StatementType.OBJECT_ASSIGN) ||
                (stmt.getStatementType() == StatementType.DECLARE_ASSIGN)) {
                String s = sb.toString();
                if (!s.endsWith(";") && !s.endsWith("\n") && !s.endsWith("}")) {
                    sb.append(";\n");
                }
            }
        }
        sb.append("}\n");

        if (looping) {
            program.addLoopingFooter(sb);
        }

        program.discardTempVars(alTemp);
        return sb.toString();
    }
}

