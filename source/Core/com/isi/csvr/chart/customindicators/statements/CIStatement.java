package com.isi.csvr.chart.customindicators.statements;

import com.isi.csvr.chart.customindicators.StatementType;
import com.isi.csvr.chart.customindicators.ICodeGenerator;

public class CIStatement implements ICodeGenerator {

    private StatementType type;
    public StatementType getStatementType() {
        return type;
    }

    public CIStatement() {
        this.type = StatementType.EMPTY;
    }

    public CIStatement(StatementType type) {
        this.type = type;
    }

    public String generateCode(CIProgram program) {
        return "";
    }

}
