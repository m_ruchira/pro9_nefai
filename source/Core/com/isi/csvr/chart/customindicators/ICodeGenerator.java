package com.isi.csvr.chart.customindicators;

import com.isi.csvr.chart.customindicators.statements.CIProgram;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Feb 4, 2007
 * Time: 4:01:06 PM
 * To change this template use File | Settings | File Templates.
 */
public interface ICodeGenerator {
    String generateCode(CIProgram program);
}
