//using System;
//using System.Collections.Generic;
//using System.Text;
//using CustomIndicators.Statements;

package com.isi.csvr.chart.customindicators.expressions;

import com.isi.csvr.chart.customindicators.StringLiteral;
import com.isi.csvr.chart.customindicators.statements.CIProgram;

    public class CIStringLiteral extends CIExpression {

        private StringLiteral literal;
        public StringLiteral getLiteral() {
            return literal;
        }

        public CIStringLiteral(StringLiteral literal) {
            this.literal = literal;
        }

        public static CIStringLiteral createLiteral(String text){
            if ("false".equals(text)) {
                return new CIStringLiteral(StringLiteral.FALSE);
            } else if ("true".equals(text)) {
                return new CIStringLiteral(StringLiteral.TRUE);
            } else if ("Open".equals(text)) {
                return new CIStringLiteral(StringLiteral.OPEN);
            } else if ("High".equals(text)) {
                return new CIStringLiteral(StringLiteral.HIGH);
            } else if ("Low".equals(text)) {
                return new CIStringLiteral(StringLiteral.LOW);
            } else if ("Close".equals(text)) {
                return new CIStringLiteral(StringLiteral.CLOSE);
            } else if ("Volume".equals(text)) {
                return new CIStringLiteral(StringLiteral.VOLUME);
            } else if ("Exponential".equals(text)) {
                return new CIStringLiteral(StringLiteral.EXPONENTIAL);
            } else if ("Simple".equals(text)) {
                return new CIStringLiteral(StringLiteral.SIMPLE);
            } else if ("TimeSeries".equals(text)) {
                return new CIStringLiteral(StringLiteral.TIMESERIES);
            } else if ("Triangular".equals(text)) {
                return new CIStringLiteral(StringLiteral.TRIANGULAR);
            } else if ("Variable".equals(text)) {
                return new CIStringLiteral(StringLiteral.VARIABLE);
            } else if ("VolumeAdjusted".equals(text)) {
                return new CIStringLiteral(StringLiteral.VOLUMEADJUSTED);
            } else if ("Weighted".equals(text)) {
                return new CIStringLiteral(StringLiteral.WEIGHTED);
            } else if ("Percent".equals(text)) {
                return new CIStringLiteral(StringLiteral.PERCENT);
            } else if ("Price".equals(text)) {
                return new CIStringLiteral(StringLiteral.PRICE);
            } else if ("Monthly".equals(text)) {
                return new CIStringLiteral(StringLiteral.MONTHLY);
            } else if ("Daily".equals(text)) {
                return new CIStringLiteral(StringLiteral.DAILY);
            }
            return new CIStringLiteral(StringLiteral.NULL);
        }

        public String getParamValue(CIProgram program) {
            switch (this.literal) {
                case FALSE:
                    return "false";
                case TRUE:
                    return "true";
                case OPEN:
                    return "OhlcPriority.OPEN";
                case HIGH:
                    return "OhlcPriority.HIGH";
                case LOW:
                    return "OhlcPriority.LOW";
                case CLOSE:
                    return "OhlcPriority.CLOSE";
                case VOLUME:
                    return "OhlcPriority.VOLUME";
                case PERCENT:
                    return "CalcMethod.PERCENT";
                case PRICE:
                    return "CalcMethod.POINTS";
                case EXPONENTIAL:
                    return "MAMethod.EXPONENTIAL";
                case SIMPLE:
                    return "MAMethod.SIMPLE";
                case TIMESERIES:
                    return "MAMethod.TIME_SERIES";
                case TRIANGULAR:
                    return "MAMethod.TRIANGULAR";
                case VARIABLE:
                    return "MAMethod.VARIABLE";
                case VOLUMEADJUSTED:
                    return "MAMethod.VOLUME_ADJUSTED";
                case MONTHLY:
                    return "CopCurvePeriod.MONTHLY";
                case DAILY:
                    return "CopCurvePeriod.DAILY";
                default: //WEIGHTED
                    return "MAMethod.WEIGHTED";
            }
        }

        public String generateCode(CIProgram program) {
            switch (this.literal) {
                case FALSE:
                    return "false";
                case TRUE:
                    return "true";
                case OPEN:
                    return "cR.Open";
                case HIGH:
                    return "cR.High";
                case LOW:
                    return "cR.Low";
                case CLOSE:
                    return "cR.Close";
                case VOLUME:
                    return "cR.Volume";
                case PERCENT:
                    return "CalcMethod.PERCENT";
                case PRICE:
                    return "CalcMethod.POINTS";
                case EXPONENTIAL:
                    return "MAMethod.EXPONENTIAL";
                case SIMPLE:
                    return "MAMethod.SIMPLE";
                case TIMESERIES:
                    return "MAMethod.TIME_SERIES";
                case TRIANGULAR:
                    return "MAMethod.TRIANGULAR";
                case VARIABLE:
                    return "MAMethod.VARIABLE";
                case VOLUMEADJUSTED:
                    return "MAMethod.VOLUME_ADJUSTED";
                case MONTHLY:
                    return "CopCurvePeriod.MONTHLY";
                case DAILY:
                    return "CopCurvePeriod.DAILY";
                default: //WEIGHTED
                    return "MAMethod.WEIGHTED";
            }
        } 

    }

