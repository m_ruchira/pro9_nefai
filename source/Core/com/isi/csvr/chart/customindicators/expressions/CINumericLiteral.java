//using System;
//using System.Collections.Generic;
//using System.Text;
//using CustomIndicators.Statements;

package com.isi.csvr.chart.customindicators.expressions;

import com.isi.csvr.chart.customindicators.statements.CIProgram;

    public class CINumericLiteral extends CIExpression {

        private String literal;
        public String getLiteral() {
            return literal;
        }

        public CINumericLiteral(String literal) {
            this.literal = literal;
        }

        public String getParamValue(CIProgram program) {
            return literal;
        }

        public String generateCode(CIProgram program) {
            return literal;
        }
    }

