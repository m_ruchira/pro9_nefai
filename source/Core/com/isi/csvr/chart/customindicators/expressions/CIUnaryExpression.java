//using System;
//using System.Collections.Generic;
//using System.Text;
//using CustomIndicators.Statements;

package com.isi.csvr.chart.customindicators.expressions;

import com.isi.csvr.chart.customindicators.statements.CIProgram;

    public class CIUnaryExpression extends CIExpression {

        private CIExpression expression;
        public CIExpression getExpression() {
            return expression;
        }

        private String sign;
        public String getSign() {
            return sign;
        }

        public CIUnaryExpression(String sign, CIExpression expression) {
            this.expression = expression;
            this.sign = sign;
        }

        public String getParamValue(CIProgram program) {
            return ("-".equals(sign) ? sign : "") + expression.getParamValue(program);
        }

        public String generateCode(CIProgram program) {
            return ("-".equals(sign) ? sign : "") + expression.generateCode(program);
        } 

    }

