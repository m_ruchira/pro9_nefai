//using System;
//using System.Collections.Generic;
//using System.Text;
//using System.Collections;
//using CustomIndicators.Statements;

package com.isi.csvr.chart.customindicators.expressions;

import com.isi.csvr.chart.customindicators.statements.CIProgram;
import com.isi.csvr.chart.customindicators.FunctionType;
import com.isi.csvr.chart.CustomIndicatorInterface;

public class CIFunction extends CIExpression {

    private FunctionType functionType;
    public FunctionType getFunctionType() {
        return functionType;
    }

    private CIExpression exprsn1;
    public CIExpression getParamOne() {
        return exprsn1;
    }

    private CIExpression exprsn2;
    public CIExpression getParamTwo() {
        return exprsn2;
    }

    private CIExpression exprsn3;
    public CIExpression getParamThree() {
        return exprsn3;
    }

    public CIFunction(FunctionType functionType, CIExpression exprsn1) {
        this.functionType = functionType;
        this.exprsn1 = exprsn1;
        this.exprsn2 = null;
    }

    public CIFunction(FunctionType functionType, CIExpression exprsn1, CIExpression exprsn2) {
        this.functionType = functionType;
        this.exprsn1 = exprsn1;
        this.exprsn2 = exprsn2;
    }


    public CIFunction(FunctionType functionType, CIExpression exprsn1, CIExpression exprsn2, CIExpression exprsn3) {
        this.functionType = functionType;
        this.exprsn1 = exprsn1;
        this.exprsn2 = exprsn2;
        this.exprsn3 = exprsn3;
    }

    public String getParamValue(CIProgram program) {
        return generateCode(program);
    }

    public static String getName(FunctionType functionType) {
        switch (functionType) {
            case ABS:
                return "abs";
            case ROUND:
                return "round";
            case SQR:
                return "sqr";
            case SQRT:
                return "sqrt";
            case LOG:
                return "log";
            case LN:
                return "ln";
            case EXP:
                return "exp";
            case CUM:
                return "cum";
            case MIN:
                return "min";
            case MAX:
                return "max";
            case POW:
                return "pow";
            case REF:
                return "ref";
            case ACOS:
                return "acos";
            case ASIN:
                return "asin";
            case ATAN:
                return "atan";
            case ATAN2:
                return "atan2";
            case CEILING:
                return "ceiling";
            case COS:
                return "cos";
            case COSH:
                return "cosh";
            case FLOOR:
                return "floor";
            case FRAC:
                return "frac";
            case HHV:
                return "hhv";
            case HHVBARS:
                return "hhvbars";
            case HIGHEST:
                return "highest";
            case HIGHESTBARS:
                return "highestbars";
            case HIGHESTSINCE:
                return "highestsince";
            case HIGHESTSINCEBARS:
                return "highestsincebars";
            case LLV:
                return "llv";
            case LLVBARS:
                return "llvbars";
            case LOWEST:
                return "lowest";
            case LOWESTBARS:
                return "lowestbars";
            case LOWESTSINCE:
                return "lowestsince";
            case LOWESTSINCEBARS:
                return "lowestsincebars";
            case PREC:
                return "prec";
            case SIGN:
                return "sign";
            case SIN:
                return "sin";
            case SINH:
                return "sinh";
            case SUM:
                return "sum";
            case TAN:
                return "tan";
            case TANH:
                return "tanh";
            case TRUNC:
                return "trunc";
            default:
                return "ref";        }
    }

    public String generateCode(CIProgram program) {
        switch (functionType) {                                       
            case ABS:
                return "Math.abs(" + exprsn1.generateCode(program) + ")";
            case ROUND:
                return "(int)Math.round(" + exprsn1.generateCode(program) + ")"; // Changed from Double to Int
            case SQR:
                return "(double)Math.pow(" + exprsn1.generateCode(program) + ", 2)";
            case SQRT:
                return "(double)Math.sqrt(" + exprsn1.generateCode(program) + ")";
            case LOG:
                return "(double)Math.log10(" + exprsn1.generateCode(program) + ")";
            case LN:
                return "(double)Math.log(" + exprsn1.generateCode(program) + ")";
            case EXP:
                return "(double)Math.exp(" + exprsn1.generateCode(program) + ")";
            case CUM:
                program.stepCount++; //sum step
                String stepName = "STEP_" + program.stepCount;
                String stepString = program.getCumStepString(stepName, exprsn1);
                program.sbCalc.append(stepString);
                return "cR.getStepValue(" + stepName + ")";
            case MIN:
                return "(double)Math.min(" + exprsn1.generateCode(program) + ", " +
                        exprsn2.generateCode(program) + ")";
            case MAX:
                return "(double)Math.max(" + exprsn1.generateCode(program) + ", " +
                        exprsn2.generateCode(program) + ")";
            case POW:
                return "(double)Math.pow(" + exprsn1.generateCode(program) + ", " +
                        exprsn2.generateCode(program) + ")";
            case REF:
                String refStepString = program.getRefStepString(exprsn1);
                String jump = exprsn2.generateCode(program);
                return "((" + CustomIndicatorInterface.getChartRecordClassName() + ")al.get(i+(int)(" + jump + "))).getStepValue(" + refStepString + ")";
            case ACOS:
                return "(double)Math.acos(" + exprsn1.generateCode(program) + ")";
            case ASIN:
                return "(double)Math.asin(" + exprsn1.generateCode(program) + ")";
            case ATAN:
                return "(double)Math.atan(" + exprsn1.generateCode(program) + ")";
            case ATAN2:
                return "(double)Math.atan2(" + exprsn1.generateCode(program) + ", " + exprsn2.generateCode(program) + ")";
            case CEILING:
                return "(double)Math.ceil(" + exprsn1.generateCode(program) + ")";
            case COS:
                return "(double)Math.cos(" + exprsn1.generateCode(program) + ")";
            case COSH:
                return "(double)Math.cosh(" + exprsn1.generateCode(program) + ")";
            case FLOOR:
                return "(double)Math.floor(" + exprsn1.generateCode(program) + ")";
            case FRAC:
                return "frac(" + exprsn1.generateCode(program) + ")";
            case TRUNC:
                return "truncate(" + exprsn1.generateCode(program) + ")";
            case HHV:
            case HHVBARS:
            case HIGHEST:
            case HIGHESTBARS:
            case HIGHESTSINCE:
            case HIGHESTSINCEBARS:
            case LLV:
            case LLVBARS:
            case LOWEST:
            case LOWESTBARS:
            case LOWESTSINCE:
            case LOWESTSINCEBARS:
                return getStepString(program, functionType);
            case PREC:
                return "precision(" + exprsn1.generateCode(program) + ", " +
                        exprsn2.generateCode(program) + ")";
            case SIGN:
                return "Math.signum(" + exprsn1.generateCode(program) + ")";
            case SIN:
                return "(double)Math.sin(" + exprsn1.generateCode(program) + ")";
            case SINH:
                return "(double)Math.sinh(" + exprsn1.generateCode(program) + ")";
            case SUM:
                return getStepString(program, functionType);
            case TAN:
                return "(double)Math.tan(" + exprsn1.generateCode(program) + ")";
            case TANH:
                return "(double)Math.tanh(" + exprsn1.generateCode(program) + ")";
            default:
                return "";
        }
    }

    private String getStepString(CIProgram program, FunctionType functionType) {
        String stepName, stepString = "";
        program.stepCount++;
        stepName = "STEP_" + program.stepCount;
        switch (functionType) {
            case CUM:
                stepString = program.getCumStepString(stepName, exprsn1);
                break;
            case SUM:
                stepString = program.getSumStepString(stepName, exprsn1, exprsn2);
                break;
            case HHV:
                stepString = program.getHHVStepString(stepName, exprsn1, exprsn2);
                break;
            case HHVBARS:
                stepString = program.getHHVBarsStepString(stepName, exprsn1, exprsn2);
                break;
            case HIGHEST:
                stepString = program.getHighestStepString(stepName, exprsn1);
                break;
            case HIGHESTBARS:
                stepString = program.getHighestBarsStepString(stepName, exprsn1);
                break;
            case HIGHESTSINCE:
                stepString = program.getHSinceStepString(stepName, exprsn1, exprsn2, exprsn3);
                break;
            case HIGHESTSINCEBARS:
                stepString = program.getHSinceBarsStepString(stepName, exprsn1, exprsn2, exprsn3);
                break;
            case LLV:
                stepString = program.getLLVStepString(stepName, exprsn1, exprsn2);
                break;
            case LLVBARS:
                stepString = program.getLLVBarsStepString(stepName, exprsn1, exprsn2);
                break;
            case LOWEST:
                stepString = program.getLowestStepString(stepName, exprsn1);
                break;
            case LOWESTBARS:
                stepString = program.getLowestBarsStepString(stepName, exprsn1);
                break;
            case LOWESTSINCE:
                stepString = program.getLSinceStepString(stepName, exprsn1, exprsn2, exprsn3);
                break;
            case LOWESTSINCEBARS:
                stepString = program.getLSinceBarsStepString(stepName, exprsn1, exprsn2, exprsn3);
                break;
        }
        program.sbCalc.append(stepString);
        return "cR.getStepValue(" + stepName + ")";
    }

}


