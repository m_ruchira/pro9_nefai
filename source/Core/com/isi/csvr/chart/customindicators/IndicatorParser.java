package com.isi.csvr.chart.customindicators;


import com.isi.csvr.chart.customindicators.statements.*;
import com.isi.csvr.chart.customindicators.expressions.*;
import com.isi.csvr.chart.customindicators.generalparams.*;
import com.isi.csvr.parser.*;

import javax.swing.*;
import javax.swing.text.Document;
import javax.swing.text.Element;
import javax.swing.text.AttributeSet;
import javax.swing.text.BadLocationException;
import java.util.Hashtable;
import java.util.ArrayList;


public class IndicatorParser implements GPMessageConstants {

/*
    private interface SymbolConstants {
        final int SYMBOL_EOF = 0; // (EOF)
        final int SYMBOL_ERROR = 1; // (Error)
        final int SYMBOL_WHITESPACE = 2; // (Whitespace)
        final int SYMBOL_COMMENTEND = 3; // (Comment End)
        final int SYMBOL_COMMENTLINE = 4; // (Comment Line)
        final int SYMBOL_COMMENTSTART = 5; // (Comment Start)
   final int SYMBOL_MINUS           =  6;  // '-'
   final int SYMBOL_LPARAN          =  7;  // '('
   final int SYMBOL_RPARAN          =  8;  // ')'
   final int SYMBOL_TIMES           =  9;  // '*'
   final int SYMBOL_COMMA           = 10;  // ','
   final int SYMBOL_DIV             = 11;  // '/'
   final int SYMBOL_SEMI            = 12;  // ';'
   final int SYMBOL_PLUS            = 13;  // '+'
   final int SYMBOL_LT              = 14;  // '<'
   final int SYMBOL_LTEQ            = 15;  // '<='
   final int SYMBOL_LTGT            = 16;  // '<>'
   final int SYMBOL_EQ              = 17;  // '='
   final int SYMBOL_EQEQ            = 18;  // '=='
   final int SYMBOL_GT              = 19;  // '>'
   final int SYMBOL_GTEQ            = 20;  // '>='
        final int SYMBOL_AND = 21; // and
        final int SYMBOL_BOOLEANLITERAL = 22; // BooleanLiteral
   final int SYMBOL_BUY             = 23;  // buy
   final int SYMBOL_BUYTOCOVER      = 24;  // buytocover
   final int SYMBOL_CALCMETDLITERAL = 25;  // CalcMetdLiteral
        final int SYMBOL_CCPERIODLITERAL = 78;  // Coppock Curve Period
   final int SYMBOL_COLORLITERAL    = 26;  // ColorLiteral
   final int SYMBOL_DO              = 27;  // do
   final int SYMBOL_DRAWSYMBOL      = 28;  // drawsymbol
   final int SYMBOL_ELSE            = 29;  // else
   final int SYMBOL_END             = 30;  // end
   final int SYMBOL_FOR             = 31;  // for
   final int SYMBOL_FUNCTION1PARAM  = 32;  // 'Function1Param'
   final int SYMBOL_FUNCTION2PARAM  = 33;  // 'Function2Param'
   final int SYMBOL_FUNCTION3PARAM  = 34;  // 'Function3Param'
   final int SYMBOL_IDENTIFIER      = 35;  // Identifier
   final int SYMBOL_IF              = 36;  // if
   final int SYMBOL_INTEGERLITERAL  = 37;  // IntegerLiteral
   final int SYMBOL_MAMETHODLITERAL = 38;  // MAMethodLiteral
   final int SYMBOL_MARKPATTERN     = 39;  // markpattern
   final int SYMBOL_NUMERICLITERAL  = 40;  // NumericLiteral
   final int SYMBOL_OHLCVLITERAL    = 41;  // OHLCVLiteral
   final int SYMBOL_OR              = 42;  // or
   final int SYMBOL_PARAM           = 43;  // param
   final int SYMBOL_PLOT            = 44;  // plot
   final int SYMBOL_POSITIONLITERAL = 45;  // PositionLiteral
   final int SYMBOL_RGB             = 46;  // RGB
   final int SYMBOL_SELL            = 47;  // sell
   final int SYMBOL_SELLSHORT       = 48;  // sellshort
   final int SYMBOL_STYLELITERAL    = 49;  // StyleLiteral
   final int SYMBOL_SYMBOLLITERAL   = 50;  // SymbolLiteral
   final int SYMBOL_THEN            = 51;  // then
   final int SYMBOL_TYPEDECLARATION = 52;  // TypeDeclaration
   final int SYMBOL_WHILE           = 53;  // while
   final int SYMBOL_ADDEXP          = 54;  // <Add Exp>
   final int SYMBOL_COLOR           = 55;  // <Color>
   final int SYMBOL_COMPAREEXP      = 56;  // <Compare Exp>
   final int SYMBOL_DECLAREVAR      = 57;  // <DeclareVar>
   final int SYMBOL_DECLASSIGN      = 58;  // <DeclAssign>
   final int SYMBOL_EXPRESSION      = 59;  // <Expression>
   final int SYMBOL_FUNCTION        = 60;  // <Function>
   final int SYMBOL_INDICATOR       = 61;  // <Indicator>
   final int SYMBOL_LOOPASSIGN      = 62;  // <LoopAssign>
   final int SYMBOL_LOOPDECASSN     = 63;  // <LoopDecAssn>
   final int SYMBOL_MULTEXP         = 64;  // <Mult Exp>
   final int SYMBOL_NEGATEEXP       = 65;  // <Negate Exp>
   final int SYMBOL_PARAMLIST       = 66;  // <ParamList>
   final int SYMBOL_PARAMS          = 67;  // <Params>
   final int SYMBOL_PLOTSTMT        = 68;  // <Plot Stmt>
   final int SYMBOL_POSITION        = 69;  // <Position>
   final int SYMBOL_PRGMSTMT        = 70;  // <Prgm Stmt>
   final int SYMBOL_PRGMSTMTS       = 71;  // <Prgm Stmts>
   final int SYMBOL_PROGRAM         = 72;  // <Program>
   final int SYMBOL_STATEMENT       = 73;  // <Statement>
   final int SYMBOL_STATEMENTS      = 74;  // <Statements>
   final int SYMBOL_STYLE           = 75;  // <Style>
   final int SYMBOL_SYMBOL          = 76;  // <Symbol>
   final int SYMBOL_VALUE           = 77;  // <Value>

    }

    private interface RuleConstants {
        final int RULE_PROGRAM = 0; // <Program> ::= <Prgm Stmts>
        final int RULE_PRGMSTMTS = 1; // <Prgm Stmts> ::= <Prgm Stmt> <Prgm Stmts>
        final int RULE_PRGMSTMTS2 = 2; // <Prgm Stmts> ::= <Prgm Stmt>
        final int RULE_PRGMSTMT = 3; // <Prgm Stmt> ::= <Statement>
        final int RULE_PRGMSTMT2 = 4; // <Prgm Stmt> ::= <Plot Stmt>
        final int RULE_STATEMENTS = 5; // <Statements> ::= <Statement> <Statements>
        final int RULE_STATEMENTS2 = 6; // <Statements> ::= <Statement>
   final int RULE_STATEMENT_SEMI                                                                   =  7;  // <Statement> ::= <DeclareVar> ';'
   final int RULE_STATEMENT_SEMI2                                                                  =  8;  // <Statement> ::= <DeclAssign> ';'
   final int RULE_STATEMENT_IDENTIFIER_EQ_SEMI                                                     =  9;  // <Statement> ::= Identifier '=' <Expression> ';'
   final int RULE_STATEMENT_FOR_LPARAN_SEMI_SEMI_RPARAN_DO_END                                     = 10;  // <Statement> ::= for '(' <LoopDecAssn> ';' <Expression> ';' <LoopAssign> ')' do <Statements> end
   final int RULE_STATEMENT_WHILE_LPARAN_RPARAN_DO_END                                             = 11;  // <Statement> ::= while '(' <Expression> ')' do <Statements> end
   final int RULE_STATEMENT_IF_LPARAN_RPARAN_THEN_END                                              = 12;  // <Statement> ::= if '(' <Expression> ')' then <Statements> end
   final int RULE_STATEMENT_IF_LPARAN_RPARAN_THEN_ELSE_END                                         = 13;  // <Statement> ::= if '(' <Expression> ')' then <Statements> else <Statements> end
   final int RULE_STATEMENT_SEMI3                                                                  = 14;  // <Statement> ::= ';'
   final int RULE_PLOTSTMT_PLOT_SEMI                                                               = 15;  // <Plot Stmt> ::= plot <Add Exp> ';'
   final int RULE_PLOTSTMT_PLOT_LPARAN_COMMA_RPARAN_SEMI                                           = 16;  // <Plot Stmt> ::= plot '(' <Add Exp> ',' <Color> ')' ';'
   final int RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_RPARAN_SEMI                                     = 17;  // <Plot Stmt> ::= plot '(' <Add Exp> ',' <Color> ',' <Style> ')' ';'
   final int RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_INTEGERLITERAL_RPARAN_SEMI                = 18;  // <Plot Stmt> ::= plot '(' <Add Exp> ',' <Color> ',' <Style> ',' IntegerLiteral ')' ';'
   final int RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_NUMERICLITERAL_RPARAN_SEMI                = 19;  // <Plot Stmt> ::= plot '(' <Add Exp> ',' <Color> ',' <Style> ',' NumericLiteral ')' ';'
   final int RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_COMMA_INTEGERLITERAL_RPARAN_SEMI          = 20;  // <Plot Stmt> ::= plot '(' <Add Exp> ',' <Color> ',' <Color> ',' <Style> ',' IntegerLiteral ')' ';'
   final int RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_COMMA_NUMERICLITERAL_RPARAN_SEMI          = 21;  // <Plot Stmt> ::= plot '(' <Add Exp> ',' <Color> ',' <Color> ',' <Style> ',' NumericLiteral ')' ';'
   final int RULE_PLOTSTMT_DRAWSYMBOL_LPARAN_COMMA_RPARAN_SEMI                                     = 22;  // <Plot Stmt> ::= drawsymbol '(' <Expression> ',' <Symbol> ')' ';'
   final int RULE_PLOTSTMT_DRAWSYMBOL_LPARAN_COMMA_COMMA_RPARAN_SEMI                               = 23;  // <Plot Stmt> ::= drawsymbol '(' <Expression> ',' <Symbol> ',' <Color> ')' ';'
   final int RULE_PLOTSTMT_DRAWSYMBOL_LPARAN_COMMA_COMMA_COMMA_RPARAN_SEMI                         = 24;  // <Plot Stmt> ::= drawsymbol '(' <Expression> ',' <Symbol> ',' <Color> ',' <Position> ')' ';'
   final int RULE_PLOTSTMT_MARKPATTERN_LPARAN_RPARAN_SEMI                                          = 25;  // <Plot Stmt> ::= markpattern '(' <Expression> ')' ';'
   final int RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_RPARAN_SEMI                     = 26;  // <Plot Stmt> ::= markpattern '(' <Expression> ',' IntegerLiteral ')' ';'
   final int RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_COMMA_RPARAN_SEMI               = 27;  // <Plot Stmt> ::= markpattern '(' <Expression> ',' IntegerLiteral ',' <Color> ')' ';'
   final int RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_COMMA_COMMA_RPARAN_SEMI         = 28;  // <Plot Stmt> ::= markpattern '(' <Expression> ',' IntegerLiteral ',' <Color> ',' <Color> ')' ';'
   final int RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_COMMA_COMMA_COMMA_RPARAN_SEMI   = 29;  // <Plot Stmt> ::= markpattern '(' <Expression> ',' IntegerLiteral ',' <Color> ',' <Color> ',' <Position> ')' ';'
   final int RULE_PLOTSTMT_BUY_LPARAN_RPARAN_SEMI                                                  = 30;  // <Plot Stmt> ::= buy '(' <Expression> ')' ';'
   final int RULE_PLOTSTMT_SELL_LPARAN_RPARAN_SEMI                                                 = 31;  // <Plot Stmt> ::= sell '(' <Expression> ')' ';'
   final int RULE_PLOTSTMT_SELLSHORT_LPARAN_RPARAN_SEMI                                            = 32;  // <Plot Stmt> ::= sellshort '(' <Expression> ')' ';'
   final int RULE_PLOTSTMT_BUYTOCOVER_LPARAN_RPARAN_SEMI                                           = 33;  // <Plot Stmt> ::= buytocover '(' <Expression> ')' ';'
   final int RULE_DECLAREVAR_PARAM_TYPEDECLARATION_IDENTIFIER                                      = 34;  // <DeclareVar> ::= param TypeDeclaration Identifier
   final int RULE_DECLAREVAR_TYPEDECLARATION_IDENTIFIER                                            = 35;  // <DeclareVar> ::= TypeDeclaration Identifier
   final int RULE_DECLAREVAR_COMMA_IDENTIFIER                                                      = 36;  // <DeclareVar> ::= <DeclareVar> ',' Identifier
   final int RULE_LOOPDECASSN                                                                      = 37;  // <LoopDecAssn> ::= <DeclAssign>
   final int RULE_LOOPDECASSN2                                                                     = 38;  // <LoopDecAssn> ::= <LoopAssign>
   final int RULE_LOOPASSIGN_IDENTIFIER_EQ                                                         = 39;  // <LoopAssign> ::= Identifier '=' <Add Exp>
   final int RULE_LOOPASSIGN                                                                       = 40;  // <LoopAssign> ::=
   final int RULE_DECLASSIGN_PARAM_TYPEDECLARATION_IDENTIFIER_EQ                                   = 41;  // <DeclAssign> ::= param TypeDeclaration Identifier '=' <Expression>
   final int RULE_DECLASSIGN_TYPEDECLARATION_IDENTIFIER_EQ                                         = 42;  // <DeclAssign> ::= TypeDeclaration Identifier '=' <Expression>
   final int RULE_EXPRESSION_AND                                                                   = 43;  // <Expression> ::= <Expression> and <Compare Exp>
   final int RULE_EXPRESSION_OR                                                                    = 44;  // <Expression> ::= <Expression> or <Compare Exp>
   final int RULE_EXPRESSION_BOOLEANLITERAL                                                        = 45;  // <Expression> ::= BooleanLiteral
   final int RULE_EXPRESSION                                                                       = 46;  // <Expression> ::= <Compare Exp>
   final int RULE_COMPAREEXP_GT                                                                    = 47;  // <Compare Exp> ::= <Add Exp> '>' <Add Exp>
   final int RULE_COMPAREEXP_LT                                                                    = 48;  // <Compare Exp> ::= <Add Exp> '<' <Add Exp>
   final int RULE_COMPAREEXP_LTEQ                                                                  = 49;  // <Compare Exp> ::= <Add Exp> '<=' <Add Exp>
   final int RULE_COMPAREEXP_GTEQ                                                                  = 50;  // <Compare Exp> ::= <Add Exp> '>=' <Add Exp>
   final int RULE_COMPAREEXP_EQEQ                                                                  = 51;  // <Compare Exp> ::= <Add Exp> '==' <Add Exp>
   final int RULE_COMPAREEXP_LTGT                                                                  = 52;  // <Compare Exp> ::= <Add Exp> '<>' <Add Exp>
   final int RULE_COMPAREEXP                                                                       = 53;  // <Compare Exp> ::= <Add Exp>
   final int RULE_ADDEXP_PLUS                                                                      = 54;  // <Add Exp> ::= <Add Exp> '+' <Mult Exp>
   final int RULE_ADDEXP_MINUS                                                                     = 55;  // <Add Exp> ::= <Add Exp> '-' <Mult Exp>
   final int RULE_ADDEXP                                                                           = 56;  // <Add Exp> ::= <Mult Exp>
   final int RULE_MULTEXP_TIMES                                                                    = 57;  // <Mult Exp> ::= <Mult Exp> '*' <Negate Exp>
   final int RULE_MULTEXP_DIV                                                                      = 58;  // <Mult Exp> ::= <Mult Exp> '/' <Negate Exp>
   final int RULE_MULTEXP                                                                          = 59;  // <Mult Exp> ::= <Negate Exp>
   final int RULE_NEGATEEXP_MINUS                                                                  = 60;  // <Negate Exp> ::= '-' <Value>
   final int RULE_NEGATEEXP                                                                        = 61;  // <Negate Exp> ::= <Value>
   final int RULE_VALUE_IDENTIFIER                                                                 = 62;  // <Value> ::= Identifier
   final int RULE_VALUE_INTEGERLITERAL                                                             = 63;  // <Value> ::= IntegerLiteral
   final int RULE_VALUE_NUMERICLITERAL                                                             = 64;  // <Value> ::= NumericLiteral
   final int RULE_VALUE_OHLCVLITERAL                                                               = 65;  // <Value> ::= OHLCVLiteral
   final int RULE_VALUE                                                                            = 66;  // <Value> ::= <Indicator>
   final int RULE_VALUE2                                                                           = 67;  // <Value> ::= <Function>
   final int RULE_VALUE_LPARAN_RPARAN                                                              = 68;  // <Value> ::= '(' <Expression> ')'
   final int RULE_VALUE_MAMETHODLITERAL                                                            = 69;  // <Value> ::= MAMethodLiteral
   final int RULE_VALUE_CALCMETDLITERAL                                                            = 70;  // <Value> ::= CalcMetdLiteral
   final int RULE_INDICATOR_IDENTIFIER_LPARAN_RPARAN                                               = 71;  // <Indicator> ::= Identifier '(' <Params> ')'
   final int RULE_PARAMS                                                                           = 72;  // <Params> ::= <ParamList>
   final int RULE_PARAMS2                                                                          = 73;  // <Params> ::=
   final int RULE_PARAMLIST_COMMA                                                                  = 74;  // <ParamList> ::= <ParamList> ',' <Expression>
   final int RULE_PARAMLIST                                                                        = 75;  // <ParamList> ::= <Expression>
   final int RULE_FUNCTION_FUNCTION1PARAM_LPARAN_RPARAN                                            = 76;  // <Function> ::= 'Function1Param' '(' <Expression> ')'
   final int RULE_FUNCTION_FUNCTION2PARAM_LPARAN_COMMA_RPARAN                                      = 77;  // <Function> ::= 'Function2Param' '(' <Expression> ',' <Expression> ')'
   final int RULE_FUNCTION_FUNCTION3PARAM_LPARAN_COMMA_COMMA_RPARAN                                = 78;  // <Function> ::= 'Function3Param' '(' <Expression> ',' <Expression> ',' <Expression> ')'
   final int RULE_COLOR_COLORLITERAL                                                               = 79;  // <Color> ::= ColorLiteral
   final int RULE_COLOR_RGB_LPARAN_INTEGERLITERAL_COMMA_INTEGERLITERAL_COMMA_INTEGERLITERAL_RPARAN = 80;  // <Color> ::= RGB '(' IntegerLiteral ',' IntegerLiteral ',' IntegerLiteral ')'
   final int RULE_STYLE_STYLELITERAL                                                               = 81;  // <Style> ::= StyleLiteral
   final int RULE_STYLE_INTEGERLITERAL                                                             = 82;  // <Style> ::= IntegerLiteral
   final int RULE_SYMBOL_SYMBOLLITERAL                                                             = 83;  // <Symbol> ::= SymbolLiteral
   final int RULE_SYMBOL_INTEGERLITERAL                                                            = 84;  // <Symbol> ::= IntegerLiteral
   final int RULE_POSITION_POSITIONLITERAL                                                         = 85;  // <Position> ::= PositionLiteral
   final int RULE_POSITION_INTEGERLITERAL                                                          = 86;  // <Position> ::= IntegerLiteral
        final int RULE_VALUE_CCPERIODLITERAL = 87;  // <Value> ::= CopCurvePeriodLiteral
    }
*/

    private interface SymbolConstants {
        final int SYMBOL_EOF = 0; // (EOF)
        final int SYMBOL_ERROR = 1; // (Error)
        final int SYMBOL_WHITESPACE = 2; // (Whitespace)
        final int SYMBOL_COMMENTEND = 3; // (Comment End)
        final int SYMBOL_COMMENTLINE = 4; // (Comment Line)
        final int SYMBOL_COMMENTSTART = 5; // (Comment Start)
        final int SYMBOL_MINUS = 6;  // '-'
        final int SYMBOL_LPARAN = 7;  // '('
        final int SYMBOL_RPARAN = 8;  // ')'
        final int SYMBOL_TIMES = 9;  // '*'
        final int SYMBOL_COMMA = 10;  // ','
        final int SYMBOL_DIV = 11;  // '/'
        final int SYMBOL_SEMI = 12;  // ';'
        final int SYMBOL_PLUS = 13;  // '+'
        final int SYMBOL_LT = 14;  // '<'
        final int SYMBOL_LTEQ = 15;  // '<='
        final int SYMBOL_LTGT = 16;  // '<>'
        final int SYMBOL_EQ = 17;  // '='
        final int SYMBOL_EQEQ = 18;  // '=='
        final int SYMBOL_GT = 19;  // '>'
        final int SYMBOL_GTEQ = 20;  // '>='
        final int SYMBOL_AND = 21; // and
        final int SYMBOL_BOOLEANLITERAL = 22; // BooleanLiteral
        final int SYMBOL_BUY = 23;  // buy
        final int SYMBOL_BUYTOCOVER = 24;  // buytocover
        final int SYMBOL_CALCMETDLITERAL = 25;  // CalcMetdLiteral
        final int SYMBOL_CCPERIODLITERAL = 26;  // Coppock Curve Period
        final int SYMBOL_COLORLITERAL = 27;  // ColorLiteral
        final int SYMBOL_DO = 28;  // do
        final int SYMBOL_DRAWSYMBOL = 29;  // drawsymbol
        final int SYMBOL_ELSE = 30;  // else
        final int SYMBOL_END = 31;  // end
        final int SYMBOL_FOR = 32;  // for
        final int SYMBOL_FUNCTION1PARAM = 33;  // 'Function1Param'
        final int SYMBOL_FUNCTION2PARAM = 34;  // 'Function2Param'
        final int SYMBOL_FUNCTION3PARAM = 35;  // 'Function3Param'
        final int SYMBOL_IDENTIFIER = 36;  // Identifier
        final int SYMBOL_IF = 37;  // if
        final int SYMBOL_INTEGERLITERAL = 38;  // IntegerLiteral
        final int SYMBOL_MAMETHODLITERAL = 39;  // MAMethodLiteral
        final int SYMBOL_MARKPATTERN = 40;  // markpattern
        final int SYMBOL_NUMERICLITERAL = 41;  // NumericLiteral
        final int SYMBOL_OHLCVLITERAL = 42;  // OHLCVLiteral
        final int SYMBOL_OR = 43;  // or
        final int SYMBOL_PARAM = 44;  // param
        final int SYMBOL_PLOT = 45;  // plot
        final int SYMBOL_POSITIONLITERAL = 46;  // PositionLiteral
        final int SYMBOL_RGB = 47;  // RGB
        final int SYMBOL_SELL = 48;  // sell
        final int SYMBOL_SELLSHORT = 49;  // sellshort
        final int SYMBOL_STYLELITERAL = 50;  // StyleLiteral
        final int SYMBOL_SYMBOLLITERAL = 51;  // SymbolLiteral
        final int SYMBOL_THEN = 52;  // then
        final int SYMBOL_TYPEDECLARATION = 53;  // TypeDeclaration
        final int SYMBOL_WHILE = 54;  // while
        final int SYMBOL_ADDEXP = 55;  // <Add Exp>
        final int SYMBOL_COLOR = 56;  // <Color>
        final int SYMBOL_COMPAREEXP = 57;  // <Compare Exp>
        final int SYMBOL_DECLAREVAR = 58;  // <DeclareVar>
        final int SYMBOL_DECLASSIGN = 59;  // <DeclAssign>
        final int SYMBOL_EXPRESSION = 60;  // <Expression>
        final int SYMBOL_FUNCTION = 61;  // <Function>
        final int SYMBOL_INDICATOR = 62;  // <Indicator>
        final int SYMBOL_LOOPASSIGN = 63;  // <LoopAssign>
        final int SYMBOL_LOOPDECASSN = 64;  // <LoopDecAssn>
        final int SYMBOL_MULTEXP = 65;  // <Mult Exp>
        final int SYMBOL_NEGATEEXP = 66;  // <Negate Exp>
        final int SYMBOL_PARAMLIST = 67;  // <ParamList>
        final int SYMBOL_PARAMS = 68;  // <Params>
        final int SYMBOL_PLOTSTMT = 69;  // <Plot Stmt>
        final int SYMBOL_POSITION = 70;  // <Position>
        final int SYMBOL_PRGMSTMT = 71;  // <Prgm Stmt>
        final int SYMBOL_PRGMSTMTS = 72;  // <Prgm Stmts>
        final int SYMBOL_PROGRAM = 73;  // <Program>
        final int SYMBOL_STATEMENT = 74;  // <Statement>
        final int SYMBOL_STATEMENTS = 75;  // <Statements>
        final int SYMBOL_STYLE = 77;  // <Style>
        final int SYMBOL_SYMBOL = 77;  // <Symbol>
        final int SYMBOL_VALUE = 78;  // <Value>

    }

    private interface RuleConstants {
        final int RULE_PROGRAM = 0; // <Program> ::= <Prgm Stmts>
        final int RULE_PRGMSTMTS = 1; // <Prgm Stmts> ::= <Prgm Stmt> <Prgm Stmts>
        final int RULE_PRGMSTMTS2 = 2; // <Prgm Stmts> ::= <Prgm Stmt>
        final int RULE_PRGMSTMT = 3; // <Prgm Stmt> ::= <Statement>
        final int RULE_PRGMSTMT2 = 4; // <Prgm Stmt> ::= <Plot Stmt>
        final int RULE_STATEMENTS = 5; // <Statements> ::= <Statement> <Statements>
        final int RULE_STATEMENTS2 = 6; // <Statements> ::= <Statement>
        final int RULE_STATEMENT_SEMI = 7;  // <Statement> ::= <DeclareVar> ';'
        final int RULE_STATEMENT_SEMI2 = 8;  // <Statement> ::= <DeclAssign> ';'
        final int RULE_STATEMENT_IDENTIFIER_EQ_SEMI = 9;  // <Statement> ::= Identifier '=' <Expression> ';'
        final int RULE_STATEMENT_FOR_LPARAN_SEMI_SEMI_RPARAN_DO_END = 10;  // <Statement> ::= for '(' <LoopDecAssn> ';' <Expression> ';' <LoopAssign> ')' do <Statements> end
        final int RULE_STATEMENT_WHILE_LPARAN_RPARAN_DO_END = 11;  // <Statement> ::= while '(' <Expression> ')' do <Statements> end
        final int RULE_STATEMENT_IF_LPARAN_RPARAN_THEN_END = 12;  // <Statement> ::= if '(' <Expression> ')' then <Statements> end
        final int RULE_STATEMENT_IF_LPARAN_RPARAN_THEN_ELSE_END = 13;  // <Statement> ::= if '(' <Expression> ')' then <Statements> else <Statements> end
        final int RULE_STATEMENT_SEMI3 = 14;  // <Statement> ::= ';'
        final int RULE_PLOTSTMT_PLOT_SEMI = 15;  // <Plot Stmt> ::= plot <Add Exp> ';'
        final int RULE_PLOTSTMT_PLOT_LPARAN_COMMA_RPARAN_SEMI = 16;  // <Plot Stmt> ::= plot '(' <Add Exp> ',' <Color> ')' ';'
        final int RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_RPARAN_SEMI = 17;  // <Plot Stmt> ::= plot '(' <Add Exp> ',' <Color> ',' <Style> ')' ';'
        final int RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_INTEGERLITERAL_RPARAN_SEMI = 18;  // <Plot Stmt> ::= plot '(' <Add Exp> ',' <Color> ',' <Style> ',' IntegerLiteral ')' ';'
        final int RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_NUMERICLITERAL_RPARAN_SEMI = 19;  // <Plot Stmt> ::= plot '(' <Add Exp> ',' <Color> ',' <Style> ',' NumericLiteral ')' ';'
        final int RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_COMMA_INTEGERLITERAL_RPARAN_SEMI = 20;  // <Plot Stmt> ::= plot '(' <Add Exp> ',' <Color> ',' <Color> ',' <Style> ',' IntegerLiteral ')' ';'
        final int RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_COMMA_NUMERICLITERAL_RPARAN_SEMI = 21;  // <Plot Stmt> ::= plot '(' <Add Exp> ',' <Color> ',' <Color> ',' <Style> ',' NumericLiteral ')' ';'
        final int RULE_PLOTSTMT_DRAWSYMBOL_LPARAN_COMMA_RPARAN_SEMI = 22;  // <Plot Stmt> ::= drawsymbol '(' <Expression> ',' <Symbol> ')' ';'
        final int RULE_PLOTSTMT_DRAWSYMBOL_LPARAN_COMMA_COMMA_RPARAN_SEMI = 23;  // <Plot Stmt> ::= drawsymbol '(' <Expression> ',' <Symbol> ',' <Color> ')' ';'
        final int RULE_PLOTSTMT_DRAWSYMBOL_LPARAN_COMMA_COMMA_COMMA_RPARAN_SEMI = 24;  // <Plot Stmt> ::= drawsymbol '(' <Expression> ',' <Symbol> ',' <Color> ',' <Position> ')' ';'
        final int RULE_PLOTSTMT_MARKPATTERN_LPARAN_RPARAN_SEMI = 25;  // <Plot Stmt> ::= markpattern '(' <Expression> ')' ';'
        final int RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_RPARAN_SEMI = 26;  // <Plot Stmt> ::= markpattern '(' <Expression> ',' IntegerLiteral ')' ';'
        final int RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_COMMA_RPARAN_SEMI = 27;  // <Plot Stmt> ::= markpattern '(' <Expression> ',' IntegerLiteral ',' <Color> ')' ';'
        final int RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_COMMA_COMMA_RPARAN_SEMI = 28;  // <Plot Stmt> ::= markpattern '(' <Expression> ',' IntegerLiteral ',' <Color> ',' <Color> ')' ';'
        final int RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_COMMA_COMMA_COMMA_RPARAN_SEMI = 29;  // <Plot Stmt> ::= markpattern '(' <Expression> ',' IntegerLiteral ',' <Color> ',' <Color> ',' <Position> ')' ';'
        final int RULE_PLOTSTMT_BUY_LPARAN_RPARAN_SEMI = 30;  // <Plot Stmt> ::= buy '(' <Expression> ')' ';'
        final int RULE_PLOTSTMT_SELL_LPARAN_RPARAN_SEMI = 31;  // <Plot Stmt> ::= sell '(' <Expression> ')' ';'
        final int RULE_PLOTSTMT_SELLSHORT_LPARAN_RPARAN_SEMI = 32;  // <Plot Stmt> ::= sellshort '(' <Expression> ')' ';'
        final int RULE_PLOTSTMT_BUYTOCOVER_LPARAN_RPARAN_SEMI = 33;  // <Plot Stmt> ::= buytocover '(' <Expression> ')' ';'
        final int RULE_DECLAREVAR_PARAM_TYPEDECLARATION_IDENTIFIER = 34;  // <DeclareVar> ::= param TypeDeclaration Identifier
        final int RULE_DECLAREVAR_TYPEDECLARATION_IDENTIFIER = 35;  // <DeclareVar> ::= TypeDeclaration Identifier
        final int RULE_DECLAREVAR_COMMA_IDENTIFIER = 36;  // <DeclareVar> ::= <DeclareVar> ',' Identifier
        final int RULE_LOOPDECASSN = 37;  // <LoopDecAssn> ::= <DeclAssign>
        final int RULE_LOOPDECASSN2 = 38;  // <LoopDecAssn> ::= <LoopAssign>
        final int RULE_LOOPASSIGN_IDENTIFIER_EQ = 39;  // <LoopAssign> ::= Identifier '=' <Add Exp>
        final int RULE_LOOPASSIGN = 40;  // <LoopAssign> ::=
        final int RULE_DECLASSIGN_PARAM_TYPEDECLARATION_IDENTIFIER_EQ = 41;  // <DeclAssign> ::= param TypeDeclaration Identifier '=' <Expression>
        final int RULE_DECLASSIGN_TYPEDECLARATION_IDENTIFIER_EQ = 42;  // <DeclAssign> ::= TypeDeclaration Identifier '=' <Expression>
        final int RULE_EXPRESSION_AND = 43;  // <Expression> ::= <Expression> and <Compare Exp>
        final int RULE_EXPRESSION_OR = 44;  // <Expression> ::= <Expression> or <Compare Exp>
        final int RULE_EXPRESSION_BOOLEANLITERAL = 45;  // <Expression> ::= BooleanLiteral
        final int RULE_EXPRESSION = 46;  // <Expression> ::= <Compare Exp>
        final int RULE_COMPAREEXP_GT = 47;  // <Compare Exp> ::= <Add Exp> '>' <Add Exp>
        final int RULE_COMPAREEXP_LT = 48;  // <Compare Exp> ::= <Add Exp> '<' <Add Exp>
        final int RULE_COMPAREEXP_LTEQ = 49;  // <Compare Exp> ::= <Add Exp> '<=' <Add Exp>
        final int RULE_COMPAREEXP_GTEQ = 50;  // <Compare Exp> ::= <Add Exp> '>=' <Add Exp>
        final int RULE_COMPAREEXP_EQEQ = 51;  // <Compare Exp> ::= <Add Exp> '==' <Add Exp>
        final int RULE_COMPAREEXP_LTGT = 52;  // <Compare Exp> ::= <Add Exp> '<>' <Add Exp>
        final int RULE_COMPAREEXP = 53;  // <Compare Exp> ::= <Add Exp>
        final int RULE_ADDEXP_PLUS = 54;  // <Add Exp> ::= <Add Exp> '+' <Mult Exp>
        final int RULE_ADDEXP_MINUS = 55;  // <Add Exp> ::= <Add Exp> '-' <Mult Exp>
        final int RULE_ADDEXP = 56;  // <Add Exp> ::= <Mult Exp>
        final int RULE_MULTEXP_TIMES = 57;  // <Mult Exp> ::= <Mult Exp> '*' <Negate Exp>
        final int RULE_MULTEXP_DIV = 58;  // <Mult Exp> ::= <Mult Exp> '/' <Negate Exp>
        final int RULE_MULTEXP = 59;  // <Mult Exp> ::= <Negate Exp>
        final int RULE_NEGATEEXP_MINUS = 60;  // <Negate Exp> ::= '-' <Value>
        final int RULE_NEGATEEXP = 61;  // <Negate Exp> ::= <Value>
        final int RULE_VALUE_IDENTIFIER = 62;  // <Value> ::= Identifier
        final int RULE_VALUE_INTEGERLITERAL = 63;  // <Value> ::= IntegerLiteral
        final int RULE_VALUE_NUMERICLITERAL = 64;  // <Value> ::= NumericLiteral
        final int RULE_VALUE_OHLCVLITERAL = 65;  // <Value> ::= OHLCVLiteral
        final int RULE_VALUE = 66;  // <Value> ::= <Indicator>
        final int RULE_VALUE2 = 67;  // <Value> ::= <Function>
        final int RULE_VALUE_LPARAN_RPARAN = 68;  // <Value> ::= '(' <Expression> ')'
        final int RULE_VALUE_MAMETHODLITERAL = 69;  // <Value> ::= MAMethodLiteral
        final int RULE_VALUE_CALCMETDLITERAL = 70;  // <Value> ::= CalcMetdLiteral
        final int RULE_VALUE_CCPERIODLITERAL = 71;  // <Value> ::= CopCurvePeriodLiteral
        final int RULE_INDICATOR_IDENTIFIER_LPARAN_RPARAN = 72;  // <Indicator> ::= Identifier '(' <Params> ')'
        final int RULE_PARAMS = 73;  // <Params> ::= <ParamList>
        final int RULE_PARAMS2 = 74;  // <Params> ::=
        final int RULE_PARAMLIST_COMMA = 75;  // <ParamList> ::= <ParamList> ',' <Expression>
        final int RULE_PARAMLIST = 76;  // <ParamList> ::= <Expression>
        final int RULE_FUNCTION_FUNCTION1PARAM_LPARAN_RPARAN = 77;  // <Function> ::= 'Function1Param' '(' <Expression> ')'
        final int RULE_FUNCTION_FUNCTION2PARAM_LPARAN_COMMA_RPARAN = 78;  // <Function> ::= 'Function2Param' '(' <Expression> ',' <Expression> ')'
        final int RULE_FUNCTION_FUNCTION3PARAM_LPARAN_COMMA_COMMA_RPARAN = 79;  // <Function> ::= 'Function3Param' '(' <Expression> ',' <Expression> ',' <Expression> ')'
        final int RULE_COLOR_COLORLITERAL = 80;  // <Color> ::= ColorLiteral
        final int RULE_COLOR_RGB_LPARAN_INTEGERLITERAL_COMMA_INTEGERLITERAL_COMMA_INTEGERLITERAL_RPARAN = 81;  // <Color> ::= RGB '(' IntegerLiteral ',' IntegerLiteral ',' IntegerLiteral ')'
        final int RULE_STYLE_STYLELITERAL = 82;  // <Style> ::= StyleLiteral
        final int RULE_STYLE_INTEGERLITERAL = 83;  // <Style> ::= IntegerLiteral
        final int RULE_SYMBOL_SYMBOLLITERAL = 84;  // <Symbol> ::= SymbolLiteral
        final int RULE_SYMBOL_INTEGERLITERAL = 85;  // <Symbol> ::= IntegerLiteral
        final int RULE_POSITION_POSITIONLITERAL = 86;  // <Position> ::= PositionLiteral
        final int RULE_POSITION_INTEGERLITERAL = 87;  // <Position> ::= IntegerLiteral

    }

    private GOLDParser parser;
    private Hashtable htStatements = new Hashtable();
    private int statementLevel = 0;
    private Hashtable htIndParams = new Hashtable();
    private int paramLevel = 0;
    private ArrayList alPlotStatements;
    private ArrayList alVars;
    private CIDeclareVar currentDeclaration;

    private JTextPane errorMessageArea;

    public IndicatorParser(String cgtPath) {   //"./system/CustomIndicator.cgt"
        parser = new GOLDParser();
        try {
            parser.loadCompiledGrammar(cgtPath);
        } catch (Exception ex) {
            System.out.println("**PARSER ERROR**\n" + ex.toString());
        }
    }

    public IndicatorParser(String cgtPath, JTextPane errorMessageArea) {
        this.errorMessageArea = errorMessageArea;
        parser = new GOLDParser();
        try {
            parser.loadCompiledGrammar(cgtPath);
        } catch (Exception ex) {
            System.out.println("**PARSER ERROR**\n" + ex.toString());
            Document doc = errorMessageArea.getDocument();
            Element e = doc.getDefaultRootElement();
            // Copy attribute Set
            AttributeSet attr = e.getAttributes().copyAttributes();
            try {
                doc.insertString(doc.getLength(), "**PARSER ERROR**\n", attr);
            } catch (BadLocationException e1) {
                e1.printStackTrace(); // remove later
            }
        }
    }


    private CIProgram getProgramTree(Reduction program) {
        CIProgram custIndProgram;
        System.out.println("start parsing...");
        // This switch statement is used as a _bug fix to what mathew hawkins parser does
        // The parser Accepts without parsing down to the StartSymbol - Huge _Bug
        switch (program.getParentRule().getTableIndex()) {
            case RuleConstants.RULE_PROGRAM:
                custIndProgram = (CIProgram) CreateObject(program);
                break;
//            case RuleConstants.RULE_PLOTSTMT_PLOT_SEMI:
//            case RuleConstants.RULE_PLOTSTMT_PLOT_LPARAN_COMMA_RPARAN_SEMI:
//            case RuleConstants.RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_RPARAN_SEMI:
//            case RuleConstants.RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_INTEGERLITERAL_RPARAN_SEMI:
//            case RuleConstants.RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_COMMA_INTEGERLITERAL_RPARAN_SEMI:
//            case RuleConstants.RULE_PLOTSTMT_DRAWSYMBOL_LPARAN_COMMA_RPARAN_SEMI:
//            case RuleConstants.RULE_PLOTSTMT_DRAWSYMBOL_LPARAN_COMMA_COMMA_RPARAN_SEMI:
//            case RuleConstants.RULE_PLOTSTMT_DRAWSYMBOL_LPARAN_COMMA_COMMA_COMMA_RPARAN_SEMI:
//            case RuleConstants.RULE_PLOTSTMT_MARKPATTERN_LPARAN_RPARAN_SEMI:
//            case RuleConstants.RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_RPARAN_SEMI:
//            case RuleConstants.RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_COMMA_RPARAN_SEMI:
//            case RuleConstants.RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_COMMA_COMMA_RPARAN_SEMI:
//            case RuleConstants.RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_COMMA_COMMA_COMMA_RPARAN_SEMI:
//            case RuleConstants.RULE_PRGMSTMTS:
            default:
                custIndProgram = new CIProgram(createStatements(program));
                break;
        }
        System.out.println("...ended parsing!");
        return custIndProgram;
    }

    private Object CreateObject(Object token) {
        if (token instanceof Reduction) {
            return CreateObjectFromNonterminal((Reduction) token);
        } else if (token instanceof Token) {
            return CreateObjectFromTerminal((Token) token);
        }
        return null;
    }

    public Object CreateObjectFromNonterminal(Reduction token) {
        switch (token.getParentRule().getTableIndex()) {

            //#region Main Blocks /////////////////////////////////////////////////////////////////////////
            //case RuleConstants.RULE_PROGRAM:
            //    //<Program> ::= <Statements> <Plot Stmts>
            //    return new CIProgram(
            //        createStatements(token.getToken(0).getData()),
            //        createPlotStatements(token.getToken(1).getData()));

            //case RuleConstants.RULE_PROGRAM2:
            //    //<Program> ::= <Plot Stmts>
            //    return new CIProgram(
            //        null,
            //        createPlotStatements(token.getToken(0).getData()));

            case RuleConstants.RULE_PROGRAM:
                //<Program> ::= <Prgm Stmts>
                return new CIProgram(createStatements(token.getToken(0).getData()));

            case RuleConstants.RULE_PRGMSTMTS:
                //<Prgm Stmts> ::= <Prgm Stmt> <Prgm Stmts>
                ArrayList alStatements = (ArrayList) htStatements.get(statementLevel);
                alStatements.add(CreateObject(token.getToken(0).getData()));
                return CreateObject(token.getToken(1).getData());

            case RuleConstants.RULE_PRGMSTMTS2:
                //<Prgm Stmts> ::= <Prgm Stmt>
                return CreateObject(token.getToken(0).getData());

            case RuleConstants.RULE_PRGMSTMT:
                //<Prgm Stmt> ::= <Statement>
                return CreateObject(token.getToken(0).getData());

            case RuleConstants.RULE_PRGMSTMT2:
                //<Prgm Stmt> ::= <Plot Stmt>
                return CreateObject(token.getToken(0).getData());

            case RuleConstants.RULE_STATEMENTS:
                //<Statements> ::= <Statement> <Statements>
                ArrayList alStmts = (ArrayList) htStatements.get(statementLevel);
                alStmts.add(CreateObject(token.getToken(0).getData()));
                return CreateObject(token.getToken(1).getData());

            case RuleConstants.RULE_STATEMENTS2:
                //<Statements> ::= <Statement>
                return CreateObject(token.getToken(0).getData());
            //#endregion /////////////////////////////////////////////////////////////////////////////////

            //#region Statements /////////////////////////////////////////////////////////////////////////
            case RuleConstants.RULE_STATEMENT_SEMI:
                //<Statement> ::= <DeclareVar> ;
                return createDeclaration(token.getToken(0).getData());

            case RuleConstants.RULE_STATEMENT_SEMI2:
                //<Statement> ::= <DeclAssign> ;
                return (CIDeclareAssign) CreateObject(token.getToken(0).getData());

            case RuleConstants.RULE_STATEMENT_IDENTIFIER_EQ_SEMI:
                //<Statement> ::= Identifier = <Expression> ;
                return new CIObjectAssign(
                        new CIAssignment((String) CreateObject(token.getToken(0)),
                                (CIExpression) CreateObject(token.getToken(2).getData()))
                );

            case RuleConstants.RULE_STATEMENT_FOR_LPARAN_SEMI_SEMI_RPARAN_DO_END:
                //<Statement> ::= for ( <LoopDecAssn> ; <Expression> ; <LoopAssign> ) do <Statements> end
                return new CIForDoEnd((CIDeclareAssign) CreateObject(token.getToken(2).getData()),
                        (CIExpression) CreateObject(token.getToken(4).getData()),
                        (CIAssignment) CreateObject(token.getToken(6).getData()),
                        createStatements(token.getToken(9).getData()));

            case RuleConstants.RULE_STATEMENT_WHILE_LPARAN_RPARAN_DO_END:
                //<Statement> ::= while ( <Expression> ) do <Statements> end
                return new CIWhileDoEnd((CIExpression) CreateObject(token.getToken(2).getData()),
                        createStatements(token.getToken(5).getData()));

            case RuleConstants.RULE_STATEMENT_IF_LPARAN_RPARAN_THEN_END:
                //<Statement> ::= if ( <Expression> ) then <Statements> end
                return new CIIfThenEnd((CIExpression) CreateObject(token.getToken(2).getData()),
                        createStatements(token.getToken(5).getData()));

            case RuleConstants.RULE_STATEMENT_IF_LPARAN_RPARAN_THEN_ELSE_END:
                //<Statement> ::= if ( <Expression> ) then <Statements> else <Statements> end
                return new CIIfThenElseEnd((CIExpression) CreateObject(token.getToken(2).getData()),
                        createStatements(token.getToken(5).getData()),
                        createStatements(token.getToken(7).getData()));

            case RuleConstants.RULE_STATEMENT_SEMI3:
                //<Statement> ::= ;
                return new CIStatement();
            //#endregion  ////////////////////////////////////////////////////////////////////////////////

            //#region Plot ///////////////////////////////////////////////////////////////////////////////
//            case RuleConstants.RULE_PLOTSTMTS :
//                //<Plot Stmts> ::= <Plot Stmt> <Plot Stmts>
//                alPlotStatements.add(CreateObject(token.getToken(0).getData()));
//                return CreateObject(token.getToken(1).getData());
//
//            case RuleConstants.RULE_PLOTSTMTS2 :
//                //<Plot Stmts> ::= <Plot Stmt>
//                return CreateObject(token.getToken(0).getData());
            case RuleConstants.RULE_PLOTSTMT_PLOT_SEMI:
                //<Plot Stmt> ::= plot <Add Exp> ;
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(1).getData()));

            case RuleConstants.RULE_PLOTSTMT_PLOT_LPARAN_COMMA_RPARAN_SEMI:
                //<Plot Stmt> ::= plot ( <Add Exp> , <Color> ) ;
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()), (CIColor) CreateObject(token.getToken(4).getData()));

            case RuleConstants.RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_RPARAN_SEMI:
                //<Plot Stmt> ::= plot ( <Add Exp> , <Color> , <Style> ) ;
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()), (CIColor) CreateObject(token.getToken(4).getData()),
                        (CIStyle) CreateObject(token.getToken(6).getData()));

            case RuleConstants.RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_INTEGERLITERAL_RPARAN_SEMI:
                //<Plot Stmt> ::= plot ( <Add Exp> , <Color> , <Style> , IntegerLiteral ) ;
                // TODO: Validate NumericLiteral for a value 1, 1.5, 2, 3, 5
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()), (CIColor) CreateObject(token.getToken(4).getData()),
                        (CIStyle) CreateObject(token.getToken(6).getData()), Float.parseFloat(((CINumericLiteral) CreateObject(token.getToken(8))).getLiteral()));

            case RuleConstants.RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_NUMERICLITERAL_RPARAN_SEMI:
                //<Plot Stmt> ::= plot ( <Add Exp> , <Color> , <Style> , NumericLiteral ) ;
                // TODO: Validate NumericLiteral for a value 1, 1.5, 2, 3, 5
                Object obj = CreateObject(token.getToken(8));
                String temp = ((CINumericLiteral) obj).getLiteral();
                float thickness = Float.parseFloat(temp);

                float test = Float.parseFloat(((CINumericLiteral)CreateObject(token.getToken(8))).getLiteral());
                // return new CIPlotStatement((CIExpression)CreateObject(token.getToken(2).getData()), (CIColor)CreateObject(token.getToken(4).getData()),
                         //           (CIStyle)CreateObject(token.getToken(6).getData()), Float.parseFloat(((CINumericLiteral)CreateObject(token.getToken(8).getData())).getLiteral()));

                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()), (CIColor) CreateObject(token.getToken(4).getData()),
                        (CIStyle) CreateObject(token.getToken(6).getData()), Float.parseFloat(((CINumericLiteral) CreateObject(token.getToken(8))).getLiteral()));

            case RuleConstants.RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_COMMA_INTEGERLITERAL_RPARAN_SEMI:
                //<Plot Stmt> ::= plot ( <Add Exp> , <Color> , <Color> , <Style> , IntegerLiteral ) ;
                // TODO: Validate NumericLiteral for a value 1, 1.5, 2, 3, 5
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()), (CIColor) CreateObject(token.getToken(4).getData()),
                        (CIColor) CreateObject(token.getToken(6).getData()), (CIStyle) CreateObject(token.getToken(8).getData()), Float.parseFloat(((CINumericLiteral) CreateObject(token.getToken(10))).getLiteral()));

            case RuleConstants.RULE_PLOTSTMT_PLOT_LPARAN_COMMA_COMMA_COMMA_COMMA_NUMERICLITERAL_RPARAN_SEMI:
                //<Plot Stmt> ::= plot ( <Add Exp> , <Color> , <Color> , <Style> , NumericLiteral ) ;
                // TODO: Validate NumericLiteral for a value 1, 1.5, 2, 3, 5
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()), (CIColor) CreateObject(token.getToken(4).getData()),
                        (CIColor) CreateObject(token.getToken(6).getData()), (CIStyle) CreateObject(token.getToken(8).getData()), Float.parseFloat(((CINumericLiteral) CreateObject(token.getToken(10))).getLiteral()));

            case RuleConstants.RULE_PLOTSTMT_DRAWSYMBOL_LPARAN_COMMA_RPARAN_SEMI:
                //<Plot Stmt> ::= drawsymbol ( <Expression> , <Symbol> ) ;
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()), (CISymbol) CreateObject(token.getToken(4).getData()));

            case RuleConstants.RULE_PLOTSTMT_DRAWSYMBOL_LPARAN_COMMA_COMMA_RPARAN_SEMI:
                //<Plot Stmt> ::= drawsymbol ( <Expression> , <Symbol> , <Color> ) ;
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()), (CISymbol) CreateObject(token.getToken(4).getData()),
                        (CIColor) CreateObject(token.getToken(6).getData()));

            case RuleConstants.RULE_PLOTSTMT_DRAWSYMBOL_LPARAN_COMMA_COMMA_COMMA_RPARAN_SEMI:
                //<Plot Stmt> ::= drawsymbol ( <Expression> , <Symbol> , <Color> , <Position> ) ;
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()), (CISymbol) CreateObject(token.getToken(4).getData()),
                        (CIColor) CreateObject(token.getToken(6).getData()), (CIPosition) CreateObject(token.getToken(8).getData()));

            case RuleConstants.RULE_PLOTSTMT_MARKPATTERN_LPARAN_RPARAN_SEMI:
                //<Plot Stmt> ::= markpattern ( <Expression> ) ;
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()), new CISymbol(1000), new CIColor(250, 235, 135));

            case RuleConstants.RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_RPARAN_SEMI:
                //<Plot Stmt> ::= markpattern ( <Expression> , IntegerLiteral ) ;
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()),
                        new CISymbol(999 + Integer.parseInt(((CINumericLiteral) CreateObject(token.getToken(4))).getLiteral())), new CIColor(250, 235, 135));

            case RuleConstants.RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_COMMA_RPARAN_SEMI:
                //<Plot Stmt> ::= markpattern ( <Expression> , IntegerLiteral , <Color> ) ;
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()),
                        new CISymbol(999 + Integer.parseInt(((CINumericLiteral) CreateObject(token.getToken(4))).getLiteral())), (CIColor) CreateObject(token.getToken(6).getData()));

            case RuleConstants.RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_COMMA_COMMA_RPARAN_SEMI:
                //<Plot Stmt> ::= markpattern ( <Expression> , IntegerLiteral , <Color> , <Color> ) ;
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()),
                        new CISymbol(999 + Integer.parseInt(((CINumericLiteral) CreateObject(token.getToken(4))).getLiteral())),
                        (CIColor) CreateObject(token.getToken(6).getData()), (CIColor) CreateObject(token.getToken(8).getData()));

            case RuleConstants.RULE_PLOTSTMT_MARKPATTERN_LPARAN_COMMA_INTEGERLITERAL_COMMA_COMMA_COMMA_RPARAN_SEMI:
                //<Plot Stmt> ::= markpattern ( <Expression> , IntegerLiteral , <Color> , <Color> , <Position> ) ;
                return new CIPlotStatement((CIExpression) CreateObject(token.getToken(2).getData()),
                        new CISymbol(999 + Integer.parseInt(((CINumericLiteral) CreateObject(token.getToken(4))).getLiteral())),
                        (CIColor) CreateObject(token.getToken(6).getData()), (CIColor) CreateObject(token.getToken(8).getData()), (CIPosition) CreateObject(token.getToken(10).getData()));

            case (int)RuleConstants.RULE_PLOTSTMT_BUY_LPARAN_RPARAN_SEMI:
                //<Plot Stmt> ::= buy ( <Expression> ) ;
                return new CIPlotStatement((CIExpression)CreateObject(token.getToken(2).getData()),
                        new CISymbol(SymbolLiteral.BUY), new CIColor(ColorLiteral.BLUE));

            case (int)RuleConstants.RULE_PLOTSTMT_SELL_LPARAN_RPARAN_SEMI:
                //<Plot Stmt> ::= sell ( <Expression> ) ;
                return new CIPlotStatement((CIExpression)CreateObject(token.getToken(2).getData()),
                        new CISymbol(SymbolLiteral.SELL), new CIColor(ColorLiteral.MAGENTA));

            case (int)RuleConstants.RULE_PLOTSTMT_SELLSHORT_LPARAN_RPARAN_SEMI:
                //<Plot Stmt> ::= sellshort ( <Expression> ) ;
                return new CIPlotStatement((CIExpression)CreateObject(token.getToken(2).getData()),
                        new CISymbol(SymbolLiteral.SELL_SHORT), new CIColor(ColorLiteral.RED));

            case (int)RuleConstants.RULE_PLOTSTMT_BUYTOCOVER_LPARAN_RPARAN_SEMI:
                //<Plot Stmt> ::= buytocover ( <Expression> ) ;
                return new CIPlotStatement((CIExpression)CreateObject(token.getToken(2).getData()),
                        new CISymbol(SymbolLiteral.BUY_TO_COVER), new CIColor(ColorLiteral.GREEN));

            //#endregion  ////////////////////////////////////////////////////////////////////////////////

            //region Param, Var Declaration //////////////////////////////////////////////////////////////
            case RuleConstants.RULE_DECLAREVAR_PARAM_TYPEDECLARATION_IDENTIFIER:
                //<DeclareVar> ::= param TypeDeclaration Identifier
                currentDeclaration.setDeclarationType(DeclarationType.PARAM);
                currentDeclaration.setParamType((ParamType) CreateObject(token.getToken(1)));
                return new CIIdentifier((String) CreateObject(token.getToken(2)),
                        currentDeclaration.getParamType());

            case RuleConstants.RULE_DECLAREVAR_TYPEDECLARATION_IDENTIFIER:
                //<DeclareVar> ::= TypeDeclaration Identifier
                currentDeclaration.setDeclarationType(DeclarationType.VAR);
                currentDeclaration.setParamType((ParamType) CreateObject(token.getToken(0)));
                return new CIIdentifier((String) CreateObject(token.getToken(1)),
                        currentDeclaration.getParamType());

            case RuleConstants.RULE_DECLAREVAR_COMMA_IDENTIFIER:
                //<DeclareVar> ::= <DeclareVar> , Identifier
                alVars.add(CreateObject(token.getToken(0).getData()));
                return new CIIdentifier((String) CreateObject(token.getToken(2)),
                        currentDeclaration.getParamType());
            //#endregion /////////////////////////////////////////////////////////////////////////////////

            //#region Loop / Object Assign ///////////////////////////////////////////////////////////////
            case RuleConstants.RULE_LOOPDECASSN:
                //<LoopDecAssn> ::= <DeclAssign>
                return (CIDeclareAssign) CreateObject(token.getToken(0).getData());

            case RuleConstants.RULE_LOOPDECASSN2:
                //<LoopDecAssn> ::= <LoopAssign>
                return new CIDeclareAssign(DeclarationType.VAR,
                        null,
                        (CIAssignment) CreateObject(token.getToken(0).getData()));

            case RuleConstants.RULE_LOOPASSIGN_IDENTIFIER_EQ:
                //<LoopAssign> ::= Identifier = <Add Exp>
                return new CIAssignment((String) CreateObject(token.getToken(0)),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_LOOPASSIGN:
                //<LoopAssign> ::=
                return null;

            case RuleConstants.RULE_DECLASSIGN_PARAM_TYPEDECLARATION_IDENTIFIER_EQ:
                //<DeclAssign> ::= param TypeDeclaration Identifier = <Expression>
                return new CIDeclareAssign(DeclarationType.PARAM,
                        new CIIdentifier((String) CreateObject(token.getToken(2)),
                                (ParamType) CreateObject(token.getToken(1))),
                        new CIAssignment((String) CreateObject(token.getToken(2)),
                                (CIExpression) CreateObject(token.getToken(4).getData())));

            case RuleConstants.RULE_DECLASSIGN_TYPEDECLARATION_IDENTIFIER_EQ:
                //<DeclAssign> ::= TypeDeclaration Identifier = <Expression>
                return new CIDeclareAssign(DeclarationType.VAR,
                        new CIIdentifier((String) CreateObject(token.getToken(1)),
                                (ParamType) CreateObject(token.getToken(0))),
                        new CIAssignment((String) CreateObject(token.getToken(1)),
                                (CIExpression) CreateObject(token.getToken(3).getData())));
            //#endregion /////////////////////////////////////////////////////////////////////////////////

            //#region Boolean Expression /////////////////////////////////////////////////////////////////
            case RuleConstants.RULE_EXPRESSION_AND:
                //<Expression> ::= <Expression> AND <Compare Exp>
                return new CIBinaryExpression((String) CreateObject(token.getToken(1)),
                        (CIExpression) CreateObject(token.getToken(0).getData()),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_EXPRESSION_OR:
                //<Expression> ::= <Expression> OR <Compare Exp>
                return new CIBinaryExpression((String) CreateObject(token.getToken(1)),
                        (CIExpression) CreateObject(token.getToken(0).getData()),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_EXPRESSION_BOOLEANLITERAL:
                //<Expression> ::= BooleanLiteral
                return (CIStringLiteral) CreateObject(token.getToken(0));

            case RuleConstants.RULE_EXPRESSION:
                //<Expression> ::= <Compare Exp>
                return CreateObject(token.getToken(0).getData());
            //#endregion /////////////////////////////////////////////////////////////////////////////////

            //#region Compare Expression /////////////////////////////////////////////////////////////////
            case RuleConstants.RULE_COMPAREEXP_GT:
                //<Compare Exp> ::= <Add Exp> > <Add Exp>
                return new CIBinaryExpression((String) CreateObject(token.getToken(1)),
                        (CIExpression) CreateObject(token.getToken(0).getData()),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_COMPAREEXP_LT:
                //<Compare Exp> ::= <Add Exp> < <Add Exp>
                return new CIBinaryExpression((String) CreateObject(token.getToken(1)),
                        (CIExpression) CreateObject(token.getToken(0).getData()),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_COMPAREEXP_LTEQ:
                //<Compare Exp> ::= <Add Exp> <= <Add Exp>
                return new CIBinaryExpression((String) CreateObject(token.getToken(1)),
                        (CIExpression) CreateObject(token.getToken(0).getData()),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_COMPAREEXP_GTEQ:
                //<Compare Exp> ::= <Add Exp> >= <Add Exp>
                return new CIBinaryExpression((String) CreateObject(token.getToken(1)),
                        (CIExpression) CreateObject(token.getToken(0).getData()),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_COMPAREEXP_EQEQ:
                //<Compare Exp> ::= <Add Exp> == <Add Exp>
                return new CIBinaryExpression((String) CreateObject(token.getToken(1)),
                        (CIExpression) CreateObject(token.getToken(0).getData()),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_COMPAREEXP_LTGT:
                //<Compare Exp> ::= <Add Exp> <> <Add Exp>
                return new CIBinaryExpression((String) CreateObject(token.getToken(1)),
                        (CIExpression) CreateObject(token.getToken(0).getData()),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_COMPAREEXP:
                //<Compare Exp> ::= <Add Exp>
                return CreateObject(token.getToken(0).getData());
            //#endregion  ////////////////////////////////////////////////////////////////////////////////

            //#region Add Expression /////////////////////////////////////////////////////////////////////
            case RuleConstants.RULE_ADDEXP_PLUS:
                //<Add Exp> ::= <Add Exp> + <Mult Exp>
                return new CIBinaryExpression((String) CreateObject(token.getToken(1)),
                        (CIExpression) CreateObject(token.getToken(0).getData()),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_ADDEXP_MINUS:
                //<Add Exp> ::= <Add Exp> - <Mult Exp>
                return new CIBinaryExpression((String) CreateObject(token.getToken(1)),
                        (CIExpression) CreateObject(token.getToken(0).getData()),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_ADDEXP:
                //<Add Exp> ::= <Mult Exp>
                return CreateObject(token.getToken(0).getData());
            //#endregion /////////////////////////////////////////////////////////////////////////////////

            //#region Multiply / Negate //////////////////////////////////////////////////////////////////
            case RuleConstants.RULE_MULTEXP_TIMES:
                //<Mult Exp> ::= <Mult Exp> * <Negate Exp>
                return new CIBinaryExpression((String) CreateObject(token.getToken(1)),
                        (CIExpression) CreateObject(token.getToken(0).getData()),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_MULTEXP_DIV:
                //<Mult Exp> ::= <Mult Exp> / <Negate Exp>
                return new CIBinaryExpression((String) CreateObject(token.getToken(1)),
                        (CIExpression) CreateObject(token.getToken(0).getData()),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_MULTEXP:
                //<Mult Exp> ::= <Negate Exp>
                return CreateObject(token.getToken(0).getData());

            case RuleConstants.RULE_NEGATEEXP_MINUS:
                //<Negate Exp> ::= - <Value>
                return new CIUnaryExpression((String) CreateObject(token.getToken(0)),
                        (CIExpression) CreateObject(token.getToken(1).getData()));

            case RuleConstants.RULE_NEGATEEXP:
                //<Negate Exp> ::= <Value>
                return CreateObject(token.getToken(0).getData());
            //#endregion /////////////////////////////////////////////////////////////////////////////////

            //#region Value Types ////////////////////////////////////////////////////////////////////////
            case RuleConstants.RULE_VALUE_IDENTIFIER:
                //<Value> ::= Identifier
                return new CIIdentifier((String) CreateObject(token.getToken(0)), ParamType.UNKNOWN);

            case RuleConstants.RULE_VALUE_INTEGERLITERAL:
                //<Value> ::= IntegerLiteral
                return (CINumericLiteral) CreateObject(token.getToken(0));

            case RuleConstants.RULE_VALUE_NUMERICLITERAL:
                //<Value> ::= NumericLiteral
                return (CINumericLiteral) CreateObject(token.getToken(0));

            case RuleConstants.RULE_VALUE_OHLCVLITERAL:
                //<Value> ::= OHLCVLiteral
                return (CIStringLiteral) CreateObject(token.getToken(0));

            case RuleConstants.RULE_VALUE:
                //<Value> ::= <Indicator>
                return CreateObject(token.getToken(0).getData());

            case RuleConstants.RULE_VALUE2:
                //<Value> ::= <Function>
                return CreateObject(token.getToken(0).getData());

            case RuleConstants.RULE_VALUE_LPARAN_RPARAN:
                //<Value> ::= ( <Expression> )
                return new CIBracketExpression((CIExpression) CreateObject(token.getToken(1).getData()));

            case RuleConstants.RULE_VALUE_MAMETHODLITERAL:
                //<Value> ::= MAMethodLiteral
                return (CIStringLiteral) CreateObject(token.getToken(0));

            case RuleConstants.RULE_VALUE_CALCMETDLITERAL:
                //<Value> ::= CalcMetdLiteral
                return (CIStringLiteral) CreateObject(token.getToken(0));

            case RuleConstants.RULE_VALUE_CCPERIODLITERAL:
                //<Value> ::= CCPERIODLITERAL
                return (CIStringLiteral) CreateObject(token.getToken(0));
            //#endregion /////////////////////////////////////////////////////////////////////////////////

            //#region General Params  ////////////////////////////////////////////////////////////////////

            case RuleConstants.RULE_COLOR_COLORLITERAL:
                //<Color> ::= ColorLiteral
                return (CIColor) CreateObject(token.getToken(0));

            case RuleConstants.RULE_COLOR_RGB_LPARAN_INTEGERLITERAL_COMMA_INTEGERLITERAL_COMMA_INTEGERLITERAL_RPARAN:
                //<Color> ::= RGB ( IntegerLiteral , IntegerLiteral , IntegerLiteral )
                return new CIColor(Integer.parseInt(((CINumericLiteral) CreateObject(token.getToken(2))).getLiteral()),
                        Integer.parseInt(((CINumericLiteral) CreateObject(token.getToken(4))).getLiteral()),
                        Integer.parseInt(((CINumericLiteral) CreateObject(token.getToken(6))).getLiteral()));

            case RuleConstants.RULE_STYLE_STYLELITERAL:
                //<Style> ::= StyleLiteral
                return (CIStyle) CreateObject(token.getToken(0));

            case RuleConstants.RULE_STYLE_INTEGERLITERAL:
                //<Style> ::= IntegerLiteral
                return new CIStyle(Integer.parseInt(((CINumericLiteral) CreateObject(token.getToken(0).getData())).getLiteral()));

            case RuleConstants.RULE_SYMBOL_SYMBOLLITERAL:
                //<Symbol> ::= SymbolLiteral
                return (CISymbol) CreateObject(token.getToken(0));

            case RuleConstants.RULE_SYMBOL_INTEGERLITERAL:
                //<Symbol> ::= IntegerLiteral
                return new CISymbol(Integer.parseInt(((CINumericLiteral) CreateObject(token.getToken(0).getData())).getLiteral()));

            case RuleConstants.RULE_POSITION_POSITIONLITERAL:
                //<Position> ::= PositionLiteral
                return (CIPosition) CreateObject(token.getToken(0));

            case RuleConstants.RULE_POSITION_INTEGERLITERAL:
                //<Position> ::= IntegerLiteral
                return new CIPosition(Integer.parseInt(((CINumericLiteral) CreateObject(token.getToken(0).getData())).getLiteral()));

            //#endregion /////////////////////////////////////////////////////////////////////////////////

            //#region Indicators /////////////////////////////////////////////////////////////////////////
            case RuleConstants.RULE_INDICATOR_IDENTIFIER_LPARAN_RPARAN:
                //<Indicator> ::= Identifier ( <Params> )
                return new CIIndicator((String) CreateObject(token.getToken(0)),
                        createParams(token.getToken(2).getData()));

            case RuleConstants.RULE_PARAMS:
                //<Params> ::= <ParamList>
                return CreateObject(token.getToken(0).getData());

            case RuleConstants.RULE_PARAMS2:
                //<Params> ::=
                return null;

            case RuleConstants.RULE_PARAMLIST_COMMA:
                //<ParamList> ::= <ParamList> , <Expression>
                ArrayList alParams = (ArrayList) htIndParams.get(paramLevel);
                alParams.add(CreateObject(token.getToken(0).getData()));
                return CreateObject(token.getToken(2).getData());

            case RuleConstants.RULE_PARAMLIST:
                //<ParamList> ::= <Expression>
                return CreateObject(token.getToken(0).getData());
            //#endregion /////////////////////////////////////////////////////////////////////////////////

            //#region Functions //////////////////////////////////////////////////////////////////////////
            case RuleConstants.RULE_FUNCTION_FUNCTION1PARAM_LPARAN_RPARAN:
                //<Function> ::= Function1Param ( <Expression> )
                return new CIFunction((FunctionType) CreateObject(token.getToken(0)),
                        (CIExpression) CreateObject(token.getToken(2).getData()));

            case RuleConstants.RULE_FUNCTION_FUNCTION2PARAM_LPARAN_COMMA_RPARAN:
                //<Function> ::= Function2Param ( <Expression> , <Expression> )
                return new CIFunction((FunctionType) CreateObject(token.getToken(0)),
                        (CIExpression) CreateObject(token.getToken(2).getData()),
                        (CIExpression) CreateObject(token.getToken(4).getData()));

            case RuleConstants.RULE_FUNCTION_FUNCTION3PARAM_LPARAN_COMMA_COMMA_RPARAN:
                return new CIFunction((FunctionType) CreateObject(token.getToken(0)),
                        (CIExpression) CreateObject(token.getToken(2).getData()),
                        (CIExpression) CreateObject(token.getToken(4).getData()),
                        (CIExpression) CreateObject(token.getToken(6).getData()));

            //#endregion /////////////////////////////////////////////////////////////////////////////////

        }
        new Exception("Unknown Rule" + token.getParentRule()).printStackTrace(); // just reports the bug - it doesnt throw the exception
        return null;
    }

    private Object CreateObjectFromTerminal(Token token) {
        switch (token.getPSymbol().getTableIndex()) {
            //#region Ignore List 1
            case SymbolConstants.SYMBOL_EOF:
                //(EOF)
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_ERROR:
                //(Error)
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_WHITESPACE:
                //(Whitespace)
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_COMMENTEND:
                //(Comment End)
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_COMMENTLINE:
                //(Comment Line)
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_COMMENTSTART:
                //(Comment Start)
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_LPARAN:
                //(
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_RPARAN:
                //)
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_COMMA:
                //,
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_SEMI:
                //;
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_EQ:
                //=
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_DO:
                //do
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_ELSE:
                //else
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_END:
                //end
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_FOR:
                //for
                //todo: Create a new object that corresponds to the symbol
                return null;
            //#endregion

            //#region Operators (signs)
            case SymbolConstants.SYMBOL_MINUS:
                //-
                return token.getData().toString();

            case SymbolConstants.SYMBOL_TIMES:
                //*
                return token.getData().toString();

            case SymbolConstants.SYMBOL_DIV:
                ///
                return token.getData().toString();

            case SymbolConstants.SYMBOL_PLUS:
                //+
                return token.getData().toString();

            case SymbolConstants.SYMBOL_LT:
                //<
                return token.getData().toString();

            case SymbolConstants.SYMBOL_LTEQ:
                //<=
                return token.getData().toString();

            case SymbolConstants.SYMBOL_LTGT:
                //<>
                return token.getData().toString();

            case SymbolConstants.SYMBOL_EQEQ:
                //==
                return token.getData().toString();

            case SymbolConstants.SYMBOL_GT:
                //>
                return token.getData().toString();

            case SymbolConstants.SYMBOL_GTEQ:
                //>=
                return token.getData().toString();

            case SymbolConstants.SYMBOL_AND:
                //and
                return "&&";//token.getData().toString();

            case SymbolConstants.SYMBOL_OR:
                //or
                return "||";//token.getData().toString();
            //#endregion

            //#region Types, Functions and Identifier (Reference)
            case SymbolConstants.SYMBOL_IDENTIFIER:
                //Identifier
                return token.getData().toString();

            case SymbolConstants.SYMBOL_TYPEDECLARATION:
                //TypeDeclaration
                return IndicatorParser.getParamType(token.getData().toString());

            case SymbolConstants.SYMBOL_FUNCTION1PARAM:
                //Function1Param
                return IndicatorParser.getFunctionType(token.getData().toString());

            case SymbolConstants.SYMBOL_FUNCTION2PARAM:
                //Function2Param
                return IndicatorParser.getFunctionType(token.getData().toString());

            case SymbolConstants.SYMBOL_FUNCTION3PARAM:
                //Function3Param
                return IndicatorParser.getFunctionType(token.getData().toString());

            case (int) SymbolConstants.SYMBOL_RGB:
                //RGB
                // do nothing
                return null;
            //#endregion

            //#region Values
            case SymbolConstants.SYMBOL_BOOLEANLITERAL:
                //BooleanLiteral
                return CIStringLiteral.createLiteral(token.getData().toString());

            case SymbolConstants.SYMBOL_MAMETHODLITERAL:
                //MAMethodLiteral
                return CIStringLiteral.createLiteral(token.getData().toString());

            case SymbolConstants.SYMBOL_NUMERICLITERAL:
                //NumericLiteral
                return new CINumericLiteral(token.getData().toString());

            case SymbolConstants.SYMBOL_OHLCVLITERAL:
                //OHLCVLiteral
                return CIStringLiteral.createLiteral(token.getData().toString());

            case SymbolConstants.SYMBOL_CALCMETDLITERAL:
                //CalcMetdLiteral
                return CIStringLiteral.createLiteral(token.getData().toString());

            case SymbolConstants.SYMBOL_CCPERIODLITERAL:
                //CopCurvePeriodLiteral
                return CIStringLiteral.createLiteral(token.getData().toString());

            case (int) SymbolConstants.SYMBOL_COLORLITERAL:
                //ColorLiteral
                return CIColor.createLiteral(token.getData().toString());

            case (int) SymbolConstants.SYMBOL_INTEGERLITERAL:
                //IntegerLiteral
                return new CINumericLiteral(token.getData().toString());

            case (int) SymbolConstants.SYMBOL_POSITIONLITERAL:
                //PositionLiteral
                return new CIPosition(token.getData().toString());

            case (int) SymbolConstants.SYMBOL_STYLELITERAL:
                //StyleLiteral
                return new CIStyle(token.getData().toString());

            case (int) SymbolConstants.SYMBOL_SYMBOLLITERAL:
                //SymbolLiteral
                return new CISymbol(token.getData().toString());

            //#endregion

            //#region Ignore List
            case SymbolConstants.SYMBOL_DECLASSIGN:
                //<DeclAssign>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_IF:
                //if
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_PARAM:
                //param
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_PLOT:
                //plot
                //todo: Create a new object that corresponds to the symbol
                return null;

            case (int) SymbolConstants.SYMBOL_DRAWSYMBOL:
                //drawsymbol
                //todo: Create a new object that corresponds to the symbol
                return null;

            case (int) SymbolConstants.SYMBOL_MARKPATTERN:
                //markpattern
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_THEN:
                //then
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_WHILE:
                //while
                //todo: Create a new object that corresponds to the symbol
                return null;
            //#endregion

            //#region Rule List
            case SymbolConstants.SYMBOL_ADDEXP:
                //<Add Exp>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_COMPAREEXP:
                //<Compare Exp>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_DECLAREVAR:
                //<DeclareVar>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_EXPRESSION:
                //<Expression>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_FUNCTION:
                //<Function>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_INDICATOR:
                //<Indicator>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_LOOPASSIGN:
                //<LoopAssign>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_MULTEXP:
                //<Mult Exp>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_NEGATEEXP:
                //<Negate Exp>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_LOOPDECASSN:
                //<LoopDecAssn>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_PARAMLIST:
                //<ParamList>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_PARAMS:
                //<Params>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_PROGRAM:
                //<Program>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case (int) SymbolConstants.SYMBOL_PRGMSTMTS:
                //<Prgm Stmts>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case (int) SymbolConstants.SYMBOL_PRGMSTMT:
                //<Prgm Stmt>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_STATEMENT:
                //<Statement>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_PLOTSTMT:
                //<Plot Stmt>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_STATEMENTS:
                //<Statements>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case SymbolConstants.SYMBOL_VALUE:
                //<Value>
                //todo: Create a new object that corresponds to the symbol
                return null;
            //#endregion

            case (int) SymbolConstants.SYMBOL_COLOR:
                //<Color>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case (int) SymbolConstants.SYMBOL_POSITION:
                //<Position>
                //todo: Create a new object that corresponds to the symbol
                return null;

            case (int) SymbolConstants.SYMBOL_SYMBOL:
                //<Symbol>
                //todo: Create a new object that corresponds to the symbol
                return null;
        }
        new Exception("Unknown Symbol" + token.getPSymbol()).printStackTrace(); // just reports the bug - it doesnt throw the exception
        return null;
    }

    private ArrayList createStatements(Object token) {
        ArrayList alStatements = new ArrayList();
        statementLevel++;
        htStatements.put(statementLevel, alStatements);
        alStatements.add(CreateObject(token));
        htStatements.remove(statementLevel);
        statementLevel--;
        return alStatements;
    }

    private ArrayList createParams(Object token) {
        ArrayList alIndParams = new ArrayList();
        paramLevel++;
        htIndParams.put(paramLevel, alIndParams);
        Object obj = CreateObject(token);
        if (obj != null) alIndParams.add(obj);
        htIndParams.remove(paramLevel);
        paramLevel--;
        return alIndParams;
    }

//    private ArrayList createPlotStatements(Object token) {
//        alPlotStatements = new ArrayList();
//        alPlotStatements.add(CreateObject(token));
//        return alPlotStatements;
//    }

    private CIDeclareVar createDeclaration(Object token) {
        alVars = new ArrayList();
        currentDeclaration = new CIDeclareVar(alVars);
        alVars.add(CreateObject(token));
        return currentDeclaration;
    }

    public static ParamType getParamType(String type) {
        if ("int".equals(type)) {
            return ParamType.INT;
        } else if ("double".equals(type)) {
            return ParamType.DOUBLE;
        } else if ("MAMethod".equals(type)) {
            return ParamType.MA_METHOD;
        } else if ("CalcMethod".equals(type)) {
            return ParamType.CALC_METHOD;
        } else if ("CopCurvePeriod".equals(type)) {
            return ParamType.CC_PERIOD;
        } else if ("DataArray".equals(type)) {
            return ParamType.DATA_ARRAY;
        } else if ("bool".equals(type)) {
            return ParamType.BOOL;
        } else {
            return ParamType.DATA_ARRAY;
        }
    }

    public static FunctionType getFunctionType(String type) {
        if ("abs".equals(type)) {
            return FunctionType.ABS;
        } else if ("round".equals(type)) {
            return FunctionType.ROUND;
        } else if ("sqr".equals(type)) {
            return FunctionType.SQR;
        } else if ("sqrt".equals(type)) {
            return FunctionType.SQRT;
        } else if ("log".equals(type)) {
            return FunctionType.LOG;
        } else if ("ln".equals(type)) {
            return FunctionType.LN;
        } else if ("exp".equals(type)) {
            return FunctionType.EXP;
        } else if ("cum".equals(type)) {
            return FunctionType.CUM;
        } else if ("min".equals(type)) {
            return FunctionType.MIN;
        } else if ("max".equals(type)) {
            return FunctionType.MAX;
        } else if ("pow".equals(type)) {
            return FunctionType.POW;
        } else if ("ref".equals(type)) {
            return FunctionType.REF;
        } else if ("highest".equals(type)) {
            return FunctionType.HIGHEST;
        } else if ("lowest".equals(type)) {
            return FunctionType.LOWEST;
        } else if ("highestbars".equals(type)) {
            return FunctionType.HIGHESTBARS;
        } else if ("lowestbars".equals(type)) {
            return FunctionType.LOWESTBARS;
        } else if ("acos".equals(type)) {
            return FunctionType.ACOS;
        } else if ("asin".equals(type)) {
            return FunctionType.ASIN;
        } else if ("atan".equals(type)) {
            return FunctionType.ATAN;
        } else if ("ceiling".equals(type)) {
            return FunctionType.CEILING;
        } else if ("cos".equals(type)) {
            return FunctionType.COS;
        } else if ("cosh".equals(type)) {
            return FunctionType.COSH;
        } else if ("floor".equals(type)) {
            return FunctionType.FLOOR;
        } else if ("frac".equals(type)) {
            return FunctionType.FRAC;
        } else if ("trunc".equals(type)) {
            return FunctionType.TRUNC;
        } else if ("sign".equals(type)) {
            return FunctionType.SIGN;
        } else if ("sin".equals(type)) {
            return FunctionType.SIN;
        } else if ("sinh".equals(type)) {
            return FunctionType.SINH;
        } else if ("tan".equals(type)) {
            return FunctionType.TAN;
        } else if ("tanh".equals(type)) {
            return FunctionType.TANH;
        } else if ("sum".equals(type)) {
            return FunctionType.SUM;
        } else if ("hhv".equals(type)) {
            return FunctionType.HHV;
        } else if ("llv".equals(type)) {
            return FunctionType.LLV;
        } else if ("hhvbars".equals(type)) {
            return FunctionType.HHVBARS;
        } else if ("llvbars".equals(type)) {
            return FunctionType.LLVBARS;
        } else if ("atan2".equals(type)) {
            return FunctionType.ATAN2;
        } else if ("prec".equals(type)) {
            return FunctionType.PREC;
        } else if ("highestsince".equals(type)) {
            return FunctionType.HIGHESTSINCE;
        } else if ("lowestsince".equals(type)) {
            return FunctionType.LOWESTSINCE;
        } else if ("highestsincebars".equals(type)) {
            return FunctionType.HIGHESTSINCEBARS;
        } else if ("lowestsincebars".equals(type)) {
            return FunctionType.LOWESTSINCEBARS;
        } else {
            return FunctionType.REF;
        }
    }

    //private static BufferedReader buffR;

    /**
     * ************************************************************
     * This class will run the engine, and needs a file called config.dat
     * in the current directory. This file should contain two lines,
     * The first should be the absolute path name to the .cgt file, the second
     * should be the source file you wish to parse.
     *
     * @param grammar - grammar to be parsed.
     *                *************************************************************
     */
    public CIProgram parse(String grammar) {
        try {
            parser.openGrammar(grammar + "\n");
        } catch (Exception ex) {
            System.out.println("**PARSER ERROR**\n" + ex.toString());
            if (errorMessageArea != null) {
                Document doc = errorMessageArea.getDocument();
                Element e = doc.getDefaultRootElement();
                // Copy attribute Set
                AttributeSet attr = e.getAttributes().copyAttributes();
                try {
                    doc.insertString(doc.getLength(), "**PARSER ERROR**\n", attr);
                } catch (BadLocationException e1) {
                    e1.printStackTrace(); // remove later
                }
            }
        }

        boolean done = false;
        int response = -1;
//        Reduction program = null;

        while (!done) {
            try {
                response = parser.parse();
            } catch (ParserException parse) {
                System.out.println("**PARSER ERROR**\n" + parse.toString());
                if (errorMessageArea != null) {
                    Document doc = errorMessageArea.getDocument();
                    Element e = doc.getDefaultRootElement();
                    // Copy attribute Set
                    AttributeSet attr = e.getAttributes().copyAttributes();
                    try {
                        doc.insertString(doc.getLength(), "**PARSER ERROR**\n", attr);
                    } catch (BadLocationException e1) {
                        e1.printStackTrace(); // remove later
                    }
                }
            }

            switch (response) {
                case gpMsgTokenRead:
                    /* A token was read by the parser. The Token Object can be accessed
                      through the CurrentToken() property:  Parser.CurrentToken */
                    break;

                case gpMsgReduction:
                    /* This message is returned when a rule was reduced by the parse engine.
                      The CurrentReduction property is assigned a Reduction object
                      containing the rule and its related tokens. You can reassign this
                      property to your own customized class. If this is not the case,
                      this message can be ignored and the Reduction object will be used
                      to store the parse tree.  */

//                      switch(parser.currentReduction().getParentRule().getTableIndex())
//                      {
//                         case RuleConstants.RULE_PROGRAM:
//                            //<Program> ::= <Statements> <Plot Stmts>
//                            program = parser.currentReduction();
//                            break;
//                         case RuleConstants.RULE_PROGRAM2:
//                            //<Program> ::= <Plot Stmts>
//                            program = parser.currentReduction();
//                            break;
//                      }

                    // ************************************** log file
//                    System.out.println("gpMsgReduction");
                    Reduction myRed = parser.currentReduction();
//                    System.out.println(myRed.getParentRule().getText());
                    // ************************************** end log

                    break;

                case gpMsgAccept:
                    /* The program was accepted by the parsing engine */

                    // ************************************** log file
                    System.out.println("gpMsgAccept");
                    // ************************************** end log

                    done = true;
                    //if (program!=null){
                    return getProgramTree(parser.currentReduction());
                //}
                //break;

                case gpMsgLexicalError:
                    /* Place code here to handle a illegal or unrecognized token
                           To recover, pop the token from the stack: Parser.PopInputToken */

                    // ************************************** log file
                    System.out.println("gpMsgLexicalError");
                    // ************************************** end log

                    parser.popInputToken();

                    break;

                case gpMsgNotLoadedError:
                    /* Load the Compiled Grammar Table file first. */

                    // ************************************** log file
                    System.out.println("gpMsgNotLoadedError");
                    // ************************************** end log

                    done = true;

                    break;

                case gpMsgSyntaxError:
                    /* This is a syntax error: the source has produced a token that was
                           not expected by the LALR State Machine. The expected tokens are stored
                           into the Tokens() list. To recover, push one of the
                              expected tokens onto the parser's input queue (the first in this case):
                           You should limit the number of times this type of recovery can take
                           place. */

                    done = true;

                    Token theTok = parser.currentToken();
                    System.out.println("Token not expected: " + theTok.getData());
                    if (errorMessageArea != null) {
                        String existingText = errorMessageArea.getText();
                        errorMessageArea.setText(existingText + "Token not expected: " + theTok.getData() + "\n");
//                        Document doc = messageArea.getDocument();
//                        Element e = doc.getDefaultRootElement();
//                        // Copy attribute Set
//                        AttributeSet attr = e.getAttributes().copyAttributes();
//                        try {
//                            doc.insertString(doc.getLength(), "Token not expected: " + theTok.getData() + "\n", attr);
//                        } catch (BadLocationException e1) {
//                            e1.printStackTrace(); // remove later
//                        }
                    }

                    // ************************************** log file
//                    System.out.println("gpMsgSyntaxError");
                    // ************************************** end log

                    break;

                case gpMsgCommentError:
                    /* The end of the input was reached while reading a comment.
                             This is caused by a comment that was not terminated */

                    // ************************************** log file
                    System.out.println("gpMsgCommentError");
                    // ************************************** end log

                    done = true;

                    break;

                case gpMsgInternalError:
                    /* Something horrid happened inside the parser. You cannot recover */

                    // ************************************** log file
                    System.out.println("gpMsgInternalError");
                    // ************************************** end log

                    done = true;

                    break;
            }
        }

        try {
            parser.closeGrammar();
        } catch (Exception ex) {
            System.out.println("**PARSER ERROR**\n" + ex.toString());
            if (errorMessageArea != null) {
                Document doc = errorMessageArea.getDocument();
                Element e = doc.getDefaultRootElement();
                // Copy attribute Set
                AttributeSet attr = e.getAttributes().copyAttributes();
                try {
                    doc.insertString(doc.getLength(), "**PARSER ERROR**\n", attr);
                } catch (BadLocationException e1) {
                    e1.printStackTrace(); // remove later
                }
            }
            //System.exit(1);
        }
        return null;
    }
}