package com.isi.csvr.chart.customindicators;

import com.isi.csvr.chart.*;
import com.isi.csvr.chart.customindicators.statements.CIProgram;
import com.isi.csvr.chart.radarscreen.engine.RadarConstants;
import com.isi.csvr.chart.radarscreen.engine.RadarSettings;
import com.isi.csvr.chart.radarscreen.engine.StrategyData;
import com.isi.csvr.shared.Settings;
import com.mubasher.formulagen.TWCompiler;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.NodeList;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.transform.Result;
import javax.xml.transform.Source;
import javax.xml.transform.Transformer;
import javax.xml.transform.TransformerFactory;
import javax.xml.transform.dom.DOMSource;
import javax.xml.transform.stream.StreamResult;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathFactory;
import java.awt.*;
import java.io.*;
import java.lang.reflect.Constructor;
import java.util.ArrayList;
import java.util.Arrays;


public class IndicatorFactory {

    public static String Prefix = "__";
    public static String ParamPrefix = "_f_";
    //    public static String cixmlPath = SharedMethods.getExecutablePath() + "\\system\\cixml.dll";
    public static String cixmlPath = Settings.CHART_DATA_PATH + "/cixml.dll";
    public static String stxmlPath = Settings.CHART_DATA_PATH + "/stxml.dll";
    public static String csxmlPath = Settings.CHART_DATA_PATH + "/csxml.dll";
    public static String ciTmpltPath = Settings.CHART_DATA_PATH + "/cit.dll";
    public static String classPath = Settings.getAbsolutepath() + "CustomIndicators/";
//    public static String classPath = "C:/Program Files/Mubasher/Mubasher Pro/customindicators/";

    //#region indicator lists
    public static String[] generalIndicators = {
            CustomIndicatorInterface.getBollingerBandsClassName(),
            CustomIndicatorInterface.getChandeMomentOcslClassName(),
            CustomIndicatorInterface.getDEMAClassName(),
            CustomIndicatorInterface.getDetrendPriceOcslClassName(),
            CustomIndicatorInterface.getEnvelopesClassName(),
            CustomIndicatorInterface.getForcastOcslClassName(),
            CustomIndicatorInterface.getLinearRegressionClassName(),
            CustomIndicatorInterface.getMACDClassName(),
            CustomIndicatorInterface.getMomentumClassName(),
            CustomIndicatorInterface.getMovingAverageClassName(),
            CustomIndicatorInterface.getOBVClassName(),
            CustomIndicatorInterface.getPerformance(),
            CustomIndicatorInterface.getPriceOcslClassName(),
            CustomIndicatorInterface.getPriceROCClassName(),
            CustomIndicatorInterface.getRelativeMomentumClassName(),
            CustomIndicatorInterface.getRelativeStrengthClassName(),
            CustomIndicatorInterface.getStandardDeviationClassName(),
            CustomIndicatorInterface.getStandardErrorClassName(),
            CustomIndicatorInterface.getTEMAClassName(),
            CustomIndicatorInterface.getTSFClassName(),
            CustomIndicatorInterface.getTRIXClassName(),
            CustomIndicatorInterface.getVHFClassName(),
            CustomIndicatorInterface.getWildersSmoothingClassName(),
            CustomIndicatorInterface.getZigZagClassName(),
            CustomIndicatorInterface.getCopockCurveClassName(),
            CustomIndicatorInterface.getZigZagSlopeClassName()
    };
    //#endregion

    //#region Code Generation

    public static String GenerateCode(IndicatorConfigInfo info, CIProgram custIndProgram) throws Exception {
        try {
            File file = new File(ciTmpltPath);
            FileReader fr = new FileReader(file);
            BufferedReader br = new BufferedReader(fr);
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line).append("\n");
            }
            br.close();
            fr.close();
            String template = sb.toString();
            template = template.replace("<NameSpace>", info.NameSpace);
            template = template.replace("<ClassName>", info.ClassName);
            template = template.replace("<IndicatorBase>", CustomIndicatorInterface.getIndicatorBaseClassName());
            template = template.replace("<ShortName>", info.ShortName);
            template = template.replace("<LongName>", info.LongName);
            template = template.replace("<WindowPanel>", CustomIndicatorInterface.getWindowPanelClassName());
            template = template.replace("<ChartProperties>", CustomIndicatorInterface.getChartPropertiesClassName());

            template = template.replace("<HasItsOwnScale>", info.HasItsOwnScale);
            template = template.replace("<Color>", (info.Color.getRed() + "," + info.Color.getGreen() + "," + info.Color.getBlue()));
            template = template.replace("<WarningColor>", (info.WarningColor.getRed() + "," + info.WarningColor.getGreen() + "," + info.WarningColor.getBlue()));
            template = template.replace("<UseSameColor>", Boolean.toString(info.UseSameColor).toLowerCase());
            template = template.replace("<ChartStyle>", Byte.toString(info.ChartStyle));
            template = template.replace("<PenStyle>", Byte.toString(info.PenStyle));
            template = template.replace("<PenWidth>", Float.toString(info.PenWidth));
            template = template.replace("<ChartRecord>", CustomIndicatorInterface.getChartRecordClassName());

            return custIndProgram.GenerateCode(template);
        } catch (Exception ex) {
            // handle
            ex.printStackTrace();
            throw ex;
            //return null;
        }
    }
    //#endregion

    public static String getTypeString(ParamType type) {
        switch (type) {
            case DOUBLE:
                return "double";
            case INT:
                return "int";
            case BOOL:
                return "boolean";
            case DATA_ARRAY:
                return "OhlcPriority";
            case MA_METHOD:
                return "MAMethod"; //MACalculationMethod
            case CALC_METHOD:
                return "CalcMethod"; //CalculationMethod
            case CC_PERIOD:
                return "CopCurvePeriod"; //Coppock Curve Period
            case UNKNOWN:
            default:
                return "0bject";
        }
    }

    public static String getDeafaultValue(ParamType type) {
        switch (type) {
            case DOUBLE:
                return "1.0";
            case INT:
                return "14";
            case BOOL:
                return "false";
            case DATA_ARRAY:
                return "OhlcPriority.CLOSE";
            case MA_METHOD:
                return "MAMethod.SIMPLE";
            case CALC_METHOD:
                return "CalcMethod.PERCENT";
            case CC_PERIOD:
                return "CopCurvePeriod.MONTHLY";
            case UNKNOWN:
            default:
                return "null";
        }
    }

    public static void getIndicatorAttributes(String indicator, boolean[] IsSourceNeededRef, int[] auxStepCountRef) {
        IsSourceNeededRef[0] = false;
        for (int i = 0; i < IndicatorFactory.generalIndicators.length; i++) {
            if (IndicatorFactory.generalIndicators[i].equals(getExactIndicatorName(indicator))) {
                IsSourceNeededRef[0] = true;
                break;
            }
        }
        try {
            auxStepCountRef[0] = (Integer) Class.forName(/*"com.isi.csvr.chart." +*/
                    getExactIndicatorName(indicator)).getMethod("getAuxStepCount").invoke(null);
        } catch (Exception ex) {
            auxStepCountRef[0] = 0;
        }
    }


    public static void buildAssembly(IndicatorParser parser) {
        IndicatorConfigInfo[] arrInfo = getIndicatorInfo();
        if (arrInfo != null) {
            for (int i = 0; i < arrInfo.length; i++) {
                File calssFile = new File(IndicatorFactory.classPath + arrInfo[i].ClassName + ".ind");
                if (!calssFile.exists()) {
                    try {
                        CIProgram program = parser.parse(arrInfo[i].Grammer);
                        String code = GenerateCode(arrInfo[i], program);
                        compileCode(code, arrInfo[i].ClassName);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                if (IndicatorBuilder.getInstance().getCiListeners() != null) {
                    Class indClass = null;
                    try {
                        indClass = Class.forName(arrInfo[i].ClassName, false, new IndicatorLoader());
                        Constructor constructor = indClass.getConstructor(ArrayList.class, String.class, Color.class, WindowPanel.class);
                        ChartProperties indicator = (ChartProperties) constructor.newInstance(new ArrayList(), "", Color.red, new WindowPanel());
                        indicator = null;
                    } catch (Exception e) {
                        // Generate code, compile and load from the begining, if class loading fails
                        try {
                            CIProgram program = parser.parse(arrInfo[i].Grammer);
                            String code = GenerateCode(arrInfo[i], program);
                            compileCode(code, arrInfo[i].ClassName);

                            indClass = Class.forName(arrInfo[i].ClassName, false, new IndicatorLoader());
                        } catch (Exception ex) {
                            ex.printStackTrace();
                        }
                    }
                    for (CustomIndicatorListener cil : IndicatorBuilder.getInstance().getCiListeners()) {
                        cil.customIndicatorCreated(arrInfo[i].LongName, indClass, arrInfo[i].Grammer);
                        IndicatorDetailStore.CustomIndicators.add(new IndicatorMenuObject(indClass, arrInfo[i].LongName));
                    }
                }
                deleteSource(arrInfo[i].ClassName);
            }
        }
    }

    public static void deleteOldVersionFiles() {

        String versionNumber = Settings.TW_VERSION_NUMBER.replaceAll("\\.", "");
        File dir = new File(IndicatorFactory.classPath);

        String[] files = dir.list();

        boolean isNewVersion = false;
        if (files != null) {
            for (int k = 0; k < files.length; k++) {
                String name = files[k];
                if (name.contains("CI_") && !name.contains("_" + versionNumber)) {
                    isNewVersion = true;
                    break;
                }
            }
        }

        //isNewVersion = true;
        if (isNewVersion) {

            for (int k = 0; k < files.length; k++) {
                if (files[k].contains("CI_")) {
                    File file = new File(IndicatorFactory.classPath + files[k]);
                    boolean result = file.delete();
                }
            }
        }
    }


    public static void buildStrategiesAndPatterns(IndicatorParser parser) {

        deleteOldVersionFiles();

        IndicatorConfigInfo[] arrInfo = getStrategyInfo();

        if (arrInfo != null) {
            for (int i = 0; i < arrInfo.length; i++) {
                String fileName = IndicatorFactory.classPath + arrInfo[i].ClassName + ".ind";
                File calssFile = new File(fileName);

                if (!calssFile.exists()) {
                    try {
                        CIProgram program = parser.parse(arrInfo[i].Grammer);
                        String code = GenerateCode(arrInfo[i], program);
                        compileCode(code, arrInfo[i].ClassName);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                if (IndicatorBuilder.getInstance().getCiListeners() != null) {
                    Class indClass = null;
                    try {
                        indClass = Class.forName(arrInfo[i].ClassName, false, new IndicatorLoader());
                        Constructor constructor = indClass.getConstructor(ArrayList.class, String.class, Color.class, WindowPanel.class);
                        ChartProperties indicator = (ChartProperties) constructor.newInstance(new ArrayList(), "", Color.red, new WindowPanel());
                        indicator = null;
                    } catch (Exception e) {
                        // Generate code, compile and load from the begining, if class loading fails
                        try {
                            CIProgram program = parser.parse(arrInfo[i].Grammer);
                            String code = GenerateCode(arrInfo[i], program);
                            compileCode(code, arrInfo[i].ClassName);

                            indClass = Class.forName(arrInfo[i].ClassName, false, new IndicatorLoader());
                        } catch (Exception ex) {
//                            ex.printStackTrace();
                        }
                    }
                    for (CustomIndicatorListener cil : IndicatorBuilder.getInstance().getCiListeners()) {
                        cil.strategyCreated(arrInfo[i].LongName, indClass);
                        IndicatorDetailStore.alStrategies.add(new IndicatorMenuObject(indClass, arrInfo[i].LongName));
                    }
                }
                deleteSource(arrInfo[i].ClassName);
            }
        }
        arrInfo = getPatternInfo();
        if (arrInfo != null) {
            for (int i = 0; i < arrInfo.length; i++) {

                File calssFile = new File(IndicatorFactory.classPath + arrInfo[i].ClassName + ".ind");

                if (!calssFile.exists()) {
                    try {
                        CIProgram program = parser.parse(arrInfo[i].Grammer);
                        String code = GenerateCode(arrInfo[i], program);
                        compileCode(code, arrInfo[i].ClassName);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                if (IndicatorBuilder.getInstance().getCiListeners() != null) {
                    Class indClass = null;
                    try {
                        indClass = Class.forName(arrInfo[i].ClassName, false, new IndicatorLoader());
                        Constructor constructor = indClass.getConstructor(ArrayList.class, String.class, Color.class, WindowPanel.class);
                        ChartProperties indicator = (ChartProperties) constructor.newInstance(new ArrayList(), "", Color.red, new WindowPanel());
                        indicator = null;
                    } catch (Exception e) {
                        // Generate code, compile and load from the begining, if class loading fails
                        try {
                            CIProgram program = parser.parse(arrInfo[i].Grammer);
                            String code = GenerateCode(arrInfo[i], program);
                            compileCode(code, arrInfo[i].ClassName);

                            indClass = Class.forName(arrInfo[i].ClassName, false, new IndicatorLoader());
                        } catch (Exception ex) {
//                            ex.printStackTrace();
                        }
                    }
                    for (CustomIndicatorListener cil : IndicatorBuilder.getInstance().getCiListeners()) {
                        cil.patternCreated(arrInfo[i].LongName, indClass);
                        IndicatorDetailStore.alPatterns.add(new IndicatorMenuObject(indClass, arrInfo[i].LongName));
                    }
                }
                deleteSource(arrInfo[i].ClassName);
            }
        }
    }

    public static void rebuildAssembly() {
        IndicatorParser parser = new IndicatorParser(Settings.CHART_DATA_PATH + "/CustomIndicator.cgt");
        buildAssembly(parser);
        buildStrategiesAndPatterns(parser);
        //buildRadarScreenStrategies(parser);

    }

    public static int compileCode(String source, String className) {
        int result = -1;
        try {
            result = TWCompiler.compile(IndicatorFactory.getSourcePath(className, source), IndicatorFactory.classPath);

            if (result == 0) {
                File classFile = new File(IndicatorFactory.classPath + className + ".class");
                File fileRenamed = new File(IndicatorFactory.classPath + className + ".ind");
                if (fileRenamed.exists()) fileRenamed.delete();
                classFile.renameTo(fileRenamed);
                Encoder.encode(fileRenamed);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
        return result;
    }

    private static String getSourcePath(String className, String source) {
        String srcPath = (IndicatorFactory.classPath) + className + ".java";
        try {
            File file = new File(srcPath);
            if (file.exists()) file.delete();
            file.createNewFile();
            BufferedWriter bwriter = new BufferedWriter(new FileWriter(file));
            bwriter.write(source);
            bwriter.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return srcPath;
    }

    private static void deleteSource(String className) { // high priority
        String srcPath = (IndicatorFactory.classPath) + className + ".java";
        File file = new File(srcPath);
        if (file.exists()) file.delete();
    }

    public static IndicatorConfigInfo getIndicatorConfigInfo(String name) {
        File file = new File(cixmlPath);
        if (file.exists()) {
            try {
                DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document document = builder.parse(file);
                XPath xpath = XPathFactory.newInstance().newXPath();

                String xpathExp = "Indicators/Indicator[Name=\"" + name.replace("\"", "\"\"") + "\"]";
                NodeList custIndicators = (NodeList) xpath.evaluate(xpathExp, document, XPathConstants.NODESET);
                if ((custIndicators != null) && (custIndicators.getLength() > 0)) {
                    String expression = xpathExp + "/";
                    String grammer = Evaluate(xpath, document, expression + "Code");
                    //String name = Evaluate(xpath, document, expression+"Name");
                    String className = Evaluate(xpath, document, expression + "ClassName");
                    if ("".equals(className)) className = name.replace(" ", "_");
                    String sname = Evaluate(xpath, document, expression + "ShortName");
                    String ownScale = Evaluate(xpath, document, expression + "HasItsOwnScale");
                    String strStrat = Evaluate(xpath, document, expression + "IsStrategy");
                    boolean isStrat = !"".equals(strStrat) && Boolean.parseBoolean(strStrat);
                    String[] sa = Evaluate(xpath, document, expression + "Color").split(",");
                    Color color = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                    sa = Evaluate(xpath, document, expression + "WarningColor").split(",");
                    Color warningColor = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                    byte ctStlye = Byte.parseByte(Evaluate(xpath, document, expression + "ChartStyle"));
                    boolean useSameColor = Boolean.parseBoolean(Evaluate(xpath, document, expression + "UseSameColor"));
                    byte penStyle = Byte.parseByte(Evaluate(xpath, document, expression + "PenStyle"));
                    float penWidth = Float.parseFloat(Evaluate(xpath, document, expression + "PenWidth"));
                    return new IndicatorConfigInfo("", grammer, className,
                            ownScale, sname, name, isStrat, color, warningColor, ctStlye, useSameColor, penStyle, penWidth);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        file = new File(stxmlPath);
        if (file.exists()) {
            try {
                DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document document = builder.parse(file);
                XPath xpath = XPathFactory.newInstance().newXPath();

                String xpathExp = "Strategies/Indicator[Name=\"" + name.replace("\"", "\"\"") + "\"]";
                NodeList custIndicators = (NodeList) xpath.evaluate(xpathExp, document, XPathConstants.NODESET);
                if ((custIndicators != null) && (custIndicators.getLength() > 0)) {
                    String expression = xpathExp + "/";
                    String grammer = Evaluate(xpath, document, expression + "Code");
                    //String name = Evaluate(xpath, document, expression+"Name");
                    String className = Evaluate(xpath, document, expression + "ClassName");
                    if ("".equals(className)) className = name.replace(" ", "_");
                    String sname = Evaluate(xpath, document, expression + "ShortName");
                    String ownScale = Evaluate(xpath, document, expression + "HasItsOwnScale");
                    String strStrat = Evaluate(xpath, document, expression + "IsStrategy");
                    boolean isStrat = !"".equals(strStrat) && Boolean.parseBoolean(strStrat);
                    String[] sa = Evaluate(xpath, document, expression + "Color").split(",");
                    Color color = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                    sa = Evaluate(xpath, document, expression + "WarningColor").split(",");
                    Color warningColor = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                    byte ctStlye = Byte.parseByte(Evaluate(xpath, document, expression + "ChartStyle"));
                    boolean useSameColor = Boolean.parseBoolean(Evaluate(xpath, document, expression + "UseSameColor"));
                    byte penStyle = Byte.parseByte(Evaluate(xpath, document, expression + "PenStyle"));
                    float penWidth = Float.parseFloat(Evaluate(xpath, document, expression + "PenWidth"));
                    return new IndicatorConfigInfo("", grammer, className,
                            ownScale, sname, name, isStrat, color, warningColor, ctStlye, useSameColor, penStyle, penWidth);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        file = new File(csxmlPath);
        if (file.exists()) {
            try {
                DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document document = builder.parse(file);
                XPath xpath = XPathFactory.newInstance().newXPath();

                String xpathExp = "CandlestickPatterns/Indicator[Name=\"" + name.replace("\"", "\"\"") + "\"]";
                NodeList custIndicators = (NodeList) xpath.evaluate(xpathExp, document, XPathConstants.NODESET);
                if ((custIndicators != null) && (custIndicators.getLength() > 0)) {
                    String expression = xpathExp + "/";
                    String grammer = Evaluate(xpath, document, expression + "Code");
                    //String name = Evaluate(xpath, document, expression+"Name");
                    String className = Evaluate(xpath, document, expression + "ClassName");
                    if ("".equals(className)) className = name.replace(" ", "_");
                    String sname = Evaluate(xpath, document, expression + "ShortName");
                    String ownScale = Evaluate(xpath, document, expression + "HasItsOwnScale");
                    String strStrat = Evaluate(xpath, document, expression + "IsStrategy");
                    boolean isStrat = !"".equals(strStrat) && Boolean.parseBoolean(strStrat);
                    String[] sa = Evaluate(xpath, document, expression + "Color").split(",");
                    Color color = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                    sa = Evaluate(xpath, document, expression + "WarningColor").split(",");
                    Color warningColor = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                    byte ctStlye = Byte.parseByte(Evaluate(xpath, document, expression + "ChartStyle"));
                    boolean useSameColor = Boolean.parseBoolean(Evaluate(xpath, document, expression + "UseSameColor"));
                    byte penStyle = Byte.parseByte(Evaluate(xpath, document, expression + "PenStyle"));
                    float penWidth = Float.parseFloat(Evaluate(xpath, document, expression + "PenWidth"));
                    return new IndicatorConfigInfo("", grammer, className,
                            ownScale, sname, name, isStrat, color, warningColor, ctStlye, useSameColor, penStyle, penWidth);
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return null;
    }

    private static IndicatorConfigInfo[] getIndicatorInfo() {
        File file = new File(cixmlPath);
        if (!file.exists()) return null;
        IndicatorConfigInfo[] infoArr = null;
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(file);
            XPath xpath = XPathFactory.newInstance().newXPath();

            String xpathExp = "Indicators/Indicator";
            NodeList custIndicators = (NodeList) xpath.evaluate(xpathExp, document, XPathConstants.NODESET);
            if ((custIndicators != null) && (custIndicators.getLength() > 0)) {
                infoArr = new IndicatorConfigInfo[custIndicators.getLength()];
                for (int i = 1; i <= custIndicators.getLength(); i++) {
                    //Node indicator = custIndicators.item(i);
                    String expression = xpathExp + "[" + i + "]/";
                    String grammer = Evaluate(xpath, document, expression + "Code");
                    String name = Evaluate(xpath, document, expression + "Name");
                    String className = Evaluate(xpath, document, expression + "ClassName");
                    if ("".equals(className)) className = name.replace(" ", "_");
                    String sname = Evaluate(xpath, document, expression + "ShortName");
                    String ownScale = Evaluate(xpath, document, expression + "HasItsOwnScale");
                    String strStrat = Evaluate(xpath, document, expression + "IsStrategy");
                    boolean isStrat = !"".equals(strStrat) && Boolean.parseBoolean(strStrat);
                    String[] sa = Evaluate(xpath, document, expression + "Color").split(",");
                    Color color = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                    sa = Evaluate(xpath, document, expression + "WarningColor").split(",");
                    Color warningColor = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                    byte ctStlye = Byte.parseByte(Evaluate(xpath, document, expression + "ChartStyle"));
                    boolean useSameColor = Boolean.parseBoolean(Evaluate(xpath, document, expression + "UseSameColor"));
                    byte penStyle = Byte.parseByte(Evaluate(xpath, document, expression + "PenStyle"));
                    float penWidth = Float.parseFloat(Evaluate(xpath, document, expression + "PenWidth"));
                    infoArr[i - 1] = new IndicatorConfigInfo("", grammer, className,
                            ownScale, sname, name, isStrat, color, warningColor, ctStlye, useSameColor, penStyle, penWidth);
                }
            }
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return infoArr;
    }

    private static String getVersionNumber() {
        String[] str = Settings.TW_VERSION_NUMBER.split("\\.");
        String res = "";
        for (int i = 0; i < str.length; i++) {
            res += str[i];
        }

        return res;
    }

    private static IndicatorConfigInfo[] getStrategyInfo() {
        IndicatorConfigInfo[] infoArr = null;
        File file = new File(stxmlPath);
        if (file.exists()) {
            try {
                DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document document = builder.parse(file);
                XPath xpath = XPathFactory.newInstance().newXPath();

                String xpathExp = "Strategies/Indicator";
                NodeList custIndicators = (NodeList) xpath.evaluate(xpathExp, document, XPathConstants.NODESET);
                if ((custIndicators != null) && (custIndicators.getLength() > 0)) {
                    infoArr = new IndicatorConfigInfo[custIndicators.getLength()];
                    for (int i = 1; i <= custIndicators.getLength(); i++) {
                        //Node indicator = custIndicators.item(i);
                        String expression = xpathExp + "[" + i + "]/";
                        String grammer = Evaluate(xpath, document, expression + "Code");
                        String name = Evaluate(xpath, document, expression + "Name");
                        String className = Evaluate(xpath, document, expression + "ClassName") + "_" + Settings.TW_VERSION_NUMBER.replaceAll("\\.", "");
                        if ("".equals(className)) className = name.replace(" ", "_");
                        String sname = Evaluate(xpath, document, expression + "ShortName");
                        String ownScale = Evaluate(xpath, document, expression + "HasItsOwnScale");
                        String strStrat = Evaluate(xpath, document, expression + "IsStrategy");
                        boolean isStrat = !"".equals(strStrat) && Boolean.parseBoolean(strStrat);
                        String[] sa = Evaluate(xpath, document, expression + "Color").split(",");
                        Color color = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                        sa = Evaluate(xpath, document, expression + "WarningColor").split(",");
                        Color warningColor = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                        byte ctStlye = Byte.parseByte(Evaluate(xpath, document, expression + "ChartStyle"));
                        boolean useSameColor = Boolean.parseBoolean(Evaluate(xpath, document, expression + "UseSameColor"));
                        byte penStyle = Byte.parseByte(Evaluate(xpath, document, expression + "PenStyle"));
                        float penWidth = Float.parseFloat(Evaluate(xpath, document, expression + "PenWidth"));
                        infoArr[i - 1] = new IndicatorConfigInfo("", grammer, className,
                                ownScale, sname, name, isStrat, color, warningColor, ctStlye, useSameColor, penStyle, penWidth);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        Arrays.sort(infoArr);
        return infoArr;
    }

    private static IndicatorConfigInfo[] getPatternInfo() {
        IndicatorConfigInfo[] infoArr = null;
        File file = new File(csxmlPath);
        if (file.exists()) {
            try {
                DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
                Document document = builder.parse(file);
                XPath xpath = XPathFactory.newInstance().newXPath();

                String xpathExp = "CandlestickPatterns/Indicator";
                NodeList custIndicators = (NodeList) xpath.evaluate(xpathExp, document, XPathConstants.NODESET);
                if ((custIndicators != null) && (custIndicators.getLength() > 0)) {
                    infoArr = new IndicatorConfigInfo[custIndicators.getLength()];
                    for (int i = 1; i <= custIndicators.getLength(); i++) {
                        //Node indicator = custIndicators.item(i);
                        String expression = xpathExp + "[" + i + "]/";
                        String grammer = Evaluate(xpath, document, expression + "Code");
                        String name = Evaluate(xpath, document, expression + "Name");
                        String className = Evaluate(xpath, document, expression + "ClassName") + "_" + Settings.TW_VERSION_NUMBER.replaceAll("\\.", "");
                        if ("".equals(className)) className = name.replace(" ", "_");
                        String sname = Evaluate(xpath, document, expression + "ShortName");
                        String ownScale = Evaluate(xpath, document, expression + "HasItsOwnScale");
                        String strStrat = Evaluate(xpath, document, expression + "IsStrategy");
                        boolean isStrat = !"".equals(strStrat) && Boolean.parseBoolean(strStrat);
                        String[] sa = Evaluate(xpath, document, expression + "Color").split(",");
                        Color color = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                        sa = Evaluate(xpath, document, expression + "WarningColor").split(",");
                        Color warningColor = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                        byte ctStlye = Byte.parseByte(Evaluate(xpath, document, expression + "ChartStyle"));
                        boolean useSameColor = Boolean.parseBoolean(Evaluate(xpath, document, expression + "UseSameColor"));
                        byte penStyle = Byte.parseByte(Evaluate(xpath, document, expression + "PenStyle"));
                        float penWidth = Float.parseFloat(Evaluate(xpath, document, expression + "PenWidth"));
                        infoArr[i - 1] = new IndicatorConfigInfo("", grammer, className,
                                ownScale, sname, name, isStrat, color, warningColor, ctStlye, useSameColor, penStyle, penWidth);
                    }
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        Arrays.sort(infoArr);
        return infoArr;
    }


    private static String Evaluate(XPath xpath, Document document, String expression) {
        try {
            return xpath.evaluate(expression, document, XPathConstants.STRING).toString();
        } catch (Exception ex) {
            ex.printStackTrace();
            return "";
        }
    }

    public static String[] getIndicatorNames() {
        File file = new File(cixmlPath);
        if (!file.exists()) return null;
        String[] nameArr = new String[0];
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(file);
            XPath xpath = XPathFactory.newInstance().newXPath();

            String xpathExp = "Indicators/Indicator";
            NodeList custIndicators = (NodeList) xpath.evaluate(xpathExp, document, XPathConstants.NODESET);
            if ((custIndicators != null) && (custIndicators.getLength() > 0)) {
                nameArr = new String[custIndicators.getLength()];
                for (int i = 1; i <= custIndicators.getLength(); i++) {
                    String expression = xpathExp + "[" + i + "]/";
                    nameArr[i - 1] = Evaluate(xpath, document, expression + "Name");
                }
            }
        } catch (Exception ex) {
        }
        return nameArr;
    }

    public static void createIndicator(IndicatorConfigInfo info) {
        try {
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document;

            File file = new File(cixmlPath);
            if (!file.exists()) {
                document = builder.newDocument();
                document.appendChild(document.createElement("Indicators"));
            } else {
                document = builder.parse(file);
            }
            Element root = document.getDocumentElement();

            Element indicatorNode = document.createElement("Indicator");
            root.appendChild(indicatorNode);

            Element propertyNode;
            //Name
            propertyNode = document.createElement("Name");
            propertyNode.setTextContent(info.LongName);
            indicatorNode.appendChild(propertyNode);
            //ShortName
            propertyNode = document.createElement("ShortName");
            propertyNode.setTextContent(info.ShortName);
            indicatorNode.appendChild(propertyNode);
            //Name
            propertyNode = document.createElement("ClassName");
            propertyNode.setTextContent(info.ClassName);
            indicatorNode.appendChild(propertyNode);
            //HasItsOwnScale
            propertyNode = document.createElement("HasItsOwnScale");
            propertyNode.setTextContent(info.HasItsOwnScale);
            indicatorNode.appendChild(propertyNode);
            //IsStrategy
            propertyNode = document.createElement("IsStrategy");
            propertyNode.setTextContent(Boolean.toString(info.IsStrategy));
            indicatorNode.appendChild(propertyNode);
            //Color
            propertyNode = document.createElement("Color");
            propertyNode.setTextContent(info.Color.getRed() + "," + info.Color.getGreen() + "," + info.Color.getBlue());
            indicatorNode.appendChild(propertyNode);
            //WarningColor
            propertyNode = document.createElement("WarningColor");
            propertyNode.setTextContent(info.WarningColor.getRed() + "," + info.WarningColor.getGreen() + "," + info.WarningColor.getBlue());
            indicatorNode.appendChild(propertyNode);
            //ChartStyle
            propertyNode = document.createElement("ChartStyle");
            propertyNode.setTextContent(Byte.toString(info.ChartStyle));
            indicatorNode.appendChild(propertyNode);
            //UseSameColor
            propertyNode = document.createElement("UseSameColor");
            propertyNode.setTextContent(Boolean.toString(info.UseSameColor));
            indicatorNode.appendChild(propertyNode);
            //PenStyle
            propertyNode = document.createElement("PenStyle");
            propertyNode.setTextContent(Byte.toString(info.PenStyle));
            indicatorNode.appendChild(propertyNode);
            //PenWidth
            propertyNode = document.createElement("PenWidth");
            propertyNode.setTextContent(Float.toString(info.PenWidth));
            indicatorNode.appendChild(propertyNode);
            //Code (Grammer)
            propertyNode = document.createElement("Code");
            propertyNode.appendChild(document.createCDATASection(info.Grammer));
            //propertyNode.setTextContent("<![CDATA[" + info.Grammer + "]]>");
            indicatorNode.appendChild(propertyNode);

            TransformerFactory tranFactory = TransformerFactory.newInstance();
            Transformer aTransformer = tranFactory.newTransformer();

            Source src = new DOMSource(document);
            Result dest = new StreamResult(file);
            aTransformer.transform(src, dest);
        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }

    public static void alterIndicator(IndicatorConfigInfo info) {
        removeIndicator(info.LongName);
        createIndicator(info);
    }

    public static boolean removeIndicator(String name) {
        try {
            File file = new File(cixmlPath);
            if (!file.exists()) {
                return true;
            }
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(file);

            XPath xpath = XPathFactory.newInstance().newXPath();
            String xpathExp = "Indicators/Indicator[Name=\"" + name + "\"]";
            NodeList resultNodes = (NodeList) xpath.evaluate(xpathExp, document, XPathConstants.NODESET);
            if ((resultNodes != null) && (resultNodes.getLength() > 0)) {
                document.getDocumentElement().removeChild(resultNodes.item(0));
            }

            TransformerFactory tranFactory = TransformerFactory.newInstance();
            Transformer aTransformer = tranFactory.newTransformer();

            Source src = new DOMSource(document);
            Result dest = new StreamResult(file);
            aTransformer.transform(src, dest);
            return true;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return false;
    }

    public static IndicatorConfigInfo getIndicatoInfoForName(String name) {
        try {
            File file = new File(cixmlPath);
            if (!file.exists()) {
                return null;
            }
            DocumentBuilder builder = DocumentBuilderFactory.newInstance().newDocumentBuilder();
            Document document = builder.parse(file);

            XPath xpath = XPathFactory.newInstance().newXPath();
            String xpathExp = "Indicators/Indicator[Name=\"" + name + "\"]";
            NodeList resultNodes = (NodeList) xpath.evaluate(xpathExp, document, XPathConstants.NODESET);
            IndicatorConfigInfo ici = null;
            if ((resultNodes != null) && (resultNodes.getLength() > 0)) {
                String expression = xpathExp + "/";
                String grammer = Evaluate(xpath, document, expression + "Code");
                String className = Evaluate(xpath, document, expression + "ClassName");
                if ("".equals(className)) className = name.replace(" ", "_");
                String sname = Evaluate(xpath, document, expression + "ShortName");
                String ownScale = Evaluate(xpath, document, expression + "HasItsOwnScale");
                String strStrat = Evaluate(xpath, document, expression + "IsStrategy");
                boolean isStrat = !"".equals(strStrat) && Boolean.parseBoolean(strStrat);
                String[] sa = Evaluate(xpath, document, expression + "Color").split(",");
                Color color = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                sa = Evaluate(xpath, document, expression + "WarningColor").split(",");
                Color warningColor = new Color(Integer.parseInt(sa[0]), Integer.parseInt(sa[1]), Integer.parseInt(sa[2]));
                byte ctStlye = Byte.parseByte(Evaluate(xpath, document, expression + "ChartStyle"));
                boolean useSameColor = Boolean.parseBoolean(Evaluate(xpath, document, expression + "UseSameColor"));
                byte penStyle = Byte.parseByte(Evaluate(xpath, document, expression + "PenStyle"));
                float penWidth = Float.parseFloat(Evaluate(xpath, document, expression + "PenWidth"));
                ici = new IndicatorConfigInfo("", grammer, className,
                        ownScale, sname, name, isStrat, color, warningColor, ctStlye, useSameColor, penStyle, penWidth);
            }

            return ici;
        } catch (Exception ex) {
            ex.printStackTrace();
        }
        return null;
    }

    private static IndicatorConfigInfo[] getRadarScreenStrategyInfo() {
        IndicatorConfigInfo[] infoArr = null;
        File file = new File(RadarConstants.RADAR_SETTINGS_FILE_PATH);
        if (file.exists()) {
            try {

                ArrayList<StrategyData> data = RadarSettings.getStrategyData();
                infoArr = new IndicatorConfigInfo[data.size() * 2];
                int count = 0;
                for (int i = 0; i < data.size(); i++) {

                    StrategyData dataItem = data.get(i);
                    //String grammer = dataItem.getGrammar();
                    String sname = dataItem.getShortName();
                    String name = dataItem.getName();
                    String buyclassName = dataItem.getBuyClassName();
                    String sellclassName = dataItem.getSellClassName();

                    String buyGrammar = dataItem.getBuyGrammar();
                    //System.out.println("++++++++++ buy grammar ++++++++++\n "+buyGrammar);
                    String sellGrammar = dataItem.getSellGrammar();
                    //System.out.println("++++++++++ sell grammar +++++++++\n "+sellGrammar);

                    byte chartStyle = 0;
                    byte penStyle = 0;
                    infoArr[count] = new IndicatorConfigInfo("", buyGrammar, buyclassName, "true", sname, name, true, Color.BLACK, Color.BLACK, chartStyle, true, penStyle, 1f);
                    infoArr[count + 1] = new IndicatorConfigInfo("", sellGrammar, sellclassName, "true", sname, name, true, Color.BLACK, Color.BLACK, chartStyle, true, penStyle, 1f);
                    count = count + 2;
                }
            } catch (Exception ex) {
                ex.printStackTrace();
            }
        }
        return infoArr;
    }

    public static void buildRadarScreenStrategies(IndicatorParser parser) {
        IndicatorConfigInfo[] arrInfo = getRadarScreenStrategyInfo();
        if (arrInfo != null) {
            for (int i = 0; i < arrInfo.length; i++) {
                File calssFile = new File(IndicatorFactory.classPath + arrInfo[i].ClassName + ".ind");
                //File calssFile = new File(RadarConstants.RADAR_CLASS_PATH + arrInfo[i].ClassName + ".ind");
                if (!calssFile.exists()) {
                    try {
                        CIProgram program = parser.parse(arrInfo[i].Grammer);
                        String code = GenerateCode(arrInfo[i], program);
                        compileCode(code, arrInfo[i].ClassName);
                    } catch (Exception ex) {
                        ex.printStackTrace();
                    }
                }
                deleteSource(arrInfo[i].ClassName);
            }
        }
    }

    public static String getExactIndicatorName(String indName) {
        if (indName.equals("BollingerBands")) {
            return CustomIndicatorInterface.getBollingerBandsClassName();
        } else if (indName.equals("ChandeMomentumOscillator")) {
            return CustomIndicatorInterface.getChandeMomentOcslClassName();
        } else if (indName.equals("DEMA")) {
            return CustomIndicatorInterface.getDEMAClassName();
        } else if (indName.equals("DetrendedPriceOscillator")) {
            return CustomIndicatorInterface.getDetrendPriceOcslClassName();
        } else if (indName.equals("Envelopes")) {
            return CustomIndicatorInterface.getEnvelopesClassName();
        } else if (indName.equals("ForecastOscillator")) {
            return CustomIndicatorInterface.getForcastOcslClassName();
        } else if (indName.equals("LinearRegression")) {
            return CustomIndicatorInterface.getLinearRegressionClassName();
        } else if (indName.equals("MACD")) {
            return CustomIndicatorInterface.getMACDClassName();
        } else if (indName.equals("Momentum")) {
            return CustomIndicatorInterface.getMomentumClassName();
        } else if (indName.equals("MovingAverage")) {
            return CustomIndicatorInterface.getMovingAverageClassName();
        } else if (indName.equals("OnBalanceVolume")) {
            return CustomIndicatorInterface.getOBVClassName();
        } else if (indName.equals("ParabolicSAR")) {
            return CustomIndicatorInterface.getParabolicSARClassName();
        } else if (indName.equals("Performance")) {
            return CustomIndicatorInterface.getPerformance();
        } else if (indName.equals("PriceOscillator")) {
            return CustomIndicatorInterface.getPriceOcslClassName();
        } else if (indName.equals("PriceROC")) {
            return CustomIndicatorInterface.getPriceROCClassName();
        } else if (indName.equals("RelativeMomentumIndex")) {
            return CustomIndicatorInterface.getRelativeMomentumClassName();
        } else if (indName.equals("RelativeStrengthIndex")) {
            return CustomIndicatorInterface.getRelativeStrengthClassName();
        } else if (indName.equals("StandardDeviation")) {
            return CustomIndicatorInterface.getStandardDeviationClassName();
        } else if (indName.equals("StandardError")) {
            return CustomIndicatorInterface.getStandardErrorClassName();
        } else if (indName.equals("StochasticOscillator")) {
            return CustomIndicatorInterface.getStochasticOscillatorClassName();
        } else if (indName.equals("TEMA")) {
            return CustomIndicatorInterface.getTEMAClassName();
        } else if (indName.equals("TimeSeriesForecast")) {
            return CustomIndicatorInterface.getTSFClassName();
        } else if (indName.equals("TRIX")) {
            return CustomIndicatorInterface.getTRIXClassName();
        } else if (indName.equals("VerticalHorizontalFilter")) {
            return CustomIndicatorInterface.getVHFClassName();
        } else if (indName.equals("WildersSmoothing")) {
            return CustomIndicatorInterface.getWildersSmoothingClassName();
        } else if (indName.equals("ZigZag")) {
            return CustomIndicatorInterface.getZigZagClassName();
        } else if (indName.equals("ZigZagSlope")) {
            return CustomIndicatorInterface.getZigZagSlopeClassName();
        } else if (indName.equals("CommodityChannelIndex")) {
            return CustomIndicatorInterface.getCommodityChannelIndexClassName();
        } else if (indName.equals("AccumulationDistribution")) {
            return CustomIndicatorInterface.getAccumulationDistributionClassName();
        } else if (indName.equals("Aroon")) {
            return CustomIndicatorInterface.getAroonClassName();
        } else if (indName.equals("AverageTrueRange")) {
            return CustomIndicatorInterface.getAverageTrueRangeClassName();
        } else if (indName.equals("ChaikinADOscillator")) {
            return CustomIndicatorInterface.getChaikinADOscillatorClassName();
        } else if (indName.equals("ChaikinMoneyFlow")) {
            return CustomIndicatorInterface.getChaikinMoneyFlowClassName();
        } else if (indName.equals("EaseOfMovement")) {
            return CustomIndicatorInterface.getEaseOfMovementClassName();
        } else if (indName.equals("ForcastOscillator")) {
            return CustomIndicatorInterface.getForcastOscillatorClassName();
        } else if (indName.equals("IchimokuKinkoHyo")) {
            return CustomIndicatorInterface.getIchimokuKinkoHyoClassName();
        } else if (indName.equals("IntradayMomentumIndex")) {
            return CustomIndicatorInterface.getIntradayMomentumIndexClassName();
        } else if (indName.equals("KlingerOscillator")) {
            return CustomIndicatorInterface.getKlingerOscillatorClassName();
        } else if (indName.equals("MACDVolume")) {
            return CustomIndicatorInterface.getMACDVolumeClassName();
        } else if (indName.equals("MarketFacilitationIndex")) {
            return CustomIndicatorInterface.getMarketFacilitationIndexClassName();
        } else if (indName.equals("MassIndex")) {
            return CustomIndicatorInterface.getMassIndexClassName();
        } else if (indName.equals("MedianPrice")) {
            return CustomIndicatorInterface.getMedianPriceClassName();
        } else if (indName.equals("MoneyFlowIndex")) {
            return CustomIndicatorInterface.getMoneyFlowIndexClassName();
        } else if (indName.equals("NegativeVolumeIndex")) {
            return CustomIndicatorInterface.getNegativeVolumeIndexClassName();
        } else if (indName.equals("PositiveVolumeIndex")) {
            return CustomIndicatorInterface.getPositiveVolumeIndexClassName();
        } else if (indName.equals("PriceChannel")) {
            return CustomIndicatorInterface.getPriceChannelClassName();
        } else if (indName.equals("PriceVolumeTrend")) {
            return CustomIndicatorInterface.getPriceVolumeTrendClassName();
        } else if (indName.equals("Qstick")) {
            return CustomIndicatorInterface.getQstickClassName();
        } else if (indName.equals("RelativeVolatilityIndex")) {
            return CustomIndicatorInterface.getRelativeVolatilityIndexClassName();
        } else if (indName.equals("StochasticMomentumIndex")) {
            return CustomIndicatorInterface.getStochasticMomentumIndexClassName();
        } else if (indName.equals("SwingIndex")) {
            return CustomIndicatorInterface.getSwingIndexClassName();
        } else if (indName.equals("TypicalPrice")) {
            return CustomIndicatorInterface.getTypicalPriceClassName();
        } else if (indName.equals("Volatility")) {
            return CustomIndicatorInterface.getVolatilityClassName();
        } else if (indName.equals("VolumeOscillator")) {
            return CustomIndicatorInterface.getVolumeOscillatorClassName();
        } else if (indName.equals("VolumeROC")) {
            return CustomIndicatorInterface.getVolumeROCClassName();
        } else if (indName.equals("WeightedClose")) {
            return CustomIndicatorInterface.getWeightedCloseClassName();
        } else if (indName.equals("WilliamsR")) {
            return CustomIndicatorInterface.getWilliamsRClassName();
        } else if (indName.equals("WillsAccumDistribution")) {
            return CustomIndicatorInterface.getWillsAccumDistributionClassName();
        } else if (indName.equals("CoppockCurve")) {
            return CustomIndicatorInterface.getCopockCurveClassName();
        } else {
            return "";
        }
    }

}


