package com.isi.csvr.chart.customindicators.generalparams;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Mar 24, 2008
 * Time: 5:52:35 PM
 * To change this template use File | Settings | File Templates.
 */
public interface PositionLiteral {
    public final int ABOVE = 1,
    CENTER = 0,
    BELOW = -1;
}
