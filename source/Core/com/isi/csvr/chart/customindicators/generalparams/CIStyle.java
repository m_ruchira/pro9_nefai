package com.isi.csvr.chart.customindicators.generalparams;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Mar 24, 2008
 * Time: 5:41:56 PM
 * To change this template use File | Settings | File Templates.
 */
public class CIStyle {
    private int style;
    public int getStyle() {
        return style;
    }

    public CIStyle(String style) {
        this.style = CIStyle.getStyleFromString(style);
    }

    public CIStyle(int pos) {
        try {
            this.style = (pos-1);
        } catch(Exception ex) {
            this.style = StyleLiteral.SOLID;
        }
    }

    public static int getStyleFromString(String style) {
        style = style.toUpperCase();
        if (style.equals("SOLID")) {
            return StyleLiteral.SOLID;
        } else if (style.equals("DOT")) {
            return StyleLiteral.DOT;
        } else if (style.equals("DASH")) {
            return StyleLiteral.DASH;
        } else if (style.equals("DASHDOT")) {
            return StyleLiteral.DASHDOT;
        } else if (style.equals("DASHDOTDOT")) {
            return StyleLiteral.DASHDOTDOT;
        } else {
            return StyleLiteral.SOLID;
        }
    }
}
