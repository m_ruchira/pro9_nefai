package com.isi.csvr.chart.customindicators;

/**
 * Created by IntelliJ IDEA.
 * User: udaka
 * Date: Feb 5, 2007
 * Time: 1:13:00 AM
 * To change this template use File | Settings | File Templates.
 */
public enum StringLiteral {
    NULL,
    FALSE,
    TRUE,
    OPEN,
    HIGH,
    LOW,
    CLOSE,
    VOLUME,
    EXPONENTIAL,
    SIMPLE,
    TIMESERIES,
    TRIANGULAR,
    VARIABLE,
    VOLUMEADJUSTED,
    WEIGHTED,
    PERCENT,
    PRICE,
    MONTHLY,
    DAILY
}
