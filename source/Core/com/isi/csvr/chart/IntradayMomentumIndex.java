package com.isi.csvr.chart;

import com.isi.csvr.shared.Language;
import com.isi.csvr.chart.indicatorproperties.IndicatorPropertyStore;
import com.isi.csvr.chart.indicatorproperties.IndicatorTypeOneParameterProperty;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.LinkedList;
import java.awt.*;

/**
 * User: P.Dissanayake
 * Date: Jan 25, 2006
 * Time: 10:51:57 AM
 */

public class IntradayMomentumIndex extends ChartProperties implements Indicator, Serializable {

    //Fields
    private int timePeriods;
    private ChartProperties innerSource;
    private String tableColumnHeading;

    private static final long serialVersionUID = UID_INTRADAY_MOMENTUM_INDEX;

    public IntradayMomentumIndex(ArrayList data, String symbl, byte anID, Color c, WindowPanel r) {
        super(data, Language.getString("IND_IMI") + Indicator.FD + symbl, anID, c, r);
        tableColumnHeading = Language.getString("IND_TABLE_HEADING_IMI");
        this.timePeriods = 14;
        isIndicator = true;
        isUsingUserDefault = IndicatorPropertyStore.getSharedInstance().hasDefauiltProperties(anID);
        if (isUsingUserDefault) {
            IndicatorTypeOneParameterProperty idp = (IndicatorTypeOneParameterProperty) IndicatorPropertyStore.getSharedInstance().getPropertyStore().get( anID);
            this.setColor(idp.getUpColor());
            this.setWarningColor(idp.getDownColor());
            this.setPenStyle(idp.getLineStyle());
            this.setPenWidth(idp.getLineThickness());
            this.setUseSameColor(idp.isUsingSameColor());
            this.setTimePeriods(idp.getTimePeriods());
        }
    }
    
    public void assignDefaultValues(){
        super.assignDefaultValues();
        this.timePeriods = 14;
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout) {
        super.loadTemplate(xpath, document, preExpression, isLayout);
        this.timePeriods = Integer.parseInt(TemplateFactory.loadProperty(xpath, document, preExpression + "/TimePeriods"));
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document) {
        super.saveTemplate(chart, document);
        TemplateFactory.saveProperty(chart, document, "TimePeriods", Integer.toString(this.timePeriods));
    }

    public void assignValuesFrom(ChartProperties cp) {
        super.assignValuesFrom(cp);
        if (cp instanceof IntradayMomentumIndex) {
            IntradayMomentumIndex imi = (IntradayMomentumIndex) cp;
            this.timePeriods = imi.timePeriods;
            this.innerSource = imi.innerSource;
        }
    }

    public String toString() {
        String[] sa = getSymbol().split(Indicator.FD);
        String parent;
        if (sa.length >= 2) {
            parent = "(" + StockGraph.extractSymbolFromStr(sa[1]) + ") ";
        } else {
            parent = "(" + Language.getString("INDICATOR") + ") ";
        }

        parent += Language.getString("INDICATOR_TITLE_TIME_PERIOD") + " " + timePeriods;
        return Language.getString("IND_IMI") + " " + parent;
    }

    //############################################
    //implementing Indicator
    public boolean hasItsOwnScale() {
        return true;
    }

    public ChartProperties getInnerSource() {
        return innerSource;
    }

    public void setInnerSource(ChartProperties cp) {
        innerSource = cp;
    }

    public int getInnerSourceIndex(ArrayList Sources) {
        if (Sources != null)
            for (int i = 0; i < Sources.size(); i++) {
                ChartProperties cp = (ChartProperties) Sources.get(i);
                if (cp == innerSource) return i;
            }
        return 0;
    }

    //timePeriods
    public int getTimePeriods() {
        return timePeriods;
    }

    public void setTimePeriods(int tp) {
        timePeriods = tp;
    }

    public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index) {
        double upSum = 0f;
        double downSum = 0f;
        double currVal;
        double prevVal;
        ChartRecord cr, crOld;

        long entryTime = System.currentTimeMillis();

        int loopBegin = timePeriods;

        if ((loopBegin >= al.size()) || (loopBegin < 2)) return;

        for (int i = 0; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.Close - cr.Open;
            if (currVal > 0) {
                upSum += currVal;
            } else {
                downSum += (-currVal); // (-currVal) is positive in this case
            }
            GDM.removeIndicatorPoint(cr.Time, index, getID());
            if (i >= loopBegin - 1) {
                ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
                if (aPoint != null) {
                    aPoint.setIndicatorValue(upSum + downSum == 0 ? 0 : 100 * upSum / (upSum + downSum));
                }
            }
        }

        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.Close - cr.Open;
            crOld = (ChartRecord) al.get(i - timePeriods);
            prevVal = crOld.Close - crOld.Open;

            if (currVal > 0) {
                upSum = upSum + currVal;
            } else {
                downSum = downSum - currVal; // (-currVal) is positive in this case
            }
            if (prevVal > 0) {
                upSum = upSum - prevVal;
            } else {
                downSum = downSum + prevVal; // prevVal is negative in this case
            }
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
            if (aPoint != null) {
                aPoint.setIndicatorValue(upSum + downSum == 0 ? 0 : 100 * upSum / (upSum + downSum));
            }
        }
        //System.out.println("**** IMI Calc time " + (entryTime - System.currentTimeMillis()));
    }
    //############################################

    public String getShortName() {
        return Language.getString("IND_IMI");
    }

    public static double getIMI(LinkedList<ChartRecord> ll, int timePeriod) {
        double upSum = 0f;
        double downSum = 0f;
        double currVal;
        ChartRecord cr;

        if ((timePeriod >= ll.size()) || (timePeriod < 2)) return 0.0;

        for (int i = (ll.size() - timePeriod); i < (ll.size()); i++) {
            cr = ll.get(i);
            currVal = cr.Close - cr.Open;
            if (currVal > 0) {
                upSum += currVal;
            } else {
                downSum += (-currVal); // (-currVal) is positive in this case
            }
        }

        return ((upSum + downSum) == 0) ? 0 : 100 * upSum / (upSum + downSum);
    }


    public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public static void calculateIndicator(GraphDataManagerIF GDM, ArrayList al, int bIndex,
                                          int t_im_ePeriods, byte destIndex) {
        getIntradayMomentumIndex(al, bIndex, t_im_ePeriods, GDM, destIndex);
    }


    public static void getIntradayMomentumIndex(ArrayList al, int bIndex, int timePeriods,
                                                GraphDataManagerIF GDM, byte destIndex) {

        long entryTime = System.currentTimeMillis();
        double upSum = 0f;
        double downSum = 0f;
        double currVal;
        double prevVal;
        ChartRecord cr, crOld;

        /////////// no need to avoid null points as there is no srcIndex ////////////

        int loopBegin = bIndex + timePeriods;

        if ((loopBegin >= al.size()) || (timePeriods < 2)) { // not enough data points
            for (int i = 0; i < al.size(); i++) {
                cr = (ChartRecord) al.get(i);
                cr.setValue(destIndex, Indicator.NULL);
            }
            return;
        }

        for (int i = 0; i < bIndex; i++) {
            cr = (ChartRecord) al.get(i);
            cr.setValue(destIndex, Indicator.NULL);
        }
        for (int i = bIndex; i < loopBegin; i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.Close - cr.Open;
            if (currVal > 0) {
                upSum += currVal;
            } else {
                downSum += (-currVal); // (-currVal) is positive in this case
            }
            if (i >= loopBegin - 1) {
                cr.setValue(destIndex, (upSum + downSum == 0) ? 0 : 100 * upSum / (upSum + downSum));
            } else {
                cr.setValue(destIndex, Indicator.NULL);
            }
        }

        for (int i = loopBegin; i < al.size(); i++) {
            cr = (ChartRecord) al.get(i);
            currVal = cr.Close - cr.Open;
            crOld = (ChartRecord) al.get(i - timePeriods);
            prevVal = crOld.Close - crOld.Open;

            if (currVal > 0) {
                upSum = upSum + currVal;
            } else {
                downSum = downSum - currVal; // (-currVal) is positive in this case
            }
            if (prevVal > 0) {
                upSum = upSum - prevVal;
            } else {
                downSum = downSum + prevVal; // prevVal is negative in this case
            }
            cr.setValue(destIndex, (upSum + downSum == 0) ? 0 : 100 * upSum / (upSum + downSum));
        }
        //System.out.println("**** Inner IMI Calc time " + (entryTime - System.currentTimeMillis()));

    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

    // To be used by custom indicators
    public static int getAuxStepCount() {
        return 0;
    }
}
