package com.isi.csvr.chart;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: charith nidarsha
 * Date: Oct 3, 2008
 * Time: 3:47:03 PM
 * To change this template use File | Settings | File Templates.
 */
public class CustomRectangularDrawingLabel extends JLabel {

    private int height;
    private int width;
    private Color color;

    public CustomRectangularDrawingLabel(int w, int h, Color c) {
        //super();
        this.width = w;
        this.height = h;
        this.color = c;
    }

    public void paint(Graphics g) {
        super.paint(g);
        g.setColor(new Color(100, 100, 100));
        g.drawRect(width / 4, height / 4, width / 2, height / 2);
        g.setColor(color);
        g.fillRect(width / 4, height / 4, width / 2, height / 2);
    }
}