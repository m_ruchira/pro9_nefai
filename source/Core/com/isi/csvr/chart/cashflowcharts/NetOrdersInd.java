package com.isi.csvr.chart.cashflowcharts;

import com.isi.csvr.chart.*;
import com.isi.csvr.shared.Language;
import com.isi.csvr.cashFlowWatch.CashFlowHistoryRecord;

import java.io.Serializable;
import java.util.ArrayList;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Sep 30, 2008
 * Time: 10:28:28 AM
 * To change this template use File | Settings | File Templates.
 */


public class NetOrdersInd extends ChartProperties implements Indicator, Serializable,GraphCalcTypeApplicable {
	//Fields
	private ChartProperties innerSource;

    private static final long serialVersionUID = UID_NET_ORDERS;
    private String tableColumnHeading;
    static final int    POINTS = 0;
    static final  int PERCENT = 1;
    private int  calcType = 0;
    final private int TOOLTIPC_CAPACITY =  4 ;
    private ArrayList<ToolTipDataRow> dataRows = new ArrayList<ToolTipDataRow>(TOOLTIPC_CAPACITY );

	public NetOrdersInd(ArrayList data, String symbl, byte anID, Color c, WindowPanel r){
		super(data, Language.getString("IND_NET_ORDERS") + Indicator.FD + symbl, anID, c, r);

        tableColumnHeading = Language.getString("IND_TABLE_HEADING_NET_ORDERS");
//        setChartStyle((byte)StockGraph.INT_GRAPH_STYLE_BAR);
        setChartStyle((byte)StockGraph.INT_GRAPH_STYLE_PERCET);
    }

    protected void loadTemplate(javax.xml.xpath.XPath xpath, org.w3c.dom.Document document, String preExpression, boolean isLayout){
        super.loadTemplate(xpath, document, preExpression, isLayout);
    }

    protected void saveTemplate(org.w3c.dom.Element chart, org.w3c.dom.Document document){
        super.saveTemplate(chart, document);
    }

	public void assignValuesFrom(ChartProperties cp){
		super.assignValuesFrom(cp);
		if (cp instanceof NetOrdersInd ){
			NetOrdersInd  no = (NetOrdersInd )cp;
			this.innerSource = no.innerSource;
            this.calcType = no.calcType;
		}
	}

	public String toString(){
		String[] sa = getSymbol().split(Indicator.FD);
		String parent;
		if (sa.length>=2){
			parent = "("+ StockGraph.extractSymbolFromStr(sa[1])+") ";
		}else{
            parent = "(" + Language.getString("INDICATOR") + ") ";
		}
		return Language.getString("IND_NET_ORDERS") + " " + parent;
	}
	//############################################
	//implementing Indicator
	public boolean hasItsOwnScale(){ //this should

        return true;
	}
	public ChartProperties getInnerSource(){
		return innerSource;
	}
	public void setInnerSource(ChartProperties cp){
		innerSource = cp;
	}
	public int getInnerSourceIndex(ArrayList Sources){
		if (Sources!=null)
		for (int i=0; i<Sources.size(); i++) {
			ChartProperties cp = (ChartProperties)Sources.get(i);
			if (cp==innerSource) return i;
		}
		return 0;
	}
	public void insertIndicatorToGraphStore(ArrayList al, GraphDataManagerIF GDM, int index){
		//tmp var for sop
		long entryTime = System.currentTimeMillis();
        CashFlowHistoryRecord cr;

//        for(int i=0; i<al.size(); i++){
//            cr = (ChartRecord)al.get(i);
//            ChartPoint aPoint = GDM.getIndicatorPoint(cr.Time, index, getID());
//            if (aPoint!=null){
//                aPoint.setIndicatorValue((cr.High+cr.Low)/2f);
//            }
//        }

        for(int i=0; i<al.size(); i++){
            cr = (CashFlowHistoryRecord)al.get(i);
            ChartPoint aPoint = GDM.getIndicatorPoint(cr.getDate(), index, getID());
            if (aPoint!=null){
                if (calcType == POINTS){
                    //offer redcashout      bid = green - cashin
                aPoint.setIndicatorValue(cr.getNetOrders());
                }else {
                    //offer redcashout      bid = green - cashin

                    if (cr.getCashInOrders() == 0.0d){

                         aPoint.setIndicatorValue(0d,(double) cr.getCashInOrders() ,(double)cr.getCashoutOrders()); //how indicate the differnce
                    }else if (cr.getCashoutOrders() == 0.0d) {
                         aPoint.setIndicatorValue(100d,(double) cr.getCashInOrders() ,(double)cr.getCashoutOrders()); //how indicate the differnce
                    }else {
                        double bidPercentValue = ((double) cr.getCashInOrders() )/ ((double)(cr.getCashInOrders() + (double)Math.abs(cr.getCashoutOrders())));
                        bidPercentValue = bidPercentValue * 100d;
                        System.out.println("NetOrder  bidpercnt value:  " + bidPercentValue );
                        aPoint.setIndicatorValue(bidPercentValue,(double) cr.getCashInOrders() ,(double)cr.getCashoutOrders()); //how indicate the differnce
                        
                    }



                }
            }
        }
        //System.out.println("**** Net orders  Calc time " + (entryTime - System.currentTimeMillis()));
	}
	//############################################

	public String getShortName(){
		return Language.getString("IND_NET_ORDERS");
	}

        public String getTableColumnHeading() {
        return tableColumnHeading;
    }

    public void setTableColumnHeading(String tableColumnHeading) {
        this.tableColumnHeading = tableColumnHeading;
    }

    public int getCalcType() {
        return calcType;
    }

    public void setCalcType(int calcType) {
        this.calcType = calcType;
    }
    public  ArrayList<ToolTipDataRow> getToolTipRows() {
           // to be implemented by the sub classes extending this
           return dataRows;
       }
    
    public  void clearToolTipData(){
        dataRows.clear();
    }

    public void addToolTipData(String name, String value) {
        dataRows.add(new ToolTipDataRow(Language.getString(name), value));
    }
}



