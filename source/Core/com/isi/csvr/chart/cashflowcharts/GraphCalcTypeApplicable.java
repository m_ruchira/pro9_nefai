package com.isi.csvr.chart.cashflowcharts;

import com.isi.csvr.chart.ToolTipDataRow;

import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: sathyajith
 * Date: Oct 22, 2008
 * Time: 2:24:39 PM
 * To change this template use File | Settings | File Templates.
 */
public interface GraphCalcTypeApplicable {
    public int getCalcType() ;
    public void setCalcType(int calcType) ;
    public  ArrayList<ToolTipDataRow> getToolTipRows();
    public  void addToolTipData(String name,String value);
    public  void clearToolTipData();
}
