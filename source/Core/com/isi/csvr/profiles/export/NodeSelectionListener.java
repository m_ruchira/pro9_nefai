package com.isi.csvr.profiles.export;

import javax.swing.*;
import javax.swing.tree.TreePath;
import javax.swing.tree.DefaultTreeModel;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.WindowAdapter;
import java.awt.event.WindowEvent;
import java.util.Enumeration;

import javax.swing.ButtonGroup;
import javax.swing.Icon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;
import javax.swing.JRadioButton;
import javax.swing.JScrollPane;
import javax.swing.JTextArea;
import javax.swing.JTree;
import javax.swing.UIManager;
import javax.swing.border.TitledBorder;
import javax.swing.plaf.ColorUIResource;
import javax.swing.plaf.basic.BasicOptionPaneUI;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeCellRenderer;
import javax.swing.tree.TreeNode;
import javax.swing.tree.TreePath;
import javax.swing.tree.TreeSelectionModel;
/**
 * Created by IntelliJ IDEA.
 * User: admin
 * Date: 23-Apr-2007
 * Time: 16:49:26
 * To change this template use File | Settings | File Templates.
 */
  class NodeSelectionListener extends MouseAdapter {
        JTree tree;

        NodeSelectionListener(JTree tree) {
          this.tree = tree;
        }

        public void mouseClicked(MouseEvent e) {
          int x = e.getX();
          int y = e.getY();
          int row = tree.getRowForLocation(x, y);
          TreePath path = tree.getPathForRow(row);
          //TreePath  path = tree.getSelectionPath();
          if (path != null) {
            CheckNode node = (CheckNode)path.getLastPathComponent();
            boolean isSelected = ! (node.isSelected());
            node.setSelected(isSelected);


            if (node.getSelectionMode() == CheckNode.DIG_IN_SELECTION) {
              if ( isSelected) {
                tree.expandPath(path);
              } else {
                tree.collapsePath(path);
              }
              if(node.isSelected()){

              }
            }
            ((DefaultTreeModel) tree.getModel()).nodeChanged(node);
            // I need revalidate if node is root.  but why?
            if (row == 0) {
              tree.revalidate();
              tree.repaint();
            }
          }
        }
      }