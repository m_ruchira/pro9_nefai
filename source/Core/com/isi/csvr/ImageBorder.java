package com.isi.csvr;

import javax.swing.border.MatteBorder;
import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Jul 23, 2008
 * Time: 10:53:42 AM
 * To change this template use File | Settings | File Templates.
 */
public class ImageBorder extends MatteBorder {
      public  ImageBorder(int top, int left, int bottom, int right, Color matteColor) {
            super(top, left, bottom, right, matteColor);
        }


        ImageBorder(Insets borderInsets, Color matteColor) {
            super(borderInsets, matteColor);
        }

        ImageBorder(int top, int left, int bottom, int right, Icon tileIcon) {
            super(top, left, bottom, right, tileIcon);
        }

        ImageBorder(Insets borderInsets, Icon tileIcon) {
            super(borderInsets, tileIcon);
        }

        ImageBorder(Icon tileIcon) {
            super(tileIcon);
        }

        public void setColor(Color color){
            super.color = color;
        }
    }
