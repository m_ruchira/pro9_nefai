package com.isi.csvr;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shortcutkeys.AcceleratorListener;
import com.isi.csvr.shortcutkeys.AcceleratorStore;

import javax.swing.*;
import java.awt.*;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Jan 4, 2005
 * Time: 3:29:18 PM
 * To change this template use File | Settings | File Templates.
 */
public class TWCheckBoxMenuItem extends JCheckBoxMenuItem implements AcceleratorListener {

    private String iconFile;
    private String acceleratorText;
      private int acceleratorID;

    public TWCheckBoxMenuItem() {
        super();
        setUI(new TWCheckBoxMenuItemUI());
    }

    public TWCheckBoxMenuItem(String text) {
        super(text);
        iconFile = null;
        setUI(new TWCheckBoxMenuItemUI());
    }

    public TWCheckBoxMenuItem(String text, String icon) {
        super(text);
        iconFile = icon;
        setUI(new TWCheckBoxMenuItemUI());
    }

    public TWCheckBoxMenuItem(String text, boolean b) {
        super(text, b);
        setUI(new TWCheckBoxMenuItemUI());
    }

      public TWCheckBoxMenuItem(String text, String icon, boolean acceleratorListener) {
        super(text);
        iconFile = icon;
        setUI(new TWCheckBoxMenuItemUI());
          if(acceleratorListener){
              AcceleratorStore.getInstance().addAcceleratorListener(this);
          }
    }

    public Dimension getPreferredSize() {
        Dimension dimension = super.getPreferredSize();
        dimension.setSize(dimension.getWidth() + Constants.OUTLOOK_MENU_COLUMN_WIDTH, dimension.getHeight());
        return dimension;
    }

    public void paint(Graphics g) {
        g.translate(Constants.OUTLOOK_MENU_COLUMN_WIDTH * Constants.OUTLOOK_MENU_SIDE, 0);
        super.paint(g);
        g.translate(-(Constants.OUTLOOK_MENU_COLUMN_WIDTH * Constants.OUTLOOK_MENU_SIDE), 0);
        g.setColor(Theme.MENU_SELECTION_COLOR);
    }

    public void setSelected(boolean b) {
        super.setSelected(b);
    }

    public void setIconFile(String iconFile) {
        this.iconFile = iconFile;
    }

    public void updateUI() {
        setBorderPainted(false);
        if (iconFile != null)
            super.setIcon(new ImageIcon("images/Theme" + Theme.getID() + "/menu/" + iconFile));
        super.updateUI();
        setUI(new TWCheckBoxMenuItemUI());
    }

    public String getAcceleratorText() {
        return acceleratorText;
    }

    public void setAccelerator(String acceleratorText) {
        this.acceleratorText = acceleratorText;
    }

    public void acceleratorChanged() {
        String accText = TWActions.getInstance().getActionKeyForID(getAcceleratorID());
        setAccelerator(accText);
    }

    public int getAcceleratorID() {
        return acceleratorID;
    }

    public void setAcceleratorID(int acceleratorID) {
        this.acceleratorID = acceleratorID;
         setAccelerator(TWActions.getInstance().getActionKeyForID(acceleratorID));
    }
}
