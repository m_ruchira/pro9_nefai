package com.mubasher.formulagen;

import com.isi.csvr.shared.Constants;

import javax.tools.JavaCompiler;
import javax.tools.JavaFileObject;
import javax.tools.StandardJavaFileManager;
import javax.tools.ToolProvider;
import java.io.File;
import java.util.ArrayList;
import java.util.Arrays;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Aug 13, 2006
 * Time: 10:21:16 PM
 */
public class TWCompiler {
    /*public static void main(String[] args) {
        long now = System.currentTimeMillis();
        compile(args[0], args[1]);
        System.out.println("Compiler " + (System.currentTimeMillis() - now));
        now = System.currentTimeMillis();
        jcompile(args[0], args[1]);
        System.out.println("JCompiler " + (System.currentTimeMillis() - now));
        System.out.println("COMPILED");
    }*/

    /*public static int compile(String... args) {
        long now = System.currentTimeMillis();
        int ret = jcompile(args[0], args[1]);
        *//*System.out.println("Compiler " + (System.currentTimeMillis() - now));
        now = System.currentTimeMillis();
        jcompile(args[0], args[1]);
        System.out.println("JCompiler " + (System.currentTimeMillis() - now));*//*
        System.out.println("COMPILED");

        return ret;
    }*/

    /*public static int compilex(String source, String destination) {
        try {
            System.out.println("LOADING COMPILER....");
//            String[] optionsAndSources = {"-g:none","-verbose", "-Xbootclasspath/p:lib/loader.zip" ,"-classpath", "./lib/compiler.mlf;./lib/mubasher.mlf;./lib/lang.mlf", "-d", destination, source};
//            String[] optionsAndSources = {"-g:none", "-verbose" ,"-classpath", "./lib/compiler.mlf;./lib/mubasher.mlf;./lib/lang.mlf", "-d", destination, source};
            String[] optionsAndSources = {"-g:none", "-classpath", "../classes;./lib/mubasher.mlf;./lib/lang.mlf","-target", "1.6", "-d", destination, source};   //./lib/compiler.mlf;

            PrintWriter out = new PrintWriter(System.out);

            //long start = System.currentTimeMillis();
            return Main.compile(optionsAndSources, out);
            //System.out.println("status: " + status);
            //System.out.println("Compiled " + (System.currentTimeMillis() - start));

            *//*Class.forName("Test",false,new DynamicClassLoader()).getDeclaredMethod("main", new Class[]{String[].class}).invoke(null,
                    new Object[]{null});
            start = System.currentTimeMillis();
            TestInterface testInterface = (TestInterface)Class.forName("Test",false,new DynamicClassLoader()).newInstance();
            System.out.println("Loaded " + (System.currentTimeMillis() - start));
            start = System.currentTimeMillis();
            //for (int i = 0; i < 1000; i++){
                System.out.println("Value = " + testInterface.evaluate());
                //Thread.sleep(1);
            //}
            System.out.println("Executed " + (System.currentTimeMillis() - start));*//*
        } catch (Exception e) {
            e.printStackTrace();
            return 1;
        }
    }*/

    public static int compile(String source, String destination) {

        System.out.println("Compiling ...");
        ArrayList<String> list = new ArrayList<String>();
        list.add("-g:none");
        list.add("-classpath");

        list.add("./lib/mubasher.mlf;../classes;./lib/lang.mlf");
//        list.add("D:\\ProGeneral\\Pro9_Morocco\\Classes");
//        list.add("D:\\Pro9_Morocco\\Classes");
//        list.add(("E:\\Developments\\PRO-Version-9.00\\9.99.00.060-Intl\\Classes V9"));
//        list.add("D:\\Development\\8.93Plugins\\Classes");

        list.add("-target");
        list.add("1.6");
        list.add("-d");
        list.add(destination);
        File[] files = new File[1];
        files[0] = new File(source);
        JavaCompiler compiler = ToolProvider.getSystemJavaCompiler();
        StandardJavaFileManager fileManager = compiler.getStandardFileManager(null, null, null);
        Iterable<? extends JavaFileObject> compilationUnits1 =
           fileManager.getJavaFileObjectsFromFiles(Arrays.asList(files));
        boolean sucess =  compiler.getTask(null, null, null, list, null, compilationUnits1).call();
        System.out.println("COMPILED");

        if (sucess)
            return Constants.COMPILER_COMPILE_SUCCESS;
        else
            return Constants.COMPILER_COMPILE_FAILED;
    }
}
