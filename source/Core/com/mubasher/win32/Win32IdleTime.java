package com.mubasher.win32;
import com.neva.Coroutine;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.ArrayList;

/**
 * Created by IntelliJ IDEA.
 * User: Uditha Nagahawatta
 * Date: Apr 21, 2009
 * Time: 9:49:42 AM
 * Checks with the underlying operating system to check if user idle state has occured.
 * Calls to GetLastInputInfo and GetTickCount methods of Win32 system.
 */
public class Win32IdleTime implements Runnable{

    private boolean active;
    private int idleTime;
    private Thread thread;
    private Coroutine user32;
    private Coroutine kernel32;
    private ArrayList<Win32IdleTimeListener> listeners;
    private final String lock = "Win32IdleTime";

    public enum State {
		UNKNOWN, ONLINE, IDLE
	}

    public Win32IdleTime() {
        user32 = new Coroutine("User32", "GetLastInputInfo");
        kernel32 = new Coroutine("Kernel32", "GetTickCount");

        listeners = new ArrayList<Win32IdleTimeListener>();
    }

    /**
     * Adds a Win32IdleTimeListener which notifies the idle event
     * @param listener
     */
    public void addWin32IdleTimeListener(Win32IdleTimeListener listener){
        try {
            listeners.add(listener);
        } catch (Exception e) {
        }
    }

    /**
     * Removes a Win32IdleTimeListener
     * @param listener
     */
    public void removeWin32IdleTimeListener(Win32IdleTimeListener listener){
        try {
            listeners.remove(listener);
        } catch (Exception e) {
        }
    }

    /**
     * Start the idle timer
     * @param idleTime in minutes
     */
    public void start(int idleTime){
        synchronized (lock){
            if (thread == null){
                this.idleTime = idleTime * 60; // in seconds
                active = true;
                thread = new Thread(this, "Win32 Idle Timer");
                thread.setPriority(Thread.MIN_PRIORITY);
                thread.start();
            } else {
                throw new RuntimeException("Idle Timer is already running.");
            }
        }
    }

    /**
     * stop the idle timer
     */
    public void stop(){
        synchronized (lock){
            active = false;
            try {
                thread.interrupt();
            } catch (Exception e) {
            }
            thread = null;                       
        }
    }

    public void run() {
        State state = State.UNKNOWN;
		DateFormat dateFormat = new SimpleDateFormat("EEE, d MMM yyyy HH:mm:ss");

		while (active) {
			int idleSec = getIdleTimeMillisWin32() / 1000;

            System.out.println("sec " + idleSec);

			State newState = idleSec < idleTime ? State.ONLINE :State.IDLE;

			if (newState != state) {
				state = newState;
				System.out.println(dateFormat.format(new Date()) + " # " + state);
                fireEventOccured(newState);
			}
			try { Thread.sleep(5000); } catch (Exception ex) {}
		}
    }

    private void fireEventOccured(State state){
        try {
            for( Win32IdleTimeListener listener: listeners){
                try {
                    listener.Win32IdleEventOccured(state);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
	 * Get the amount of milliseconds that have elapsed since the last input event
	 * (mouse or keyboard)
	 * @return idle time in milliseconds
	 */
	private int getIdleTimeMillisWin32() {
        try {
            return getTickCount() - getLastInputInfo();
        } catch (Exception e) {
            return 0;
        }
    }

    /**
     * The GetLastInputInfo function retrieves the time of the last input event.
     * Calls to the GetLastInputInfo function of the underlying Operating System
     * @return If the function succeeds, the return value is nonzero. else zero
     * @throws Exception
     */
    private int getLastInputInfo() throws Exception {
        try {
            int[] data = new int[2];
            data[0] = 8;
            user32.addArg(data);
            user32.invoke();
            if ((user32.lastError() != null) || (!user32.lastError().equals(""))){
                data = user32.intArrayFromParameterAt(0,2);
                return data[1];
            } else {
                throw new Exception(user32.lastError());
            }
        } catch (Exception e) {
            throw e;
        }
    }

    /**
     * Retrieves the number of milliseconds that have elapsed since the system was started,
     * up to 49.7 days. Calls to the getTickCount function of the underlying Operating System
     * @return number of milliseconds that have elapsed since the system was started.
     * @throws Exception
     */
    private int getTickCount() throws Exception{
        try {
            kernel32.invoke();
            if ((kernel32.lastError() != null) || (!kernel32.lastError().equals(""))){
                return kernel32.answerAsInteger();
            } else {
                throw new Exception(kernel32.lastError());
            }
        } catch (Exception e) {
            throw e;
        }
    }
}
