package com.mubasher.mediaplayer;
/*
 * Copyright (c) 2002-2007 TeamDev Ltd. All rights reserved.
 *
 * Use is subject to license terms.
 *
 * The complete licence text can be found at
 * http://www.teamdev.com/comfyj/license.jsf
 */

import com.isi.csvr.Client;
import com.isi.csvr.vas.MenuLoader;
import com.isi.csvr.event.VASMenuListener;
import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.properties.SmartProperties;
import com.isi.csvr.shared.*;
import com.isi.csvr.theme.Theme;
import com.jniwrapper.win32.automation.Automation;
import com.jniwrapper.win32.automation.OleContainer;
import com.jniwrapper.win32.automation.OleMessageLoop;
import com.jniwrapper.win32.automation.types.VariantBool;
import com.jniwrapper.win32.ie.Browser;
import com.jniwrapper.win32.ole.types.OleVerbs;

import javax.swing.*;
import javax.swing.event.InternalFrameEvent;
import javax.swing.event.InternalFrameListener;
import java.awt.*;
import java.io.FileOutputStream;
import java.util.ArrayList;
import java.util.Enumeration;
import java.util.Iterator;


/**
 * This sample demonstrates the technique of embedding Windows Media Player application into a java application
 * using OleContainer and generated java stubs for Windows Media Player application.
 * <p/>
 * <p/>
 * This sample requires generated stubs for COM type library:
 * Description: Windows Media Player
 * ProgID:      MediaPlayer.MediaPlayer
 * GUID:        {22D6F304-B0F6-11D0-94AB-0080C74C7E95}
 * In the package: wmp
 * <p/>
 * You can generate stubs using the Code Generator application.
 *
 * @author Alexei Orischenko
 */
public class MediaPlayer  implements InternalFrameListener, URLSelectionListener, VASMenuListener {
    private static final String WINDOW_TITLE = Language.getString("MEDIA_PLAYER");

    private URLSelector urlSelector;
    private InternalFrame frame;
    private long selectedURLID = 0;
    private Browser browser;

    public MediaPlayer() {
        init();
    }

    private void init() {
        browser = new Browser();
        String mediaPath = System.getProperty("user.dir")+"\\Templates\\media.html";
        browser.navigate(mediaPath);
        browser.waitReady();
        frame = new MediaFrame();
        frame.setLayout(new BorderLayout());

        urlSelector = new URLSelector();
        urlSelector.addURLSelectionListener(this);
        frame.add(urlSelector, BorderLayout.NORTH);
        frame.hideTitleBarMenu();
        frame.getContentPane().add(browser, BorderLayout.CENTER);

        frame.addInternalFrameListener(this);

        frame.setDefaultCloseOperation(frame.DISPOSE_ON_CLOSE);
        frame.setTitle(WINDOW_TITLE);
        frame.setResizable(true);
        frame.setClosable(true);
        frame.setDetachable(true);

        loadData();

        Theme.registerComponent(frame);
        GUISettings.applyOrientation(frame);
        setFrameProperties();
        MenuLoader.getSharedInstance().addListener(this);
    }

    public InternalFrame getFrame() {
        return frame;
    }

    public void applyTheme() {
        SwingUtilities.updateComponentTreeUI(frame);
    }


    private void doOpen(String path) {
        try {
            browser.executeScript("play(\""+ path +"\")");
        }
        catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void setFrameProperties() {
        frame.setSize(640, 480);
    }

    public void internalFrameOpened(InternalFrameEvent e) {
        //showOleObject();
    }

    public void internalFrameClosing(InternalFrameEvent e) {
    }

    public void internalFrameClosed(InternalFrameEvent e) {

    }

    public void internalFrameIconified(InternalFrameEvent e) {
    }

    public void internalFrameDeiconified(InternalFrameEvent e) {
    }

    public void internalFrameActivated(InternalFrameEvent e) {
    }

    public void internalFrameDeactivated(InternalFrameEvent e) {
    }

    public void URLSelected(URLComponentRecord url) {
        if (url != null) { // open URL
            doOpen(url.getUrl());
            selectedURLID = url.getId();

            frame.setTitle(WINDOW_TITLE + " - " + url.getCaption());
        } else { // Add new URL
            FavouriteUI ui;
            if (frame.isDetached()) {
                ui = new FavouriteUI((JFrame) frame.getDetachedFrame());
            } else {
                ui = new FavouriteUI(Client.getInstance().getFrame());
            }
            ui.setVisible(true);
            URLComponentRecord record = ui.getRecord();
            if (record != null) {
                urlSelector.addURL(record);
                saveData();
            }
            ui = null;
        }
    }

    public void deleteSelected(URLComponentRecord url) {
        urlSelector.removeURL(url);
        saveData();
    }

    public void editSelected(URLComponentRecord url) {
        if (frame.isDetached()) {
            new FavouriteUI((JFrame) frame.getDetachedFrame(), url).show();
        } else {
            new FavouriteUI(Client.getInstance().getFrame(), url).show();
        }
        saveData();
    }

    public void applicationExiting() {
        frame.applicationExiting();
    }

    private void saveData() {
        try {
            SmartProperties propCustom = new SmartProperties(Meta.FS);
            SmartProperties propDefault = new SmartProperties(Meta.FS);

            ArrayList<URLComponentRecord> urls = urlSelector.getURLs(true);
            for (URLComponentRecord record : urls) {
                propDefault.setProperty("" + record.getId(), UnicodeUtils.getUnicodeString(record.getCaption()) + Meta.DS + record.getUrl());
            }

            urls = urlSelector.getURLs(false);
            for (URLComponentRecord record : urls) {
                propCustom.setProperty("" + record.getId(), UnicodeUtils.getUnicodeString(record.getCaption()) + Meta.DS + record.getUrl());
            }

            FileOutputStream out = new FileOutputStream(Settings.getAbsolutepath()+"datastore/mediaplayer.msf");
            propCustom.storeCompressed(out, "Media Player URLs");

            out = new FileOutputStream(Settings.getAbsolutepath()+"datastore/mediaplayer_default.msf");
            propDefault.storeCompressed(out, "Media Player default URLs");
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void loadData() {
        try {
            //load custom URLs
            SmartProperties prop = new SmartProperties(Meta.FS);
            prop.loadCompressed(Settings.getAbsolutepath()+"datastore/mediaplayer.msf");
            Enumeration records = prop.keys();
            while (records.hasMoreElements()) {
                try {
                    long id = Long.parseLong((String) records.nextElement());
                    String caption = UnicodeUtils.getNativeString(prop.getProperty("" + id).split(Meta.DS)[0]);
                    String url = prop.getProperty("" + id).split(Meta.DS)[1];
                    URLComponentRecord record = new URLComponentRecord(id, caption, url, false);
                    urlSelector.addURL(record);
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            //load default URLs
              SmartProperties propD = new SmartProperties(Meta.FS);
            propD.loadCompressed(Settings.getAbsolutepath()+"datastore/mediaplayer_default.msf");
            Enumeration recordsD = propD.keys();
            while (recordsD.hasMoreElements()) {
                try {
                    long id = Long.parseLong((String) recordsD.nextElement());
                    String caption = UnicodeUtils.getNativeString(propD.getProperty("" + id).split(Meta.DS)[0]);
                    String url = propD.getProperty("" + id).split(Meta.DS)[1];
                    URLComponentRecord record = new URLComponentRecord(id, caption, url, true);
                    urlSelector.addURL(record);
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private URLComponentRecord getURLComponentRecord(long id) {
        URLComponentRecord matched = null;
        ArrayList<URLComponentRecord> urls = urlSelector.getURLs(true);
        for (URLComponentRecord ucomp : urls) {
            if (ucomp.getId() == id) {
                matched = ucomp;
            }
        }
        urls = urlSelector.getURLs(false);
        for (URLComponentRecord ucomp : urls) {
            if (ucomp.getId() == id) {
                matched = ucomp;
            }
        }
        return matched;
    }

    public void setWorkspaceString(String value) {

        try {
            String[] results = value.split(";");
            boolean isVisible = Boolean.parseBoolean(results[0]);
            int windowWidth = Integer.parseInt(results[1]);
            int windowHeight = Integer.parseInt(results[2]);
            int x = Integer.parseInt(results[3]);
            int y = Integer.parseInt(results[4]);
            selectedURLID = Long.parseLong(results[5]);
            if (selectedURLID != 0) {
                if (Settings.isConnected()) {
                    URLComponentRecord urec = getURLComponentRecord(selectedURLID);
                    frame.setTitle(WINDOW_TITLE + " - " + urec.getCaption());
                    MediaPlayerPlugin.setLastSelectedURLID(urec.getCaption());
                    doOpen(urec.getUrl());
                }
            }
            MediaPlayerPlugin.setRectangle(new Rectangle(x, y, windowWidth, windowHeight), false);

        } catch (NumberFormatException e) {
            e.printStackTrace();
        }
    }

    public void lordLastSelectedChanel() {
        try {
            if (Settings.isConnected()) {
                if (selectedURLID != 0) {
                    URLComponentRecord urec = getURLComponentRecord(selectedURLID);
                    frame.setTitle(WINDOW_TITLE + " - " + urec.getCaption());
                    MediaPlayerPlugin.setLastSelectedURLID(urec.getCaption());
                    doOpen(urec.getUrl());
                }
            }
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
        }
    }

    public String getWorkspaceString(Rectangle rect) {
        Rectangle re = getFrame().getBounds();
        if (rect != null) {
            return (String.valueOf(this.getFrame().isVisible()) + ";" + (int) re.getWidth() + ";" + (int) re.getHeight() + ";" +
                    re.getLocation().x + ";" + re.getLocation().y + ";" + selectedURLID);
        } else {
//                         return (String.valueOf(this.getFrame().isVisible()) + ";" + rect.getWidth() + ";" +rect.getHeight() + ";" +
//                              rect.getLocation().x + ";" + rect.getLocation().y );
            return (String.valueOf(this.getFrame().isVisible()) + ";" + this.getFrame().getWidth() + ";" + this.getFrame().getHeight() + ";" +
                    this.getFrame().getLocation().x + ";" + this.getFrame().getLocation().y + ";" + selectedURLID);
        }

    }

    public void destroy() {
//        super.destroy();    //To change body of overridden methods use File | Settings | File Templates.
    }

    public void VASDataDownloadStarted() {

    }

    public void VASDataDownloadSucess(ArrayList<JComponent> menus, ArrayList<JComponent> links) {
        //for vas menus - no need to implement here
    }

    public void VASDataDownloadSucess(String id, ArrayList<String> links) {

        if (id.equals("MEDIAPLAYER")) {
            urlSelector.clearDefaultURLs();
            Iterator records = links.iterator();
            while (records.hasNext()) {
                String[] channelData = ((String) records.next()).split(",");
                long index = System.currentTimeMillis() + (int) (Math.random() * 100);
                String caption = channelData[0];
                String url = channelData[1];
                URLComponentRecord record = new URLComponentRecord(index, caption, url, true);
                urlSelector.addURL(record);
            }
           saveData();
        }
    }

    public void VASDataDownloadFailed() {

    }

    class MediaFrame extends InternalFrame implements NonEscapable {

         /*public void show() {
             super.show();
             MediaPlayerPlugin.showUI();
         }

        public void hideWindow(){
            try {
                System.out.println("destroy");
                MediaPlayerPlugin.setRectangle(frame.getBounds(), true);
                new Thread() {
                    @Override
                    public void run() {
                        try {
                            OleMessageLoop.invokeMethod(MediaPlayer.this, "destroy");
                        } catch (Exception e1) {
                            e1.printStackTrace();
                        }
                    }
                }.start();

                System.out.println("destroy ok");
            } catch (Exception e) {
                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
        }*/

        public void setVisible(boolean value) {
            try {
                if (!value && !frame.isDetached()) {
                try {
                    System.out.println("destroy");
                    MediaPlayerPlugin.setRectangle(frame.getBounds(), true);
                    new Thread() {
                        @Override
                        public void run() {
                            try {
                                OleMessageLoop.invokeMethod(MediaPlayer.this, "destroy");
                                browser.stop();
                                //browser.getParentBrowser().stop();
                            } catch (Exception e1) {
                                e1.printStackTrace();
                            }
                        }
                    }.start();

                    System.out.println("destroy ok");
//                    browser.close();
                } catch (Exception e) {
                    e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
                }
            }
            } catch (Exception e) {
//                e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            }
            super.setVisible(value);    //To change body of overridden methods use File | Settings | File Templates.
        }
    }
}
