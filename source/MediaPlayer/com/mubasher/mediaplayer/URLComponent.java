package com.mubasher.mediaplayer;

import com.isi.csvr.theme.Theme;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.GUISettings;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Mar 2, 2009
 * Time: 9:23:56 AM
 */
public class URLComponent extends JPanel implements MouseListener {

    private URLComponentRecord record;
    private JLabel caption;
    private JLabel edit;
    private JLabel delete;
    private URLSelectionListener parent;

    public URLComponent(URLComponentRecord record) {
        this.record = record;

        edit = new JLabel(new ImageIcon(Theme.getTheamedImagePath("m_player-edit")));
        delete = new JLabel(new ImageIcon(Theme.getTheamedImagePath("m_player-delete")));
        if (record != null) {
            caption = new JLabel(record.getCaption());
            edit.setToolTipText(Language.getString("MEDIA_PLAYER_TIP_EDIT"));
            delete.setToolTipText(Language.getString("MEDIA_PLAYER_TIP_DELETE"));
            FlexGridLayout layout = new FlexGridLayout(new String[]{"100%", "0", "0"}, new String[]{"0"}, 2, 2);
            setLayout(layout);
            caption.setOpaque(false);
            add(caption);
            if (!record.isDefault()) {
            add(edit);
            add(delete);
           // caption.addMouseListener(this);
            edit.addMouseListener(this);
            delete.addMouseListener(this);
            } else {
                add(new JLabel());
                add(new JLabel());
            }
            caption.addMouseListener(this);

        } else { // Add New item
            caption = new JLabel(Language.getString("MEDIA_PLAYER_ADD_CHANNEL"));
            FlexGridLayout layout = new FlexGridLayout(new String[]{"100%"}, new String[]{"0"}, 2, 2);
            setLayout(layout);
            caption.setOpaque(false);
            add(caption);
            caption.addMouseListener(this);
        }
        setOpaque(true);
        setBackground(UIManager.getColor("MenuItem.background"));
        setForeground(UIManager.getColor("MenuItem.foreground"));

//        setBackground(UIManager.getColor("TextField.background"));
//        setForeground(UIManager.getColor("TextField.foreground"));
        GUISettings.applyOrientation(this);
    }

    public void addURLSelectionListener(URLSelectionListener parent) {
        this.parent = parent;
    }

    @Override
    public Dimension getPreferredSize() {
        Dimension d = new Dimension();
        if (record == null) {
            d.setSize(caption.getPreferredSize().width,
                    Math.max(Math.max(caption.getPreferredSize().height, edit.getPreferredSize().height), delete.getPreferredSize().height)+4);
        } else {
            d.setSize(caption.getPreferredSize().width + edit.getPreferredSize().width + delete.getPreferredSize().width,
                    Math.max(Math.max(caption.getPreferredSize().height, edit.getPreferredSize().height), delete.getPreferredSize().height)+4);
        }
        return d;
    }

    public void mouseClicked(MouseEvent e) {
    }

    public void mouseEntered(MouseEvent e) {
        setBackground(Theme.getColor("MENU_SELECTION_BGCOLOR"));
        setForeground(Theme.getColor("MENU_SELECTION_FGCOLOR"));
    }

    public void mouseExited(MouseEvent e) {
        setBackground(UIManager.getColor("MenuItem.background"));
        setForeground(UIManager.getColor("MenuItem.foreground"));
    }

    public void mousePressed(MouseEvent e) {
    }

    public void mouseReleased(MouseEvent e) {
        try {
            Object popup = SwingUtilities.getAncestorOfClass(JPopupMenu.class, (Component) e.getSource());
            if (popup != null) {
                ((JPopupMenu) popup).setVisible(false);
            }
        } catch (Exception e1) {
            e1.printStackTrace();
        }
        if (parent != null) {
            if (e.getSource().equals(caption)) {
                    parent.URLSelected(record);
            } else if (e.getSource().equals(edit)) {
                parent.editSelected(record);
            } else if (e.getSource().equals(delete)) {
                parent.deleteSelected(record);
            }
        }
    }
}
