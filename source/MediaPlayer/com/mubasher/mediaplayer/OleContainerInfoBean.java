package com.mubasher.mediaplayer;
/*
 * Copyright (c) 2002-2007 TeamDev Ltd. All rights reserved.
 *
 * Use is subject to license terms.
 *
 * The complete licence text can be found at
 * http://www.teamdev.com/comfyj/license.jsf
 */

import com.jniwrapper.win32.automation.OleContainer;
import com.jniwrapper.win32.com.ComException;
import com.jniwrapper.win32.com.types.CLSID;
import com.jniwrapper.win32.ole.types.OleVerbs;
import com.jniwrapper.win32.shell.SHFileInfo;
import com.jniwrapper.win32.HResult;

import javax.swing.*;
import java.io.File;
import java.io.IOException;
import java.awt.*;


/**
 * @author Serge Piletsky
 */
public abstract class OleContainerInfoBean
{
    protected String _className;
    protected String _extentions;
    protected String _defaultAssociation;
    protected String _description;
    protected OleContainer _container;
    protected ImageIcon _associatedIcon;

    private boolean _installed;

    public OleContainerInfoBean(String className, String extentions, String description, String defaultAssociation)
    {
        _className = className;
        _extentions = extentions;
        _description = description;
        _defaultAssociation = defaultAssociation;

        try
        {
            CLSID.createFromProgID(className);
            _installed = true;
        }
        catch (ComException e)
        {
            // A component is not registered
            if (e.getHResult() == HResult.CO_E_CLASSSTRING)
            {
                _installed = false;
            }
            else
            {
                throw e;
            }
        }
    }

    public boolean isInstalled()
    {
        return _installed;
    }

    public String getClassName()
    {
        return _className;
    }

    public String getExtentions()
    {
        return _extentions;
    }

    public String getDescription()
    {
        return _description;
    }

    public OleContainer getContainer()
    {
        if (_container == null && isInstalled())
        {
            _container = new OleContainer();
            try
            {
                _container.createObject(_className);
            }
            catch (ComException e)
            {
//                _installed = false;
//                JOptionPane.showMessageDialog(null, e.getMessage(), "Unable to create ActiveX control.",
//                        JOptionPane.ERROR_MESSAGE);
            }
        }
        return _container;
    }

    public abstract void loadFile(String fileName);

    public JPanel createFileInfoPanel()
    {
        return null;
    }

    public void activate()
    {
        if (isInstalled())
        {
            final OleContainer container = getContainer();
            container.doVerb(OleVerbs.SHOW);
        }
    }

    public void destroy()
    {
        if (isInstalled())
        {
            getContainer().destroyObject();
        }
    }

    public ImageIcon getIcon()
    {
        if (_associatedIcon == null)
        {
            try
            {
                File temporaryEmptyFile = new File("temp." + _defaultAssociation);
                try
                {
                    temporaryEmptyFile.createNewFile();
                    _associatedIcon = new ImageIcon(SHFileInfo.getFileInfo(temporaryEmptyFile.getAbsolutePath(),
                            SHFileInfo.SHGFI_SMALLICON |
                            SHFileInfo.SHGFI_ICON).getIcon().toImage());
                }
                finally
                {
                    temporaryEmptyFile.delete();
                }
            }
            catch (IOException e)
            {
            }
        }
        return _associatedIcon;
    }
}
