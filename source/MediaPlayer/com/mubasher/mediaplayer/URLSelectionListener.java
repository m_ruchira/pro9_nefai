package com.mubasher.mediaplayer;

/**
 * Created by IntelliJ IDEA.
 * User: uditha
 * Date: Mar 2, 2009
 * Time: 11:02:17 AM
 */
public interface URLSelectionListener {
    public void URLSelected(URLComponentRecord url);
    public void editSelected(URLComponentRecord url);
    public void deleteSelected(URLComponentRecord url);
}
