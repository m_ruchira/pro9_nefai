package com.mubasher.csvr.optionscalculator;

import com.isi.csvr.iframe.InternalFrame;
import com.isi.csvr.table.Table;
import com.isi.csvr.util.TWSpinnerNumberModel;
import com.isi.csvr.shared.*;
import com.isi.csvr.properties.ViewSetting;
import com.isi.csvr.properties.ViewSettingsManager;
import com.isi.csvr.Client;
import com.isi.csvr.ShowMessage;
import com.isi.util.FlexGridLayout;

import javax.swing.*;
import javax.swing.JLabel;
import java.awt.*;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.Dimension;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;
import java.awt.event.ItemListener;
import java.awt.event.ItemEvent;
import java.util.ArrayList;


/**
 * Created by IntelliJ IDEA.
 * User: Udaya Athukorala
 * Date: Dec 1, 2008
 * Time: 12:10:38 PM
 * To change this template use File | Settings | File Templates.
 */
public class OptionCalculatorUI extends InternalFrame implements ItemListener {

    private static OptionCalculatorUI self;
    public static String COX_ROSS = "COX_ROSS";
    public static String BLACK_SCHOLES = "BLACK_SCHOLES";

    private Table resultTable;
    private TWButton calulate;
    OptionCalculatorModel omodel;

    ViewSetting oSetting;

    private JLabel lblOptionType;
    private JLabel lblSpotPrice;
    private JLabel lblStrikePrice;
    private JLabel lblVolatility;
    private JLabel lblInterestRates;
    private JLabel lblDividendYield;
    private JLabel lblDateUntExpiary;
    private JLabel lblNoOfSteps;
    private JLabel lblCalculationMtd;
    private JLabel lblactMktVal;

    private TWComboBox cmbOptionType;
    private TWComboBox cmbCalculationMtd;
    private JSpinner spinSpotPrice;
    private JSpinner spinStrikePrice;
    private JSpinner spinVolatility;
    private JSpinner spinInterestRates;
    private JSpinner spinDividendYield;
    private JSpinner spinDateUntExpiary;
    private JSpinner spinNoOfSteps;
    private JSpinner spinCalculationMtd;
    private JSpinner spinActMktVal;
    private JCheckBox calclImpliedVolatility;


    private ArrayList<Double> results;


    public static OptionCalculatorUI getSharedInstance() {
        if (self == null) {
            self = new OptionCalculatorUI();
        }
        return self;
    }


    private OptionCalculatorUI() {

        try {
            oSetting = ViewSettingsManager.getSummaryView("OPTIONS_CALCULATOR");
        } catch (Exception e) {
            e.printStackTrace();  //To change body of catch statement use File | Settings | File Templates.
            return;
        }
        GUISettings.setColumnSettings(oSetting, TWColumnSettings.getItem("OPTION_CALCULATOR"));
        results = new ArrayList<Double>(6);
        this.setSize(oSetting.getSize());
        this.setTitle(oSetting.getCaption());
        oSetting.setParent(this);
        this.setDefaultCloseOperation(JInternalFrame.HIDE_ON_CLOSE);
        this.setLayout(new BorderLayout(10, 10));
        this.setMaximizable(false);
        this.setPrintable(false);
        this.setShowServicesMenu(false);
        this.setIconifiable(false);
        this.setClosable(true);
        this.hideTitleBarMenu();

        initUI();
        populateComboBoxes();
        createTable();
        addComponents();
        cmbCalculationMtd.addItemListener(this);
        calclImpliedVolatility.addItemListener(this);
        this.setLayer(GUISettings.INTERNAL_DIALOG_LAYER);
        Client.getInstance().getDesktop().add(this);

    }

    private void initUI() {
        calulate = new TWButton(Language.getString("CALCULATE"));
        calulate.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent e) {
                calculate();
            }
        });

        lblOptionType = new JLabel(Language.getString("OPTION_TYPE"));
        lblSpotPrice = new JLabel(Language.getString("SPOT_PRICE"));
        lblStrikePrice = new JLabel(Language.getString("STRIKE_PRICE"));
        lblVolatility = new JLabel(Language.getString("VOLATILITY"));
        lblInterestRates = new JLabel(Language.getString("INTEREST_RATES"));
        lblDividendYield = new JLabel(Language.getString("DIVIDEND_YIELD"));
        lblDateUntExpiary = new JLabel(Language.getString("EXPIRAY_DATE"));
        lblNoOfSteps = new JLabel(Language.getString("NO_OF_STEPS"));
        lblCalculationMtd = new JLabel(Language.getString("CALC_MTD"));
        lblactMktVal = new JLabel(Language.getString("ACT_MKTVAL"));

        cmbOptionType = new TWComboBox();
        cmbCalculationMtd = new TWComboBox();
        cmbCalculationMtd.setPreferredSize(new Dimension(150, 25));

        spinSpotPrice = new JSpinner(new TWSpinnerNumberModel(0.00, 0.00, Double.MAX_VALUE, 0.01));
        spinStrikePrice = new JSpinner(new TWSpinnerNumberModel(0.00, 0.00, Double.MAX_VALUE, 0.01));
        spinVolatility = new JSpinner(new TWSpinnerNumberModel(0.00, 0.00, Double.MAX_VALUE, 0.01));
        spinInterestRates = new JSpinner(new TWSpinnerNumberModel(0.00, 0.00, Double.MAX_VALUE, 0.01));
        spinDividendYield = new JSpinner(new TWSpinnerNumberModel(0.00, 0.00, Double.MAX_VALUE, 0.01));
        spinDateUntExpiary = new JSpinner(new TWSpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        spinNoOfSteps = new JSpinner(new TWSpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        spinCalculationMtd = new JSpinner(new TWSpinnerNumberModel(0, 0, Integer.MAX_VALUE, 1));
        spinActMktVal = new JSpinner(new TWSpinnerNumberModel(0.00, 0.00, Double.MAX_VALUE, 0.01));
        calclImpliedVolatility = new JCheckBox(Language.getString("OC_IMPLIED_VOL_HEADING"), false);


    }

    private void createTable() {
        resultTable = new Table();
        omodel = new OptionCalculatorModel();
        omodel.setViewSettings(oSetting);
        resultTable.setModel(omodel);
        omodel.setTable(resultTable);
        resultTable.getModel().updateGUI();
        omodel.setDataStore(results);
        

    }

    private void populateComboBoxes() {

        cmbOptionType.addItem(new TWComboItem(1, Language.getString("OPT_CALL")));
        cmbOptionType.addItem(new TWComboItem(2, Language.getString("OPT_PUT")));


        cmbCalculationMtd.addItem(new TWComboItem(COX_ROSS, Language.getString("COX_ROSS")));
        cmbCalculationMtd.addItem(new TWComboItem(BLACK_SCHOLES, Language.getString("BLACK_SCHOLES")));


    }

    private void addComponents() {

        JPanel inputVals = new JPanel();
        JPanel inputVals2 = new JPanel(new FlowLayout(FlowLayout.LEFT, 0, 0));
        //inputVals.setBorder(BorderFactory.createTitledBorder(Language.getString("INPUT_VALUES")));

        JPanel inputValContainer = new JPanel();
        inputValContainer.setBorder(BorderFactory.createTitledBorder(Language.getString("INPUT_VALUES")));

        JPanel results = new JPanel();
        results.setBorder(BorderFactory.createTitledBorder(Language.getString("OUTPUT_VALUES")));


        FlexGridLayout inpLAyout = new FlexGridLayout(new String[]{"150", "60"}, new String[]{"20", "10", "20", "10", "20", "10", "20", "10", "20", "10", "20", "10", "20", "10", "20", "10", "20", "10", "20"});
        inputVals.setLayout(inpLAyout);

        FlexGridLayout outLayout = new FlexGridLayout(new String[]{"20", "100%", "33"}, new String[]{"20", "100%", "128"});
        results.setLayout(outLayout);

        JPanel bottomPanel = new JPanel();
        bottomPanel.setLayout(new BoxLayout(bottomPanel, BoxLayout.X_AXIS));

        inputVals.add(lblOptionType);
        inputVals.add(cmbOptionType);
        inputVals.add(new JLabel());
        inputVals.add(new JLabel());
        inputVals.add(lblSpotPrice);
        inputVals.add(spinSpotPrice);
        inputVals.add(new JLabel());
        inputVals.add(new JLabel());
        inputVals.add(lblStrikePrice);
        inputVals.add(spinStrikePrice);
        inputVals.add(new JLabel());
        inputVals.add(new JLabel());
        inputVals.add(lblVolatility);
        inputVals.add(spinVolatility);
        inputVals.add(new JLabel());
        inputVals.add(new JLabel());
        inputVals.add(lblInterestRates);
        inputVals.add(spinInterestRates);
        inputVals.add(new JLabel());
        inputVals.add(new JLabel());
        inputVals.add(lblDividendYield);
        inputVals.add(spinDividendYield);
        inputVals.add(new JLabel());
        inputVals.add(new JLabel());
        inputVals.add(lblDateUntExpiary);
        inputVals.add(spinDateUntExpiary);
        inputVals.add(new JLabel());
        inputVals.add(new JLabel());
        inputVals.add(lblNoOfSteps);
        inputVals.add(spinNoOfSteps);
        inputVals.add(new JLabel());
        inputVals.add(new JLabel());
        inputVals.add(lblactMktVal);
        inputVals.add(spinActMktVal);

        inputVals2.add(calclImpliedVolatility);

       // bottomPanel.setBorder(B);
        JPanel btmContainer = new JPanel();
        btmContainer.setLayout(new BorderLayout(5,5));
        bottomPanel.add(Box.createRigidArea(new Dimension(2,25)));
        bottomPanel.add(lblCalculationMtd);
        bottomPanel.add(Box.createRigidArea(new Dimension(10,25)));
        bottomPanel.add(cmbCalculationMtd);
        bottomPanel.add(Box.createHorizontalGlue());
        bottomPanel.add(calulate);
        bottomPanel.add(Box.createRigidArea(new Dimension(4,25)));
        btmContainer.add(bottomPanel, BorderLayout.CENTER);
        JPanel bar = new JPanel();
        bar.setPreferredSize(new Dimension(bottomPanel.getPreferredSize().width,5));
        btmContainer.add(bar, BorderLayout.SOUTH);
        //  resultTable.setPreferredSize(new Dimension(250,200));
        results.add(new JPanel());
        results.add(new JPanel());
        results.add(new JPanel());
        results.add(new JPanel());
        results.add(resultTable);
        results.add(new JPanel());
        results.add(new JPanel());
        results.add(new JPanel());
        results.add(new JPanel());

        inputValContainer.setLayout(new BorderLayout());
        inputValContainer.add(inputVals, BorderLayout.CENTER);
        inputValContainer.add(inputVals2, BorderLayout.SOUTH);

        // this.add(inputVals, BorderLayout.WEST);
        this.add(inputValContainer, BorderLayout.WEST);
        this.add(results, BorderLayout.CENTER);
        this.add(btmContainer, BorderLayout.SOUTH);

    }

    public void clearvalues(){
        spinSpotPrice.setValue(0.00d);
        spinStrikePrice.setValue(0.00d);
        spinVolatility.setValue(0.00d);
        spinInterestRates.setValue(0.00d);
        spinDividendYield.setValue(0.00d);
        spinDateUntExpiary.setValue(0);
        spinNoOfSteps.setValue(0);
        results.clear();
    }

    public void showWindow() {
        clearvalues();
        this.setVisible(true);
        this.setLocationRelativeTo(Client.getInstance().getDesktop());
        validateCheckBox();

    }

    public void calculate() {

        if (getSelectedCalculationMethod().equals(BLACK_SCHOLES) && isInputValid()) {
            double spot = (Double) spinSpotPrice.getValue();
            double strike = (Double) spinStrikePrice.getValue();
            double volatility = (Double) spinVolatility.getValue();
            double interest = (Double) spinInterestRates.getValue();
            double div = (Double) spinDividendYield.getValue();
            int days = (Integer) spinDateUntExpiary.getValue();
            double actMktVal = (Double) spinActMktVal.getValue();
            boolean isImpVolEnabled = calclImpliedVolatility.isSelected();
            
            BlackAndScholes.getSharedInstance().calculateTheoritaclPrice(getSelectedOptionType(), spot, ((double) days / 365), strike, interest, volatility, div, isImpVolEnabled, actMktVal);
            BlackAndScholes.getSharedInstance().fillDataToList(results);
            resultTable.getTable().updateUI();
        }else{
            if(isInputValid()){
                double spot = (Double) spinSpotPrice.getValue();
            double strike = (Double) spinStrikePrice.getValue();
            double volatility = (Double) spinVolatility.getValue();
            double interest = (Double) spinInterestRates.getValue();
            double div = (Double) spinDividendYield.getValue();
            int days = (Integer) spinDateUntExpiary.getValue();
            int steps = (Integer) spinNoOfSteps.getValue();
             BinomialCalculator.EnumCallPut tempEnum = BinomialCalculator.EnumCallPut.Put ;
             if(getSelectedOptionType().equalsIgnoreCase(Language.getString("OPT_CALL"))){
                  tempEnum = BinomialCalculator.EnumCallPut.Call;
             }
                BinomialCalculator calculator = new BinomialCalculator(spot,strike,((double) days / 365),volatility/100,interest/100,div/100,tempEnum, BinomialCalculator.EnumStyle.European,steps);
                calculator.GetOptionValue(results);
                 resultTable.getTable().updateUI();
            }
            
        }

    }

    private boolean isInputValid() {

        double spot = (Double) spinSpotPrice.getValue();
        if (spot <= 0) {
            new ShowMessage(Language.getString("OC_INVALID_SPOTPRICE"), "E");
            return false;
        }
        double strike = (Double) spinStrikePrice.getValue();
        if (strike <= 0) {
            new ShowMessage(Language.getString("OC_INVALID_STRIKEPRICE"), "E");
            return false;
        }
        if (!(getSelectedCalculationMethod().equals(BLACK_SCHOLES) && calclImpliedVolatility.isSelected())) {
        double volatility = (Double) spinVolatility.getValue();
        if (volatility <= 0) {
            new ShowMessage(Language.getString("OC_INVALID_VOLATILITY"), "E");
            return false;
        }
        }
        double interest = (Double) spinInterestRates.getValue();
        if (interest <= 0) {
            new ShowMessage(Language.getString("FVC_INVALID_INTEREST"), "E");
            return false;
        }
        double div = (Double) spinDividendYield.getValue();
        if (div <= 0) {
            new ShowMessage(Language.getString("OC_INVALID_DIVIDENDYIELD"), "E");
            return false;
        }

        if (getSelectedCalculationMethod().equals(BLACK_SCHOLES) && calclImpliedVolatility.isSelected()) {
            double actMkt = (Double) spinActMktVal.getValue();
            if (div <= 0) {
                new ShowMessage(Language.getString("OC_INVALID_ACTMKTVAL"), "E");
                return false;
            }
        }
        int days = (Integer) spinDateUntExpiary.getValue();
        if (days <= 0) {
            new ShowMessage(Language.getString("OC_INVALID_EXPIARY"), "E");
            return false;
        }
        if (getSelectedCalculationMethod().equals(COX_ROSS)) {
            int steps = (Integer) spinNoOfSteps.getValue();
            if (steps <= 0) {
                new ShowMessage(Language.getString("OC_INVALID_STEPS"), "E");
                return false;
            }

        }

        return true;


    }

    public void itemStateChanged(ItemEvent e) {
        if (e.getSource().equals(cmbCalculationMtd)) {
            TWComboItem item = (TWComboItem) cmbCalculationMtd.getSelectedItem();
            String selected = item.getValue();
            if (selected == Language.getString("COX_ROSS")) {
                spinNoOfSteps.setEnabled(true);
                calclImpliedVolatility.setEnabled(false);
                spinActMktVal.setEnabled(false);
                spinVolatility.setEnabled(true);

            } else {
                spinNoOfSteps.setEnabled(false);
                calclImpliedVolatility.setEnabled(true);

                if (calclImpliedVolatility.isSelected()) {
                    spinVolatility.setEnabled(false);
                    spinActMktVal.setEnabled(true);
                } else {
                    spinVolatility.setEnabled(true);
                    spinActMktVal.setEnabled(false);
                }
            }

            this.updateUI();
        } else if (e.getSource().equals(calclImpliedVolatility)) {
            if (calclImpliedVolatility.isSelected()) {
                spinVolatility.setEnabled(false);
                spinActMktVal.setEnabled(true);
            } else {
                spinVolatility.setEnabled(true);
                spinActMktVal.setEnabled(false);
            }


        }


    }

    private void validateCheckBox() {
        try {
            TWComboItem item = (TWComboItem) cmbCalculationMtd.getSelectedItem();
            String selected = item.getValue();
            if (selected == Language.getString("COX_ROSS")) {
                spinNoOfSteps.setEnabled(true);
                calclImpliedVolatility.setEnabled(false);
                spinActMktVal.setEnabled(false);
                spinVolatility.setEnabled(true);

            } else {
                spinNoOfSteps.setEnabled(false);
                calclImpliedVolatility.setEnabled(true);

                if (calclImpliedVolatility.isSelected()) {
                    spinVolatility.setEnabled(false);
                    spinActMktVal.setEnabled(true);
                } else {
                    spinVolatility.setEnabled(true);
                    spinActMktVal.setEnabled(false);
                }
            }
        } catch (Exception e) {

        }

    }

    public String getSelectedCalculationMethod() {
        TWComboItem item = (TWComboItem) cmbCalculationMtd.getSelectedItem();
        String selected = item.getId();
        return selected;

    }

    public String getSelectedOptionType(){
        TWComboItem item = (TWComboItem) cmbOptionType.getSelectedItem();
        String selected = item.getValue();
        return selected;
    }
}
