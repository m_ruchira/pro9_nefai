package com.isi.csvr.optionstrategymaker;

import com.isi.csvr.customizer.CommonTableSettings;
import com.isi.csvr.shared.Constants;
import com.isi.csvr.shared.Language;
import com.isi.csvr.shared.TWDecimalFormat;
import com.isi.csvr.table.SmartTable;
import com.isi.csvr.table.TWTableRenderer;
import com.isi.csvr.theme.Theme;

import javax.swing.*;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.TableCellRenderer;
import java.awt.*;


/**
 * Created by IntelliJ IDEA.
 * User: janithk
 * Date: Jan 19, 2009
 * Time: 10:25:41 AM
 * To change this template use File | Settings | File Templates.
 */

public class Strategy3DRenderer implements TableCellRenderer, TWTableRenderer {

    String[] g_asColumns;
    int[] g_asRendIDs;
    boolean g_bLTR;
    int g_iStringAlign;
    int g_iNumberAlign;

    String g_sNA = "NA";

    static Color g_oUpFGColor;
    static Color g_oDownFGColor;
    static Color g_oUpBGColor;
    static Color g_oDownBGColor;
    static Color g_oUpColor;
    static Color g_oDownColor;
    static Color g_oSelectedFG;
    static Color g_oSelectedBG;
    static Color g_oFG1;
    static Color g_oBG1;
    static Color g_oFG2;
    static Color g_oBG2;
    Color upColor;
    Color downColor;

    TWDecimalFormat oPriceFormat = new TWDecimalFormat(" ###,##0.00  ");
    TWDecimalFormat oQuantityFormat = new TWDecimalFormat(" ###,##0  ");
    TWDecimalFormat oPChangeFormat = new TWDecimalFormat(" ###,##0.00  ");
    TWDecimalFormat oPDecimalFormat = new TWDecimalFormat(" ###,##0.00000");

    private Color foreground = null;
    private Color background = null;
    private String columnName = null;

    private static ImageIcon upImage = null;
    private static ImageIcon downImage = null;
    private byte decimalPlaces = Constants.TWO_DECIMAL_PLACES;

    public Strategy3DRenderer(String[] asColumns, int[] asRendIDs) {

        g_asColumns = asColumns;
        g_asRendIDs = asRendIDs;
        g_bLTR = Language.isLTR();

        reload();
        g_sNA = " " + Language.getString("NA") + " ";

        if (Language.isLTR()) {
            g_iStringAlign = JLabel.CENTER;
            g_iNumberAlign = JLabel.RIGHT;
        } else {
            g_iStringAlign = JLabel.CENTER;
            g_iNumberAlign = JLabel.LEFT;
        }
    }

    public void initRenderer(String[] asColumns, int[] asRendIDs) {
    }

    public static void reload() {
        try {
            g_oUpFGColor = Theme.getColor("BOARD_TABLE_CELL_UP_FGCOLOR");
            g_oDownFGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_FGCOLOR");
            g_oUpBGColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownBGColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oUpColor = Theme.getColor("BOARD_TABLE_CELL_UP_BGCOLOR");
            g_oDownColor = Theme.getColor("BOARD_TABLE_CELL_DOWN_BGCOLOR");
            g_oSelectedFG = Theme.getColor("BOARD_TABLE_SELECTED_FGCOLOR");
            g_oSelectedBG = Theme.getColor("BOARD_TABLE_SELECTED_BGCOLOR");
            g_oFG1 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR1");
            g_oBG1 = Theme.getColor("STRATEGY_TABLE_CELL_BGCOLOR1");
            g_oFG2 = Theme.getColor("BOARD_TABLE_CELL_FGCOLOR2");
            g_oBG2 = Theme.getColor("STRATEGY_TABLE_CELL_BGCOLOR2");
        } catch (Exception e) {
            g_oUpBGColor = Color.green;
            g_oDownBGColor = Color.red;
            g_oUpFGColor = Color.black;
            g_oDownFGColor = Color.black;
            g_oUpColor = Color.green;
            g_oDownColor = Color.red;
            g_oSelectedFG = Color.black;
            g_oSelectedBG = Color.white;
            g_oFG1 = Color.white;
            g_oBG1 = Color.black;
            g_oFG2 = Color.white;
            g_oBG2 = Color.black;
        }
    }

    public void propertyChanged(int property) {
    }

    public static final DefaultTableCellRenderer DEFAULT_RENDERER = new DefaultTableCellRenderer();

    public Component getTableCellRendererComponent(JTable table, Object value,
                                                   boolean isSelected, boolean hasFocus, int row, int column) {
        Component renderer = DEFAULT_RENDERER.getTableCellRendererComponent(table, value,
                isSelected, hasFocus, row, column);

        JLabel lblRenderer = (JLabel) renderer;
        lblRenderer.setOpaque(true);

        boolean isCustomThemeEnabled = ((SmartTable) table).isCuatomThemeEnabled();
        CommonTableSettings sett = null;
        if (isCustomThemeEnabled) {
            sett = (CommonTableSettings) ((SmartTable) table).getTableSettings();
            if (isSelected) {
                foreground = sett.getSelectedColumnFG();
                background = sett.getSelectedColumnBG();
            } else {
                if (row % 2 == 0) {
                    foreground = sett.getRowColor1FG();
                    background = sett.getRowColor1BG();
                } else {
                    foreground = sett.getRowColor2FG();
                    background = sett.getRowColor2BG();
                }
            }
            upColor = sett.getPositiveChangeFG();
            downColor = sett.getNegativeChangeFG();
        } else {
            if (isSelected) {
                foreground = g_oSelectedFG;
                background = g_oSelectedBG;
            } else if (row % 2 == 0) {
                foreground = g_oFG1;
                background = g_oBG1;
            } else {
                foreground = g_oFG2;
                background = g_oBG2;
            }
            upColor = g_oUpColor;
            downColor = g_oDownColor;
        }

        if (row == table.getModel().getRowCount() / 2) {
            // lblRenderer.setBackground(g_oSelectedBG);
        }
        renderer.setForeground(foreground);
        renderer.setBackground(background);
        renderer.setFont(new Font("Arial", Font.PLAIN, 12));

        columnName = table.getColumnName(column);
        int iRendID = 'I';
        try {
            iRendID = g_asRendIDs[table.convertColumnIndexToModel(column)];
        } catch (Exception e) {
            e.printStackTrace();
        }

        try {

            lblRenderer.setIcon(null);
            switch (iRendID) {

                case 0: // % increment

                    if (String.valueOf(value).equals("0%")) {
                        lblRenderer.setText(Language.getString("STRATEGY_CENTRE"));
                    } else {
                        lblRenderer.setText(String.valueOf(value));
                    }
                    lblRenderer.setHorizontalAlignment(g_iStringAlign);

                    break;
                case 1: // other numbers
                    /*lblRenderer.setText(oPriceFormat.format(Float.parseFloat(String.valueOf(value))));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);*/
                    float floatValue = Float.parseFloat(String.valueOf(value));
                    lblRenderer.setText(oPriceFormat.format(floatValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    break;

                default:
                    floatValue = Float.parseFloat(String.valueOf(value));
                    lblRenderer.setText(oPriceFormat.format(floatValue));
                    lblRenderer.setHorizontalAlignment(g_iNumberAlign);
                    if (floatValue < 0) {
                        lblRenderer.setForeground(downColor);
                    } else {
                        lblRenderer.setForeground(upColor);
                    }
            }
        } catch (Exception e) {
            lblRenderer.setText("");
        }
        if (row == table.getModel().getRowCount() / 2) {
            lblRenderer.setBackground(Theme.getColor("STRATEGY_ROW_HIGHLIGHT_COLOR"));
        }
        foreground = null;
        background = null;
        lblRenderer = null;

        return renderer;
    }
}

